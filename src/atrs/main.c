/** \file
*
*   \brief Main block for AMICS COM-D firmware.
*
*   AMICS COM-D firmware. This firmware will be in charge of receiving beacons from the satellite, process them transmit
*   a packet to the satellite if needing while implementing CRDSA protocol. It will have communications capacities with the
*   PROC-D teriminal, and GPS module.
*
*   \author Javier Verde
*   \author Aistech Space
*
*/
//libs
#include <ax8052f143.h>
#include <libmftypes.h>
#include <libmfradio.h>
#include <libmfflash.h>
#include <libmfwtimer.h>
#include <libmfcrypto.h>
#include <string.h>
#include <stdlib.h>
#include <libmfdbglink.h>
#include <libdvk2leds.h>
// includes
#include "configmaster.h"
#include "UBLOX.h"
#include "easyax5043.h"
#include "UartComProc.h"
#include "misc.h"
#include "GoldSequence.h"
#include "Beacon.h"
#include "display_com0.h"
#include "configcommon.h"
#include "Memory.h"
#include "cryptography.h"

//Main software variables
uint8_t RfStateMachine = e_WaitForBeaconAndMessageSend; /**< \brief RF state machine actual state */
int UpLinkIsrFlag; /**< \brief Rx uplink beacon interruption flag */
int DownLinkIsrFlag; /**< \brief Rx downlink beacon interruption flag */
int ACKReceivedFlag; /**< \brief ACK reception flag */
int GPSMessageReadyFlag =0;
uint8_t BackOffCounter;
int ACKTimerFlag;
uint32_t freq = FREQUENCY_HZ -10000;
RF_beacon_t BeaconRx;/**< \brief Received beacon processed */
uint16_t __xdata *RxBuffer;/**< \brief  Verify this variable */
uint8_t i;/**< \brief counter */
uint8_t Tmr0Flg = 0;/**< \brief timer0 activation flag */
#if defined(SDCC)
extern uint8_t _start__stack[];
#endif
uint8_t counter = 0;/**< \brief counter */
uint16_t __data pkt_counter = 0;/**< \brief amount of packets received */
uint8_t __data coldstart = 1; /**< \brief Cold start indicator*/ // caution: initialization with 1 is necessary! Variables are initialized upon _sdcc_external_startup returning 0 -> the coldstart value returned from _sdcc_external startup does not survive in the coldstart case
typedef unsigned char bool;
__xdata  uint8_t *TxPackageOrig;
__xdata  uint8_t *TxPackageTwin1;
__xdata  uint8_t *TxPackageTwin2;
__xdata   uint8_t *AES_KEY;
__xdata  uint8_t *EncryptedData;
uint8_t  TxPacketLength;
uint8_t BackOffFlag;
uint8_t ChannelChangeActive;
int TimerDiscarded;


static void pwrmgmt_irq(void) __interrupt(INT_POWERMGMT)
{
    uint8_t pc = PCON;
    if (!(pc & 0x80))
        return;
    GPIOENABLE = 0;
    IE = EIE = E2IE = 0;
    for (;;)
        PCON |= 0x01;
}

void transmit_packet(void)
{
    static uint8_t i = 0;
    static uint8_t __xdata demo_packet_[sizeof(demo_packet)];
    axradio_set_mode(AXRADIO_MODE_ASYNC_TRANSMIT);
    ++pkt_counter;
    memcpy(demo_packet_, demo_packet, sizeof(demo_packet));
    if (framing_insert_counter) {
        demo_packet_[framing_counter_pos] = pkt_counter & 0xFF ;
        demo_packet_[framing_counter_pos+1] = (pkt_counter>>8) & 0xFF;
    }
       axradio_transmit(&remoteaddr, demo_packet_, sizeof(demo_packet));

}


void axradio_statuschange(struct axradio_status __xdata *st)
{

    int i = 0;
    switch (st->status)
    {
    case AXRADIO_STAT_TRANSMITSTART:
        led0_on();
        if (st->error == AXRADIO_ERR_RETRANSMISSION)
            led2_on();
        break;

    case AXRADIO_STAT_TRANSMITEND:
        led0_off();
        if (st->error == AXRADIO_ERR_NOERROR) {
            led2_off();

        } else if (st->error == AXRADIO_ERR_TIMEOUT) {
            led2_on();

        }
        if (st->error == AXRADIO_ERR_BUSY)
            led3_on();
        else
            led3_off();
        break;

#if RADIO_MODE == AXRADIO_MODE_SYNC_MASTER || RADIO_MODE == AXRADIO_MODE_SYNC_ACK_MASTER
    case AXRADIO_STAT_TRANSMITDATA:
        // in SYNC_MASTER mode, transmit data may be prepared between the call to TRANSMITEND until the call to TRANSMITSTART
        // TRANSMITDATA is called when the crystal oscillator is enabled, approximately 1ms before transmission
//        transmit_packet();
        break;
#endif

    case AXRADIO_STAT_CHANNELSTATE:
        if (st->u.cs.busy)
            led3_on();
        else
            led3_off();
        break;

    default:
        break;
    }
}


uint8_t _sdcc_external_startup(void)

{
    LPXOSCGM = 0x8A;
    wtimer0_setclksrc(WTIMER0_CLKSRC, WTIMER0_PRESCALER);
    wtimer1_setclksrc(CLKSRC_FRCOSC, 7);
    LPOSCCONFIG = 0x09; // Slow, PRESC /1, no cal. Does NOT enable LPOSC. LPOSC is enabled upon configuring WTCFGA (MODE_TX_PERIODIC and receive_ack() )
    coldstart = !(PCON & 0x40);
    ANALOGA = 0x18; // PA[3,4] LPXOSC, other PA are used as digital pins
    PORTA = 0x21;
    PORTB = 0xFD | (PINB & 0x02); // init LEDs to previous (frozen) state
    PORTC = 0x00; //
    PORTR = 0x0B; //
    DIRA = 0x3F; //
    DIRB = 0x0C; //  PB1 = LED; PB2 / PB3 are outputs (in case PWRAMP / ANSTSEL are used)
    DIRC = 0x10; //  PC4 = button
    DIRR = 0x15; //
    PALTA  = 0x00;//for comanding cirucuit outputs
    PALTB  = 0x11; // PB3 as aoutput
    PALTC  = 0x00;
    PINSEL |= 0xC0;//con esta linea no funciona el gpio, se usara al reves ?
    //random number generator
    RNGMODE = 0x0F;
    RNGCLKSRC0 = 0x09;
    RNGCLKSRC1 = 0x00;
    axradio_setup_pincfg1();
    DPS = 0;
    IE = 0x40;
    EIE = 0x13;//habilito interrupcion de UART + timer0 + timer 1
    IP_4 = 1; // radio interrupt high priority
    EIP_0 = 0; //timer0 interrupt high priority
    EIP_1 = 0;
    E2IE = 0x00;// Keep random number generation interrupt disabled, if needed a random number just check the variable, and wait for it
  //  E2IE_4 = 1;
  // //////////////////////// UART_Proc_PortInit();
 //   flash_pageerase(0x02); // for memory init, if its done after in the code it does not work
    //display_portinit();
    GPIOENABLE = 1; // unfreeze GPIO
#if defined(__ICC8051__)
    return coldstart;
#else
    return !coldstart; // coldstart -> return 0 -> var initialization; start from sleep -> return 1 -> no var initialization
#endif
}
/** \brief Main function
*
*   Program flow will be controled from this function
**/
void main(void)
{
// Variables declaration
    volatile uint8_t    PROCD_PayloadLength= 0;     /**< \var length of the payload received from PROC-D*/
    volatile uint8_t __xdata *PROCD_UartRxBuffer;   /**< \brief PROC-D UART interface buffer */
    uint8_t PROCD_Command;                          /**< \brief Command word received from PROC-D*/
    extern int UpLinkIsrFlag;
    extern int DownLinkIsrFlag;
    extern int ACKReceivedFlag;
    extern uint8_t BackOffCounter;
    uint8_t c=0;                                    /**< \brief counter */
    extern RF_beacon_t BeaconRx;
    extern uint8_t GPS_PoolFlag;
    __xdata uint16_t UBLOX_Rx_Length = 0;               /**< \brief UBLOX GPS received data length */
    __xdata uint8_t  *UBLOX_Rx_Payload;                 /**< \brief UBLOX GPS payload */
    uint32_t gold_sequence_tx;
    uint8_t rnd1;
    uint8_t rnd2;
    uint8_t rnd3;
    extern struct wtimer_desc __xdata ACKTimer;
    extern struct wtimer_desc __xdata Channel_change;
    extern struct wtimer_desc __xdata GPS_tmr;
    uint8_t RxBuff[10];
//End of variable declarations
#if !defined(SDCC) && !defined(__ICC8051__)
    _sdcc_external_startup();
#endif
#if defined(SDCC)
    __asm
    G$_start__stack$0$0 = __start__stack
                          .globl G$_start__stack$0$0
                          __endasm;
#endif


    EA = 1;
    wtimer_init();
    flash_apply_calibration();
    GOLDSEQUENCE_Init(); // ver que pasa con esta funcion*/
    CLKCON = 0x00;
#ifdef dbglink
 //   dbglink_init();
 //  dbglink_writestr("Gold sequence initialized");

    dbglink_init(); // inicio la UART de debug
    dbglink_writestr(" DIRB: ");
    dbglink_writehex16(DIRB,1,WRNUM_PADZERO);
    dbglink_writestr("\r\n PALTB: ");
    dbglink_writehex16(PALTB,1,WRNUM_PADZERO);
    dbglink_writestr("\r\n PINSEL: ");
    dbglink_writehex16(PINSEL,1,WRNUM_PADZERO);
    dbglink_writestr("\r\n PORTB_3: ");
    dbglink_writehex16(PORTB_3,1,WRNUM_PADZERO);
#endif
  //  DISABLE_LNA
 //   ENABLE_GPS
    if (coldstart) {
        // coldstart
        axradio_init();
        axradio_set_local_address(&localaddr);
        axradio_set_default_remote_address(&remoteaddr);
        delay_ms(lpxosc_settlingtime+1500);//this delay is important to let the radio init
        axradio_set_mode(RADIO_MODE);
    }
    else
    {
        // warmstart
        ax5043_commsleepexit();
        IE_4 = TRUE ;
    }
    axradio_setup_pincfg2();

    //state machine init

    UART_Proc_PortInit(); // inicio la UART con PC o PROC-D
#ifdef dbglink
    dbglink_writestr(" Comienza el software de testing ");
    dbglink_writestr("\r\n DIRB: ");
    dbglink_writehex16(DIRB,1,WRNUM_PADZERO);
    dbglink_writestr("\r\n PALTB: ");
    dbglink_writehex16(PALTB,1,WRNUM_PADZERO);
    dbglink_writestr("\r\n PINSEL: ");
    dbglink_writehex16(PINSEL,1,WRNUM_PADZERO);
    dbglink_writestr("\r\n PORTB_3: ");
    dbglink_writehex16(PORTB_3,1,WRNUM_PADZERO);
#endif // dbglink
    for(;;)
    {

             i = 0;
       RxBuff[0] = uart0_rx();


       switch(RxBuff[0])
       {
       case('a'):
           ENABLE_SW_CTRL1
           uart0_tx('a');
           #ifdef dbglink
           dbglink_writestr("ENABLE_SW_CTRL1 ");
           #endif // dbglink
        break;
       case('b'):
           DISABLE_SW_CTRL1
           uart0_tx('b');
           #ifdef dbglink
           dbglink_writestr("DISABLE_SW_CTRL1  ");
           #endif // dbglink
        break;
       case('c'):
           ENABLE_GPS
           uart0_tx('c');
           #ifdef dbglink
           dbglink_writestr("ENABLE_GPS ");
           #endif // dbglink
        break;
       case('d'):
           DISABLE_GPS
           uart0_tx('d');
           #ifdef dbglink
           dbglink_writestr("DISABLE_GPS ");
           #endif
        break;
       case('e'):
           ENABLE_LNA
           uart0_tx('e');
           #ifdef dbglink
           dbglink_writestr("ENABLE_LNA ");
           #endif
        break;
       case('f'):
           DISABLE_LNA
           uart0_tx('f');
           #ifdef dbglink
           dbglink_writestr("DISABLE_LNA ");
           #endif
        break;
       case('g'):
           GPS_WAKEUP
           uart0_tx('g');
           #ifdef dbglink
           dbglink_writestr("GPS_WAKEUP ");
           #endif
        break;
       case('h'):
           GPS_TURNOFF
           uart0_tx('h');
           #ifdef dbglink
           dbglink_writestr("GPS_TURNOFF ");
           #endif
        break;
       case('i'):
           GPS_RESET_ON
           uart0_tx('i');
           #ifdef dbglink
           dbglink_writestr("GPS_RESET_ON ");
           #endif
        break;
       case('j'):
           GPS_RESET_OFF
           uart0_tx('j');
           #ifdef dbglink
           dbglink_writestr("GPS_RESET_OFF ");
           #endif
        break;
       case('k'):
           ENABLE_PA_3_6V
           #ifdef dbglink
           uart0_tx('k');
           dbglink_writestr("ENABLE_PA_3.6V ");
           #endif
        break;
       case('l'):
           DISABLE_PA_3_6V
           uart0_tx('l');
           #ifdef dbglink
           dbglink_writestr("DISABLE_PA_3.6V ");
           #endif
        break;
       case('m'):
            UBLOX_GPS_PortInit();
            uart0_tx('m');
            break;
       case('n'):
            UBLOX_GPS_SendCommand_WaitACK(eNAV,POSLLH,0x00,UBLOX_Rx_Payload,ANS,&UBLOX_Rx_Length);
            #ifdef dbglink
            dbglink_writestr(" GPS payload: ");
            for(i=0;i<UBLOX_Rx_Length;i++)
                {
                dbglink_writestr(" ");
                dbglink_writehex16(UBLOX_Rx_Payload[i],1,WRNUM_PADZERO);
                }
            uart0_tx('n');
            #endif // dbglink
            uart0_tx('n');
            break;
       case('o'):
            transmit_packet();
            uart0_tx('o');
        break;
       default:
           #ifdef dbglink
           dbglink_writestr(" caracter erroneo recibido ");
           #endif
           dbglink_writehex16(RxBuff[0],1,WRNUM_PADZERO);
           uart0_tx('F');
           uart0_tx('F');
        break;
       }

    wtimer_runcallbacks();
    wtimer_idle(WTFLAG_CANSTANDBY);
    IE_4 = 1;
    EA = 1;
    }
}


