/**
*   \file
*   \brief Miscelaneus functions
*
*    This module contains function for complementing different tasks
*
*   \author Javier Verde
*   \author Aistech Space
*/
// Copyright (c) 2007,2008,2009,2010,2011,2012,2013, 2014 AXSEM AG
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//     1.Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     2.Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     3.Neither the name of AXSEM AG, Duebendorf nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//     4.All advertising materials mentioning features or use of this software
//       must display the following acknowledgement:
//       This product includes software developed by AXSEM AG and its contributors.
//     5.The usage of this source code is only granted for operation with AX5043
//       and AX8052F143. Porting to other radio or communication devices is
//       strictly prohibited.
//
// THIS SOFTWARE IS PROVIDED BY AXSEM AG AND CONTRIBUTORS ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL AXSEM AG AND CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

//libs
#include <ax8052.h>
#include <libmftypes.h>
#include <libmfwtimer.h>
#include <stdlib.h>
#include <string.h>
//includes
#include "misc.h"
#include "axradio.h"
#include "display_com0.h"
#include "configmaster.h"
#include "UBLOX.h"


static struct wtimer_desc __xdata delaymstimer;
struct wtimer_desc __xdata OriginalMessage_tmr;
struct wtimer_desc __xdata TwinMessage1_tmr;
struct wtimer_desc __xdata TwinMessage2_tmr;
struct wtimer_desc __xdata GPS_tmr;
struct wtimer_desc __xdata BackOff;
struct wtimer_desc __xdata ACKTimer;
struct wtimer_desc __xdata Channel_change;
uint8_t GPS_PoolFlag;
__xdata extern uint8_t *TxPackageOrig;
__xdata extern uint8_t *TxPackageTwin1;
__xdata extern uint8_t *TxPackageTwin2;
extern uint8_t TxPacketLength;
extern uint8_t BackOffCounter;


/**
* \brief Callback function for delay_ms
*
*
*
* @param[in] desc : timer descriptor
*
*/

 static void delayms_callback(struct wtimer_desc __xdata *desc)
{
    desc;
    delaymstimer.handler = 0;
}
/**
* Creates a delay of ms milliseconds
*
* @param[in] ms length of the delay in milliseconds
*/
 void delay_ms(uint16_t ms) __reentrant
{
    // scaling: 20e6/64/1e3=312.5=2^8+2^6-2^3+2^-1
    uint32_t x;
    wtimer_remove(&delaymstimer);
    x = ms;
    delaymstimer.time = ms >> 1;
    x <<= 3;
    delaymstimer.time -= x;
    x <<= 3;
    delaymstimer.time += x;
    x <<= 2;
    delaymstimer.time += x;
    delaymstimer.handler = delayms_callback;
    wtimer1_addrelative(&delaymstimer);
    wtimer_runcallbacks();
    do
    {
        wtimer_idle(WTFLAG_CANSTANDBY);
        wtimer_runcallbacks();
    } while (delaymstimer.handler);
}

/**
* \brief Callback function for transmitting the original message
*
*
*
* @param[in] desc : timer descriptor
*
*/
 void OriginalMessage_callback(struct wtimer_desc __xdata *desc)
{
    desc;
   //recargar el contador
//   axradio_transmit(&remoteaddr,OrigPacket,sizeof(OrigPacket));
    #ifdef dbglink
    dbglink_writestr(" transmit ");
    #endif // dbglink
  // axradio_set_mode(AXRADIO_MODE_ASYNC_TRANSMIT);
    axradio_transmit(&remoteaddr, TxPackageOrig, TxPacketLength);
   //Tmr0Flg = 0x01;
}

/**
* \brief Callback function for transmitting the first copy
*
*
*
* @param[in] desc : timer descriptor
*
*/
 void TwinMessage1_callback(struct wtimer_desc __xdata *desc)
{
   desc;
   //recargar el contador
 //  axradio_transmit(&remoteaddr,Copy1Packet,sizeof(Copy1Packet));
  // axradio_set_mode(AXRADIO_MODE_ASYNC_TRANSMIT);
    axradio_transmit(&remoteaddr, TxPackageTwin1, TxPacketLength);
}


/**
* \brief Callback function for the second copy
*
*
*
* @param[in] desc : timer descriptor
*
*/
 void TwinMessage2_callback(struct wtimer_desc __xdata *desc)
{
   desc;
   //recargar el contador
  //  axradio_set_mode(AXRADIO_MODE_ASYNC_TRANSMIT);
   axradio_transmit(&remoteaddr, TxPackageTwin2, TxPacketLength);
    //  delay_ms(5000);
}

/**
* \brief Callback function for GPS pooling
*
*
* This function enables a flag that will be poolled on the main software loop, this flag will indicate that is time to refresh the GPS information
*
* @param[in] desc : timer descriptor
*
*/
 void GPS_callback(struct wtimer_desc __xdata *desc)
{
   desc;
   #ifdef dbglink
   dbglink_writestr(" \r\n GPS callback \r\n");
   #endif // dbglink
   GPS_PoolFlag = 1;
  // transmit_packet(); solo cuando necesito enviar beacon desde el otro terminal
   wtimer0_remove(&GPS_tmr);
   GPS_tmr.time +=  (uint32_t) TIME_FOR_TIMER_SEC(GPS_POLL_TIME_S);//(uint32_t) TIME_FOR_TIMER_SEC(10); //+= ? o solo = ? verificar esto
   wtimer0_addrelative(&GPS_tmr);
   //IE = 0x40;
   //EA = 1;
}
/**
* \brief Callback function for doppler tracking
*
* This function is called whenever the doopler tracking timer interrupt is executed. The frequency of the radio is increase by 600Hz. The variations in frequency goes from 466.1 Mhz to 466.2 Mhz
*
* @param[in] desc : timer descriptor
*
*/
 void Channel_change_callback(struct wtimer_desc __xdata *desc)
{
    extern uint32_t freq;
    extern uint8_t ChannelChangeActive;
    desc;
  //  IE = 0; // deshabilito las interrupciones
    if(ChannelChangeActive)
    {
        if(freq << FREQUENCY_HZ -50000) freq = (FREQUENCY_HZ -10000);
        axradio_set_channel(FREQHZ_TO_AXFREQ(freq));
        freq +=600;
        dbglink_writestr(" frecuencia: ");
        wtimer0_remove(&Channel_change);
        dbglink_writenum32(freq,1,WRNUM_PADZERO);
        if( freq > FREQUENCY_HZ + 10000) freq = (FREQUENCY_HZ -10000);
        Channel_change.time +=  (uint32_t) TIME_FOR_TIMER_MSEC(1000);//(uint32_t) TIME_FOR_TIMER_SEC(10); //+= ? o solo = ? verificar esto
        wtimer0_addabsolute(&Channel_change);
    }
  //  IE = 1; // las vuelvo a habilitar
}

/**
* \brief Callback function for BackOff state counter
*
*
*
*
* @param[in] desc : timer descriptor
*
*/
 void BackOff_Callback(struct wtimer_desc __xdata *desc)
{
    extern uint8_t BackOffFlag;
    desc;
    BackOffFlag = 1;
    #ifdef dbglink
    dbglink_writestr(" \n backoff timer expired ");
    #endif // dbglink
    /*hacer algo con el backoff*/
}

/**
* \brief Callback function for ACK reception timer
*
*
*
* @param[in] desc : timer descriptor
*
*/
__reentrantb void ACK_Callback(struct wtimer_desc __xdata *desc)
{
    extern int ACKTimerFlag;
    desc;
    #ifdef dbglink
    dbglink_writestr("ACK timer expired");
    #endif // dbglink
    ACKTimerFlag = 1;

}

/**
* \brief sets up timer for three transmisions
*
* This function sets the value for the timer, after the beacon is received
* And the slot where the transmissions will be done are chosen
*
* @param[in] time1_ms : value in milliseconds for the first message
* @param[in] time2_ms : value in milliseconds for the second message
* @param[in] time3_ms : value in milliseconds for the third message
*
*/

void delay_tx(uint32_t time1_ms, uint32_t time2_ms, uint32_t time3_ms)
{
    wtimer0_remove(&OriginalMessage_tmr);
    OriginalMessage_tmr.handler = OriginalMessage_callback;//Handler for timer 0
    time1_ms = (uint32_t) TIME_FOR_TIMER_MSEC(time1_ms);
    OriginalMessage_tmr.time = time1_ms; //+= ? o solo = ? verificar esto
    wtimer0_addrelative(&OriginalMessage_tmr);
    wtimer0_remove(&TwinMessage1_tmr);
    TwinMessage1_tmr.handler = TwinMessage1_callback;//Handler for timer 0
    time2_ms = (uint32_t) TIME_FOR_TIMER_MSEC(time2_ms);
    TwinMessage1_tmr.time = time2_ms; //+= ? o solo = ? verificar esto
    wtimer0_addrelative(&TwinMessage1_tmr);
    wtimer0_remove(&TwinMessage2_tmr);
    TwinMessage2_tmr.handler = TwinMessage2_callback;//Handler for timer 0
    time3_ms = (uint32_t) TIME_FOR_TIMER_MSEC(time3_ms);
    TwinMessage2_tmr.time = time3_ms; //+= ? o solo = ? verificar esto
    wtimer0_addrelative(&TwinMessage2_tmr);
}

/**
* \brief sets up periodic timer for GPS refreshing
*
* This function  initialices timer1 for periodic GPS polling
*
* @param[in] GPS_Period_S : GPS refresh period in seconds
*
*/
void InitGPSPolling (uint32_t GPS_Period_S)
{
    GPS_tmr.handler = GPS_callback;//Handler for timer 0
    GPS_Period_S = (uint32_t) TIME_FOR_TIMER_SEC(GPS_Period_S);
    GPS_tmr.time = GPS_Period_S; //+= ? o solo = ? verificar esto
    wtimer0_remove(&GPS_tmr);
    wtimer0_addrelative(&GPS_tmr);
   // IE = 0x40;
  // EA = 1;
}

/**
* \brief sets up periodic timer for channel_change
*
*/
void InitChannelChange(void)
{
    Channel_change.handler = Channel_change_callback;
    Channel_change.time = (uint32_t) TIME_FOR_TIMER_MSEC(250);
    wtimer0_remove(&Channel_change);
    wtimer0_addrelative(&Channel_change);
}

/**
* \brief starts BackOff state timer
*
*
*/
 void StartBackoffTimer(void)
 {
    BackOff.handler = BackOff_Callback;
    BackOff.time = (uint32_t) TIME_FOR_TIMER_SEC(30+(RNGBYTE%60));
    #ifdef dbglink
    dbglink_writestr(" \n Backoff timer started \n");
    #endif // dbglink
    wtimer0_remove(&BackOff);
    wtimer0_addrelative(&BackOff);
 }

 /**
* \brief starts ACK reception timer
*
*
*/
  void StartACKTimer(void)
 {
     ACKTimer.handler = ACK_Callback;
     ACKTimer.time = (uint32_t) TIME_FOR_TIMER_SEC(/*(RNGBYTE%15)+*/5);
     wtimer0_remove(&ACKTimer);
     wtimer0_addrelative(&ACKTimer);
     #ifdef dbglink
      dbglink_writestr(" ACK timer started");
     #endif // dbglink
 }

/**
* \brief Function for swapping the value of two variables
*
* This function starts initialices timer1 for periodic GPS polling
*
* @param[in] a: First variable to swap
* @param[in] b: Second variable to swap
*
*/
void swap_variables(uint8_t *a,uint8_t *b)
{
    *a = *a+*b;
    *b= *a-*b;
    *a = *a-*b;
    return;
}

/**
* \brief Generates random slots for message TX
*
* @param[in] slot1 : slot number for the original message
* @param[in] slot2 : slot number for the first copy
* @param[in] slot3 : slot number for the second copy
*
*/
void GetRandomSlots(uint8_t* slot1, uint8_t* slot2, uint8_t* slot3)
{
    *slot1 = RND_NUMBER_TO_SLOTS(RNGBYTE);
    *slot2 = RND_NUMBER_TO_SLOTS(RNGBYTE);
    *slot3 = RND_NUMBER_TO_SLOTS(RNGBYTE);
    if(*slot1 == *slot2) *slot2 = RND_NUMBER_TO_SLOTS(RNGBYTE);
    if(*slot2 == *slot3) *slot3 = RND_NUMBER_TO_SLOTS(RNGBYTE);
    if(*slot1 == *slot3) *slot3 = RND_NUMBER_TO_SLOTS(RNGBYTE);
    //ordeno los tres numeros aleatorios
    if(*slot1 > *slot2) swap_variables(slot1,slot2);
    if(*slot2 > *slot3) swap_variables(slot2,slot3);
    if(*slot1 > *slot2) swap_variables(slot1,slot2);
}

/**
* \brief Generates random slots for message TX
*
* @param[in] GoldCode : Gold code generated
* @param[in] Copy1: Slot number of one copy of the message
* @param[in] Copy2: Slot number of the other copy of the message
* @param[in] Payload: Pointer to payload array
* @param[in] PayloadLength : Length of the payload
* @param[in] ack: ACK of previous downlink message
* @param[out] TxPackage: Pointer to the complete message array
* @param[out] TotalLength: Total length of the message (payload + header)
*
*/
void AssemblyPacket(uint32_t GoldCode, uint8_t Copy1, uint8_t Copy2, uint8_t *Payload, uint8_t PayloadLength, uint8_t ack, uint8_t *TxPackage, uint8_t *TotalLength)
{
    //recuperar board ID del a memoria
    uint32_t boardID = 0x123456;
    uint32_t HMAC= 0x12345678;
    uint8_t StdPrem = 0x1;
    uint32_t CRC32;

    *TxPackage =(uint8_t) (GoldCode & 0x000000FF);
    *(TxPackage+1) = (uint8_t) ((GoldCode & 0x0000FF00)>>8);
    *(TxPackage+2) = (uint8_t) ((GoldCode & 0x00FF0000)>>16);
    *(TxPackage+3) = (uint8_t) (((GoldCode & 0x7F000000)>>24)| (Copy1 & 0x01)<<7);
    *(TxPackage+4) = (uint8_t)  (Copy1 & 0xFE)>>1 | ((Copy2 & 0x07)<<5);
    *(TxPackage+5) = (uint8_t) ((Copy2 & 0x38)>>3) | (boardID & 0x00001F)<<3;
    *(TxPackage+6) = (uint8_t) ((boardID & 0x001FE0)>>5);
    *(TxPackage+7) = (uint8_t) ((boardID & 0x1FE000)>>13);
    *(TxPackage+8) = (uint8_t) ((boardID & 0xE0000000)>>21)| (StdPrem & 0x0001) <<4|(ack << 5 & 0x0001);//quedan los tres bits reservados
    memcpy(TxPackage+9,&HMAC,sizeof(uint32_t));
    memcpy(TxPackage+13,Payload,sizeof(uint8_t)*PayloadLength);
    CRC32 = crc_crc32_msb(TxPackage,13+PayloadLength,0xFFFFFFFF);
    memcpy(TxPackage+13+PayloadLength,&CRC32,sizeof(uint32_t));
    *TotalLength = 13+PayloadLength+4;
    return;

}


