/**
*   \file
*   \brief Cryptography module
*
*    This module containts cryptography functions
*
*   \author Javier Verde
*   \author Aistech Space
*/
//includes
#include "Memory.h"
#include "string.h"
#include "misc.h"
#include "stdlib.h"
//libs
#include <ax8052f143.h>
#include <libmfcrypto.h>
#include <libmfdbglink.h>


const uint8_t __code aes_sbox[256] = {
	0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76,
	0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0,
	0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15,
	0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75,
	0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84,
	0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf,
	0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8,
	0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2,
	0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73,
	0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb,
	0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79,
	0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08,
	0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a,
	0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e,
	0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf,
	0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16
};

/**
*  \brief Initialices cryptograpic registers
*
* @param[in]  AES_KEY_ADDR : X space address of the keystream buffer
* @param[in] *AES_IN_ADDR  : X space address of the input buffer
* @param[in] *AES_OUT_ADDR : X space address of the output buffer
*
*/

void CRYPTO_InitRegisters(uint16_t AES_KEY_ADDRESS, uint16_t AES_IN_ADDR, uint16_t AES_OUT_ADDR)
{
    AESCONFIG   = 0x8A; //10 rounds + CFB mode
    AESKEYADDR0 = AES_KEY_ADDRESS & 0x00FF;
    AESKEYADDR1 = (AES_KEY_ADDRESS & 0xFF00) << 8;
    AESINADDR0  = AES_IN_ADDR & 0x00FF;
    AESINADDR1  = (AES_IN_ADDR & 0xFF00) << 8;
    AESOUTADDR0 = AES_OUT_ADDR & 0x00FF;
    AESOUTADDR1 = (AES_OUT_ADDR & 0xFF00) << 8;
    return;
}

/**
* \brief Gets random AES key and expands it
*
* @param[in] AES_KEY_ADDRESS : buffer where the expanded key will be located
*
*  @return returns key number between 1 and 8
*/

uint8_t CRYPTO_KeySelectAndExpansion (uint8_t *AES_KEY_BUFFER)
{
    uint8_t Rnd;
    uint16_t *Key;
    uint8_t i;
    uint8_t __autodata wcnt;
	uint8_t __autodata w[4];
	uint8_t __autodata bnum;
	uint8_t __autodata rcon;
    Key = malloc(8*sizeof(uint16_t));
    Rnd = RNGBYTE;
    Rnd = (Rnd % 8);

    /*for(i=0;i<8;i++)
    {
        Key[i] = ReadFromMemory(MEM_CRYPTOGRAPHY_KEYS_START+(i*2)+(Rnd*16));
    }*/ //reemplazo por testing
    *Key = 0x1112;
    *(Key+1) = 0x3334;
    *(Key+2) = 0x0505;
    *(Key+3) = 0x0707;
    *(Key+4) = 0x0909;
    *(Key+5) = 0x0B0B;
    *(Key+6) = 0x0D0D;
    *(Key+7) = 0x0F0F;
  //  aes128_setup((uint8_t)Key,*AES_KEY_BUFFER);//porque key tiene q ser un array ee 16 posiciones
    // funciones reemplazadas
    memcpy((void*)AES_KEY_BUFFER,Key, 16); // agregar free

    dbglink_writehex16(*Key,1,WRNUM_PADZERO);
    dbglink_writehex16(*(Key+1),1,WRNUM_PADZERO);
    dbglink_writestr(" crytpo1: ");
    dbglink_writehex16(*(AES_KEY_BUFFER+2),1,WRNUM_PADZERO);
      dbglink_writestr("  ");
    dbglink_writehex16(*(AES_KEY_BUFFER+3),1,WRNUM_PADZERO);
	//aes_keyexp(AES_KEY_BUFFER, 4, 10); funcion reemplazada
	wcnt = 4 * (10+1);
	if (wcnt <= 4)
		return;
	wcnt -= 4;
	AES_KEY_BUFFER += 4 * 4;
	bnum = 0;
	rcon = 1;
	do {
		AES_KEY_BUFFER -= 4;
		w[0] = *AES_KEY_BUFFER++;
		w[1] = *AES_KEY_BUFFER++;
		w[2] = *AES_KEY_BUFFER++;
		w[3] = *AES_KEY_BUFFER++;
		if (!bnum) {
			/* RotWord */
			bnum = w[0];
			w[0] = w[1];
			w[1] = w[2];
			w[2] = w[3];
			w[3] = bnum;
			/* SubWord */
			w[0] = aes_sbox[w[0]];
			w[1] = aes_sbox[w[1]];
			w[2] = aes_sbox[w[2]];
			w[3] = aes_sbox[w[3]];
			/* xor rcon */
			w[0] ^= rcon;
			/* update rcon */
			bnum = (rcon & 0x80);
			rcon <<= 1;
			if (bnum)
				rcon ^= 0x1b;
			bnum = 0;
		}
		AES_KEY_BUFFER -= 4 * 4;
		w[0] ^= *AES_KEY_BUFFER++;
		w[1] ^= *AES_KEY_BUFFER++;
		w[2] ^= *AES_KEY_BUFFER++;
		w[3] ^= *AES_KEY_BUFFER++;
		AES_KEY_BUFFER += 4 * 4 - 4;
		*AES_KEY_BUFFER++ = w[0];
		*AES_KEY_BUFFER++ = w[1];
		*AES_KEY_BUFFER++ = w[2];
		*AES_KEY_BUFFER++ = w[3];
		++bnum;
		if (bnum >= 4)
			bnum = 0;
	} while (--wcnt);

// fin de funciones reemplazadas

	AESCONFIG = (AESCONFIG & 0xC0) | 10;
	AESKEYADDR0 = (uint16_t)AES_KEY_BUFFER;
	AESKEYADDR1 = ((uint16_t)AES_KEY_BUFFER) >> 8;

    return Rnd;
}

/**
*
* \brief Encrypts data using AES, key and output buffer are stated on previous functions
*
* @param[in] *payload : data that needs to be encrypted
* @param[in] payloadLength : Length of the data to be encrypted
*
* @return Returns 1 if succesfully encrypted
*/

uint8_t CRYPTO_EncryptData(uint8_t *payload)
{
    memcpy(AESINADDR0|(0xFF00 &(AESINADDR1<<8)),payload,32);
    AESMODE = 0x82; //starts encryption + encrypts 2 blocks of 16 bytes
    do
    {
        delay_ms(1);
    }while(AESCURBLOCK & 0x80);
    return 1;
}



















