/**
*   \file
*   \brief Flash memory driver
*
*    This module contains
*
*   \author Javier Verde
*   \author Aistech Space
*/
#include <stdlib.h>
#include <string.h>
#include "Memory.h"
#include <libmfflash.h>
#include "misc.h"
#include <libmfdbglink.h>

//memory read
/**
*   \brief Reads the amount of byte requested
*
*   @param[out] uint8_t *RxBuffer : pointer to buffer where the data is needed
*/
uint16_t ReadFromMemory( uint16_t address)
{
    uint16_t RetVal;
    flash_unlock();
    RetVal = flash_read(address);
    flash_lock();
    return RetVal;
}


//memory write
/**
*   \brief Writes a memory address
*
*    Writes data in memory address, page has to be erased before writting
*
*   @param[in] uint16_t address
*   @param[in] uint16_t data
*   @param[in] bool erase_flag
*/

void WriteMemory(uint16_t address, uint16_t data, char erase_flag)
{
   // flash_unlock();
  /*  if(erase_flag)
    {
    flash_pageerase(address);
    }*/
    flash_write(address,data);
    //flash_wait(65535);
    //flash_lock();

}

/**
*   \brief Erases a memory page
*
*    Memory driver is only capable of flash bits from 1 to 0, therefore in order to write any memory byte it is necesarry to
*    Erase the whole page ( unless consecutive data is being written)
*
*   @param[in] uint16_t address

*/
void EraseMemoryPage(uint16_t address)
{
    EA = 0;
    flash_unlock();
    NVADDR0 = (char) address & 0x00FF;
	NVADDR1 = (char) (address >> 8 & 0x00FF);
	NVSTATUS = 0x20;
	//flash_wait(6510);
	//delay_ms(5);
    flash_lock();
     EA = 1;
}
