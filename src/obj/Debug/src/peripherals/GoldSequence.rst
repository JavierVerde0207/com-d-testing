                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.6.0 #9615 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module GoldSequence
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _flash_read
                                     12 	.globl _flash_write
                                     13 	.globl _flash_lock
                                     14 	.globl _flash_unlock
                                     15 	.globl _random
                                     16 	.globl _aligned_alloc_PARM_2
                                     17 	.globl _GOLDSEQUENCE_Init
                                     18 	.globl _GOLDSEQUENCE_Obtain
                                     19 ;--------------------------------------------------------
                                     20 ; special function registers
                                     21 ;--------------------------------------------------------
                                     22 	.area RSEG    (ABS,DATA)
      000000                         23 	.org 0x0000
                                     24 ;--------------------------------------------------------
                                     25 ; special function bits
                                     26 ;--------------------------------------------------------
                                     27 	.area RSEG    (ABS,DATA)
      000000                         28 	.org 0x0000
                                     29 ;--------------------------------------------------------
                                     30 ; overlayable register banks
                                     31 ;--------------------------------------------------------
                                     32 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                         33 	.ds 8
                                     34 ;--------------------------------------------------------
                                     35 ; internal ram data
                                     36 ;--------------------------------------------------------
                                     37 	.area DSEG    (DATA)
                                     38 ;--------------------------------------------------------
                                     39 ; overlayable items in internal ram 
                                     40 ;--------------------------------------------------------
                                     41 ;--------------------------------------------------------
                                     42 ; indirectly addressable internal ram data
                                     43 ;--------------------------------------------------------
                                     44 	.area ISEG    (DATA)
                                     45 ;--------------------------------------------------------
                                     46 ; absolute internal ram data
                                     47 ;--------------------------------------------------------
                                     48 	.area IABS    (ABS,DATA)
                                     49 	.area IABS    (ABS,DATA)
                                     50 ;--------------------------------------------------------
                                     51 ; bit data
                                     52 ;--------------------------------------------------------
                                     53 	.area BSEG    (BIT)
                                     54 ;--------------------------------------------------------
                                     55 ; paged external ram data
                                     56 ;--------------------------------------------------------
                                     57 	.area PSEG    (PAG,XDATA)
                                     58 ;--------------------------------------------------------
                                     59 ; external ram data
                                     60 ;--------------------------------------------------------
                                     61 	.area XSEG    (XDATA)
      000378                         62 _aligned_alloc_PARM_2:
      000378                         63 	.ds 2
                           00FC06    64 _flash_deviceid	=	0xfc06
                                     65 ;--------------------------------------------------------
                                     66 ; absolute external ram data
                                     67 ;--------------------------------------------------------
                                     68 	.area XABS    (ABS,XDATA)
                                     69 ;--------------------------------------------------------
                                     70 ; external initialized ram data
                                     71 ;--------------------------------------------------------
                                     72 	.area XISEG   (XDATA)
                                     73 	.area HOME    (CODE)
                                     74 	.area GSINIT0 (CODE)
                                     75 	.area GSINIT1 (CODE)
                                     76 	.area GSINIT2 (CODE)
                                     77 	.area GSINIT3 (CODE)
                                     78 	.area GSINIT4 (CODE)
                                     79 	.area GSINIT5 (CODE)
                                     80 	.area GSINIT  (CODE)
                                     81 	.area GSFINAL (CODE)
                                     82 	.area CSEG    (CODE)
                                     83 ;--------------------------------------------------------
                                     84 ; global & static initialisations
                                     85 ;--------------------------------------------------------
                                     86 	.area HOME    (CODE)
                                     87 	.area GSINIT  (CODE)
                                     88 	.area GSFINAL (CODE)
                                     89 	.area GSINIT  (CODE)
                                     90 ;--------------------------------------------------------
                                     91 ; Home
                                     92 ;--------------------------------------------------------
                                     93 	.area HOME    (CODE)
                                     94 	.area HOME    (CODE)
                                     95 ;--------------------------------------------------------
                                     96 ; code
                                     97 ;--------------------------------------------------------
                                     98 	.area CSEG    (CODE)
                                     99 ;------------------------------------------------------------
                                    100 ;Allocation info for local variables in function 'GOLDSEQUENCE_Init'
                                    101 ;------------------------------------------------------------
                                    102 ;	peripherals\GoldSequence.c:26: void GOLDSEQUENCE_Init(void)
                                    103 ;	-----------------------------------------
                                    104 ;	 function GOLDSEQUENCE_Init
                                    105 ;	-----------------------------------------
      00516B                        106 _GOLDSEQUENCE_Init:
                           000007   107 	ar7 = 0x07
                           000006   108 	ar6 = 0x06
                           000005   109 	ar5 = 0x05
                           000004   110 	ar4 = 0x04
                           000003   111 	ar3 = 0x03
                           000002   112 	ar2 = 0x02
                           000001   113 	ar1 = 0x01
                           000000   114 	ar0 = 0x00
                                    115 ;	peripherals\GoldSequence.c:29: flash_unlock();
      00516B 12 73 99         [24]  116 	lcall	_flash_unlock
                                    117 ;	peripherals\GoldSequence.c:32: flash_write(0x0000,0b1111100110100100);
      00516E 90 04 64         [24]  118 	mov	dptr,#_flash_write_PARM_2
      005171 74 A4            [12]  119 	mov	a,#0xa4
      005173 F0               [24]  120 	movx	@dptr,a
      005174 74 F9            [12]  121 	mov	a,#0xf9
      005176 A3               [24]  122 	inc	dptr
      005177 F0               [24]  123 	movx	@dptr,a
      005178 90 00 00         [24]  124 	mov	dptr,#0x0000
      00517B 12 71 D2         [24]  125 	lcall	_flash_write
                                    126 ;	peripherals\GoldSequence.c:34: flash_write(0x0002,0b001010111011000);
      00517E 90 04 64         [24]  127 	mov	dptr,#_flash_write_PARM_2
      005181 74 D8            [12]  128 	mov	a,#0xd8
      005183 F0               [24]  129 	movx	@dptr,a
      005184 74 15            [12]  130 	mov	a,#0x15
      005186 A3               [24]  131 	inc	dptr
      005187 F0               [24]  132 	movx	@dptr,a
      005188 90 00 02         [24]  133 	mov	dptr,#0x0002
      00518B 12 71 D2         [24]  134 	lcall	_flash_write
                                    135 ;	peripherals\GoldSequence.c:37: flash_write(0x0004,0b1111100100110000);
      00518E 90 04 64         [24]  136 	mov	dptr,#_flash_write_PARM_2
      005191 74 30            [12]  137 	mov	a,#0x30
      005193 F0               [24]  138 	movx	@dptr,a
      005194 74 F9            [12]  139 	mov	a,#0xf9
      005196 A3               [24]  140 	inc	dptr
      005197 F0               [24]  141 	movx	@dptr,a
      005198 90 00 04         [24]  142 	mov	dptr,#0x0004
      00519B 12 71 D2         [24]  143 	lcall	_flash_write
                                    144 ;	peripherals\GoldSequence.c:39: flash_write(0x0006,0b101101010001110);
      00519E 90 04 64         [24]  145 	mov	dptr,#_flash_write_PARM_2
      0051A1 74 8E            [12]  146 	mov	a,#0x8e
      0051A3 F0               [24]  147 	movx	@dptr,a
      0051A4 74 5A            [12]  148 	mov	a,#0x5a
      0051A6 A3               [24]  149 	inc	dptr
      0051A7 F0               [24]  150 	movx	@dptr,a
      0051A8 90 00 06         [24]  151 	mov	dptr,#0x0006
      0051AB 12 71 D2         [24]  152 	lcall	_flash_write
                                    153 ;	peripherals\GoldSequence.c:42: flash_write(0x0008,0b0000000010010100);
      0051AE 90 04 64         [24]  154 	mov	dptr,#_flash_write_PARM_2
      0051B1 74 94            [12]  155 	mov	a,#0x94
      0051B3 F0               [24]  156 	movx	@dptr,a
      0051B4 E4               [12]  157 	clr	a
      0051B5 A3               [24]  158 	inc	dptr
      0051B6 F0               [24]  159 	movx	@dptr,a
      0051B7 90 00 08         [24]  160 	mov	dptr,#0x0008
      0051BA 12 71 D2         [24]  161 	lcall	_flash_write
                                    162 ;	peripherals\GoldSequence.c:44: flash_write(0x000A,0b100111101010110);
      0051BD 90 04 64         [24]  163 	mov	dptr,#_flash_write_PARM_2
      0051C0 74 56            [12]  164 	mov	a,#0x56
      0051C2 F0               [24]  165 	movx	@dptr,a
      0051C3 74 4F            [12]  166 	mov	a,#0x4f
      0051C5 A3               [24]  167 	inc	dptr
      0051C6 F0               [24]  168 	movx	@dptr,a
      0051C7 90 00 0A         [24]  169 	mov	dptr,#0x000a
      0051CA 12 71 D2         [24]  170 	lcall	_flash_write
                                    171 ;	peripherals\GoldSequence.c:47: flash_write(0x000C,0b1000010100111100);
      0051CD 90 04 64         [24]  172 	mov	dptr,#_flash_write_PARM_2
      0051D0 74 3C            [12]  173 	mov	a,#0x3c
      0051D2 F0               [24]  174 	movx	@dptr,a
      0051D3 74 85            [12]  175 	mov	a,#0x85
      0051D5 A3               [24]  176 	inc	dptr
      0051D6 F0               [24]  177 	movx	@dptr,a
      0051D7 90 00 0C         [24]  178 	mov	dptr,#0x000c
      0051DA 12 71 D2         [24]  179 	lcall	_flash_write
                                    180 ;	peripherals\GoldSequence.c:49: flash_write(0x000E,0b011100010011111);
      0051DD 90 04 64         [24]  181 	mov	dptr,#_flash_write_PARM_2
      0051E0 74 9F            [12]  182 	mov	a,#0x9f
      0051E2 F0               [24]  183 	movx	@dptr,a
      0051E3 74 38            [12]  184 	mov	a,#0x38
      0051E5 A3               [24]  185 	inc	dptr
      0051E6 F0               [24]  186 	movx	@dptr,a
      0051E7 90 00 0E         [24]  187 	mov	dptr,#0x000e
      0051EA 12 71 D2         [24]  188 	lcall	_flash_write
                                    189 ;	peripherals\GoldSequence.c:52: flash_write(0x0010,0b0100011111101000);
      0051ED 90 04 64         [24]  190 	mov	dptr,#_flash_write_PARM_2
      0051F0 74 E8            [12]  191 	mov	a,#0xe8
      0051F2 F0               [24]  192 	movx	@dptr,a
      0051F3 74 47            [12]  193 	mov	a,#0x47
      0051F5 A3               [24]  194 	inc	dptr
      0051F6 F0               [24]  195 	movx	@dptr,a
      0051F7 90 00 10         [24]  196 	mov	dptr,#0x0010
      0051FA 12 71 D2         [24]  197 	lcall	_flash_write
                                    198 ;	peripherals\GoldSequence.c:54: flash_write(0x0012,0b000001101111011);
      0051FD 90 04 64         [24]  199 	mov	dptr,#_flash_write_PARM_2
      005200 74 7B            [12]  200 	mov	a,#0x7b
      005202 F0               [24]  201 	movx	@dptr,a
      005203 74 03            [12]  202 	mov	a,#0x03
      005205 A3               [24]  203 	inc	dptr
      005206 F0               [24]  204 	movx	@dptr,a
      005207 90 00 12         [24]  205 	mov	dptr,#0x0012
      00520A 12 71 D2         [24]  206 	lcall	_flash_write
                                    207 ;	peripherals\GoldSequence.c:57: flash_write(0x0014,0b0010011010000010);
      00520D 90 04 64         [24]  208 	mov	dptr,#_flash_write_PARM_2
      005210 74 82            [12]  209 	mov	a,#0x82
      005212 F0               [24]  210 	movx	@dptr,a
      005213 74 26            [12]  211 	mov	a,#0x26
      005215 A3               [24]  212 	inc	dptr
      005216 F0               [24]  213 	movx	@dptr,a
      005217 90 00 14         [24]  214 	mov	dptr,#0x0014
      00521A 12 71 D2         [24]  215 	lcall	_flash_write
                                    216 ;	peripherals\GoldSequence.c:59: flash_write(0x0016,0b001111010001001);
      00521D 90 04 64         [24]  217 	mov	dptr,#_flash_write_PARM_2
      005220 74 89            [12]  218 	mov	a,#0x89
      005222 F0               [24]  219 	movx	@dptr,a
      005223 74 1E            [12]  220 	mov	a,#0x1e
      005225 A3               [24]  221 	inc	dptr
      005226 F0               [24]  222 	movx	@dptr,a
      005227 90 00 16         [24]  223 	mov	dptr,#0x0016
      00522A 12 71 D2         [24]  224 	lcall	_flash_write
                                    225 ;	peripherals\GoldSequence.c:62: flash_write(0x0018,0b0001011000110111);
      00522D 90 04 64         [24]  226 	mov	dptr,#_flash_write_PARM_2
      005230 74 37            [12]  227 	mov	a,#0x37
      005232 F0               [24]  228 	movx	@dptr,a
      005233 74 16            [12]  229 	mov	a,#0x16
      005235 A3               [24]  230 	inc	dptr
      005236 F0               [24]  231 	movx	@dptr,a
      005237 90 00 18         [24]  232 	mov	dptr,#0x0018
      00523A 12 71 D2         [24]  233 	lcall	_flash_write
                                    234 ;	peripherals\GoldSequence.c:64: flash_write(0x001A,0b001000001110000);
      00523D 90 04 64         [24]  235 	mov	dptr,#_flash_write_PARM_2
      005240 74 70            [12]  236 	mov	a,#0x70
      005242 F0               [24]  237 	movx	@dptr,a
      005243 74 10            [12]  238 	mov	a,#0x10
      005245 A3               [24]  239 	inc	dptr
      005246 F0               [24]  240 	movx	@dptr,a
      005247 90 00 1A         [24]  241 	mov	dptr,#0x001a
      00524A 12 71 D2         [24]  242 	lcall	_flash_write
                                    243 ;	peripherals\GoldSequence.c:67: flash_write(0x001C,0b1000111001101101);
      00524D 90 04 64         [24]  244 	mov	dptr,#_flash_write_PARM_2
      005250 74 6D            [12]  245 	mov	a,#0x6d
      005252 F0               [24]  246 	movx	@dptr,a
      005253 74 8E            [12]  247 	mov	a,#0x8e
      005255 A3               [24]  248 	inc	dptr
      005256 F0               [24]  249 	movx	@dptr,a
      005257 90 00 1C         [24]  250 	mov	dptr,#0x001c
      00525A 12 71 D2         [24]  251 	lcall	_flash_write
                                    252 ;	peripherals\GoldSequence.c:69: flash_write(0x001E,0b101011100001100);
      00525D 90 04 64         [24]  253 	mov	dptr,#_flash_write_PARM_2
      005260 74 0C            [12]  254 	mov	a,#0x0c
      005262 F0               [24]  255 	movx	@dptr,a
      005263 74 57            [12]  256 	mov	a,#0x57
      005265 A3               [24]  257 	inc	dptr
      005266 F0               [24]  258 	movx	@dptr,a
      005267 90 00 1E         [24]  259 	mov	dptr,#0x001e
      00526A 12 71 D2         [24]  260 	lcall	_flash_write
                                    261 ;	peripherals\GoldSequence.c:72: flash_write(0x0020,0b1100001001000000);
      00526D 90 04 64         [24]  262 	mov	dptr,#_flash_write_PARM_2
      005270 74 40            [12]  263 	mov	a,#0x40
      005272 F0               [24]  264 	movx	@dptr,a
      005273 74 C2            [12]  265 	mov	a,#0xc2
      005275 A3               [24]  266 	inc	dptr
      005276 F0               [24]  267 	movx	@dptr,a
      005277 90 00 20         [24]  268 	mov	dptr,#0x0020
      00527A 12 71 D2         [24]  269 	lcall	_flash_write
                                    270 ;	peripherals\GoldSequence.c:74: flash_write(0x0022,0b111010010110010);
      00527D 90 04 64         [24]  271 	mov	dptr,#_flash_write_PARM_2
      005280 74 B2            [12]  272 	mov	a,#0xb2
      005282 F0               [24]  273 	movx	@dptr,a
      005283 74 74            [12]  274 	mov	a,#0x74
      005285 A3               [24]  275 	inc	dptr
      005286 F0               [24]  276 	movx	@dptr,a
      005287 90 00 22         [24]  277 	mov	dptr,#0x0022
      00528A 12 71 D2         [24]  278 	lcall	_flash_write
                                    279 ;	peripherals\GoldSequence.c:77: flash_write(0x0024,0b1110010001010110);
      00528D 90 04 64         [24]  280 	mov	dptr,#_flash_write_PARM_2
      005290 74 56            [12]  281 	mov	a,#0x56
      005292 F0               [24]  282 	movx	@dptr,a
      005293 74 E4            [12]  283 	mov	a,#0xe4
      005295 A3               [24]  284 	inc	dptr
      005296 F0               [24]  285 	movx	@dptr,a
      005297 90 00 24         [24]  286 	mov	dptr,#0x0024
      00529A 12 71 D2         [24]  287 	lcall	_flash_write
                                    288 ;	peripherals\GoldSequence.c:79: flash_write(0x0026,0b010010101101101);
      00529D 90 04 64         [24]  289 	mov	dptr,#_flash_write_PARM_2
      0052A0 74 6D            [12]  290 	mov	a,#0x6d
      0052A2 F0               [24]  291 	movx	@dptr,a
      0052A3 74 25            [12]  292 	mov	a,#0x25
      0052A5 A3               [24]  293 	inc	dptr
      0052A6 F0               [24]  294 	movx	@dptr,a
      0052A7 90 00 26         [24]  295 	mov	dptr,#0x0026
      0052AA 12 71 D2         [24]  296 	lcall	_flash_write
                                    297 ;	peripherals\GoldSequence.c:82: flash_write(0x0028,0b0111011101011101);
      0052AD 90 04 64         [24]  298 	mov	dptr,#_flash_write_PARM_2
      0052B0 74 5D            [12]  299 	mov	a,#0x5d
      0052B2 F0               [24]  300 	movx	@dptr,a
      0052B3 74 77            [12]  301 	mov	a,#0x77
      0052B5 A3               [24]  302 	inc	dptr
      0052B6 F0               [24]  303 	movx	@dptr,a
      0052B7 90 00 28         [24]  304 	mov	dptr,#0x0028
      0052BA 12 71 D2         [24]  305 	lcall	_flash_write
                                    306 ;	peripherals\GoldSequence.c:84: flash_write(0x002A,0b000110110000010);
      0052BD 90 04 64         [24]  307 	mov	dptr,#_flash_write_PARM_2
      0052C0 74 82            [12]  308 	mov	a,#0x82
      0052C2 F0               [24]  309 	movx	@dptr,a
      0052C3 74 0D            [12]  310 	mov	a,#0x0d
      0052C5 A3               [24]  311 	inc	dptr
      0052C6 F0               [24]  312 	movx	@dptr,a
      0052C7 90 00 2A         [24]  313 	mov	dptr,#0x002a
      0052CA 12 71 D2         [24]  314 	lcall	_flash_write
                                    315 ;	peripherals\GoldSequence.c:87: flash_write(0x002C,0b1011111011011000);
      0052CD 90 04 64         [24]  316 	mov	dptr,#_flash_write_PARM_2
      0052D0 74 D8            [12]  317 	mov	a,#0xd8
      0052D2 F0               [24]  318 	movx	@dptr,a
      0052D3 74 BE            [12]  319 	mov	a,#0xbe
      0052D5 A3               [24]  320 	inc	dptr
      0052D6 F0               [24]  321 	movx	@dptr,a
      0052D7 90 00 2C         [24]  322 	mov	dptr,#0x002c
      0052DA 12 71 D2         [24]  323 	lcall	_flash_write
                                    324 ;	peripherals\GoldSequence.c:89: flash_write(0x002E,0b101100111110101);
      0052DD 90 04 64         [24]  325 	mov	dptr,#_flash_write_PARM_2
      0052E0 74 F5            [12]  326 	mov	a,#0xf5
      0052E2 F0               [24]  327 	movx	@dptr,a
      0052E3 74 59            [12]  328 	mov	a,#0x59
      0052E5 A3               [24]  329 	inc	dptr
      0052E6 F0               [24]  330 	movx	@dptr,a
      0052E7 90 00 2E         [24]  331 	mov	dptr,#0x002e
      0052EA 12 71 D2         [24]  332 	lcall	_flash_write
                                    333 ;	peripherals\GoldSequence.c:92: flash_write(0x0030,0b0101101000011010);
      0052ED 90 04 64         [24]  334 	mov	dptr,#_flash_write_PARM_2
      0052F0 74 1A            [12]  335 	mov	a,#0x1a
      0052F2 F0               [24]  336 	movx	@dptr,a
      0052F3 74 5A            [12]  337 	mov	a,#0x5a
      0052F5 A3               [24]  338 	inc	dptr
      0052F6 F0               [24]  339 	movx	@dptr,a
      0052F7 90 00 30         [24]  340 	mov	dptr,#0x0030
      0052FA 12 71 D2         [24]  341 	lcall	_flash_write
                                    342 ;	peripherals\GoldSequence.c:94: flash_write(0x0032,0b011001111001110);
      0052FD 90 04 64         [24]  343 	mov	dptr,#_flash_write_PARM_2
      005300 74 CE            [12]  344 	mov	a,#0xce
      005302 F0               [24]  345 	movx	@dptr,a
      005303 74 33            [12]  346 	mov	a,#0x33
      005305 A3               [24]  347 	inc	dptr
      005306 F0               [24]  348 	movx	@dptr,a
      005307 90 00 32         [24]  349 	mov	dptr,#0x0032
      00530A 12 71 D2         [24]  350 	lcall	_flash_write
                                    351 ;	peripherals\GoldSequence.c:97: flash_write(0x0034,0b1010100001111011);
      00530D 90 04 64         [24]  352 	mov	dptr,#_flash_write_PARM_2
      005310 74 7B            [12]  353 	mov	a,#0x7b
      005312 F0               [24]  354 	movx	@dptr,a
      005313 74 A8            [12]  355 	mov	a,#0xa8
      005315 A3               [24]  356 	inc	dptr
      005316 F0               [24]  357 	movx	@dptr,a
      005317 90 00 34         [24]  358 	mov	dptr,#0x0034
      00531A 12 71 D2         [24]  359 	lcall	_flash_write
                                    360 ;	peripherals\GoldSequence.c:99: flash_write(0x0036,0b000011011010011);
      00531D 90 04 64         [24]  361 	mov	dptr,#_flash_write_PARM_2
      005320 74 D3            [12]  362 	mov	a,#0xd3
      005322 F0               [24]  363 	movx	@dptr,a
      005323 74 06            [12]  364 	mov	a,#0x06
      005325 A3               [24]  365 	inc	dptr
      005326 F0               [24]  366 	movx	@dptr,a
      005327 90 00 36         [24]  367 	mov	dptr,#0x0036
      00532A 12 71 D2         [24]  368 	lcall	_flash_write
                                    369 ;	peripherals\GoldSequence.c:102: flash_write(0x0038,0b0101000101001011);
      00532D 90 04 64         [24]  370 	mov	dptr,#_flash_write_PARM_2
      005330 74 4B            [12]  371 	mov	a,#0x4b
      005332 F0               [24]  372 	movx	@dptr,a
      005333 74 51            [12]  373 	mov	a,#0x51
      005335 A3               [24]  374 	inc	dptr
      005336 F0               [24]  375 	movx	@dptr,a
      005337 90 00 38         [24]  376 	mov	dptr,#0x0038
      00533A 12 71 D2         [24]  377 	lcall	_flash_write
                                    378 ;	peripherals\GoldSequence.c:104: flash_write(0x003A,0b101110001011101);
      00533D 90 04 64         [24]  379 	mov	dptr,#_flash_write_PARM_2
      005340 74 5D            [12]  380 	mov	a,#0x5d
      005342 F0               [24]  381 	movx	@dptr,a
      005343 14               [12]  382 	dec	a
      005344 A3               [24]  383 	inc	dptr
      005345 F0               [24]  384 	movx	@dptr,a
      005346 90 00 3A         [24]  385 	mov	dptr,#0x003a
      005349 12 71 D2         [24]  386 	lcall	_flash_write
                                    387 ;	peripherals\GoldSequence.c:107: flash_write(0x003C,0b0010110111010011);
      00534C 90 04 64         [24]  388 	mov	dptr,#_flash_write_PARM_2
      00534F 74 D3            [12]  389 	mov	a,#0xd3
      005351 F0               [24]  390 	movx	@dptr,a
      005352 74 2D            [12]  391 	mov	a,#0x2d
      005354 A3               [24]  392 	inc	dptr
      005355 F0               [24]  393 	movx	@dptr,a
      005356 90 00 3C         [24]  394 	mov	dptr,#0x003c
      005359 12 71 D2         [24]  395 	lcall	_flash_write
                                    396 ;	peripherals\GoldSequence.c:109: flash_write(0x003E,0b111000100011010);
      00535C 90 04 64         [24]  397 	mov	dptr,#_flash_write_PARM_2
      00535F 74 1A            [12]  398 	mov	a,#0x1a
      005361 F0               [24]  399 	movx	@dptr,a
      005362 74 71            [12]  400 	mov	a,#0x71
      005364 A3               [24]  401 	inc	dptr
      005365 F0               [24]  402 	movx	@dptr,a
      005366 90 00 3E         [24]  403 	mov	dptr,#0x003e
      005369 12 71 D2         [24]  404 	lcall	_flash_write
                                    405 ;	peripherals\GoldSequence.c:112: flash_write(0x0040,0b1001001110011111);
      00536C 90 04 64         [24]  406 	mov	dptr,#_flash_write_PARM_2
      00536F 74 9F            [12]  407 	mov	a,#0x9f
      005371 F0               [24]  408 	movx	@dptr,a
      005372 74 93            [12]  409 	mov	a,#0x93
      005374 A3               [24]  410 	inc	dptr
      005375 F0               [24]  411 	movx	@dptr,a
      005376 90 00 40         [24]  412 	mov	dptr,#0x0040
      005379 12 71 D2         [24]  413 	lcall	_flash_write
                                    414 ;	peripherals\GoldSequence.c:114: flash_write(0x0042,0b110011110111001);
      00537C 90 04 64         [24]  415 	mov	dptr,#_flash_write_PARM_2
      00537F 74 B9            [12]  416 	mov	a,#0xb9
      005381 F0               [24]  417 	movx	@dptr,a
      005382 74 67            [12]  418 	mov	a,#0x67
      005384 A3               [24]  419 	inc	dptr
      005385 F0               [24]  420 	movx	@dptr,a
      005386 90 00 42         [24]  421 	mov	dptr,#0x0042
      005389 12 71 D2         [24]  422 	lcall	_flash_write
                                    423 ;	peripherals\GoldSequence.c:117: flash_write(0x0044,0b0100110010111001);
      00538C 90 04 64         [24]  424 	mov	dptr,#_flash_write_PARM_2
      00538F 74 B9            [12]  425 	mov	a,#0xb9
      005391 F0               [24]  426 	movx	@dptr,a
      005392 74 4C            [12]  427 	mov	a,#0x4c
      005394 A3               [24]  428 	inc	dptr
      005395 F0               [24]  429 	movx	@dptr,a
      005396 90 00 44         [24]  430 	mov	dptr,#0x0044
      005399 12 71 D2         [24]  431 	lcall	_flash_write
                                    432 ;	peripherals\GoldSequence.c:119: flash_write(0x0046,0b110110011101000);
      00539C 90 04 64         [24]  433 	mov	dptr,#_flash_write_PARM_2
      00539F 74 E8            [12]  434 	mov	a,#0xe8
      0053A1 F0               [24]  435 	movx	@dptr,a
      0053A2 74 6C            [12]  436 	mov	a,#0x6c
      0053A4 A3               [24]  437 	inc	dptr
      0053A5 F0               [24]  438 	movx	@dptr,a
      0053A6 90 00 46         [24]  439 	mov	dptr,#0x0046
      0053A9 12 71 D2         [24]  440 	lcall	_flash_write
                                    441 ;	peripherals\GoldSequence.c:122: flash_write(0x0048,0b1010001100101010);
      0053AC 90 04 64         [24]  442 	mov	dptr,#_flash_write_PARM_2
      0053AF 74 2A            [12]  443 	mov	a,#0x2a
      0053B1 F0               [24]  444 	movx	@dptr,a
      0053B2 74 A3            [12]  445 	mov	a,#0xa3
      0053B4 A3               [24]  446 	inc	dptr
      0053B5 F0               [24]  447 	movx	@dptr,a
      0053B6 90 00 48         [24]  448 	mov	dptr,#0x0048
      0053B9 12 71 D2         [24]  449 	lcall	_flash_write
                                    450 ;	peripherals\GoldSequence.c:124: flash_write(0x004A,0b110100101000000);
      0053BC 90 04 64         [24]  451 	mov	dptr,#_flash_write_PARM_2
      0053BF 74 40            [12]  452 	mov	a,#0x40
      0053C1 F0               [24]  453 	movx	@dptr,a
      0053C2 74 69            [12]  454 	mov	a,#0x69
      0053C4 A3               [24]  455 	inc	dptr
      0053C5 F0               [24]  456 	movx	@dptr,a
      0053C6 90 00 4A         [24]  457 	mov	dptr,#0x004a
      0053C9 12 71 D2         [24]  458 	lcall	_flash_write
                                    459 ;	peripherals\GoldSequence.c:127: flash_write(0x004C,0b1101010011100011);
      0053CC 90 04 64         [24]  460 	mov	dptr,#_flash_write_PARM_2
      0053CF 74 E3            [12]  461 	mov	a,#0xe3
      0053D1 F0               [24]  462 	movx	@dptr,a
      0053D2 74 D4            [12]  463 	mov	a,#0xd4
      0053D4 A3               [24]  464 	inc	dptr
      0053D5 F0               [24]  465 	movx	@dptr,a
      0053D6 90 00 4C         [24]  466 	mov	dptr,#0x004c
      0053D9 12 71 D2         [24]  467 	lcall	_flash_write
                                    468 ;	peripherals\GoldSequence.c:129: flash_write(0x004E,0b010101110010100);
      0053DC 90 04 64         [24]  469 	mov	dptr,#_flash_write_PARM_2
      0053DF 74 94            [12]  470 	mov	a,#0x94
      0053E1 F0               [24]  471 	movx	@dptr,a
      0053E2 74 2B            [12]  472 	mov	a,#0x2b
      0053E4 A3               [24]  473 	inc	dptr
      0053E5 F0               [24]  474 	movx	@dptr,a
      0053E6 90 00 4E         [24]  475 	mov	dptr,#0x004e
      0053E9 12 71 D2         [24]  476 	lcall	_flash_write
                                    477 ;	peripherals\GoldSequence.c:132: flash_write(0x0050,0b1110111100000111);
      0053EC 90 04 64         [24]  478 	mov	dptr,#_flash_write_PARM_2
      0053EF 74 07            [12]  479 	mov	a,#0x07
      0053F1 F0               [24]  480 	movx	@dptr,a
      0053F2 74 EF            [12]  481 	mov	a,#0xef
      0053F4 A3               [24]  482 	inc	dptr
      0053F5 F0               [24]  483 	movx	@dptr,a
      0053F6 90 00 50         [24]  484 	mov	dptr,#0x0050
      0053F9 12 71 D2         [24]  485 	lcall	_flash_write
                                    486 ;	peripherals\GoldSequence.c:134: flash_write(0x0052,0b100101011111110);
      0053FC 90 04 64         [24]  487 	mov	dptr,#_flash_write_PARM_2
      0053FF 74 FE            [12]  488 	mov	a,#0xfe
      005401 F0               [24]  489 	movx	@dptr,a
      005402 74 4A            [12]  490 	mov	a,#0x4a
      005404 A3               [24]  491 	inc	dptr
      005405 F0               [24]  492 	movx	@dptr,a
      005406 90 00 52         [24]  493 	mov	dptr,#0x0052
      005409 12 71 D2         [24]  494 	lcall	_flash_write
                                    495 ;	peripherals\GoldSequence.c:137: flash_write(0x0054,0b1111001011110101);
      00540C 90 04 64         [24]  496 	mov	dptr,#_flash_write_PARM_2
      00540F 74 F5            [12]  497 	mov	a,#0xf5
      005411 F0               [24]  498 	movx	@dptr,a
      005412 74 F2            [12]  499 	mov	a,#0xf2
      005414 A3               [24]  500 	inc	dptr
      005415 F0               [24]  501 	movx	@dptr,a
      005416 90 00 54         [24]  502 	mov	dptr,#0x0054
      005419 12 71 D2         [24]  503 	lcall	_flash_write
                                    504 ;	peripherals\GoldSequence.c:139: flash_write(0x0056,0b111101001001011);
      00541C 90 04 64         [24]  505 	mov	dptr,#_flash_write_PARM_2
      00541F 74 4B            [12]  506 	mov	a,#0x4b
      005421 F0               [24]  507 	movx	@dptr,a
      005422 74 7A            [12]  508 	mov	a,#0x7a
      005424 A3               [24]  509 	inc	dptr
      005425 F0               [24]  510 	movx	@dptr,a
      005426 90 00 56         [24]  511 	mov	dptr,#0x0056
      005429 12 71 D2         [24]  512 	lcall	_flash_write
                                    513 ;	peripherals\GoldSequence.c:141: flash_write(0x0058,0b0111110000001100);
      00542C 90 04 64         [24]  514 	mov	dptr,#_flash_write_PARM_2
      00542F 74 0C            [12]  515 	mov	a,#0x0c
      005431 F0               [24]  516 	movx	@dptr,a
      005432 74 7C            [12]  517 	mov	a,#0x7c
      005434 A3               [24]  518 	inc	dptr
      005435 F0               [24]  519 	movx	@dptr,a
      005436 90 00 58         [24]  520 	mov	dptr,#0x0058
      005439 12 71 D2         [24]  521 	lcall	_flash_write
                                    522 ;	peripherals\GoldSequence.c:143: flash_write(0x005A,0b110001000010001);
      00543C 90 04 64         [24]  523 	mov	dptr,#_flash_write_PARM_2
      00543F 74 11            [12]  524 	mov	a,#0x11
      005441 F0               [24]  525 	movx	@dptr,a
      005442 74 62            [12]  526 	mov	a,#0x62
      005444 A3               [24]  527 	inc	dptr
      005445 F0               [24]  528 	movx	@dptr,a
      005446 90 00 5A         [24]  529 	mov	dptr,#0x005a
      005449 12 71 D2         [24]  530 	lcall	_flash_write
                                    531 ;	peripherals\GoldSequence.c:146: flash_write(0x005C,0b0011101101110000);
      00544C 90 04 64         [24]  532 	mov	dptr,#_flash_write_PARM_2
      00544F 74 70            [12]  533 	mov	a,#0x70
      005451 F0               [24]  534 	movx	@dptr,a
      005452 74 3B            [12]  535 	mov	a,#0x3b
      005454 A3               [24]  536 	inc	dptr
      005455 F0               [24]  537 	movx	@dptr,a
      005456 90 00 5C         [24]  538 	mov	dptr,#0x005c
      005459 12 71 D2         [24]  539 	lcall	_flash_write
                                    540 ;	peripherals\GoldSequence.c:148: flash_write(0x005E,0b010111000111100);
      00545C 90 04 64         [24]  541 	mov	dptr,#_flash_write_PARM_2
      00545F 74 3C            [12]  542 	mov	a,#0x3c
      005461 F0               [24]  543 	movx	@dptr,a
      005462 74 2E            [12]  544 	mov	a,#0x2e
      005464 A3               [24]  545 	inc	dptr
      005465 F0               [24]  546 	movx	@dptr,a
      005466 90 00 5E         [24]  547 	mov	dptr,#0x005e
      005469 12 71 D2         [24]  548 	lcall	_flash_write
                                    549 ;	peripherals\GoldSequence.c:151: flash_write(0x0060,0b1001100011001110);
      00546C 90 04 64         [24]  550 	mov	dptr,#_flash_write_PARM_2
      00546F 74 CE            [12]  551 	mov	a,#0xce
      005471 F0               [24]  552 	movx	@dptr,a
      005472 74 98            [12]  553 	mov	a,#0x98
      005474 A3               [24]  554 	inc	dptr
      005475 F0               [24]  555 	movx	@dptr,a
      005476 90 00 60         [24]  556 	mov	dptr,#0x0060
      005479 12 71 D2         [24]  557 	lcall	_flash_write
                                    558 ;	peripherals\GoldSequence.c:153: flash_write(0x0062,0b000100000101010);
      00547C 90 04 64         [24]  559 	mov	dptr,#_flash_write_PARM_2
      00547F 74 2A            [12]  560 	mov	a,#0x2a
      005481 F0               [24]  561 	movx	@dptr,a
      005482 74 08            [12]  562 	mov	a,#0x08
      005484 A3               [24]  563 	inc	dptr
      005485 F0               [24]  564 	movx	@dptr,a
      005486 90 00 62         [24]  565 	mov	dptr,#0x0062
      005489 12 71 D2         [24]  566 	lcall	_flash_write
                                    567 ;	peripherals\GoldSequence.c:156: flash_write(0x0064,0b1100100100010001);
      00548C 90 04 64         [24]  568 	mov	dptr,#_flash_write_PARM_2
      00548F 74 11            [12]  569 	mov	a,#0x11
      005491 F0               [24]  570 	movx	@dptr,a
      005492 74 C9            [12]  571 	mov	a,#0xc9
      005494 A3               [24]  572 	inc	dptr
      005495 F0               [24]  573 	movx	@dptr,a
      005496 90 00 64         [24]  574 	mov	dptr,#0x0064
      005499 12 71 D2         [24]  575 	lcall	_flash_write
                                    576 ;	peripherals\GoldSequence.c:158: flash_write(0x0066,0b001101100100001);
      00549C 90 04 64         [24]  577 	mov	dptr,#_flash_write_PARM_2
      00549F 74 21            [12]  578 	mov	a,#0x21
      0054A1 F0               [24]  579 	movx	@dptr,a
      0054A2 74 1B            [12]  580 	mov	a,#0x1b
      0054A4 A3               [24]  581 	inc	dptr
      0054A5 F0               [24]  582 	movx	@dptr,a
      0054A6 90 00 66         [24]  583 	mov	dptr,#0x0066
      0054A9 12 71 D2         [24]  584 	lcall	_flash_write
                                    585 ;	peripherals\GoldSequence.c:161: flash_write(0x0068,0b0110000111111110);
      0054AC 90 04 64         [24]  586 	mov	dptr,#_flash_write_PARM_2
      0054AF 74 FE            [12]  587 	mov	a,#0xfe
      0054B1 F0               [24]  588 	movx	@dptr,a
      0054B2 74 61            [12]  589 	mov	a,#0x61
      0054B4 A3               [24]  590 	inc	dptr
      0054B5 F0               [24]  591 	movx	@dptr,a
      0054B6 90 00 68         [24]  592 	mov	dptr,#0x0068
      0054B9 12 71 D2         [24]  593 	lcall	_flash_write
                                    594 ;	peripherals\GoldSequence.c:163: flash_write(0x006A,0b101001010100100);
      0054BC 90 04 64         [24]  595 	mov	dptr,#_flash_write_PARM_2
      0054BF 74 A4            [12]  596 	mov	a,#0xa4
      0054C1 F0               [24]  597 	movx	@dptr,a
      0054C2 03               [12]  598 	rr	a
      0054C3 A3               [24]  599 	inc	dptr
      0054C4 F0               [24]  600 	movx	@dptr,a
      0054C5 90 00 6A         [24]  601 	mov	dptr,#0x006a
      0054C8 12 71 D2         [24]  602 	lcall	_flash_write
                                    603 ;	peripherals\GoldSequence.c:166: flash_write(0x006C,0b1011010110001001);
      0054CB 90 04 64         [24]  604 	mov	dptr,#_flash_write_PARM_2
      0054CE 74 89            [12]  605 	mov	a,#0x89
      0054D0 F0               [24]  606 	movx	@dptr,a
      0054D1 74 B5            [12]  607 	mov	a,#0xb5
      0054D3 A3               [24]  608 	inc	dptr
      0054D4 F0               [24]  609 	movx	@dptr,a
      0054D5 90 00 6C         [24]  610 	mov	dptr,#0x006c
      0054D8 12 71 D2         [24]  611 	lcall	_flash_write
                                    612 ;	peripherals\GoldSequence.c:168: flash_write(0x006E,0b011011001100110);
      0054DB 90 04 64         [24]  613 	mov	dptr,#_flash_write_PARM_2
      0054DE 74 66            [12]  614 	mov	a,#0x66
      0054E0 F0               [24]  615 	movx	@dptr,a
      0054E1 74 36            [12]  616 	mov	a,#0x36
      0054E3 A3               [24]  617 	inc	dptr
      0054E4 F0               [24]  618 	movx	@dptr,a
      0054E5 90 00 6E         [24]  619 	mov	dptr,#0x006e
      0054E8 12 71 D2         [24]  620 	lcall	_flash_write
                                    621 ;	peripherals\GoldSequence.c:171: flash_write(0x0070,0b1101111110110010);
      0054EB 90 04 64         [24]  622 	mov	dptr,#_flash_write_PARM_2
      0054EE 74 B2            [12]  623 	mov	a,#0xb2
      0054F0 F0               [24]  624 	movx	@dptr,a
      0054F1 74 DF            [12]  625 	mov	a,#0xdf
      0054F3 A3               [24]  626 	inc	dptr
      0054F4 F0               [24]  627 	movx	@dptr,a
      0054F5 90 00 70         [24]  628 	mov	dptr,#0x0070
      0054F8 12 71 D2         [24]  629 	lcall	_flash_write
                                    630 ;	peripherals\GoldSequence.c:173: flash_write(0x0072,0b100010000000111);
      0054FB 90 04 64         [24]  631 	mov	dptr,#_flash_write_PARM_2
      0054FE 74 07            [12]  632 	mov	a,#0x07
      005500 F0               [24]  633 	movx	@dptr,a
      005501 74 44            [12]  634 	mov	a,#0x44
      005503 A3               [24]  635 	inc	dptr
      005504 F0               [24]  636 	movx	@dptr,a
      005505 90 00 72         [24]  637 	mov	dptr,#0x0072
      005508 12 71 D2         [24]  638 	lcall	_flash_write
                                    639 ;	peripherals\GoldSequence.c:176: flash_write(0x0074,0b0110101010101111);
      00550B 90 04 64         [24]  640 	mov	dptr,#_flash_write_PARM_2
      00550E 74 AF            [12]  641 	mov	a,#0xaf
      005510 F0               [24]  642 	movx	@dptr,a
      005511 74 6A            [12]  643 	mov	a,#0x6a
      005513 A3               [24]  644 	inc	dptr
      005514 F0               [24]  645 	movx	@dptr,a
      005515 90 00 74         [24]  646 	mov	dptr,#0x0074
      005518 12 71 D2         [24]  647 	lcall	_flash_write
                                    648 ;	peripherals\GoldSequence.c:178: flash_write(0x0076,0b011110100110111);
      00551B 90 04 64         [24]  649 	mov	dptr,#_flash_write_PARM_2
      00551E 74 37            [12]  650 	mov	a,#0x37
      005520 F0               [24]  651 	movx	@dptr,a
      005521 74 3D            [12]  652 	mov	a,#0x3d
      005523 A3               [24]  653 	inc	dptr
      005524 F0               [24]  654 	movx	@dptr,a
      005525 90 00 76         [24]  655 	mov	dptr,#0x0076
      005528 12 71 D2         [24]  656 	lcall	_flash_write
                                    657 ;	peripherals\GoldSequence.c:181: flash_write(0x0078,0b0011000000100001);
      00552B 90 04 64         [24]  658 	mov	dptr,#_flash_write_PARM_2
      00552E 74 21            [12]  659 	mov	a,#0x21
      005530 F0               [24]  660 	movx	@dptr,a
      005531 74 30            [12]  661 	mov	a,#0x30
      005533 A3               [24]  662 	inc	dptr
      005534 F0               [24]  663 	movx	@dptr,a
      005535 90 00 78         [24]  664 	mov	dptr,#0x0078
      005538 12 71 D2         [24]  665 	lcall	_flash_write
                                    666 ;	peripherals\GoldSequence.c:183: flash_write(0x007A,0b100000110101111);
      00553B 90 04 64         [24]  667 	mov	dptr,#_flash_write_PARM_2
      00553E 74 AF            [12]  668 	mov	a,#0xaf
      005540 F0               [24]  669 	movx	@dptr,a
      005541 74 41            [12]  670 	mov	a,#0x41
      005543 A3               [24]  671 	inc	dptr
      005544 F0               [24]  672 	movx	@dptr,a
      005545 90 00 7A         [24]  673 	mov	dptr,#0x007a
      005548 12 71 D2         [24]  674 	lcall	_flash_write
                                    675 ;	peripherals\GoldSequence.c:186: flash_write(0x007C,0b0001110101100110);
      00554B 90 04 64         [24]  676 	mov	dptr,#_flash_write_PARM_2
      00554E 74 66            [12]  677 	mov	a,#0x66
      005550 F0               [24]  678 	movx	@dptr,a
      005551 74 1D            [12]  679 	mov	a,#0x1d
      005553 A3               [24]  680 	inc	dptr
      005554 F0               [24]  681 	movx	@dptr,a
      005555 90 00 7C         [24]  682 	mov	dptr,#0x007c
      005558 12 71 D2         [24]  683 	lcall	_flash_write
                                    684 ;	peripherals\GoldSequence.c:188: flash_write(0x007E,0b111111111100011);
      00555B 90 04 64         [24]  685 	mov	dptr,#_flash_write_PARM_2
      00555E 74 E3            [12]  686 	mov	a,#0xe3
      005560 F0               [24]  687 	movx	@dptr,a
      005561 74 7F            [12]  688 	mov	a,#0x7f
      005563 A3               [24]  689 	inc	dptr
      005564 F0               [24]  690 	movx	@dptr,a
      005565 90 00 7E         [24]  691 	mov	dptr,#0x007e
      005568 12 71 D2         [24]  692 	lcall	_flash_write
                                    693 ;	peripherals\GoldSequence.c:191: flash_write(0x0080,0b0000101111000101);
      00556B 90 04 64         [24]  694 	mov	dptr,#_flash_write_PARM_2
      00556E 74 C5            [12]  695 	mov	a,#0xc5
      005570 F0               [24]  696 	movx	@dptr,a
      005571 74 0B            [12]  697 	mov	a,#0x0b
      005573 A3               [24]  698 	inc	dptr
      005574 F0               [24]  699 	movx	@dptr,a
      005575 90 00 80         [24]  700 	mov	dptr,#0x0080
      005578 12 71 D2         [24]  701 	lcall	_flash_write
                                    702 ;	peripherals\GoldSequence.c:193: flash_write(0x0082,0b010000011000101);
      00557B 90 04 64         [24]  703 	mov	dptr,#_flash_write_PARM_2
      00557E 74 C5            [12]  704 	mov	a,#0xc5
      005580 F0               [24]  705 	movx	@dptr,a
      005581 74 20            [12]  706 	mov	a,#0x20
      005583 A3               [24]  707 	inc	dptr
      005584 F0               [24]  708 	movx	@dptr,a
      005585 90 00 82         [24]  709 	mov	dptr,#0x0082
      005588 12 71 D2         [24]  710 	lcall	_flash_write
                                    711 ;	peripherals\GoldSequence.c:195: flash_lock();
      00558B 02 7A F3         [24]  712 	ljmp	_flash_lock
                                    713 ;------------------------------------------------------------
                                    714 ;Allocation info for local variables in function 'GOLDSEQUENCE_Obtain'
                                    715 ;------------------------------------------------------------
                                    716 ;Rnd                       Allocated with name '_GOLDSEQUENCE_Obtain_Rnd_1_120'
                                    717 ;RetVal                    Allocated with name '_GOLDSEQUENCE_Obtain_RetVal_1_120'
                                    718 ;------------------------------------------------------------
                                    719 ;	peripherals\GoldSequence.c:212: __xdata uint32_t GOLDSEQUENCE_Obtain(void)
                                    720 ;	-----------------------------------------
                                    721 ;	 function GOLDSEQUENCE_Obtain
                                    722 ;	-----------------------------------------
      00558E                        723 _GOLDSEQUENCE_Obtain:
                                    724 ;	peripherals\GoldSequence.c:217: Rnd = random();
      00558E 12 75 ED         [24]  725 	lcall	_random
      005591 AE 82            [24]  726 	mov	r6,dpl
      005593 AF 83            [24]  727 	mov	r7,dph
                                    728 ;	peripherals\GoldSequence.c:218: flash_unlock();
      005595 C0 07            [24]  729 	push	ar7
      005597 C0 06            [24]  730 	push	ar6
      005599 12 73 99         [24]  731 	lcall	_flash_unlock
      00559C D0 06            [24]  732 	pop	ar6
      00559E D0 07            [24]  733 	pop	ar7
                                    734 ;	peripherals\GoldSequence.c:220: Rnd = ((Rnd % 33)*4);  // MOD operation is done so that the number is between 0 and 33, once a number between 0 and 32 has been obtained, its multiplied by 4 to match the memory address
      0055A0 90 04 70         [24]  735 	mov	dptr,#__moduint_PARM_2
      0055A3 74 21            [12]  736 	mov	a,#0x21
      0055A5 F0               [24]  737 	movx	@dptr,a
      0055A6 E4               [12]  738 	clr	a
      0055A7 A3               [24]  739 	inc	dptr
      0055A8 F0               [24]  740 	movx	@dptr,a
      0055A9 8E 82            [24]  741 	mov	dpl,r6
      0055AB 8F 83            [24]  742 	mov	dph,r7
      0055AD 12 74 81         [24]  743 	lcall	__moduint
      0055B0 AE 82            [24]  744 	mov	r6,dpl
      0055B2 E5 83            [12]  745 	mov	a,dph
      0055B4 CE               [12]  746 	xch	a,r6
      0055B5 25 E0            [12]  747 	add	a,acc
      0055B7 CE               [12]  748 	xch	a,r6
      0055B8 33               [12]  749 	rlc	a
      0055B9 CE               [12]  750 	xch	a,r6
      0055BA 25 E0            [12]  751 	add	a,acc
      0055BC CE               [12]  752 	xch	a,r6
      0055BD 33               [12]  753 	rlc	a
      0055BE FF               [12]  754 	mov	r7,a
                                    755 ;	peripherals\GoldSequence.c:221: RetVal = ((uint32_t)flash_read(Rnd))<<16;
      0055BF 8E 82            [24]  756 	mov	dpl,r6
      0055C1 8F 83            [24]  757 	mov	dph,r7
      0055C3 C0 07            [24]  758 	push	ar7
      0055C5 C0 06            [24]  759 	push	ar6
      0055C7 12 77 F9         [24]  760 	lcall	_flash_read
      0055CA AC 82            [24]  761 	mov	r4,dpl
      0055CC AD 83            [24]  762 	mov	r5,dph
      0055CE D0 06            [24]  763 	pop	ar6
      0055D0 D0 07            [24]  764 	pop	ar7
      0055D2 8D 02            [24]  765 	mov	ar2,r5
      0055D4 8C 03            [24]  766 	mov	ar3,r4
                                    767 ;	peripherals\GoldSequence.c:222: RetVal |= (((uint32_t)flash_read(Rnd+2)) & 0x0000FFFF)<<1;
      0055D6 E4               [12]  768 	clr	a
      0055D7 FD               [12]  769 	mov	r5,a
      0055D8 FC               [12]  770 	mov	r4,a
      0055D9 74 02            [12]  771 	mov	a,#0x02
      0055DB 2E               [12]  772 	add	a,r6
      0055DC FE               [12]  773 	mov	r6,a
      0055DD E4               [12]  774 	clr	a
      0055DE 3F               [12]  775 	addc	a,r7
      0055DF FF               [12]  776 	mov	r7,a
      0055E0 8E 82            [24]  777 	mov	dpl,r6
      0055E2 8F 83            [24]  778 	mov	dph,r7
      0055E4 C0 05            [24]  779 	push	ar5
      0055E6 C0 04            [24]  780 	push	ar4
      0055E8 C0 03            [24]  781 	push	ar3
      0055EA C0 02            [24]  782 	push	ar2
      0055EC 12 77 F9         [24]  783 	lcall	_flash_read
      0055EF AE 82            [24]  784 	mov	r6,dpl
      0055F1 AF 83            [24]  785 	mov	r7,dph
      0055F3 D0 02            [24]  786 	pop	ar2
      0055F5 D0 03            [24]  787 	pop	ar3
      0055F7 D0 04            [24]  788 	pop	ar4
      0055F9 D0 05            [24]  789 	pop	ar5
      0055FB 8E 00            [24]  790 	mov	ar0,r6
      0055FD 8F 01            [24]  791 	mov	ar1,r7
      0055FF E4               [12]  792 	clr	a
      005600 FE               [12]  793 	mov	r6,a
      005601 FF               [12]  794 	mov	r7,a
      005602 E8               [12]  795 	mov	a,r0
      005603 28               [12]  796 	add	a,r0
      005604 F8               [12]  797 	mov	r0,a
      005605 E9               [12]  798 	mov	a,r1
      005606 33               [12]  799 	rlc	a
      005607 F9               [12]  800 	mov	r1,a
      005608 EE               [12]  801 	mov	a,r6
      005609 33               [12]  802 	rlc	a
      00560A FE               [12]  803 	mov	r6,a
      00560B EF               [12]  804 	mov	a,r7
      00560C 33               [12]  805 	rlc	a
      00560D FF               [12]  806 	mov	r7,a
      00560E E8               [12]  807 	mov	a,r0
      00560F 42 04            [12]  808 	orl	ar4,a
      005611 E9               [12]  809 	mov	a,r1
      005612 42 05            [12]  810 	orl	ar5,a
      005614 EE               [12]  811 	mov	a,r6
      005615 42 03            [12]  812 	orl	ar3,a
      005617 EF               [12]  813 	mov	a,r7
      005618 42 02            [12]  814 	orl	ar2,a
                                    815 ;	peripherals\GoldSequence.c:224: flash_lock();
      00561A C0 05            [24]  816 	push	ar5
      00561C C0 04            [24]  817 	push	ar4
      00561E C0 03            [24]  818 	push	ar3
      005620 C0 02            [24]  819 	push	ar2
      005622 12 7A F3         [24]  820 	lcall	_flash_lock
      005625 D0 02            [24]  821 	pop	ar2
      005627 D0 03            [24]  822 	pop	ar3
      005629 D0 04            [24]  823 	pop	ar4
      00562B D0 05            [24]  824 	pop	ar5
                                    825 ;	peripherals\GoldSequence.c:225: return(RetVal);
      00562D 8C 82            [24]  826 	mov	dpl,r4
      00562F 8D 83            [24]  827 	mov	dph,r5
      005631 8B F0            [24]  828 	mov	b,r3
      005633 EA               [12]  829 	mov	a,r2
      005634 22               [24]  830 	ret
                                    831 	.area CSEG    (CODE)
                                    832 	.area CONST   (CODE)
                                    833 	.area XINIT   (CODE)
                                    834 	.area CABS    (ABS,CODE)
