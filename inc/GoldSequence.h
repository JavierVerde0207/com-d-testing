//gold sequence generator header file

#include <libmftypes.h>


__xdata void GOLDSEQUENCE_Init(void);
__xdata uint32_t GOLDSEQUENCE_Obtain(void);
