// Copyright (c) 2007,2008,2009,2010,2011,2012,2013, 2014 AXSEM AG
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//     1.Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     2.Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     3.Neither the name of AXSEM AG, Duebendorf nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//     4.All advertising materials mentioning features or use of this software
//       must display the following acknowledgement:
//       This product includes software developed by AXSEM AG and its contributors.
//     5.The usage of this source code is only granted for operation with AX5043
//       and AX8052F143. Porting to other radio or communication devices is
//       strictly prohibited.
//
// THIS SOFTWARE IS PROVIDED BY AXSEM AG AND CONTRIBUTORS ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL AXSEM AG AND CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef MISC_H_INCLUDED
#define MISC_H_INCLUDED

#include <libmftypes.h>
#include "axradio.h"
#include "ax8052.h"

typedef enum e_RfStateMachine
{
    e_WaitForBeaconAndMessageSend,
    e_BeaconReceived,
    e_WaitForACK,
    e_ReSendMessage,
    e_BackOff
} e_RfStateMachine;



extern void calibrate_lposc(void);
extern uint8_t display_received_packet(struct axradio_status __xdata *st);
extern void dbglink_received_packet(struct axradio_status __xdata *st);
extern __reentrantb void delay_ms(uint16_t ms) __reentrant;
extern void transmit_packet(void);
void delay_tx(uint32_t time1_ms, uint32_t time2_ms, uint32_t time3_ms);
void StartBackoffTimer(void);
void StartACKTimer(void);
void InitGPSPolling (uint32_t GPS_Period_S);
void InitChannelChange (void);
void swap_variables(uint8_t *a,uint8_t *b);
void AssemblyPacket(uint32_t GoldCode, uint8_t Copy1, uint8_t Copy2, uint8_t *Payload, uint8_t PayloadLength, uint8_t ack,uint8_t *TxPackage, uint8_t *TotalLength);
void GetRandomSlots(uint8_t* slot1, uint8_t* slot2, uint8_t* slot3);

__reentrantb void Channel_change_callback(struct wtimer_desc __xdata *desc);



// USER DEFINES
#define FALSE 0
#define TRUE  1
//RF configuration defines
#define FREQHZ_TO_AXFREQ(x) ((((float) x/48000000)*(16777216))+1/2)
#define TIME_FOR_TIMER_MSEC(y) ((((float) y*640))/1000)
#define TIME_FOR_TIMER_SEC(z) z*640
#define SLOT_TO_DELAY_MS(q) q*46 //42 ms lasts a slot of 32 bytes including header (50 bytes in total -> 50bytes /9600bps  * 8b/B) +10%
#define RND_NUMBER_TO_SLOTS(j) j%50


// board gpio command
#define ENABLE_SW_CTRL1 PORTA_0 = 1;
#define DISABLE_SW_CTRL1 PORTA_0 = 0;
//GPS ENABLE
#define ENABLE_GPS PORTA_1 = 1;
#define DISABLE_GPS PORTA_1 = 0;
//LNA ENABLE
#define ENABLE_LNA PORTA_2 = 1;
#define DISABLE_LNA PORTA_2 = 0;
//GPS WAKEUP
#define GPS_WAKEUP PORTA_4 = 1;
#define GPS_TURNOFF PORTA_4 = 0;
//GPS RESET
#define GPS_RESET_ON PORTA_5 = 1;
#define GPS_RESET_OFF PORTA_5 = 0;
// 3.6V DCDC ENABLE (POWER AMPLIFIER ENABLE)
#define ENABLE_PA_3_6V PORTC_4 = 1;
#define DISABLE_PA_3_6V PORTC_4 = 0;

#define PORTB3_1 PORTB_3 = 1;
#define PORTB3_0 PORTB_3 = 0;

#define RF_SWITCH_TX  PORTB_3 = 1;
#define RF_SWITCH_RX  PORTB_3 = 0;

#define DCDC_36ON    PORTC_4 = 1;
#define DCDC_36OFF   PORTC_4 = 0;
#define GPS_POLL_TIME_S 10
#define MAX_RETRANSMISSIONS 3
#define SW_CTRL_RX  1
#define SW_CTRL_TX  0




#define dbglink
#define doppler_tracking
#endif // MISC_H_INCLUDED
