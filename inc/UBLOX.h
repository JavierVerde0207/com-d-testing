#include <libmftypes.h>
#define UART0 0
#define UART1 1
#define TIMER0 0
#define TIMER1 1
#define TIMER2 2
//#define 8BITS 8
#define UBLOX_DEFAULT_BAUDRATE 9600
#define OK 1
#define NOK 0

#define ANS 0
/* Message positions and header values */
#define UBX_HEADER1_VAL         0xB5
#define UBX_HEADER2_VAL         0x62
#define UBX_HEADER1_POS         0x00
#define UBX_HEADER2_POS         0x01
#define UBX_MESSAGE_CLASS       0x02
#define UBX_MESSAGE_ID          0x03
#define UBX_MESSAGE_LENGTH_LSB  0x04 //length of the payload
#define UBX_MESSAGE_LENGTH_MSB  0x05 //length of the payload
#define UBX_MESSAGE_PAYLOAD     0x06
#define UBX_HEADER_LENGHT       0x07



// checksum includes class id length and payload
/* Messages class ID enum */
enum eUBX_MESSAGE_CLASS {
    eNAV = 0x01,
    eRXM,
    eINF = 0x04,
    eACK,
    eCFG,
    eUPD = 0x09,
    eMON,
    eAID,
    eTIM = 0x0C,
    eESF = 0x10,
    eMGA = 0x13,
    eLOG = 0x21,
    eSEC = 0x27,
    eHNR
};
/* ACK class messages*/
#define ACK 0x01
#define NAK 0x00

/* AID class messages*/
//Not implemented, not used yet

/* CFG class messages*/
#define ANT  0x13
#define CFG  0x09
#define GNSS 0x3E
#define PM2  0x3B
#define PMS  0x86
#define PRT  0x00
#define PWR  0x57
#define RATE 0x08

/* ESF class messages*/
//Not implemented, not used yet

/* HNR class messages*/
//Not implemented, not used yet

/* INF class messages*/
#define DEBUG   0x04
#define ERROR   0x00
#define NOTICE  0x02
#define TEST    0x03
#define WARNING 0x01

/* LOG class messages*/
//Not implemented, not used yet

/* MGA class messages*/
//Not implemented, not used yet

/* MON class messages*/
//Not implemented, not used yet

/* NAV class messages*/
#define AOPSTATUS 0x60
#define ATT       0x05
#define CLOCK     0x22
#define DGPS      0x31
#define DOP       0x04
#define EOE       0x61
#define GEOFENCE  0x39
#define HPPOSECEF 0x13
#define HPPOSLLH  0x14 // geodesic high precision position
#define ODO       0x09
#define ORB       0x34
#define POSECEF   0x01
#define POSLLH    0x02 // geodesic position
#define PVT       0x07
#define RLPOSNED  0x3C
#define RESETODO  0x10
#define SAT       0x35
#define SBAS      0x32
#define SLAS      0x42
#define SOL       0x06
#define STATUS    0x03
#define SVINFO    0x30
#define SVIN      0x3B
#define TIMEBDS   0x24
#define TIMEGAL   0x25
#define TIMEGLO   0x26
#define TIMEUTC   0x21
#define VELECEF   0x11
#define VELNED    0x12
/* RXM class messages*/
//Not implemented, not used yet

/* SEC class messages*/
//Not implemented, not used yet

/* TIM class messages*/
//Not implemented, not used yet

/* UPD class messages*/
//Not implemented, not used yet


/* checksum algorithm*/
void UBLOX_GPS_FletcherChecksum8 ( uint8_t *buffer, uint8_t *CK_A, uint8_t *CK_B, uint16_t length);
//void UBLOX_GPS_Init(uint8_t uart,uint8_t baudrate);
 void UBLOX_GPS_PortInit(void);
 void UBLOX_GPS_Init(void);
//uint8_t UBLOX_GPS_ParseData(uint8_t *rx_buffer,uint16_t received_length,uint8_t msg_class,uint8_t msg_id,uint16_t msg_length, uint8_t *msg_payload);
uint8_t UBLOX_GPS_SendCommand_WaitACK(uint8_t msg_class, uint8_t msg_id, uint16_t msg_length,uint8_t *payload,uint8_t AnsOrAck,uint16_t *RxLength);




