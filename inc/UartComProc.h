#include <libmftypes.h>
#include <libmfuart0.h>
#include <libmfuart1.h>
#include <libmfuart.h>
#include <stdlib.h>
#include <string.h>
#include <libmfdbglink.h>
#include <ax8052f143.h>
#include "misc.h"


//KISS protocol defines
#define FRAME_START             0xC0
#define FRAME_END               0xC0
#define FRAME_SCAPE             0xDB
#define TRANSPOSE_FRAME_END     0xDC
#define TRANSPOSE_FRAME_ESCAPE  0xDD

//Function returns
#define ERR_NO_DATA     0x0
#define ERR_BAD_CRC     0x1
#define ERR_BAD_FORMAT  0x2
#define ERR_BAD_ENDING  0x3
#define ERR_BAD_CMD     0x4
#define SUCCESFULL      0x5

//magic numbers
#define CRC_LENGTH              0x04
#define START_CHARACTER_LENGTH  0x01
#define END_CHARACTER_LENGTH    0x01
#define COMMAND_LENGTH          0x01

void UART_Proc_PortInit(void);
uint8_t UART_Proc_VerifyIncomingMsg(uint8_t *cmd, uint8_t *payload,uint8_t *PayloadLength);
uint8_t UART_Proc_SendMessage(uint8_t *payload, uint8_t length, uint8_t command);
void UART_Proc_ModifiyKissSpecialCharacters(uint8_t *UartRxBuffer, uint8_t *RxLength);
uint32_t UART_Calc_CRC32(uint8_t *message,uint8_t length,uint8_t littleEndian);
uint8_t UART_IncomingMessageVerify(void);


uint8_t UART_ParseConfigMessage(uint8_t *payload, uint8_t PayloadLength);
uint8_t UART_ParseSetDbgModeMessage(uint8_t *payload, uint8_t PayloadLength);
uint8_t UART_ParseTxMessage(uint8_t *payload, uint8_t PayloadLength);
uint8_t UART_ParsePwrDwn(uint8_t *payload, uint8_t PayloadLength);
uint8_t UART_ParseGpsDataReq(uint8_t *payload, uint8_t PayloadLength);
uint8_t UART_ParseTxMessage(uint8_t *payload, uint8_t PayloadLength);
uint8_t UART_ParseAnsMessage(uint8_t *payload, uint8_t PayloadLength);

//defines for communication interface
//define messages subtype
#define CONFIG          0x01
#define SET_DBG_MODE    0x02
#define TX_MESSAGE      0x03
#define POWER_DOWN      0x04
#define RX_MESSAGE      0x05
#define DBG_MESSAGE     0x06
#define GPS_DATA_REQ    0x07
#define GPS_DATA_RESP   0x08
#define ANSWER_MSG      0x0A

// define general messages subtype
#define OK          0x01
#define NOK         0xF0

//define NOK codes
#define ERR_CMD_LENGTH          0xD8
#define ERR_CMD_CODE            0xD7
#define ERR_CMD_CRC             0xD6
#define ERR_CMD_PCKT_LENGTH     0xD5
#define ERR_CMD_PAYLOAD_DATA    0xD4

#define RX_SUCCESFULL 0xDD  //ver que valor poner

