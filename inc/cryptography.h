#include <libmftypes.h>
#include <libmfcrypto.h>


void CRYPTO_InitRegisters(uint16_t AES_KEY_ADDRESS, uint16_t AES_OUT_ADDR);
uint8_t CRYPTO_KeySelectAndExpansion (uint8_t *AES_KEY_BUFFER);
uint8_t CRYPTO_EncryptData(uint16_t AES_IN_ADDR);
