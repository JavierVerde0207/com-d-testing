
#include <libmftypes.h>
#define MEM_SOFT_CONFIG_START       0x0
#define MEM_SOFT_COFIG_END          0x3FF
#define MEM_CRYPTOGRAPHY_KEYS_START 0x400
#define MEM_CRYPTOGRAPHY_KEYS_END   0x7FF
#define MEM_HMAC_KEYS_START         0x800
#define MEM_HMAC_KEYS_END           0xBFF
#define MEM_GOLD_SEQUENCES_START    0x1C00
#define MEM_GOLD_SEQUENCES_END      0x1FFF
#define MEM_GSP_MESSAGES_START      0x1000
#define MEM_GPS_MESSAGES_END        0x13FF
#define MEM_GPS_DATA_START          0x1400
#define MEM_GPS_DATA_END            0x17FF
#define MEM_PROCD_DATA_START        0x1800
#define MEM_PROCD_DATA_END          0x1BFF


//configuration values offset
//standard ir premium slot number
#define MEM_CONFIG_SLOT_OFFSET        0x00 // length 1 byte
#define MEM_CONFIG_ECNRYPTON_OFFSET   0x01 // length 1 byte
#define MEM_CONFIG_TERMINAL_ID_OFFSET 0x02 // length 6 bytes
#define MEM_DBG_MODE_ENABLE_OFFSET    0x08 // length 1 byte
#define MEM_DBG_MODE_RF_OFFSET        0x09 // length 1 byte
#define MEM_DBG_MODE_SW_OFFSET        0x0A // length 1 byte
#define MEM_PWR_DOWN_OFFSET           0x0B // length 1 byte
#define MEM_GPS_DATA_REQ_OFFSET       0x0C // length 1 byte

 //cryptography keys

 // HMAC keys

 // Gold sequences offset

 //GPS messages
 // messages stored in RAM , they need to be removed from there

 //GPS data
 #define MEM_GPS_TIMESTAMP_OFFSET 0x00
 #define MEM GPS_LATITUDE_OFFSET  0x04
 #define MEM_GPS_LONGITUDE_OFFSET 0x08

 // PROC-D data offset
 #define MEM_PROCD_DATA_OFFSET 0x00




//typedef unsigned char bool;
enum Memory_pages
{
e_Software_Config,
e_Cryptograpy_keys,
e_HMAC_Keys,
e_Gold_Sequences,
e_GPS_Messages,
e_GPS_Data,
e_ProcD_Data
};

uint16_t ReadFromMemory(uint16_t address);
void WriteMemory(uint16_t address, uint16_t data, char erase_flag);
void EraseMemoryPage(uint16_t address);
