#include "ax8052.h"

#include "libaxlcd2.h"
#define uart_poll	   lcd2_poll
#define uart_txidle	   lcd2_txidle
#define uart_txfree	   lcd2_txfree
#define uart_wait_txdone   lcd2_wait_txdone
#define uart_wait_txfree   lcd2_wait_txfree
#define uart_txpokehex	   lcd2_txpokehex
#define uart_txpoke	   lcd2_txpoke
#define uart_txadvance	   lcd2_txadvance
#define uart_tx            lcd2_tx
#define uart_writestr      lcd2_writestr
#define uart_writehexu16   lcd2_writehexu16
#define uart_writehexu32   lcd2_writehexu32
#define uart_writeu16      lcd2_writeu16
#define uart_writeu32      lcd2_writeu32

__reentrantb void uart_wait_txfree(uint8_t v) __reentrant
{
	uint8_t iesave = (IE & 0x80);
	for (;;) {
		EA = 0;
		if (uart_txfree() >= v)
			break;
		if (!uart_poll())
			enter_standby();
		IE |= iesave;
	}
	IE |= iesave;
}

__reentrantb void uart_wait_txdone(void) __reentrant
{
	uint8_t iesave = (IE & 0x80);
	for (;;) {
		EA = 0;
		if (uart_txidle())
			break;
		if (!uart_poll())
			enter_standby();
		IE |= iesave;
	}
	IE |= iesave;
}

__reentrantb void uart_tx(uint8_t v) __reentrant
{
	uart_wait_txfree(1);
	uart_txpoke(0, v);
	uart_txadvance(1);
}
