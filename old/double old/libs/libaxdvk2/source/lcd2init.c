#include "ax8052.h"
#include "libaxlcd2.h"

#include <string.h>

#define WAITSHORT     0x01
#define WAITLONG      0x18
#define STATUSCMDSHORT  (0x40 | WAITSHORT)
#define STATUSCMDLONG   (0x40 | WAITLONG)
#define STATUSCMDDATA   (0xC0 | WAITSHORT)

static volatile uint8_t __data fifotxwr;
static volatile uint8_t __data fifotxrd;

extern uint8_t __xdata lcd2_txbuffer[];

#if !defined(SDCC)
extern const uint8_t __code lcd2_txbuffer_size[];
#endif

#if defined(SDCC)

static __reentrantb uint8_t lcd2_iocore(void) __reentrant __naked;

void lcd2_irq(void) __interrupt(10) __naked
{
	__asm;
	ar2=0x02
	ar3=0x03
	ar4=0x04
	ar5=0x05
	ar6=0x06
	ar7=0x07
	ar0=0x00
	ar1=0x01

	push	acc
	push	psw
	push	_DPS
	push	dpl
	push	dph
	mov	_DPS,#0
	mov	psw,#0
	mov	a,_SPSTATUS
	jnb	acc.0,00000$
	mov	a,_SPSHREG
	lcall	_lcd2_iocore
00000$:	pop	dph
	pop	dpl
	pop	_DPS
	pop	psw
	pop	acc
	reti
	__endasm;
}

__reentrantb uint8_t lcd2_poll(void) __reentrant __naked
{
	__asm;
	mov	a,#0x80
	anl	a,_IE
	mov	b,a
	clr	_EA
	mov	a,_SPSTATUS
	jnb	acc.0,00000$
	mov	a,_SPSHREG
	lcall	_lcd2_iocore
00000$:	mov	a,b
	orl	_IE,a
	ret

	.area	LCD2S0 (CODE)
	.area	LCD2S1 (CODE)

	.area	LCD2S0 (CODE)
	__endasm;
}

static __reentrantb uint8_t lcd2_iocore(void) __reentrant __naked
{
	__asm
	ar2=0x02
	ar3=0x03
	ar4=0x04
	ar5=0x05
	ar6=0x06
	ar7=0x07
	ar0=0x00
	ar1=0x01

	mov	a,_fifotxrd
	cjne	a,_fifotxwr,00000$
	setb	_PORTC_0
	mov	a,#~0x08
	anl	_SPMODE,a
	mov	dpl,#0x00
	ret
00000$:	add	a,acc
	mov	dptr,#_lcd2_txbuffer
	add	a,dpl
	mov	dpl,a
	clr	a
	addc	a,dph
	mov	dph,a
	movx	a,@dptr
	jnb	acc.6,00001$
	clr	_PORTC_0
	rlc	a
	mov	_PORTB_0,c
	rrc	a
	setb	_PORTB_1
	mov	_SPCLKSRC,#0xD8
	anl	a,#0x3F
	movx	@dptr,a
	inc	dptr
	movx	a,@dptr
	mov	_SPSHREG,a
00010$:	mov	dpl,#0x00
00011$:	mov	a,#0x08
	orl	_SPMODE,a
	ret
00001$:	setb	_PORTC_0
	mov	_SPCLKSRC,#0xF8
	mov	_SPSHREG,#0x00
	djnz	acc,00002$
	mov	a,#_lcd2_txbuffer_size+2-00012$
	movc	a,@a+pc
00012$:	add	a,_fifotxrd
	jc	00003$
	mov	a,_fifotxrd
	inc	a
00003$:	mov	_fifotxrd,a
	mov	dpl,#0x02
	sjmp	00011$
00002$:	movx	@dptr,a
	sjmp	00010$
	__endasm;
}

__reentrantb void lcd2_txadvance(uint8_t idx) __reentrant __naked
{
	idx;
	__asm;
	mov	a,#_lcd2_txbuffer_size+1-00003$
	movc	a,@a+pc
00003$:	xch	a,dpl
	jz	00000$
	add	a,_fifotxwr
	jnc	00002$
	add	a,dpl
	sjmp	00001$
00002$:	xch	a,dpl
	add	a,dpl
	jc	00001$
	mov	a,dpl
00001$:	mov	_fifotxwr,a
	mov	a,_SPMODE
	jb	acc.3,00000$
	ljmp	_lcd2_iocore
00000$:	ret
	__endasm;
}

__reentrantb uint8_t __xdata *lcd2_txbufptr(uint8_t idx) __reentrant __naked
{
	__asm;
	mov	a,#_lcd2_txbuffer_size+1-00003$
	movc	a,@a+pc
00003$:	xch	a,dpl
	add	a,_fifotxwr
	jnc	00002$
	add	a,dpl
	sjmp	00001$
00002$:	xch	a,dpl
	add	a,dpl
	jc	00001$
	mov	a,dpl
00001$:	add	a,acc
	mov	dptr,#_lcd2_txbuffer
	add	a,dpl
	mov	dpl,a
	clr	a
	addc	a,dph
	mov	dph,a
	ret
	__endasm;
}

__reentrantb uint8_t lcd2_txfreelinear(void) __reentrant __naked
{
	__asm;
	mov	a,_fifotxrd
	setb	c
	subb	a,_fifotxwr
	jnc	00000$
	mov	a,_fifotxrd
	add	a,#0xff
	cpl	c
	mov	a,#_lcd2_txbuffer_size-00001$
	movc	a,@a+pc
00001$:	subb	a,_fifotxwr
00000$:	mov	dpl,a
	ret
	__endasm;
}

__reentrantb uint8_t lcd2_txfree(void) __reentrant __naked
{
	__asm;
	mov	a,_fifotxrd
	setb	c
	subb	a,_fifotxwr
	mov	dpl,a
	jnc	00000$
	mov	a,#_lcd2_txbuffer_size-00001$
	movc	a,@a+pc
00001$:	add	a,dpl
	mov	dpl,a
00000$:	ret
	__endasm;
}

__reentrantb uint8_t lcd2_txbuffersize(void) __reentrant __naked
{
	__asm;
	mov	a,#_lcd2_txbuffer_size-00000$
	movc	a,@a+pc
00000$:	dec	a
	mov	dpl,a
	ret

_lcd2_txbuffer_size:
	.area CSEG    (CODE)
	__endasm;
}

__reentrantb void lcd2_txpokecmd(uint8_t idx, uint16_t cmd) __reentrant __naked
{
	idx;
	cmd;
	__asm;
	push	ar0
	mov	a,sp
	add	a,#-3
	mov	r0,a
	lcall	_lcd2_txbufptr
	mov	a,@r0
	movx	@dptr,a
	dec	r0
	inc	dptr
	mov	a,@r0
	movx	@dptr,a
	pop	ar0
	ret
	__endasm;
}

__reentrantb void lcd2_txpoke(uint8_t idx, uint8_t ch) __reentrant __naked
{
	idx;
	ch;
	__asm;
	push	ar0
	mov	a,sp
	add	a,#-3
	mov	r0,a
_lcd2_txpoke_hexentry:
	lcall	_lcd2_txbufptr
	mov	a,#STATUSCMDDATA
	movx	@dptr,a
	inc	dptr
	mov	a,@r0
	movx	@dptr,a
	pop	ar0
	ret
	__endasm;
}

__reentrantb void lcd2_txpokehex(uint8_t idx, uint8_t ch) __reentrant __naked
{
	idx;
	ch;
	__asm;
	push	ar0
	mov	a,sp
	add	a,#-3
	mov	r0,a
	mov	a,@r0
	anl	a,#0x0F
	add	a,#256-10
	jnc	00000$
	add	a,#'A-'9-1
00000$:	add	a,#10+'0	; '
	mov	@r0,a
	ljmp    _lcd2_txpoke_hexentry
	__endasm;
}

__reentrantb uint8_t lcd2_txidle(void) __reentrant __naked
{
	__asm;
	mov	a,_SPMODE
	swap	a
	rl	a
	cpl	a
	anl	a,#0x01
	mov	dpl,a
	ret
	__endasm;
}

static void wtimer_cansleep_dummy(void) __naked
{
	__asm
	.area WTCANSLP0 (CODE)
	.area WTCANSLP1 (CODE)
	.area WTCANSLP2 (CODE)

	.area WTCANSLP1 (CODE)
	lcall	_lcd2_txidle
	mov	a,dpl
	jnz	00000$
	ret
00000$:
	.area CSEG      (CODE)
	__endasm;
}

#elif defined __CX51__ || defined __C51__

static __reentrantb uint8_t lcd2_iocore(void) __reentrant;

__reentrantb void lcd2_irq(void) interrupt 10
{
#pragma asm
	push	acc
	push	psw
	push	DPS
	push	dpl
	push	dph
	push	ar7
	mov	DPS,#0
	mov	psw,#0
	mov	a,SPSTATUS
	jnb	acc.0,irqnotx
	mov	a,SPSHREG
	lcall	_lcd2_iocore
irqnotx:
	pop	ar7
	pop	dph
	pop	dpl
	pop	DPS
	pop	psw
	pop	acc
#pragma endasm
}

__reentrantb uint8_t lcd2_poll(void) __reentrant
{
#pragma asm
	mov	a,#0x80
	anl	a,IE
	mov	b,a
	clr	EA
	mov	a,SPSTATUS
	jnb	acc.0,pollnotx
	mov	a,SPSHREG
	lcall	_lcd2_iocore
pollnotx:
	mov	a,b
	orl	IE,a
#pragma endasm
}

static __reentrantb uint8_t lcd2_iocore(void) __reentrant
{
#pragma asm
;ar2	equ	0x02
;ar3	equ	0x03
;ar4	equ	0x04
;ar5	equ	0x05
;ar6	equ	0x06
;ar7	equ	0x07
;ar0	equ	0x00
;ar1	equ	0x01

_lcd2_iocore:
	mov	r7,#0x00
	mov	a,fifotxrd
	cjne	a,fifotxwr,iocor0
	setb	PORTC_0
	mov	a,#~0x08
	anl	SPMODE,a
	ret
iocor0:	add	a,acc
	mov	dptr,#lcd2_txbuffer
	add	a,dpl
	mov	dpl,a
	clr	a
	addc	a,dph
	mov	dph,a
	movx	a,@dptr
	jnb	acc.6,iocor1
	clr	PORTC_0
	rlc	a
	mov	PORTB_0,c
	rrc	a
	setb	PORTB_1
	mov	SPCLKSRC,#0xD8
	anl	a,#0x3F
	movx	@dptr,a
	inc	dptr
	movx	a,@dptr
	mov	SPSHREG,a
	sjmp	iorete
iocor2:	movx	@dptr,a
	sjmp	iorete
iocor1:	setb	PORTC_0
	mov	SPCLKSRC,#0xF8
	mov	SPSHREG,#0x00
	djnz	acc,iocor2
	clr	a
	mov	dptr,#lcd2_txbuffer_size+2
	movc	a,@a+dptr
	add	a,fifotxrd
	jc	iocor3
	mov	a,fifotxrd
	inc	a
iocor3:	mov	fifotxrd,a
	mov	r7,#0x02
iorete:	mov	a,#0x08
	orl	SPMODE,a
	ret
#pragma endasm
}

__reentrantb void lcd2_txadvance(uint8_t idx) __reentrant
{
#pragma asm
	clr	a
	mov	dptr,#lcd2_txbuffer_size+1
	movc	a,@a+dptr
	xch	a,r7
	jz	txad0
	add	a,fifotxwr
	jnc	txad2
	add	a,r7
	sjmp	txad1
txad2:	xch	a,r7
	add	a,r7
	jc	txad1
	mov	a,r7
txad1:	mov	fifotxwr,a
	lcall	_lcd2_iocore
txad0:
#pragma endasm
}

__reentrantb uint8_t __xdata *lcd2_txbufptr(uint8_t idx) __reentrant
{
#pragma asm
_lcd2_txbufptr:
	clr	a
	mov	dptr,#lcd2_txbuffer_size+1
	movc	a,@a+dptr
	xch	a,r7
	add	a,fifotxwr
	jnc	txpc2
	add	a,r7
	sjmp	txpc1
txpc2:	xch	a,r7
	add	a,r7
	jc	txpc1
	mov	a,r7
txpc1:	add	a,acc
	mov	dptr,#lcd2_txbuffer
	add	a,dpl
	mov	r7,a
	clr	a
	addc	a,dph
	mov	r6,a
#pragma endasm
}
	
__reentrantb uint8_t lcd2_txfreelinear(void) __reentrant
{
#pragma asm
	mov	a,fifotxrd
	setb	c
	subb	a,fifotxwr
	jnc	txfrl0
	mov	a,fifotxrd
	add	a,#0xff
	cpl	c
	clr	a
	mov	dptr,#lcd2_txbuffer_size
	movc	a,@a+dptr
	subb	a,fifotxwr
txfrl0:	mov	r7,a
#pragma endasm
}
	
__reentrantb uint8_t lcd2_txfree(void) __reentrant
{
#pragma asm
	mov	a,fifotxrd
	setb	c
	subb	a,fifotxwr
	mov	r7,a
	jnc	txfr0
	clr	a
	mov	dptr,#lcd2_txbuffer_size
	movc	a,@a+dptr
	add	a,r7
	mov	r7,a
txfr0:
#pragma endasm
}

__reentrantb uint8_t lcd2_txbuffersize(void) __reentrant
{
	return lcd2_txbuffer_size[0]-1;
}

__reentrantb void lcd2_txpokecmd(uint8_t idx, uint16_t cmd) __reentrant
{
#pragma asm
	lcall	_lcd2_txbufptr
	mov	dpl,r7
	mov	dph,r6
	mov	a,r4
	movx	@dptr,a
	inc	dptr
	mov	a,r5
	movx	@dptr,a
#pragma endasm
}

__reentrantb void lcd2_txpoke(uint8_t idx, uint8_t ch) __reentrant
{
#pragma asm
_lcd2_txpokehex_entry:
	lcall	_lcd2_txbufptr
	mov	dpl,r7
	mov	dph,r6
	mov	a,#STATUSCMDDATA
	movx	@dptr,a
	inc	dptr
	mov	a,r5
	movx	@dptr,a
#pragma endasm
}

__reentrantb void lcd2_txpokehex(uint8_t idx, uint8_t ch) __reentrant
{
#pragma asm
	mov	a,r5
	anl	a,#0x0F
	add	a,#256-10
	jnc	txph0
	add	a,#'A'-'9'-1
txph0:	add	a,#10+'0'
	mov	r5,a
	ljmp	_lcd2_txpokehex_entry
#pragma endasm
}

__reentrantb uint8_t lcd2_txidle(void) __reentrant
{
#pragma asm
	mov	a,SPMODE
	swap	a
	rl	a
	cpl	a
	anl	a,#0x01
	mov	r7,a
txnotidle:
#pragma endasm
}

#elif defined __ICC8051__

static __reentrantb uint8_t lcd2_iocore(void) __reentrant __naked;

#pragma vector=0x53
__interrupt void lcd2_irq(void)
{
	if (SPSTATUS & 0x01) {
		SPSHREG;
		lcd2_iocore();
	}
}

__reentrantb uint8_t lcd2_poll(void) __reentrant __naked
{
	uint8_t flg = 0;
	uint8_t irq = IE & 0x80;
	EA = 0;
	if (SPSTATUS & 0x01) {
		SPSHREG;
		flg = lcd2_iocore();
	}
	IE |= irq;
	return flg;
}

static __reentrantb uint8_t lcd2_iocore(void) __reentrant __naked
{
	uint8_t flg = 0;
	if (fifotxrd != fifotxwr) {
		uint8_t rp = fifotxrd + 1;
		uint8_t st = lcd2_txbuffer[fifotxrd << 1];
		if (st & 0x40) {
			PORTC_0 = 0;
			PORTB_0 = (st >> 7) & 1;
			PORTB_1 = 1;
			SPCLKSRC = 0xD8;
			st &= 0x3F;
			lcd2_txbuffer[fifotxrd << 1] = st;
			SPSHREG = lcd2_txbuffer[(fifotxrd << 1) + 1];
		} else {
			PORTC_0 = 1;
			SPCLKSRC = 0xF8;
			SPSHREG = 0x00;
			--st;
			if (!st) {
				uint8_t sz = lcd2_txbuffer_size[0];
				if (rp >= sz)
					rp -= sz;
				fifotxrd = rp;
				flg |= 2;
			} else {
				lcd2_txbuffer[fifotxrd << 1] = st;
			}
		}
		SPMODE |= 0x08;
	} else {
		PORTC_0 = 1;
		SPMODE &= (uint8_t)~0x08;
	}
	return flg;
}

__reentrantb void lcd2_txadvance(uint8_t idx) __reentrant __naked
{
	uint8_t wr;
	uint8_t sz;
	if (!idx)
		return;
	wr = fifotxwr;
	idx += wr;
	sz = lcd2_txbuffer_size[0];
	if (idx < wr || idx >= sz)
		idx -= sz;
	fifotxwr = idx;
	if (!(SPMODE & 0x08))
		lcd2_iocore();
}

__reentrantb uint8_t __xdata *lcd2_txbufptr(uint8_t idx) __reentrant __naked
{
	uint8_t wr = fifotxwr;
	uint8_t sz = lcd2_txbuffer_size[0];
	idx += wr;
	if (idx < wr || idx >= sz)
		idx -= sz;
	idx <<= 1;
	return &lcd2_txbuffer[idx];
}

__reentrantb uint8_t lcd2_txfreelinear(void) __reentrant __naked
{
	uint8_t rd = fifotxrd;
	uint8_t wr = fifotxwr;
	if (rd <= wr) {
		uint8_t r = lcd2_txbuffer_size[0] - wr;
		if (!rd)
			--r;
		return r;
	}
	return rd - wr - 1;
}

__reentrantb uint8_t lcd2_txfree(void) __reentrant __naked
{
	uint8_t rd = fifotxrd;
	uint8_t wr = fifotxwr;
	uint8_t r = rd - wr;
	if (rd <= wr)
		r += lcd2_txbuffer_size[0];
	--r;
	return r;
}

__reentrantb void lcd2_txpokecmd(uint8_t idx, uint16_t cmd) __reentrant __naked
{
	uint8_t __xdata *bp = lcd2_txbufptr(idx);
	*bp++ = cmd >> 8;
	*bp = cmd;
}

__reentrantb void lcd2_txpoke(uint8_t idx, uint8_t ch) __reentrant __naked
{
	uint8_t __xdata *bp = lcd2_txbufptr(idx);
	*bp++ = STATUSCMDDATA;
	*bp = ch;
}

__reentrantb void lcd2_txpokehex(uint8_t idx, uint8_t ch) __reentrant __naked
{
	ch &= 0x0F;
	if (ch >= 10)
		ch += 'A' - '9' - 1;
	ch += '0';
	lcd2_txpoke(idx, ch);
}

__reentrantb uint8_t lcd2_txidle(void) __reentrant __naked
{
	if (!(SPMODE & 0x08))
		return 1;
	return 0;
}

#else

#error "Compiler unsupported"

#endif

static const uint8_t __code lcdinitcmd[] = {
	WAITLONG, 0x00,       // Init Display
	STATUSCMDLONG, 0x30,  // wake-up
	STATUSCMDLONG, 0x30,  // wake-up
	STATUSCMDLONG, 0x30,  // wake-up
	STATUSCMDSHORT, 0x39, // function set
	STATUSCMDSHORT, 0x14, // internal osc frequency
	STATUSCMDSHORT, 0x56, // power control
	STATUSCMDSHORT, 0x6d, // follower control
	STATUSCMDSHORT, 0x78, // contrast
	STATUSCMDSHORT, 0x0c, // display on
	STATUSCMDSHORT, 0x06, // entry mode
	STATUSCMDLONG, 0x01, // clear display
	//STATUSCMDSHORT, 0x0f, // display on; cursor on
};

__reentrantb void lcd2_init(void) __reentrant
{
	EIE &= (uint8_t)~0x08;
	fifotxrd = 0;
	fifotxwr = sizeof(lcdinitcmd)/2;
#if defined(SDCC) || defined(__ICC8051__)
	{
		uint8_t i = sizeof(lcdinitcmd);
		const uint8_t __code *p0 = lcdinitcmd;
		uint8_t __xdata *p1 = lcd2_txbuffer;
		do {
			*p1++ = *p0++;
		} while (--i);
	}
#else
	// memcpy is not reentrant on SDCC
	memcpy(lcd2_txbuffer, lcdinitcmd, sizeof(lcdinitcmd));
#endif
	DIRB |= 0x03;
	PORTB &= (uint8_t)~0x03; // PB1=0: assert reset
	DIRC |= 0x07;
	PORTC |= 0x03;
	PORTC &= (uint8_t)~0x04;
	SPCLKSRC = 0xD8;
	SPMODE = 0x01;
	{
		uint8_t x = SPSHREG;
	}
	EIE |= 0x08;
	lcd2_iocore();
}

__reentrantb void lcd2_portinit(void) __reentrant
{
	EIE &= (uint8_t)~0x08;
	fifotxrd = fifotxwr = 0;
	DIRB |= 0x03;
	PORTB &= (uint8_t)~0x01;
	PORTB_1 = 1;
	DIRC |= 0x07;
	PORTC |= 0x03;
	PORTC &= (uint8_t)~0x04;
	SPCLKSRC = 0xD8;
	SPMODE = 0x01;
	{
		uint8_t x = SPSHREG;
	}
	EIE |= 0x08;
}
