/*-------------------------------------------------------------------------
   ax8052regaddr.h - Register Address Declarations for the Axsem Microfoot Processor Range

   Copyright (C) 2010, 2011, 2012 Axsem AG
     Author: Thomas Sailer, thomas.sailer@axsem.com

   This library is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2.1, or (at your option) any
   later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this library; see the file COPYING. If not, write to the
   Free Software Foundation, 51 Franklin Street, Fifth Floor, Boston,
   MA 02110-1301, USA.

   As a special exception, if you link this library with other files,
   some of which are compiled with SDCC, to produce an executable,
   this library does not by itself cause the resulting executable to
   be covered by the GNU General Public License. This exception does
   not however invalidate any other reasons why the executable file
   might be covered by the GNU General Public License.
-------------------------------------------------------------------------*/


#ifndef AX8052CRYPTOREGADDR_H
#define AX8052CRYPTOREGADDR_H


/* X Address Space */

#define SFRXADDR_AESCONFIG       0x7091  /* AES Configuration */
#define SFRXADDR_AESCURBLOCK     0x7098  /* AES Current Block Number */
#define SFRXADDR_AESINADDR0      0x7094  /* AES Input Address Low Byte */
#define SFRXADDR_AESINADDR1      0x7095  /* AES Input Address High Byte */
#define SFRXADDR_AESKEYADDR0     0x7092  /* AES Keystream Address Low Byte */
#define SFRXADDR_AESKEYADDR1     0x7093  /* AES Keystream Address High Byte */
#define SFRXADDR_AESMODE         0x7090  /* AES Mode */
#define SFRXADDR_AESOUTADDR0     0x7096  /* AES Output Address Low Byte */
#define SFRXADDR_AESOUTADDR1     0x7097  /* AES Output Address High Byte */
#define SFRXADDR_RNGBYTE         0x7081  /* True Random Byte */
#define SFRXADDR_RNGCLKSRC0      0x7082  /* True Random Number Generator Clock Source 0 */
#define SFRXADDR_RNGCLKSRC1      0x7083  /* True Random Number Generator Clock Source 1 */
#define SFRXADDR_RNGMODE         0x7080  /* True Random Number Generator Mode */

#endif /* AX8052CRYPTOREGADDR_H */
