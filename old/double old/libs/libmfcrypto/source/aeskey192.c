#include "ax8052crypto.h"
#include "aes.h"
#include <string.h>

void aes192_setup(const uint8_t key[24], uint8_t __xdata *keyschedule)
{
	memcpy((void *)keyschedule, key, 24);
	aes_keyexp(keyschedule, 6, 12);
	AESCONFIG = (AESCONFIG & 0xC0) | 12;
	AESKEYADDR0 = (uint16_t)keyschedule;
	AESKEYADDR1 = ((uint16_t)keyschedule) >> 8;
}
