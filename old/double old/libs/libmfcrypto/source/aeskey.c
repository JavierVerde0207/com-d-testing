#include "aes.h"

/*
 * Nb=4
 * Nk=4,6,8
 * Nr=10,12,14
 */

void aes_keyexp(uint8_t __xdata *keysched, uint8_t Nk, uint8_t Nr)
{
	uint8_t __autodata wcnt;
	uint8_t __autodata w[4];
	uint8_t __autodata bnum;
	uint8_t __autodata rcon;

	wcnt = 4 * (Nr+1);
	if (wcnt <= Nk)
		return;
	wcnt -= Nk;
	keysched += Nk * 4;
	bnum = 0;
	rcon = 1;
	do {
		keysched -= 4;
		w[0] = *keysched++;
		w[1] = *keysched++;
		w[2] = *keysched++;
		w[3] = *keysched++;
		if (!bnum) {
			/* RotWord */
			bnum = w[0];
			w[0] = w[1];
			w[1] = w[2];
			w[2] = w[3];
			w[3] = bnum;
			/* SubWord */
			w[0] = aes_sbox[w[0]];
			w[1] = aes_sbox[w[1]];
			w[2] = aes_sbox[w[2]];
			w[3] = aes_sbox[w[3]];
			/* xor rcon */
			w[0] ^= rcon;
			/* update rcon */
			bnum = (rcon & 0x80);
			rcon <<= 1;
			if (bnum)
				rcon ^= 0x1b;
			bnum = 0;
		} else if (Nk > 6 && bnum == 4) {
			/* SubWord */
			w[0] = aes_sbox[w[0]];
			w[1] = aes_sbox[w[1]];
			w[2] = aes_sbox[w[2]];
			w[3] = aes_sbox[w[3]];
		}
		keysched -= 4 * Nk;
		w[0] ^= *keysched++;
		w[1] ^= *keysched++;
		w[2] ^= *keysched++;
		w[3] ^= *keysched++;
		keysched += 4 * Nk - 4;
		*keysched++ = w[0];
		*keysched++ = w[1];
		*keysched++ = w[2];
		*keysched++ = w[3];
		++bnum;
		if (bnum >= Nk)
			bnum = 0;
	} while (--wcnt);
}
