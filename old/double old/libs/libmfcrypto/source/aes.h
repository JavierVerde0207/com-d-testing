#ifndef AES_H
#define AES_H

#include "libmftypes.h"
#include "libmfaes.h"

/*
 * AES Internals
 */
extern const uint8_t __code aes_sbox[256];
extern const uint8_t __code aes_rcon[255];

#endif /* AES_H */
