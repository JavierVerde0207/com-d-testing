#ifndef DES_H
#define DES_H

#include "libmftypes.h"
#include "libmfdes.h"

#define DES_ASM

/*
 * DES Internals
 */
extern const uint8_t __code des_sbox[8][64];

extern void des_encdec(uint16_t flags, const uint8_t __xdata *inptr, uint8_t __xdata *outptr, const uint8_t __xdata *keysched);

#endif /* DES_H */
