#ifndef LIBMFDES_H
#define LIBMFDES_H

#include "libmftypes.h"

/*
 * DES Encrypt/Decrypt
 */

extern void des_encrypt(const uint8_t __xdata *inptr, uint8_t __xdata *outptr, const uint8_t __xdata *keysched, uint8_t nr);
extern void des_decrypt(const uint8_t __xdata *inptr, uint8_t __xdata *outptr, const uint8_t __xdata *keysched, uint8_t nr);

/*
 * DES Key Schedule Expansion
 */
extern void des_keyexp(const uint8_t key[8], uint8_t __xdata *keysched); /* keyschedule size 128 bytes */

#endif /* LIBMFDES_H */
