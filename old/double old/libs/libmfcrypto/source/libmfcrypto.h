#ifndef LIBMFCRYPTO_H
#define LIBMFCRYPTO_H

#define LIBMFCRYPTOVERSION 20120217L

#include "libmfaes.h"
#include "libmfdes.h"

#endif /* LIBMFCRYPTO_H */
