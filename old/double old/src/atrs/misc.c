/**
*   \file
*   \brief Miscelaneus functions
*
*    This module contains function for complementing different tasks
*
*   \author Javier Verde
*   \author Aistech Space
*/
// Copyright (c) 2007,2008,2009,2010,2011,2012,2013, 2014 AXSEM AG
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//     1.Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     2.Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     3.Neither the name of AXSEM AG, Duebendorf nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//     4.All advertising materials mentioning features or use of this software
//       must display the following acknowledgement:
//       This product includes software developed by AXSEM AG and its contributors.
//     5.The usage of this source code is only granted for operation with AX5043
//       and AX8052F143. Porting to other radio or communication devices is
//       strictly prohibited.
//
// THIS SOFTWARE IS PROVIDED BY AXSEM AG AND CONTRIBUTORS ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL AXSEM AG AND CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

//libs
#include <ax8052.h>
#include <libmftypes.h>
#include <libmfwtimer.h>

//includes
#include "misc.h"
#include "axradio.h"
#include "display_com0.h"
#include "configmaster.h"




static struct wtimer_desc __xdata delaymstimer;
struct wtimer_desc __xdata wakeup_tmr1;
struct wtimer_desc __xdata wakeup_tmr2;
struct wtimer_desc __xdata wakeup_tmr3;




/**
* Callback for delayms function
*/
static void delayms_callback(struct wtimer_desc __xdata *desc)
{
    desc;
    delaymstimer.handler = 0;
}
/**
* Creates a delay of ms milliseconds
*
* @param[in] ms length of the delay in milliseconds
*/
__reentrantb void delay_ms(uint16_t ms) __reentrant
{
    // scaling: 20e6/64/1e3=312.5=2^8+2^6-2^3+2^-1
    uint32_t x;
    wtimer_remove(&delaymstimer);
    x = ms;
    delaymstimer.time = ms >> 1;
    x <<= 3;
    delaymstimer.time -= x;
    x <<= 3;
    delaymstimer.time += x;
    x <<= 2;
    delaymstimer.time += x;
    delaymstimer.handler = delayms_callback;
    wtimer1_addrelative(&delaymstimer);
    wtimer_runcallbacks();
    do {
        wtimer_idle(WTFLAG_CANSTANDBY);
        wtimer_runcallbacks();
    } while (delaymstimer.handler);
}


static void wakeup_callback1(struct wtimer_desc __xdata *desc)
{

   desc;
   //recargar el contador
    dbglink_writestr(" timer1 ");

   //Tmr0Flg = 0x01;
   #ifdef TRANSMIT
   transmit_packet();
    #endif // TRANSMIT
}

static void wakeup_callback2(struct wtimer_desc __xdata *desc)
{

   desc;
   //recargar el contador
    dbglink_writestr(" timer2 ");

   //Tmr0Flg = 0x01;
   #ifdef TRANSMIT
   transmit_packet();
    #endif // TRANSMIT
}

static void wakeup_callback3(struct wtimer_desc __xdata *desc)
{

   desc;
   //recargar el contador
    dbglink_writestr(" timer3 ");

   //Tmr0Flg = 0x01;
   #ifdef TRANSMIT
   transmit_packet();
    #endif // TRANSMIT
}


/**
* \brief sets up timer for three transmisions
*
* This function sets the value for the timer, after the beacon is received
* And the slot where the transmissions will be done are chosen
*
* @param[in] time1_ms : value in milliseconds for the first message
* @param[in] time2_ms : value in milliseconds for the second message
* @param[in] time3_ms : value in milliseconds for the third message
*
*/

void delay_tx(uint32_t time1_ms, uint32_t time2_ms, uint32_t time3_ms)
{



    wakeup_tmr1.handler = wakeup_callback1;//Handler for timer 0
    time1_ms = (uint32_t) TIME_FOR_TIMER_MSEC(time1_ms);
    dbglink_writestr("timer1: ");
    dbglink_writehex32(time1_ms,1,WRNUM_PADZERO);
    wakeup_tmr1.time = time1_ms; //+= ? o solo = ? verificar esto
    wtimer0_addabsolute(&wakeup_tmr1);

    wakeup_tmr2.handler = wakeup_callback2;//Handler for timer 0
    time2_ms = (uint32_t) TIME_FOR_TIMER_MSEC(time2_ms);
    dbglink_writestr("timer2: ");
    dbglink_writehex32(time2_ms,1,WRNUM_PADZERO);
    wakeup_tmr2.time = time2_ms; //+= ? o solo = ? verificar esto
    wtimer0_addabsolute(&wakeup_tmr2);

    wakeup_tmr3.handler = wakeup_callback3;//Handler for timer 0
    time3_ms = (uint32_t) TIME_FOR_TIMER_MSEC(time3_ms);
    dbglink_writestr("timer3: ");
    dbglink_writehex32(time3_ms,1,WRNUM_PADZERO);
    wakeup_tmr3.time = time3_ms; //+= ? o solo = ? verificar esto
    wtimer0_addabsolute(&wakeup_tmr3);

}

