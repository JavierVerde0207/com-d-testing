/**
*   \file
*   \brief Beacon decoding module
*
*    This module contains function for beacon decoding and validation
*
*   \author Javier Verde
*   \author Aistech Space
*/
//includes
#include "Beacon.h"
#include "string.h"

//libs
#include <libmfdbglink.h>
#include <libmftypes.h>
/**
*  \brief Decode received beacon
* This function receives data from radio and verifies if its a beacon
* Works for uplink and downlink beacons
*
*  @param[in]  Rxbuffer: Reception buffer pointer
*  @param[out] beacon: global beacon structure for processing
*  @param[in]  length: Rx length received
*
*  @return Returns 1 if beacon is correctly decoded, 0 if not.(maybe i can give different values depending on the error) TBD
*/

__xdata void BEACON_decoding(uint8_t* Rxbuffer, RF_beacon_t* beacon, uint8_t length)
{
    int i=0;
    beacon->FlagReceived = VALID_BEACON;
    if(length == 23)
    {
        beacon->SatId = *Rxbuffer <<8 | *(Rxbuffer+1);
        beacon->BeaconID = (*(Rxbuffer+POS_BEACONID) & MASK_BEACONID)>> 4;
        //if its a beaconID for downlink beacon or uplink beacon FlagReceived is set to 1, meaning that a valid beacon has been received
        //if the beacon id is not recognized, then FlagReceived is set to 0
        if(beacon->BeaconID == UPLINK_BEACON)
        {
            beacon->NumPremSlots = *(Rxbuffer+POS_NUMPREM);
            beacon->NumACK = 0xFF; //invalid data
        }else if(beacon->BeaconID == DOWNLINK_BEACON)
            {
                beacon->NumACK = *(Rxbuffer+POS_NUMPREM);
                beacon->NumPremSlots = 0xFF; //invalid data

            }
            else beacon->FlagReceived = INVALID_BEACON;

        beacon->HMAC_flag =(*(Rxbuffer+POS_SECURITY) & MASK_HMAC_FLAG)>>7;
        beacon->HMAC_type = (*(Rxbuffer+POS_SECURITY) & MASK_HMAC_TYPE)>>4;
        beacon->Encryption_flag = (*(Rxbuffer+POS_SECURITY) & MASK_ENCRYPTION_FLAG)>>3;
        beacon->Encryption_type = *(Rxbuffer+POS_SECURITY) & MASK_ENCRYPTION_TYPE;
        //Swap NONCE pointers
        *(Rxbuffer+POS_NONCE)   = *(Rxbuffer+POS_NONCE) + *(Rxbuffer+POS_NONCE+1);
        *(Rxbuffer+POS_NONCE+1) = *(Rxbuffer+POS_NONCE) - *(Rxbuffer+POS_NONCE+1);
        *(Rxbuffer+POS_NONCE)   = *(Rxbuffer+POS_NONCE) - *(Rxbuffer+POS_NONCE+1);
        //swap ending
        memcpy(&beacon->Nonce,Rxbuffer+POS_NONCE,sizeof(uint16_t));
        //may need to swapp when used 1&5 , 2&4
        memcpy(&beacon->reserved,Rxbuffer+POS_RESERVED,sizeof(uint8_t)*5);
       //somehow it doesnt work all in one line
        beacon->CRC =  *(Rxbuffer+POS_CRC) <<8 | *(Rxbuffer+POS_CRC+1);
        beacon->CRC = beacon->CRC << 16;
        beacon->CRC = beacon->CRC | *(Rxbuffer+POS_CRC+2)<<8 | *(Rxbuffer+POS_CRC+3);

        beacon->HMAC_Msb = *(Rxbuffer+POS_HMAC_MSB)<<8 | *(Rxbuffer+POS_HMAC_MSB+1);
        beacon->HMAC_Msb = beacon->HMAC_Msb << 16;
        beacon->HMAC_Msb = beacon->HMAC_Msb | (*(Rxbuffer+POS_HMAC_MSB+2)<<8 | *(Rxbuffer+POS_HMAC_MSB+3) & 0x0000FFFF);

        beacon->HMAC_Lsb = *(Rxbuffer+POS_HMAC_LSB)<<8 | *(Rxbuffer+POS_HMAC_LSB+1);
        beacon->HMAC_Lsb = beacon->HMAC_Lsb << 16;
        beacon->HMAC_Lsb = beacon->HMAC_Lsb | (*(Rxbuffer+POS_HMAC_LSB+2)<<8 | *(Rxbuffer+POS_HMAC_LSB+3) & 0x0000FFFF);
    }else beacon->FlagReceived = INVALID_BEACON;

}

//Se deja el prototipo para la funcion de integracion
/**********************************************************************
*
* Function:BEACON_validation
*
* Description: This function received the structure and validates the
               beacon received, defines wheter is downlink, uplink
               or invalid
*
* Parameters:
*
*
* Returns:
*      RetVal = 0 if there is an error on beacon decoding ( agregar mas para debug ?)
*             = 1 if the validated beacon is uplink
*             = 2 if the validated beacon is downlink
*************************************************************************/




