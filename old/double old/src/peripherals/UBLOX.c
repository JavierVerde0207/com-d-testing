/***********************************************/
/***********************************************/
/*********  Driver for UBLOX GPS  **************/
/***********************************************/
/***********************************************/

#include "UBLOX.h"
#include <libmftypes.h>
#include <libmfuart1.h>
#include <libmfuart.h>
#include <stdlib.h>
#include <string.h>
#include <libmfdbglink.h>
#include <ax8052f143.h>
#include <libmfwtimer.h>
#include  "misc.h"


/**********************************************************************
*
* Function: UBLOX_GPS_PortInit
*
* Description: This function inits the necesarry UART to communicate with the GSP hardware
*
* Parameters: NULL
*
* Returns: NULL
*
**********************************************************************/
__reentrantb void UBLOX_GPS_PortInit(void) __reentrant
{
    uart_timer1_baud(CLKSRC_SYSCLK, 9600, 20000000UL);
    uart1_init(1, 8, 1);
}

/**********************************************************************
*
* Function: UBLOX_GPS_FletcherChecksum8
*
* Description: This function calculates fletcher 8 bit checksum as explained in Ublox documentation
*
* Parameters:
*     *buffer: pointer to buffer data, must only include message payload
*     CK_A: Variable where the first byte of the checksum is saved
*     CK_B: Variable where the second byte of the checksum is saved
*     length: Lenght of the payload data
*
* Returns: NULL
*
**********************************************************************/

void UBLOX_GPS_FletcherChecksum8 ( uint8_t * buffer, uint8_t *CK_A, uint8_t *CK_B, uint16_t length)
{
    uint8_t i;
    *CK_A = 0;
    *CK_B = 0;
	for (i = 0; i < length; i++)
    {
		*CK_A += buffer[i];

		*CK_B += *CK_A;
	}
	return;
}

/**********************************************************************
*
* Function: UBLOX_GPS_ParseData
*
* Description: This function receives a buffer with data from Ublox GPS, verifies the checksum and parses it. If Checksum fails will return an error code
*
* Parameters:
*     *rx_buffer: buffer with data received through UART.
*     received_length: Lenght of the received data from UART
*     msg_class: message class according to Ublox documentation
*     msg_id: message identifier (depends on message class)
*     msg_length: payload length
*     msg_payload: pointer to message payload
*
* Returns:
*      1: Checksum is correctly verified and message is correctly parsed
*      0: Checksum is not verified
*      2: Parse error
**********************************************************************/

/*uint8_t UBLOX_GPS_ParseData(uint8_t *rx_buffer,uint16_t received_length,uint8_t msg_class,uint8_t msg_id,uint16_t msg_length, uint8_t *msg_payload)
{
    uint8_t* __xdata buffer_aux;
    uint8_t RetVal=0;
    uint8_t  CK_A=0;
    uint8_t  CK_B=0;
    buffer_aux = malloc(received_length);
    memcpy(buffer_aux,rx_buffer,received_length);

    if(buffer_aux[UBX_HEADER1_POS] == UBX_HEADER1_VAL && buffer_aux[UBX_HEADER2_POS] == UBX_HEADER2_VAL)
    {

            UBLOX_GPS_FletcherChecksum8((uint8_t*)(buffer_aux+UBX_HEADER2_POS),&CK_A,&CK_B,received_length-4);

            if(CK_A == buffer_aux[received_length-1]&& CK_B == buffer_aux[received_length]) // verifico el checksum calculado
            {
            msg_class = buffer_aux[UBX_MESSAGE_CLASS];
            msg_id = buffer_aux[UBX_MESSAGE_ID];
            msg_length = buffer_aux[UBX_MESSAGE_LENGTH_MSB]<<8 && buffer_aux[UBX_MESSAGE_LENGTH_LSB];
            msg_payload = buffer_aux+UBX_HEADER_LENGHT;

            }
            else RetVal = 0;
    }
    else RetVal = 2;
    free(buffer_aux);
return RetVal;
}*/

/**********************************************************************
*
* Function: UBLOX_GPS_SendCommand_WaitACK
*
* Description: This function gets the different components of a Ublox messages, calculates the checksum, creates the UBX packet and sends it via UART
*
* Parameters:
*    msg_class: message class according to Ublox documentation
*    msg_id: message identifier (depends on message class)
*    msg_length: payload length
*    *payload: buffer with payload data.
*
* Returns:
*      1: ACK received correctly
*      0: NAK received
**********************************************************************/

uint8_t UBLOX_GPS_SendCommand_WaitACK(uint8_t msg_class, uint8_t msg_id, uint16_t msg_length,uint8_t *payload,uint8_t AnsOrAck,uint16_t *RxLength)
{   uint8_t i;
    uint8_t k;
    uint8_t RetVal = 0;
    uint8_t __xdata *buffer_aux;
    uint8_t __xdata *rx_buffer;
    uint8_t CK_A,CK_B;
    buffer_aux =(uint8_t *) malloc((msg_length)+10);
    rx_buffer =(uint8_t *) malloc(30);
     for(i = 0; i<40;i++)
    {
        *(rx_buffer+i)=0;
    }
    i=0;
    *buffer_aux = msg_class;
    buffer_aux++;
    *buffer_aux = msg_id;
    buffer_aux++;
    *buffer_aux = msg_length;
    buffer_aux +=2;
    memcpy(buffer_aux,payload,msg_length);
    buffer_aux -=4;
    UBLOX_GPS_FletcherChecksum8(buffer_aux,&CK_A,&CK_B,(msg_length)+4);

    uart1_tx(UBX_HEADER1_VAL);
    uart1_tx(UBX_HEADER2_VAL);
    uart1_tx(msg_class);
    uart1_tx(msg_id);
    uart1_tx((uint8_t)(msg_length & 0x00FF));//LSB
    uart1_tx((uint8_t)((msg_length & 0xFF00)>>8));//MSB


    for(i = 0;i<msg_length;i++)
    {
        uart1_tx(*(payload+i));

    }
    uart1_tx(CK_A);
    uart1_tx(CK_B);

   //message reception,
   do{
    delay(2500);
   }while(!uart1_rxcount());
   delay(25000);
   k=0;

    do
    {
        wtimer_runcallbacks(); // si no pongo esto por algun motivo se pierden los eventos de timer , cuidado con los delays !!
        *(rx_buffer+k)=uart1_rx();
        k++;
        delay(2500);
    }while(uart1_rxcount());

    //format and CRC verification
  if(rx_buffer[UBX_HEADER1_POS] == UBX_HEADER1_VAL && rx_buffer[UBX_HEADER2_POS] == UBX_HEADER2_VAL)
    {
        CK_A = 0;
        CK_B = 0;
        UBLOX_GPS_FletcherChecksum8(&rx_buffer[UBX_MESSAGE_CLASS],&CK_A,&CK_B,k-4);
         if(CK_A == rx_buffer[k-2] && CK_B == rx_buffer[k-1]) // verifico el checksum calculado
            {
                //CRC and format verified, decide if ANS or ACK
                if(!AnsOrAck)
                //ANS
                {
                    if(msg_class == rx_buffer[UBX_MESSAGE_CLASS] && msg_id == rx_buffer[UBX_MESSAGE_ID])
                    {
                        *RxLength = rx_buffer[UBX_MESSAGE_LENGTH_MSB]<<8 | rx_buffer[UBX_MESSAGE_LENGTH_LSB];
                        memcpy(payload,&rx_buffer[UBX_MESSAGE_PAYLOAD],*RxLength);
                        RetVal = OK;
                    }
                }
                else
                //ACK
                {
                    if(rx_buffer[UBX_MESSAGE_CLASS] == eACK && rx_buffer[UBX_MESSAGE_ID] == ACK) RetVal = ACK;
                    else RetVal = NAK;
                }
            }
    }
    free(buffer_aux);
    free(rx_buffer);
    return RetVal;
}
