/***********************************************/
/***********************************************/
/*********  Gold sequence generator  ***********/
/***********************************************/
/***********************************************/

#include <stdlib.h>
#include "GoldSequence.h"
#include "MemoryMap.h"
#include <libmfflash.h>
#include <libmfdbglink.h>
#include "misc.h"
#include "Memory.h"



/**********************************************************************
*
* Function: GOLDSEQUENCE_Init
*
* Description: This function loads the lookup table in eeprom memory
*
* Parameters: NULL
*
* Returns: NULL
*
**********************************************************************/
void GOLDSEQUENCE_Init(void)
{
    flash_unlock();
  //  flash_pageerase(0x02);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0000,0b1111100110100100);
  //  delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x0002,0b001010111011000);
 //   delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0004,0b1111100100110000);
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x0006,0b101101010001110);
 //   delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0008,0b0000000010010100);
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x000A,0b100111101010110);
 //   delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x000C,0b1000010100111100);
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x000E,0b011100010011111);
//    delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0010,0b0100011111101000);
//    delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x0012,0b000001101111011);
 //   delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0014,0b0010011010000010);
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x0016,0b001111010001001);
 //   delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0018,0b0001011000110111);
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x001A,0b001000001110000);
 //   delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x001C,0b1000111001101101);
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x001E,0b101011100001100);
  //  delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0020,0b1100001001000000);
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x0022,0b111010010110010);
 //   delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0024,0b1110010001010110);
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x0026,0b010010101101101);
 //   delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0028,0b0111011101011101);
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x002A,0b000110110000010);
 //   delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x002C,0b1011111011011000);
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x002E,0b101100111110101);
 //   delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0030,0b0101101000011010);
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x0032,0b011001111001110);
 //   delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0034,0b1010100001111011);
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x0036,0b000011011010011);
 //   delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0038,0b0101000101001011);
//    delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x003A,0b101110001011101);
 //   delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x003C,0b0010110111010011);
//    delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x003E,0b111000100011010);
 //   delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0040,0b1001001110011111);
//    delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x0042,0b110011110111001);
//    delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0044,0b0100110010111001);
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x0046,0b110110011101000);
 //   delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0048,0b1010001100101010);
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x004A,0b110100101000000);
//    delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x004C,0b1101010011100011);
//    delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x004E,0b010101110010100);
//    delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0050,0b1110111100000111);
//    delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x0052,0b100101011111110);
//    delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0054,0b1111001011110101);
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x0056,0b111101001001011);
//    delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x0058,0b0111110000001100);
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x005A,0b110001000010001);
 //   delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x005C,0b0011101101110000);
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x005E,0b010111000111100);
 //   delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0060,0b1001100011001110);
//    delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x0062,0b000100000101010);
//    delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0064,0b1100100100010001);
//    delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x0066,0b001101100100001);
//    delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0068,0b0110000111111110);
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x006A,0b101001010100100);
//    delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x006C,0b1011010110001001);
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x006E,0b011011001100110);
 //   delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0070,0b1101111110110010);
//    delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x0072,0b100010000000111);
//    delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0074,0b0110101010101111);
//    delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x0076,0b011110100110111);
//    delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0078,0b0011000000100001);
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x007A,0b100000110101111);
//    delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x007C,0b0001110101100110);
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x007E,0b111111111100011);
//    delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0080,0b0000101111000101);
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x0082,0b010000011000101);
//    delay_ms(5);
    flash_lock();

}

/**********************************************************************
*
* Function: GOLDSEQUENCE_Obtain
*
* Description: This function generates a random numbe (between 1 and 33)
*              and looks for the matching gold sequence stored in memory
*
* Parameters: NULL
*
* Returns: uint32_t , gold sequence value
*
**********************************************************************/

__xdata uint32_t GOLDSEQUENCE_Obtain(void)
{

__xdata uint16_t Rnd;
__xdata uint32_t RetVal;
Rnd = random();
flash_unlock();

Rnd = ((Rnd % 33)*4);  // MOD operation is done so that the number is between 0 and 33, once a number between 0 and 32 has been obtained, its multiplied by 4 to match the memory address
RetVal = ((uint32_t)flash_read(Rnd))<<16;
RetVal |= (((uint32_t)flash_read(Rnd+2)) & 0x0000FFFF)<<1;
//dbglink_writehex32(Rnd,1,WRNUM_PADZERO);
flash_lock();
return(RetVal);
}










