                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.6.0 #9615 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module misc
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _dbglink_writestr
                                     12 	.globl _wtimer_remove
                                     13 	.globl _wtimer1_addrelative
                                     14 	.globl _wtimer0_addabsolute
                                     15 	.globl _wtimer_runcallbacks
                                     16 	.globl _wtimer_idle
                                     17 	.globl _PORTC_7
                                     18 	.globl _PORTC_6
                                     19 	.globl _PORTC_5
                                     20 	.globl _PORTC_4
                                     21 	.globl _PORTC_3
                                     22 	.globl _PORTC_2
                                     23 	.globl _PORTC_1
                                     24 	.globl _PORTC_0
                                     25 	.globl _PORTB_7
                                     26 	.globl _PORTB_6
                                     27 	.globl _PORTB_5
                                     28 	.globl _PORTB_4
                                     29 	.globl _PORTB_3
                                     30 	.globl _PORTB_2
                                     31 	.globl _PORTB_1
                                     32 	.globl _PORTB_0
                                     33 	.globl _PORTA_7
                                     34 	.globl _PORTA_6
                                     35 	.globl _PORTA_5
                                     36 	.globl _PORTA_4
                                     37 	.globl _PORTA_3
                                     38 	.globl _PORTA_2
                                     39 	.globl _PORTA_1
                                     40 	.globl _PORTA_0
                                     41 	.globl _PINC_7
                                     42 	.globl _PINC_6
                                     43 	.globl _PINC_5
                                     44 	.globl _PINC_4
                                     45 	.globl _PINC_3
                                     46 	.globl _PINC_2
                                     47 	.globl _PINC_1
                                     48 	.globl _PINC_0
                                     49 	.globl _PINB_7
                                     50 	.globl _PINB_6
                                     51 	.globl _PINB_5
                                     52 	.globl _PINB_4
                                     53 	.globl _PINB_3
                                     54 	.globl _PINB_2
                                     55 	.globl _PINB_1
                                     56 	.globl _PINB_0
                                     57 	.globl _PINA_7
                                     58 	.globl _PINA_6
                                     59 	.globl _PINA_5
                                     60 	.globl _PINA_4
                                     61 	.globl _PINA_3
                                     62 	.globl _PINA_2
                                     63 	.globl _PINA_1
                                     64 	.globl _PINA_0
                                     65 	.globl _CY
                                     66 	.globl _AC
                                     67 	.globl _F0
                                     68 	.globl _RS1
                                     69 	.globl _RS0
                                     70 	.globl _OV
                                     71 	.globl _F1
                                     72 	.globl _P
                                     73 	.globl _IP_7
                                     74 	.globl _IP_6
                                     75 	.globl _IP_5
                                     76 	.globl _IP_4
                                     77 	.globl _IP_3
                                     78 	.globl _IP_2
                                     79 	.globl _IP_1
                                     80 	.globl _IP_0
                                     81 	.globl _EA
                                     82 	.globl _IE_7
                                     83 	.globl _IE_6
                                     84 	.globl _IE_5
                                     85 	.globl _IE_4
                                     86 	.globl _IE_3
                                     87 	.globl _IE_2
                                     88 	.globl _IE_1
                                     89 	.globl _IE_0
                                     90 	.globl _EIP_7
                                     91 	.globl _EIP_6
                                     92 	.globl _EIP_5
                                     93 	.globl _EIP_4
                                     94 	.globl _EIP_3
                                     95 	.globl _EIP_2
                                     96 	.globl _EIP_1
                                     97 	.globl _EIP_0
                                     98 	.globl _EIE_7
                                     99 	.globl _EIE_6
                                    100 	.globl _EIE_5
                                    101 	.globl _EIE_4
                                    102 	.globl _EIE_3
                                    103 	.globl _EIE_2
                                    104 	.globl _EIE_1
                                    105 	.globl _EIE_0
                                    106 	.globl _E2IP_7
                                    107 	.globl _E2IP_6
                                    108 	.globl _E2IP_5
                                    109 	.globl _E2IP_4
                                    110 	.globl _E2IP_3
                                    111 	.globl _E2IP_2
                                    112 	.globl _E2IP_1
                                    113 	.globl _E2IP_0
                                    114 	.globl _E2IE_7
                                    115 	.globl _E2IE_6
                                    116 	.globl _E2IE_5
                                    117 	.globl _E2IE_4
                                    118 	.globl _E2IE_3
                                    119 	.globl _E2IE_2
                                    120 	.globl _E2IE_1
                                    121 	.globl _E2IE_0
                                    122 	.globl _B_7
                                    123 	.globl _B_6
                                    124 	.globl _B_5
                                    125 	.globl _B_4
                                    126 	.globl _B_3
                                    127 	.globl _B_2
                                    128 	.globl _B_1
                                    129 	.globl _B_0
                                    130 	.globl _ACC_7
                                    131 	.globl _ACC_6
                                    132 	.globl _ACC_5
                                    133 	.globl _ACC_4
                                    134 	.globl _ACC_3
                                    135 	.globl _ACC_2
                                    136 	.globl _ACC_1
                                    137 	.globl _ACC_0
                                    138 	.globl _WTSTAT
                                    139 	.globl _WTIRQEN
                                    140 	.globl _WTEVTD
                                    141 	.globl _WTEVTD1
                                    142 	.globl _WTEVTD0
                                    143 	.globl _WTEVTC
                                    144 	.globl _WTEVTC1
                                    145 	.globl _WTEVTC0
                                    146 	.globl _WTEVTB
                                    147 	.globl _WTEVTB1
                                    148 	.globl _WTEVTB0
                                    149 	.globl _WTEVTA
                                    150 	.globl _WTEVTA1
                                    151 	.globl _WTEVTA0
                                    152 	.globl _WTCNTR1
                                    153 	.globl _WTCNTB
                                    154 	.globl _WTCNTB1
                                    155 	.globl _WTCNTB0
                                    156 	.globl _WTCNTA
                                    157 	.globl _WTCNTA1
                                    158 	.globl _WTCNTA0
                                    159 	.globl _WTCFGB
                                    160 	.globl _WTCFGA
                                    161 	.globl _WDTRESET
                                    162 	.globl _WDTCFG
                                    163 	.globl _U1STATUS
                                    164 	.globl _U1SHREG
                                    165 	.globl _U1MODE
                                    166 	.globl _U1CTRL
                                    167 	.globl _U0STATUS
                                    168 	.globl _U0SHREG
                                    169 	.globl _U0MODE
                                    170 	.globl _U0CTRL
                                    171 	.globl _T2STATUS
                                    172 	.globl _T2PERIOD
                                    173 	.globl _T2PERIOD1
                                    174 	.globl _T2PERIOD0
                                    175 	.globl _T2MODE
                                    176 	.globl _T2CNT
                                    177 	.globl _T2CNT1
                                    178 	.globl _T2CNT0
                                    179 	.globl _T2CLKSRC
                                    180 	.globl _T1STATUS
                                    181 	.globl _T1PERIOD
                                    182 	.globl _T1PERIOD1
                                    183 	.globl _T1PERIOD0
                                    184 	.globl _T1MODE
                                    185 	.globl _T1CNT
                                    186 	.globl _T1CNT1
                                    187 	.globl _T1CNT0
                                    188 	.globl _T1CLKSRC
                                    189 	.globl _T0STATUS
                                    190 	.globl _T0PERIOD
                                    191 	.globl _T0PERIOD1
                                    192 	.globl _T0PERIOD0
                                    193 	.globl _T0MODE
                                    194 	.globl _T0CNT
                                    195 	.globl _T0CNT1
                                    196 	.globl _T0CNT0
                                    197 	.globl _T0CLKSRC
                                    198 	.globl _SPSTATUS
                                    199 	.globl _SPSHREG
                                    200 	.globl _SPMODE
                                    201 	.globl _SPCLKSRC
                                    202 	.globl _RADIOSTAT
                                    203 	.globl _RADIOSTAT1
                                    204 	.globl _RADIOSTAT0
                                    205 	.globl _RADIODATA
                                    206 	.globl _RADIODATA3
                                    207 	.globl _RADIODATA2
                                    208 	.globl _RADIODATA1
                                    209 	.globl _RADIODATA0
                                    210 	.globl _RADIOADDR
                                    211 	.globl _RADIOADDR1
                                    212 	.globl _RADIOADDR0
                                    213 	.globl _RADIOACC
                                    214 	.globl _OC1STATUS
                                    215 	.globl _OC1PIN
                                    216 	.globl _OC1MODE
                                    217 	.globl _OC1COMP
                                    218 	.globl _OC1COMP1
                                    219 	.globl _OC1COMP0
                                    220 	.globl _OC0STATUS
                                    221 	.globl _OC0PIN
                                    222 	.globl _OC0MODE
                                    223 	.globl _OC0COMP
                                    224 	.globl _OC0COMP1
                                    225 	.globl _OC0COMP0
                                    226 	.globl _NVSTATUS
                                    227 	.globl _NVKEY
                                    228 	.globl _NVDATA
                                    229 	.globl _NVDATA1
                                    230 	.globl _NVDATA0
                                    231 	.globl _NVADDR
                                    232 	.globl _NVADDR1
                                    233 	.globl _NVADDR0
                                    234 	.globl _IC1STATUS
                                    235 	.globl _IC1MODE
                                    236 	.globl _IC1CAPT
                                    237 	.globl _IC1CAPT1
                                    238 	.globl _IC1CAPT0
                                    239 	.globl _IC0STATUS
                                    240 	.globl _IC0MODE
                                    241 	.globl _IC0CAPT
                                    242 	.globl _IC0CAPT1
                                    243 	.globl _IC0CAPT0
                                    244 	.globl _PORTR
                                    245 	.globl _PORTC
                                    246 	.globl _PORTB
                                    247 	.globl _PORTA
                                    248 	.globl _PINR
                                    249 	.globl _PINC
                                    250 	.globl _PINB
                                    251 	.globl _PINA
                                    252 	.globl _DIRR
                                    253 	.globl _DIRC
                                    254 	.globl _DIRB
                                    255 	.globl _DIRA
                                    256 	.globl _DBGLNKSTAT
                                    257 	.globl _DBGLNKBUF
                                    258 	.globl _CODECONFIG
                                    259 	.globl _CLKSTAT
                                    260 	.globl _CLKCON
                                    261 	.globl _ANALOGCOMP
                                    262 	.globl _ADCCONV
                                    263 	.globl _ADCCLKSRC
                                    264 	.globl _ADCCH3CONFIG
                                    265 	.globl _ADCCH2CONFIG
                                    266 	.globl _ADCCH1CONFIG
                                    267 	.globl _ADCCH0CONFIG
                                    268 	.globl __XPAGE
                                    269 	.globl _XPAGE
                                    270 	.globl _SP
                                    271 	.globl _PSW
                                    272 	.globl _PCON
                                    273 	.globl _IP
                                    274 	.globl _IE
                                    275 	.globl _EIP
                                    276 	.globl _EIE
                                    277 	.globl _E2IP
                                    278 	.globl _E2IE
                                    279 	.globl _DPS
                                    280 	.globl _DPTR1
                                    281 	.globl _DPTR0
                                    282 	.globl _DPL1
                                    283 	.globl _DPL
                                    284 	.globl _DPH1
                                    285 	.globl _DPH
                                    286 	.globl _B
                                    287 	.globl _ACC
                                    288 	.globl _wakeup_desc
                                    289 	.globl _XTALREADY
                                    290 	.globl _XTALOSC
                                    291 	.globl _XTALAMPL
                                    292 	.globl _SILICONREV
                                    293 	.globl _SCRATCH3
                                    294 	.globl _SCRATCH2
                                    295 	.globl _SCRATCH1
                                    296 	.globl _SCRATCH0
                                    297 	.globl _RADIOMUX
                                    298 	.globl _RADIOFSTATADDR
                                    299 	.globl _RADIOFSTATADDR1
                                    300 	.globl _RADIOFSTATADDR0
                                    301 	.globl _RADIOFDATAADDR
                                    302 	.globl _RADIOFDATAADDR1
                                    303 	.globl _RADIOFDATAADDR0
                                    304 	.globl _OSCRUN
                                    305 	.globl _OSCREADY
                                    306 	.globl _OSCFORCERUN
                                    307 	.globl _OSCCALIB
                                    308 	.globl _MISCCTRL
                                    309 	.globl _LPXOSCGM
                                    310 	.globl _LPOSCREF
                                    311 	.globl _LPOSCREF1
                                    312 	.globl _LPOSCREF0
                                    313 	.globl _LPOSCPER
                                    314 	.globl _LPOSCPER1
                                    315 	.globl _LPOSCPER0
                                    316 	.globl _LPOSCKFILT
                                    317 	.globl _LPOSCKFILT1
                                    318 	.globl _LPOSCKFILT0
                                    319 	.globl _LPOSCFREQ
                                    320 	.globl _LPOSCFREQ1
                                    321 	.globl _LPOSCFREQ0
                                    322 	.globl _LPOSCCONFIG
                                    323 	.globl _PINSEL
                                    324 	.globl _PINCHGC
                                    325 	.globl _PINCHGB
                                    326 	.globl _PINCHGA
                                    327 	.globl _PALTRADIO
                                    328 	.globl _PALTC
                                    329 	.globl _PALTB
                                    330 	.globl _PALTA
                                    331 	.globl _INTCHGC
                                    332 	.globl _INTCHGB
                                    333 	.globl _INTCHGA
                                    334 	.globl _EXTIRQ
                                    335 	.globl _GPIOENABLE
                                    336 	.globl _ANALOGA
                                    337 	.globl _FRCOSCREF
                                    338 	.globl _FRCOSCREF1
                                    339 	.globl _FRCOSCREF0
                                    340 	.globl _FRCOSCPER
                                    341 	.globl _FRCOSCPER1
                                    342 	.globl _FRCOSCPER0
                                    343 	.globl _FRCOSCKFILT
                                    344 	.globl _FRCOSCKFILT1
                                    345 	.globl _FRCOSCKFILT0
                                    346 	.globl _FRCOSCFREQ
                                    347 	.globl _FRCOSCFREQ1
                                    348 	.globl _FRCOSCFREQ0
                                    349 	.globl _FRCOSCCTRL
                                    350 	.globl _FRCOSCCONFIG
                                    351 	.globl _DMA1CONFIG
                                    352 	.globl _DMA1ADDR
                                    353 	.globl _DMA1ADDR1
                                    354 	.globl _DMA1ADDR0
                                    355 	.globl _DMA0CONFIG
                                    356 	.globl _DMA0ADDR
                                    357 	.globl _DMA0ADDR1
                                    358 	.globl _DMA0ADDR0
                                    359 	.globl _ADCTUNE2
                                    360 	.globl _ADCTUNE1
                                    361 	.globl _ADCTUNE0
                                    362 	.globl _ADCCH3VAL
                                    363 	.globl _ADCCH3VAL1
                                    364 	.globl _ADCCH3VAL0
                                    365 	.globl _ADCCH2VAL
                                    366 	.globl _ADCCH2VAL1
                                    367 	.globl _ADCCH2VAL0
                                    368 	.globl _ADCCH1VAL
                                    369 	.globl _ADCCH1VAL1
                                    370 	.globl _ADCCH1VAL0
                                    371 	.globl _ADCCH0VAL
                                    372 	.globl _ADCCH0VAL1
                                    373 	.globl _ADCCH0VAL0
                                    374 	.globl _delay_ms
                                    375 	.globl _delay_tx
                                    376 ;--------------------------------------------------------
                                    377 ; special function registers
                                    378 ;--------------------------------------------------------
                                    379 	.area RSEG    (ABS,DATA)
      000000                        380 	.org 0x0000
                           0000E0   381 _ACC	=	0x00e0
                           0000F0   382 _B	=	0x00f0
                           000083   383 _DPH	=	0x0083
                           000085   384 _DPH1	=	0x0085
                           000082   385 _DPL	=	0x0082
                           000084   386 _DPL1	=	0x0084
                           008382   387 _DPTR0	=	0x8382
                           008584   388 _DPTR1	=	0x8584
                           000086   389 _DPS	=	0x0086
                           0000A0   390 _E2IE	=	0x00a0
                           0000C0   391 _E2IP	=	0x00c0
                           000098   392 _EIE	=	0x0098
                           0000B0   393 _EIP	=	0x00b0
                           0000A8   394 _IE	=	0x00a8
                           0000B8   395 _IP	=	0x00b8
                           000087   396 _PCON	=	0x0087
                           0000D0   397 _PSW	=	0x00d0
                           000081   398 _SP	=	0x0081
                           0000D9   399 _XPAGE	=	0x00d9
                           0000D9   400 __XPAGE	=	0x00d9
                           0000CA   401 _ADCCH0CONFIG	=	0x00ca
                           0000CB   402 _ADCCH1CONFIG	=	0x00cb
                           0000D2   403 _ADCCH2CONFIG	=	0x00d2
                           0000D3   404 _ADCCH3CONFIG	=	0x00d3
                           0000D1   405 _ADCCLKSRC	=	0x00d1
                           0000C9   406 _ADCCONV	=	0x00c9
                           0000E1   407 _ANALOGCOMP	=	0x00e1
                           0000C6   408 _CLKCON	=	0x00c6
                           0000C7   409 _CLKSTAT	=	0x00c7
                           000097   410 _CODECONFIG	=	0x0097
                           0000E3   411 _DBGLNKBUF	=	0x00e3
                           0000E2   412 _DBGLNKSTAT	=	0x00e2
                           000089   413 _DIRA	=	0x0089
                           00008A   414 _DIRB	=	0x008a
                           00008B   415 _DIRC	=	0x008b
                           00008E   416 _DIRR	=	0x008e
                           0000C8   417 _PINA	=	0x00c8
                           0000E8   418 _PINB	=	0x00e8
                           0000F8   419 _PINC	=	0x00f8
                           00008D   420 _PINR	=	0x008d
                           000080   421 _PORTA	=	0x0080
                           000088   422 _PORTB	=	0x0088
                           000090   423 _PORTC	=	0x0090
                           00008C   424 _PORTR	=	0x008c
                           0000CE   425 _IC0CAPT0	=	0x00ce
                           0000CF   426 _IC0CAPT1	=	0x00cf
                           00CFCE   427 _IC0CAPT	=	0xcfce
                           0000CC   428 _IC0MODE	=	0x00cc
                           0000CD   429 _IC0STATUS	=	0x00cd
                           0000D6   430 _IC1CAPT0	=	0x00d6
                           0000D7   431 _IC1CAPT1	=	0x00d7
                           00D7D6   432 _IC1CAPT	=	0xd7d6
                           0000D4   433 _IC1MODE	=	0x00d4
                           0000D5   434 _IC1STATUS	=	0x00d5
                           000092   435 _NVADDR0	=	0x0092
                           000093   436 _NVADDR1	=	0x0093
                           009392   437 _NVADDR	=	0x9392
                           000094   438 _NVDATA0	=	0x0094
                           000095   439 _NVDATA1	=	0x0095
                           009594   440 _NVDATA	=	0x9594
                           000096   441 _NVKEY	=	0x0096
                           000091   442 _NVSTATUS	=	0x0091
                           0000BC   443 _OC0COMP0	=	0x00bc
                           0000BD   444 _OC0COMP1	=	0x00bd
                           00BDBC   445 _OC0COMP	=	0xbdbc
                           0000B9   446 _OC0MODE	=	0x00b9
                           0000BA   447 _OC0PIN	=	0x00ba
                           0000BB   448 _OC0STATUS	=	0x00bb
                           0000C4   449 _OC1COMP0	=	0x00c4
                           0000C5   450 _OC1COMP1	=	0x00c5
                           00C5C4   451 _OC1COMP	=	0xc5c4
                           0000C1   452 _OC1MODE	=	0x00c1
                           0000C2   453 _OC1PIN	=	0x00c2
                           0000C3   454 _OC1STATUS	=	0x00c3
                           0000B1   455 _RADIOACC	=	0x00b1
                           0000B3   456 _RADIOADDR0	=	0x00b3
                           0000B2   457 _RADIOADDR1	=	0x00b2
                           00B2B3   458 _RADIOADDR	=	0xb2b3
                           0000B7   459 _RADIODATA0	=	0x00b7
                           0000B6   460 _RADIODATA1	=	0x00b6
                           0000B5   461 _RADIODATA2	=	0x00b5
                           0000B4   462 _RADIODATA3	=	0x00b4
                           B4B5B6B7   463 _RADIODATA	=	0xb4b5b6b7
                           0000BE   464 _RADIOSTAT0	=	0x00be
                           0000BF   465 _RADIOSTAT1	=	0x00bf
                           00BFBE   466 _RADIOSTAT	=	0xbfbe
                           0000DF   467 _SPCLKSRC	=	0x00df
                           0000DC   468 _SPMODE	=	0x00dc
                           0000DE   469 _SPSHREG	=	0x00de
                           0000DD   470 _SPSTATUS	=	0x00dd
                           00009A   471 _T0CLKSRC	=	0x009a
                           00009C   472 _T0CNT0	=	0x009c
                           00009D   473 _T0CNT1	=	0x009d
                           009D9C   474 _T0CNT	=	0x9d9c
                           000099   475 _T0MODE	=	0x0099
                           00009E   476 _T0PERIOD0	=	0x009e
                           00009F   477 _T0PERIOD1	=	0x009f
                           009F9E   478 _T0PERIOD	=	0x9f9e
                           00009B   479 _T0STATUS	=	0x009b
                           0000A2   480 _T1CLKSRC	=	0x00a2
                           0000A4   481 _T1CNT0	=	0x00a4
                           0000A5   482 _T1CNT1	=	0x00a5
                           00A5A4   483 _T1CNT	=	0xa5a4
                           0000A1   484 _T1MODE	=	0x00a1
                           0000A6   485 _T1PERIOD0	=	0x00a6
                           0000A7   486 _T1PERIOD1	=	0x00a7
                           00A7A6   487 _T1PERIOD	=	0xa7a6
                           0000A3   488 _T1STATUS	=	0x00a3
                           0000AA   489 _T2CLKSRC	=	0x00aa
                           0000AC   490 _T2CNT0	=	0x00ac
                           0000AD   491 _T2CNT1	=	0x00ad
                           00ADAC   492 _T2CNT	=	0xadac
                           0000A9   493 _T2MODE	=	0x00a9
                           0000AE   494 _T2PERIOD0	=	0x00ae
                           0000AF   495 _T2PERIOD1	=	0x00af
                           00AFAE   496 _T2PERIOD	=	0xafae
                           0000AB   497 _T2STATUS	=	0x00ab
                           0000E4   498 _U0CTRL	=	0x00e4
                           0000E7   499 _U0MODE	=	0x00e7
                           0000E6   500 _U0SHREG	=	0x00e6
                           0000E5   501 _U0STATUS	=	0x00e5
                           0000EC   502 _U1CTRL	=	0x00ec
                           0000EF   503 _U1MODE	=	0x00ef
                           0000EE   504 _U1SHREG	=	0x00ee
                           0000ED   505 _U1STATUS	=	0x00ed
                           0000DA   506 _WDTCFG	=	0x00da
                           0000DB   507 _WDTRESET	=	0x00db
                           0000F1   508 _WTCFGA	=	0x00f1
                           0000F9   509 _WTCFGB	=	0x00f9
                           0000F2   510 _WTCNTA0	=	0x00f2
                           0000F3   511 _WTCNTA1	=	0x00f3
                           00F3F2   512 _WTCNTA	=	0xf3f2
                           0000FA   513 _WTCNTB0	=	0x00fa
                           0000FB   514 _WTCNTB1	=	0x00fb
                           00FBFA   515 _WTCNTB	=	0xfbfa
                           0000EB   516 _WTCNTR1	=	0x00eb
                           0000F4   517 _WTEVTA0	=	0x00f4
                           0000F5   518 _WTEVTA1	=	0x00f5
                           00F5F4   519 _WTEVTA	=	0xf5f4
                           0000F6   520 _WTEVTB0	=	0x00f6
                           0000F7   521 _WTEVTB1	=	0x00f7
                           00F7F6   522 _WTEVTB	=	0xf7f6
                           0000FC   523 _WTEVTC0	=	0x00fc
                           0000FD   524 _WTEVTC1	=	0x00fd
                           00FDFC   525 _WTEVTC	=	0xfdfc
                           0000FE   526 _WTEVTD0	=	0x00fe
                           0000FF   527 _WTEVTD1	=	0x00ff
                           00FFFE   528 _WTEVTD	=	0xfffe
                           0000E9   529 _WTIRQEN	=	0x00e9
                           0000EA   530 _WTSTAT	=	0x00ea
                                    531 ;--------------------------------------------------------
                                    532 ; special function bits
                                    533 ;--------------------------------------------------------
                                    534 	.area RSEG    (ABS,DATA)
      000000                        535 	.org 0x0000
                           0000E0   536 _ACC_0	=	0x00e0
                           0000E1   537 _ACC_1	=	0x00e1
                           0000E2   538 _ACC_2	=	0x00e2
                           0000E3   539 _ACC_3	=	0x00e3
                           0000E4   540 _ACC_4	=	0x00e4
                           0000E5   541 _ACC_5	=	0x00e5
                           0000E6   542 _ACC_6	=	0x00e6
                           0000E7   543 _ACC_7	=	0x00e7
                           0000F0   544 _B_0	=	0x00f0
                           0000F1   545 _B_1	=	0x00f1
                           0000F2   546 _B_2	=	0x00f2
                           0000F3   547 _B_3	=	0x00f3
                           0000F4   548 _B_4	=	0x00f4
                           0000F5   549 _B_5	=	0x00f5
                           0000F6   550 _B_6	=	0x00f6
                           0000F7   551 _B_7	=	0x00f7
                           0000A0   552 _E2IE_0	=	0x00a0
                           0000A1   553 _E2IE_1	=	0x00a1
                           0000A2   554 _E2IE_2	=	0x00a2
                           0000A3   555 _E2IE_3	=	0x00a3
                           0000A4   556 _E2IE_4	=	0x00a4
                           0000A5   557 _E2IE_5	=	0x00a5
                           0000A6   558 _E2IE_6	=	0x00a6
                           0000A7   559 _E2IE_7	=	0x00a7
                           0000C0   560 _E2IP_0	=	0x00c0
                           0000C1   561 _E2IP_1	=	0x00c1
                           0000C2   562 _E2IP_2	=	0x00c2
                           0000C3   563 _E2IP_3	=	0x00c3
                           0000C4   564 _E2IP_4	=	0x00c4
                           0000C5   565 _E2IP_5	=	0x00c5
                           0000C6   566 _E2IP_6	=	0x00c6
                           0000C7   567 _E2IP_7	=	0x00c7
                           000098   568 _EIE_0	=	0x0098
                           000099   569 _EIE_1	=	0x0099
                           00009A   570 _EIE_2	=	0x009a
                           00009B   571 _EIE_3	=	0x009b
                           00009C   572 _EIE_4	=	0x009c
                           00009D   573 _EIE_5	=	0x009d
                           00009E   574 _EIE_6	=	0x009e
                           00009F   575 _EIE_7	=	0x009f
                           0000B0   576 _EIP_0	=	0x00b0
                           0000B1   577 _EIP_1	=	0x00b1
                           0000B2   578 _EIP_2	=	0x00b2
                           0000B3   579 _EIP_3	=	0x00b3
                           0000B4   580 _EIP_4	=	0x00b4
                           0000B5   581 _EIP_5	=	0x00b5
                           0000B6   582 _EIP_6	=	0x00b6
                           0000B7   583 _EIP_7	=	0x00b7
                           0000A8   584 _IE_0	=	0x00a8
                           0000A9   585 _IE_1	=	0x00a9
                           0000AA   586 _IE_2	=	0x00aa
                           0000AB   587 _IE_3	=	0x00ab
                           0000AC   588 _IE_4	=	0x00ac
                           0000AD   589 _IE_5	=	0x00ad
                           0000AE   590 _IE_6	=	0x00ae
                           0000AF   591 _IE_7	=	0x00af
                           0000AF   592 _EA	=	0x00af
                           0000B8   593 _IP_0	=	0x00b8
                           0000B9   594 _IP_1	=	0x00b9
                           0000BA   595 _IP_2	=	0x00ba
                           0000BB   596 _IP_3	=	0x00bb
                           0000BC   597 _IP_4	=	0x00bc
                           0000BD   598 _IP_5	=	0x00bd
                           0000BE   599 _IP_6	=	0x00be
                           0000BF   600 _IP_7	=	0x00bf
                           0000D0   601 _P	=	0x00d0
                           0000D1   602 _F1	=	0x00d1
                           0000D2   603 _OV	=	0x00d2
                           0000D3   604 _RS0	=	0x00d3
                           0000D4   605 _RS1	=	0x00d4
                           0000D5   606 _F0	=	0x00d5
                           0000D6   607 _AC	=	0x00d6
                           0000D7   608 _CY	=	0x00d7
                           0000C8   609 _PINA_0	=	0x00c8
                           0000C9   610 _PINA_1	=	0x00c9
                           0000CA   611 _PINA_2	=	0x00ca
                           0000CB   612 _PINA_3	=	0x00cb
                           0000CC   613 _PINA_4	=	0x00cc
                           0000CD   614 _PINA_5	=	0x00cd
                           0000CE   615 _PINA_6	=	0x00ce
                           0000CF   616 _PINA_7	=	0x00cf
                           0000E8   617 _PINB_0	=	0x00e8
                           0000E9   618 _PINB_1	=	0x00e9
                           0000EA   619 _PINB_2	=	0x00ea
                           0000EB   620 _PINB_3	=	0x00eb
                           0000EC   621 _PINB_4	=	0x00ec
                           0000ED   622 _PINB_5	=	0x00ed
                           0000EE   623 _PINB_6	=	0x00ee
                           0000EF   624 _PINB_7	=	0x00ef
                           0000F8   625 _PINC_0	=	0x00f8
                           0000F9   626 _PINC_1	=	0x00f9
                           0000FA   627 _PINC_2	=	0x00fa
                           0000FB   628 _PINC_3	=	0x00fb
                           0000FC   629 _PINC_4	=	0x00fc
                           0000FD   630 _PINC_5	=	0x00fd
                           0000FE   631 _PINC_6	=	0x00fe
                           0000FF   632 _PINC_7	=	0x00ff
                           000080   633 _PORTA_0	=	0x0080
                           000081   634 _PORTA_1	=	0x0081
                           000082   635 _PORTA_2	=	0x0082
                           000083   636 _PORTA_3	=	0x0083
                           000084   637 _PORTA_4	=	0x0084
                           000085   638 _PORTA_5	=	0x0085
                           000086   639 _PORTA_6	=	0x0086
                           000087   640 _PORTA_7	=	0x0087
                           000088   641 _PORTB_0	=	0x0088
                           000089   642 _PORTB_1	=	0x0089
                           00008A   643 _PORTB_2	=	0x008a
                           00008B   644 _PORTB_3	=	0x008b
                           00008C   645 _PORTB_4	=	0x008c
                           00008D   646 _PORTB_5	=	0x008d
                           00008E   647 _PORTB_6	=	0x008e
                           00008F   648 _PORTB_7	=	0x008f
                           000090   649 _PORTC_0	=	0x0090
                           000091   650 _PORTC_1	=	0x0091
                           000092   651 _PORTC_2	=	0x0092
                           000093   652 _PORTC_3	=	0x0093
                           000094   653 _PORTC_4	=	0x0094
                           000095   654 _PORTC_5	=	0x0095
                           000096   655 _PORTC_6	=	0x0096
                           000097   656 _PORTC_7	=	0x0097
                                    657 ;--------------------------------------------------------
                                    658 ; overlayable register banks
                                    659 ;--------------------------------------------------------
                                    660 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        661 	.ds 8
                                    662 ;--------------------------------------------------------
                                    663 ; internal ram data
                                    664 ;--------------------------------------------------------
                                    665 	.area DSEG    (DATA)
                                    666 ;--------------------------------------------------------
                                    667 ; overlayable items in internal ram 
                                    668 ;--------------------------------------------------------
                                    669 ;--------------------------------------------------------
                                    670 ; indirectly addressable internal ram data
                                    671 ;--------------------------------------------------------
                                    672 	.area ISEG    (DATA)
                                    673 ;--------------------------------------------------------
                                    674 ; absolute internal ram data
                                    675 ;--------------------------------------------------------
                                    676 	.area IABS    (ABS,DATA)
                                    677 	.area IABS    (ABS,DATA)
                                    678 ;--------------------------------------------------------
                                    679 ; bit data
                                    680 ;--------------------------------------------------------
                                    681 	.area BSEG    (BIT)
                                    682 ;--------------------------------------------------------
                                    683 ; paged external ram data
                                    684 ;--------------------------------------------------------
                                    685 	.area PSEG    (PAG,XDATA)
                                    686 ;--------------------------------------------------------
                                    687 ; external ram data
                                    688 ;--------------------------------------------------------
                                    689 	.area XSEG    (XDATA)
                           007020   690 _ADCCH0VAL0	=	0x7020
                           007021   691 _ADCCH0VAL1	=	0x7021
                           007020   692 _ADCCH0VAL	=	0x7020
                           007022   693 _ADCCH1VAL0	=	0x7022
                           007023   694 _ADCCH1VAL1	=	0x7023
                           007022   695 _ADCCH1VAL	=	0x7022
                           007024   696 _ADCCH2VAL0	=	0x7024
                           007025   697 _ADCCH2VAL1	=	0x7025
                           007024   698 _ADCCH2VAL	=	0x7024
                           007026   699 _ADCCH3VAL0	=	0x7026
                           007027   700 _ADCCH3VAL1	=	0x7027
                           007026   701 _ADCCH3VAL	=	0x7026
                           007028   702 _ADCTUNE0	=	0x7028
                           007029   703 _ADCTUNE1	=	0x7029
                           00702A   704 _ADCTUNE2	=	0x702a
                           007010   705 _DMA0ADDR0	=	0x7010
                           007011   706 _DMA0ADDR1	=	0x7011
                           007010   707 _DMA0ADDR	=	0x7010
                           007014   708 _DMA0CONFIG	=	0x7014
                           007012   709 _DMA1ADDR0	=	0x7012
                           007013   710 _DMA1ADDR1	=	0x7013
                           007012   711 _DMA1ADDR	=	0x7012
                           007015   712 _DMA1CONFIG	=	0x7015
                           007070   713 _FRCOSCCONFIG	=	0x7070
                           007071   714 _FRCOSCCTRL	=	0x7071
                           007076   715 _FRCOSCFREQ0	=	0x7076
                           007077   716 _FRCOSCFREQ1	=	0x7077
                           007076   717 _FRCOSCFREQ	=	0x7076
                           007072   718 _FRCOSCKFILT0	=	0x7072
                           007073   719 _FRCOSCKFILT1	=	0x7073
                           007072   720 _FRCOSCKFILT	=	0x7072
                           007078   721 _FRCOSCPER0	=	0x7078
                           007079   722 _FRCOSCPER1	=	0x7079
                           007078   723 _FRCOSCPER	=	0x7078
                           007074   724 _FRCOSCREF0	=	0x7074
                           007075   725 _FRCOSCREF1	=	0x7075
                           007074   726 _FRCOSCREF	=	0x7074
                           007007   727 _ANALOGA	=	0x7007
                           00700C   728 _GPIOENABLE	=	0x700c
                           007003   729 _EXTIRQ	=	0x7003
                           007000   730 _INTCHGA	=	0x7000
                           007001   731 _INTCHGB	=	0x7001
                           007002   732 _INTCHGC	=	0x7002
                           007008   733 _PALTA	=	0x7008
                           007009   734 _PALTB	=	0x7009
                           00700A   735 _PALTC	=	0x700a
                           007046   736 _PALTRADIO	=	0x7046
                           007004   737 _PINCHGA	=	0x7004
                           007005   738 _PINCHGB	=	0x7005
                           007006   739 _PINCHGC	=	0x7006
                           00700B   740 _PINSEL	=	0x700b
                           007060   741 _LPOSCCONFIG	=	0x7060
                           007066   742 _LPOSCFREQ0	=	0x7066
                           007067   743 _LPOSCFREQ1	=	0x7067
                           007066   744 _LPOSCFREQ	=	0x7066
                           007062   745 _LPOSCKFILT0	=	0x7062
                           007063   746 _LPOSCKFILT1	=	0x7063
                           007062   747 _LPOSCKFILT	=	0x7062
                           007068   748 _LPOSCPER0	=	0x7068
                           007069   749 _LPOSCPER1	=	0x7069
                           007068   750 _LPOSCPER	=	0x7068
                           007064   751 _LPOSCREF0	=	0x7064
                           007065   752 _LPOSCREF1	=	0x7065
                           007064   753 _LPOSCREF	=	0x7064
                           007054   754 _LPXOSCGM	=	0x7054
                           007F01   755 _MISCCTRL	=	0x7f01
                           007053   756 _OSCCALIB	=	0x7053
                           007050   757 _OSCFORCERUN	=	0x7050
                           007052   758 _OSCREADY	=	0x7052
                           007051   759 _OSCRUN	=	0x7051
                           007040   760 _RADIOFDATAADDR0	=	0x7040
                           007041   761 _RADIOFDATAADDR1	=	0x7041
                           007040   762 _RADIOFDATAADDR	=	0x7040
                           007042   763 _RADIOFSTATADDR0	=	0x7042
                           007043   764 _RADIOFSTATADDR1	=	0x7043
                           007042   765 _RADIOFSTATADDR	=	0x7042
                           007044   766 _RADIOMUX	=	0x7044
                           007084   767 _SCRATCH0	=	0x7084
                           007085   768 _SCRATCH1	=	0x7085
                           007086   769 _SCRATCH2	=	0x7086
                           007087   770 _SCRATCH3	=	0x7087
                           007F00   771 _SILICONREV	=	0x7f00
                           007F19   772 _XTALAMPL	=	0x7f19
                           007F18   773 _XTALOSC	=	0x7f18
                           007F1A   774 _XTALREADY	=	0x7f1a
                           00FC06   775 _flash_deviceid	=	0xfc06
                           00FC00   776 _flash_calsector	=	0xfc00
      000087                        777 _delaymstimer:
      000087                        778 	.ds 8
      00008F                        779 _wakeup_desc::
      00008F                        780 	.ds 8
      000097                        781 _delay_tx_time_ms_1_217:
      000097                        782 	.ds 1
                                    783 ;--------------------------------------------------------
                                    784 ; absolute external ram data
                                    785 ;--------------------------------------------------------
                                    786 	.area XABS    (ABS,XDATA)
                                    787 ;--------------------------------------------------------
                                    788 ; external initialized ram data
                                    789 ;--------------------------------------------------------
                                    790 	.area XISEG   (XDATA)
                                    791 	.area HOME    (CODE)
                                    792 	.area GSINIT0 (CODE)
                                    793 	.area GSINIT1 (CODE)
                                    794 	.area GSINIT2 (CODE)
                                    795 	.area GSINIT3 (CODE)
                                    796 	.area GSINIT4 (CODE)
                                    797 	.area GSINIT5 (CODE)
                                    798 	.area GSINIT  (CODE)
                                    799 	.area GSFINAL (CODE)
                                    800 	.area CSEG    (CODE)
                                    801 ;--------------------------------------------------------
                                    802 ; global & static initialisations
                                    803 ;--------------------------------------------------------
                                    804 	.area HOME    (CODE)
                                    805 	.area GSINIT  (CODE)
                                    806 	.area GSFINAL (CODE)
                                    807 	.area GSINIT  (CODE)
                                    808 ;--------------------------------------------------------
                                    809 ; Home
                                    810 ;--------------------------------------------------------
                                    811 	.area HOME    (CODE)
                                    812 	.area HOME    (CODE)
                                    813 ;--------------------------------------------------------
                                    814 ; code
                                    815 ;--------------------------------------------------------
                                    816 	.area CSEG    (CODE)
                                    817 ;------------------------------------------------------------
                                    818 ;Allocation info for local variables in function 'delayms_callback'
                                    819 ;------------------------------------------------------------
                                    820 ;desc                      Allocated with name '_delayms_callback_desc_1_210'
                                    821 ;------------------------------------------------------------
                                    822 ;	atrs\misc.c:61: static void delayms_callback(struct wtimer_desc __xdata *desc)
                                    823 ;	-----------------------------------------
                                    824 ;	 function delayms_callback
                                    825 ;	-----------------------------------------
      001047                        826 _delayms_callback:
                           000007   827 	ar7 = 0x07
                           000006   828 	ar6 = 0x06
                           000005   829 	ar5 = 0x05
                           000004   830 	ar4 = 0x04
                           000003   831 	ar3 = 0x03
                           000002   832 	ar2 = 0x02
                           000001   833 	ar1 = 0x01
                           000000   834 	ar0 = 0x00
                                    835 ;	atrs\misc.c:64: delaymstimer.handler = 0;
      001047 90 00 89         [24]  836 	mov	dptr,#(_delaymstimer + 0x0002)
      00104A E4               [12]  837 	clr	a
      00104B F0               [24]  838 	movx	@dptr,a
      00104C A3               [24]  839 	inc	dptr
      00104D F0               [24]  840 	movx	@dptr,a
      00104E 22               [24]  841 	ret
                                    842 ;------------------------------------------------------------
                                    843 ;Allocation info for local variables in function 'delay_ms'
                                    844 ;------------------------------------------------------------
                                    845 ;ms                        Allocated to registers r6 r7 
                                    846 ;x                         Allocated to stack - _bp +1
                                    847 ;------------------------------------------------------------
                                    848 ;	atrs\misc.c:71: __reentrantb void delay_ms(uint16_t ms) __reentrant
                                    849 ;	-----------------------------------------
                                    850 ;	 function delay_ms
                                    851 ;	-----------------------------------------
      00104F                        852 _delay_ms:
      00104F C0 1F            [24]  853 	push	_bp
      001051 E5 81            [12]  854 	mov	a,sp
      001053 F5 1F            [12]  855 	mov	_bp,a
      001055 24 04            [12]  856 	add	a,#0x04
      001057 F5 81            [12]  857 	mov	sp,a
      001059 AE 82            [24]  858 	mov	r6,dpl
      00105B AF 83            [24]  859 	mov	r7,dph
                                    860 ;	atrs\misc.c:75: wtimer_remove(&delaymstimer);
      00105D 90 00 87         [24]  861 	mov	dptr,#_delaymstimer
      001060 C0 07            [24]  862 	push	ar7
      001062 C0 06            [24]  863 	push	ar6
      001064 12 76 94         [24]  864 	lcall	_wtimer_remove
      001067 D0 06            [24]  865 	pop	ar6
      001069 D0 07            [24]  866 	pop	ar7
                                    867 ;	atrs\misc.c:76: x = ms;
      00106B A8 1F            [24]  868 	mov	r0,_bp
      00106D 08               [12]  869 	inc	r0
      00106E A6 06            [24]  870 	mov	@r0,ar6
      001070 08               [12]  871 	inc	r0
      001071 A6 07            [24]  872 	mov	@r0,ar7
      001073 08               [12]  873 	inc	r0
      001074 76 00            [12]  874 	mov	@r0,#0x00
      001076 08               [12]  875 	inc	r0
      001077 76 00            [12]  876 	mov	@r0,#0x00
                                    877 ;	atrs\misc.c:77: delaymstimer.time = ms >> 1;
      001079 EF               [12]  878 	mov	a,r7
      00107A C3               [12]  879 	clr	c
      00107B 13               [12]  880 	rrc	a
      00107C CE               [12]  881 	xch	a,r6
      00107D 13               [12]  882 	rrc	a
      00107E CE               [12]  883 	xch	a,r6
      00107F FF               [12]  884 	mov	r7,a
      001080 8E 04            [24]  885 	mov	ar4,r6
      001082 8F 05            [24]  886 	mov	ar5,r7
      001084 7E 00            [12]  887 	mov	r6,#0x00
      001086 7F 00            [12]  888 	mov	r7,#0x00
      001088 90 00 8B         [24]  889 	mov	dptr,#(_delaymstimer + 0x0004)
      00108B EC               [12]  890 	mov	a,r4
      00108C F0               [24]  891 	movx	@dptr,a
      00108D ED               [12]  892 	mov	a,r5
      00108E A3               [24]  893 	inc	dptr
      00108F F0               [24]  894 	movx	@dptr,a
      001090 EE               [12]  895 	mov	a,r6
      001091 A3               [24]  896 	inc	dptr
      001092 F0               [24]  897 	movx	@dptr,a
      001093 EF               [12]  898 	mov	a,r7
      001094 A3               [24]  899 	inc	dptr
      001095 F0               [24]  900 	movx	@dptr,a
                                    901 ;	atrs\misc.c:78: x <<= 3;
      001096 A8 1F            [24]  902 	mov	r0,_bp
      001098 08               [12]  903 	inc	r0
      001099 08               [12]  904 	inc	r0
      00109A 08               [12]  905 	inc	r0
      00109B 08               [12]  906 	inc	r0
      00109C E6               [12]  907 	mov	a,@r0
      00109D 18               [12]  908 	dec	r0
      00109E C4               [12]  909 	swap	a
      00109F 03               [12]  910 	rr	a
      0010A0 54 F8            [12]  911 	anl	a,#0xf8
      0010A2 C6               [12]  912 	xch	a,@r0
      0010A3 C4               [12]  913 	swap	a
      0010A4 03               [12]  914 	rr	a
      0010A5 C6               [12]  915 	xch	a,@r0
      0010A6 66               [12]  916 	xrl	a,@r0
      0010A7 C6               [12]  917 	xch	a,@r0
      0010A8 54 F8            [12]  918 	anl	a,#0xf8
      0010AA C6               [12]  919 	xch	a,@r0
      0010AB 66               [12]  920 	xrl	a,@r0
      0010AC 08               [12]  921 	inc	r0
      0010AD F6               [12]  922 	mov	@r0,a
      0010AE 18               [12]  923 	dec	r0
      0010AF 18               [12]  924 	dec	r0
      0010B0 E6               [12]  925 	mov	a,@r0
      0010B1 C4               [12]  926 	swap	a
      0010B2 03               [12]  927 	rr	a
      0010B3 54 07            [12]  928 	anl	a,#0x07
      0010B5 08               [12]  929 	inc	r0
      0010B6 46               [12]  930 	orl	a,@r0
      0010B7 F6               [12]  931 	mov	@r0,a
      0010B8 18               [12]  932 	dec	r0
      0010B9 E6               [12]  933 	mov	a,@r0
      0010BA 18               [12]  934 	dec	r0
      0010BB C4               [12]  935 	swap	a
      0010BC 03               [12]  936 	rr	a
      0010BD 54 F8            [12]  937 	anl	a,#0xf8
      0010BF C6               [12]  938 	xch	a,@r0
      0010C0 C4               [12]  939 	swap	a
      0010C1 03               [12]  940 	rr	a
      0010C2 C6               [12]  941 	xch	a,@r0
      0010C3 66               [12]  942 	xrl	a,@r0
      0010C4 C6               [12]  943 	xch	a,@r0
      0010C5 54 F8            [12]  944 	anl	a,#0xf8
      0010C7 C6               [12]  945 	xch	a,@r0
      0010C8 66               [12]  946 	xrl	a,@r0
      0010C9 08               [12]  947 	inc	r0
      0010CA F6               [12]  948 	mov	@r0,a
                                    949 ;	atrs\misc.c:79: delaymstimer.time -= x;
      0010CB A8 1F            [24]  950 	mov	r0,_bp
      0010CD 08               [12]  951 	inc	r0
      0010CE EC               [12]  952 	mov	a,r4
      0010CF C3               [12]  953 	clr	c
      0010D0 96               [12]  954 	subb	a,@r0
      0010D1 FC               [12]  955 	mov	r4,a
      0010D2 ED               [12]  956 	mov	a,r5
      0010D3 08               [12]  957 	inc	r0
      0010D4 96               [12]  958 	subb	a,@r0
      0010D5 FD               [12]  959 	mov	r5,a
      0010D6 EE               [12]  960 	mov	a,r6
      0010D7 08               [12]  961 	inc	r0
      0010D8 96               [12]  962 	subb	a,@r0
      0010D9 FE               [12]  963 	mov	r6,a
      0010DA EF               [12]  964 	mov	a,r7
      0010DB 08               [12]  965 	inc	r0
      0010DC 96               [12]  966 	subb	a,@r0
      0010DD FF               [12]  967 	mov	r7,a
      0010DE 90 00 8B         [24]  968 	mov	dptr,#(_delaymstimer + 0x0004)
      0010E1 EC               [12]  969 	mov	a,r4
      0010E2 F0               [24]  970 	movx	@dptr,a
      0010E3 ED               [12]  971 	mov	a,r5
      0010E4 A3               [24]  972 	inc	dptr
      0010E5 F0               [24]  973 	movx	@dptr,a
      0010E6 EE               [12]  974 	mov	a,r6
      0010E7 A3               [24]  975 	inc	dptr
      0010E8 F0               [24]  976 	movx	@dptr,a
      0010E9 EF               [12]  977 	mov	a,r7
      0010EA A3               [24]  978 	inc	dptr
      0010EB F0               [24]  979 	movx	@dptr,a
                                    980 ;	atrs\misc.c:80: x <<= 3;
      0010EC A8 1F            [24]  981 	mov	r0,_bp
      0010EE 08               [12]  982 	inc	r0
      0010EF 08               [12]  983 	inc	r0
      0010F0 08               [12]  984 	inc	r0
      0010F1 08               [12]  985 	inc	r0
      0010F2 E6               [12]  986 	mov	a,@r0
      0010F3 18               [12]  987 	dec	r0
      0010F4 C4               [12]  988 	swap	a
      0010F5 03               [12]  989 	rr	a
      0010F6 54 F8            [12]  990 	anl	a,#0xf8
      0010F8 C6               [12]  991 	xch	a,@r0
      0010F9 C4               [12]  992 	swap	a
      0010FA 03               [12]  993 	rr	a
      0010FB C6               [12]  994 	xch	a,@r0
      0010FC 66               [12]  995 	xrl	a,@r0
      0010FD C6               [12]  996 	xch	a,@r0
      0010FE 54 F8            [12]  997 	anl	a,#0xf8
      001100 C6               [12]  998 	xch	a,@r0
      001101 66               [12]  999 	xrl	a,@r0
      001102 08               [12] 1000 	inc	r0
      001103 F6               [12] 1001 	mov	@r0,a
      001104 18               [12] 1002 	dec	r0
      001105 18               [12] 1003 	dec	r0
      001106 E6               [12] 1004 	mov	a,@r0
      001107 C4               [12] 1005 	swap	a
      001108 03               [12] 1006 	rr	a
      001109 54 07            [12] 1007 	anl	a,#0x07
      00110B 08               [12] 1008 	inc	r0
      00110C 46               [12] 1009 	orl	a,@r0
      00110D F6               [12] 1010 	mov	@r0,a
      00110E 18               [12] 1011 	dec	r0
      00110F E6               [12] 1012 	mov	a,@r0
      001110 18               [12] 1013 	dec	r0
      001111 C4               [12] 1014 	swap	a
      001112 03               [12] 1015 	rr	a
      001113 54 F8            [12] 1016 	anl	a,#0xf8
      001115 C6               [12] 1017 	xch	a,@r0
      001116 C4               [12] 1018 	swap	a
      001117 03               [12] 1019 	rr	a
      001118 C6               [12] 1020 	xch	a,@r0
      001119 66               [12] 1021 	xrl	a,@r0
      00111A C6               [12] 1022 	xch	a,@r0
      00111B 54 F8            [12] 1023 	anl	a,#0xf8
      00111D C6               [12] 1024 	xch	a,@r0
      00111E 66               [12] 1025 	xrl	a,@r0
      00111F 08               [12] 1026 	inc	r0
      001120 F6               [12] 1027 	mov	@r0,a
                                   1028 ;	atrs\misc.c:81: delaymstimer.time += x;
      001121 A8 1F            [24] 1029 	mov	r0,_bp
      001123 08               [12] 1030 	inc	r0
      001124 E6               [12] 1031 	mov	a,@r0
      001125 2C               [12] 1032 	add	a,r4
      001126 FC               [12] 1033 	mov	r4,a
      001127 08               [12] 1034 	inc	r0
      001128 E6               [12] 1035 	mov	a,@r0
      001129 3D               [12] 1036 	addc	a,r5
      00112A FD               [12] 1037 	mov	r5,a
      00112B 08               [12] 1038 	inc	r0
      00112C E6               [12] 1039 	mov	a,@r0
      00112D 3E               [12] 1040 	addc	a,r6
      00112E FE               [12] 1041 	mov	r6,a
      00112F 08               [12] 1042 	inc	r0
      001130 E6               [12] 1043 	mov	a,@r0
      001131 3F               [12] 1044 	addc	a,r7
      001132 FF               [12] 1045 	mov	r7,a
      001133 90 00 8B         [24] 1046 	mov	dptr,#(_delaymstimer + 0x0004)
      001136 EC               [12] 1047 	mov	a,r4
      001137 F0               [24] 1048 	movx	@dptr,a
      001138 ED               [12] 1049 	mov	a,r5
      001139 A3               [24] 1050 	inc	dptr
      00113A F0               [24] 1051 	movx	@dptr,a
      00113B EE               [12] 1052 	mov	a,r6
      00113C A3               [24] 1053 	inc	dptr
      00113D F0               [24] 1054 	movx	@dptr,a
      00113E EF               [12] 1055 	mov	a,r7
      00113F A3               [24] 1056 	inc	dptr
      001140 F0               [24] 1057 	movx	@dptr,a
                                   1058 ;	atrs\misc.c:82: x <<= 2;
      001141 A8 1F            [24] 1059 	mov	r0,_bp
      001143 08               [12] 1060 	inc	r0
      001144 E6               [12] 1061 	mov	a,@r0
      001145 25 E0            [12] 1062 	add	a,acc
      001147 F6               [12] 1063 	mov	@r0,a
      001148 08               [12] 1064 	inc	r0
      001149 E6               [12] 1065 	mov	a,@r0
      00114A 33               [12] 1066 	rlc	a
      00114B F6               [12] 1067 	mov	@r0,a
      00114C 08               [12] 1068 	inc	r0
      00114D E6               [12] 1069 	mov	a,@r0
      00114E 33               [12] 1070 	rlc	a
      00114F F6               [12] 1071 	mov	@r0,a
      001150 08               [12] 1072 	inc	r0
      001151 E6               [12] 1073 	mov	a,@r0
      001152 33               [12] 1074 	rlc	a
      001153 F6               [12] 1075 	mov	@r0,a
      001154 18               [12] 1076 	dec	r0
      001155 18               [12] 1077 	dec	r0
      001156 18               [12] 1078 	dec	r0
      001157 E6               [12] 1079 	mov	a,@r0
      001158 25 E0            [12] 1080 	add	a,acc
      00115A F6               [12] 1081 	mov	@r0,a
      00115B 08               [12] 1082 	inc	r0
      00115C E6               [12] 1083 	mov	a,@r0
      00115D 33               [12] 1084 	rlc	a
      00115E F6               [12] 1085 	mov	@r0,a
      00115F 08               [12] 1086 	inc	r0
      001160 E6               [12] 1087 	mov	a,@r0
      001161 33               [12] 1088 	rlc	a
      001162 F6               [12] 1089 	mov	@r0,a
      001163 08               [12] 1090 	inc	r0
      001164 E6               [12] 1091 	mov	a,@r0
      001165 33               [12] 1092 	rlc	a
      001166 F6               [12] 1093 	mov	@r0,a
                                   1094 ;	atrs\misc.c:83: delaymstimer.time += x;
      001167 A8 1F            [24] 1095 	mov	r0,_bp
      001169 08               [12] 1096 	inc	r0
      00116A E6               [12] 1097 	mov	a,@r0
      00116B 2C               [12] 1098 	add	a,r4
      00116C FC               [12] 1099 	mov	r4,a
      00116D 08               [12] 1100 	inc	r0
      00116E E6               [12] 1101 	mov	a,@r0
      00116F 3D               [12] 1102 	addc	a,r5
      001170 FD               [12] 1103 	mov	r5,a
      001171 08               [12] 1104 	inc	r0
      001172 E6               [12] 1105 	mov	a,@r0
      001173 3E               [12] 1106 	addc	a,r6
      001174 FE               [12] 1107 	mov	r6,a
      001175 08               [12] 1108 	inc	r0
      001176 E6               [12] 1109 	mov	a,@r0
      001177 3F               [12] 1110 	addc	a,r7
      001178 FF               [12] 1111 	mov	r7,a
      001179 90 00 8B         [24] 1112 	mov	dptr,#(_delaymstimer + 0x0004)
      00117C EC               [12] 1113 	mov	a,r4
      00117D F0               [24] 1114 	movx	@dptr,a
      00117E ED               [12] 1115 	mov	a,r5
      00117F A3               [24] 1116 	inc	dptr
      001180 F0               [24] 1117 	movx	@dptr,a
      001181 EE               [12] 1118 	mov	a,r6
      001182 A3               [24] 1119 	inc	dptr
      001183 F0               [24] 1120 	movx	@dptr,a
      001184 EF               [12] 1121 	mov	a,r7
      001185 A3               [24] 1122 	inc	dptr
      001186 F0               [24] 1123 	movx	@dptr,a
                                   1124 ;	atrs\misc.c:84: delaymstimer.handler = delayms_callback;
      001187 90 00 89         [24] 1125 	mov	dptr,#(_delaymstimer + 0x0002)
      00118A 74 47            [12] 1126 	mov	a,#_delayms_callback
      00118C F0               [24] 1127 	movx	@dptr,a
      00118D 74 10            [12] 1128 	mov	a,#(_delayms_callback >> 8)
      00118F A3               [24] 1129 	inc	dptr
      001190 F0               [24] 1130 	movx	@dptr,a
                                   1131 ;	atrs\misc.c:85: wtimer1_addrelative(&delaymstimer);
      001191 90 00 87         [24] 1132 	mov	dptr,#_delaymstimer
      001194 12 6D 24         [24] 1133 	lcall	_wtimer1_addrelative
                                   1134 ;	atrs\misc.c:86: wtimer_runcallbacks();
      001197 12 6B 4A         [24] 1135 	lcall	_wtimer_runcallbacks
                                   1136 ;	atrs\misc.c:87: do {
      00119A                       1137 00101$:
                                   1138 ;	atrs\misc.c:88: wtimer_idle(WTFLAG_CANSTANDBY);
      00119A 75 82 02         [24] 1139 	mov	dpl,#0x02
      00119D 12 6B CB         [24] 1140 	lcall	_wtimer_idle
                                   1141 ;	atrs\misc.c:89: wtimer_runcallbacks();
      0011A0 12 6B 4A         [24] 1142 	lcall	_wtimer_runcallbacks
                                   1143 ;	atrs\misc.c:90: } while (delaymstimer.handler);
      0011A3 90 00 89         [24] 1144 	mov	dptr,#(_delaymstimer + 0x0002)
      0011A6 E0               [24] 1145 	movx	a,@dptr
      0011A7 FE               [12] 1146 	mov	r6,a
      0011A8 A3               [24] 1147 	inc	dptr
      0011A9 E0               [24] 1148 	movx	a,@dptr
      0011AA FF               [12] 1149 	mov	r7,a
      0011AB 4E               [12] 1150 	orl	a,r6
      0011AC 70 EC            [24] 1151 	jnz	00101$
      0011AE 85 1F 81         [24] 1152 	mov	sp,_bp
      0011B1 D0 1F            [24] 1153 	pop	_bp
      0011B3 22               [24] 1154 	ret
                                   1155 ;------------------------------------------------------------
                                   1156 ;Allocation info for local variables in function 'wakeup_callback'
                                   1157 ;------------------------------------------------------------
                                   1158 ;desc                      Allocated with name '_wakeup_callback_desc_1_215'
                                   1159 ;------------------------------------------------------------
                                   1160 ;	atrs\misc.c:94: static void wakeup_callback(struct wtimer_desc __xdata *desc)
                                   1161 ;	-----------------------------------------
                                   1162 ;	 function wakeup_callback
                                   1163 ;	-----------------------------------------
      0011B4                       1164 _wakeup_callback:
                                   1165 ;	atrs\misc.c:100: dbglink_writestr("timer");
      0011B4 90 7F C3         [24] 1166 	mov	dptr,#___str_0
      0011B7 75 F0 80         [24] 1167 	mov	b,#0x80
      0011BA 02 74 0A         [24] 1168 	ljmp	_dbglink_writestr
                                   1169 ;------------------------------------------------------------
                                   1170 ;Allocation info for local variables in function 'delay_tx'
                                   1171 ;------------------------------------------------------------
                                   1172 ;time_ms                   Allocated with name '_delay_tx_time_ms_1_217'
                                   1173 ;------------------------------------------------------------
                                   1174 ;	atrs\misc.c:118: void delay_tx(uint8_t time_ms)
                                   1175 ;	-----------------------------------------
                                   1176 ;	 function delay_tx
                                   1177 ;	-----------------------------------------
      0011BD                       1178 _delay_tx:
      0011BD E5 82            [12] 1179 	mov	a,dpl
      0011BF 90 00 97         [24] 1180 	mov	dptr,#_delay_tx_time_ms_1_217
      0011C2 F0               [24] 1181 	movx	@dptr,a
                                   1182 ;	atrs\misc.c:120: wakeup_desc.handler = wakeup_callback;//Handler for timer 0
      0011C3 90 00 91         [24] 1183 	mov	dptr,#(_wakeup_desc + 0x0002)
      0011C6 74 B4            [12] 1184 	mov	a,#_wakeup_callback
      0011C8 F0               [24] 1185 	movx	@dptr,a
      0011C9 74 11            [12] 1186 	mov	a,#(_wakeup_callback >> 8)
      0011CB A3               [24] 1187 	inc	dptr
      0011CC F0               [24] 1188 	movx	@dptr,a
                                   1189 ;	atrs\misc.c:121: time_ms = TIME_FOR_TIMER_MSEC(time_ms);
      0011CD 90 00 97         [24] 1190 	mov	dptr,#_delay_tx_time_ms_1_217
      0011D0 E0               [24] 1191 	movx	a,@dptr
      0011D1 F5 82            [12] 1192 	mov	dpl,a
      0011D3 12 7D 8B         [24] 1193 	lcall	___uchar2fs
      0011D6 AC 82            [24] 1194 	mov	r4,dpl
      0011D8 AD 83            [24] 1195 	mov	r5,dph
      0011DA AE F0            [24] 1196 	mov	r6,b
      0011DC FF               [12] 1197 	mov	r7,a
      0011DD C0 04            [24] 1198 	push	ar4
      0011DF C0 05            [24] 1199 	push	ar5
      0011E1 C0 06            [24] 1200 	push	ar6
      0011E3 C0 07            [24] 1201 	push	ar7
      0011E5 90 00 00         [24] 1202 	mov	dptr,#0x0000
      0011E8 75 F0 20         [24] 1203 	mov	b,#0x20
      0011EB 74 44            [12] 1204 	mov	a,#0x44
      0011ED 12 63 57         [24] 1205 	lcall	___fsmul
      0011F0 AC 82            [24] 1206 	mov	r4,dpl
      0011F2 AD 83            [24] 1207 	mov	r5,dph
      0011F4 AE F0            [24] 1208 	mov	r6,b
      0011F6 FF               [12] 1209 	mov	r7,a
      0011F7 E5 81            [12] 1210 	mov	a,sp
      0011F9 24 FC            [12] 1211 	add	a,#0xfc
      0011FB F5 81            [12] 1212 	mov	sp,a
      0011FD E4               [12] 1213 	clr	a
      0011FE C0 E0            [24] 1214 	push	acc
      001200 C0 E0            [24] 1215 	push	acc
      001202 74 7A            [12] 1216 	mov	a,#0x7a
      001204 C0 E0            [24] 1217 	push	acc
      001206 74 44            [12] 1218 	mov	a,#0x44
      001208 C0 E0            [24] 1219 	push	acc
      00120A 8C 82            [24] 1220 	mov	dpl,r4
      00120C 8D 83            [24] 1221 	mov	dph,r5
      00120E 8E F0            [24] 1222 	mov	b,r6
      001210 EF               [12] 1223 	mov	a,r7
      001211 12 7B 4F         [24] 1224 	lcall	___fsdiv
      001214 AC 82            [24] 1225 	mov	r4,dpl
      001216 AD 83            [24] 1226 	mov	r5,dph
      001218 AE F0            [24] 1227 	mov	r6,b
      00121A FF               [12] 1228 	mov	r7,a
      00121B E5 81            [12] 1229 	mov	a,sp
      00121D 24 FC            [12] 1230 	add	a,#0xfc
      00121F F5 81            [12] 1231 	mov	sp,a
      001221 8C 82            [24] 1232 	mov	dpl,r4
      001223 8D 83            [24] 1233 	mov	dph,r5
      001225 8E F0            [24] 1234 	mov	b,r6
      001227 EF               [12] 1235 	mov	a,r7
      001228 12 7D 96         [24] 1236 	lcall	___fs2uchar
      00122B E5 82            [12] 1237 	mov	a,dpl
      00122D 90 00 97         [24] 1238 	mov	dptr,#_delay_tx_time_ms_1_217
      001230 F0               [24] 1239 	movx	@dptr,a
                                   1240 ;	atrs\misc.c:122: wakeup_desc.time += time_ms; //+= ? o solo = ? verificar esto
      001231 90 00 93         [24] 1241 	mov	dptr,#(_wakeup_desc + 0x0004)
      001234 E0               [24] 1242 	movx	a,@dptr
      001235 FC               [12] 1243 	mov	r4,a
      001236 A3               [24] 1244 	inc	dptr
      001237 E0               [24] 1245 	movx	a,@dptr
      001238 FD               [12] 1246 	mov	r5,a
      001239 A3               [24] 1247 	inc	dptr
      00123A E0               [24] 1248 	movx	a,@dptr
      00123B FE               [12] 1249 	mov	r6,a
      00123C A3               [24] 1250 	inc	dptr
      00123D E0               [24] 1251 	movx	a,@dptr
      00123E FF               [12] 1252 	mov	r7,a
      00123F 90 00 97         [24] 1253 	mov	dptr,#_delay_tx_time_ms_1_217
      001242 E0               [24] 1254 	movx	a,@dptr
      001243 F8               [12] 1255 	mov	r0,a
      001244 E4               [12] 1256 	clr	a
      001245 F9               [12] 1257 	mov	r1,a
      001246 FA               [12] 1258 	mov	r2,a
      001247 FB               [12] 1259 	mov	r3,a
      001248 E8               [12] 1260 	mov	a,r0
      001249 2C               [12] 1261 	add	a,r4
      00124A FC               [12] 1262 	mov	r4,a
      00124B E9               [12] 1263 	mov	a,r1
      00124C 3D               [12] 1264 	addc	a,r5
      00124D FD               [12] 1265 	mov	r5,a
      00124E EA               [12] 1266 	mov	a,r2
      00124F 3E               [12] 1267 	addc	a,r6
      001250 FE               [12] 1268 	mov	r6,a
      001251 EB               [12] 1269 	mov	a,r3
      001252 3F               [12] 1270 	addc	a,r7
      001253 FF               [12] 1271 	mov	r7,a
      001254 90 00 93         [24] 1272 	mov	dptr,#(_wakeup_desc + 0x0004)
      001257 EC               [12] 1273 	mov	a,r4
      001258 F0               [24] 1274 	movx	@dptr,a
      001259 ED               [12] 1275 	mov	a,r5
      00125A A3               [24] 1276 	inc	dptr
      00125B F0               [24] 1277 	movx	@dptr,a
      00125C EE               [12] 1278 	mov	a,r6
      00125D A3               [24] 1279 	inc	dptr
      00125E F0               [24] 1280 	movx	@dptr,a
      00125F EF               [12] 1281 	mov	a,r7
      001260 A3               [24] 1282 	inc	dptr
      001261 F0               [24] 1283 	movx	@dptr,a
                                   1284 ;	atrs\misc.c:123: wtimer0_addabsolute(&wakeup_desc);
      001262 90 00 8F         [24] 1285 	mov	dptr,#_wakeup_desc
      001265 02 6D B9         [24] 1286 	ljmp	_wtimer0_addabsolute
                                   1287 	.area CSEG    (CODE)
                                   1288 	.area CONST   (CODE)
      007FC3                       1289 ___str_0:
      007FC3 74 69 6D 65 72        1290 	.ascii "timer"
      007FC8 00                    1291 	.db 0x00
                                   1292 	.area XINIT   (CODE)
                                   1293 	.area CABS    (ABS,CODE)
