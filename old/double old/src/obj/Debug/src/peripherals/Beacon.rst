                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.6.0 #9615 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module Beacon
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _memcpy
                                     12 	.globl _BEACON_decoding_PARM_3
                                     13 	.globl _BEACON_decoding_PARM_2
                                     14 	.globl _BEACON_decoding
                                     15 ;--------------------------------------------------------
                                     16 ; special function registers
                                     17 ;--------------------------------------------------------
                                     18 	.area RSEG    (ABS,DATA)
      000000                         19 	.org 0x0000
                                     20 ;--------------------------------------------------------
                                     21 ; special function bits
                                     22 ;--------------------------------------------------------
                                     23 	.area RSEG    (ABS,DATA)
      000000                         24 	.org 0x0000
                                     25 ;--------------------------------------------------------
                                     26 ; overlayable register banks
                                     27 ;--------------------------------------------------------
                                     28 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                         29 	.ds 8
                                     30 ;--------------------------------------------------------
                                     31 ; internal ram data
                                     32 ;--------------------------------------------------------
                                     33 	.area DSEG    (DATA)
      00000D                         34 _BEACON_decoding_sloc0_1_0:
      00000D                         35 	.ds 2
      00000F                         36 _BEACON_decoding_sloc1_1_0:
      00000F                         37 	.ds 2
      000011                         38 _BEACON_decoding_sloc2_1_0:
      000011                         39 	.ds 2
      000013                         40 _BEACON_decoding_sloc3_1_0:
      000013                         41 	.ds 3
      000016                         42 _BEACON_decoding_sloc4_1_0:
      000016                         43 	.ds 3
      000019                         44 _BEACON_decoding_sloc5_1_0:
      000019                         45 	.ds 4
                                     46 ;--------------------------------------------------------
                                     47 ; overlayable items in internal ram 
                                     48 ;--------------------------------------------------------
                                     49 ;--------------------------------------------------------
                                     50 ; indirectly addressable internal ram data
                                     51 ;--------------------------------------------------------
                                     52 	.area ISEG    (DATA)
                                     53 ;--------------------------------------------------------
                                     54 ; absolute internal ram data
                                     55 ;--------------------------------------------------------
                                     56 	.area IABS    (ABS,DATA)
                                     57 	.area IABS    (ABS,DATA)
                                     58 ;--------------------------------------------------------
                                     59 ; bit data
                                     60 ;--------------------------------------------------------
                                     61 	.area BSEG    (BIT)
                                     62 ;--------------------------------------------------------
                                     63 ; paged external ram data
                                     64 ;--------------------------------------------------------
                                     65 	.area PSEG    (PAG,XDATA)
                                     66 ;--------------------------------------------------------
                                     67 ; external ram data
                                     68 ;--------------------------------------------------------
                                     69 	.area XSEG    (XDATA)
      000372                         70 _BEACON_decoding_PARM_2:
      000372                         71 	.ds 2
      000374                         72 _BEACON_decoding_PARM_3:
      000374                         73 	.ds 1
      000375                         74 _BEACON_decoding_Rxbuffer_1_79:
      000375                         75 	.ds 3
                                     76 ;--------------------------------------------------------
                                     77 ; absolute external ram data
                                     78 ;--------------------------------------------------------
                                     79 	.area XABS    (ABS,XDATA)
                                     80 ;--------------------------------------------------------
                                     81 ; external initialized ram data
                                     82 ;--------------------------------------------------------
                                     83 	.area XISEG   (XDATA)
                                     84 	.area HOME    (CODE)
                                     85 	.area GSINIT0 (CODE)
                                     86 	.area GSINIT1 (CODE)
                                     87 	.area GSINIT2 (CODE)
                                     88 	.area GSINIT3 (CODE)
                                     89 	.area GSINIT4 (CODE)
                                     90 	.area GSINIT5 (CODE)
                                     91 	.area GSINIT  (CODE)
                                     92 	.area GSFINAL (CODE)
                                     93 	.area CSEG    (CODE)
                                     94 ;--------------------------------------------------------
                                     95 ; global & static initialisations
                                     96 ;--------------------------------------------------------
                                     97 	.area HOME    (CODE)
                                     98 	.area GSINIT  (CODE)
                                     99 	.area GSFINAL (CODE)
                                    100 	.area GSINIT  (CODE)
                                    101 ;--------------------------------------------------------
                                    102 ; Home
                                    103 ;--------------------------------------------------------
                                    104 	.area HOME    (CODE)
                                    105 	.area HOME    (CODE)
                                    106 ;--------------------------------------------------------
                                    107 ; code
                                    108 ;--------------------------------------------------------
                                    109 	.area CSEG    (CODE)
                                    110 ;------------------------------------------------------------
                                    111 ;Allocation info for local variables in function 'BEACON_decoding'
                                    112 ;------------------------------------------------------------
                                    113 ;sloc0                     Allocated with name '_BEACON_decoding_sloc0_1_0'
                                    114 ;sloc1                     Allocated with name '_BEACON_decoding_sloc1_1_0'
                                    115 ;sloc2                     Allocated with name '_BEACON_decoding_sloc2_1_0'
                                    116 ;sloc3                     Allocated with name '_BEACON_decoding_sloc3_1_0'
                                    117 ;sloc4                     Allocated with name '_BEACON_decoding_sloc4_1_0'
                                    118 ;sloc5                     Allocated with name '_BEACON_decoding_sloc5_1_0'
                                    119 ;beacon                    Allocated with name '_BEACON_decoding_PARM_2'
                                    120 ;length                    Allocated with name '_BEACON_decoding_PARM_3'
                                    121 ;Rxbuffer                  Allocated with name '_BEACON_decoding_Rxbuffer_1_79'
                                    122 ;i                         Allocated with name '_BEACON_decoding_i_1_80'
                                    123 ;------------------------------------------------------------
                                    124 ;	peripherals\Beacon.c:29: __xdata void BEACON_decoding(uint8_t* Rxbuffer, RF_beacon_t* beacon, uint8_t length)
                                    125 ;	-----------------------------------------
                                    126 ;	 function BEACON_decoding
                                    127 ;	-----------------------------------------
      004C78                        128 _BEACON_decoding:
                           000007   129 	ar7 = 0x07
                           000006   130 	ar6 = 0x06
                           000005   131 	ar5 = 0x05
                           000004   132 	ar4 = 0x04
                           000003   133 	ar3 = 0x03
                           000002   134 	ar2 = 0x02
                           000001   135 	ar1 = 0x01
                           000000   136 	ar0 = 0x00
      004C78 AF F0            [24]  137 	mov	r7,b
      004C7A AE 83            [24]  138 	mov	r6,dph
      004C7C E5 82            [12]  139 	mov	a,dpl
      004C7E 90 03 75         [24]  140 	mov	dptr,#_BEACON_decoding_Rxbuffer_1_79
      004C81 F0               [24]  141 	movx	@dptr,a
      004C82 EE               [12]  142 	mov	a,r6
      004C83 A3               [24]  143 	inc	dptr
      004C84 F0               [24]  144 	movx	@dptr,a
      004C85 EF               [12]  145 	mov	a,r7
      004C86 A3               [24]  146 	inc	dptr
      004C87 F0               [24]  147 	movx	@dptr,a
                                    148 ;	peripherals\Beacon.c:32: beacon->FlagReceived = VALID_BEACON;
      004C88 90 03 72         [24]  149 	mov	dptr,#_BEACON_decoding_PARM_2
      004C8B E0               [24]  150 	movx	a,@dptr
      004C8C F5 11            [12]  151 	mov	_BEACON_decoding_sloc2_1_0,a
      004C8E A3               [24]  152 	inc	dptr
      004C8F E0               [24]  153 	movx	a,@dptr
      004C90 F5 12            [12]  154 	mov	(_BEACON_decoding_sloc2_1_0 + 1),a
      004C92 85 11 82         [24]  155 	mov	dpl,_BEACON_decoding_sloc2_1_0
      004C95 85 12 83         [24]  156 	mov	dph,(_BEACON_decoding_sloc2_1_0 + 1)
      004C98 74 01            [12]  157 	mov	a,#0x01
      004C9A F0               [24]  158 	movx	@dptr,a
                                    159 ;	peripherals\Beacon.c:33: if(length == 23)
      004C9B 90 03 74         [24]  160 	mov	dptr,#_BEACON_decoding_PARM_3
      004C9E E0               [24]  161 	movx	a,@dptr
      004C9F FD               [12]  162 	mov	r5,a
      004CA0 BD 17 02         [24]  163 	cjne	r5,#0x17,00118$
      004CA3 80 03            [24]  164 	sjmp	00119$
      004CA5                        165 00118$:
      004CA5 02 51 62         [24]  166 	ljmp	00108$
      004CA8                        167 00119$:
                                    168 ;	peripherals\Beacon.c:35: beacon->SatId = *Rxbuffer <<8 | *(Rxbuffer+1);
      004CA8 74 01            [12]  169 	mov	a,#0x01
      004CAA 25 11            [12]  170 	add	a,_BEACON_decoding_sloc2_1_0
      004CAC F5 0D            [12]  171 	mov	_BEACON_decoding_sloc0_1_0,a
      004CAE E4               [12]  172 	clr	a
      004CAF 35 12            [12]  173 	addc	a,(_BEACON_decoding_sloc2_1_0 + 1)
      004CB1 F5 0E            [12]  174 	mov	(_BEACON_decoding_sloc0_1_0 + 1),a
      004CB3 90 03 75         [24]  175 	mov	dptr,#_BEACON_decoding_Rxbuffer_1_79
      004CB6 E0               [24]  176 	movx	a,@dptr
      004CB7 F9               [12]  177 	mov	r1,a
      004CB8 A3               [24]  178 	inc	dptr
      004CB9 E0               [24]  179 	movx	a,@dptr
      004CBA FA               [12]  180 	mov	r2,a
      004CBB A3               [24]  181 	inc	dptr
      004CBC E0               [24]  182 	movx	a,@dptr
      004CBD FB               [12]  183 	mov	r3,a
      004CBE 89 82            [24]  184 	mov	dpl,r1
      004CC0 8A 83            [24]  185 	mov	dph,r2
      004CC2 8B F0            [24]  186 	mov	b,r3
      004CC4 12 7D 6F         [24]  187 	lcall	__gptrget
      004CC7 F8               [12]  188 	mov	r0,a
      004CC8 7D 00            [12]  189 	mov	r5,#0x00
      004CCA 88 10            [24]  190 	mov	(_BEACON_decoding_sloc1_1_0 + 1),r0
                                    191 ;	1-genFromRTrack replaced	mov	_BEACON_decoding_sloc1_1_0,#0x00
      004CCC 8D 0F            [24]  192 	mov	_BEACON_decoding_sloc1_1_0,r5
      004CCE 74 01            [12]  193 	mov	a,#0x01
      004CD0 29               [12]  194 	add	a,r1
      004CD1 F8               [12]  195 	mov	r0,a
      004CD2 E4               [12]  196 	clr	a
      004CD3 3A               [12]  197 	addc	a,r2
      004CD4 FC               [12]  198 	mov	r4,a
      004CD5 8B 05            [24]  199 	mov	ar5,r3
      004CD7 88 82            [24]  200 	mov	dpl,r0
      004CD9 8C 83            [24]  201 	mov	dph,r4
      004CDB 8D F0            [24]  202 	mov	b,r5
      004CDD 12 7D 6F         [24]  203 	lcall	__gptrget
      004CE0 F8               [12]  204 	mov	r0,a
      004CE1 7D 00            [12]  205 	mov	r5,#0x00
      004CE3 E5 0F            [12]  206 	mov	a,_BEACON_decoding_sloc1_1_0
      004CE5 42 00            [12]  207 	orl	ar0,a
      004CE7 E5 10            [12]  208 	mov	a,(_BEACON_decoding_sloc1_1_0 + 1)
      004CE9 42 05            [12]  209 	orl	ar5,a
      004CEB 85 0D 82         [24]  210 	mov	dpl,_BEACON_decoding_sloc0_1_0
      004CEE 85 0E 83         [24]  211 	mov	dph,(_BEACON_decoding_sloc0_1_0 + 1)
      004CF1 E8               [12]  212 	mov	a,r0
      004CF2 F0               [24]  213 	movx	@dptr,a
      004CF3 ED               [12]  214 	mov	a,r5
      004CF4 A3               [24]  215 	inc	dptr
      004CF5 F0               [24]  216 	movx	@dptr,a
                                    217 ;	peripherals\Beacon.c:36: beacon->BeaconID = (*(Rxbuffer+POS_BEACONID) & MASK_BEACONID)>> 4;
      004CF6 74 03            [12]  218 	mov	a,#0x03
      004CF8 25 11            [12]  219 	add	a,_BEACON_decoding_sloc2_1_0
      004CFA FC               [12]  220 	mov	r4,a
      004CFB E4               [12]  221 	clr	a
      004CFC 35 12            [12]  222 	addc	a,(_BEACON_decoding_sloc2_1_0 + 1)
      004CFE FD               [12]  223 	mov	r5,a
      004CFF 74 02            [12]  224 	mov	a,#0x02
      004D01 29               [12]  225 	add	a,r1
      004D02 F8               [12]  226 	mov	r0,a
      004D03 E4               [12]  227 	clr	a
      004D04 3A               [12]  228 	addc	a,r2
      004D05 FE               [12]  229 	mov	r6,a
      004D06 8B 07            [24]  230 	mov	ar7,r3
      004D08 88 82            [24]  231 	mov	dpl,r0
      004D0A 8E 83            [24]  232 	mov	dph,r6
      004D0C 8F F0            [24]  233 	mov	b,r7
      004D0E 12 7D 6F         [24]  234 	lcall	__gptrget
      004D11 54 F0            [12]  235 	anl	a,#0xf0
      004D13 C4               [12]  236 	swap	a
      004D14 54 0F            [12]  237 	anl	a,#0x0f
      004D16 F8               [12]  238 	mov	r0,a
      004D17 8C 82            [24]  239 	mov	dpl,r4
      004D19 8D 83            [24]  240 	mov	dph,r5
      004D1B F0               [24]  241 	movx	@dptr,a
                                    242 ;	peripherals\Beacon.c:39: if(beacon->BeaconID == UPLINK_BEACON)
      004D1C B8 0C 35         [24]  243 	cjne	r0,#0x0c,00105$
                                    244 ;	peripherals\Beacon.c:41: beacon->NumPremSlots = *(Rxbuffer+POS_NUMPREM);
      004D1F 74 04            [12]  245 	mov	a,#0x04
      004D21 25 11            [12]  246 	add	a,_BEACON_decoding_sloc2_1_0
      004D23 F5 0F            [12]  247 	mov	_BEACON_decoding_sloc1_1_0,a
      004D25 E4               [12]  248 	clr	a
      004D26 35 12            [12]  249 	addc	a,(_BEACON_decoding_sloc2_1_0 + 1)
      004D28 F5 10            [12]  250 	mov	(_BEACON_decoding_sloc1_1_0 + 1),a
      004D2A 74 03            [12]  251 	mov	a,#0x03
      004D2C 29               [12]  252 	add	a,r1
      004D2D F8               [12]  253 	mov	r0,a
      004D2E E4               [12]  254 	clr	a
      004D2F 3A               [12]  255 	addc	a,r2
      004D30 FE               [12]  256 	mov	r6,a
      004D31 8B 07            [24]  257 	mov	ar7,r3
      004D33 88 82            [24]  258 	mov	dpl,r0
      004D35 8E 83            [24]  259 	mov	dph,r6
      004D37 8F F0            [24]  260 	mov	b,r7
      004D39 12 7D 6F         [24]  261 	lcall	__gptrget
      004D3C F8               [12]  262 	mov	r0,a
      004D3D 85 0F 82         [24]  263 	mov	dpl,_BEACON_decoding_sloc1_1_0
      004D40 85 10 83         [24]  264 	mov	dph,(_BEACON_decoding_sloc1_1_0 + 1)
      004D43 F0               [24]  265 	movx	@dptr,a
                                    266 ;	peripherals\Beacon.c:42: beacon->NumACK = 0xFF; //invalid data
      004D44 85 11 82         [24]  267 	mov	dpl,_BEACON_decoding_sloc2_1_0
      004D47 85 12 83         [24]  268 	mov	dph,(_BEACON_decoding_sloc2_1_0 + 1)
      004D4A A3               [24]  269 	inc	dptr
      004D4B A3               [24]  270 	inc	dptr
      004D4C A3               [24]  271 	inc	dptr
      004D4D A3               [24]  272 	inc	dptr
      004D4E A3               [24]  273 	inc	dptr
      004D4F 74 FF            [12]  274 	mov	a,#0xff
      004D51 F0               [24]  275 	movx	@dptr,a
      004D52 80 34            [24]  276 	sjmp	00106$
      004D54                        277 00105$:
                                    278 ;	peripherals\Beacon.c:43: }else if(beacon->BeaconID = DOWNLINK_BEACON)
      004D54 8C 82            [24]  279 	mov	dpl,r4
      004D56 8D 83            [24]  280 	mov	dph,r5
      004D58 74 0A            [12]  281 	mov	a,#0x0a
      004D5A F0               [24]  282 	movx	@dptr,a
                                    283 ;	peripherals\Beacon.c:45: beacon->NumACK = *(Rxbuffer+POS_NUMPREM);
      004D5B 03               [12]  284 	rr	a
      004D5C 25 11            [12]  285 	add	a,_BEACON_decoding_sloc2_1_0
      004D5E FE               [12]  286 	mov	r6,a
      004D5F E4               [12]  287 	clr	a
      004D60 35 12            [12]  288 	addc	a,(_BEACON_decoding_sloc2_1_0 + 1)
      004D62 FF               [12]  289 	mov	r7,a
      004D63 74 03            [12]  290 	mov	a,#0x03
      004D65 29               [12]  291 	add	a,r1
      004D66 F8               [12]  292 	mov	r0,a
      004D67 E4               [12]  293 	clr	a
      004D68 3A               [12]  294 	addc	a,r2
      004D69 FC               [12]  295 	mov	r4,a
      004D6A 8B 05            [24]  296 	mov	ar5,r3
      004D6C 88 82            [24]  297 	mov	dpl,r0
      004D6E 8C 83            [24]  298 	mov	dph,r4
      004D70 8D F0            [24]  299 	mov	b,r5
      004D72 12 7D 6F         [24]  300 	lcall	__gptrget
      004D75 F8               [12]  301 	mov	r0,a
      004D76 8E 82            [24]  302 	mov	dpl,r6
      004D78 8F 83            [24]  303 	mov	dph,r7
      004D7A F0               [24]  304 	movx	@dptr,a
                                    305 ;	peripherals\Beacon.c:46: beacon->NumPremSlots = 0xFF; //invalid data
      004D7B 85 11 82         [24]  306 	mov	dpl,_BEACON_decoding_sloc2_1_0
      004D7E 85 12 83         [24]  307 	mov	dph,(_BEACON_decoding_sloc2_1_0 + 1)
      004D81 A3               [24]  308 	inc	dptr
      004D82 A3               [24]  309 	inc	dptr
      004D83 A3               [24]  310 	inc	dptr
      004D84 A3               [24]  311 	inc	dptr
      004D85 74 FF            [12]  312 	mov	a,#0xff
      004D87 F0               [24]  313 	movx	@dptr,a
                                    314 ;	peripherals\Beacon.c:49: else beacon->FlagReceived = INVALID_BEACON;
      004D88                        315 00106$:
                                    316 ;	peripherals\Beacon.c:51: beacon->HMAC_flag =(*(Rxbuffer+POS_SECURITY) & MASK_HMAC_FLAG)>>7;
      004D88 74 06            [12]  317 	mov	a,#0x06
      004D8A 25 11            [12]  318 	add	a,_BEACON_decoding_sloc2_1_0
      004D8C F5 0F            [12]  319 	mov	_BEACON_decoding_sloc1_1_0,a
      004D8E E4               [12]  320 	clr	a
      004D8F 35 12            [12]  321 	addc	a,(_BEACON_decoding_sloc2_1_0 + 1)
      004D91 F5 10            [12]  322 	mov	(_BEACON_decoding_sloc1_1_0 + 1),a
      004D93 74 04            [12]  323 	mov	a,#0x04
      004D95 29               [12]  324 	add	a,r1
      004D96 F8               [12]  325 	mov	r0,a
      004D97 E4               [12]  326 	clr	a
      004D98 3A               [12]  327 	addc	a,r2
      004D99 FC               [12]  328 	mov	r4,a
      004D9A 8B 05            [24]  329 	mov	ar5,r3
      004D9C 88 82            [24]  330 	mov	dpl,r0
      004D9E 8C 83            [24]  331 	mov	dph,r4
      004DA0 8D F0            [24]  332 	mov	b,r5
      004DA2 12 7D 6F         [24]  333 	lcall	__gptrget
      004DA5 54 80            [12]  334 	anl	a,#0x80
      004DA7 23               [12]  335 	rl	a
      004DA8 54 01            [12]  336 	anl	a,#0x01
      004DAA 85 0F 82         [24]  337 	mov	dpl,_BEACON_decoding_sloc1_1_0
      004DAD 85 10 83         [24]  338 	mov	dph,(_BEACON_decoding_sloc1_1_0 + 1)
      004DB0 F0               [24]  339 	movx	@dptr,a
                                    340 ;	peripherals\Beacon.c:52: beacon->HMAC_type = (*(Rxbuffer+POS_SECURITY) & MASK_HMAC_TYPE)>>4;
      004DB1 74 07            [12]  341 	mov	a,#0x07
      004DB3 25 11            [12]  342 	add	a,_BEACON_decoding_sloc2_1_0
      004DB5 F5 0F            [12]  343 	mov	_BEACON_decoding_sloc1_1_0,a
      004DB7 E4               [12]  344 	clr	a
      004DB8 35 12            [12]  345 	addc	a,(_BEACON_decoding_sloc2_1_0 + 1)
      004DBA F5 10            [12]  346 	mov	(_BEACON_decoding_sloc1_1_0 + 1),a
      004DBC 88 82            [24]  347 	mov	dpl,r0
      004DBE 8C 83            [24]  348 	mov	dph,r4
      004DC0 8D F0            [24]  349 	mov	b,r5
      004DC2 12 7D 6F         [24]  350 	lcall	__gptrget
      004DC5 54 70            [12]  351 	anl	a,#0x70
      004DC7 C4               [12]  352 	swap	a
      004DC8 54 0F            [12]  353 	anl	a,#0x0f
      004DCA 85 0F 82         [24]  354 	mov	dpl,_BEACON_decoding_sloc1_1_0
      004DCD 85 10 83         [24]  355 	mov	dph,(_BEACON_decoding_sloc1_1_0 + 1)
      004DD0 F0               [24]  356 	movx	@dptr,a
                                    357 ;	peripherals\Beacon.c:53: beacon->Encryption_flag = (*(Rxbuffer+POS_SECURITY) & MASK_ENCRYPTION_FLAG)>>3;
      004DD1 74 08            [12]  358 	mov	a,#0x08
      004DD3 25 11            [12]  359 	add	a,_BEACON_decoding_sloc2_1_0
      004DD5 F5 0F            [12]  360 	mov	_BEACON_decoding_sloc1_1_0,a
      004DD7 E4               [12]  361 	clr	a
      004DD8 35 12            [12]  362 	addc	a,(_BEACON_decoding_sloc2_1_0 + 1)
      004DDA F5 10            [12]  363 	mov	(_BEACON_decoding_sloc1_1_0 + 1),a
      004DDC 88 82            [24]  364 	mov	dpl,r0
      004DDE 8C 83            [24]  365 	mov	dph,r4
      004DE0 8D F0            [24]  366 	mov	b,r5
      004DE2 12 7D 6F         [24]  367 	lcall	__gptrget
      004DE5 54 08            [12]  368 	anl	a,#0x08
      004DE7 C4               [12]  369 	swap	a
      004DE8 23               [12]  370 	rl	a
      004DE9 54 1F            [12]  371 	anl	a,#0x1f
      004DEB 85 0F 82         [24]  372 	mov	dpl,_BEACON_decoding_sloc1_1_0
      004DEE 85 10 83         [24]  373 	mov	dph,(_BEACON_decoding_sloc1_1_0 + 1)
      004DF1 F0               [24]  374 	movx	@dptr,a
                                    375 ;	peripherals\Beacon.c:54: beacon->Encryption_type = *(Rxbuffer+POS_SECURITY) & MASK_ENCRYPTION_TYPE;
      004DF2 74 09            [12]  376 	mov	a,#0x09
      004DF4 25 11            [12]  377 	add	a,_BEACON_decoding_sloc2_1_0
      004DF6 FE               [12]  378 	mov	r6,a
      004DF7 E4               [12]  379 	clr	a
      004DF8 35 12            [12]  380 	addc	a,(_BEACON_decoding_sloc2_1_0 + 1)
      004DFA FF               [12]  381 	mov	r7,a
      004DFB 88 82            [24]  382 	mov	dpl,r0
      004DFD 8C 83            [24]  383 	mov	dph,r4
      004DFF 8D F0            [24]  384 	mov	b,r5
      004E01 12 7D 6F         [24]  385 	lcall	__gptrget
      004E04 F8               [12]  386 	mov	r0,a
      004E05 53 00 07         [24]  387 	anl	ar0,#0x07
      004E08 8E 82            [24]  388 	mov	dpl,r6
      004E0A 8F 83            [24]  389 	mov	dph,r7
      004E0C E8               [12]  390 	mov	a,r0
      004E0D F0               [24]  391 	movx	@dptr,a
                                    392 ;	peripherals\Beacon.c:56: *(Rxbuffer+POS_NONCE)   = *(Rxbuffer+POS_NONCE) + *(Rxbuffer+POS_NONCE+1);
      004E0E 74 05            [12]  393 	mov	a,#0x05
      004E10 29               [12]  394 	add	a,r1
      004E11 F5 13            [12]  395 	mov	_BEACON_decoding_sloc3_1_0,a
      004E13 E4               [12]  396 	clr	a
      004E14 3A               [12]  397 	addc	a,r2
      004E15 F5 14            [12]  398 	mov	(_BEACON_decoding_sloc3_1_0 + 1),a
      004E17 8B 15            [24]  399 	mov	(_BEACON_decoding_sloc3_1_0 + 2),r3
      004E19 85 13 82         [24]  400 	mov	dpl,_BEACON_decoding_sloc3_1_0
      004E1C 85 14 83         [24]  401 	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
      004E1F 85 15 F0         [24]  402 	mov	b,(_BEACON_decoding_sloc3_1_0 + 2)
      004E22 12 7D 6F         [24]  403 	lcall	__gptrget
      004E25 F5 0F            [12]  404 	mov	_BEACON_decoding_sloc1_1_0,a
      004E27 74 06            [12]  405 	mov	a,#0x06
      004E29 29               [12]  406 	add	a,r1
      004E2A F8               [12]  407 	mov	r0,a
      004E2B E4               [12]  408 	clr	a
      004E2C 3A               [12]  409 	addc	a,r2
      004E2D FC               [12]  410 	mov	r4,a
      004E2E 8B 07            [24]  411 	mov	ar7,r3
      004E30 88 82            [24]  412 	mov	dpl,r0
      004E32 8C 83            [24]  413 	mov	dph,r4
      004E34 8F F0            [24]  414 	mov	b,r7
      004E36 12 7D 6F         [24]  415 	lcall	__gptrget
      004E39 25 0F            [12]  416 	add	a,_BEACON_decoding_sloc1_1_0
      004E3B FE               [12]  417 	mov	r6,a
      004E3C 85 13 82         [24]  418 	mov	dpl,_BEACON_decoding_sloc3_1_0
      004E3F 85 14 83         [24]  419 	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
      004E42 85 15 F0         [24]  420 	mov	b,(_BEACON_decoding_sloc3_1_0 + 2)
      004E45 12 70 8A         [24]  421 	lcall	__gptrput
                                    422 ;	peripherals\Beacon.c:57: *(Rxbuffer+POS_NONCE+1) = *(Rxbuffer+POS_NONCE) - *(Rxbuffer+POS_NONCE+1);
      004E48 88 82            [24]  423 	mov	dpl,r0
      004E4A 8C 83            [24]  424 	mov	dph,r4
      004E4C 8F F0            [24]  425 	mov	b,r7
      004E4E 12 7D 6F         [24]  426 	lcall	__gptrget
      004E51 FD               [12]  427 	mov	r5,a
      004E52 EE               [12]  428 	mov	a,r6
      004E53 C3               [12]  429 	clr	c
      004E54 9D               [12]  430 	subb	a,r5
      004E55 FD               [12]  431 	mov	r5,a
      004E56 88 82            [24]  432 	mov	dpl,r0
      004E58 8C 83            [24]  433 	mov	dph,r4
      004E5A 8F F0            [24]  434 	mov	b,r7
      004E5C 12 70 8A         [24]  435 	lcall	__gptrput
                                    436 ;	peripherals\Beacon.c:58: *(Rxbuffer+POS_NONCE)   = *(Rxbuffer+POS_NONCE) - *(Rxbuffer+POS_NONCE+1);
      004E5F EE               [12]  437 	mov	a,r6
      004E60 C3               [12]  438 	clr	c
      004E61 9D               [12]  439 	subb	a,r5
      004E62 85 13 82         [24]  440 	mov	dpl,_BEACON_decoding_sloc3_1_0
      004E65 85 14 83         [24]  441 	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
      004E68 85 15 F0         [24]  442 	mov	b,(_BEACON_decoding_sloc3_1_0 + 2)
      004E6B 12 70 8A         [24]  443 	lcall	__gptrput
                                    444 ;	peripherals\Beacon.c:60: memcpy(&beacon->Nonce,Rxbuffer+POS_NONCE,sizeof(uint16_t));
      004E6E 74 0A            [12]  445 	mov	a,#0x0a
      004E70 25 11            [12]  446 	add	a,_BEACON_decoding_sloc2_1_0
      004E72 FE               [12]  447 	mov	r6,a
      004E73 E4               [12]  448 	clr	a
      004E74 35 12            [12]  449 	addc	a,(_BEACON_decoding_sloc2_1_0 + 1)
      004E76 FF               [12]  450 	mov	r7,a
      004E77 8E 16            [24]  451 	mov	_BEACON_decoding_sloc4_1_0,r6
      004E79 8F 17            [24]  452 	mov	(_BEACON_decoding_sloc4_1_0 + 1),r7
      004E7B 75 18 00         [24]  453 	mov	(_BEACON_decoding_sloc4_1_0 + 2),#0x00
      004E7E A8 13            [24]  454 	mov	r0,_BEACON_decoding_sloc3_1_0
      004E80 AC 14            [24]  455 	mov	r4,(_BEACON_decoding_sloc3_1_0 + 1)
      004E82 AF 15            [24]  456 	mov	r7,(_BEACON_decoding_sloc3_1_0 + 2)
      004E84 90 03 DB         [24]  457 	mov	dptr,#_memcpy_PARM_2
      004E87 E8               [12]  458 	mov	a,r0
      004E88 F0               [24]  459 	movx	@dptr,a
      004E89 EC               [12]  460 	mov	a,r4
      004E8A A3               [24]  461 	inc	dptr
      004E8B F0               [24]  462 	movx	@dptr,a
      004E8C EF               [12]  463 	mov	a,r7
      004E8D A3               [24]  464 	inc	dptr
      004E8E F0               [24]  465 	movx	@dptr,a
      004E8F 90 03 DE         [24]  466 	mov	dptr,#_memcpy_PARM_3
      004E92 74 02            [12]  467 	mov	a,#0x02
      004E94 F0               [24]  468 	movx	@dptr,a
      004E95 E4               [12]  469 	clr	a
      004E96 A3               [24]  470 	inc	dptr
      004E97 F0               [24]  471 	movx	@dptr,a
      004E98 85 16 82         [24]  472 	mov	dpl,_BEACON_decoding_sloc4_1_0
      004E9B 85 17 83         [24]  473 	mov	dph,(_BEACON_decoding_sloc4_1_0 + 1)
      004E9E 85 18 F0         [24]  474 	mov	b,(_BEACON_decoding_sloc4_1_0 + 2)
      004EA1 C0 03            [24]  475 	push	ar3
      004EA3 C0 02            [24]  476 	push	ar2
      004EA5 C0 01            [24]  477 	push	ar1
      004EA7 12 6C 4F         [24]  478 	lcall	_memcpy
      004EAA D0 01            [24]  479 	pop	ar1
      004EAC D0 02            [24]  480 	pop	ar2
      004EAE D0 03            [24]  481 	pop	ar3
                                    482 ;	peripherals\Beacon.c:62: memcpy(&beacon->reserved,Rxbuffer+POS_RESERVED,sizeof(uint8_t)*5);
      004EB0 74 0C            [12]  483 	mov	a,#0x0c
      004EB2 25 11            [12]  484 	add	a,_BEACON_decoding_sloc2_1_0
      004EB4 FE               [12]  485 	mov	r6,a
      004EB5 E4               [12]  486 	clr	a
      004EB6 35 12            [12]  487 	addc	a,(_BEACON_decoding_sloc2_1_0 + 1)
      004EB8 FF               [12]  488 	mov	r7,a
      004EB9 8E 16            [24]  489 	mov	_BEACON_decoding_sloc4_1_0,r6
      004EBB 8F 17            [24]  490 	mov	(_BEACON_decoding_sloc4_1_0 + 1),r7
      004EBD 75 18 00         [24]  491 	mov	(_BEACON_decoding_sloc4_1_0 + 2),#0x00
      004EC0 74 07            [12]  492 	mov	a,#0x07
      004EC2 29               [12]  493 	add	a,r1
      004EC3 F8               [12]  494 	mov	r0,a
      004EC4 E4               [12]  495 	clr	a
      004EC5 3A               [12]  496 	addc	a,r2
      004EC6 FC               [12]  497 	mov	r4,a
      004EC7 8B 07            [24]  498 	mov	ar7,r3
      004EC9 90 03 DB         [24]  499 	mov	dptr,#_memcpy_PARM_2
      004ECC E8               [12]  500 	mov	a,r0
      004ECD F0               [24]  501 	movx	@dptr,a
      004ECE EC               [12]  502 	mov	a,r4
      004ECF A3               [24]  503 	inc	dptr
      004ED0 F0               [24]  504 	movx	@dptr,a
      004ED1 EF               [12]  505 	mov	a,r7
      004ED2 A3               [24]  506 	inc	dptr
      004ED3 F0               [24]  507 	movx	@dptr,a
      004ED4 90 03 DE         [24]  508 	mov	dptr,#_memcpy_PARM_3
      004ED7 74 05            [12]  509 	mov	a,#0x05
      004ED9 F0               [24]  510 	movx	@dptr,a
      004EDA E4               [12]  511 	clr	a
      004EDB A3               [24]  512 	inc	dptr
      004EDC F0               [24]  513 	movx	@dptr,a
      004EDD 85 16 82         [24]  514 	mov	dpl,_BEACON_decoding_sloc4_1_0
      004EE0 85 17 83         [24]  515 	mov	dph,(_BEACON_decoding_sloc4_1_0 + 1)
      004EE3 85 18 F0         [24]  516 	mov	b,(_BEACON_decoding_sloc4_1_0 + 2)
      004EE6 C0 03            [24]  517 	push	ar3
      004EE8 C0 02            [24]  518 	push	ar2
      004EEA C0 01            [24]  519 	push	ar1
      004EEC 12 6C 4F         [24]  520 	lcall	_memcpy
      004EEF D0 01            [24]  521 	pop	ar1
      004EF1 D0 02            [24]  522 	pop	ar2
      004EF3 D0 03            [24]  523 	pop	ar3
                                    524 ;	peripherals\Beacon.c:64: beacon->CRC =  *(Rxbuffer+POS_CRC) <<8 | *(Rxbuffer+POS_CRC+1);
      004EF5 74 11            [12]  525 	mov	a,#0x11
      004EF7 25 11            [12]  526 	add	a,_BEACON_decoding_sloc2_1_0
      004EF9 F5 13            [12]  527 	mov	_BEACON_decoding_sloc3_1_0,a
      004EFB E4               [12]  528 	clr	a
      004EFC 35 12            [12]  529 	addc	a,(_BEACON_decoding_sloc2_1_0 + 1)
      004EFE F5 14            [12]  530 	mov	(_BEACON_decoding_sloc3_1_0 + 1),a
      004F00 74 0C            [12]  531 	mov	a,#0x0c
      004F02 29               [12]  532 	add	a,r1
      004F03 F8               [12]  533 	mov	r0,a
      004F04 E4               [12]  534 	clr	a
      004F05 3A               [12]  535 	addc	a,r2
      004F06 FC               [12]  536 	mov	r4,a
      004F07 8B 05            [24]  537 	mov	ar5,r3
      004F09 88 82            [24]  538 	mov	dpl,r0
      004F0B 8C 83            [24]  539 	mov	dph,r4
      004F0D 8D F0            [24]  540 	mov	b,r5
      004F0F 12 7D 6F         [24]  541 	lcall	__gptrget
      004F12 F8               [12]  542 	mov	r0,a
      004F13 7D 00            [12]  543 	mov	r5,#0x00
      004F15 88 17            [24]  544 	mov	(_BEACON_decoding_sloc4_1_0 + 1),r0
                                    545 ;	1-genFromRTrack replaced	mov	_BEACON_decoding_sloc4_1_0,#0x00
      004F17 8D 16            [24]  546 	mov	_BEACON_decoding_sloc4_1_0,r5
      004F19 74 0D            [12]  547 	mov	a,#0x0d
      004F1B 29               [12]  548 	add	a,r1
      004F1C F8               [12]  549 	mov	r0,a
      004F1D E4               [12]  550 	clr	a
      004F1E 3A               [12]  551 	addc	a,r2
      004F1F FC               [12]  552 	mov	r4,a
      004F20 8B 05            [24]  553 	mov	ar5,r3
      004F22 88 82            [24]  554 	mov	dpl,r0
      004F24 8C 83            [24]  555 	mov	dph,r4
      004F26 8D F0            [24]  556 	mov	b,r5
      004F28 12 7D 6F         [24]  557 	lcall	__gptrget
      004F2B F8               [12]  558 	mov	r0,a
      004F2C 7D 00            [12]  559 	mov	r5,#0x00
      004F2E E5 16            [12]  560 	mov	a,_BEACON_decoding_sloc4_1_0
      004F30 42 00            [12]  561 	orl	ar0,a
      004F32 E5 17            [12]  562 	mov	a,(_BEACON_decoding_sloc4_1_0 + 1)
      004F34 42 05            [12]  563 	orl	ar5,a
      004F36 ED               [12]  564 	mov	a,r5
      004F37 FC               [12]  565 	mov	r4,a
      004F38 33               [12]  566 	rlc	a
      004F39 95 E0            [12]  567 	subb	a,acc
      004F3B FD               [12]  568 	mov	r5,a
      004F3C FF               [12]  569 	mov	r7,a
      004F3D 85 13 82         [24]  570 	mov	dpl,_BEACON_decoding_sloc3_1_0
      004F40 85 14 83         [24]  571 	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
      004F43 E8               [12]  572 	mov	a,r0
      004F44 F0               [24]  573 	movx	@dptr,a
      004F45 EC               [12]  574 	mov	a,r4
      004F46 A3               [24]  575 	inc	dptr
      004F47 F0               [24]  576 	movx	@dptr,a
      004F48 ED               [12]  577 	mov	a,r5
      004F49 A3               [24]  578 	inc	dptr
      004F4A F0               [24]  579 	movx	@dptr,a
      004F4B EF               [12]  580 	mov	a,r7
      004F4C A3               [24]  581 	inc	dptr
      004F4D F0               [24]  582 	movx	@dptr,a
                                    583 ;	peripherals\Beacon.c:65: beacon->CRC = beacon->CRC << 16;
      004F4E 8C 1C            [24]  584 	mov	(_BEACON_decoding_sloc5_1_0 + 3),r4
      004F50 88 1B            [24]  585 	mov	(_BEACON_decoding_sloc5_1_0 + 2),r0
      004F52 75 1A 00         [24]  586 	mov	(_BEACON_decoding_sloc5_1_0 + 1),#0x00
      004F55 75 19 00         [24]  587 	mov	_BEACON_decoding_sloc5_1_0,#0x00
      004F58 85 13 82         [24]  588 	mov	dpl,_BEACON_decoding_sloc3_1_0
      004F5B 85 14 83         [24]  589 	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
      004F5E E5 19            [12]  590 	mov	a,_BEACON_decoding_sloc5_1_0
      004F60 F0               [24]  591 	movx	@dptr,a
      004F61 E5 1A            [12]  592 	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
      004F63 A3               [24]  593 	inc	dptr
      004F64 F0               [24]  594 	movx	@dptr,a
      004F65 E5 1B            [12]  595 	mov	a,(_BEACON_decoding_sloc5_1_0 + 2)
      004F67 A3               [24]  596 	inc	dptr
      004F68 F0               [24]  597 	movx	@dptr,a
      004F69 E5 1C            [12]  598 	mov	a,(_BEACON_decoding_sloc5_1_0 + 3)
      004F6B A3               [24]  599 	inc	dptr
      004F6C F0               [24]  600 	movx	@dptr,a
                                    601 ;	peripherals\Beacon.c:66: beacon->CRC = beacon->CRC | *(Rxbuffer+POS_CRC+2)<<8 | *(Rxbuffer+POS_CRC+3);
      004F6D 74 0E            [12]  602 	mov	a,#0x0e
      004F6F 29               [12]  603 	add	a,r1
      004F70 FD               [12]  604 	mov	r5,a
      004F71 E4               [12]  605 	clr	a
      004F72 3A               [12]  606 	addc	a,r2
      004F73 FE               [12]  607 	mov	r6,a
      004F74 8B 07            [24]  608 	mov	ar7,r3
      004F76 8D 82            [24]  609 	mov	dpl,r5
      004F78 8E 83            [24]  610 	mov	dph,r6
      004F7A 8F F0            [24]  611 	mov	b,r7
      004F7C 12 7D 6F         [24]  612 	lcall	__gptrget
      004F7F FF               [12]  613 	mov	r7,a
      004F80 7D 00            [12]  614 	mov	r5,#0x00
      004F82 33               [12]  615 	rlc	a
      004F83 95 E0            [12]  616 	subb	a,acc
      004F85 FE               [12]  617 	mov	r6,a
      004F86 FC               [12]  618 	mov	r4,a
      004F87 ED               [12]  619 	mov	a,r5
      004F88 42 19            [12]  620 	orl	_BEACON_decoding_sloc5_1_0,a
      004F8A EF               [12]  621 	mov	a,r7
      004F8B 42 1A            [12]  622 	orl	(_BEACON_decoding_sloc5_1_0 + 1),a
      004F8D EE               [12]  623 	mov	a,r6
      004F8E 42 1B            [12]  624 	orl	(_BEACON_decoding_sloc5_1_0 + 2),a
      004F90 EC               [12]  625 	mov	a,r4
      004F91 42 1C            [12]  626 	orl	(_BEACON_decoding_sloc5_1_0 + 3),a
      004F93 74 0F            [12]  627 	mov	a,#0x0f
      004F95 29               [12]  628 	add	a,r1
      004F96 F8               [12]  629 	mov	r0,a
      004F97 E4               [12]  630 	clr	a
      004F98 3A               [12]  631 	addc	a,r2
      004F99 FE               [12]  632 	mov	r6,a
      004F9A 8B 07            [24]  633 	mov	ar7,r3
      004F9C 88 82            [24]  634 	mov	dpl,r0
      004F9E 8E 83            [24]  635 	mov	dph,r6
      004FA0 8F F0            [24]  636 	mov	b,r7
      004FA2 12 7D 6F         [24]  637 	lcall	__gptrget
      004FA5 F8               [12]  638 	mov	r0,a
      004FA6 E4               [12]  639 	clr	a
      004FA7 FF               [12]  640 	mov	r7,a
      004FA8 FE               [12]  641 	mov	r6,a
      004FA9 FD               [12]  642 	mov	r5,a
      004FAA E5 19            [12]  643 	mov	a,_BEACON_decoding_sloc5_1_0
      004FAC 42 00            [12]  644 	orl	ar0,a
      004FAE E5 1A            [12]  645 	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
      004FB0 42 07            [12]  646 	orl	ar7,a
      004FB2 E5 1B            [12]  647 	mov	a,(_BEACON_decoding_sloc5_1_0 + 2)
      004FB4 42 06            [12]  648 	orl	ar6,a
      004FB6 E5 1C            [12]  649 	mov	a,(_BEACON_decoding_sloc5_1_0 + 3)
      004FB8 42 05            [12]  650 	orl	ar5,a
      004FBA 85 13 82         [24]  651 	mov	dpl,_BEACON_decoding_sloc3_1_0
      004FBD 85 14 83         [24]  652 	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
      004FC0 E8               [12]  653 	mov	a,r0
      004FC1 F0               [24]  654 	movx	@dptr,a
      004FC2 EF               [12]  655 	mov	a,r7
      004FC3 A3               [24]  656 	inc	dptr
      004FC4 F0               [24]  657 	movx	@dptr,a
      004FC5 EE               [12]  658 	mov	a,r6
      004FC6 A3               [24]  659 	inc	dptr
      004FC7 F0               [24]  660 	movx	@dptr,a
      004FC8 ED               [12]  661 	mov	a,r5
      004FC9 A3               [24]  662 	inc	dptr
      004FCA F0               [24]  663 	movx	@dptr,a
                                    664 ;	peripherals\Beacon.c:68: beacon->HMAC_Msb = *(Rxbuffer+POS_HMAC_MSB)<<8 | *(Rxbuffer+POS_HMAC_MSB+1);
      004FCB 74 15            [12]  665 	mov	a,#0x15
      004FCD 25 11            [12]  666 	add	a,_BEACON_decoding_sloc2_1_0
      004FCF F5 16            [12]  667 	mov	_BEACON_decoding_sloc4_1_0,a
      004FD1 E4               [12]  668 	clr	a
      004FD2 35 12            [12]  669 	addc	a,(_BEACON_decoding_sloc2_1_0 + 1)
      004FD4 F5 17            [12]  670 	mov	(_BEACON_decoding_sloc4_1_0 + 1),a
      004FD6 74 10            [12]  671 	mov	a,#0x10
      004FD8 29               [12]  672 	add	a,r1
      004FD9 F8               [12]  673 	mov	r0,a
      004FDA E4               [12]  674 	clr	a
      004FDB 3A               [12]  675 	addc	a,r2
      004FDC FC               [12]  676 	mov	r4,a
      004FDD 8B 05            [24]  677 	mov	ar5,r3
      004FDF 88 82            [24]  678 	mov	dpl,r0
      004FE1 8C 83            [24]  679 	mov	dph,r4
      004FE3 8D F0            [24]  680 	mov	b,r5
      004FE5 12 7D 6F         [24]  681 	lcall	__gptrget
      004FE8 F8               [12]  682 	mov	r0,a
      004FE9 7D 00            [12]  683 	mov	r5,#0x00
      004FEB 88 1A            [24]  684 	mov	(_BEACON_decoding_sloc5_1_0 + 1),r0
                                    685 ;	1-genFromRTrack replaced	mov	_BEACON_decoding_sloc5_1_0,#0x00
      004FED 8D 19            [24]  686 	mov	_BEACON_decoding_sloc5_1_0,r5
      004FEF 74 11            [12]  687 	mov	a,#0x11
      004FF1 29               [12]  688 	add	a,r1
      004FF2 F8               [12]  689 	mov	r0,a
      004FF3 E4               [12]  690 	clr	a
      004FF4 3A               [12]  691 	addc	a,r2
      004FF5 FC               [12]  692 	mov	r4,a
      004FF6 8B 05            [24]  693 	mov	ar5,r3
      004FF8 88 82            [24]  694 	mov	dpl,r0
      004FFA 8C 83            [24]  695 	mov	dph,r4
      004FFC 8D F0            [24]  696 	mov	b,r5
      004FFE 12 7D 6F         [24]  697 	lcall	__gptrget
      005001 F8               [12]  698 	mov	r0,a
      005002 7D 00            [12]  699 	mov	r5,#0x00
      005004 E5 19            [12]  700 	mov	a,_BEACON_decoding_sloc5_1_0
      005006 42 00            [12]  701 	orl	ar0,a
      005008 E5 1A            [12]  702 	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
      00500A 42 05            [12]  703 	orl	ar5,a
      00500C ED               [12]  704 	mov	a,r5
      00500D FC               [12]  705 	mov	r4,a
      00500E 33               [12]  706 	rlc	a
      00500F 95 E0            [12]  707 	subb	a,acc
      005011 FD               [12]  708 	mov	r5,a
      005012 FF               [12]  709 	mov	r7,a
      005013 85 16 82         [24]  710 	mov	dpl,_BEACON_decoding_sloc4_1_0
      005016 85 17 83         [24]  711 	mov	dph,(_BEACON_decoding_sloc4_1_0 + 1)
      005019 E8               [12]  712 	mov	a,r0
      00501A F0               [24]  713 	movx	@dptr,a
      00501B EC               [12]  714 	mov	a,r4
      00501C A3               [24]  715 	inc	dptr
      00501D F0               [24]  716 	movx	@dptr,a
      00501E ED               [12]  717 	mov	a,r5
      00501F A3               [24]  718 	inc	dptr
      005020 F0               [24]  719 	movx	@dptr,a
      005021 EF               [12]  720 	mov	a,r7
      005022 A3               [24]  721 	inc	dptr
      005023 F0               [24]  722 	movx	@dptr,a
                                    723 ;	peripherals\Beacon.c:69: beacon->HMAC_Msb = beacon->HMAC_Msb << 16;
      005024 8C 1C            [24]  724 	mov	(_BEACON_decoding_sloc5_1_0 + 3),r4
      005026 88 1B            [24]  725 	mov	(_BEACON_decoding_sloc5_1_0 + 2),r0
      005028 75 1A 00         [24]  726 	mov	(_BEACON_decoding_sloc5_1_0 + 1),#0x00
      00502B 75 19 00         [24]  727 	mov	_BEACON_decoding_sloc5_1_0,#0x00
      00502E 85 16 82         [24]  728 	mov	dpl,_BEACON_decoding_sloc4_1_0
      005031 85 17 83         [24]  729 	mov	dph,(_BEACON_decoding_sloc4_1_0 + 1)
      005034 E5 19            [12]  730 	mov	a,_BEACON_decoding_sloc5_1_0
      005036 F0               [24]  731 	movx	@dptr,a
      005037 E5 1A            [12]  732 	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
      005039 A3               [24]  733 	inc	dptr
      00503A F0               [24]  734 	movx	@dptr,a
      00503B E5 1B            [12]  735 	mov	a,(_BEACON_decoding_sloc5_1_0 + 2)
      00503D A3               [24]  736 	inc	dptr
      00503E F0               [24]  737 	movx	@dptr,a
      00503F E5 1C            [12]  738 	mov	a,(_BEACON_decoding_sloc5_1_0 + 3)
      005041 A3               [24]  739 	inc	dptr
      005042 F0               [24]  740 	movx	@dptr,a
                                    741 ;	peripherals\Beacon.c:70: beacon->HMAC_Msb = beacon->HMAC_Msb | (*(Rxbuffer+POS_HMAC_MSB+2)<<8 | *(Rxbuffer+POS_HMAC_MSB+3) & 0x0000FFFF);
      005043 74 12            [12]  742 	mov	a,#0x12
      005045 29               [12]  743 	add	a,r1
      005046 FD               [12]  744 	mov	r5,a
      005047 E4               [12]  745 	clr	a
      005048 3A               [12]  746 	addc	a,r2
      005049 FE               [12]  747 	mov	r6,a
      00504A 8B 07            [24]  748 	mov	ar7,r3
      00504C 8D 82            [24]  749 	mov	dpl,r5
      00504E 8E 83            [24]  750 	mov	dph,r6
      005050 8F F0            [24]  751 	mov	b,r7
      005052 12 7D 6F         [24]  752 	lcall	__gptrget
      005055 FF               [12]  753 	mov	r7,a
      005056 7D 00            [12]  754 	mov	r5,#0x00
      005058 74 13            [12]  755 	mov	a,#0x13
      00505A 29               [12]  756 	add	a,r1
      00505B F8               [12]  757 	mov	r0,a
      00505C E4               [12]  758 	clr	a
      00505D 3A               [12]  759 	addc	a,r2
      00505E FC               [12]  760 	mov	r4,a
      00505F 8B 06            [24]  761 	mov	ar6,r3
      005061 88 82            [24]  762 	mov	dpl,r0
      005063 8C 83            [24]  763 	mov	dph,r4
      005065 8E F0            [24]  764 	mov	b,r6
      005067 12 7D 6F         [24]  765 	lcall	__gptrget
      00506A FE               [12]  766 	mov	r6,a
      00506B 78 00            [12]  767 	mov	r0,#0x00
      00506D ED               [12]  768 	mov	a,r5
      00506E 42 06            [12]  769 	orl	ar6,a
      005070 EF               [12]  770 	mov	a,r7
      005071 42 00            [12]  771 	orl	ar0,a
      005073 E4               [12]  772 	clr	a
      005074 FF               [12]  773 	mov	r7,a
      005075 FD               [12]  774 	mov	r5,a
      005076 E5 19            [12]  775 	mov	a,_BEACON_decoding_sloc5_1_0
      005078 42 06            [12]  776 	orl	ar6,a
      00507A E5 1A            [12]  777 	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
      00507C 42 00            [12]  778 	orl	ar0,a
      00507E E5 1B            [12]  779 	mov	a,(_BEACON_decoding_sloc5_1_0 + 2)
      005080 42 07            [12]  780 	orl	ar7,a
      005082 E5 1C            [12]  781 	mov	a,(_BEACON_decoding_sloc5_1_0 + 3)
      005084 42 05            [12]  782 	orl	ar5,a
      005086 85 16 82         [24]  783 	mov	dpl,_BEACON_decoding_sloc4_1_0
      005089 85 17 83         [24]  784 	mov	dph,(_BEACON_decoding_sloc4_1_0 + 1)
      00508C EE               [12]  785 	mov	a,r6
      00508D F0               [24]  786 	movx	@dptr,a
      00508E E8               [12]  787 	mov	a,r0
      00508F A3               [24]  788 	inc	dptr
      005090 F0               [24]  789 	movx	@dptr,a
      005091 EF               [12]  790 	mov	a,r7
      005092 A3               [24]  791 	inc	dptr
      005093 F0               [24]  792 	movx	@dptr,a
      005094 ED               [12]  793 	mov	a,r5
      005095 A3               [24]  794 	inc	dptr
      005096 F0               [24]  795 	movx	@dptr,a
                                    796 ;	peripherals\Beacon.c:72: beacon->HMAC_Lsb = *(Rxbuffer+POS_HMAC_LSB)<<8 | *(Rxbuffer+POS_HMAC_LSB+1);
      005097 74 19            [12]  797 	mov	a,#0x19
      005099 25 11            [12]  798 	add	a,_BEACON_decoding_sloc2_1_0
      00509B F5 16            [12]  799 	mov	_BEACON_decoding_sloc4_1_0,a
      00509D E4               [12]  800 	clr	a
      00509E 35 12            [12]  801 	addc	a,(_BEACON_decoding_sloc2_1_0 + 1)
      0050A0 F5 17            [12]  802 	mov	(_BEACON_decoding_sloc4_1_0 + 1),a
      0050A2 74 14            [12]  803 	mov	a,#0x14
      0050A4 29               [12]  804 	add	a,r1
      0050A5 F8               [12]  805 	mov	r0,a
      0050A6 E4               [12]  806 	clr	a
      0050A7 3A               [12]  807 	addc	a,r2
      0050A8 FC               [12]  808 	mov	r4,a
      0050A9 8B 05            [24]  809 	mov	ar5,r3
      0050AB 88 82            [24]  810 	mov	dpl,r0
      0050AD 8C 83            [24]  811 	mov	dph,r4
      0050AF 8D F0            [24]  812 	mov	b,r5
      0050B1 12 7D 6F         [24]  813 	lcall	__gptrget
      0050B4 F8               [12]  814 	mov	r0,a
      0050B5 7D 00            [12]  815 	mov	r5,#0x00
      0050B7 88 1A            [24]  816 	mov	(_BEACON_decoding_sloc5_1_0 + 1),r0
                                    817 ;	1-genFromRTrack replaced	mov	_BEACON_decoding_sloc5_1_0,#0x00
      0050B9 8D 19            [24]  818 	mov	_BEACON_decoding_sloc5_1_0,r5
      0050BB 74 15            [12]  819 	mov	a,#0x15
      0050BD 29               [12]  820 	add	a,r1
      0050BE F8               [12]  821 	mov	r0,a
      0050BF E4               [12]  822 	clr	a
      0050C0 3A               [12]  823 	addc	a,r2
      0050C1 FC               [12]  824 	mov	r4,a
      0050C2 8B 05            [24]  825 	mov	ar5,r3
      0050C4 88 82            [24]  826 	mov	dpl,r0
      0050C6 8C 83            [24]  827 	mov	dph,r4
      0050C8 8D F0            [24]  828 	mov	b,r5
      0050CA 12 7D 6F         [24]  829 	lcall	__gptrget
      0050CD F8               [12]  830 	mov	r0,a
      0050CE 7D 00            [12]  831 	mov	r5,#0x00
      0050D0 E5 19            [12]  832 	mov	a,_BEACON_decoding_sloc5_1_0
      0050D2 42 00            [12]  833 	orl	ar0,a
      0050D4 E5 1A            [12]  834 	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
      0050D6 42 05            [12]  835 	orl	ar5,a
      0050D8 ED               [12]  836 	mov	a,r5
      0050D9 FC               [12]  837 	mov	r4,a
      0050DA 33               [12]  838 	rlc	a
      0050DB 95 E0            [12]  839 	subb	a,acc
      0050DD FD               [12]  840 	mov	r5,a
      0050DE FF               [12]  841 	mov	r7,a
      0050DF 85 16 82         [24]  842 	mov	dpl,_BEACON_decoding_sloc4_1_0
      0050E2 85 17 83         [24]  843 	mov	dph,(_BEACON_decoding_sloc4_1_0 + 1)
      0050E5 E8               [12]  844 	mov	a,r0
      0050E6 F0               [24]  845 	movx	@dptr,a
      0050E7 EC               [12]  846 	mov	a,r4
      0050E8 A3               [24]  847 	inc	dptr
      0050E9 F0               [24]  848 	movx	@dptr,a
      0050EA ED               [12]  849 	mov	a,r5
      0050EB A3               [24]  850 	inc	dptr
      0050EC F0               [24]  851 	movx	@dptr,a
      0050ED EF               [12]  852 	mov	a,r7
      0050EE A3               [24]  853 	inc	dptr
      0050EF F0               [24]  854 	movx	@dptr,a
                                    855 ;	peripherals\Beacon.c:73: beacon->HMAC_Lsb = beacon->HMAC_Lsb << 16;
      0050F0 8C 1C            [24]  856 	mov	(_BEACON_decoding_sloc5_1_0 + 3),r4
      0050F2 88 1B            [24]  857 	mov	(_BEACON_decoding_sloc5_1_0 + 2),r0
      0050F4 75 1A 00         [24]  858 	mov	(_BEACON_decoding_sloc5_1_0 + 1),#0x00
      0050F7 75 19 00         [24]  859 	mov	_BEACON_decoding_sloc5_1_0,#0x00
      0050FA 85 16 82         [24]  860 	mov	dpl,_BEACON_decoding_sloc4_1_0
      0050FD 85 17 83         [24]  861 	mov	dph,(_BEACON_decoding_sloc4_1_0 + 1)
      005100 E5 19            [12]  862 	mov	a,_BEACON_decoding_sloc5_1_0
      005102 F0               [24]  863 	movx	@dptr,a
      005103 E5 1A            [12]  864 	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
      005105 A3               [24]  865 	inc	dptr
      005106 F0               [24]  866 	movx	@dptr,a
      005107 E5 1B            [12]  867 	mov	a,(_BEACON_decoding_sloc5_1_0 + 2)
      005109 A3               [24]  868 	inc	dptr
      00510A F0               [24]  869 	movx	@dptr,a
      00510B E5 1C            [12]  870 	mov	a,(_BEACON_decoding_sloc5_1_0 + 3)
      00510D A3               [24]  871 	inc	dptr
      00510E F0               [24]  872 	movx	@dptr,a
                                    873 ;	peripherals\Beacon.c:74: beacon->HMAC_Lsb = beacon->HMAC_Lsb | (*(Rxbuffer+POS_HMAC_LSB+2)<<8 | *(Rxbuffer+POS_HMAC_LSB+3) & 0x0000FFFF);
      00510F 74 16            [12]  874 	mov	a,#0x16
      005111 29               [12]  875 	add	a,r1
      005112 FD               [12]  876 	mov	r5,a
      005113 E4               [12]  877 	clr	a
      005114 3A               [12]  878 	addc	a,r2
      005115 FE               [12]  879 	mov	r6,a
      005116 8B 07            [24]  880 	mov	ar7,r3
      005118 8D 82            [24]  881 	mov	dpl,r5
      00511A 8E 83            [24]  882 	mov	dph,r6
      00511C 8F F0            [24]  883 	mov	b,r7
      00511E 12 7D 6F         [24]  884 	lcall	__gptrget
      005121 FF               [12]  885 	mov	r7,a
      005122 7D 00            [12]  886 	mov	r5,#0x00
      005124 74 17            [12]  887 	mov	a,#0x17
      005126 29               [12]  888 	add	a,r1
      005127 F9               [12]  889 	mov	r1,a
      005128 E4               [12]  890 	clr	a
      005129 3A               [12]  891 	addc	a,r2
      00512A FA               [12]  892 	mov	r2,a
      00512B 89 82            [24]  893 	mov	dpl,r1
      00512D 8A 83            [24]  894 	mov	dph,r2
      00512F 8B F0            [24]  895 	mov	b,r3
      005131 12 7D 6F         [24]  896 	lcall	__gptrget
      005134 FE               [12]  897 	mov	r6,a
      005135 79 00            [12]  898 	mov	r1,#0x00
      005137 ED               [12]  899 	mov	a,r5
      005138 42 06            [12]  900 	orl	ar6,a
      00513A EF               [12]  901 	mov	a,r7
      00513B 42 01            [12]  902 	orl	ar1,a
      00513D E4               [12]  903 	clr	a
      00513E FF               [12]  904 	mov	r7,a
      00513F FD               [12]  905 	mov	r5,a
      005140 E5 19            [12]  906 	mov	a,_BEACON_decoding_sloc5_1_0
      005142 42 06            [12]  907 	orl	ar6,a
      005144 E5 1A            [12]  908 	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
      005146 42 01            [12]  909 	orl	ar1,a
      005148 E5 1B            [12]  910 	mov	a,(_BEACON_decoding_sloc5_1_0 + 2)
      00514A 42 07            [12]  911 	orl	ar7,a
      00514C E5 1C            [12]  912 	mov	a,(_BEACON_decoding_sloc5_1_0 + 3)
      00514E 42 05            [12]  913 	orl	ar5,a
      005150 85 16 82         [24]  914 	mov	dpl,_BEACON_decoding_sloc4_1_0
      005153 85 17 83         [24]  915 	mov	dph,(_BEACON_decoding_sloc4_1_0 + 1)
      005156 EE               [12]  916 	mov	a,r6
      005157 F0               [24]  917 	movx	@dptr,a
      005158 E9               [12]  918 	mov	a,r1
      005159 A3               [24]  919 	inc	dptr
      00515A F0               [24]  920 	movx	@dptr,a
      00515B EF               [12]  921 	mov	a,r7
      00515C A3               [24]  922 	inc	dptr
      00515D F0               [24]  923 	movx	@dptr,a
      00515E ED               [12]  924 	mov	a,r5
      00515F A3               [24]  925 	inc	dptr
      005160 F0               [24]  926 	movx	@dptr,a
      005161 22               [24]  927 	ret
      005162                        928 00108$:
                                    929 ;	peripherals\Beacon.c:75: }else beacon->FlagReceived = INVALID_BEACON;
      005162 85 11 82         [24]  930 	mov	dpl,_BEACON_decoding_sloc2_1_0
      005165 85 12 83         [24]  931 	mov	dph,(_BEACON_decoding_sloc2_1_0 + 1)
      005168 E4               [12]  932 	clr	a
      005169 F0               [24]  933 	movx	@dptr,a
      00516A 22               [24]  934 	ret
                                    935 	.area CSEG    (CODE)
                                    936 	.area CONST   (CODE)
                                    937 	.area XINIT   (CODE)
                                    938 	.area CABS    (ABS,CODE)
