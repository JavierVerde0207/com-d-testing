;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.6.0 #9615 (MINGW64)
;--------------------------------------------------------
	.module GoldSequence
	.optsdcc -mmcs51 --model-large
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _flash_read
	.globl _flash_write
	.globl _flash_lock
	.globl _flash_unlock
	.globl _random
	.globl _aligned_alloc_PARM_2
	.globl _GOLDSEQUENCE_Init
	.globl _GOLDSEQUENCE_Obtain
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
;--------------------------------------------------------
; special function bits
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
;--------------------------------------------------------
; overlayable register banks
;--------------------------------------------------------
	.area REG_BANK_0	(REL,OVR,DATA)
	.ds 8
;--------------------------------------------------------
; internal ram data
;--------------------------------------------------------
	.area DSEG    (DATA)
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;--------------------------------------------------------
; indirectly addressable internal ram data
;--------------------------------------------------------
	.area ISEG    (DATA)
;--------------------------------------------------------
; absolute internal ram data
;--------------------------------------------------------
	.area IABS    (ABS,DATA)
	.area IABS    (ABS,DATA)
;--------------------------------------------------------
; bit data
;--------------------------------------------------------
	.area BSEG    (BIT)
;--------------------------------------------------------
; paged external ram data
;--------------------------------------------------------
	.area PSEG    (PAG,XDATA)
;--------------------------------------------------------
; external ram data
;--------------------------------------------------------
	.area XSEG    (XDATA)
_aligned_alloc_PARM_2:
	.ds 2
_flash_deviceid	=	0xfc06
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area XABS    (ABS,XDATA)
;--------------------------------------------------------
; external initialized ram data
;--------------------------------------------------------
	.area XISEG   (XDATA)
	.area HOME    (CODE)
	.area GSINIT0 (CODE)
	.area GSINIT1 (CODE)
	.area GSINIT2 (CODE)
	.area GSINIT3 (CODE)
	.area GSINIT4 (CODE)
	.area GSINIT5 (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area CSEG    (CODE)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME    (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area GSINIT  (CODE)
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME    (CODE)
	.area HOME    (CODE)
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CSEG    (CODE)
;------------------------------------------------------------
;Allocation info for local variables in function 'GOLDSEQUENCE_Init'
;------------------------------------------------------------
;	peripherals\GoldSequence.c:26: void GOLDSEQUENCE_Init(void)
;	-----------------------------------------
;	 function GOLDSEQUENCE_Init
;	-----------------------------------------
_GOLDSEQUENCE_Init:
	ar7 = 0x07
	ar6 = 0x06
	ar5 = 0x05
	ar4 = 0x04
	ar3 = 0x03
	ar2 = 0x02
	ar1 = 0x01
	ar0 = 0x00
;	peripherals\GoldSequence.c:29: flash_unlock();
	lcall	_flash_unlock
;	peripherals\GoldSequence.c:32: flash_write(0x0000,0b1111100110100100);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0xa4
	movx	@dptr,a
	mov	a,#0xf9
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0000
	lcall	_flash_write
;	peripherals\GoldSequence.c:34: flash_write(0x0002,0b001010111011000);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0xd8
	movx	@dptr,a
	mov	a,#0x15
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0002
	lcall	_flash_write
;	peripherals\GoldSequence.c:37: flash_write(0x0004,0b1111100100110000);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x30
	movx	@dptr,a
	mov	a,#0xf9
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0004
	lcall	_flash_write
;	peripherals\GoldSequence.c:39: flash_write(0x0006,0b101101010001110);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x8e
	movx	@dptr,a
	mov	a,#0x5a
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0006
	lcall	_flash_write
;	peripherals\GoldSequence.c:42: flash_write(0x0008,0b0000000010010100);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x94
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0008
	lcall	_flash_write
;	peripherals\GoldSequence.c:44: flash_write(0x000A,0b100111101010110);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x56
	movx	@dptr,a
	mov	a,#0x4f
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x000a
	lcall	_flash_write
;	peripherals\GoldSequence.c:47: flash_write(0x000C,0b1000010100111100);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x3c
	movx	@dptr,a
	mov	a,#0x85
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x000c
	lcall	_flash_write
;	peripherals\GoldSequence.c:49: flash_write(0x000E,0b011100010011111);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x9f
	movx	@dptr,a
	mov	a,#0x38
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x000e
	lcall	_flash_write
;	peripherals\GoldSequence.c:52: flash_write(0x0010,0b0100011111101000);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0xe8
	movx	@dptr,a
	mov	a,#0x47
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0010
	lcall	_flash_write
;	peripherals\GoldSequence.c:54: flash_write(0x0012,0b000001101111011);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x7b
	movx	@dptr,a
	mov	a,#0x03
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0012
	lcall	_flash_write
;	peripherals\GoldSequence.c:57: flash_write(0x0014,0b0010011010000010);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x82
	movx	@dptr,a
	mov	a,#0x26
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0014
	lcall	_flash_write
;	peripherals\GoldSequence.c:59: flash_write(0x0016,0b001111010001001);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x89
	movx	@dptr,a
	mov	a,#0x1e
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0016
	lcall	_flash_write
;	peripherals\GoldSequence.c:62: flash_write(0x0018,0b0001011000110111);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x37
	movx	@dptr,a
	mov	a,#0x16
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0018
	lcall	_flash_write
;	peripherals\GoldSequence.c:64: flash_write(0x001A,0b001000001110000);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x70
	movx	@dptr,a
	mov	a,#0x10
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x001a
	lcall	_flash_write
;	peripherals\GoldSequence.c:67: flash_write(0x001C,0b1000111001101101);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x6d
	movx	@dptr,a
	mov	a,#0x8e
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x001c
	lcall	_flash_write
;	peripherals\GoldSequence.c:69: flash_write(0x001E,0b101011100001100);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x0c
	movx	@dptr,a
	mov	a,#0x57
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x001e
	lcall	_flash_write
;	peripherals\GoldSequence.c:72: flash_write(0x0020,0b1100001001000000);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x40
	movx	@dptr,a
	mov	a,#0xc2
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0020
	lcall	_flash_write
;	peripherals\GoldSequence.c:74: flash_write(0x0022,0b111010010110010);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0xb2
	movx	@dptr,a
	mov	a,#0x74
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0022
	lcall	_flash_write
;	peripherals\GoldSequence.c:77: flash_write(0x0024,0b1110010001010110);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x56
	movx	@dptr,a
	mov	a,#0xe4
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0024
	lcall	_flash_write
;	peripherals\GoldSequence.c:79: flash_write(0x0026,0b010010101101101);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x6d
	movx	@dptr,a
	mov	a,#0x25
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0026
	lcall	_flash_write
;	peripherals\GoldSequence.c:82: flash_write(0x0028,0b0111011101011101);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x5d
	movx	@dptr,a
	mov	a,#0x77
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0028
	lcall	_flash_write
;	peripherals\GoldSequence.c:84: flash_write(0x002A,0b000110110000010);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x82
	movx	@dptr,a
	mov	a,#0x0d
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x002a
	lcall	_flash_write
;	peripherals\GoldSequence.c:87: flash_write(0x002C,0b1011111011011000);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0xd8
	movx	@dptr,a
	mov	a,#0xbe
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x002c
	lcall	_flash_write
;	peripherals\GoldSequence.c:89: flash_write(0x002E,0b101100111110101);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0xf5
	movx	@dptr,a
	mov	a,#0x59
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x002e
	lcall	_flash_write
;	peripherals\GoldSequence.c:92: flash_write(0x0030,0b0101101000011010);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x1a
	movx	@dptr,a
	mov	a,#0x5a
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0030
	lcall	_flash_write
;	peripherals\GoldSequence.c:94: flash_write(0x0032,0b011001111001110);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0xce
	movx	@dptr,a
	mov	a,#0x33
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0032
	lcall	_flash_write
;	peripherals\GoldSequence.c:97: flash_write(0x0034,0b1010100001111011);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x7b
	movx	@dptr,a
	mov	a,#0xa8
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0034
	lcall	_flash_write
;	peripherals\GoldSequence.c:99: flash_write(0x0036,0b000011011010011);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0xd3
	movx	@dptr,a
	mov	a,#0x06
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0036
	lcall	_flash_write
;	peripherals\GoldSequence.c:102: flash_write(0x0038,0b0101000101001011);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x4b
	movx	@dptr,a
	mov	a,#0x51
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0038
	lcall	_flash_write
;	peripherals\GoldSequence.c:104: flash_write(0x003A,0b101110001011101);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x5d
	movx	@dptr,a
	dec	a
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x003a
	lcall	_flash_write
;	peripherals\GoldSequence.c:107: flash_write(0x003C,0b0010110111010011);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0xd3
	movx	@dptr,a
	mov	a,#0x2d
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x003c
	lcall	_flash_write
;	peripherals\GoldSequence.c:109: flash_write(0x003E,0b111000100011010);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x1a
	movx	@dptr,a
	mov	a,#0x71
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x003e
	lcall	_flash_write
;	peripherals\GoldSequence.c:112: flash_write(0x0040,0b1001001110011111);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x9f
	movx	@dptr,a
	mov	a,#0x93
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0040
	lcall	_flash_write
;	peripherals\GoldSequence.c:114: flash_write(0x0042,0b110011110111001);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0xb9
	movx	@dptr,a
	mov	a,#0x67
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0042
	lcall	_flash_write
;	peripherals\GoldSequence.c:117: flash_write(0x0044,0b0100110010111001);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0xb9
	movx	@dptr,a
	mov	a,#0x4c
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0044
	lcall	_flash_write
;	peripherals\GoldSequence.c:119: flash_write(0x0046,0b110110011101000);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0xe8
	movx	@dptr,a
	mov	a,#0x6c
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0046
	lcall	_flash_write
;	peripherals\GoldSequence.c:122: flash_write(0x0048,0b1010001100101010);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x2a
	movx	@dptr,a
	mov	a,#0xa3
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0048
	lcall	_flash_write
;	peripherals\GoldSequence.c:124: flash_write(0x004A,0b110100101000000);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x40
	movx	@dptr,a
	mov	a,#0x69
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x004a
	lcall	_flash_write
;	peripherals\GoldSequence.c:127: flash_write(0x004C,0b1101010011100011);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0xe3
	movx	@dptr,a
	mov	a,#0xd4
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x004c
	lcall	_flash_write
;	peripherals\GoldSequence.c:129: flash_write(0x004E,0b010101110010100);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x94
	movx	@dptr,a
	mov	a,#0x2b
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x004e
	lcall	_flash_write
;	peripherals\GoldSequence.c:132: flash_write(0x0050,0b1110111100000111);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x07
	movx	@dptr,a
	mov	a,#0xef
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0050
	lcall	_flash_write
;	peripherals\GoldSequence.c:134: flash_write(0x0052,0b100101011111110);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0xfe
	movx	@dptr,a
	mov	a,#0x4a
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0052
	lcall	_flash_write
;	peripherals\GoldSequence.c:137: flash_write(0x0054,0b1111001011110101);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0xf5
	movx	@dptr,a
	mov	a,#0xf2
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0054
	lcall	_flash_write
;	peripherals\GoldSequence.c:139: flash_write(0x0056,0b111101001001011);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x4b
	movx	@dptr,a
	mov	a,#0x7a
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0056
	lcall	_flash_write
;	peripherals\GoldSequence.c:141: flash_write(0x0058,0b0111110000001100);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x0c
	movx	@dptr,a
	mov	a,#0x7c
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0058
	lcall	_flash_write
;	peripherals\GoldSequence.c:143: flash_write(0x005A,0b110001000010001);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x11
	movx	@dptr,a
	mov	a,#0x62
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x005a
	lcall	_flash_write
;	peripherals\GoldSequence.c:146: flash_write(0x005C,0b0011101101110000);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x70
	movx	@dptr,a
	mov	a,#0x3b
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x005c
	lcall	_flash_write
;	peripherals\GoldSequence.c:148: flash_write(0x005E,0b010111000111100);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x3c
	movx	@dptr,a
	mov	a,#0x2e
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x005e
	lcall	_flash_write
;	peripherals\GoldSequence.c:151: flash_write(0x0060,0b1001100011001110);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0xce
	movx	@dptr,a
	mov	a,#0x98
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0060
	lcall	_flash_write
;	peripherals\GoldSequence.c:153: flash_write(0x0062,0b000100000101010);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x2a
	movx	@dptr,a
	mov	a,#0x08
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0062
	lcall	_flash_write
;	peripherals\GoldSequence.c:156: flash_write(0x0064,0b1100100100010001);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x11
	movx	@dptr,a
	mov	a,#0xc9
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0064
	lcall	_flash_write
;	peripherals\GoldSequence.c:158: flash_write(0x0066,0b001101100100001);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x21
	movx	@dptr,a
	mov	a,#0x1b
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0066
	lcall	_flash_write
;	peripherals\GoldSequence.c:161: flash_write(0x0068,0b0110000111111110);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0xfe
	movx	@dptr,a
	mov	a,#0x61
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0068
	lcall	_flash_write
;	peripherals\GoldSequence.c:163: flash_write(0x006A,0b101001010100100);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0xa4
	movx	@dptr,a
	rr	a
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x006a
	lcall	_flash_write
;	peripherals\GoldSequence.c:166: flash_write(0x006C,0b1011010110001001);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x89
	movx	@dptr,a
	mov	a,#0xb5
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x006c
	lcall	_flash_write
;	peripherals\GoldSequence.c:168: flash_write(0x006E,0b011011001100110);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x66
	movx	@dptr,a
	mov	a,#0x36
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x006e
	lcall	_flash_write
;	peripherals\GoldSequence.c:171: flash_write(0x0070,0b1101111110110010);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0xb2
	movx	@dptr,a
	mov	a,#0xdf
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0070
	lcall	_flash_write
;	peripherals\GoldSequence.c:173: flash_write(0x0072,0b100010000000111);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x07
	movx	@dptr,a
	mov	a,#0x44
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0072
	lcall	_flash_write
;	peripherals\GoldSequence.c:176: flash_write(0x0074,0b0110101010101111);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0xaf
	movx	@dptr,a
	mov	a,#0x6a
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0074
	lcall	_flash_write
;	peripherals\GoldSequence.c:178: flash_write(0x0076,0b011110100110111);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x37
	movx	@dptr,a
	mov	a,#0x3d
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0076
	lcall	_flash_write
;	peripherals\GoldSequence.c:181: flash_write(0x0078,0b0011000000100001);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x21
	movx	@dptr,a
	mov	a,#0x30
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0078
	lcall	_flash_write
;	peripherals\GoldSequence.c:183: flash_write(0x007A,0b100000110101111);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0xaf
	movx	@dptr,a
	mov	a,#0x41
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x007a
	lcall	_flash_write
;	peripherals\GoldSequence.c:186: flash_write(0x007C,0b0001110101100110);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0x66
	movx	@dptr,a
	mov	a,#0x1d
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x007c
	lcall	_flash_write
;	peripherals\GoldSequence.c:188: flash_write(0x007E,0b111111111100011);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0xe3
	movx	@dptr,a
	mov	a,#0x7f
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x007e
	lcall	_flash_write
;	peripherals\GoldSequence.c:191: flash_write(0x0080,0b0000101111000101);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0xc5
	movx	@dptr,a
	mov	a,#0x0b
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0080
	lcall	_flash_write
;	peripherals\GoldSequence.c:193: flash_write(0x0082,0b010000011000101);
	mov	dptr,#_flash_write_PARM_2
	mov	a,#0xc5
	movx	@dptr,a
	mov	a,#0x20
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0082
	lcall	_flash_write
;	peripherals\GoldSequence.c:195: flash_lock();
	ljmp	_flash_lock
;------------------------------------------------------------
;Allocation info for local variables in function 'GOLDSEQUENCE_Obtain'
;------------------------------------------------------------
;Rnd                       Allocated with name '_GOLDSEQUENCE_Obtain_Rnd_1_120'
;RetVal                    Allocated with name '_GOLDSEQUENCE_Obtain_RetVal_1_120'
;------------------------------------------------------------
;	peripherals\GoldSequence.c:212: __xdata uint32_t GOLDSEQUENCE_Obtain(void)
;	-----------------------------------------
;	 function GOLDSEQUENCE_Obtain
;	-----------------------------------------
_GOLDSEQUENCE_Obtain:
;	peripherals\GoldSequence.c:217: Rnd = random();
	lcall	_random
	mov	r6,dpl
	mov	r7,dph
;	peripherals\GoldSequence.c:218: flash_unlock();
	push	ar7
	push	ar6
	lcall	_flash_unlock
	pop	ar6
	pop	ar7
;	peripherals\GoldSequence.c:220: Rnd = ((Rnd % 33)*4);  // MOD operation is done so that the number is between 0 and 33, once a number between 0 and 32 has been obtained, its multiplied by 4 to match the memory address
	mov	dptr,#__moduint_PARM_2
	mov	a,#0x21
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
	mov	dpl,r6
	mov	dph,r7
	lcall	__moduint
	mov	r6,dpl
	mov	a,dph
	xch	a,r6
	add	a,acc
	xch	a,r6
	rlc	a
	xch	a,r6
	add	a,acc
	xch	a,r6
	rlc	a
	mov	r7,a
;	peripherals\GoldSequence.c:221: RetVal = ((uint32_t)flash_read(Rnd))<<16;
	mov	dpl,r6
	mov	dph,r7
	push	ar7
	push	ar6
	lcall	_flash_read
	mov	r4,dpl
	mov	r5,dph
	pop	ar6
	pop	ar7
	mov	ar2,r5
	mov	ar3,r4
;	peripherals\GoldSequence.c:222: RetVal |= (((uint32_t)flash_read(Rnd+2)) & 0x0000FFFF)<<1;
	clr	a
	mov	r5,a
	mov	r4,a
	mov	a,#0x02
	add	a,r6
	mov	r6,a
	clr	a
	addc	a,r7
	mov	r7,a
	mov	dpl,r6
	mov	dph,r7
	push	ar5
	push	ar4
	push	ar3
	push	ar2
	lcall	_flash_read
	mov	r6,dpl
	mov	r7,dph
	pop	ar2
	pop	ar3
	pop	ar4
	pop	ar5
	mov	ar0,r6
	mov	ar1,r7
	clr	a
	mov	r6,a
	mov	r7,a
	mov	a,r0
	add	a,r0
	mov	r0,a
	mov	a,r1
	rlc	a
	mov	r1,a
	mov	a,r6
	rlc	a
	mov	r6,a
	mov	a,r7
	rlc	a
	mov	r7,a
	mov	a,r0
	orl	ar4,a
	mov	a,r1
	orl	ar5,a
	mov	a,r6
	orl	ar3,a
	mov	a,r7
	orl	ar2,a
;	peripherals\GoldSequence.c:224: flash_lock();
	push	ar5
	push	ar4
	push	ar3
	push	ar2
	lcall	_flash_lock
	pop	ar2
	pop	ar3
	pop	ar4
	pop	ar5
;	peripherals\GoldSequence.c:225: return(RetVal);
	mov	dpl,r4
	mov	dph,r5
	mov	b,r3
	mov	a,r2
	ret
	.area CSEG    (CODE)
	.area CONST   (CODE)
	.area XINIT   (CODE)
	.area CABS    (ABS,CODE)
