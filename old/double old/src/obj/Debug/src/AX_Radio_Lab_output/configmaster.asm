;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.6.0 #9615 (MINGW64)
;--------------------------------------------------------
	.module configmaster
	.optsdcc -mmcs51 --model-large
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _lpxosc_settlingtime
	.globl _framing_counter_pos
	.globl _framing_insert_counter
	.globl _localaddr
	.globl _remoteaddr
	.globl _demo_packet
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
;--------------------------------------------------------
; special function bits
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
;--------------------------------------------------------
; overlayable register banks
;--------------------------------------------------------
	.area REG_BANK_0	(REL,OVR,DATA)
	.ds 8
;--------------------------------------------------------
; internal ram data
;--------------------------------------------------------
	.area DSEG    (DATA)
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;--------------------------------------------------------
; indirectly addressable internal ram data
;--------------------------------------------------------
	.area ISEG    (DATA)
;--------------------------------------------------------
; absolute internal ram data
;--------------------------------------------------------
	.area IABS    (ABS,DATA)
	.area IABS    (ABS,DATA)
;--------------------------------------------------------
; bit data
;--------------------------------------------------------
	.area BSEG    (BIT)
;--------------------------------------------------------
; paged external ram data
;--------------------------------------------------------
	.area PSEG    (PAG,XDATA)
;--------------------------------------------------------
; external ram data
;--------------------------------------------------------
	.area XSEG    (XDATA)
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area XABS    (ABS,XDATA)
;--------------------------------------------------------
; external initialized ram data
;--------------------------------------------------------
	.area XISEG   (XDATA)
_demo_packet::
	.ds 24
	.area HOME    (CODE)
	.area GSINIT0 (CODE)
	.area GSINIT1 (CODE)
	.area GSINIT2 (CODE)
	.area GSINIT3 (CODE)
	.area GSINIT4 (CODE)
	.area GSINIT5 (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area CSEG    (CODE)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME    (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area GSINIT  (CODE)
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME    (CODE)
	.area HOME    (CODE)
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CSEG    (CODE)
	.area CSEG    (CODE)
	.area CONST   (CODE)
_remoteaddr:
	.db #0x32	; 50	'2'
	.db #0x34	; 52	'4'
	.db #0x00	; 0
	.db #0x00	; 0
_localaddr:
	.db #0x32	; 50	'2'
	.db #0x34	; 52	'4'
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0xff	; 255
	.db #0xff	; 255
	.db #0x00	; 0
	.db #0x00	; 0
_framing_insert_counter:
	.db #0x00	; 0
_framing_counter_pos:
	.db #0x00	; 0
_lpxosc_settlingtime:
	.byte #0xb8,#0x0b	; 3000
	.area XINIT   (CODE)
__xinit__demo_packet:
	.db #0xa1	; 161
	.db #0xa5	; 165
	.db #0x35	; 53	'5'
	.db #0x05	; 5
	.db #0x99	; 153
	.db #0x13	; 19
	.db #0x14	; 20
	.db #0x11	; 17
	.db #0x22	; 34
	.db #0x33	; 51	'3'
	.db #0x44	; 68	'D'
	.db #0x55	; 85	'U'
	.db #0x9c	; 156
	.db #0xdf	; 223
	.db #0x13	; 19
	.db #0x78	; 120	'x'
	.db #0x01	; 1
	.db #0x23	; 35
	.db #0x45	; 69	'E'
	.db #0x67	; 103	'g'
	.db #0x89	; 137
	.db #0xab	; 171
	.db #0xdc	; 220
	.db #0xef	; 239
	.area CABS    (ABS,CODE)
