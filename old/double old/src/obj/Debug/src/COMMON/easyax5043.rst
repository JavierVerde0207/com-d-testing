                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.6.0 #9615 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module easyax5043
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _ax5043_init_registers_rx
                                     12 	.globl _ax5043_init_registers_tx
                                     13 	.globl _BEACON_decoding
                                     14 	.globl _dbglink_writenum16
                                     15 	.globl _dbglink_writestr
                                     16 	.globl _dbglink_tx
                                     17 	.globl _memset
                                     18 	.globl _memcpy
                                     19 	.globl _wtimer_remove_callback
                                     20 	.globl _wtimer_add_callback
                                     21 	.globl _wtimer_remove
                                     22 	.globl _wtimer1_addrelative
                                     23 	.globl _wtimer0_addrelative
                                     24 	.globl _wtimer0_addabsolute
                                     25 	.globl _wtimer0_curtime
                                     26 	.globl _wtimer_runcallbacks
                                     27 	.globl _wtimer_idle
                                     28 	.globl _ax5043_writefifo
                                     29 	.globl _ax5043_readfifo
                                     30 	.globl _ax5043_wakeup_deepsleep
                                     31 	.globl _ax5043_enter_deepsleep
                                     32 	.globl _ax5043_reset
                                     33 	.globl _radio_read24
                                     34 	.globl _radio_read16
                                     35 	.globl _pn9_buffer
                                     36 	.globl _pn9_advance_byte
                                     37 	.globl _pn9_advance_bits
                                     38 	.globl _axradio_framing_append_crc
                                     39 	.globl _axradio_framing_check_crc
                                     40 	.globl _ax5043_set_registers_rxcont_singleparamset
                                     41 	.globl _ax5043_set_registers_rxcont
                                     42 	.globl _ax5043_set_registers_rxwor
                                     43 	.globl _ax5043_set_registers_rx
                                     44 	.globl _ax5043_set_registers_tx
                                     45 	.globl _ax5043_set_registers
                                     46 	.globl _axradio_conv_freq_fromreg
                                     47 	.globl _axradio_statuschange
                                     48 	.globl _axradio_conv_timeinterval_totimer0
                                     49 	.globl _checksignedlimit32
                                     50 	.globl _checksignedlimit16
                                     51 	.globl _signedlimit16
                                     52 	.globl _signextend24
                                     53 	.globl _signextend20
                                     54 	.globl _signextend16
                                     55 	.globl _PORTC_7
                                     56 	.globl _PORTC_6
                                     57 	.globl _PORTC_5
                                     58 	.globl _PORTC_4
                                     59 	.globl _PORTC_3
                                     60 	.globl _PORTC_2
                                     61 	.globl _PORTC_1
                                     62 	.globl _PORTC_0
                                     63 	.globl _PORTB_7
                                     64 	.globl _PORTB_6
                                     65 	.globl _PORTB_5
                                     66 	.globl _PORTB_4
                                     67 	.globl _PORTB_3
                                     68 	.globl _PORTB_2
                                     69 	.globl _PORTB_1
                                     70 	.globl _PORTB_0
                                     71 	.globl _PORTA_7
                                     72 	.globl _PORTA_6
                                     73 	.globl _PORTA_5
                                     74 	.globl _PORTA_4
                                     75 	.globl _PORTA_3
                                     76 	.globl _PORTA_2
                                     77 	.globl _PORTA_1
                                     78 	.globl _PORTA_0
                                     79 	.globl _PINC_7
                                     80 	.globl _PINC_6
                                     81 	.globl _PINC_5
                                     82 	.globl _PINC_4
                                     83 	.globl _PINC_3
                                     84 	.globl _PINC_2
                                     85 	.globl _PINC_1
                                     86 	.globl _PINC_0
                                     87 	.globl _PINB_7
                                     88 	.globl _PINB_6
                                     89 	.globl _PINB_5
                                     90 	.globl _PINB_4
                                     91 	.globl _PINB_3
                                     92 	.globl _PINB_2
                                     93 	.globl _PINB_1
                                     94 	.globl _PINB_0
                                     95 	.globl _PINA_7
                                     96 	.globl _PINA_6
                                     97 	.globl _PINA_5
                                     98 	.globl _PINA_4
                                     99 	.globl _PINA_3
                                    100 	.globl _PINA_2
                                    101 	.globl _PINA_1
                                    102 	.globl _PINA_0
                                    103 	.globl _CY
                                    104 	.globl _AC
                                    105 	.globl _F0
                                    106 	.globl _RS1
                                    107 	.globl _RS0
                                    108 	.globl _OV
                                    109 	.globl _F1
                                    110 	.globl _P
                                    111 	.globl _IP_7
                                    112 	.globl _IP_6
                                    113 	.globl _IP_5
                                    114 	.globl _IP_4
                                    115 	.globl _IP_3
                                    116 	.globl _IP_2
                                    117 	.globl _IP_1
                                    118 	.globl _IP_0
                                    119 	.globl _EA
                                    120 	.globl _IE_7
                                    121 	.globl _IE_6
                                    122 	.globl _IE_5
                                    123 	.globl _IE_4
                                    124 	.globl _IE_3
                                    125 	.globl _IE_2
                                    126 	.globl _IE_1
                                    127 	.globl _IE_0
                                    128 	.globl _EIP_7
                                    129 	.globl _EIP_6
                                    130 	.globl _EIP_5
                                    131 	.globl _EIP_4
                                    132 	.globl _EIP_3
                                    133 	.globl _EIP_2
                                    134 	.globl _EIP_1
                                    135 	.globl _EIP_0
                                    136 	.globl _EIE_7
                                    137 	.globl _EIE_6
                                    138 	.globl _EIE_5
                                    139 	.globl _EIE_4
                                    140 	.globl _EIE_3
                                    141 	.globl _EIE_2
                                    142 	.globl _EIE_1
                                    143 	.globl _EIE_0
                                    144 	.globl _E2IP_7
                                    145 	.globl _E2IP_6
                                    146 	.globl _E2IP_5
                                    147 	.globl _E2IP_4
                                    148 	.globl _E2IP_3
                                    149 	.globl _E2IP_2
                                    150 	.globl _E2IP_1
                                    151 	.globl _E2IP_0
                                    152 	.globl _E2IE_7
                                    153 	.globl _E2IE_6
                                    154 	.globl _E2IE_5
                                    155 	.globl _E2IE_4
                                    156 	.globl _E2IE_3
                                    157 	.globl _E2IE_2
                                    158 	.globl _E2IE_1
                                    159 	.globl _E2IE_0
                                    160 	.globl _B_7
                                    161 	.globl _B_6
                                    162 	.globl _B_5
                                    163 	.globl _B_4
                                    164 	.globl _B_3
                                    165 	.globl _B_2
                                    166 	.globl _B_1
                                    167 	.globl _B_0
                                    168 	.globl _ACC_7
                                    169 	.globl _ACC_6
                                    170 	.globl _ACC_5
                                    171 	.globl _ACC_4
                                    172 	.globl _ACC_3
                                    173 	.globl _ACC_2
                                    174 	.globl _ACC_1
                                    175 	.globl _ACC_0
                                    176 	.globl _WTSTAT
                                    177 	.globl _WTIRQEN
                                    178 	.globl _WTEVTD
                                    179 	.globl _WTEVTD1
                                    180 	.globl _WTEVTD0
                                    181 	.globl _WTEVTC
                                    182 	.globl _WTEVTC1
                                    183 	.globl _WTEVTC0
                                    184 	.globl _WTEVTB
                                    185 	.globl _WTEVTB1
                                    186 	.globl _WTEVTB0
                                    187 	.globl _WTEVTA
                                    188 	.globl _WTEVTA1
                                    189 	.globl _WTEVTA0
                                    190 	.globl _WTCNTR1
                                    191 	.globl _WTCNTB
                                    192 	.globl _WTCNTB1
                                    193 	.globl _WTCNTB0
                                    194 	.globl _WTCNTA
                                    195 	.globl _WTCNTA1
                                    196 	.globl _WTCNTA0
                                    197 	.globl _WTCFGB
                                    198 	.globl _WTCFGA
                                    199 	.globl _WDTRESET
                                    200 	.globl _WDTCFG
                                    201 	.globl _U1STATUS
                                    202 	.globl _U1SHREG
                                    203 	.globl _U1MODE
                                    204 	.globl _U1CTRL
                                    205 	.globl _U0STATUS
                                    206 	.globl _U0SHREG
                                    207 	.globl _U0MODE
                                    208 	.globl _U0CTRL
                                    209 	.globl _T2STATUS
                                    210 	.globl _T2PERIOD
                                    211 	.globl _T2PERIOD1
                                    212 	.globl _T2PERIOD0
                                    213 	.globl _T2MODE
                                    214 	.globl _T2CNT
                                    215 	.globl _T2CNT1
                                    216 	.globl _T2CNT0
                                    217 	.globl _T2CLKSRC
                                    218 	.globl _T1STATUS
                                    219 	.globl _T1PERIOD
                                    220 	.globl _T1PERIOD1
                                    221 	.globl _T1PERIOD0
                                    222 	.globl _T1MODE
                                    223 	.globl _T1CNT
                                    224 	.globl _T1CNT1
                                    225 	.globl _T1CNT0
                                    226 	.globl _T1CLKSRC
                                    227 	.globl _T0STATUS
                                    228 	.globl _T0PERIOD
                                    229 	.globl _T0PERIOD1
                                    230 	.globl _T0PERIOD0
                                    231 	.globl _T0MODE
                                    232 	.globl _T0CNT
                                    233 	.globl _T0CNT1
                                    234 	.globl _T0CNT0
                                    235 	.globl _T0CLKSRC
                                    236 	.globl _SPSTATUS
                                    237 	.globl _SPSHREG
                                    238 	.globl _SPMODE
                                    239 	.globl _SPCLKSRC
                                    240 	.globl _RADIOSTAT
                                    241 	.globl _RADIOSTAT1
                                    242 	.globl _RADIOSTAT0
                                    243 	.globl _RADIODATA
                                    244 	.globl _RADIODATA3
                                    245 	.globl _RADIODATA2
                                    246 	.globl _RADIODATA1
                                    247 	.globl _RADIODATA0
                                    248 	.globl _RADIOADDR
                                    249 	.globl _RADIOADDR1
                                    250 	.globl _RADIOADDR0
                                    251 	.globl _RADIOACC
                                    252 	.globl _OC1STATUS
                                    253 	.globl _OC1PIN
                                    254 	.globl _OC1MODE
                                    255 	.globl _OC1COMP
                                    256 	.globl _OC1COMP1
                                    257 	.globl _OC1COMP0
                                    258 	.globl _OC0STATUS
                                    259 	.globl _OC0PIN
                                    260 	.globl _OC0MODE
                                    261 	.globl _OC0COMP
                                    262 	.globl _OC0COMP1
                                    263 	.globl _OC0COMP0
                                    264 	.globl _NVSTATUS
                                    265 	.globl _NVKEY
                                    266 	.globl _NVDATA
                                    267 	.globl _NVDATA1
                                    268 	.globl _NVDATA0
                                    269 	.globl _NVADDR
                                    270 	.globl _NVADDR1
                                    271 	.globl _NVADDR0
                                    272 	.globl _IC1STATUS
                                    273 	.globl _IC1MODE
                                    274 	.globl _IC1CAPT
                                    275 	.globl _IC1CAPT1
                                    276 	.globl _IC1CAPT0
                                    277 	.globl _IC0STATUS
                                    278 	.globl _IC0MODE
                                    279 	.globl _IC0CAPT
                                    280 	.globl _IC0CAPT1
                                    281 	.globl _IC0CAPT0
                                    282 	.globl _PORTR
                                    283 	.globl _PORTC
                                    284 	.globl _PORTB
                                    285 	.globl _PORTA
                                    286 	.globl _PINR
                                    287 	.globl _PINC
                                    288 	.globl _PINB
                                    289 	.globl _PINA
                                    290 	.globl _DIRR
                                    291 	.globl _DIRC
                                    292 	.globl _DIRB
                                    293 	.globl _DIRA
                                    294 	.globl _DBGLNKSTAT
                                    295 	.globl _DBGLNKBUF
                                    296 	.globl _CODECONFIG
                                    297 	.globl _CLKSTAT
                                    298 	.globl _CLKCON
                                    299 	.globl _ANALOGCOMP
                                    300 	.globl _ADCCONV
                                    301 	.globl _ADCCLKSRC
                                    302 	.globl _ADCCH3CONFIG
                                    303 	.globl _ADCCH2CONFIG
                                    304 	.globl _ADCCH1CONFIG
                                    305 	.globl _ADCCH0CONFIG
                                    306 	.globl __XPAGE
                                    307 	.globl _XPAGE
                                    308 	.globl _SP
                                    309 	.globl _PSW
                                    310 	.globl _PCON
                                    311 	.globl _IP
                                    312 	.globl _IE
                                    313 	.globl _EIP
                                    314 	.globl _EIE
                                    315 	.globl _E2IP
                                    316 	.globl _E2IE
                                    317 	.globl _DPS
                                    318 	.globl _DPTR1
                                    319 	.globl _DPTR0
                                    320 	.globl _DPL1
                                    321 	.globl _DPL
                                    322 	.globl _DPH1
                                    323 	.globl _DPH
                                    324 	.globl _B
                                    325 	.globl _ACC
                                    326 	.globl _f33_saved
                                    327 	.globl _f32_saved
                                    328 	.globl _f31_saved
                                    329 	.globl _f30_saved
                                    330 	.globl _axradio_transmit_PARM_3
                                    331 	.globl _axradio_transmit_PARM_2
                                    332 	.globl _axradio_timer
                                    333 	.globl _axradio_cb_transmitdata
                                    334 	.globl _axradio_cb_transmitend
                                    335 	.globl _axradio_cb_transmitstart
                                    336 	.globl _axradio_cb_channelstate
                                    337 	.globl _axradio_cb_receivesfd
                                    338 	.globl _axradio_cb_receive
                                    339 	.globl _axradio_rxbuffer
                                    340 	.globl _axradio_txbuffer
                                    341 	.globl _axradio_default_remoteaddr
                                    342 	.globl _axradio_localaddr
                                    343 	.globl _axradio_timeanchor
                                    344 	.globl _axradio_sync_periodcorr
                                    345 	.globl _axradio_sync_time
                                    346 	.globl _axradio_ack_seqnr
                                    347 	.globl _axradio_ack_count
                                    348 	.globl _axradio_curfreqoffset
                                    349 	.globl _axradio_curchannel
                                    350 	.globl _axradio_txbuffer_cnt
                                    351 	.globl _axradio_txbuffer_len
                                    352 	.globl _axradio_syncstate
                                    353 	.globl _aligned_alloc_PARM_2
                                    354 	.globl _AX5043_XTALAMPL
                                    355 	.globl _AX5043_XTALOSC
                                    356 	.globl _AX5043_MODCFGP
                                    357 	.globl _AX5043_POWCTRL1
                                    358 	.globl _AX5043_REF
                                    359 	.globl _AX5043_0xF44
                                    360 	.globl _AX5043_0xF35
                                    361 	.globl _AX5043_0xF34
                                    362 	.globl _AX5043_0xF33
                                    363 	.globl _AX5043_0xF32
                                    364 	.globl _AX5043_0xF31
                                    365 	.globl _AX5043_0xF30
                                    366 	.globl _AX5043_0xF26
                                    367 	.globl _AX5043_0xF23
                                    368 	.globl _AX5043_0xF22
                                    369 	.globl _AX5043_0xF21
                                    370 	.globl _AX5043_0xF1C
                                    371 	.globl _AX5043_0xF18
                                    372 	.globl _AX5043_0xF11
                                    373 	.globl _AX5043_0xF10
                                    374 	.globl _AX5043_0xF0C
                                    375 	.globl _AX5043_0xF00
                                    376 	.globl _AX5043_TIMEGAIN3NB
                                    377 	.globl _AX5043_TIMEGAIN2NB
                                    378 	.globl _AX5043_TIMEGAIN1NB
                                    379 	.globl _AX5043_TIMEGAIN0NB
                                    380 	.globl _AX5043_RXPARAMSETSNB
                                    381 	.globl _AX5043_RXPARAMCURSETNB
                                    382 	.globl _AX5043_PKTMAXLENNB
                                    383 	.globl _AX5043_PKTLENOFFSETNB
                                    384 	.globl _AX5043_PKTLENCFGNB
                                    385 	.globl _AX5043_PKTADDRMASK3NB
                                    386 	.globl _AX5043_PKTADDRMASK2NB
                                    387 	.globl _AX5043_PKTADDRMASK1NB
                                    388 	.globl _AX5043_PKTADDRMASK0NB
                                    389 	.globl _AX5043_PKTADDRCFGNB
                                    390 	.globl _AX5043_PKTADDR3NB
                                    391 	.globl _AX5043_PKTADDR2NB
                                    392 	.globl _AX5043_PKTADDR1NB
                                    393 	.globl _AX5043_PKTADDR0NB
                                    394 	.globl _AX5043_PHASEGAIN3NB
                                    395 	.globl _AX5043_PHASEGAIN2NB
                                    396 	.globl _AX5043_PHASEGAIN1NB
                                    397 	.globl _AX5043_PHASEGAIN0NB
                                    398 	.globl _AX5043_FREQUENCYLEAKNB
                                    399 	.globl _AX5043_FREQUENCYGAIND3NB
                                    400 	.globl _AX5043_FREQUENCYGAIND2NB
                                    401 	.globl _AX5043_FREQUENCYGAIND1NB
                                    402 	.globl _AX5043_FREQUENCYGAIND0NB
                                    403 	.globl _AX5043_FREQUENCYGAINC3NB
                                    404 	.globl _AX5043_FREQUENCYGAINC2NB
                                    405 	.globl _AX5043_FREQUENCYGAINC1NB
                                    406 	.globl _AX5043_FREQUENCYGAINC0NB
                                    407 	.globl _AX5043_FREQUENCYGAINB3NB
                                    408 	.globl _AX5043_FREQUENCYGAINB2NB
                                    409 	.globl _AX5043_FREQUENCYGAINB1NB
                                    410 	.globl _AX5043_FREQUENCYGAINB0NB
                                    411 	.globl _AX5043_FREQUENCYGAINA3NB
                                    412 	.globl _AX5043_FREQUENCYGAINA2NB
                                    413 	.globl _AX5043_FREQUENCYGAINA1NB
                                    414 	.globl _AX5043_FREQUENCYGAINA0NB
                                    415 	.globl _AX5043_FREQDEV13NB
                                    416 	.globl _AX5043_FREQDEV12NB
                                    417 	.globl _AX5043_FREQDEV11NB
                                    418 	.globl _AX5043_FREQDEV10NB
                                    419 	.globl _AX5043_FREQDEV03NB
                                    420 	.globl _AX5043_FREQDEV02NB
                                    421 	.globl _AX5043_FREQDEV01NB
                                    422 	.globl _AX5043_FREQDEV00NB
                                    423 	.globl _AX5043_FOURFSK3NB
                                    424 	.globl _AX5043_FOURFSK2NB
                                    425 	.globl _AX5043_FOURFSK1NB
                                    426 	.globl _AX5043_FOURFSK0NB
                                    427 	.globl _AX5043_DRGAIN3NB
                                    428 	.globl _AX5043_DRGAIN2NB
                                    429 	.globl _AX5043_DRGAIN1NB
                                    430 	.globl _AX5043_DRGAIN0NB
                                    431 	.globl _AX5043_BBOFFSRES3NB
                                    432 	.globl _AX5043_BBOFFSRES2NB
                                    433 	.globl _AX5043_BBOFFSRES1NB
                                    434 	.globl _AX5043_BBOFFSRES0NB
                                    435 	.globl _AX5043_AMPLITUDEGAIN3NB
                                    436 	.globl _AX5043_AMPLITUDEGAIN2NB
                                    437 	.globl _AX5043_AMPLITUDEGAIN1NB
                                    438 	.globl _AX5043_AMPLITUDEGAIN0NB
                                    439 	.globl _AX5043_AGCTARGET3NB
                                    440 	.globl _AX5043_AGCTARGET2NB
                                    441 	.globl _AX5043_AGCTARGET1NB
                                    442 	.globl _AX5043_AGCTARGET0NB
                                    443 	.globl _AX5043_AGCMINMAX3NB
                                    444 	.globl _AX5043_AGCMINMAX2NB
                                    445 	.globl _AX5043_AGCMINMAX1NB
                                    446 	.globl _AX5043_AGCMINMAX0NB
                                    447 	.globl _AX5043_AGCGAIN3NB
                                    448 	.globl _AX5043_AGCGAIN2NB
                                    449 	.globl _AX5043_AGCGAIN1NB
                                    450 	.globl _AX5043_AGCGAIN0NB
                                    451 	.globl _AX5043_AGCAHYST3NB
                                    452 	.globl _AX5043_AGCAHYST2NB
                                    453 	.globl _AX5043_AGCAHYST1NB
                                    454 	.globl _AX5043_AGCAHYST0NB
                                    455 	.globl _AX5043_0xF44NB
                                    456 	.globl _AX5043_0xF35NB
                                    457 	.globl _AX5043_0xF34NB
                                    458 	.globl _AX5043_0xF33NB
                                    459 	.globl _AX5043_0xF32NB
                                    460 	.globl _AX5043_0xF31NB
                                    461 	.globl _AX5043_0xF30NB
                                    462 	.globl _AX5043_0xF26NB
                                    463 	.globl _AX5043_0xF23NB
                                    464 	.globl _AX5043_0xF22NB
                                    465 	.globl _AX5043_0xF21NB
                                    466 	.globl _AX5043_0xF1CNB
                                    467 	.globl _AX5043_0xF18NB
                                    468 	.globl _AX5043_0xF0CNB
                                    469 	.globl _AX5043_0xF00NB
                                    470 	.globl _AX5043_XTALSTATUSNB
                                    471 	.globl _AX5043_XTALOSCNB
                                    472 	.globl _AX5043_XTALCAPNB
                                    473 	.globl _AX5043_XTALAMPLNB
                                    474 	.globl _AX5043_WAKEUPXOEARLYNB
                                    475 	.globl _AX5043_WAKEUPTIMER1NB
                                    476 	.globl _AX5043_WAKEUPTIMER0NB
                                    477 	.globl _AX5043_WAKEUPFREQ1NB
                                    478 	.globl _AX5043_WAKEUPFREQ0NB
                                    479 	.globl _AX5043_WAKEUP1NB
                                    480 	.globl _AX5043_WAKEUP0NB
                                    481 	.globl _AX5043_TXRATE2NB
                                    482 	.globl _AX5043_TXRATE1NB
                                    483 	.globl _AX5043_TXRATE0NB
                                    484 	.globl _AX5043_TXPWRCOEFFE1NB
                                    485 	.globl _AX5043_TXPWRCOEFFE0NB
                                    486 	.globl _AX5043_TXPWRCOEFFD1NB
                                    487 	.globl _AX5043_TXPWRCOEFFD0NB
                                    488 	.globl _AX5043_TXPWRCOEFFC1NB
                                    489 	.globl _AX5043_TXPWRCOEFFC0NB
                                    490 	.globl _AX5043_TXPWRCOEFFB1NB
                                    491 	.globl _AX5043_TXPWRCOEFFB0NB
                                    492 	.globl _AX5043_TXPWRCOEFFA1NB
                                    493 	.globl _AX5043_TXPWRCOEFFA0NB
                                    494 	.globl _AX5043_TRKRFFREQ2NB
                                    495 	.globl _AX5043_TRKRFFREQ1NB
                                    496 	.globl _AX5043_TRKRFFREQ0NB
                                    497 	.globl _AX5043_TRKPHASE1NB
                                    498 	.globl _AX5043_TRKPHASE0NB
                                    499 	.globl _AX5043_TRKFSKDEMOD1NB
                                    500 	.globl _AX5043_TRKFSKDEMOD0NB
                                    501 	.globl _AX5043_TRKFREQ1NB
                                    502 	.globl _AX5043_TRKFREQ0NB
                                    503 	.globl _AX5043_TRKDATARATE2NB
                                    504 	.globl _AX5043_TRKDATARATE1NB
                                    505 	.globl _AX5043_TRKDATARATE0NB
                                    506 	.globl _AX5043_TRKAMPLITUDE1NB
                                    507 	.globl _AX5043_TRKAMPLITUDE0NB
                                    508 	.globl _AX5043_TRKAFSKDEMOD1NB
                                    509 	.globl _AX5043_TRKAFSKDEMOD0NB
                                    510 	.globl _AX5043_TMGTXSETTLENB
                                    511 	.globl _AX5043_TMGTXBOOSTNB
                                    512 	.globl _AX5043_TMGRXSETTLENB
                                    513 	.globl _AX5043_TMGRXRSSINB
                                    514 	.globl _AX5043_TMGRXPREAMBLE3NB
                                    515 	.globl _AX5043_TMGRXPREAMBLE2NB
                                    516 	.globl _AX5043_TMGRXPREAMBLE1NB
                                    517 	.globl _AX5043_TMGRXOFFSACQNB
                                    518 	.globl _AX5043_TMGRXCOARSEAGCNB
                                    519 	.globl _AX5043_TMGRXBOOSTNB
                                    520 	.globl _AX5043_TMGRXAGCNB
                                    521 	.globl _AX5043_TIMER2NB
                                    522 	.globl _AX5043_TIMER1NB
                                    523 	.globl _AX5043_TIMER0NB
                                    524 	.globl _AX5043_SILICONREVISIONNB
                                    525 	.globl _AX5043_SCRATCHNB
                                    526 	.globl _AX5043_RXDATARATE2NB
                                    527 	.globl _AX5043_RXDATARATE1NB
                                    528 	.globl _AX5043_RXDATARATE0NB
                                    529 	.globl _AX5043_RSSIREFERENCENB
                                    530 	.globl _AX5043_RSSIABSTHRNB
                                    531 	.globl _AX5043_RSSINB
                                    532 	.globl _AX5043_REFNB
                                    533 	.globl _AX5043_RADIOSTATENB
                                    534 	.globl _AX5043_RADIOEVENTREQ1NB
                                    535 	.globl _AX5043_RADIOEVENTREQ0NB
                                    536 	.globl _AX5043_RADIOEVENTMASK1NB
                                    537 	.globl _AX5043_RADIOEVENTMASK0NB
                                    538 	.globl _AX5043_PWRMODENB
                                    539 	.globl _AX5043_PWRAMPNB
                                    540 	.globl _AX5043_POWSTICKYSTATNB
                                    541 	.globl _AX5043_POWSTATNB
                                    542 	.globl _AX5043_POWIRQMASKNB
                                    543 	.globl _AX5043_POWCTRL1NB
                                    544 	.globl _AX5043_PLLVCOIRNB
                                    545 	.globl _AX5043_PLLVCOINB
                                    546 	.globl _AX5043_PLLVCODIVNB
                                    547 	.globl _AX5043_PLLRNGCLKNB
                                    548 	.globl _AX5043_PLLRANGINGBNB
                                    549 	.globl _AX5043_PLLRANGINGANB
                                    550 	.globl _AX5043_PLLLOOPBOOSTNB
                                    551 	.globl _AX5043_PLLLOOPNB
                                    552 	.globl _AX5043_PLLLOCKDETNB
                                    553 	.globl _AX5043_PLLCPIBOOSTNB
                                    554 	.globl _AX5043_PLLCPINB
                                    555 	.globl _AX5043_PKTSTOREFLAGSNB
                                    556 	.globl _AX5043_PKTMISCFLAGSNB
                                    557 	.globl _AX5043_PKTCHUNKSIZENB
                                    558 	.globl _AX5043_PKTACCEPTFLAGSNB
                                    559 	.globl _AX5043_PINSTATENB
                                    560 	.globl _AX5043_PINFUNCSYSCLKNB
                                    561 	.globl _AX5043_PINFUNCPWRAMPNB
                                    562 	.globl _AX5043_PINFUNCIRQNB
                                    563 	.globl _AX5043_PINFUNCDCLKNB
                                    564 	.globl _AX5043_PINFUNCDATANB
                                    565 	.globl _AX5043_PINFUNCANTSELNB
                                    566 	.globl _AX5043_MODULATIONNB
                                    567 	.globl _AX5043_MODCFGPNB
                                    568 	.globl _AX5043_MODCFGFNB
                                    569 	.globl _AX5043_MODCFGANB
                                    570 	.globl _AX5043_MAXRFOFFSET2NB
                                    571 	.globl _AX5043_MAXRFOFFSET1NB
                                    572 	.globl _AX5043_MAXRFOFFSET0NB
                                    573 	.globl _AX5043_MAXDROFFSET2NB
                                    574 	.globl _AX5043_MAXDROFFSET1NB
                                    575 	.globl _AX5043_MAXDROFFSET0NB
                                    576 	.globl _AX5043_MATCH1PAT1NB
                                    577 	.globl _AX5043_MATCH1PAT0NB
                                    578 	.globl _AX5043_MATCH1MINNB
                                    579 	.globl _AX5043_MATCH1MAXNB
                                    580 	.globl _AX5043_MATCH1LENNB
                                    581 	.globl _AX5043_MATCH0PAT3NB
                                    582 	.globl _AX5043_MATCH0PAT2NB
                                    583 	.globl _AX5043_MATCH0PAT1NB
                                    584 	.globl _AX5043_MATCH0PAT0NB
                                    585 	.globl _AX5043_MATCH0MINNB
                                    586 	.globl _AX5043_MATCH0MAXNB
                                    587 	.globl _AX5043_MATCH0LENNB
                                    588 	.globl _AX5043_LPOSCSTATUSNB
                                    589 	.globl _AX5043_LPOSCREF1NB
                                    590 	.globl _AX5043_LPOSCREF0NB
                                    591 	.globl _AX5043_LPOSCPER1NB
                                    592 	.globl _AX5043_LPOSCPER0NB
                                    593 	.globl _AX5043_LPOSCKFILT1NB
                                    594 	.globl _AX5043_LPOSCKFILT0NB
                                    595 	.globl _AX5043_LPOSCFREQ1NB
                                    596 	.globl _AX5043_LPOSCFREQ0NB
                                    597 	.globl _AX5043_LPOSCCONFIGNB
                                    598 	.globl _AX5043_IRQREQUEST1NB
                                    599 	.globl _AX5043_IRQREQUEST0NB
                                    600 	.globl _AX5043_IRQMASK1NB
                                    601 	.globl _AX5043_IRQMASK0NB
                                    602 	.globl _AX5043_IRQINVERSION1NB
                                    603 	.globl _AX5043_IRQINVERSION0NB
                                    604 	.globl _AX5043_IFFREQ1NB
                                    605 	.globl _AX5043_IFFREQ0NB
                                    606 	.globl _AX5043_GPADCPERIODNB
                                    607 	.globl _AX5043_GPADCCTRLNB
                                    608 	.globl _AX5043_GPADC13VALUE1NB
                                    609 	.globl _AX5043_GPADC13VALUE0NB
                                    610 	.globl _AX5043_FSKDMIN1NB
                                    611 	.globl _AX5043_FSKDMIN0NB
                                    612 	.globl _AX5043_FSKDMAX1NB
                                    613 	.globl _AX5043_FSKDMAX0NB
                                    614 	.globl _AX5043_FSKDEV2NB
                                    615 	.globl _AX5043_FSKDEV1NB
                                    616 	.globl _AX5043_FSKDEV0NB
                                    617 	.globl _AX5043_FREQB3NB
                                    618 	.globl _AX5043_FREQB2NB
                                    619 	.globl _AX5043_FREQB1NB
                                    620 	.globl _AX5043_FREQB0NB
                                    621 	.globl _AX5043_FREQA3NB
                                    622 	.globl _AX5043_FREQA2NB
                                    623 	.globl _AX5043_FREQA1NB
                                    624 	.globl _AX5043_FREQA0NB
                                    625 	.globl _AX5043_FRAMINGNB
                                    626 	.globl _AX5043_FIFOTHRESH1NB
                                    627 	.globl _AX5043_FIFOTHRESH0NB
                                    628 	.globl _AX5043_FIFOSTATNB
                                    629 	.globl _AX5043_FIFOFREE1NB
                                    630 	.globl _AX5043_FIFOFREE0NB
                                    631 	.globl _AX5043_FIFODATANB
                                    632 	.globl _AX5043_FIFOCOUNT1NB
                                    633 	.globl _AX5043_FIFOCOUNT0NB
                                    634 	.globl _AX5043_FECSYNCNB
                                    635 	.globl _AX5043_FECSTATUSNB
                                    636 	.globl _AX5043_FECNB
                                    637 	.globl _AX5043_ENCODINGNB
                                    638 	.globl _AX5043_DIVERSITYNB
                                    639 	.globl _AX5043_DECIMATIONNB
                                    640 	.globl _AX5043_DACVALUE1NB
                                    641 	.globl _AX5043_DACVALUE0NB
                                    642 	.globl _AX5043_DACCONFIGNB
                                    643 	.globl _AX5043_CRCINIT3NB
                                    644 	.globl _AX5043_CRCINIT2NB
                                    645 	.globl _AX5043_CRCINIT1NB
                                    646 	.globl _AX5043_CRCINIT0NB
                                    647 	.globl _AX5043_BGNDRSSITHRNB
                                    648 	.globl _AX5043_BGNDRSSIGAINNB
                                    649 	.globl _AX5043_BGNDRSSINB
                                    650 	.globl _AX5043_BBTUNENB
                                    651 	.globl _AX5043_BBOFFSCAPNB
                                    652 	.globl _AX5043_AMPLFILTERNB
                                    653 	.globl _AX5043_AGCCOUNTERNB
                                    654 	.globl _AX5043_AFSKSPACE1NB
                                    655 	.globl _AX5043_AFSKSPACE0NB
                                    656 	.globl _AX5043_AFSKMARK1NB
                                    657 	.globl _AX5043_AFSKMARK0NB
                                    658 	.globl _AX5043_AFSKCTRLNB
                                    659 	.globl _AX5043_TIMEGAIN3
                                    660 	.globl _AX5043_TIMEGAIN2
                                    661 	.globl _AX5043_TIMEGAIN1
                                    662 	.globl _AX5043_TIMEGAIN0
                                    663 	.globl _AX5043_RXPARAMSETS
                                    664 	.globl _AX5043_RXPARAMCURSET
                                    665 	.globl _AX5043_PKTMAXLEN
                                    666 	.globl _AX5043_PKTLENOFFSET
                                    667 	.globl _AX5043_PKTLENCFG
                                    668 	.globl _AX5043_PKTADDRMASK3
                                    669 	.globl _AX5043_PKTADDRMASK2
                                    670 	.globl _AX5043_PKTADDRMASK1
                                    671 	.globl _AX5043_PKTADDRMASK0
                                    672 	.globl _AX5043_PKTADDRCFG
                                    673 	.globl _AX5043_PKTADDR3
                                    674 	.globl _AX5043_PKTADDR2
                                    675 	.globl _AX5043_PKTADDR1
                                    676 	.globl _AX5043_PKTADDR0
                                    677 	.globl _AX5043_PHASEGAIN3
                                    678 	.globl _AX5043_PHASEGAIN2
                                    679 	.globl _AX5043_PHASEGAIN1
                                    680 	.globl _AX5043_PHASEGAIN0
                                    681 	.globl _AX5043_FREQUENCYLEAK
                                    682 	.globl _AX5043_FREQUENCYGAIND3
                                    683 	.globl _AX5043_FREQUENCYGAIND2
                                    684 	.globl _AX5043_FREQUENCYGAIND1
                                    685 	.globl _AX5043_FREQUENCYGAIND0
                                    686 	.globl _AX5043_FREQUENCYGAINC3
                                    687 	.globl _AX5043_FREQUENCYGAINC2
                                    688 	.globl _AX5043_FREQUENCYGAINC1
                                    689 	.globl _AX5043_FREQUENCYGAINC0
                                    690 	.globl _AX5043_FREQUENCYGAINB3
                                    691 	.globl _AX5043_FREQUENCYGAINB2
                                    692 	.globl _AX5043_FREQUENCYGAINB1
                                    693 	.globl _AX5043_FREQUENCYGAINB0
                                    694 	.globl _AX5043_FREQUENCYGAINA3
                                    695 	.globl _AX5043_FREQUENCYGAINA2
                                    696 	.globl _AX5043_FREQUENCYGAINA1
                                    697 	.globl _AX5043_FREQUENCYGAINA0
                                    698 	.globl _AX5043_FREQDEV13
                                    699 	.globl _AX5043_FREQDEV12
                                    700 	.globl _AX5043_FREQDEV11
                                    701 	.globl _AX5043_FREQDEV10
                                    702 	.globl _AX5043_FREQDEV03
                                    703 	.globl _AX5043_FREQDEV02
                                    704 	.globl _AX5043_FREQDEV01
                                    705 	.globl _AX5043_FREQDEV00
                                    706 	.globl _AX5043_FOURFSK3
                                    707 	.globl _AX5043_FOURFSK2
                                    708 	.globl _AX5043_FOURFSK1
                                    709 	.globl _AX5043_FOURFSK0
                                    710 	.globl _AX5043_DRGAIN3
                                    711 	.globl _AX5043_DRGAIN2
                                    712 	.globl _AX5043_DRGAIN1
                                    713 	.globl _AX5043_DRGAIN0
                                    714 	.globl _AX5043_BBOFFSRES3
                                    715 	.globl _AX5043_BBOFFSRES2
                                    716 	.globl _AX5043_BBOFFSRES1
                                    717 	.globl _AX5043_BBOFFSRES0
                                    718 	.globl _AX5043_AMPLITUDEGAIN3
                                    719 	.globl _AX5043_AMPLITUDEGAIN2
                                    720 	.globl _AX5043_AMPLITUDEGAIN1
                                    721 	.globl _AX5043_AMPLITUDEGAIN0
                                    722 	.globl _AX5043_AGCTARGET3
                                    723 	.globl _AX5043_AGCTARGET2
                                    724 	.globl _AX5043_AGCTARGET1
                                    725 	.globl _AX5043_AGCTARGET0
                                    726 	.globl _AX5043_AGCMINMAX3
                                    727 	.globl _AX5043_AGCMINMAX2
                                    728 	.globl _AX5043_AGCMINMAX1
                                    729 	.globl _AX5043_AGCMINMAX0
                                    730 	.globl _AX5043_AGCGAIN3
                                    731 	.globl _AX5043_AGCGAIN2
                                    732 	.globl _AX5043_AGCGAIN1
                                    733 	.globl _AX5043_AGCGAIN0
                                    734 	.globl _AX5043_AGCAHYST3
                                    735 	.globl _AX5043_AGCAHYST2
                                    736 	.globl _AX5043_AGCAHYST1
                                    737 	.globl _AX5043_AGCAHYST0
                                    738 	.globl _AX5043_XTALSTATUS
                                    739 	.globl _AX5043_XTALCAP
                                    740 	.globl _AX5043_WAKEUPXOEARLY
                                    741 	.globl _AX5043_WAKEUPTIMER1
                                    742 	.globl _AX5043_WAKEUPTIMER0
                                    743 	.globl _AX5043_WAKEUPFREQ1
                                    744 	.globl _AX5043_WAKEUPFREQ0
                                    745 	.globl _AX5043_WAKEUP1
                                    746 	.globl _AX5043_WAKEUP0
                                    747 	.globl _AX5043_TXRATE2
                                    748 	.globl _AX5043_TXRATE1
                                    749 	.globl _AX5043_TXRATE0
                                    750 	.globl _AX5043_TXPWRCOEFFE1
                                    751 	.globl _AX5043_TXPWRCOEFFE0
                                    752 	.globl _AX5043_TXPWRCOEFFD1
                                    753 	.globl _AX5043_TXPWRCOEFFD0
                                    754 	.globl _AX5043_TXPWRCOEFFC1
                                    755 	.globl _AX5043_TXPWRCOEFFC0
                                    756 	.globl _AX5043_TXPWRCOEFFB1
                                    757 	.globl _AX5043_TXPWRCOEFFB0
                                    758 	.globl _AX5043_TXPWRCOEFFA1
                                    759 	.globl _AX5043_TXPWRCOEFFA0
                                    760 	.globl _AX5043_TRKRFFREQ2
                                    761 	.globl _AX5043_TRKRFFREQ1
                                    762 	.globl _AX5043_TRKRFFREQ0
                                    763 	.globl _AX5043_TRKPHASE1
                                    764 	.globl _AX5043_TRKPHASE0
                                    765 	.globl _AX5043_TRKFSKDEMOD1
                                    766 	.globl _AX5043_TRKFSKDEMOD0
                                    767 	.globl _AX5043_TRKFREQ1
                                    768 	.globl _AX5043_TRKFREQ0
                                    769 	.globl _AX5043_TRKDATARATE2
                                    770 	.globl _AX5043_TRKDATARATE1
                                    771 	.globl _AX5043_TRKDATARATE0
                                    772 	.globl _AX5043_TRKAMPLITUDE1
                                    773 	.globl _AX5043_TRKAMPLITUDE0
                                    774 	.globl _AX5043_TRKAFSKDEMOD1
                                    775 	.globl _AX5043_TRKAFSKDEMOD0
                                    776 	.globl _AX5043_TMGTXSETTLE
                                    777 	.globl _AX5043_TMGTXBOOST
                                    778 	.globl _AX5043_TMGRXSETTLE
                                    779 	.globl _AX5043_TMGRXRSSI
                                    780 	.globl _AX5043_TMGRXPREAMBLE3
                                    781 	.globl _AX5043_TMGRXPREAMBLE2
                                    782 	.globl _AX5043_TMGRXPREAMBLE1
                                    783 	.globl _AX5043_TMGRXOFFSACQ
                                    784 	.globl _AX5043_TMGRXCOARSEAGC
                                    785 	.globl _AX5043_TMGRXBOOST
                                    786 	.globl _AX5043_TMGRXAGC
                                    787 	.globl _AX5043_TIMER2
                                    788 	.globl _AX5043_TIMER1
                                    789 	.globl _AX5043_TIMER0
                                    790 	.globl _AX5043_SILICONREVISION
                                    791 	.globl _AX5043_SCRATCH
                                    792 	.globl _AX5043_RXDATARATE2
                                    793 	.globl _AX5043_RXDATARATE1
                                    794 	.globl _AX5043_RXDATARATE0
                                    795 	.globl _AX5043_RSSIREFERENCE
                                    796 	.globl _AX5043_RSSIABSTHR
                                    797 	.globl _AX5043_RSSI
                                    798 	.globl _AX5043_RADIOSTATE
                                    799 	.globl _AX5043_RADIOEVENTREQ1
                                    800 	.globl _AX5043_RADIOEVENTREQ0
                                    801 	.globl _AX5043_RADIOEVENTMASK1
                                    802 	.globl _AX5043_RADIOEVENTMASK0
                                    803 	.globl _AX5043_PWRMODE
                                    804 	.globl _AX5043_PWRAMP
                                    805 	.globl _AX5043_POWSTICKYSTAT
                                    806 	.globl _AX5043_POWSTAT
                                    807 	.globl _AX5043_POWIRQMASK
                                    808 	.globl _AX5043_PLLVCOIR
                                    809 	.globl _AX5043_PLLVCOI
                                    810 	.globl _AX5043_PLLVCODIV
                                    811 	.globl _AX5043_PLLRNGCLK
                                    812 	.globl _AX5043_PLLRANGINGB
                                    813 	.globl _AX5043_PLLRANGINGA
                                    814 	.globl _AX5043_PLLLOOPBOOST
                                    815 	.globl _AX5043_PLLLOOP
                                    816 	.globl _AX5043_PLLLOCKDET
                                    817 	.globl _AX5043_PLLCPIBOOST
                                    818 	.globl _AX5043_PLLCPI
                                    819 	.globl _AX5043_PKTSTOREFLAGS
                                    820 	.globl _AX5043_PKTMISCFLAGS
                                    821 	.globl _AX5043_PKTCHUNKSIZE
                                    822 	.globl _AX5043_PKTACCEPTFLAGS
                                    823 	.globl _AX5043_PINSTATE
                                    824 	.globl _AX5043_PINFUNCSYSCLK
                                    825 	.globl _AX5043_PINFUNCPWRAMP
                                    826 	.globl _AX5043_PINFUNCIRQ
                                    827 	.globl _AX5043_PINFUNCDCLK
                                    828 	.globl _AX5043_PINFUNCDATA
                                    829 	.globl _AX5043_PINFUNCANTSEL
                                    830 	.globl _AX5043_MODULATION
                                    831 	.globl _AX5043_MODCFGF
                                    832 	.globl _AX5043_MODCFGA
                                    833 	.globl _AX5043_MAXRFOFFSET2
                                    834 	.globl _AX5043_MAXRFOFFSET1
                                    835 	.globl _AX5043_MAXRFOFFSET0
                                    836 	.globl _AX5043_MAXDROFFSET2
                                    837 	.globl _AX5043_MAXDROFFSET1
                                    838 	.globl _AX5043_MAXDROFFSET0
                                    839 	.globl _AX5043_MATCH1PAT1
                                    840 	.globl _AX5043_MATCH1PAT0
                                    841 	.globl _AX5043_MATCH1MIN
                                    842 	.globl _AX5043_MATCH1MAX
                                    843 	.globl _AX5043_MATCH1LEN
                                    844 	.globl _AX5043_MATCH0PAT3
                                    845 	.globl _AX5043_MATCH0PAT2
                                    846 	.globl _AX5043_MATCH0PAT1
                                    847 	.globl _AX5043_MATCH0PAT0
                                    848 	.globl _AX5043_MATCH0MIN
                                    849 	.globl _AX5043_MATCH0MAX
                                    850 	.globl _AX5043_MATCH0LEN
                                    851 	.globl _AX5043_LPOSCSTATUS
                                    852 	.globl _AX5043_LPOSCREF1
                                    853 	.globl _AX5043_LPOSCREF0
                                    854 	.globl _AX5043_LPOSCPER1
                                    855 	.globl _AX5043_LPOSCPER0
                                    856 	.globl _AX5043_LPOSCKFILT1
                                    857 	.globl _AX5043_LPOSCKFILT0
                                    858 	.globl _AX5043_LPOSCFREQ1
                                    859 	.globl _AX5043_LPOSCFREQ0
                                    860 	.globl _AX5043_LPOSCCONFIG
                                    861 	.globl _AX5043_IRQREQUEST1
                                    862 	.globl _AX5043_IRQREQUEST0
                                    863 	.globl _AX5043_IRQMASK1
                                    864 	.globl _AX5043_IRQMASK0
                                    865 	.globl _AX5043_IRQINVERSION1
                                    866 	.globl _AX5043_IRQINVERSION0
                                    867 	.globl _AX5043_IFFREQ1
                                    868 	.globl _AX5043_IFFREQ0
                                    869 	.globl _AX5043_GPADCPERIOD
                                    870 	.globl _AX5043_GPADCCTRL
                                    871 	.globl _AX5043_GPADC13VALUE1
                                    872 	.globl _AX5043_GPADC13VALUE0
                                    873 	.globl _AX5043_FSKDMIN1
                                    874 	.globl _AX5043_FSKDMIN0
                                    875 	.globl _AX5043_FSKDMAX1
                                    876 	.globl _AX5043_FSKDMAX0
                                    877 	.globl _AX5043_FSKDEV2
                                    878 	.globl _AX5043_FSKDEV1
                                    879 	.globl _AX5043_FSKDEV0
                                    880 	.globl _AX5043_FREQB3
                                    881 	.globl _AX5043_FREQB2
                                    882 	.globl _AX5043_FREQB1
                                    883 	.globl _AX5043_FREQB0
                                    884 	.globl _AX5043_FREQA3
                                    885 	.globl _AX5043_FREQA2
                                    886 	.globl _AX5043_FREQA1
                                    887 	.globl _AX5043_FREQA0
                                    888 	.globl _AX5043_FRAMING
                                    889 	.globl _AX5043_FIFOTHRESH1
                                    890 	.globl _AX5043_FIFOTHRESH0
                                    891 	.globl _AX5043_FIFOSTAT
                                    892 	.globl _AX5043_FIFOFREE1
                                    893 	.globl _AX5043_FIFOFREE0
                                    894 	.globl _AX5043_FIFODATA
                                    895 	.globl _AX5043_FIFOCOUNT1
                                    896 	.globl _AX5043_FIFOCOUNT0
                                    897 	.globl _AX5043_FECSYNC
                                    898 	.globl _AX5043_FECSTATUS
                                    899 	.globl _AX5043_FEC
                                    900 	.globl _AX5043_ENCODING
                                    901 	.globl _AX5043_DIVERSITY
                                    902 	.globl _AX5043_DECIMATION
                                    903 	.globl _AX5043_DACVALUE1
                                    904 	.globl _AX5043_DACVALUE0
                                    905 	.globl _AX5043_DACCONFIG
                                    906 	.globl _AX5043_CRCINIT3
                                    907 	.globl _AX5043_CRCINIT2
                                    908 	.globl _AX5043_CRCINIT1
                                    909 	.globl _AX5043_CRCINIT0
                                    910 	.globl _AX5043_BGNDRSSITHR
                                    911 	.globl _AX5043_BGNDRSSIGAIN
                                    912 	.globl _AX5043_BGNDRSSI
                                    913 	.globl _AX5043_BBTUNE
                                    914 	.globl _AX5043_BBOFFSCAP
                                    915 	.globl _AX5043_AMPLFILTER
                                    916 	.globl _AX5043_AGCCOUNTER
                                    917 	.globl _AX5043_AFSKSPACE1
                                    918 	.globl _AX5043_AFSKSPACE0
                                    919 	.globl _AX5043_AFSKMARK1
                                    920 	.globl _AX5043_AFSKMARK0
                                    921 	.globl _AX5043_AFSKCTRL
                                    922 	.globl _XWTSTAT
                                    923 	.globl _XWTIRQEN
                                    924 	.globl _XWTEVTD
                                    925 	.globl _XWTEVTD1
                                    926 	.globl _XWTEVTD0
                                    927 	.globl _XWTEVTC
                                    928 	.globl _XWTEVTC1
                                    929 	.globl _XWTEVTC0
                                    930 	.globl _XWTEVTB
                                    931 	.globl _XWTEVTB1
                                    932 	.globl _XWTEVTB0
                                    933 	.globl _XWTEVTA
                                    934 	.globl _XWTEVTA1
                                    935 	.globl _XWTEVTA0
                                    936 	.globl _XWTCNTR1
                                    937 	.globl _XWTCNTB
                                    938 	.globl _XWTCNTB1
                                    939 	.globl _XWTCNTB0
                                    940 	.globl _XWTCNTA
                                    941 	.globl _XWTCNTA1
                                    942 	.globl _XWTCNTA0
                                    943 	.globl _XWTCFGB
                                    944 	.globl _XWTCFGA
                                    945 	.globl _XWDTRESET
                                    946 	.globl _XWDTCFG
                                    947 	.globl _XU1STATUS
                                    948 	.globl _XU1SHREG
                                    949 	.globl _XU1MODE
                                    950 	.globl _XU1CTRL
                                    951 	.globl _XU0STATUS
                                    952 	.globl _XU0SHREG
                                    953 	.globl _XU0MODE
                                    954 	.globl _XU0CTRL
                                    955 	.globl _XT2STATUS
                                    956 	.globl _XT2PERIOD
                                    957 	.globl _XT2PERIOD1
                                    958 	.globl _XT2PERIOD0
                                    959 	.globl _XT2MODE
                                    960 	.globl _XT2CNT
                                    961 	.globl _XT2CNT1
                                    962 	.globl _XT2CNT0
                                    963 	.globl _XT2CLKSRC
                                    964 	.globl _XT1STATUS
                                    965 	.globl _XT1PERIOD
                                    966 	.globl _XT1PERIOD1
                                    967 	.globl _XT1PERIOD0
                                    968 	.globl _XT1MODE
                                    969 	.globl _XT1CNT
                                    970 	.globl _XT1CNT1
                                    971 	.globl _XT1CNT0
                                    972 	.globl _XT1CLKSRC
                                    973 	.globl _XT0STATUS
                                    974 	.globl _XT0PERIOD
                                    975 	.globl _XT0PERIOD1
                                    976 	.globl _XT0PERIOD0
                                    977 	.globl _XT0MODE
                                    978 	.globl _XT0CNT
                                    979 	.globl _XT0CNT1
                                    980 	.globl _XT0CNT0
                                    981 	.globl _XT0CLKSRC
                                    982 	.globl _XSPSTATUS
                                    983 	.globl _XSPSHREG
                                    984 	.globl _XSPMODE
                                    985 	.globl _XSPCLKSRC
                                    986 	.globl _XRADIOSTAT
                                    987 	.globl _XRADIOSTAT1
                                    988 	.globl _XRADIOSTAT0
                                    989 	.globl _XRADIODATA3
                                    990 	.globl _XRADIODATA2
                                    991 	.globl _XRADIODATA1
                                    992 	.globl _XRADIODATA0
                                    993 	.globl _XRADIOADDR1
                                    994 	.globl _XRADIOADDR0
                                    995 	.globl _XRADIOACC
                                    996 	.globl _XOC1STATUS
                                    997 	.globl _XOC1PIN
                                    998 	.globl _XOC1MODE
                                    999 	.globl _XOC1COMP
                                   1000 	.globl _XOC1COMP1
                                   1001 	.globl _XOC1COMP0
                                   1002 	.globl _XOC0STATUS
                                   1003 	.globl _XOC0PIN
                                   1004 	.globl _XOC0MODE
                                   1005 	.globl _XOC0COMP
                                   1006 	.globl _XOC0COMP1
                                   1007 	.globl _XOC0COMP0
                                   1008 	.globl _XNVSTATUS
                                   1009 	.globl _XNVKEY
                                   1010 	.globl _XNVDATA
                                   1011 	.globl _XNVDATA1
                                   1012 	.globl _XNVDATA0
                                   1013 	.globl _XNVADDR
                                   1014 	.globl _XNVADDR1
                                   1015 	.globl _XNVADDR0
                                   1016 	.globl _XIC1STATUS
                                   1017 	.globl _XIC1MODE
                                   1018 	.globl _XIC1CAPT
                                   1019 	.globl _XIC1CAPT1
                                   1020 	.globl _XIC1CAPT0
                                   1021 	.globl _XIC0STATUS
                                   1022 	.globl _XIC0MODE
                                   1023 	.globl _XIC0CAPT
                                   1024 	.globl _XIC0CAPT1
                                   1025 	.globl _XIC0CAPT0
                                   1026 	.globl _XPORTR
                                   1027 	.globl _XPORTC
                                   1028 	.globl _XPORTB
                                   1029 	.globl _XPORTA
                                   1030 	.globl _XPINR
                                   1031 	.globl _XPINC
                                   1032 	.globl _XPINB
                                   1033 	.globl _XPINA
                                   1034 	.globl _XDIRR
                                   1035 	.globl _XDIRC
                                   1036 	.globl _XDIRB
                                   1037 	.globl _XDIRA
                                   1038 	.globl _XDBGLNKSTAT
                                   1039 	.globl _XDBGLNKBUF
                                   1040 	.globl _XCODECONFIG
                                   1041 	.globl _XCLKSTAT
                                   1042 	.globl _XCLKCON
                                   1043 	.globl _XANALOGCOMP
                                   1044 	.globl _XADCCONV
                                   1045 	.globl _XADCCLKSRC
                                   1046 	.globl _XADCCH3CONFIG
                                   1047 	.globl _XADCCH2CONFIG
                                   1048 	.globl _XADCCH1CONFIG
                                   1049 	.globl _XADCCH0CONFIG
                                   1050 	.globl _XPCON
                                   1051 	.globl _XIP
                                   1052 	.globl _XIE
                                   1053 	.globl _XDPTR1
                                   1054 	.globl _XDPTR0
                                   1055 	.globl _XTALREADY
                                   1056 	.globl _XTALOSC
                                   1057 	.globl _XTALAMPL
                                   1058 	.globl _SILICONREV
                                   1059 	.globl _SCRATCH3
                                   1060 	.globl _SCRATCH2
                                   1061 	.globl _SCRATCH1
                                   1062 	.globl _SCRATCH0
                                   1063 	.globl _RADIOMUX
                                   1064 	.globl _RADIOFSTATADDR
                                   1065 	.globl _RADIOFSTATADDR1
                                   1066 	.globl _RADIOFSTATADDR0
                                   1067 	.globl _RADIOFDATAADDR
                                   1068 	.globl _RADIOFDATAADDR1
                                   1069 	.globl _RADIOFDATAADDR0
                                   1070 	.globl _OSCRUN
                                   1071 	.globl _OSCREADY
                                   1072 	.globl _OSCFORCERUN
                                   1073 	.globl _OSCCALIB
                                   1074 	.globl _MISCCTRL
                                   1075 	.globl _LPXOSCGM
                                   1076 	.globl _LPOSCREF
                                   1077 	.globl _LPOSCREF1
                                   1078 	.globl _LPOSCREF0
                                   1079 	.globl _LPOSCPER
                                   1080 	.globl _LPOSCPER1
                                   1081 	.globl _LPOSCPER0
                                   1082 	.globl _LPOSCKFILT
                                   1083 	.globl _LPOSCKFILT1
                                   1084 	.globl _LPOSCKFILT0
                                   1085 	.globl _LPOSCFREQ
                                   1086 	.globl _LPOSCFREQ1
                                   1087 	.globl _LPOSCFREQ0
                                   1088 	.globl _LPOSCCONFIG
                                   1089 	.globl _PINSEL
                                   1090 	.globl _PINCHGC
                                   1091 	.globl _PINCHGB
                                   1092 	.globl _PINCHGA
                                   1093 	.globl _PALTRADIO
                                   1094 	.globl _PALTC
                                   1095 	.globl _PALTB
                                   1096 	.globl _PALTA
                                   1097 	.globl _INTCHGC
                                   1098 	.globl _INTCHGB
                                   1099 	.globl _INTCHGA
                                   1100 	.globl _EXTIRQ
                                   1101 	.globl _GPIOENABLE
                                   1102 	.globl _ANALOGA
                                   1103 	.globl _FRCOSCREF
                                   1104 	.globl _FRCOSCREF1
                                   1105 	.globl _FRCOSCREF0
                                   1106 	.globl _FRCOSCPER
                                   1107 	.globl _FRCOSCPER1
                                   1108 	.globl _FRCOSCPER0
                                   1109 	.globl _FRCOSCKFILT
                                   1110 	.globl _FRCOSCKFILT1
                                   1111 	.globl _FRCOSCKFILT0
                                   1112 	.globl _FRCOSCFREQ
                                   1113 	.globl _FRCOSCFREQ1
                                   1114 	.globl _FRCOSCFREQ0
                                   1115 	.globl _FRCOSCCTRL
                                   1116 	.globl _FRCOSCCONFIG
                                   1117 	.globl _DMA1CONFIG
                                   1118 	.globl _DMA1ADDR
                                   1119 	.globl _DMA1ADDR1
                                   1120 	.globl _DMA1ADDR0
                                   1121 	.globl _DMA0CONFIG
                                   1122 	.globl _DMA0ADDR
                                   1123 	.globl _DMA0ADDR1
                                   1124 	.globl _DMA0ADDR0
                                   1125 	.globl _ADCTUNE2
                                   1126 	.globl _ADCTUNE1
                                   1127 	.globl _ADCTUNE0
                                   1128 	.globl _ADCCH3VAL
                                   1129 	.globl _ADCCH3VAL1
                                   1130 	.globl _ADCCH3VAL0
                                   1131 	.globl _ADCCH2VAL
                                   1132 	.globl _ADCCH2VAL1
                                   1133 	.globl _ADCCH2VAL0
                                   1134 	.globl _ADCCH1VAL
                                   1135 	.globl _ADCCH1VAL1
                                   1136 	.globl _ADCCH1VAL0
                                   1137 	.globl _ADCCH0VAL
                                   1138 	.globl _ADCCH0VAL1
                                   1139 	.globl _ADCCH0VAL0
                                   1140 	.globl _axradio_trxstate
                                   1141 	.globl _axradio_mode
                                   1142 	.globl _axradio_conv_time_totimer0
                                   1143 	.globl _axradio_isr
                                   1144 	.globl _ax5043_receiver_on_continuous
                                   1145 	.globl _ax5043_receiver_on_wor
                                   1146 	.globl _ax5043_prepare_tx
                                   1147 	.globl _ax5043_off
                                   1148 	.globl _ax5043_off_xtal
                                   1149 	.globl _axradio_wait_for_xtal
                                   1150 	.globl _axradio_init
                                   1151 	.globl _axradio_cansleep
                                   1152 	.globl _axradio_set_mode
                                   1153 	.globl _axradio_get_mode
                                   1154 	.globl _axradio_set_channel
                                   1155 	.globl _axradio_get_channel
                                   1156 	.globl _axradio_get_pllrange
                                   1157 	.globl _axradio_get_pllvcoi
                                   1158 	.globl _axradio_set_freqoffset
                                   1159 	.globl _axradio_get_freqoffset
                                   1160 	.globl _axradio_set_local_address
                                   1161 	.globl _axradio_get_local_address
                                   1162 	.globl _axradio_set_default_remote_address
                                   1163 	.globl _axradio_get_default_remote_address
                                   1164 	.globl _axradio_transmit
                                   1165 	.globl _axradio_agc_freeze
                                   1166 	.globl _axradio_agc_thaw
                                   1167 ;--------------------------------------------------------
                                   1168 ; special function registers
                                   1169 ;--------------------------------------------------------
                                   1170 	.area RSEG    (ABS,DATA)
      000000                       1171 	.org 0x0000
                           0000E0  1172 _ACC	=	0x00e0
                           0000F0  1173 _B	=	0x00f0
                           000083  1174 _DPH	=	0x0083
                           000085  1175 _DPH1	=	0x0085
                           000082  1176 _DPL	=	0x0082
                           000084  1177 _DPL1	=	0x0084
                           008382  1178 _DPTR0	=	0x8382
                           008584  1179 _DPTR1	=	0x8584
                           000086  1180 _DPS	=	0x0086
                           0000A0  1181 _E2IE	=	0x00a0
                           0000C0  1182 _E2IP	=	0x00c0
                           000098  1183 _EIE	=	0x0098
                           0000B0  1184 _EIP	=	0x00b0
                           0000A8  1185 _IE	=	0x00a8
                           0000B8  1186 _IP	=	0x00b8
                           000087  1187 _PCON	=	0x0087
                           0000D0  1188 _PSW	=	0x00d0
                           000081  1189 _SP	=	0x0081
                           0000D9  1190 _XPAGE	=	0x00d9
                           0000D9  1191 __XPAGE	=	0x00d9
                           0000CA  1192 _ADCCH0CONFIG	=	0x00ca
                           0000CB  1193 _ADCCH1CONFIG	=	0x00cb
                           0000D2  1194 _ADCCH2CONFIG	=	0x00d2
                           0000D3  1195 _ADCCH3CONFIG	=	0x00d3
                           0000D1  1196 _ADCCLKSRC	=	0x00d1
                           0000C9  1197 _ADCCONV	=	0x00c9
                           0000E1  1198 _ANALOGCOMP	=	0x00e1
                           0000C6  1199 _CLKCON	=	0x00c6
                           0000C7  1200 _CLKSTAT	=	0x00c7
                           000097  1201 _CODECONFIG	=	0x0097
                           0000E3  1202 _DBGLNKBUF	=	0x00e3
                           0000E2  1203 _DBGLNKSTAT	=	0x00e2
                           000089  1204 _DIRA	=	0x0089
                           00008A  1205 _DIRB	=	0x008a
                           00008B  1206 _DIRC	=	0x008b
                           00008E  1207 _DIRR	=	0x008e
                           0000C8  1208 _PINA	=	0x00c8
                           0000E8  1209 _PINB	=	0x00e8
                           0000F8  1210 _PINC	=	0x00f8
                           00008D  1211 _PINR	=	0x008d
                           000080  1212 _PORTA	=	0x0080
                           000088  1213 _PORTB	=	0x0088
                           000090  1214 _PORTC	=	0x0090
                           00008C  1215 _PORTR	=	0x008c
                           0000CE  1216 _IC0CAPT0	=	0x00ce
                           0000CF  1217 _IC0CAPT1	=	0x00cf
                           00CFCE  1218 _IC0CAPT	=	0xcfce
                           0000CC  1219 _IC0MODE	=	0x00cc
                           0000CD  1220 _IC0STATUS	=	0x00cd
                           0000D6  1221 _IC1CAPT0	=	0x00d6
                           0000D7  1222 _IC1CAPT1	=	0x00d7
                           00D7D6  1223 _IC1CAPT	=	0xd7d6
                           0000D4  1224 _IC1MODE	=	0x00d4
                           0000D5  1225 _IC1STATUS	=	0x00d5
                           000092  1226 _NVADDR0	=	0x0092
                           000093  1227 _NVADDR1	=	0x0093
                           009392  1228 _NVADDR	=	0x9392
                           000094  1229 _NVDATA0	=	0x0094
                           000095  1230 _NVDATA1	=	0x0095
                           009594  1231 _NVDATA	=	0x9594
                           000096  1232 _NVKEY	=	0x0096
                           000091  1233 _NVSTATUS	=	0x0091
                           0000BC  1234 _OC0COMP0	=	0x00bc
                           0000BD  1235 _OC0COMP1	=	0x00bd
                           00BDBC  1236 _OC0COMP	=	0xbdbc
                           0000B9  1237 _OC0MODE	=	0x00b9
                           0000BA  1238 _OC0PIN	=	0x00ba
                           0000BB  1239 _OC0STATUS	=	0x00bb
                           0000C4  1240 _OC1COMP0	=	0x00c4
                           0000C5  1241 _OC1COMP1	=	0x00c5
                           00C5C4  1242 _OC1COMP	=	0xc5c4
                           0000C1  1243 _OC1MODE	=	0x00c1
                           0000C2  1244 _OC1PIN	=	0x00c2
                           0000C3  1245 _OC1STATUS	=	0x00c3
                           0000B1  1246 _RADIOACC	=	0x00b1
                           0000B3  1247 _RADIOADDR0	=	0x00b3
                           0000B2  1248 _RADIOADDR1	=	0x00b2
                           00B2B3  1249 _RADIOADDR	=	0xb2b3
                           0000B7  1250 _RADIODATA0	=	0x00b7
                           0000B6  1251 _RADIODATA1	=	0x00b6
                           0000B5  1252 _RADIODATA2	=	0x00b5
                           0000B4  1253 _RADIODATA3	=	0x00b4
                           B4B5B6B7  1254 _RADIODATA	=	0xb4b5b6b7
                           0000BE  1255 _RADIOSTAT0	=	0x00be
                           0000BF  1256 _RADIOSTAT1	=	0x00bf
                           00BFBE  1257 _RADIOSTAT	=	0xbfbe
                           0000DF  1258 _SPCLKSRC	=	0x00df
                           0000DC  1259 _SPMODE	=	0x00dc
                           0000DE  1260 _SPSHREG	=	0x00de
                           0000DD  1261 _SPSTATUS	=	0x00dd
                           00009A  1262 _T0CLKSRC	=	0x009a
                           00009C  1263 _T0CNT0	=	0x009c
                           00009D  1264 _T0CNT1	=	0x009d
                           009D9C  1265 _T0CNT	=	0x9d9c
                           000099  1266 _T0MODE	=	0x0099
                           00009E  1267 _T0PERIOD0	=	0x009e
                           00009F  1268 _T0PERIOD1	=	0x009f
                           009F9E  1269 _T0PERIOD	=	0x9f9e
                           00009B  1270 _T0STATUS	=	0x009b
                           0000A2  1271 _T1CLKSRC	=	0x00a2
                           0000A4  1272 _T1CNT0	=	0x00a4
                           0000A5  1273 _T1CNT1	=	0x00a5
                           00A5A4  1274 _T1CNT	=	0xa5a4
                           0000A1  1275 _T1MODE	=	0x00a1
                           0000A6  1276 _T1PERIOD0	=	0x00a6
                           0000A7  1277 _T1PERIOD1	=	0x00a7
                           00A7A6  1278 _T1PERIOD	=	0xa7a6
                           0000A3  1279 _T1STATUS	=	0x00a3
                           0000AA  1280 _T2CLKSRC	=	0x00aa
                           0000AC  1281 _T2CNT0	=	0x00ac
                           0000AD  1282 _T2CNT1	=	0x00ad
                           00ADAC  1283 _T2CNT	=	0xadac
                           0000A9  1284 _T2MODE	=	0x00a9
                           0000AE  1285 _T2PERIOD0	=	0x00ae
                           0000AF  1286 _T2PERIOD1	=	0x00af
                           00AFAE  1287 _T2PERIOD	=	0xafae
                           0000AB  1288 _T2STATUS	=	0x00ab
                           0000E4  1289 _U0CTRL	=	0x00e4
                           0000E7  1290 _U0MODE	=	0x00e7
                           0000E6  1291 _U0SHREG	=	0x00e6
                           0000E5  1292 _U0STATUS	=	0x00e5
                           0000EC  1293 _U1CTRL	=	0x00ec
                           0000EF  1294 _U1MODE	=	0x00ef
                           0000EE  1295 _U1SHREG	=	0x00ee
                           0000ED  1296 _U1STATUS	=	0x00ed
                           0000DA  1297 _WDTCFG	=	0x00da
                           0000DB  1298 _WDTRESET	=	0x00db
                           0000F1  1299 _WTCFGA	=	0x00f1
                           0000F9  1300 _WTCFGB	=	0x00f9
                           0000F2  1301 _WTCNTA0	=	0x00f2
                           0000F3  1302 _WTCNTA1	=	0x00f3
                           00F3F2  1303 _WTCNTA	=	0xf3f2
                           0000FA  1304 _WTCNTB0	=	0x00fa
                           0000FB  1305 _WTCNTB1	=	0x00fb
                           00FBFA  1306 _WTCNTB	=	0xfbfa
                           0000EB  1307 _WTCNTR1	=	0x00eb
                           0000F4  1308 _WTEVTA0	=	0x00f4
                           0000F5  1309 _WTEVTA1	=	0x00f5
                           00F5F4  1310 _WTEVTA	=	0xf5f4
                           0000F6  1311 _WTEVTB0	=	0x00f6
                           0000F7  1312 _WTEVTB1	=	0x00f7
                           00F7F6  1313 _WTEVTB	=	0xf7f6
                           0000FC  1314 _WTEVTC0	=	0x00fc
                           0000FD  1315 _WTEVTC1	=	0x00fd
                           00FDFC  1316 _WTEVTC	=	0xfdfc
                           0000FE  1317 _WTEVTD0	=	0x00fe
                           0000FF  1318 _WTEVTD1	=	0x00ff
                           00FFFE  1319 _WTEVTD	=	0xfffe
                           0000E9  1320 _WTIRQEN	=	0x00e9
                           0000EA  1321 _WTSTAT	=	0x00ea
                                   1322 ;--------------------------------------------------------
                                   1323 ; special function bits
                                   1324 ;--------------------------------------------------------
                                   1325 	.area RSEG    (ABS,DATA)
      000000                       1326 	.org 0x0000
                           0000E0  1327 _ACC_0	=	0x00e0
                           0000E1  1328 _ACC_1	=	0x00e1
                           0000E2  1329 _ACC_2	=	0x00e2
                           0000E3  1330 _ACC_3	=	0x00e3
                           0000E4  1331 _ACC_4	=	0x00e4
                           0000E5  1332 _ACC_5	=	0x00e5
                           0000E6  1333 _ACC_6	=	0x00e6
                           0000E7  1334 _ACC_7	=	0x00e7
                           0000F0  1335 _B_0	=	0x00f0
                           0000F1  1336 _B_1	=	0x00f1
                           0000F2  1337 _B_2	=	0x00f2
                           0000F3  1338 _B_3	=	0x00f3
                           0000F4  1339 _B_4	=	0x00f4
                           0000F5  1340 _B_5	=	0x00f5
                           0000F6  1341 _B_6	=	0x00f6
                           0000F7  1342 _B_7	=	0x00f7
                           0000A0  1343 _E2IE_0	=	0x00a0
                           0000A1  1344 _E2IE_1	=	0x00a1
                           0000A2  1345 _E2IE_2	=	0x00a2
                           0000A3  1346 _E2IE_3	=	0x00a3
                           0000A4  1347 _E2IE_4	=	0x00a4
                           0000A5  1348 _E2IE_5	=	0x00a5
                           0000A6  1349 _E2IE_6	=	0x00a6
                           0000A7  1350 _E2IE_7	=	0x00a7
                           0000C0  1351 _E2IP_0	=	0x00c0
                           0000C1  1352 _E2IP_1	=	0x00c1
                           0000C2  1353 _E2IP_2	=	0x00c2
                           0000C3  1354 _E2IP_3	=	0x00c3
                           0000C4  1355 _E2IP_4	=	0x00c4
                           0000C5  1356 _E2IP_5	=	0x00c5
                           0000C6  1357 _E2IP_6	=	0x00c6
                           0000C7  1358 _E2IP_7	=	0x00c7
                           000098  1359 _EIE_0	=	0x0098
                           000099  1360 _EIE_1	=	0x0099
                           00009A  1361 _EIE_2	=	0x009a
                           00009B  1362 _EIE_3	=	0x009b
                           00009C  1363 _EIE_4	=	0x009c
                           00009D  1364 _EIE_5	=	0x009d
                           00009E  1365 _EIE_6	=	0x009e
                           00009F  1366 _EIE_7	=	0x009f
                           0000B0  1367 _EIP_0	=	0x00b0
                           0000B1  1368 _EIP_1	=	0x00b1
                           0000B2  1369 _EIP_2	=	0x00b2
                           0000B3  1370 _EIP_3	=	0x00b3
                           0000B4  1371 _EIP_4	=	0x00b4
                           0000B5  1372 _EIP_5	=	0x00b5
                           0000B6  1373 _EIP_6	=	0x00b6
                           0000B7  1374 _EIP_7	=	0x00b7
                           0000A8  1375 _IE_0	=	0x00a8
                           0000A9  1376 _IE_1	=	0x00a9
                           0000AA  1377 _IE_2	=	0x00aa
                           0000AB  1378 _IE_3	=	0x00ab
                           0000AC  1379 _IE_4	=	0x00ac
                           0000AD  1380 _IE_5	=	0x00ad
                           0000AE  1381 _IE_6	=	0x00ae
                           0000AF  1382 _IE_7	=	0x00af
                           0000AF  1383 _EA	=	0x00af
                           0000B8  1384 _IP_0	=	0x00b8
                           0000B9  1385 _IP_1	=	0x00b9
                           0000BA  1386 _IP_2	=	0x00ba
                           0000BB  1387 _IP_3	=	0x00bb
                           0000BC  1388 _IP_4	=	0x00bc
                           0000BD  1389 _IP_5	=	0x00bd
                           0000BE  1390 _IP_6	=	0x00be
                           0000BF  1391 _IP_7	=	0x00bf
                           0000D0  1392 _P	=	0x00d0
                           0000D1  1393 _F1	=	0x00d1
                           0000D2  1394 _OV	=	0x00d2
                           0000D3  1395 _RS0	=	0x00d3
                           0000D4  1396 _RS1	=	0x00d4
                           0000D5  1397 _F0	=	0x00d5
                           0000D6  1398 _AC	=	0x00d6
                           0000D7  1399 _CY	=	0x00d7
                           0000C8  1400 _PINA_0	=	0x00c8
                           0000C9  1401 _PINA_1	=	0x00c9
                           0000CA  1402 _PINA_2	=	0x00ca
                           0000CB  1403 _PINA_3	=	0x00cb
                           0000CC  1404 _PINA_4	=	0x00cc
                           0000CD  1405 _PINA_5	=	0x00cd
                           0000CE  1406 _PINA_6	=	0x00ce
                           0000CF  1407 _PINA_7	=	0x00cf
                           0000E8  1408 _PINB_0	=	0x00e8
                           0000E9  1409 _PINB_1	=	0x00e9
                           0000EA  1410 _PINB_2	=	0x00ea
                           0000EB  1411 _PINB_3	=	0x00eb
                           0000EC  1412 _PINB_4	=	0x00ec
                           0000ED  1413 _PINB_5	=	0x00ed
                           0000EE  1414 _PINB_6	=	0x00ee
                           0000EF  1415 _PINB_7	=	0x00ef
                           0000F8  1416 _PINC_0	=	0x00f8
                           0000F9  1417 _PINC_1	=	0x00f9
                           0000FA  1418 _PINC_2	=	0x00fa
                           0000FB  1419 _PINC_3	=	0x00fb
                           0000FC  1420 _PINC_4	=	0x00fc
                           0000FD  1421 _PINC_5	=	0x00fd
                           0000FE  1422 _PINC_6	=	0x00fe
                           0000FF  1423 _PINC_7	=	0x00ff
                           000080  1424 _PORTA_0	=	0x0080
                           000081  1425 _PORTA_1	=	0x0081
                           000082  1426 _PORTA_2	=	0x0082
                           000083  1427 _PORTA_3	=	0x0083
                           000084  1428 _PORTA_4	=	0x0084
                           000085  1429 _PORTA_5	=	0x0085
                           000086  1430 _PORTA_6	=	0x0086
                           000087  1431 _PORTA_7	=	0x0087
                           000088  1432 _PORTB_0	=	0x0088
                           000089  1433 _PORTB_1	=	0x0089
                           00008A  1434 _PORTB_2	=	0x008a
                           00008B  1435 _PORTB_3	=	0x008b
                           00008C  1436 _PORTB_4	=	0x008c
                           00008D  1437 _PORTB_5	=	0x008d
                           00008E  1438 _PORTB_6	=	0x008e
                           00008F  1439 _PORTB_7	=	0x008f
                           000090  1440 _PORTC_0	=	0x0090
                           000091  1441 _PORTC_1	=	0x0091
                           000092  1442 _PORTC_2	=	0x0092
                           000093  1443 _PORTC_3	=	0x0093
                           000094  1444 _PORTC_4	=	0x0094
                           000095  1445 _PORTC_5	=	0x0095
                           000096  1446 _PORTC_6	=	0x0096
                           000097  1447 _PORTC_7	=	0x0097
                                   1448 ;--------------------------------------------------------
                                   1449 ; overlayable register banks
                                   1450 ;--------------------------------------------------------
                                   1451 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                       1452 	.ds 8
                                   1453 ;--------------------------------------------------------
                                   1454 ; overlayable bit register bank
                                   1455 ;--------------------------------------------------------
                                   1456 	.area BIT_BANK	(REL,OVR,DATA)
      000021                       1457 bits:
      000021                       1458 	.ds 1
                           008000  1459 	b0 = bits[0]
                           008100  1460 	b1 = bits[1]
                           008200  1461 	b2 = bits[2]
                           008300  1462 	b3 = bits[3]
                           008400  1463 	b4 = bits[4]
                           008500  1464 	b5 = bits[5]
                           008600  1465 	b6 = bits[6]
                           008700  1466 	b7 = bits[7]
                                   1467 ;--------------------------------------------------------
                                   1468 ; internal ram data
                                   1469 ;--------------------------------------------------------
                                   1470 	.area DSEG    (DATA)
      00000B                       1471 _axradio_mode::
      00000B                       1472 	.ds 1
      00000C                       1473 _axradio_trxstate::
      00000C                       1474 	.ds 1
                                   1475 ;--------------------------------------------------------
                                   1476 ; overlayable items in internal ram 
                                   1477 ;--------------------------------------------------------
                                   1478 	.area	OSEG    (OVR,DATA)
                                   1479 	.area	OSEG    (OVR,DATA)
      000043                       1480 _axradio_set_channel_rng_1_426:
      000043                       1481 	.ds 1
                                   1482 ;--------------------------------------------------------
                                   1483 ; indirectly addressable internal ram data
                                   1484 ;--------------------------------------------------------
                                   1485 	.area ISEG    (DATA)
                                   1486 ;--------------------------------------------------------
                                   1487 ; absolute internal ram data
                                   1488 ;--------------------------------------------------------
                                   1489 	.area IABS    (ABS,DATA)
                                   1490 	.area IABS    (ABS,DATA)
                                   1491 ;--------------------------------------------------------
                                   1492 ; bit data
                                   1493 ;--------------------------------------------------------
                                   1494 	.area BSEG    (BIT)
      000001                       1495 _axradio_timer_callback_sloc0_1_0:
      000001                       1496 	.ds 1
      000002                       1497 _axradio_init_sloc0_1_0:
      000002                       1498 	.ds 1
                                   1499 ;--------------------------------------------------------
                                   1500 ; paged external ram data
                                   1501 ;--------------------------------------------------------
                                   1502 	.area PSEG    (PAG,XDATA)
                                   1503 ;--------------------------------------------------------
                                   1504 ; external ram data
                                   1505 ;--------------------------------------------------------
                                   1506 	.area XSEG    (XDATA)
                           007020  1507 _ADCCH0VAL0	=	0x7020
                           007021  1508 _ADCCH0VAL1	=	0x7021
                           007020  1509 _ADCCH0VAL	=	0x7020
                           007022  1510 _ADCCH1VAL0	=	0x7022
                           007023  1511 _ADCCH1VAL1	=	0x7023
                           007022  1512 _ADCCH1VAL	=	0x7022
                           007024  1513 _ADCCH2VAL0	=	0x7024
                           007025  1514 _ADCCH2VAL1	=	0x7025
                           007024  1515 _ADCCH2VAL	=	0x7024
                           007026  1516 _ADCCH3VAL0	=	0x7026
                           007027  1517 _ADCCH3VAL1	=	0x7027
                           007026  1518 _ADCCH3VAL	=	0x7026
                           007028  1519 _ADCTUNE0	=	0x7028
                           007029  1520 _ADCTUNE1	=	0x7029
                           00702A  1521 _ADCTUNE2	=	0x702a
                           007010  1522 _DMA0ADDR0	=	0x7010
                           007011  1523 _DMA0ADDR1	=	0x7011
                           007010  1524 _DMA0ADDR	=	0x7010
                           007014  1525 _DMA0CONFIG	=	0x7014
                           007012  1526 _DMA1ADDR0	=	0x7012
                           007013  1527 _DMA1ADDR1	=	0x7013
                           007012  1528 _DMA1ADDR	=	0x7012
                           007015  1529 _DMA1CONFIG	=	0x7015
                           007070  1530 _FRCOSCCONFIG	=	0x7070
                           007071  1531 _FRCOSCCTRL	=	0x7071
                           007076  1532 _FRCOSCFREQ0	=	0x7076
                           007077  1533 _FRCOSCFREQ1	=	0x7077
                           007076  1534 _FRCOSCFREQ	=	0x7076
                           007072  1535 _FRCOSCKFILT0	=	0x7072
                           007073  1536 _FRCOSCKFILT1	=	0x7073
                           007072  1537 _FRCOSCKFILT	=	0x7072
                           007078  1538 _FRCOSCPER0	=	0x7078
                           007079  1539 _FRCOSCPER1	=	0x7079
                           007078  1540 _FRCOSCPER	=	0x7078
                           007074  1541 _FRCOSCREF0	=	0x7074
                           007075  1542 _FRCOSCREF1	=	0x7075
                           007074  1543 _FRCOSCREF	=	0x7074
                           007007  1544 _ANALOGA	=	0x7007
                           00700C  1545 _GPIOENABLE	=	0x700c
                           007003  1546 _EXTIRQ	=	0x7003
                           007000  1547 _INTCHGA	=	0x7000
                           007001  1548 _INTCHGB	=	0x7001
                           007002  1549 _INTCHGC	=	0x7002
                           007008  1550 _PALTA	=	0x7008
                           007009  1551 _PALTB	=	0x7009
                           00700A  1552 _PALTC	=	0x700a
                           007046  1553 _PALTRADIO	=	0x7046
                           007004  1554 _PINCHGA	=	0x7004
                           007005  1555 _PINCHGB	=	0x7005
                           007006  1556 _PINCHGC	=	0x7006
                           00700B  1557 _PINSEL	=	0x700b
                           007060  1558 _LPOSCCONFIG	=	0x7060
                           007066  1559 _LPOSCFREQ0	=	0x7066
                           007067  1560 _LPOSCFREQ1	=	0x7067
                           007066  1561 _LPOSCFREQ	=	0x7066
                           007062  1562 _LPOSCKFILT0	=	0x7062
                           007063  1563 _LPOSCKFILT1	=	0x7063
                           007062  1564 _LPOSCKFILT	=	0x7062
                           007068  1565 _LPOSCPER0	=	0x7068
                           007069  1566 _LPOSCPER1	=	0x7069
                           007068  1567 _LPOSCPER	=	0x7068
                           007064  1568 _LPOSCREF0	=	0x7064
                           007065  1569 _LPOSCREF1	=	0x7065
                           007064  1570 _LPOSCREF	=	0x7064
                           007054  1571 _LPXOSCGM	=	0x7054
                           007F01  1572 _MISCCTRL	=	0x7f01
                           007053  1573 _OSCCALIB	=	0x7053
                           007050  1574 _OSCFORCERUN	=	0x7050
                           007052  1575 _OSCREADY	=	0x7052
                           007051  1576 _OSCRUN	=	0x7051
                           007040  1577 _RADIOFDATAADDR0	=	0x7040
                           007041  1578 _RADIOFDATAADDR1	=	0x7041
                           007040  1579 _RADIOFDATAADDR	=	0x7040
                           007042  1580 _RADIOFSTATADDR0	=	0x7042
                           007043  1581 _RADIOFSTATADDR1	=	0x7043
                           007042  1582 _RADIOFSTATADDR	=	0x7042
                           007044  1583 _RADIOMUX	=	0x7044
                           007084  1584 _SCRATCH0	=	0x7084
                           007085  1585 _SCRATCH1	=	0x7085
                           007086  1586 _SCRATCH2	=	0x7086
                           007087  1587 _SCRATCH3	=	0x7087
                           007F00  1588 _SILICONREV	=	0x7f00
                           007F19  1589 _XTALAMPL	=	0x7f19
                           007F18  1590 _XTALOSC	=	0x7f18
                           007F1A  1591 _XTALREADY	=	0x7f1a
                           003F82  1592 _XDPTR0	=	0x3f82
                           003F84  1593 _XDPTR1	=	0x3f84
                           003FA8  1594 _XIE	=	0x3fa8
                           003FB8  1595 _XIP	=	0x3fb8
                           003F87  1596 _XPCON	=	0x3f87
                           003FCA  1597 _XADCCH0CONFIG	=	0x3fca
                           003FCB  1598 _XADCCH1CONFIG	=	0x3fcb
                           003FD2  1599 _XADCCH2CONFIG	=	0x3fd2
                           003FD3  1600 _XADCCH3CONFIG	=	0x3fd3
                           003FD1  1601 _XADCCLKSRC	=	0x3fd1
                           003FC9  1602 _XADCCONV	=	0x3fc9
                           003FE1  1603 _XANALOGCOMP	=	0x3fe1
                           003FC6  1604 _XCLKCON	=	0x3fc6
                           003FC7  1605 _XCLKSTAT	=	0x3fc7
                           003F97  1606 _XCODECONFIG	=	0x3f97
                           003FE3  1607 _XDBGLNKBUF	=	0x3fe3
                           003FE2  1608 _XDBGLNKSTAT	=	0x3fe2
                           003F89  1609 _XDIRA	=	0x3f89
                           003F8A  1610 _XDIRB	=	0x3f8a
                           003F8B  1611 _XDIRC	=	0x3f8b
                           003F8E  1612 _XDIRR	=	0x3f8e
                           003FC8  1613 _XPINA	=	0x3fc8
                           003FE8  1614 _XPINB	=	0x3fe8
                           003FF8  1615 _XPINC	=	0x3ff8
                           003F8D  1616 _XPINR	=	0x3f8d
                           003F80  1617 _XPORTA	=	0x3f80
                           003F88  1618 _XPORTB	=	0x3f88
                           003F90  1619 _XPORTC	=	0x3f90
                           003F8C  1620 _XPORTR	=	0x3f8c
                           003FCE  1621 _XIC0CAPT0	=	0x3fce
                           003FCF  1622 _XIC0CAPT1	=	0x3fcf
                           003FCE  1623 _XIC0CAPT	=	0x3fce
                           003FCC  1624 _XIC0MODE	=	0x3fcc
                           003FCD  1625 _XIC0STATUS	=	0x3fcd
                           003FD6  1626 _XIC1CAPT0	=	0x3fd6
                           003FD7  1627 _XIC1CAPT1	=	0x3fd7
                           003FD6  1628 _XIC1CAPT	=	0x3fd6
                           003FD4  1629 _XIC1MODE	=	0x3fd4
                           003FD5  1630 _XIC1STATUS	=	0x3fd5
                           003F92  1631 _XNVADDR0	=	0x3f92
                           003F93  1632 _XNVADDR1	=	0x3f93
                           003F92  1633 _XNVADDR	=	0x3f92
                           003F94  1634 _XNVDATA0	=	0x3f94
                           003F95  1635 _XNVDATA1	=	0x3f95
                           003F94  1636 _XNVDATA	=	0x3f94
                           003F96  1637 _XNVKEY	=	0x3f96
                           003F91  1638 _XNVSTATUS	=	0x3f91
                           003FBC  1639 _XOC0COMP0	=	0x3fbc
                           003FBD  1640 _XOC0COMP1	=	0x3fbd
                           003FBC  1641 _XOC0COMP	=	0x3fbc
                           003FB9  1642 _XOC0MODE	=	0x3fb9
                           003FBA  1643 _XOC0PIN	=	0x3fba
                           003FBB  1644 _XOC0STATUS	=	0x3fbb
                           003FC4  1645 _XOC1COMP0	=	0x3fc4
                           003FC5  1646 _XOC1COMP1	=	0x3fc5
                           003FC4  1647 _XOC1COMP	=	0x3fc4
                           003FC1  1648 _XOC1MODE	=	0x3fc1
                           003FC2  1649 _XOC1PIN	=	0x3fc2
                           003FC3  1650 _XOC1STATUS	=	0x3fc3
                           003FB1  1651 _XRADIOACC	=	0x3fb1
                           003FB3  1652 _XRADIOADDR0	=	0x3fb3
                           003FB2  1653 _XRADIOADDR1	=	0x3fb2
                           003FB7  1654 _XRADIODATA0	=	0x3fb7
                           003FB6  1655 _XRADIODATA1	=	0x3fb6
                           003FB5  1656 _XRADIODATA2	=	0x3fb5
                           003FB4  1657 _XRADIODATA3	=	0x3fb4
                           003FBE  1658 _XRADIOSTAT0	=	0x3fbe
                           003FBF  1659 _XRADIOSTAT1	=	0x3fbf
                           003FBE  1660 _XRADIOSTAT	=	0x3fbe
                           003FDF  1661 _XSPCLKSRC	=	0x3fdf
                           003FDC  1662 _XSPMODE	=	0x3fdc
                           003FDE  1663 _XSPSHREG	=	0x3fde
                           003FDD  1664 _XSPSTATUS	=	0x3fdd
                           003F9A  1665 _XT0CLKSRC	=	0x3f9a
                           003F9C  1666 _XT0CNT0	=	0x3f9c
                           003F9D  1667 _XT0CNT1	=	0x3f9d
                           003F9C  1668 _XT0CNT	=	0x3f9c
                           003F99  1669 _XT0MODE	=	0x3f99
                           003F9E  1670 _XT0PERIOD0	=	0x3f9e
                           003F9F  1671 _XT0PERIOD1	=	0x3f9f
                           003F9E  1672 _XT0PERIOD	=	0x3f9e
                           003F9B  1673 _XT0STATUS	=	0x3f9b
                           003FA2  1674 _XT1CLKSRC	=	0x3fa2
                           003FA4  1675 _XT1CNT0	=	0x3fa4
                           003FA5  1676 _XT1CNT1	=	0x3fa5
                           003FA4  1677 _XT1CNT	=	0x3fa4
                           003FA1  1678 _XT1MODE	=	0x3fa1
                           003FA6  1679 _XT1PERIOD0	=	0x3fa6
                           003FA7  1680 _XT1PERIOD1	=	0x3fa7
                           003FA6  1681 _XT1PERIOD	=	0x3fa6
                           003FA3  1682 _XT1STATUS	=	0x3fa3
                           003FAA  1683 _XT2CLKSRC	=	0x3faa
                           003FAC  1684 _XT2CNT0	=	0x3fac
                           003FAD  1685 _XT2CNT1	=	0x3fad
                           003FAC  1686 _XT2CNT	=	0x3fac
                           003FA9  1687 _XT2MODE	=	0x3fa9
                           003FAE  1688 _XT2PERIOD0	=	0x3fae
                           003FAF  1689 _XT2PERIOD1	=	0x3faf
                           003FAE  1690 _XT2PERIOD	=	0x3fae
                           003FAB  1691 _XT2STATUS	=	0x3fab
                           003FE4  1692 _XU0CTRL	=	0x3fe4
                           003FE7  1693 _XU0MODE	=	0x3fe7
                           003FE6  1694 _XU0SHREG	=	0x3fe6
                           003FE5  1695 _XU0STATUS	=	0x3fe5
                           003FEC  1696 _XU1CTRL	=	0x3fec
                           003FEF  1697 _XU1MODE	=	0x3fef
                           003FEE  1698 _XU1SHREG	=	0x3fee
                           003FED  1699 _XU1STATUS	=	0x3fed
                           003FDA  1700 _XWDTCFG	=	0x3fda
                           003FDB  1701 _XWDTRESET	=	0x3fdb
                           003FF1  1702 _XWTCFGA	=	0x3ff1
                           003FF9  1703 _XWTCFGB	=	0x3ff9
                           003FF2  1704 _XWTCNTA0	=	0x3ff2
                           003FF3  1705 _XWTCNTA1	=	0x3ff3
                           003FF2  1706 _XWTCNTA	=	0x3ff2
                           003FFA  1707 _XWTCNTB0	=	0x3ffa
                           003FFB  1708 _XWTCNTB1	=	0x3ffb
                           003FFA  1709 _XWTCNTB	=	0x3ffa
                           003FEB  1710 _XWTCNTR1	=	0x3feb
                           003FF4  1711 _XWTEVTA0	=	0x3ff4
                           003FF5  1712 _XWTEVTA1	=	0x3ff5
                           003FF4  1713 _XWTEVTA	=	0x3ff4
                           003FF6  1714 _XWTEVTB0	=	0x3ff6
                           003FF7  1715 _XWTEVTB1	=	0x3ff7
                           003FF6  1716 _XWTEVTB	=	0x3ff6
                           003FFC  1717 _XWTEVTC0	=	0x3ffc
                           003FFD  1718 _XWTEVTC1	=	0x3ffd
                           003FFC  1719 _XWTEVTC	=	0x3ffc
                           003FFE  1720 _XWTEVTD0	=	0x3ffe
                           003FFF  1721 _XWTEVTD1	=	0x3fff
                           003FFE  1722 _XWTEVTD	=	0x3ffe
                           003FE9  1723 _XWTIRQEN	=	0x3fe9
                           003FEA  1724 _XWTSTAT	=	0x3fea
                           004114  1725 _AX5043_AFSKCTRL	=	0x4114
                           004113  1726 _AX5043_AFSKMARK0	=	0x4113
                           004112  1727 _AX5043_AFSKMARK1	=	0x4112
                           004111  1728 _AX5043_AFSKSPACE0	=	0x4111
                           004110  1729 _AX5043_AFSKSPACE1	=	0x4110
                           004043  1730 _AX5043_AGCCOUNTER	=	0x4043
                           004115  1731 _AX5043_AMPLFILTER	=	0x4115
                           004189  1732 _AX5043_BBOFFSCAP	=	0x4189
                           004188  1733 _AX5043_BBTUNE	=	0x4188
                           004041  1734 _AX5043_BGNDRSSI	=	0x4041
                           00422E  1735 _AX5043_BGNDRSSIGAIN	=	0x422e
                           00422F  1736 _AX5043_BGNDRSSITHR	=	0x422f
                           004017  1737 _AX5043_CRCINIT0	=	0x4017
                           004016  1738 _AX5043_CRCINIT1	=	0x4016
                           004015  1739 _AX5043_CRCINIT2	=	0x4015
                           004014  1740 _AX5043_CRCINIT3	=	0x4014
                           004332  1741 _AX5043_DACCONFIG	=	0x4332
                           004331  1742 _AX5043_DACVALUE0	=	0x4331
                           004330  1743 _AX5043_DACVALUE1	=	0x4330
                           004102  1744 _AX5043_DECIMATION	=	0x4102
                           004042  1745 _AX5043_DIVERSITY	=	0x4042
                           004011  1746 _AX5043_ENCODING	=	0x4011
                           004018  1747 _AX5043_FEC	=	0x4018
                           00401A  1748 _AX5043_FECSTATUS	=	0x401a
                           004019  1749 _AX5043_FECSYNC	=	0x4019
                           00402B  1750 _AX5043_FIFOCOUNT0	=	0x402b
                           00402A  1751 _AX5043_FIFOCOUNT1	=	0x402a
                           004029  1752 _AX5043_FIFODATA	=	0x4029
                           00402D  1753 _AX5043_FIFOFREE0	=	0x402d
                           00402C  1754 _AX5043_FIFOFREE1	=	0x402c
                           004028  1755 _AX5043_FIFOSTAT	=	0x4028
                           00402F  1756 _AX5043_FIFOTHRESH0	=	0x402f
                           00402E  1757 _AX5043_FIFOTHRESH1	=	0x402e
                           004012  1758 _AX5043_FRAMING	=	0x4012
                           004037  1759 _AX5043_FREQA0	=	0x4037
                           004036  1760 _AX5043_FREQA1	=	0x4036
                           004035  1761 _AX5043_FREQA2	=	0x4035
                           004034  1762 _AX5043_FREQA3	=	0x4034
                           00403F  1763 _AX5043_FREQB0	=	0x403f
                           00403E  1764 _AX5043_FREQB1	=	0x403e
                           00403D  1765 _AX5043_FREQB2	=	0x403d
                           00403C  1766 _AX5043_FREQB3	=	0x403c
                           004163  1767 _AX5043_FSKDEV0	=	0x4163
                           004162  1768 _AX5043_FSKDEV1	=	0x4162
                           004161  1769 _AX5043_FSKDEV2	=	0x4161
                           00410D  1770 _AX5043_FSKDMAX0	=	0x410d
                           00410C  1771 _AX5043_FSKDMAX1	=	0x410c
                           00410F  1772 _AX5043_FSKDMIN0	=	0x410f
                           00410E  1773 _AX5043_FSKDMIN1	=	0x410e
                           004309  1774 _AX5043_GPADC13VALUE0	=	0x4309
                           004308  1775 _AX5043_GPADC13VALUE1	=	0x4308
                           004300  1776 _AX5043_GPADCCTRL	=	0x4300
                           004301  1777 _AX5043_GPADCPERIOD	=	0x4301
                           004101  1778 _AX5043_IFFREQ0	=	0x4101
                           004100  1779 _AX5043_IFFREQ1	=	0x4100
                           00400B  1780 _AX5043_IRQINVERSION0	=	0x400b
                           00400A  1781 _AX5043_IRQINVERSION1	=	0x400a
                           004007  1782 _AX5043_IRQMASK0	=	0x4007
                           004006  1783 _AX5043_IRQMASK1	=	0x4006
                           00400D  1784 _AX5043_IRQREQUEST0	=	0x400d
                           00400C  1785 _AX5043_IRQREQUEST1	=	0x400c
                           004310  1786 _AX5043_LPOSCCONFIG	=	0x4310
                           004317  1787 _AX5043_LPOSCFREQ0	=	0x4317
                           004316  1788 _AX5043_LPOSCFREQ1	=	0x4316
                           004313  1789 _AX5043_LPOSCKFILT0	=	0x4313
                           004312  1790 _AX5043_LPOSCKFILT1	=	0x4312
                           004319  1791 _AX5043_LPOSCPER0	=	0x4319
                           004318  1792 _AX5043_LPOSCPER1	=	0x4318
                           004315  1793 _AX5043_LPOSCREF0	=	0x4315
                           004314  1794 _AX5043_LPOSCREF1	=	0x4314
                           004311  1795 _AX5043_LPOSCSTATUS	=	0x4311
                           004214  1796 _AX5043_MATCH0LEN	=	0x4214
                           004216  1797 _AX5043_MATCH0MAX	=	0x4216
                           004215  1798 _AX5043_MATCH0MIN	=	0x4215
                           004213  1799 _AX5043_MATCH0PAT0	=	0x4213
                           004212  1800 _AX5043_MATCH0PAT1	=	0x4212
                           004211  1801 _AX5043_MATCH0PAT2	=	0x4211
                           004210  1802 _AX5043_MATCH0PAT3	=	0x4210
                           00421C  1803 _AX5043_MATCH1LEN	=	0x421c
                           00421E  1804 _AX5043_MATCH1MAX	=	0x421e
                           00421D  1805 _AX5043_MATCH1MIN	=	0x421d
                           004219  1806 _AX5043_MATCH1PAT0	=	0x4219
                           004218  1807 _AX5043_MATCH1PAT1	=	0x4218
                           004108  1808 _AX5043_MAXDROFFSET0	=	0x4108
                           004107  1809 _AX5043_MAXDROFFSET1	=	0x4107
                           004106  1810 _AX5043_MAXDROFFSET2	=	0x4106
                           00410B  1811 _AX5043_MAXRFOFFSET0	=	0x410b
                           00410A  1812 _AX5043_MAXRFOFFSET1	=	0x410a
                           004109  1813 _AX5043_MAXRFOFFSET2	=	0x4109
                           004164  1814 _AX5043_MODCFGA	=	0x4164
                           004160  1815 _AX5043_MODCFGF	=	0x4160
                           004010  1816 _AX5043_MODULATION	=	0x4010
                           004025  1817 _AX5043_PINFUNCANTSEL	=	0x4025
                           004023  1818 _AX5043_PINFUNCDATA	=	0x4023
                           004022  1819 _AX5043_PINFUNCDCLK	=	0x4022
                           004024  1820 _AX5043_PINFUNCIRQ	=	0x4024
                           004026  1821 _AX5043_PINFUNCPWRAMP	=	0x4026
                           004021  1822 _AX5043_PINFUNCSYSCLK	=	0x4021
                           004020  1823 _AX5043_PINSTATE	=	0x4020
                           004233  1824 _AX5043_PKTACCEPTFLAGS	=	0x4233
                           004230  1825 _AX5043_PKTCHUNKSIZE	=	0x4230
                           004231  1826 _AX5043_PKTMISCFLAGS	=	0x4231
                           004232  1827 _AX5043_PKTSTOREFLAGS	=	0x4232
                           004031  1828 _AX5043_PLLCPI	=	0x4031
                           004039  1829 _AX5043_PLLCPIBOOST	=	0x4039
                           004182  1830 _AX5043_PLLLOCKDET	=	0x4182
                           004030  1831 _AX5043_PLLLOOP	=	0x4030
                           004038  1832 _AX5043_PLLLOOPBOOST	=	0x4038
                           004033  1833 _AX5043_PLLRANGINGA	=	0x4033
                           00403B  1834 _AX5043_PLLRANGINGB	=	0x403b
                           004183  1835 _AX5043_PLLRNGCLK	=	0x4183
                           004032  1836 _AX5043_PLLVCODIV	=	0x4032
                           004180  1837 _AX5043_PLLVCOI	=	0x4180
                           004181  1838 _AX5043_PLLVCOIR	=	0x4181
                           004005  1839 _AX5043_POWIRQMASK	=	0x4005
                           004003  1840 _AX5043_POWSTAT	=	0x4003
                           004004  1841 _AX5043_POWSTICKYSTAT	=	0x4004
                           004027  1842 _AX5043_PWRAMP	=	0x4027
                           004002  1843 _AX5043_PWRMODE	=	0x4002
                           004009  1844 _AX5043_RADIOEVENTMASK0	=	0x4009
                           004008  1845 _AX5043_RADIOEVENTMASK1	=	0x4008
                           00400F  1846 _AX5043_RADIOEVENTREQ0	=	0x400f
                           00400E  1847 _AX5043_RADIOEVENTREQ1	=	0x400e
                           00401C  1848 _AX5043_RADIOSTATE	=	0x401c
                           004040  1849 _AX5043_RSSI	=	0x4040
                           00422D  1850 _AX5043_RSSIABSTHR	=	0x422d
                           00422C  1851 _AX5043_RSSIREFERENCE	=	0x422c
                           004105  1852 _AX5043_RXDATARATE0	=	0x4105
                           004104  1853 _AX5043_RXDATARATE1	=	0x4104
                           004103  1854 _AX5043_RXDATARATE2	=	0x4103
                           004001  1855 _AX5043_SCRATCH	=	0x4001
                           004000  1856 _AX5043_SILICONREVISION	=	0x4000
                           00405B  1857 _AX5043_TIMER0	=	0x405b
                           00405A  1858 _AX5043_TIMER1	=	0x405a
                           004059  1859 _AX5043_TIMER2	=	0x4059
                           004227  1860 _AX5043_TMGRXAGC	=	0x4227
                           004223  1861 _AX5043_TMGRXBOOST	=	0x4223
                           004226  1862 _AX5043_TMGRXCOARSEAGC	=	0x4226
                           004225  1863 _AX5043_TMGRXOFFSACQ	=	0x4225
                           004229  1864 _AX5043_TMGRXPREAMBLE1	=	0x4229
                           00422A  1865 _AX5043_TMGRXPREAMBLE2	=	0x422a
                           00422B  1866 _AX5043_TMGRXPREAMBLE3	=	0x422b
                           004228  1867 _AX5043_TMGRXRSSI	=	0x4228
                           004224  1868 _AX5043_TMGRXSETTLE	=	0x4224
                           004220  1869 _AX5043_TMGTXBOOST	=	0x4220
                           004221  1870 _AX5043_TMGTXSETTLE	=	0x4221
                           004055  1871 _AX5043_TRKAFSKDEMOD0	=	0x4055
                           004054  1872 _AX5043_TRKAFSKDEMOD1	=	0x4054
                           004049  1873 _AX5043_TRKAMPLITUDE0	=	0x4049
                           004048  1874 _AX5043_TRKAMPLITUDE1	=	0x4048
                           004047  1875 _AX5043_TRKDATARATE0	=	0x4047
                           004046  1876 _AX5043_TRKDATARATE1	=	0x4046
                           004045  1877 _AX5043_TRKDATARATE2	=	0x4045
                           004051  1878 _AX5043_TRKFREQ0	=	0x4051
                           004050  1879 _AX5043_TRKFREQ1	=	0x4050
                           004053  1880 _AX5043_TRKFSKDEMOD0	=	0x4053
                           004052  1881 _AX5043_TRKFSKDEMOD1	=	0x4052
                           00404B  1882 _AX5043_TRKPHASE0	=	0x404b
                           00404A  1883 _AX5043_TRKPHASE1	=	0x404a
                           00404F  1884 _AX5043_TRKRFFREQ0	=	0x404f
                           00404E  1885 _AX5043_TRKRFFREQ1	=	0x404e
                           00404D  1886 _AX5043_TRKRFFREQ2	=	0x404d
                           004169  1887 _AX5043_TXPWRCOEFFA0	=	0x4169
                           004168  1888 _AX5043_TXPWRCOEFFA1	=	0x4168
                           00416B  1889 _AX5043_TXPWRCOEFFB0	=	0x416b
                           00416A  1890 _AX5043_TXPWRCOEFFB1	=	0x416a
                           00416D  1891 _AX5043_TXPWRCOEFFC0	=	0x416d
                           00416C  1892 _AX5043_TXPWRCOEFFC1	=	0x416c
                           00416F  1893 _AX5043_TXPWRCOEFFD0	=	0x416f
                           00416E  1894 _AX5043_TXPWRCOEFFD1	=	0x416e
                           004171  1895 _AX5043_TXPWRCOEFFE0	=	0x4171
                           004170  1896 _AX5043_TXPWRCOEFFE1	=	0x4170
                           004167  1897 _AX5043_TXRATE0	=	0x4167
                           004166  1898 _AX5043_TXRATE1	=	0x4166
                           004165  1899 _AX5043_TXRATE2	=	0x4165
                           00406B  1900 _AX5043_WAKEUP0	=	0x406b
                           00406A  1901 _AX5043_WAKEUP1	=	0x406a
                           00406D  1902 _AX5043_WAKEUPFREQ0	=	0x406d
                           00406C  1903 _AX5043_WAKEUPFREQ1	=	0x406c
                           004069  1904 _AX5043_WAKEUPTIMER0	=	0x4069
                           004068  1905 _AX5043_WAKEUPTIMER1	=	0x4068
                           00406E  1906 _AX5043_WAKEUPXOEARLY	=	0x406e
                           004184  1907 _AX5043_XTALCAP	=	0x4184
                           00401D  1908 _AX5043_XTALSTATUS	=	0x401d
                           004122  1909 _AX5043_AGCAHYST0	=	0x4122
                           004132  1910 _AX5043_AGCAHYST1	=	0x4132
                           004142  1911 _AX5043_AGCAHYST2	=	0x4142
                           004152  1912 _AX5043_AGCAHYST3	=	0x4152
                           004120  1913 _AX5043_AGCGAIN0	=	0x4120
                           004130  1914 _AX5043_AGCGAIN1	=	0x4130
                           004140  1915 _AX5043_AGCGAIN2	=	0x4140
                           004150  1916 _AX5043_AGCGAIN3	=	0x4150
                           004123  1917 _AX5043_AGCMINMAX0	=	0x4123
                           004133  1918 _AX5043_AGCMINMAX1	=	0x4133
                           004143  1919 _AX5043_AGCMINMAX2	=	0x4143
                           004153  1920 _AX5043_AGCMINMAX3	=	0x4153
                           004121  1921 _AX5043_AGCTARGET0	=	0x4121
                           004131  1922 _AX5043_AGCTARGET1	=	0x4131
                           004141  1923 _AX5043_AGCTARGET2	=	0x4141
                           004151  1924 _AX5043_AGCTARGET3	=	0x4151
                           00412B  1925 _AX5043_AMPLITUDEGAIN0	=	0x412b
                           00413B  1926 _AX5043_AMPLITUDEGAIN1	=	0x413b
                           00414B  1927 _AX5043_AMPLITUDEGAIN2	=	0x414b
                           00415B  1928 _AX5043_AMPLITUDEGAIN3	=	0x415b
                           00412F  1929 _AX5043_BBOFFSRES0	=	0x412f
                           00413F  1930 _AX5043_BBOFFSRES1	=	0x413f
                           00414F  1931 _AX5043_BBOFFSRES2	=	0x414f
                           00415F  1932 _AX5043_BBOFFSRES3	=	0x415f
                           004125  1933 _AX5043_DRGAIN0	=	0x4125
                           004135  1934 _AX5043_DRGAIN1	=	0x4135
                           004145  1935 _AX5043_DRGAIN2	=	0x4145
                           004155  1936 _AX5043_DRGAIN3	=	0x4155
                           00412E  1937 _AX5043_FOURFSK0	=	0x412e
                           00413E  1938 _AX5043_FOURFSK1	=	0x413e
                           00414E  1939 _AX5043_FOURFSK2	=	0x414e
                           00415E  1940 _AX5043_FOURFSK3	=	0x415e
                           00412D  1941 _AX5043_FREQDEV00	=	0x412d
                           00413D  1942 _AX5043_FREQDEV01	=	0x413d
                           00414D  1943 _AX5043_FREQDEV02	=	0x414d
                           00415D  1944 _AX5043_FREQDEV03	=	0x415d
                           00412C  1945 _AX5043_FREQDEV10	=	0x412c
                           00413C  1946 _AX5043_FREQDEV11	=	0x413c
                           00414C  1947 _AX5043_FREQDEV12	=	0x414c
                           00415C  1948 _AX5043_FREQDEV13	=	0x415c
                           004127  1949 _AX5043_FREQUENCYGAINA0	=	0x4127
                           004137  1950 _AX5043_FREQUENCYGAINA1	=	0x4137
                           004147  1951 _AX5043_FREQUENCYGAINA2	=	0x4147
                           004157  1952 _AX5043_FREQUENCYGAINA3	=	0x4157
                           004128  1953 _AX5043_FREQUENCYGAINB0	=	0x4128
                           004138  1954 _AX5043_FREQUENCYGAINB1	=	0x4138
                           004148  1955 _AX5043_FREQUENCYGAINB2	=	0x4148
                           004158  1956 _AX5043_FREQUENCYGAINB3	=	0x4158
                           004129  1957 _AX5043_FREQUENCYGAINC0	=	0x4129
                           004139  1958 _AX5043_FREQUENCYGAINC1	=	0x4139
                           004149  1959 _AX5043_FREQUENCYGAINC2	=	0x4149
                           004159  1960 _AX5043_FREQUENCYGAINC3	=	0x4159
                           00412A  1961 _AX5043_FREQUENCYGAIND0	=	0x412a
                           00413A  1962 _AX5043_FREQUENCYGAIND1	=	0x413a
                           00414A  1963 _AX5043_FREQUENCYGAIND2	=	0x414a
                           00415A  1964 _AX5043_FREQUENCYGAIND3	=	0x415a
                           004116  1965 _AX5043_FREQUENCYLEAK	=	0x4116
                           004126  1966 _AX5043_PHASEGAIN0	=	0x4126
                           004136  1967 _AX5043_PHASEGAIN1	=	0x4136
                           004146  1968 _AX5043_PHASEGAIN2	=	0x4146
                           004156  1969 _AX5043_PHASEGAIN3	=	0x4156
                           004207  1970 _AX5043_PKTADDR0	=	0x4207
                           004206  1971 _AX5043_PKTADDR1	=	0x4206
                           004205  1972 _AX5043_PKTADDR2	=	0x4205
                           004204  1973 _AX5043_PKTADDR3	=	0x4204
                           004200  1974 _AX5043_PKTADDRCFG	=	0x4200
                           00420B  1975 _AX5043_PKTADDRMASK0	=	0x420b
                           00420A  1976 _AX5043_PKTADDRMASK1	=	0x420a
                           004209  1977 _AX5043_PKTADDRMASK2	=	0x4209
                           004208  1978 _AX5043_PKTADDRMASK3	=	0x4208
                           004201  1979 _AX5043_PKTLENCFG	=	0x4201
                           004202  1980 _AX5043_PKTLENOFFSET	=	0x4202
                           004203  1981 _AX5043_PKTMAXLEN	=	0x4203
                           004118  1982 _AX5043_RXPARAMCURSET	=	0x4118
                           004117  1983 _AX5043_RXPARAMSETS	=	0x4117
                           004124  1984 _AX5043_TIMEGAIN0	=	0x4124
                           004134  1985 _AX5043_TIMEGAIN1	=	0x4134
                           004144  1986 _AX5043_TIMEGAIN2	=	0x4144
                           004154  1987 _AX5043_TIMEGAIN3	=	0x4154
                           005114  1988 _AX5043_AFSKCTRLNB	=	0x5114
                           005113  1989 _AX5043_AFSKMARK0NB	=	0x5113
                           005112  1990 _AX5043_AFSKMARK1NB	=	0x5112
                           005111  1991 _AX5043_AFSKSPACE0NB	=	0x5111
                           005110  1992 _AX5043_AFSKSPACE1NB	=	0x5110
                           005043  1993 _AX5043_AGCCOUNTERNB	=	0x5043
                           005115  1994 _AX5043_AMPLFILTERNB	=	0x5115
                           005189  1995 _AX5043_BBOFFSCAPNB	=	0x5189
                           005188  1996 _AX5043_BBTUNENB	=	0x5188
                           005041  1997 _AX5043_BGNDRSSINB	=	0x5041
                           00522E  1998 _AX5043_BGNDRSSIGAINNB	=	0x522e
                           00522F  1999 _AX5043_BGNDRSSITHRNB	=	0x522f
                           005017  2000 _AX5043_CRCINIT0NB	=	0x5017
                           005016  2001 _AX5043_CRCINIT1NB	=	0x5016
                           005015  2002 _AX5043_CRCINIT2NB	=	0x5015
                           005014  2003 _AX5043_CRCINIT3NB	=	0x5014
                           005332  2004 _AX5043_DACCONFIGNB	=	0x5332
                           005331  2005 _AX5043_DACVALUE0NB	=	0x5331
                           005330  2006 _AX5043_DACVALUE1NB	=	0x5330
                           005102  2007 _AX5043_DECIMATIONNB	=	0x5102
                           005042  2008 _AX5043_DIVERSITYNB	=	0x5042
                           005011  2009 _AX5043_ENCODINGNB	=	0x5011
                           005018  2010 _AX5043_FECNB	=	0x5018
                           00501A  2011 _AX5043_FECSTATUSNB	=	0x501a
                           005019  2012 _AX5043_FECSYNCNB	=	0x5019
                           00502B  2013 _AX5043_FIFOCOUNT0NB	=	0x502b
                           00502A  2014 _AX5043_FIFOCOUNT1NB	=	0x502a
                           005029  2015 _AX5043_FIFODATANB	=	0x5029
                           00502D  2016 _AX5043_FIFOFREE0NB	=	0x502d
                           00502C  2017 _AX5043_FIFOFREE1NB	=	0x502c
                           005028  2018 _AX5043_FIFOSTATNB	=	0x5028
                           00502F  2019 _AX5043_FIFOTHRESH0NB	=	0x502f
                           00502E  2020 _AX5043_FIFOTHRESH1NB	=	0x502e
                           005012  2021 _AX5043_FRAMINGNB	=	0x5012
                           005037  2022 _AX5043_FREQA0NB	=	0x5037
                           005036  2023 _AX5043_FREQA1NB	=	0x5036
                           005035  2024 _AX5043_FREQA2NB	=	0x5035
                           005034  2025 _AX5043_FREQA3NB	=	0x5034
                           00503F  2026 _AX5043_FREQB0NB	=	0x503f
                           00503E  2027 _AX5043_FREQB1NB	=	0x503e
                           00503D  2028 _AX5043_FREQB2NB	=	0x503d
                           00503C  2029 _AX5043_FREQB3NB	=	0x503c
                           005163  2030 _AX5043_FSKDEV0NB	=	0x5163
                           005162  2031 _AX5043_FSKDEV1NB	=	0x5162
                           005161  2032 _AX5043_FSKDEV2NB	=	0x5161
                           00510D  2033 _AX5043_FSKDMAX0NB	=	0x510d
                           00510C  2034 _AX5043_FSKDMAX1NB	=	0x510c
                           00510F  2035 _AX5043_FSKDMIN0NB	=	0x510f
                           00510E  2036 _AX5043_FSKDMIN1NB	=	0x510e
                           005309  2037 _AX5043_GPADC13VALUE0NB	=	0x5309
                           005308  2038 _AX5043_GPADC13VALUE1NB	=	0x5308
                           005300  2039 _AX5043_GPADCCTRLNB	=	0x5300
                           005301  2040 _AX5043_GPADCPERIODNB	=	0x5301
                           005101  2041 _AX5043_IFFREQ0NB	=	0x5101
                           005100  2042 _AX5043_IFFREQ1NB	=	0x5100
                           00500B  2043 _AX5043_IRQINVERSION0NB	=	0x500b
                           00500A  2044 _AX5043_IRQINVERSION1NB	=	0x500a
                           005007  2045 _AX5043_IRQMASK0NB	=	0x5007
                           005006  2046 _AX5043_IRQMASK1NB	=	0x5006
                           00500D  2047 _AX5043_IRQREQUEST0NB	=	0x500d
                           00500C  2048 _AX5043_IRQREQUEST1NB	=	0x500c
                           005310  2049 _AX5043_LPOSCCONFIGNB	=	0x5310
                           005317  2050 _AX5043_LPOSCFREQ0NB	=	0x5317
                           005316  2051 _AX5043_LPOSCFREQ1NB	=	0x5316
                           005313  2052 _AX5043_LPOSCKFILT0NB	=	0x5313
                           005312  2053 _AX5043_LPOSCKFILT1NB	=	0x5312
                           005319  2054 _AX5043_LPOSCPER0NB	=	0x5319
                           005318  2055 _AX5043_LPOSCPER1NB	=	0x5318
                           005315  2056 _AX5043_LPOSCREF0NB	=	0x5315
                           005314  2057 _AX5043_LPOSCREF1NB	=	0x5314
                           005311  2058 _AX5043_LPOSCSTATUSNB	=	0x5311
                           005214  2059 _AX5043_MATCH0LENNB	=	0x5214
                           005216  2060 _AX5043_MATCH0MAXNB	=	0x5216
                           005215  2061 _AX5043_MATCH0MINNB	=	0x5215
                           005213  2062 _AX5043_MATCH0PAT0NB	=	0x5213
                           005212  2063 _AX5043_MATCH0PAT1NB	=	0x5212
                           005211  2064 _AX5043_MATCH0PAT2NB	=	0x5211
                           005210  2065 _AX5043_MATCH0PAT3NB	=	0x5210
                           00521C  2066 _AX5043_MATCH1LENNB	=	0x521c
                           00521E  2067 _AX5043_MATCH1MAXNB	=	0x521e
                           00521D  2068 _AX5043_MATCH1MINNB	=	0x521d
                           005219  2069 _AX5043_MATCH1PAT0NB	=	0x5219
                           005218  2070 _AX5043_MATCH1PAT1NB	=	0x5218
                           005108  2071 _AX5043_MAXDROFFSET0NB	=	0x5108
                           005107  2072 _AX5043_MAXDROFFSET1NB	=	0x5107
                           005106  2073 _AX5043_MAXDROFFSET2NB	=	0x5106
                           00510B  2074 _AX5043_MAXRFOFFSET0NB	=	0x510b
                           00510A  2075 _AX5043_MAXRFOFFSET1NB	=	0x510a
                           005109  2076 _AX5043_MAXRFOFFSET2NB	=	0x5109
                           005164  2077 _AX5043_MODCFGANB	=	0x5164
                           005160  2078 _AX5043_MODCFGFNB	=	0x5160
                           005F5F  2079 _AX5043_MODCFGPNB	=	0x5f5f
                           005010  2080 _AX5043_MODULATIONNB	=	0x5010
                           005025  2081 _AX5043_PINFUNCANTSELNB	=	0x5025
                           005023  2082 _AX5043_PINFUNCDATANB	=	0x5023
                           005022  2083 _AX5043_PINFUNCDCLKNB	=	0x5022
                           005024  2084 _AX5043_PINFUNCIRQNB	=	0x5024
                           005026  2085 _AX5043_PINFUNCPWRAMPNB	=	0x5026
                           005021  2086 _AX5043_PINFUNCSYSCLKNB	=	0x5021
                           005020  2087 _AX5043_PINSTATENB	=	0x5020
                           005233  2088 _AX5043_PKTACCEPTFLAGSNB	=	0x5233
                           005230  2089 _AX5043_PKTCHUNKSIZENB	=	0x5230
                           005231  2090 _AX5043_PKTMISCFLAGSNB	=	0x5231
                           005232  2091 _AX5043_PKTSTOREFLAGSNB	=	0x5232
                           005031  2092 _AX5043_PLLCPINB	=	0x5031
                           005039  2093 _AX5043_PLLCPIBOOSTNB	=	0x5039
                           005182  2094 _AX5043_PLLLOCKDETNB	=	0x5182
                           005030  2095 _AX5043_PLLLOOPNB	=	0x5030
                           005038  2096 _AX5043_PLLLOOPBOOSTNB	=	0x5038
                           005033  2097 _AX5043_PLLRANGINGANB	=	0x5033
                           00503B  2098 _AX5043_PLLRANGINGBNB	=	0x503b
                           005183  2099 _AX5043_PLLRNGCLKNB	=	0x5183
                           005032  2100 _AX5043_PLLVCODIVNB	=	0x5032
                           005180  2101 _AX5043_PLLVCOINB	=	0x5180
                           005181  2102 _AX5043_PLLVCOIRNB	=	0x5181
                           005F08  2103 _AX5043_POWCTRL1NB	=	0x5f08
                           005005  2104 _AX5043_POWIRQMASKNB	=	0x5005
                           005003  2105 _AX5043_POWSTATNB	=	0x5003
                           005004  2106 _AX5043_POWSTICKYSTATNB	=	0x5004
                           005027  2107 _AX5043_PWRAMPNB	=	0x5027
                           005002  2108 _AX5043_PWRMODENB	=	0x5002
                           005009  2109 _AX5043_RADIOEVENTMASK0NB	=	0x5009
                           005008  2110 _AX5043_RADIOEVENTMASK1NB	=	0x5008
                           00500F  2111 _AX5043_RADIOEVENTREQ0NB	=	0x500f
                           00500E  2112 _AX5043_RADIOEVENTREQ1NB	=	0x500e
                           00501C  2113 _AX5043_RADIOSTATENB	=	0x501c
                           005F0D  2114 _AX5043_REFNB	=	0x5f0d
                           005040  2115 _AX5043_RSSINB	=	0x5040
                           00522D  2116 _AX5043_RSSIABSTHRNB	=	0x522d
                           00522C  2117 _AX5043_RSSIREFERENCENB	=	0x522c
                           005105  2118 _AX5043_RXDATARATE0NB	=	0x5105
                           005104  2119 _AX5043_RXDATARATE1NB	=	0x5104
                           005103  2120 _AX5043_RXDATARATE2NB	=	0x5103
                           005001  2121 _AX5043_SCRATCHNB	=	0x5001
                           005000  2122 _AX5043_SILICONREVISIONNB	=	0x5000
                           00505B  2123 _AX5043_TIMER0NB	=	0x505b
                           00505A  2124 _AX5043_TIMER1NB	=	0x505a
                           005059  2125 _AX5043_TIMER2NB	=	0x5059
                           005227  2126 _AX5043_TMGRXAGCNB	=	0x5227
                           005223  2127 _AX5043_TMGRXBOOSTNB	=	0x5223
                           005226  2128 _AX5043_TMGRXCOARSEAGCNB	=	0x5226
                           005225  2129 _AX5043_TMGRXOFFSACQNB	=	0x5225
                           005229  2130 _AX5043_TMGRXPREAMBLE1NB	=	0x5229
                           00522A  2131 _AX5043_TMGRXPREAMBLE2NB	=	0x522a
                           00522B  2132 _AX5043_TMGRXPREAMBLE3NB	=	0x522b
                           005228  2133 _AX5043_TMGRXRSSINB	=	0x5228
                           005224  2134 _AX5043_TMGRXSETTLENB	=	0x5224
                           005220  2135 _AX5043_TMGTXBOOSTNB	=	0x5220
                           005221  2136 _AX5043_TMGTXSETTLENB	=	0x5221
                           005055  2137 _AX5043_TRKAFSKDEMOD0NB	=	0x5055
                           005054  2138 _AX5043_TRKAFSKDEMOD1NB	=	0x5054
                           005049  2139 _AX5043_TRKAMPLITUDE0NB	=	0x5049
                           005048  2140 _AX5043_TRKAMPLITUDE1NB	=	0x5048
                           005047  2141 _AX5043_TRKDATARATE0NB	=	0x5047
                           005046  2142 _AX5043_TRKDATARATE1NB	=	0x5046
                           005045  2143 _AX5043_TRKDATARATE2NB	=	0x5045
                           005051  2144 _AX5043_TRKFREQ0NB	=	0x5051
                           005050  2145 _AX5043_TRKFREQ1NB	=	0x5050
                           005053  2146 _AX5043_TRKFSKDEMOD0NB	=	0x5053
                           005052  2147 _AX5043_TRKFSKDEMOD1NB	=	0x5052
                           00504B  2148 _AX5043_TRKPHASE0NB	=	0x504b
                           00504A  2149 _AX5043_TRKPHASE1NB	=	0x504a
                           00504F  2150 _AX5043_TRKRFFREQ0NB	=	0x504f
                           00504E  2151 _AX5043_TRKRFFREQ1NB	=	0x504e
                           00504D  2152 _AX5043_TRKRFFREQ2NB	=	0x504d
                           005169  2153 _AX5043_TXPWRCOEFFA0NB	=	0x5169
                           005168  2154 _AX5043_TXPWRCOEFFA1NB	=	0x5168
                           00516B  2155 _AX5043_TXPWRCOEFFB0NB	=	0x516b
                           00516A  2156 _AX5043_TXPWRCOEFFB1NB	=	0x516a
                           00516D  2157 _AX5043_TXPWRCOEFFC0NB	=	0x516d
                           00516C  2158 _AX5043_TXPWRCOEFFC1NB	=	0x516c
                           00516F  2159 _AX5043_TXPWRCOEFFD0NB	=	0x516f
                           00516E  2160 _AX5043_TXPWRCOEFFD1NB	=	0x516e
                           005171  2161 _AX5043_TXPWRCOEFFE0NB	=	0x5171
                           005170  2162 _AX5043_TXPWRCOEFFE1NB	=	0x5170
                           005167  2163 _AX5043_TXRATE0NB	=	0x5167
                           005166  2164 _AX5043_TXRATE1NB	=	0x5166
                           005165  2165 _AX5043_TXRATE2NB	=	0x5165
                           00506B  2166 _AX5043_WAKEUP0NB	=	0x506b
                           00506A  2167 _AX5043_WAKEUP1NB	=	0x506a
                           00506D  2168 _AX5043_WAKEUPFREQ0NB	=	0x506d
                           00506C  2169 _AX5043_WAKEUPFREQ1NB	=	0x506c
                           005069  2170 _AX5043_WAKEUPTIMER0NB	=	0x5069
                           005068  2171 _AX5043_WAKEUPTIMER1NB	=	0x5068
                           00506E  2172 _AX5043_WAKEUPXOEARLYNB	=	0x506e
                           005F11  2173 _AX5043_XTALAMPLNB	=	0x5f11
                           005184  2174 _AX5043_XTALCAPNB	=	0x5184
                           005F10  2175 _AX5043_XTALOSCNB	=	0x5f10
                           00501D  2176 _AX5043_XTALSTATUSNB	=	0x501d
                           005F00  2177 _AX5043_0xF00NB	=	0x5f00
                           005F0C  2178 _AX5043_0xF0CNB	=	0x5f0c
                           005F18  2179 _AX5043_0xF18NB	=	0x5f18
                           005F1C  2180 _AX5043_0xF1CNB	=	0x5f1c
                           005F21  2181 _AX5043_0xF21NB	=	0x5f21
                           005F22  2182 _AX5043_0xF22NB	=	0x5f22
                           005F23  2183 _AX5043_0xF23NB	=	0x5f23
                           005F26  2184 _AX5043_0xF26NB	=	0x5f26
                           005F30  2185 _AX5043_0xF30NB	=	0x5f30
                           005F31  2186 _AX5043_0xF31NB	=	0x5f31
                           005F32  2187 _AX5043_0xF32NB	=	0x5f32
                           005F33  2188 _AX5043_0xF33NB	=	0x5f33
                           005F34  2189 _AX5043_0xF34NB	=	0x5f34
                           005F35  2190 _AX5043_0xF35NB	=	0x5f35
                           005F44  2191 _AX5043_0xF44NB	=	0x5f44
                           005122  2192 _AX5043_AGCAHYST0NB	=	0x5122
                           005132  2193 _AX5043_AGCAHYST1NB	=	0x5132
                           005142  2194 _AX5043_AGCAHYST2NB	=	0x5142
                           005152  2195 _AX5043_AGCAHYST3NB	=	0x5152
                           005120  2196 _AX5043_AGCGAIN0NB	=	0x5120
                           005130  2197 _AX5043_AGCGAIN1NB	=	0x5130
                           005140  2198 _AX5043_AGCGAIN2NB	=	0x5140
                           005150  2199 _AX5043_AGCGAIN3NB	=	0x5150
                           005123  2200 _AX5043_AGCMINMAX0NB	=	0x5123
                           005133  2201 _AX5043_AGCMINMAX1NB	=	0x5133
                           005143  2202 _AX5043_AGCMINMAX2NB	=	0x5143
                           005153  2203 _AX5043_AGCMINMAX3NB	=	0x5153
                           005121  2204 _AX5043_AGCTARGET0NB	=	0x5121
                           005131  2205 _AX5043_AGCTARGET1NB	=	0x5131
                           005141  2206 _AX5043_AGCTARGET2NB	=	0x5141
                           005151  2207 _AX5043_AGCTARGET3NB	=	0x5151
                           00512B  2208 _AX5043_AMPLITUDEGAIN0NB	=	0x512b
                           00513B  2209 _AX5043_AMPLITUDEGAIN1NB	=	0x513b
                           00514B  2210 _AX5043_AMPLITUDEGAIN2NB	=	0x514b
                           00515B  2211 _AX5043_AMPLITUDEGAIN3NB	=	0x515b
                           00512F  2212 _AX5043_BBOFFSRES0NB	=	0x512f
                           00513F  2213 _AX5043_BBOFFSRES1NB	=	0x513f
                           00514F  2214 _AX5043_BBOFFSRES2NB	=	0x514f
                           00515F  2215 _AX5043_BBOFFSRES3NB	=	0x515f
                           005125  2216 _AX5043_DRGAIN0NB	=	0x5125
                           005135  2217 _AX5043_DRGAIN1NB	=	0x5135
                           005145  2218 _AX5043_DRGAIN2NB	=	0x5145
                           005155  2219 _AX5043_DRGAIN3NB	=	0x5155
                           00512E  2220 _AX5043_FOURFSK0NB	=	0x512e
                           00513E  2221 _AX5043_FOURFSK1NB	=	0x513e
                           00514E  2222 _AX5043_FOURFSK2NB	=	0x514e
                           00515E  2223 _AX5043_FOURFSK3NB	=	0x515e
                           00512D  2224 _AX5043_FREQDEV00NB	=	0x512d
                           00513D  2225 _AX5043_FREQDEV01NB	=	0x513d
                           00514D  2226 _AX5043_FREQDEV02NB	=	0x514d
                           00515D  2227 _AX5043_FREQDEV03NB	=	0x515d
                           00512C  2228 _AX5043_FREQDEV10NB	=	0x512c
                           00513C  2229 _AX5043_FREQDEV11NB	=	0x513c
                           00514C  2230 _AX5043_FREQDEV12NB	=	0x514c
                           00515C  2231 _AX5043_FREQDEV13NB	=	0x515c
                           005127  2232 _AX5043_FREQUENCYGAINA0NB	=	0x5127
                           005137  2233 _AX5043_FREQUENCYGAINA1NB	=	0x5137
                           005147  2234 _AX5043_FREQUENCYGAINA2NB	=	0x5147
                           005157  2235 _AX5043_FREQUENCYGAINA3NB	=	0x5157
                           005128  2236 _AX5043_FREQUENCYGAINB0NB	=	0x5128
                           005138  2237 _AX5043_FREQUENCYGAINB1NB	=	0x5138
                           005148  2238 _AX5043_FREQUENCYGAINB2NB	=	0x5148
                           005158  2239 _AX5043_FREQUENCYGAINB3NB	=	0x5158
                           005129  2240 _AX5043_FREQUENCYGAINC0NB	=	0x5129
                           005139  2241 _AX5043_FREQUENCYGAINC1NB	=	0x5139
                           005149  2242 _AX5043_FREQUENCYGAINC2NB	=	0x5149
                           005159  2243 _AX5043_FREQUENCYGAINC3NB	=	0x5159
                           00512A  2244 _AX5043_FREQUENCYGAIND0NB	=	0x512a
                           00513A  2245 _AX5043_FREQUENCYGAIND1NB	=	0x513a
                           00514A  2246 _AX5043_FREQUENCYGAIND2NB	=	0x514a
                           00515A  2247 _AX5043_FREQUENCYGAIND3NB	=	0x515a
                           005116  2248 _AX5043_FREQUENCYLEAKNB	=	0x5116
                           005126  2249 _AX5043_PHASEGAIN0NB	=	0x5126
                           005136  2250 _AX5043_PHASEGAIN1NB	=	0x5136
                           005146  2251 _AX5043_PHASEGAIN2NB	=	0x5146
                           005156  2252 _AX5043_PHASEGAIN3NB	=	0x5156
                           005207  2253 _AX5043_PKTADDR0NB	=	0x5207
                           005206  2254 _AX5043_PKTADDR1NB	=	0x5206
                           005205  2255 _AX5043_PKTADDR2NB	=	0x5205
                           005204  2256 _AX5043_PKTADDR3NB	=	0x5204
                           005200  2257 _AX5043_PKTADDRCFGNB	=	0x5200
                           00520B  2258 _AX5043_PKTADDRMASK0NB	=	0x520b
                           00520A  2259 _AX5043_PKTADDRMASK1NB	=	0x520a
                           005209  2260 _AX5043_PKTADDRMASK2NB	=	0x5209
                           005208  2261 _AX5043_PKTADDRMASK3NB	=	0x5208
                           005201  2262 _AX5043_PKTLENCFGNB	=	0x5201
                           005202  2263 _AX5043_PKTLENOFFSETNB	=	0x5202
                           005203  2264 _AX5043_PKTMAXLENNB	=	0x5203
                           005118  2265 _AX5043_RXPARAMCURSETNB	=	0x5118
                           005117  2266 _AX5043_RXPARAMSETSNB	=	0x5117
                           005124  2267 _AX5043_TIMEGAIN0NB	=	0x5124
                           005134  2268 _AX5043_TIMEGAIN1NB	=	0x5134
                           005144  2269 _AX5043_TIMEGAIN2NB	=	0x5144
                           005154  2270 _AX5043_TIMEGAIN3NB	=	0x5154
                           004F00  2271 _AX5043_0xF00	=	0x4f00
                           004F0C  2272 _AX5043_0xF0C	=	0x4f0c
                           004F10  2273 _AX5043_0xF10	=	0x4f10
                           004F11  2274 _AX5043_0xF11	=	0x4f11
                           004F18  2275 _AX5043_0xF18	=	0x4f18
                           004F1C  2276 _AX5043_0xF1C	=	0x4f1c
                           004F21  2277 _AX5043_0xF21	=	0x4f21
                           004F22  2278 _AX5043_0xF22	=	0x4f22
                           004F23  2279 _AX5043_0xF23	=	0x4f23
                           004F26  2280 _AX5043_0xF26	=	0x4f26
                           004F30  2281 _AX5043_0xF30	=	0x4f30
                           004F31  2282 _AX5043_0xF31	=	0x4f31
                           004F32  2283 _AX5043_0xF32	=	0x4f32
                           004F33  2284 _AX5043_0xF33	=	0x4f33
                           004F34  2285 _AX5043_0xF34	=	0x4f34
                           004F35  2286 _AX5043_0xF35	=	0x4f35
                           004F44  2287 _AX5043_0xF44	=	0x4f44
                           004F0D  2288 _AX5043_REF	=	0x4f0d
                           004F08  2289 _AX5043_POWCTRL1	=	0x4f08
                           004F5F  2290 _AX5043_MODCFGP	=	0x4f5f
                           004F10  2291 _AX5043_XTALOSC	=	0x4f10
                           004F11  2292 _AX5043_XTALAMPL	=	0x4f11
      0000A4                       2293 _aligned_alloc_PARM_2:
      0000A4                       2294 	.ds 2
      0000A6                       2295 _axradio_syncstate::
      0000A6                       2296 	.ds 1
      0000A7                       2297 _axradio_txbuffer_len::
      0000A7                       2298 	.ds 2
      0000A9                       2299 _axradio_txbuffer_cnt::
      0000A9                       2300 	.ds 2
      0000AB                       2301 _axradio_curchannel::
      0000AB                       2302 	.ds 1
      0000AC                       2303 _axradio_curfreqoffset::
      0000AC                       2304 	.ds 4
      0000B0                       2305 _axradio_ack_count::
      0000B0                       2306 	.ds 1
      0000B1                       2307 _axradio_ack_seqnr::
      0000B1                       2308 	.ds 1
      0000B2                       2309 _axradio_sync_time::
      0000B2                       2310 	.ds 4
      0000B6                       2311 _axradio_sync_periodcorr::
      0000B6                       2312 	.ds 2
      0000B8                       2313 _axradio_timeanchor::
      0000B8                       2314 	.ds 8
      0000C0                       2315 _axradio_localaddr::
      0000C0                       2316 	.ds 8
      0000C8                       2317 _axradio_default_remoteaddr::
      0000C8                       2318 	.ds 4
      0000CC                       2319 _axradio_txbuffer::
      0000CC                       2320 	.ds 260
      0001D0                       2321 _axradio_rxbuffer::
      0001D0                       2322 	.ds 260
      0002D4                       2323 _axradio_cb_receive::
      0002D4                       2324 	.ds 34
      0002F6                       2325 _axradio_cb_receivesfd::
      0002F6                       2326 	.ds 10
      000300                       2327 _axradio_cb_channelstate::
      000300                       2328 	.ds 13
      00030D                       2329 _axradio_cb_transmitstart::
      00030D                       2330 	.ds 10
      000317                       2331 _axradio_cb_transmitend::
      000317                       2332 	.ds 10
      000321                       2333 _axradio_cb_transmitdata::
      000321                       2334 	.ds 10
      00032B                       2335 _axradio_timer::
      00032B                       2336 	.ds 8
      000333                       2337 _receive_isr_BeaconRx_1_245:
      000333                       2338 	.ds 29
      000350                       2339 _axradio_init_x_3_396:
      000350                       2340 	.ds 1
      000351                       2341 _axradio_init_j_3_397:
      000351                       2342 	.ds 1
      000352                       2343 _axradio_init_x_6_401:
      000352                       2344 	.ds 1
      000353                       2345 _axradio_set_mode_mode_1_412:
      000353                       2346 	.ds 1
      000354                       2347 _axradio_set_channel_chnum_1_425:
      000354                       2348 	.ds 1
      000355                       2349 _axradio_get_pllvcoi_x_2_437:
      000355                       2350 	.ds 1
      000356                       2351 _axradio_set_curfreqoffset_offs_1_440:
      000356                       2352 	.ds 4
      00035A                       2353 _axradio_set_freqoffset_offs_1_442:
      00035A                       2354 	.ds 4
      00035E                       2355 _axradio_set_local_address_addr_1_447:
      00035E                       2356 	.ds 3
      000361                       2357 _axradio_get_local_address_addr_1_449:
      000361                       2358 	.ds 3
      000364                       2359 _axradio_set_default_remote_address_addr_1_451:
      000364                       2360 	.ds 3
      000367                       2361 _axradio_get_default_remote_address_addr_1_453:
      000367                       2362 	.ds 3
      00036A                       2363 _axradio_transmit_PARM_2:
      00036A                       2364 	.ds 3
      00036D                       2365 _axradio_transmit_PARM_3:
      00036D                       2366 	.ds 2
      00036F                       2367 _axradio_transmit_addr_1_455:
      00036F                       2368 	.ds 3
                                   2369 ;--------------------------------------------------------
                                   2370 ; absolute external ram data
                                   2371 ;--------------------------------------------------------
                                   2372 	.area XABS    (ABS,XDATA)
                                   2373 ;--------------------------------------------------------
                                   2374 ; external initialized ram data
                                   2375 ;--------------------------------------------------------
                                   2376 	.area XISEG   (XDATA)
      0009A5                       2377 _f30_saved::
      0009A5                       2378 	.ds 1
      0009A6                       2379 _f31_saved::
      0009A6                       2380 	.ds 1
      0009A7                       2381 _f32_saved::
      0009A7                       2382 	.ds 1
      0009A8                       2383 _f33_saved::
      0009A8                       2384 	.ds 1
                                   2385 	.area HOME    (CODE)
                                   2386 	.area GSINIT0 (CODE)
                                   2387 	.area GSINIT1 (CODE)
                                   2388 	.area GSINIT2 (CODE)
                                   2389 	.area GSINIT3 (CODE)
                                   2390 	.area GSINIT4 (CODE)
                                   2391 	.area GSINIT5 (CODE)
                                   2392 	.area GSINIT  (CODE)
                                   2393 	.area GSFINAL (CODE)
                                   2394 	.area CSEG    (CODE)
                                   2395 ;--------------------------------------------------------
                                   2396 ; global & static initialisations
                                   2397 ;--------------------------------------------------------
                                   2398 	.area HOME    (CODE)
                                   2399 	.area GSINIT  (CODE)
                                   2400 	.area GSFINAL (CODE)
                                   2401 	.area GSINIT  (CODE)
                                   2402 ;	COMMON\easyax5043.c:60: volatile uint8_t __data axradio_mode = AXRADIO_MODE_UNINIT;
      000391 75 0B 00         [24] 2403 	mov	_axradio_mode,#0x00
                                   2404 ;	COMMON\easyax5043.c:61: volatile axradio_trxstate_t __data axradio_trxstate = trxstate_off;
      000394 75 0C 00         [24] 2405 	mov	_axradio_trxstate,#0x00
                                   2406 ;--------------------------------------------------------
                                   2407 ; Home
                                   2408 ;--------------------------------------------------------
                                   2409 	.area HOME    (CODE)
                                   2410 	.area HOME    (CODE)
                                   2411 ;--------------------------------------------------------
                                   2412 ; code
                                   2413 ;--------------------------------------------------------
                                   2414 	.area CSEG    (CODE)
                                   2415 ;------------------------------------------------------------
                                   2416 ;Allocation info for local variables in function 'update_timeanchor'
                                   2417 ;------------------------------------------------------------
                                   2418 ;iesave                    Allocated to registers r7 
                                   2419 ;------------------------------------------------------------
                                   2420 ;	COMMON\easyax5043.c:239: static __reentrantb void update_timeanchor(void) __reentrant
                                   2421 ;	-----------------------------------------
                                   2422 ;	 function update_timeanchor
                                   2423 ;	-----------------------------------------
      001B43                       2424 _update_timeanchor:
                           000007  2425 	ar7 = 0x07
                           000006  2426 	ar6 = 0x06
                           000005  2427 	ar5 = 0x05
                           000004  2428 	ar4 = 0x04
                           000003  2429 	ar3 = 0x03
                           000002  2430 	ar2 = 0x02
                           000001  2431 	ar1 = 0x01
                           000000  2432 	ar0 = 0x00
                                   2433 ;	COMMON\easyax5043.c:241: uint8_t iesave = IE & 0x80;
      001B43 74 80            [12] 2434 	mov	a,#0x80
      001B45 55 A8            [12] 2435 	anl	a,_IE
      001B47 FF               [12] 2436 	mov	r7,a
                                   2437 ;	COMMON\easyax5043.c:242: EA = 0;
      001B48 C2 AF            [12] 2438 	clr	_EA
                                   2439 ;	COMMON\easyax5043.c:243: axradio_timeanchor.timer0 = wtimer0_curtime();
      001B4A C0 07            [24] 2440 	push	ar7
      001B4C 12 7D B7         [24] 2441 	lcall	_wtimer0_curtime
      001B4F AB 82            [24] 2442 	mov	r3,dpl
      001B51 AC 83            [24] 2443 	mov	r4,dph
      001B53 AD F0            [24] 2444 	mov	r5,b
      001B55 FE               [12] 2445 	mov	r6,a
      001B56 D0 07            [24] 2446 	pop	ar7
      001B58 90 00 B8         [24] 2447 	mov	dptr,#_axradio_timeanchor
      001B5B EB               [12] 2448 	mov	a,r3
      001B5C F0               [24] 2449 	movx	@dptr,a
      001B5D EC               [12] 2450 	mov	a,r4
      001B5E A3               [24] 2451 	inc	dptr
      001B5F F0               [24] 2452 	movx	@dptr,a
      001B60 ED               [12] 2453 	mov	a,r5
      001B61 A3               [24] 2454 	inc	dptr
      001B62 F0               [24] 2455 	movx	@dptr,a
      001B63 EE               [12] 2456 	mov	a,r6
      001B64 A3               [24] 2457 	inc	dptr
      001B65 F0               [24] 2458 	movx	@dptr,a
                                   2459 ;	COMMON\easyax5043.c:244: axradio_timeanchor.radiotimer = radio_read24((uint16_t)&AX5043_TIMER2);
      001B66 7D 59            [12] 2460 	mov	r5,#_AX5043_TIMER2
      001B68 7E 40            [12] 2461 	mov	r6,#(_AX5043_TIMER2 >> 8)
      001B6A 8D 82            [24] 2462 	mov	dpl,r5
      001B6C 8E 83            [24] 2463 	mov	dph,r6
      001B6E 12 6D E5         [24] 2464 	lcall	_radio_read24
      001B71 AB 82            [24] 2465 	mov	r3,dpl
      001B73 AC 83            [24] 2466 	mov	r4,dph
      001B75 AD F0            [24] 2467 	mov	r5,b
      001B77 FE               [12] 2468 	mov	r6,a
      001B78 90 00 BC         [24] 2469 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      001B7B EB               [12] 2470 	mov	a,r3
      001B7C F0               [24] 2471 	movx	@dptr,a
      001B7D EC               [12] 2472 	mov	a,r4
      001B7E A3               [24] 2473 	inc	dptr
      001B7F F0               [24] 2474 	movx	@dptr,a
      001B80 ED               [12] 2475 	mov	a,r5
      001B81 A3               [24] 2476 	inc	dptr
      001B82 F0               [24] 2477 	movx	@dptr,a
      001B83 EE               [12] 2478 	mov	a,r6
      001B84 A3               [24] 2479 	inc	dptr
      001B85 F0               [24] 2480 	movx	@dptr,a
                                   2481 ;	COMMON\easyax5043.c:245: IE |= iesave;
      001B86 EF               [12] 2482 	mov	a,r7
      001B87 42 A8            [12] 2483 	orl	_IE,a
      001B89 22               [24] 2484 	ret
                                   2485 ;------------------------------------------------------------
                                   2486 ;Allocation info for local variables in function 'axradio_conv_time_totimer0'
                                   2487 ;------------------------------------------------------------
                                   2488 ;dt                        Allocated to registers r4 r5 r6 r7 
                                   2489 ;------------------------------------------------------------
                                   2490 ;	COMMON\easyax5043.c:248: __reentrantb uint32_t axradio_conv_time_totimer0(uint32_t dt) __reentrant
                                   2491 ;	-----------------------------------------
                                   2492 ;	 function axradio_conv_time_totimer0
                                   2493 ;	-----------------------------------------
      001B8A                       2494 _axradio_conv_time_totimer0:
      001B8A AC 82            [24] 2495 	mov	r4,dpl
      001B8C AD 83            [24] 2496 	mov	r5,dph
      001B8E AE F0            [24] 2497 	mov	r6,b
      001B90 FF               [12] 2498 	mov	r7,a
                                   2499 ;	COMMON\easyax5043.c:250: dt -= axradio_timeanchor.radiotimer;
      001B91 90 00 BC         [24] 2500 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      001B94 E0               [24] 2501 	movx	a,@dptr
      001B95 F8               [12] 2502 	mov	r0,a
      001B96 A3               [24] 2503 	inc	dptr
      001B97 E0               [24] 2504 	movx	a,@dptr
      001B98 F9               [12] 2505 	mov	r1,a
      001B99 A3               [24] 2506 	inc	dptr
      001B9A E0               [24] 2507 	movx	a,@dptr
      001B9B FA               [12] 2508 	mov	r2,a
      001B9C A3               [24] 2509 	inc	dptr
      001B9D E0               [24] 2510 	movx	a,@dptr
      001B9E FB               [12] 2511 	mov	r3,a
      001B9F EC               [12] 2512 	mov	a,r4
      001BA0 C3               [12] 2513 	clr	c
      001BA1 98               [12] 2514 	subb	a,r0
      001BA2 FC               [12] 2515 	mov	r4,a
      001BA3 ED               [12] 2516 	mov	a,r5
      001BA4 99               [12] 2517 	subb	a,r1
      001BA5 FD               [12] 2518 	mov	r5,a
      001BA6 EE               [12] 2519 	mov	a,r6
      001BA7 9A               [12] 2520 	subb	a,r2
      001BA8 FE               [12] 2521 	mov	r6,a
      001BA9 EF               [12] 2522 	mov	a,r7
      001BAA 9B               [12] 2523 	subb	a,r3
                                   2524 ;	COMMON\easyax5043.c:251: dt = axradio_conv_timeinterval_totimer0(signextend24(dt));
      001BAB 8C 82            [24] 2525 	mov	dpl,r4
      001BAD 8D 83            [24] 2526 	mov	dph,r5
      001BAF 8E F0            [24] 2527 	mov	b,r6
      001BB1 12 7D 9E         [24] 2528 	lcall	_signextend24
      001BB4 12 17 78         [24] 2529 	lcall	_axradio_conv_timeinterval_totimer0
      001BB7 AC 82            [24] 2530 	mov	r4,dpl
      001BB9 AD 83            [24] 2531 	mov	r5,dph
      001BBB AE F0            [24] 2532 	mov	r6,b
      001BBD FF               [12] 2533 	mov	r7,a
                                   2534 ;	COMMON\easyax5043.c:252: dt += axradio_timeanchor.timer0;
      001BBE 90 00 B8         [24] 2535 	mov	dptr,#_axradio_timeanchor
      001BC1 E0               [24] 2536 	movx	a,@dptr
      001BC2 F8               [12] 2537 	mov	r0,a
      001BC3 A3               [24] 2538 	inc	dptr
      001BC4 E0               [24] 2539 	movx	a,@dptr
      001BC5 F9               [12] 2540 	mov	r1,a
      001BC6 A3               [24] 2541 	inc	dptr
      001BC7 E0               [24] 2542 	movx	a,@dptr
      001BC8 FA               [12] 2543 	mov	r2,a
      001BC9 A3               [24] 2544 	inc	dptr
      001BCA E0               [24] 2545 	movx	a,@dptr
      001BCB FB               [12] 2546 	mov	r3,a
      001BCC E8               [12] 2547 	mov	a,r0
      001BCD 2C               [12] 2548 	add	a,r4
      001BCE FC               [12] 2549 	mov	r4,a
      001BCF E9               [12] 2550 	mov	a,r1
      001BD0 3D               [12] 2551 	addc	a,r5
      001BD1 FD               [12] 2552 	mov	r5,a
      001BD2 EA               [12] 2553 	mov	a,r2
      001BD3 3E               [12] 2554 	addc	a,r6
      001BD4 FE               [12] 2555 	mov	r6,a
      001BD5 EB               [12] 2556 	mov	a,r3
      001BD6 3F               [12] 2557 	addc	a,r7
                                   2558 ;	COMMON\easyax5043.c:253: return dt;
      001BD7 8C 82            [24] 2559 	mov	dpl,r4
      001BD9 8D 83            [24] 2560 	mov	dph,r5
      001BDB 8E F0            [24] 2561 	mov	b,r6
      001BDD 22               [24] 2562 	ret
                                   2563 ;------------------------------------------------------------
                                   2564 ;Allocation info for local variables in function 'ax5043_init_registers_common'
                                   2565 ;------------------------------------------------------------
                                   2566 ;rng                       Allocated to registers r7 
                                   2567 ;------------------------------------------------------------
                                   2568 ;	COMMON\easyax5043.c:256: static __reentrantb uint8_t ax5043_init_registers_common(void) __reentrant
                                   2569 ;	-----------------------------------------
                                   2570 ;	 function ax5043_init_registers_common
                                   2571 ;	-----------------------------------------
      001BDE                       2572 _ax5043_init_registers_common:
                                   2573 ;	COMMON\easyax5043.c:258: uint8_t rng = axradio_phy_chanpllrng[axradio_curchannel];
      001BDE 90 00 AB         [24] 2574 	mov	dptr,#_axradio_curchannel
      001BE1 E0               [24] 2575 	movx	a,@dptr
      001BE2 24 A0            [12] 2576 	add	a,#_axradio_phy_chanpllrng
      001BE4 F5 82            [12] 2577 	mov	dpl,a
      001BE6 E4               [12] 2578 	clr	a
      001BE7 34 00            [12] 2579 	addc	a,#(_axradio_phy_chanpllrng >> 8)
      001BE9 F5 83            [12] 2580 	mov	dph,a
      001BEB E0               [24] 2581 	movx	a,@dptr
                                   2582 ;	COMMON\easyax5043.c:259: if (rng & 0x20)
      001BEC FF               [12] 2583 	mov	r7,a
      001BED 30 E5 04         [24] 2584 	jnb	acc.5,00102$
                                   2585 ;	COMMON\easyax5043.c:260: return AXRADIO_ERR_RANGING;
      001BF0 75 82 06         [24] 2586 	mov	dpl,#0x06
      001BF3 22               [24] 2587 	ret
      001BF4                       2588 00102$:
                                   2589 ;	COMMON\easyax5043.c:261: if (AX5043_PLLLOOP & 0x80) {
      001BF4 90 40 30         [24] 2590 	mov	dptr,#_AX5043_PLLLOOP
      001BF7 E0               [24] 2591 	movx	a,@dptr
      001BF8 FE               [12] 2592 	mov	r6,a
      001BF9 30 E7 09         [24] 2593 	jnb	acc.7,00104$
                                   2594 ;	COMMON\easyax5043.c:262: AX5043_PLLRANGINGB = rng & 0x0F;
      001BFC 90 40 3B         [24] 2595 	mov	dptr,#_AX5043_PLLRANGINGB
      001BFF 74 0F            [12] 2596 	mov	a,#0x0f
      001C01 5F               [12] 2597 	anl	a,r7
      001C02 F0               [24] 2598 	movx	@dptr,a
      001C03 80 07            [24] 2599 	sjmp	00105$
      001C05                       2600 00104$:
                                   2601 ;	COMMON\easyax5043.c:264: AX5043_PLLRANGINGA = rng & 0x0F;
      001C05 90 40 33         [24] 2602 	mov	dptr,#_AX5043_PLLRANGINGA
      001C08 74 0F            [12] 2603 	mov	a,#0x0f
      001C0A 5F               [12] 2604 	anl	a,r7
      001C0B F0               [24] 2605 	movx	@dptr,a
      001C0C                       2606 00105$:
                                   2607 ;	COMMON\easyax5043.c:266: rng = axradio_get_pllvcoi();
      001C0C 12 46 48         [24] 2608 	lcall	_axradio_get_pllvcoi
      001C0F AE 82            [24] 2609 	mov	r6,dpl
      001C11 8E 07            [24] 2610 	mov	ar7,r6
                                   2611 ;	COMMON\easyax5043.c:267: if (rng & 0x80)
      001C13 EF               [12] 2612 	mov	a,r7
      001C14 30 E7 05         [24] 2613 	jnb	acc.7,00107$
                                   2614 ;	COMMON\easyax5043.c:268: AX5043_PLLVCOI = rng;
      001C17 90 41 80         [24] 2615 	mov	dptr,#_AX5043_PLLVCOI
      001C1A EF               [12] 2616 	mov	a,r7
      001C1B F0               [24] 2617 	movx	@dptr,a
      001C1C                       2618 00107$:
                                   2619 ;	COMMON\easyax5043.c:269: return AXRADIO_ERR_NOERROR;
      001C1C 75 82 00         [24] 2620 	mov	dpl,#0x00
      001C1F 22               [24] 2621 	ret
                                   2622 ;------------------------------------------------------------
                                   2623 ;Allocation info for local variables in function 'ax5043_init_registers_tx'
                                   2624 ;------------------------------------------------------------
                                   2625 ;	COMMON\easyax5043.c:272: __reentrantb uint8_t ax5043_init_registers_tx(void) __reentrant
                                   2626 ;	-----------------------------------------
                                   2627 ;	 function ax5043_init_registers_tx
                                   2628 ;	-----------------------------------------
      001C20                       2629 _ax5043_init_registers_tx:
                                   2630 ;	COMMON\easyax5043.c:274: ax5043_set_registers_tx();
      001C20 12 15 24         [24] 2631 	lcall	_ax5043_set_registers_tx
                                   2632 ;	COMMON\easyax5043.c:275: return ax5043_init_registers_common();
      001C23 02 1B DE         [24] 2633 	ljmp	_ax5043_init_registers_common
                                   2634 ;------------------------------------------------------------
                                   2635 ;Allocation info for local variables in function 'ax5043_init_registers_rx'
                                   2636 ;------------------------------------------------------------
                                   2637 ;	COMMON\easyax5043.c:278: __reentrantb uint8_t ax5043_init_registers_rx(void) __reentrant
                                   2638 ;	-----------------------------------------
                                   2639 ;	 function ax5043_init_registers_rx
                                   2640 ;	-----------------------------------------
      001C26                       2641 _ax5043_init_registers_rx:
                                   2642 ;	COMMON\easyax5043.c:280: ax5043_set_registers_rx();
      001C26 12 15 48         [24] 2643 	lcall	_ax5043_set_registers_rx
                                   2644 ;	COMMON\easyax5043.c:281: return ax5043_init_registers_common();
      001C29 02 1B DE         [24] 2645 	ljmp	_ax5043_init_registers_common
                                   2646 ;------------------------------------------------------------
                                   2647 ;Allocation info for local variables in function 'receive_isr'
                                   2648 ;------------------------------------------------------------
                                   2649 ;fifo_cmd                  Allocated to registers r6 
                                   2650 ;flags                     Allocated to registers 
                                   2651 ;i                         Allocated to registers r6 
                                   2652 ;len                       Allocated to registers r7 
                                   2653 ;r                         Allocated to registers r6 
                                   2654 ;r                         Allocated to registers r6 
                                   2655 ;r                         Allocated to registers r6 
                                   2656 ;BeaconRx                  Allocated with name '_receive_isr_BeaconRx_1_245'
                                   2657 ;------------------------------------------------------------
                                   2658 ;	COMMON\easyax5043.c:284: static __reentrantb void receive_isr(void) __reentrant
                                   2659 ;	-----------------------------------------
                                   2660 ;	 function receive_isr
                                   2661 ;	-----------------------------------------
      001C2C                       2662 _receive_isr:
                                   2663 ;	COMMON\easyax5043.c:288: uint8_t len = AX5043_RADIOEVENTREQ0; // clear request so interrupt does not fire again. sync_rx enables interrupt on radio state changed in order to wake up on SDF detected
      001C2C 90 40 0F         [24] 2664 	mov	dptr,#_AX5043_RADIOEVENTREQ0
      001C2F E0               [24] 2665 	movx	a,@dptr
      001C30 FF               [12] 2666 	mov	r7,a
                                   2667 ;	COMMON\easyax5043.c:290: dbglink_writestr(" receive_isr ");
      001C31 90 80 EB         [24] 2668 	mov	dptr,#___str_0
      001C34 75 F0 80         [24] 2669 	mov	b,#0x80
      001C37 C0 07            [24] 2670 	push	ar7
      001C39 12 74 0A         [24] 2671 	lcall	_dbglink_writestr
      001C3C D0 07            [24] 2672 	pop	ar7
                                   2673 ;	COMMON\easyax5043.c:291: if ((len & 0x04) && AX5043_RADIOSTATE == 0x0F) {
      001C3E EF               [12] 2674 	mov	a,r7
      001C3F 30 E2 51         [24] 2675 	jnb	acc.2,00169$
      001C42 90 40 1C         [24] 2676 	mov	dptr,#_AX5043_RADIOSTATE
      001C45 E0               [24] 2677 	movx	a,@dptr
      001C46 FE               [12] 2678 	mov	r6,a
      001C47 BE 0F 49         [24] 2679 	cjne	r6,#0x0f,00169$
                                   2680 ;	COMMON\easyax5043.c:293: dbglink_writestr(" time anchor ");
      001C4A 90 80 F9         [24] 2681 	mov	dptr,#___str_1
      001C4D 75 F0 80         [24] 2682 	mov	b,#0x80
      001C50 12 74 0A         [24] 2683 	lcall	_dbglink_writestr
                                   2684 ;	COMMON\easyax5043.c:294: update_timeanchor();
      001C53 12 1B 43         [24] 2685 	lcall	_update_timeanchor
                                   2686 ;	COMMON\easyax5043.c:295: if(axradio_framing_enable_sfdcallback)
      001C56 90 7F F9         [24] 2687 	mov	dptr,#_axradio_framing_enable_sfdcallback
      001C59 E4               [12] 2688 	clr	a
      001C5A 93               [24] 2689 	movc	a,@a+dptr
      001C5B 60 36            [24] 2690 	jz	00169$
                                   2691 ;	COMMON\easyax5043.c:297: dbglink_writestr(" enable_sfdcallback ");
      001C5D 90 81 07         [24] 2692 	mov	dptr,#___str_2
      001C60 75 F0 80         [24] 2693 	mov	b,#0x80
      001C63 12 74 0A         [24] 2694 	lcall	_dbglink_writestr
                                   2695 ;	COMMON\easyax5043.c:298: wtimer_remove_callback(&axradio_cb_receivesfd.cb);
      001C66 90 02 F6         [24] 2696 	mov	dptr,#_axradio_cb_receivesfd
      001C69 12 78 A9         [24] 2697 	lcall	_wtimer_remove_callback
                                   2698 ;	COMMON\easyax5043.c:299: axradio_cb_receivesfd.st.error = AXRADIO_ERR_NOERROR;
      001C6C 90 02 FB         [24] 2699 	mov	dptr,#(_axradio_cb_receivesfd + 0x0005)
      001C6F E4               [12] 2700 	clr	a
      001C70 F0               [24] 2701 	movx	@dptr,a
                                   2702 ;	COMMON\easyax5043.c:300: axradio_cb_receivesfd.st.time.t = axradio_timeanchor.radiotimer;
      001C71 90 00 BC         [24] 2703 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      001C74 E0               [24] 2704 	movx	a,@dptr
      001C75 FB               [12] 2705 	mov	r3,a
      001C76 A3               [24] 2706 	inc	dptr
      001C77 E0               [24] 2707 	movx	a,@dptr
      001C78 FC               [12] 2708 	mov	r4,a
      001C79 A3               [24] 2709 	inc	dptr
      001C7A E0               [24] 2710 	movx	a,@dptr
      001C7B FD               [12] 2711 	mov	r5,a
      001C7C A3               [24] 2712 	inc	dptr
      001C7D E0               [24] 2713 	movx	a,@dptr
      001C7E FE               [12] 2714 	mov	r6,a
      001C7F 90 02 FC         [24] 2715 	mov	dptr,#(_axradio_cb_receivesfd + 0x0006)
      001C82 EB               [12] 2716 	mov	a,r3
      001C83 F0               [24] 2717 	movx	@dptr,a
      001C84 EC               [12] 2718 	mov	a,r4
      001C85 A3               [24] 2719 	inc	dptr
      001C86 F0               [24] 2720 	movx	@dptr,a
      001C87 ED               [12] 2721 	mov	a,r5
      001C88 A3               [24] 2722 	inc	dptr
      001C89 F0               [24] 2723 	movx	@dptr,a
      001C8A EE               [12] 2724 	mov	a,r6
      001C8B A3               [24] 2725 	inc	dptr
      001C8C F0               [24] 2726 	movx	@dptr,a
                                   2727 ;	COMMON\easyax5043.c:301: wtimer_add_callback(&axradio_cb_receivesfd.cb);
      001C8D 90 02 F6         [24] 2728 	mov	dptr,#_axradio_cb_receivesfd
      001C90 12 6C C3         [24] 2729 	lcall	_wtimer_add_callback
                                   2730 ;	COMMON\easyax5043.c:313: while (AX5043_IRQREQUEST0 & 0x01) {    // while fifo not empty
      001C93                       2731 00169$:
      001C93                       2732 00153$:
      001C93 90 40 0D         [24] 2733 	mov	dptr,#_AX5043_IRQREQUEST0
      001C96 E0               [24] 2734 	movx	a,@dptr
      001C97 FE               [12] 2735 	mov	r6,a
      001C98 20 E0 01         [24] 2736 	jb	acc.0,00250$
      001C9B 22               [24] 2737 	ret
      001C9C                       2738 00250$:
                                   2739 ;	COMMON\easyax5043.c:315: fifo_cmd = AX5043_FIFODATA; // read command
      001C9C 90 40 29         [24] 2740 	mov	dptr,#_AX5043_FIFODATA
      001C9F E0               [24] 2741 	movx	a,@dptr
      001CA0 FE               [12] 2742 	mov	r6,a
                                   2743 ;	COMMON\easyax5043.c:316: len = (fifo_cmd & 0xE0) >> 5; // top 3 bits encode payload len
      001CA1 74 E0            [12] 2744 	mov	a,#0xe0
      001CA3 5E               [12] 2745 	anl	a,r6
      001CA4 FD               [12] 2746 	mov	r5,a
      001CA5 C4               [12] 2747 	swap	a
      001CA6 03               [12] 2748 	rr	a
      001CA7 54 07            [12] 2749 	anl	a,#0x07
      001CA9 FF               [12] 2750 	mov	r7,a
                                   2751 ;	COMMON\easyax5043.c:317: if (len == 7)
      001CAA BF 07 06         [24] 2752 	cjne	r7,#0x07,00107$
                                   2753 ;	COMMON\easyax5043.c:318: len = AX5043_FIFODATA; // 7 means variable length, -> get length byte
      001CAD 90 40 29         [24] 2754 	mov	dptr,#_AX5043_FIFODATA
      001CB0 E0               [24] 2755 	movx	a,@dptr
      001CB1 FD               [12] 2756 	mov	r5,a
      001CB2 FF               [12] 2757 	mov	r7,a
      001CB3                       2758 00107$:
                                   2759 ;	COMMON\easyax5043.c:319: fifo_cmd &= 0x1F;
      001CB3 53 06 1F         [24] 2760 	anl	ar6,#0x1f
                                   2761 ;	COMMON\easyax5043.c:320: switch (fifo_cmd) {
      001CB6 BE 01 02         [24] 2762 	cjne	r6,#0x01,00253$
      001CB9 80 21            [24] 2763 	sjmp	00108$
      001CBB                       2764 00253$:
      001CBB BE 10 03         [24] 2765 	cjne	r6,#0x10,00254$
      001CBE 02 1F 28         [24] 2766 	ljmp	00139$
      001CC1                       2767 00254$:
      001CC1 BE 11 03         [24] 2768 	cjne	r6,#0x11,00255$
      001CC4 02 1E FB         [24] 2769 	ljmp	00136$
      001CC7                       2770 00255$:
      001CC7 BE 12 03         [24] 2771 	cjne	r6,#0x12,00256$
      001CCA 02 1E AB         [24] 2772 	ljmp	00132$
      001CCD                       2773 00256$:
      001CCD BE 13 03         [24] 2774 	cjne	r6,#0x13,00257$
      001CD0 02 1E 64         [24] 2775 	ljmp	00128$
      001CD3                       2776 00257$:
      001CD3 BE 15 03         [24] 2777 	cjne	r6,#0x15,00258$
      001CD6 02 1F 51         [24] 2778 	ljmp	00142$
      001CD9                       2779 00258$:
      001CD9 02 1F C9         [24] 2780 	ljmp	00146$
                                   2781 ;	COMMON\easyax5043.c:321: case AX5043_FIFOCMD_DATA:
      001CDC                       2782 00108$:
                                   2783 ;	COMMON\easyax5043.c:323: if (!len)
      001CDC EF               [12] 2784 	mov	a,r7
      001CDD 60 B4            [24] 2785 	jz	00153$
                                   2786 ;	COMMON\easyax5043.c:326: flags = AX5043_FIFODATA;
      001CDF 90 40 29         [24] 2787 	mov	dptr,#_AX5043_FIFODATA
      001CE2 E0               [24] 2788 	movx	a,@dptr
                                   2789 ;	COMMON\easyax5043.c:327: --len;
      001CE3 1F               [12] 2790 	dec	r7
                                   2791 ;	COMMON\easyax5043.c:328: ax5043_readfifo(axradio_rxbuffer, len);
      001CE4 C0 07            [24] 2792 	push	ar7
      001CE6 C0 07            [24] 2793 	push	ar7
      001CE8 90 01 D0         [24] 2794 	mov	dptr,#_axradio_rxbuffer
      001CEB 75 F0 00         [24] 2795 	mov	b,#0x00
      001CEE 12 76 40         [24] 2796 	lcall	_ax5043_readfifo
      001CF1 15 81            [12] 2797 	dec	sp
      001CF3 D0 07            [24] 2798 	pop	ar7
                                   2799 ;	COMMON\easyax5043.c:329: if(axradio_mode == AXRADIO_MODE_WOR_RECEIVE || axradio_mode == AXRADIO_MODE_WOR_ACK_RECEIVE)
      001CF5 74 21            [12] 2800 	mov	a,#0x21
      001CF7 B5 0B 02         [24] 2801 	cjne	a,_axradio_mode,00260$
      001CFA 80 05            [24] 2802 	sjmp	00111$
      001CFC                       2803 00260$:
      001CFC 74 23            [12] 2804 	mov	a,#0x23
      001CFE B5 0B 21         [24] 2805 	cjne	a,_axradio_mode,00112$
      001D01                       2806 00111$:
                                   2807 ;	COMMON\easyax5043.c:331: f30_saved = AX5043_0xF30;
      001D01 90 4F 30         [24] 2808 	mov	dptr,#_AX5043_0xF30
      001D04 E0               [24] 2809 	movx	a,@dptr
      001D05 90 09 A5         [24] 2810 	mov	dptr,#_f30_saved
      001D08 F0               [24] 2811 	movx	@dptr,a
                                   2812 ;	COMMON\easyax5043.c:332: f31_saved = AX5043_0xF31;
      001D09 90 4F 31         [24] 2813 	mov	dptr,#_AX5043_0xF31
      001D0C E0               [24] 2814 	movx	a,@dptr
      001D0D 90 09 A6         [24] 2815 	mov	dptr,#_f31_saved
      001D10 F0               [24] 2816 	movx	@dptr,a
                                   2817 ;	COMMON\easyax5043.c:333: f32_saved = AX5043_0xF32;
      001D11 90 4F 32         [24] 2818 	mov	dptr,#_AX5043_0xF32
      001D14 E0               [24] 2819 	movx	a,@dptr
      001D15 90 09 A7         [24] 2820 	mov	dptr,#_f32_saved
      001D18 F0               [24] 2821 	movx	@dptr,a
                                   2822 ;	COMMON\easyax5043.c:334: f33_saved = AX5043_0xF33;
      001D19 90 4F 33         [24] 2823 	mov	dptr,#_AX5043_0xF33
      001D1C E0               [24] 2824 	movx	a,@dptr
      001D1D FE               [12] 2825 	mov	r6,a
      001D1E 90 09 A8         [24] 2826 	mov	dptr,#_f33_saved
      001D21 F0               [24] 2827 	movx	@dptr,a
      001D22                       2828 00112$:
                                   2829 ;	COMMON\easyax5043.c:336: if (axradio_mode == AXRADIO_MODE_WOR_RECEIVE ||
      001D22 74 21            [12] 2830 	mov	a,#0x21
      001D24 B5 0B 02         [24] 2831 	cjne	a,_axradio_mode,00263$
      001D27 80 05            [24] 2832 	sjmp	00114$
      001D29                       2833 00263$:
                                   2834 ;	COMMON\easyax5043.c:337: axradio_mode == AXRADIO_MODE_SYNC_SLAVE)
      001D29 74 32            [12] 2835 	mov	a,#0x32
      001D2B B5 0B 05         [24] 2836 	cjne	a,_axradio_mode,00115$
      001D2E                       2837 00114$:
                                   2838 ;	COMMON\easyax5043.c:338: AX5043_PWRMODE = AX5043_PWRSTATE_POWERDOWN;
      001D2E 90 40 02         [24] 2839 	mov	dptr,#_AX5043_PWRMODE
      001D31 E4               [12] 2840 	clr	a
      001D32 F0               [24] 2841 	movx	@dptr,a
      001D33                       2842 00115$:
                                   2843 ;	COMMON\easyax5043.c:339: AX5043_IRQMASK0 &= (uint8_t)~0x01; // disable FIFO not empty irq
      001D33 90 40 07         [24] 2844 	mov	dptr,#_AX5043_IRQMASK0
      001D36 E0               [24] 2845 	movx	a,@dptr
      001D37 FE               [12] 2846 	mov	r6,a
      001D38 74 FE            [12] 2847 	mov	a,#0xfe
      001D3A 5E               [12] 2848 	anl	a,r6
      001D3B F0               [24] 2849 	movx	@dptr,a
                                   2850 ;	COMMON\easyax5043.c:340: wtimer_remove_callback(&axradio_cb_receive.cb);
      001D3C 90 02 D4         [24] 2851 	mov	dptr,#_axradio_cb_receive
      001D3F C0 07            [24] 2852 	push	ar7
      001D41 12 78 A9         [24] 2853 	lcall	_wtimer_remove_callback
      001D44 D0 07            [24] 2854 	pop	ar7
                                   2855 ;	COMMON\easyax5043.c:341: axradio_cb_receive.st.error = AXRADIO_ERR_NOERROR;
      001D46 90 02 D9         [24] 2856 	mov	dptr,#(_axradio_cb_receive + 0x0005)
      001D49 E4               [12] 2857 	clr	a
      001D4A F0               [24] 2858 	movx	@dptr,a
                                   2859 ;	COMMON\easyax5043.c:343: axradio_cb_receive.st.rx.mac.raw = axradio_rxbuffer;
      001D4B 90 02 F0         [24] 2860 	mov	dptr,#(_axradio_cb_receive + 0x001c)
      001D4E 74 D0            [12] 2861 	mov	a,#_axradio_rxbuffer
      001D50 F0               [24] 2862 	movx	@dptr,a
      001D51 74 01            [12] 2863 	mov	a,#(_axradio_rxbuffer >> 8)
      001D53 A3               [24] 2864 	inc	dptr
      001D54 F0               [24] 2865 	movx	@dptr,a
                                   2866 ;	COMMON\easyax5043.c:344: if (AXRADIO_MODE_IS_STREAM_RECEIVE(axradio_mode)) {
      001D55 74 F8            [12] 2867 	mov	a,#0xf8
      001D57 55 0B            [12] 2868 	anl	a,_axradio_mode
      001D59 FE               [12] 2869 	mov	r6,a
      001D5A BE 28 02         [24] 2870 	cjne	r6,#0x28,00266$
      001D5D 80 03            [24] 2871 	sjmp	00267$
      001D5F                       2872 00266$:
      001D5F 02 1D F5         [24] 2873 	ljmp	00121$
      001D62                       2874 00267$:
                                   2875 ;	COMMON\easyax5043.c:345: axradio_cb_receive.st.rx.pktdata = axradio_rxbuffer;
      001D62 90 02 F2         [24] 2876 	mov	dptr,#(_axradio_cb_receive + 0x001e)
      001D65 74 D0            [12] 2877 	mov	a,#_axradio_rxbuffer
      001D67 F0               [24] 2878 	movx	@dptr,a
      001D68 74 01            [12] 2879 	mov	a,#(_axradio_rxbuffer >> 8)
      001D6A A3               [24] 2880 	inc	dptr
      001D6B F0               [24] 2881 	movx	@dptr,a
                                   2882 ;	COMMON\easyax5043.c:346: axradio_cb_receive.st.rx.pktlen = len;
      001D6C 8F 05            [24] 2883 	mov	ar5,r7
      001D6E 7E 00            [12] 2884 	mov	r6,#0x00
      001D70 90 02 F4         [24] 2885 	mov	dptr,#(_axradio_cb_receive + 0x0020)
      001D73 ED               [12] 2886 	mov	a,r5
      001D74 F0               [24] 2887 	movx	@dptr,a
      001D75 EE               [12] 2888 	mov	a,r6
      001D76 A3               [24] 2889 	inc	dptr
      001D77 F0               [24] 2890 	movx	@dptr,a
                                   2891 ;	COMMON\easyax5043.c:348: int8_t r = AX5043_RSSI;
      001D78 90 40 40         [24] 2892 	mov	dptr,#_AX5043_RSSI
      001D7B E0               [24] 2893 	movx	a,@dptr
                                   2894 ;	COMMON\easyax5043.c:349: axradio_cb_receive.st.rx.phy.rssi = r - (int16_t)axradio_phy_rssioffset;
      001D7C FE               [12] 2895 	mov	r6,a
      001D7D 33               [12] 2896 	rlc	a
      001D7E 95 E0            [12] 2897 	subb	a,acc
      001D80 FD               [12] 2898 	mov	r5,a
      001D81 90 7F D7         [24] 2899 	mov	dptr,#_axradio_phy_rssioffset
      001D84 E4               [12] 2900 	clr	a
      001D85 93               [24] 2901 	movc	a,@a+dptr
      001D86 FC               [12] 2902 	mov	r4,a
      001D87 33               [12] 2903 	rlc	a
      001D88 95 E0            [12] 2904 	subb	a,acc
      001D8A FB               [12] 2905 	mov	r3,a
      001D8B EE               [12] 2906 	mov	a,r6
      001D8C C3               [12] 2907 	clr	c
      001D8D 9C               [12] 2908 	subb	a,r4
      001D8E FE               [12] 2909 	mov	r6,a
      001D8F ED               [12] 2910 	mov	a,r5
      001D90 9B               [12] 2911 	subb	a,r3
      001D91 FD               [12] 2912 	mov	r5,a
      001D92 90 02 DE         [24] 2913 	mov	dptr,#(_axradio_cb_receive + 0x000a)
      001D95 EE               [12] 2914 	mov	a,r6
      001D96 F0               [24] 2915 	movx	@dptr,a
      001D97 ED               [12] 2916 	mov	a,r5
      001D98 A3               [24] 2917 	inc	dptr
      001D99 F0               [24] 2918 	movx	@dptr,a
                                   2919 ;	COMMON\easyax5043.c:351: if (axradio_phy_innerfreqloop)
      001D9A 90 7F C9         [24] 2920 	mov	dptr,#_axradio_phy_innerfreqloop
      001D9D E4               [12] 2921 	clr	a
      001D9E 93               [24] 2922 	movc	a,@a+dptr
      001D9F 60 28            [24] 2923 	jz	00118$
                                   2924 ;	COMMON\easyax5043.c:352: axradio_cb_receive.st.rx.phy.offset.o = axradio_conv_freq_fromreg(signextend16(radio_read16((uint16_t)&AX5043_TRKFREQ1)));
      001DA1 7D 50            [12] 2925 	mov	r5,#_AX5043_TRKFREQ1
      001DA3 7E 40            [12] 2926 	mov	r6,#(_AX5043_TRKFREQ1 >> 8)
      001DA5 8D 82            [24] 2927 	mov	dpl,r5
      001DA7 8E 83            [24] 2928 	mov	dph,r6
      001DA9 12 6F 1E         [24] 2929 	lcall	_radio_read16
      001DAC 12 7D DE         [24] 2930 	lcall	_signextend16
      001DAF 12 17 26         [24] 2931 	lcall	_axradio_conv_freq_fromreg
      001DB2 AB 82            [24] 2932 	mov	r3,dpl
      001DB4 AC 83            [24] 2933 	mov	r4,dph
      001DB6 AD F0            [24] 2934 	mov	r5,b
      001DB8 FE               [12] 2935 	mov	r6,a
      001DB9 90 02 E0         [24] 2936 	mov	dptr,#(_axradio_cb_receive + 0x000c)
      001DBC EB               [12] 2937 	mov	a,r3
      001DBD F0               [24] 2938 	movx	@dptr,a
      001DBE EC               [12] 2939 	mov	a,r4
      001DBF A3               [24] 2940 	inc	dptr
      001DC0 F0               [24] 2941 	movx	@dptr,a
      001DC1 ED               [12] 2942 	mov	a,r5
      001DC2 A3               [24] 2943 	inc	dptr
      001DC3 F0               [24] 2944 	movx	@dptr,a
      001DC4 EE               [12] 2945 	mov	a,r6
      001DC5 A3               [24] 2946 	inc	dptr
      001DC6 F0               [24] 2947 	movx	@dptr,a
      001DC7 80 23            [24] 2948 	sjmp	00119$
      001DC9                       2949 00118$:
                                   2950 ;	COMMON\easyax5043.c:354: axradio_cb_receive.st.rx.phy.offset.o = signextend20(radio_read24((uint16_t)&AX5043_TRKRFFREQ2));
      001DC9 7D 4D            [12] 2951 	mov	r5,#_AX5043_TRKRFFREQ2
      001DCB 7E 40            [12] 2952 	mov	r6,#(_AX5043_TRKRFFREQ2 >> 8)
      001DCD 8D 82            [24] 2953 	mov	dpl,r5
      001DCF 8E 83            [24] 2954 	mov	dph,r6
      001DD1 12 6D E5         [24] 2955 	lcall	_radio_read24
      001DD4 12 7C 67         [24] 2956 	lcall	_signextend20
      001DD7 AB 82            [24] 2957 	mov	r3,dpl
      001DD9 AC 83            [24] 2958 	mov	r4,dph
      001DDB AD F0            [24] 2959 	mov	r5,b
      001DDD FE               [12] 2960 	mov	r6,a
      001DDE 90 02 E0         [24] 2961 	mov	dptr,#(_axradio_cb_receive + 0x000c)
      001DE1 EB               [12] 2962 	mov	a,r3
      001DE2 F0               [24] 2963 	movx	@dptr,a
      001DE3 EC               [12] 2964 	mov	a,r4
      001DE4 A3               [24] 2965 	inc	dptr
      001DE5 F0               [24] 2966 	movx	@dptr,a
      001DE6 ED               [12] 2967 	mov	a,r5
      001DE7 A3               [24] 2968 	inc	dptr
      001DE8 F0               [24] 2969 	movx	@dptr,a
      001DE9 EE               [12] 2970 	mov	a,r6
      001DEA A3               [24] 2971 	inc	dptr
      001DEB F0               [24] 2972 	movx	@dptr,a
      001DEC                       2973 00119$:
                                   2974 ;	COMMON\easyax5043.c:355: wtimer_add_callback(&axradio_cb_receive.cb);
      001DEC 90 02 D4         [24] 2975 	mov	dptr,#_axradio_cb_receive
      001DEF 12 6C C3         [24] 2976 	lcall	_wtimer_add_callback
                                   2977 ;	COMMON\easyax5043.c:356: break;
      001DF2 02 1C 93         [24] 2978 	ljmp	00153$
      001DF5                       2979 00121$:
                                   2980 ;	COMMON\easyax5043.c:358: axradio_cb_receive.st.rx.pktdata = &axradio_rxbuffer[axradio_framing_maclen];
      001DF5 90 7F EB         [24] 2981 	mov	dptr,#_axradio_framing_maclen
      001DF8 E4               [12] 2982 	clr	a
      001DF9 93               [24] 2983 	movc	a,@a+dptr
      001DFA FE               [12] 2984 	mov	r6,a
      001DFB 24 D0            [12] 2985 	add	a,#_axradio_rxbuffer
      001DFD FC               [12] 2986 	mov	r4,a
      001DFE E4               [12] 2987 	clr	a
      001DFF 34 01            [12] 2988 	addc	a,#(_axradio_rxbuffer >> 8)
      001E01 FD               [12] 2989 	mov	r5,a
      001E02 90 02 F2         [24] 2990 	mov	dptr,#(_axradio_cb_receive + 0x001e)
      001E05 EC               [12] 2991 	mov	a,r4
      001E06 F0               [24] 2992 	movx	@dptr,a
      001E07 ED               [12] 2993 	mov	a,r5
      001E08 A3               [24] 2994 	inc	dptr
      001E09 F0               [24] 2995 	movx	@dptr,a
                                   2996 ;	COMMON\easyax5043.c:359: if (len < axradio_framing_maclen) {
      001E0A C3               [12] 2997 	clr	c
      001E0B EF               [12] 2998 	mov	a,r7
      001E0C 9E               [12] 2999 	subb	a,r6
      001E0D 50 0B            [24] 3000 	jnc	00126$
                                   3001 ;	COMMON\easyax5043.c:360: len = 0;
      001E0F 7F 00            [12] 3002 	mov	r7,#0x00
                                   3003 ;	COMMON\easyax5043.c:361: axradio_cb_receive.st.rx.pktlen = 0;
      001E11 90 02 F4         [24] 3004 	mov	dptr,#(_axradio_cb_receive + 0x0020)
      001E14 E4               [12] 3005 	clr	a
      001E15 F0               [24] 3006 	movx	@dptr,a
      001E16 A3               [24] 3007 	inc	dptr
      001E17 F0               [24] 3008 	movx	@dptr,a
      001E18 80 2F            [24] 3009 	sjmp	00127$
      001E1A                       3010 00126$:
                                   3011 ;	COMMON\easyax5043.c:363: len -= axradio_framing_maclen;
      001E1A EF               [12] 3012 	mov	a,r7
      001E1B C3               [12] 3013 	clr	c
      001E1C 9E               [12] 3014 	subb	a,r6
                                   3015 ;	COMMON\easyax5043.c:364: axradio_cb_receive.st.rx.pktlen = len;
      001E1D FF               [12] 3016 	mov	r7,a
      001E1E FD               [12] 3017 	mov	r5,a
      001E1F 7E 00            [12] 3018 	mov	r6,#0x00
      001E21 90 02 F4         [24] 3019 	mov	dptr,#(_axradio_cb_receive + 0x0020)
      001E24 ED               [12] 3020 	mov	a,r5
      001E25 F0               [24] 3021 	movx	@dptr,a
      001E26 EE               [12] 3022 	mov	a,r6
      001E27 A3               [24] 3023 	inc	dptr
      001E28 F0               [24] 3024 	movx	@dptr,a
                                   3025 ;	COMMON\easyax5043.c:365: wtimer_add_callback(&axradio_cb_receive.cb);
      001E29 90 02 D4         [24] 3026 	mov	dptr,#_axradio_cb_receive
      001E2C C0 07            [24] 3027 	push	ar7
      001E2E 12 6C C3         [24] 3028 	lcall	_wtimer_add_callback
      001E31 D0 07            [24] 3029 	pop	ar7
                                   3030 ;	COMMON\easyax5043.c:366: if (axradio_mode == AXRADIO_MODE_SYNC_SLAVE ||
      001E33 74 32            [12] 3031 	mov	a,#0x32
      001E35 B5 0B 02         [24] 3032 	cjne	a,_axradio_mode,00270$
      001E38 80 05            [24] 3033 	sjmp	00122$
      001E3A                       3034 00270$:
                                   3035 ;	COMMON\easyax5043.c:367: axradio_mode == AXRADIO_MODE_SYNC_ACK_SLAVE)
      001E3A 74 33            [12] 3036 	mov	a,#0x33
      001E3C B5 0B 0A         [24] 3037 	cjne	a,_axradio_mode,00127$
      001E3F                       3038 00122$:
                                   3039 ;	COMMON\easyax5043.c:368: wtimer_remove(&axradio_timer);
      001E3F 90 03 2B         [24] 3040 	mov	dptr,#_axradio_timer
      001E42 C0 07            [24] 3041 	push	ar7
      001E44 12 76 94         [24] 3042 	lcall	_wtimer_remove
      001E47 D0 07            [24] 3043 	pop	ar7
      001E49                       3044 00127$:
                                   3045 ;	COMMON\easyax5043.c:370: BEACON_decoding(axradio_rxbuffer+3,&BeaconRx,len);
      001E49 90 03 72         [24] 3046 	mov	dptr,#_BEACON_decoding_PARM_2
      001E4C 74 33            [12] 3047 	mov	a,#_receive_isr_BeaconRx_1_245
      001E4E F0               [24] 3048 	movx	@dptr,a
      001E4F 74 03            [12] 3049 	mov	a,#(_receive_isr_BeaconRx_1_245 >> 8)
      001E51 A3               [24] 3050 	inc	dptr
      001E52 F0               [24] 3051 	movx	@dptr,a
      001E53 90 03 74         [24] 3052 	mov	dptr,#_BEACON_decoding_PARM_3
      001E56 EF               [12] 3053 	mov	a,r7
      001E57 F0               [24] 3054 	movx	@dptr,a
      001E58 90 01 D3         [24] 3055 	mov	dptr,#(_axradio_rxbuffer + 0x0003)
      001E5B 75 F0 00         [24] 3056 	mov	b,#0x00
      001E5E 12 4C 78         [24] 3057 	lcall	_BEACON_decoding
                                   3058 ;	COMMON\easyax5043.c:371: break;
      001E61 02 1C 93         [24] 3059 	ljmp	00153$
                                   3060 ;	COMMON\easyax5043.c:373: case AX5043_FIFOCMD_RFFREQOFFS:
      001E64                       3061 00128$:
                                   3062 ;	COMMON\easyax5043.c:374: if (axradio_phy_innerfreqloop || len != 3)
      001E64 90 7F C9         [24] 3063 	mov	dptr,#_axradio_phy_innerfreqloop
      001E67 E4               [12] 3064 	clr	a
      001E68 93               [24] 3065 	movc	a,@a+dptr
      001E69 60 03            [24] 3066 	jz	00273$
      001E6B 02 1F C9         [24] 3067 	ljmp	00146$
      001E6E                       3068 00273$:
      001E6E BF 03 02         [24] 3069 	cjne	r7,#0x03,00274$
      001E71 80 03            [24] 3070 	sjmp	00275$
      001E73                       3071 00274$:
      001E73 02 1F C9         [24] 3072 	ljmp	00146$
      001E76                       3073 00275$:
                                   3074 ;	COMMON\easyax5043.c:376: i = AX5043_FIFODATA;
      001E76 90 40 29         [24] 3075 	mov	dptr,#_AX5043_FIFODATA
      001E79 E0               [24] 3076 	movx	a,@dptr
      001E7A FE               [12] 3077 	mov	r6,a
                                   3078 ;	COMMON\easyax5043.c:377: i &= 0x0F;
      001E7B 53 06 0F         [24] 3079 	anl	ar6,#0x0f
                                   3080 ;	COMMON\easyax5043.c:378: i |= 1 + (uint8_t)~(i & 0x08);
      001E7E 74 08            [12] 3081 	mov	a,#0x08
      001E80 5E               [12] 3082 	anl	a,r6
      001E81 F4               [12] 3083 	cpl	a
      001E82 FD               [12] 3084 	mov	r5,a
      001E83 0D               [12] 3085 	inc	r5
      001E84 ED               [12] 3086 	mov	a,r5
      001E85 42 06            [12] 3087 	orl	ar6,a
                                   3088 ;	COMMON\easyax5043.c:379: axradio_cb_receive.st.rx.phy.offset.b.b3 = ((int8_t)i) >> 8;
      001E87 8E 05            [24] 3089 	mov	ar5,r6
      001E89 ED               [12] 3090 	mov	a,r5
      001E8A 33               [12] 3091 	rlc	a
      001E8B 95 E0            [12] 3092 	subb	a,acc
      001E8D FD               [12] 3093 	mov	r5,a
      001E8E 90 02 E3         [24] 3094 	mov	dptr,#(_axradio_cb_receive + 0x000f)
      001E91 F0               [24] 3095 	movx	@dptr,a
                                   3096 ;	COMMON\easyax5043.c:380: axradio_cb_receive.st.rx.phy.offset.b.b2 = i;
      001E92 90 02 E2         [24] 3097 	mov	dptr,#(_axradio_cb_receive + 0x000e)
      001E95 EE               [12] 3098 	mov	a,r6
      001E96 F0               [24] 3099 	movx	@dptr,a
                                   3100 ;	COMMON\easyax5043.c:381: axradio_cb_receive.st.rx.phy.offset.b.b1 = AX5043_FIFODATA;
      001E97 90 40 29         [24] 3101 	mov	dptr,#_AX5043_FIFODATA
      001E9A E0               [24] 3102 	movx	a,@dptr
      001E9B 90 02 E1         [24] 3103 	mov	dptr,#(_axradio_cb_receive + 0x000d)
      001E9E F0               [24] 3104 	movx	@dptr,a
                                   3105 ;	COMMON\easyax5043.c:382: axradio_cb_receive.st.rx.phy.offset.b.b0 = AX5043_FIFODATA;
      001E9F 90 40 29         [24] 3106 	mov	dptr,#_AX5043_FIFODATA
      001EA2 E0               [24] 3107 	movx	a,@dptr
      001EA3 FE               [12] 3108 	mov	r6,a
      001EA4 90 02 E0         [24] 3109 	mov	dptr,#(_axradio_cb_receive + 0x000c)
      001EA7 F0               [24] 3110 	movx	@dptr,a
                                   3111 ;	COMMON\easyax5043.c:383: break;
      001EA8 02 1C 93         [24] 3112 	ljmp	00153$
                                   3113 ;	COMMON\easyax5043.c:385: case AX5043_FIFOCMD_FREQOFFS:
      001EAB                       3114 00132$:
                                   3115 ;	COMMON\easyax5043.c:386: if (!axradio_phy_innerfreqloop || len != 2)
      001EAB 90 7F C9         [24] 3116 	mov	dptr,#_axradio_phy_innerfreqloop
      001EAE E4               [12] 3117 	clr	a
      001EAF 93               [24] 3118 	movc	a,@a+dptr
      001EB0 70 03            [24] 3119 	jnz	00276$
      001EB2 02 1F C9         [24] 3120 	ljmp	00146$
      001EB5                       3121 00276$:
      001EB5 BF 02 02         [24] 3122 	cjne	r7,#0x02,00277$
      001EB8 80 03            [24] 3123 	sjmp	00278$
      001EBA                       3124 00277$:
      001EBA 02 1F C9         [24] 3125 	ljmp	00146$
      001EBD                       3126 00278$:
                                   3127 ;	COMMON\easyax5043.c:388: axradio_cb_receive.st.rx.phy.offset.b.b1 = AX5043_FIFODATA;
      001EBD 90 40 29         [24] 3128 	mov	dptr,#_AX5043_FIFODATA
      001EC0 E0               [24] 3129 	movx	a,@dptr
      001EC1 90 02 E1         [24] 3130 	mov	dptr,#(_axradio_cb_receive + 0x000d)
      001EC4 F0               [24] 3131 	movx	@dptr,a
                                   3132 ;	COMMON\easyax5043.c:389: axradio_cb_receive.st.rx.phy.offset.b.b0 = AX5043_FIFODATA;
      001EC5 90 40 29         [24] 3133 	mov	dptr,#_AX5043_FIFODATA
      001EC8 E0               [24] 3134 	movx	a,@dptr
      001EC9 90 02 E0         [24] 3135 	mov	dptr,#(_axradio_cb_receive + 0x000c)
      001ECC F0               [24] 3136 	movx	@dptr,a
                                   3137 ;	COMMON\easyax5043.c:390: axradio_cb_receive.st.rx.phy.offset.o = axradio_conv_freq_fromreg(signextend16(axradio_cb_receive.st.rx.phy.offset.o));
      001ECD 90 02 E0         [24] 3138 	mov	dptr,#(_axradio_cb_receive + 0x000c)
      001ED0 E0               [24] 3139 	movx	a,@dptr
      001ED1 FB               [12] 3140 	mov	r3,a
      001ED2 A3               [24] 3141 	inc	dptr
      001ED3 E0               [24] 3142 	movx	a,@dptr
      001ED4 FC               [12] 3143 	mov	r4,a
      001ED5 A3               [24] 3144 	inc	dptr
      001ED6 E0               [24] 3145 	movx	a,@dptr
      001ED7 A3               [24] 3146 	inc	dptr
      001ED8 E0               [24] 3147 	movx	a,@dptr
      001ED9 8B 82            [24] 3148 	mov	dpl,r3
      001EDB 8C 83            [24] 3149 	mov	dph,r4
      001EDD 12 7D DE         [24] 3150 	lcall	_signextend16
      001EE0 12 17 26         [24] 3151 	lcall	_axradio_conv_freq_fromreg
      001EE3 AB 82            [24] 3152 	mov	r3,dpl
      001EE5 AC 83            [24] 3153 	mov	r4,dph
      001EE7 AD F0            [24] 3154 	mov	r5,b
      001EE9 FE               [12] 3155 	mov	r6,a
      001EEA 90 02 E0         [24] 3156 	mov	dptr,#(_axradio_cb_receive + 0x000c)
      001EED EB               [12] 3157 	mov	a,r3
      001EEE F0               [24] 3158 	movx	@dptr,a
      001EEF EC               [12] 3159 	mov	a,r4
      001EF0 A3               [24] 3160 	inc	dptr
      001EF1 F0               [24] 3161 	movx	@dptr,a
      001EF2 ED               [12] 3162 	mov	a,r5
      001EF3 A3               [24] 3163 	inc	dptr
      001EF4 F0               [24] 3164 	movx	@dptr,a
      001EF5 EE               [12] 3165 	mov	a,r6
      001EF6 A3               [24] 3166 	inc	dptr
      001EF7 F0               [24] 3167 	movx	@dptr,a
                                   3168 ;	COMMON\easyax5043.c:391: break;
      001EF8 02 1C 93         [24] 3169 	ljmp	00153$
                                   3170 ;	COMMON\easyax5043.c:393: case AX5043_FIFOCMD_RSSI:
      001EFB                       3171 00136$:
                                   3172 ;	COMMON\easyax5043.c:394: if (len != 1)
      001EFB BF 01 02         [24] 3173 	cjne	r7,#0x01,00279$
      001EFE 80 03            [24] 3174 	sjmp	00280$
      001F00                       3175 00279$:
      001F00 02 1F C9         [24] 3176 	ljmp	00146$
      001F03                       3177 00280$:
                                   3178 ;	COMMON\easyax5043.c:397: int8_t r = AX5043_FIFODATA;
      001F03 90 40 29         [24] 3179 	mov	dptr,#_AX5043_FIFODATA
      001F06 E0               [24] 3180 	movx	a,@dptr
                                   3181 ;	COMMON\easyax5043.c:398: axradio_cb_receive.st.rx.phy.rssi = r - (int16_t)axradio_phy_rssioffset;
      001F07 FE               [12] 3182 	mov	r6,a
      001F08 33               [12] 3183 	rlc	a
      001F09 95 E0            [12] 3184 	subb	a,acc
      001F0B FD               [12] 3185 	mov	r5,a
      001F0C 90 7F D7         [24] 3186 	mov	dptr,#_axradio_phy_rssioffset
      001F0F E4               [12] 3187 	clr	a
      001F10 93               [24] 3188 	movc	a,@a+dptr
      001F11 FC               [12] 3189 	mov	r4,a
      001F12 33               [12] 3190 	rlc	a
      001F13 95 E0            [12] 3191 	subb	a,acc
      001F15 FB               [12] 3192 	mov	r3,a
      001F16 EE               [12] 3193 	mov	a,r6
      001F17 C3               [12] 3194 	clr	c
      001F18 9C               [12] 3195 	subb	a,r4
      001F19 FE               [12] 3196 	mov	r6,a
      001F1A ED               [12] 3197 	mov	a,r5
      001F1B 9B               [12] 3198 	subb	a,r3
      001F1C FD               [12] 3199 	mov	r5,a
      001F1D 90 02 DE         [24] 3200 	mov	dptr,#(_axradio_cb_receive + 0x000a)
      001F20 EE               [12] 3201 	mov	a,r6
      001F21 F0               [24] 3202 	movx	@dptr,a
      001F22 ED               [12] 3203 	mov	a,r5
      001F23 A3               [24] 3204 	inc	dptr
      001F24 F0               [24] 3205 	movx	@dptr,a
                                   3206 ;	COMMON\easyax5043.c:400: break;
      001F25 02 1C 93         [24] 3207 	ljmp	00153$
                                   3208 ;	COMMON\easyax5043.c:402: case AX5043_FIFOCMD_TIMER:
      001F28                       3209 00139$:
                                   3210 ;	COMMON\easyax5043.c:403: if (len != 3)
      001F28 BF 03 02         [24] 3211 	cjne	r7,#0x03,00281$
      001F2B 80 03            [24] 3212 	sjmp	00282$
      001F2D                       3213 00281$:
      001F2D 02 1F C9         [24] 3214 	ljmp	00146$
      001F30                       3215 00282$:
                                   3216 ;	COMMON\easyax5043.c:407: axradio_cb_receive.st.time.b.b3 = 0;
      001F30 90 02 DD         [24] 3217 	mov	dptr,#(_axradio_cb_receive + 0x0009)
      001F33 E4               [12] 3218 	clr	a
      001F34 F0               [24] 3219 	movx	@dptr,a
                                   3220 ;	COMMON\easyax5043.c:408: axradio_cb_receive.st.time.b.b2 = AX5043_FIFODATA;
      001F35 90 40 29         [24] 3221 	mov	dptr,#_AX5043_FIFODATA
      001F38 E0               [24] 3222 	movx	a,@dptr
      001F39 90 02 DC         [24] 3223 	mov	dptr,#(_axradio_cb_receive + 0x0008)
      001F3C F0               [24] 3224 	movx	@dptr,a
                                   3225 ;	COMMON\easyax5043.c:409: axradio_cb_receive.st.time.b.b1 = AX5043_FIFODATA;
      001F3D 90 40 29         [24] 3226 	mov	dptr,#_AX5043_FIFODATA
      001F40 E0               [24] 3227 	movx	a,@dptr
      001F41 90 02 DB         [24] 3228 	mov	dptr,#(_axradio_cb_receive + 0x0007)
      001F44 F0               [24] 3229 	movx	@dptr,a
                                   3230 ;	COMMON\easyax5043.c:410: axradio_cb_receive.st.time.b.b0 = AX5043_FIFODATA;
      001F45 90 40 29         [24] 3231 	mov	dptr,#_AX5043_FIFODATA
      001F48 E0               [24] 3232 	movx	a,@dptr
      001F49 FE               [12] 3233 	mov	r6,a
      001F4A 90 02 DA         [24] 3234 	mov	dptr,#(_axradio_cb_receive + 0x0006)
      001F4D F0               [24] 3235 	movx	@dptr,a
                                   3236 ;	COMMON\easyax5043.c:411: break;
      001F4E 02 1C 93         [24] 3237 	ljmp	00153$
                                   3238 ;	COMMON\easyax5043.c:413: case AX5043_FIFOCMD_ANTRSSI:
      001F51                       3239 00142$:
                                   3240 ;	COMMON\easyax5043.c:414: if (!len)
      001F51 EF               [12] 3241 	mov	a,r7
      001F52 70 03            [24] 3242 	jnz	00283$
      001F54 02 1C 93         [24] 3243 	ljmp	00153$
      001F57                       3244 00283$:
                                   3245 ;	COMMON\easyax5043.c:416: update_timeanchor();
      001F57 C0 07            [24] 3246 	push	ar7
      001F59 12 1B 43         [24] 3247 	lcall	_update_timeanchor
                                   3248 ;	COMMON\easyax5043.c:417: wtimer_remove_callback(&axradio_cb_channelstate.cb);
      001F5C 90 03 00         [24] 3249 	mov	dptr,#_axradio_cb_channelstate
      001F5F 12 78 A9         [24] 3250 	lcall	_wtimer_remove_callback
                                   3251 ;	COMMON\easyax5043.c:418: axradio_cb_channelstate.st.error = AXRADIO_ERR_NOERROR;
      001F62 90 03 05         [24] 3252 	mov	dptr,#(_axradio_cb_channelstate + 0x0005)
      001F65 E4               [12] 3253 	clr	a
      001F66 F0               [24] 3254 	movx	@dptr,a
                                   3255 ;	COMMON\easyax5043.c:420: int8_t r = AX5043_FIFODATA;
      001F67 90 40 29         [24] 3256 	mov	dptr,#_AX5043_FIFODATA
      001F6A E0               [24] 3257 	movx	a,@dptr
                                   3258 ;	COMMON\easyax5043.c:421: axradio_cb_channelstate.st.cs.rssi = r - (int16_t)axradio_phy_rssioffset;
      001F6B FE               [12] 3259 	mov	r6,a
      001F6C FC               [12] 3260 	mov	r4,a
      001F6D 33               [12] 3261 	rlc	a
      001F6E 95 E0            [12] 3262 	subb	a,acc
      001F70 FD               [12] 3263 	mov	r5,a
      001F71 90 7F D7         [24] 3264 	mov	dptr,#_axradio_phy_rssioffset
      001F74 E4               [12] 3265 	clr	a
      001F75 93               [24] 3266 	movc	a,@a+dptr
      001F76 FB               [12] 3267 	mov	r3,a
      001F77 33               [12] 3268 	rlc	a
      001F78 95 E0            [12] 3269 	subb	a,acc
      001F7A FA               [12] 3270 	mov	r2,a
      001F7B EC               [12] 3271 	mov	a,r4
      001F7C C3               [12] 3272 	clr	c
      001F7D 9B               [12] 3273 	subb	a,r3
      001F7E FC               [12] 3274 	mov	r4,a
      001F7F ED               [12] 3275 	mov	a,r5
      001F80 9A               [12] 3276 	subb	a,r2
      001F81 FD               [12] 3277 	mov	r5,a
      001F82 90 03 0A         [24] 3278 	mov	dptr,#(_axradio_cb_channelstate + 0x000a)
      001F85 EC               [12] 3279 	mov	a,r4
      001F86 F0               [24] 3280 	movx	@dptr,a
      001F87 ED               [12] 3281 	mov	a,r5
      001F88 A3               [24] 3282 	inc	dptr
      001F89 F0               [24] 3283 	movx	@dptr,a
                                   3284 ;	COMMON\easyax5043.c:422: axradio_cb_channelstate.st.cs.busy = r >= axradio_phy_channelbusy;
      001F8A 90 7F D9         [24] 3285 	mov	dptr,#_axradio_phy_channelbusy
      001F8D E4               [12] 3286 	clr	a
      001F8E 93               [24] 3287 	movc	a,@a+dptr
      001F8F FD               [12] 3288 	mov	r5,a
      001F90 C3               [12] 3289 	clr	c
      001F91 EE               [12] 3290 	mov	a,r6
      001F92 64 80            [12] 3291 	xrl	a,#0x80
      001F94 8D F0            [24] 3292 	mov	b,r5
      001F96 63 F0 80         [24] 3293 	xrl	b,#0x80
      001F99 95 F0            [12] 3294 	subb	a,b
      001F9B B3               [12] 3295 	cpl	c
      001F9C 92 08            [24] 3296 	mov	b0,c
      001F9E E4               [12] 3297 	clr	a
      001F9F 33               [12] 3298 	rlc	a
      001FA0 90 03 0C         [24] 3299 	mov	dptr,#(_axradio_cb_channelstate + 0x000c)
      001FA3 F0               [24] 3300 	movx	@dptr,a
                                   3301 ;	COMMON\easyax5043.c:424: axradio_cb_channelstate.st.time.t = axradio_timeanchor.radiotimer;
      001FA4 90 00 BC         [24] 3302 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      001FA7 E0               [24] 3303 	movx	a,@dptr
      001FA8 FB               [12] 3304 	mov	r3,a
      001FA9 A3               [24] 3305 	inc	dptr
      001FAA E0               [24] 3306 	movx	a,@dptr
      001FAB FC               [12] 3307 	mov	r4,a
      001FAC A3               [24] 3308 	inc	dptr
      001FAD E0               [24] 3309 	movx	a,@dptr
      001FAE FD               [12] 3310 	mov	r5,a
      001FAF A3               [24] 3311 	inc	dptr
      001FB0 E0               [24] 3312 	movx	a,@dptr
      001FB1 FE               [12] 3313 	mov	r6,a
      001FB2 90 03 06         [24] 3314 	mov	dptr,#(_axradio_cb_channelstate + 0x0006)
      001FB5 EB               [12] 3315 	mov	a,r3
      001FB6 F0               [24] 3316 	movx	@dptr,a
      001FB7 EC               [12] 3317 	mov	a,r4
      001FB8 A3               [24] 3318 	inc	dptr
      001FB9 F0               [24] 3319 	movx	@dptr,a
      001FBA ED               [12] 3320 	mov	a,r5
      001FBB A3               [24] 3321 	inc	dptr
      001FBC F0               [24] 3322 	movx	@dptr,a
      001FBD EE               [12] 3323 	mov	a,r6
      001FBE A3               [24] 3324 	inc	dptr
      001FBF F0               [24] 3325 	movx	@dptr,a
                                   3326 ;	COMMON\easyax5043.c:425: wtimer_add_callback(&axradio_cb_channelstate.cb);
      001FC0 90 03 00         [24] 3327 	mov	dptr,#_axradio_cb_channelstate
      001FC3 12 6C C3         [24] 3328 	lcall	_wtimer_add_callback
      001FC6 D0 07            [24] 3329 	pop	ar7
                                   3330 ;	COMMON\easyax5043.c:426: --len;
      001FC8 1F               [12] 3331 	dec	r7
                                   3332 ;	COMMON\easyax5043.c:431: dropchunk:
      001FC9                       3333 00146$:
                                   3334 ;	COMMON\easyax5043.c:432: if (!len)
      001FC9 EF               [12] 3335 	mov	a,r7
      001FCA 70 03            [24] 3336 	jnz	00284$
      001FCC 02 1C 93         [24] 3337 	ljmp	00153$
      001FCF                       3338 00284$:
                                   3339 ;	COMMON\easyax5043.c:435: do {
      001FCF                       3340 00149$:
                                   3341 ;	COMMON\easyax5043.c:436: AX5043_FIFODATA;        // purge FIFO
      001FCF 90 40 29         [24] 3342 	mov	dptr,#_AX5043_FIFODATA
      001FD2 E0               [24] 3343 	movx	a,@dptr
                                   3344 ;	COMMON\easyax5043.c:438: while (--i);
      001FD3 DF FA            [24] 3345 	djnz	r7,00149$
                                   3346 ;	COMMON\easyax5043.c:440: } // end switch(fifo_cmd)
      001FD5 02 1C 93         [24] 3347 	ljmp	00153$
                                   3348 ;------------------------------------------------------------
                                   3349 ;Allocation info for local variables in function 'transmit_isr'
                                   3350 ;------------------------------------------------------------
                                   3351 ;cnt                       Allocated to registers r7 
                                   3352 ;byte                      Allocated to registers r7 
                                   3353 ;len_byte                  Allocated to registers r4 
                                   3354 ;i                         Allocated to registers r3 
                                   3355 ;byte                      Allocated to registers r6 
                                   3356 ;flags                     Allocated to registers r6 
                                   3357 ;len                       Allocated to registers r4 r5 
                                   3358 ;------------------------------------------------------------
                                   3359 ;	COMMON\easyax5043.c:444: static __reentrantb void transmit_isr(void) __reentrant
                                   3360 ;	-----------------------------------------
                                   3361 ;	 function transmit_isr
                                   3362 ;	-----------------------------------------
      001FD8                       3363 _transmit_isr:
                                   3364 ;	COMMON\easyax5043.c:583: axradio_trxstate = trxstate_tx_waitdone;
      001FD8                       3365 00157$:
                                   3366 ;	COMMON\easyax5043.c:447: uint8_t cnt = AX5043_FIFOFREE0;
      001FD8 90 40 2D         [24] 3367 	mov	dptr,#_AX5043_FIFOFREE0
      001FDB E0               [24] 3368 	movx	a,@dptr
      001FDC FF               [12] 3369 	mov	r7,a
                                   3370 ;	COMMON\easyax5043.c:448: if (AX5043_FIFOFREE1)
      001FDD 90 40 2C         [24] 3371 	mov	dptr,#_AX5043_FIFOFREE1
      001FE0 E0               [24] 3372 	movx	a,@dptr
      001FE1 E0               [24] 3373 	movx	a,@dptr
      001FE2 60 02            [24] 3374 	jz	00102$
                                   3375 ;	COMMON\easyax5043.c:449: cnt = 0xff;
      001FE4 7F FF            [12] 3376 	mov	r7,#0xff
      001FE6                       3377 00102$:
                                   3378 ;	COMMON\easyax5043.c:450: switch (axradio_trxstate) {
      001FE6 AE 0C            [24] 3379 	mov	r6,_axradio_trxstate
      001FE8 BE 0A 02         [24] 3380 	cjne	r6,#0x0a,00246$
      001FEB 80 0D            [24] 3381 	sjmp	00103$
      001FED                       3382 00246$:
      001FED BE 0B 03         [24] 3383 	cjne	r6,#0x0b,00247$
      001FF0 02 20 8C         [24] 3384 	ljmp	00115$
      001FF3                       3385 00247$:
      001FF3 BE 0C 03         [24] 3386 	cjne	r6,#0x0c,00248$
      001FF6 02 22 5D         [24] 3387 	ljmp	00138$
      001FF9                       3388 00248$:
      001FF9 22               [24] 3389 	ret
                                   3390 ;	COMMON\easyax5043.c:451: case trxstate_tx_longpreamble:
      001FFA                       3391 00103$:
                                   3392 ;	COMMON\easyax5043.c:452: if (!axradio_txbuffer_cnt) {
      001FFA 90 00 A9         [24] 3393 	mov	dptr,#_axradio_txbuffer_cnt
      001FFD E0               [24] 3394 	movx	a,@dptr
      001FFE FD               [12] 3395 	mov	r5,a
      001FFF A3               [24] 3396 	inc	dptr
      002000 E0               [24] 3397 	movx	a,@dptr
      002001 FE               [12] 3398 	mov	r6,a
      002002 4D               [12] 3399 	orl	a,r5
      002003 70 37            [24] 3400 	jnz	00109$
                                   3401 ;	COMMON\easyax5043.c:453: axradio_trxstate = trxstate_tx_shortpreamble;
      002005 75 0C 0B         [24] 3402 	mov	_axradio_trxstate,#0x0b
                                   3403 ;	COMMON\easyax5043.c:454: if( axradio_mode == AXRADIO_MODE_WOR_TRANSMIT || axradio_mode == AXRADIO_MODE_WOR_ACK_TRANSMIT )
      002008 74 11            [12] 3404 	mov	a,#0x11
      00200A B5 0B 02         [24] 3405 	cjne	a,_axradio_mode,00250$
      00200D 80 05            [24] 3406 	sjmp	00104$
      00200F                       3407 00250$:
      00200F 74 13            [12] 3408 	mov	a,#0x13
      002011 B5 0B 14         [24] 3409 	cjne	a,_axradio_mode,00105$
      002014                       3410 00104$:
                                   3411 ;	COMMON\easyax5043.c:455: axradio_txbuffer_cnt = axradio_phy_preamble_wor_len;
      002014 90 7F E1         [24] 3412 	mov	dptr,#_axradio_phy_preamble_wor_len
      002017 E4               [12] 3413 	clr	a
      002018 93               [24] 3414 	movc	a,@a+dptr
      002019 FB               [12] 3415 	mov	r3,a
      00201A 74 01            [12] 3416 	mov	a,#0x01
      00201C 93               [24] 3417 	movc	a,@a+dptr
      00201D FC               [12] 3418 	mov	r4,a
      00201E 90 00 A9         [24] 3419 	mov	dptr,#_axradio_txbuffer_cnt
      002021 EB               [12] 3420 	mov	a,r3
      002022 F0               [24] 3421 	movx	@dptr,a
      002023 EC               [12] 3422 	mov	a,r4
      002024 A3               [24] 3423 	inc	dptr
      002025 F0               [24] 3424 	movx	@dptr,a
      002026 80 64            [24] 3425 	sjmp	00115$
      002028                       3426 00105$:
                                   3427 ;	COMMON\easyax5043.c:457: axradio_txbuffer_cnt = axradio_phy_preamble_len;
      002028 90 7F E5         [24] 3428 	mov	dptr,#_axradio_phy_preamble_len
      00202B E4               [12] 3429 	clr	a
      00202C 93               [24] 3430 	movc	a,@a+dptr
      00202D FB               [12] 3431 	mov	r3,a
      00202E 74 01            [12] 3432 	mov	a,#0x01
      002030 93               [24] 3433 	movc	a,@a+dptr
      002031 FC               [12] 3434 	mov	r4,a
      002032 90 00 A9         [24] 3435 	mov	dptr,#_axradio_txbuffer_cnt
      002035 EB               [12] 3436 	mov	a,r3
      002036 F0               [24] 3437 	movx	@dptr,a
      002037 EC               [12] 3438 	mov	a,r4
      002038 A3               [24] 3439 	inc	dptr
      002039 F0               [24] 3440 	movx	@dptr,a
                                   3441 ;	COMMON\easyax5043.c:458: goto shortpreamble;
      00203A 80 50            [24] 3442 	sjmp	00115$
      00203C                       3443 00109$:
                                   3444 ;	COMMON\easyax5043.c:460: if (cnt < 4)
      00203C BF 04 00         [24] 3445 	cjne	r7,#0x04,00253$
      00203F                       3446 00253$:
      00203F 50 03            [24] 3447 	jnc	00254$
      002041 02 22 FE         [24] 3448 	ljmp	00153$
      002044                       3449 00254$:
                                   3450 ;	COMMON\easyax5043.c:462: cnt = 7;
      002044 7F 07            [12] 3451 	mov	r7,#0x07
                                   3452 ;	COMMON\easyax5043.c:463: if (axradio_txbuffer_cnt < 7)
      002046 C3               [12] 3453 	clr	c
      002047 ED               [12] 3454 	mov	a,r5
      002048 94 07            [12] 3455 	subb	a,#0x07
      00204A EE               [12] 3456 	mov	a,r6
      00204B 94 00            [12] 3457 	subb	a,#0x00
      00204D 50 02            [24] 3458 	jnc	00113$
                                   3459 ;	COMMON\easyax5043.c:464: cnt = axradio_txbuffer_cnt;
      00204F 8D 07            [24] 3460 	mov	ar7,r5
      002051                       3461 00113$:
                                   3462 ;	COMMON\easyax5043.c:465: axradio_txbuffer_cnt -= cnt;
      002051 8F 05            [24] 3463 	mov	ar5,r7
      002053 7E 00            [12] 3464 	mov	r6,#0x00
      002055 90 00 A9         [24] 3465 	mov	dptr,#_axradio_txbuffer_cnt
      002058 E0               [24] 3466 	movx	a,@dptr
      002059 FB               [12] 3467 	mov	r3,a
      00205A A3               [24] 3468 	inc	dptr
      00205B E0               [24] 3469 	movx	a,@dptr
      00205C FC               [12] 3470 	mov	r4,a
      00205D 90 00 A9         [24] 3471 	mov	dptr,#_axradio_txbuffer_cnt
      002060 EB               [12] 3472 	mov	a,r3
      002061 C3               [12] 3473 	clr	c
      002062 9D               [12] 3474 	subb	a,r5
      002063 F0               [24] 3475 	movx	@dptr,a
      002064 EC               [12] 3476 	mov	a,r4
      002065 9E               [12] 3477 	subb	a,r6
      002066 A3               [24] 3478 	inc	dptr
      002067 F0               [24] 3479 	movx	@dptr,a
                                   3480 ;	COMMON\easyax5043.c:466: cnt <<= 5;
      002068 EF               [12] 3481 	mov	a,r7
      002069 C4               [12] 3482 	swap	a
      00206A 23               [12] 3483 	rl	a
      00206B 54 E0            [12] 3484 	anl	a,#0xe0
      00206D FF               [12] 3485 	mov	r7,a
                                   3486 ;	COMMON\easyax5043.c:467: AX5043_FIFODATA = AX5043_FIFOCMD_REPEATDATA | (3 << 5);
      00206E 90 40 29         [24] 3487 	mov	dptr,#_AX5043_FIFODATA
      002071 74 62            [12] 3488 	mov	a,#0x62
      002073 F0               [24] 3489 	movx	@dptr,a
                                   3490 ;	COMMON\easyax5043.c:468: AX5043_FIFODATA = axradio_phy_preamble_flags;
      002074 90 7F E8         [24] 3491 	mov	dptr,#_axradio_phy_preamble_flags
      002077 E4               [12] 3492 	clr	a
      002078 93               [24] 3493 	movc	a,@a+dptr
      002079 90 40 29         [24] 3494 	mov	dptr,#_AX5043_FIFODATA
      00207C F0               [24] 3495 	movx	@dptr,a
                                   3496 ;	COMMON\easyax5043.c:469: AX5043_FIFODATA = cnt;
      00207D EF               [12] 3497 	mov	a,r7
      00207E F0               [24] 3498 	movx	@dptr,a
                                   3499 ;	COMMON\easyax5043.c:470: AX5043_FIFODATA = axradio_phy_preamble_byte;
      00207F 90 7F E7         [24] 3500 	mov	dptr,#_axradio_phy_preamble_byte
      002082 E4               [12] 3501 	clr	a
      002083 93               [24] 3502 	movc	a,@a+dptr
      002084 FE               [12] 3503 	mov	r6,a
      002085 90 40 29         [24] 3504 	mov	dptr,#_AX5043_FIFODATA
      002088 F0               [24] 3505 	movx	@dptr,a
                                   3506 ;	COMMON\easyax5043.c:471: break;
      002089 02 1F D8         [24] 3507 	ljmp	00157$
                                   3508 ;	COMMON\easyax5043.c:474: shortpreamble:
      00208C                       3509 00115$:
                                   3510 ;	COMMON\easyax5043.c:475: if (!axradio_txbuffer_cnt) {
      00208C 90 00 A9         [24] 3511 	mov	dptr,#_axradio_txbuffer_cnt
      00208F E0               [24] 3512 	movx	a,@dptr
      002090 FD               [12] 3513 	mov	r5,a
      002091 A3               [24] 3514 	inc	dptr
      002092 E0               [24] 3515 	movx	a,@dptr
      002093 FE               [12] 3516 	mov	r6,a
      002094 4D               [12] 3517 	orl	a,r5
      002095 60 03            [24] 3518 	jz	00256$
      002097 02 21 71         [24] 3519 	ljmp	00128$
      00209A                       3520 00256$:
                                   3521 ;	COMMON\easyax5043.c:476: if (cnt < 15)
      00209A BF 0F 00         [24] 3522 	cjne	r7,#0x0f,00257$
      00209D                       3523 00257$:
      00209D 50 03            [24] 3524 	jnc	00258$
      00209F 02 22 FE         [24] 3525 	ljmp	00153$
      0020A2                       3526 00258$:
                                   3527 ;	COMMON\easyax5043.c:478: if (axradio_phy_preamble_appendbits) {
      0020A2 90 7F E9         [24] 3528 	mov	dptr,#_axradio_phy_preamble_appendbits
      0020A5 E4               [12] 3529 	clr	a
      0020A6 93               [24] 3530 	movc	a,@a+dptr
      0020A7 FC               [12] 3531 	mov	r4,a
      0020A8 60 6F            [24] 3532 	jz	00122$
                                   3533 ;	COMMON\easyax5043.c:480: AX5043_FIFODATA = AX5043_FIFOCMD_DATA | (2 << 5);
      0020AA 90 40 29         [24] 3534 	mov	dptr,#_AX5043_FIFODATA
      0020AD 74 41            [12] 3535 	mov	a,#0x41
      0020AF F0               [24] 3536 	movx	@dptr,a
                                   3537 ;	COMMON\easyax5043.c:481: AX5043_FIFODATA = 0x1C;
      0020B0 74 1C            [12] 3538 	mov	a,#0x1c
      0020B2 F0               [24] 3539 	movx	@dptr,a
                                   3540 ;	COMMON\easyax5043.c:482: byte = axradio_phy_preamble_appendpattern;
      0020B3 90 7F EA         [24] 3541 	mov	dptr,#_axradio_phy_preamble_appendpattern
      0020B6 E4               [12] 3542 	clr	a
      0020B7 93               [24] 3543 	movc	a,@a+dptr
      0020B8 FB               [12] 3544 	mov	r3,a
      0020B9 FF               [12] 3545 	mov	r7,a
                                   3546 ;	COMMON\easyax5043.c:483: if (AX5043_PKTADDRCFG & 0x80) {
      0020BA 90 42 00         [24] 3547 	mov	dptr,#_AX5043_PKTADDRCFG
      0020BD E0               [24] 3548 	movx	a,@dptr
      0020BE FA               [12] 3549 	mov	r2,a
      0020BF 30 E7 26         [24] 3550 	jnb	acc.7,00119$
                                   3551 ;	COMMON\easyax5043.c:485: byte &= 0xFF << (8-axradio_phy_preamble_appendbits);
      0020C2 74 08            [12] 3552 	mov	a,#0x08
      0020C4 C3               [12] 3553 	clr	c
      0020C5 9C               [12] 3554 	subb	a,r4
      0020C6 F5 F0            [12] 3555 	mov	b,a
      0020C8 05 F0            [12] 3556 	inc	b
      0020CA 74 FF            [12] 3557 	mov	a,#0xff
      0020CC 80 02            [24] 3558 	sjmp	00263$
      0020CE                       3559 00261$:
      0020CE 25 E0            [12] 3560 	add	a,acc
      0020D0                       3561 00263$:
      0020D0 D5 F0 FB         [24] 3562 	djnz	b,00261$
      0020D3 FA               [12] 3563 	mov	r2,a
      0020D4 52 07            [12] 3564 	anl	ar7,a
                                   3565 ;	COMMON\easyax5043.c:486: byte |= 0x80 >> axradio_phy_preamble_appendbits;
      0020D6 8C F0            [24] 3566 	mov	b,r4
      0020D8 05 F0            [12] 3567 	inc	b
      0020DA 74 80            [12] 3568 	mov	a,#0x80
      0020DC 80 02            [24] 3569 	sjmp	00265$
      0020DE                       3570 00264$:
      0020DE C3               [12] 3571 	clr	c
      0020DF 13               [12] 3572 	rrc	a
      0020E0                       3573 00265$:
      0020E0 D5 F0 FB         [24] 3574 	djnz	b,00264$
      0020E3 FA               [12] 3575 	mov	r2,a
      0020E4 42 07            [12] 3576 	orl	ar7,a
      0020E6 80 2C            [24] 3577 	sjmp	00120$
      0020E8                       3578 00119$:
                                   3579 ;	COMMON\easyax5043.c:489: byte &= 0xFF >> (8-axradio_phy_preamble_appendbits);
      0020E8 8C 02            [24] 3580 	mov	ar2,r4
      0020EA 7B 00            [12] 3581 	mov	r3,#0x00
      0020EC 74 08            [12] 3582 	mov	a,#0x08
      0020EE C3               [12] 3583 	clr	c
      0020EF 9A               [12] 3584 	subb	a,r2
      0020F0 FA               [12] 3585 	mov	r2,a
      0020F1 E4               [12] 3586 	clr	a
      0020F2 9B               [12] 3587 	subb	a,r3
      0020F3 FB               [12] 3588 	mov	r3,a
      0020F4 8A F0            [24] 3589 	mov	b,r2
      0020F6 05 F0            [12] 3590 	inc	b
      0020F8 74 FF            [12] 3591 	mov	a,#0xff
      0020FA 80 02            [24] 3592 	sjmp	00267$
      0020FC                       3593 00266$:
      0020FC C3               [12] 3594 	clr	c
      0020FD 13               [12] 3595 	rrc	a
      0020FE                       3596 00267$:
      0020FE D5 F0 FB         [24] 3597 	djnz	b,00266$
      002101 FA               [12] 3598 	mov	r2,a
      002102 52 07            [12] 3599 	anl	ar7,a
                                   3600 ;	COMMON\easyax5043.c:490: byte |= 0x01 << axradio_phy_preamble_appendbits;
      002104 8C F0            [24] 3601 	mov	b,r4
      002106 05 F0            [12] 3602 	inc	b
      002108 74 01            [12] 3603 	mov	a,#0x01
      00210A 80 02            [24] 3604 	sjmp	00270$
      00210C                       3605 00268$:
      00210C 25 E0            [12] 3606 	add	a,acc
      00210E                       3607 00270$:
      00210E D5 F0 FB         [24] 3608 	djnz	b,00268$
      002111 FC               [12] 3609 	mov	r4,a
      002112 42 07            [12] 3610 	orl	ar7,a
      002114                       3611 00120$:
                                   3612 ;	COMMON\easyax5043.c:492: AX5043_FIFODATA = byte;
      002114 90 40 29         [24] 3613 	mov	dptr,#_AX5043_FIFODATA
      002117 EF               [12] 3614 	mov	a,r7
      002118 F0               [24] 3615 	movx	@dptr,a
      002119                       3616 00122$:
                                   3617 ;	COMMON\easyax5043.c:498: if ((AX5043_FRAMING & 0x0E) == 0x06 && axradio_framing_synclen) {
      002119 90 40 12         [24] 3618 	mov	dptr,#_AX5043_FRAMING
      00211C E0               [24] 3619 	movx	a,@dptr
      00211D FC               [12] 3620 	mov	r4,a
      00211E 53 04 0E         [24] 3621 	anl	ar4,#0x0e
      002121 BC 06 47         [24] 3622 	cjne	r4,#0x06,00125$
      002124 90 7F F3         [24] 3623 	mov	dptr,#_axradio_framing_synclen
      002127 E4               [12] 3624 	clr	a
      002128 93               [24] 3625 	movc	a,@a+dptr
      002129 FC               [12] 3626 	mov	r4,a
      00212A E4               [12] 3627 	clr	a
      00212B 93               [24] 3628 	movc	a,@a+dptr
      00212C 60 3D            [24] 3629 	jz	00125$
                                   3630 ;	COMMON\easyax5043.c:500: uint8_t len_byte = axradio_framing_synclen;
                                   3631 ;	COMMON\easyax5043.c:501: uint8_t i = (len_byte & 0x07) ? 0x04 : 0;
      00212E EC               [12] 3632 	mov	a,r4
      00212F 54 07            [12] 3633 	anl	a,#0x07
      002131 60 02            [24] 3634 	jz	00161$
      002133 74 04            [12] 3635 	mov	a,#0x04
      002135                       3636 00161$:
      002135 FB               [12] 3637 	mov	r3,a
                                   3638 ;	COMMON\easyax5043.c:503: len_byte += 7;
      002136 74 07            [12] 3639 	mov	a,#0x07
      002138 2C               [12] 3640 	add	a,r4
                                   3641 ;	COMMON\easyax5043.c:504: len_byte >>= 3;
      002139 C4               [12] 3642 	swap	a
      00213A 23               [12] 3643 	rl	a
      00213B 54 1F            [12] 3644 	anl	a,#0x1f
                                   3645 ;	COMMON\easyax5043.c:505: AX5043_FIFODATA = AX5043_FIFOCMD_DATA | ((len_byte + 1) << 5);
      00213D FC               [12] 3646 	mov	r4,a
      00213E 04               [12] 3647 	inc	a
      00213F C4               [12] 3648 	swap	a
      002140 23               [12] 3649 	rl	a
      002141 54 E0            [12] 3650 	anl	a,#0xe0
      002143 FA               [12] 3651 	mov	r2,a
      002144 90 40 29         [24] 3652 	mov	dptr,#_AX5043_FIFODATA
      002147 74 01            [12] 3653 	mov	a,#0x01
      002149 4A               [12] 3654 	orl	a,r2
      00214A F0               [24] 3655 	movx	@dptr,a
                                   3656 ;	COMMON\easyax5043.c:506: AX5043_FIFODATA = axradio_framing_syncflags | i;
      00214B 90 7F F8         [24] 3657 	mov	dptr,#_axradio_framing_syncflags
      00214E E4               [12] 3658 	clr	a
      00214F 93               [24] 3659 	movc	a,@a+dptr
      002150 FA               [12] 3660 	mov	r2,a
      002151 90 40 29         [24] 3661 	mov	dptr,#_AX5043_FIFODATA
      002154 EB               [12] 3662 	mov	a,r3
      002155 4A               [12] 3663 	orl	a,r2
      002156 F0               [24] 3664 	movx	@dptr,a
                                   3665 ;	COMMON\easyax5043.c:507: for (i = 0; i < len_byte; ++i) {
      002157 7B 00            [12] 3666 	mov	r3,#0x00
      002159                       3667 00155$:
      002159 C3               [12] 3668 	clr	c
      00215A EB               [12] 3669 	mov	a,r3
      00215B 9C               [12] 3670 	subb	a,r4
      00215C 50 0D            [24] 3671 	jnc	00125$
                                   3672 ;	COMMON\easyax5043.c:509: AX5043_FIFODATA = axradio_framing_syncword[i];
      00215E EB               [12] 3673 	mov	a,r3
      00215F 90 7F F4         [24] 3674 	mov	dptr,#_axradio_framing_syncword
      002162 93               [24] 3675 	movc	a,@a+dptr
      002163 FA               [12] 3676 	mov	r2,a
      002164 90 40 29         [24] 3677 	mov	dptr,#_AX5043_FIFODATA
      002167 F0               [24] 3678 	movx	@dptr,a
                                   3679 ;	COMMON\easyax5043.c:507: for (i = 0; i < len_byte; ++i) {
      002168 0B               [12] 3680 	inc	r3
      002169 80 EE            [24] 3681 	sjmp	00155$
      00216B                       3682 00125$:
                                   3683 ;	COMMON\easyax5043.c:516: axradio_trxstate = trxstate_tx_packet;
      00216B 75 0C 0C         [24] 3684 	mov	_axradio_trxstate,#0x0c
                                   3685 ;	COMMON\easyax5043.c:517: break;
      00216E 02 1F D8         [24] 3686 	ljmp	00157$
      002171                       3687 00128$:
                                   3688 ;	COMMON\easyax5043.c:519: if (cnt < 4)
      002171 BF 04 00         [24] 3689 	cjne	r7,#0x04,00276$
      002174                       3690 00276$:
      002174 50 03            [24] 3691 	jnc	00277$
      002176 02 22 FE         [24] 3692 	ljmp	00153$
      002179                       3693 00277$:
                                   3694 ;	COMMON\easyax5043.c:521: cnt = 255;
      002179 7F FF            [12] 3695 	mov	r7,#0xff
                                   3696 ;	COMMON\easyax5043.c:522: if (axradio_txbuffer_cnt < 255*8)
      00217B C3               [12] 3697 	clr	c
      00217C ED               [12] 3698 	mov	a,r5
      00217D 94 F8            [12] 3699 	subb	a,#0xf8
      00217F EE               [12] 3700 	mov	a,r6
      002180 94 07            [12] 3701 	subb	a,#0x07
      002182 50 12            [24] 3702 	jnc	00132$
                                   3703 ;	COMMON\easyax5043.c:523: cnt = axradio_txbuffer_cnt >> 3;
      002184 EE               [12] 3704 	mov	a,r6
      002185 C4               [12] 3705 	swap	a
      002186 23               [12] 3706 	rl	a
      002187 CD               [12] 3707 	xch	a,r5
      002188 C4               [12] 3708 	swap	a
      002189 23               [12] 3709 	rl	a
      00218A 54 1F            [12] 3710 	anl	a,#0x1f
      00218C 6D               [12] 3711 	xrl	a,r5
      00218D CD               [12] 3712 	xch	a,r5
      00218E 54 1F            [12] 3713 	anl	a,#0x1f
      002190 CD               [12] 3714 	xch	a,r5
      002191 6D               [12] 3715 	xrl	a,r5
      002192 CD               [12] 3716 	xch	a,r5
      002193 FE               [12] 3717 	mov	r6,a
      002194 8D 07            [24] 3718 	mov	ar7,r5
      002196                       3719 00132$:
                                   3720 ;	COMMON\easyax5043.c:524: if (cnt) {
      002196 EF               [12] 3721 	mov	a,r7
      002197 60 42            [24] 3722 	jz	00134$
                                   3723 ;	COMMON\easyax5043.c:525: axradio_txbuffer_cnt -= ((uint16_t)cnt) << 3;
      002199 8F 05            [24] 3724 	mov	ar5,r7
      00219B E4               [12] 3725 	clr	a
      00219C 03               [12] 3726 	rr	a
      00219D 54 F8            [12] 3727 	anl	a,#0xf8
      00219F CD               [12] 3728 	xch	a,r5
      0021A0 C4               [12] 3729 	swap	a
      0021A1 03               [12] 3730 	rr	a
      0021A2 CD               [12] 3731 	xch	a,r5
      0021A3 6D               [12] 3732 	xrl	a,r5
      0021A4 CD               [12] 3733 	xch	a,r5
      0021A5 54 F8            [12] 3734 	anl	a,#0xf8
      0021A7 CD               [12] 3735 	xch	a,r5
      0021A8 6D               [12] 3736 	xrl	a,r5
      0021A9 FE               [12] 3737 	mov	r6,a
      0021AA 90 00 A9         [24] 3738 	mov	dptr,#_axradio_txbuffer_cnt
      0021AD E0               [24] 3739 	movx	a,@dptr
      0021AE FB               [12] 3740 	mov	r3,a
      0021AF A3               [24] 3741 	inc	dptr
      0021B0 E0               [24] 3742 	movx	a,@dptr
      0021B1 FC               [12] 3743 	mov	r4,a
      0021B2 90 00 A9         [24] 3744 	mov	dptr,#_axradio_txbuffer_cnt
      0021B5 EB               [12] 3745 	mov	a,r3
      0021B6 C3               [12] 3746 	clr	c
      0021B7 9D               [12] 3747 	subb	a,r5
      0021B8 F0               [24] 3748 	movx	@dptr,a
      0021B9 EC               [12] 3749 	mov	a,r4
      0021BA 9E               [12] 3750 	subb	a,r6
      0021BB A3               [24] 3751 	inc	dptr
      0021BC F0               [24] 3752 	movx	@dptr,a
                                   3753 ;	COMMON\easyax5043.c:526: AX5043_FIFODATA = AX5043_FIFOCMD_REPEATDATA | (3 << 5);
      0021BD 90 40 29         [24] 3754 	mov	dptr,#_AX5043_FIFODATA
      0021C0 74 62            [12] 3755 	mov	a,#0x62
      0021C2 F0               [24] 3756 	movx	@dptr,a
                                   3757 ;	COMMON\easyax5043.c:527: AX5043_FIFODATA = axradio_phy_preamble_flags;
      0021C3 90 7F E8         [24] 3758 	mov	dptr,#_axradio_phy_preamble_flags
      0021C6 E4               [12] 3759 	clr	a
      0021C7 93               [24] 3760 	movc	a,@a+dptr
      0021C8 90 40 29         [24] 3761 	mov	dptr,#_AX5043_FIFODATA
      0021CB F0               [24] 3762 	movx	@dptr,a
                                   3763 ;	COMMON\easyax5043.c:528: AX5043_FIFODATA = cnt;
      0021CC EF               [12] 3764 	mov	a,r7
      0021CD F0               [24] 3765 	movx	@dptr,a
                                   3766 ;	COMMON\easyax5043.c:529: AX5043_FIFODATA = axradio_phy_preamble_byte;
      0021CE 90 7F E7         [24] 3767 	mov	dptr,#_axradio_phy_preamble_byte
      0021D1 E4               [12] 3768 	clr	a
      0021D2 93               [24] 3769 	movc	a,@a+dptr
      0021D3 FE               [12] 3770 	mov	r6,a
      0021D4 90 40 29         [24] 3771 	mov	dptr,#_AX5043_FIFODATA
      0021D7 F0               [24] 3772 	movx	@dptr,a
                                   3773 ;	COMMON\easyax5043.c:530: break;
      0021D8 02 1F D8         [24] 3774 	ljmp	00157$
      0021DB                       3775 00134$:
                                   3776 ;	COMMON\easyax5043.c:533: uint8_t byte = axradio_phy_preamble_byte;
      0021DB 90 7F E7         [24] 3777 	mov	dptr,#_axradio_phy_preamble_byte
      0021DE E4               [12] 3778 	clr	a
      0021DF 93               [24] 3779 	movc	a,@a+dptr
      0021E0 FE               [12] 3780 	mov	r6,a
                                   3781 ;	COMMON\easyax5043.c:534: cnt = axradio_txbuffer_cnt;
      0021E1 90 00 A9         [24] 3782 	mov	dptr,#_axradio_txbuffer_cnt
      0021E4 E0               [24] 3783 	movx	a,@dptr
      0021E5 FC               [12] 3784 	mov	r4,a
      0021E6 A3               [24] 3785 	inc	dptr
      0021E7 E0               [24] 3786 	movx	a,@dptr
      0021E8 8C 07            [24] 3787 	mov	ar7,r4
                                   3788 ;	COMMON\easyax5043.c:535: axradio_txbuffer_cnt = 0;
      0021EA 90 00 A9         [24] 3789 	mov	dptr,#_axradio_txbuffer_cnt
      0021ED E4               [12] 3790 	clr	a
      0021EE F0               [24] 3791 	movx	@dptr,a
      0021EF A3               [24] 3792 	inc	dptr
      0021F0 F0               [24] 3793 	movx	@dptr,a
                                   3794 ;	COMMON\easyax5043.c:536: AX5043_FIFODATA = AX5043_FIFOCMD_DATA | (2 << 5);
      0021F1 90 40 29         [24] 3795 	mov	dptr,#_AX5043_FIFODATA
      0021F4 74 41            [12] 3796 	mov	a,#0x41
      0021F6 F0               [24] 3797 	movx	@dptr,a
                                   3798 ;	COMMON\easyax5043.c:537: AX5043_FIFODATA = 0x1C;
      0021F7 74 1C            [12] 3799 	mov	a,#0x1c
      0021F9 F0               [24] 3800 	movx	@dptr,a
                                   3801 ;	COMMON\easyax5043.c:538: if (AX5043_PKTADDRCFG & 0x80) {
      0021FA 90 42 00         [24] 3802 	mov	dptr,#_AX5043_PKTADDRCFG
      0021FD E0               [24] 3803 	movx	a,@dptr
      0021FE FD               [12] 3804 	mov	r5,a
      0021FF 30 E7 27         [24] 3805 	jnb	acc.7,00136$
                                   3806 ;	COMMON\easyax5043.c:540: byte &= 0xFF << (8-cnt);
      002202 74 08            [12] 3807 	mov	a,#0x08
      002204 C3               [12] 3808 	clr	c
      002205 9F               [12] 3809 	subb	a,r7
      002206 FD               [12] 3810 	mov	r5,a
      002207 8D F0            [24] 3811 	mov	b,r5
      002209 05 F0            [12] 3812 	inc	b
      00220B 74 FF            [12] 3813 	mov	a,#0xff
      00220D 80 02            [24] 3814 	sjmp	00283$
      00220F                       3815 00281$:
      00220F 25 E0            [12] 3816 	add	a,acc
      002211                       3817 00283$:
      002211 D5 F0 FB         [24] 3818 	djnz	b,00281$
      002214 FD               [12] 3819 	mov	r5,a
      002215 52 06            [12] 3820 	anl	ar6,a
                                   3821 ;	COMMON\easyax5043.c:541: byte |= 0x80 >> cnt;
      002217 8F F0            [24] 3822 	mov	b,r7
      002219 05 F0            [12] 3823 	inc	b
      00221B 74 80            [12] 3824 	mov	a,#0x80
      00221D 80 02            [24] 3825 	sjmp	00285$
      00221F                       3826 00284$:
      00221F C3               [12] 3827 	clr	c
      002220 13               [12] 3828 	rrc	a
      002221                       3829 00285$:
      002221 D5 F0 FB         [24] 3830 	djnz	b,00284$
      002224 FD               [12] 3831 	mov	r5,a
      002225 42 06            [12] 3832 	orl	ar6,a
      002227 80 2C            [24] 3833 	sjmp	00137$
      002229                       3834 00136$:
                                   3835 ;	COMMON\easyax5043.c:544: byte &= 0xFF >> (8-cnt);
      002229 8F 04            [24] 3836 	mov	ar4,r7
      00222B 7D 00            [12] 3837 	mov	r5,#0x00
      00222D 74 08            [12] 3838 	mov	a,#0x08
      00222F C3               [12] 3839 	clr	c
      002230 9C               [12] 3840 	subb	a,r4
      002231 FC               [12] 3841 	mov	r4,a
      002232 E4               [12] 3842 	clr	a
      002233 9D               [12] 3843 	subb	a,r5
      002234 FD               [12] 3844 	mov	r5,a
      002235 8C F0            [24] 3845 	mov	b,r4
      002237 05 F0            [12] 3846 	inc	b
      002239 74 FF            [12] 3847 	mov	a,#0xff
      00223B 80 02            [24] 3848 	sjmp	00287$
      00223D                       3849 00286$:
      00223D C3               [12] 3850 	clr	c
      00223E 13               [12] 3851 	rrc	a
      00223F                       3852 00287$:
      00223F D5 F0 FB         [24] 3853 	djnz	b,00286$
      002242 FC               [12] 3854 	mov	r4,a
      002243 52 06            [12] 3855 	anl	ar6,a
                                   3856 ;	COMMON\easyax5043.c:545: byte |= 0x01 << cnt;
      002245 8F F0            [24] 3857 	mov	b,r7
      002247 05 F0            [12] 3858 	inc	b
      002249 74 01            [12] 3859 	mov	a,#0x01
      00224B 80 02            [24] 3860 	sjmp	00290$
      00224D                       3861 00288$:
      00224D 25 E0            [12] 3862 	add	a,acc
      00224F                       3863 00290$:
      00224F D5 F0 FB         [24] 3864 	djnz	b,00288$
      002252 FD               [12] 3865 	mov	r5,a
      002253 42 06            [12] 3866 	orl	ar6,a
      002255                       3867 00137$:
                                   3868 ;	COMMON\easyax5043.c:547: AX5043_FIFODATA = byte;
      002255 90 40 29         [24] 3869 	mov	dptr,#_AX5043_FIFODATA
      002258 EE               [12] 3870 	mov	a,r6
      002259 F0               [24] 3871 	movx	@dptr,a
                                   3872 ;	COMMON\easyax5043.c:549: break;
      00225A 02 1F D8         [24] 3873 	ljmp	00157$
                                   3874 ;	COMMON\easyax5043.c:551: case trxstate_tx_packet:
      00225D                       3875 00138$:
                                   3876 ;	COMMON\easyax5043.c:552: if (cnt < 11)
      00225D BF 0B 00         [24] 3877 	cjne	r7,#0x0b,00291$
      002260                       3878 00291$:
      002260 50 03            [24] 3879 	jnc	00292$
      002262 02 22 FE         [24] 3880 	ljmp	00153$
      002265                       3881 00292$:
                                   3882 ;	COMMON\easyax5043.c:555: uint8_t flags = 0;
      002265 7E 00            [12] 3883 	mov	r6,#0x00
                                   3884 ;	COMMON\easyax5043.c:556: if (!axradio_txbuffer_cnt)
      002267 90 00 A9         [24] 3885 	mov	dptr,#_axradio_txbuffer_cnt
      00226A E0               [24] 3886 	movx	a,@dptr
      00226B F5 F0            [12] 3887 	mov	b,a
      00226D A3               [24] 3888 	inc	dptr
      00226E E0               [24] 3889 	movx	a,@dptr
      00226F 45 F0            [12] 3890 	orl	a,b
      002271 70 02            [24] 3891 	jnz	00142$
                                   3892 ;	COMMON\easyax5043.c:557: flags |= 0x01; // flag byte: pkt_start
      002273 7E 01            [12] 3893 	mov	r6,#0x01
      002275                       3894 00142$:
                                   3895 ;	COMMON\easyax5043.c:559: uint16_t len = axradio_txbuffer_len - axradio_txbuffer_cnt;
      002275 90 00 A9         [24] 3896 	mov	dptr,#_axradio_txbuffer_cnt
      002278 E0               [24] 3897 	movx	a,@dptr
      002279 FC               [12] 3898 	mov	r4,a
      00227A A3               [24] 3899 	inc	dptr
      00227B E0               [24] 3900 	movx	a,@dptr
      00227C FD               [12] 3901 	mov	r5,a
      00227D 90 00 A7         [24] 3902 	mov	dptr,#_axradio_txbuffer_len
      002280 E0               [24] 3903 	movx	a,@dptr
      002281 FA               [12] 3904 	mov	r2,a
      002282 A3               [24] 3905 	inc	dptr
      002283 E0               [24] 3906 	movx	a,@dptr
      002284 FB               [12] 3907 	mov	r3,a
      002285 EA               [12] 3908 	mov	a,r2
      002286 C3               [12] 3909 	clr	c
      002287 9C               [12] 3910 	subb	a,r4
      002288 FC               [12] 3911 	mov	r4,a
      002289 EB               [12] 3912 	mov	a,r3
      00228A 9D               [12] 3913 	subb	a,r5
      00228B FD               [12] 3914 	mov	r5,a
                                   3915 ;	COMMON\easyax5043.c:560: cnt -= 3;
      00228C 1F               [12] 3916 	dec	r7
      00228D 1F               [12] 3917 	dec	r7
      00228E 1F               [12] 3918 	dec	r7
                                   3919 ;	COMMON\easyax5043.c:561: if (cnt >= len) {
      00228F 8F 02            [24] 3920 	mov	ar2,r7
      002291 7B 00            [12] 3921 	mov	r3,#0x00
      002293 C3               [12] 3922 	clr	c
      002294 EA               [12] 3923 	mov	a,r2
      002295 9C               [12] 3924 	subb	a,r4
      002296 EB               [12] 3925 	mov	a,r3
      002297 9D               [12] 3926 	subb	a,r5
      002298 40 05            [24] 3927 	jc	00144$
                                   3928 ;	COMMON\easyax5043.c:562: cnt = len;
      00229A 8C 07            [24] 3929 	mov	ar7,r4
                                   3930 ;	COMMON\easyax5043.c:563: flags |= 0x02; // flag byte: pkt_end
      00229C 43 06 02         [24] 3931 	orl	ar6,#0x02
      00229F                       3932 00144$:
                                   3933 ;	COMMON\easyax5043.c:566: if (!cnt)
      00229F EF               [12] 3934 	mov	a,r7
      0022A0 60 4D            [24] 3935 	jz	00152$
                                   3936 ;	COMMON\easyax5043.c:568: AX5043_FIFODATA = AX5043_FIFOCMD_DATA | (7 << 5);
      0022A2 90 40 29         [24] 3937 	mov	dptr,#_AX5043_FIFODATA
      0022A5 74 E1            [12] 3938 	mov	a,#0xe1
      0022A7 F0               [24] 3939 	movx	@dptr,a
                                   3940 ;	COMMON\easyax5043.c:569: AX5043_FIFODATA = cnt + 1; // write FIFO chunk length byte (length includes the flag byte, thus the +1)
      0022A8 EF               [12] 3941 	mov	a,r7
      0022A9 04               [12] 3942 	inc	a
      0022AA F0               [24] 3943 	movx	@dptr,a
                                   3944 ;	COMMON\easyax5043.c:570: AX5043_FIFODATA = flags;
      0022AB EE               [12] 3945 	mov	a,r6
      0022AC F0               [24] 3946 	movx	@dptr,a
                                   3947 ;	COMMON\easyax5043.c:571: ax5043_writefifo(&axradio_txbuffer[axradio_txbuffer_cnt], cnt);
      0022AD 90 00 A9         [24] 3948 	mov	dptr,#_axradio_txbuffer_cnt
      0022B0 E0               [24] 3949 	movx	a,@dptr
      0022B1 FC               [12] 3950 	mov	r4,a
      0022B2 A3               [24] 3951 	inc	dptr
      0022B3 E0               [24] 3952 	movx	a,@dptr
      0022B4 FD               [12] 3953 	mov	r5,a
      0022B5 EC               [12] 3954 	mov	a,r4
      0022B6 24 CC            [12] 3955 	add	a,#_axradio_txbuffer
      0022B8 FC               [12] 3956 	mov	r4,a
      0022B9 ED               [12] 3957 	mov	a,r5
      0022BA 34 00            [12] 3958 	addc	a,#(_axradio_txbuffer >> 8)
      0022BC FD               [12] 3959 	mov	r5,a
      0022BD 7B 00            [12] 3960 	mov	r3,#0x00
      0022BF C0 07            [24] 3961 	push	ar7
      0022C1 C0 06            [24] 3962 	push	ar6
      0022C3 C0 07            [24] 3963 	push	ar7
      0022C5 8C 82            [24] 3964 	mov	dpl,r4
      0022C7 8D 83            [24] 3965 	mov	dph,r5
      0022C9 8B F0            [24] 3966 	mov	b,r3
      0022CB 12 7A 78         [24] 3967 	lcall	_ax5043_writefifo
      0022CE 15 81            [12] 3968 	dec	sp
      0022D0 D0 06            [24] 3969 	pop	ar6
      0022D2 D0 07            [24] 3970 	pop	ar7
                                   3971 ;	COMMON\easyax5043.c:572: axradio_txbuffer_cnt += cnt;
      0022D4 7D 00            [12] 3972 	mov	r5,#0x00
      0022D6 90 00 A9         [24] 3973 	mov	dptr,#_axradio_txbuffer_cnt
      0022D9 E0               [24] 3974 	movx	a,@dptr
      0022DA FB               [12] 3975 	mov	r3,a
      0022DB A3               [24] 3976 	inc	dptr
      0022DC E0               [24] 3977 	movx	a,@dptr
      0022DD FC               [12] 3978 	mov	r4,a
      0022DE 90 00 A9         [24] 3979 	mov	dptr,#_axradio_txbuffer_cnt
      0022E1 EF               [12] 3980 	mov	a,r7
      0022E2 2B               [12] 3981 	add	a,r3
      0022E3 F0               [24] 3982 	movx	@dptr,a
      0022E4 ED               [12] 3983 	mov	a,r5
      0022E5 3C               [12] 3984 	addc	a,r4
      0022E6 A3               [24] 3985 	inc	dptr
      0022E7 F0               [24] 3986 	movx	@dptr,a
                                   3987 ;	COMMON\easyax5043.c:573: if (flags & 0x02)
      0022E8 EE               [12] 3988 	mov	a,r6
      0022E9 20 E1 03         [24] 3989 	jb	acc.1,00152$
                                   3990 ;	COMMON\easyax5043.c:574: goto pktend;
                                   3991 ;	COMMON\easyax5043.c:578: default:
                                   3992 ;	COMMON\easyax5043.c:579: return;
                                   3993 ;	COMMON\easyax5043.c:582: pktend:
      0022EC 02 1F D8         [24] 3994 	ljmp	00157$
      0022EF                       3995 00152$:
                                   3996 ;	COMMON\easyax5043.c:583: axradio_trxstate = trxstate_tx_waitdone;
      0022EF 75 0C 0D         [24] 3997 	mov	_axradio_trxstate,#0x0d
                                   3998 ;	COMMON\easyax5043.c:584: AX5043_RADIOEVENTMASK0 = 0x01; // enable REVRDONE event
      0022F2 90 40 09         [24] 3999 	mov	dptr,#_AX5043_RADIOEVENTMASK0
      0022F5 74 01            [12] 4000 	mov	a,#0x01
      0022F7 F0               [24] 4001 	movx	@dptr,a
                                   4002 ;	COMMON\easyax5043.c:585: AX5043_IRQMASK0 = 0x40; // enable radio controller irq
      0022F8 90 40 07         [24] 4003 	mov	dptr,#_AX5043_IRQMASK0
      0022FB 74 40            [12] 4004 	mov	a,#0x40
      0022FD F0               [24] 4005 	movx	@dptr,a
                                   4006 ;	COMMON\easyax5043.c:586: fifocommit:
      0022FE                       4007 00153$:
                                   4008 ;	COMMON\easyax5043.c:587: AX5043_FIFOSTAT = 4; // commit
      0022FE 90 40 28         [24] 4009 	mov	dptr,#_AX5043_FIFOSTAT
      002301 74 04            [12] 4010 	mov	a,#0x04
      002303 F0               [24] 4011 	movx	@dptr,a
      002304 22               [24] 4012 	ret
                                   4013 ;------------------------------------------------------------
                                   4014 ;Allocation info for local variables in function 'axradio_isr'
                                   4015 ;------------------------------------------------------------
                                   4016 ;evt                       Allocated to registers r7 
                                   4017 ;------------------------------------------------------------
                                   4018 ;	COMMON\easyax5043.c:591: void axradio_isr(void) __interrupt INT_RADIO
                                   4019 ;	-----------------------------------------
                                   4020 ;	 function axradio_isr
                                   4021 ;	-----------------------------------------
      002305                       4022 _axradio_isr:
      002305 C0 21            [24] 4023 	push	bits
      002307 C0 E0            [24] 4024 	push	acc
      002309 C0 F0            [24] 4025 	push	b
      00230B C0 82            [24] 4026 	push	dpl
      00230D C0 83            [24] 4027 	push	dph
      00230F C0 07            [24] 4028 	push	(0+7)
      002311 C0 06            [24] 4029 	push	(0+6)
      002313 C0 05            [24] 4030 	push	(0+5)
      002315 C0 04            [24] 4031 	push	(0+4)
      002317 C0 03            [24] 4032 	push	(0+3)
      002319 C0 02            [24] 4033 	push	(0+2)
      00231B C0 01            [24] 4034 	push	(0+1)
      00231D C0 00            [24] 4035 	push	(0+0)
      00231F C0 D0            [24] 4036 	push	psw
      002321 75 D0 00         [24] 4037 	mov	psw,#0x00
                                   4038 ;	COMMON\easyax5043.c:601: switch (axradio_trxstate) {
      002324 E5 0C            [12] 4039 	mov	a,_axradio_trxstate
      002326 FF               [12] 4040 	mov	r7,a
      002327 24 EF            [12] 4041 	add	a,#0xff - 0x10
      002329 50 03            [24] 4042 	jnc	00256$
      00232B 02 23 61         [24] 4043 	ljmp	00101$
      00232E                       4044 00256$:
      00232E EF               [12] 4045 	mov	a,r7
      00232F F5 F0            [12] 4046 	mov	b,a
      002331 24 0B            [12] 4047 	add	a,#(00257$-3-.)
      002333 83               [24] 4048 	movc	a,@a+pc
      002334 F5 82            [12] 4049 	mov	dpl,a
      002336 E5 F0            [12] 4050 	mov	a,b
      002338 24 15            [12] 4051 	add	a,#(00258$-3-.)
      00233A 83               [24] 4052 	movc	a,@a+pc
      00233B F5 83            [12] 4053 	mov	dph,a
      00233D E4               [12] 4054 	clr	a
      00233E 73               [24] 4055 	jmp	@a+dptr
      00233F                       4056 00257$:
      00233F 61                    4057 	.db	00101$
      002340 3B                    4058 	.db	00165$
      002341 D9                    4059 	.db	00158$
      002342 6D                    4060 	.db	00102$
      002343 61                    4061 	.db	00101$
      002344 81                    4062 	.db	00103$
      002345 61                    4063 	.db	00101$
      002346 95                    4064 	.db	00104$
      002347 61                    4065 	.db	00101$
      002348 A9                    4066 	.db	00105$
      002349 42                    4067 	.db	00114$
      00234A 42                    4068 	.db	00115$
      00234B 42                    4069 	.db	00116$
      00234C 51                    4070 	.db	00117$
      00234D 8F                    4071 	.db	00144$
      00234E DD                    4072 	.db	00145$
      00234F 0E                    4073 	.db	00148$
      002350                       4074 00258$:
      002350 23                    4075 	.db	00101$>>8
      002351 27                    4076 	.db	00165$>>8
      002352 26                    4077 	.db	00158$>>8
      002353 23                    4078 	.db	00102$>>8
      002354 23                    4079 	.db	00101$>>8
      002355 23                    4080 	.db	00103$>>8
      002356 23                    4081 	.db	00101$>>8
      002357 23                    4082 	.db	00104$>>8
      002358 23                    4083 	.db	00101$>>8
      002359 23                    4084 	.db	00105$>>8
      00235A 24                    4085 	.db	00114$>>8
      00235B 24                    4086 	.db	00115$>>8
      00235C 24                    4087 	.db	00116$>>8
      00235D 24                    4088 	.db	00117$>>8
      00235E 25                    4089 	.db	00144$>>8
      00235F 25                    4090 	.db	00145$>>8
      002360 26                    4091 	.db	00148$>>8
                                   4092 ;	COMMON\easyax5043.c:602: default:
      002361                       4093 00101$:
                                   4094 ;	COMMON\easyax5043.c:603: AX5043_IRQMASK1 = 0x00;
      002361 90 40 06         [24] 4095 	mov	dptr,#_AX5043_IRQMASK1
      002364 E4               [12] 4096 	clr	a
      002365 F0               [24] 4097 	movx	@dptr,a
                                   4098 ;	COMMON\easyax5043.c:604: AX5043_IRQMASK0 = 0x00;
      002366 90 40 07         [24] 4099 	mov	dptr,#_AX5043_IRQMASK0
      002369 F0               [24] 4100 	movx	@dptr,a
                                   4101 ;	COMMON\easyax5043.c:605: break;
      00236A 02 27 47         [24] 4102 	ljmp	00167$
                                   4103 ;	COMMON\easyax5043.c:607: case trxstate_wait_xtal:
      00236D                       4104 00102$:
                                   4105 ;	COMMON\easyax5043.c:608: dbglink_writestr("trxstate_wait_xtal");
      00236D 90 81 1C         [24] 4106 	mov	dptr,#___str_3
      002370 75 F0 80         [24] 4107 	mov	b,#0x80
      002373 12 74 0A         [24] 4108 	lcall	_dbglink_writestr
                                   4109 ;	COMMON\easyax5043.c:609: AX5043_IRQMASK1 = 0x00; // otherwise crystal ready will fire all over again
      002376 90 40 06         [24] 4110 	mov	dptr,#_AX5043_IRQMASK1
      002379 E4               [12] 4111 	clr	a
      00237A F0               [24] 4112 	movx	@dptr,a
                                   4113 ;	COMMON\easyax5043.c:610: axradio_trxstate = trxstate_xtal_ready;
      00237B 75 0C 04         [24] 4114 	mov	_axradio_trxstate,#0x04
                                   4115 ;	COMMON\easyax5043.c:611: break;
      00237E 02 27 47         [24] 4116 	ljmp	00167$
                                   4117 ;	COMMON\easyax5043.c:613: case trxstate_pll_ranging:
      002381                       4118 00103$:
                                   4119 ;	COMMON\easyax5043.c:614: dbglink_writestr("trxstate_pll_ranging");
      002381 90 81 2F         [24] 4120 	mov	dptr,#___str_4
      002384 75 F0 80         [24] 4121 	mov	b,#0x80
      002387 12 74 0A         [24] 4122 	lcall	_dbglink_writestr
                                   4123 ;	COMMON\easyax5043.c:615: AX5043_IRQMASK1 = 0x00; // otherwise autoranging done will fire all over again
      00238A 90 40 06         [24] 4124 	mov	dptr,#_AX5043_IRQMASK1
      00238D E4               [12] 4125 	clr	a
      00238E F0               [24] 4126 	movx	@dptr,a
                                   4127 ;	COMMON\easyax5043.c:616: axradio_trxstate = trxstate_pll_ranging_done;
      00238F 75 0C 06         [24] 4128 	mov	_axradio_trxstate,#0x06
                                   4129 ;	COMMON\easyax5043.c:617: break;
      002392 02 27 47         [24] 4130 	ljmp	00167$
                                   4131 ;	COMMON\easyax5043.c:619: case trxstate_pll_settling:
      002395                       4132 00104$:
                                   4133 ;	COMMON\easyax5043.c:620: dbglink_writestr("trxstate_pll_settling");
      002395 90 81 44         [24] 4134 	mov	dptr,#___str_5
      002398 75 F0 80         [24] 4135 	mov	b,#0x80
      00239B 12 74 0A         [24] 4136 	lcall	_dbglink_writestr
                                   4137 ;	COMMON\easyax5043.c:621: AX5043_RADIOEVENTMASK0 = 0x00;
      00239E 90 40 09         [24] 4138 	mov	dptr,#_AX5043_RADIOEVENTMASK0
      0023A1 E4               [12] 4139 	clr	a
      0023A2 F0               [24] 4140 	movx	@dptr,a
                                   4141 ;	COMMON\easyax5043.c:622: axradio_trxstate = trxstate_pll_settled;
      0023A3 75 0C 08         [24] 4142 	mov	_axradio_trxstate,#0x08
                                   4143 ;	COMMON\easyax5043.c:623: break;
      0023A6 02 27 47         [24] 4144 	ljmp	00167$
                                   4145 ;	COMMON\easyax5043.c:625: case trxstate_tx_xtalwait:
      0023A9                       4146 00105$:
                                   4147 ;	COMMON\easyax5043.c:626: dbglink_writestr("trxstate_tx_xtalwait");
      0023A9 90 81 5A         [24] 4148 	mov	dptr,#___str_6
      0023AC 75 F0 80         [24] 4149 	mov	b,#0x80
      0023AF 12 74 0A         [24] 4150 	lcall	_dbglink_writestr
                                   4151 ;	COMMON\easyax5043.c:627: AX5043_RADIOEVENTREQ0; // make sure REVRDONE bit is cleared, so it is a reliable indicator that the packet is out
      0023B2 90 40 0F         [24] 4152 	mov	dptr,#_AX5043_RADIOEVENTREQ0
      0023B5 E0               [24] 4153 	movx	a,@dptr
                                   4154 ;	COMMON\easyax5043.c:628: AX5043_FIFOSTAT = 3; // clear FIFO data & flags (prevent transmitting anything left over in the FIFO, this has no effect if the FIFO is not powerered, in this case it is reset any way)
      0023B6 90 40 28         [24] 4155 	mov	dptr,#_AX5043_FIFOSTAT
      0023B9 74 03            [12] 4156 	mov	a,#0x03
      0023BB F0               [24] 4157 	movx	@dptr,a
                                   4158 ;	COMMON\easyax5043.c:629: AX5043_IRQMASK1 = 0x00;
      0023BC 90 40 06         [24] 4159 	mov	dptr,#_AX5043_IRQMASK1
      0023BF E4               [12] 4160 	clr	a
      0023C0 F0               [24] 4161 	movx	@dptr,a
                                   4162 ;	COMMON\easyax5043.c:630: AX5043_IRQMASK0 = 0x08; // enable fifo free threshold
      0023C1 90 40 07         [24] 4163 	mov	dptr,#_AX5043_IRQMASK0
      0023C4 74 08            [12] 4164 	mov	a,#0x08
      0023C6 F0               [24] 4165 	movx	@dptr,a
                                   4166 ;	COMMON\easyax5043.c:631: axradio_trxstate = trxstate_tx_longpreamble;
      0023C7 75 0C 0A         [24] 4167 	mov	_axradio_trxstate,#0x0a
                                   4168 ;	COMMON\easyax5043.c:633: if ((AX5043_MODULATION & 0x0F) == 9) { // 4-FSK
      0023CA 90 40 10         [24] 4169 	mov	dptr,#_AX5043_MODULATION
      0023CD E0               [24] 4170 	movx	a,@dptr
      0023CE FF               [12] 4171 	mov	r7,a
      0023CF 53 07 0F         [24] 4172 	anl	ar7,#0x0f
      0023D2 BF 09 0E         [24] 4173 	cjne	r7,#0x09,00107$
                                   4174 ;	COMMON\easyax5043.c:634: AX5043_FIFODATA = AX5043_FIFOCMD_DATA | (7 << 5);
      0023D5 90 40 29         [24] 4175 	mov	dptr,#_AX5043_FIFODATA
      0023D8 74 E1            [12] 4176 	mov	a,#0xe1
      0023DA F0               [24] 4177 	movx	@dptr,a
                                   4178 ;	COMMON\easyax5043.c:635: AX5043_FIFODATA = 2;  // length (including flags)
      0023DB 74 02            [12] 4179 	mov	a,#0x02
      0023DD F0               [24] 4180 	movx	@dptr,a
                                   4181 ;	COMMON\easyax5043.c:636: AX5043_FIFODATA = 0x01;  // flag PKTSTART -> dibit sync
      0023DE 14               [12] 4182 	dec	a
      0023DF F0               [24] 4183 	movx	@dptr,a
                                   4184 ;	COMMON\easyax5043.c:637: AX5043_FIFODATA = 0x11; // dummy byte for forcing dibit sync
      0023E0 74 11            [12] 4185 	mov	a,#0x11
      0023E2 F0               [24] 4186 	movx	@dptr,a
      0023E3                       4187 00107$:
                                   4188 ;	COMMON\easyax5043.c:644: transmit_isr();
      0023E3 12 1F D8         [24] 4189 	lcall	_transmit_isr
                                   4190 ;	COMMON\easyax5043.c:645: AX5043_PWRMODE = AX5043_PWRSTATE_FULL_TX;
      0023E6 90 40 02         [24] 4191 	mov	dptr,#_AX5043_PWRMODE
      0023E9 74 0D            [12] 4192 	mov	a,#0x0d
      0023EB F0               [24] 4193 	movx	@dptr,a
                                   4194 ;	COMMON\easyax5043.c:646: update_timeanchor();
      0023EC 12 1B 43         [24] 4195 	lcall	_update_timeanchor
                                   4196 ;	COMMON\easyax5043.c:647: wtimer_remove_callback(&axradio_cb_transmitstart.cb);
      0023EF 90 03 0D         [24] 4197 	mov	dptr,#_axradio_cb_transmitstart
      0023F2 12 78 A9         [24] 4198 	lcall	_wtimer_remove_callback
                                   4199 ;	COMMON\easyax5043.c:648: switch (axradio_mode) {
      0023F5 AF 0B            [24] 4200 	mov	r7,_axradio_mode
      0023F7 BF 12 02         [24] 4201 	cjne	r7,#0x12,00261$
      0023FA 80 03            [24] 4202 	sjmp	00109$
      0023FC                       4203 00261$:
      0023FC BF 13 19         [24] 4204 	cjne	r7,#0x13,00112$
                                   4205 ;	COMMON\easyax5043.c:650: case AXRADIO_MODE_WOR_ACK_TRANSMIT:
      0023FF                       4206 00109$:
                                   4207 ;	COMMON\easyax5043.c:651: if (axradio_ack_count != axradio_framing_ack_retransmissions) {
      0023FF 90 00 B0         [24] 4208 	mov	dptr,#_axradio_ack_count
      002402 E0               [24] 4209 	movx	a,@dptr
      002403 FF               [12] 4210 	mov	r7,a
      002404 90 80 02         [24] 4211 	mov	dptr,#_axradio_framing_ack_retransmissions
      002407 E4               [12] 4212 	clr	a
      002408 93               [24] 4213 	movc	a,@a+dptr
      002409 FE               [12] 4214 	mov	r6,a
      00240A EF               [12] 4215 	mov	a,r7
      00240B B5 06 02         [24] 4216 	cjne	a,ar6,00264$
      00240E 80 08            [24] 4217 	sjmp	00112$
      002410                       4218 00264$:
                                   4219 ;	COMMON\easyax5043.c:652: axradio_cb_transmitstart.st.error = AXRADIO_ERR_RETRANSMISSION;
      002410 90 03 12         [24] 4220 	mov	dptr,#(_axradio_cb_transmitstart + 0x0005)
      002413 74 08            [12] 4221 	mov	a,#0x08
      002415 F0               [24] 4222 	movx	@dptr,a
                                   4223 ;	COMMON\easyax5043.c:653: break;
                                   4224 ;	COMMON\easyax5043.c:656: default:
      002416 80 05            [24] 4225 	sjmp	00113$
      002418                       4226 00112$:
                                   4227 ;	COMMON\easyax5043.c:657: axradio_cb_transmitstart.st.error = AXRADIO_ERR_NOERROR;
      002418 90 03 12         [24] 4228 	mov	dptr,#(_axradio_cb_transmitstart + 0x0005)
      00241B E4               [12] 4229 	clr	a
      00241C F0               [24] 4230 	movx	@dptr,a
                                   4231 ;	COMMON\easyax5043.c:659: }
      00241D                       4232 00113$:
                                   4233 ;	COMMON\easyax5043.c:660: axradio_cb_transmitstart.st.time.t = axradio_timeanchor.radiotimer;
      00241D 90 00 BC         [24] 4234 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      002420 E0               [24] 4235 	movx	a,@dptr
      002421 FC               [12] 4236 	mov	r4,a
      002422 A3               [24] 4237 	inc	dptr
      002423 E0               [24] 4238 	movx	a,@dptr
      002424 FD               [12] 4239 	mov	r5,a
      002425 A3               [24] 4240 	inc	dptr
      002426 E0               [24] 4241 	movx	a,@dptr
      002427 FE               [12] 4242 	mov	r6,a
      002428 A3               [24] 4243 	inc	dptr
      002429 E0               [24] 4244 	movx	a,@dptr
      00242A FF               [12] 4245 	mov	r7,a
      00242B 90 03 13         [24] 4246 	mov	dptr,#(_axradio_cb_transmitstart + 0x0006)
      00242E EC               [12] 4247 	mov	a,r4
      00242F F0               [24] 4248 	movx	@dptr,a
      002430 ED               [12] 4249 	mov	a,r5
      002431 A3               [24] 4250 	inc	dptr
      002432 F0               [24] 4251 	movx	@dptr,a
      002433 EE               [12] 4252 	mov	a,r6
      002434 A3               [24] 4253 	inc	dptr
      002435 F0               [24] 4254 	movx	@dptr,a
      002436 EF               [12] 4255 	mov	a,r7
      002437 A3               [24] 4256 	inc	dptr
      002438 F0               [24] 4257 	movx	@dptr,a
                                   4258 ;	COMMON\easyax5043.c:661: wtimer_add_callback(&axradio_cb_transmitstart.cb);
      002439 90 03 0D         [24] 4259 	mov	dptr,#_axradio_cb_transmitstart
      00243C 12 6C C3         [24] 4260 	lcall	_wtimer_add_callback
                                   4261 ;	COMMON\easyax5043.c:662: break;
      00243F 02 27 47         [24] 4262 	ljmp	00167$
                                   4263 ;	COMMON\easyax5043.c:664: case trxstate_tx_longpreamble:
      002442                       4264 00114$:
                                   4265 ;	COMMON\easyax5043.c:665: case trxstate_tx_shortpreamble:
      002442                       4266 00115$:
                                   4267 ;	COMMON\easyax5043.c:666: case trxstate_tx_packet:
      002442                       4268 00116$:
                                   4269 ;	COMMON\easyax5043.c:667: dbglink_writestr("trxstate_tx_packet");
      002442 90 81 6F         [24] 4270 	mov	dptr,#___str_7
      002445 75 F0 80         [24] 4271 	mov	b,#0x80
      002448 12 74 0A         [24] 4272 	lcall	_dbglink_writestr
                                   4273 ;	COMMON\easyax5043.c:668: transmit_isr();
      00244B 12 1F D8         [24] 4274 	lcall	_transmit_isr
                                   4275 ;	COMMON\easyax5043.c:669: break;
      00244E 02 27 47         [24] 4276 	ljmp	00167$
                                   4277 ;	COMMON\easyax5043.c:671: case trxstate_tx_waitdone:
      002451                       4278 00117$:
                                   4279 ;	COMMON\easyax5043.c:672: AX5043_RADIOEVENTREQ0;
      002451 90 40 0F         [24] 4280 	mov	dptr,#_AX5043_RADIOEVENTREQ0
      002454 E0               [24] 4281 	movx	a,@dptr
                                   4282 ;	COMMON\easyax5043.c:673: dbglink_writestr("trxstate_tx_waitdone");
      002455 90 81 82         [24] 4283 	mov	dptr,#___str_8
      002458 75 F0 80         [24] 4284 	mov	b,#0x80
      00245B 12 74 0A         [24] 4285 	lcall	_dbglink_writestr
                                   4286 ;	COMMON\easyax5043.c:674: if (AX5043_RADIOSTATE != 0)
      00245E 90 40 1C         [24] 4287 	mov	dptr,#_AX5043_RADIOSTATE
      002461 E0               [24] 4288 	movx	a,@dptr
      002462 E0               [24] 4289 	movx	a,@dptr
      002463 60 03            [24] 4290 	jz	00265$
      002465 02 27 47         [24] 4291 	ljmp	00167$
      002468                       4292 00265$:
                                   4293 ;	COMMON\easyax5043.c:676: AX5043_RADIOEVENTMASK0 = 0x00;
      002468 90 40 09         [24] 4294 	mov	dptr,#_AX5043_RADIOEVENTMASK0
      00246B E4               [12] 4295 	clr	a
      00246C F0               [24] 4296 	movx	@dptr,a
                                   4297 ;	COMMON\easyax5043.c:677: switch (axradio_mode) {
      00246D AF 0B            [24] 4298 	mov	r7,_axradio_mode
      00246F BF 12 02         [24] 4299 	cjne	r7,#0x12,00266$
      002472 80 6A            [24] 4300 	sjmp	00131$
      002474                       4301 00266$:
      002474 BF 13 02         [24] 4302 	cjne	r7,#0x13,00267$
      002477 80 65            [24] 4303 	sjmp	00131$
      002479                       4304 00267$:
      002479 BF 20 02         [24] 4305 	cjne	r7,#0x20,00268$
      00247C 80 1D            [24] 4306 	sjmp	00120$
      00247E                       4307 00268$:
      00247E BF 21 02         [24] 4308 	cjne	r7,#0x21,00269$
      002481 80 36            [24] 4309 	sjmp	00125$
      002483                       4310 00269$:
      002483 BF 22 02         [24] 4311 	cjne	r7,#0x22,00270$
      002486 80 1C            [24] 4312 	sjmp	00121$
      002488                       4313 00270$:
      002488 BF 23 02         [24] 4314 	cjne	r7,#0x23,00271$
      00248B 80 3C            [24] 4315 	sjmp	00128$
      00248D                       4316 00271$:
      00248D BF 30 03         [24] 4317 	cjne	r7,#0x30,00272$
      002490 02 25 12         [24] 4318 	ljmp	00132$
      002493                       4319 00272$:
      002493 BF 31 02         [24] 4320 	cjne	r7,#0x31,00273$
      002496 80 39            [24] 4321 	sjmp	00129$
      002498                       4322 00273$:
      002498 02 25 1F         [24] 4323 	ljmp	00133$
                                   4324 ;	COMMON\easyax5043.c:678: case AXRADIO_MODE_ASYNC_RECEIVE:
      00249B                       4325 00120$:
                                   4326 ;	COMMON\easyax5043.c:679: ax5043_init_registers_rx();
      00249B 12 1C 26         [24] 4327 	lcall	_ax5043_init_registers_rx
                                   4328 ;	COMMON\easyax5043.c:680: ax5043_receiver_on_continuous();
      00249E 12 27 64         [24] 4329 	lcall	_ax5043_receiver_on_continuous
                                   4330 ;	COMMON\easyax5043.c:681: break;
      0024A1 02 25 22         [24] 4331 	ljmp	00134$
                                   4332 ;	COMMON\easyax5043.c:683: case AXRADIO_MODE_ACK_RECEIVE:
      0024A4                       4333 00121$:
                                   4334 ;	COMMON\easyax5043.c:684: if (axradio_cb_receive.st.error == AXRADIO_ERR_PACKETDONE) {
      0024A4 90 02 D9         [24] 4335 	mov	dptr,#(_axradio_cb_receive + 0x0005)
      0024A7 E0               [24] 4336 	movx	a,@dptr
      0024A8 FF               [12] 4337 	mov	r7,a
      0024A9 BF F0 08         [24] 4338 	cjne	r7,#0xf0,00124$
                                   4339 ;	COMMON\easyax5043.c:685: ax5043_init_registers_rx();
      0024AC 12 1C 26         [24] 4340 	lcall	_ax5043_init_registers_rx
                                   4341 ;	COMMON\easyax5043.c:686: ax5043_receiver_on_continuous();
      0024AF 12 27 64         [24] 4342 	lcall	_ax5043_receiver_on_continuous
                                   4343 ;	COMMON\easyax5043.c:687: break;
                                   4344 ;	COMMON\easyax5043.c:689: offxtal:
      0024B2 80 6E            [24] 4345 	sjmp	00134$
      0024B4                       4346 00124$:
                                   4347 ;	COMMON\easyax5043.c:690: ax5043_off_xtal();
      0024B4 12 28 C4         [24] 4348 	lcall	_ax5043_off_xtal
                                   4349 ;	COMMON\easyax5043.c:691: break;
                                   4350 ;	COMMON\easyax5043.c:693: case AXRADIO_MODE_WOR_RECEIVE:
      0024B7 80 69            [24] 4351 	sjmp	00134$
      0024B9                       4352 00125$:
                                   4353 ;	COMMON\easyax5043.c:694: if (axradio_cb_receive.st.error == AXRADIO_ERR_PACKETDONE) {
      0024B9 90 02 D9         [24] 4354 	mov	dptr,#(_axradio_cb_receive + 0x0005)
      0024BC E0               [24] 4355 	movx	a,@dptr
      0024BD FF               [12] 4356 	mov	r7,a
      0024BE BF F0 F3         [24] 4357 	cjne	r7,#0xf0,00124$
                                   4358 ;	COMMON\easyax5043.c:695: ax5043_init_registers_rx();
      0024C1 12 1C 26         [24] 4359 	lcall	_ax5043_init_registers_rx
                                   4360 ;	COMMON\easyax5043.c:696: ax5043_receiver_on_wor();
      0024C4 12 27 CC         [24] 4361 	lcall	_ax5043_receiver_on_wor
                                   4362 ;	COMMON\easyax5043.c:697: break;
                                   4363 ;	COMMON\easyax5043.c:701: case AXRADIO_MODE_WOR_ACK_RECEIVE:
      0024C7 80 59            [24] 4364 	sjmp	00134$
      0024C9                       4365 00128$:
                                   4366 ;	COMMON\easyax5043.c:702: ax5043_init_registers_rx();
      0024C9 12 1C 26         [24] 4367 	lcall	_ax5043_init_registers_rx
                                   4368 ;	COMMON\easyax5043.c:703: ax5043_receiver_on_wor();
      0024CC 12 27 CC         [24] 4369 	lcall	_ax5043_receiver_on_wor
                                   4370 ;	COMMON\easyax5043.c:704: break;
                                   4371 ;	COMMON\easyax5043.c:706: case AXRADIO_MODE_SYNC_ACK_MASTER:
      0024CF 80 51            [24] 4372 	sjmp	00134$
      0024D1                       4373 00129$:
                                   4374 ;	COMMON\easyax5043.c:707: axradio_txbuffer_len = axradio_framing_minpayloadlen;
      0024D1 90 80 04         [24] 4375 	mov	dptr,#_axradio_framing_minpayloadlen
      0024D4 E4               [12] 4376 	clr	a
      0024D5 93               [24] 4377 	movc	a,@a+dptr
      0024D6 FF               [12] 4378 	mov	r7,a
      0024D7 90 00 A7         [24] 4379 	mov	dptr,#_axradio_txbuffer_len
      0024DA F0               [24] 4380 	movx	@dptr,a
      0024DB E4               [12] 4381 	clr	a
      0024DC A3               [24] 4382 	inc	dptr
      0024DD F0               [24] 4383 	movx	@dptr,a
                                   4384 ;	COMMON\easyax5043.c:711: case AXRADIO_MODE_WOR_ACK_TRANSMIT:
      0024DE                       4385 00131$:
                                   4386 ;	COMMON\easyax5043.c:712: ax5043_init_registers_rx();
      0024DE 12 1C 26         [24] 4387 	lcall	_ax5043_init_registers_rx
                                   4388 ;	COMMON\easyax5043.c:713: ax5043_receiver_on_continuous();
      0024E1 12 27 64         [24] 4389 	lcall	_ax5043_receiver_on_continuous
                                   4390 ;	COMMON\easyax5043.c:714: wtimer_remove(&axradio_timer);
      0024E4 90 03 2B         [24] 4391 	mov	dptr,#_axradio_timer
      0024E7 12 76 94         [24] 4392 	lcall	_wtimer_remove
                                   4393 ;	COMMON\easyax5043.c:715: axradio_timer.time = axradio_framing_ack_timeout;
      0024EA 90 7F FA         [24] 4394 	mov	dptr,#_axradio_framing_ack_timeout
      0024ED E4               [12] 4395 	clr	a
      0024EE 93               [24] 4396 	movc	a,@a+dptr
      0024EF FC               [12] 4397 	mov	r4,a
      0024F0 74 01            [12] 4398 	mov	a,#0x01
      0024F2 93               [24] 4399 	movc	a,@a+dptr
      0024F3 FD               [12] 4400 	mov	r5,a
      0024F4 74 02            [12] 4401 	mov	a,#0x02
      0024F6 93               [24] 4402 	movc	a,@a+dptr
      0024F7 FE               [12] 4403 	mov	r6,a
      0024F8 74 03            [12] 4404 	mov	a,#0x03
      0024FA 93               [24] 4405 	movc	a,@a+dptr
      0024FB FF               [12] 4406 	mov	r7,a
      0024FC 90 03 2F         [24] 4407 	mov	dptr,#(_axradio_timer + 0x0004)
      0024FF EC               [12] 4408 	mov	a,r4
      002500 F0               [24] 4409 	movx	@dptr,a
      002501 ED               [12] 4410 	mov	a,r5
      002502 A3               [24] 4411 	inc	dptr
      002503 F0               [24] 4412 	movx	@dptr,a
      002504 EE               [12] 4413 	mov	a,r6
      002505 A3               [24] 4414 	inc	dptr
      002506 F0               [24] 4415 	movx	@dptr,a
      002507 EF               [12] 4416 	mov	a,r7
      002508 A3               [24] 4417 	inc	dptr
      002509 F0               [24] 4418 	movx	@dptr,a
                                   4419 ;	COMMON\easyax5043.c:716: wtimer0_addrelative(&axradio_timer);
      00250A 90 03 2B         [24] 4420 	mov	dptr,#_axradio_timer
      00250D 12 6C DD         [24] 4421 	lcall	_wtimer0_addrelative
                                   4422 ;	COMMON\easyax5043.c:717: break;
                                   4423 ;	COMMON\easyax5043.c:719: case AXRADIO_MODE_SYNC_MASTER:
      002510 80 10            [24] 4424 	sjmp	00134$
      002512                       4425 00132$:
                                   4426 ;	COMMON\easyax5043.c:720: axradio_txbuffer_len = axradio_framing_minpayloadlen;
      002512 90 80 04         [24] 4427 	mov	dptr,#_axradio_framing_minpayloadlen
      002515 E4               [12] 4428 	clr	a
      002516 93               [24] 4429 	movc	a,@a+dptr
      002517 FF               [12] 4430 	mov	r7,a
      002518 90 00 A7         [24] 4431 	mov	dptr,#_axradio_txbuffer_len
      00251B F0               [24] 4432 	movx	@dptr,a
      00251C E4               [12] 4433 	clr	a
      00251D A3               [24] 4434 	inc	dptr
      00251E F0               [24] 4435 	movx	@dptr,a
                                   4436 ;	COMMON\easyax5043.c:723: default:
      00251F                       4437 00133$:
                                   4438 ;	COMMON\easyax5043.c:724: ax5043_off();
      00251F 12 28 BB         [24] 4439 	lcall	_ax5043_off
                                   4440 ;	COMMON\easyax5043.c:726: }
      002522                       4441 00134$:
                                   4442 ;	COMMON\easyax5043.c:727: if (axradio_mode != AXRADIO_MODE_SYNC_MASTER &&
      002522 74 30            [12] 4443 	mov	a,#0x30
      002524 B5 0B 02         [24] 4444 	cjne	a,_axradio_mode,00278$
      002527 80 1A            [24] 4445 	sjmp	00136$
      002529                       4446 00278$:
                                   4447 ;	COMMON\easyax5043.c:728: axradio_mode != AXRADIO_MODE_SYNC_ACK_MASTER &&
      002529 74 31            [12] 4448 	mov	a,#0x31
      00252B B5 0B 02         [24] 4449 	cjne	a,_axradio_mode,00279$
      00252E 80 13            [24] 4450 	sjmp	00136$
      002530                       4451 00279$:
                                   4452 ;	COMMON\easyax5043.c:729: axradio_mode != AXRADIO_MODE_SYNC_SLAVE &&
      002530 74 32            [12] 4453 	mov	a,#0x32
      002532 B5 0B 02         [24] 4454 	cjne	a,_axradio_mode,00280$
      002535 80 0C            [24] 4455 	sjmp	00136$
      002537                       4456 00280$:
                                   4457 ;	COMMON\easyax5043.c:730: axradio_mode != AXRADIO_MODE_SYNC_ACK_SLAVE)
      002537 74 33            [12] 4458 	mov	a,#0x33
      002539 B5 0B 02         [24] 4459 	cjne	a,_axradio_mode,00281$
      00253C 80 05            [24] 4460 	sjmp	00136$
      00253E                       4461 00281$:
                                   4462 ;	COMMON\easyax5043.c:731: axradio_syncstate = syncstate_off;
      00253E 90 00 A6         [24] 4463 	mov	dptr,#_axradio_syncstate
      002541 E4               [12] 4464 	clr	a
      002542 F0               [24] 4465 	movx	@dptr,a
      002543                       4466 00136$:
                                   4467 ;	COMMON\easyax5043.c:732: update_timeanchor();
      002543 12 1B 43         [24] 4468 	lcall	_update_timeanchor
                                   4469 ;	COMMON\easyax5043.c:733: wtimer_remove_callback(&axradio_cb_transmitend.cb);
      002546 90 03 17         [24] 4470 	mov	dptr,#_axradio_cb_transmitend
      002549 12 78 A9         [24] 4471 	lcall	_wtimer_remove_callback
                                   4472 ;	COMMON\easyax5043.c:734: axradio_cb_transmitend.st.error = AXRADIO_ERR_NOERROR;
      00254C 90 03 1C         [24] 4473 	mov	dptr,#(_axradio_cb_transmitend + 0x0005)
      00254F E4               [12] 4474 	clr	a
      002550 F0               [24] 4475 	movx	@dptr,a
                                   4476 ;	COMMON\easyax5043.c:735: if (axradio_mode == AXRADIO_MODE_ACK_TRANSMIT ||
      002551 74 12            [12] 4477 	mov	a,#0x12
      002553 B5 0B 02         [24] 4478 	cjne	a,_axradio_mode,00282$
      002556 80 0C            [24] 4479 	sjmp	00140$
      002558                       4480 00282$:
                                   4481 ;	COMMON\easyax5043.c:736: axradio_mode == AXRADIO_MODE_WOR_ACK_TRANSMIT ||
      002558 74 13            [12] 4482 	mov	a,#0x13
      00255A B5 0B 02         [24] 4483 	cjne	a,_axradio_mode,00283$
      00255D 80 05            [24] 4484 	sjmp	00140$
      00255F                       4485 00283$:
                                   4486 ;	COMMON\easyax5043.c:737: axradio_mode == AXRADIO_MODE_SYNC_ACK_MASTER)
      00255F 74 31            [12] 4487 	mov	a,#0x31
      002561 B5 0B 06         [24] 4488 	cjne	a,_axradio_mode,00141$
      002564                       4489 00140$:
                                   4490 ;	COMMON\easyax5043.c:738: axradio_cb_transmitend.st.error = AXRADIO_ERR_BUSY;
      002564 90 03 1C         [24] 4491 	mov	dptr,#(_axradio_cb_transmitend + 0x0005)
      002567 74 02            [12] 4492 	mov	a,#0x02
      002569 F0               [24] 4493 	movx	@dptr,a
      00256A                       4494 00141$:
                                   4495 ;	COMMON\easyax5043.c:739: axradio_cb_transmitend.st.time.t = axradio_timeanchor.radiotimer;
      00256A 90 00 BC         [24] 4496 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      00256D E0               [24] 4497 	movx	a,@dptr
      00256E FC               [12] 4498 	mov	r4,a
      00256F A3               [24] 4499 	inc	dptr
      002570 E0               [24] 4500 	movx	a,@dptr
      002571 FD               [12] 4501 	mov	r5,a
      002572 A3               [24] 4502 	inc	dptr
      002573 E0               [24] 4503 	movx	a,@dptr
      002574 FE               [12] 4504 	mov	r6,a
      002575 A3               [24] 4505 	inc	dptr
      002576 E0               [24] 4506 	movx	a,@dptr
      002577 FF               [12] 4507 	mov	r7,a
      002578 90 03 1D         [24] 4508 	mov	dptr,#(_axradio_cb_transmitend + 0x0006)
      00257B EC               [12] 4509 	mov	a,r4
      00257C F0               [24] 4510 	movx	@dptr,a
      00257D ED               [12] 4511 	mov	a,r5
      00257E A3               [24] 4512 	inc	dptr
      00257F F0               [24] 4513 	movx	@dptr,a
      002580 EE               [12] 4514 	mov	a,r6
      002581 A3               [24] 4515 	inc	dptr
      002582 F0               [24] 4516 	movx	@dptr,a
      002583 EF               [12] 4517 	mov	a,r7
      002584 A3               [24] 4518 	inc	dptr
      002585 F0               [24] 4519 	movx	@dptr,a
                                   4520 ;	COMMON\easyax5043.c:740: wtimer_add_callback(&axradio_cb_transmitend.cb);
      002586 90 03 17         [24] 4521 	mov	dptr,#_axradio_cb_transmitend
      002589 12 6C C3         [24] 4522 	lcall	_wtimer_add_callback
                                   4523 ;	COMMON\easyax5043.c:741: break;
      00258C 02 27 47         [24] 4524 	ljmp	00167$
                                   4525 ;	COMMON\easyax5043.c:744: case trxstate_txcw_xtalwait:
      00258F                       4526 00144$:
                                   4527 ;	COMMON\easyax5043.c:745: dbglink_writestr("trxstate_txcw_xtalwait");
      00258F 90 81 97         [24] 4528 	mov	dptr,#___str_9
      002592 75 F0 80         [24] 4529 	mov	b,#0x80
      002595 12 74 0A         [24] 4530 	lcall	_dbglink_writestr
                                   4531 ;	COMMON\easyax5043.c:746: AX5043_IRQMASK1 = 0x00;
      002598 90 40 06         [24] 4532 	mov	dptr,#_AX5043_IRQMASK1
      00259B E4               [12] 4533 	clr	a
      00259C F0               [24] 4534 	movx	@dptr,a
                                   4535 ;	COMMON\easyax5043.c:747: AX5043_IRQMASK0 = 0x00;
      00259D 90 40 07         [24] 4536 	mov	dptr,#_AX5043_IRQMASK0
      0025A0 F0               [24] 4537 	movx	@dptr,a
                                   4538 ;	COMMON\easyax5043.c:748: AX5043_PWRMODE = AX5043_PWRSTATE_FULL_TX;
      0025A1 90 40 02         [24] 4539 	mov	dptr,#_AX5043_PWRMODE
      0025A4 74 0D            [12] 4540 	mov	a,#0x0d
      0025A6 F0               [24] 4541 	movx	@dptr,a
                                   4542 ;	COMMON\easyax5043.c:749: axradio_trxstate = trxstate_off;
      0025A7 75 0C 00         [24] 4543 	mov	_axradio_trxstate,#0x00
                                   4544 ;	COMMON\easyax5043.c:750: update_timeanchor();
      0025AA 12 1B 43         [24] 4545 	lcall	_update_timeanchor
                                   4546 ;	COMMON\easyax5043.c:751: wtimer_remove_callback(&axradio_cb_transmitstart.cb);
      0025AD 90 03 0D         [24] 4547 	mov	dptr,#_axradio_cb_transmitstart
      0025B0 12 78 A9         [24] 4548 	lcall	_wtimer_remove_callback
                                   4549 ;	COMMON\easyax5043.c:752: axradio_cb_transmitstart.st.error = AXRADIO_ERR_NOERROR;
      0025B3 90 03 12         [24] 4550 	mov	dptr,#(_axradio_cb_transmitstart + 0x0005)
      0025B6 E4               [12] 4551 	clr	a
      0025B7 F0               [24] 4552 	movx	@dptr,a
                                   4553 ;	COMMON\easyax5043.c:753: axradio_cb_transmitstart.st.time.t = axradio_timeanchor.radiotimer;
      0025B8 90 00 BC         [24] 4554 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      0025BB E0               [24] 4555 	movx	a,@dptr
      0025BC FC               [12] 4556 	mov	r4,a
      0025BD A3               [24] 4557 	inc	dptr
      0025BE E0               [24] 4558 	movx	a,@dptr
      0025BF FD               [12] 4559 	mov	r5,a
      0025C0 A3               [24] 4560 	inc	dptr
      0025C1 E0               [24] 4561 	movx	a,@dptr
      0025C2 FE               [12] 4562 	mov	r6,a
      0025C3 A3               [24] 4563 	inc	dptr
      0025C4 E0               [24] 4564 	movx	a,@dptr
      0025C5 FF               [12] 4565 	mov	r7,a
      0025C6 90 03 13         [24] 4566 	mov	dptr,#(_axradio_cb_transmitstart + 0x0006)
      0025C9 EC               [12] 4567 	mov	a,r4
      0025CA F0               [24] 4568 	movx	@dptr,a
      0025CB ED               [12] 4569 	mov	a,r5
      0025CC A3               [24] 4570 	inc	dptr
      0025CD F0               [24] 4571 	movx	@dptr,a
      0025CE EE               [12] 4572 	mov	a,r6
      0025CF A3               [24] 4573 	inc	dptr
      0025D0 F0               [24] 4574 	movx	@dptr,a
      0025D1 EF               [12] 4575 	mov	a,r7
      0025D2 A3               [24] 4576 	inc	dptr
      0025D3 F0               [24] 4577 	movx	@dptr,a
                                   4578 ;	COMMON\easyax5043.c:754: wtimer_add_callback(&axradio_cb_transmitstart.cb);
      0025D4 90 03 0D         [24] 4579 	mov	dptr,#_axradio_cb_transmitstart
      0025D7 12 6C C3         [24] 4580 	lcall	_wtimer_add_callback
                                   4581 ;	COMMON\easyax5043.c:755: break;
      0025DA 02 27 47         [24] 4582 	ljmp	00167$
                                   4583 ;	COMMON\easyax5043.c:757: case trxstate_txstream_xtalwait:
      0025DD                       4584 00145$:
                                   4585 ;	COMMON\easyax5043.c:758: dbglink_writestr("trxstate_txstream_xtalwait");
      0025DD 90 81 AE         [24] 4586 	mov	dptr,#___str_10
      0025E0 75 F0 80         [24] 4587 	mov	b,#0x80
      0025E3 12 74 0A         [24] 4588 	lcall	_dbglink_writestr
                                   4589 ;	COMMON\easyax5043.c:759: if (AX5043_IRQREQUEST1 & 0x01) {
      0025E6 90 40 0C         [24] 4590 	mov	dptr,#_AX5043_IRQREQUEST1
      0025E9 E0               [24] 4591 	movx	a,@dptr
      0025EA FF               [12] 4592 	mov	r7,a
      0025EB 20 E0 03         [24] 4593 	jb	acc.0,00286$
      0025EE 02 26 8D         [24] 4594 	ljmp	00155$
      0025F1                       4595 00286$:
                                   4596 ;	COMMON\easyax5043.c:760: AX5043_RADIOEVENTMASK0 = 0x03; // enable PLL settled and done event
      0025F1 90 40 09         [24] 4597 	mov	dptr,#_AX5043_RADIOEVENTMASK0
      0025F4 74 03            [12] 4598 	mov	a,#0x03
      0025F6 F0               [24] 4599 	movx	@dptr,a
                                   4600 ;	COMMON\easyax5043.c:761: AX5043_IRQMASK1 = 0x00;
      0025F7 90 40 06         [24] 4601 	mov	dptr,#_AX5043_IRQMASK1
      0025FA E4               [12] 4602 	clr	a
      0025FB F0               [24] 4603 	movx	@dptr,a
                                   4604 ;	COMMON\easyax5043.c:762: AX5043_IRQMASK0 = 0x40; // enable radio controller irq
      0025FC 90 40 07         [24] 4605 	mov	dptr,#_AX5043_IRQMASK0
      0025FF 74 40            [12] 4606 	mov	a,#0x40
      002601 F0               [24] 4607 	movx	@dptr,a
                                   4608 ;	COMMON\easyax5043.c:763: AX5043_PWRMODE = AX5043_PWRSTATE_FULL_TX;
      002602 90 40 02         [24] 4609 	mov	dptr,#_AX5043_PWRMODE
      002605 74 0D            [12] 4610 	mov	a,#0x0d
      002607 F0               [24] 4611 	movx	@dptr,a
                                   4612 ;	COMMON\easyax5043.c:764: axradio_trxstate = trxstate_txstream;
      002608 75 0C 10         [24] 4613 	mov	_axradio_trxstate,#0x10
                                   4614 ;	COMMON\easyax5043.c:766: goto txstreamdatacb;
      00260B 02 26 8D         [24] 4615 	ljmp	00155$
                                   4616 ;	COMMON\easyax5043.c:768: case trxstate_txstream:
      00260E                       4617 00148$:
                                   4618 ;	COMMON\easyax5043.c:769: dbglink_writestr("trxstate_txstream");
      00260E 90 81 C9         [24] 4619 	mov	dptr,#___str_11
      002611 75 F0 80         [24] 4620 	mov	b,#0x80
      002614 12 74 0A         [24] 4621 	lcall	_dbglink_writestr
                                   4622 ;	COMMON\easyax5043.c:771: uint8_t __autodata evt = AX5043_RADIOEVENTREQ0;
      002617 90 40 0F         [24] 4623 	mov	dptr,#_AX5043_RADIOEVENTREQ0
      00261A E0               [24] 4624 	movx	a,@dptr
                                   4625 ;	COMMON\easyax5043.c:772: if (evt & 0x03)
      00261B FF               [12] 4626 	mov	r7,a
      00261C 54 03            [12] 4627 	anl	a,#0x03
      00261E 60 07            [24] 4628 	jz	00150$
                                   4629 ;	COMMON\easyax5043.c:773: update_timeanchor();
      002620 C0 07            [24] 4630 	push	ar7
      002622 12 1B 43         [24] 4631 	lcall	_update_timeanchor
      002625 D0 07            [24] 4632 	pop	ar7
      002627                       4633 00150$:
                                   4634 ;	COMMON\easyax5043.c:774: if (evt & 0x01) {
      002627 EF               [12] 4635 	mov	a,r7
      002628 30 E0 31         [24] 4636 	jnb	acc.0,00152$
                                   4637 ;	COMMON\easyax5043.c:775: wtimer_remove_callback(&axradio_cb_transmitend.cb);
      00262B 90 03 17         [24] 4638 	mov	dptr,#_axradio_cb_transmitend
      00262E C0 07            [24] 4639 	push	ar7
      002630 12 78 A9         [24] 4640 	lcall	_wtimer_remove_callback
                                   4641 ;	COMMON\easyax5043.c:776: axradio_cb_transmitend.st.error = AXRADIO_ERR_NOERROR;
      002633 90 03 1C         [24] 4642 	mov	dptr,#(_axradio_cb_transmitend + 0x0005)
      002636 E4               [12] 4643 	clr	a
      002637 F0               [24] 4644 	movx	@dptr,a
                                   4645 ;	COMMON\easyax5043.c:777: axradio_cb_transmitend.st.time.t = axradio_timeanchor.radiotimer;
      002638 90 00 BC         [24] 4646 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      00263B E0               [24] 4647 	movx	a,@dptr
      00263C FB               [12] 4648 	mov	r3,a
      00263D A3               [24] 4649 	inc	dptr
      00263E E0               [24] 4650 	movx	a,@dptr
      00263F FC               [12] 4651 	mov	r4,a
      002640 A3               [24] 4652 	inc	dptr
      002641 E0               [24] 4653 	movx	a,@dptr
      002642 FD               [12] 4654 	mov	r5,a
      002643 A3               [24] 4655 	inc	dptr
      002644 E0               [24] 4656 	movx	a,@dptr
      002645 FE               [12] 4657 	mov	r6,a
      002646 90 03 1D         [24] 4658 	mov	dptr,#(_axradio_cb_transmitend + 0x0006)
      002649 EB               [12] 4659 	mov	a,r3
      00264A F0               [24] 4660 	movx	@dptr,a
      00264B EC               [12] 4661 	mov	a,r4
      00264C A3               [24] 4662 	inc	dptr
      00264D F0               [24] 4663 	movx	@dptr,a
      00264E ED               [12] 4664 	mov	a,r5
      00264F A3               [24] 4665 	inc	dptr
      002650 F0               [24] 4666 	movx	@dptr,a
      002651 EE               [12] 4667 	mov	a,r6
      002652 A3               [24] 4668 	inc	dptr
      002653 F0               [24] 4669 	movx	@dptr,a
                                   4670 ;	COMMON\easyax5043.c:778: wtimer_add_callback(&axradio_cb_transmitend.cb);
      002654 90 03 17         [24] 4671 	mov	dptr,#_axradio_cb_transmitend
      002657 12 6C C3         [24] 4672 	lcall	_wtimer_add_callback
      00265A D0 07            [24] 4673 	pop	ar7
      00265C                       4674 00152$:
                                   4675 ;	COMMON\easyax5043.c:780: if (evt & 0x02) {
      00265C EF               [12] 4676 	mov	a,r7
      00265D 30 E1 2D         [24] 4677 	jnb	acc.1,00155$
                                   4678 ;	COMMON\easyax5043.c:781: wtimer_remove_callback(&axradio_cb_transmitstart.cb);
      002660 90 03 0D         [24] 4679 	mov	dptr,#_axradio_cb_transmitstart
      002663 12 78 A9         [24] 4680 	lcall	_wtimer_remove_callback
                                   4681 ;	COMMON\easyax5043.c:782: axradio_cb_transmitstart.st.error = AXRADIO_ERR_NOERROR;
      002666 90 03 12         [24] 4682 	mov	dptr,#(_axradio_cb_transmitstart + 0x0005)
      002669 E4               [12] 4683 	clr	a
      00266A F0               [24] 4684 	movx	@dptr,a
                                   4685 ;	COMMON\easyax5043.c:783: axradio_cb_transmitstart.st.time.t = axradio_timeanchor.radiotimer;
      00266B 90 00 BC         [24] 4686 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      00266E E0               [24] 4687 	movx	a,@dptr
      00266F FC               [12] 4688 	mov	r4,a
      002670 A3               [24] 4689 	inc	dptr
      002671 E0               [24] 4690 	movx	a,@dptr
      002672 FD               [12] 4691 	mov	r5,a
      002673 A3               [24] 4692 	inc	dptr
      002674 E0               [24] 4693 	movx	a,@dptr
      002675 FE               [12] 4694 	mov	r6,a
      002676 A3               [24] 4695 	inc	dptr
      002677 E0               [24] 4696 	movx	a,@dptr
      002678 FF               [12] 4697 	mov	r7,a
      002679 90 03 13         [24] 4698 	mov	dptr,#(_axradio_cb_transmitstart + 0x0006)
      00267C EC               [12] 4699 	mov	a,r4
      00267D F0               [24] 4700 	movx	@dptr,a
      00267E ED               [12] 4701 	mov	a,r5
      00267F A3               [24] 4702 	inc	dptr
      002680 F0               [24] 4703 	movx	@dptr,a
      002681 EE               [12] 4704 	mov	a,r6
      002682 A3               [24] 4705 	inc	dptr
      002683 F0               [24] 4706 	movx	@dptr,a
      002684 EF               [12] 4707 	mov	a,r7
      002685 A3               [24] 4708 	inc	dptr
      002686 F0               [24] 4709 	movx	@dptr,a
                                   4710 ;	COMMON\easyax5043.c:784: wtimer_add_callback(&axradio_cb_transmitstart.cb);
      002687 90 03 0D         [24] 4711 	mov	dptr,#_axradio_cb_transmitstart
      00268A 12 6C C3         [24] 4712 	lcall	_wtimer_add_callback
                                   4713 ;	COMMON\easyax5043.c:787: txstreamdatacb:
      00268D                       4714 00155$:
                                   4715 ;	COMMON\easyax5043.c:788: if (AX5043_IRQREQUEST0 & AX5043_IRQMASK0 & 0x08) {
      00268D 90 40 0D         [24] 4716 	mov	dptr,#_AX5043_IRQREQUEST0
      002690 E0               [24] 4717 	movx	a,@dptr
      002691 FF               [12] 4718 	mov	r7,a
      002692 90 40 07         [24] 4719 	mov	dptr,#_AX5043_IRQMASK0
      002695 E0               [24] 4720 	movx	a,@dptr
      002696 FE               [12] 4721 	mov	r6,a
      002697 5F               [12] 4722 	anl	a,r7
      002698 20 E3 03         [24] 4723 	jb	acc.3,00290$
      00269B 02 27 47         [24] 4724 	ljmp	00167$
      00269E                       4725 00290$:
                                   4726 ;	COMMON\easyax5043.c:789: AX5043_IRQMASK0 &= (uint8_t)~0x08;
      00269E 90 40 07         [24] 4727 	mov	dptr,#_AX5043_IRQMASK0
      0026A1 E0               [24] 4728 	movx	a,@dptr
      0026A2 FF               [12] 4729 	mov	r7,a
      0026A3 74 F7            [12] 4730 	mov	a,#0xf7
      0026A5 5F               [12] 4731 	anl	a,r7
      0026A6 F0               [24] 4732 	movx	@dptr,a
                                   4733 ;	COMMON\easyax5043.c:790: update_timeanchor();
      0026A7 12 1B 43         [24] 4734 	lcall	_update_timeanchor
                                   4735 ;	COMMON\easyax5043.c:791: wtimer_remove_callback(&axradio_cb_transmitdata.cb);
      0026AA 90 03 21         [24] 4736 	mov	dptr,#_axradio_cb_transmitdata
      0026AD 12 78 A9         [24] 4737 	lcall	_wtimer_remove_callback
                                   4738 ;	COMMON\easyax5043.c:792: axradio_cb_transmitdata.st.error = AXRADIO_ERR_NOERROR;
      0026B0 90 03 26         [24] 4739 	mov	dptr,#(_axradio_cb_transmitdata + 0x0005)
      0026B3 E4               [12] 4740 	clr	a
      0026B4 F0               [24] 4741 	movx	@dptr,a
                                   4742 ;	COMMON\easyax5043.c:793: axradio_cb_transmitdata.st.time.t = axradio_timeanchor.radiotimer;
      0026B5 90 00 BC         [24] 4743 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      0026B8 E0               [24] 4744 	movx	a,@dptr
      0026B9 FC               [12] 4745 	mov	r4,a
      0026BA A3               [24] 4746 	inc	dptr
      0026BB E0               [24] 4747 	movx	a,@dptr
      0026BC FD               [12] 4748 	mov	r5,a
      0026BD A3               [24] 4749 	inc	dptr
      0026BE E0               [24] 4750 	movx	a,@dptr
      0026BF FE               [12] 4751 	mov	r6,a
      0026C0 A3               [24] 4752 	inc	dptr
      0026C1 E0               [24] 4753 	movx	a,@dptr
      0026C2 FF               [12] 4754 	mov	r7,a
      0026C3 90 03 27         [24] 4755 	mov	dptr,#(_axradio_cb_transmitdata + 0x0006)
      0026C6 EC               [12] 4756 	mov	a,r4
      0026C7 F0               [24] 4757 	movx	@dptr,a
      0026C8 ED               [12] 4758 	mov	a,r5
      0026C9 A3               [24] 4759 	inc	dptr
      0026CA F0               [24] 4760 	movx	@dptr,a
      0026CB EE               [12] 4761 	mov	a,r6
      0026CC A3               [24] 4762 	inc	dptr
      0026CD F0               [24] 4763 	movx	@dptr,a
      0026CE EF               [12] 4764 	mov	a,r7
      0026CF A3               [24] 4765 	inc	dptr
      0026D0 F0               [24] 4766 	movx	@dptr,a
                                   4767 ;	COMMON\easyax5043.c:794: wtimer_add_callback(&axradio_cb_transmitdata.cb);
      0026D1 90 03 21         [24] 4768 	mov	dptr,#_axradio_cb_transmitdata
      0026D4 12 6C C3         [24] 4769 	lcall	_wtimer_add_callback
                                   4770 ;	COMMON\easyax5043.c:796: break;
                                   4771 ;	COMMON\easyax5043.c:798: case trxstate_rxwor:
      0026D7 80 6E            [24] 4772 	sjmp	00167$
      0026D9                       4773 00158$:
                                   4774 ;	COMMON\easyax5043.c:799: dbglink_writestr("trxstate_rxwor");
      0026D9 90 81 DB         [24] 4775 	mov	dptr,#___str_12
      0026DC 75 F0 80         [24] 4776 	mov	b,#0x80
      0026DF 12 74 0A         [24] 4777 	lcall	_dbglink_writestr
                                   4778 ;	COMMON\easyax5043.c:802: if( AX5043_IRQREQUEST0 & 0x80 ) // vdda ready (note irqinversion does not act upon AX5043_IRQREQUEST0)
      0026E2 90 40 0D         [24] 4779 	mov	dptr,#_AX5043_IRQREQUEST0
      0026E5 E0               [24] 4780 	movx	a,@dptr
      0026E6 FF               [12] 4781 	mov	r7,a
      0026E7 30 E7 0B         [24] 4782 	jnb	acc.7,00160$
                                   4783 ;	COMMON\easyax5043.c:804: AX5043_IRQINVERSION0 |= 0x80; // invert pwr irq, so it does not fire continuously
      0026EA 90 40 0B         [24] 4784 	mov	dptr,#_AX5043_IRQINVERSION0
      0026ED E0               [24] 4785 	movx	a,@dptr
      0026EE FF               [12] 4786 	mov	r7,a
      0026EF 74 80            [12] 4787 	mov	a,#0x80
      0026F1 4F               [12] 4788 	orl	a,r7
      0026F2 F0               [24] 4789 	movx	@dptr,a
      0026F3 80 09            [24] 4790 	sjmp	00161$
      0026F5                       4791 00160$:
                                   4792 ;	COMMON\easyax5043.c:808: AX5043_IRQINVERSION0 &= (uint8_t)~0x80; // drop pwr irq inversion --> armed again
      0026F5 90 40 0B         [24] 4793 	mov	dptr,#_AX5043_IRQINVERSION0
      0026F8 E0               [24] 4794 	movx	a,@dptr
      0026F9 FF               [12] 4795 	mov	r7,a
      0026FA 74 7F            [12] 4796 	mov	a,#0x7f
      0026FC 5F               [12] 4797 	anl	a,r7
      0026FD F0               [24] 4798 	movx	@dptr,a
      0026FE                       4799 00161$:
                                   4800 ;	COMMON\easyax5043.c:812: if( AX5043_IRQREQUEST1 & 0x01 ) // XTAL ready
      0026FE 90 40 0C         [24] 4801 	mov	dptr,#_AX5043_IRQREQUEST1
      002701 E0               [24] 4802 	movx	a,@dptr
      002702 FF               [12] 4803 	mov	r7,a
      002703 30 E0 0B         [24] 4804 	jnb	acc.0,00163$
                                   4805 ;	COMMON\easyax5043.c:814: AX5043_IRQINVERSION1 |= 0x01; // invert the xtal ready irq so it does not fire continuously
      002706 90 40 0A         [24] 4806 	mov	dptr,#_AX5043_IRQINVERSION1
      002709 E0               [24] 4807 	movx	a,@dptr
      00270A FF               [12] 4808 	mov	r7,a
      00270B 74 01            [12] 4809 	mov	a,#0x01
      00270D 4F               [12] 4810 	orl	a,r7
      00270E F0               [24] 4811 	movx	@dptr,a
      00270F 80 2A            [24] 4812 	sjmp	00165$
      002711                       4813 00163$:
                                   4814 ;	COMMON\easyax5043.c:818: AX5043_IRQINVERSION1 &= (uint8_t)~0x01; // drop xtal ready irq inversion --> armed again for next wake-up
      002711 90 40 0A         [24] 4815 	mov	dptr,#_AX5043_IRQINVERSION1
      002714 E0               [24] 4816 	movx	a,@dptr
      002715 FF               [12] 4817 	mov	r7,a
      002716 74 FE            [12] 4818 	mov	a,#0xfe
      002718 5F               [12] 4819 	anl	a,r7
      002719 F0               [24] 4820 	movx	@dptr,a
                                   4821 ;	COMMON\easyax5043.c:819: AX5043_0xF30 = f30_saved;
      00271A 90 09 A5         [24] 4822 	mov	dptr,#_f30_saved
      00271D E0               [24] 4823 	movx	a,@dptr
      00271E 90 4F 30         [24] 4824 	mov	dptr,#_AX5043_0xF30
      002721 F0               [24] 4825 	movx	@dptr,a
                                   4826 ;	COMMON\easyax5043.c:820: AX5043_0xF31 = f31_saved;
      002722 90 09 A6         [24] 4827 	mov	dptr,#_f31_saved
      002725 E0               [24] 4828 	movx	a,@dptr
      002726 90 4F 31         [24] 4829 	mov	dptr,#_AX5043_0xF31
      002729 F0               [24] 4830 	movx	@dptr,a
                                   4831 ;	COMMON\easyax5043.c:821: AX5043_0xF32 = f32_saved;
      00272A 90 09 A7         [24] 4832 	mov	dptr,#_f32_saved
      00272D E0               [24] 4833 	movx	a,@dptr
      00272E 90 4F 32         [24] 4834 	mov	dptr,#_AX5043_0xF32
      002731 F0               [24] 4835 	movx	@dptr,a
                                   4836 ;	COMMON\easyax5043.c:822: AX5043_0xF33 = f33_saved;
      002732 90 09 A8         [24] 4837 	mov	dptr,#_f33_saved
      002735 E0               [24] 4838 	movx	a,@dptr
      002736 FF               [12] 4839 	mov	r7,a
      002737 90 4F 33         [24] 4840 	mov	dptr,#_AX5043_0xF33
      00273A F0               [24] 4841 	movx	@dptr,a
                                   4842 ;	COMMON\easyax5043.c:826: case trxstate_rx:
      00273B                       4843 00165$:
                                   4844 ;	COMMON\easyax5043.c:827: dbglink_writestr("trxstate_rx");
      00273B 90 81 EA         [24] 4845 	mov	dptr,#___str_13
      00273E 75 F0 80         [24] 4846 	mov	b,#0x80
      002741 12 74 0A         [24] 4847 	lcall	_dbglink_writestr
                                   4848 ;	COMMON\easyax5043.c:828: receive_isr();
      002744 12 1C 2C         [24] 4849 	lcall	_receive_isr
                                   4850 ;	COMMON\easyax5043.c:831: } // end switch(axradio_trxstate)
      002747                       4851 00167$:
      002747 D0 D0            [24] 4852 	pop	psw
      002749 D0 00            [24] 4853 	pop	(0+0)
      00274B D0 01            [24] 4854 	pop	(0+1)
      00274D D0 02            [24] 4855 	pop	(0+2)
      00274F D0 03            [24] 4856 	pop	(0+3)
      002751 D0 04            [24] 4857 	pop	(0+4)
      002753 D0 05            [24] 4858 	pop	(0+5)
      002755 D0 06            [24] 4859 	pop	(0+6)
      002757 D0 07            [24] 4860 	pop	(0+7)
      002759 D0 83            [24] 4861 	pop	dph
      00275B D0 82            [24] 4862 	pop	dpl
      00275D D0 F0            [24] 4863 	pop	b
      00275F D0 E0            [24] 4864 	pop	acc
      002761 D0 21            [24] 4865 	pop	bits
      002763 32               [24] 4866 	reti
                                   4867 ;------------------------------------------------------------
                                   4868 ;Allocation info for local variables in function 'ax5043_receiver_on_continuous'
                                   4869 ;------------------------------------------------------------
                                   4870 ;rschanged_int             Allocated to registers r6 
                                   4871 ;------------------------------------------------------------
                                   4872 ;	COMMON\easyax5043.c:835: __reentrantb void ax5043_receiver_on_continuous(void) __reentrant
                                   4873 ;	-----------------------------------------
                                   4874 ;	 function ax5043_receiver_on_continuous
                                   4875 ;	-----------------------------------------
      002764                       4876 _ax5043_receiver_on_continuous:
                                   4877 ;	COMMON\easyax5043.c:837: uint8_t rschanged_int = (axradio_framing_enable_sfdcallback | (axradio_mode == AXRADIO_MODE_SYNC_ACK_SLAVE) | (axradio_mode == AXRADIO_MODE_SYNC_SLAVE) );
      002764 74 33            [12] 4878 	mov	a,#0x33
      002766 B5 0B 04         [24] 4879 	cjne	a,_axradio_mode,00114$
      002769 74 01            [12] 4880 	mov	a,#0x01
      00276B 80 01            [24] 4881 	sjmp	00115$
      00276D                       4882 00114$:
      00276D E4               [12] 4883 	clr	a
      00276E                       4884 00115$:
      00276E FF               [12] 4885 	mov	r7,a
      00276F 90 7F F9         [24] 4886 	mov	dptr,#_axradio_framing_enable_sfdcallback
      002772 E4               [12] 4887 	clr	a
      002773 93               [24] 4888 	movc	a,@a+dptr
      002774 FE               [12] 4889 	mov	r6,a
      002775 42 07            [12] 4890 	orl	ar7,a
      002777 74 32            [12] 4891 	mov	a,#0x32
      002779 B5 0B 04         [24] 4892 	cjne	a,_axradio_mode,00116$
      00277C 74 01            [12] 4893 	mov	a,#0x01
      00277E 80 01            [24] 4894 	sjmp	00117$
      002780                       4895 00116$:
      002780 E4               [12] 4896 	clr	a
      002781                       4897 00117$:
      002781 42 07            [12] 4898 	orl	ar7,a
                                   4899 ;	COMMON\easyax5043.c:838: if(rschanged_int)
      002783 EF               [12] 4900 	mov	a,r7
      002784 FE               [12] 4901 	mov	r6,a
      002785 60 06            [24] 4902 	jz	00102$
                                   4903 ;	COMMON\easyax5043.c:839: AX5043_RADIOEVENTMASK0 = 0x04;
      002787 90 40 09         [24] 4904 	mov	dptr,#_AX5043_RADIOEVENTMASK0
      00278A 74 04            [12] 4905 	mov	a,#0x04
      00278C F0               [24] 4906 	movx	@dptr,a
      00278D                       4907 00102$:
                                   4908 ;	COMMON\easyax5043.c:840: AX5043_RSSIREFERENCE = axradio_phy_rssireference;
      00278D 90 7F D8         [24] 4909 	mov	dptr,#_axradio_phy_rssireference
      002790 E4               [12] 4910 	clr	a
      002791 93               [24] 4911 	movc	a,@a+dptr
      002792 90 42 2C         [24] 4912 	mov	dptr,#_AX5043_RSSIREFERENCE
      002795 F0               [24] 4913 	movx	@dptr,a
                                   4914 ;	COMMON\easyax5043.c:841: ax5043_set_registers_rxcont();
      002796 C0 06            [24] 4915 	push	ar6
      002798 12 15 7F         [24] 4916 	lcall	_ax5043_set_registers_rxcont
      00279B D0 06            [24] 4917 	pop	ar6
                                   4918 ;	COMMON\easyax5043.c:854: AX5043_PKTSTOREFLAGS &= (uint8_t)~0x40;
      00279D 90 42 32         [24] 4919 	mov	dptr,#_AX5043_PKTSTOREFLAGS
      0027A0 E0               [24] 4920 	movx	a,@dptr
      0027A1 FF               [12] 4921 	mov	r7,a
      0027A2 74 BF            [12] 4922 	mov	a,#0xbf
      0027A4 5F               [12] 4923 	anl	a,r7
      0027A5 F0               [24] 4924 	movx	@dptr,a
                                   4925 ;	COMMON\easyax5043.c:857: AX5043_FIFOSTAT = 3; // clear FIFO data & flags
      0027A6 90 40 28         [24] 4926 	mov	dptr,#_AX5043_FIFOSTAT
      0027A9 74 03            [12] 4927 	mov	a,#0x03
      0027AB F0               [24] 4928 	movx	@dptr,a
                                   4929 ;	COMMON\easyax5043.c:858: AX5043_PWRMODE = AX5043_PWRSTATE_FULL_RX;
      0027AC 90 40 02         [24] 4930 	mov	dptr,#_AX5043_PWRMODE
      0027AF 74 09            [12] 4931 	mov	a,#0x09
      0027B1 F0               [24] 4932 	movx	@dptr,a
                                   4933 ;	COMMON\easyax5043.c:859: axradio_trxstate = trxstate_rx;
      0027B2 75 0C 01         [24] 4934 	mov	_axradio_trxstate,#0x01
                                   4935 ;	COMMON\easyax5043.c:860: if(rschanged_int)
      0027B5 EE               [12] 4936 	mov	a,r6
      0027B6 60 08            [24] 4937 	jz	00104$
                                   4938 ;	COMMON\easyax5043.c:861: AX5043_IRQMASK0 = 0x41; //  enable FIFO not empty / radio controller irq
      0027B8 90 40 07         [24] 4939 	mov	dptr,#_AX5043_IRQMASK0
      0027BB 74 41            [12] 4940 	mov	a,#0x41
      0027BD F0               [24] 4941 	movx	@dptr,a
      0027BE 80 06            [24] 4942 	sjmp	00105$
      0027C0                       4943 00104$:
                                   4944 ;	COMMON\easyax5043.c:863: AX5043_IRQMASK0 = 0x01; //  enable FIFO not empty
      0027C0 90 40 07         [24] 4945 	mov	dptr,#_AX5043_IRQMASK0
      0027C3 74 01            [12] 4946 	mov	a,#0x01
      0027C5 F0               [24] 4947 	movx	@dptr,a
      0027C6                       4948 00105$:
                                   4949 ;	COMMON\easyax5043.c:864: AX5043_IRQMASK1 = 0x00;
      0027C6 90 40 06         [24] 4950 	mov	dptr,#_AX5043_IRQMASK1
      0027C9 E4               [12] 4951 	clr	a
      0027CA F0               [24] 4952 	movx	@dptr,a
      0027CB 22               [24] 4953 	ret
                                   4954 ;------------------------------------------------------------
                                   4955 ;Allocation info for local variables in function 'ax5043_receiver_on_wor'
                                   4956 ;------------------------------------------------------------
                                   4957 ;wp                        Allocated to registers r6 r7 
                                   4958 ;------------------------------------------------------------
                                   4959 ;	COMMON\easyax5043.c:867: __reentrantb void ax5043_receiver_on_wor(void) __reentrant
                                   4960 ;	-----------------------------------------
                                   4961 ;	 function ax5043_receiver_on_wor
                                   4962 ;	-----------------------------------------
      0027CC                       4963 _ax5043_receiver_on_wor:
                                   4964 ;	COMMON\easyax5043.c:869: AX5043_BGNDRSSIGAIN = 0x02;
      0027CC 90 42 2E         [24] 4965 	mov	dptr,#_AX5043_BGNDRSSIGAIN
      0027CF 74 02            [12] 4966 	mov	a,#0x02
      0027D1 F0               [24] 4967 	movx	@dptr,a
                                   4968 ;	COMMON\easyax5043.c:870: if(axradio_framing_enable_sfdcallback)
      0027D2 90 7F F9         [24] 4969 	mov	dptr,#_axradio_framing_enable_sfdcallback
      0027D5 E4               [12] 4970 	clr	a
      0027D6 93               [24] 4971 	movc	a,@a+dptr
      0027D7 60 06            [24] 4972 	jz	00102$
                                   4973 ;	COMMON\easyax5043.c:871: AX5043_RADIOEVENTMASK0 = 0x04;
      0027D9 90 40 09         [24] 4974 	mov	dptr,#_AX5043_RADIOEVENTMASK0
      0027DC 74 04            [12] 4975 	mov	a,#0x04
      0027DE F0               [24] 4976 	movx	@dptr,a
      0027DF                       4977 00102$:
                                   4978 ;	COMMON\easyax5043.c:872: AX5043_FIFOSTAT = 3; // clear FIFO data & flags
      0027DF 90 40 28         [24] 4979 	mov	dptr,#_AX5043_FIFOSTAT
      0027E2 74 03            [12] 4980 	mov	a,#0x03
      0027E4 F0               [24] 4981 	movx	@dptr,a
                                   4982 ;	COMMON\easyax5043.c:873: AX5043_LPOSCCONFIG = 0x01; // start LPOSC, slow mode
      0027E5 90 43 10         [24] 4983 	mov	dptr,#_AX5043_LPOSCCONFIG
      0027E8 74 01            [12] 4984 	mov	a,#0x01
      0027EA F0               [24] 4985 	movx	@dptr,a
                                   4986 ;	COMMON\easyax5043.c:874: AX5043_RSSIREFERENCE = axradio_phy_rssireference;
      0027EB 90 7F D8         [24] 4987 	mov	dptr,#_axradio_phy_rssireference
      0027EE E4               [12] 4988 	clr	a
      0027EF 93               [24] 4989 	movc	a,@a+dptr
      0027F0 90 42 2C         [24] 4990 	mov	dptr,#_AX5043_RSSIREFERENCE
      0027F3 F0               [24] 4991 	movx	@dptr,a
                                   4992 ;	COMMON\easyax5043.c:875: ax5043_set_registers_rxwor();
      0027F4 12 15 6C         [24] 4993 	lcall	_ax5043_set_registers_rxwor
                                   4994 ;	COMMON\easyax5043.c:876: AX5043_PKTSTOREFLAGS &= (uint8_t)~0x40;
      0027F7 90 42 32         [24] 4995 	mov	dptr,#_AX5043_PKTSTOREFLAGS
      0027FA E0               [24] 4996 	movx	a,@dptr
      0027FB FF               [12] 4997 	mov	r7,a
      0027FC 74 BF            [12] 4998 	mov	a,#0xbf
      0027FE 5F               [12] 4999 	anl	a,r7
      0027FF F0               [24] 5000 	movx	@dptr,a
                                   5001 ;	COMMON\easyax5043.c:878: AX5043_PWRMODE = AX5043_PWRSTATE_WOR_RX;
      002800 90 40 02         [24] 5002 	mov	dptr,#_AX5043_PWRMODE
      002803 74 0B            [12] 5003 	mov	a,#0x0b
      002805 F0               [24] 5004 	movx	@dptr,a
                                   5005 ;	COMMON\easyax5043.c:879: axradio_trxstate = trxstate_rxwor;
      002806 75 0C 02         [24] 5006 	mov	_axradio_trxstate,#0x02
                                   5007 ;	COMMON\easyax5043.c:880: if(axradio_framing_enable_sfdcallback)
      002809 90 7F F9         [24] 5008 	mov	dptr,#_axradio_framing_enable_sfdcallback
      00280C E4               [12] 5009 	clr	a
      00280D 93               [24] 5010 	movc	a,@a+dptr
      00280E 60 08            [24] 5011 	jz	00104$
                                   5012 ;	COMMON\easyax5043.c:881: AX5043_IRQMASK0 = 0x41; //  enable FIFO not empty / radio controller irq
      002810 90 40 07         [24] 5013 	mov	dptr,#_AX5043_IRQMASK0
      002813 74 41            [12] 5014 	mov	a,#0x41
      002815 F0               [24] 5015 	movx	@dptr,a
      002816 80 06            [24] 5016 	sjmp	00105$
      002818                       5017 00104$:
                                   5018 ;	COMMON\easyax5043.c:883: AX5043_IRQMASK0 = 0x01; //  enable FIFO not empty
      002818 90 40 07         [24] 5019 	mov	dptr,#_AX5043_IRQMASK0
      00281B 74 01            [12] 5020 	mov	a,#0x01
      00281D F0               [24] 5021 	movx	@dptr,a
      00281E                       5022 00105$:
                                   5023 ;	COMMON\easyax5043.c:885: if( ( (PALTRADIO & 0x40) && ((AX5043_PINFUNCPWRAMP & 0x0F) == 0x07) ) || ( (PALTRADIO & 0x80) && ( (AX5043_PINFUNCANTSEL & 0x07 ) == 0x04 ) ) ) // pass through of TCXO_EN
      00281E 90 70 46         [24] 5024 	mov	dptr,#_PALTRADIO
      002821 E0               [24] 5025 	movx	a,@dptr
      002822 FF               [12] 5026 	mov	r7,a
      002823 30 E6 0D         [24] 5027 	jnb	acc.6,00110$
      002826 90 40 26         [24] 5028 	mov	dptr,#_AX5043_PINFUNCPWRAMP
      002829 E0               [24] 5029 	movx	a,@dptr
      00282A FF               [12] 5030 	mov	r7,a
      00282B 53 07 0F         [24] 5031 	anl	ar7,#0x0f
      00282E BF 07 02         [24] 5032 	cjne	r7,#0x07,00128$
      002831 80 13            [24] 5033 	sjmp	00106$
      002833                       5034 00128$:
      002833                       5035 00110$:
      002833 90 70 46         [24] 5036 	mov	dptr,#_PALTRADIO
      002836 E0               [24] 5037 	movx	a,@dptr
      002837 FF               [12] 5038 	mov	r7,a
      002838 30 E7 1A         [24] 5039 	jnb	acc.7,00107$
      00283B 90 40 25         [24] 5040 	mov	dptr,#_AX5043_PINFUNCANTSEL
      00283E E0               [24] 5041 	movx	a,@dptr
      00283F FF               [12] 5042 	mov	r7,a
      002840 53 07 07         [24] 5043 	anl	ar7,#0x07
      002843 BF 04 0F         [24] 5044 	cjne	r7,#0x04,00107$
      002846                       5045 00106$:
                                   5046 ;	COMMON\easyax5043.c:888: AX5043_IRQMASK0 |= 0x80; // power irq (AX8052F143 WOR with TCXO)
      002846 90 40 07         [24] 5047 	mov	dptr,#_AX5043_IRQMASK0
      002849 E0               [24] 5048 	movx	a,@dptr
      00284A FF               [12] 5049 	mov	r7,a
      00284B 74 80            [12] 5050 	mov	a,#0x80
      00284D 4F               [12] 5051 	orl	a,r7
      00284E F0               [24] 5052 	movx	@dptr,a
                                   5053 ;	COMMON\easyax5043.c:889: AX5043_POWIRQMASK = 0x90; // interrupt when vddana ready (AX8052F143 WOR with TCXO)
      00284F 90 40 05         [24] 5054 	mov	dptr,#_AX5043_POWIRQMASK
      002852 74 90            [12] 5055 	mov	a,#0x90
      002854 F0               [24] 5056 	movx	@dptr,a
      002855                       5057 00107$:
                                   5058 ;	COMMON\easyax5043.c:892: AX5043_IRQMASK1 = 0x01; // xtal ready
      002855 90 40 06         [24] 5059 	mov	dptr,#_AX5043_IRQMASK1
      002858 74 01            [12] 5060 	mov	a,#0x01
      00285A F0               [24] 5061 	movx	@dptr,a
                                   5062 ;	COMMON\easyax5043.c:894: uint16_t wp = axradio_wor_period;
      00285B 90 80 05         [24] 5063 	mov	dptr,#_axradio_wor_period
      00285E E4               [12] 5064 	clr	a
      00285F 93               [24] 5065 	movc	a,@a+dptr
      002860 FE               [12] 5066 	mov	r6,a
      002861 74 01            [12] 5067 	mov	a,#0x01
      002863 93               [24] 5068 	movc	a,@a+dptr
                                   5069 ;	COMMON\easyax5043.c:895: AX5043_WAKEUPFREQ1 = (wp >> 8) & 0xFF;
      002864 FF               [12] 5070 	mov	r7,a
      002865 FD               [12] 5071 	mov	r5,a
      002866 90 40 6C         [24] 5072 	mov	dptr,#_AX5043_WAKEUPFREQ1
      002869 ED               [12] 5073 	mov	a,r5
      00286A F0               [24] 5074 	movx	@dptr,a
                                   5075 ;	COMMON\easyax5043.c:896: AX5043_WAKEUPFREQ0 = (wp >> 0) & 0xFF;  // actually wakeup period measured in LP OSC cycles
      00286B 8E 05            [24] 5076 	mov	ar5,r6
      00286D 90 40 6D         [24] 5077 	mov	dptr,#_AX5043_WAKEUPFREQ0
      002870 ED               [12] 5078 	mov	a,r5
      002871 F0               [24] 5079 	movx	@dptr,a
                                   5080 ;	COMMON\easyax5043.c:897: wp += radio_read16((uint16_t)&AX5043_WAKEUPTIMER1);
      002872 7C 68            [12] 5081 	mov	r4,#_AX5043_WAKEUPTIMER1
      002874 7D 40            [12] 5082 	mov	r5,#(_AX5043_WAKEUPTIMER1 >> 8)
      002876 8C 82            [24] 5083 	mov	dpl,r4
      002878 8D 83            [24] 5084 	mov	dph,r5
      00287A 12 6F 1E         [24] 5085 	lcall	_radio_read16
      00287D AC 82            [24] 5086 	mov	r4,dpl
      00287F AD 83            [24] 5087 	mov	r5,dph
      002881 EC               [12] 5088 	mov	a,r4
      002882 2E               [12] 5089 	add	a,r6
      002883 FE               [12] 5090 	mov	r6,a
      002884 ED               [12] 5091 	mov	a,r5
      002885 3F               [12] 5092 	addc	a,r7
                                   5093 ;	COMMON\easyax5043.c:898: AX5043_WAKEUP1 = (wp >> 8) & 0xFF;
      002886 FD               [12] 5094 	mov	r5,a
      002887 90 40 6A         [24] 5095 	mov	dptr,#_AX5043_WAKEUP1
      00288A ED               [12] 5096 	mov	a,r5
      00288B F0               [24] 5097 	movx	@dptr,a
                                   5098 ;	COMMON\easyax5043.c:899: AX5043_WAKEUP0 = (wp >> 0) & 0xFF;
      00288C 90 40 6B         [24] 5099 	mov	dptr,#_AX5043_WAKEUP0
      00288F EE               [12] 5100 	mov	a,r6
      002890 F0               [24] 5101 	movx	@dptr,a
      002891 22               [24] 5102 	ret
                                   5103 ;------------------------------------------------------------
                                   5104 ;Allocation info for local variables in function 'ax5043_prepare_tx'
                                   5105 ;------------------------------------------------------------
                                   5106 ;	COMMON\easyax5043.c:902: __reentrantb void ax5043_prepare_tx(void) __reentrant
                                   5107 ;	-----------------------------------------
                                   5108 ;	 function ax5043_prepare_tx
                                   5109 ;	-----------------------------------------
      002892                       5110 _ax5043_prepare_tx:
                                   5111 ;	COMMON\easyax5043.c:904: AX5043_PWRMODE = AX5043_PWRSTATE_XTAL_ON;
      002892 90 40 02         [24] 5112 	mov	dptr,#_AX5043_PWRMODE
      002895 74 05            [12] 5113 	mov	a,#0x05
      002897 F0               [24] 5114 	movx	@dptr,a
                                   5115 ;	COMMON\easyax5043.c:905: AX5043_PWRMODE = AX5043_PWRSTATE_FIFO_ON;
      002898 74 07            [12] 5116 	mov	a,#0x07
      00289A F0               [24] 5117 	movx	@dptr,a
                                   5118 ;	COMMON\easyax5043.c:906: ax5043_init_registers_tx();
      00289B 12 1C 20         [24] 5119 	lcall	_ax5043_init_registers_tx
                                   5120 ;	COMMON\easyax5043.c:907: AX5043_FIFOTHRESH1 = 0;
      00289E 90 40 2E         [24] 5121 	mov	dptr,#_AX5043_FIFOTHRESH1
      0028A1 E4               [12] 5122 	clr	a
      0028A2 F0               [24] 5123 	movx	@dptr,a
                                   5124 ;	COMMON\easyax5043.c:908: AX5043_FIFOTHRESH0 = 0x80;
      0028A3 90 40 2F         [24] 5125 	mov	dptr,#_AX5043_FIFOTHRESH0
      0028A6 74 80            [12] 5126 	mov	a,#0x80
      0028A8 F0               [24] 5127 	movx	@dptr,a
                                   5128 ;	COMMON\easyax5043.c:909: axradio_trxstate = trxstate_tx_xtalwait;
      0028A9 75 0C 09         [24] 5129 	mov	_axradio_trxstate,#0x09
                                   5130 ;	COMMON\easyax5043.c:910: AX5043_IRQMASK0 = 0x00;
      0028AC 90 40 07         [24] 5131 	mov	dptr,#_AX5043_IRQMASK0
      0028AF E4               [12] 5132 	clr	a
      0028B0 F0               [24] 5133 	movx	@dptr,a
                                   5134 ;	COMMON\easyax5043.c:911: AX5043_IRQMASK1 = 0x01; // enable xtal ready interrupt
      0028B1 90 40 06         [24] 5135 	mov	dptr,#_AX5043_IRQMASK1
      0028B4 04               [12] 5136 	inc	a
      0028B5 F0               [24] 5137 	movx	@dptr,a
                                   5138 ;	COMMON\easyax5043.c:912: AX5043_POWSTICKYSTAT; // clear pwr management sticky status --> brownout gate works
      0028B6 90 40 04         [24] 5139 	mov	dptr,#_AX5043_POWSTICKYSTAT
      0028B9 E0               [24] 5140 	movx	a,@dptr
      0028BA 22               [24] 5141 	ret
                                   5142 ;------------------------------------------------------------
                                   5143 ;Allocation info for local variables in function 'ax5043_off'
                                   5144 ;------------------------------------------------------------
                                   5145 ;	COMMON\easyax5043.c:915: __reentrantb void ax5043_off(void) __reentrant
                                   5146 ;	-----------------------------------------
                                   5147 ;	 function ax5043_off
                                   5148 ;	-----------------------------------------
      0028BB                       5149 _ax5043_off:
                                   5150 ;	COMMON\easyax5043.c:917: ax5043_off_xtal();
      0028BB 12 28 C4         [24] 5151 	lcall	_ax5043_off_xtal
                                   5152 ;	COMMON\easyax5043.c:918: AX5043_PWRMODE = AX5043_PWRSTATE_POWERDOWN;
      0028BE 90 40 02         [24] 5153 	mov	dptr,#_AX5043_PWRMODE
      0028C1 E4               [12] 5154 	clr	a
      0028C2 F0               [24] 5155 	movx	@dptr,a
      0028C3 22               [24] 5156 	ret
                                   5157 ;------------------------------------------------------------
                                   5158 ;Allocation info for local variables in function 'ax5043_off_xtal'
                                   5159 ;------------------------------------------------------------
                                   5160 ;	COMMON\easyax5043.c:921: __reentrantb void ax5043_off_xtal(void) __reentrant
                                   5161 ;	-----------------------------------------
                                   5162 ;	 function ax5043_off_xtal
                                   5163 ;	-----------------------------------------
      0028C4                       5164 _ax5043_off_xtal:
                                   5165 ;	COMMON\easyax5043.c:923: AX5043_IRQMASK0 = 0x00; // IRQ off
      0028C4 90 40 07         [24] 5166 	mov	dptr,#_AX5043_IRQMASK0
      0028C7 E4               [12] 5167 	clr	a
      0028C8 F0               [24] 5168 	movx	@dptr,a
                                   5169 ;	COMMON\easyax5043.c:924: AX5043_IRQMASK1 = 0x00;
      0028C9 90 40 06         [24] 5170 	mov	dptr,#_AX5043_IRQMASK1
      0028CC F0               [24] 5171 	movx	@dptr,a
                                   5172 ;	COMMON\easyax5043.c:925: AX5043_PWRMODE = AX5043_PWRSTATE_XTAL_ON;
      0028CD 90 40 02         [24] 5173 	mov	dptr,#_AX5043_PWRMODE
      0028D0 74 05            [12] 5174 	mov	a,#0x05
      0028D2 F0               [24] 5175 	movx	@dptr,a
                                   5176 ;	COMMON\easyax5043.c:926: AX5043_LPOSCCONFIG = 0x00; // LPOSC off
      0028D3 90 43 10         [24] 5177 	mov	dptr,#_AX5043_LPOSCCONFIG
      0028D6 E4               [12] 5178 	clr	a
      0028D7 F0               [24] 5179 	movx	@dptr,a
                                   5180 ;	COMMON\easyax5043.c:927: axradio_trxstate = trxstate_off;
                                   5181 ;	1-genFromRTrack replaced	mov	_axradio_trxstate,#0x00
      0028D8 F5 0C            [12] 5182 	mov	_axradio_trxstate,a
      0028DA 22               [24] 5183 	ret
                                   5184 ;------------------------------------------------------------
                                   5185 ;Allocation info for local variables in function 'axradio_wait_for_xtal'
                                   5186 ;------------------------------------------------------------
                                   5187 ;iesave                    Allocated to registers r7 
                                   5188 ;------------------------------------------------------------
                                   5189 ;	COMMON\easyax5043.c:930: void axradio_wait_for_xtal(void)
                                   5190 ;	-----------------------------------------
                                   5191 ;	 function axradio_wait_for_xtal
                                   5192 ;	-----------------------------------------
      0028DB                       5193 _axradio_wait_for_xtal:
                                   5194 ;	COMMON\easyax5043.c:932: uint8_t __autodata iesave = IE & 0x80;
      0028DB 74 80            [12] 5195 	mov	a,#0x80
      0028DD 55 A8            [12] 5196 	anl	a,_IE
      0028DF FF               [12] 5197 	mov	r7,a
                                   5198 ;	COMMON\easyax5043.c:933: EA = 0;
      0028E0 C2 AF            [12] 5199 	clr	_EA
                                   5200 ;	COMMON\easyax5043.c:934: axradio_trxstate = trxstate_wait_xtal;
      0028E2 75 0C 03         [24] 5201 	mov	_axradio_trxstate,#0x03
                                   5202 ;	COMMON\easyax5043.c:935: AX5043_IRQMASK1 |= 0x01; // enable xtal ready interrupt
      0028E5 90 40 06         [24] 5203 	mov	dptr,#_AX5043_IRQMASK1
      0028E8 E0               [24] 5204 	movx	a,@dptr
      0028E9 FE               [12] 5205 	mov	r6,a
      0028EA 74 01            [12] 5206 	mov	a,#0x01
      0028EC 4E               [12] 5207 	orl	a,r6
      0028ED F0               [24] 5208 	movx	@dptr,a
      0028EE                       5209 00104$:
                                   5210 ;	COMMON\easyax5043.c:937: EA = 0;
      0028EE C2 AF            [12] 5211 	clr	_EA
                                   5212 ;	COMMON\easyax5043.c:938: if (axradio_trxstate == trxstate_xtal_ready)
      0028F0 74 04            [12] 5213 	mov	a,#0x04
      0028F2 B5 0C 02         [24] 5214 	cjne	a,_axradio_trxstate,00114$
      0028F5 80 11            [24] 5215 	sjmp	00103$
      0028F7                       5216 00114$:
                                   5217 ;	COMMON\easyax5043.c:940: wtimer_idle(WTFLAG_CANSTANDBY);
      0028F7 75 82 02         [24] 5218 	mov	dpl,#0x02
      0028FA C0 07            [24] 5219 	push	ar7
      0028FC 12 6B CB         [24] 5220 	lcall	_wtimer_idle
                                   5221 ;	COMMON\easyax5043.c:941: EA = 1;
      0028FF D2 AF            [12] 5222 	setb	_EA
                                   5223 ;	COMMON\easyax5043.c:942: wtimer_runcallbacks();
      002901 12 6B 4A         [24] 5224 	lcall	_wtimer_runcallbacks
      002904 D0 07            [24] 5225 	pop	ar7
      002906 80 E6            [24] 5226 	sjmp	00104$
      002908                       5227 00103$:
                                   5228 ;	COMMON\easyax5043.c:944: IE |= iesave;
      002908 EF               [12] 5229 	mov	a,r7
      002909 42 A8            [12] 5230 	orl	_IE,a
      00290B 22               [24] 5231 	ret
                                   5232 ;------------------------------------------------------------
                                   5233 ;Allocation info for local variables in function 'axradio_setaddrregs'
                                   5234 ;------------------------------------------------------------
                                   5235 ;pn                        Allocated to registers r6 r7 
                                   5236 ;inv                       Allocated to registers r5 
                                   5237 ;------------------------------------------------------------
                                   5238 ;	COMMON\easyax5043.c:947: static void axradio_setaddrregs(void)
                                   5239 ;	-----------------------------------------
                                   5240 ;	 function axradio_setaddrregs
                                   5241 ;	-----------------------------------------
      00290C                       5242 _axradio_setaddrregs:
                                   5243 ;	COMMON\easyax5043.c:949: AX5043_PKTADDR0 = axradio_localaddr.addr[0];
      00290C 90 00 C0         [24] 5244 	mov	dptr,#_axradio_localaddr
      00290F E0               [24] 5245 	movx	a,@dptr
      002910 90 42 07         [24] 5246 	mov	dptr,#_AX5043_PKTADDR0
      002913 F0               [24] 5247 	movx	@dptr,a
                                   5248 ;	COMMON\easyax5043.c:950: AX5043_PKTADDR1 = axradio_localaddr.addr[1];
      002914 90 00 C1         [24] 5249 	mov	dptr,#(_axradio_localaddr + 0x0001)
      002917 E0               [24] 5250 	movx	a,@dptr
      002918 90 42 06         [24] 5251 	mov	dptr,#_AX5043_PKTADDR1
      00291B F0               [24] 5252 	movx	@dptr,a
                                   5253 ;	COMMON\easyax5043.c:951: AX5043_PKTADDR2 = axradio_localaddr.addr[2];
      00291C 90 00 C2         [24] 5254 	mov	dptr,#(_axradio_localaddr + 0x0002)
      00291F E0               [24] 5255 	movx	a,@dptr
      002920 90 42 05         [24] 5256 	mov	dptr,#_AX5043_PKTADDR2
      002923 F0               [24] 5257 	movx	@dptr,a
                                   5258 ;	COMMON\easyax5043.c:952: AX5043_PKTADDR3 = axradio_localaddr.addr[3];
      002924 90 00 C3         [24] 5259 	mov	dptr,#(_axradio_localaddr + 0x0003)
      002927 E0               [24] 5260 	movx	a,@dptr
      002928 90 42 04         [24] 5261 	mov	dptr,#_AX5043_PKTADDR3
      00292B F0               [24] 5262 	movx	@dptr,a
                                   5263 ;	COMMON\easyax5043.c:954: AX5043_PKTADDRMASK0 = axradio_localaddr.mask[0];
      00292C 90 00 C4         [24] 5264 	mov	dptr,#(_axradio_localaddr + 0x0004)
      00292F E0               [24] 5265 	movx	a,@dptr
      002930 90 42 0B         [24] 5266 	mov	dptr,#_AX5043_PKTADDRMASK0
      002933 F0               [24] 5267 	movx	@dptr,a
                                   5268 ;	COMMON\easyax5043.c:955: AX5043_PKTADDRMASK1 = axradio_localaddr.mask[1];
      002934 90 00 C5         [24] 5269 	mov	dptr,#(_axradio_localaddr + 0x0005)
      002937 E0               [24] 5270 	movx	a,@dptr
      002938 90 42 0A         [24] 5271 	mov	dptr,#_AX5043_PKTADDRMASK1
      00293B F0               [24] 5272 	movx	@dptr,a
                                   5273 ;	COMMON\easyax5043.c:956: AX5043_PKTADDRMASK2 = axradio_localaddr.mask[2];
      00293C 90 00 C6         [24] 5274 	mov	dptr,#(_axradio_localaddr + 0x0006)
      00293F E0               [24] 5275 	movx	a,@dptr
      002940 90 42 09         [24] 5276 	mov	dptr,#_AX5043_PKTADDRMASK2
      002943 F0               [24] 5277 	movx	@dptr,a
                                   5278 ;	COMMON\easyax5043.c:957: AX5043_PKTADDRMASK3 = axradio_localaddr.mask[3];
      002944 90 00 C7         [24] 5279 	mov	dptr,#(_axradio_localaddr + 0x0007)
      002947 E0               [24] 5280 	movx	a,@dptr
      002948 FF               [12] 5281 	mov	r7,a
      002949 90 42 08         [24] 5282 	mov	dptr,#_AX5043_PKTADDRMASK3
      00294C F0               [24] 5283 	movx	@dptr,a
                                   5284 ;	COMMON\easyax5043.c:959: if (axradio_phy_pn9 && axradio_framing_addrlen) {
      00294D 90 7F CA         [24] 5285 	mov	dptr,#_axradio_phy_pn9
      002950 E4               [12] 5286 	clr	a
      002951 93               [24] 5287 	movc	a,@a+dptr
      002952 70 01            [24] 5288 	jnz	00117$
      002954 22               [24] 5289 	ret
      002955                       5290 00117$:
      002955 90 7F EC         [24] 5291 	mov	dptr,#_axradio_framing_addrlen
      002958 E4               [12] 5292 	clr	a
      002959 93               [24] 5293 	movc	a,@a+dptr
      00295A 70 01            [24] 5294 	jnz	00118$
      00295C 22               [24] 5295 	ret
      00295D                       5296 00118$:
                                   5297 ;	COMMON\easyax5043.c:960: uint16_t __autodata pn = 0x1ff;
      00295D 7E FF            [12] 5298 	mov	r6,#0xff
      00295F 7F 01            [12] 5299 	mov	r7,#0x01
                                   5300 ;	COMMON\easyax5043.c:961: uint8_t __autodata inv = -(AX5043_ENCODING & 0x01);
      002961 90 40 11         [24] 5301 	mov	dptr,#_AX5043_ENCODING
      002964 E0               [24] 5302 	movx	a,@dptr
      002965 FD               [12] 5303 	mov	r5,a
      002966 53 05 01         [24] 5304 	anl	ar5,#0x01
      002969 C3               [12] 5305 	clr	c
      00296A E4               [12] 5306 	clr	a
      00296B 9D               [12] 5307 	subb	a,r5
      00296C FD               [12] 5308 	mov	r5,a
                                   5309 ;	COMMON\easyax5043.c:962: if (axradio_framing_destaddrpos != 0xff)
      00296D 90 7F ED         [24] 5310 	mov	dptr,#_axradio_framing_destaddrpos
      002970 E4               [12] 5311 	clr	a
      002971 93               [24] 5312 	movc	a,@a+dptr
      002972 FC               [12] 5313 	mov	r4,a
      002973 BC FF 02         [24] 5314 	cjne	r4,#0xff,00119$
      002976 80 25            [24] 5315 	sjmp	00102$
      002978                       5316 00119$:
                                   5317 ;	COMMON\easyax5043.c:963: pn = pn9_advance_bits(pn, axradio_framing_destaddrpos << 3);
      002978 E4               [12] 5318 	clr	a
      002979 03               [12] 5319 	rr	a
      00297A 54 F8            [12] 5320 	anl	a,#0xf8
      00297C CC               [12] 5321 	xch	a,r4
      00297D C4               [12] 5322 	swap	a
      00297E 03               [12] 5323 	rr	a
      00297F CC               [12] 5324 	xch	a,r4
      002980 6C               [12] 5325 	xrl	a,r4
      002981 CC               [12] 5326 	xch	a,r4
      002982 54 F8            [12] 5327 	anl	a,#0xf8
      002984 CC               [12] 5328 	xch	a,r4
      002985 6C               [12] 5329 	xrl	a,r4
      002986 FB               [12] 5330 	mov	r3,a
      002987 C0 05            [24] 5331 	push	ar5
      002989 C0 04            [24] 5332 	push	ar4
      00298B C0 03            [24] 5333 	push	ar3
      00298D 90 01 FF         [24] 5334 	mov	dptr,#0x01ff
      002990 12 7B 29         [24] 5335 	lcall	_pn9_advance_bits
      002993 AE 82            [24] 5336 	mov	r6,dpl
      002995 AF 83            [24] 5337 	mov	r7,dph
      002997 15 81            [12] 5338 	dec	sp
      002999 15 81            [12] 5339 	dec	sp
      00299B D0 05            [24] 5340 	pop	ar5
      00299D                       5341 00102$:
                                   5342 ;	COMMON\easyax5043.c:964: AX5043_PKTADDR0 ^= pn ^ inv;
      00299D 7C 00            [12] 5343 	mov	r4,#0x00
      00299F ED               [12] 5344 	mov	a,r5
      0029A0 6E               [12] 5345 	xrl	a,r6
      0029A1 FA               [12] 5346 	mov	r2,a
      0029A2 EC               [12] 5347 	mov	a,r4
      0029A3 6F               [12] 5348 	xrl	a,r7
      0029A4 FB               [12] 5349 	mov	r3,a
      0029A5 90 42 07         [24] 5350 	mov	dptr,#_AX5043_PKTADDR0
      0029A8 E0               [24] 5351 	movx	a,@dptr
      0029A9 79 00            [12] 5352 	mov	r1,#0x00
      0029AB 62 02            [12] 5353 	xrl	ar2,a
      0029AD E9               [12] 5354 	mov	a,r1
      0029AE 62 03            [12] 5355 	xrl	ar3,a
      0029B0 90 42 07         [24] 5356 	mov	dptr,#_AX5043_PKTADDR0
      0029B3 EA               [12] 5357 	mov	a,r2
      0029B4 F0               [24] 5358 	movx	@dptr,a
                                   5359 ;	COMMON\easyax5043.c:965: pn = pn9_advance_byte(pn);
      0029B5 8E 82            [24] 5360 	mov	dpl,r6
      0029B7 8F 83            [24] 5361 	mov	dph,r7
      0029B9 C0 05            [24] 5362 	push	ar5
      0029BB C0 04            [24] 5363 	push	ar4
      0029BD 12 7C 33         [24] 5364 	lcall	_pn9_advance_byte
      0029C0 AE 82            [24] 5365 	mov	r6,dpl
      0029C2 AF 83            [24] 5366 	mov	r7,dph
      0029C4 D0 04            [24] 5367 	pop	ar4
      0029C6 D0 05            [24] 5368 	pop	ar5
                                   5369 ;	COMMON\easyax5043.c:966: AX5043_PKTADDR1 ^= pn ^ inv;
      0029C8 ED               [12] 5370 	mov	a,r5
      0029C9 6E               [12] 5371 	xrl	a,r6
      0029CA FA               [12] 5372 	mov	r2,a
      0029CB EC               [12] 5373 	mov	a,r4
      0029CC 6F               [12] 5374 	xrl	a,r7
      0029CD FB               [12] 5375 	mov	r3,a
      0029CE 90 42 06         [24] 5376 	mov	dptr,#_AX5043_PKTADDR1
      0029D1 E0               [24] 5377 	movx	a,@dptr
      0029D2 79 00            [12] 5378 	mov	r1,#0x00
      0029D4 62 02            [12] 5379 	xrl	ar2,a
      0029D6 E9               [12] 5380 	mov	a,r1
      0029D7 62 03            [12] 5381 	xrl	ar3,a
      0029D9 90 42 06         [24] 5382 	mov	dptr,#_AX5043_PKTADDR1
      0029DC EA               [12] 5383 	mov	a,r2
      0029DD F0               [24] 5384 	movx	@dptr,a
                                   5385 ;	COMMON\easyax5043.c:967: pn = pn9_advance_byte(pn);
      0029DE 8E 82            [24] 5386 	mov	dpl,r6
      0029E0 8F 83            [24] 5387 	mov	dph,r7
      0029E2 C0 05            [24] 5388 	push	ar5
      0029E4 C0 04            [24] 5389 	push	ar4
      0029E6 12 7C 33         [24] 5390 	lcall	_pn9_advance_byte
      0029E9 AE 82            [24] 5391 	mov	r6,dpl
      0029EB AF 83            [24] 5392 	mov	r7,dph
      0029ED D0 04            [24] 5393 	pop	ar4
      0029EF D0 05            [24] 5394 	pop	ar5
                                   5395 ;	COMMON\easyax5043.c:968: AX5043_PKTADDR2 ^= pn ^ inv;
      0029F1 ED               [12] 5396 	mov	a,r5
      0029F2 6E               [12] 5397 	xrl	a,r6
      0029F3 FA               [12] 5398 	mov	r2,a
      0029F4 EC               [12] 5399 	mov	a,r4
      0029F5 6F               [12] 5400 	xrl	a,r7
      0029F6 FB               [12] 5401 	mov	r3,a
      0029F7 90 42 05         [24] 5402 	mov	dptr,#_AX5043_PKTADDR2
      0029FA E0               [24] 5403 	movx	a,@dptr
      0029FB 79 00            [12] 5404 	mov	r1,#0x00
      0029FD 62 02            [12] 5405 	xrl	ar2,a
      0029FF E9               [12] 5406 	mov	a,r1
      002A00 62 03            [12] 5407 	xrl	ar3,a
      002A02 90 42 05         [24] 5408 	mov	dptr,#_AX5043_PKTADDR2
      002A05 EA               [12] 5409 	mov	a,r2
      002A06 F0               [24] 5410 	movx	@dptr,a
                                   5411 ;	COMMON\easyax5043.c:969: pn = pn9_advance_byte(pn);
      002A07 8E 82            [24] 5412 	mov	dpl,r6
      002A09 8F 83            [24] 5413 	mov	dph,r7
      002A0B C0 05            [24] 5414 	push	ar5
      002A0D C0 04            [24] 5415 	push	ar4
      002A0F 12 7C 33         [24] 5416 	lcall	_pn9_advance_byte
      002A12 AE 82            [24] 5417 	mov	r6,dpl
      002A14 AF 83            [24] 5418 	mov	r7,dph
      002A16 D0 04            [24] 5419 	pop	ar4
      002A18 D0 05            [24] 5420 	pop	ar5
                                   5421 ;	COMMON\easyax5043.c:970: AX5043_PKTADDR3 ^= pn ^ inv;
      002A1A ED               [12] 5422 	mov	a,r5
      002A1B 62 06            [12] 5423 	xrl	ar6,a
      002A1D EC               [12] 5424 	mov	a,r4
      002A1E 62 07            [12] 5425 	xrl	ar7,a
      002A20 90 42 04         [24] 5426 	mov	dptr,#_AX5043_PKTADDR3
      002A23 E0               [24] 5427 	movx	a,@dptr
      002A24 7C 00            [12] 5428 	mov	r4,#0x00
      002A26 62 06            [12] 5429 	xrl	ar6,a
      002A28 EC               [12] 5430 	mov	a,r4
      002A29 62 07            [12] 5431 	xrl	ar7,a
      002A2B 90 42 04         [24] 5432 	mov	dptr,#_AX5043_PKTADDR3
      002A2E EE               [12] 5433 	mov	a,r6
      002A2F F0               [24] 5434 	movx	@dptr,a
      002A30 22               [24] 5435 	ret
                                   5436 ;------------------------------------------------------------
                                   5437 ;Allocation info for local variables in function 'ax5043_init_registers'
                                   5438 ;------------------------------------------------------------
                                   5439 ;	COMMON\easyax5043.c:974: static void ax5043_init_registers(void)
                                   5440 ;	-----------------------------------------
                                   5441 ;	 function ax5043_init_registers
                                   5442 ;	-----------------------------------------
      002A31                       5443 _ax5043_init_registers:
                                   5444 ;	COMMON\easyax5043.c:976: ax5043_set_registers();
      002A31 12 12 68         [24] 5445 	lcall	_ax5043_set_registers
                                   5446 ;	COMMON\easyax5043.c:981: AX5043_PKTLENOFFSET += axradio_framing_swcrclen; // add len offs for software CRC16 (used for both, fixed and variable length packets
      002A34 90 7F F2         [24] 5447 	mov	dptr,#_axradio_framing_swcrclen
      002A37 E4               [12] 5448 	clr	a
      002A38 93               [24] 5449 	movc	a,@a+dptr
      002A39 FF               [12] 5450 	mov	r7,a
      002A3A 90 42 02         [24] 5451 	mov	dptr,#_AX5043_PKTLENOFFSET
      002A3D E0               [24] 5452 	movx	a,@dptr
      002A3E FE               [12] 5453 	mov	r6,a
      002A3F 2F               [12] 5454 	add	a,r7
      002A40 F0               [24] 5455 	movx	@dptr,a
                                   5456 ;	COMMON\easyax5043.c:982: AX5043_PINFUNCIRQ = 0x03; // use as IRQ pin
      002A41 90 40 24         [24] 5457 	mov	dptr,#_AX5043_PINFUNCIRQ
      002A44 74 03            [12] 5458 	mov	a,#0x03
      002A46 F0               [24] 5459 	movx	@dptr,a
                                   5460 ;	COMMON\easyax5043.c:983: AX5043_PKTSTOREFLAGS = axradio_phy_innerfreqloop ? 0x13 : 0x15; // store RF offset, RSSI and delimiter timing
      002A47 90 7F C9         [24] 5461 	mov	dptr,#_axradio_phy_innerfreqloop
      002A4A E4               [12] 5462 	clr	a
      002A4B 93               [24] 5463 	movc	a,@a+dptr
      002A4C FF               [12] 5464 	mov	r7,a
      002A4D 60 04            [24] 5465 	jz	00103$
      002A4F 7F 13            [12] 5466 	mov	r7,#0x13
      002A51 80 02            [24] 5467 	sjmp	00104$
      002A53                       5468 00103$:
      002A53 7F 15            [12] 5469 	mov	r7,#0x15
      002A55                       5470 00104$:
      002A55 90 42 32         [24] 5471 	mov	dptr,#_AX5043_PKTSTOREFLAGS
      002A58 EF               [12] 5472 	mov	a,r7
      002A59 F0               [24] 5473 	movx	@dptr,a
                                   5474 ;	COMMON\easyax5043.c:984: axradio_setaddrregs();
      002A5A 02 29 0C         [24] 5475 	ljmp	_axradio_setaddrregs
                                   5476 ;------------------------------------------------------------
                                   5477 ;Allocation info for local variables in function 'axradio_sync_addtime'
                                   5478 ;------------------------------------------------------------
                                   5479 ;dt                        Allocated to registers r4 r5 r6 r7 
                                   5480 ;------------------------------------------------------------
                                   5481 ;	COMMON\easyax5043.c:991: static __reentrantb void axradio_sync_addtime(uint32_t dt) __reentrant
                                   5482 ;	-----------------------------------------
                                   5483 ;	 function axradio_sync_addtime
                                   5484 ;	-----------------------------------------
      002A5D                       5485 _axradio_sync_addtime:
      002A5D AC 82            [24] 5486 	mov	r4,dpl
      002A5F AD 83            [24] 5487 	mov	r5,dph
      002A61 AE F0            [24] 5488 	mov	r6,b
      002A63 FF               [12] 5489 	mov	r7,a
                                   5490 ;	COMMON\easyax5043.c:993: axradio_sync_time += dt;
      002A64 90 00 B2         [24] 5491 	mov	dptr,#_axradio_sync_time
      002A67 E0               [24] 5492 	movx	a,@dptr
      002A68 F8               [12] 5493 	mov	r0,a
      002A69 A3               [24] 5494 	inc	dptr
      002A6A E0               [24] 5495 	movx	a,@dptr
      002A6B F9               [12] 5496 	mov	r1,a
      002A6C A3               [24] 5497 	inc	dptr
      002A6D E0               [24] 5498 	movx	a,@dptr
      002A6E FA               [12] 5499 	mov	r2,a
      002A6F A3               [24] 5500 	inc	dptr
      002A70 E0               [24] 5501 	movx	a,@dptr
      002A71 FB               [12] 5502 	mov	r3,a
      002A72 90 00 B2         [24] 5503 	mov	dptr,#_axradio_sync_time
      002A75 EC               [12] 5504 	mov	a,r4
      002A76 28               [12] 5505 	add	a,r0
      002A77 F0               [24] 5506 	movx	@dptr,a
      002A78 ED               [12] 5507 	mov	a,r5
      002A79 39               [12] 5508 	addc	a,r1
      002A7A A3               [24] 5509 	inc	dptr
      002A7B F0               [24] 5510 	movx	@dptr,a
      002A7C EE               [12] 5511 	mov	a,r6
      002A7D 3A               [12] 5512 	addc	a,r2
      002A7E A3               [24] 5513 	inc	dptr
      002A7F F0               [24] 5514 	movx	@dptr,a
      002A80 EF               [12] 5515 	mov	a,r7
      002A81 3B               [12] 5516 	addc	a,r3
      002A82 A3               [24] 5517 	inc	dptr
      002A83 F0               [24] 5518 	movx	@dptr,a
      002A84 22               [24] 5519 	ret
                                   5520 ;------------------------------------------------------------
                                   5521 ;Allocation info for local variables in function 'axradio_sync_subtime'
                                   5522 ;------------------------------------------------------------
                                   5523 ;dt                        Allocated to registers r4 r5 r6 r7 
                                   5524 ;------------------------------------------------------------
                                   5525 ;	COMMON\easyax5043.c:996: static __reentrantb void axradio_sync_subtime(uint32_t dt) __reentrant
                                   5526 ;	-----------------------------------------
                                   5527 ;	 function axradio_sync_subtime
                                   5528 ;	-----------------------------------------
      002A85                       5529 _axradio_sync_subtime:
      002A85 AC 82            [24] 5530 	mov	r4,dpl
      002A87 AD 83            [24] 5531 	mov	r5,dph
      002A89 AE F0            [24] 5532 	mov	r6,b
      002A8B FF               [12] 5533 	mov	r7,a
                                   5534 ;	COMMON\easyax5043.c:998: axradio_sync_time -= dt;
      002A8C 90 00 B2         [24] 5535 	mov	dptr,#_axradio_sync_time
      002A8F E0               [24] 5536 	movx	a,@dptr
      002A90 F8               [12] 5537 	mov	r0,a
      002A91 A3               [24] 5538 	inc	dptr
      002A92 E0               [24] 5539 	movx	a,@dptr
      002A93 F9               [12] 5540 	mov	r1,a
      002A94 A3               [24] 5541 	inc	dptr
      002A95 E0               [24] 5542 	movx	a,@dptr
      002A96 FA               [12] 5543 	mov	r2,a
      002A97 A3               [24] 5544 	inc	dptr
      002A98 E0               [24] 5545 	movx	a,@dptr
      002A99 FB               [12] 5546 	mov	r3,a
      002A9A 90 00 B2         [24] 5547 	mov	dptr,#_axradio_sync_time
      002A9D E8               [12] 5548 	mov	a,r0
      002A9E C3               [12] 5549 	clr	c
      002A9F 9C               [12] 5550 	subb	a,r4
      002AA0 F0               [24] 5551 	movx	@dptr,a
      002AA1 E9               [12] 5552 	mov	a,r1
      002AA2 9D               [12] 5553 	subb	a,r5
      002AA3 A3               [24] 5554 	inc	dptr
      002AA4 F0               [24] 5555 	movx	@dptr,a
      002AA5 EA               [12] 5556 	mov	a,r2
      002AA6 9E               [12] 5557 	subb	a,r6
      002AA7 A3               [24] 5558 	inc	dptr
      002AA8 F0               [24] 5559 	movx	@dptr,a
      002AA9 EB               [12] 5560 	mov	a,r3
      002AAA 9F               [12] 5561 	subb	a,r7
      002AAB A3               [24] 5562 	inc	dptr
      002AAC F0               [24] 5563 	movx	@dptr,a
      002AAD 22               [24] 5564 	ret
                                   5565 ;------------------------------------------------------------
                                   5566 ;Allocation info for local variables in function 'axradio_sync_settimeradv'
                                   5567 ;------------------------------------------------------------
                                   5568 ;dt                        Allocated to registers r4 r5 r6 r7 
                                   5569 ;------------------------------------------------------------
                                   5570 ;	COMMON\easyax5043.c:1001: static __reentrantb void axradio_sync_settimeradv(uint32_t dt) __reentrant
                                   5571 ;	-----------------------------------------
                                   5572 ;	 function axradio_sync_settimeradv
                                   5573 ;	-----------------------------------------
      002AAE                       5574 _axradio_sync_settimeradv:
      002AAE AC 82            [24] 5575 	mov	r4,dpl
      002AB0 AD 83            [24] 5576 	mov	r5,dph
      002AB2 AE F0            [24] 5577 	mov	r6,b
      002AB4 FF               [12] 5578 	mov	r7,a
                                   5579 ;	COMMON\easyax5043.c:1003: axradio_timer.time = axradio_sync_time;
      002AB5 90 00 B2         [24] 5580 	mov	dptr,#_axradio_sync_time
      002AB8 E0               [24] 5581 	movx	a,@dptr
      002AB9 F8               [12] 5582 	mov	r0,a
      002ABA A3               [24] 5583 	inc	dptr
      002ABB E0               [24] 5584 	movx	a,@dptr
      002ABC F9               [12] 5585 	mov	r1,a
      002ABD A3               [24] 5586 	inc	dptr
      002ABE E0               [24] 5587 	movx	a,@dptr
      002ABF FA               [12] 5588 	mov	r2,a
      002AC0 A3               [24] 5589 	inc	dptr
      002AC1 E0               [24] 5590 	movx	a,@dptr
      002AC2 FB               [12] 5591 	mov	r3,a
      002AC3 90 03 2F         [24] 5592 	mov	dptr,#(_axradio_timer + 0x0004)
      002AC6 E8               [12] 5593 	mov	a,r0
      002AC7 F0               [24] 5594 	movx	@dptr,a
      002AC8 E9               [12] 5595 	mov	a,r1
      002AC9 A3               [24] 5596 	inc	dptr
      002ACA F0               [24] 5597 	movx	@dptr,a
      002ACB EA               [12] 5598 	mov	a,r2
      002ACC A3               [24] 5599 	inc	dptr
      002ACD F0               [24] 5600 	movx	@dptr,a
      002ACE EB               [12] 5601 	mov	a,r3
      002ACF A3               [24] 5602 	inc	dptr
      002AD0 F0               [24] 5603 	movx	@dptr,a
                                   5604 ;	COMMON\easyax5043.c:1004: axradio_timer.time -= dt;
      002AD1 E8               [12] 5605 	mov	a,r0
      002AD2 C3               [12] 5606 	clr	c
      002AD3 9C               [12] 5607 	subb	a,r4
      002AD4 FC               [12] 5608 	mov	r4,a
      002AD5 E9               [12] 5609 	mov	a,r1
      002AD6 9D               [12] 5610 	subb	a,r5
      002AD7 FD               [12] 5611 	mov	r5,a
      002AD8 EA               [12] 5612 	mov	a,r2
      002AD9 9E               [12] 5613 	subb	a,r6
      002ADA FE               [12] 5614 	mov	r6,a
      002ADB EB               [12] 5615 	mov	a,r3
      002ADC 9F               [12] 5616 	subb	a,r7
      002ADD FF               [12] 5617 	mov	r7,a
      002ADE 90 03 2F         [24] 5618 	mov	dptr,#(_axradio_timer + 0x0004)
      002AE1 EC               [12] 5619 	mov	a,r4
      002AE2 F0               [24] 5620 	movx	@dptr,a
      002AE3 ED               [12] 5621 	mov	a,r5
      002AE4 A3               [24] 5622 	inc	dptr
      002AE5 F0               [24] 5623 	movx	@dptr,a
      002AE6 EE               [12] 5624 	mov	a,r6
      002AE7 A3               [24] 5625 	inc	dptr
      002AE8 F0               [24] 5626 	movx	@dptr,a
      002AE9 EF               [12] 5627 	mov	a,r7
      002AEA A3               [24] 5628 	inc	dptr
      002AEB F0               [24] 5629 	movx	@dptr,a
      002AEC 22               [24] 5630 	ret
                                   5631 ;------------------------------------------------------------
                                   5632 ;Allocation info for local variables in function 'axradio_sync_adjustperiodcorr'
                                   5633 ;------------------------------------------------------------
                                   5634 ;dt                        Allocated to registers r4 r5 r6 r7 
                                   5635 ;------------------------------------------------------------
                                   5636 ;	COMMON\easyax5043.c:1007: static void axradio_sync_adjustperiodcorr(void)
                                   5637 ;	-----------------------------------------
                                   5638 ;	 function axradio_sync_adjustperiodcorr
                                   5639 ;	-----------------------------------------
      002AED                       5640 _axradio_sync_adjustperiodcorr:
                                   5641 ;	COMMON\easyax5043.c:1009: int32_t __autodata dt = axradio_conv_time_totimer0(axradio_cb_receive.st.time.t) - axradio_sync_time;
      002AED 90 02 DA         [24] 5642 	mov	dptr,#(_axradio_cb_receive + 0x0006)
      002AF0 E0               [24] 5643 	movx	a,@dptr
      002AF1 FC               [12] 5644 	mov	r4,a
      002AF2 A3               [24] 5645 	inc	dptr
      002AF3 E0               [24] 5646 	movx	a,@dptr
      002AF4 FD               [12] 5647 	mov	r5,a
      002AF5 A3               [24] 5648 	inc	dptr
      002AF6 E0               [24] 5649 	movx	a,@dptr
      002AF7 FE               [12] 5650 	mov	r6,a
      002AF8 A3               [24] 5651 	inc	dptr
      002AF9 E0               [24] 5652 	movx	a,@dptr
      002AFA 8C 82            [24] 5653 	mov	dpl,r4
      002AFC 8D 83            [24] 5654 	mov	dph,r5
      002AFE 8E F0            [24] 5655 	mov	b,r6
      002B00 12 1B 8A         [24] 5656 	lcall	_axradio_conv_time_totimer0
      002B03 AC 82            [24] 5657 	mov	r4,dpl
      002B05 AD 83            [24] 5658 	mov	r5,dph
      002B07 AE F0            [24] 5659 	mov	r6,b
      002B09 FF               [12] 5660 	mov	r7,a
      002B0A 90 00 B2         [24] 5661 	mov	dptr,#_axradio_sync_time
      002B0D E0               [24] 5662 	movx	a,@dptr
      002B0E F8               [12] 5663 	mov	r0,a
      002B0F A3               [24] 5664 	inc	dptr
      002B10 E0               [24] 5665 	movx	a,@dptr
      002B11 F9               [12] 5666 	mov	r1,a
      002B12 A3               [24] 5667 	inc	dptr
      002B13 E0               [24] 5668 	movx	a,@dptr
      002B14 FA               [12] 5669 	mov	r2,a
      002B15 A3               [24] 5670 	inc	dptr
      002B16 E0               [24] 5671 	movx	a,@dptr
      002B17 FB               [12] 5672 	mov	r3,a
      002B18 EC               [12] 5673 	mov	a,r4
      002B19 C3               [12] 5674 	clr	c
      002B1A 98               [12] 5675 	subb	a,r0
      002B1B FC               [12] 5676 	mov	r4,a
      002B1C ED               [12] 5677 	mov	a,r5
      002B1D 99               [12] 5678 	subb	a,r1
      002B1E FD               [12] 5679 	mov	r5,a
      002B1F EE               [12] 5680 	mov	a,r6
      002B20 9A               [12] 5681 	subb	a,r2
      002B21 FE               [12] 5682 	mov	r6,a
      002B22 EF               [12] 5683 	mov	a,r7
      002B23 9B               [12] 5684 	subb	a,r3
      002B24 FF               [12] 5685 	mov	r7,a
                                   5686 ;	COMMON\easyax5043.c:1010: axradio_cb_receive.st.rx.phy.timeoffset = dt;
      002B25 8C 02            [24] 5687 	mov	ar2,r4
      002B27 8D 03            [24] 5688 	mov	ar3,r5
      002B29 90 02 E4         [24] 5689 	mov	dptr,#(_axradio_cb_receive + 0x0010)
      002B2C EA               [12] 5690 	mov	a,r2
      002B2D F0               [24] 5691 	movx	@dptr,a
      002B2E EB               [12] 5692 	mov	a,r3
      002B2F A3               [24] 5693 	inc	dptr
      002B30 F0               [24] 5694 	movx	@dptr,a
                                   5695 ;	COMMON\easyax5043.c:1011: if (!checksignedlimit16(axradio_sync_periodcorr, axradio_sync_slave_maxperiod)) {
      002B31 90 00 B6         [24] 5696 	mov	dptr,#_axradio_sync_periodcorr
      002B34 E0               [24] 5697 	movx	a,@dptr
      002B35 FA               [12] 5698 	mov	r2,a
      002B36 A3               [24] 5699 	inc	dptr
      002B37 E0               [24] 5700 	movx	a,@dptr
      002B38 FB               [12] 5701 	mov	r3,a
      002B39 90 80 1B         [24] 5702 	mov	dptr,#_axradio_sync_slave_maxperiod
      002B3C E4               [12] 5703 	clr	a
      002B3D 93               [24] 5704 	movc	a,@a+dptr
      002B3E C0 E0            [24] 5705 	push	acc
      002B40 74 01            [12] 5706 	mov	a,#0x01
      002B42 93               [24] 5707 	movc	a,@a+dptr
      002B43 C0 E0            [24] 5708 	push	acc
      002B45 8A 82            [24] 5709 	mov	dpl,r2
      002B47 8B 83            [24] 5710 	mov	dph,r3
      002B49 12 73 E3         [24] 5711 	lcall	_checksignedlimit16
      002B4C AB 82            [24] 5712 	mov	r3,dpl
      002B4E 15 81            [12] 5713 	dec	sp
      002B50 15 81            [12] 5714 	dec	sp
      002B52 EB               [12] 5715 	mov	a,r3
      002B53 70 4B            [24] 5716 	jnz	00102$
                                   5717 ;	COMMON\easyax5043.c:1012: axradio_sync_addtime(dt);
      002B55 8C 82            [24] 5718 	mov	dpl,r4
      002B57 8D 83            [24] 5719 	mov	dph,r5
      002B59 8E F0            [24] 5720 	mov	b,r6
      002B5B EF               [12] 5721 	mov	a,r7
      002B5C C0 07            [24] 5722 	push	ar7
      002B5E C0 06            [24] 5723 	push	ar6
      002B60 C0 05            [24] 5724 	push	ar5
      002B62 C0 04            [24] 5725 	push	ar4
      002B64 12 2A 5D         [24] 5726 	lcall	_axradio_sync_addtime
      002B67 D0 04            [24] 5727 	pop	ar4
      002B69 D0 05            [24] 5728 	pop	ar5
      002B6B D0 06            [24] 5729 	pop	ar6
      002B6D D0 07            [24] 5730 	pop	ar7
                                   5731 ;	COMMON\easyax5043.c:1013: dt <<= SYNC_K1;
      002B6F EF               [12] 5732 	mov	a,r7
      002B70 C4               [12] 5733 	swap	a
      002B71 23               [12] 5734 	rl	a
      002B72 54 E0            [12] 5735 	anl	a,#0xe0
      002B74 CE               [12] 5736 	xch	a,r6
      002B75 C4               [12] 5737 	swap	a
      002B76 23               [12] 5738 	rl	a
      002B77 CE               [12] 5739 	xch	a,r6
      002B78 6E               [12] 5740 	xrl	a,r6
      002B79 CE               [12] 5741 	xch	a,r6
      002B7A 54 E0            [12] 5742 	anl	a,#0xe0
      002B7C CE               [12] 5743 	xch	a,r6
      002B7D 6E               [12] 5744 	xrl	a,r6
      002B7E FF               [12] 5745 	mov	r7,a
      002B7F ED               [12] 5746 	mov	a,r5
      002B80 C4               [12] 5747 	swap	a
      002B81 23               [12] 5748 	rl	a
      002B82 54 1F            [12] 5749 	anl	a,#0x1f
      002B84 4E               [12] 5750 	orl	a,r6
      002B85 FE               [12] 5751 	mov	r6,a
      002B86 ED               [12] 5752 	mov	a,r5
      002B87 C4               [12] 5753 	swap	a
      002B88 23               [12] 5754 	rl	a
      002B89 54 E0            [12] 5755 	anl	a,#0xe0
      002B8B CC               [12] 5756 	xch	a,r4
      002B8C C4               [12] 5757 	swap	a
      002B8D 23               [12] 5758 	rl	a
      002B8E CC               [12] 5759 	xch	a,r4
      002B8F 6C               [12] 5760 	xrl	a,r4
      002B90 CC               [12] 5761 	xch	a,r4
      002B91 54 E0            [12] 5762 	anl	a,#0xe0
      002B93 CC               [12] 5763 	xch	a,r4
      002B94 6C               [12] 5764 	xrl	a,r4
      002B95 FD               [12] 5765 	mov	r5,a
                                   5766 ;	COMMON\easyax5043.c:1014: axradio_sync_periodcorr = dt;
      002B96 90 00 B6         [24] 5767 	mov	dptr,#_axradio_sync_periodcorr
      002B99 EC               [12] 5768 	mov	a,r4
      002B9A F0               [24] 5769 	movx	@dptr,a
      002B9B ED               [12] 5770 	mov	a,r5
      002B9C A3               [24] 5771 	inc	dptr
      002B9D F0               [24] 5772 	movx	@dptr,a
      002B9E 80 48            [24] 5773 	sjmp	00103$
      002BA0                       5774 00102$:
                                   5775 ;	COMMON\easyax5043.c:1016: axradio_sync_periodcorr += dt;
      002BA0 90 00 B6         [24] 5776 	mov	dptr,#_axradio_sync_periodcorr
      002BA3 E0               [24] 5777 	movx	a,@dptr
      002BA4 FA               [12] 5778 	mov	r2,a
      002BA5 A3               [24] 5779 	inc	dptr
      002BA6 E0               [24] 5780 	movx	a,@dptr
      002BA7 FB               [12] 5781 	mov	r3,a
      002BA8 8A 00            [24] 5782 	mov	ar0,r2
      002BAA EB               [12] 5783 	mov	a,r3
      002BAB F9               [12] 5784 	mov	r1,a
      002BAC 33               [12] 5785 	rlc	a
      002BAD 95 E0            [12] 5786 	subb	a,acc
      002BAF FA               [12] 5787 	mov	r2,a
      002BB0 FB               [12] 5788 	mov	r3,a
      002BB1 EC               [12] 5789 	mov	a,r4
      002BB2 28               [12] 5790 	add	a,r0
      002BB3 F8               [12] 5791 	mov	r0,a
      002BB4 ED               [12] 5792 	mov	a,r5
      002BB5 39               [12] 5793 	addc	a,r1
      002BB6 F9               [12] 5794 	mov	r1,a
      002BB7 EE               [12] 5795 	mov	a,r6
      002BB8 3A               [12] 5796 	addc	a,r2
      002BB9 EF               [12] 5797 	mov	a,r7
      002BBA 3B               [12] 5798 	addc	a,r3
      002BBB 90 00 B6         [24] 5799 	mov	dptr,#_axradio_sync_periodcorr
      002BBE E8               [12] 5800 	mov	a,r0
      002BBF F0               [24] 5801 	movx	@dptr,a
      002BC0 E9               [12] 5802 	mov	a,r1
      002BC1 A3               [24] 5803 	inc	dptr
      002BC2 F0               [24] 5804 	movx	@dptr,a
                                   5805 ;	COMMON\easyax5043.c:1017: dt >>= SYNC_K0;
      002BC3 EF               [12] 5806 	mov	a,r7
      002BC4 A2 E7            [12] 5807 	mov	c,acc.7
      002BC6 13               [12] 5808 	rrc	a
      002BC7 FF               [12] 5809 	mov	r7,a
      002BC8 EE               [12] 5810 	mov	a,r6
      002BC9 13               [12] 5811 	rrc	a
      002BCA FE               [12] 5812 	mov	r6,a
      002BCB ED               [12] 5813 	mov	a,r5
      002BCC 13               [12] 5814 	rrc	a
      002BCD FD               [12] 5815 	mov	r5,a
      002BCE EC               [12] 5816 	mov	a,r4
      002BCF 13               [12] 5817 	rrc	a
      002BD0 FC               [12] 5818 	mov	r4,a
      002BD1 EF               [12] 5819 	mov	a,r7
      002BD2 A2 E7            [12] 5820 	mov	c,acc.7
      002BD4 13               [12] 5821 	rrc	a
      002BD5 FF               [12] 5822 	mov	r7,a
      002BD6 EE               [12] 5823 	mov	a,r6
      002BD7 13               [12] 5824 	rrc	a
      002BD8 FE               [12] 5825 	mov	r6,a
      002BD9 ED               [12] 5826 	mov	a,r5
      002BDA 13               [12] 5827 	rrc	a
      002BDB FD               [12] 5828 	mov	r5,a
      002BDC EC               [12] 5829 	mov	a,r4
      002BDD 13               [12] 5830 	rrc	a
                                   5831 ;	COMMON\easyax5043.c:1018: axradio_sync_addtime(dt);
      002BDE F5 82            [12] 5832 	mov	dpl,a
      002BE0 8D 83            [24] 5833 	mov	dph,r5
      002BE2 8E F0            [24] 5834 	mov	b,r6
      002BE4 EF               [12] 5835 	mov	a,r7
      002BE5 12 2A 5D         [24] 5836 	lcall	_axradio_sync_addtime
      002BE8                       5837 00103$:
                                   5838 ;	COMMON\easyax5043.c:1020: axradio_sync_periodcorr = signedlimit16(axradio_sync_periodcorr, axradio_sync_slave_maxperiod);
      002BE8 90 00 B6         [24] 5839 	mov	dptr,#_axradio_sync_periodcorr
      002BEB E0               [24] 5840 	movx	a,@dptr
      002BEC FE               [12] 5841 	mov	r6,a
      002BED A3               [24] 5842 	inc	dptr
      002BEE E0               [24] 5843 	movx	a,@dptr
      002BEF FF               [12] 5844 	mov	r7,a
      002BF0 90 80 1B         [24] 5845 	mov	dptr,#_axradio_sync_slave_maxperiod
      002BF3 E4               [12] 5846 	clr	a
      002BF4 93               [24] 5847 	movc	a,@a+dptr
      002BF5 C0 E0            [24] 5848 	push	acc
      002BF7 74 01            [12] 5849 	mov	a,#0x01
      002BF9 93               [24] 5850 	movc	a,@a+dptr
      002BFA C0 E0            [24] 5851 	push	acc
      002BFC 8E 82            [24] 5852 	mov	dpl,r6
      002BFE 8F 83            [24] 5853 	mov	dph,r7
      002C00 12 75 B6         [24] 5854 	lcall	_signedlimit16
      002C03 AE 82            [24] 5855 	mov	r6,dpl
      002C05 AF 83            [24] 5856 	mov	r7,dph
      002C07 15 81            [12] 5857 	dec	sp
      002C09 15 81            [12] 5858 	dec	sp
      002C0B 90 00 B6         [24] 5859 	mov	dptr,#_axradio_sync_periodcorr
      002C0E EE               [12] 5860 	mov	a,r6
      002C0F F0               [24] 5861 	movx	@dptr,a
      002C10 EF               [12] 5862 	mov	a,r7
      002C11 A3               [24] 5863 	inc	dptr
      002C12 F0               [24] 5864 	movx	@dptr,a
      002C13 22               [24] 5865 	ret
                                   5866 ;------------------------------------------------------------
                                   5867 ;Allocation info for local variables in function 'axradio_sync_slave_nextperiod'
                                   5868 ;------------------------------------------------------------
                                   5869 ;c                         Allocated to registers r6 r7 
                                   5870 ;------------------------------------------------------------
                                   5871 ;	COMMON\easyax5043.c:1023: static void axradio_sync_slave_nextperiod()
                                   5872 ;	-----------------------------------------
                                   5873 ;	 function axradio_sync_slave_nextperiod
                                   5874 ;	-----------------------------------------
      002C14                       5875 _axradio_sync_slave_nextperiod:
                                   5876 ;	COMMON\easyax5043.c:1025: axradio_sync_addtime(axradio_sync_period);
      002C14 90 80 07         [24] 5877 	mov	dptr,#_axradio_sync_period
      002C17 E4               [12] 5878 	clr	a
      002C18 93               [24] 5879 	movc	a,@a+dptr
      002C19 FC               [12] 5880 	mov	r4,a
      002C1A 74 01            [12] 5881 	mov	a,#0x01
      002C1C 93               [24] 5882 	movc	a,@a+dptr
      002C1D FD               [12] 5883 	mov	r5,a
      002C1E 74 02            [12] 5884 	mov	a,#0x02
      002C20 93               [24] 5885 	movc	a,@a+dptr
      002C21 FE               [12] 5886 	mov	r6,a
      002C22 74 03            [12] 5887 	mov	a,#0x03
      002C24 93               [24] 5888 	movc	a,@a+dptr
      002C25 8C 82            [24] 5889 	mov	dpl,r4
      002C27 8D 83            [24] 5890 	mov	dph,r5
      002C29 8E F0            [24] 5891 	mov	b,r6
      002C2B 12 2A 5D         [24] 5892 	lcall	_axradio_sync_addtime
                                   5893 ;	COMMON\easyax5043.c:1026: if (!checksignedlimit16(axradio_sync_periodcorr, axradio_sync_slave_maxperiod))
      002C2E 90 00 B6         [24] 5894 	mov	dptr,#_axradio_sync_periodcorr
      002C31 E0               [24] 5895 	movx	a,@dptr
      002C32 FE               [12] 5896 	mov	r6,a
      002C33 A3               [24] 5897 	inc	dptr
      002C34 E0               [24] 5898 	movx	a,@dptr
      002C35 FF               [12] 5899 	mov	r7,a
      002C36 90 80 1B         [24] 5900 	mov	dptr,#_axradio_sync_slave_maxperiod
      002C39 E4               [12] 5901 	clr	a
      002C3A 93               [24] 5902 	movc	a,@a+dptr
      002C3B C0 E0            [24] 5903 	push	acc
      002C3D 74 01            [12] 5904 	mov	a,#0x01
      002C3F 93               [24] 5905 	movc	a,@a+dptr
      002C40 C0 E0            [24] 5906 	push	acc
      002C42 8E 82            [24] 5907 	mov	dpl,r6
      002C44 8F 83            [24] 5908 	mov	dph,r7
      002C46 12 73 E3         [24] 5909 	lcall	_checksignedlimit16
      002C49 AF 82            [24] 5910 	mov	r7,dpl
      002C4B 15 81            [12] 5911 	dec	sp
      002C4D 15 81            [12] 5912 	dec	sp
      002C4F EF               [12] 5913 	mov	a,r7
      002C50 70 01            [24] 5914 	jnz	00102$
                                   5915 ;	COMMON\easyax5043.c:1027: return;
      002C52 22               [24] 5916 	ret
      002C53                       5917 00102$:
                                   5918 ;	COMMON\easyax5043.c:1029: int16_t __autodata c = axradio_sync_periodcorr;
      002C53 90 00 B6         [24] 5919 	mov	dptr,#_axradio_sync_periodcorr
      002C56 E0               [24] 5920 	movx	a,@dptr
      002C57 FE               [12] 5921 	mov	r6,a
      002C58 A3               [24] 5922 	inc	dptr
      002C59 E0               [24] 5923 	movx	a,@dptr
                                   5924 ;	COMMON\easyax5043.c:1030: axradio_sync_addtime(c >> SYNC_K1);
      002C5A FF               [12] 5925 	mov	r7,a
      002C5B C4               [12] 5926 	swap	a
      002C5C 03               [12] 5927 	rr	a
      002C5D CE               [12] 5928 	xch	a,r6
      002C5E C4               [12] 5929 	swap	a
      002C5F 03               [12] 5930 	rr	a
      002C60 54 07            [12] 5931 	anl	a,#0x07
      002C62 6E               [12] 5932 	xrl	a,r6
      002C63 CE               [12] 5933 	xch	a,r6
      002C64 54 07            [12] 5934 	anl	a,#0x07
      002C66 CE               [12] 5935 	xch	a,r6
      002C67 6E               [12] 5936 	xrl	a,r6
      002C68 CE               [12] 5937 	xch	a,r6
      002C69 30 E2 02         [24] 5938 	jnb	acc.2,00109$
      002C6C 44 F8            [12] 5939 	orl	a,#0xf8
      002C6E                       5940 00109$:
      002C6E FF               [12] 5941 	mov	r7,a
      002C6F 33               [12] 5942 	rlc	a
      002C70 95 E0            [12] 5943 	subb	a,acc
      002C72 FD               [12] 5944 	mov	r5,a
      002C73 8E 82            [24] 5945 	mov	dpl,r6
      002C75 8F 83            [24] 5946 	mov	dph,r7
      002C77 8D F0            [24] 5947 	mov	b,r5
      002C79 02 2A 5D         [24] 5948 	ljmp	_axradio_sync_addtime
                                   5949 ;------------------------------------------------------------
                                   5950 ;Allocation info for local variables in function 'axradio_timer_callback'
                                   5951 ;------------------------------------------------------------
                                   5952 ;r                         Allocated to registers r7 
                                   5953 ;idx                       Allocated to registers r7 
                                   5954 ;rs                        Allocated to registers r6 
                                   5955 ;idx                       Allocated to registers r7 
                                   5956 ;desc                      Allocated with name '_axradio_timer_callback_desc_1_326'
                                   5957 ;------------------------------------------------------------
                                   5958 ;	COMMON\easyax5043.c:1036: static void axradio_timer_callback(struct wtimer_desc __xdata *desc)
                                   5959 ;	-----------------------------------------
                                   5960 ;	 function axradio_timer_callback
                                   5961 ;	-----------------------------------------
      002C7C                       5962 _axradio_timer_callback:
                                   5963 ;	COMMON\easyax5043.c:1039: switch (axradio_mode) {
      002C7C AF 0B            [24] 5964 	mov	r7,_axradio_mode
      002C7E BF 10 00         [24] 5965 	cjne	r7,#0x10,00266$
      002C81                       5966 00266$:
      002C81 50 01            [24] 5967 	jnc	00267$
      002C83 22               [24] 5968 	ret
      002C84                       5969 00267$:
      002C84 EF               [12] 5970 	mov	a,r7
      002C85 24 CC            [12] 5971 	add	a,#0xff - 0x33
      002C87 50 01            [24] 5972 	jnc	00268$
      002C89 22               [24] 5973 	ret
      002C8A                       5974 00268$:
      002C8A EF               [12] 5975 	mov	a,r7
      002C8B 24 F0            [12] 5976 	add	a,#0xf0
      002C8D FF               [12] 5977 	mov	r7,a
      002C8E 24 0A            [12] 5978 	add	a,#(00269$-3-.)
      002C90 83               [24] 5979 	movc	a,@a+pc
      002C91 F5 82            [12] 5980 	mov	dpl,a
      002C93 EF               [12] 5981 	mov	a,r7
      002C94 24 28            [12] 5982 	add	a,#(00270$-3-.)
      002C96 83               [24] 5983 	movc	a,@a+pc
      002C97 F5 83            [12] 5984 	mov	dph,a
      002C99 E4               [12] 5985 	clr	a
      002C9A 73               [24] 5986 	jmp	@a+dptr
      002C9B                       5987 00269$:
      002C9B 83                    5988 	.db	00112$
      002C9C 83                    5989 	.db	00113$
      002C9D 19                    5990 	.db	00123$
      002C9E 19                    5991 	.db	00124$
      002C9F B4                    5992 	.db	00175$
      002CA0 B4                    5993 	.db	00175$
      002CA1 B4                    5994 	.db	00175$
      002CA2 B4                    5995 	.db	00175$
      002CA3 B4                    5996 	.db	00175$
      002CA4 B4                    5997 	.db	00175$
      002CA5 B4                    5998 	.db	00175$
      002CA6 B4                    5999 	.db	00175$
      002CA7 B4                    6000 	.db	00175$
      002CA8 B4                    6001 	.db	00175$
      002CA9 B4                    6002 	.db	00175$
      002CAA B4                    6003 	.db	00175$
      002CAB E3                    6004 	.db	00106$
      002CAC E3                    6005 	.db	00107$
      002CAD 7B                    6006 	.db	00129$
      002CAE 7B                    6007 	.db	00130$
      002CAF B4                    6008 	.db	00175$
      002CB0 B4                    6009 	.db	00175$
      002CB1 B4                    6010 	.db	00175$
      002CB2 B4                    6011 	.db	00175$
      002CB3 E3                    6012 	.db	00102$
      002CB4 E3                    6013 	.db	00103$
      002CB5 E3                    6014 	.db	00104$
      002CB6 E3                    6015 	.db	00105$
      002CB7 E3                    6016 	.db	00101$
      002CB8 B4                    6017 	.db	00175$
      002CB9 B4                    6018 	.db	00175$
      002CBA B4                    6019 	.db	00175$
      002CBB 10                    6020 	.db	00139$
      002CBC 10                    6021 	.db	00140$
      002CBD AC                    6022 	.db	00152$
      002CBE AC                    6023 	.db	00153$
      002CBF                       6024 00270$:
      002CBF 2D                    6025 	.db	00112$>>8
      002CC0 2D                    6026 	.db	00113$>>8
      002CC1 2E                    6027 	.db	00123$>>8
      002CC2 2E                    6028 	.db	00124$>>8
      002CC3 34                    6029 	.db	00175$>>8
      002CC4 34                    6030 	.db	00175$>>8
      002CC5 34                    6031 	.db	00175$>>8
      002CC6 34                    6032 	.db	00175$>>8
      002CC7 34                    6033 	.db	00175$>>8
      002CC8 34                    6034 	.db	00175$>>8
      002CC9 34                    6035 	.db	00175$>>8
      002CCA 34                    6036 	.db	00175$>>8
      002CCB 34                    6037 	.db	00175$>>8
      002CCC 34                    6038 	.db	00175$>>8
      002CCD 34                    6039 	.db	00175$>>8
      002CCE 34                    6040 	.db	00175$>>8
      002CCF 2C                    6041 	.db	00106$>>8
      002CD0 2C                    6042 	.db	00107$>>8
      002CD1 2E                    6043 	.db	00129$>>8
      002CD2 2E                    6044 	.db	00130$>>8
      002CD3 34                    6045 	.db	00175$>>8
      002CD4 34                    6046 	.db	00175$>>8
      002CD5 34                    6047 	.db	00175$>>8
      002CD6 34                    6048 	.db	00175$>>8
      002CD7 2C                    6049 	.db	00102$>>8
      002CD8 2C                    6050 	.db	00103$>>8
      002CD9 2C                    6051 	.db	00104$>>8
      002CDA 2C                    6052 	.db	00105$>>8
      002CDB 2C                    6053 	.db	00101$>>8
      002CDC 34                    6054 	.db	00175$>>8
      002CDD 34                    6055 	.db	00175$>>8
      002CDE 34                    6056 	.db	00175$>>8
      002CDF 2F                    6057 	.db	00139$>>8
      002CE0 2F                    6058 	.db	00140$>>8
      002CE1 30                    6059 	.db	00152$>>8
      002CE2 30                    6060 	.db	00153$>>8
                                   6061 ;	COMMON\easyax5043.c:1040: case AXRADIO_MODE_STREAM_RECEIVE:
      002CE3                       6062 00101$:
                                   6063 ;	COMMON\easyax5043.c:1041: case AXRADIO_MODE_STREAM_RECEIVE_UNENC:
      002CE3                       6064 00102$:
                                   6065 ;	COMMON\easyax5043.c:1042: case AXRADIO_MODE_STREAM_RECEIVE_SCRAM:
      002CE3                       6066 00103$:
                                   6067 ;	COMMON\easyax5043.c:1043: case AXRADIO_MODE_STREAM_RECEIVE_UNENC_LSB:
      002CE3                       6068 00104$:
                                   6069 ;	COMMON\easyax5043.c:1044: case AXRADIO_MODE_STREAM_RECEIVE_SCRAM_LSB:
      002CE3                       6070 00105$:
                                   6071 ;	COMMON\easyax5043.c:1045: case AXRADIO_MODE_ASYNC_RECEIVE:
      002CE3                       6072 00106$:
                                   6073 ;	COMMON\easyax5043.c:1046: case AXRADIO_MODE_WOR_RECEIVE:
      002CE3                       6074 00107$:
                                   6075 ;	COMMON\easyax5043.c:1047: if (axradio_syncstate == syncstate_asynctx)
      002CE3 90 00 A6         [24] 6076 	mov	dptr,#_axradio_syncstate
      002CE6 E0               [24] 6077 	movx	a,@dptr
      002CE7 FF               [12] 6078 	mov	r7,a
      002CE8 BF 02 03         [24] 6079 	cjne	r7,#0x02,00271$
      002CEB 02 2D 83         [24] 6080 	ljmp	00114$
      002CEE                       6081 00271$:
                                   6082 ;	COMMON\easyax5043.c:1049: wtimer_remove(&axradio_timer);
      002CEE 90 03 2B         [24] 6083 	mov	dptr,#_axradio_timer
      002CF1 12 76 94         [24] 6084 	lcall	_wtimer_remove
                                   6085 ;	COMMON\easyax5043.c:1050: rearmcstimer:
      002CF4                       6086 00110$:
                                   6087 ;	COMMON\easyax5043.c:1051: axradio_timer.time = axradio_phy_cs_period;
      002CF4 90 7F DA         [24] 6088 	mov	dptr,#_axradio_phy_cs_period
      002CF7 E4               [12] 6089 	clr	a
      002CF8 93               [24] 6090 	movc	a,@a+dptr
      002CF9 FE               [12] 6091 	mov	r6,a
      002CFA 74 01            [12] 6092 	mov	a,#0x01
      002CFC 93               [24] 6093 	movc	a,@a+dptr
      002CFD FF               [12] 6094 	mov	r7,a
      002CFE 7D 00            [12] 6095 	mov	r5,#0x00
      002D00 7C 00            [12] 6096 	mov	r4,#0x00
      002D02 90 03 2F         [24] 6097 	mov	dptr,#(_axradio_timer + 0x0004)
      002D05 EE               [12] 6098 	mov	a,r6
      002D06 F0               [24] 6099 	movx	@dptr,a
      002D07 EF               [12] 6100 	mov	a,r7
      002D08 A3               [24] 6101 	inc	dptr
      002D09 F0               [24] 6102 	movx	@dptr,a
      002D0A ED               [12] 6103 	mov	a,r5
      002D0B A3               [24] 6104 	inc	dptr
      002D0C F0               [24] 6105 	movx	@dptr,a
      002D0D EC               [12] 6106 	mov	a,r4
      002D0E A3               [24] 6107 	inc	dptr
      002D0F F0               [24] 6108 	movx	@dptr,a
                                   6109 ;	COMMON\easyax5043.c:1052: wtimer0_addrelative(&axradio_timer);
      002D10 90 03 2B         [24] 6110 	mov	dptr,#_axradio_timer
      002D13 12 6C DD         [24] 6111 	lcall	_wtimer0_addrelative
                                   6112 ;	COMMON\easyax5043.c:1053: chanstatecb:
      002D16                       6113 00111$:
                                   6114 ;	COMMON\easyax5043.c:1054: update_timeanchor();
      002D16 12 1B 43         [24] 6115 	lcall	_update_timeanchor
                                   6116 ;	COMMON\easyax5043.c:1055: wtimer_remove_callback(&axradio_cb_channelstate.cb);
      002D19 90 03 00         [24] 6117 	mov	dptr,#_axradio_cb_channelstate
      002D1C 12 78 A9         [24] 6118 	lcall	_wtimer_remove_callback
                                   6119 ;	COMMON\easyax5043.c:1056: axradio_cb_channelstate.st.error = AXRADIO_ERR_NOERROR;
      002D1F 90 03 05         [24] 6120 	mov	dptr,#(_axradio_cb_channelstate + 0x0005)
      002D22 E4               [12] 6121 	clr	a
      002D23 F0               [24] 6122 	movx	@dptr,a
                                   6123 ;	COMMON\easyax5043.c:1058: int8_t __autodata r = AX5043_RSSI;
      002D24 90 40 40         [24] 6124 	mov	dptr,#_AX5043_RSSI
      002D27 E0               [24] 6125 	movx	a,@dptr
                                   6126 ;	COMMON\easyax5043.c:1059: axradio_cb_channelstate.st.cs.rssi = r - (int16_t)axradio_phy_rssioffset;
      002D28 FF               [12] 6127 	mov	r7,a
      002D29 FD               [12] 6128 	mov	r5,a
      002D2A 33               [12] 6129 	rlc	a
      002D2B 95 E0            [12] 6130 	subb	a,acc
      002D2D FE               [12] 6131 	mov	r6,a
      002D2E 90 7F D7         [24] 6132 	mov	dptr,#_axradio_phy_rssioffset
      002D31 E4               [12] 6133 	clr	a
      002D32 93               [24] 6134 	movc	a,@a+dptr
      002D33 FC               [12] 6135 	mov	r4,a
      002D34 33               [12] 6136 	rlc	a
      002D35 95 E0            [12] 6137 	subb	a,acc
      002D37 FB               [12] 6138 	mov	r3,a
      002D38 ED               [12] 6139 	mov	a,r5
      002D39 C3               [12] 6140 	clr	c
      002D3A 9C               [12] 6141 	subb	a,r4
      002D3B FD               [12] 6142 	mov	r5,a
      002D3C EE               [12] 6143 	mov	a,r6
      002D3D 9B               [12] 6144 	subb	a,r3
      002D3E FE               [12] 6145 	mov	r6,a
      002D3F 90 03 0A         [24] 6146 	mov	dptr,#(_axradio_cb_channelstate + 0x000a)
      002D42 ED               [12] 6147 	mov	a,r5
      002D43 F0               [24] 6148 	movx	@dptr,a
      002D44 EE               [12] 6149 	mov	a,r6
      002D45 A3               [24] 6150 	inc	dptr
      002D46 F0               [24] 6151 	movx	@dptr,a
                                   6152 ;	COMMON\easyax5043.c:1060: axradio_cb_channelstate.st.cs.busy = r >= axradio_phy_channelbusy;
      002D47 90 7F D9         [24] 6153 	mov	dptr,#_axradio_phy_channelbusy
      002D4A E4               [12] 6154 	clr	a
      002D4B 93               [24] 6155 	movc	a,@a+dptr
      002D4C FE               [12] 6156 	mov	r6,a
      002D4D C3               [12] 6157 	clr	c
      002D4E EF               [12] 6158 	mov	a,r7
      002D4F 64 80            [12] 6159 	xrl	a,#0x80
      002D51 8E F0            [24] 6160 	mov	b,r6
      002D53 63 F0 80         [24] 6161 	xrl	b,#0x80
      002D56 95 F0            [12] 6162 	subb	a,b
      002D58 B3               [12] 6163 	cpl	c
      002D59 92 01            [24] 6164 	mov	_axradio_timer_callback_sloc0_1_0,c
      002D5B E4               [12] 6165 	clr	a
      002D5C 33               [12] 6166 	rlc	a
      002D5D 90 03 0C         [24] 6167 	mov	dptr,#(_axradio_cb_channelstate + 0x000c)
      002D60 F0               [24] 6168 	movx	@dptr,a
                                   6169 ;	COMMON\easyax5043.c:1062: axradio_cb_channelstate.st.time.t = axradio_timeanchor.radiotimer;
      002D61 90 00 BC         [24] 6170 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      002D64 E0               [24] 6171 	movx	a,@dptr
      002D65 FC               [12] 6172 	mov	r4,a
      002D66 A3               [24] 6173 	inc	dptr
      002D67 E0               [24] 6174 	movx	a,@dptr
      002D68 FD               [12] 6175 	mov	r5,a
      002D69 A3               [24] 6176 	inc	dptr
      002D6A E0               [24] 6177 	movx	a,@dptr
      002D6B FE               [12] 6178 	mov	r6,a
      002D6C A3               [24] 6179 	inc	dptr
      002D6D E0               [24] 6180 	movx	a,@dptr
      002D6E FF               [12] 6181 	mov	r7,a
      002D6F 90 03 06         [24] 6182 	mov	dptr,#(_axradio_cb_channelstate + 0x0006)
      002D72 EC               [12] 6183 	mov	a,r4
      002D73 F0               [24] 6184 	movx	@dptr,a
      002D74 ED               [12] 6185 	mov	a,r5
      002D75 A3               [24] 6186 	inc	dptr
      002D76 F0               [24] 6187 	movx	@dptr,a
      002D77 EE               [12] 6188 	mov	a,r6
      002D78 A3               [24] 6189 	inc	dptr
      002D79 F0               [24] 6190 	movx	@dptr,a
      002D7A EF               [12] 6191 	mov	a,r7
      002D7B A3               [24] 6192 	inc	dptr
      002D7C F0               [24] 6193 	movx	@dptr,a
                                   6194 ;	COMMON\easyax5043.c:1063: wtimer_add_callback(&axradio_cb_channelstate.cb);
      002D7D 90 03 00         [24] 6195 	mov	dptr,#_axradio_cb_channelstate
                                   6196 ;	COMMON\easyax5043.c:1064: break;
      002D80 02 6C C3         [24] 6197 	ljmp	_wtimer_add_callback
                                   6198 ;	COMMON\easyax5043.c:1066: case AXRADIO_MODE_ASYNC_TRANSMIT:
      002D83                       6199 00112$:
                                   6200 ;	COMMON\easyax5043.c:1067: case AXRADIO_MODE_WOR_TRANSMIT:
      002D83                       6201 00113$:
                                   6202 ;	COMMON\easyax5043.c:1068: transmitcs:
      002D83                       6203 00114$:
                                   6204 ;	COMMON\easyax5043.c:1069: if (axradio_ack_count)
      002D83 90 00 B0         [24] 6205 	mov	dptr,#_axradio_ack_count
      002D86 E0               [24] 6206 	movx	a,@dptr
      002D87 FF               [12] 6207 	mov	r7,a
      002D88 E0               [24] 6208 	movx	a,@dptr
      002D89 60 06            [24] 6209 	jz	00116$
                                   6210 ;	COMMON\easyax5043.c:1070: --axradio_ack_count;
      002D8B EF               [12] 6211 	mov	a,r7
      002D8C 14               [12] 6212 	dec	a
      002D8D 90 00 B0         [24] 6213 	mov	dptr,#_axradio_ack_count
      002D90 F0               [24] 6214 	movx	@dptr,a
      002D91                       6215 00116$:
                                   6216 ;	COMMON\easyax5043.c:1071: wtimer_remove(&axradio_timer);
      002D91 90 03 2B         [24] 6217 	mov	dptr,#_axradio_timer
      002D94 12 76 94         [24] 6218 	lcall	_wtimer_remove
                                   6219 ;	COMMON\easyax5043.c:1072: if ((int8_t)AX5043_RSSI < axradio_phy_channelbusy ||
      002D97 90 40 40         [24] 6220 	mov	dptr,#_AX5043_RSSI
      002D9A E0               [24] 6221 	movx	a,@dptr
      002D9B FF               [12] 6222 	mov	r7,a
      002D9C 90 7F D9         [24] 6223 	mov	dptr,#_axradio_phy_channelbusy
      002D9F E4               [12] 6224 	clr	a
      002DA0 93               [24] 6225 	movc	a,@a+dptr
      002DA1 FE               [12] 6226 	mov	r6,a
      002DA2 C3               [12] 6227 	clr	c
      002DA3 EF               [12] 6228 	mov	a,r7
      002DA4 64 80            [12] 6229 	xrl	a,#0x80
      002DA6 8E F0            [24] 6230 	mov	b,r6
      002DA8 63 F0 80         [24] 6231 	xrl	b,#0x80
      002DAB 95 F0            [12] 6232 	subb	a,b
      002DAD 40 0F            [24] 6233 	jc	00117$
                                   6234 ;	COMMON\easyax5043.c:1073: (!axradio_ack_count && axradio_phy_lbt_forcetx)) {
      002DAF 90 00 B0         [24] 6235 	mov	dptr,#_axradio_ack_count
      002DB2 E0               [24] 6236 	movx	a,@dptr
      002DB3 FF               [12] 6237 	mov	r7,a
      002DB4 E0               [24] 6238 	movx	a,@dptr
      002DB5 70 23            [24] 6239 	jnz	00118$
      002DB7 90 7F DE         [24] 6240 	mov	dptr,#_axradio_phy_lbt_forcetx
      002DBA E4               [12] 6241 	clr	a
      002DBB 93               [24] 6242 	movc	a,@a+dptr
      002DBC 60 1C            [24] 6243 	jz	00118$
      002DBE                       6244 00117$:
                                   6245 ;	COMMON\easyax5043.c:1074: axradio_syncstate = syncstate_off;
      002DBE 90 00 A6         [24] 6246 	mov	dptr,#_axradio_syncstate
      002DC1 E4               [12] 6247 	clr	a
      002DC2 F0               [24] 6248 	movx	@dptr,a
                                   6249 ;	COMMON\easyax5043.c:1075: axradio_txbuffer_cnt = axradio_phy_preamble_longlen;
      002DC3 90 7F E3         [24] 6250 	mov	dptr,#_axradio_phy_preamble_longlen
                                   6251 ;	genFromRTrack removed	clr	a
      002DC6 93               [24] 6252 	movc	a,@a+dptr
      002DC7 FD               [12] 6253 	mov	r5,a
      002DC8 74 01            [12] 6254 	mov	a,#0x01
      002DCA 93               [24] 6255 	movc	a,@a+dptr
      002DCB FE               [12] 6256 	mov	r6,a
      002DCC 90 00 A9         [24] 6257 	mov	dptr,#_axradio_txbuffer_cnt
      002DCF ED               [12] 6258 	mov	a,r5
      002DD0 F0               [24] 6259 	movx	@dptr,a
      002DD1 EE               [12] 6260 	mov	a,r6
      002DD2 A3               [24] 6261 	inc	dptr
      002DD3 F0               [24] 6262 	movx	@dptr,a
                                   6263 ;	COMMON\easyax5043.c:1076: ax5043_prepare_tx();
      002DD4 12 28 92         [24] 6264 	lcall	_ax5043_prepare_tx
                                   6265 ;	COMMON\easyax5043.c:1077: goto chanstatecb;
      002DD7 02 2D 16         [24] 6266 	ljmp	00111$
      002DDA                       6267 00118$:
                                   6268 ;	COMMON\easyax5043.c:1079: if (axradio_ack_count)
      002DDA EF               [12] 6269 	mov	a,r7
      002DDB 60 03            [24] 6270 	jz	00276$
      002DDD 02 2C F4         [24] 6271 	ljmp	00110$
      002DE0                       6272 00276$:
                                   6273 ;	COMMON\easyax5043.c:1081: update_timeanchor();
      002DE0 12 1B 43         [24] 6274 	lcall	_update_timeanchor
                                   6275 ;	COMMON\easyax5043.c:1082: axradio_syncstate = syncstate_off;
      002DE3 90 00 A6         [24] 6276 	mov	dptr,#_axradio_syncstate
      002DE6 E4               [12] 6277 	clr	a
      002DE7 F0               [24] 6278 	movx	@dptr,a
                                   6279 ;	COMMON\easyax5043.c:1083: ax5043_off();
      002DE8 12 28 BB         [24] 6280 	lcall	_ax5043_off
                                   6281 ;	COMMON\easyax5043.c:1084: wtimer_remove_callback(&axradio_cb_transmitstart.cb);
      002DEB 90 03 0D         [24] 6282 	mov	dptr,#_axradio_cb_transmitstart
      002DEE 12 78 A9         [24] 6283 	lcall	_wtimer_remove_callback
                                   6284 ;	COMMON\easyax5043.c:1085: axradio_cb_transmitstart.st.error = AXRADIO_ERR_TIMEOUT;
      002DF1 90 03 12         [24] 6285 	mov	dptr,#(_axradio_cb_transmitstart + 0x0005)
      002DF4 74 03            [12] 6286 	mov	a,#0x03
      002DF6 F0               [24] 6287 	movx	@dptr,a
                                   6288 ;	COMMON\easyax5043.c:1086: axradio_cb_transmitstart.st.time.t = axradio_timeanchor.radiotimer;
      002DF7 90 00 BC         [24] 6289 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      002DFA E0               [24] 6290 	movx	a,@dptr
      002DFB FC               [12] 6291 	mov	r4,a
      002DFC A3               [24] 6292 	inc	dptr
      002DFD E0               [24] 6293 	movx	a,@dptr
      002DFE FD               [12] 6294 	mov	r5,a
      002DFF A3               [24] 6295 	inc	dptr
      002E00 E0               [24] 6296 	movx	a,@dptr
      002E01 FE               [12] 6297 	mov	r6,a
      002E02 A3               [24] 6298 	inc	dptr
      002E03 E0               [24] 6299 	movx	a,@dptr
      002E04 FF               [12] 6300 	mov	r7,a
      002E05 90 03 13         [24] 6301 	mov	dptr,#(_axradio_cb_transmitstart + 0x0006)
      002E08 EC               [12] 6302 	mov	a,r4
      002E09 F0               [24] 6303 	movx	@dptr,a
      002E0A ED               [12] 6304 	mov	a,r5
      002E0B A3               [24] 6305 	inc	dptr
      002E0C F0               [24] 6306 	movx	@dptr,a
      002E0D EE               [12] 6307 	mov	a,r6
      002E0E A3               [24] 6308 	inc	dptr
      002E0F F0               [24] 6309 	movx	@dptr,a
      002E10 EF               [12] 6310 	mov	a,r7
      002E11 A3               [24] 6311 	inc	dptr
      002E12 F0               [24] 6312 	movx	@dptr,a
                                   6313 ;	COMMON\easyax5043.c:1087: wtimer_add_callback(&axradio_cb_transmitstart.cb);
      002E13 90 03 0D         [24] 6314 	mov	dptr,#_axradio_cb_transmitstart
                                   6315 ;	COMMON\easyax5043.c:1088: break;
      002E16 02 6C C3         [24] 6316 	ljmp	_wtimer_add_callback
                                   6317 ;	COMMON\easyax5043.c:1090: case AXRADIO_MODE_ACK_TRANSMIT:
      002E19                       6318 00123$:
                                   6319 ;	COMMON\easyax5043.c:1091: case AXRADIO_MODE_WOR_ACK_TRANSMIT:
      002E19                       6320 00124$:
                                   6321 ;	COMMON\easyax5043.c:1092: if (axradio_syncstate == syncstate_lbt)
      002E19 90 00 A6         [24] 6322 	mov	dptr,#_axradio_syncstate
      002E1C E0               [24] 6323 	movx	a,@dptr
      002E1D FF               [12] 6324 	mov	r7,a
      002E1E BF 01 03         [24] 6325 	cjne	r7,#0x01,00277$
      002E21 02 2D 83         [24] 6326 	ljmp	00114$
      002E24                       6327 00277$:
                                   6328 ;	COMMON\easyax5043.c:1094: ax5043_off();
      002E24 12 28 BB         [24] 6329 	lcall	_ax5043_off
                                   6330 ;	COMMON\easyax5043.c:1095: if (!axradio_ack_count) {
      002E27 90 00 B0         [24] 6331 	mov	dptr,#_axradio_ack_count
      002E2A E0               [24] 6332 	movx	a,@dptr
      002E2B FF               [12] 6333 	mov	r7,a
      002E2C E0               [24] 6334 	movx	a,@dptr
      002E2D 70 31            [24] 6335 	jnz	00128$
                                   6336 ;	COMMON\easyax5043.c:1096: update_timeanchor();
      002E2F 12 1B 43         [24] 6337 	lcall	_update_timeanchor
                                   6338 ;	COMMON\easyax5043.c:1097: wtimer_remove_callback(&axradio_cb_transmitend.cb);
      002E32 90 03 17         [24] 6339 	mov	dptr,#_axradio_cb_transmitend
      002E35 12 78 A9         [24] 6340 	lcall	_wtimer_remove_callback
                                   6341 ;	COMMON\easyax5043.c:1098: axradio_cb_transmitend.st.error = AXRADIO_ERR_TIMEOUT;
      002E38 90 03 1C         [24] 6342 	mov	dptr,#(_axradio_cb_transmitend + 0x0005)
      002E3B 74 03            [12] 6343 	mov	a,#0x03
      002E3D F0               [24] 6344 	movx	@dptr,a
                                   6345 ;	COMMON\easyax5043.c:1099: axradio_cb_transmitend.st.time.t = axradio_timeanchor.radiotimer;
      002E3E 90 00 BC         [24] 6346 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      002E41 E0               [24] 6347 	movx	a,@dptr
      002E42 FB               [12] 6348 	mov	r3,a
      002E43 A3               [24] 6349 	inc	dptr
      002E44 E0               [24] 6350 	movx	a,@dptr
      002E45 FC               [12] 6351 	mov	r4,a
      002E46 A3               [24] 6352 	inc	dptr
      002E47 E0               [24] 6353 	movx	a,@dptr
      002E48 FD               [12] 6354 	mov	r5,a
      002E49 A3               [24] 6355 	inc	dptr
      002E4A E0               [24] 6356 	movx	a,@dptr
      002E4B FE               [12] 6357 	mov	r6,a
      002E4C 90 03 1D         [24] 6358 	mov	dptr,#(_axradio_cb_transmitend + 0x0006)
      002E4F EB               [12] 6359 	mov	a,r3
      002E50 F0               [24] 6360 	movx	@dptr,a
      002E51 EC               [12] 6361 	mov	a,r4
      002E52 A3               [24] 6362 	inc	dptr
      002E53 F0               [24] 6363 	movx	@dptr,a
      002E54 ED               [12] 6364 	mov	a,r5
      002E55 A3               [24] 6365 	inc	dptr
      002E56 F0               [24] 6366 	movx	@dptr,a
      002E57 EE               [12] 6367 	mov	a,r6
      002E58 A3               [24] 6368 	inc	dptr
      002E59 F0               [24] 6369 	movx	@dptr,a
                                   6370 ;	COMMON\easyax5043.c:1100: wtimer_add_callback(&axradio_cb_transmitend.cb);
      002E5A 90 03 17         [24] 6371 	mov	dptr,#_axradio_cb_transmitend
                                   6372 ;	COMMON\easyax5043.c:1101: break;
      002E5D 02 6C C3         [24] 6373 	ljmp	_wtimer_add_callback
      002E60                       6374 00128$:
                                   6375 ;	COMMON\easyax5043.c:1103: --axradio_ack_count;
      002E60 EF               [12] 6376 	mov	a,r7
      002E61 14               [12] 6377 	dec	a
      002E62 90 00 B0         [24] 6378 	mov	dptr,#_axradio_ack_count
      002E65 F0               [24] 6379 	movx	@dptr,a
                                   6380 ;	COMMON\easyax5043.c:1104: axradio_txbuffer_cnt = axradio_phy_preamble_longlen;
      002E66 90 7F E3         [24] 6381 	mov	dptr,#_axradio_phy_preamble_longlen
      002E69 E4               [12] 6382 	clr	a
      002E6A 93               [24] 6383 	movc	a,@a+dptr
      002E6B FE               [12] 6384 	mov	r6,a
      002E6C 74 01            [12] 6385 	mov	a,#0x01
      002E6E 93               [24] 6386 	movc	a,@a+dptr
      002E6F FF               [12] 6387 	mov	r7,a
      002E70 90 00 A9         [24] 6388 	mov	dptr,#_axradio_txbuffer_cnt
      002E73 EE               [12] 6389 	mov	a,r6
      002E74 F0               [24] 6390 	movx	@dptr,a
      002E75 EF               [12] 6391 	mov	a,r7
      002E76 A3               [24] 6392 	inc	dptr
      002E77 F0               [24] 6393 	movx	@dptr,a
                                   6394 ;	COMMON\easyax5043.c:1105: ax5043_prepare_tx();
                                   6395 ;	COMMON\easyax5043.c:1106: break;
      002E78 02 28 92         [24] 6396 	ljmp	_ax5043_prepare_tx
                                   6397 ;	COMMON\easyax5043.c:1108: case AXRADIO_MODE_ACK_RECEIVE:
      002E7B                       6398 00129$:
                                   6399 ;	COMMON\easyax5043.c:1109: case AXRADIO_MODE_WOR_ACK_RECEIVE:
      002E7B                       6400 00130$:
                                   6401 ;	COMMON\easyax5043.c:1110: if (axradio_syncstate == syncstate_lbt)
      002E7B 90 00 A6         [24] 6402 	mov	dptr,#_axradio_syncstate
      002E7E E0               [24] 6403 	movx	a,@dptr
      002E7F FF               [12] 6404 	mov	r7,a
      002E80 BF 01 03         [24] 6405 	cjne	r7,#0x01,00279$
      002E83 02 2D 83         [24] 6406 	ljmp	00114$
      002E86                       6407 00279$:
                                   6408 ;	COMMON\easyax5043.c:1112: transmitack:
      002E86                       6409 00133$:
                                   6410 ;	COMMON\easyax5043.c:1113: AX5043_FIFOSTAT = 3;
      002E86 90 40 28         [24] 6411 	mov	dptr,#_AX5043_FIFOSTAT
      002E89 74 03            [12] 6412 	mov	a,#0x03
      002E8B F0               [24] 6413 	movx	@dptr,a
                                   6414 ;	COMMON\easyax5043.c:1114: AX5043_PWRMODE = AX5043_PWRSTATE_FULL_TX;
      002E8C 90 40 02         [24] 6415 	mov	dptr,#_AX5043_PWRMODE
      002E8F 74 0D            [12] 6416 	mov	a,#0x0d
      002E91 F0               [24] 6417 	movx	@dptr,a
                                   6418 ;	COMMON\easyax5043.c:1115: while (!(AX5043_POWSTAT & 0x08)); // wait for modem vdd so writing the FIFO is safe
      002E92                       6419 00134$:
      002E92 90 40 03         [24] 6420 	mov	dptr,#_AX5043_POWSTAT
      002E95 E0               [24] 6421 	movx	a,@dptr
      002E96 FF               [12] 6422 	mov	r7,a
      002E97 30 E3 F8         [24] 6423 	jnb	acc.3,00134$
                                   6424 ;	COMMON\easyax5043.c:1116: ax5043_init_registers_tx();
      002E9A 12 1C 20         [24] 6425 	lcall	_ax5043_init_registers_tx
                                   6426 ;	COMMON\easyax5043.c:1117: AX5043_RADIOEVENTREQ0; // make sure REVRDONE bit is cleared, so it is a reliable indicator that the packet is out
      002E9D 90 40 0F         [24] 6427 	mov	dptr,#_AX5043_RADIOEVENTREQ0
      002EA0 E0               [24] 6428 	movx	a,@dptr
                                   6429 ;	COMMON\easyax5043.c:1118: AX5043_FIFOTHRESH1 = 0;
      002EA1 90 40 2E         [24] 6430 	mov	dptr,#_AX5043_FIFOTHRESH1
      002EA4 E4               [12] 6431 	clr	a
      002EA5 F0               [24] 6432 	movx	@dptr,a
                                   6433 ;	COMMON\easyax5043.c:1119: AX5043_FIFOTHRESH0 = 0x80;
      002EA6 90 40 2F         [24] 6434 	mov	dptr,#_AX5043_FIFOTHRESH0
      002EA9 74 80            [12] 6435 	mov	a,#0x80
      002EAB F0               [24] 6436 	movx	@dptr,a
                                   6437 ;	COMMON\easyax5043.c:1120: axradio_trxstate = trxstate_tx_longpreamble;
      002EAC 75 0C 0A         [24] 6438 	mov	_axradio_trxstate,#0x0a
                                   6439 ;	COMMON\easyax5043.c:1121: axradio_txbuffer_cnt = axradio_phy_preamble_longlen;
      002EAF 90 7F E3         [24] 6440 	mov	dptr,#_axradio_phy_preamble_longlen
      002EB2 E4               [12] 6441 	clr	a
      002EB3 93               [24] 6442 	movc	a,@a+dptr
      002EB4 FE               [12] 6443 	mov	r6,a
      002EB5 74 01            [12] 6444 	mov	a,#0x01
      002EB7 93               [24] 6445 	movc	a,@a+dptr
      002EB8 FF               [12] 6446 	mov	r7,a
      002EB9 90 00 A9         [24] 6447 	mov	dptr,#_axradio_txbuffer_cnt
      002EBC EE               [12] 6448 	mov	a,r6
      002EBD F0               [24] 6449 	movx	@dptr,a
      002EBE EF               [12] 6450 	mov	a,r7
      002EBF A3               [24] 6451 	inc	dptr
      002EC0 F0               [24] 6452 	movx	@dptr,a
                                   6453 ;	COMMON\easyax5043.c:1123: if ((AX5043_MODULATION & 0x0F) == 9) { // 4-FSK
      002EC1 90 40 10         [24] 6454 	mov	dptr,#_AX5043_MODULATION
      002EC4 E0               [24] 6455 	movx	a,@dptr
      002EC5 FF               [12] 6456 	mov	r7,a
      002EC6 53 07 0F         [24] 6457 	anl	ar7,#0x0f
      002EC9 BF 09 0E         [24] 6458 	cjne	r7,#0x09,00138$
                                   6459 ;	COMMON\easyax5043.c:1124: AX5043_FIFODATA = AX5043_FIFOCMD_DATA | (7 << 5);
      002ECC 90 40 29         [24] 6460 	mov	dptr,#_AX5043_FIFODATA
      002ECF 74 E1            [12] 6461 	mov	a,#0xe1
      002ED1 F0               [24] 6462 	movx	@dptr,a
                                   6463 ;	COMMON\easyax5043.c:1125: AX5043_FIFODATA = 2;  // length (including flags)
      002ED2 74 02            [12] 6464 	mov	a,#0x02
      002ED4 F0               [24] 6465 	movx	@dptr,a
                                   6466 ;	COMMON\easyax5043.c:1126: AX5043_FIFODATA = 0x01;  // flag PKTSTART -> dibit sync
      002ED5 14               [12] 6467 	dec	a
      002ED6 F0               [24] 6468 	movx	@dptr,a
                                   6469 ;	COMMON\easyax5043.c:1127: AX5043_FIFODATA = 0x11; // dummy byte for forcing dibit sync
      002ED7 74 11            [12] 6470 	mov	a,#0x11
      002ED9 F0               [24] 6471 	movx	@dptr,a
      002EDA                       6472 00138$:
                                   6473 ;	COMMON\easyax5043.c:1134: AX5043_IRQMASK0 = 0x08; // enable fifo free threshold
      002EDA 90 40 07         [24] 6474 	mov	dptr,#_AX5043_IRQMASK0
      002EDD 74 08            [12] 6475 	mov	a,#0x08
      002EDF F0               [24] 6476 	movx	@dptr,a
                                   6477 ;	COMMON\easyax5043.c:1135: update_timeanchor();
      002EE0 12 1B 43         [24] 6478 	lcall	_update_timeanchor
                                   6479 ;	COMMON\easyax5043.c:1136: wtimer_remove_callback(&axradio_cb_transmitstart.cb);
      002EE3 90 03 0D         [24] 6480 	mov	dptr,#_axradio_cb_transmitstart
      002EE6 12 78 A9         [24] 6481 	lcall	_wtimer_remove_callback
                                   6482 ;	COMMON\easyax5043.c:1137: axradio_cb_transmitstart.st.error = AXRADIO_ERR_NOERROR;
      002EE9 90 03 12         [24] 6483 	mov	dptr,#(_axradio_cb_transmitstart + 0x0005)
      002EEC E4               [12] 6484 	clr	a
      002EED F0               [24] 6485 	movx	@dptr,a
                                   6486 ;	COMMON\easyax5043.c:1138: axradio_cb_transmitstart.st.time.t = axradio_timeanchor.radiotimer;
      002EEE 90 00 BC         [24] 6487 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      002EF1 E0               [24] 6488 	movx	a,@dptr
      002EF2 FC               [12] 6489 	mov	r4,a
      002EF3 A3               [24] 6490 	inc	dptr
      002EF4 E0               [24] 6491 	movx	a,@dptr
      002EF5 FD               [12] 6492 	mov	r5,a
      002EF6 A3               [24] 6493 	inc	dptr
      002EF7 E0               [24] 6494 	movx	a,@dptr
      002EF8 FE               [12] 6495 	mov	r6,a
      002EF9 A3               [24] 6496 	inc	dptr
      002EFA E0               [24] 6497 	movx	a,@dptr
      002EFB FF               [12] 6498 	mov	r7,a
      002EFC 90 03 13         [24] 6499 	mov	dptr,#(_axradio_cb_transmitstart + 0x0006)
      002EFF EC               [12] 6500 	mov	a,r4
      002F00 F0               [24] 6501 	movx	@dptr,a
      002F01 ED               [12] 6502 	mov	a,r5
      002F02 A3               [24] 6503 	inc	dptr
      002F03 F0               [24] 6504 	movx	@dptr,a
      002F04 EE               [12] 6505 	mov	a,r6
      002F05 A3               [24] 6506 	inc	dptr
      002F06 F0               [24] 6507 	movx	@dptr,a
      002F07 EF               [12] 6508 	mov	a,r7
      002F08 A3               [24] 6509 	inc	dptr
      002F09 F0               [24] 6510 	movx	@dptr,a
                                   6511 ;	COMMON\easyax5043.c:1139: wtimer_add_callback(&axradio_cb_transmitstart.cb);
      002F0A 90 03 0D         [24] 6512 	mov	dptr,#_axradio_cb_transmitstart
                                   6513 ;	COMMON\easyax5043.c:1140: break;
      002F0D 02 6C C3         [24] 6514 	ljmp	_wtimer_add_callback
                                   6515 ;	COMMON\easyax5043.c:1142: case AXRADIO_MODE_SYNC_MASTER:
      002F10                       6516 00139$:
                                   6517 ;	COMMON\easyax5043.c:1143: case AXRADIO_MODE_SYNC_ACK_MASTER:
      002F10                       6518 00140$:
                                   6519 ;	COMMON\easyax5043.c:1144: switch (axradio_syncstate) {
      002F10 90 00 A6         [24] 6520 	mov	dptr,#_axradio_syncstate
      002F13 E0               [24] 6521 	movx	a,@dptr
      002F14 FF               [12] 6522 	mov	r7,a
      002F15 BF 04 02         [24] 6523 	cjne	r7,#0x04,00283$
      002F18 80 58            [24] 6524 	sjmp	00142$
      002F1A                       6525 00283$:
      002F1A BF 05 03         [24] 6526 	cjne	r7,#0x05,00284$
      002F1D 02 30 4C         [24] 6527 	ljmp	00150$
      002F20                       6528 00284$:
                                   6529 ;	COMMON\easyax5043.c:1146: AX5043_PWRMODE = AX5043_PWRSTATE_XTAL_ON;
      002F20 90 40 02         [24] 6530 	mov	dptr,#_AX5043_PWRMODE
      002F23 74 05            [12] 6531 	mov	a,#0x05
      002F25 F0               [24] 6532 	movx	@dptr,a
                                   6533 ;	COMMON\easyax5043.c:1147: ax5043_init_registers_tx();
      002F26 12 1C 20         [24] 6534 	lcall	_ax5043_init_registers_tx
                                   6535 ;	COMMON\easyax5043.c:1148: axradio_syncstate = syncstate_master_xostartup;
      002F29 90 00 A6         [24] 6536 	mov	dptr,#_axradio_syncstate
      002F2C 74 04            [12] 6537 	mov	a,#0x04
      002F2E F0               [24] 6538 	movx	@dptr,a
                                   6539 ;	COMMON\easyax5043.c:1149: wtimer_remove_callback(&axradio_cb_transmitdata.cb);
      002F2F 90 03 21         [24] 6540 	mov	dptr,#_axradio_cb_transmitdata
      002F32 12 78 A9         [24] 6541 	lcall	_wtimer_remove_callback
                                   6542 ;	COMMON\easyax5043.c:1150: axradio_cb_transmitdata.st.error = AXRADIO_ERR_NOERROR;
      002F35 90 03 26         [24] 6543 	mov	dptr,#(_axradio_cb_transmitdata + 0x0005)
      002F38 E4               [12] 6544 	clr	a
      002F39 F0               [24] 6545 	movx	@dptr,a
                                   6546 ;	COMMON\easyax5043.c:1151: axradio_cb_transmitdata.st.time.t = 0;
      002F3A 90 03 27         [24] 6547 	mov	dptr,#(_axradio_cb_transmitdata + 0x0006)
      002F3D F0               [24] 6548 	movx	@dptr,a
      002F3E A3               [24] 6549 	inc	dptr
      002F3F F0               [24] 6550 	movx	@dptr,a
      002F40 A3               [24] 6551 	inc	dptr
      002F41 F0               [24] 6552 	movx	@dptr,a
      002F42 A3               [24] 6553 	inc	dptr
      002F43 F0               [24] 6554 	movx	@dptr,a
                                   6555 ;	COMMON\easyax5043.c:1152: wtimer_add_callback(&axradio_cb_transmitdata.cb);
      002F44 90 03 21         [24] 6556 	mov	dptr,#_axradio_cb_transmitdata
      002F47 12 6C C3         [24] 6557 	lcall	_wtimer_add_callback
                                   6558 ;	COMMON\easyax5043.c:1153: wtimer_remove(&axradio_timer);
      002F4A 90 03 2B         [24] 6559 	mov	dptr,#_axradio_timer
      002F4D 12 76 94         [24] 6560 	lcall	_wtimer_remove
                                   6561 ;	COMMON\easyax5043.c:1154: axradio_timer.time = axradio_sync_time;
      002F50 90 00 B2         [24] 6562 	mov	dptr,#_axradio_sync_time
      002F53 E0               [24] 6563 	movx	a,@dptr
      002F54 FC               [12] 6564 	mov	r4,a
      002F55 A3               [24] 6565 	inc	dptr
      002F56 E0               [24] 6566 	movx	a,@dptr
      002F57 FD               [12] 6567 	mov	r5,a
      002F58 A3               [24] 6568 	inc	dptr
      002F59 E0               [24] 6569 	movx	a,@dptr
      002F5A FE               [12] 6570 	mov	r6,a
      002F5B A3               [24] 6571 	inc	dptr
      002F5C E0               [24] 6572 	movx	a,@dptr
      002F5D FF               [12] 6573 	mov	r7,a
      002F5E 90 03 2F         [24] 6574 	mov	dptr,#(_axradio_timer + 0x0004)
      002F61 EC               [12] 6575 	mov	a,r4
      002F62 F0               [24] 6576 	movx	@dptr,a
      002F63 ED               [12] 6577 	mov	a,r5
      002F64 A3               [24] 6578 	inc	dptr
      002F65 F0               [24] 6579 	movx	@dptr,a
      002F66 EE               [12] 6580 	mov	a,r6
      002F67 A3               [24] 6581 	inc	dptr
      002F68 F0               [24] 6582 	movx	@dptr,a
      002F69 EF               [12] 6583 	mov	a,r7
      002F6A A3               [24] 6584 	inc	dptr
      002F6B F0               [24] 6585 	movx	@dptr,a
                                   6586 ;	COMMON\easyax5043.c:1155: wtimer0_addabsolute(&axradio_timer);
      002F6C 90 03 2B         [24] 6587 	mov	dptr,#_axradio_timer
                                   6588 ;	COMMON\easyax5043.c:1156: break;
      002F6F 02 6D B9         [24] 6589 	ljmp	_wtimer0_addabsolute
                                   6590 ;	COMMON\easyax5043.c:1158: case syncstate_master_xostartup:
      002F72                       6591 00142$:
                                   6592 ;	COMMON\easyax5043.c:1159: AX5043_FIFOSTAT = 3;
      002F72 90 40 28         [24] 6593 	mov	dptr,#_AX5043_FIFOSTAT
      002F75 74 03            [12] 6594 	mov	a,#0x03
      002F77 F0               [24] 6595 	movx	@dptr,a
                                   6596 ;	COMMON\easyax5043.c:1160: AX5043_PWRMODE = AX5043_PWRSTATE_FULL_TX;
      002F78 90 40 02         [24] 6597 	mov	dptr,#_AX5043_PWRMODE
      002F7B 74 0D            [12] 6598 	mov	a,#0x0d
      002F7D F0               [24] 6599 	movx	@dptr,a
                                   6600 ;	COMMON\easyax5043.c:1161: while (!(AX5043_POWSTAT & 0x08)); // wait for modem vdd so writing the FIFO is safe
      002F7E                       6601 00143$:
      002F7E 90 40 03         [24] 6602 	mov	dptr,#_AX5043_POWSTAT
      002F81 E0               [24] 6603 	movx	a,@dptr
      002F82 FF               [12] 6604 	mov	r7,a
      002F83 30 E3 F8         [24] 6605 	jnb	acc.3,00143$
                                   6606 ;	COMMON\easyax5043.c:1162: AX5043_RADIOEVENTREQ0; // make sure REVRDONE bit is cleared, so it is a reliable indicator that the packet is out
      002F86 90 40 0F         [24] 6607 	mov	dptr,#_AX5043_RADIOEVENTREQ0
      002F89 E0               [24] 6608 	movx	a,@dptr
                                   6609 ;	COMMON\easyax5043.c:1163: AX5043_FIFOTHRESH1 = 0;
      002F8A 90 40 2E         [24] 6610 	mov	dptr,#_AX5043_FIFOTHRESH1
      002F8D E4               [12] 6611 	clr	a
      002F8E F0               [24] 6612 	movx	@dptr,a
                                   6613 ;	COMMON\easyax5043.c:1164: AX5043_FIFOTHRESH0 = 0x80;
      002F8F 90 40 2F         [24] 6614 	mov	dptr,#_AX5043_FIFOTHRESH0
      002F92 74 80            [12] 6615 	mov	a,#0x80
      002F94 F0               [24] 6616 	movx	@dptr,a
                                   6617 ;	COMMON\easyax5043.c:1165: axradio_trxstate = trxstate_tx_longpreamble;
      002F95 75 0C 0A         [24] 6618 	mov	_axradio_trxstate,#0x0a
                                   6619 ;	COMMON\easyax5043.c:1166: axradio_txbuffer_cnt = axradio_phy_preamble_longlen;
      002F98 90 7F E3         [24] 6620 	mov	dptr,#_axradio_phy_preamble_longlen
      002F9B E4               [12] 6621 	clr	a
      002F9C 93               [24] 6622 	movc	a,@a+dptr
      002F9D FE               [12] 6623 	mov	r6,a
      002F9E 74 01            [12] 6624 	mov	a,#0x01
      002FA0 93               [24] 6625 	movc	a,@a+dptr
      002FA1 FF               [12] 6626 	mov	r7,a
      002FA2 90 00 A9         [24] 6627 	mov	dptr,#_axradio_txbuffer_cnt
      002FA5 EE               [12] 6628 	mov	a,r6
      002FA6 F0               [24] 6629 	movx	@dptr,a
      002FA7 EF               [12] 6630 	mov	a,r7
      002FA8 A3               [24] 6631 	inc	dptr
      002FA9 F0               [24] 6632 	movx	@dptr,a
                                   6633 ;	COMMON\easyax5043.c:1168: if ((AX5043_MODULATION & 0x0F) == 9) { // 4-FSK
      002FAA 90 40 10         [24] 6634 	mov	dptr,#_AX5043_MODULATION
      002FAD E0               [24] 6635 	movx	a,@dptr
      002FAE FF               [12] 6636 	mov	r7,a
      002FAF 53 07 0F         [24] 6637 	anl	ar7,#0x0f
      002FB2 BF 09 0E         [24] 6638 	cjne	r7,#0x09,00147$
                                   6639 ;	COMMON\easyax5043.c:1169: AX5043_FIFODATA = AX5043_FIFOCMD_DATA | (7 << 5);
      002FB5 90 40 29         [24] 6640 	mov	dptr,#_AX5043_FIFODATA
      002FB8 74 E1            [12] 6641 	mov	a,#0xe1
      002FBA F0               [24] 6642 	movx	@dptr,a
                                   6643 ;	COMMON\easyax5043.c:1170: AX5043_FIFODATA = 2;  // length (including flags)
      002FBB 74 02            [12] 6644 	mov	a,#0x02
      002FBD F0               [24] 6645 	movx	@dptr,a
                                   6646 ;	COMMON\easyax5043.c:1171: AX5043_FIFODATA = 0x01;  // flag PKTSTART -> dibit sync
      002FBE 14               [12] 6647 	dec	a
      002FBF F0               [24] 6648 	movx	@dptr,a
                                   6649 ;	COMMON\easyax5043.c:1172: AX5043_FIFODATA = 0x11; // dummy byte for forcing dibit sync
      002FC0 74 11            [12] 6650 	mov	a,#0x11
      002FC2 F0               [24] 6651 	movx	@dptr,a
      002FC3                       6652 00147$:
                                   6653 ;	COMMON\easyax5043.c:1179: wtimer_remove(&axradio_timer);
      002FC3 90 03 2B         [24] 6654 	mov	dptr,#_axradio_timer
      002FC6 12 76 94         [24] 6655 	lcall	_wtimer_remove
                                   6656 ;	COMMON\easyax5043.c:1180: update_timeanchor();
      002FC9 12 1B 43         [24] 6657 	lcall	_update_timeanchor
                                   6658 ;	COMMON\easyax5043.c:1181: AX5043_IRQMASK0 = 0x08; // enable fifo free threshold
      002FCC 90 40 07         [24] 6659 	mov	dptr,#_AX5043_IRQMASK0
      002FCF 74 08            [12] 6660 	mov	a,#0x08
      002FD1 F0               [24] 6661 	movx	@dptr,a
                                   6662 ;	COMMON\easyax5043.c:1182: axradio_sync_addtime(axradio_sync_period);
      002FD2 90 80 07         [24] 6663 	mov	dptr,#_axradio_sync_period
      002FD5 E4               [12] 6664 	clr	a
      002FD6 93               [24] 6665 	movc	a,@a+dptr
      002FD7 FC               [12] 6666 	mov	r4,a
      002FD8 74 01            [12] 6667 	mov	a,#0x01
      002FDA 93               [24] 6668 	movc	a,@a+dptr
      002FDB FD               [12] 6669 	mov	r5,a
      002FDC 74 02            [12] 6670 	mov	a,#0x02
      002FDE 93               [24] 6671 	movc	a,@a+dptr
      002FDF FE               [12] 6672 	mov	r6,a
      002FE0 74 03            [12] 6673 	mov	a,#0x03
      002FE2 93               [24] 6674 	movc	a,@a+dptr
      002FE3 8C 82            [24] 6675 	mov	dpl,r4
      002FE5 8D 83            [24] 6676 	mov	dph,r5
      002FE7 8E F0            [24] 6677 	mov	b,r6
      002FE9 12 2A 5D         [24] 6678 	lcall	_axradio_sync_addtime
                                   6679 ;	COMMON\easyax5043.c:1183: axradio_syncstate = syncstate_master_waitack;
      002FEC 90 00 A6         [24] 6680 	mov	dptr,#_axradio_syncstate
      002FEF 74 05            [12] 6681 	mov	a,#0x05
      002FF1 F0               [24] 6682 	movx	@dptr,a
                                   6683 ;	COMMON\easyax5043.c:1184: if (axradio_mode != AXRADIO_MODE_SYNC_ACK_MASTER) {
      002FF2 74 31            [12] 6684 	mov	a,#0x31
      002FF4 B5 0B 02         [24] 6685 	cjne	a,_axradio_mode,00288$
      002FF7 80 26            [24] 6686 	sjmp	00149$
      002FF9                       6687 00288$:
                                   6688 ;	COMMON\easyax5043.c:1185: axradio_syncstate = syncstate_master_normal;
      002FF9 90 00 A6         [24] 6689 	mov	dptr,#_axradio_syncstate
      002FFC 74 03            [12] 6690 	mov	a,#0x03
      002FFE F0               [24] 6691 	movx	@dptr,a
                                   6692 ;	COMMON\easyax5043.c:1186: axradio_sync_settimeradv(axradio_sync_xoscstartup);
      002FFF 90 80 0B         [24] 6693 	mov	dptr,#_axradio_sync_xoscstartup
      003002 E4               [12] 6694 	clr	a
      003003 93               [24] 6695 	movc	a,@a+dptr
      003004 FC               [12] 6696 	mov	r4,a
      003005 74 01            [12] 6697 	mov	a,#0x01
      003007 93               [24] 6698 	movc	a,@a+dptr
      003008 FD               [12] 6699 	mov	r5,a
      003009 74 02            [12] 6700 	mov	a,#0x02
      00300B 93               [24] 6701 	movc	a,@a+dptr
      00300C FE               [12] 6702 	mov	r6,a
      00300D 74 03            [12] 6703 	mov	a,#0x03
      00300F 93               [24] 6704 	movc	a,@a+dptr
      003010 8C 82            [24] 6705 	mov	dpl,r4
      003012 8D 83            [24] 6706 	mov	dph,r5
      003014 8E F0            [24] 6707 	mov	b,r6
      003016 12 2A AE         [24] 6708 	lcall	_axradio_sync_settimeradv
                                   6709 ;	COMMON\easyax5043.c:1187: wtimer0_addabsolute(&axradio_timer);
      003019 90 03 2B         [24] 6710 	mov	dptr,#_axradio_timer
      00301C 12 6D B9         [24] 6711 	lcall	_wtimer0_addabsolute
      00301F                       6712 00149$:
                                   6713 ;	COMMON\easyax5043.c:1189: wtimer_remove_callback(&axradio_cb_transmitstart.cb);
      00301F 90 03 0D         [24] 6714 	mov	dptr,#_axradio_cb_transmitstart
      003022 12 78 A9         [24] 6715 	lcall	_wtimer_remove_callback
                                   6716 ;	COMMON\easyax5043.c:1190: axradio_cb_transmitstart.st.error = AXRADIO_ERR_NOERROR;
      003025 90 03 12         [24] 6717 	mov	dptr,#(_axradio_cb_transmitstart + 0x0005)
      003028 E4               [12] 6718 	clr	a
      003029 F0               [24] 6719 	movx	@dptr,a
                                   6720 ;	COMMON\easyax5043.c:1191: axradio_cb_transmitstart.st.time.t = axradio_timeanchor.radiotimer;
      00302A 90 00 BC         [24] 6721 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      00302D E0               [24] 6722 	movx	a,@dptr
      00302E FC               [12] 6723 	mov	r4,a
      00302F A3               [24] 6724 	inc	dptr
      003030 E0               [24] 6725 	movx	a,@dptr
      003031 FD               [12] 6726 	mov	r5,a
      003032 A3               [24] 6727 	inc	dptr
      003033 E0               [24] 6728 	movx	a,@dptr
      003034 FE               [12] 6729 	mov	r6,a
      003035 A3               [24] 6730 	inc	dptr
      003036 E0               [24] 6731 	movx	a,@dptr
      003037 FF               [12] 6732 	mov	r7,a
      003038 90 03 13         [24] 6733 	mov	dptr,#(_axradio_cb_transmitstart + 0x0006)
      00303B EC               [12] 6734 	mov	a,r4
      00303C F0               [24] 6735 	movx	@dptr,a
      00303D ED               [12] 6736 	mov	a,r5
      00303E A3               [24] 6737 	inc	dptr
      00303F F0               [24] 6738 	movx	@dptr,a
      003040 EE               [12] 6739 	mov	a,r6
      003041 A3               [24] 6740 	inc	dptr
      003042 F0               [24] 6741 	movx	@dptr,a
      003043 EF               [12] 6742 	mov	a,r7
      003044 A3               [24] 6743 	inc	dptr
      003045 F0               [24] 6744 	movx	@dptr,a
                                   6745 ;	COMMON\easyax5043.c:1192: wtimer_add_callback(&axradio_cb_transmitstart.cb);
      003046 90 03 0D         [24] 6746 	mov	dptr,#_axradio_cb_transmitstart
                                   6747 ;	COMMON\easyax5043.c:1193: break;
      003049 02 6C C3         [24] 6748 	ljmp	_wtimer_add_callback
                                   6749 ;	COMMON\easyax5043.c:1195: case syncstate_master_waitack:
      00304C                       6750 00150$:
                                   6751 ;	COMMON\easyax5043.c:1196: ax5043_off();
      00304C 12 28 BB         [24] 6752 	lcall	_ax5043_off
                                   6753 ;	COMMON\easyax5043.c:1197: axradio_syncstate = syncstate_master_normal;
      00304F 90 00 A6         [24] 6754 	mov	dptr,#_axradio_syncstate
      003052 74 03            [12] 6755 	mov	a,#0x03
      003054 F0               [24] 6756 	movx	@dptr,a
                                   6757 ;	COMMON\easyax5043.c:1198: wtimer_remove(&axradio_timer);
      003055 90 03 2B         [24] 6758 	mov	dptr,#_axradio_timer
      003058 12 76 94         [24] 6759 	lcall	_wtimer_remove
                                   6760 ;	COMMON\easyax5043.c:1199: axradio_sync_settimeradv(axradio_sync_xoscstartup);
      00305B 90 80 0B         [24] 6761 	mov	dptr,#_axradio_sync_xoscstartup
      00305E E4               [12] 6762 	clr	a
      00305F 93               [24] 6763 	movc	a,@a+dptr
      003060 FC               [12] 6764 	mov	r4,a
      003061 74 01            [12] 6765 	mov	a,#0x01
      003063 93               [24] 6766 	movc	a,@a+dptr
      003064 FD               [12] 6767 	mov	r5,a
      003065 74 02            [12] 6768 	mov	a,#0x02
      003067 93               [24] 6769 	movc	a,@a+dptr
      003068 FE               [12] 6770 	mov	r6,a
      003069 74 03            [12] 6771 	mov	a,#0x03
      00306B 93               [24] 6772 	movc	a,@a+dptr
      00306C 8C 82            [24] 6773 	mov	dpl,r4
      00306E 8D 83            [24] 6774 	mov	dph,r5
      003070 8E F0            [24] 6775 	mov	b,r6
      003072 12 2A AE         [24] 6776 	lcall	_axradio_sync_settimeradv
                                   6777 ;	COMMON\easyax5043.c:1200: wtimer0_addabsolute(&axradio_timer);
      003075 90 03 2B         [24] 6778 	mov	dptr,#_axradio_timer
      003078 12 6D B9         [24] 6779 	lcall	_wtimer0_addabsolute
                                   6780 ;	COMMON\easyax5043.c:1201: update_timeanchor();
      00307B 12 1B 43         [24] 6781 	lcall	_update_timeanchor
                                   6782 ;	COMMON\easyax5043.c:1202: wtimer_remove_callback(&axradio_cb_transmitend.cb);
      00307E 90 03 17         [24] 6783 	mov	dptr,#_axradio_cb_transmitend
      003081 12 78 A9         [24] 6784 	lcall	_wtimer_remove_callback
                                   6785 ;	COMMON\easyax5043.c:1203: axradio_cb_transmitend.st.error = AXRADIO_ERR_TIMEOUT;
      003084 90 03 1C         [24] 6786 	mov	dptr,#(_axradio_cb_transmitend + 0x0005)
      003087 74 03            [12] 6787 	mov	a,#0x03
      003089 F0               [24] 6788 	movx	@dptr,a
                                   6789 ;	COMMON\easyax5043.c:1204: axradio_cb_transmitend.st.time.t = axradio_timeanchor.radiotimer;
      00308A 90 00 BC         [24] 6790 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      00308D E0               [24] 6791 	movx	a,@dptr
      00308E FC               [12] 6792 	mov	r4,a
      00308F A3               [24] 6793 	inc	dptr
      003090 E0               [24] 6794 	movx	a,@dptr
      003091 FD               [12] 6795 	mov	r5,a
      003092 A3               [24] 6796 	inc	dptr
      003093 E0               [24] 6797 	movx	a,@dptr
      003094 FE               [12] 6798 	mov	r6,a
      003095 A3               [24] 6799 	inc	dptr
      003096 E0               [24] 6800 	movx	a,@dptr
      003097 FF               [12] 6801 	mov	r7,a
      003098 90 03 1D         [24] 6802 	mov	dptr,#(_axradio_cb_transmitend + 0x0006)
      00309B EC               [12] 6803 	mov	a,r4
      00309C F0               [24] 6804 	movx	@dptr,a
      00309D ED               [12] 6805 	mov	a,r5
      00309E A3               [24] 6806 	inc	dptr
      00309F F0               [24] 6807 	movx	@dptr,a
      0030A0 EE               [12] 6808 	mov	a,r6
      0030A1 A3               [24] 6809 	inc	dptr
      0030A2 F0               [24] 6810 	movx	@dptr,a
      0030A3 EF               [12] 6811 	mov	a,r7
      0030A4 A3               [24] 6812 	inc	dptr
      0030A5 F0               [24] 6813 	movx	@dptr,a
                                   6814 ;	COMMON\easyax5043.c:1205: wtimer_add_callback(&axradio_cb_transmitend.cb);
      0030A6 90 03 17         [24] 6815 	mov	dptr,#_axradio_cb_transmitend
                                   6816 ;	COMMON\easyax5043.c:1208: break;
      0030A9 02 6C C3         [24] 6817 	ljmp	_wtimer_add_callback
                                   6818 ;	COMMON\easyax5043.c:1210: case AXRADIO_MODE_SYNC_SLAVE:
      0030AC                       6819 00152$:
                                   6820 ;	COMMON\easyax5043.c:1211: case AXRADIO_MODE_SYNC_ACK_SLAVE:
      0030AC                       6821 00153$:
                                   6822 ;	COMMON\easyax5043.c:1212: switch (axradio_syncstate) {
      0030AC 90 00 A6         [24] 6823 	mov	dptr,#_axradio_syncstate
      0030AF E0               [24] 6824 	movx	a,@dptr
      0030B0 FF               [12] 6825 	mov  r7,a
      0030B1 24 F3            [12] 6826 	add	a,#0xff - 0x0c
      0030B3 50 03            [24] 6827 	jnc	00289$
      0030B5 02 30 E3         [24] 6828 	ljmp	00155$
      0030B8                       6829 00289$:
      0030B8 EF               [12] 6830 	mov	a,r7
      0030B9 F5 F0            [12] 6831 	mov	b,a
      0030BB 24 0B            [12] 6832 	add	a,#(00290$-3-.)
      0030BD 83               [24] 6833 	movc	a,@a+pc
      0030BE F5 82            [12] 6834 	mov	dpl,a
      0030C0 E5 F0            [12] 6835 	mov	a,b
      0030C2 24 11            [12] 6836 	add	a,#(00291$-3-.)
      0030C4 83               [24] 6837 	movc	a,@a+pc
      0030C5 F5 83            [12] 6838 	mov	dph,a
      0030C7 E4               [12] 6839 	clr	a
      0030C8 73               [24] 6840 	jmp	@a+dptr
      0030C9                       6841 00290$:
      0030C9 E3                    6842 	.db	00154$
      0030CA E3                    6843 	.db	00154$
      0030CB E3                    6844 	.db	00154$
      0030CC E3                    6845 	.db	00154$
      0030CD E3                    6846 	.db	00154$
      0030CE E3                    6847 	.db	00154$
      0030CF E3                    6848 	.db	00155$
      0030D0 73                    6849 	.db	00156$
      0030D1 06                    6850 	.db	00157$
      0030D2 58                    6851 	.db	00158$
      0030D3 0E                    6852 	.db	00161$
      0030D4 6C                    6853 	.db	00166$
      0030D5 85                    6854 	.db	00173$
      0030D6                       6855 00291$:
      0030D6 30                    6856 	.db	00154$>>8
      0030D7 30                    6857 	.db	00154$>>8
      0030D8 30                    6858 	.db	00154$>>8
      0030D9 30                    6859 	.db	00154$>>8
      0030DA 30                    6860 	.db	00154$>>8
      0030DB 30                    6861 	.db	00154$>>8
      0030DC 30                    6862 	.db	00155$>>8
      0030DD 31                    6863 	.db	00156$>>8
      0030DE 32                    6864 	.db	00157$>>8
      0030DF 32                    6865 	.db	00158$>>8
      0030E0 33                    6866 	.db	00161$>>8
      0030E1 33                    6867 	.db	00166$>>8
      0030E2 34                    6868 	.db	00173$>>8
                                   6869 ;	COMMON\easyax5043.c:1213: default:
      0030E3                       6870 00154$:
                                   6871 ;	COMMON\easyax5043.c:1214: case syncstate_slave_synchunt:
      0030E3                       6872 00155$:
                                   6873 ;	COMMON\easyax5043.c:1215: ax5043_off();
      0030E3 12 28 BB         [24] 6874 	lcall	_ax5043_off
                                   6875 ;	COMMON\easyax5043.c:1216: axradio_syncstate = syncstate_slave_syncpause;
      0030E6 90 00 A6         [24] 6876 	mov	dptr,#_axradio_syncstate
      0030E9 74 07            [12] 6877 	mov	a,#0x07
      0030EB F0               [24] 6878 	movx	@dptr,a
                                   6879 ;	COMMON\easyax5043.c:1217: axradio_sync_addtime(axradio_sync_slave_syncpause);
      0030EC 90 80 17         [24] 6880 	mov	dptr,#_axradio_sync_slave_syncpause
      0030EF E4               [12] 6881 	clr	a
      0030F0 93               [24] 6882 	movc	a,@a+dptr
      0030F1 FC               [12] 6883 	mov	r4,a
      0030F2 74 01            [12] 6884 	mov	a,#0x01
      0030F4 93               [24] 6885 	movc	a,@a+dptr
      0030F5 FD               [12] 6886 	mov	r5,a
      0030F6 74 02            [12] 6887 	mov	a,#0x02
      0030F8 93               [24] 6888 	movc	a,@a+dptr
      0030F9 FE               [12] 6889 	mov	r6,a
      0030FA 74 03            [12] 6890 	mov	a,#0x03
      0030FC 93               [24] 6891 	movc	a,@a+dptr
      0030FD 8C 82            [24] 6892 	mov	dpl,r4
      0030FF 8D 83            [24] 6893 	mov	dph,r5
      003101 8E F0            [24] 6894 	mov	b,r6
      003103 12 2A 5D         [24] 6895 	lcall	_axradio_sync_addtime
                                   6896 ;	COMMON\easyax5043.c:1218: wtimer_remove(&axradio_timer);
      003106 90 03 2B         [24] 6897 	mov	dptr,#_axradio_timer
      003109 12 76 94         [24] 6898 	lcall	_wtimer_remove
                                   6899 ;	COMMON\easyax5043.c:1219: axradio_timer.time = axradio_sync_time;
      00310C 90 00 B2         [24] 6900 	mov	dptr,#_axradio_sync_time
      00310F E0               [24] 6901 	movx	a,@dptr
      003110 FC               [12] 6902 	mov	r4,a
      003111 A3               [24] 6903 	inc	dptr
      003112 E0               [24] 6904 	movx	a,@dptr
      003113 FD               [12] 6905 	mov	r5,a
      003114 A3               [24] 6906 	inc	dptr
      003115 E0               [24] 6907 	movx	a,@dptr
      003116 FE               [12] 6908 	mov	r6,a
      003117 A3               [24] 6909 	inc	dptr
      003118 E0               [24] 6910 	movx	a,@dptr
      003119 FF               [12] 6911 	mov	r7,a
      00311A 90 03 2F         [24] 6912 	mov	dptr,#(_axradio_timer + 0x0004)
      00311D EC               [12] 6913 	mov	a,r4
      00311E F0               [24] 6914 	movx	@dptr,a
      00311F ED               [12] 6915 	mov	a,r5
      003120 A3               [24] 6916 	inc	dptr
      003121 F0               [24] 6917 	movx	@dptr,a
      003122 EE               [12] 6918 	mov	a,r6
      003123 A3               [24] 6919 	inc	dptr
      003124 F0               [24] 6920 	movx	@dptr,a
      003125 EF               [12] 6921 	mov	a,r7
      003126 A3               [24] 6922 	inc	dptr
      003127 F0               [24] 6923 	movx	@dptr,a
                                   6924 ;	COMMON\easyax5043.c:1220: wtimer0_addabsolute(&axradio_timer);
      003128 90 03 2B         [24] 6925 	mov	dptr,#_axradio_timer
      00312B 12 6D B9         [24] 6926 	lcall	_wtimer0_addabsolute
                                   6927 ;	COMMON\easyax5043.c:1221: wtimer_remove_callback(&axradio_cb_receive.cb);
      00312E 90 02 D4         [24] 6928 	mov	dptr,#_axradio_cb_receive
      003131 12 78 A9         [24] 6929 	lcall	_wtimer_remove_callback
                                   6930 ;	COMMON\easyax5043.c:1222: memset_xdata(&axradio_cb_receive.st, 0, sizeof(axradio_cb_receive.st));
      003134 90 03 C6         [24] 6931 	mov	dptr,#_memset_PARM_2
      003137 E4               [12] 6932 	clr	a
      003138 F0               [24] 6933 	movx	@dptr,a
      003139 90 03 C7         [24] 6934 	mov	dptr,#_memset_PARM_3
      00313C 74 1E            [12] 6935 	mov	a,#0x1e
      00313E F0               [24] 6936 	movx	@dptr,a
      00313F E4               [12] 6937 	clr	a
      003140 A3               [24] 6938 	inc	dptr
      003141 F0               [24] 6939 	movx	@dptr,a
      003142 90 02 D8         [24] 6940 	mov	dptr,#(_axradio_cb_receive + 0x0004)
      003145 75 F0 00         [24] 6941 	mov	b,#0x00
      003148 12 69 59         [24] 6942 	lcall	_memset
                                   6943 ;	COMMON\easyax5043.c:1223: axradio_cb_receive.st.time.t = axradio_timeanchor.radiotimer;
      00314B 90 00 BC         [24] 6944 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      00314E E0               [24] 6945 	movx	a,@dptr
      00314F FC               [12] 6946 	mov	r4,a
      003150 A3               [24] 6947 	inc	dptr
      003151 E0               [24] 6948 	movx	a,@dptr
      003152 FD               [12] 6949 	mov	r5,a
      003153 A3               [24] 6950 	inc	dptr
      003154 E0               [24] 6951 	movx	a,@dptr
      003155 FE               [12] 6952 	mov	r6,a
      003156 A3               [24] 6953 	inc	dptr
      003157 E0               [24] 6954 	movx	a,@dptr
      003158 FF               [12] 6955 	mov	r7,a
      003159 90 02 DA         [24] 6956 	mov	dptr,#(_axradio_cb_receive + 0x0006)
      00315C EC               [12] 6957 	mov	a,r4
      00315D F0               [24] 6958 	movx	@dptr,a
      00315E ED               [12] 6959 	mov	a,r5
      00315F A3               [24] 6960 	inc	dptr
      003160 F0               [24] 6961 	movx	@dptr,a
      003161 EE               [12] 6962 	mov	a,r6
      003162 A3               [24] 6963 	inc	dptr
      003163 F0               [24] 6964 	movx	@dptr,a
      003164 EF               [12] 6965 	mov	a,r7
      003165 A3               [24] 6966 	inc	dptr
      003166 F0               [24] 6967 	movx	@dptr,a
                                   6968 ;	COMMON\easyax5043.c:1224: axradio_cb_receive.st.error = AXRADIO_ERR_RESYNCTIMEOUT;
      003167 90 02 D9         [24] 6969 	mov	dptr,#(_axradio_cb_receive + 0x0005)
      00316A 74 0A            [12] 6970 	mov	a,#0x0a
      00316C F0               [24] 6971 	movx	@dptr,a
                                   6972 ;	COMMON\easyax5043.c:1225: wtimer_add_callback(&axradio_cb_receive.cb);
      00316D 90 02 D4         [24] 6973 	mov	dptr,#_axradio_cb_receive
                                   6974 ;	COMMON\easyax5043.c:1226: break;
      003170 02 6C C3         [24] 6975 	ljmp	_wtimer_add_callback
                                   6976 ;	COMMON\easyax5043.c:1228: case syncstate_slave_syncpause:
      003173                       6977 00156$:
                                   6978 ;	COMMON\easyax5043.c:1229: ax5043_receiver_on_continuous();
      003173 12 27 64         [24] 6979 	lcall	_ax5043_receiver_on_continuous
                                   6980 ;	COMMON\easyax5043.c:1230: axradio_syncstate = syncstate_slave_synchunt;
      003176 90 00 A6         [24] 6981 	mov	dptr,#_axradio_syncstate
      003179 74 06            [12] 6982 	mov	a,#0x06
      00317B F0               [24] 6983 	movx	@dptr,a
                                   6984 ;	COMMON\easyax5043.c:1231: axradio_sync_addtime(axradio_sync_slave_syncwindow);
      00317C 90 80 0F         [24] 6985 	mov	dptr,#_axradio_sync_slave_syncwindow
      00317F E4               [12] 6986 	clr	a
      003180 93               [24] 6987 	movc	a,@a+dptr
      003181 FC               [12] 6988 	mov	r4,a
      003182 74 01            [12] 6989 	mov	a,#0x01
      003184 93               [24] 6990 	movc	a,@a+dptr
      003185 FD               [12] 6991 	mov	r5,a
      003186 74 02            [12] 6992 	mov	a,#0x02
      003188 93               [24] 6993 	movc	a,@a+dptr
      003189 FE               [12] 6994 	mov	r6,a
      00318A 74 03            [12] 6995 	mov	a,#0x03
      00318C 93               [24] 6996 	movc	a,@a+dptr
      00318D 8C 82            [24] 6997 	mov	dpl,r4
      00318F 8D 83            [24] 6998 	mov	dph,r5
      003191 8E F0            [24] 6999 	mov	b,r6
      003193 12 2A 5D         [24] 7000 	lcall	_axradio_sync_addtime
                                   7001 ;	COMMON\easyax5043.c:1232: wtimer_remove(&axradio_timer);
      003196 90 03 2B         [24] 7002 	mov	dptr,#_axradio_timer
      003199 12 76 94         [24] 7003 	lcall	_wtimer_remove
                                   7004 ;	COMMON\easyax5043.c:1233: axradio_timer.time = axradio_sync_time;
      00319C 90 00 B2         [24] 7005 	mov	dptr,#_axradio_sync_time
      00319F E0               [24] 7006 	movx	a,@dptr
      0031A0 FC               [12] 7007 	mov	r4,a
      0031A1 A3               [24] 7008 	inc	dptr
      0031A2 E0               [24] 7009 	movx	a,@dptr
      0031A3 FD               [12] 7010 	mov	r5,a
      0031A4 A3               [24] 7011 	inc	dptr
      0031A5 E0               [24] 7012 	movx	a,@dptr
      0031A6 FE               [12] 7013 	mov	r6,a
      0031A7 A3               [24] 7014 	inc	dptr
      0031A8 E0               [24] 7015 	movx	a,@dptr
      0031A9 FF               [12] 7016 	mov	r7,a
      0031AA 90 03 2F         [24] 7017 	mov	dptr,#(_axradio_timer + 0x0004)
      0031AD EC               [12] 7018 	mov	a,r4
      0031AE F0               [24] 7019 	movx	@dptr,a
      0031AF ED               [12] 7020 	mov	a,r5
      0031B0 A3               [24] 7021 	inc	dptr
      0031B1 F0               [24] 7022 	movx	@dptr,a
      0031B2 EE               [12] 7023 	mov	a,r6
      0031B3 A3               [24] 7024 	inc	dptr
      0031B4 F0               [24] 7025 	movx	@dptr,a
      0031B5 EF               [12] 7026 	mov	a,r7
      0031B6 A3               [24] 7027 	inc	dptr
      0031B7 F0               [24] 7028 	movx	@dptr,a
                                   7029 ;	COMMON\easyax5043.c:1234: wtimer0_addabsolute(&axradio_timer);
      0031B8 90 03 2B         [24] 7030 	mov	dptr,#_axradio_timer
      0031BB 12 6D B9         [24] 7031 	lcall	_wtimer0_addabsolute
                                   7032 ;	COMMON\easyax5043.c:1235: update_timeanchor();
      0031BE 12 1B 43         [24] 7033 	lcall	_update_timeanchor
                                   7034 ;	COMMON\easyax5043.c:1236: wtimer_remove_callback(&axradio_cb_receive.cb);
      0031C1 90 02 D4         [24] 7035 	mov	dptr,#_axradio_cb_receive
      0031C4 12 78 A9         [24] 7036 	lcall	_wtimer_remove_callback
                                   7037 ;	COMMON\easyax5043.c:1237: memset_xdata(&axradio_cb_receive.st, 0, sizeof(axradio_cb_receive.st));
      0031C7 90 03 C6         [24] 7038 	mov	dptr,#_memset_PARM_2
      0031CA E4               [12] 7039 	clr	a
      0031CB F0               [24] 7040 	movx	@dptr,a
      0031CC 90 03 C7         [24] 7041 	mov	dptr,#_memset_PARM_3
      0031CF 74 1E            [12] 7042 	mov	a,#0x1e
      0031D1 F0               [24] 7043 	movx	@dptr,a
      0031D2 E4               [12] 7044 	clr	a
      0031D3 A3               [24] 7045 	inc	dptr
      0031D4 F0               [24] 7046 	movx	@dptr,a
      0031D5 90 02 D8         [24] 7047 	mov	dptr,#(_axradio_cb_receive + 0x0004)
      0031D8 75 F0 00         [24] 7048 	mov	b,#0x00
      0031DB 12 69 59         [24] 7049 	lcall	_memset
                                   7050 ;	COMMON\easyax5043.c:1238: axradio_cb_receive.st.time.t = axradio_timeanchor.radiotimer;
      0031DE 90 00 BC         [24] 7051 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      0031E1 E0               [24] 7052 	movx	a,@dptr
      0031E2 FC               [12] 7053 	mov	r4,a
      0031E3 A3               [24] 7054 	inc	dptr
      0031E4 E0               [24] 7055 	movx	a,@dptr
      0031E5 FD               [12] 7056 	mov	r5,a
      0031E6 A3               [24] 7057 	inc	dptr
      0031E7 E0               [24] 7058 	movx	a,@dptr
      0031E8 FE               [12] 7059 	mov	r6,a
      0031E9 A3               [24] 7060 	inc	dptr
      0031EA E0               [24] 7061 	movx	a,@dptr
      0031EB FF               [12] 7062 	mov	r7,a
      0031EC 90 02 DA         [24] 7063 	mov	dptr,#(_axradio_cb_receive + 0x0006)
      0031EF EC               [12] 7064 	mov	a,r4
      0031F0 F0               [24] 7065 	movx	@dptr,a
      0031F1 ED               [12] 7066 	mov	a,r5
      0031F2 A3               [24] 7067 	inc	dptr
      0031F3 F0               [24] 7068 	movx	@dptr,a
      0031F4 EE               [12] 7069 	mov	a,r6
      0031F5 A3               [24] 7070 	inc	dptr
      0031F6 F0               [24] 7071 	movx	@dptr,a
      0031F7 EF               [12] 7072 	mov	a,r7
      0031F8 A3               [24] 7073 	inc	dptr
      0031F9 F0               [24] 7074 	movx	@dptr,a
                                   7075 ;	COMMON\easyax5043.c:1239: axradio_cb_receive.st.error = AXRADIO_ERR_RESYNC;
      0031FA 90 02 D9         [24] 7076 	mov	dptr,#(_axradio_cb_receive + 0x0005)
      0031FD 74 09            [12] 7077 	mov	a,#0x09
      0031FF F0               [24] 7078 	movx	@dptr,a
                                   7079 ;	COMMON\easyax5043.c:1240: wtimer_add_callback(&axradio_cb_receive.cb);
      003200 90 02 D4         [24] 7080 	mov	dptr,#_axradio_cb_receive
                                   7081 ;	COMMON\easyax5043.c:1241: break;
      003203 02 6C C3         [24] 7082 	ljmp	_wtimer_add_callback
                                   7083 ;	COMMON\easyax5043.c:1243: case syncstate_slave_rxidle:
      003206                       7084 00157$:
                                   7085 ;	COMMON\easyax5043.c:1244: AX5043_PWRMODE = AX5043_PWRSTATE_XTAL_ON;
      003206 90 40 02         [24] 7086 	mov	dptr,#_AX5043_PWRMODE
      003209 74 05            [12] 7087 	mov	a,#0x05
      00320B F0               [24] 7088 	movx	@dptr,a
                                   7089 ;	COMMON\easyax5043.c:1245: axradio_syncstate = syncstate_slave_rxxosc;
      00320C 90 00 A6         [24] 7090 	mov	dptr,#_axradio_syncstate
      00320F 74 09            [12] 7091 	mov	a,#0x09
      003211 F0               [24] 7092 	movx	@dptr,a
                                   7093 ;	COMMON\easyax5043.c:1246: wtimer_remove(&axradio_timer);
      003212 90 03 2B         [24] 7094 	mov	dptr,#_axradio_timer
      003215 12 76 94         [24] 7095 	lcall	_wtimer_remove
                                   7096 ;	COMMON\easyax5043.c:1247: axradio_timer.time += axradio_sync_xoscstartup;
      003218 90 03 2F         [24] 7097 	mov	dptr,#(_axradio_timer + 0x0004)
      00321B E0               [24] 7098 	movx	a,@dptr
      00321C FC               [12] 7099 	mov	r4,a
      00321D A3               [24] 7100 	inc	dptr
      00321E E0               [24] 7101 	movx	a,@dptr
      00321F FD               [12] 7102 	mov	r5,a
      003220 A3               [24] 7103 	inc	dptr
      003221 E0               [24] 7104 	movx	a,@dptr
      003222 FE               [12] 7105 	mov	r6,a
      003223 A3               [24] 7106 	inc	dptr
      003224 E0               [24] 7107 	movx	a,@dptr
      003225 FF               [12] 7108 	mov	r7,a
      003226 90 80 0B         [24] 7109 	mov	dptr,#_axradio_sync_xoscstartup
      003229 E4               [12] 7110 	clr	a
      00322A 93               [24] 7111 	movc	a,@a+dptr
      00322B F8               [12] 7112 	mov	r0,a
      00322C 74 01            [12] 7113 	mov	a,#0x01
      00322E 93               [24] 7114 	movc	a,@a+dptr
      00322F F9               [12] 7115 	mov	r1,a
      003230 74 02            [12] 7116 	mov	a,#0x02
      003232 93               [24] 7117 	movc	a,@a+dptr
      003233 FA               [12] 7118 	mov	r2,a
      003234 74 03            [12] 7119 	mov	a,#0x03
      003236 93               [24] 7120 	movc	a,@a+dptr
      003237 FB               [12] 7121 	mov	r3,a
      003238 E8               [12] 7122 	mov	a,r0
      003239 2C               [12] 7123 	add	a,r4
      00323A FC               [12] 7124 	mov	r4,a
      00323B E9               [12] 7125 	mov	a,r1
      00323C 3D               [12] 7126 	addc	a,r5
      00323D FD               [12] 7127 	mov	r5,a
      00323E EA               [12] 7128 	mov	a,r2
      00323F 3E               [12] 7129 	addc	a,r6
      003240 FE               [12] 7130 	mov	r6,a
      003241 EB               [12] 7131 	mov	a,r3
      003242 3F               [12] 7132 	addc	a,r7
      003243 FF               [12] 7133 	mov	r7,a
      003244 90 03 2F         [24] 7134 	mov	dptr,#(_axradio_timer + 0x0004)
      003247 EC               [12] 7135 	mov	a,r4
      003248 F0               [24] 7136 	movx	@dptr,a
      003249 ED               [12] 7137 	mov	a,r5
      00324A A3               [24] 7138 	inc	dptr
      00324B F0               [24] 7139 	movx	@dptr,a
      00324C EE               [12] 7140 	mov	a,r6
      00324D A3               [24] 7141 	inc	dptr
      00324E F0               [24] 7142 	movx	@dptr,a
      00324F EF               [12] 7143 	mov	a,r7
      003250 A3               [24] 7144 	inc	dptr
      003251 F0               [24] 7145 	movx	@dptr,a
                                   7146 ;	COMMON\easyax5043.c:1248: wtimer0_addabsolute(&axradio_timer);
      003252 90 03 2B         [24] 7147 	mov	dptr,#_axradio_timer
                                   7148 ;	COMMON\easyax5043.c:1249: break;
      003255 02 6D B9         [24] 7149 	ljmp	_wtimer0_addabsolute
                                   7150 ;	COMMON\easyax5043.c:1251: case syncstate_slave_rxxosc:
      003258                       7151 00158$:
                                   7152 ;	COMMON\easyax5043.c:1252: ax5043_receiver_on_continuous();
      003258 12 27 64         [24] 7153 	lcall	_ax5043_receiver_on_continuous
                                   7154 ;	COMMON\easyax5043.c:1253: axradio_syncstate = syncstate_slave_rxsfdwindow;
      00325B 90 00 A6         [24] 7155 	mov	dptr,#_axradio_syncstate
      00325E 74 0A            [12] 7156 	mov	a,#0x0a
      003260 F0               [24] 7157 	movx	@dptr,a
                                   7158 ;	COMMON\easyax5043.c:1254: update_timeanchor();
      003261 12 1B 43         [24] 7159 	lcall	_update_timeanchor
                                   7160 ;	COMMON\easyax5043.c:1255: wtimer_remove_callback(&axradio_cb_receive.cb);
      003264 90 02 D4         [24] 7161 	mov	dptr,#_axradio_cb_receive
      003267 12 78 A9         [24] 7162 	lcall	_wtimer_remove_callback
                                   7163 ;	COMMON\easyax5043.c:1256: memset_xdata(&axradio_cb_receive.st, 0, sizeof(axradio_cb_receive.st));
      00326A 90 03 C6         [24] 7164 	mov	dptr,#_memset_PARM_2
      00326D E4               [12] 7165 	clr	a
      00326E F0               [24] 7166 	movx	@dptr,a
      00326F 90 03 C7         [24] 7167 	mov	dptr,#_memset_PARM_3
      003272 74 1E            [12] 7168 	mov	a,#0x1e
      003274 F0               [24] 7169 	movx	@dptr,a
      003275 E4               [12] 7170 	clr	a
      003276 A3               [24] 7171 	inc	dptr
      003277 F0               [24] 7172 	movx	@dptr,a
      003278 90 02 D8         [24] 7173 	mov	dptr,#(_axradio_cb_receive + 0x0004)
      00327B 75 F0 00         [24] 7174 	mov	b,#0x00
      00327E 12 69 59         [24] 7175 	lcall	_memset
                                   7176 ;	COMMON\easyax5043.c:1257: axradio_cb_receive.st.time.t = axradio_timeanchor.radiotimer;
      003281 90 00 BC         [24] 7177 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      003284 E0               [24] 7178 	movx	a,@dptr
      003285 FC               [12] 7179 	mov	r4,a
      003286 A3               [24] 7180 	inc	dptr
      003287 E0               [24] 7181 	movx	a,@dptr
      003288 FD               [12] 7182 	mov	r5,a
      003289 A3               [24] 7183 	inc	dptr
      00328A E0               [24] 7184 	movx	a,@dptr
      00328B FE               [12] 7185 	mov	r6,a
      00328C A3               [24] 7186 	inc	dptr
      00328D E0               [24] 7187 	movx	a,@dptr
      00328E FF               [12] 7188 	mov	r7,a
      00328F 90 02 DA         [24] 7189 	mov	dptr,#(_axradio_cb_receive + 0x0006)
      003292 EC               [12] 7190 	mov	a,r4
      003293 F0               [24] 7191 	movx	@dptr,a
      003294 ED               [12] 7192 	mov	a,r5
      003295 A3               [24] 7193 	inc	dptr
      003296 F0               [24] 7194 	movx	@dptr,a
      003297 EE               [12] 7195 	mov	a,r6
      003298 A3               [24] 7196 	inc	dptr
      003299 F0               [24] 7197 	movx	@dptr,a
      00329A EF               [12] 7198 	mov	a,r7
      00329B A3               [24] 7199 	inc	dptr
      00329C F0               [24] 7200 	movx	@dptr,a
                                   7201 ;	COMMON\easyax5043.c:1258: axradio_cb_receive.st.error = AXRADIO_ERR_RECEIVESTART;
      00329D 90 02 D9         [24] 7202 	mov	dptr,#(_axradio_cb_receive + 0x0005)
      0032A0 74 0B            [12] 7203 	mov	a,#0x0b
      0032A2 F0               [24] 7204 	movx	@dptr,a
                                   7205 ;	COMMON\easyax5043.c:1259: wtimer_add_callback(&axradio_cb_receive.cb);
      0032A3 90 02 D4         [24] 7206 	mov	dptr,#_axradio_cb_receive
      0032A6 12 6C C3         [24] 7207 	lcall	_wtimer_add_callback
                                   7208 ;	COMMON\easyax5043.c:1260: wtimer_remove(&axradio_timer);
      0032A9 90 03 2B         [24] 7209 	mov	dptr,#_axradio_timer
      0032AC 12 76 94         [24] 7210 	lcall	_wtimer_remove
                                   7211 ;	COMMON\easyax5043.c:1262: uint8_t __autodata idx = axradio_sync_seqnr;
      0032AF 90 00 B1         [24] 7212 	mov	dptr,#_axradio_ack_seqnr
      0032B2 E0               [24] 7213 	movx	a,@dptr
      0032B3 FF               [12] 7214 	mov	r7,a
                                   7215 ;	COMMON\easyax5043.c:1263: if (idx >= axradio_sync_slave_nrrx)
      0032B4 90 80 1E         [24] 7216 	mov	dptr,#_axradio_sync_slave_nrrx
      0032B7 E4               [12] 7217 	clr	a
      0032B8 93               [24] 7218 	movc	a,@a+dptr
      0032B9 FE               [12] 7219 	mov	r6,a
      0032BA C3               [12] 7220 	clr	c
      0032BB EF               [12] 7221 	mov	a,r7
      0032BC 9E               [12] 7222 	subb	a,r6
      0032BD 40 03            [24] 7223 	jc	00160$
                                   7224 ;	COMMON\easyax5043.c:1264: idx = axradio_sync_slave_nrrx - 1;
      0032BF EE               [12] 7225 	mov	a,r6
      0032C0 14               [12] 7226 	dec	a
      0032C1 FF               [12] 7227 	mov	r7,a
      0032C2                       7228 00160$:
                                   7229 ;	COMMON\easyax5043.c:1265: axradio_timer.time += axradio_sync_slave_rxwindow[idx];
      0032C2 90 03 2F         [24] 7230 	mov	dptr,#(_axradio_timer + 0x0004)
      0032C5 E0               [24] 7231 	movx	a,@dptr
      0032C6 FB               [12] 7232 	mov	r3,a
      0032C7 A3               [24] 7233 	inc	dptr
      0032C8 E0               [24] 7234 	movx	a,@dptr
      0032C9 FC               [12] 7235 	mov	r4,a
      0032CA A3               [24] 7236 	inc	dptr
      0032CB E0               [24] 7237 	movx	a,@dptr
      0032CC FD               [12] 7238 	mov	r5,a
      0032CD A3               [24] 7239 	inc	dptr
      0032CE E0               [24] 7240 	movx	a,@dptr
      0032CF FE               [12] 7241 	mov	r6,a
      0032D0 EF               [12] 7242 	mov	a,r7
      0032D1 75 F0 04         [24] 7243 	mov	b,#0x04
      0032D4 A4               [48] 7244 	mul	ab
      0032D5 24 2B            [12] 7245 	add	a,#_axradio_sync_slave_rxwindow
      0032D7 F5 82            [12] 7246 	mov	dpl,a
      0032D9 74 80            [12] 7247 	mov	a,#(_axradio_sync_slave_rxwindow >> 8)
      0032DB 35 F0            [12] 7248 	addc	a,b
      0032DD F5 83            [12] 7249 	mov	dph,a
      0032DF E4               [12] 7250 	clr	a
      0032E0 93               [24] 7251 	movc	a,@a+dptr
      0032E1 F8               [12] 7252 	mov	r0,a
      0032E2 A3               [24] 7253 	inc	dptr
      0032E3 E4               [12] 7254 	clr	a
      0032E4 93               [24] 7255 	movc	a,@a+dptr
      0032E5 F9               [12] 7256 	mov	r1,a
      0032E6 A3               [24] 7257 	inc	dptr
      0032E7 E4               [12] 7258 	clr	a
      0032E8 93               [24] 7259 	movc	a,@a+dptr
      0032E9 FA               [12] 7260 	mov	r2,a
      0032EA A3               [24] 7261 	inc	dptr
      0032EB E4               [12] 7262 	clr	a
      0032EC 93               [24] 7263 	movc	a,@a+dptr
      0032ED FF               [12] 7264 	mov	r7,a
      0032EE E8               [12] 7265 	mov	a,r0
      0032EF 2B               [12] 7266 	add	a,r3
      0032F0 FB               [12] 7267 	mov	r3,a
      0032F1 E9               [12] 7268 	mov	a,r1
      0032F2 3C               [12] 7269 	addc	a,r4
      0032F3 FC               [12] 7270 	mov	r4,a
      0032F4 EA               [12] 7271 	mov	a,r2
      0032F5 3D               [12] 7272 	addc	a,r5
      0032F6 FD               [12] 7273 	mov	r5,a
      0032F7 EF               [12] 7274 	mov	a,r7
      0032F8 3E               [12] 7275 	addc	a,r6
      0032F9 FE               [12] 7276 	mov	r6,a
      0032FA 90 03 2F         [24] 7277 	mov	dptr,#(_axradio_timer + 0x0004)
      0032FD EB               [12] 7278 	mov	a,r3
      0032FE F0               [24] 7279 	movx	@dptr,a
      0032FF EC               [12] 7280 	mov	a,r4
      003300 A3               [24] 7281 	inc	dptr
      003301 F0               [24] 7282 	movx	@dptr,a
      003302 ED               [12] 7283 	mov	a,r5
      003303 A3               [24] 7284 	inc	dptr
      003304 F0               [24] 7285 	movx	@dptr,a
      003305 EE               [12] 7286 	mov	a,r6
      003306 A3               [24] 7287 	inc	dptr
      003307 F0               [24] 7288 	movx	@dptr,a
                                   7289 ;	COMMON\easyax5043.c:1267: wtimer0_addabsolute(&axradio_timer);
      003308 90 03 2B         [24] 7290 	mov	dptr,#_axradio_timer
                                   7291 ;	COMMON\easyax5043.c:1268: break;
      00330B 02 6D B9         [24] 7292 	ljmp	_wtimer0_addabsolute
                                   7293 ;	COMMON\easyax5043.c:1270: case syncstate_slave_rxsfdwindow:
      00330E                       7294 00161$:
                                   7295 ;	COMMON\easyax5043.c:1272: uint8_t __autodata rs = AX5043_RADIOSTATE;
      00330E 90 40 1C         [24] 7296 	mov	dptr,#_AX5043_RADIOSTATE
      003311 E0               [24] 7297 	movx	a,@dptr
                                   7298 ;	COMMON\easyax5043.c:1273: if( !rs )
      003312 FF               [12] 7299 	mov	r7,a
      003313 FE               [12] 7300 	mov	r6,a
      003314 70 01            [24] 7301 	jnz	00293$
      003316 22               [24] 7302 	ret
      003317                       7303 00293$:
                                   7304 ;	COMMON\easyax5043.c:1276: if (!(0x0F & (uint8_t)~rs)) {
      003317 EE               [12] 7305 	mov	a,r6
      003318 F4               [12] 7306 	cpl	a
      003319 FE               [12] 7307 	mov	r6,a
      00331A 54 0F            [12] 7308 	anl	a,#0x0f
      00331C 60 02            [24] 7309 	jz	00295$
      00331E 80 4C            [24] 7310 	sjmp	00166$
      003320                       7311 00295$:
                                   7312 ;	COMMON\easyax5043.c:1277: axradio_syncstate = syncstate_slave_rxpacket;
      003320 90 00 A6         [24] 7313 	mov	dptr,#_axradio_syncstate
      003323 74 0B            [12] 7314 	mov	a,#0x0b
      003325 F0               [24] 7315 	movx	@dptr,a
                                   7316 ;	COMMON\easyax5043.c:1278: wtimer_remove(&axradio_timer);
      003326 90 03 2B         [24] 7317 	mov	dptr,#_axradio_timer
      003329 12 76 94         [24] 7318 	lcall	_wtimer_remove
                                   7319 ;	COMMON\easyax5043.c:1279: axradio_timer.time += axradio_sync_slave_rxtimeout;
      00332C 90 03 2F         [24] 7320 	mov	dptr,#(_axradio_timer + 0x0004)
      00332F E0               [24] 7321 	movx	a,@dptr
      003330 FC               [12] 7322 	mov	r4,a
      003331 A3               [24] 7323 	inc	dptr
      003332 E0               [24] 7324 	movx	a,@dptr
      003333 FD               [12] 7325 	mov	r5,a
      003334 A3               [24] 7326 	inc	dptr
      003335 E0               [24] 7327 	movx	a,@dptr
      003336 FE               [12] 7328 	mov	r6,a
      003337 A3               [24] 7329 	inc	dptr
      003338 E0               [24] 7330 	movx	a,@dptr
      003339 FF               [12] 7331 	mov	r7,a
      00333A 90 80 37         [24] 7332 	mov	dptr,#_axradio_sync_slave_rxtimeout
      00333D E4               [12] 7333 	clr	a
      00333E 93               [24] 7334 	movc	a,@a+dptr
      00333F F8               [12] 7335 	mov	r0,a
      003340 74 01            [12] 7336 	mov	a,#0x01
      003342 93               [24] 7337 	movc	a,@a+dptr
      003343 F9               [12] 7338 	mov	r1,a
      003344 74 02            [12] 7339 	mov	a,#0x02
      003346 93               [24] 7340 	movc	a,@a+dptr
      003347 FA               [12] 7341 	mov	r2,a
      003348 74 03            [12] 7342 	mov	a,#0x03
      00334A 93               [24] 7343 	movc	a,@a+dptr
      00334B FB               [12] 7344 	mov	r3,a
      00334C E8               [12] 7345 	mov	a,r0
      00334D 2C               [12] 7346 	add	a,r4
      00334E FC               [12] 7347 	mov	r4,a
      00334F E9               [12] 7348 	mov	a,r1
      003350 3D               [12] 7349 	addc	a,r5
      003351 FD               [12] 7350 	mov	r5,a
      003352 EA               [12] 7351 	mov	a,r2
      003353 3E               [12] 7352 	addc	a,r6
      003354 FE               [12] 7353 	mov	r6,a
      003355 EB               [12] 7354 	mov	a,r3
      003356 3F               [12] 7355 	addc	a,r7
      003357 FF               [12] 7356 	mov	r7,a
      003358 90 03 2F         [24] 7357 	mov	dptr,#(_axradio_timer + 0x0004)
      00335B EC               [12] 7358 	mov	a,r4
      00335C F0               [24] 7359 	movx	@dptr,a
      00335D ED               [12] 7360 	mov	a,r5
      00335E A3               [24] 7361 	inc	dptr
      00335F F0               [24] 7362 	movx	@dptr,a
      003360 EE               [12] 7363 	mov	a,r6
      003361 A3               [24] 7364 	inc	dptr
      003362 F0               [24] 7365 	movx	@dptr,a
      003363 EF               [12] 7366 	mov	a,r7
      003364 A3               [24] 7367 	inc	dptr
      003365 F0               [24] 7368 	movx	@dptr,a
                                   7369 ;	COMMON\easyax5043.c:1280: wtimer0_addabsolute(&axradio_timer);
      003366 90 03 2B         [24] 7370 	mov	dptr,#_axradio_timer
                                   7371 ;	COMMON\easyax5043.c:1281: break;
      003369 02 6D B9         [24] 7372 	ljmp	_wtimer0_addabsolute
                                   7373 ;	COMMON\easyax5043.c:1286: case syncstate_slave_rxpacket:
      00336C                       7374 00166$:
                                   7375 ;	COMMON\easyax5043.c:1287: ax5043_off();
      00336C 12 28 BB         [24] 7376 	lcall	_ax5043_off
                                   7377 ;	COMMON\easyax5043.c:1288: if (!axradio_sync_seqnr)
      00336F 90 00 B1         [24] 7378 	mov	dptr,#_axradio_ack_seqnr
      003372 E0               [24] 7379 	movx	a,@dptr
      003373 70 06            [24] 7380 	jnz	00168$
                                   7381 ;	COMMON\easyax5043.c:1289: axradio_sync_seqnr = 1;
      003375 90 00 B1         [24] 7382 	mov	dptr,#_axradio_ack_seqnr
      003378 74 01            [12] 7383 	mov	a,#0x01
      00337A F0               [24] 7384 	movx	@dptr,a
      00337B                       7385 00168$:
                                   7386 ;	COMMON\easyax5043.c:1290: ++axradio_sync_seqnr;
      00337B 90 00 B1         [24] 7387 	mov	dptr,#_axradio_ack_seqnr
      00337E E0               [24] 7388 	movx	a,@dptr
      00337F 24 01            [12] 7389 	add	a,#0x01
      003381 F0               [24] 7390 	movx	@dptr,a
                                   7391 ;	COMMON\easyax5043.c:1291: update_timeanchor();
      003382 12 1B 43         [24] 7392 	lcall	_update_timeanchor
                                   7393 ;	COMMON\easyax5043.c:1292: wtimer_remove_callback(&axradio_cb_receive.cb);
      003385 90 02 D4         [24] 7394 	mov	dptr,#_axradio_cb_receive
      003388 12 78 A9         [24] 7395 	lcall	_wtimer_remove_callback
                                   7396 ;	COMMON\easyax5043.c:1293: memset_xdata(&axradio_cb_receive.st, 0, sizeof(axradio_cb_receive.st));
      00338B 90 03 C6         [24] 7397 	mov	dptr,#_memset_PARM_2
      00338E E4               [12] 7398 	clr	a
      00338F F0               [24] 7399 	movx	@dptr,a
      003390 90 03 C7         [24] 7400 	mov	dptr,#_memset_PARM_3
      003393 74 1E            [12] 7401 	mov	a,#0x1e
      003395 F0               [24] 7402 	movx	@dptr,a
      003396 E4               [12] 7403 	clr	a
      003397 A3               [24] 7404 	inc	dptr
      003398 F0               [24] 7405 	movx	@dptr,a
      003399 90 02 D8         [24] 7406 	mov	dptr,#(_axradio_cb_receive + 0x0004)
      00339C 75 F0 00         [24] 7407 	mov	b,#0x00
      00339F 12 69 59         [24] 7408 	lcall	_memset
                                   7409 ;	COMMON\easyax5043.c:1294: axradio_cb_receive.st.time.t = axradio_timeanchor.radiotimer;
      0033A2 90 00 BC         [24] 7410 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      0033A5 E0               [24] 7411 	movx	a,@dptr
      0033A6 FC               [12] 7412 	mov	r4,a
      0033A7 A3               [24] 7413 	inc	dptr
      0033A8 E0               [24] 7414 	movx	a,@dptr
      0033A9 FD               [12] 7415 	mov	r5,a
      0033AA A3               [24] 7416 	inc	dptr
      0033AB E0               [24] 7417 	movx	a,@dptr
      0033AC FE               [12] 7418 	mov	r6,a
      0033AD A3               [24] 7419 	inc	dptr
      0033AE E0               [24] 7420 	movx	a,@dptr
      0033AF FF               [12] 7421 	mov	r7,a
      0033B0 90 02 DA         [24] 7422 	mov	dptr,#(_axradio_cb_receive + 0x0006)
      0033B3 EC               [12] 7423 	mov	a,r4
      0033B4 F0               [24] 7424 	movx	@dptr,a
      0033B5 ED               [12] 7425 	mov	a,r5
      0033B6 A3               [24] 7426 	inc	dptr
      0033B7 F0               [24] 7427 	movx	@dptr,a
      0033B8 EE               [12] 7428 	mov	a,r6
      0033B9 A3               [24] 7429 	inc	dptr
      0033BA F0               [24] 7430 	movx	@dptr,a
      0033BB EF               [12] 7431 	mov	a,r7
      0033BC A3               [24] 7432 	inc	dptr
      0033BD F0               [24] 7433 	movx	@dptr,a
                                   7434 ;	COMMON\easyax5043.c:1295: axradio_cb_receive.st.error = AXRADIO_ERR_TIMEOUT;
      0033BE 90 02 D9         [24] 7435 	mov	dptr,#(_axradio_cb_receive + 0x0005)
      0033C1 74 03            [12] 7436 	mov	a,#0x03
      0033C3 F0               [24] 7437 	movx	@dptr,a
                                   7438 ;	COMMON\easyax5043.c:1296: if (axradio_sync_seqnr <= axradio_sync_slave_resyncloss) {
      0033C4 90 00 B1         [24] 7439 	mov	dptr,#_axradio_ack_seqnr
      0033C7 E0               [24] 7440 	movx	a,@dptr
      0033C8 FF               [12] 7441 	mov	r7,a
      0033C9 90 80 1D         [24] 7442 	mov	dptr,#_axradio_sync_slave_resyncloss
      0033CC E4               [12] 7443 	clr	a
      0033CD 93               [24] 7444 	movc	a,@a+dptr
      0033CE FE               [12] 7445 	mov	r6,a
      0033CF C3               [12] 7446 	clr	c
      0033D0 9F               [12] 7447 	subb	a,r7
      0033D1 40 54            [24] 7448 	jc	00172$
                                   7449 ;	COMMON\easyax5043.c:1297: wtimer_add_callback(&axradio_cb_receive.cb);
      0033D3 90 02 D4         [24] 7450 	mov	dptr,#_axradio_cb_receive
      0033D6 12 6C C3         [24] 7451 	lcall	_wtimer_add_callback
                                   7452 ;	COMMON\easyax5043.c:1298: axradio_sync_slave_nextperiod();
      0033D9 12 2C 14         [24] 7453 	lcall	_axradio_sync_slave_nextperiod
                                   7454 ;	COMMON\easyax5043.c:1299: axradio_syncstate = syncstate_slave_rxidle;
      0033DC 90 00 A6         [24] 7455 	mov	dptr,#_axradio_syncstate
      0033DF 74 08            [12] 7456 	mov	a,#0x08
      0033E1 F0               [24] 7457 	movx	@dptr,a
                                   7458 ;	COMMON\easyax5043.c:1300: wtimer_remove(&axradio_timer);
      0033E2 90 03 2B         [24] 7459 	mov	dptr,#_axradio_timer
      0033E5 12 76 94         [24] 7460 	lcall	_wtimer_remove
                                   7461 ;	COMMON\easyax5043.c:1302: uint8_t __autodata idx = axradio_sync_seqnr;
      0033E8 90 00 B1         [24] 7462 	mov	dptr,#_axradio_ack_seqnr
      0033EB E0               [24] 7463 	movx	a,@dptr
      0033EC FF               [12] 7464 	mov	r7,a
                                   7465 ;	COMMON\easyax5043.c:1303: if (idx >= axradio_sync_slave_nrrx)
      0033ED 90 80 1E         [24] 7466 	mov	dptr,#_axradio_sync_slave_nrrx
      0033F0 E4               [12] 7467 	clr	a
      0033F1 93               [24] 7468 	movc	a,@a+dptr
      0033F2 FE               [12] 7469 	mov	r6,a
      0033F3 C3               [12] 7470 	clr	c
      0033F4 EF               [12] 7471 	mov	a,r7
      0033F5 9E               [12] 7472 	subb	a,r6
      0033F6 40 03            [24] 7473 	jc	00170$
                                   7474 ;	COMMON\easyax5043.c:1304: idx = axradio_sync_slave_nrrx - 1;
      0033F8 EE               [12] 7475 	mov	a,r6
      0033F9 14               [12] 7476 	dec	a
      0033FA FF               [12] 7477 	mov	r7,a
      0033FB                       7478 00170$:
                                   7479 ;	COMMON\easyax5043.c:1305: axradio_sync_settimeradv(axradio_sync_slave_rxadvance[idx]);
      0033FB EF               [12] 7480 	mov	a,r7
      0033FC 75 F0 04         [24] 7481 	mov	b,#0x04
      0033FF A4               [48] 7482 	mul	ab
      003400 24 1F            [12] 7483 	add	a,#_axradio_sync_slave_rxadvance
      003402 F5 82            [12] 7484 	mov	dpl,a
      003404 74 80            [12] 7485 	mov	a,#(_axradio_sync_slave_rxadvance >> 8)
      003406 35 F0            [12] 7486 	addc	a,b
      003408 F5 83            [12] 7487 	mov	dph,a
      00340A E4               [12] 7488 	clr	a
      00340B 93               [24] 7489 	movc	a,@a+dptr
      00340C FC               [12] 7490 	mov	r4,a
      00340D A3               [24] 7491 	inc	dptr
      00340E E4               [12] 7492 	clr	a
      00340F 93               [24] 7493 	movc	a,@a+dptr
      003410 FD               [12] 7494 	mov	r5,a
      003411 A3               [24] 7495 	inc	dptr
      003412 E4               [12] 7496 	clr	a
      003413 93               [24] 7497 	movc	a,@a+dptr
      003414 FE               [12] 7498 	mov	r6,a
      003415 A3               [24] 7499 	inc	dptr
      003416 E4               [12] 7500 	clr	a
      003417 93               [24] 7501 	movc	a,@a+dptr
      003418 8C 82            [24] 7502 	mov	dpl,r4
      00341A 8D 83            [24] 7503 	mov	dph,r5
      00341C 8E F0            [24] 7504 	mov	b,r6
      00341E 12 2A AE         [24] 7505 	lcall	_axradio_sync_settimeradv
                                   7506 ;	COMMON\easyax5043.c:1307: wtimer0_addabsolute(&axradio_timer);
      003421 90 03 2B         [24] 7507 	mov	dptr,#_axradio_timer
                                   7508 ;	COMMON\easyax5043.c:1308: break;
      003424 02 6D B9         [24] 7509 	ljmp	_wtimer0_addabsolute
      003427                       7510 00172$:
                                   7511 ;	COMMON\easyax5043.c:1310: axradio_cb_receive.st.error = AXRADIO_ERR_RESYNC;
      003427 90 02 D9         [24] 7512 	mov	dptr,#(_axradio_cb_receive + 0x0005)
      00342A 74 09            [12] 7513 	mov	a,#0x09
      00342C F0               [24] 7514 	movx	@dptr,a
                                   7515 ;	COMMON\easyax5043.c:1311: wtimer_add_callback(&axradio_cb_receive.cb);
      00342D 90 02 D4         [24] 7516 	mov	dptr,#_axradio_cb_receive
      003430 12 6C C3         [24] 7517 	lcall	_wtimer_add_callback
                                   7518 ;	COMMON\easyax5043.c:1312: ax5043_receiver_on_continuous();
      003433 12 27 64         [24] 7519 	lcall	_ax5043_receiver_on_continuous
                                   7520 ;	COMMON\easyax5043.c:1313: axradio_syncstate = syncstate_slave_synchunt;
      003436 90 00 A6         [24] 7521 	mov	dptr,#_axradio_syncstate
      003439 74 06            [12] 7522 	mov	a,#0x06
      00343B F0               [24] 7523 	movx	@dptr,a
                                   7524 ;	COMMON\easyax5043.c:1314: wtimer_remove(&axradio_timer);
      00343C 90 03 2B         [24] 7525 	mov	dptr,#_axradio_timer
      00343F 12 76 94         [24] 7526 	lcall	_wtimer_remove
                                   7527 ;	COMMON\easyax5043.c:1315: axradio_timer.time = axradio_sync_slave_syncwindow;
      003442 90 80 0F         [24] 7528 	mov	dptr,#_axradio_sync_slave_syncwindow
      003445 E4               [12] 7529 	clr	a
      003446 93               [24] 7530 	movc	a,@a+dptr
      003447 FC               [12] 7531 	mov	r4,a
      003448 74 01            [12] 7532 	mov	a,#0x01
      00344A 93               [24] 7533 	movc	a,@a+dptr
      00344B FD               [12] 7534 	mov	r5,a
      00344C 74 02            [12] 7535 	mov	a,#0x02
      00344E 93               [24] 7536 	movc	a,@a+dptr
      00344F FE               [12] 7537 	mov	r6,a
      003450 74 03            [12] 7538 	mov	a,#0x03
      003452 93               [24] 7539 	movc	a,@a+dptr
      003453 FF               [12] 7540 	mov	r7,a
      003454 90 03 2F         [24] 7541 	mov	dptr,#(_axradio_timer + 0x0004)
      003457 EC               [12] 7542 	mov	a,r4
      003458 F0               [24] 7543 	movx	@dptr,a
      003459 ED               [12] 7544 	mov	a,r5
      00345A A3               [24] 7545 	inc	dptr
      00345B F0               [24] 7546 	movx	@dptr,a
      00345C EE               [12] 7547 	mov	a,r6
      00345D A3               [24] 7548 	inc	dptr
      00345E F0               [24] 7549 	movx	@dptr,a
      00345F EF               [12] 7550 	mov	a,r7
      003460 A3               [24] 7551 	inc	dptr
      003461 F0               [24] 7552 	movx	@dptr,a
                                   7553 ;	COMMON\easyax5043.c:1316: wtimer0_addrelative(&axradio_timer);
      003462 90 03 2B         [24] 7554 	mov	dptr,#_axradio_timer
      003465 12 6C DD         [24] 7555 	lcall	_wtimer0_addrelative
                                   7556 ;	COMMON\easyax5043.c:1317: axradio_sync_time = axradio_timer.time;
      003468 90 03 2F         [24] 7557 	mov	dptr,#(_axradio_timer + 0x0004)
      00346B E0               [24] 7558 	movx	a,@dptr
      00346C FC               [12] 7559 	mov	r4,a
      00346D A3               [24] 7560 	inc	dptr
      00346E E0               [24] 7561 	movx	a,@dptr
      00346F FD               [12] 7562 	mov	r5,a
      003470 A3               [24] 7563 	inc	dptr
      003471 E0               [24] 7564 	movx	a,@dptr
      003472 FE               [12] 7565 	mov	r6,a
      003473 A3               [24] 7566 	inc	dptr
      003474 E0               [24] 7567 	movx	a,@dptr
      003475 FF               [12] 7568 	mov	r7,a
      003476 90 00 B2         [24] 7569 	mov	dptr,#_axradio_sync_time
      003479 EC               [12] 7570 	mov	a,r4
      00347A F0               [24] 7571 	movx	@dptr,a
      00347B ED               [12] 7572 	mov	a,r5
      00347C A3               [24] 7573 	inc	dptr
      00347D F0               [24] 7574 	movx	@dptr,a
      00347E EE               [12] 7575 	mov	a,r6
      00347F A3               [24] 7576 	inc	dptr
      003480 F0               [24] 7577 	movx	@dptr,a
      003481 EF               [12] 7578 	mov	a,r7
      003482 A3               [24] 7579 	inc	dptr
      003483 F0               [24] 7580 	movx	@dptr,a
                                   7581 ;	COMMON\easyax5043.c:1318: break;
                                   7582 ;	COMMON\easyax5043.c:1320: case syncstate_slave_rxack:
      003484 22               [24] 7583 	ret
      003485                       7584 00173$:
                                   7585 ;	COMMON\easyax5043.c:1321: axradio_syncstate = syncstate_slave_rxidle;
      003485 90 00 A6         [24] 7586 	mov	dptr,#_axradio_syncstate
      003488 74 08            [12] 7587 	mov	a,#0x08
      00348A F0               [24] 7588 	movx	@dptr,a
                                   7589 ;	COMMON\easyax5043.c:1322: wtimer_remove(&axradio_timer);
      00348B 90 03 2B         [24] 7590 	mov	dptr,#_axradio_timer
      00348E 12 76 94         [24] 7591 	lcall	_wtimer_remove
                                   7592 ;	COMMON\easyax5043.c:1323: axradio_sync_settimeradv(axradio_sync_slave_rxadvance[1]);
      003491 90 80 23         [24] 7593 	mov	dptr,#(_axradio_sync_slave_rxadvance + 0x0004)
      003494 E4               [12] 7594 	clr	a
      003495 93               [24] 7595 	movc	a,@a+dptr
      003496 FC               [12] 7596 	mov	r4,a
      003497 A3               [24] 7597 	inc	dptr
      003498 E4               [12] 7598 	clr	a
      003499 93               [24] 7599 	movc	a,@a+dptr
      00349A FD               [12] 7600 	mov	r5,a
      00349B A3               [24] 7601 	inc	dptr
      00349C E4               [12] 7602 	clr	a
      00349D 93               [24] 7603 	movc	a,@a+dptr
      00349E FE               [12] 7604 	mov	r6,a
      00349F A3               [24] 7605 	inc	dptr
      0034A0 E4               [12] 7606 	clr	a
      0034A1 93               [24] 7607 	movc	a,@a+dptr
      0034A2 8C 82            [24] 7608 	mov	dpl,r4
      0034A4 8D 83            [24] 7609 	mov	dph,r5
      0034A6 8E F0            [24] 7610 	mov	b,r6
      0034A8 12 2A AE         [24] 7611 	lcall	_axradio_sync_settimeradv
                                   7612 ;	COMMON\easyax5043.c:1324: wtimer0_addabsolute(&axradio_timer);
      0034AB 90 03 2B         [24] 7613 	mov	dptr,#_axradio_timer
      0034AE 12 6D B9         [24] 7614 	lcall	_wtimer0_addabsolute
                                   7615 ;	COMMON\easyax5043.c:1325: goto transmitack;
      0034B1 02 2E 86         [24] 7616 	ljmp	00133$
                                   7617 ;	COMMON\easyax5043.c:1329: default:
      0034B4                       7618 00175$:
                                   7619 ;	COMMON\easyax5043.c:1331: }
      0034B4 22               [24] 7620 	ret
                                   7621 ;------------------------------------------------------------
                                   7622 ;Allocation info for local variables in function 'axradio_callback_fwd'
                                   7623 ;------------------------------------------------------------
                                   7624 ;desc                      Allocated to registers r6 r7 
                                   7625 ;------------------------------------------------------------
                                   7626 ;	COMMON\easyax5043.c:1334: static __reentrantb void axradio_callback_fwd(struct wtimer_callback __xdata *desc) __reentrant
                                   7627 ;	-----------------------------------------
                                   7628 ;	 function axradio_callback_fwd
                                   7629 ;	-----------------------------------------
      0034B5                       7630 _axradio_callback_fwd:
      0034B5 AE 82            [24] 7631 	mov	r6,dpl
      0034B7 AF 83            [24] 7632 	mov	r7,dph
                                   7633 ;	COMMON\easyax5043.c:1336: axradio_statuschange((struct axradio_status __xdata *)(desc + 1));
      0034B9 74 04            [12] 7634 	mov	a,#0x04
      0034BB 2E               [12] 7635 	add	a,r6
      0034BC FE               [12] 7636 	mov	r6,a
      0034BD E4               [12] 7637 	clr	a
      0034BE 3F               [12] 7638 	addc	a,r7
      0034BF FF               [12] 7639 	mov	r7,a
      0034C0 8E 82            [24] 7640 	mov	dpl,r6
      0034C2 8F 83            [24] 7641 	mov	dph,r7
      0034C4 02 0C 19         [24] 7642 	ljmp	_axradio_statuschange
                                   7643 ;------------------------------------------------------------
                                   7644 ;Allocation info for local variables in function 'axradio_receive_callback_fwd'
                                   7645 ;------------------------------------------------------------
                                   7646 ;len                       Allocated to registers r6 r7 
                                   7647 ;len                       Allocated to registers r6 r7 
                                   7648 ;trxst                     Allocated to registers r6 
                                   7649 ;iesave                    Allocated to registers r7 
                                   7650 ;desc                      Allocated with name '_axradio_receive_callback_fwd_desc_1_344'
                                   7651 ;seqnr                     Allocated with name '_axradio_receive_callback_fwd_seqnr_3_352'
                                   7652 ;len_byte                  Allocated with name '_axradio_receive_callback_fwd_len_byte_3_354'
                                   7653 ;------------------------------------------------------------
                                   7654 ;	COMMON\easyax5043.c:1339: static void axradio_receive_callback_fwd(struct wtimer_callback __xdata *desc)
                                   7655 ;	-----------------------------------------
                                   7656 ;	 function axradio_receive_callback_fwd
                                   7657 ;	-----------------------------------------
      0034C7                       7658 _axradio_receive_callback_fwd:
                                   7659 ;	COMMON\easyax5043.c:1343: if (axradio_cb_receive.st.error != AXRADIO_ERR_NOERROR) {
      0034C7 90 02 D9         [24] 7660 	mov	dptr,#(_axradio_cb_receive + 0x0005)
      0034CA E0               [24] 7661 	movx	a,@dptr
      0034CB 60 06            [24] 7662 	jz	00102$
                                   7663 ;	COMMON\easyax5043.c:1344: axradio_statuschange((struct axradio_status __xdata *)&axradio_cb_receive.st);
      0034CD 90 02 D8         [24] 7664 	mov	dptr,#(_axradio_cb_receive + 0x0004)
                                   7665 ;	COMMON\easyax5043.c:1345: return;
      0034D0 02 0C 19         [24] 7666 	ljmp	_axradio_statuschange
      0034D3                       7667 00102$:
                                   7668 ;	COMMON\easyax5043.c:1347: if (axradio_phy_pn9 && !AXRADIO_MODE_IS_STREAM_RECEIVE(axradio_mode)) {
      0034D3 90 7F CA         [24] 7669 	mov	dptr,#_axradio_phy_pn9
      0034D6 E4               [12] 7670 	clr	a
      0034D7 93               [24] 7671 	movc	a,@a+dptr
      0034D8 60 51            [24] 7672 	jz	00104$
      0034DA 74 F8            [12] 7673 	mov	a,#0xf8
      0034DC 55 0B            [12] 7674 	anl	a,_axradio_mode
      0034DE FF               [12] 7675 	mov	r7,a
      0034DF BF 28 02         [24] 7676 	cjne	r7,#0x28,00282$
      0034E2 80 47            [24] 7677 	sjmp	00104$
      0034E4                       7678 00282$:
                                   7679 ;	COMMON\easyax5043.c:1348: uint16_t __autodata len = axradio_cb_receive.st.rx.pktlen;
      0034E4 90 02 F4         [24] 7680 	mov	dptr,#(_axradio_cb_receive + 0x0020)
      0034E7 E0               [24] 7681 	movx	a,@dptr
      0034E8 FE               [12] 7682 	mov	r6,a
      0034E9 A3               [24] 7683 	inc	dptr
      0034EA E0               [24] 7684 	movx	a,@dptr
      0034EB FF               [12] 7685 	mov	r7,a
                                   7686 ;	COMMON\easyax5043.c:1349: len += axradio_framing_maclen;
      0034EC 90 7F EB         [24] 7687 	mov	dptr,#_axradio_framing_maclen
      0034EF E4               [12] 7688 	clr	a
      0034F0 93               [24] 7689 	movc	a,@a+dptr
      0034F1 7C 00            [12] 7690 	mov	r4,#0x00
      0034F3 2E               [12] 7691 	add	a,r6
      0034F4 FE               [12] 7692 	mov	r6,a
      0034F5 EC               [12] 7693 	mov	a,r4
      0034F6 3F               [12] 7694 	addc	a,r7
      0034F7 FF               [12] 7695 	mov	r7,a
                                   7696 ;	COMMON\easyax5043.c:1350: pn9_buffer((__xdata uint8_t *)axradio_cb_receive.st.rx.mac.raw, len, 0x1ff, -(AX5043_ENCODING & 0x01));
      0034F8 90 40 11         [24] 7697 	mov	dptr,#_AX5043_ENCODING
      0034FB E0               [24] 7698 	movx	a,@dptr
      0034FC FD               [12] 7699 	mov	r5,a
      0034FD 53 05 01         [24] 7700 	anl	ar5,#0x01
      003500 C3               [12] 7701 	clr	c
      003501 E4               [12] 7702 	clr	a
      003502 9D               [12] 7703 	subb	a,r5
      003503 FD               [12] 7704 	mov	r5,a
      003504 90 02 F0         [24] 7705 	mov	dptr,#(_axradio_cb_receive + 0x001c)
      003507 E0               [24] 7706 	movx	a,@dptr
      003508 FB               [12] 7707 	mov	r3,a
      003509 A3               [24] 7708 	inc	dptr
      00350A E0               [24] 7709 	movx	a,@dptr
      00350B FC               [12] 7710 	mov	r4,a
      00350C 7A 00            [12] 7711 	mov	r2,#0x00
      00350E C0 05            [24] 7712 	push	ar5
      003510 74 FF            [12] 7713 	mov	a,#0xff
      003512 C0 E0            [24] 7714 	push	acc
      003514 74 01            [12] 7715 	mov	a,#0x01
      003516 C0 E0            [24] 7716 	push	acc
      003518 C0 06            [24] 7717 	push	ar6
      00351A C0 07            [24] 7718 	push	ar7
      00351C 8B 82            [24] 7719 	mov	dpl,r3
      00351E 8C 83            [24] 7720 	mov	dph,r4
      003520 8A F0            [24] 7721 	mov	b,r2
      003522 12 6E 0C         [24] 7722 	lcall	_pn9_buffer
      003525 E5 81            [12] 7723 	mov	a,sp
      003527 24 FB            [12] 7724 	add	a,#0xfb
      003529 F5 81            [12] 7725 	mov	sp,a
      00352B                       7726 00104$:
                                   7727 ;	COMMON\easyax5043.c:1352: if (axradio_framing_swcrclen && !AXRADIO_MODE_IS_STREAM_RECEIVE(axradio_mode)) {
      00352B 90 7F F2         [24] 7728 	mov	dptr,#_axradio_framing_swcrclen
      00352E E4               [12] 7729 	clr	a
      00352F 93               [24] 7730 	movc	a,@a+dptr
      003530 60 66            [24] 7731 	jz	00109$
      003532 74 F8            [12] 7732 	mov	a,#0xf8
      003534 55 0B            [12] 7733 	anl	a,_axradio_mode
      003536 FF               [12] 7734 	mov	r7,a
      003537 BF 28 02         [24] 7735 	cjne	r7,#0x28,00284$
      00353A 80 5C            [24] 7736 	sjmp	00109$
      00353C                       7737 00284$:
                                   7738 ;	COMMON\easyax5043.c:1353: uint16_t __autodata len = axradio_cb_receive.st.rx.pktlen;
      00353C 90 02 F4         [24] 7739 	mov	dptr,#(_axradio_cb_receive + 0x0020)
      00353F E0               [24] 7740 	movx	a,@dptr
      003540 FE               [12] 7741 	mov	r6,a
      003541 A3               [24] 7742 	inc	dptr
      003542 E0               [24] 7743 	movx	a,@dptr
      003543 FF               [12] 7744 	mov	r7,a
                                   7745 ;	COMMON\easyax5043.c:1354: len += axradio_framing_maclen;
      003544 90 7F EB         [24] 7746 	mov	dptr,#_axradio_framing_maclen
      003547 E4               [12] 7747 	clr	a
      003548 93               [24] 7748 	movc	a,@a+dptr
      003549 7C 00            [12] 7749 	mov	r4,#0x00
      00354B 2E               [12] 7750 	add	a,r6
      00354C FE               [12] 7751 	mov	r6,a
      00354D EC               [12] 7752 	mov	a,r4
      00354E 3F               [12] 7753 	addc	a,r7
      00354F FF               [12] 7754 	mov	r7,a
                                   7755 ;	COMMON\easyax5043.c:1355: len = axradio_framing_check_crc((uint8_t __xdata *)axradio_cb_receive.st.rx.mac.raw, len);
      003550 90 02 F0         [24] 7756 	mov	dptr,#(_axradio_cb_receive + 0x001c)
      003553 E0               [24] 7757 	movx	a,@dptr
      003554 FC               [12] 7758 	mov	r4,a
      003555 A3               [24] 7759 	inc	dptr
      003556 E0               [24] 7760 	movx	a,@dptr
      003557 FD               [12] 7761 	mov	r5,a
      003558 C0 06            [24] 7762 	push	ar6
      00355A C0 07            [24] 7763 	push	ar7
      00355C 8C 82            [24] 7764 	mov	dpl,r4
      00355E 8D 83            [24] 7765 	mov	dph,r5
      003560 12 19 25         [24] 7766 	lcall	_axradio_framing_check_crc
      003563 AE 82            [24] 7767 	mov	r6,dpl
      003565 AF 83            [24] 7768 	mov	r7,dph
      003567 15 81            [12] 7769 	dec	sp
      003569 15 81            [12] 7770 	dec	sp
                                   7771 ;	COMMON\easyax5043.c:1356: if (!len)
      00356B EE               [12] 7772 	mov	a,r6
      00356C 4F               [12] 7773 	orl	a,r7
      00356D 70 03            [24] 7774 	jnz	00285$
      00356F 02 39 A8         [24] 7775 	ljmp	00159$
      003572                       7776 00285$:
                                   7777 ;	COMMON\easyax5043.c:1359: len -= axradio_framing_maclen;
      003572 90 7F EB         [24] 7778 	mov	dptr,#_axradio_framing_maclen
      003575 E4               [12] 7779 	clr	a
      003576 93               [24] 7780 	movc	a,@a+dptr
      003577 FD               [12] 7781 	mov	r5,a
      003578 7C 00            [12] 7782 	mov	r4,#0x00
      00357A EE               [12] 7783 	mov	a,r6
      00357B C3               [12] 7784 	clr	c
      00357C 9D               [12] 7785 	subb	a,r5
      00357D FE               [12] 7786 	mov	r6,a
      00357E EF               [12] 7787 	mov	a,r7
      00357F 9C               [12] 7788 	subb	a,r4
      003580 FF               [12] 7789 	mov	r7,a
                                   7790 ;	COMMON\easyax5043.c:1360: len -= axradio_framing_swcrclen; // drop crc
      003581 90 7F F2         [24] 7791 	mov	dptr,#_axradio_framing_swcrclen
      003584 E4               [12] 7792 	clr	a
      003585 93               [24] 7793 	movc	a,@a+dptr
      003586 FD               [12] 7794 	mov	r5,a
      003587 7C 00            [12] 7795 	mov	r4,#0x00
      003589 EE               [12] 7796 	mov	a,r6
      00358A C3               [12] 7797 	clr	c
      00358B 9D               [12] 7798 	subb	a,r5
      00358C FE               [12] 7799 	mov	r6,a
      00358D EF               [12] 7800 	mov	a,r7
      00358E 9C               [12] 7801 	subb	a,r4
      00358F FF               [12] 7802 	mov	r7,a
                                   7803 ;	COMMON\easyax5043.c:1361: axradio_cb_receive.st.rx.pktlen = len;
      003590 90 02 F4         [24] 7804 	mov	dptr,#(_axradio_cb_receive + 0x0020)
      003593 EE               [12] 7805 	mov	a,r6
      003594 F0               [24] 7806 	movx	@dptr,a
      003595 EF               [12] 7807 	mov	a,r7
      003596 A3               [24] 7808 	inc	dptr
      003597 F0               [24] 7809 	movx	@dptr,a
      003598                       7810 00109$:
                                   7811 ;	COMMON\easyax5043.c:1365: axradio_cb_receive.st.rx.phy.timeoffset = 0;
      003598 90 02 E4         [24] 7812 	mov	dptr,#(_axradio_cb_receive + 0x0010)
      00359B E4               [12] 7813 	clr	a
      00359C F0               [24] 7814 	movx	@dptr,a
      00359D A3               [24] 7815 	inc	dptr
      00359E F0               [24] 7816 	movx	@dptr,a
                                   7817 ;	COMMON\easyax5043.c:1366: axradio_cb_receive.st.rx.phy.period = 0;
      00359F 90 02 E6         [24] 7818 	mov	dptr,#(_axradio_cb_receive + 0x0012)
      0035A2 F0               [24] 7819 	movx	@dptr,a
      0035A3 A3               [24] 7820 	inc	dptr
      0035A4 F0               [24] 7821 	movx	@dptr,a
                                   7822 ;	COMMON\easyax5043.c:1367: if (axradio_mode == AXRADIO_MODE_ACK_TRANSMIT ||
      0035A5 74 12            [12] 7823 	mov	a,#0x12
      0035A7 B5 0B 02         [24] 7824 	cjne	a,_axradio_mode,00286$
      0035AA 80 0C            [24] 7825 	sjmp	00113$
      0035AC                       7826 00286$:
                                   7827 ;	COMMON\easyax5043.c:1368: axradio_mode == AXRADIO_MODE_WOR_ACK_TRANSMIT ||
      0035AC 74 13            [12] 7828 	mov	a,#0x13
      0035AE B5 0B 02         [24] 7829 	cjne	a,_axradio_mode,00287$
      0035B1 80 05            [24] 7830 	sjmp	00113$
      0035B3                       7831 00287$:
                                   7832 ;	COMMON\easyax5043.c:1369: axradio_mode == AXRADIO_MODE_SYNC_ACK_MASTER) {
      0035B3 74 31            [12] 7833 	mov	a,#0x31
      0035B5 B5 0B 65         [24] 7834 	cjne	a,_axradio_mode,00114$
      0035B8                       7835 00113$:
                                   7836 ;	COMMON\easyax5043.c:1370: ax5043_off();
      0035B8 12 28 BB         [24] 7837 	lcall	_ax5043_off
                                   7838 ;	COMMON\easyax5043.c:1371: wtimer_remove(&axradio_timer);
      0035BB 90 03 2B         [24] 7839 	mov	dptr,#_axradio_timer
      0035BE 12 76 94         [24] 7840 	lcall	_wtimer_remove
                                   7841 ;	COMMON\easyax5043.c:1372: if (axradio_mode == AXRADIO_MODE_SYNC_ACK_MASTER) {
      0035C1 74 31            [12] 7842 	mov	a,#0x31
      0035C3 B5 0B 26         [24] 7843 	cjne	a,_axradio_mode,00112$
                                   7844 ;	COMMON\easyax5043.c:1373: axradio_syncstate = syncstate_master_normal;
      0035C6 90 00 A6         [24] 7845 	mov	dptr,#_axradio_syncstate
      0035C9 74 03            [12] 7846 	mov	a,#0x03
      0035CB F0               [24] 7847 	movx	@dptr,a
                                   7848 ;	COMMON\easyax5043.c:1374: axradio_sync_settimeradv(axradio_sync_xoscstartup);
      0035CC 90 80 0B         [24] 7849 	mov	dptr,#_axradio_sync_xoscstartup
      0035CF E4               [12] 7850 	clr	a
      0035D0 93               [24] 7851 	movc	a,@a+dptr
      0035D1 FC               [12] 7852 	mov	r4,a
      0035D2 74 01            [12] 7853 	mov	a,#0x01
      0035D4 93               [24] 7854 	movc	a,@a+dptr
      0035D5 FD               [12] 7855 	mov	r5,a
      0035D6 74 02            [12] 7856 	mov	a,#0x02
      0035D8 93               [24] 7857 	movc	a,@a+dptr
      0035D9 FE               [12] 7858 	mov	r6,a
      0035DA 74 03            [12] 7859 	mov	a,#0x03
      0035DC 93               [24] 7860 	movc	a,@a+dptr
      0035DD 8C 82            [24] 7861 	mov	dpl,r4
      0035DF 8D 83            [24] 7862 	mov	dph,r5
      0035E1 8E F0            [24] 7863 	mov	b,r6
      0035E3 12 2A AE         [24] 7864 	lcall	_axradio_sync_settimeradv
                                   7865 ;	COMMON\easyax5043.c:1375: wtimer0_addabsolute(&axradio_timer);
      0035E6 90 03 2B         [24] 7866 	mov	dptr,#_axradio_timer
      0035E9 12 6D B9         [24] 7867 	lcall	_wtimer0_addabsolute
      0035EC                       7868 00112$:
                                   7869 ;	COMMON\easyax5043.c:1377: wtimer_remove_callback(&axradio_cb_transmitend.cb);
      0035EC 90 03 17         [24] 7870 	mov	dptr,#_axradio_cb_transmitend
      0035EF 12 78 A9         [24] 7871 	lcall	_wtimer_remove_callback
                                   7872 ;	COMMON\easyax5043.c:1378: axradio_cb_transmitend.st.error = AXRADIO_ERR_NOERROR;
      0035F2 90 03 1C         [24] 7873 	mov	dptr,#(_axradio_cb_transmitend + 0x0005)
      0035F5 E4               [12] 7874 	clr	a
      0035F6 F0               [24] 7875 	movx	@dptr,a
                                   7876 ;	COMMON\easyax5043.c:1379: axradio_cb_transmitend.st.time.t = radio_read24((uint16_t)&AX5043_TIMER2);
      0035F7 7E 59            [12] 7877 	mov	r6,#_AX5043_TIMER2
      0035F9 7F 40            [12] 7878 	mov	r7,#(_AX5043_TIMER2 >> 8)
      0035FB 8E 82            [24] 7879 	mov	dpl,r6
      0035FD 8F 83            [24] 7880 	mov	dph,r7
      0035FF 12 6D E5         [24] 7881 	lcall	_radio_read24
      003602 AC 82            [24] 7882 	mov	r4,dpl
      003604 AD 83            [24] 7883 	mov	r5,dph
      003606 AE F0            [24] 7884 	mov	r6,b
      003608 FF               [12] 7885 	mov	r7,a
      003609 90 03 1D         [24] 7886 	mov	dptr,#(_axradio_cb_transmitend + 0x0006)
      00360C EC               [12] 7887 	mov	a,r4
      00360D F0               [24] 7888 	movx	@dptr,a
      00360E ED               [12] 7889 	mov	a,r5
      00360F A3               [24] 7890 	inc	dptr
      003610 F0               [24] 7891 	movx	@dptr,a
      003611 EE               [12] 7892 	mov	a,r6
      003612 A3               [24] 7893 	inc	dptr
      003613 F0               [24] 7894 	movx	@dptr,a
      003614 EF               [12] 7895 	mov	a,r7
      003615 A3               [24] 7896 	inc	dptr
      003616 F0               [24] 7897 	movx	@dptr,a
                                   7898 ;	COMMON\easyax5043.c:1380: wtimer_add_callback(&axradio_cb_transmitend.cb);
      003617 90 03 17         [24] 7899 	mov	dptr,#_axradio_cb_transmitend
      00361A 12 6C C3         [24] 7900 	lcall	_wtimer_add_callback
      00361D                       7901 00114$:
                                   7902 ;	COMMON\easyax5043.c:1382: if (axradio_framing_destaddrpos != 0xff)
      00361D 90 7F ED         [24] 7903 	mov	dptr,#_axradio_framing_destaddrpos
      003620 E4               [12] 7904 	clr	a
      003621 93               [24] 7905 	movc	a,@a+dptr
      003622 FF               [12] 7906 	mov	r7,a
      003623 BF FF 02         [24] 7907 	cjne	r7,#0xff,00292$
      003626 80 34            [24] 7908 	sjmp	00118$
      003628                       7909 00292$:
                                   7910 ;	COMMON\easyax5043.c:1383: memcpy_xdata(&axradio_cb_receive.st.rx.mac.localaddr, &axradio_cb_receive.st.rx.mac.raw[axradio_framing_destaddrpos], axradio_framing_addrlen);
      003628 90 02 F0         [24] 7911 	mov	dptr,#(_axradio_cb_receive + 0x001c)
      00362B E0               [24] 7912 	movx	a,@dptr
      00362C FD               [12] 7913 	mov	r5,a
      00362D A3               [24] 7914 	inc	dptr
      00362E E0               [24] 7915 	movx	a,@dptr
      00362F FE               [12] 7916 	mov	r6,a
      003630 EF               [12] 7917 	mov	a,r7
      003631 2D               [12] 7918 	add	a,r5
      003632 FF               [12] 7919 	mov	r7,a
      003633 E4               [12] 7920 	clr	a
      003634 3E               [12] 7921 	addc	a,r6
      003635 FC               [12] 7922 	mov	r4,a
      003636 7E 00            [12] 7923 	mov	r6,#0x00
      003638 90 7F EC         [24] 7924 	mov	dptr,#_axradio_framing_addrlen
      00363B E4               [12] 7925 	clr	a
      00363C 93               [24] 7926 	movc	a,@a+dptr
      00363D FD               [12] 7927 	mov	r5,a
      00363E 7B 00            [12] 7928 	mov	r3,#0x00
      003640 90 03 DB         [24] 7929 	mov	dptr,#_memcpy_PARM_2
      003643 EF               [12] 7930 	mov	a,r7
      003644 F0               [24] 7931 	movx	@dptr,a
      003645 EC               [12] 7932 	mov	a,r4
      003646 A3               [24] 7933 	inc	dptr
      003647 F0               [24] 7934 	movx	@dptr,a
      003648 EE               [12] 7935 	mov	a,r6
      003649 A3               [24] 7936 	inc	dptr
      00364A F0               [24] 7937 	movx	@dptr,a
      00364B 90 03 DE         [24] 7938 	mov	dptr,#_memcpy_PARM_3
      00364E ED               [12] 7939 	mov	a,r5
      00364F F0               [24] 7940 	movx	@dptr,a
      003650 EB               [12] 7941 	mov	a,r3
      003651 A3               [24] 7942 	inc	dptr
      003652 F0               [24] 7943 	movx	@dptr,a
      003653 90 02 EC         [24] 7944 	mov	dptr,#(_axradio_cb_receive + 0x0018)
      003656 75 F0 00         [24] 7945 	mov	b,#0x00
      003659 12 6C 4F         [24] 7946 	lcall	_memcpy
      00365C                       7947 00118$:
                                   7948 ;	COMMON\easyax5043.c:1384: if (axradio_framing_sourceaddrpos != 0xff)
      00365C 90 7F EE         [24] 7949 	mov	dptr,#_axradio_framing_sourceaddrpos
      00365F E4               [12] 7950 	clr	a
      003660 93               [24] 7951 	movc	a,@a+dptr
      003661 FF               [12] 7952 	mov	r7,a
      003662 BF FF 02         [24] 7953 	cjne	r7,#0xff,00293$
      003665 80 34            [24] 7954 	sjmp	00120$
      003667                       7955 00293$:
                                   7956 ;	COMMON\easyax5043.c:1385: memcpy_xdata(&axradio_cb_receive.st.rx.mac.remoteaddr, &axradio_cb_receive.st.rx.mac.raw[axradio_framing_sourceaddrpos], axradio_framing_addrlen);
      003667 90 02 F0         [24] 7957 	mov	dptr,#(_axradio_cb_receive + 0x001c)
      00366A E0               [24] 7958 	movx	a,@dptr
      00366B FD               [12] 7959 	mov	r5,a
      00366C A3               [24] 7960 	inc	dptr
      00366D E0               [24] 7961 	movx	a,@dptr
      00366E FE               [12] 7962 	mov	r6,a
      00366F EF               [12] 7963 	mov	a,r7
      003670 2D               [12] 7964 	add	a,r5
      003671 FF               [12] 7965 	mov	r7,a
      003672 E4               [12] 7966 	clr	a
      003673 3E               [12] 7967 	addc	a,r6
      003674 FC               [12] 7968 	mov	r4,a
      003675 7E 00            [12] 7969 	mov	r6,#0x00
      003677 90 7F EC         [24] 7970 	mov	dptr,#_axradio_framing_addrlen
      00367A E4               [12] 7971 	clr	a
      00367B 93               [24] 7972 	movc	a,@a+dptr
      00367C FD               [12] 7973 	mov	r5,a
      00367D 7B 00            [12] 7974 	mov	r3,#0x00
      00367F 90 03 DB         [24] 7975 	mov	dptr,#_memcpy_PARM_2
      003682 EF               [12] 7976 	mov	a,r7
      003683 F0               [24] 7977 	movx	@dptr,a
      003684 EC               [12] 7978 	mov	a,r4
      003685 A3               [24] 7979 	inc	dptr
      003686 F0               [24] 7980 	movx	@dptr,a
      003687 EE               [12] 7981 	mov	a,r6
      003688 A3               [24] 7982 	inc	dptr
      003689 F0               [24] 7983 	movx	@dptr,a
      00368A 90 03 DE         [24] 7984 	mov	dptr,#_memcpy_PARM_3
      00368D ED               [12] 7985 	mov	a,r5
      00368E F0               [24] 7986 	movx	@dptr,a
      00368F EB               [12] 7987 	mov	a,r3
      003690 A3               [24] 7988 	inc	dptr
      003691 F0               [24] 7989 	movx	@dptr,a
      003692 90 02 E8         [24] 7990 	mov	dptr,#(_axradio_cb_receive + 0x0014)
      003695 75 F0 00         [24] 7991 	mov	b,#0x00
      003698 12 6C 4F         [24] 7992 	lcall	_memcpy
      00369B                       7993 00120$:
                                   7994 ;	COMMON\easyax5043.c:1386: if (axradio_mode == AXRADIO_MODE_ACK_RECEIVE ||
      00369B 74 22            [12] 7995 	mov	a,#0x22
      00369D B5 0B 02         [24] 7996 	cjne	a,_axradio_mode,00294$
      0036A0 80 11            [24] 7997 	sjmp	00142$
      0036A2                       7998 00294$:
                                   7999 ;	COMMON\easyax5043.c:1387: axradio_mode == AXRADIO_MODE_WOR_ACK_RECEIVE ||
      0036A2 74 23            [12] 8000 	mov	a,#0x23
      0036A4 B5 0B 02         [24] 8001 	cjne	a,_axradio_mode,00295$
      0036A7 80 0A            [24] 8002 	sjmp	00142$
      0036A9                       8003 00295$:
                                   8004 ;	COMMON\easyax5043.c:1388: axradio_mode == AXRADIO_MODE_SYNC_ACK_SLAVE) {
      0036A9 74 33            [12] 8005 	mov	a,#0x33
      0036AB B5 0B 02         [24] 8006 	cjne	a,_axradio_mode,00296$
      0036AE 80 03            [24] 8007 	sjmp	00297$
      0036B0                       8008 00296$:
      0036B0 02 38 BE         [24] 8009 	ljmp	00143$
      0036B3                       8010 00297$:
      0036B3                       8011 00142$:
                                   8012 ;	COMMON\easyax5043.c:1389: axradio_ack_count = 0;
      0036B3 90 00 B0         [24] 8013 	mov	dptr,#_axradio_ack_count
      0036B6 E4               [12] 8014 	clr	a
      0036B7 F0               [24] 8015 	movx	@dptr,a
                                   8016 ;	COMMON\easyax5043.c:1390: axradio_txbuffer_len = axradio_framing_maclen + axradio_framing_minpayloadlen;
      0036B8 90 7F EB         [24] 8017 	mov	dptr,#_axradio_framing_maclen
                                   8018 ;	genFromRTrack removed	clr	a
      0036BB 93               [24] 8019 	movc	a,@a+dptr
      0036BC FF               [12] 8020 	mov	r7,a
      0036BD FD               [12] 8021 	mov	r5,a
      0036BE 7E 00            [12] 8022 	mov	r6,#0x00
      0036C0 90 80 04         [24] 8023 	mov	dptr,#_axradio_framing_minpayloadlen
      0036C3 E4               [12] 8024 	clr	a
      0036C4 93               [24] 8025 	movc	a,@a+dptr
      0036C5 FC               [12] 8026 	mov	r4,a
      0036C6 7B 00            [12] 8027 	mov	r3,#0x00
      0036C8 90 00 A7         [24] 8028 	mov	dptr,#_axradio_txbuffer_len
      0036CB EC               [12] 8029 	mov	a,r4
      0036CC 2D               [12] 8030 	add	a,r5
      0036CD F0               [24] 8031 	movx	@dptr,a
      0036CE EB               [12] 8032 	mov	a,r3
      0036CF 3E               [12] 8033 	addc	a,r6
      0036D0 A3               [24] 8034 	inc	dptr
      0036D1 F0               [24] 8035 	movx	@dptr,a
                                   8036 ;	COMMON\easyax5043.c:1391: memset_xdata(axradio_txbuffer, 0, axradio_framing_maclen);
      0036D2 7E 00            [12] 8037 	mov	r6,#0x00
      0036D4 90 03 C6         [24] 8038 	mov	dptr,#_memset_PARM_2
      0036D7 E4               [12] 8039 	clr	a
      0036D8 F0               [24] 8040 	movx	@dptr,a
      0036D9 90 03 C7         [24] 8041 	mov	dptr,#_memset_PARM_3
      0036DC EF               [12] 8042 	mov	a,r7
      0036DD F0               [24] 8043 	movx	@dptr,a
      0036DE EE               [12] 8044 	mov	a,r6
      0036DF A3               [24] 8045 	inc	dptr
      0036E0 F0               [24] 8046 	movx	@dptr,a
      0036E1 90 00 CC         [24] 8047 	mov	dptr,#_axradio_txbuffer
      0036E4 75 F0 00         [24] 8048 	mov	b,#0x00
      0036E7 12 69 59         [24] 8049 	lcall	_memset
                                   8050 ;	COMMON\easyax5043.c:1392: if (axradio_framing_ack_seqnrpos != 0xff) {
      0036EA 90 80 03         [24] 8051 	mov	dptr,#_axradio_framing_ack_seqnrpos
      0036ED E4               [12] 8052 	clr	a
      0036EE 93               [24] 8053 	movc	a,@a+dptr
      0036EF FF               [12] 8054 	mov	r7,a
      0036F0 BF FF 02         [24] 8055 	cjne	r7,#0xff,00298$
      0036F3 80 35            [24] 8056 	sjmp	00125$
      0036F5                       8057 00298$:
                                   8058 ;	COMMON\easyax5043.c:1393: uint8_t seqnr = axradio_cb_receive.st.rx.mac.raw[axradio_framing_ack_seqnrpos];
      0036F5 90 02 F0         [24] 8059 	mov	dptr,#(_axradio_cb_receive + 0x001c)
      0036F8 E0               [24] 8060 	movx	a,@dptr
      0036F9 FD               [12] 8061 	mov	r5,a
      0036FA A3               [24] 8062 	inc	dptr
      0036FB E0               [24] 8063 	movx	a,@dptr
      0036FC FE               [12] 8064 	mov	r6,a
      0036FD EF               [12] 8065 	mov	a,r7
      0036FE 2D               [12] 8066 	add	a,r5
      0036FF F5 82            [12] 8067 	mov	dpl,a
      003701 E4               [12] 8068 	clr	a
      003702 3E               [12] 8069 	addc	a,r6
      003703 F5 83            [12] 8070 	mov	dph,a
      003705 E0               [24] 8071 	movx	a,@dptr
      003706 FE               [12] 8072 	mov	r6,a
                                   8073 ;	COMMON\easyax5043.c:1394: axradio_txbuffer[axradio_framing_ack_seqnrpos] = seqnr;
      003707 EF               [12] 8074 	mov	a,r7
      003708 24 CC            [12] 8075 	add	a,#_axradio_txbuffer
      00370A F5 82            [12] 8076 	mov	dpl,a
      00370C E4               [12] 8077 	clr	a
      00370D 34 00            [12] 8078 	addc	a,#(_axradio_txbuffer >> 8)
      00370F F5 83            [12] 8079 	mov	dph,a
      003711 EE               [12] 8080 	mov	a,r6
      003712 F0               [24] 8081 	movx	@dptr,a
                                   8082 ;	COMMON\easyax5043.c:1395: if (axradio_ack_seqnr != seqnr)
      003713 90 00 B1         [24] 8083 	mov	dptr,#_axradio_ack_seqnr
      003716 E0               [24] 8084 	movx	a,@dptr
      003717 FF               [12] 8085 	mov	r7,a
      003718 B5 06 02         [24] 8086 	cjne	a,ar6,00299$
      00371B 80 07            [24] 8087 	sjmp	00122$
      00371D                       8088 00299$:
                                   8089 ;	COMMON\easyax5043.c:1396: axradio_ack_seqnr = seqnr;
      00371D 90 00 B1         [24] 8090 	mov	dptr,#_axradio_ack_seqnr
      003720 EE               [12] 8091 	mov	a,r6
      003721 F0               [24] 8092 	movx	@dptr,a
      003722 80 06            [24] 8093 	sjmp	00125$
      003724                       8094 00122$:
                                   8095 ;	COMMON\easyax5043.c:1398: axradio_cb_receive.st.error = AXRADIO_ERR_RETRANSMISSION;
      003724 90 02 D9         [24] 8096 	mov	dptr,#(_axradio_cb_receive + 0x0005)
      003727 74 08            [12] 8097 	mov	a,#0x08
      003729 F0               [24] 8098 	movx	@dptr,a
      00372A                       8099 00125$:
                                   8100 ;	COMMON\easyax5043.c:1400: if (axradio_framing_destaddrpos != 0xff) {
      00372A 90 7F ED         [24] 8101 	mov	dptr,#_axradio_framing_destaddrpos
      00372D E4               [12] 8102 	clr	a
      00372E 93               [24] 8103 	movc	a,@a+dptr
      00372F FF               [12] 8104 	mov	r7,a
      003730 BF FF 02         [24] 8105 	cjne	r7,#0xff,00300$
      003733 80 6D            [24] 8106 	sjmp	00130$
      003735                       8107 00300$:
                                   8108 ;	COMMON\easyax5043.c:1401: if (axradio_framing_sourceaddrpos != 0xff)
      003735 90 7F EE         [24] 8109 	mov	dptr,#_axradio_framing_sourceaddrpos
      003738 E4               [12] 8110 	clr	a
      003739 93               [24] 8111 	movc	a,@a+dptr
      00373A FE               [12] 8112 	mov	r6,a
      00373B BE FF 02         [24] 8113 	cjne	r6,#0xff,00301$
      00373E 80 32            [24] 8114 	sjmp	00127$
      003740                       8115 00301$:
                                   8116 ;	COMMON\easyax5043.c:1402: memcpy_xdata(&axradio_txbuffer[axradio_framing_destaddrpos], &axradio_cb_receive.st.rx.mac.remoteaddr, axradio_framing_addrlen);
      003740 EF               [12] 8117 	mov	a,r7
      003741 24 CC            [12] 8118 	add	a,#_axradio_txbuffer
      003743 FD               [12] 8119 	mov	r5,a
      003744 E4               [12] 8120 	clr	a
      003745 34 00            [12] 8121 	addc	a,#(_axradio_txbuffer >> 8)
      003747 FE               [12] 8122 	mov	r6,a
      003748 7C 00            [12] 8123 	mov	r4,#0x00
      00374A 90 7F EC         [24] 8124 	mov	dptr,#_axradio_framing_addrlen
      00374D E4               [12] 8125 	clr	a
      00374E 93               [24] 8126 	movc	a,@a+dptr
      00374F FB               [12] 8127 	mov	r3,a
      003750 7A 00            [12] 8128 	mov	r2,#0x00
      003752 90 03 DB         [24] 8129 	mov	dptr,#_memcpy_PARM_2
      003755 74 E8            [12] 8130 	mov	a,#(_axradio_cb_receive + 0x0014)
      003757 F0               [24] 8131 	movx	@dptr,a
      003758 74 02            [12] 8132 	mov	a,#((_axradio_cb_receive + 0x0014) >> 8)
      00375A A3               [24] 8133 	inc	dptr
      00375B F0               [24] 8134 	movx	@dptr,a
      00375C E4               [12] 8135 	clr	a
      00375D A3               [24] 8136 	inc	dptr
      00375E F0               [24] 8137 	movx	@dptr,a
      00375F 90 03 DE         [24] 8138 	mov	dptr,#_memcpy_PARM_3
      003762 EB               [12] 8139 	mov	a,r3
      003763 F0               [24] 8140 	movx	@dptr,a
      003764 EA               [12] 8141 	mov	a,r2
      003765 A3               [24] 8142 	inc	dptr
      003766 F0               [24] 8143 	movx	@dptr,a
      003767 8D 82            [24] 8144 	mov	dpl,r5
      003769 8E 83            [24] 8145 	mov	dph,r6
      00376B 8C F0            [24] 8146 	mov	b,r4
      00376D 12 6C 4F         [24] 8147 	lcall	_memcpy
      003770 80 30            [24] 8148 	sjmp	00130$
      003772                       8149 00127$:
                                   8150 ;	COMMON\easyax5043.c:1404: memcpy_xdata(&axradio_txbuffer[axradio_framing_destaddrpos], &axradio_default_remoteaddr, axradio_framing_addrlen);
      003772 EF               [12] 8151 	mov	a,r7
      003773 24 CC            [12] 8152 	add	a,#_axradio_txbuffer
      003775 FF               [12] 8153 	mov	r7,a
      003776 E4               [12] 8154 	clr	a
      003777 34 00            [12] 8155 	addc	a,#(_axradio_txbuffer >> 8)
      003779 FE               [12] 8156 	mov	r6,a
      00377A 7D 00            [12] 8157 	mov	r5,#0x00
      00377C 90 7F EC         [24] 8158 	mov	dptr,#_axradio_framing_addrlen
      00377F E4               [12] 8159 	clr	a
      003780 93               [24] 8160 	movc	a,@a+dptr
      003781 FC               [12] 8161 	mov	r4,a
      003782 7B 00            [12] 8162 	mov	r3,#0x00
      003784 90 03 DB         [24] 8163 	mov	dptr,#_memcpy_PARM_2
      003787 74 C8            [12] 8164 	mov	a,#_axradio_default_remoteaddr
      003789 F0               [24] 8165 	movx	@dptr,a
      00378A 74 00            [12] 8166 	mov	a,#(_axradio_default_remoteaddr >> 8)
      00378C A3               [24] 8167 	inc	dptr
      00378D F0               [24] 8168 	movx	@dptr,a
      00378E E4               [12] 8169 	clr	a
      00378F A3               [24] 8170 	inc	dptr
      003790 F0               [24] 8171 	movx	@dptr,a
      003791 90 03 DE         [24] 8172 	mov	dptr,#_memcpy_PARM_3
      003794 EC               [12] 8173 	mov	a,r4
      003795 F0               [24] 8174 	movx	@dptr,a
      003796 EB               [12] 8175 	mov	a,r3
      003797 A3               [24] 8176 	inc	dptr
      003798 F0               [24] 8177 	movx	@dptr,a
      003799 8F 82            [24] 8178 	mov	dpl,r7
      00379B 8E 83            [24] 8179 	mov	dph,r6
      00379D 8D F0            [24] 8180 	mov	b,r5
      00379F 12 6C 4F         [24] 8181 	lcall	_memcpy
      0037A2                       8182 00130$:
                                   8183 ;	COMMON\easyax5043.c:1406: if (axradio_framing_sourceaddrpos != 0xff)
      0037A2 90 7F EE         [24] 8184 	mov	dptr,#_axradio_framing_sourceaddrpos
      0037A5 E4               [12] 8185 	clr	a
      0037A6 93               [24] 8186 	movc	a,@a+dptr
      0037A7 FF               [12] 8187 	mov	r7,a
      0037A8 BF FF 02         [24] 8188 	cjne	r7,#0xff,00302$
      0037AB 80 30            [24] 8189 	sjmp	00132$
      0037AD                       8190 00302$:
                                   8191 ;	COMMON\easyax5043.c:1407: memcpy_xdata(&axradio_txbuffer[axradio_framing_sourceaddrpos], &axradio_localaddr.addr, axradio_framing_addrlen);
      0037AD EF               [12] 8192 	mov	a,r7
      0037AE 24 CC            [12] 8193 	add	a,#_axradio_txbuffer
      0037B0 FF               [12] 8194 	mov	r7,a
      0037B1 E4               [12] 8195 	clr	a
      0037B2 34 00            [12] 8196 	addc	a,#(_axradio_txbuffer >> 8)
      0037B4 FE               [12] 8197 	mov	r6,a
      0037B5 7D 00            [12] 8198 	mov	r5,#0x00
      0037B7 90 7F EC         [24] 8199 	mov	dptr,#_axradio_framing_addrlen
      0037BA E4               [12] 8200 	clr	a
      0037BB 93               [24] 8201 	movc	a,@a+dptr
      0037BC FC               [12] 8202 	mov	r4,a
      0037BD 7B 00            [12] 8203 	mov	r3,#0x00
      0037BF 90 03 DB         [24] 8204 	mov	dptr,#_memcpy_PARM_2
      0037C2 74 C0            [12] 8205 	mov	a,#_axradio_localaddr
      0037C4 F0               [24] 8206 	movx	@dptr,a
      0037C5 74 00            [12] 8207 	mov	a,#(_axradio_localaddr >> 8)
      0037C7 A3               [24] 8208 	inc	dptr
      0037C8 F0               [24] 8209 	movx	@dptr,a
      0037C9 E4               [12] 8210 	clr	a
      0037CA A3               [24] 8211 	inc	dptr
      0037CB F0               [24] 8212 	movx	@dptr,a
      0037CC 90 03 DE         [24] 8213 	mov	dptr,#_memcpy_PARM_3
      0037CF EC               [12] 8214 	mov	a,r4
      0037D0 F0               [24] 8215 	movx	@dptr,a
      0037D1 EB               [12] 8216 	mov	a,r3
      0037D2 A3               [24] 8217 	inc	dptr
      0037D3 F0               [24] 8218 	movx	@dptr,a
      0037D4 8F 82            [24] 8219 	mov	dpl,r7
      0037D6 8E 83            [24] 8220 	mov	dph,r6
      0037D8 8D F0            [24] 8221 	mov	b,r5
      0037DA 12 6C 4F         [24] 8222 	lcall	_memcpy
      0037DD                       8223 00132$:
                                   8224 ;	COMMON\easyax5043.c:1408: if (axradio_framing_lenmask) {
      0037DD 90 7F F1         [24] 8225 	mov	dptr,#_axradio_framing_lenmask
      0037E0 E4               [12] 8226 	clr	a
      0037E1 93               [24] 8227 	movc	a,@a+dptr
      0037E2 FF               [12] 8228 	mov	r7,a
      0037E3 60 30            [24] 8229 	jz	00134$
                                   8230 ;	COMMON\easyax5043.c:1409: uint8_t len_byte = (uint8_t)(axradio_txbuffer_len - axradio_framing_lenoffs) & axradio_framing_lenmask; // if you prefer not counting the len byte itself, set LENOFFS = 1
      0037E5 90 00 A7         [24] 8231 	mov	dptr,#_axradio_txbuffer_len
      0037E8 E0               [24] 8232 	movx	a,@dptr
      0037E9 FD               [12] 8233 	mov	r5,a
      0037EA A3               [24] 8234 	inc	dptr
      0037EB E0               [24] 8235 	movx	a,@dptr
      0037EC 90 7F F0         [24] 8236 	mov	dptr,#_axradio_framing_lenoffs
      0037EF E4               [12] 8237 	clr	a
      0037F0 93               [24] 8238 	movc	a,@a+dptr
      0037F1 FE               [12] 8239 	mov	r6,a
      0037F2 ED               [12] 8240 	mov	a,r5
      0037F3 C3               [12] 8241 	clr	c
      0037F4 9E               [12] 8242 	subb	a,r6
      0037F5 5F               [12] 8243 	anl	a,r7
      0037F6 FE               [12] 8244 	mov	r6,a
                                   8245 ;	COMMON\easyax5043.c:1410: axradio_txbuffer[axradio_framing_lenpos] = (axradio_txbuffer[axradio_framing_lenpos] & (uint8_t)~axradio_framing_lenmask) | len_byte;
      0037F7 90 7F EF         [24] 8246 	mov	dptr,#_axradio_framing_lenpos
      0037FA E4               [12] 8247 	clr	a
      0037FB 93               [24] 8248 	movc	a,@a+dptr
      0037FC 24 CC            [12] 8249 	add	a,#_axradio_txbuffer
      0037FE FD               [12] 8250 	mov	r5,a
      0037FF E4               [12] 8251 	clr	a
      003800 34 00            [12] 8252 	addc	a,#(_axradio_txbuffer >> 8)
      003802 FC               [12] 8253 	mov	r4,a
      003803 8D 82            [24] 8254 	mov	dpl,r5
      003805 8C 83            [24] 8255 	mov	dph,r4
      003807 E0               [24] 8256 	movx	a,@dptr
      003808 FB               [12] 8257 	mov	r3,a
      003809 EF               [12] 8258 	mov	a,r7
      00380A F4               [12] 8259 	cpl	a
      00380B FF               [12] 8260 	mov	r7,a
      00380C 5B               [12] 8261 	anl	a,r3
      00380D 42 06            [12] 8262 	orl	ar6,a
      00380F 8D 82            [24] 8263 	mov	dpl,r5
      003811 8C 83            [24] 8264 	mov	dph,r4
      003813 EE               [12] 8265 	mov	a,r6
      003814 F0               [24] 8266 	movx	@dptr,a
      003815                       8267 00134$:
                                   8268 ;	COMMON\easyax5043.c:1412: if (axradio_framing_swcrclen)
      003815 90 7F F2         [24] 8269 	mov	dptr,#_axradio_framing_swcrclen
      003818 E4               [12] 8270 	clr	a
      003819 93               [24] 8271 	movc	a,@a+dptr
      00381A 60 20            [24] 8272 	jz	00136$
                                   8273 ;	COMMON\easyax5043.c:1413: axradio_txbuffer_len = axradio_framing_append_crc(axradio_txbuffer, axradio_txbuffer_len);
      00381C 90 00 A7         [24] 8274 	mov	dptr,#_axradio_txbuffer_len
      00381F E0               [24] 8275 	movx	a,@dptr
      003820 C0 E0            [24] 8276 	push	acc
      003822 A3               [24] 8277 	inc	dptr
      003823 E0               [24] 8278 	movx	a,@dptr
      003824 C0 E0            [24] 8279 	push	acc
      003826 90 00 CC         [24] 8280 	mov	dptr,#_axradio_txbuffer
      003829 12 19 7C         [24] 8281 	lcall	_axradio_framing_append_crc
      00382C AE 82            [24] 8282 	mov	r6,dpl
      00382E AF 83            [24] 8283 	mov	r7,dph
      003830 15 81            [12] 8284 	dec	sp
      003832 15 81            [12] 8285 	dec	sp
      003834 90 00 A7         [24] 8286 	mov	dptr,#_axradio_txbuffer_len
      003837 EE               [12] 8287 	mov	a,r6
      003838 F0               [24] 8288 	movx	@dptr,a
      003839 EF               [12] 8289 	mov	a,r7
      00383A A3               [24] 8290 	inc	dptr
      00383B F0               [24] 8291 	movx	@dptr,a
      00383C                       8292 00136$:
                                   8293 ;	COMMON\easyax5043.c:1414: if (axradio_phy_pn9) {
      00383C 90 7F CA         [24] 8294 	mov	dptr,#_axradio_phy_pn9
      00383F E4               [12] 8295 	clr	a
      003840 93               [24] 8296 	movc	a,@a+dptr
      003841 60 2F            [24] 8297 	jz	00138$
                                   8298 ;	COMMON\easyax5043.c:1415: pn9_buffer(axradio_txbuffer, axradio_txbuffer_len, 0x1ff, -(AX5043_ENCODING & 0x01));
      003843 90 40 11         [24] 8299 	mov	dptr,#_AX5043_ENCODING
      003846 E0               [24] 8300 	movx	a,@dptr
      003847 FF               [12] 8301 	mov	r7,a
      003848 53 07 01         [24] 8302 	anl	ar7,#0x01
      00384B C3               [12] 8303 	clr	c
      00384C E4               [12] 8304 	clr	a
      00384D 9F               [12] 8305 	subb	a,r7
      00384E FF               [12] 8306 	mov	r7,a
      00384F C0 07            [24] 8307 	push	ar7
      003851 74 FF            [12] 8308 	mov	a,#0xff
      003853 C0 E0            [24] 8309 	push	acc
      003855 74 01            [12] 8310 	mov	a,#0x01
      003857 C0 E0            [24] 8311 	push	acc
      003859 90 00 A7         [24] 8312 	mov	dptr,#_axradio_txbuffer_len
      00385C E0               [24] 8313 	movx	a,@dptr
      00385D C0 E0            [24] 8314 	push	acc
      00385F A3               [24] 8315 	inc	dptr
      003860 E0               [24] 8316 	movx	a,@dptr
      003861 C0 E0            [24] 8317 	push	acc
      003863 90 00 CC         [24] 8318 	mov	dptr,#_axradio_txbuffer
      003866 75 F0 00         [24] 8319 	mov	b,#0x00
      003869 12 6E 0C         [24] 8320 	lcall	_pn9_buffer
      00386C E5 81            [12] 8321 	mov	a,sp
      00386E 24 FB            [12] 8322 	add	a,#0xfb
      003870 F5 81            [12] 8323 	mov	sp,a
      003872                       8324 00138$:
                                   8325 ;	COMMON\easyax5043.c:1417: AX5043_IRQMASK1 = 0x00;
      003872 90 40 06         [24] 8326 	mov	dptr,#_AX5043_IRQMASK1
      003875 E4               [12] 8327 	clr	a
      003876 F0               [24] 8328 	movx	@dptr,a
                                   8329 ;	COMMON\easyax5043.c:1418: AX5043_IRQMASK0 = 0x00;
      003877 90 40 07         [24] 8330 	mov	dptr,#_AX5043_IRQMASK0
      00387A F0               [24] 8331 	movx	@dptr,a
                                   8332 ;	COMMON\easyax5043.c:1419: AX5043_PWRMODE = AX5043_PWRSTATE_XTAL_ON;
      00387B 90 40 02         [24] 8333 	mov	dptr,#_AX5043_PWRMODE
      00387E 74 05            [12] 8334 	mov	a,#0x05
      003880 F0               [24] 8335 	movx	@dptr,a
                                   8336 ;	COMMON\easyax5043.c:1420: AX5043_FIFOSTAT = 3;
      003881 90 40 28         [24] 8337 	mov	dptr,#_AX5043_FIFOSTAT
      003884 74 03            [12] 8338 	mov	a,#0x03
      003886 F0               [24] 8339 	movx	@dptr,a
                                   8340 ;	COMMON\easyax5043.c:1421: axradio_trxstate = trxstate_tx_longpreamble; // ensure that trxstate != off, otherwise we would prematurely enable the receiver, see below
      003887 75 0C 0A         [24] 8341 	mov	_axradio_trxstate,#0x0a
                                   8342 ;	COMMON\easyax5043.c:1422: while (AX5043_POWSTAT & 0x08);
      00388A                       8343 00139$:
      00388A 90 40 03         [24] 8344 	mov	dptr,#_AX5043_POWSTAT
      00388D E0               [24] 8345 	movx	a,@dptr
      00388E FF               [12] 8346 	mov	r7,a
      00388F 20 E3 F8         [24] 8347 	jb	acc.3,00139$
                                   8348 ;	COMMON\easyax5043.c:1423: wtimer_remove(&axradio_timer);
      003892 90 03 2B         [24] 8349 	mov	dptr,#_axradio_timer
      003895 12 76 94         [24] 8350 	lcall	_wtimer_remove
                                   8351 ;	COMMON\easyax5043.c:1424: axradio_timer.time = axradio_framing_ack_delay;
      003898 90 7F FE         [24] 8352 	mov	dptr,#_axradio_framing_ack_delay
      00389B E4               [12] 8353 	clr	a
      00389C 93               [24] 8354 	movc	a,@a+dptr
      00389D FC               [12] 8355 	mov	r4,a
      00389E 74 01            [12] 8356 	mov	a,#0x01
      0038A0 93               [24] 8357 	movc	a,@a+dptr
      0038A1 FD               [12] 8358 	mov	r5,a
      0038A2 74 02            [12] 8359 	mov	a,#0x02
      0038A4 93               [24] 8360 	movc	a,@a+dptr
      0038A5 FE               [12] 8361 	mov	r6,a
      0038A6 74 03            [12] 8362 	mov	a,#0x03
      0038A8 93               [24] 8363 	movc	a,@a+dptr
      0038A9 FF               [12] 8364 	mov	r7,a
      0038AA 90 03 2F         [24] 8365 	mov	dptr,#(_axradio_timer + 0x0004)
      0038AD EC               [12] 8366 	mov	a,r4
      0038AE F0               [24] 8367 	movx	@dptr,a
      0038AF ED               [12] 8368 	mov	a,r5
      0038B0 A3               [24] 8369 	inc	dptr
      0038B1 F0               [24] 8370 	movx	@dptr,a
      0038B2 EE               [12] 8371 	mov	a,r6
      0038B3 A3               [24] 8372 	inc	dptr
      0038B4 F0               [24] 8373 	movx	@dptr,a
      0038B5 EF               [12] 8374 	mov	a,r7
      0038B6 A3               [24] 8375 	inc	dptr
      0038B7 F0               [24] 8376 	movx	@dptr,a
                                   8377 ;	COMMON\easyax5043.c:1425: wtimer1_addrelative(&axradio_timer);
      0038B8 90 03 2B         [24] 8378 	mov	dptr,#_axradio_timer
      0038BB 12 6D 24         [24] 8379 	lcall	_wtimer1_addrelative
      0038BE                       8380 00143$:
                                   8381 ;	COMMON\easyax5043.c:1427: if (axradio_mode == AXRADIO_MODE_SYNC_SLAVE ||
      0038BE 74 32            [12] 8382 	mov	a,#0x32
      0038C0 B5 0B 02         [24] 8383 	cjne	a,_axradio_mode,00307$
      0038C3 80 0A            [24] 8384 	sjmp	00156$
      0038C5                       8385 00307$:
                                   8386 ;	COMMON\easyax5043.c:1428: axradio_mode == AXRADIO_MODE_SYNC_ACK_SLAVE) {
      0038C5 74 33            [12] 8387 	mov	a,#0x33
      0038C7 B5 0B 02         [24] 8388 	cjne	a,_axradio_mode,00308$
      0038CA 80 03            [24] 8389 	sjmp	00309$
      0038CC                       8390 00308$:
      0038CC 02 39 A2         [24] 8391 	ljmp	00157$
      0038CF                       8392 00309$:
      0038CF                       8393 00156$:
                                   8394 ;	COMMON\easyax5043.c:1429: if (axradio_mode != AXRADIO_MODE_SYNC_ACK_SLAVE)
      0038CF 74 33            [12] 8395 	mov	a,#0x33
      0038D1 B5 0B 02         [24] 8396 	cjne	a,_axradio_mode,00310$
      0038D4 80 03            [24] 8397 	sjmp	00147$
      0038D6                       8398 00310$:
                                   8399 ;	COMMON\easyax5043.c:1430: ax5043_off();
      0038D6 12 28 BB         [24] 8400 	lcall	_ax5043_off
      0038D9                       8401 00147$:
                                   8402 ;	COMMON\easyax5043.c:1431: switch (axradio_syncstate) {
      0038D9 90 00 A6         [24] 8403 	mov	dptr,#_axradio_syncstate
      0038DC E0               [24] 8404 	movx	a,@dptr
      0038DD FF               [12] 8405 	mov	r7,a
      0038DE BF 08 02         [24] 8406 	cjne	r7,#0x08,00311$
      0038E1 80 45            [24] 8407 	sjmp	00151$
      0038E3                       8408 00311$:
      0038E3 BF 0A 02         [24] 8409 	cjne	r7,#0x0a,00312$
      0038E6 80 40            [24] 8410 	sjmp	00151$
      0038E8                       8411 00312$:
      0038E8 BF 0B 02         [24] 8412 	cjne	r7,#0x0b,00313$
      0038EB 80 3B            [24] 8413 	sjmp	00151$
      0038ED                       8414 00313$:
                                   8415 ;	COMMON\easyax5043.c:1435: axradio_sync_time = axradio_conv_time_totimer0(axradio_cb_receive.st.time.t);
      0038ED 90 02 DA         [24] 8416 	mov	dptr,#(_axradio_cb_receive + 0x0006)
      0038F0 E0               [24] 8417 	movx	a,@dptr
      0038F1 FC               [12] 8418 	mov	r4,a
      0038F2 A3               [24] 8419 	inc	dptr
      0038F3 E0               [24] 8420 	movx	a,@dptr
      0038F4 FD               [12] 8421 	mov	r5,a
      0038F5 A3               [24] 8422 	inc	dptr
      0038F6 E0               [24] 8423 	movx	a,@dptr
      0038F7 FE               [12] 8424 	mov	r6,a
      0038F8 A3               [24] 8425 	inc	dptr
      0038F9 E0               [24] 8426 	movx	a,@dptr
      0038FA 8C 82            [24] 8427 	mov	dpl,r4
      0038FC 8D 83            [24] 8428 	mov	dph,r5
      0038FE 8E F0            [24] 8429 	mov	b,r6
      003900 12 1B 8A         [24] 8430 	lcall	_axradio_conv_time_totimer0
      003903 AC 82            [24] 8431 	mov	r4,dpl
      003905 AD 83            [24] 8432 	mov	r5,dph
      003907 AE F0            [24] 8433 	mov	r6,b
      003909 FF               [12] 8434 	mov	r7,a
      00390A 90 00 B2         [24] 8435 	mov	dptr,#_axradio_sync_time
      00390D EC               [12] 8436 	mov	a,r4
      00390E F0               [24] 8437 	movx	@dptr,a
      00390F ED               [12] 8438 	mov	a,r5
      003910 A3               [24] 8439 	inc	dptr
      003911 F0               [24] 8440 	movx	@dptr,a
      003912 EE               [12] 8441 	mov	a,r6
      003913 A3               [24] 8442 	inc	dptr
      003914 F0               [24] 8443 	movx	@dptr,a
      003915 EF               [12] 8444 	mov	a,r7
      003916 A3               [24] 8445 	inc	dptr
      003917 F0               [24] 8446 	movx	@dptr,a
                                   8447 ;	COMMON\easyax5043.c:1436: axradio_sync_periodcorr = -32768;
      003918 90 00 B6         [24] 8448 	mov	dptr,#_axradio_sync_periodcorr
      00391B E4               [12] 8449 	clr	a
      00391C F0               [24] 8450 	movx	@dptr,a
      00391D 74 80            [12] 8451 	mov	a,#0x80
      00391F A3               [24] 8452 	inc	dptr
      003920 F0               [24] 8453 	movx	@dptr,a
                                   8454 ;	COMMON\easyax5043.c:1437: axradio_sync_seqnr = 0;
      003921 90 00 B1         [24] 8455 	mov	dptr,#_axradio_ack_seqnr
      003924 E4               [12] 8456 	clr	a
      003925 F0               [24] 8457 	movx	@dptr,a
                                   8458 ;	COMMON\easyax5043.c:1438: break;
                                   8459 ;	COMMON\easyax5043.c:1442: case syncstate_slave_rxpacket:
      003926 80 2D            [24] 8460 	sjmp	00152$
      003928                       8461 00151$:
                                   8462 ;	COMMON\easyax5043.c:1443: axradio_sync_adjustperiodcorr();
      003928 12 2A ED         [24] 8463 	lcall	_axradio_sync_adjustperiodcorr
                                   8464 ;	COMMON\easyax5043.c:1444: axradio_cb_receive.st.rx.phy.period = axradio_sync_periodcorr >> SYNC_K1;
      00392B 90 00 B6         [24] 8465 	mov	dptr,#_axradio_sync_periodcorr
      00392E E0               [24] 8466 	movx	a,@dptr
      00392F FE               [12] 8467 	mov	r6,a
      003930 A3               [24] 8468 	inc	dptr
      003931 E0               [24] 8469 	movx	a,@dptr
      003932 FF               [12] 8470 	mov	r7,a
      003933 C4               [12] 8471 	swap	a
      003934 03               [12] 8472 	rr	a
      003935 CE               [12] 8473 	xch	a,r6
      003936 C4               [12] 8474 	swap	a
      003937 03               [12] 8475 	rr	a
      003938 54 07            [12] 8476 	anl	a,#0x07
      00393A 6E               [12] 8477 	xrl	a,r6
      00393B CE               [12] 8478 	xch	a,r6
      00393C 54 07            [12] 8479 	anl	a,#0x07
      00393E CE               [12] 8480 	xch	a,r6
      00393F 6E               [12] 8481 	xrl	a,r6
      003940 CE               [12] 8482 	xch	a,r6
      003941 30 E2 02         [24] 8483 	jnb	acc.2,00314$
      003944 44 F8            [12] 8484 	orl	a,#0xf8
      003946                       8485 00314$:
      003946 FF               [12] 8486 	mov	r7,a
      003947 90 02 E6         [24] 8487 	mov	dptr,#(_axradio_cb_receive + 0x0012)
      00394A EE               [12] 8488 	mov	a,r6
      00394B F0               [24] 8489 	movx	@dptr,a
      00394C EF               [12] 8490 	mov	a,r7
      00394D A3               [24] 8491 	inc	dptr
      00394E F0               [24] 8492 	movx	@dptr,a
                                   8493 ;	COMMON\easyax5043.c:1445: axradio_sync_seqnr = 1;
      00394F 90 00 B1         [24] 8494 	mov	dptr,#_axradio_ack_seqnr
      003952 74 01            [12] 8495 	mov	a,#0x01
      003954 F0               [24] 8496 	movx	@dptr,a
                                   8497 ;	COMMON\easyax5043.c:1447: };
      003955                       8498 00152$:
                                   8499 ;	COMMON\easyax5043.c:1448: axradio_sync_slave_nextperiod();
      003955 12 2C 14         [24] 8500 	lcall	_axradio_sync_slave_nextperiod
                                   8501 ;	COMMON\easyax5043.c:1449: if (axradio_mode != AXRADIO_MODE_SYNC_ACK_SLAVE) {
      003958 74 33            [12] 8502 	mov	a,#0x33
      00395A B5 0B 02         [24] 8503 	cjne	a,_axradio_mode,00315$
      00395D 80 3D            [24] 8504 	sjmp	00154$
      00395F                       8505 00315$:
                                   8506 ;	COMMON\easyax5043.c:1450: axradio_syncstate = syncstate_slave_rxidle;
      00395F 90 00 A6         [24] 8507 	mov	dptr,#_axradio_syncstate
      003962 74 08            [12] 8508 	mov	a,#0x08
      003964 F0               [24] 8509 	movx	@dptr,a
                                   8510 ;	COMMON\easyax5043.c:1451: wtimer_remove(&axradio_timer);
      003965 90 03 2B         [24] 8511 	mov	dptr,#_axradio_timer
      003968 12 76 94         [24] 8512 	lcall	_wtimer_remove
                                   8513 ;	COMMON\easyax5043.c:1452: axradio_sync_settimeradv(axradio_sync_slave_rxadvance[axradio_sync_seqnr]);
      00396B 90 00 B1         [24] 8514 	mov	dptr,#_axradio_ack_seqnr
      00396E E0               [24] 8515 	movx	a,@dptr
      00396F 75 F0 04         [24] 8516 	mov	b,#0x04
      003972 A4               [48] 8517 	mul	ab
      003973 24 1F            [12] 8518 	add	a,#_axradio_sync_slave_rxadvance
      003975 F5 82            [12] 8519 	mov	dpl,a
      003977 74 80            [12] 8520 	mov	a,#(_axradio_sync_slave_rxadvance >> 8)
      003979 35 F0            [12] 8521 	addc	a,b
      00397B F5 83            [12] 8522 	mov	dph,a
      00397D E4               [12] 8523 	clr	a
      00397E 93               [24] 8524 	movc	a,@a+dptr
      00397F FC               [12] 8525 	mov	r4,a
      003980 A3               [24] 8526 	inc	dptr
      003981 E4               [12] 8527 	clr	a
      003982 93               [24] 8528 	movc	a,@a+dptr
      003983 FD               [12] 8529 	mov	r5,a
      003984 A3               [24] 8530 	inc	dptr
      003985 E4               [12] 8531 	clr	a
      003986 93               [24] 8532 	movc	a,@a+dptr
      003987 FE               [12] 8533 	mov	r6,a
      003988 A3               [24] 8534 	inc	dptr
      003989 E4               [12] 8535 	clr	a
      00398A 93               [24] 8536 	movc	a,@a+dptr
      00398B 8C 82            [24] 8537 	mov	dpl,r4
      00398D 8D 83            [24] 8538 	mov	dph,r5
      00398F 8E F0            [24] 8539 	mov	b,r6
      003991 12 2A AE         [24] 8540 	lcall	_axradio_sync_settimeradv
                                   8541 ;	COMMON\easyax5043.c:1453: wtimer0_addabsolute(&axradio_timer);
      003994 90 03 2B         [24] 8542 	mov	dptr,#_axradio_timer
      003997 12 6D B9         [24] 8543 	lcall	_wtimer0_addabsolute
      00399A 80 06            [24] 8544 	sjmp	00157$
      00399C                       8545 00154$:
                                   8546 ;	COMMON\easyax5043.c:1455: axradio_syncstate = syncstate_slave_rxack;
      00399C 90 00 A6         [24] 8547 	mov	dptr,#_axradio_syncstate
      00399F 74 0C            [12] 8548 	mov	a,#0x0c
      0039A1 F0               [24] 8549 	movx	@dptr,a
      0039A2                       8550 00157$:
                                   8551 ;	COMMON\easyax5043.c:1458: axradio_statuschange((struct axradio_status __xdata *)&axradio_cb_receive.st);
      0039A2 90 02 D8         [24] 8552 	mov	dptr,#(_axradio_cb_receive + 0x0004)
      0039A5 12 0C 19         [24] 8553 	lcall	_axradio_statuschange
                                   8554 ;	COMMON\easyax5043.c:1459: endcb:
      0039A8                       8555 00159$:
                                   8556 ;	COMMON\easyax5043.c:1460: if (axradio_mode == AXRADIO_MODE_WOR_RECEIVE) {
      0039A8 74 21            [12] 8557 	mov	a,#0x21
      0039AA B5 0B 03         [24] 8558 	cjne	a,_axradio_mode,00174$
                                   8559 ;	COMMON\easyax5043.c:1461: ax5043_receiver_on_wor();
      0039AD 02 27 CC         [24] 8560 	ljmp	_ax5043_receiver_on_wor
      0039B0                       8561 00174$:
                                   8562 ;	COMMON\easyax5043.c:1462: } else if (axradio_mode == AXRADIO_MODE_ACK_RECEIVE ||
      0039B0 74 22            [12] 8563 	mov	a,#0x22
      0039B2 B5 0B 02         [24] 8564 	cjne	a,_axradio_mode,00318$
      0039B5 80 05            [24] 8565 	sjmp	00169$
      0039B7                       8566 00318$:
                                   8567 ;	COMMON\easyax5043.c:1463: axradio_mode == AXRADIO_MODE_WOR_ACK_RECEIVE) {
      0039B7 74 23            [12] 8568 	mov	a,#0x23
      0039B9 B5 0B 20         [24] 8569 	cjne	a,_axradio_mode,00170$
      0039BC                       8570 00169$:
                                   8571 ;	COMMON\easyax5043.c:1466: uint8_t __autodata iesave = IE & 0x80;
      0039BC 74 80            [12] 8572 	mov	a,#0x80
      0039BE 55 A8            [12] 8573 	anl	a,_IE
      0039C0 FF               [12] 8574 	mov	r7,a
                                   8575 ;	COMMON\easyax5043.c:1467: EA = 0;
      0039C1 C2 AF            [12] 8576 	clr	_EA
                                   8577 ;	COMMON\easyax5043.c:1468: trxst = axradio_trxstate;
      0039C3 AE 0C            [24] 8578 	mov	r6,_axradio_trxstate
                                   8579 ;	COMMON\easyax5043.c:1469: axradio_cb_receive.st.error = AXRADIO_ERR_PACKETDONE;
      0039C5 90 02 D9         [24] 8580 	mov	dptr,#(_axradio_cb_receive + 0x0005)
      0039C8 74 F0            [12] 8581 	mov	a,#0xf0
      0039CA F0               [24] 8582 	movx	@dptr,a
                                   8583 ;	COMMON\easyax5043.c:1470: IE |= iesave;
      0039CB EF               [12] 8584 	mov	a,r7
      0039CC 42 A8            [12] 8585 	orl	_IE,a
                                   8586 ;	COMMON\easyax5043.c:1472: if (trxst == trxstate_off) {
      0039CE EE               [12] 8587 	mov	a,r6
      0039CF 70 1E            [24] 8588 	jnz	00176$
                                   8589 ;	COMMON\easyax5043.c:1473: if (axradio_mode == AXRADIO_MODE_WOR_ACK_RECEIVE)
      0039D1 74 23            [12] 8590 	mov	a,#0x23
      0039D3 B5 0B 03         [24] 8591 	cjne	a,_axradio_mode,00161$
                                   8592 ;	COMMON\easyax5043.c:1474: ax5043_receiver_on_wor();
      0039D6 02 27 CC         [24] 8593 	ljmp	_ax5043_receiver_on_wor
      0039D9                       8594 00161$:
                                   8595 ;	COMMON\easyax5043.c:1476: ax5043_receiver_on_continuous();
      0039D9 02 27 64         [24] 8596 	ljmp	_ax5043_receiver_on_continuous
      0039DC                       8597 00170$:
                                   8598 ;	COMMON\easyax5043.c:1479: switch (axradio_trxstate) {
      0039DC AF 0C            [24] 8599 	mov	r7,_axradio_trxstate
      0039DE BF 01 02         [24] 8600 	cjne	r7,#0x01,00324$
      0039E1 80 03            [24] 8601 	sjmp	00166$
      0039E3                       8602 00324$:
      0039E3 BF 02 09         [24] 8603 	cjne	r7,#0x02,00176$
                                   8604 ;	COMMON\easyax5043.c:1481: case trxstate_rxwor:
      0039E6                       8605 00166$:
                                   8606 ;	COMMON\easyax5043.c:1482: AX5043_IRQMASK0 |= 0x01; // re-enable FIFO not empty irq
      0039E6 90 40 07         [24] 8607 	mov	dptr,#_AX5043_IRQMASK0
      0039E9 E0               [24] 8608 	movx	a,@dptr
      0039EA FF               [12] 8609 	mov	r7,a
      0039EB 74 01            [12] 8610 	mov	a,#0x01
      0039ED 4F               [12] 8611 	orl	a,r7
      0039EE F0               [24] 8612 	movx	@dptr,a
                                   8613 ;	COMMON\easyax5043.c:1487: }
      0039EF                       8614 00176$:
      0039EF 22               [24] 8615 	ret
                                   8616 ;------------------------------------------------------------
                                   8617 ;Allocation info for local variables in function 'axradio_killallcb'
                                   8618 ;------------------------------------------------------------
                                   8619 ;	COMMON\easyax5043.c:1491: static void axradio_killallcb(void)
                                   8620 ;	-----------------------------------------
                                   8621 ;	 function axradio_killallcb
                                   8622 ;	-----------------------------------------
      0039F0                       8623 _axradio_killallcb:
                                   8624 ;	COMMON\easyax5043.c:1493: wtimer_remove_callback(&axradio_cb_receive.cb);
      0039F0 90 02 D4         [24] 8625 	mov	dptr,#_axradio_cb_receive
      0039F3 12 78 A9         [24] 8626 	lcall	_wtimer_remove_callback
                                   8627 ;	COMMON\easyax5043.c:1494: wtimer_remove_callback(&axradio_cb_receivesfd.cb);
      0039F6 90 02 F6         [24] 8628 	mov	dptr,#_axradio_cb_receivesfd
      0039F9 12 78 A9         [24] 8629 	lcall	_wtimer_remove_callback
                                   8630 ;	COMMON\easyax5043.c:1495: wtimer_remove_callback(&axradio_cb_channelstate.cb);
      0039FC 90 03 00         [24] 8631 	mov	dptr,#_axradio_cb_channelstate
      0039FF 12 78 A9         [24] 8632 	lcall	_wtimer_remove_callback
                                   8633 ;	COMMON\easyax5043.c:1496: wtimer_remove_callback(&axradio_cb_transmitstart.cb);
      003A02 90 03 0D         [24] 8634 	mov	dptr,#_axradio_cb_transmitstart
      003A05 12 78 A9         [24] 8635 	lcall	_wtimer_remove_callback
                                   8636 ;	COMMON\easyax5043.c:1497: wtimer_remove_callback(&axradio_cb_transmitend.cb);
      003A08 90 03 17         [24] 8637 	mov	dptr,#_axradio_cb_transmitend
      003A0B 12 78 A9         [24] 8638 	lcall	_wtimer_remove_callback
                                   8639 ;	COMMON\easyax5043.c:1498: wtimer_remove_callback(&axradio_cb_transmitdata.cb);
      003A0E 90 03 21         [24] 8640 	mov	dptr,#_axradio_cb_transmitdata
      003A11 12 78 A9         [24] 8641 	lcall	_wtimer_remove_callback
                                   8642 ;	COMMON\easyax5043.c:1499: wtimer_remove(&axradio_timer);
      003A14 90 03 2B         [24] 8643 	mov	dptr,#_axradio_timer
      003A17 02 76 94         [24] 8644 	ljmp	_wtimer_remove
                                   8645 ;------------------------------------------------------------
                                   8646 ;Allocation info for local variables in function 'axradio_tunevoltage'
                                   8647 ;------------------------------------------------------------
                                   8648 ;x                         Allocated with name '_axradio_tunevoltage_x_3_374'
                                   8649 ;r                         Allocated to registers r6 r7 
                                   8650 ;cnt                       Allocated to registers r5 
                                   8651 ;------------------------------------------------------------
                                   8652 ;	COMMON\easyax5043.c:1526: static int16_t axradio_tunevoltage(void)
                                   8653 ;	-----------------------------------------
                                   8654 ;	 function axradio_tunevoltage
                                   8655 ;	-----------------------------------------
      003A1A                       8656 _axradio_tunevoltage:
                                   8657 ;	COMMON\easyax5043.c:1528: int16_t __autodata r = 0;
      003A1A 7E 00            [12] 8658 	mov	r6,#0x00
      003A1C 7F 00            [12] 8659 	mov	r7,#0x00
                                   8660 ;	COMMON\easyax5043.c:1530: do {
      003A1E 7D 40            [12] 8661 	mov	r5,#0x40
      003A20                       8662 00103$:
                                   8663 ;	COMMON\easyax5043.c:1531: AX5043_GPADCCTRL = 0x84;
      003A20 90 43 00         [24] 8664 	mov	dptr,#_AX5043_GPADCCTRL
      003A23 74 84            [12] 8665 	mov	a,#0x84
      003A25 F0               [24] 8666 	movx	@dptr,a
                                   8667 ;	COMMON\easyax5043.c:1532: do {} while (AX5043_GPADCCTRL & 0x80);
      003A26                       8668 00101$:
      003A26 90 43 00         [24] 8669 	mov	dptr,#_AX5043_GPADCCTRL
      003A29 E0               [24] 8670 	movx	a,@dptr
      003A2A FC               [12] 8671 	mov	r4,a
      003A2B 20 E7 F8         [24] 8672 	jb	acc.7,00101$
                                   8673 ;	COMMON\easyax5043.c:1533: } while (--cnt);
      003A2E DD F0            [24] 8674 	djnz	r5,00103$
                                   8675 ;	COMMON\easyax5043.c:1535: do {
      003A30 7D 20            [12] 8676 	mov	r5,#0x20
      003A32                       8677 00108$:
                                   8678 ;	COMMON\easyax5043.c:1536: AX5043_GPADCCTRL = 0x84;
      003A32 90 43 00         [24] 8679 	mov	dptr,#_AX5043_GPADCCTRL
      003A35 74 84            [12] 8680 	mov	a,#0x84
      003A37 F0               [24] 8681 	movx	@dptr,a
                                   8682 ;	COMMON\easyax5043.c:1537: do {} while (AX5043_GPADCCTRL & 0x80);
      003A38                       8683 00106$:
      003A38 90 43 00         [24] 8684 	mov	dptr,#_AX5043_GPADCCTRL
      003A3B E0               [24] 8685 	movx	a,@dptr
      003A3C FC               [12] 8686 	mov	r4,a
      003A3D 20 E7 F8         [24] 8687 	jb	acc.7,00106$
                                   8688 ;	COMMON\easyax5043.c:1539: int16_t x = AX5043_GPADC13VALUE1 & 0x03;
      003A40 90 43 08         [24] 8689 	mov	dptr,#_AX5043_GPADC13VALUE1
      003A43 E0               [24] 8690 	movx	a,@dptr
      003A44 FC               [12] 8691 	mov	r4,a
      003A45 53 04 03         [24] 8692 	anl	ar4,#0x03
                                   8693 ;	COMMON\easyax5043.c:1540: x <<= 8;
      003A48 8C 03            [24] 8694 	mov	ar3,r4
      003A4A 7C 00            [12] 8695 	mov	r4,#0x00
                                   8696 ;	COMMON\easyax5043.c:1541: x |= AX5043_GPADC13VALUE0;
      003A4C 90 43 09         [24] 8697 	mov	dptr,#_AX5043_GPADC13VALUE0
      003A4F E0               [24] 8698 	movx	a,@dptr
      003A50 F9               [12] 8699 	mov	r1,a
      003A51 7A 00            [12] 8700 	mov	r2,#0x00
      003A53 E9               [12] 8701 	mov	a,r1
      003A54 42 04            [12] 8702 	orl	ar4,a
      003A56 EA               [12] 8703 	mov	a,r2
      003A57 42 03            [12] 8704 	orl	ar3,a
                                   8705 ;	COMMON\easyax5043.c:1542: r += x;
      003A59 EC               [12] 8706 	mov	a,r4
      003A5A 2E               [12] 8707 	add	a,r6
      003A5B FE               [12] 8708 	mov	r6,a
      003A5C EB               [12] 8709 	mov	a,r3
      003A5D 3F               [12] 8710 	addc	a,r7
      003A5E FF               [12] 8711 	mov	r7,a
                                   8712 ;	COMMON\easyax5043.c:1544: } while (--cnt);
      003A5F DD D1            [24] 8713 	djnz	r5,00108$
                                   8714 ;	COMMON\easyax5043.c:1545: return r;
      003A61 8E 82            [24] 8715 	mov	dpl,r6
      003A63 8F 83            [24] 8716 	mov	dph,r7
      003A65 22               [24] 8717 	ret
                                   8718 ;------------------------------------------------------------
                                   8719 ;Allocation info for local variables in function 'axradio_adjustvcoi'
                                   8720 ;------------------------------------------------------------
                                   8721 ;rng                       Allocated to registers r7 
                                   8722 ;offs                      Allocated to registers r3 
                                   8723 ;bestrng                   Allocated to registers r4 
                                   8724 ;bestval                   Allocated to registers r5 r6 
                                   8725 ;val                       Allocated to stack - _bp +1
                                   8726 ;------------------------------------------------------------
                                   8727 ;	COMMON\easyax5043.c:1550: static __reentrantb uint8_t axradio_adjustvcoi(uint8_t rng) __reentrant
                                   8728 ;	-----------------------------------------
                                   8729 ;	 function axradio_adjustvcoi
                                   8730 ;	-----------------------------------------
      003A66                       8731 _axradio_adjustvcoi:
      003A66 C0 1F            [24] 8732 	push	_bp
      003A68 85 81 1F         [24] 8733 	mov	_bp,sp
      003A6B 05 81            [12] 8734 	inc	sp
      003A6D 05 81            [12] 8735 	inc	sp
      003A6F AF 82            [24] 8736 	mov	r7,dpl
                                   8737 ;	COMMON\easyax5043.c:1554: uint16_t bestval = ~0;
      003A71 7D FF            [12] 8738 	mov	r5,#0xff
      003A73 7E FF            [12] 8739 	mov	r6,#0xff
                                   8740 ;	COMMON\easyax5043.c:1555: rng &= 0x7F;
      003A75 53 07 7F         [24] 8741 	anl	ar7,#0x7f
                                   8742 ;	COMMON\easyax5043.c:1556: bestrng = rng;
      003A78 8F 04            [24] 8743 	mov	ar4,r7
                                   8744 ;	COMMON\easyax5043.c:1557: for (offs = 0; offs != 16; ++offs) {
      003A7A 7B 00            [12] 8745 	mov	r3,#0x00
      003A7C                       8746 00115$:
                                   8747 ;	COMMON\easyax5043.c:1559: if (!((uint8_t)(rng + offs) & 0xC0)) {
      003A7C EB               [12] 8748 	mov	a,r3
      003A7D 2F               [12] 8749 	add	a,r7
      003A7E 54 C0            [12] 8750 	anl	a,#0xc0
      003A80 60 02            [24] 8751 	jz	00144$
      003A82 80 42            [24] 8752 	sjmp	00104$
      003A84                       8753 00144$:
                                   8754 ;	COMMON\easyax5043.c:1560: AX5043_PLLVCOI = 0x80 | (rng + offs);
      003A84 C0 04            [24] 8755 	push	ar4
      003A86 EB               [12] 8756 	mov	a,r3
      003A87 2F               [12] 8757 	add	a,r7
      003A88 90 41 80         [24] 8758 	mov	dptr,#_AX5043_PLLVCOI
      003A8B 44 80            [12] 8759 	orl	a,#0x80
      003A8D F0               [24] 8760 	movx	@dptr,a
                                   8761 ;	COMMON\easyax5043.c:1561: val = axradio_tunevoltage();
      003A8E C0 07            [24] 8762 	push	ar7
      003A90 C0 06            [24] 8763 	push	ar6
      003A92 C0 05            [24] 8764 	push	ar5
      003A94 C0 03            [24] 8765 	push	ar3
      003A96 12 3A 1A         [24] 8766 	lcall	_axradio_tunevoltage
      003A99 AA 82            [24] 8767 	mov	r2,dpl
      003A9B AC 83            [24] 8768 	mov	r4,dph
      003A9D D0 03            [24] 8769 	pop	ar3
      003A9F D0 05            [24] 8770 	pop	ar5
      003AA1 D0 06            [24] 8771 	pop	ar6
      003AA3 D0 07            [24] 8772 	pop	ar7
      003AA5 A8 1F            [24] 8773 	mov	r0,_bp
      003AA7 08               [12] 8774 	inc	r0
      003AA8 A6 02            [24] 8775 	mov	@r0,ar2
      003AAA 08               [12] 8776 	inc	r0
      003AAB A6 04            [24] 8777 	mov	@r0,ar4
                                   8778 ;	COMMON\easyax5043.c:1562: if (val < bestval) {
      003AAD A8 1F            [24] 8779 	mov	r0,_bp
      003AAF 08               [12] 8780 	inc	r0
      003AB0 C3               [12] 8781 	clr	c
      003AB1 E6               [12] 8782 	mov	a,@r0
      003AB2 9D               [12] 8783 	subb	a,r5
      003AB3 08               [12] 8784 	inc	r0
      003AB4 E6               [12] 8785 	mov	a,@r0
      003AB5 9E               [12] 8786 	subb	a,r6
      003AB6 D0 04            [24] 8787 	pop	ar4
      003AB8 50 0C            [24] 8788 	jnc	00104$
                                   8789 ;	COMMON\easyax5043.c:1563: bestval = val;
      003ABA A8 1F            [24] 8790 	mov	r0,_bp
      003ABC 08               [12] 8791 	inc	r0
      003ABD 86 05            [24] 8792 	mov	ar5,@r0
      003ABF 08               [12] 8793 	inc	r0
      003AC0 86 06            [24] 8794 	mov	ar6,@r0
                                   8795 ;	COMMON\easyax5043.c:1564: bestrng = rng + offs;
      003AC2 EB               [12] 8796 	mov	a,r3
      003AC3 2F               [12] 8797 	add	a,r7
      003AC4 FA               [12] 8798 	mov	r2,a
      003AC5 FC               [12] 8799 	mov	r4,a
      003AC6                       8800 00104$:
                                   8801 ;	COMMON\easyax5043.c:1567: if (!offs)
      003AC6 EB               [12] 8802 	mov	a,r3
      003AC7 60 4D            [24] 8803 	jz	00111$
                                   8804 ;	COMMON\easyax5043.c:1569: if (!((uint8_t)(rng - offs) & 0xC0)) {
      003AC9 EF               [12] 8805 	mov	a,r7
      003ACA C3               [12] 8806 	clr	c
      003ACB 9B               [12] 8807 	subb	a,r3
      003ACC 54 C0            [12] 8808 	anl	a,#0xc0
      003ACE 60 02            [24] 8809 	jz	00148$
      003AD0 80 44            [24] 8810 	sjmp	00111$
      003AD2                       8811 00148$:
                                   8812 ;	COMMON\easyax5043.c:1570: AX5043_PLLVCOI = 0x80 | (rng - offs);
      003AD2 C0 04            [24] 8813 	push	ar4
      003AD4 EF               [12] 8814 	mov	a,r7
      003AD5 C3               [12] 8815 	clr	c
      003AD6 9B               [12] 8816 	subb	a,r3
      003AD7 90 41 80         [24] 8817 	mov	dptr,#_AX5043_PLLVCOI
      003ADA 44 80            [12] 8818 	orl	a,#0x80
      003ADC F0               [24] 8819 	movx	@dptr,a
                                   8820 ;	COMMON\easyax5043.c:1571: val = axradio_tunevoltage();
      003ADD C0 07            [24] 8821 	push	ar7
      003ADF C0 06            [24] 8822 	push	ar6
      003AE1 C0 05            [24] 8823 	push	ar5
      003AE3 C0 03            [24] 8824 	push	ar3
      003AE5 12 3A 1A         [24] 8825 	lcall	_axradio_tunevoltage
      003AE8 AA 82            [24] 8826 	mov	r2,dpl
      003AEA AC 83            [24] 8827 	mov	r4,dph
      003AEC D0 03            [24] 8828 	pop	ar3
      003AEE D0 05            [24] 8829 	pop	ar5
      003AF0 D0 06            [24] 8830 	pop	ar6
      003AF2 D0 07            [24] 8831 	pop	ar7
      003AF4 A8 1F            [24] 8832 	mov	r0,_bp
      003AF6 08               [12] 8833 	inc	r0
      003AF7 A6 02            [24] 8834 	mov	@r0,ar2
      003AF9 08               [12] 8835 	inc	r0
      003AFA A6 04            [24] 8836 	mov	@r0,ar4
                                   8837 ;	COMMON\easyax5043.c:1572: if (val < bestval) {
      003AFC A8 1F            [24] 8838 	mov	r0,_bp
      003AFE 08               [12] 8839 	inc	r0
      003AFF C3               [12] 8840 	clr	c
      003B00 E6               [12] 8841 	mov	a,@r0
      003B01 9D               [12] 8842 	subb	a,r5
      003B02 08               [12] 8843 	inc	r0
      003B03 E6               [12] 8844 	mov	a,@r0
      003B04 9E               [12] 8845 	subb	a,r6
      003B05 D0 04            [24] 8846 	pop	ar4
      003B07 50 0D            [24] 8847 	jnc	00111$
                                   8848 ;	COMMON\easyax5043.c:1573: bestval = val;
      003B09 A8 1F            [24] 8849 	mov	r0,_bp
      003B0B 08               [12] 8850 	inc	r0
      003B0C 86 05            [24] 8851 	mov	ar5,@r0
      003B0E 08               [12] 8852 	inc	r0
      003B0F 86 06            [24] 8853 	mov	ar6,@r0
                                   8854 ;	COMMON\easyax5043.c:1574: bestrng = rng - offs;
      003B11 EF               [12] 8855 	mov	a,r7
      003B12 C3               [12] 8856 	clr	c
      003B13 9B               [12] 8857 	subb	a,r3
      003B14 FA               [12] 8858 	mov	r2,a
      003B15 FC               [12] 8859 	mov	r4,a
      003B16                       8860 00111$:
                                   8861 ;	COMMON\easyax5043.c:1557: for (offs = 0; offs != 16; ++offs) {
      003B16 0B               [12] 8862 	inc	r3
      003B17 BB 10 02         [24] 8863 	cjne	r3,#0x10,00150$
      003B1A 80 03            [24] 8864 	sjmp	00151$
      003B1C                       8865 00150$:
      003B1C 02 3A 7C         [24] 8866 	ljmp	00115$
      003B1F                       8867 00151$:
                                   8868 ;	COMMON\easyax5043.c:1579: if (bestval <= 0x0010)
      003B1F C3               [12] 8869 	clr	c
      003B20 74 10            [12] 8870 	mov	a,#0x10
      003B22 9D               [12] 8871 	subb	a,r5
      003B23 E4               [12] 8872 	clr	a
      003B24 9E               [12] 8873 	subb	a,r6
      003B25 40 07            [24] 8874 	jc	00114$
                                   8875 ;	COMMON\easyax5043.c:1580: return rng | 0x80;
      003B27 43 07 80         [24] 8876 	orl	ar7,#0x80
      003B2A 8F 82            [24] 8877 	mov	dpl,r7
      003B2C 80 05            [24] 8878 	sjmp	00116$
      003B2E                       8879 00114$:
                                   8880 ;	COMMON\easyax5043.c:1581: return bestrng | 0x80;
      003B2E 43 04 80         [24] 8881 	orl	ar4,#0x80
      003B31 8C 82            [24] 8882 	mov	dpl,r4
      003B33                       8883 00116$:
      003B33 85 1F 81         [24] 8884 	mov	sp,_bp
      003B36 D0 1F            [24] 8885 	pop	_bp
      003B38 22               [24] 8886 	ret
                                   8887 ;------------------------------------------------------------
                                   8888 ;Allocation info for local variables in function 'axradio_calvcoi'
                                   8889 ;------------------------------------------------------------
                                   8890 ;i                         Allocated to registers r2 
                                   8891 ;r                         Allocated to registers r7 
                                   8892 ;vmin                      Allocated to registers r5 r6 
                                   8893 ;vmax                      Allocated to stack - _bp +1
                                   8894 ;curtune                   Allocated to registers r3 r4 
                                   8895 ;------------------------------------------------------------
                                   8896 ;	COMMON\easyax5043.c:1584: static __reentrantb uint8_t axradio_calvcoi(void) __reentrant
                                   8897 ;	-----------------------------------------
                                   8898 ;	 function axradio_calvcoi
                                   8899 ;	-----------------------------------------
      003B39                       8900 _axradio_calvcoi:
      003B39 C0 1F            [24] 8901 	push	_bp
      003B3B 85 81 1F         [24] 8902 	mov	_bp,sp
      003B3E 05 81            [12] 8903 	inc	sp
      003B40 05 81            [12] 8904 	inc	sp
                                   8905 ;	COMMON\easyax5043.c:1587: uint8_t r = 0;
      003B42 7F 00            [12] 8906 	mov	r7,#0x00
                                   8907 ;	COMMON\easyax5043.c:1588: uint16_t vmin = 0xffff;
      003B44 7D FF            [12] 8908 	mov	r5,#0xff
      003B46 7E FF            [12] 8909 	mov	r6,#0xff
                                   8910 ;	COMMON\easyax5043.c:1589: uint16_t vmax = 0x0000;
      003B48 A8 1F            [24] 8911 	mov	r0,_bp
      003B4A 08               [12] 8912 	inc	r0
      003B4B E4               [12] 8913 	clr	a
      003B4C F6               [12] 8914 	mov	@r0,a
      003B4D 08               [12] 8915 	inc	r0
      003B4E F6               [12] 8916 	mov	@r0,a
                                   8917 ;	COMMON\easyax5043.c:1590: for (i = 0x40; i != 0;) {
      003B4F 7A 40            [12] 8918 	mov	r2,#0x40
      003B51                       8919 00113$:
                                   8920 ;	COMMON\easyax5043.c:1592: --i;
      003B51 C0 07            [24] 8921 	push	ar7
      003B53 1A               [12] 8922 	dec	r2
                                   8923 ;	COMMON\easyax5043.c:1593: AX5043_PLLVCOI = 0x80 | i;
      003B54 90 41 80         [24] 8924 	mov	dptr,#_AX5043_PLLVCOI
      003B57 74 80            [12] 8925 	mov	a,#0x80
      003B59 4A               [12] 8926 	orl	a,r2
      003B5A F0               [24] 8927 	movx	@dptr,a
                                   8928 ;	COMMON\easyax5043.c:1594: AX5043_PLLRANGINGA; // clear PLL lock loss
      003B5B 90 40 33         [24] 8929 	mov	dptr,#_AX5043_PLLRANGINGA
      003B5E E0               [24] 8930 	movx	a,@dptr
                                   8931 ;	COMMON\easyax5043.c:1595: curtune = axradio_tunevoltage();
      003B5F C0 06            [24] 8932 	push	ar6
      003B61 C0 05            [24] 8933 	push	ar5
      003B63 C0 02            [24] 8934 	push	ar2
      003B65 12 3A 1A         [24] 8935 	lcall	_axradio_tunevoltage
      003B68 AF 82            [24] 8936 	mov	r7,dpl
      003B6A AC 83            [24] 8937 	mov	r4,dph
      003B6C D0 02            [24] 8938 	pop	ar2
      003B6E D0 05            [24] 8939 	pop	ar5
      003B70 D0 06            [24] 8940 	pop	ar6
      003B72 8F 03            [24] 8941 	mov	ar3,r7
                                   8942 ;	COMMON\easyax5043.c:1596: AX5043_PLLRANGINGA; // clear PLL lock loss
      003B74 90 40 33         [24] 8943 	mov	dptr,#_AX5043_PLLRANGINGA
      003B77 E0               [24] 8944 	movx	a,@dptr
                                   8945 ;	COMMON\easyax5043.c:1597: ((uint16_t __xdata *)axradio_rxbuffer)[i] = curtune;
      003B78 EA               [12] 8946 	mov	a,r2
      003B79 75 F0 02         [24] 8947 	mov	b,#0x02
      003B7C A4               [48] 8948 	mul	ab
      003B7D 24 D0            [12] 8949 	add	a,#_axradio_rxbuffer
      003B7F F5 82            [12] 8950 	mov	dpl,a
      003B81 74 01            [12] 8951 	mov	a,#(_axradio_rxbuffer >> 8)
      003B83 35 F0            [12] 8952 	addc	a,b
      003B85 F5 83            [12] 8953 	mov	dph,a
      003B87 EB               [12] 8954 	mov	a,r3
      003B88 F0               [24] 8955 	movx	@dptr,a
      003B89 EC               [12] 8956 	mov	a,r4
      003B8A A3               [24] 8957 	inc	dptr
      003B8B F0               [24] 8958 	movx	@dptr,a
                                   8959 ;	COMMON\easyax5043.c:1598: if (curtune > vmax)
      003B8C A8 1F            [24] 8960 	mov	r0,_bp
      003B8E 08               [12] 8961 	inc	r0
      003B8F C3               [12] 8962 	clr	c
      003B90 E6               [12] 8963 	mov	a,@r0
      003B91 9B               [12] 8964 	subb	a,r3
      003B92 08               [12] 8965 	inc	r0
      003B93 E6               [12] 8966 	mov	a,@r0
      003B94 9C               [12] 8967 	subb	a,r4
      003B95 D0 07            [24] 8968 	pop	ar7
      003B97 50 08            [24] 8969 	jnc	00102$
                                   8970 ;	COMMON\easyax5043.c:1599: vmax = curtune;
      003B99 A8 1F            [24] 8971 	mov	r0,_bp
      003B9B 08               [12] 8972 	inc	r0
      003B9C A6 03            [24] 8973 	mov	@r0,ar3
      003B9E 08               [12] 8974 	inc	r0
      003B9F A6 04            [24] 8975 	mov	@r0,ar4
      003BA1                       8976 00102$:
                                   8977 ;	COMMON\easyax5043.c:1600: if (curtune < vmin) {
      003BA1 C3               [12] 8978 	clr	c
      003BA2 EB               [12] 8979 	mov	a,r3
      003BA3 9D               [12] 8980 	subb	a,r5
      003BA4 EC               [12] 8981 	mov	a,r4
      003BA5 9E               [12] 8982 	subb	a,r6
      003BA6 50 14            [24] 8983 	jnc	00114$
                                   8984 ;	COMMON\easyax5043.c:1601: vmin = curtune;
      003BA8 8B 05            [24] 8985 	mov	ar5,r3
      003BAA 8C 06            [24] 8986 	mov	ar6,r4
                                   8987 ;	COMMON\easyax5043.c:1603: if (!(0xC0 & (uint8_t)~AX5043_PLLRANGINGA))
      003BAC 90 40 33         [24] 8988 	mov	dptr,#_AX5043_PLLRANGINGA
      003BAF E0               [24] 8989 	movx	a,@dptr
      003BB0 F4               [12] 8990 	cpl	a
      003BB1 FC               [12] 8991 	mov	r4,a
      003BB2 54 C0            [12] 8992 	anl	a,#0xc0
      003BB4 60 02            [24] 8993 	jz	00147$
      003BB6 80 04            [24] 8994 	sjmp	00114$
      003BB8                       8995 00147$:
                                   8996 ;	COMMON\easyax5043.c:1604: r = i | 0x80;
      003BB8 74 80            [12] 8997 	mov	a,#0x80
      003BBA 4A               [12] 8998 	orl	a,r2
      003BBB FF               [12] 8999 	mov	r7,a
      003BBC                       9000 00114$:
                                   9001 ;	COMMON\easyax5043.c:1590: for (i = 0x40; i != 0;) {
      003BBC EA               [12] 9002 	mov	a,r2
      003BBD 70 92            [24] 9003 	jnz	00113$
                                   9004 ;	COMMON\easyax5043.c:1607: if (!(r & 0x80) || vmax >= 0xFF00 || vmin < 0x0100 || vmax - vmin < 0x6000)
      003BBF EF               [12] 9005 	mov	a,r7
      003BC0 30 E7 1F         [24] 9006 	jnb	acc.7,00108$
      003BC3 A8 1F            [24] 9007 	mov	r0,_bp
      003BC5 08               [12] 9008 	inc	r0
      003BC6 C3               [12] 9009 	clr	c
      003BC7 08               [12] 9010 	inc	r0
      003BC8 E6               [12] 9011 	mov	a,@r0
      003BC9 94 FF            [12] 9012 	subb	a,#0xff
      003BCB 50 15            [24] 9013 	jnc	00108$
      003BCD 74 FF            [12] 9014 	mov	a,#0x100 - 0x01
      003BCF 2E               [12] 9015 	add	a,r6
      003BD0 50 10            [24] 9016 	jnc	00108$
      003BD2 A8 1F            [24] 9017 	mov	r0,_bp
      003BD4 08               [12] 9018 	inc	r0
      003BD5 E6               [12] 9019 	mov	a,@r0
      003BD6 C3               [12] 9020 	clr	c
      003BD7 9D               [12] 9021 	subb	a,r5
      003BD8 FD               [12] 9022 	mov	r5,a
      003BD9 08               [12] 9023 	inc	r0
      003BDA E6               [12] 9024 	mov	a,@r0
      003BDB 9E               [12] 9025 	subb	a,r6
      003BDC FE               [12] 9026 	mov	r6,a
      003BDD C3               [12] 9027 	clr	c
      003BDE 94 60            [12] 9028 	subb	a,#0x60
      003BE0 50 05            [24] 9029 	jnc	00109$
      003BE2                       9030 00108$:
                                   9031 ;	COMMON\easyax5043.c:1608: return 0;
      003BE2 75 82 00         [24] 9032 	mov	dpl,#0x00
      003BE5 80 02            [24] 9033 	sjmp	00115$
      003BE7                       9034 00109$:
                                   9035 ;	COMMON\easyax5043.c:1609: return r;
      003BE7 8F 82            [24] 9036 	mov	dpl,r7
      003BE9                       9037 00115$:
      003BE9 85 1F 81         [24] 9038 	mov	sp,_bp
      003BEC D0 1F            [24] 9039 	pop	_bp
      003BEE 22               [24] 9040 	ret
                                   9041 ;------------------------------------------------------------
                                   9042 ;Allocation info for local variables in function 'axradio_init'
                                   9043 ;------------------------------------------------------------
                                   9044 ;i                         Allocated to registers r7 
                                   9045 ;iesave                    Allocated to registers r6 
                                   9046 ;f                         Allocated to registers r3 r4 r5 r6 
                                   9047 ;r                         Allocated to registers r5 
                                   9048 ;vcoisave                  Allocated to registers r7 
                                   9049 ;f                         Allocated to registers r0 r1 r2 r3 
                                   9050 ;f                         Allocated to registers r4 r5 r6 r7 
                                   9051 ;x                         Allocated with name '_axradio_init_x_3_396'
                                   9052 ;j                         Allocated with name '_axradio_init_j_3_397'
                                   9053 ;x                         Allocated with name '_axradio_init_x_6_401'
                                   9054 ;chg                       Allocated with name '_axradio_init_chg_4_404'
                                   9055 ;------------------------------------------------------------
                                   9056 ;	COMMON\easyax5043.c:1616: uint8_t axradio_init(void)
                                   9057 ;	-----------------------------------------
                                   9058 ;	 function axradio_init
                                   9059 ;	-----------------------------------------
      003BEF                       9060 _axradio_init:
                                   9061 ;	COMMON\easyax5043.c:1619: axradio_mode = AXRADIO_MODE_UNINIT;
      003BEF 75 0B 00         [24] 9062 	mov	_axradio_mode,#0x00
                                   9063 ;	COMMON\easyax5043.c:1620: axradio_killallcb();
      003BF2 12 39 F0         [24] 9064 	lcall	_axradio_killallcb
                                   9065 ;	COMMON\easyax5043.c:1621: axradio_cb_receive.cb.handler = axradio_receive_callback_fwd;
      003BF5 90 02 D6         [24] 9066 	mov	dptr,#(_axradio_cb_receive + 0x0002)
      003BF8 74 C7            [12] 9067 	mov	a,#_axradio_receive_callback_fwd
      003BFA F0               [24] 9068 	movx	@dptr,a
      003BFB 74 34            [12] 9069 	mov	a,#(_axradio_receive_callback_fwd >> 8)
      003BFD A3               [24] 9070 	inc	dptr
      003BFE F0               [24] 9071 	movx	@dptr,a
                                   9072 ;	COMMON\easyax5043.c:1622: axradio_cb_receive.st.status = AXRADIO_STAT_RECEIVE;
      003BFF 90 02 D8         [24] 9073 	mov	dptr,#(_axradio_cb_receive + 0x0004)
      003C02 E4               [12] 9074 	clr	a
      003C03 F0               [24] 9075 	movx	@dptr,a
                                   9076 ;	COMMON\easyax5043.c:1623: memset_xdata(axradio_cb_receive.st.rx.mac.remoteaddr, 0, sizeof(axradio_cb_receive.st.rx.mac.remoteaddr));
      003C04 90 03 C6         [24] 9077 	mov	dptr,#_memset_PARM_2
      003C07 F0               [24] 9078 	movx	@dptr,a
      003C08 90 03 C7         [24] 9079 	mov	dptr,#_memset_PARM_3
      003C0B 74 04            [12] 9080 	mov	a,#0x04
      003C0D F0               [24] 9081 	movx	@dptr,a
      003C0E E4               [12] 9082 	clr	a
      003C0F A3               [24] 9083 	inc	dptr
      003C10 F0               [24] 9084 	movx	@dptr,a
      003C11 90 02 E8         [24] 9085 	mov	dptr,#(_axradio_cb_receive + 0x0014)
      003C14 75 F0 00         [24] 9086 	mov	b,#0x00
      003C17 12 69 59         [24] 9087 	lcall	_memset
                                   9088 ;	COMMON\easyax5043.c:1624: memset_xdata(axradio_cb_receive.st.rx.mac.localaddr, 0, sizeof(axradio_cb_receive.st.rx.mac.localaddr));
      003C1A 90 03 C6         [24] 9089 	mov	dptr,#_memset_PARM_2
      003C1D E4               [12] 9090 	clr	a
      003C1E F0               [24] 9091 	movx	@dptr,a
      003C1F 90 03 C7         [24] 9092 	mov	dptr,#_memset_PARM_3
      003C22 74 04            [12] 9093 	mov	a,#0x04
      003C24 F0               [24] 9094 	movx	@dptr,a
      003C25 E4               [12] 9095 	clr	a
      003C26 A3               [24] 9096 	inc	dptr
      003C27 F0               [24] 9097 	movx	@dptr,a
      003C28 90 02 EC         [24] 9098 	mov	dptr,#(_axradio_cb_receive + 0x0018)
      003C2B 75 F0 00         [24] 9099 	mov	b,#0x00
      003C2E 12 69 59         [24] 9100 	lcall	_memset
                                   9101 ;	COMMON\easyax5043.c:1625: axradio_cb_receivesfd.cb.handler = axradio_callback_fwd;
      003C31 90 02 F8         [24] 9102 	mov	dptr,#(_axradio_cb_receivesfd + 0x0002)
      003C34 74 B5            [12] 9103 	mov	a,#_axradio_callback_fwd
      003C36 F0               [24] 9104 	movx	@dptr,a
      003C37 74 34            [12] 9105 	mov	a,#(_axradio_callback_fwd >> 8)
      003C39 A3               [24] 9106 	inc	dptr
      003C3A F0               [24] 9107 	movx	@dptr,a
                                   9108 ;	COMMON\easyax5043.c:1626: axradio_cb_receivesfd.st.status = AXRADIO_STAT_RECEIVESFD;
      003C3B 90 02 FA         [24] 9109 	mov	dptr,#(_axradio_cb_receivesfd + 0x0004)
      003C3E 74 01            [12] 9110 	mov	a,#0x01
      003C40 F0               [24] 9111 	movx	@dptr,a
                                   9112 ;	COMMON\easyax5043.c:1627: axradio_cb_channelstate.cb.handler = axradio_callback_fwd;
      003C41 90 03 02         [24] 9113 	mov	dptr,#(_axradio_cb_channelstate + 0x0002)
      003C44 74 B5            [12] 9114 	mov	a,#_axradio_callback_fwd
      003C46 F0               [24] 9115 	movx	@dptr,a
      003C47 74 34            [12] 9116 	mov	a,#(_axradio_callback_fwd >> 8)
      003C49 A3               [24] 9117 	inc	dptr
      003C4A F0               [24] 9118 	movx	@dptr,a
                                   9119 ;	COMMON\easyax5043.c:1628: axradio_cb_channelstate.st.status = AXRADIO_STAT_CHANNELSTATE;
      003C4B 90 03 04         [24] 9120 	mov	dptr,#(_axradio_cb_channelstate + 0x0004)
      003C4E 74 02            [12] 9121 	mov	a,#0x02
      003C50 F0               [24] 9122 	movx	@dptr,a
                                   9123 ;	COMMON\easyax5043.c:1629: axradio_cb_transmitstart.cb.handler = axradio_callback_fwd;
      003C51 90 03 0F         [24] 9124 	mov	dptr,#(_axradio_cb_transmitstart + 0x0002)
      003C54 74 B5            [12] 9125 	mov	a,#_axradio_callback_fwd
      003C56 F0               [24] 9126 	movx	@dptr,a
      003C57 74 34            [12] 9127 	mov	a,#(_axradio_callback_fwd >> 8)
      003C59 A3               [24] 9128 	inc	dptr
      003C5A F0               [24] 9129 	movx	@dptr,a
                                   9130 ;	COMMON\easyax5043.c:1630: axradio_cb_transmitstart.st.status = AXRADIO_STAT_TRANSMITSTART;
      003C5B 90 03 11         [24] 9131 	mov	dptr,#(_axradio_cb_transmitstart + 0x0004)
      003C5E 74 03            [12] 9132 	mov	a,#0x03
      003C60 F0               [24] 9133 	movx	@dptr,a
                                   9134 ;	COMMON\easyax5043.c:1631: axradio_cb_transmitend.cb.handler = axradio_callback_fwd;
      003C61 90 03 19         [24] 9135 	mov	dptr,#(_axradio_cb_transmitend + 0x0002)
      003C64 74 B5            [12] 9136 	mov	a,#_axradio_callback_fwd
      003C66 F0               [24] 9137 	movx	@dptr,a
      003C67 74 34            [12] 9138 	mov	a,#(_axradio_callback_fwd >> 8)
      003C69 A3               [24] 9139 	inc	dptr
      003C6A F0               [24] 9140 	movx	@dptr,a
                                   9141 ;	COMMON\easyax5043.c:1632: axradio_cb_transmitend.st.status = AXRADIO_STAT_TRANSMITEND;
      003C6B 90 03 1B         [24] 9142 	mov	dptr,#(_axradio_cb_transmitend + 0x0004)
      003C6E 74 04            [12] 9143 	mov	a,#0x04
      003C70 F0               [24] 9144 	movx	@dptr,a
                                   9145 ;	COMMON\easyax5043.c:1633: axradio_cb_transmitdata.cb.handler = axradio_callback_fwd;
      003C71 90 03 23         [24] 9146 	mov	dptr,#(_axradio_cb_transmitdata + 0x0002)
      003C74 74 B5            [12] 9147 	mov	a,#_axradio_callback_fwd
      003C76 F0               [24] 9148 	movx	@dptr,a
      003C77 74 34            [12] 9149 	mov	a,#(_axradio_callback_fwd >> 8)
      003C79 A3               [24] 9150 	inc	dptr
      003C7A F0               [24] 9151 	movx	@dptr,a
                                   9152 ;	COMMON\easyax5043.c:1634: axradio_cb_transmitdata.st.status = AXRADIO_STAT_TRANSMITDATA;
      003C7B 90 03 25         [24] 9153 	mov	dptr,#(_axradio_cb_transmitdata + 0x0004)
      003C7E 74 05            [12] 9154 	mov	a,#0x05
      003C80 F0               [24] 9155 	movx	@dptr,a
                                   9156 ;	COMMON\easyax5043.c:1635: axradio_timer.handler = axradio_timer_callback;
      003C81 90 03 2D         [24] 9157 	mov	dptr,#(_axradio_timer + 0x0002)
      003C84 74 7C            [12] 9158 	mov	a,#_axradio_timer_callback
      003C86 F0               [24] 9159 	movx	@dptr,a
      003C87 74 2C            [12] 9160 	mov	a,#(_axradio_timer_callback >> 8)
      003C89 A3               [24] 9161 	inc	dptr
      003C8A F0               [24] 9162 	movx	@dptr,a
                                   9163 ;	COMMON\easyax5043.c:1636: axradio_curchannel = 0;
      003C8B 90 00 AB         [24] 9164 	mov	dptr,#_axradio_curchannel
      003C8E E4               [12] 9165 	clr	a
      003C8F F0               [24] 9166 	movx	@dptr,a
                                   9167 ;	COMMON\easyax5043.c:1637: axradio_curfreqoffset = 0;
      003C90 90 00 AC         [24] 9168 	mov	dptr,#_axradio_curfreqoffset
      003C93 F0               [24] 9169 	movx	@dptr,a
      003C94 A3               [24] 9170 	inc	dptr
      003C95 F0               [24] 9171 	movx	@dptr,a
      003C96 A3               [24] 9172 	inc	dptr
      003C97 F0               [24] 9173 	movx	@dptr,a
      003C98 A3               [24] 9174 	inc	dptr
      003C99 F0               [24] 9175 	movx	@dptr,a
                                   9176 ;	COMMON\easyax5043.c:1638: IE_4 = 0;
      003C9A C2 AC            [12] 9177 	clr	_IE_4
                                   9178 ;	COMMON\easyax5043.c:1639: axradio_trxstate = trxstate_off;
      003C9C 75 0C 00         [24] 9179 	mov	_axradio_trxstate,#0x00
                                   9180 ;	COMMON\easyax5043.c:1640: if (ax5043_reset())
      003C9F 12 64 FB         [24] 9181 	lcall	_ax5043_reset
      003CA2 E5 82            [12] 9182 	mov	a,dpl
      003CA4 60 04            [24] 9183 	jz	00102$
                                   9184 ;	COMMON\easyax5043.c:1641: return AXRADIO_ERR_NOCHIP;
      003CA6 75 82 05         [24] 9185 	mov	dpl,#0x05
      003CA9 22               [24] 9186 	ret
      003CAA                       9187 00102$:
                                   9188 ;	COMMON\easyax5043.c:1642: ax5043_init_registers();
      003CAA 12 2A 31         [24] 9189 	lcall	_ax5043_init_registers
                                   9190 ;	COMMON\easyax5043.c:1643: ax5043_set_registers_tx();
      003CAD 12 15 24         [24] 9191 	lcall	_ax5043_set_registers_tx
                                   9192 ;	COMMON\easyax5043.c:1644: AX5043_PLLLOOP = 0x09; // default 100kHz loop BW for ranging
      003CB0 90 40 30         [24] 9193 	mov	dptr,#_AX5043_PLLLOOP
      003CB3 74 09            [12] 9194 	mov	a,#0x09
      003CB5 F0               [24] 9195 	movx	@dptr,a
                                   9196 ;	COMMON\easyax5043.c:1645: AX5043_PLLCPI = 0x08;
      003CB6 90 40 31         [24] 9197 	mov	dptr,#_AX5043_PLLCPI
      003CB9 14               [12] 9198 	dec	a
      003CBA F0               [24] 9199 	movx	@dptr,a
                                   9200 ;	COMMON\easyax5043.c:1647: IE_4 = 1;
      003CBB D2 AC            [12] 9201 	setb	_IE_4
                                   9202 ;	COMMON\easyax5043.c:1649: AX5043_PWRMODE = AX5043_PWRSTATE_XTAL_ON;
      003CBD 90 40 02         [24] 9203 	mov	dptr,#_AX5043_PWRMODE
      003CC0 74 05            [12] 9204 	mov	a,#0x05
      003CC2 F0               [24] 9205 	movx	@dptr,a
                                   9206 ;	COMMON\easyax5043.c:1650: AX5043_MODULATION = 0x08;
      003CC3 90 40 10         [24] 9207 	mov	dptr,#_AX5043_MODULATION
      003CC6 74 08            [12] 9208 	mov	a,#0x08
      003CC8 F0               [24] 9209 	movx	@dptr,a
                                   9210 ;	COMMON\easyax5043.c:1651: AX5043_FSKDEV2 = 0x00;
      003CC9 90 41 61         [24] 9211 	mov	dptr,#_AX5043_FSKDEV2
      003CCC E4               [12] 9212 	clr	a
      003CCD F0               [24] 9213 	movx	@dptr,a
                                   9214 ;	COMMON\easyax5043.c:1652: AX5043_FSKDEV1 = 0x00;
      003CCE 90 41 62         [24] 9215 	mov	dptr,#_AX5043_FSKDEV1
      003CD1 F0               [24] 9216 	movx	@dptr,a
                                   9217 ;	COMMON\easyax5043.c:1653: AX5043_FSKDEV0 = 0x00;
      003CD2 90 41 63         [24] 9218 	mov	dptr,#_AX5043_FSKDEV0
      003CD5 F0               [24] 9219 	movx	@dptr,a
                                   9220 ;	COMMON\easyax5043.c:1654: axradio_wait_for_xtal();
      003CD6 12 28 DB         [24] 9221 	lcall	_axradio_wait_for_xtal
                                   9222 ;	COMMON\easyax5043.c:1655: for (i = 0; i < axradio_phy_nrchannels; ++i) {
      003CD9 7F 00            [12] 9223 	mov	r7,#0x00
      003CDB                       9224 00149$:
      003CDB 90 7F CB         [24] 9225 	mov	dptr,#_axradio_phy_nrchannels
      003CDE E4               [12] 9226 	clr	a
      003CDF 93               [24] 9227 	movc	a,@a+dptr
      003CE0 FE               [12] 9228 	mov	r6,a
      003CE1 C3               [12] 9229 	clr	c
      003CE2 EF               [12] 9230 	mov	a,r7
      003CE3 9E               [12] 9231 	subb	a,r6
      003CE4 40 03            [24] 9232 	jc	00267$
      003CE6 02 3D AE         [24] 9233 	ljmp	00113$
      003CE9                       9234 00267$:
                                   9235 ;	COMMON\easyax5043.c:1658: uint32_t __autodata f = axradio_phy_chanfreq[i];
      003CE9 EF               [12] 9236 	mov	a,r7
      003CEA 75 F0 04         [24] 9237 	mov	b,#0x04
      003CED A4               [48] 9238 	mul	ab
      003CEE 24 CC            [12] 9239 	add	a,#_axradio_phy_chanfreq
      003CF0 F5 82            [12] 9240 	mov	dpl,a
      003CF2 74 7F            [12] 9241 	mov	a,#(_axradio_phy_chanfreq >> 8)
      003CF4 35 F0            [12] 9242 	addc	a,b
      003CF6 F5 83            [12] 9243 	mov	dph,a
      003CF8 E4               [12] 9244 	clr	a
      003CF9 93               [24] 9245 	movc	a,@a+dptr
      003CFA FB               [12] 9246 	mov	r3,a
      003CFB A3               [24] 9247 	inc	dptr
      003CFC E4               [12] 9248 	clr	a
      003CFD 93               [24] 9249 	movc	a,@a+dptr
      003CFE FC               [12] 9250 	mov	r4,a
      003CFF A3               [24] 9251 	inc	dptr
      003D00 E4               [12] 9252 	clr	a
      003D01 93               [24] 9253 	movc	a,@a+dptr
      003D02 FD               [12] 9254 	mov	r5,a
      003D03 A3               [24] 9255 	inc	dptr
      003D04 E4               [12] 9256 	clr	a
      003D05 93               [24] 9257 	movc	a,@a+dptr
      003D06 FE               [12] 9258 	mov	r6,a
                                   9259 ;	COMMON\easyax5043.c:1659: AX5043_FREQA0 = f;
      003D07 90 40 37         [24] 9260 	mov	dptr,#_AX5043_FREQA0
      003D0A EB               [12] 9261 	mov	a,r3
      003D0B F0               [24] 9262 	movx	@dptr,a
                                   9263 ;	COMMON\easyax5043.c:1660: AX5043_FREQA1 = f >> 8;
      003D0C 90 40 36         [24] 9264 	mov	dptr,#_AX5043_FREQA1
      003D0F EC               [12] 9265 	mov	a,r4
      003D10 F0               [24] 9266 	movx	@dptr,a
                                   9267 ;	COMMON\easyax5043.c:1661: AX5043_FREQA2 = f >> 16;
      003D11 90 40 35         [24] 9268 	mov	dptr,#_AX5043_FREQA2
      003D14 ED               [12] 9269 	mov	a,r5
      003D15 F0               [24] 9270 	movx	@dptr,a
                                   9271 ;	COMMON\easyax5043.c:1662: AX5043_FREQA3 = f >> 24;
      003D16 90 40 34         [24] 9272 	mov	dptr,#_AX5043_FREQA3
      003D19 EE               [12] 9273 	mov	a,r6
      003D1A F0               [24] 9274 	movx	@dptr,a
                                   9275 ;	COMMON\easyax5043.c:1664: iesave = IE & 0x80;
      003D1B 74 80            [12] 9276 	mov	a,#0x80
      003D1D 55 A8            [12] 9277 	anl	a,_IE
      003D1F FE               [12] 9278 	mov	r6,a
                                   9279 ;	COMMON\easyax5043.c:1665: EA = 0;
      003D20 C2 AF            [12] 9280 	clr	_EA
                                   9281 ;	COMMON\easyax5043.c:1666: axradio_trxstate = trxstate_pll_ranging;
      003D22 75 0C 05         [24] 9282 	mov	_axradio_trxstate,#0x05
                                   9283 ;	COMMON\easyax5043.c:1667: AX5043_IRQMASK1 = 0x10; // enable pll autoranging done interrupt
      003D25 90 40 06         [24] 9284 	mov	dptr,#_AX5043_IRQMASK1
      003D28 74 10            [12] 9285 	mov	a,#0x10
      003D2A F0               [24] 9286 	movx	@dptr,a
                                   9287 ;	COMMON\easyax5043.c:1670: if( !(axradio_phy_chanpllrnginit[0] & 0xF0) ) { // start values for ranging available
      003D2B 90 7F D0         [24] 9288 	mov	dptr,#_axradio_phy_chanpllrnginit
      003D2E E4               [12] 9289 	clr	a
      003D2F 93               [24] 9290 	movc	a,@a+dptr
      003D30 FD               [12] 9291 	mov	r5,a
      003D31 54 F0            [12] 9292 	anl	a,#0xf0
      003D33 70 0B            [24] 9293 	jnz	00108$
                                   9294 ;	COMMON\easyax5043.c:1671: r = axradio_phy_chanpllrnginit[i] | 0x10;
      003D35 EF               [12] 9295 	mov	a,r7
      003D36 90 7F D0         [24] 9296 	mov	dptr,#_axradio_phy_chanpllrnginit
      003D39 93               [24] 9297 	movc	a,@a+dptr
      003D3A FD               [12] 9298 	mov	r5,a
      003D3B 43 05 10         [24] 9299 	orl	ar5,#0x10
      003D3E 80 25            [24] 9300 	sjmp	00109$
      003D40                       9301 00108$:
                                   9302 ;	COMMON\easyax5043.c:1674: r = 0x18;
      003D40 7D 18            [12] 9303 	mov	r5,#0x18
                                   9304 ;	COMMON\easyax5043.c:1675: if (i) {
      003D42 EF               [12] 9305 	mov	a,r7
      003D43 60 20            [24] 9306 	jz	00109$
                                   9307 ;	COMMON\easyax5043.c:1676: r = axradio_phy_chanpllrng[i - 1];
      003D45 8F 03            [24] 9308 	mov	ar3,r7
      003D47 7C 00            [12] 9309 	mov	r4,#0x00
      003D49 1B               [12] 9310 	dec	r3
      003D4A BB FF 01         [24] 9311 	cjne	r3,#0xff,00271$
      003D4D 1C               [12] 9312 	dec	r4
      003D4E                       9313 00271$:
      003D4E EB               [12] 9314 	mov	a,r3
      003D4F 24 A0            [12] 9315 	add	a,#_axradio_phy_chanpllrng
      003D51 F5 82            [12] 9316 	mov	dpl,a
      003D53 EC               [12] 9317 	mov	a,r4
      003D54 34 00            [12] 9318 	addc	a,#(_axradio_phy_chanpllrng >> 8)
      003D56 F5 83            [12] 9319 	mov	dph,a
      003D58 E0               [24] 9320 	movx	a,@dptr
                                   9321 ;	COMMON\easyax5043.c:1677: if (r & 0x20)
      003D59 FD               [12] 9322 	mov	r5,a
      003D5A 30 E5 02         [24] 9323 	jnb	acc.5,00104$
                                   9324 ;	COMMON\easyax5043.c:1678: r = 0x08;
      003D5D 7D 08            [12] 9325 	mov	r5,#0x08
      003D5F                       9326 00104$:
                                   9327 ;	COMMON\easyax5043.c:1679: r &= 0x0F;
      003D5F 53 05 0F         [24] 9328 	anl	ar5,#0x0f
                                   9329 ;	COMMON\easyax5043.c:1680: r |= 0x10;
      003D62 43 05 10         [24] 9330 	orl	ar5,#0x10
      003D65                       9331 00109$:
                                   9332 ;	COMMON\easyax5043.c:1683: AX5043_PLLRANGINGA = r; // init ranging process starting from "range"
      003D65 90 40 33         [24] 9333 	mov	dptr,#_AX5043_PLLRANGINGA
      003D68 ED               [12] 9334 	mov	a,r5
      003D69 F0               [24] 9335 	movx	@dptr,a
      003D6A                       9336 00146$:
                                   9337 ;	COMMON\easyax5043.c:1686: EA = 0;
      003D6A C2 AF            [12] 9338 	clr	_EA
                                   9339 ;	COMMON\easyax5043.c:1687: if (axradio_trxstate == trxstate_pll_ranging_done)
      003D6C 74 06            [12] 9340 	mov	a,#0x06
      003D6E B5 0C 02         [24] 9341 	cjne	a,_axradio_trxstate,00273$
      003D71 80 1A            [24] 9342 	sjmp	00112$
      003D73                       9343 00273$:
                                   9344 ;	COMMON\easyax5043.c:1689: wtimer_idle(WTFLAG_CANSTANDBY);
      003D73 75 82 02         [24] 9345 	mov	dpl,#0x02
      003D76 C0 07            [24] 9346 	push	ar7
      003D78 C0 06            [24] 9347 	push	ar6
      003D7A 12 6B CB         [24] 9348 	lcall	_wtimer_idle
      003D7D D0 06            [24] 9349 	pop	ar6
                                   9350 ;	COMMON\easyax5043.c:1690: IE |= iesave;
      003D7F EE               [12] 9351 	mov	a,r6
      003D80 42 A8            [12] 9352 	orl	_IE,a
                                   9353 ;	COMMON\easyax5043.c:1691: wtimer_runcallbacks();
      003D82 C0 06            [24] 9354 	push	ar6
      003D84 12 6B 4A         [24] 9355 	lcall	_wtimer_runcallbacks
      003D87 D0 06            [24] 9356 	pop	ar6
      003D89 D0 07            [24] 9357 	pop	ar7
      003D8B 80 DD            [24] 9358 	sjmp	00146$
      003D8D                       9359 00112$:
                                   9360 ;	COMMON\easyax5043.c:1693: axradio_trxstate = trxstate_off;
      003D8D 75 0C 00         [24] 9361 	mov	_axradio_trxstate,#0x00
                                   9362 ;	COMMON\easyax5043.c:1694: AX5043_IRQMASK1 = 0x00;
      003D90 90 40 06         [24] 9363 	mov	dptr,#_AX5043_IRQMASK1
      003D93 E4               [12] 9364 	clr	a
      003D94 F0               [24] 9365 	movx	@dptr,a
                                   9366 ;	COMMON\easyax5043.c:1695: axradio_phy_chanpllrng[i] = AX5043_PLLRANGINGA;
      003D95 EF               [12] 9367 	mov	a,r7
      003D96 24 A0            [12] 9368 	add	a,#_axradio_phy_chanpllrng
      003D98 FC               [12] 9369 	mov	r4,a
      003D99 E4               [12] 9370 	clr	a
      003D9A 34 00            [12] 9371 	addc	a,#(_axradio_phy_chanpllrng >> 8)
      003D9C FD               [12] 9372 	mov	r5,a
      003D9D 90 40 33         [24] 9373 	mov	dptr,#_AX5043_PLLRANGINGA
      003DA0 E0               [24] 9374 	movx	a,@dptr
      003DA1 FB               [12] 9375 	mov	r3,a
      003DA2 8C 82            [24] 9376 	mov	dpl,r4
      003DA4 8D 83            [24] 9377 	mov	dph,r5
      003DA6 F0               [24] 9378 	movx	@dptr,a
                                   9379 ;	COMMON\easyax5043.c:1696: IE |= iesave;
      003DA7 EE               [12] 9380 	mov	a,r6
      003DA8 42 A8            [12] 9381 	orl	_IE,a
                                   9382 ;	COMMON\easyax5043.c:1655: for (i = 0; i < axradio_phy_nrchannels; ++i) {
      003DAA 0F               [12] 9383 	inc	r7
      003DAB 02 3C DB         [24] 9384 	ljmp	00149$
      003DAE                       9385 00113$:
                                   9386 ;	COMMON\easyax5043.c:1699: if (axradio_phy_vcocalib) {
      003DAE 90 7F D2         [24] 9387 	mov	dptr,#_axradio_phy_vcocalib
      003DB1 E4               [12] 9388 	clr	a
      003DB2 93               [24] 9389 	movc	a,@a+dptr
      003DB3 70 03            [24] 9390 	jnz	00274$
      003DB5 02 40 9F         [24] 9391 	ljmp	00142$
      003DB8                       9392 00274$:
                                   9393 ;	COMMON\easyax5043.c:1700: ax5043_set_registers_tx();
      003DB8 12 15 24         [24] 9394 	lcall	_ax5043_set_registers_tx
                                   9395 ;	COMMON\easyax5043.c:1701: AX5043_MODULATION = 0x08;
      003DBB 90 40 10         [24] 9396 	mov	dptr,#_AX5043_MODULATION
      003DBE 74 08            [12] 9397 	mov	a,#0x08
      003DC0 F0               [24] 9398 	movx	@dptr,a
                                   9399 ;	COMMON\easyax5043.c:1702: AX5043_FSKDEV2 = 0x00;
      003DC1 90 41 61         [24] 9400 	mov	dptr,#_AX5043_FSKDEV2
      003DC4 E4               [12] 9401 	clr	a
      003DC5 F0               [24] 9402 	movx	@dptr,a
                                   9403 ;	COMMON\easyax5043.c:1703: AX5043_FSKDEV1 = 0x00;
      003DC6 90 41 62         [24] 9404 	mov	dptr,#_AX5043_FSKDEV1
      003DC9 F0               [24] 9405 	movx	@dptr,a
                                   9406 ;	COMMON\easyax5043.c:1704: AX5043_FSKDEV0 = 0x00;
      003DCA 90 41 63         [24] 9407 	mov	dptr,#_AX5043_FSKDEV0
      003DCD F0               [24] 9408 	movx	@dptr,a
                                   9409 ;	COMMON\easyax5043.c:1705: AX5043_PLLLOOP |= 0x04;
      003DCE 90 40 30         [24] 9410 	mov	dptr,#_AX5043_PLLLOOP
      003DD1 E0               [24] 9411 	movx	a,@dptr
      003DD2 FF               [12] 9412 	mov	r7,a
      003DD3 74 04            [12] 9413 	mov	a,#0x04
      003DD5 4F               [12] 9414 	orl	a,r7
      003DD6 F0               [24] 9415 	movx	@dptr,a
                                   9416 ;	COMMON\easyax5043.c:1707: uint8_t x = AX5043_0xF35;
      003DD7 90 4F 35         [24] 9417 	mov	dptr,#_AX5043_0xF35
      003DDA E0               [24] 9418 	movx	a,@dptr
      003DDB FF               [12] 9419 	mov	r7,a
                                   9420 ;	COMMON\easyax5043.c:1708: x |= 0x80;
      003DDC 43 07 80         [24] 9421 	orl	ar7,#0x80
      003DDF 90 03 50         [24] 9422 	mov	dptr,#_axradio_init_x_3_396
      003DE2 EF               [12] 9423 	mov	a,r7
      003DE3 F0               [24] 9424 	movx	@dptr,a
                                   9425 ;	COMMON\easyax5043.c:1709: if (2 & (uint8_t)~x)
      003DE4 EF               [12] 9426 	mov	a,r7
      003DE5 F4               [12] 9427 	cpl	a
      003DE6 FE               [12] 9428 	mov	r6,a
      003DE7 30 E1 06         [24] 9429 	jnb	acc.1,00115$
                                   9430 ;	COMMON\easyax5043.c:1710: ++x;
      003DEA 90 03 50         [24] 9431 	mov	dptr,#_axradio_init_x_3_396
      003DED EF               [12] 9432 	mov	a,r7
      003DEE 04               [12] 9433 	inc	a
      003DEF F0               [24] 9434 	movx	@dptr,a
      003DF0                       9435 00115$:
                                   9436 ;	COMMON\easyax5043.c:1711: AX5043_0xF35 = x;
      003DF0 90 03 50         [24] 9437 	mov	dptr,#_axradio_init_x_3_396
      003DF3 E0               [24] 9438 	movx	a,@dptr
      003DF4 90 4F 35         [24] 9439 	mov	dptr,#_AX5043_0xF35
      003DF7 F0               [24] 9440 	movx	@dptr,a
                                   9441 ;	COMMON\easyax5043.c:1713: AX5043_PWRMODE = AX5043_PWRSTATE_SYNTH_TX;
      003DF8 90 40 02         [24] 9442 	mov	dptr,#_AX5043_PWRMODE
      003DFB 74 0C            [12] 9443 	mov	a,#0x0c
      003DFD F0               [24] 9444 	movx	@dptr,a
                                   9445 ;	COMMON\easyax5043.c:1715: uint8_t __autodata vcoisave = AX5043_PLLVCOI;
      003DFE 90 41 80         [24] 9446 	mov	dptr,#_AX5043_PLLVCOI
      003E01 E0               [24] 9447 	movx	a,@dptr
      003E02 FF               [12] 9448 	mov	r7,a
                                   9449 ;	COMMON\easyax5043.c:1716: uint8_t j = 2;
      003E03 90 03 51         [24] 9450 	mov	dptr,#_axradio_init_j_3_397
      003E06 74 02            [12] 9451 	mov	a,#0x02
      003E08 F0               [24] 9452 	movx	@dptr,a
                                   9453 ;	COMMON\easyax5043.c:1717: for (i = 0; i < axradio_phy_nrchannels; ++i) {
      003E09 7E 00            [12] 9454 	mov	r6,#0x00
      003E0B                       9455 00152$:
      003E0B 90 7F CB         [24] 9456 	mov	dptr,#_axradio_phy_nrchannels
      003E0E E4               [12] 9457 	clr	a
      003E0F 93               [24] 9458 	movc	a,@a+dptr
      003E10 FD               [12] 9459 	mov	r5,a
      003E11 C3               [12] 9460 	clr	c
      003E12 EE               [12] 9461 	mov	a,r6
      003E13 9D               [12] 9462 	subb	a,r5
      003E14 40 03            [24] 9463 	jc	00276$
      003E16 02 3F 23         [24] 9464 	ljmp	00127$
      003E19                       9465 00276$:
                                   9466 ;	COMMON\easyax5043.c:1718: axradio_phy_chanvcoi[i] = 0;
      003E19 EE               [12] 9467 	mov	a,r6
      003E1A 24 A1            [12] 9468 	add	a,#_axradio_phy_chanvcoi
      003E1C F5 82            [12] 9469 	mov	dpl,a
      003E1E E4               [12] 9470 	clr	a
      003E1F 34 00            [12] 9471 	addc	a,#(_axradio_phy_chanvcoi >> 8)
      003E21 F5 83            [12] 9472 	mov	dph,a
      003E23 E4               [12] 9473 	clr	a
      003E24 F0               [24] 9474 	movx	@dptr,a
                                   9475 ;	COMMON\easyax5043.c:1719: if (axradio_phy_chanpllrng[i] & 0x20)
      003E25 EE               [12] 9476 	mov	a,r6
      003E26 24 A0            [12] 9477 	add	a,#_axradio_phy_chanpllrng
      003E28 FC               [12] 9478 	mov	r4,a
      003E29 E4               [12] 9479 	clr	a
      003E2A 34 00            [12] 9480 	addc	a,#(_axradio_phy_chanpllrng >> 8)
      003E2C FD               [12] 9481 	mov	r5,a
      003E2D 8C 82            [24] 9482 	mov	dpl,r4
      003E2F 8D 83            [24] 9483 	mov	dph,r5
      003E31 E0               [24] 9484 	movx	a,@dptr
      003E32 FB               [12] 9485 	mov	r3,a
      003E33 30 E5 03         [24] 9486 	jnb	acc.5,00277$
      003E36 02 3F 1F         [24] 9487 	ljmp	00126$
      003E39                       9488 00277$:
                                   9489 ;	COMMON\easyax5043.c:1721: AX5043_PLLRANGINGA = axradio_phy_chanpllrng[i] & 0x0F;
      003E39 90 40 33         [24] 9490 	mov	dptr,#_AX5043_PLLRANGINGA
      003E3C 74 0F            [12] 9491 	mov	a,#0x0f
      003E3E 5B               [12] 9492 	anl	a,r3
      003E3F F0               [24] 9493 	movx	@dptr,a
                                   9494 ;	COMMON\easyax5043.c:1723: uint32_t __autodata f = axradio_phy_chanfreq[i];
      003E40 EE               [12] 9495 	mov	a,r6
      003E41 75 F0 04         [24] 9496 	mov	b,#0x04
      003E44 A4               [48] 9497 	mul	ab
      003E45 24 CC            [12] 9498 	add	a,#_axradio_phy_chanfreq
      003E47 F5 82            [12] 9499 	mov	dpl,a
      003E49 74 7F            [12] 9500 	mov	a,#(_axradio_phy_chanfreq >> 8)
      003E4B 35 F0            [12] 9501 	addc	a,b
      003E4D F5 83            [12] 9502 	mov	dph,a
      003E4F E4               [12] 9503 	clr	a
      003E50 93               [24] 9504 	movc	a,@a+dptr
      003E51 F8               [12] 9505 	mov	r0,a
      003E52 A3               [24] 9506 	inc	dptr
      003E53 E4               [12] 9507 	clr	a
      003E54 93               [24] 9508 	movc	a,@a+dptr
      003E55 F9               [12] 9509 	mov	r1,a
      003E56 A3               [24] 9510 	inc	dptr
      003E57 E4               [12] 9511 	clr	a
      003E58 93               [24] 9512 	movc	a,@a+dptr
      003E59 FA               [12] 9513 	mov	r2,a
      003E5A A3               [24] 9514 	inc	dptr
      003E5B E4               [12] 9515 	clr	a
      003E5C 93               [24] 9516 	movc	a,@a+dptr
      003E5D FB               [12] 9517 	mov	r3,a
                                   9518 ;	COMMON\easyax5043.c:1724: AX5043_FREQA0 = f;
      003E5E 90 40 37         [24] 9519 	mov	dptr,#_AX5043_FREQA0
      003E61 E8               [12] 9520 	mov	a,r0
      003E62 F0               [24] 9521 	movx	@dptr,a
                                   9522 ;	COMMON\easyax5043.c:1725: AX5043_FREQA1 = f >> 8;
      003E63 90 40 36         [24] 9523 	mov	dptr,#_AX5043_FREQA1
      003E66 E9               [12] 9524 	mov	a,r1
      003E67 F0               [24] 9525 	movx	@dptr,a
                                   9526 ;	COMMON\easyax5043.c:1726: AX5043_FREQA2 = f >> 16;
      003E68 90 40 35         [24] 9527 	mov	dptr,#_AX5043_FREQA2
      003E6B EA               [12] 9528 	mov	a,r2
      003E6C F0               [24] 9529 	movx	@dptr,a
                                   9530 ;	COMMON\easyax5043.c:1727: AX5043_FREQA3 = f >> 24;
      003E6D 90 40 34         [24] 9531 	mov	dptr,#_AX5043_FREQA3
      003E70 EB               [12] 9532 	mov	a,r3
      003E71 F0               [24] 9533 	movx	@dptr,a
                                   9534 ;	COMMON\easyax5043.c:1729: do {
      003E72 90 03 51         [24] 9535 	mov	dptr,#_axradio_init_j_3_397
      003E75 E0               [24] 9536 	movx	a,@dptr
      003E76 FB               [12] 9537 	mov	r3,a
      003E77                       9538 00123$:
                                   9539 ;	COMMON\easyax5043.c:1730: if (axradio_phy_chanvcoiinit[0]) {
      003E77 90 7F D1         [24] 9540 	mov	dptr,#_axradio_phy_chanvcoiinit
      003E7A E4               [12] 9541 	clr	a
      003E7B 93               [24] 9542 	movc	a,@a+dptr
      003E7C 60 65            [24] 9543 	jz	00121$
                                   9544 ;	COMMON\easyax5043.c:1731: uint8_t x = axradio_phy_chanvcoiinit[i];
      003E7E EE               [12] 9545 	mov	a,r6
      003E7F 90 7F D1         [24] 9546 	mov	dptr,#_axradio_phy_chanvcoiinit
      003E82 93               [24] 9547 	movc	a,@a+dptr
      003E83 FA               [12] 9548 	mov	r2,a
      003E84 90 03 52         [24] 9549 	mov	dptr,#_axradio_init_x_6_401
      003E87 F0               [24] 9550 	movx	@dptr,a
                                   9551 ;	COMMON\easyax5043.c:1732: if (!(axradio_phy_chanpllrnginit[0] & 0xF0))
      003E88 90 7F D0         [24] 9552 	mov	dptr,#_axradio_phy_chanpllrnginit
      003E8B E4               [12] 9553 	clr	a
      003E8C 93               [24] 9554 	movc	a,@a+dptr
      003E8D F9               [12] 9555 	mov	r1,a
      003E8E 54 F0            [12] 9556 	anl	a,#0xf0
      003E90 70 1A            [24] 9557 	jnz	00119$
                                   9558 ;	COMMON\easyax5043.c:1733: x += (axradio_phy_chanpllrng[i] & 0x0F) - (axradio_phy_chanpllrnginit[i] & 0x0F);
      003E92 8C 82            [24] 9559 	mov	dpl,r4
      003E94 8D 83            [24] 9560 	mov	dph,r5
      003E96 E0               [24] 9561 	movx	a,@dptr
      003E97 F9               [12] 9562 	mov	r1,a
      003E98 53 01 0F         [24] 9563 	anl	ar1,#0x0f
      003E9B EE               [12] 9564 	mov	a,r6
      003E9C 90 7F D0         [24] 9565 	mov	dptr,#_axradio_phy_chanpllrnginit
      003E9F 93               [24] 9566 	movc	a,@a+dptr
      003EA0 F8               [12] 9567 	mov	r0,a
      003EA1 74 0F            [12] 9568 	mov	a,#0x0f
      003EA3 58               [12] 9569 	anl	a,r0
      003EA4 D3               [12] 9570 	setb	c
      003EA5 99               [12] 9571 	subb	a,r1
      003EA6 F4               [12] 9572 	cpl	a
      003EA7 90 03 52         [24] 9573 	mov	dptr,#_axradio_init_x_6_401
      003EAA 2A               [12] 9574 	add	a,r2
      003EAB F0               [24] 9575 	movx	@dptr,a
      003EAC                       9576 00119$:
                                   9577 ;	COMMON\easyax5043.c:1734: axradio_phy_chanvcoi[i] = axradio_adjustvcoi(x);
      003EAC EE               [12] 9578 	mov	a,r6
      003EAD 24 A1            [12] 9579 	add	a,#_axradio_phy_chanvcoi
      003EAF F9               [12] 9580 	mov	r1,a
      003EB0 E4               [12] 9581 	clr	a
      003EB1 34 00            [12] 9582 	addc	a,#(_axradio_phy_chanvcoi >> 8)
      003EB3 FA               [12] 9583 	mov	r2,a
      003EB4 90 03 52         [24] 9584 	mov	dptr,#_axradio_init_x_6_401
      003EB7 E0               [24] 9585 	movx	a,@dptr
      003EB8 F5 82            [12] 9586 	mov	dpl,a
      003EBA C0 07            [24] 9587 	push	ar7
      003EBC C0 06            [24] 9588 	push	ar6
      003EBE C0 05            [24] 9589 	push	ar5
      003EC0 C0 04            [24] 9590 	push	ar4
      003EC2 C0 03            [24] 9591 	push	ar3
      003EC4 C0 02            [24] 9592 	push	ar2
      003EC6 C0 01            [24] 9593 	push	ar1
      003EC8 12 3A 66         [24] 9594 	lcall	_axradio_adjustvcoi
      003ECB A8 82            [24] 9595 	mov	r0,dpl
      003ECD D0 01            [24] 9596 	pop	ar1
      003ECF D0 02            [24] 9597 	pop	ar2
      003ED1 D0 03            [24] 9598 	pop	ar3
      003ED3 D0 04            [24] 9599 	pop	ar4
      003ED5 D0 05            [24] 9600 	pop	ar5
      003ED7 D0 06            [24] 9601 	pop	ar6
      003ED9 D0 07            [24] 9602 	pop	ar7
      003EDB 89 82            [24] 9603 	mov	dpl,r1
      003EDD 8A 83            [24] 9604 	mov	dph,r2
      003EDF E8               [12] 9605 	mov	a,r0
      003EE0 F0               [24] 9606 	movx	@dptr,a
      003EE1 80 2F            [24] 9607 	sjmp	00124$
      003EE3                       9608 00121$:
                                   9609 ;	COMMON\easyax5043.c:1736: axradio_phy_chanvcoi[i] = axradio_calvcoi();
      003EE3 EE               [12] 9610 	mov	a,r6
      003EE4 24 A1            [12] 9611 	add	a,#_axradio_phy_chanvcoi
      003EE6 F9               [12] 9612 	mov	r1,a
      003EE7 E4               [12] 9613 	clr	a
      003EE8 34 00            [12] 9614 	addc	a,#(_axradio_phy_chanvcoi >> 8)
      003EEA FA               [12] 9615 	mov	r2,a
      003EEB C0 07            [24] 9616 	push	ar7
      003EED C0 06            [24] 9617 	push	ar6
      003EEF C0 05            [24] 9618 	push	ar5
      003EF1 C0 04            [24] 9619 	push	ar4
      003EF3 C0 03            [24] 9620 	push	ar3
      003EF5 C0 02            [24] 9621 	push	ar2
      003EF7 C0 01            [24] 9622 	push	ar1
      003EF9 12 3B 39         [24] 9623 	lcall	_axradio_calvcoi
      003EFC A8 82            [24] 9624 	mov	r0,dpl
      003EFE D0 01            [24] 9625 	pop	ar1
      003F00 D0 02            [24] 9626 	pop	ar2
      003F02 D0 03            [24] 9627 	pop	ar3
      003F04 D0 04            [24] 9628 	pop	ar4
      003F06 D0 05            [24] 9629 	pop	ar5
      003F08 D0 06            [24] 9630 	pop	ar6
      003F0A D0 07            [24] 9631 	pop	ar7
      003F0C 89 82            [24] 9632 	mov	dpl,r1
      003F0E 8A 83            [24] 9633 	mov	dph,r2
      003F10 E8               [12] 9634 	mov	a,r0
      003F11 F0               [24] 9635 	movx	@dptr,a
      003F12                       9636 00124$:
                                   9637 ;	COMMON\easyax5043.c:1738: } while (--j);
      003F12 DB 02            [24] 9638 	djnz	r3,00281$
      003F14 80 03            [24] 9639 	sjmp	00282$
      003F16                       9640 00281$:
      003F16 02 3E 77         [24] 9641 	ljmp	00123$
      003F19                       9642 00282$:
                                   9643 ;	COMMON\easyax5043.c:1739: j = 1;
      003F19 90 03 51         [24] 9644 	mov	dptr,#_axradio_init_j_3_397
      003F1C 74 01            [12] 9645 	mov	a,#0x01
      003F1E F0               [24] 9646 	movx	@dptr,a
      003F1F                       9647 00126$:
                                   9648 ;	COMMON\easyax5043.c:1717: for (i = 0; i < axradio_phy_nrchannels; ++i) {
      003F1F 0E               [12] 9649 	inc	r6
      003F20 02 3E 0B         [24] 9650 	ljmp	00152$
      003F23                       9651 00127$:
                                   9652 ;	COMMON\easyax5043.c:1759: AX5043_PLLVCOI = vcoisave;
      003F23 90 41 80         [24] 9653 	mov	dptr,#_AX5043_PLLVCOI
      003F26 EF               [12] 9654 	mov	a,r7
      003F27 F0               [24] 9655 	movx	@dptr,a
                                   9656 ;	COMMON\easyax5043.c:1762: if (DBGLNKSTAT & 0x10) {
      003F28 E5 E2            [12] 9657 	mov	a,_DBGLNKSTAT
      003F2A 20 E4 03         [24] 9658 	jb	acc.4,00283$
      003F2D 02 40 9F         [24] 9659 	ljmp	00142$
      003F30                       9660 00283$:
                                   9661 ;	COMMON\easyax5043.c:1763: for (i = 0; i < axradio_phy_nrchannels; ++i) {
      003F30 7F 00            [12] 9662 	mov	r7,#0x00
      003F32                       9663 00154$:
      003F32 90 7F CB         [24] 9664 	mov	dptr,#_axradio_phy_nrchannels
      003F35 E4               [12] 9665 	clr	a
      003F36 93               [24] 9666 	movc	a,@a+dptr
      003F37 FE               [12] 9667 	mov	r6,a
      003F38 C3               [12] 9668 	clr	c
      003F39 EF               [12] 9669 	mov	a,r7
      003F3A 9E               [12] 9670 	subb	a,r6
      003F3B 40 03            [24] 9671 	jc	00284$
      003F3D 02 40 9F         [24] 9672 	ljmp	00142$
      003F40                       9673 00284$:
                                   9674 ;	COMMON\easyax5043.c:1764: uint8_t chg = ((axradio_phy_chanpllrnginit[0] & 0xF0) || axradio_phy_chanpllrnginit[i] != axradio_phy_chanpllrng[i])
      003F40 90 7F D0         [24] 9675 	mov	dptr,#_axradio_phy_chanpllrnginit
      003F43 E4               [12] 9676 	clr	a
      003F44 93               [24] 9677 	movc	a,@a+dptr
      003F45 FE               [12] 9678 	mov	r6,a
      003F46 54 F0            [12] 9679 	anl	a,#0xf0
      003F48 70 40            [24] 9680 	jnz	00161$
      003F4A EF               [12] 9681 	mov	a,r7
      003F4B 90 7F D0         [24] 9682 	mov	dptr,#_axradio_phy_chanpllrnginit
      003F4E 93               [24] 9683 	movc	a,@a+dptr
      003F4F FE               [12] 9684 	mov	r6,a
      003F50 EF               [12] 9685 	mov	a,r7
      003F51 24 A0            [12] 9686 	add	a,#_axradio_phy_chanpllrng
      003F53 F5 82            [12] 9687 	mov	dpl,a
      003F55 E4               [12] 9688 	clr	a
      003F56 34 00            [12] 9689 	addc	a,#(_axradio_phy_chanpllrng >> 8)
      003F58 F5 83            [12] 9690 	mov	dph,a
      003F5A E0               [24] 9691 	movx	a,@dptr
      003F5B FD               [12] 9692 	mov	r5,a
      003F5C EE               [12] 9693 	mov	a,r6
      003F5D B5 05 03         [24] 9694 	cjne	a,ar5,00287$
      003F60 D3               [12] 9695 	setb	c
      003F61 80 01            [24] 9696 	sjmp	00288$
      003F63                       9697 00287$:
      003F63 C3               [12] 9698 	clr	c
      003F64                       9699 00288$:
      003F64 92 02            [24] 9700 	mov	_axradio_init_sloc0_1_0,c
      003F66 50 22            [24] 9701 	jnc	00161$
      003F68 90 7F D1         [24] 9702 	mov	dptr,#_axradio_phy_chanvcoiinit
      003F6B E4               [12] 9703 	clr	a
      003F6C 93               [24] 9704 	movc	a,@a+dptr
      003F6D 60 1B            [24] 9705 	jz	00161$
      003F6F EF               [12] 9706 	mov	a,r7
      003F70 90 7F D1         [24] 9707 	mov	dptr,#_axradio_phy_chanvcoiinit
      003F73 93               [24] 9708 	movc	a,@a+dptr
      003F74 FE               [12] 9709 	mov	r6,a
      003F75 EF               [12] 9710 	mov	a,r7
      003F76 24 A1            [12] 9711 	add	a,#_axradio_phy_chanvcoi
      003F78 F5 82            [12] 9712 	mov	dpl,a
      003F7A E4               [12] 9713 	clr	a
      003F7B 34 00            [12] 9714 	addc	a,#(_axradio_phy_chanvcoi >> 8)
      003F7D F5 83            [12] 9715 	mov	dph,a
      003F7F E0               [24] 9716 	movx	a,@dptr
      003F80 FD               [12] 9717 	mov	r5,a
      003F81 6E               [12] 9718 	xrl	a,r6
      003F82 54 7F            [12] 9719 	anl	a,#0x7f
      003F84 70 04            [24] 9720 	jnz	00161$
      003F86 C2 02            [12] 9721 	clr	_axradio_init_sloc0_1_0
      003F88 80 02            [24] 9722 	sjmp	00162$
      003F8A                       9723 00161$:
      003F8A D2 02            [12] 9724 	setb	_axradio_init_sloc0_1_0
      003F8C                       9725 00162$:
      003F8C A2 02            [12] 9726 	mov	c,_axradio_init_sloc0_1_0
      003F8E E4               [12] 9727 	clr	a
      003F8F 33               [12] 9728 	rlc	a
      003F90 FE               [12] 9729 	mov	r6,a
                                   9730 ;	COMMON\easyax5043.c:1766: if (1 && !chg)
      003F91 20 02 03         [24] 9731 	jb	_axradio_init_sloc0_1_0,00293$
      003F94 02 40 9B         [24] 9732 	ljmp	00137$
      003F97                       9733 00293$:
                                   9734 ;	COMMON\easyax5043.c:1768: dbglink_writestr("CH ");
      003F97 90 81 F6         [24] 9735 	mov	dptr,#___str_14
      003F9A 75 F0 80         [24] 9736 	mov	b,#0x80
      003F9D C0 07            [24] 9737 	push	ar7
      003F9F C0 06            [24] 9738 	push	ar6
      003FA1 12 74 0A         [24] 9739 	lcall	_dbglink_writestr
      003FA4 D0 06            [24] 9740 	pop	ar6
      003FA6 D0 07            [24] 9741 	pop	ar7
                                   9742 ;	COMMON\easyax5043.c:1769: dbglink_writenum16(i, 0, 0);
      003FA8 8F 04            [24] 9743 	mov	ar4,r7
      003FAA 7D 00            [12] 9744 	mov	r5,#0x00
      003FAC C0 07            [24] 9745 	push	ar7
      003FAE C0 06            [24] 9746 	push	ar6
      003FB0 E4               [12] 9747 	clr	a
      003FB1 C0 E0            [24] 9748 	push	acc
      003FB3 C0 E0            [24] 9749 	push	acc
      003FB5 8C 82            [24] 9750 	mov	dpl,r4
      003FB7 8D 83            [24] 9751 	mov	dph,r5
      003FB9 12 7C 79         [24] 9752 	lcall	_dbglink_writenum16
      003FBC 15 81            [12] 9753 	dec	sp
      003FBE 15 81            [12] 9754 	dec	sp
                                   9755 ;	COMMON\easyax5043.c:1770: dbglink_writestr(" RNG ");
      003FC0 90 81 FA         [24] 9756 	mov	dptr,#___str_15
      003FC3 75 F0 80         [24] 9757 	mov	b,#0x80
      003FC6 12 74 0A         [24] 9758 	lcall	_dbglink_writestr
      003FC9 D0 06            [24] 9759 	pop	ar6
      003FCB D0 07            [24] 9760 	pop	ar7
                                   9761 ;	COMMON\easyax5043.c:1771: if (!(axradio_phy_chanpllrnginit[0] & 0xF0)) {
      003FCD 90 7F D0         [24] 9762 	mov	dptr,#_axradio_phy_chanpllrnginit
      003FD0 E4               [12] 9763 	clr	a
      003FD1 93               [24] 9764 	movc	a,@a+dptr
      003FD2 FD               [12] 9765 	mov	r5,a
      003FD3 54 F0            [12] 9766 	anl	a,#0xf0
      003FD5 70 26            [24] 9767 	jnz	00132$
                                   9768 ;	COMMON\easyax5043.c:1772: dbglink_writenum16(axradio_phy_chanpllrnginit[i], 0, 0);
      003FD7 EF               [12] 9769 	mov	a,r7
      003FD8 90 7F D0         [24] 9770 	mov	dptr,#_axradio_phy_chanpllrnginit
      003FDB 93               [24] 9771 	movc	a,@a+dptr
      003FDC FD               [12] 9772 	mov	r5,a
      003FDD 7C 00            [12] 9773 	mov	r4,#0x00
      003FDF C0 07            [24] 9774 	push	ar7
      003FE1 C0 06            [24] 9775 	push	ar6
      003FE3 E4               [12] 9776 	clr	a
      003FE4 C0 E0            [24] 9777 	push	acc
      003FE6 C0 E0            [24] 9778 	push	acc
      003FE8 8D 82            [24] 9779 	mov	dpl,r5
      003FEA 8C 83            [24] 9780 	mov	dph,r4
      003FEC 12 7C 79         [24] 9781 	lcall	_dbglink_writenum16
      003FEF 15 81            [12] 9782 	dec	sp
      003FF1 15 81            [12] 9783 	dec	sp
      003FF3 D0 06            [24] 9784 	pop	ar6
      003FF5 D0 07            [24] 9785 	pop	ar7
                                   9786 ;	COMMON\easyax5043.c:1773: dbglink_tx('/');
      003FF7 75 82 2F         [24] 9787 	mov	dpl,#0x2f
      003FFA 12 64 4D         [24] 9788 	lcall	_dbglink_tx
      003FFD                       9789 00132$:
                                   9790 ;	COMMON\easyax5043.c:1775: dbglink_writenum16(axradio_phy_chanpllrng[i], 0, 0);
      003FFD EF               [12] 9791 	mov	a,r7
      003FFE 24 A0            [12] 9792 	add	a,#_axradio_phy_chanpllrng
      004000 F5 82            [12] 9793 	mov	dpl,a
      004002 E4               [12] 9794 	clr	a
      004003 34 00            [12] 9795 	addc	a,#(_axradio_phy_chanpllrng >> 8)
      004005 F5 83            [12] 9796 	mov	dph,a
      004007 E0               [24] 9797 	movx	a,@dptr
      004008 FD               [12] 9798 	mov	r5,a
      004009 7C 00            [12] 9799 	mov	r4,#0x00
      00400B C0 07            [24] 9800 	push	ar7
      00400D C0 06            [24] 9801 	push	ar6
      00400F E4               [12] 9802 	clr	a
      004010 C0 E0            [24] 9803 	push	acc
      004012 C0 E0            [24] 9804 	push	acc
      004014 8D 82            [24] 9805 	mov	dpl,r5
      004016 8C 83            [24] 9806 	mov	dph,r4
      004018 12 7C 79         [24] 9807 	lcall	_dbglink_writenum16
      00401B 15 81            [12] 9808 	dec	sp
      00401D 15 81            [12] 9809 	dec	sp
                                   9810 ;	COMMON\easyax5043.c:1776: dbglink_writestr(" VCOI ");
      00401F 90 82 00         [24] 9811 	mov	dptr,#___str_16
      004022 75 F0 80         [24] 9812 	mov	b,#0x80
      004025 12 74 0A         [24] 9813 	lcall	_dbglink_writestr
      004028 D0 06            [24] 9814 	pop	ar6
      00402A D0 07            [24] 9815 	pop	ar7
                                   9816 ;	COMMON\easyax5043.c:1777: if (axradio_phy_chanvcoiinit[0]) {
      00402C 90 7F D1         [24] 9817 	mov	dptr,#_axradio_phy_chanvcoiinit
      00402F E4               [12] 9818 	clr	a
      004030 93               [24] 9819 	movc	a,@a+dptr
      004031 60 29            [24] 9820 	jz	00134$
                                   9821 ;	COMMON\easyax5043.c:1778: dbglink_writenum16(axradio_phy_chanvcoiinit[i] & 0x7F, 0, 0);
      004033 EF               [12] 9822 	mov	a,r7
      004034 90 7F D1         [24] 9823 	mov	dptr,#_axradio_phy_chanvcoiinit
      004037 93               [24] 9824 	movc	a,@a+dptr
      004038 FD               [12] 9825 	mov	r5,a
      004039 53 05 7F         [24] 9826 	anl	ar5,#0x7f
      00403C 7C 00            [12] 9827 	mov	r4,#0x00
      00403E C0 07            [24] 9828 	push	ar7
      004040 C0 06            [24] 9829 	push	ar6
      004042 E4               [12] 9830 	clr	a
      004043 C0 E0            [24] 9831 	push	acc
      004045 C0 E0            [24] 9832 	push	acc
      004047 8D 82            [24] 9833 	mov	dpl,r5
      004049 8C 83            [24] 9834 	mov	dph,r4
      00404B 12 7C 79         [24] 9835 	lcall	_dbglink_writenum16
      00404E 15 81            [12] 9836 	dec	sp
      004050 15 81            [12] 9837 	dec	sp
      004052 D0 06            [24] 9838 	pop	ar6
      004054 D0 07            [24] 9839 	pop	ar7
                                   9840 ;	COMMON\easyax5043.c:1779: dbglink_tx('/');
      004056 75 82 2F         [24] 9841 	mov	dpl,#0x2f
      004059 12 64 4D         [24] 9842 	lcall	_dbglink_tx
      00405C                       9843 00134$:
                                   9844 ;	COMMON\easyax5043.c:1781: dbglink_writenum16(axradio_phy_chanvcoi[i] & 0x7F, 0, 0);
      00405C EF               [12] 9845 	mov	a,r7
      00405D 24 A1            [12] 9846 	add	a,#_axradio_phy_chanvcoi
      00405F F5 82            [12] 9847 	mov	dpl,a
      004061 E4               [12] 9848 	clr	a
      004062 34 00            [12] 9849 	addc	a,#(_axradio_phy_chanvcoi >> 8)
      004064 F5 83            [12] 9850 	mov	dph,a
      004066 E0               [24] 9851 	movx	a,@dptr
      004067 FD               [12] 9852 	mov	r5,a
      004068 53 05 7F         [24] 9853 	anl	ar5,#0x7f
      00406B 7C 00            [12] 9854 	mov	r4,#0x00
      00406D C0 07            [24] 9855 	push	ar7
      00406F C0 06            [24] 9856 	push	ar6
      004071 E4               [12] 9857 	clr	a
      004072 C0 E0            [24] 9858 	push	acc
      004074 C0 E0            [24] 9859 	push	acc
      004076 8D 82            [24] 9860 	mov	dpl,r5
      004078 8C 83            [24] 9861 	mov	dph,r4
      00407A 12 7C 79         [24] 9862 	lcall	_dbglink_writenum16
      00407D 15 81            [12] 9863 	dec	sp
      00407F 15 81            [12] 9864 	dec	sp
      004081 D0 06            [24] 9865 	pop	ar6
      004083 D0 07            [24] 9866 	pop	ar7
                                   9867 ;	COMMON\easyax5043.c:1782: if (chg)
      004085 EE               [12] 9868 	mov	a,r6
      004086 60 0D            [24] 9869 	jz	00136$
                                   9870 ;	COMMON\easyax5043.c:1783: dbglink_writestr(" *");
      004088 90 82 07         [24] 9871 	mov	dptr,#___str_17
      00408B 75 F0 80         [24] 9872 	mov	b,#0x80
      00408E C0 07            [24] 9873 	push	ar7
      004090 12 74 0A         [24] 9874 	lcall	_dbglink_writestr
      004093 D0 07            [24] 9875 	pop	ar7
      004095                       9876 00136$:
                                   9877 ;	COMMON\easyax5043.c:1784: dbglink_tx('\n');
      004095 75 82 0A         [24] 9878 	mov	dpl,#0x0a
      004098 12 64 4D         [24] 9879 	lcall	_dbglink_tx
      00409B                       9880 00137$:
                                   9881 ;	COMMON\easyax5043.c:1763: for (i = 0; i < axradio_phy_nrchannels; ++i) {
      00409B 0F               [12] 9882 	inc	r7
      00409C 02 3F 32         [24] 9883 	ljmp	00154$
      00409F                       9884 00142$:
                                   9885 ;	COMMON\easyax5043.c:1789: AX5043_PWRMODE = AX5043_PWRSTATE_POWERDOWN;
      00409F 90 40 02         [24] 9886 	mov	dptr,#_AX5043_PWRMODE
      0040A2 E4               [12] 9887 	clr	a
      0040A3 F0               [24] 9888 	movx	@dptr,a
                                   9889 ;	COMMON\easyax5043.c:1790: ax5043_init_registers();
      0040A4 12 2A 31         [24] 9890 	lcall	_ax5043_init_registers
                                   9891 ;	COMMON\easyax5043.c:1791: ax5043_set_registers_rx();
      0040A7 12 15 48         [24] 9892 	lcall	_ax5043_set_registers_rx
                                   9893 ;	COMMON\easyax5043.c:1792: AX5043_PLLRANGINGA = axradio_phy_chanpllrng[0] & 0x0F;
      0040AA 90 00 A0         [24] 9894 	mov	dptr,#_axradio_phy_chanpllrng
      0040AD E0               [24] 9895 	movx	a,@dptr
      0040AE FF               [12] 9896 	mov	r7,a
      0040AF 90 40 33         [24] 9897 	mov	dptr,#_AX5043_PLLRANGINGA
      0040B2 74 0F            [12] 9898 	mov	a,#0x0f
      0040B4 5F               [12] 9899 	anl	a,r7
      0040B5 F0               [24] 9900 	movx	@dptr,a
                                   9901 ;	COMMON\easyax5043.c:1794: uint32_t __autodata f = axradio_phy_chanfreq[0];
      0040B6 90 7F CC         [24] 9902 	mov	dptr,#_axradio_phy_chanfreq
      0040B9 E4               [12] 9903 	clr	a
      0040BA 93               [24] 9904 	movc	a,@a+dptr
      0040BB FC               [12] 9905 	mov	r4,a
      0040BC A3               [24] 9906 	inc	dptr
      0040BD E4               [12] 9907 	clr	a
      0040BE 93               [24] 9908 	movc	a,@a+dptr
      0040BF FD               [12] 9909 	mov	r5,a
      0040C0 A3               [24] 9910 	inc	dptr
      0040C1 E4               [12] 9911 	clr	a
      0040C2 93               [24] 9912 	movc	a,@a+dptr
      0040C3 FE               [12] 9913 	mov	r6,a
      0040C4 A3               [24] 9914 	inc	dptr
      0040C5 E4               [12] 9915 	clr	a
      0040C6 93               [24] 9916 	movc	a,@a+dptr
      0040C7 FF               [12] 9917 	mov	r7,a
                                   9918 ;	COMMON\easyax5043.c:1795: AX5043_FREQA0 = f;
      0040C8 90 40 37         [24] 9919 	mov	dptr,#_AX5043_FREQA0
      0040CB EC               [12] 9920 	mov	a,r4
      0040CC F0               [24] 9921 	movx	@dptr,a
                                   9922 ;	COMMON\easyax5043.c:1796: AX5043_FREQA1 = f >> 8;
      0040CD 90 40 36         [24] 9923 	mov	dptr,#_AX5043_FREQA1
      0040D0 ED               [12] 9924 	mov	a,r5
      0040D1 F0               [24] 9925 	movx	@dptr,a
                                   9926 ;	COMMON\easyax5043.c:1797: AX5043_FREQA2 = f >> 16;
      0040D2 90 40 35         [24] 9927 	mov	dptr,#_AX5043_FREQA2
      0040D5 EE               [12] 9928 	mov	a,r6
      0040D6 F0               [24] 9929 	movx	@dptr,a
                                   9930 ;	COMMON\easyax5043.c:1798: AX5043_FREQA3 = f >> 24;
      0040D7 90 40 34         [24] 9931 	mov	dptr,#_AX5043_FREQA3
      0040DA EF               [12] 9932 	mov	a,r7
      0040DB F0               [24] 9933 	movx	@dptr,a
                                   9934 ;	COMMON\easyax5043.c:1801: axradio_mode = AXRADIO_MODE_OFF;
      0040DC 75 0B 01         [24] 9935 	mov	_axradio_mode,#0x01
                                   9936 ;	COMMON\easyax5043.c:1802: for (i = 0; i < axradio_phy_nrchannels; ++i)
      0040DF 7F 00            [12] 9937 	mov	r7,#0x00
      0040E1                       9938 00156$:
      0040E1 90 7F CB         [24] 9939 	mov	dptr,#_axradio_phy_nrchannels
      0040E4 E4               [12] 9940 	clr	a
      0040E5 93               [24] 9941 	movc	a,@a+dptr
      0040E6 FE               [12] 9942 	mov	r6,a
      0040E7 C3               [12] 9943 	clr	c
      0040E8 EF               [12] 9944 	mov	a,r7
      0040E9 9E               [12] 9945 	subb	a,r6
      0040EA 50 16            [24] 9946 	jnc	00145$
                                   9947 ;	COMMON\easyax5043.c:1803: if (axradio_phy_chanpllrng[i] & 0x20)
      0040EC EF               [12] 9948 	mov	a,r7
      0040ED 24 A0            [12] 9949 	add	a,#_axradio_phy_chanpllrng
      0040EF F5 82            [12] 9950 	mov	dpl,a
      0040F1 E4               [12] 9951 	clr	a
      0040F2 34 00            [12] 9952 	addc	a,#(_axradio_phy_chanpllrng >> 8)
      0040F4 F5 83            [12] 9953 	mov	dph,a
      0040F6 E0               [24] 9954 	movx	a,@dptr
      0040F7 FE               [12] 9955 	mov	r6,a
      0040F8 30 E5 04         [24] 9956 	jnb	acc.5,00157$
                                   9957 ;	COMMON\easyax5043.c:1804: return AXRADIO_ERR_RANGING;
      0040FB 75 82 06         [24] 9958 	mov	dpl,#0x06
      0040FE 22               [24] 9959 	ret
      0040FF                       9960 00157$:
                                   9961 ;	COMMON\easyax5043.c:1802: for (i = 0; i < axradio_phy_nrchannels; ++i)
      0040FF 0F               [12] 9962 	inc	r7
      004100 80 DF            [24] 9963 	sjmp	00156$
      004102                       9964 00145$:
                                   9965 ;	COMMON\easyax5043.c:1805: return AXRADIO_ERR_NOERROR;
      004102 75 82 00         [24] 9966 	mov	dpl,#0x00
      004105 22               [24] 9967 	ret
                                   9968 ;------------------------------------------------------------
                                   9969 ;Allocation info for local variables in function 'axradio_cansleep'
                                   9970 ;------------------------------------------------------------
                                   9971 ;	COMMON\easyax5043.c:1808: __reentrantb uint8_t axradio_cansleep(void) __reentrant
                                   9972 ;	-----------------------------------------
                                   9973 ;	 function axradio_cansleep
                                   9974 ;	-----------------------------------------
      004106                       9975 _axradio_cansleep:
                                   9976 ;	COMMON\easyax5043.c:1810: if (axradio_trxstate == trxstate_off || axradio_trxstate == trxstate_rxwor)
      004106 E5 0C            [12] 9977 	mov	a,_axradio_trxstate
      004108 60 05            [24] 9978 	jz	00101$
      00410A 74 02            [12] 9979 	mov	a,#0x02
      00410C B5 0C 04         [24] 9980 	cjne	a,_axradio_trxstate,00102$
      00410F                       9981 00101$:
                                   9982 ;	COMMON\easyax5043.c:1811: return 1;
      00410F 75 82 01         [24] 9983 	mov	dpl,#0x01
      004112 22               [24] 9984 	ret
      004113                       9985 00102$:
                                   9986 ;	COMMON\easyax5043.c:1812: return 0;
      004113 75 82 00         [24] 9987 	mov	dpl,#0x00
      004116 22               [24] 9988 	ret
                                   9989 ;------------------------------------------------------------
                                   9990 ;Allocation info for local variables in function 'wtimer_cansleep_dummy'
                                   9991 ;------------------------------------------------------------
                                   9992 ;	COMMON\easyax5043.c:1816: static void wtimer_cansleep_dummy(void) __naked
                                   9993 ;	-----------------------------------------
                                   9994 ;	 function wtimer_cansleep_dummy
                                   9995 ;	-----------------------------------------
      004117                       9996 _wtimer_cansleep_dummy:
                                   9997 ;	naked function: no prologue.
                                   9998 ;	COMMON\easyax5043.c:1830: __endasm;
                                   9999 	.area	WTCANSLP0 (CODE)
                                  10000 	.area	WTCANSLP1 (CODE)
                                  10001 	.area	WTCANSLP2 (CODE)
                                  10002 	.area	WTCANSLP1 (CODE)
      00865B 12 41 06         [24]10003 	lcall	_axradio_cansleep
      00865E E5 82            [12]10004 	mov	a,dpl
      008660 70 01            [24]10005 	jnz	00000$
      008662 22               [24]10006 	ret
      008663                      10007 	00000$:
                                  10008 	.area	CSEG (CODE)
                                  10009 ;	naked function: no epilogue.
                                  10010 ;------------------------------------------------------------
                                  10011 ;Allocation info for local variables in function 'axradio_set_mode'
                                  10012 ;------------------------------------------------------------
                                  10013 ;r                         Allocated to registers r5 
                                  10014 ;r                         Allocated to registers r6 
                                  10015 ;iesave                    Allocated to registers r6 
                                  10016 ;mode                      Allocated with name '_axradio_set_mode_mode_1_412'
                                  10017 ;------------------------------------------------------------
                                  10018 ;	COMMON\easyax5043.c:1834: uint8_t axradio_set_mode(uint8_t mode)
                                  10019 ;	-----------------------------------------
                                  10020 ;	 function axradio_set_mode
                                  10021 ;	-----------------------------------------
      004117                      10022 _axradio_set_mode:
      004117 E5 82            [12]10023 	mov	a,dpl
      004119 90 03 53         [24]10024 	mov	dptr,#_axradio_set_mode_mode_1_412
      00411C F0               [24]10025 	movx	@dptr,a
                                  10026 ;	COMMON\easyax5043.c:1836: if (mode == axradio_mode)
      00411D E0               [24]10027 	movx	a,@dptr
      00411E FF               [12]10028 	mov	r7,a
      00411F B5 0B 04         [24]10029 	cjne	a,_axradio_mode,00102$
                                  10030 ;	COMMON\easyax5043.c:1837: return AXRADIO_ERR_NOERROR;
      004122 75 82 00         [24]10031 	mov	dpl,#0x00
      004125 22               [24]10032 	ret
      004126                      10033 00102$:
                                  10034 ;	COMMON\easyax5043.c:1838: switch (axradio_mode) {
      004126 AE 0B            [24]10035 	mov	r6,_axradio_mode
      004128 BE 00 02         [24]10036 	cjne	r6,#0x00,00283$
      00412B 80 4C            [24]10037 	sjmp	00103$
      00412D                      10038 00283$:
      00412D BE 02 02         [24]10039 	cjne	r6,#0x02,00284$
      004130 80 5A            [24]10040 	sjmp	00106$
      004132                      10041 00284$:
      004132 BE 03 03         [24]10042 	cjne	r6,#0x03,00285$
      004135 02 41 BC         [24]10043 	ljmp	00116$
      004138                      10044 00285$:
      004138 BE 18 03         [24]10045 	cjne	r6,#0x18,00286$
      00413B 02 41 BC         [24]10046 	ljmp	00116$
      00413E                      10047 00286$:
      00413E BE 19 02         [24]10048 	cjne	r6,#0x19,00287$
      004141 80 79            [24]10049 	sjmp	00116$
      004143                      10050 00287$:
      004143 BE 1A 02         [24]10051 	cjne	r6,#0x1a,00288$
      004146 80 74            [24]10052 	sjmp	00116$
      004148                      10053 00288$:
      004148 BE 1B 02         [24]10054 	cjne	r6,#0x1b,00289$
      00414B 80 6F            [24]10055 	sjmp	00116$
      00414D                      10056 00289$:
      00414D BE 1C 02         [24]10057 	cjne	r6,#0x1c,00290$
      004150 80 6A            [24]10058 	sjmp	00116$
      004152                      10059 00290$:
      004152 BE 28 03         [24]10060 	cjne	r6,#0x28,00291$
      004155 02 42 15         [24]10061 	ljmp	00124$
      004158                      10062 00291$:
      004158 BE 29 03         [24]10063 	cjne	r6,#0x29,00292$
      00415B 02 42 15         [24]10064 	ljmp	00124$
      00415E                      10065 00292$:
      00415E BE 2A 03         [24]10066 	cjne	r6,#0x2a,00293$
      004161 02 42 15         [24]10067 	ljmp	00124$
      004164                      10068 00293$:
      004164 BE 2B 03         [24]10069 	cjne	r6,#0x2b,00294$
      004167 02 42 15         [24]10070 	ljmp	00124$
      00416A                      10071 00294$:
      00416A BE 2C 03         [24]10072 	cjne	r6,#0x2c,00295$
      00416D 02 42 15         [24]10073 	ljmp	00124$
      004170                      10074 00295$:
      004170 BE 2D 03         [24]10075 	cjne	r6,#0x2d,00296$
      004173 02 42 15         [24]10076 	ljmp	00124$
      004176                      10077 00296$:
      004176 02 42 22         [24]10078 	ljmp	00125$
                                  10079 ;	COMMON\easyax5043.c:1839: case AXRADIO_MODE_UNINIT:
      004179                      10080 00103$:
                                  10081 ;	COMMON\easyax5043.c:1841: uint8_t __autodata r = axradio_init();
      004179 C0 07            [24]10082 	push	ar7
      00417B 12 3B EF         [24]10083 	lcall	_axradio_init
      00417E AE 82            [24]10084 	mov	r6,dpl
      004180 D0 07            [24]10085 	pop	ar7
                                  10086 ;	COMMON\easyax5043.c:1842: if (r != AXRADIO_ERR_NOERROR)
      004182 EE               [12]10087 	mov	a,r6
      004183 FD               [12]10088 	mov	r5,a
      004184 70 03            [24]10089 	jnz	00297$
      004186 02 42 2C         [24]10090 	ljmp	00126$
      004189                      10091 00297$:
                                  10092 ;	COMMON\easyax5043.c:1843: return r;
      004189 8D 82            [24]10093 	mov	dpl,r5
      00418B 22               [24]10094 	ret
                                  10095 ;	COMMON\easyax5043.c:1847: case AXRADIO_MODE_DEEPSLEEP:
      00418C                      10096 00106$:
                                  10097 ;	COMMON\easyax5043.c:1849: uint8_t __autodata r = ax5043_wakeup_deepsleep();
      00418C C0 07            [24]10098 	push	ar7
      00418E 12 64 B8         [24]10099 	lcall	_ax5043_wakeup_deepsleep
      004191 AE 82            [24]10100 	mov	r6,dpl
      004193 D0 07            [24]10101 	pop	ar7
                                  10102 ;	COMMON\easyax5043.c:1850: if (r)
      004195 EE               [12]10103 	mov	a,r6
      004196 60 04            [24]10104 	jz	00108$
                                  10105 ;	COMMON\easyax5043.c:1851: return AXRADIO_ERR_NOCHIP;
      004198 75 82 05         [24]10106 	mov	dpl,#0x05
      00419B 22               [24]10107 	ret
      00419C                      10108 00108$:
                                  10109 ;	COMMON\easyax5043.c:1852: ax5043_init_registers();
      00419C C0 07            [24]10110 	push	ar7
      00419E 12 2A 31         [24]10111 	lcall	_ax5043_init_registers
                                  10112 ;	COMMON\easyax5043.c:1853: r = axradio_set_channel(axradio_curchannel);
      0041A1 90 00 AB         [24]10113 	mov	dptr,#_axradio_curchannel
      0041A4 E0               [24]10114 	movx	a,@dptr
      0041A5 F5 82            [12]10115 	mov	dpl,a
      0041A7 12 45 75         [24]10116 	lcall	_axradio_set_channel
      0041AA AE 82            [24]10117 	mov	r6,dpl
      0041AC D0 07            [24]10118 	pop	ar7
                                  10119 ;	COMMON\easyax5043.c:1854: if (r != AXRADIO_ERR_NOERROR)
      0041AE EE               [12]10120 	mov	a,r6
      0041AF 60 03            [24]10121 	jz	00110$
                                  10122 ;	COMMON\easyax5043.c:1855: return r;
      0041B1 8E 82            [24]10123 	mov	dpl,r6
      0041B3 22               [24]10124 	ret
      0041B4                      10125 00110$:
                                  10126 ;	COMMON\easyax5043.c:1856: axradio_trxstate = trxstate_off;
      0041B4 75 0C 00         [24]10127 	mov	_axradio_trxstate,#0x00
                                  10128 ;	COMMON\easyax5043.c:1857: axradio_mode = AXRADIO_MODE_OFF;
      0041B7 75 0B 01         [24]10129 	mov	_axradio_mode,#0x01
                                  10130 ;	COMMON\easyax5043.c:1858: break;
                                  10131 ;	COMMON\easyax5043.c:1866: case AXRADIO_MODE_CW_TRANSMIT:
      0041BA 80 70            [24]10132 	sjmp	00126$
      0041BC                      10133 00116$:
                                  10134 ;	COMMON\easyax5043.c:1868: uint8_t __autodata iesave = IE & 0x80;
      0041BC 74 80            [12]10135 	mov	a,#0x80
      0041BE 55 A8            [12]10136 	anl	a,_IE
      0041C0 FE               [12]10137 	mov	r6,a
                                  10138 ;	COMMON\easyax5043.c:1869: EA = 0;
      0041C1 C2 AF            [12]10139 	clr	_EA
                                  10140 ;	COMMON\easyax5043.c:1870: if (axradio_trxstate == trxstate_off) {
      0041C3 E5 0C            [12]10141 	mov	a,_axradio_trxstate
      0041C5 70 38            [24]10142 	jnz	00118$
                                  10143 ;	COMMON\easyax5043.c:1871: update_timeanchor();
      0041C7 C0 07            [24]10144 	push	ar7
      0041C9 C0 06            [24]10145 	push	ar6
      0041CB 12 1B 43         [24]10146 	lcall	_update_timeanchor
                                  10147 ;	COMMON\easyax5043.c:1872: wtimer_remove_callback(&axradio_cb_transmitend.cb);
      0041CE 90 03 17         [24]10148 	mov	dptr,#_axradio_cb_transmitend
      0041D1 12 78 A9         [24]10149 	lcall	_wtimer_remove_callback
                                  10150 ;	COMMON\easyax5043.c:1873: axradio_cb_transmitend.st.error = AXRADIO_ERR_NOERROR;
      0041D4 90 03 1C         [24]10151 	mov	dptr,#(_axradio_cb_transmitend + 0x0005)
      0041D7 E4               [12]10152 	clr	a
      0041D8 F0               [24]10153 	movx	@dptr,a
                                  10154 ;	COMMON\easyax5043.c:1874: axradio_cb_transmitend.st.time.t = axradio_timeanchor.radiotimer;
      0041D9 90 00 BC         [24]10155 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      0041DC E0               [24]10156 	movx	a,@dptr
      0041DD FA               [12]10157 	mov	r2,a
      0041DE A3               [24]10158 	inc	dptr
      0041DF E0               [24]10159 	movx	a,@dptr
      0041E0 FB               [12]10160 	mov	r3,a
      0041E1 A3               [24]10161 	inc	dptr
      0041E2 E0               [24]10162 	movx	a,@dptr
      0041E3 FC               [12]10163 	mov	r4,a
      0041E4 A3               [24]10164 	inc	dptr
      0041E5 E0               [24]10165 	movx	a,@dptr
      0041E6 FD               [12]10166 	mov	r5,a
      0041E7 90 03 1D         [24]10167 	mov	dptr,#(_axradio_cb_transmitend + 0x0006)
      0041EA EA               [12]10168 	mov	a,r2
      0041EB F0               [24]10169 	movx	@dptr,a
      0041EC EB               [12]10170 	mov	a,r3
      0041ED A3               [24]10171 	inc	dptr
      0041EE F0               [24]10172 	movx	@dptr,a
      0041EF EC               [12]10173 	mov	a,r4
      0041F0 A3               [24]10174 	inc	dptr
      0041F1 F0               [24]10175 	movx	@dptr,a
      0041F2 ED               [12]10176 	mov	a,r5
      0041F3 A3               [24]10177 	inc	dptr
      0041F4 F0               [24]10178 	movx	@dptr,a
                                  10179 ;	COMMON\easyax5043.c:1875: wtimer_add_callback(&axradio_cb_transmitend.cb);
      0041F5 90 03 17         [24]10180 	mov	dptr,#_axradio_cb_transmitend
      0041F8 12 6C C3         [24]10181 	lcall	_wtimer_add_callback
      0041FB D0 06            [24]10182 	pop	ar6
      0041FD D0 07            [24]10183 	pop	ar7
      0041FF                      10184 00118$:
                                  10185 ;	COMMON\easyax5043.c:1877: ax5043_off();
      0041FF C0 07            [24]10186 	push	ar7
      004201 C0 06            [24]10187 	push	ar6
      004203 12 28 BB         [24]10188 	lcall	_ax5043_off
      004206 D0 06            [24]10189 	pop	ar6
                                  10190 ;	COMMON\easyax5043.c:1878: IE |= iesave;
      004208 EE               [12]10191 	mov	a,r6
      004209 42 A8            [12]10192 	orl	_IE,a
                                  10193 ;	COMMON\easyax5043.c:1880: ax5043_init_registers();
      00420B 12 2A 31         [24]10194 	lcall	_ax5043_init_registers
      00420E D0 07            [24]10195 	pop	ar7
                                  10196 ;	COMMON\easyax5043.c:1881: axradio_mode = AXRADIO_MODE_OFF;
      004210 75 0B 01         [24]10197 	mov	_axradio_mode,#0x01
                                  10198 ;	COMMON\easyax5043.c:1882: break;
                                  10199 ;	COMMON\easyax5043.c:1890: case AXRADIO_MODE_STREAM_RECEIVE_DATAPIN:
      004213 80 17            [24]10200 	sjmp	00126$
      004215                      10201 00124$:
                                  10202 ;	COMMON\easyax5043.c:1891: ax5043_off();
      004215 C0 07            [24]10203 	push	ar7
      004217 12 28 BB         [24]10204 	lcall	_ax5043_off
                                  10205 ;	COMMON\easyax5043.c:1892: ax5043_init_registers();
      00421A 12 2A 31         [24]10206 	lcall	_ax5043_init_registers
      00421D D0 07            [24]10207 	pop	ar7
                                  10208 ;	COMMON\easyax5043.c:1893: axradio_mode = AXRADIO_MODE_OFF;
      00421F 75 0B 01         [24]10209 	mov	_axradio_mode,#0x01
                                  10210 ;	COMMON\easyax5043.c:1895: default:
      004222                      10211 00125$:
                                  10212 ;	COMMON\easyax5043.c:1896: ax5043_off();
      004222 C0 07            [24]10213 	push	ar7
      004224 12 28 BB         [24]10214 	lcall	_ax5043_off
      004227 D0 07            [24]10215 	pop	ar7
                                  10216 ;	COMMON\easyax5043.c:1897: axradio_mode = AXRADIO_MODE_OFF;
      004229 75 0B 01         [24]10217 	mov	_axradio_mode,#0x01
                                  10218 ;	COMMON\easyax5043.c:1899: }
      00422C                      10219 00126$:
                                  10220 ;	COMMON\easyax5043.c:1900: axradio_killallcb();
      00422C C0 07            [24]10221 	push	ar7
      00422E 12 39 F0         [24]10222 	lcall	_axradio_killallcb
      004231 D0 07            [24]10223 	pop	ar7
                                  10224 ;	COMMON\easyax5043.c:1901: if (mode == AXRADIO_MODE_UNINIT)
      004233 EF               [12]10225 	mov	a,r7
      004234 70 04            [24]10226 	jnz	00128$
                                  10227 ;	COMMON\easyax5043.c:1902: return AXRADIO_ERR_NOTSUPPORTED;
      004236 75 82 01         [24]10228 	mov	dpl,#0x01
      004239 22               [24]10229 	ret
      00423A                      10230 00128$:
                                  10231 ;	COMMON\easyax5043.c:1903: axradio_syncstate = syncstate_off;
      00423A 90 00 A6         [24]10232 	mov	dptr,#_axradio_syncstate
      00423D E4               [12]10233 	clr	a
      00423E F0               [24]10234 	movx	@dptr,a
                                  10235 ;	COMMON\easyax5043.c:1904: switch (mode) {
      00423F EF               [12]10236 	mov	a,r7
      004240 FE               [12]10237 	mov	r6,a
      004241 24 CC            [12]10238 	add	a,#0xff - 0x33
      004243 50 03            [24]10239 	jnc	00302$
      004245 02 45 6D         [24]10240 	ljmp	00181$
      004248                      10241 00302$:
      004248 EE               [12]10242 	mov	a,r6
      004249 24 0A            [12]10243 	add	a,#(00303$-3-.)
      00424B 83               [24]10244 	movc	a,@a+pc
      00424C F5 82            [12]10245 	mov	dpl,a
      00424E EE               [12]10246 	mov	a,r6
      00424F 24 38            [12]10247 	add	a,#(00304$-3-.)
      004251 83               [24]10248 	movc	a,@a+pc
      004252 F5 83            [12]10249 	mov	dph,a
      004254 E4               [12]10250 	clr	a
      004255 73               [24]10251 	jmp	@a+dptr
      004256                      10252 00303$:
      004256 6D                   10253 	.db	00181$
      004257 BE                   10254 	.db	00129$
      004258 C2                   10255 	.db	00130$
      004259 30                   10256 	.db	00176$
      00425A 6D                   10257 	.db	00181$
      00425B 6D                   10258 	.db	00181$
      00425C 6D                   10259 	.db	00181$
      00425D 6D                   10260 	.db	00181$
      00425E 6D                   10261 	.db	00181$
      00425F 6D                   10262 	.db	00181$
      004260 6D                   10263 	.db	00181$
      004261 6D                   10264 	.db	00181$
      004262 6D                   10265 	.db	00181$
      004263 6D                   10266 	.db	00181$
      004264 6D                   10267 	.db	00181$
      004265 6D                   10268 	.db	00181$
      004266 CC                   10269 	.db	00131$
      004267 DB                   10270 	.db	00133$
      004268 CC                   10271 	.db	00132$
      004269 DB                   10272 	.db	00134$
      00426A 6D                   10273 	.db	00181$
      00426B 6D                   10274 	.db	00181$
      00426C 6D                   10275 	.db	00181$
      00426D 6D                   10276 	.db	00181$
      00426E 3D                   10277 	.db	00143$
      00426F 3D                   10278 	.db	00144$
      004270 3D                   10279 	.db	00145$
      004271 3D                   10280 	.db	00146$
      004272 3D                   10281 	.db	00142$
      004273 6D                   10282 	.db	00181$
      004274 6D                   10283 	.db	00181$
      004275 6D                   10284 	.db	00181$
      004276 EA                   10285 	.db	00135$
      004277 2B                   10286 	.db	00140$
      004278 EA                   10287 	.db	00136$
      004279 2B                   10288 	.db	00141$
      00427A 6D                   10289 	.db	00181$
      00427B 6D                   10290 	.db	00181$
      00427C 6D                   10291 	.db	00181$
      00427D 6D                   10292 	.db	00181$
      00427E CB                   10293 	.db	00160$
      00427F CB                   10294 	.db	00161$
      004280 CB                   10295 	.db	00162$
      004281 CB                   10296 	.db	00163$
      004282 CB                   10297 	.db	00159$
      004283 CB                   10298 	.db	00164$
      004284 6D                   10299 	.db	00181$
      004285 6D                   10300 	.db	00181$
      004286 73                   10301 	.db	00177$
      004287 73                   10302 	.db	00178$
      004288 CE                   10303 	.db	00179$
      004289 CE                   10304 	.db	00180$
      00428A                      10305 00304$:
      00428A 45                   10306 	.db	00181$>>8
      00428B 42                   10307 	.db	00129$>>8
      00428C 42                   10308 	.db	00130$>>8
      00428D 44                   10309 	.db	00176$>>8
      00428E 45                   10310 	.db	00181$>>8
      00428F 45                   10311 	.db	00181$>>8
      004290 45                   10312 	.db	00181$>>8
      004291 45                   10313 	.db	00181$>>8
      004292 45                   10314 	.db	00181$>>8
      004293 45                   10315 	.db	00181$>>8
      004294 45                   10316 	.db	00181$>>8
      004295 45                   10317 	.db	00181$>>8
      004296 45                   10318 	.db	00181$>>8
      004297 45                   10319 	.db	00181$>>8
      004298 45                   10320 	.db	00181$>>8
      004299 45                   10321 	.db	00181$>>8
      00429A 42                   10322 	.db	00131$>>8
      00429B 42                   10323 	.db	00133$>>8
      00429C 42                   10324 	.db	00132$>>8
      00429D 42                   10325 	.db	00134$>>8
      00429E 45                   10326 	.db	00181$>>8
      00429F 45                   10327 	.db	00181$>>8
      0042A0 45                   10328 	.db	00181$>>8
      0042A1 45                   10329 	.db	00181$>>8
      0042A2 43                   10330 	.db	00143$>>8
      0042A3 43                   10331 	.db	00144$>>8
      0042A4 43                   10332 	.db	00145$>>8
      0042A5 43                   10333 	.db	00146$>>8
      0042A6 43                   10334 	.db	00142$>>8
      0042A7 45                   10335 	.db	00181$>>8
      0042A8 45                   10336 	.db	00181$>>8
      0042A9 45                   10337 	.db	00181$>>8
      0042AA 42                   10338 	.db	00135$>>8
      0042AB 43                   10339 	.db	00140$>>8
      0042AC 42                   10340 	.db	00136$>>8
      0042AD 43                   10341 	.db	00141$>>8
      0042AE 45                   10342 	.db	00181$>>8
      0042AF 45                   10343 	.db	00181$>>8
      0042B0 45                   10344 	.db	00181$>>8
      0042B1 45                   10345 	.db	00181$>>8
      0042B2 43                   10346 	.db	00160$>>8
      0042B3 43                   10347 	.db	00161$>>8
      0042B4 43                   10348 	.db	00162$>>8
      0042B5 43                   10349 	.db	00163$>>8
      0042B6 43                   10350 	.db	00159$>>8
      0042B7 43                   10351 	.db	00164$>>8
      0042B8 45                   10352 	.db	00181$>>8
      0042B9 45                   10353 	.db	00181$>>8
      0042BA 44                   10354 	.db	00177$>>8
      0042BB 44                   10355 	.db	00178$>>8
      0042BC 44                   10356 	.db	00179$>>8
      0042BD 44                   10357 	.db	00180$>>8
                                  10358 ;	COMMON\easyax5043.c:1905: case AXRADIO_MODE_OFF:
      0042BE                      10359 00129$:
                                  10360 ;	COMMON\easyax5043.c:1906: return AXRADIO_ERR_NOERROR;
      0042BE 75 82 00         [24]10361 	mov	dpl,#0x00
      0042C1 22               [24]10362 	ret
                                  10363 ;	COMMON\easyax5043.c:1908: case AXRADIO_MODE_DEEPSLEEP:
      0042C2                      10364 00130$:
                                  10365 ;	COMMON\easyax5043.c:1909: ax5043_enter_deepsleep();
      0042C2 12 64 98         [24]10366 	lcall	_ax5043_enter_deepsleep
                                  10367 ;	COMMON\easyax5043.c:1910: axradio_mode = AXRADIO_MODE_DEEPSLEEP;
      0042C5 75 0B 02         [24]10368 	mov	_axradio_mode,#0x02
                                  10369 ;	COMMON\easyax5043.c:1911: return AXRADIO_ERR_NOERROR;
      0042C8 75 82 00         [24]10370 	mov	dpl,#0x00
      0042CB 22               [24]10371 	ret
                                  10372 ;	COMMON\easyax5043.c:1913: case AXRADIO_MODE_ASYNC_TRANSMIT:
      0042CC                      10373 00131$:
                                  10374 ;	COMMON\easyax5043.c:1914: case AXRADIO_MODE_ACK_TRANSMIT:
      0042CC                      10375 00132$:
                                  10376 ;	COMMON\easyax5043.c:1915: axradio_mode = mode;
      0042CC 8F 0B            [24]10377 	mov	_axradio_mode,r7
                                  10378 ;	COMMON\easyax5043.c:1916: axradio_ack_seqnr = 0xff;
      0042CE 90 00 B1         [24]10379 	mov	dptr,#_axradio_ack_seqnr
      0042D1 74 FF            [12]10380 	mov	a,#0xff
      0042D3 F0               [24]10381 	movx	@dptr,a
                                  10382 ;	COMMON\easyax5043.c:1917: ax5043_init_registers_tx();
      0042D4 12 1C 20         [24]10383 	lcall	_ax5043_init_registers_tx
                                  10384 ;	COMMON\easyax5043.c:1918: return AXRADIO_ERR_NOERROR;
      0042D7 75 82 00         [24]10385 	mov	dpl,#0x00
      0042DA 22               [24]10386 	ret
                                  10387 ;	COMMON\easyax5043.c:1920: case AXRADIO_MODE_WOR_TRANSMIT:
      0042DB                      10388 00133$:
                                  10389 ;	COMMON\easyax5043.c:1921: case AXRADIO_MODE_WOR_ACK_TRANSMIT:
      0042DB                      10390 00134$:
                                  10391 ;	COMMON\easyax5043.c:1922: axradio_mode = mode;
      0042DB 8F 0B            [24]10392 	mov	_axradio_mode,r7
                                  10393 ;	COMMON\easyax5043.c:1923: axradio_ack_seqnr = 0xff;
      0042DD 90 00 B1         [24]10394 	mov	dptr,#_axradio_ack_seqnr
      0042E0 74 FF            [12]10395 	mov	a,#0xff
      0042E2 F0               [24]10396 	movx	@dptr,a
                                  10397 ;	COMMON\easyax5043.c:1924: ax5043_init_registers_tx();
      0042E3 12 1C 20         [24]10398 	lcall	_ax5043_init_registers_tx
                                  10399 ;	COMMON\easyax5043.c:1925: return AXRADIO_ERR_NOERROR;
      0042E6 75 82 00         [24]10400 	mov	dpl,#0x00
      0042E9 22               [24]10401 	ret
                                  10402 ;	COMMON\easyax5043.c:1927: case AXRADIO_MODE_ASYNC_RECEIVE:
      0042EA                      10403 00135$:
                                  10404 ;	COMMON\easyax5043.c:1928: case AXRADIO_MODE_ACK_RECEIVE:
      0042EA                      10405 00136$:
                                  10406 ;	COMMON\easyax5043.c:1929: axradio_mode = mode;
      0042EA 8F 0B            [24]10407 	mov	_axradio_mode,r7
                                  10408 ;	COMMON\easyax5043.c:1930: axradio_ack_seqnr = 0xff;
      0042EC 90 00 B1         [24]10409 	mov	dptr,#_axradio_ack_seqnr
      0042EF 74 FF            [12]10410 	mov	a,#0xff
      0042F1 F0               [24]10411 	movx	@dptr,a
                                  10412 ;	COMMON\easyax5043.c:1931: ax5043_init_registers_rx();
      0042F2 12 1C 26         [24]10413 	lcall	_ax5043_init_registers_rx
                                  10414 ;	COMMON\easyax5043.c:1932: ax5043_receiver_on_continuous();
      0042F5 12 27 64         [24]10415 	lcall	_ax5043_receiver_on_continuous
                                  10416 ;	COMMON\easyax5043.c:1933: enablecs:
      0042F8                      10417 00137$:
                                  10418 ;	COMMON\easyax5043.c:1934: if (axradio_phy_cs_enabled) {
      0042F8 90 7F DC         [24]10419 	mov	dptr,#_axradio_phy_cs_enabled
      0042FB E4               [12]10420 	clr	a
      0042FC 93               [24]10421 	movc	a,@a+dptr
      0042FD 60 28            [24]10422 	jz	00139$
                                  10423 ;	COMMON\easyax5043.c:1935: wtimer_remove(&axradio_timer);
      0042FF 90 03 2B         [24]10424 	mov	dptr,#_axradio_timer
      004302 12 76 94         [24]10425 	lcall	_wtimer_remove
                                  10426 ;	COMMON\easyax5043.c:1936: axradio_timer.time = axradio_phy_cs_period;
      004305 90 7F DA         [24]10427 	mov	dptr,#_axradio_phy_cs_period
      004308 E4               [12]10428 	clr	a
      004309 93               [24]10429 	movc	a,@a+dptr
      00430A FD               [12]10430 	mov	r5,a
      00430B 74 01            [12]10431 	mov	a,#0x01
      00430D 93               [24]10432 	movc	a,@a+dptr
      00430E FE               [12]10433 	mov	r6,a
      00430F 7C 00            [12]10434 	mov	r4,#0x00
      004311 7B 00            [12]10435 	mov	r3,#0x00
      004313 90 03 2F         [24]10436 	mov	dptr,#(_axradio_timer + 0x0004)
      004316 ED               [12]10437 	mov	a,r5
      004317 F0               [24]10438 	movx	@dptr,a
      004318 EE               [12]10439 	mov	a,r6
      004319 A3               [24]10440 	inc	dptr
      00431A F0               [24]10441 	movx	@dptr,a
      00431B EC               [12]10442 	mov	a,r4
      00431C A3               [24]10443 	inc	dptr
      00431D F0               [24]10444 	movx	@dptr,a
      00431E EB               [12]10445 	mov	a,r3
      00431F A3               [24]10446 	inc	dptr
      004320 F0               [24]10447 	movx	@dptr,a
                                  10448 ;	COMMON\easyax5043.c:1937: wtimer0_addrelative(&axradio_timer);
      004321 90 03 2B         [24]10449 	mov	dptr,#_axradio_timer
      004324 12 6C DD         [24]10450 	lcall	_wtimer0_addrelative
      004327                      10451 00139$:
                                  10452 ;	COMMON\easyax5043.c:1939: return AXRADIO_ERR_NOERROR;
      004327 75 82 00         [24]10453 	mov	dpl,#0x00
      00432A 22               [24]10454 	ret
                                  10455 ;	COMMON\easyax5043.c:1941: case AXRADIO_MODE_WOR_RECEIVE:
      00432B                      10456 00140$:
                                  10457 ;	COMMON\easyax5043.c:1942: case AXRADIO_MODE_WOR_ACK_RECEIVE:
      00432B                      10458 00141$:
                                  10459 ;	COMMON\easyax5043.c:1943: axradio_ack_seqnr = 0xff;
      00432B 90 00 B1         [24]10460 	mov	dptr,#_axradio_ack_seqnr
      00432E 74 FF            [12]10461 	mov	a,#0xff
      004330 F0               [24]10462 	movx	@dptr,a
                                  10463 ;	COMMON\easyax5043.c:1944: axradio_mode = mode;
      004331 8F 0B            [24]10464 	mov	_axradio_mode,r7
                                  10465 ;	COMMON\easyax5043.c:1945: ax5043_init_registers_rx();
      004333 12 1C 26         [24]10466 	lcall	_ax5043_init_registers_rx
                                  10467 ;	COMMON\easyax5043.c:1946: ax5043_receiver_on_wor();
      004336 12 27 CC         [24]10468 	lcall	_ax5043_receiver_on_wor
                                  10469 ;	COMMON\easyax5043.c:1947: return AXRADIO_ERR_NOERROR;
      004339 75 82 00         [24]10470 	mov	dpl,#0x00
      00433C 22               [24]10471 	ret
                                  10472 ;	COMMON\easyax5043.c:1949: case AXRADIO_MODE_STREAM_TRANSMIT:
      00433D                      10473 00142$:
                                  10474 ;	COMMON\easyax5043.c:1950: case AXRADIO_MODE_STREAM_TRANSMIT_UNENC:
      00433D                      10475 00143$:
                                  10476 ;	COMMON\easyax5043.c:1951: case AXRADIO_MODE_STREAM_TRANSMIT_SCRAM:
      00433D                      10477 00144$:
                                  10478 ;	COMMON\easyax5043.c:1952: case AXRADIO_MODE_STREAM_TRANSMIT_UNENC_LSB:
      00433D                      10479 00145$:
                                  10480 ;	COMMON\easyax5043.c:1953: case AXRADIO_MODE_STREAM_TRANSMIT_SCRAM_LSB:
      00433D                      10481 00146$:
                                  10482 ;	COMMON\easyax5043.c:1954: axradio_mode = mode;
      00433D 8F 0B            [24]10483 	mov	_axradio_mode,r7
                                  10484 ;	COMMON\easyax5043.c:1955: if (axradio_mode == AXRADIO_MODE_STREAM_TRANSMIT_UNENC ||
      00433F 74 18            [12]10485 	mov	a,#0x18
      004341 B5 0B 02         [24]10486 	cjne	a,_axradio_mode,00306$
      004344 80 05            [24]10487 	sjmp	00147$
      004346                      10488 00306$:
                                  10489 ;	COMMON\easyax5043.c:1956: axradio_mode == AXRADIO_MODE_STREAM_TRANSMIT_UNENC_LSB)
      004346 74 1A            [12]10490 	mov	a,#0x1a
      004348 B5 0B 05         [24]10491 	cjne	a,_axradio_mode,00148$
      00434B                      10492 00147$:
                                  10493 ;	COMMON\easyax5043.c:1957: AX5043_ENCODING = 0;
      00434B 90 40 11         [24]10494 	mov	dptr,#_AX5043_ENCODING
      00434E E4               [12]10495 	clr	a
      00434F F0               [24]10496 	movx	@dptr,a
      004350                      10497 00148$:
                                  10498 ;	COMMON\easyax5043.c:1958: if (axradio_mode == AXRADIO_MODE_STREAM_TRANSMIT_SCRAM ||
      004350 74 19            [12]10499 	mov	a,#0x19
      004352 B5 0B 02         [24]10500 	cjne	a,_axradio_mode,00309$
      004355 80 05            [24]10501 	sjmp	00150$
      004357                      10502 00309$:
                                  10503 ;	COMMON\easyax5043.c:1959: axradio_mode == AXRADIO_MODE_STREAM_TRANSMIT_SCRAM_LSB)
      004357 74 1B            [12]10504 	mov	a,#0x1b
      004359 B5 0B 06         [24]10505 	cjne	a,_axradio_mode,00151$
      00435C                      10506 00150$:
                                  10507 ;	COMMON\easyax5043.c:1960: AX5043_ENCODING = 4;
      00435C 90 40 11         [24]10508 	mov	dptr,#_AX5043_ENCODING
      00435F 74 04            [12]10509 	mov	a,#0x04
      004361 F0               [24]10510 	movx	@dptr,a
      004362                      10511 00151$:
                                  10512 ;	COMMON\easyax5043.c:1961: if (axradio_mode == AXRADIO_MODE_STREAM_TRANSMIT_UNENC_LSB ||
      004362 74 1A            [12]10513 	mov	a,#0x1a
      004364 B5 0B 02         [24]10514 	cjne	a,_axradio_mode,00312$
      004367 80 05            [24]10515 	sjmp	00153$
      004369                      10516 00312$:
                                  10517 ;	COMMON\easyax5043.c:1962: axradio_mode == AXRADIO_MODE_STREAM_TRANSMIT_SCRAM_LSB)
      004369 74 1B            [12]10518 	mov	a,#0x1b
      00436B B5 0B 09         [24]10519 	cjne	a,_axradio_mode,00154$
      00436E                      10520 00153$:
                                  10521 ;	COMMON\easyax5043.c:1963: AX5043_PKTADDRCFG &= 0x7F;
      00436E 90 42 00         [24]10522 	mov	dptr,#_AX5043_PKTADDRCFG
      004371 E0               [24]10523 	movx	a,@dptr
      004372 FE               [12]10524 	mov	r6,a
      004373 74 7F            [12]10525 	mov	a,#0x7f
      004375 5E               [12]10526 	anl	a,r6
      004376 F0               [24]10527 	movx	@dptr,a
      004377                      10528 00154$:
                                  10529 ;	COMMON\easyax5043.c:1964: ax5043_init_registers_tx();
      004377 12 1C 20         [24]10530 	lcall	_ax5043_init_registers_tx
                                  10531 ;	COMMON\easyax5043.c:1965: AX5043_FRAMING = 0;
      00437A 90 40 12         [24]10532 	mov	dptr,#_AX5043_FRAMING
      00437D E4               [12]10533 	clr	a
      00437E F0               [24]10534 	movx	@dptr,a
                                  10535 ;	COMMON\easyax5043.c:1966: ax5043_prepare_tx();
      00437F 12 28 92         [24]10536 	lcall	_ax5043_prepare_tx
                                  10537 ;	COMMON\easyax5043.c:1967: axradio_trxstate = trxstate_txstream_xtalwait;
      004382 75 0C 0F         [24]10538 	mov	_axradio_trxstate,#0x0f
                                  10539 ;	COMMON\easyax5043.c:1968: while (!(AX5043_POWSTAT & 0x08)) {}; // wait for modem vdd so writing the FIFO is safe
      004385                      10540 00156$:
      004385 90 40 03         [24]10541 	mov	dptr,#_AX5043_POWSTAT
      004388 E0               [24]10542 	movx	a,@dptr
      004389 FE               [12]10543 	mov	r6,a
      00438A 30 E3 F8         [24]10544 	jnb	acc.3,00156$
                                  10545 ;	COMMON\easyax5043.c:1969: AX5043_FIFOSTAT = 3; // clear FIFO data & flags (prevent transmitting anything left over in the FIFO, this has no effect if the FIFO is not powerered, in this case it is reset any way)
      00438D 90 40 28         [24]10546 	mov	dptr,#_AX5043_FIFOSTAT
      004390 74 03            [12]10547 	mov	a,#0x03
      004392 F0               [24]10548 	movx	@dptr,a
                                  10549 ;	COMMON\easyax5043.c:1970: AX5043_RADIOEVENTREQ0; // make sure REVRDONE bit is cleared, so it is a reliable indicator that the packet is out
      004393 90 40 0F         [24]10550 	mov	dptr,#_AX5043_RADIOEVENTREQ0
      004396 E0               [24]10551 	movx	a,@dptr
                                  10552 ;	COMMON\easyax5043.c:1971: update_timeanchor();
      004397 12 1B 43         [24]10553 	lcall	_update_timeanchor
                                  10554 ;	COMMON\easyax5043.c:1972: wtimer_remove_callback(&axradio_cb_transmitdata.cb);
      00439A 90 03 21         [24]10555 	mov	dptr,#_axradio_cb_transmitdata
      00439D 12 78 A9         [24]10556 	lcall	_wtimer_remove_callback
                                  10557 ;	COMMON\easyax5043.c:1973: axradio_cb_transmitdata.st.error = AXRADIO_ERR_NOERROR;
      0043A0 90 03 26         [24]10558 	mov	dptr,#(_axradio_cb_transmitdata + 0x0005)
      0043A3 E4               [12]10559 	clr	a
      0043A4 F0               [24]10560 	movx	@dptr,a
                                  10561 ;	COMMON\easyax5043.c:1974: axradio_cb_transmitdata.st.time.t = axradio_timeanchor.radiotimer;
      0043A5 90 00 BC         [24]10562 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      0043A8 E0               [24]10563 	movx	a,@dptr
      0043A9 FB               [12]10564 	mov	r3,a
      0043AA A3               [24]10565 	inc	dptr
      0043AB E0               [24]10566 	movx	a,@dptr
      0043AC FC               [12]10567 	mov	r4,a
      0043AD A3               [24]10568 	inc	dptr
      0043AE E0               [24]10569 	movx	a,@dptr
      0043AF FD               [12]10570 	mov	r5,a
      0043B0 A3               [24]10571 	inc	dptr
      0043B1 E0               [24]10572 	movx	a,@dptr
      0043B2 FE               [12]10573 	mov	r6,a
      0043B3 90 03 27         [24]10574 	mov	dptr,#(_axradio_cb_transmitdata + 0x0006)
      0043B6 EB               [12]10575 	mov	a,r3
      0043B7 F0               [24]10576 	movx	@dptr,a
      0043B8 EC               [12]10577 	mov	a,r4
      0043B9 A3               [24]10578 	inc	dptr
      0043BA F0               [24]10579 	movx	@dptr,a
      0043BB ED               [12]10580 	mov	a,r5
      0043BC A3               [24]10581 	inc	dptr
      0043BD F0               [24]10582 	movx	@dptr,a
      0043BE EE               [12]10583 	mov	a,r6
      0043BF A3               [24]10584 	inc	dptr
      0043C0 F0               [24]10585 	movx	@dptr,a
                                  10586 ;	COMMON\easyax5043.c:1975: wtimer_add_callback(&axradio_cb_transmitdata.cb);
      0043C1 90 03 21         [24]10587 	mov	dptr,#_axradio_cb_transmitdata
      0043C4 12 6C C3         [24]10588 	lcall	_wtimer_add_callback
                                  10589 ;	COMMON\easyax5043.c:1976: return AXRADIO_ERR_NOERROR;
      0043C7 75 82 00         [24]10590 	mov	dpl,#0x00
      0043CA 22               [24]10591 	ret
                                  10592 ;	COMMON\easyax5043.c:1978: case AXRADIO_MODE_STREAM_RECEIVE:
      0043CB                      10593 00159$:
                                  10594 ;	COMMON\easyax5043.c:1979: case AXRADIO_MODE_STREAM_RECEIVE_UNENC:
      0043CB                      10595 00160$:
                                  10596 ;	COMMON\easyax5043.c:1980: case AXRADIO_MODE_STREAM_RECEIVE_SCRAM:
      0043CB                      10597 00161$:
                                  10598 ;	COMMON\easyax5043.c:1981: case AXRADIO_MODE_STREAM_RECEIVE_UNENC_LSB:
      0043CB                      10599 00162$:
                                  10600 ;	COMMON\easyax5043.c:1982: case AXRADIO_MODE_STREAM_RECEIVE_SCRAM_LSB:
      0043CB                      10601 00163$:
                                  10602 ;	COMMON\easyax5043.c:1983: case AXRADIO_MODE_STREAM_RECEIVE_DATAPIN:
      0043CB                      10603 00164$:
                                  10604 ;	COMMON\easyax5043.c:1984: axradio_mode = mode;
      0043CB 8F 0B            [24]10605 	mov	_axradio_mode,r7
                                  10606 ;	COMMON\easyax5043.c:1985: ax5043_init_registers_rx();
      0043CD 12 1C 26         [24]10607 	lcall	_ax5043_init_registers_rx
                                  10608 ;	COMMON\easyax5043.c:1986: if (axradio_mode == AXRADIO_MODE_STREAM_RECEIVE_UNENC ||
      0043D0 74 28            [12]10609 	mov	a,#0x28
      0043D2 B5 0B 02         [24]10610 	cjne	a,_axradio_mode,00316$
      0043D5 80 05            [24]10611 	sjmp	00165$
      0043D7                      10612 00316$:
                                  10613 ;	COMMON\easyax5043.c:1987: axradio_mode == AXRADIO_MODE_STREAM_RECEIVE_UNENC_LSB)
      0043D7 74 2A            [12]10614 	mov	a,#0x2a
      0043D9 B5 0B 05         [24]10615 	cjne	a,_axradio_mode,00166$
      0043DC                      10616 00165$:
                                  10617 ;	COMMON\easyax5043.c:1988: AX5043_ENCODING = 0;
      0043DC 90 40 11         [24]10618 	mov	dptr,#_AX5043_ENCODING
      0043DF E4               [12]10619 	clr	a
      0043E0 F0               [24]10620 	movx	@dptr,a
      0043E1                      10621 00166$:
                                  10622 ;	COMMON\easyax5043.c:1989: if (axradio_mode == AXRADIO_MODE_STREAM_RECEIVE_SCRAM ||
      0043E1 74 29            [12]10623 	mov	a,#0x29
      0043E3 B5 0B 02         [24]10624 	cjne	a,_axradio_mode,00319$
      0043E6 80 05            [24]10625 	sjmp	00168$
      0043E8                      10626 00319$:
                                  10627 ;	COMMON\easyax5043.c:1990: axradio_mode == AXRADIO_MODE_STREAM_RECEIVE_SCRAM_LSB)
      0043E8 74 2B            [12]10628 	mov	a,#0x2b
      0043EA B5 0B 06         [24]10629 	cjne	a,_axradio_mode,00169$
      0043ED                      10630 00168$:
                                  10631 ;	COMMON\easyax5043.c:1991: AX5043_ENCODING = 4;
      0043ED 90 40 11         [24]10632 	mov	dptr,#_AX5043_ENCODING
      0043F0 74 04            [12]10633 	mov	a,#0x04
      0043F2 F0               [24]10634 	movx	@dptr,a
      0043F3                      10635 00169$:
                                  10636 ;	COMMON\easyax5043.c:1992: if (axradio_mode == AXRADIO_MODE_STREAM_RECEIVE_UNENC_LSB ||
      0043F3 74 2A            [12]10637 	mov	a,#0x2a
      0043F5 B5 0B 02         [24]10638 	cjne	a,_axradio_mode,00322$
      0043F8 80 05            [24]10639 	sjmp	00171$
      0043FA                      10640 00322$:
                                  10641 ;	COMMON\easyax5043.c:1993: axradio_mode == AXRADIO_MODE_STREAM_RECEIVE_SCRAM_LSB)
      0043FA 74 2B            [12]10642 	mov	a,#0x2b
      0043FC B5 0B 09         [24]10643 	cjne	a,_axradio_mode,00172$
      0043FF                      10644 00171$:
                                  10645 ;	COMMON\easyax5043.c:1994: AX5043_PKTADDRCFG &= 0x7F;
      0043FF 90 42 00         [24]10646 	mov	dptr,#_AX5043_PKTADDRCFG
      004402 E0               [24]10647 	movx	a,@dptr
      004403 FE               [12]10648 	mov	r6,a
      004404 74 7F            [12]10649 	mov	a,#0x7f
      004406 5E               [12]10650 	anl	a,r6
      004407 F0               [24]10651 	movx	@dptr,a
      004408                      10652 00172$:
                                  10653 ;	COMMON\easyax5043.c:1995: AX5043_FRAMING = 0;
      004408 90 40 12         [24]10654 	mov	dptr,#_AX5043_FRAMING
      00440B E4               [12]10655 	clr	a
      00440C F0               [24]10656 	movx	@dptr,a
                                  10657 ;	COMMON\easyax5043.c:1996: AX5043_PKTCHUNKSIZE = 8; // 64 byte
      00440D 90 42 30         [24]10658 	mov	dptr,#_AX5043_PKTCHUNKSIZE
      004410 74 08            [12]10659 	mov	a,#0x08
      004412 F0               [24]10660 	movx	@dptr,a
                                  10661 ;	COMMON\easyax5043.c:1997: AX5043_RXPARAMSETS = 0x00;
      004413 90 41 17         [24]10662 	mov	dptr,#_AX5043_RXPARAMSETS
      004416 E4               [12]10663 	clr	a
      004417 F0               [24]10664 	movx	@dptr,a
                                  10665 ;	COMMON\easyax5043.c:1998: if( axradio_mode == AXRADIO_MODE_STREAM_RECEIVE_DATAPIN )
      004418 74 2D            [12]10666 	mov	a,#0x2d
      00441A B5 0B 0D         [24]10667 	cjne	a,_axradio_mode,00175$
                                  10668 ;	COMMON\easyax5043.c:2000: ax5043_set_registers_rxcont_singleparamset();
      00441D 12 15 8D         [24]10669 	lcall	_ax5043_set_registers_rxcont_singleparamset
                                  10670 ;	COMMON\easyax5043.c:2001: AX5043_PINFUNCDATA = 0x04;
      004420 90 40 23         [24]10671 	mov	dptr,#_AX5043_PINFUNCDATA
      004423 74 04            [12]10672 	mov	a,#0x04
      004425 F0               [24]10673 	movx	@dptr,a
                                  10674 ;	COMMON\easyax5043.c:2002: AX5043_PINFUNCDCLK = 0x04;
      004426 90 40 22         [24]10675 	mov	dptr,#_AX5043_PINFUNCDCLK
      004429 F0               [24]10676 	movx	@dptr,a
      00442A                      10677 00175$:
                                  10678 ;	COMMON\easyax5043.c:2004: ax5043_receiver_on_continuous();
      00442A 12 27 64         [24]10679 	lcall	_ax5043_receiver_on_continuous
                                  10680 ;	COMMON\easyax5043.c:2005: goto enablecs;
      00442D 02 42 F8         [24]10681 	ljmp	00137$
                                  10682 ;	COMMON\easyax5043.c:2007: case AXRADIO_MODE_CW_TRANSMIT:
      004430                      10683 00176$:
                                  10684 ;	COMMON\easyax5043.c:2008: axradio_mode = AXRADIO_MODE_CW_TRANSMIT;
      004430 75 0B 03         [24]10685 	mov	_axradio_mode,#0x03
                                  10686 ;	COMMON\easyax5043.c:2009: ax5043_init_registers_tx();
      004433 12 1C 20         [24]10687 	lcall	_ax5043_init_registers_tx
                                  10688 ;	COMMON\easyax5043.c:2010: AX5043_MODULATION = 8;   // Set an FSK mode
      004436 90 40 10         [24]10689 	mov	dptr,#_AX5043_MODULATION
      004439 74 08            [12]10690 	mov	a,#0x08
      00443B F0               [24]10691 	movx	@dptr,a
                                  10692 ;	COMMON\easyax5043.c:2011: AX5043_FSKDEV2 = 0x00;
      00443C 90 41 61         [24]10693 	mov	dptr,#_AX5043_FSKDEV2
      00443F E4               [12]10694 	clr	a
      004440 F0               [24]10695 	movx	@dptr,a
                                  10696 ;	COMMON\easyax5043.c:2012: AX5043_FSKDEV1 = 0x00;
      004441 90 41 62         [24]10697 	mov	dptr,#_AX5043_FSKDEV1
      004444 F0               [24]10698 	movx	@dptr,a
                                  10699 ;	COMMON\easyax5043.c:2013: AX5043_FSKDEV0 = 0x00;
      004445 90 41 63         [24]10700 	mov	dptr,#_AX5043_FSKDEV0
      004448 F0               [24]10701 	movx	@dptr,a
                                  10702 ;	COMMON\easyax5043.c:2014: AX5043_TXRATE2 = 0x00;
      004449 90 41 65         [24]10703 	mov	dptr,#_AX5043_TXRATE2
      00444C F0               [24]10704 	movx	@dptr,a
                                  10705 ;	COMMON\easyax5043.c:2015: AX5043_TXRATE1 = 0x00;
      00444D 90 41 66         [24]10706 	mov	dptr,#_AX5043_TXRATE1
      004450 F0               [24]10707 	movx	@dptr,a
                                  10708 ;	COMMON\easyax5043.c:2016: AX5043_TXRATE0 = 0x01;
      004451 90 41 67         [24]10709 	mov	dptr,#_AX5043_TXRATE0
      004454 04               [12]10710 	inc	a
      004455 F0               [24]10711 	movx	@dptr,a
                                  10712 ;	COMMON\easyax5043.c:2017: AX5043_PINFUNCDATA = 0x04;
      004456 90 40 23         [24]10713 	mov	dptr,#_AX5043_PINFUNCDATA
      004459 74 04            [12]10714 	mov	a,#0x04
      00445B F0               [24]10715 	movx	@dptr,a
                                  10716 ;	COMMON\easyax5043.c:2018: AX5043_PWRMODE = AX5043_PWRSTATE_FIFO_ON;
      00445C 90 40 02         [24]10717 	mov	dptr,#_AX5043_PWRMODE
      00445F 74 07            [12]10718 	mov	a,#0x07
      004461 F0               [24]10719 	movx	@dptr,a
                                  10720 ;	COMMON\easyax5043.c:2019: axradio_trxstate = trxstate_txcw_xtalwait;
      004462 75 0C 0E         [24]10721 	mov	_axradio_trxstate,#0x0e
                                  10722 ;	COMMON\easyax5043.c:2020: AX5043_IRQMASK0 = 0x00;
      004465 90 40 07         [24]10723 	mov	dptr,#_AX5043_IRQMASK0
      004468 E4               [12]10724 	clr	a
      004469 F0               [24]10725 	movx	@dptr,a
                                  10726 ;	COMMON\easyax5043.c:2021: AX5043_IRQMASK1 = 0x01; // enable xtal ready interrupt
      00446A 90 40 06         [24]10727 	mov	dptr,#_AX5043_IRQMASK1
      00446D 04               [12]10728 	inc	a
      00446E F0               [24]10729 	movx	@dptr,a
                                  10730 ;	COMMON\easyax5043.c:2022: return AXRADIO_ERR_NOERROR;
      00446F 75 82 00         [24]10731 	mov	dpl,#0x00
      004472 22               [24]10732 	ret
                                  10733 ;	COMMON\easyax5043.c:2024: case AXRADIO_MODE_SYNC_MASTER:
      004473                      10734 00177$:
                                  10735 ;	COMMON\easyax5043.c:2025: case AXRADIO_MODE_SYNC_ACK_MASTER:
      004473                      10736 00178$:
                                  10737 ;	COMMON\easyax5043.c:2026: axradio_mode = mode;
      004473 8F 0B            [24]10738 	mov	_axradio_mode,r7
                                  10739 ;	COMMON\easyax5043.c:2027: axradio_syncstate = syncstate_master_normal;
      004475 90 00 A6         [24]10740 	mov	dptr,#_axradio_syncstate
      004478 74 03            [12]10741 	mov	a,#0x03
      00447A F0               [24]10742 	movx	@dptr,a
                                  10743 ;	COMMON\easyax5043.c:2029: wtimer_remove(&axradio_timer);
      00447B 90 03 2B         [24]10744 	mov	dptr,#_axradio_timer
      00447E 12 76 94         [24]10745 	lcall	_wtimer_remove
                                  10746 ;	COMMON\easyax5043.c:2030: axradio_timer.time = 2;
      004481 90 03 2F         [24]10747 	mov	dptr,#(_axradio_timer + 0x0004)
      004484 74 02            [12]10748 	mov	a,#0x02
      004486 F0               [24]10749 	movx	@dptr,a
      004487 E4               [12]10750 	clr	a
      004488 A3               [24]10751 	inc	dptr
      004489 F0               [24]10752 	movx	@dptr,a
      00448A A3               [24]10753 	inc	dptr
      00448B F0               [24]10754 	movx	@dptr,a
      00448C A3               [24]10755 	inc	dptr
      00448D F0               [24]10756 	movx	@dptr,a
                                  10757 ;	COMMON\easyax5043.c:2031: wtimer0_addrelative(&axradio_timer);
      00448E 90 03 2B         [24]10758 	mov	dptr,#_axradio_timer
      004491 12 6C DD         [24]10759 	lcall	_wtimer0_addrelative
                                  10760 ;	COMMON\easyax5043.c:2032: axradio_sync_time = axradio_timer.time;
      004494 90 03 2F         [24]10761 	mov	dptr,#(_axradio_timer + 0x0004)
      004497 E0               [24]10762 	movx	a,@dptr
      004498 FB               [12]10763 	mov	r3,a
      004499 A3               [24]10764 	inc	dptr
      00449A E0               [24]10765 	movx	a,@dptr
      00449B FC               [12]10766 	mov	r4,a
      00449C A3               [24]10767 	inc	dptr
      00449D E0               [24]10768 	movx	a,@dptr
      00449E FD               [12]10769 	mov	r5,a
      00449F A3               [24]10770 	inc	dptr
      0044A0 E0               [24]10771 	movx	a,@dptr
      0044A1 FE               [12]10772 	mov	r6,a
      0044A2 90 00 B2         [24]10773 	mov	dptr,#_axradio_sync_time
      0044A5 EB               [12]10774 	mov	a,r3
      0044A6 F0               [24]10775 	movx	@dptr,a
      0044A7 EC               [12]10776 	mov	a,r4
      0044A8 A3               [24]10777 	inc	dptr
      0044A9 F0               [24]10778 	movx	@dptr,a
      0044AA ED               [12]10779 	mov	a,r5
      0044AB A3               [24]10780 	inc	dptr
      0044AC F0               [24]10781 	movx	@dptr,a
      0044AD EE               [12]10782 	mov	a,r6
      0044AE A3               [24]10783 	inc	dptr
      0044AF F0               [24]10784 	movx	@dptr,a
                                  10785 ;	COMMON\easyax5043.c:2033: axradio_sync_addtime(axradio_sync_xoscstartup);
      0044B0 90 80 0B         [24]10786 	mov	dptr,#_axradio_sync_xoscstartup
      0044B3 E4               [12]10787 	clr	a
      0044B4 93               [24]10788 	movc	a,@a+dptr
      0044B5 FB               [12]10789 	mov	r3,a
      0044B6 74 01            [12]10790 	mov	a,#0x01
      0044B8 93               [24]10791 	movc	a,@a+dptr
      0044B9 FC               [12]10792 	mov	r4,a
      0044BA 74 02            [12]10793 	mov	a,#0x02
      0044BC 93               [24]10794 	movc	a,@a+dptr
      0044BD FD               [12]10795 	mov	r5,a
      0044BE 74 03            [12]10796 	mov	a,#0x03
      0044C0 93               [24]10797 	movc	a,@a+dptr
      0044C1 8B 82            [24]10798 	mov	dpl,r3
      0044C3 8C 83            [24]10799 	mov	dph,r4
      0044C5 8D F0            [24]10800 	mov	b,r5
      0044C7 12 2A 5D         [24]10801 	lcall	_axradio_sync_addtime
                                  10802 ;	COMMON\easyax5043.c:2034: return AXRADIO_ERR_NOERROR;
      0044CA 75 82 00         [24]10803 	mov	dpl,#0x00
      0044CD 22               [24]10804 	ret
                                  10805 ;	COMMON\easyax5043.c:2036: case AXRADIO_MODE_SYNC_SLAVE:
      0044CE                      10806 00179$:
                                  10807 ;	COMMON\easyax5043.c:2037: case AXRADIO_MODE_SYNC_ACK_SLAVE:
      0044CE                      10808 00180$:
                                  10809 ;	COMMON\easyax5043.c:2038: axradio_mode = mode;
      0044CE 8F 0B            [24]10810 	mov	_axradio_mode,r7
                                  10811 ;	COMMON\easyax5043.c:2039: ax5043_init_registers_rx();
      0044D0 12 1C 26         [24]10812 	lcall	_ax5043_init_registers_rx
                                  10813 ;	COMMON\easyax5043.c:2040: ax5043_receiver_on_continuous();
      0044D3 12 27 64         [24]10814 	lcall	_ax5043_receiver_on_continuous
                                  10815 ;	COMMON\easyax5043.c:2041: axradio_syncstate = syncstate_slave_synchunt;
      0044D6 90 00 A6         [24]10816 	mov	dptr,#_axradio_syncstate
      0044D9 74 06            [12]10817 	mov	a,#0x06
      0044DB F0               [24]10818 	movx	@dptr,a
                                  10819 ;	COMMON\easyax5043.c:2042: wtimer_remove(&axradio_timer);
      0044DC 90 03 2B         [24]10820 	mov	dptr,#_axradio_timer
      0044DF 12 76 94         [24]10821 	lcall	_wtimer_remove
                                  10822 ;	COMMON\easyax5043.c:2043: axradio_timer.time = axradio_sync_slave_initialsyncwindow;
      0044E2 90 80 13         [24]10823 	mov	dptr,#_axradio_sync_slave_initialsyncwindow
      0044E5 E4               [12]10824 	clr	a
      0044E6 93               [24]10825 	movc	a,@a+dptr
      0044E7 FC               [12]10826 	mov	r4,a
      0044E8 74 01            [12]10827 	mov	a,#0x01
      0044EA 93               [24]10828 	movc	a,@a+dptr
      0044EB FD               [12]10829 	mov	r5,a
      0044EC 74 02            [12]10830 	mov	a,#0x02
      0044EE 93               [24]10831 	movc	a,@a+dptr
      0044EF FE               [12]10832 	mov	r6,a
      0044F0 74 03            [12]10833 	mov	a,#0x03
      0044F2 93               [24]10834 	movc	a,@a+dptr
      0044F3 FF               [12]10835 	mov	r7,a
      0044F4 90 03 2F         [24]10836 	mov	dptr,#(_axradio_timer + 0x0004)
      0044F7 EC               [12]10837 	mov	a,r4
      0044F8 F0               [24]10838 	movx	@dptr,a
      0044F9 ED               [12]10839 	mov	a,r5
      0044FA A3               [24]10840 	inc	dptr
      0044FB F0               [24]10841 	movx	@dptr,a
      0044FC EE               [12]10842 	mov	a,r6
      0044FD A3               [24]10843 	inc	dptr
      0044FE F0               [24]10844 	movx	@dptr,a
      0044FF EF               [12]10845 	mov	a,r7
      004500 A3               [24]10846 	inc	dptr
      004501 F0               [24]10847 	movx	@dptr,a
                                  10848 ;	COMMON\easyax5043.c:2044: wtimer0_addrelative(&axradio_timer);
      004502 90 03 2B         [24]10849 	mov	dptr,#_axradio_timer
      004505 12 6C DD         [24]10850 	lcall	_wtimer0_addrelative
                                  10851 ;	COMMON\easyax5043.c:2045: axradio_sync_time = axradio_timer.time;
      004508 90 03 2F         [24]10852 	mov	dptr,#(_axradio_timer + 0x0004)
      00450B E0               [24]10853 	movx	a,@dptr
      00450C FC               [12]10854 	mov	r4,a
      00450D A3               [24]10855 	inc	dptr
      00450E E0               [24]10856 	movx	a,@dptr
      00450F FD               [12]10857 	mov	r5,a
      004510 A3               [24]10858 	inc	dptr
      004511 E0               [24]10859 	movx	a,@dptr
      004512 FE               [12]10860 	mov	r6,a
      004513 A3               [24]10861 	inc	dptr
      004514 E0               [24]10862 	movx	a,@dptr
      004515 FF               [12]10863 	mov	r7,a
      004516 90 00 B2         [24]10864 	mov	dptr,#_axradio_sync_time
      004519 EC               [12]10865 	mov	a,r4
      00451A F0               [24]10866 	movx	@dptr,a
      00451B ED               [12]10867 	mov	a,r5
      00451C A3               [24]10868 	inc	dptr
      00451D F0               [24]10869 	movx	@dptr,a
      00451E EE               [12]10870 	mov	a,r6
      00451F A3               [24]10871 	inc	dptr
      004520 F0               [24]10872 	movx	@dptr,a
      004521 EF               [12]10873 	mov	a,r7
      004522 A3               [24]10874 	inc	dptr
      004523 F0               [24]10875 	movx	@dptr,a
                                  10876 ;	COMMON\easyax5043.c:2046: wtimer_remove_callback(&axradio_cb_receive.cb);
      004524 90 02 D4         [24]10877 	mov	dptr,#_axradio_cb_receive
      004527 12 78 A9         [24]10878 	lcall	_wtimer_remove_callback
                                  10879 ;	COMMON\easyax5043.c:2047: memset_xdata(&axradio_cb_receive.st, 0, sizeof(axradio_cb_receive.st));
      00452A 90 03 C6         [24]10880 	mov	dptr,#_memset_PARM_2
      00452D E4               [12]10881 	clr	a
      00452E F0               [24]10882 	movx	@dptr,a
      00452F 90 03 C7         [24]10883 	mov	dptr,#_memset_PARM_3
      004532 74 1E            [12]10884 	mov	a,#0x1e
      004534 F0               [24]10885 	movx	@dptr,a
      004535 E4               [12]10886 	clr	a
      004536 A3               [24]10887 	inc	dptr
      004537 F0               [24]10888 	movx	@dptr,a
      004538 90 02 D8         [24]10889 	mov	dptr,#(_axradio_cb_receive + 0x0004)
      00453B 75 F0 00         [24]10890 	mov	b,#0x00
      00453E 12 69 59         [24]10891 	lcall	_memset
                                  10892 ;	COMMON\easyax5043.c:2048: axradio_cb_receive.st.time.t = axradio_timeanchor.radiotimer;
      004541 90 00 BC         [24]10893 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      004544 E0               [24]10894 	movx	a,@dptr
      004545 FC               [12]10895 	mov	r4,a
      004546 A3               [24]10896 	inc	dptr
      004547 E0               [24]10897 	movx	a,@dptr
      004548 FD               [12]10898 	mov	r5,a
      004549 A3               [24]10899 	inc	dptr
      00454A E0               [24]10900 	movx	a,@dptr
      00454B FE               [12]10901 	mov	r6,a
      00454C A3               [24]10902 	inc	dptr
      00454D E0               [24]10903 	movx	a,@dptr
      00454E FF               [12]10904 	mov	r7,a
      00454F 90 02 DA         [24]10905 	mov	dptr,#(_axradio_cb_receive + 0x0006)
      004552 EC               [12]10906 	mov	a,r4
      004553 F0               [24]10907 	movx	@dptr,a
      004554 ED               [12]10908 	mov	a,r5
      004555 A3               [24]10909 	inc	dptr
      004556 F0               [24]10910 	movx	@dptr,a
      004557 EE               [12]10911 	mov	a,r6
      004558 A3               [24]10912 	inc	dptr
      004559 F0               [24]10913 	movx	@dptr,a
      00455A EF               [12]10914 	mov	a,r7
      00455B A3               [24]10915 	inc	dptr
      00455C F0               [24]10916 	movx	@dptr,a
                                  10917 ;	COMMON\easyax5043.c:2049: axradio_cb_receive.st.error = AXRADIO_ERR_RESYNC;
      00455D 90 02 D9         [24]10918 	mov	dptr,#(_axradio_cb_receive + 0x0005)
      004560 74 09            [12]10919 	mov	a,#0x09
      004562 F0               [24]10920 	movx	@dptr,a
                                  10921 ;	COMMON\easyax5043.c:2050: wtimer_add_callback(&axradio_cb_receive.cb);
      004563 90 02 D4         [24]10922 	mov	dptr,#_axradio_cb_receive
      004566 12 6C C3         [24]10923 	lcall	_wtimer_add_callback
                                  10924 ;	COMMON\easyax5043.c:2051: return AXRADIO_ERR_NOERROR;
      004569 75 82 00         [24]10925 	mov	dpl,#0x00
                                  10926 ;	COMMON\easyax5043.c:2053: default:
      00456C 22               [24]10927 	ret
      00456D                      10928 00181$:
                                  10929 ;	COMMON\easyax5043.c:2054: return AXRADIO_ERR_NOTSUPPORTED;
      00456D 75 82 01         [24]10930 	mov	dpl,#0x01
                                  10931 ;	COMMON\easyax5043.c:2055: }
      004570 22               [24]10932 	ret
                                  10933 ;------------------------------------------------------------
                                  10934 ;Allocation info for local variables in function 'axradio_get_mode'
                                  10935 ;------------------------------------------------------------
                                  10936 ;	COMMON\easyax5043.c:2058: uint8_t axradio_get_mode(void)
                                  10937 ;	-----------------------------------------
                                  10938 ;	 function axradio_get_mode
                                  10939 ;	-----------------------------------------
      004571                      10940 _axradio_get_mode:
                                  10941 ;	COMMON\easyax5043.c:2060: return axradio_mode;
      004571 85 0B 82         [24]10942 	mov	dpl,_axradio_mode
      004574 22               [24]10943 	ret
                                  10944 ;------------------------------------------------------------
                                  10945 ;Allocation info for local variables in function 'axradio_set_channel'
                                  10946 ;------------------------------------------------------------
                                  10947 ;chnum                     Allocated with name '_axradio_set_channel_chnum_1_425'
                                  10948 ;rng                       Allocated with name '_axradio_set_channel_rng_1_426'
                                  10949 ;f                         Allocated to registers r3 r4 r5 r7 
                                  10950 ;------------------------------------------------------------
                                  10951 ;	COMMON\easyax5043.c:2063: uint8_t axradio_set_channel(uint8_t chnum)
                                  10952 ;	-----------------------------------------
                                  10953 ;	 function axradio_set_channel
                                  10954 ;	-----------------------------------------
      004575                      10955 _axradio_set_channel:
      004575 E5 82            [12]10956 	mov	a,dpl
      004577 90 03 54         [24]10957 	mov	dptr,#_axradio_set_channel_chnum_1_425
      00457A F0               [24]10958 	movx	@dptr,a
                                  10959 ;	COMMON\easyax5043.c:2066: if (chnum >= axradio_phy_nrchannels)
      00457B E0               [24]10960 	movx	a,@dptr
      00457C FF               [12]10961 	mov	r7,a
      00457D 90 7F CB         [24]10962 	mov	dptr,#_axradio_phy_nrchannels
      004580 E4               [12]10963 	clr	a
      004581 93               [24]10964 	movc	a,@a+dptr
      004582 FE               [12]10965 	mov	r6,a
      004583 C3               [12]10966 	clr	c
      004584 EF               [12]10967 	mov	a,r7
      004585 9E               [12]10968 	subb	a,r6
      004586 40 04            [24]10969 	jc	00102$
                                  10970 ;	COMMON\easyax5043.c:2067: return AXRADIO_ERR_INVALID;
      004588 75 82 04         [24]10971 	mov	dpl,#0x04
      00458B 22               [24]10972 	ret
      00458C                      10973 00102$:
                                  10974 ;	COMMON\easyax5043.c:2068: axradio_curchannel = chnum;
      00458C 90 00 AB         [24]10975 	mov	dptr,#_axradio_curchannel
      00458F EF               [12]10976 	mov	a,r7
      004590 F0               [24]10977 	movx	@dptr,a
                                  10978 ;	COMMON\easyax5043.c:2069: rng = axradio_phy_chanpllrng[chnum];
      004591 EF               [12]10979 	mov	a,r7
      004592 24 A0            [12]10980 	add	a,#_axradio_phy_chanpllrng
      004594 F5 82            [12]10981 	mov	dpl,a
      004596 E4               [12]10982 	clr	a
      004597 34 00            [12]10983 	addc	a,#(_axradio_phy_chanpllrng >> 8)
      004599 F5 83            [12]10984 	mov	dph,a
      00459B E0               [24]10985 	movx	a,@dptr
                                  10986 ;	COMMON\easyax5043.c:2070: if (rng & 0x20)
      00459C F5 43            [12]10987 	mov	_axradio_set_channel_rng_1_426,a
      00459E 30 E5 04         [24]10988 	jnb	acc.5,00104$
                                  10989 ;	COMMON\easyax5043.c:2071: return AXRADIO_ERR_RANGING;
      0045A1 75 82 06         [24]10990 	mov	dpl,#0x06
      0045A4 22               [24]10991 	ret
      0045A5                      10992 00104$:
                                  10993 ;	COMMON\easyax5043.c:2073: uint32_t __autodata f = axradio_phy_chanfreq[chnum];
      0045A5 EF               [12]10994 	mov	a,r7
      0045A6 75 F0 04         [24]10995 	mov	b,#0x04
      0045A9 A4               [48]10996 	mul	ab
      0045AA 24 CC            [12]10997 	add	a,#_axradio_phy_chanfreq
      0045AC F5 82            [12]10998 	mov	dpl,a
      0045AE 74 7F            [12]10999 	mov	a,#(_axradio_phy_chanfreq >> 8)
      0045B0 35 F0            [12]11000 	addc	a,b
      0045B2 F5 83            [12]11001 	mov	dph,a
      0045B4 E4               [12]11002 	clr	a
      0045B5 93               [24]11003 	movc	a,@a+dptr
      0045B6 FB               [12]11004 	mov	r3,a
      0045B7 A3               [24]11005 	inc	dptr
      0045B8 E4               [12]11006 	clr	a
      0045B9 93               [24]11007 	movc	a,@a+dptr
      0045BA FC               [12]11008 	mov	r4,a
      0045BB A3               [24]11009 	inc	dptr
      0045BC E4               [12]11010 	clr	a
      0045BD 93               [24]11011 	movc	a,@a+dptr
      0045BE FD               [12]11012 	mov	r5,a
      0045BF A3               [24]11013 	inc	dptr
      0045C0 E4               [12]11014 	clr	a
      0045C1 93               [24]11015 	movc	a,@a+dptr
      0045C2 FF               [12]11016 	mov	r7,a
                                  11017 ;	COMMON\easyax5043.c:2074: f += axradio_curfreqoffset;
      0045C3 90 00 AC         [24]11018 	mov	dptr,#_axradio_curfreqoffset
      0045C6 E0               [24]11019 	movx	a,@dptr
      0045C7 F8               [12]11020 	mov	r0,a
      0045C8 A3               [24]11021 	inc	dptr
      0045C9 E0               [24]11022 	movx	a,@dptr
      0045CA F9               [12]11023 	mov	r1,a
      0045CB A3               [24]11024 	inc	dptr
      0045CC E0               [24]11025 	movx	a,@dptr
      0045CD FA               [12]11026 	mov	r2,a
      0045CE A3               [24]11027 	inc	dptr
      0045CF E0               [24]11028 	movx	a,@dptr
      0045D0 FE               [12]11029 	mov	r6,a
      0045D1 E8               [12]11030 	mov	a,r0
      0045D2 2B               [12]11031 	add	a,r3
      0045D3 FB               [12]11032 	mov	r3,a
      0045D4 E9               [12]11033 	mov	a,r1
      0045D5 3C               [12]11034 	addc	a,r4
      0045D6 FC               [12]11035 	mov	r4,a
      0045D7 EA               [12]11036 	mov	a,r2
      0045D8 3D               [12]11037 	addc	a,r5
      0045D9 FD               [12]11038 	mov	r5,a
      0045DA EE               [12]11039 	mov	a,r6
      0045DB 3F               [12]11040 	addc	a,r7
      0045DC FF               [12]11041 	mov	r7,a
                                  11042 ;	COMMON\easyax5043.c:2075: if (AX5043_PLLLOOP & 0x80) {
      0045DD 90 40 30         [24]11043 	mov	dptr,#_AX5043_PLLLOOP
      0045E0 E0               [24]11044 	movx	a,@dptr
      0045E1 FE               [12]11045 	mov	r6,a
      0045E2 30 E7 1E         [24]11046 	jnb	acc.7,00106$
                                  11047 ;	COMMON\easyax5043.c:2076: AX5043_PLLRANGINGA = rng & 0x0F;
      0045E5 90 40 33         [24]11048 	mov	dptr,#_AX5043_PLLRANGINGA
      0045E8 74 0F            [12]11049 	mov	a,#0x0f
      0045EA 55 43            [12]11050 	anl	a,_axradio_set_channel_rng_1_426
      0045EC F0               [24]11051 	movx	@dptr,a
                                  11052 ;	COMMON\easyax5043.c:2077: AX5043_FREQA0 = f;
      0045ED 90 40 37         [24]11053 	mov	dptr,#_AX5043_FREQA0
      0045F0 EB               [12]11054 	mov	a,r3
      0045F1 F0               [24]11055 	movx	@dptr,a
                                  11056 ;	COMMON\easyax5043.c:2078: AX5043_FREQA1 = f >> 8;
      0045F2 90 40 36         [24]11057 	mov	dptr,#_AX5043_FREQA1
      0045F5 EC               [12]11058 	mov	a,r4
      0045F6 F0               [24]11059 	movx	@dptr,a
                                  11060 ;	COMMON\easyax5043.c:2079: AX5043_FREQA2 = f >> 16;
      0045F7 90 40 35         [24]11061 	mov	dptr,#_AX5043_FREQA2
      0045FA ED               [12]11062 	mov	a,r5
      0045FB F0               [24]11063 	movx	@dptr,a
                                  11064 ;	COMMON\easyax5043.c:2080: AX5043_FREQA3 = f >> 24;
      0045FC 90 40 34         [24]11065 	mov	dptr,#_AX5043_FREQA3
      0045FF EF               [12]11066 	mov	a,r7
      004600 F0               [24]11067 	movx	@dptr,a
      004601 80 1C            [24]11068 	sjmp	00107$
      004603                      11069 00106$:
                                  11070 ;	COMMON\easyax5043.c:2082: AX5043_PLLRANGINGB = rng & 0x0F;
      004603 90 40 3B         [24]11071 	mov	dptr,#_AX5043_PLLRANGINGB
      004606 74 0F            [12]11072 	mov	a,#0x0f
      004608 55 43            [12]11073 	anl	a,_axradio_set_channel_rng_1_426
      00460A F0               [24]11074 	movx	@dptr,a
                                  11075 ;	COMMON\easyax5043.c:2083: AX5043_FREQB0 = f;
      00460B 90 40 3F         [24]11076 	mov	dptr,#_AX5043_FREQB0
      00460E EB               [12]11077 	mov	a,r3
      00460F F0               [24]11078 	movx	@dptr,a
                                  11079 ;	COMMON\easyax5043.c:2084: AX5043_FREQB1 = f >> 8;
      004610 90 40 3E         [24]11080 	mov	dptr,#_AX5043_FREQB1
      004613 EC               [12]11081 	mov	a,r4
      004614 F0               [24]11082 	movx	@dptr,a
                                  11083 ;	COMMON\easyax5043.c:2085: AX5043_FREQB2 = f >> 16;
      004615 90 40 3D         [24]11084 	mov	dptr,#_AX5043_FREQB2
      004618 ED               [12]11085 	mov	a,r5
      004619 F0               [24]11086 	movx	@dptr,a
                                  11087 ;	COMMON\easyax5043.c:2086: AX5043_FREQB3 = f >> 24;
      00461A 90 40 3C         [24]11088 	mov	dptr,#_AX5043_FREQB3
      00461D EF               [12]11089 	mov	a,r7
      00461E F0               [24]11090 	movx	@dptr,a
      00461F                      11091 00107$:
                                  11092 ;	COMMON\easyax5043.c:2089: AX5043_PLLLOOP ^= 0x80;
      00461F 90 40 30         [24]11093 	mov	dptr,#_AX5043_PLLLOOP
      004622 E0               [24]11094 	movx	a,@dptr
      004623 FF               [12]11095 	mov	r7,a
      004624 74 80            [12]11096 	mov	a,#0x80
      004626 6F               [12]11097 	xrl	a,r7
      004627 F0               [24]11098 	movx	@dptr,a
                                  11099 ;	COMMON\easyax5043.c:2090: return AXRADIO_ERR_NOERROR;
      004628 75 82 00         [24]11100 	mov	dpl,#0x00
      00462B 22               [24]11101 	ret
                                  11102 ;------------------------------------------------------------
                                  11103 ;Allocation info for local variables in function 'axradio_get_channel'
                                  11104 ;------------------------------------------------------------
                                  11105 ;	COMMON\easyax5043.c:2093: uint8_t axradio_get_channel(void)
                                  11106 ;	-----------------------------------------
                                  11107 ;	 function axradio_get_channel
                                  11108 ;	-----------------------------------------
      00462C                      11109 _axradio_get_channel:
                                  11110 ;	COMMON\easyax5043.c:2095: return axradio_curchannel;
      00462C 90 00 AB         [24]11111 	mov	dptr,#_axradio_curchannel
      00462F E0               [24]11112 	movx	a,@dptr
      004630 F5 82            [12]11113 	mov	dpl,a
      004632 22               [24]11114 	ret
                                  11115 ;------------------------------------------------------------
                                  11116 ;Allocation info for local variables in function 'axradio_get_pllrange'
                                  11117 ;------------------------------------------------------------
                                  11118 ;	COMMON\easyax5043.c:2098: uint8_t axradio_get_pllrange(void)
                                  11119 ;	-----------------------------------------
                                  11120 ;	 function axradio_get_pllrange
                                  11121 ;	-----------------------------------------
      004633                      11122 _axradio_get_pllrange:
                                  11123 ;	COMMON\easyax5043.c:2100: return axradio_phy_chanpllrng[axradio_curchannel] & 0x0F;
      004633 90 00 AB         [24]11124 	mov	dptr,#_axradio_curchannel
      004636 E0               [24]11125 	movx	a,@dptr
      004637 24 A0            [12]11126 	add	a,#_axradio_phy_chanpllrng
      004639 F5 82            [12]11127 	mov	dpl,a
      00463B E4               [12]11128 	clr	a
      00463C 34 00            [12]11129 	addc	a,#(_axradio_phy_chanpllrng >> 8)
      00463E F5 83            [12]11130 	mov	dph,a
      004640 E0               [24]11131 	movx	a,@dptr
      004641 FF               [12]11132 	mov	r7,a
      004642 53 07 0F         [24]11133 	anl	ar7,#0x0f
      004645 8F 82            [24]11134 	mov	dpl,r7
      004647 22               [24]11135 	ret
                                  11136 ;------------------------------------------------------------
                                  11137 ;Allocation info for local variables in function 'axradio_get_pllvcoi'
                                  11138 ;------------------------------------------------------------
                                  11139 ;x                         Allocated with name '_axradio_get_pllvcoi_x_2_436'
                                  11140 ;x                         Allocated with name '_axradio_get_pllvcoi_x_2_437'
                                  11141 ;------------------------------------------------------------
                                  11142 ;	COMMON\easyax5043.c:2103: uint8_t axradio_get_pllvcoi(void)
                                  11143 ;	-----------------------------------------
                                  11144 ;	 function axradio_get_pllvcoi
                                  11145 ;	-----------------------------------------
      004648                      11146 _axradio_get_pllvcoi:
                                  11147 ;	COMMON\easyax5043.c:2105: if (axradio_phy_vcocalib) {
      004648 90 7F D2         [24]11148 	mov	dptr,#_axradio_phy_vcocalib
      00464B E4               [12]11149 	clr	a
      00464C 93               [24]11150 	movc	a,@a+dptr
      00464D 60 15            [24]11151 	jz	00104$
                                  11152 ;	COMMON\easyax5043.c:2106: uint8_t x = axradio_phy_chanvcoi[axradio_curchannel];
      00464F 90 00 AB         [24]11153 	mov	dptr,#_axradio_curchannel
      004652 E0               [24]11154 	movx	a,@dptr
      004653 24 A1            [12]11155 	add	a,#_axradio_phy_chanvcoi
      004655 F5 82            [12]11156 	mov	dpl,a
      004657 E4               [12]11157 	clr	a
      004658 34 00            [12]11158 	addc	a,#(_axradio_phy_chanvcoi >> 8)
      00465A F5 83            [12]11159 	mov	dph,a
      00465C E0               [24]11160 	movx	a,@dptr
                                  11161 ;	COMMON\easyax5043.c:2107: if (x & 0x80)
      00465D FF               [12]11162 	mov	r7,a
      00465E 30 E7 03         [24]11163 	jnb	acc.7,00104$
                                  11164 ;	COMMON\easyax5043.c:2108: return x;
      004661 8F 82            [24]11165 	mov	dpl,r7
      004663 22               [24]11166 	ret
      004664                      11167 00104$:
                                  11168 ;	COMMON\easyax5043.c:2111: uint8_t x = axradio_phy_chanvcoiinit[axradio_curchannel];
      004664 90 00 AB         [24]11169 	mov	dptr,#_axradio_curchannel
      004667 E0               [24]11170 	movx	a,@dptr
      004668 FF               [12]11171 	mov	r7,a
      004669 90 7F D1         [24]11172 	mov	dptr,#_axradio_phy_chanvcoiinit
      00466C 93               [24]11173 	movc	a,@a+dptr
      00466D FE               [12]11174 	mov	r6,a
      00466E 90 03 55         [24]11175 	mov	dptr,#_axradio_get_pllvcoi_x_2_437
      004671 F0               [24]11176 	movx	@dptr,a
                                  11177 ;	COMMON\easyax5043.c:2112: if (x & 0x80) {
      004672 EE               [12]11178 	mov	a,r6
      004673 30 E7 37         [24]11179 	jnb	acc.7,00108$
                                  11180 ;	COMMON\easyax5043.c:2113: if (!(axradio_phy_chanpllrnginit[0] & 0xF0)) {
      004676 90 7F D0         [24]11181 	mov	dptr,#_axradio_phy_chanpllrnginit
      004679 E4               [12]11182 	clr	a
      00467A 93               [24]11183 	movc	a,@a+dptr
      00467B FD               [12]11184 	mov	r5,a
      00467C 54 F0            [12]11185 	anl	a,#0xf0
      00467E 60 02            [24]11186 	jz	00127$
      004680 80 24            [24]11187 	sjmp	00106$
      004682                      11188 00127$:
                                  11189 ;	COMMON\easyax5043.c:2114: x += (axradio_phy_chanpllrng[axradio_curchannel] & 0x0F) - (axradio_phy_chanpllrnginit[axradio_curchannel] & 0x0F);
      004682 EF               [12]11190 	mov	a,r7
      004683 24 A0            [12]11191 	add	a,#_axradio_phy_chanpllrng
      004685 F5 82            [12]11192 	mov	dpl,a
      004687 E4               [12]11193 	clr	a
      004688 34 00            [12]11194 	addc	a,#(_axradio_phy_chanpllrng >> 8)
      00468A F5 83            [12]11195 	mov	dph,a
      00468C E0               [24]11196 	movx	a,@dptr
      00468D FD               [12]11197 	mov	r5,a
      00468E 53 05 0F         [24]11198 	anl	ar5,#0x0f
      004691 EF               [12]11199 	mov	a,r7
      004692 90 7F D0         [24]11200 	mov	dptr,#_axradio_phy_chanpllrnginit
      004695 93               [24]11201 	movc	a,@a+dptr
      004696 FF               [12]11202 	mov	r7,a
      004697 74 0F            [12]11203 	mov	a,#0x0f
      004699 5F               [12]11204 	anl	a,r7
      00469A D3               [12]11205 	setb	c
      00469B 9D               [12]11206 	subb	a,r5
      00469C F4               [12]11207 	cpl	a
      00469D 2E               [12]11208 	add	a,r6
                                  11209 ;	COMMON\easyax5043.c:2115: x &= 0x3f;
      00469E 54 3F            [12]11210 	anl	a,#0x3f
                                  11211 ;	COMMON\easyax5043.c:2116: x |= 0x80;
      0046A0 90 03 55         [24]11212 	mov	dptr,#_axradio_get_pllvcoi_x_2_437
      0046A3 44 80            [12]11213 	orl	a,#0x80
      0046A5 F0               [24]11214 	movx	@dptr,a
      0046A6                      11215 00106$:
                                  11216 ;	COMMON\easyax5043.c:2118: return x;
      0046A6 90 03 55         [24]11217 	mov	dptr,#_axradio_get_pllvcoi_x_2_437
      0046A9 E0               [24]11218 	movx	a,@dptr
      0046AA F5 82            [12]11219 	mov	dpl,a
      0046AC 22               [24]11220 	ret
      0046AD                      11221 00108$:
                                  11222 ;	COMMON\easyax5043.c:2121: return AX5043_PLLVCOI;
      0046AD 90 41 80         [24]11223 	mov	dptr,#_AX5043_PLLVCOI
      0046B0 E0               [24]11224 	movx	a,@dptr
      0046B1 F5 82            [12]11225 	mov	dpl,a
      0046B3 22               [24]11226 	ret
                                  11227 ;------------------------------------------------------------
                                  11228 ;Allocation info for local variables in function 'axradio_set_curfreqoffset'
                                  11229 ;------------------------------------------------------------
                                  11230 ;offs                      Allocated with name '_axradio_set_curfreqoffset_offs_1_440'
                                  11231 ;------------------------------------------------------------
                                  11232 ;	COMMON\easyax5043.c:2124: static uint8_t axradio_set_curfreqoffset(int32_t offs)
                                  11233 ;	-----------------------------------------
                                  11234 ;	 function axradio_set_curfreqoffset
                                  11235 ;	-----------------------------------------
      0046B4                      11236 _axradio_set_curfreqoffset:
      0046B4 AF 82            [24]11237 	mov	r7,dpl
      0046B6 AE 83            [24]11238 	mov	r6,dph
      0046B8 AD F0            [24]11239 	mov	r5,b
      0046BA FC               [12]11240 	mov	r4,a
      0046BB 90 03 56         [24]11241 	mov	dptr,#_axradio_set_curfreqoffset_offs_1_440
      0046BE EF               [12]11242 	mov	a,r7
      0046BF F0               [24]11243 	movx	@dptr,a
      0046C0 EE               [12]11244 	mov	a,r6
      0046C1 A3               [24]11245 	inc	dptr
      0046C2 F0               [24]11246 	movx	@dptr,a
      0046C3 ED               [12]11247 	mov	a,r5
      0046C4 A3               [24]11248 	inc	dptr
      0046C5 F0               [24]11249 	movx	@dptr,a
      0046C6 EC               [12]11250 	mov	a,r4
      0046C7 A3               [24]11251 	inc	dptr
      0046C8 F0               [24]11252 	movx	@dptr,a
                                  11253 ;	COMMON\easyax5043.c:2126: axradio_curfreqoffset = offs;
      0046C9 90 03 56         [24]11254 	mov	dptr,#_axradio_set_curfreqoffset_offs_1_440
      0046CC E0               [24]11255 	movx	a,@dptr
      0046CD FC               [12]11256 	mov	r4,a
      0046CE A3               [24]11257 	inc	dptr
      0046CF E0               [24]11258 	movx	a,@dptr
      0046D0 FD               [12]11259 	mov	r5,a
      0046D1 A3               [24]11260 	inc	dptr
      0046D2 E0               [24]11261 	movx	a,@dptr
      0046D3 FE               [12]11262 	mov	r6,a
      0046D4 A3               [24]11263 	inc	dptr
      0046D5 E0               [24]11264 	movx	a,@dptr
      0046D6 FF               [12]11265 	mov	r7,a
      0046D7 90 00 AC         [24]11266 	mov	dptr,#_axradio_curfreqoffset
      0046DA EC               [12]11267 	mov	a,r4
      0046DB F0               [24]11268 	movx	@dptr,a
      0046DC ED               [12]11269 	mov	a,r5
      0046DD A3               [24]11270 	inc	dptr
      0046DE F0               [24]11271 	movx	@dptr,a
      0046DF EE               [12]11272 	mov	a,r6
      0046E0 A3               [24]11273 	inc	dptr
      0046E1 F0               [24]11274 	movx	@dptr,a
      0046E2 EF               [12]11275 	mov	a,r7
      0046E3 A3               [24]11276 	inc	dptr
      0046E4 F0               [24]11277 	movx	@dptr,a
                                  11278 ;	COMMON\easyax5043.c:2127: if (checksignedlimit32(offs, axradio_phy_maxfreqoffset))
      0046E5 90 7F D3         [24]11279 	mov	dptr,#_axradio_phy_maxfreqoffset
      0046E8 E4               [12]11280 	clr	a
      0046E9 93               [24]11281 	movc	a,@a+dptr
      0046EA C0 E0            [24]11282 	push	acc
      0046EC 74 01            [12]11283 	mov	a,#0x01
      0046EE 93               [24]11284 	movc	a,@a+dptr
      0046EF C0 E0            [24]11285 	push	acc
      0046F1 74 02            [12]11286 	mov	a,#0x02
      0046F3 93               [24]11287 	movc	a,@a+dptr
      0046F4 C0 E0            [24]11288 	push	acc
      0046F6 74 03            [12]11289 	mov	a,#0x03
      0046F8 93               [24]11290 	movc	a,@a+dptr
      0046F9 C0 E0            [24]11291 	push	acc
      0046FB 8C 82            [24]11292 	mov	dpl,r4
      0046FD 8D 83            [24]11293 	mov	dph,r5
      0046FF 8E F0            [24]11294 	mov	b,r6
      004701 EF               [12]11295 	mov	a,r7
      004702 12 73 AA         [24]11296 	lcall	_checksignedlimit32
      004705 AF 82            [24]11297 	mov	r7,dpl
      004707 E5 81            [12]11298 	mov	a,sp
      004709 24 FC            [12]11299 	add	a,#0xfc
      00470B F5 81            [12]11300 	mov	sp,a
      00470D EF               [12]11301 	mov	a,r7
      00470E 60 04            [24]11302 	jz	00102$
                                  11303 ;	COMMON\easyax5043.c:2128: return AXRADIO_ERR_NOERROR;
      004710 75 82 00         [24]11304 	mov	dpl,#0x00
      004713 22               [24]11305 	ret
      004714                      11306 00102$:
                                  11307 ;	COMMON\easyax5043.c:2129: if (axradio_curfreqoffset < 0)
      004714 90 00 AC         [24]11308 	mov	dptr,#_axradio_curfreqoffset
      004717 E0               [24]11309 	movx	a,@dptr
      004718 FC               [12]11310 	mov	r4,a
      004719 A3               [24]11311 	inc	dptr
      00471A E0               [24]11312 	movx	a,@dptr
      00471B FD               [12]11313 	mov	r5,a
      00471C A3               [24]11314 	inc	dptr
      00471D E0               [24]11315 	movx	a,@dptr
      00471E FE               [12]11316 	mov	r6,a
      00471F A3               [24]11317 	inc	dptr
      004720 E0               [24]11318 	movx	a,@dptr
      004721 FF               [12]11319 	mov	r7,a
      004722 30 E7 27         [24]11320 	jnb	acc.7,00104$
                                  11321 ;	COMMON\easyax5043.c:2130: axradio_curfreqoffset = -axradio_phy_maxfreqoffset;
      004725 90 7F D3         [24]11322 	mov	dptr,#_axradio_phy_maxfreqoffset
      004728 E4               [12]11323 	clr	a
      004729 93               [24]11324 	movc	a,@a+dptr
      00472A FC               [12]11325 	mov	r4,a
      00472B 74 01            [12]11326 	mov	a,#0x01
      00472D 93               [24]11327 	movc	a,@a+dptr
      00472E FD               [12]11328 	mov	r5,a
      00472F 74 02            [12]11329 	mov	a,#0x02
      004731 93               [24]11330 	movc	a,@a+dptr
      004732 FE               [12]11331 	mov	r6,a
      004733 74 03            [12]11332 	mov	a,#0x03
      004735 93               [24]11333 	movc	a,@a+dptr
      004736 FF               [12]11334 	mov	r7,a
      004737 90 00 AC         [24]11335 	mov	dptr,#_axradio_curfreqoffset
      00473A C3               [12]11336 	clr	c
      00473B E4               [12]11337 	clr	a
      00473C 9C               [12]11338 	subb	a,r4
      00473D F0               [24]11339 	movx	@dptr,a
      00473E E4               [12]11340 	clr	a
      00473F 9D               [12]11341 	subb	a,r5
      004740 A3               [24]11342 	inc	dptr
      004741 F0               [24]11343 	movx	@dptr,a
      004742 E4               [12]11344 	clr	a
      004743 9E               [12]11345 	subb	a,r6
      004744 A3               [24]11346 	inc	dptr
      004745 F0               [24]11347 	movx	@dptr,a
      004746 E4               [12]11348 	clr	a
      004747 9F               [12]11349 	subb	a,r7
      004748 A3               [24]11350 	inc	dptr
      004749 F0               [24]11351 	movx	@dptr,a
      00474A 80 20            [24]11352 	sjmp	00105$
      00474C                      11353 00104$:
                                  11354 ;	COMMON\easyax5043.c:2132: axradio_curfreqoffset = axradio_phy_maxfreqoffset;
      00474C 90 7F D3         [24]11355 	mov	dptr,#_axradio_phy_maxfreqoffset
      00474F E4               [12]11356 	clr	a
      004750 93               [24]11357 	movc	a,@a+dptr
      004751 FC               [12]11358 	mov	r4,a
      004752 74 01            [12]11359 	mov	a,#0x01
      004754 93               [24]11360 	movc	a,@a+dptr
      004755 FD               [12]11361 	mov	r5,a
      004756 74 02            [12]11362 	mov	a,#0x02
      004758 93               [24]11363 	movc	a,@a+dptr
      004759 FE               [12]11364 	mov	r6,a
      00475A 74 03            [12]11365 	mov	a,#0x03
      00475C 93               [24]11366 	movc	a,@a+dptr
      00475D FF               [12]11367 	mov	r7,a
      00475E 90 00 AC         [24]11368 	mov	dptr,#_axradio_curfreqoffset
      004761 EC               [12]11369 	mov	a,r4
      004762 F0               [24]11370 	movx	@dptr,a
      004763 ED               [12]11371 	mov	a,r5
      004764 A3               [24]11372 	inc	dptr
      004765 F0               [24]11373 	movx	@dptr,a
      004766 EE               [12]11374 	mov	a,r6
      004767 A3               [24]11375 	inc	dptr
      004768 F0               [24]11376 	movx	@dptr,a
      004769 EF               [12]11377 	mov	a,r7
      00476A A3               [24]11378 	inc	dptr
      00476B F0               [24]11379 	movx	@dptr,a
      00476C                      11380 00105$:
                                  11381 ;	COMMON\easyax5043.c:2133: return AXRADIO_ERR_INVALID;
      00476C 75 82 04         [24]11382 	mov	dpl,#0x04
      00476F 22               [24]11383 	ret
                                  11384 ;------------------------------------------------------------
                                  11385 ;Allocation info for local variables in function 'axradio_set_freqoffset'
                                  11386 ;------------------------------------------------------------
                                  11387 ;ret                       Allocated to registers r7 
                                  11388 ;ret2                      Allocated to registers r6 
                                  11389 ;offs                      Allocated with name '_axradio_set_freqoffset_offs_1_442'
                                  11390 ;------------------------------------------------------------
                                  11391 ;	COMMON\easyax5043.c:2136: uint8_t axradio_set_freqoffset(int32_t offs)
                                  11392 ;	-----------------------------------------
                                  11393 ;	 function axradio_set_freqoffset
                                  11394 ;	-----------------------------------------
      004770                      11395 _axradio_set_freqoffset:
      004770 AF 82            [24]11396 	mov	r7,dpl
      004772 AE 83            [24]11397 	mov	r6,dph
      004774 AD F0            [24]11398 	mov	r5,b
      004776 FC               [12]11399 	mov	r4,a
      004777 90 03 5A         [24]11400 	mov	dptr,#_axradio_set_freqoffset_offs_1_442
      00477A EF               [12]11401 	mov	a,r7
      00477B F0               [24]11402 	movx	@dptr,a
      00477C EE               [12]11403 	mov	a,r6
      00477D A3               [24]11404 	inc	dptr
      00477E F0               [24]11405 	movx	@dptr,a
      00477F ED               [12]11406 	mov	a,r5
      004780 A3               [24]11407 	inc	dptr
      004781 F0               [24]11408 	movx	@dptr,a
      004782 EC               [12]11409 	mov	a,r4
      004783 A3               [24]11410 	inc	dptr
      004784 F0               [24]11411 	movx	@dptr,a
                                  11412 ;	COMMON\easyax5043.c:2138: uint8_t __autodata ret = axradio_set_curfreqoffset(offs);
      004785 90 03 5A         [24]11413 	mov	dptr,#_axradio_set_freqoffset_offs_1_442
      004788 E0               [24]11414 	movx	a,@dptr
      004789 FC               [12]11415 	mov	r4,a
      00478A A3               [24]11416 	inc	dptr
      00478B E0               [24]11417 	movx	a,@dptr
      00478C FD               [12]11418 	mov	r5,a
      00478D A3               [24]11419 	inc	dptr
      00478E E0               [24]11420 	movx	a,@dptr
      00478F FE               [12]11421 	mov	r6,a
      004790 A3               [24]11422 	inc	dptr
      004791 E0               [24]11423 	movx	a,@dptr
      004792 8C 82            [24]11424 	mov	dpl,r4
      004794 8D 83            [24]11425 	mov	dph,r5
      004796 8E F0            [24]11426 	mov	b,r6
      004798 12 46 B4         [24]11427 	lcall	_axradio_set_curfreqoffset
      00479B AF 82            [24]11428 	mov	r7,dpl
                                  11429 ;	COMMON\easyax5043.c:2140: uint8_t __autodata ret2 = axradio_set_channel(axradio_curchannel);
      00479D 90 00 AB         [24]11430 	mov	dptr,#_axradio_curchannel
      0047A0 E0               [24]11431 	movx	a,@dptr
      0047A1 F5 82            [12]11432 	mov	dpl,a
      0047A3 C0 07            [24]11433 	push	ar7
      0047A5 12 45 75         [24]11434 	lcall	_axradio_set_channel
      0047A8 AE 82            [24]11435 	mov	r6,dpl
      0047AA D0 07            [24]11436 	pop	ar7
                                  11437 ;	COMMON\easyax5043.c:2141: if (ret == AXRADIO_ERR_NOERROR)
      0047AC EF               [12]11438 	mov	a,r7
      0047AD 70 02            [24]11439 	jnz	00102$
                                  11440 ;	COMMON\easyax5043.c:2142: ret = ret2;
      0047AF 8E 07            [24]11441 	mov	ar7,r6
      0047B1                      11442 00102$:
                                  11443 ;	COMMON\easyax5043.c:2144: return ret;
      0047B1 8F 82            [24]11444 	mov	dpl,r7
      0047B3 22               [24]11445 	ret
                                  11446 ;------------------------------------------------------------
                                  11447 ;Allocation info for local variables in function 'axradio_get_freqoffset'
                                  11448 ;------------------------------------------------------------
                                  11449 ;	COMMON\easyax5043.c:2147: int32_t axradio_get_freqoffset(void)
                                  11450 ;	-----------------------------------------
                                  11451 ;	 function axradio_get_freqoffset
                                  11452 ;	-----------------------------------------
      0047B4                      11453 _axradio_get_freqoffset:
                                  11454 ;	COMMON\easyax5043.c:2149: return axradio_curfreqoffset;
      0047B4 90 00 AC         [24]11455 	mov	dptr,#_axradio_curfreqoffset
      0047B7 E0               [24]11456 	movx	a,@dptr
      0047B8 FC               [12]11457 	mov	r4,a
      0047B9 A3               [24]11458 	inc	dptr
      0047BA E0               [24]11459 	movx	a,@dptr
      0047BB FD               [12]11460 	mov	r5,a
      0047BC A3               [24]11461 	inc	dptr
      0047BD E0               [24]11462 	movx	a,@dptr
      0047BE FE               [12]11463 	mov	r6,a
      0047BF A3               [24]11464 	inc	dptr
      0047C0 E0               [24]11465 	movx	a,@dptr
      0047C1 8C 82            [24]11466 	mov	dpl,r4
      0047C3 8D 83            [24]11467 	mov	dph,r5
      0047C5 8E F0            [24]11468 	mov	b,r6
      0047C7 22               [24]11469 	ret
                                  11470 ;------------------------------------------------------------
                                  11471 ;Allocation info for local variables in function 'axradio_set_local_address'
                                  11472 ;------------------------------------------------------------
                                  11473 ;addr                      Allocated with name '_axradio_set_local_address_addr_1_447'
                                  11474 ;------------------------------------------------------------
                                  11475 ;	COMMON\easyax5043.c:2152: void axradio_set_local_address(const struct axradio_address_mask __generic *addr)
                                  11476 ;	-----------------------------------------
                                  11477 ;	 function axradio_set_local_address
                                  11478 ;	-----------------------------------------
      0047C8                      11479 _axradio_set_local_address:
      0047C8 AF F0            [24]11480 	mov	r7,b
      0047CA AE 83            [24]11481 	mov	r6,dph
      0047CC E5 82            [12]11482 	mov	a,dpl
      0047CE 90 03 5E         [24]11483 	mov	dptr,#_axradio_set_local_address_addr_1_447
      0047D1 F0               [24]11484 	movx	@dptr,a
      0047D2 EE               [12]11485 	mov	a,r6
      0047D3 A3               [24]11486 	inc	dptr
      0047D4 F0               [24]11487 	movx	@dptr,a
      0047D5 EF               [12]11488 	mov	a,r7
      0047D6 A3               [24]11489 	inc	dptr
      0047D7 F0               [24]11490 	movx	@dptr,a
                                  11491 ;	COMMON\easyax5043.c:2154: memcpy_xdatageneric(&axradio_localaddr, addr, sizeof(axradio_localaddr));
      0047D8 90 03 5E         [24]11492 	mov	dptr,#_axradio_set_local_address_addr_1_447
      0047DB E0               [24]11493 	movx	a,@dptr
      0047DC FD               [12]11494 	mov	r5,a
      0047DD A3               [24]11495 	inc	dptr
      0047DE E0               [24]11496 	movx	a,@dptr
      0047DF FE               [12]11497 	mov	r6,a
      0047E0 A3               [24]11498 	inc	dptr
      0047E1 E0               [24]11499 	movx	a,@dptr
      0047E2 FF               [12]11500 	mov	r7,a
      0047E3 90 03 DB         [24]11501 	mov	dptr,#_memcpy_PARM_2
      0047E6 ED               [12]11502 	mov	a,r5
      0047E7 F0               [24]11503 	movx	@dptr,a
      0047E8 EE               [12]11504 	mov	a,r6
      0047E9 A3               [24]11505 	inc	dptr
      0047EA F0               [24]11506 	movx	@dptr,a
      0047EB EF               [12]11507 	mov	a,r7
      0047EC A3               [24]11508 	inc	dptr
      0047ED F0               [24]11509 	movx	@dptr,a
      0047EE 90 03 DE         [24]11510 	mov	dptr,#_memcpy_PARM_3
      0047F1 74 08            [12]11511 	mov	a,#0x08
      0047F3 F0               [24]11512 	movx	@dptr,a
      0047F4 E4               [12]11513 	clr	a
      0047F5 A3               [24]11514 	inc	dptr
      0047F6 F0               [24]11515 	movx	@dptr,a
      0047F7 90 00 C0         [24]11516 	mov	dptr,#_axradio_localaddr
      0047FA 75 F0 00         [24]11517 	mov	b,#0x00
      0047FD 12 6C 4F         [24]11518 	lcall	_memcpy
                                  11519 ;	COMMON\easyax5043.c:2155: axradio_setaddrregs();
      004800 02 29 0C         [24]11520 	ljmp	_axradio_setaddrregs
                                  11521 ;------------------------------------------------------------
                                  11522 ;Allocation info for local variables in function 'axradio_get_local_address'
                                  11523 ;------------------------------------------------------------
                                  11524 ;addr                      Allocated with name '_axradio_get_local_address_addr_1_449'
                                  11525 ;------------------------------------------------------------
                                  11526 ;	COMMON\easyax5043.c:2158: void axradio_get_local_address(struct axradio_address_mask __generic *addr)
                                  11527 ;	-----------------------------------------
                                  11528 ;	 function axradio_get_local_address
                                  11529 ;	-----------------------------------------
      004803                      11530 _axradio_get_local_address:
      004803 AF F0            [24]11531 	mov	r7,b
      004805 AE 83            [24]11532 	mov	r6,dph
      004807 E5 82            [12]11533 	mov	a,dpl
      004809 90 03 61         [24]11534 	mov	dptr,#_axradio_get_local_address_addr_1_449
      00480C F0               [24]11535 	movx	@dptr,a
      00480D EE               [12]11536 	mov	a,r6
      00480E A3               [24]11537 	inc	dptr
      00480F F0               [24]11538 	movx	@dptr,a
      004810 EF               [12]11539 	mov	a,r7
      004811 A3               [24]11540 	inc	dptr
      004812 F0               [24]11541 	movx	@dptr,a
                                  11542 ;	COMMON\easyax5043.c:2160: memcpy_genericxdata(addr, &axradio_localaddr, sizeof(axradio_localaddr));
      004813 90 03 61         [24]11543 	mov	dptr,#_axradio_get_local_address_addr_1_449
      004816 E0               [24]11544 	movx	a,@dptr
      004817 FD               [12]11545 	mov	r5,a
      004818 A3               [24]11546 	inc	dptr
      004819 E0               [24]11547 	movx	a,@dptr
      00481A FE               [12]11548 	mov	r6,a
      00481B A3               [24]11549 	inc	dptr
      00481C E0               [24]11550 	movx	a,@dptr
      00481D FF               [12]11551 	mov	r7,a
      00481E 90 03 DB         [24]11552 	mov	dptr,#_memcpy_PARM_2
      004821 74 C0            [12]11553 	mov	a,#_axradio_localaddr
      004823 F0               [24]11554 	movx	@dptr,a
      004824 74 00            [12]11555 	mov	a,#(_axradio_localaddr >> 8)
      004826 A3               [24]11556 	inc	dptr
      004827 F0               [24]11557 	movx	@dptr,a
      004828 E4               [12]11558 	clr	a
      004829 A3               [24]11559 	inc	dptr
      00482A F0               [24]11560 	movx	@dptr,a
      00482B 90 03 DE         [24]11561 	mov	dptr,#_memcpy_PARM_3
      00482E 74 08            [12]11562 	mov	a,#0x08
      004830 F0               [24]11563 	movx	@dptr,a
      004831 E4               [12]11564 	clr	a
      004832 A3               [24]11565 	inc	dptr
      004833 F0               [24]11566 	movx	@dptr,a
      004834 8D 82            [24]11567 	mov	dpl,r5
      004836 8E 83            [24]11568 	mov	dph,r6
      004838 8F F0            [24]11569 	mov	b,r7
      00483A 02 6C 4F         [24]11570 	ljmp	_memcpy
                                  11571 ;------------------------------------------------------------
                                  11572 ;Allocation info for local variables in function 'axradio_set_default_remote_address'
                                  11573 ;------------------------------------------------------------
                                  11574 ;addr                      Allocated with name '_axradio_set_default_remote_address_addr_1_451'
                                  11575 ;------------------------------------------------------------
                                  11576 ;	COMMON\easyax5043.c:2163: void axradio_set_default_remote_address(const struct axradio_address __generic *addr)
                                  11577 ;	-----------------------------------------
                                  11578 ;	 function axradio_set_default_remote_address
                                  11579 ;	-----------------------------------------
      00483D                      11580 _axradio_set_default_remote_address:
      00483D AF F0            [24]11581 	mov	r7,b
      00483F AE 83            [24]11582 	mov	r6,dph
      004841 E5 82            [12]11583 	mov	a,dpl
      004843 90 03 64         [24]11584 	mov	dptr,#_axradio_set_default_remote_address_addr_1_451
      004846 F0               [24]11585 	movx	@dptr,a
      004847 EE               [12]11586 	mov	a,r6
      004848 A3               [24]11587 	inc	dptr
      004849 F0               [24]11588 	movx	@dptr,a
      00484A EF               [12]11589 	mov	a,r7
      00484B A3               [24]11590 	inc	dptr
      00484C F0               [24]11591 	movx	@dptr,a
                                  11592 ;	COMMON\easyax5043.c:2165: memcpy_xdatageneric(&axradio_default_remoteaddr, addr, sizeof(axradio_default_remoteaddr));
      00484D 90 03 64         [24]11593 	mov	dptr,#_axradio_set_default_remote_address_addr_1_451
      004850 E0               [24]11594 	movx	a,@dptr
      004851 FD               [12]11595 	mov	r5,a
      004852 A3               [24]11596 	inc	dptr
      004853 E0               [24]11597 	movx	a,@dptr
      004854 FE               [12]11598 	mov	r6,a
      004855 A3               [24]11599 	inc	dptr
      004856 E0               [24]11600 	movx	a,@dptr
      004857 FF               [12]11601 	mov	r7,a
      004858 90 03 DB         [24]11602 	mov	dptr,#_memcpy_PARM_2
      00485B ED               [12]11603 	mov	a,r5
      00485C F0               [24]11604 	movx	@dptr,a
      00485D EE               [12]11605 	mov	a,r6
      00485E A3               [24]11606 	inc	dptr
      00485F F0               [24]11607 	movx	@dptr,a
      004860 EF               [12]11608 	mov	a,r7
      004861 A3               [24]11609 	inc	dptr
      004862 F0               [24]11610 	movx	@dptr,a
      004863 90 03 DE         [24]11611 	mov	dptr,#_memcpy_PARM_3
      004866 74 04            [12]11612 	mov	a,#0x04
      004868 F0               [24]11613 	movx	@dptr,a
      004869 E4               [12]11614 	clr	a
      00486A A3               [24]11615 	inc	dptr
      00486B F0               [24]11616 	movx	@dptr,a
      00486C 90 00 C8         [24]11617 	mov	dptr,#_axradio_default_remoteaddr
      00486F 75 F0 00         [24]11618 	mov	b,#0x00
      004872 02 6C 4F         [24]11619 	ljmp	_memcpy
                                  11620 ;------------------------------------------------------------
                                  11621 ;Allocation info for local variables in function 'axradio_get_default_remote_address'
                                  11622 ;------------------------------------------------------------
                                  11623 ;addr                      Allocated with name '_axradio_get_default_remote_address_addr_1_453'
                                  11624 ;------------------------------------------------------------
                                  11625 ;	COMMON\easyax5043.c:2168: void axradio_get_default_remote_address(struct axradio_address __generic *addr)
                                  11626 ;	-----------------------------------------
                                  11627 ;	 function axradio_get_default_remote_address
                                  11628 ;	-----------------------------------------
      004875                      11629 _axradio_get_default_remote_address:
      004875 AF F0            [24]11630 	mov	r7,b
      004877 AE 83            [24]11631 	mov	r6,dph
      004879 E5 82            [12]11632 	mov	a,dpl
      00487B 90 03 67         [24]11633 	mov	dptr,#_axradio_get_default_remote_address_addr_1_453
      00487E F0               [24]11634 	movx	@dptr,a
      00487F EE               [12]11635 	mov	a,r6
      004880 A3               [24]11636 	inc	dptr
      004881 F0               [24]11637 	movx	@dptr,a
      004882 EF               [12]11638 	mov	a,r7
      004883 A3               [24]11639 	inc	dptr
      004884 F0               [24]11640 	movx	@dptr,a
                                  11641 ;	COMMON\easyax5043.c:2170: memcpy_genericxdata(addr, &axradio_default_remoteaddr, sizeof(axradio_default_remoteaddr));
      004885 90 03 67         [24]11642 	mov	dptr,#_axradio_get_default_remote_address_addr_1_453
      004888 E0               [24]11643 	movx	a,@dptr
      004889 FD               [12]11644 	mov	r5,a
      00488A A3               [24]11645 	inc	dptr
      00488B E0               [24]11646 	movx	a,@dptr
      00488C FE               [12]11647 	mov	r6,a
      00488D A3               [24]11648 	inc	dptr
      00488E E0               [24]11649 	movx	a,@dptr
      00488F FF               [12]11650 	mov	r7,a
      004890 90 03 DB         [24]11651 	mov	dptr,#_memcpy_PARM_2
      004893 74 C8            [12]11652 	mov	a,#_axradio_default_remoteaddr
      004895 F0               [24]11653 	movx	@dptr,a
      004896 74 00            [12]11654 	mov	a,#(_axradio_default_remoteaddr >> 8)
      004898 A3               [24]11655 	inc	dptr
      004899 F0               [24]11656 	movx	@dptr,a
      00489A E4               [12]11657 	clr	a
      00489B A3               [24]11658 	inc	dptr
      00489C F0               [24]11659 	movx	@dptr,a
      00489D 90 03 DE         [24]11660 	mov	dptr,#_memcpy_PARM_3
      0048A0 74 04            [12]11661 	mov	a,#0x04
      0048A2 F0               [24]11662 	movx	@dptr,a
      0048A3 E4               [12]11663 	clr	a
      0048A4 A3               [24]11664 	inc	dptr
      0048A5 F0               [24]11665 	movx	@dptr,a
      0048A6 8D 82            [24]11666 	mov	dpl,r5
      0048A8 8E 83            [24]11667 	mov	dph,r6
      0048AA 8F F0            [24]11668 	mov	b,r7
      0048AC 02 6C 4F         [24]11669 	ljmp	_memcpy
                                  11670 ;------------------------------------------------------------
                                  11671 ;Allocation info for local variables in function 'axradio_transmit'
                                  11672 ;------------------------------------------------------------
                                  11673 ;fifofree                  Allocated to registers r6 r7 
                                  11674 ;i                         Allocated to registers r4 
                                  11675 ;iesave                    Allocated to registers r7 
                                  11676 ;len_byte                  Allocated to registers r6 
                                  11677 ;pkt                       Allocated with name '_axradio_transmit_PARM_2'
                                  11678 ;pktlen                    Allocated with name '_axradio_transmit_PARM_3'
                                  11679 ;addr                      Allocated with name '_axradio_transmit_addr_1_455'
                                  11680 ;------------------------------------------------------------
                                  11681 ;	COMMON\easyax5043.c:2173: uint8_t axradio_transmit(const struct axradio_address __generic *addr, const uint8_t __generic *pkt, uint16_t pktlen)
                                  11682 ;	-----------------------------------------
                                  11683 ;	 function axradio_transmit
                                  11684 ;	-----------------------------------------
      0048AF                      11685 _axradio_transmit:
      0048AF AF F0            [24]11686 	mov	r7,b
      0048B1 AE 83            [24]11687 	mov	r6,dph
      0048B3 E5 82            [12]11688 	mov	a,dpl
      0048B5 90 03 6F         [24]11689 	mov	dptr,#_axradio_transmit_addr_1_455
      0048B8 F0               [24]11690 	movx	@dptr,a
      0048B9 EE               [12]11691 	mov	a,r6
      0048BA A3               [24]11692 	inc	dptr
      0048BB F0               [24]11693 	movx	@dptr,a
      0048BC EF               [12]11694 	mov	a,r7
      0048BD A3               [24]11695 	inc	dptr
      0048BE F0               [24]11696 	movx	@dptr,a
                                  11697 ;	COMMON\easyax5043.c:2175: switch (axradio_mode) {
      0048BF AF 0B            [24]11698 	mov	r7,_axradio_mode
      0048C1 BF 10 03         [24]11699 	cjne	r7,#0x10,00278$
      0048C4 02 49 B1         [24]11700 	ljmp	00125$
      0048C7                      11701 00278$:
      0048C7 BF 11 03         [24]11702 	cjne	r7,#0x11,00279$
      0048CA 02 49 B1         [24]11703 	ljmp	00125$
      0048CD                      11704 00279$:
      0048CD BF 12 03         [24]11705 	cjne	r7,#0x12,00280$
      0048D0 02 49 B1         [24]11706 	ljmp	00125$
      0048D3                      11707 00280$:
      0048D3 BF 13 03         [24]11708 	cjne	r7,#0x13,00281$
      0048D6 02 49 B1         [24]11709 	ljmp	00125$
      0048D9                      11710 00281$:
      0048D9 BF 18 02         [24]11711 	cjne	r7,#0x18,00282$
      0048DC 80 2F            [24]11712 	sjmp	00105$
      0048DE                      11713 00282$:
      0048DE BF 19 02         [24]11714 	cjne	r7,#0x19,00283$
      0048E1 80 2A            [24]11715 	sjmp	00105$
      0048E3                      11716 00283$:
      0048E3 BF 1A 02         [24]11717 	cjne	r7,#0x1a,00284$
      0048E6 80 25            [24]11718 	sjmp	00105$
      0048E8                      11719 00284$:
      0048E8 BF 1B 02         [24]11720 	cjne	r7,#0x1b,00285$
      0048EB 80 20            [24]11721 	sjmp	00105$
      0048ED                      11722 00285$:
      0048ED BF 1C 02         [24]11723 	cjne	r7,#0x1c,00286$
      0048F0 80 1B            [24]11724 	sjmp	00105$
      0048F2                      11725 00286$:
      0048F2 BF 20 03         [24]11726 	cjne	r7,#0x20,00287$
      0048F5 02 49 84         [24]11727 	ljmp	00116$
      0048F8                      11728 00287$:
      0048F8 BF 21 03         [24]11729 	cjne	r7,#0x21,00288$
      0048FB 02 49 84         [24]11730 	ljmp	00116$
      0048FE                      11731 00288$:
      0048FE BF 30 03         [24]11732 	cjne	r7,#0x30,00289$
      004901 02 49 BC         [24]11733 	ljmp	00128$
      004904                      11734 00289$:
      004904 BF 31 03         [24]11735 	cjne	r7,#0x31,00290$
      004907 02 49 BC         [24]11736 	ljmp	00128$
      00490A                      11737 00290$:
      00490A 02 4C 4F         [24]11738 	ljmp	00162$
                                  11739 ;	COMMON\easyax5043.c:2180: case AXRADIO_MODE_STREAM_TRANSMIT_SCRAM_LSB:
      00490D                      11740 00105$:
                                  11741 ;	COMMON\easyax5043.c:2182: uint16_t __autodata fifofree = radio_read16((uint16_t)&AX5043_FIFOFREE1);
      00490D 7E 2C            [12]11742 	mov	r6,#_AX5043_FIFOFREE1
      00490F 7F 40            [12]11743 	mov	r7,#(_AX5043_FIFOFREE1 >> 8)
      004911 8E 82            [24]11744 	mov	dpl,r6
      004913 8F 83            [24]11745 	mov	dph,r7
      004915 12 6F 1E         [24]11746 	lcall	_radio_read16
      004918 AE 82            [24]11747 	mov	r6,dpl
      00491A AF 83            [24]11748 	mov	r7,dph
                                  11749 ;	COMMON\easyax5043.c:2183: if (fifofree < pktlen + 3)
      00491C 90 03 6D         [24]11750 	mov	dptr,#_axradio_transmit_PARM_3
      00491F E0               [24]11751 	movx	a,@dptr
      004920 FC               [12]11752 	mov	r4,a
      004921 A3               [24]11753 	inc	dptr
      004922 E0               [24]11754 	movx	a,@dptr
      004923 FD               [12]11755 	mov	r5,a
      004924 74 03            [12]11756 	mov	a,#0x03
      004926 2C               [12]11757 	add	a,r4
      004927 FA               [12]11758 	mov	r2,a
      004928 E4               [12]11759 	clr	a
      004929 3D               [12]11760 	addc	a,r5
      00492A FB               [12]11761 	mov	r3,a
      00492B C3               [12]11762 	clr	c
      00492C EE               [12]11763 	mov	a,r6
      00492D 9A               [12]11764 	subb	a,r2
      00492E EF               [12]11765 	mov	a,r7
      00492F 9B               [12]11766 	subb	a,r3
      004930 50 04            [24]11767 	jnc	00107$
                                  11768 ;	COMMON\easyax5043.c:2184: return AXRADIO_ERR_INVALID;
      004932 75 82 04         [24]11769 	mov	dpl,#0x04
      004935 22               [24]11770 	ret
      004936                      11771 00107$:
                                  11772 ;	COMMON\easyax5043.c:2186: if (pktlen) {
      004936 EC               [12]11773 	mov	a,r4
      004937 4D               [12]11774 	orl	a,r5
      004938 60 2D            [24]11775 	jz	00112$
                                  11776 ;	COMMON\easyax5043.c:2187: uint8_t __autodata i = pktlen;
                                  11777 ;	COMMON\easyax5043.c:2188: AX5043_FIFODATA = AX5043_FIFOCMD_DATA | (7 << 5);
      00493A 90 40 29         [24]11778 	mov	dptr,#_AX5043_FIFODATA
      00493D 74 E1            [12]11779 	mov	a,#0xe1
      00493F F0               [24]11780 	movx	@dptr,a
                                  11781 ;	COMMON\easyax5043.c:2189: AX5043_FIFODATA = i + 1;
      004940 EC               [12]11782 	mov	a,r4
      004941 04               [12]11783 	inc	a
      004942 F0               [24]11784 	movx	@dptr,a
                                  11785 ;	COMMON\easyax5043.c:2190: AX5043_FIFODATA = 0x08;
      004943 74 08            [12]11786 	mov	a,#0x08
      004945 F0               [24]11787 	movx	@dptr,a
                                  11788 ;	COMMON\easyax5043.c:2191: do {
      004946 90 03 6A         [24]11789 	mov	dptr,#_axradio_transmit_PARM_2
      004949 E0               [24]11790 	movx	a,@dptr
      00494A FD               [12]11791 	mov	r5,a
      00494B A3               [24]11792 	inc	dptr
      00494C E0               [24]11793 	movx	a,@dptr
      00494D FE               [12]11794 	mov	r6,a
      00494E A3               [24]11795 	inc	dptr
      00494F E0               [24]11796 	movx	a,@dptr
      004950 FF               [12]11797 	mov	r7,a
      004951                      11798 00108$:
                                  11799 ;	COMMON\easyax5043.c:2192: AX5043_FIFODATA = *pkt++;
      004951 8D 82            [24]11800 	mov	dpl,r5
      004953 8E 83            [24]11801 	mov	dph,r6
      004955 8F F0            [24]11802 	mov	b,r7
      004957 12 7D 6F         [24]11803 	lcall	__gptrget
      00495A FB               [12]11804 	mov	r3,a
      00495B A3               [24]11805 	inc	dptr
      00495C AD 82            [24]11806 	mov	r5,dpl
      00495E AE 83            [24]11807 	mov	r6,dph
      004960 90 40 29         [24]11808 	mov	dptr,#_AX5043_FIFODATA
      004963 EB               [12]11809 	mov	a,r3
      004964 F0               [24]11810 	movx	@dptr,a
                                  11811 ;	COMMON\easyax5043.c:2193: } while (--i);
      004965 DC EA            [24]11812 	djnz	r4,00108$
      004967                      11813 00112$:
                                  11814 ;	COMMON\easyax5043.c:2195: AX5043_FIFOSTAT =  4; // FIFO commit
      004967 90 40 28         [24]11815 	mov	dptr,#_AX5043_FIFOSTAT
      00496A 74 04            [12]11816 	mov	a,#0x04
      00496C F0               [24]11817 	movx	@dptr,a
                                  11818 ;	COMMON\easyax5043.c:2197: uint8_t __autodata iesave = IE & 0x80;
      00496D 74 80            [12]11819 	mov	a,#0x80
      00496F 55 A8            [12]11820 	anl	a,_IE
      004971 FF               [12]11821 	mov	r7,a
                                  11822 ;	COMMON\easyax5043.c:2198: EA = 0;
      004972 C2 AF            [12]11823 	clr	_EA
                                  11824 ;	COMMON\easyax5043.c:2199: AX5043_IRQMASK0 |= 0x08;
      004974 90 40 07         [24]11825 	mov	dptr,#_AX5043_IRQMASK0
      004977 E0               [24]11826 	movx	a,@dptr
      004978 FE               [12]11827 	mov	r6,a
      004979 74 08            [12]11828 	mov	a,#0x08
      00497B 4E               [12]11829 	orl	a,r6
      00497C F0               [24]11830 	movx	@dptr,a
                                  11831 ;	COMMON\easyax5043.c:2200: IE |= iesave;
      00497D EF               [12]11832 	mov	a,r7
      00497E 42 A8            [12]11833 	orl	_IE,a
                                  11834 ;	COMMON\easyax5043.c:2202: return AXRADIO_ERR_NOERROR;
      004980 75 82 00         [24]11835 	mov	dpl,#0x00
      004983 22               [24]11836 	ret
                                  11837 ;	COMMON\easyax5043.c:2209: case AXRADIO_MODE_WOR_RECEIVE:
      004984                      11838 00116$:
                                  11839 ;	COMMON\easyax5043.c:2210: if (axradio_syncstate != syncstate_off)
      004984 90 00 A6         [24]11840 	mov	dptr,#_axradio_syncstate
      004987 E0               [24]11841 	movx	a,@dptr
      004988 E0               [24]11842 	movx	a,@dptr
      004989 60 04            [24]11843 	jz	00118$
                                  11844 ;	COMMON\easyax5043.c:2211: return AXRADIO_ERR_BUSY;
      00498B 75 82 02         [24]11845 	mov	dpl,#0x02
      00498E 22               [24]11846 	ret
      00498F                      11847 00118$:
                                  11848 ;	COMMON\easyax5043.c:2212: AX5043_IRQMASK1 = 0x00;
      00498F 90 40 06         [24]11849 	mov	dptr,#_AX5043_IRQMASK1
      004992 E4               [12]11850 	clr	a
      004993 F0               [24]11851 	movx	@dptr,a
                                  11852 ;	COMMON\easyax5043.c:2213: AX5043_IRQMASK0 = 0x00;
      004994 90 40 07         [24]11853 	mov	dptr,#_AX5043_IRQMASK0
      004997 F0               [24]11854 	movx	@dptr,a
                                  11855 ;	COMMON\easyax5043.c:2214: AX5043_PWRMODE = AX5043_PWRSTATE_XTAL_ON;
      004998 90 40 02         [24]11856 	mov	dptr,#_AX5043_PWRMODE
      00499B 74 05            [12]11857 	mov	a,#0x05
      00499D F0               [24]11858 	movx	@dptr,a
                                  11859 ;	COMMON\easyax5043.c:2215: AX5043_FIFOSTAT = 3;
      00499E 90 40 28         [24]11860 	mov	dptr,#_AX5043_FIFOSTAT
      0049A1 74 03            [12]11861 	mov	a,#0x03
      0049A3 F0               [24]11862 	movx	@dptr,a
                                  11863 ;	COMMON\easyax5043.c:2216: while (AX5043_POWSTAT & 0x08);
      0049A4                      11864 00119$:
      0049A4 90 40 03         [24]11865 	mov	dptr,#_AX5043_POWSTAT
      0049A7 E0               [24]11866 	movx	a,@dptr
      0049A8 FF               [12]11867 	mov	r7,a
      0049A9 20 E3 F8         [24]11868 	jb	acc.3,00119$
                                  11869 ;	COMMON\easyax5043.c:2217: ax5043_init_registers_tx();
      0049AC 12 1C 20         [24]11870 	lcall	_ax5043_init_registers_tx
                                  11871 ;	COMMON\easyax5043.c:2218: goto dotx;
                                  11872 ;	COMMON\easyax5043.c:2223: case AXRADIO_MODE_WOR_ACK_TRANSMIT:
      0049AF 80 0B            [24]11873 	sjmp	00128$
      0049B1                      11874 00125$:
                                  11875 ;	COMMON\easyax5043.c:2224: if (axradio_syncstate != syncstate_off)
      0049B1 90 00 A6         [24]11876 	mov	dptr,#_axradio_syncstate
      0049B4 E0               [24]11877 	movx	a,@dptr
      0049B5 E0               [24]11878 	movx	a,@dptr
      0049B6 60 04            [24]11879 	jz	00128$
                                  11880 ;	COMMON\easyax5043.c:2225: return AXRADIO_ERR_BUSY;
      0049B8 75 82 02         [24]11881 	mov	dpl,#0x02
      0049BB 22               [24]11882 	ret
                                  11883 ;	COMMON\easyax5043.c:2226: dotx:
      0049BC                      11884 00128$:
                                  11885 ;	COMMON\easyax5043.c:2227: axradio_ack_count = axradio_framing_ack_retransmissions;
      0049BC 90 80 02         [24]11886 	mov	dptr,#_axradio_framing_ack_retransmissions
      0049BF E4               [12]11887 	clr	a
      0049C0 93               [24]11888 	movc	a,@a+dptr
      0049C1 90 00 B0         [24]11889 	mov	dptr,#_axradio_ack_count
      0049C4 F0               [24]11890 	movx	@dptr,a
                                  11891 ;	COMMON\easyax5043.c:2228: ++axradio_ack_seqnr;
      0049C5 90 00 B1         [24]11892 	mov	dptr,#_axradio_ack_seqnr
      0049C8 E0               [24]11893 	movx	a,@dptr
      0049C9 24 01            [12]11894 	add	a,#0x01
      0049CB F0               [24]11895 	movx	@dptr,a
                                  11896 ;	COMMON\easyax5043.c:2229: axradio_txbuffer_len = pktlen + axradio_framing_maclen;
      0049CC 90 7F EB         [24]11897 	mov	dptr,#_axradio_framing_maclen
      0049CF E4               [12]11898 	clr	a
      0049D0 93               [24]11899 	movc	a,@a+dptr
      0049D1 FF               [12]11900 	mov	r7,a
      0049D2 FD               [12]11901 	mov	r5,a
      0049D3 7E 00            [12]11902 	mov	r6,#0x00
      0049D5 90 03 6D         [24]11903 	mov	dptr,#_axradio_transmit_PARM_3
      0049D8 E0               [24]11904 	movx	a,@dptr
      0049D9 FB               [12]11905 	mov	r3,a
      0049DA A3               [24]11906 	inc	dptr
      0049DB E0               [24]11907 	movx	a,@dptr
      0049DC FC               [12]11908 	mov	r4,a
      0049DD ED               [12]11909 	mov	a,r5
      0049DE 2B               [12]11910 	add	a,r3
      0049DF FD               [12]11911 	mov	r5,a
      0049E0 EE               [12]11912 	mov	a,r6
      0049E1 3C               [12]11913 	addc	a,r4
      0049E2 FE               [12]11914 	mov	r6,a
      0049E3 90 00 A7         [24]11915 	mov	dptr,#_axradio_txbuffer_len
      0049E6 ED               [12]11916 	mov	a,r5
      0049E7 F0               [24]11917 	movx	@dptr,a
      0049E8 EE               [12]11918 	mov	a,r6
      0049E9 A3               [24]11919 	inc	dptr
      0049EA F0               [24]11920 	movx	@dptr,a
                                  11921 ;	COMMON\easyax5043.c:2230: if (axradio_txbuffer_len > sizeof(axradio_txbuffer))
      0049EB C3               [12]11922 	clr	c
      0049EC 74 04            [12]11923 	mov	a,#0x04
      0049EE 9D               [12]11924 	subb	a,r5
      0049EF 74 01            [12]11925 	mov	a,#0x01
      0049F1 9E               [12]11926 	subb	a,r6
      0049F2 50 04            [24]11927 	jnc	00130$
                                  11928 ;	COMMON\easyax5043.c:2231: return AXRADIO_ERR_INVALID;
      0049F4 75 82 04         [24]11929 	mov	dpl,#0x04
      0049F7 22               [24]11930 	ret
      0049F8                      11931 00130$:
                                  11932 ;	COMMON\easyax5043.c:2232: memset_xdata(axradio_txbuffer, 0, axradio_framing_maclen);
      0049F8 7E 00            [12]11933 	mov	r6,#0x00
      0049FA 90 03 C6         [24]11934 	mov	dptr,#_memset_PARM_2
      0049FD E4               [12]11935 	clr	a
      0049FE F0               [24]11936 	movx	@dptr,a
      0049FF 90 03 C7         [24]11937 	mov	dptr,#_memset_PARM_3
      004A02 EF               [12]11938 	mov	a,r7
      004A03 F0               [24]11939 	movx	@dptr,a
      004A04 EE               [12]11940 	mov	a,r6
      004A05 A3               [24]11941 	inc	dptr
      004A06 F0               [24]11942 	movx	@dptr,a
      004A07 90 00 CC         [24]11943 	mov	dptr,#_axradio_txbuffer
      004A0A 75 F0 00         [24]11944 	mov	b,#0x00
      004A0D C0 04            [24]11945 	push	ar4
      004A0F C0 03            [24]11946 	push	ar3
      004A11 12 69 59         [24]11947 	lcall	_memset
      004A14 D0 03            [24]11948 	pop	ar3
      004A16 D0 04            [24]11949 	pop	ar4
                                  11950 ;	COMMON\easyax5043.c:2233: memcpy_xdatageneric(&axradio_txbuffer[axradio_framing_maclen], pkt, pktlen);
      004A18 90 7F EB         [24]11951 	mov	dptr,#_axradio_framing_maclen
      004A1B E4               [12]11952 	clr	a
      004A1C 93               [24]11953 	movc	a,@a+dptr
      004A1D 24 CC            [12]11954 	add	a,#_axradio_txbuffer
      004A1F FF               [12]11955 	mov	r7,a
      004A20 E4               [12]11956 	clr	a
      004A21 34 00            [12]11957 	addc	a,#(_axradio_txbuffer >> 8)
      004A23 FE               [12]11958 	mov	r6,a
      004A24 7D 00            [12]11959 	mov	r5,#0x00
      004A26 90 03 6A         [24]11960 	mov	dptr,#_axradio_transmit_PARM_2
      004A29 E0               [24]11961 	movx	a,@dptr
      004A2A F8               [12]11962 	mov	r0,a
      004A2B A3               [24]11963 	inc	dptr
      004A2C E0               [24]11964 	movx	a,@dptr
      004A2D F9               [12]11965 	mov	r1,a
      004A2E A3               [24]11966 	inc	dptr
      004A2F E0               [24]11967 	movx	a,@dptr
      004A30 FA               [12]11968 	mov	r2,a
      004A31 90 03 DB         [24]11969 	mov	dptr,#_memcpy_PARM_2
      004A34 E8               [12]11970 	mov	a,r0
      004A35 F0               [24]11971 	movx	@dptr,a
      004A36 E9               [12]11972 	mov	a,r1
      004A37 A3               [24]11973 	inc	dptr
      004A38 F0               [24]11974 	movx	@dptr,a
      004A39 EA               [12]11975 	mov	a,r2
      004A3A A3               [24]11976 	inc	dptr
      004A3B F0               [24]11977 	movx	@dptr,a
      004A3C 90 03 DE         [24]11978 	mov	dptr,#_memcpy_PARM_3
      004A3F EB               [12]11979 	mov	a,r3
      004A40 F0               [24]11980 	movx	@dptr,a
      004A41 EC               [12]11981 	mov	a,r4
      004A42 A3               [24]11982 	inc	dptr
      004A43 F0               [24]11983 	movx	@dptr,a
      004A44 8F 82            [24]11984 	mov	dpl,r7
      004A46 8E 83            [24]11985 	mov	dph,r6
      004A48 8D F0            [24]11986 	mov	b,r5
      004A4A 12 6C 4F         [24]11987 	lcall	_memcpy
                                  11988 ;	COMMON\easyax5043.c:2234: if (axradio_framing_ack_seqnrpos != 0xff)
      004A4D 90 80 03         [24]11989 	mov	dptr,#_axradio_framing_ack_seqnrpos
      004A50 E4               [12]11990 	clr	a
      004A51 93               [24]11991 	movc	a,@a+dptr
      004A52 FF               [12]11992 	mov	r7,a
      004A53 BF FF 02         [24]11993 	cjne	r7,#0xff,00299$
      004A56 80 12            [24]11994 	sjmp	00132$
      004A58                      11995 00299$:
                                  11996 ;	COMMON\easyax5043.c:2235: axradio_txbuffer[axradio_framing_ack_seqnrpos] = axradio_ack_seqnr;
      004A58 EF               [12]11997 	mov	a,r7
      004A59 24 CC            [12]11998 	add	a,#_axradio_txbuffer
      004A5B FF               [12]11999 	mov	r7,a
      004A5C E4               [12]12000 	clr	a
      004A5D 34 00            [12]12001 	addc	a,#(_axradio_txbuffer >> 8)
      004A5F FE               [12]12002 	mov	r6,a
      004A60 90 00 B1         [24]12003 	mov	dptr,#_axradio_ack_seqnr
      004A63 E0               [24]12004 	movx	a,@dptr
      004A64 FD               [12]12005 	mov	r5,a
      004A65 8F 82            [24]12006 	mov	dpl,r7
      004A67 8E 83            [24]12007 	mov	dph,r6
      004A69 F0               [24]12008 	movx	@dptr,a
      004A6A                      12009 00132$:
                                  12010 ;	COMMON\easyax5043.c:2236: if (axradio_framing_destaddrpos != 0xff)
      004A6A 90 7F ED         [24]12011 	mov	dptr,#_axradio_framing_destaddrpos
      004A6D E4               [12]12012 	clr	a
      004A6E 93               [24]12013 	movc	a,@a+dptr
      004A6F FF               [12]12014 	mov	r7,a
      004A70 BF FF 02         [24]12015 	cjne	r7,#0xff,00300$
      004A73 80 39            [24]12016 	sjmp	00134$
      004A75                      12017 00300$:
                                  12018 ;	COMMON\easyax5043.c:2237: memcpy_xdatageneric(&axradio_txbuffer[axradio_framing_destaddrpos], &addr->addr, axradio_framing_addrlen);
      004A75 EF               [12]12019 	mov	a,r7
      004A76 24 CC            [12]12020 	add	a,#_axradio_txbuffer
      004A78 FF               [12]12021 	mov	r7,a
      004A79 E4               [12]12022 	clr	a
      004A7A 34 00            [12]12023 	addc	a,#(_axradio_txbuffer >> 8)
      004A7C FE               [12]12024 	mov	r6,a
      004A7D 7D 00            [12]12025 	mov	r5,#0x00
      004A7F 90 03 6F         [24]12026 	mov	dptr,#_axradio_transmit_addr_1_455
      004A82 E0               [24]12027 	movx	a,@dptr
      004A83 FA               [12]12028 	mov	r2,a
      004A84 A3               [24]12029 	inc	dptr
      004A85 E0               [24]12030 	movx	a,@dptr
      004A86 FB               [12]12031 	mov	r3,a
      004A87 A3               [24]12032 	inc	dptr
      004A88 E0               [24]12033 	movx	a,@dptr
      004A89 FC               [12]12034 	mov	r4,a
      004A8A 90 7F EC         [24]12035 	mov	dptr,#_axradio_framing_addrlen
      004A8D E4               [12]12036 	clr	a
      004A8E 93               [24]12037 	movc	a,@a+dptr
      004A8F F8               [12]12038 	mov	r0,a
      004A90 79 00            [12]12039 	mov	r1,#0x00
      004A92 90 03 DB         [24]12040 	mov	dptr,#_memcpy_PARM_2
      004A95 EA               [12]12041 	mov	a,r2
      004A96 F0               [24]12042 	movx	@dptr,a
      004A97 EB               [12]12043 	mov	a,r3
      004A98 A3               [24]12044 	inc	dptr
      004A99 F0               [24]12045 	movx	@dptr,a
      004A9A EC               [12]12046 	mov	a,r4
      004A9B A3               [24]12047 	inc	dptr
      004A9C F0               [24]12048 	movx	@dptr,a
      004A9D 90 03 DE         [24]12049 	mov	dptr,#_memcpy_PARM_3
      004AA0 E8               [12]12050 	mov	a,r0
      004AA1 F0               [24]12051 	movx	@dptr,a
      004AA2 E9               [12]12052 	mov	a,r1
      004AA3 A3               [24]12053 	inc	dptr
      004AA4 F0               [24]12054 	movx	@dptr,a
      004AA5 8F 82            [24]12055 	mov	dpl,r7
      004AA7 8E 83            [24]12056 	mov	dph,r6
      004AA9 8D F0            [24]12057 	mov	b,r5
      004AAB 12 6C 4F         [24]12058 	lcall	_memcpy
      004AAE                      12059 00134$:
                                  12060 ;	COMMON\easyax5043.c:2238: if (axradio_framing_sourceaddrpos != 0xff)
      004AAE 90 7F EE         [24]12061 	mov	dptr,#_axradio_framing_sourceaddrpos
      004AB1 E4               [12]12062 	clr	a
      004AB2 93               [24]12063 	movc	a,@a+dptr
      004AB3 FF               [12]12064 	mov	r7,a
      004AB4 BF FF 02         [24]12065 	cjne	r7,#0xff,00301$
      004AB7 80 30            [24]12066 	sjmp	00136$
      004AB9                      12067 00301$:
                                  12068 ;	COMMON\easyax5043.c:2239: memcpy_xdata(&axradio_txbuffer[axradio_framing_sourceaddrpos], &axradio_localaddr.addr, axradio_framing_addrlen);
      004AB9 EF               [12]12069 	mov	a,r7
      004ABA 24 CC            [12]12070 	add	a,#_axradio_txbuffer
      004ABC FF               [12]12071 	mov	r7,a
      004ABD E4               [12]12072 	clr	a
      004ABE 34 00            [12]12073 	addc	a,#(_axradio_txbuffer >> 8)
      004AC0 FE               [12]12074 	mov	r6,a
      004AC1 7D 00            [12]12075 	mov	r5,#0x00
      004AC3 90 7F EC         [24]12076 	mov	dptr,#_axradio_framing_addrlen
      004AC6 E4               [12]12077 	clr	a
      004AC7 93               [24]12078 	movc	a,@a+dptr
      004AC8 FC               [12]12079 	mov	r4,a
      004AC9 7B 00            [12]12080 	mov	r3,#0x00
      004ACB 90 03 DB         [24]12081 	mov	dptr,#_memcpy_PARM_2
      004ACE 74 C0            [12]12082 	mov	a,#_axradio_localaddr
      004AD0 F0               [24]12083 	movx	@dptr,a
      004AD1 74 00            [12]12084 	mov	a,#(_axradio_localaddr >> 8)
      004AD3 A3               [24]12085 	inc	dptr
      004AD4 F0               [24]12086 	movx	@dptr,a
      004AD5 E4               [12]12087 	clr	a
      004AD6 A3               [24]12088 	inc	dptr
      004AD7 F0               [24]12089 	movx	@dptr,a
      004AD8 90 03 DE         [24]12090 	mov	dptr,#_memcpy_PARM_3
      004ADB EC               [12]12091 	mov	a,r4
      004ADC F0               [24]12092 	movx	@dptr,a
      004ADD EB               [12]12093 	mov	a,r3
      004ADE A3               [24]12094 	inc	dptr
      004ADF F0               [24]12095 	movx	@dptr,a
      004AE0 8F 82            [24]12096 	mov	dpl,r7
      004AE2 8E 83            [24]12097 	mov	dph,r6
      004AE4 8D F0            [24]12098 	mov	b,r5
      004AE6 12 6C 4F         [24]12099 	lcall	_memcpy
      004AE9                      12100 00136$:
                                  12101 ;	COMMON\easyax5043.c:2240: if (axradio_framing_lenmask) {
      004AE9 90 7F F1         [24]12102 	mov	dptr,#_axradio_framing_lenmask
      004AEC E4               [12]12103 	clr	a
      004AED 93               [24]12104 	movc	a,@a+dptr
      004AEE FF               [12]12105 	mov	r7,a
      004AEF 60 30            [24]12106 	jz	00138$
                                  12107 ;	COMMON\easyax5043.c:2241: uint8_t __autodata len_byte = (uint8_t)(axradio_txbuffer_len - axradio_framing_lenoffs) & axradio_framing_lenmask; // if you prefer not counting the len byte itself, set LENOFFS = 1
      004AF1 90 00 A7         [24]12108 	mov	dptr,#_axradio_txbuffer_len
      004AF4 E0               [24]12109 	movx	a,@dptr
      004AF5 FD               [12]12110 	mov	r5,a
      004AF6 A3               [24]12111 	inc	dptr
      004AF7 E0               [24]12112 	movx	a,@dptr
      004AF8 90 7F F0         [24]12113 	mov	dptr,#_axradio_framing_lenoffs
      004AFB E4               [12]12114 	clr	a
      004AFC 93               [24]12115 	movc	a,@a+dptr
      004AFD FE               [12]12116 	mov	r6,a
      004AFE ED               [12]12117 	mov	a,r5
      004AFF C3               [12]12118 	clr	c
      004B00 9E               [12]12119 	subb	a,r6
      004B01 5F               [12]12120 	anl	a,r7
      004B02 FE               [12]12121 	mov	r6,a
                                  12122 ;	COMMON\easyax5043.c:2242: axradio_txbuffer[axradio_framing_lenpos] = (axradio_txbuffer[axradio_framing_lenpos] & (uint8_t)~axradio_framing_lenmask) | len_byte;
      004B03 90 7F EF         [24]12123 	mov	dptr,#_axradio_framing_lenpos
      004B06 E4               [12]12124 	clr	a
      004B07 93               [24]12125 	movc	a,@a+dptr
      004B08 24 CC            [12]12126 	add	a,#_axradio_txbuffer
      004B0A FD               [12]12127 	mov	r5,a
      004B0B E4               [12]12128 	clr	a
      004B0C 34 00            [12]12129 	addc	a,#(_axradio_txbuffer >> 8)
      004B0E FC               [12]12130 	mov	r4,a
      004B0F 8D 82            [24]12131 	mov	dpl,r5
      004B11 8C 83            [24]12132 	mov	dph,r4
      004B13 E0               [24]12133 	movx	a,@dptr
      004B14 FB               [12]12134 	mov	r3,a
      004B15 EF               [12]12135 	mov	a,r7
      004B16 F4               [12]12136 	cpl	a
      004B17 FF               [12]12137 	mov	r7,a
      004B18 5B               [12]12138 	anl	a,r3
      004B19 42 06            [12]12139 	orl	ar6,a
      004B1B 8D 82            [24]12140 	mov	dpl,r5
      004B1D 8C 83            [24]12141 	mov	dph,r4
      004B1F EE               [12]12142 	mov	a,r6
      004B20 F0               [24]12143 	movx	@dptr,a
      004B21                      12144 00138$:
                                  12145 ;	COMMON\easyax5043.c:2244: if (axradio_framing_swcrclen)
      004B21 90 7F F2         [24]12146 	mov	dptr,#_axradio_framing_swcrclen
      004B24 E4               [12]12147 	clr	a
      004B25 93               [24]12148 	movc	a,@a+dptr
      004B26 60 20            [24]12149 	jz	00140$
                                  12150 ;	COMMON\easyax5043.c:2245: axradio_txbuffer_len = axradio_framing_append_crc(axradio_txbuffer, axradio_txbuffer_len);
      004B28 90 00 A7         [24]12151 	mov	dptr,#_axradio_txbuffer_len
      004B2B E0               [24]12152 	movx	a,@dptr
      004B2C C0 E0            [24]12153 	push	acc
      004B2E A3               [24]12154 	inc	dptr
      004B2F E0               [24]12155 	movx	a,@dptr
      004B30 C0 E0            [24]12156 	push	acc
      004B32 90 00 CC         [24]12157 	mov	dptr,#_axradio_txbuffer
      004B35 12 19 7C         [24]12158 	lcall	_axradio_framing_append_crc
      004B38 AE 82            [24]12159 	mov	r6,dpl
      004B3A AF 83            [24]12160 	mov	r7,dph
      004B3C 15 81            [12]12161 	dec	sp
      004B3E 15 81            [12]12162 	dec	sp
      004B40 90 00 A7         [24]12163 	mov	dptr,#_axradio_txbuffer_len
      004B43 EE               [12]12164 	mov	a,r6
      004B44 F0               [24]12165 	movx	@dptr,a
      004B45 EF               [12]12166 	mov	a,r7
      004B46 A3               [24]12167 	inc	dptr
      004B47 F0               [24]12168 	movx	@dptr,a
      004B48                      12169 00140$:
                                  12170 ;	COMMON\easyax5043.c:2246: if (axradio_phy_pn9)
      004B48 90 7F CA         [24]12171 	mov	dptr,#_axradio_phy_pn9
      004B4B E4               [12]12172 	clr	a
      004B4C 93               [24]12173 	movc	a,@a+dptr
      004B4D 60 2F            [24]12174 	jz	00142$
                                  12175 ;	COMMON\easyax5043.c:2247: pn9_buffer(axradio_txbuffer, axradio_txbuffer_len, 0x1ff, -(AX5043_ENCODING & 0x01));
      004B4F 90 40 11         [24]12176 	mov	dptr,#_AX5043_ENCODING
      004B52 E0               [24]12177 	movx	a,@dptr
      004B53 FF               [12]12178 	mov	r7,a
      004B54 53 07 01         [24]12179 	anl	ar7,#0x01
      004B57 C3               [12]12180 	clr	c
      004B58 E4               [12]12181 	clr	a
      004B59 9F               [12]12182 	subb	a,r7
      004B5A FF               [12]12183 	mov	r7,a
      004B5B C0 07            [24]12184 	push	ar7
      004B5D 74 FF            [12]12185 	mov	a,#0xff
      004B5F C0 E0            [24]12186 	push	acc
      004B61 74 01            [12]12187 	mov	a,#0x01
      004B63 C0 E0            [24]12188 	push	acc
      004B65 90 00 A7         [24]12189 	mov	dptr,#_axradio_txbuffer_len
      004B68 E0               [24]12190 	movx	a,@dptr
      004B69 C0 E0            [24]12191 	push	acc
      004B6B A3               [24]12192 	inc	dptr
      004B6C E0               [24]12193 	movx	a,@dptr
      004B6D C0 E0            [24]12194 	push	acc
      004B6F 90 00 CC         [24]12195 	mov	dptr,#_axradio_txbuffer
      004B72 75 F0 00         [24]12196 	mov	b,#0x00
      004B75 12 6E 0C         [24]12197 	lcall	_pn9_buffer
      004B78 E5 81            [12]12198 	mov	a,sp
      004B7A 24 FB            [12]12199 	add	a,#0xfb
      004B7C F5 81            [12]12200 	mov	sp,a
      004B7E                      12201 00142$:
                                  12202 ;	COMMON\easyax5043.c:2248: if (axradio_mode == AXRADIO_MODE_SYNC_MASTER ||
      004B7E 74 30            [12]12203 	mov	a,#0x30
      004B80 B5 0B 02         [24]12204 	cjne	a,_axradio_mode,00305$
      004B83 80 05            [24]12205 	sjmp	00143$
      004B85                      12206 00305$:
                                  12207 ;	COMMON\easyax5043.c:2249: axradio_mode == AXRADIO_MODE_SYNC_ACK_MASTER)
      004B85 74 31            [12]12208 	mov	a,#0x31
      004B87 B5 0B 04         [24]12209 	cjne	a,_axradio_mode,00144$
      004B8A                      12210 00143$:
                                  12211 ;	COMMON\easyax5043.c:2250: return AXRADIO_ERR_NOERROR;
      004B8A 75 82 00         [24]12212 	mov	dpl,#0x00
      004B8D 22               [24]12213 	ret
      004B8E                      12214 00144$:
                                  12215 ;	COMMON\easyax5043.c:2251: if (axradio_mode == AXRADIO_MODE_WOR_TRANSMIT ||
      004B8E 74 11            [12]12216 	mov	a,#0x11
      004B90 B5 0B 02         [24]12217 	cjne	a,_axradio_mode,00308$
      004B93 80 05            [24]12218 	sjmp	00146$
      004B95                      12219 00308$:
                                  12220 ;	COMMON\easyax5043.c:2252: axradio_mode == AXRADIO_MODE_WOR_ACK_TRANSMIT)
      004B95 74 13            [12]12221 	mov	a,#0x13
      004B97 B5 0B 14         [24]12222 	cjne	a,_axradio_mode,00147$
      004B9A                      12223 00146$:
                                  12224 ;	COMMON\easyax5043.c:2253: axradio_txbuffer_cnt = axradio_phy_preamble_wor_longlen;
      004B9A 90 7F DF         [24]12225 	mov	dptr,#_axradio_phy_preamble_wor_longlen
      004B9D E4               [12]12226 	clr	a
      004B9E 93               [24]12227 	movc	a,@a+dptr
      004B9F FE               [12]12228 	mov	r6,a
      004BA0 74 01            [12]12229 	mov	a,#0x01
      004BA2 93               [24]12230 	movc	a,@a+dptr
      004BA3 FF               [12]12231 	mov	r7,a
      004BA4 90 00 A9         [24]12232 	mov	dptr,#_axradio_txbuffer_cnt
      004BA7 EE               [12]12233 	mov	a,r6
      004BA8 F0               [24]12234 	movx	@dptr,a
      004BA9 EF               [12]12235 	mov	a,r7
      004BAA A3               [24]12236 	inc	dptr
      004BAB F0               [24]12237 	movx	@dptr,a
      004BAC 80 12            [24]12238 	sjmp	00148$
      004BAE                      12239 00147$:
                                  12240 ;	COMMON\easyax5043.c:2255: axradio_txbuffer_cnt = axradio_phy_preamble_longlen;
      004BAE 90 7F E3         [24]12241 	mov	dptr,#_axradio_phy_preamble_longlen
      004BB1 E4               [12]12242 	clr	a
      004BB2 93               [24]12243 	movc	a,@a+dptr
      004BB3 FE               [12]12244 	mov	r6,a
      004BB4 74 01            [12]12245 	mov	a,#0x01
      004BB6 93               [24]12246 	movc	a,@a+dptr
      004BB7 FF               [12]12247 	mov	r7,a
      004BB8 90 00 A9         [24]12248 	mov	dptr,#_axradio_txbuffer_cnt
      004BBB EE               [12]12249 	mov	a,r6
      004BBC F0               [24]12250 	movx	@dptr,a
      004BBD EF               [12]12251 	mov	a,r7
      004BBE A3               [24]12252 	inc	dptr
      004BBF F0               [24]12253 	movx	@dptr,a
      004BC0                      12254 00148$:
                                  12255 ;	COMMON\easyax5043.c:2256: if (axradio_phy_lbt_retries) {
      004BC0 90 7F DD         [24]12256 	mov	dptr,#_axradio_phy_lbt_retries
      004BC3 E4               [12]12257 	clr	a
      004BC4 93               [24]12258 	movc	a,@a+dptr
      004BC5 70 03            [24]12259 	jnz	00311$
      004BC7 02 4C 42         [24]12260 	ljmp	00161$
      004BCA                      12261 00311$:
                                  12262 ;	COMMON\easyax5043.c:2257: switch (axradio_mode) {
      004BCA AF 0B            [24]12263 	mov	r7,_axradio_mode
      004BCC BF 10 02         [24]12264 	cjne	r7,#0x10,00312$
      004BCF 80 21            [24]12265 	sjmp	00157$
      004BD1                      12266 00312$:
      004BD1 BF 11 02         [24]12267 	cjne	r7,#0x11,00313$
      004BD4 80 1C            [24]12268 	sjmp	00157$
      004BD6                      12269 00313$:
      004BD6 BF 12 02         [24]12270 	cjne	r7,#0x12,00314$
      004BD9 80 17            [24]12271 	sjmp	00157$
      004BDB                      12272 00314$:
      004BDB BF 13 02         [24]12273 	cjne	r7,#0x13,00315$
      004BDE 80 12            [24]12274 	sjmp	00157$
      004BE0                      12275 00315$:
      004BE0 BF 20 02         [24]12276 	cjne	r7,#0x20,00316$
      004BE3 80 0D            [24]12277 	sjmp	00157$
      004BE5                      12278 00316$:
      004BE5 BF 21 02         [24]12279 	cjne	r7,#0x21,00317$
      004BE8 80 08            [24]12280 	sjmp	00157$
      004BEA                      12281 00317$:
      004BEA BF 22 02         [24]12282 	cjne	r7,#0x22,00318$
      004BED 80 03            [24]12283 	sjmp	00157$
      004BEF                      12284 00318$:
      004BEF BF 23 50         [24]12285 	cjne	r7,#0x23,00161$
                                  12286 ;	COMMON\easyax5043.c:2265: case AXRADIO_MODE_ACK_RECEIVE:
      004BF2                      12287 00157$:
                                  12288 ;	COMMON\easyax5043.c:2266: ax5043_off_xtal();
      004BF2 12 28 C4         [24]12289 	lcall	_ax5043_off_xtal
                                  12290 ;	COMMON\easyax5043.c:2267: ax5043_init_registers_rx();
      004BF5 12 1C 26         [24]12291 	lcall	_ax5043_init_registers_rx
                                  12292 ;	COMMON\easyax5043.c:2268: AX5043_RSSIREFERENCE = axradio_phy_rssireference;
      004BF8 90 7F D8         [24]12293 	mov	dptr,#_axradio_phy_rssireference
      004BFB E4               [12]12294 	clr	a
      004BFC 93               [24]12295 	movc	a,@a+dptr
      004BFD 90 42 2C         [24]12296 	mov	dptr,#_AX5043_RSSIREFERENCE
      004C00 F0               [24]12297 	movx	@dptr,a
                                  12298 ;	COMMON\easyax5043.c:2269: AX5043_PWRMODE = AX5043_PWRSTATE_FULL_RX;
      004C01 90 40 02         [24]12299 	mov	dptr,#_AX5043_PWRMODE
      004C04 74 09            [12]12300 	mov	a,#0x09
      004C06 F0               [24]12301 	movx	@dptr,a
                                  12302 ;	COMMON\easyax5043.c:2270: axradio_ack_count = axradio_phy_lbt_retries;
      004C07 90 7F DD         [24]12303 	mov	dptr,#_axradio_phy_lbt_retries
      004C0A E4               [12]12304 	clr	a
      004C0B 93               [24]12305 	movc	a,@a+dptr
      004C0C 90 00 B0         [24]12306 	mov	dptr,#_axradio_ack_count
      004C0F F0               [24]12307 	movx	@dptr,a
                                  12308 ;	COMMON\easyax5043.c:2271: axradio_syncstate = syncstate_lbt;
      004C10 90 00 A6         [24]12309 	mov	dptr,#_axradio_syncstate
      004C13 74 01            [12]12310 	mov	a,#0x01
      004C15 F0               [24]12311 	movx	@dptr,a
                                  12312 ;	COMMON\easyax5043.c:2272: wtimer_remove(&axradio_timer);
      004C16 90 03 2B         [24]12313 	mov	dptr,#_axradio_timer
      004C19 12 76 94         [24]12314 	lcall	_wtimer_remove
                                  12315 ;	COMMON\easyax5043.c:2273: axradio_timer.time = axradio_phy_cs_period;
      004C1C 90 7F DA         [24]12316 	mov	dptr,#_axradio_phy_cs_period
      004C1F E4               [12]12317 	clr	a
      004C20 93               [24]12318 	movc	a,@a+dptr
      004C21 FE               [12]12319 	mov	r6,a
      004C22 74 01            [12]12320 	mov	a,#0x01
      004C24 93               [24]12321 	movc	a,@a+dptr
      004C25 FF               [12]12322 	mov	r7,a
      004C26 7D 00            [12]12323 	mov	r5,#0x00
      004C28 7C 00            [12]12324 	mov	r4,#0x00
      004C2A 90 03 2F         [24]12325 	mov	dptr,#(_axradio_timer + 0x0004)
      004C2D EE               [12]12326 	mov	a,r6
      004C2E F0               [24]12327 	movx	@dptr,a
      004C2F EF               [12]12328 	mov	a,r7
      004C30 A3               [24]12329 	inc	dptr
      004C31 F0               [24]12330 	movx	@dptr,a
      004C32 ED               [12]12331 	mov	a,r5
      004C33 A3               [24]12332 	inc	dptr
      004C34 F0               [24]12333 	movx	@dptr,a
      004C35 EC               [12]12334 	mov	a,r4
      004C36 A3               [24]12335 	inc	dptr
      004C37 F0               [24]12336 	movx	@dptr,a
                                  12337 ;	COMMON\easyax5043.c:2274: wtimer0_addrelative(&axradio_timer);
      004C38 90 03 2B         [24]12338 	mov	dptr,#_axradio_timer
      004C3B 12 6C DD         [24]12339 	lcall	_wtimer0_addrelative
                                  12340 ;	COMMON\easyax5043.c:2275: return AXRADIO_ERR_NOERROR;
      004C3E 75 82 00         [24]12341 	mov	dpl,#0x00
                                  12342 ;	COMMON\easyax5043.c:2279: }
      004C41 22               [24]12343 	ret
      004C42                      12344 00161$:
                                  12345 ;	COMMON\easyax5043.c:2281: axradio_syncstate = syncstate_asynctx;
      004C42 90 00 A6         [24]12346 	mov	dptr,#_axradio_syncstate
      004C45 74 02            [12]12347 	mov	a,#0x02
      004C47 F0               [24]12348 	movx	@dptr,a
                                  12349 ;	COMMON\easyax5043.c:2282: ax5043_prepare_tx();
      004C48 12 28 92         [24]12350 	lcall	_ax5043_prepare_tx
                                  12351 ;	COMMON\easyax5043.c:2283: return AXRADIO_ERR_NOERROR;
      004C4B 75 82 00         [24]12352 	mov	dpl,#0x00
                                  12353 ;	COMMON\easyax5043.c:2285: default:
      004C4E 22               [24]12354 	ret
      004C4F                      12355 00162$:
                                  12356 ;	COMMON\easyax5043.c:2286: return AXRADIO_ERR_NOTSUPPORTED;
      004C4F 75 82 01         [24]12357 	mov	dpl,#0x01
                                  12358 ;	COMMON\easyax5043.c:2287: }
      004C52 22               [24]12359 	ret
                                  12360 ;------------------------------------------------------------
                                  12361 ;Allocation info for local variables in function 'axradio_set_paramsets'
                                  12362 ;------------------------------------------------------------
                                  12363 ;val                       Allocated to registers r7 
                                  12364 ;------------------------------------------------------------
                                  12365 ;	COMMON\easyax5043.c:2290: static __reentrantb uint8_t axradio_set_paramsets(uint8_t val) __reentrant
                                  12366 ;	-----------------------------------------
                                  12367 ;	 function axradio_set_paramsets
                                  12368 ;	-----------------------------------------
      004C53                      12369 _axradio_set_paramsets:
      004C53 AF 82            [24]12370 	mov	r7,dpl
                                  12371 ;	COMMON\easyax5043.c:2292: if (!AXRADIO_MODE_IS_STREAM_RECEIVE(axradio_mode))
      004C55 74 F8            [12]12372 	mov	a,#0xf8
      004C57 55 0B            [12]12373 	anl	a,_axradio_mode
      004C59 FE               [12]12374 	mov	r6,a
      004C5A BE 28 02         [24]12375 	cjne	r6,#0x28,00108$
      004C5D 80 04            [24]12376 	sjmp	00102$
      004C5F                      12377 00108$:
                                  12378 ;	COMMON\easyax5043.c:2293: return AXRADIO_ERR_NOTSUPPORTED;
      004C5F 75 82 01         [24]12379 	mov	dpl,#0x01
      004C62 22               [24]12380 	ret
      004C63                      12381 00102$:
                                  12382 ;	COMMON\easyax5043.c:2294: AX5043_RXPARAMSETS = val;
      004C63 90 41 17         [24]12383 	mov	dptr,#_AX5043_RXPARAMSETS
      004C66 EF               [12]12384 	mov	a,r7
      004C67 F0               [24]12385 	movx	@dptr,a
                                  12386 ;	COMMON\easyax5043.c:2295: return AXRADIO_ERR_NOERROR;
      004C68 75 82 00         [24]12387 	mov	dpl,#0x00
      004C6B 22               [24]12388 	ret
                                  12389 ;------------------------------------------------------------
                                  12390 ;Allocation info for local variables in function 'axradio_agc_freeze'
                                  12391 ;------------------------------------------------------------
                                  12392 ;	COMMON\easyax5043.c:2298: uint8_t axradio_agc_freeze(void)
                                  12393 ;	-----------------------------------------
                                  12394 ;	 function axradio_agc_freeze
                                  12395 ;	-----------------------------------------
      004C6C                      12396 _axradio_agc_freeze:
                                  12397 ;	COMMON\easyax5043.c:2300: return axradio_set_paramsets(0xff);
      004C6C 75 82 FF         [24]12398 	mov	dpl,#0xff
      004C6F 02 4C 53         [24]12399 	ljmp	_axradio_set_paramsets
                                  12400 ;------------------------------------------------------------
                                  12401 ;Allocation info for local variables in function 'axradio_agc_thaw'
                                  12402 ;------------------------------------------------------------
                                  12403 ;	COMMON\easyax5043.c:2303: uint8_t axradio_agc_thaw(void)
                                  12404 ;	-----------------------------------------
                                  12405 ;	 function axradio_agc_thaw
                                  12406 ;	-----------------------------------------
      004C72                      12407 _axradio_agc_thaw:
                                  12408 ;	COMMON\easyax5043.c:2305: return axradio_set_paramsets(0x00);
      004C72 75 82 00         [24]12409 	mov	dpl,#0x00
      004C75 02 4C 53         [24]12410 	ljmp	_axradio_set_paramsets
                                  12411 	.area CSEG    (CODE)
                                  12412 	.area CONST   (CODE)
      0080EB                      12413 ___str_0:
      0080EB 20 72 65 63 65 69 76 12414 	.ascii " receive_isr "
             65 5F 69 73 72 20
      0080F8 00                   12415 	.db 0x00
      0080F9                      12416 ___str_1:
      0080F9 20 74 69 6D 65 20 61 12417 	.ascii " time anchor "
             6E 63 68 6F 72 20
      008106 00                   12418 	.db 0x00
      008107                      12419 ___str_2:
      008107 20 65 6E 61 62 6C 65 12420 	.ascii " enable_sfdcallback "
             5F 73 66 64 63 61 6C
             6C 62 61 63 6B 20
      00811B 00                   12421 	.db 0x00
      00811C                      12422 ___str_3:
      00811C 74 72 78 73 74 61 74 12423 	.ascii "trxstate_wait_xtal"
             65 5F 77 61 69 74 5F
             78 74 61 6C
      00812E 00                   12424 	.db 0x00
      00812F                      12425 ___str_4:
      00812F 74 72 78 73 74 61 74 12426 	.ascii "trxstate_pll_ranging"
             65 5F 70 6C 6C 5F 72
             61 6E 67 69 6E 67
      008143 00                   12427 	.db 0x00
      008144                      12428 ___str_5:
      008144 74 72 78 73 74 61 74 12429 	.ascii "trxstate_pll_settling"
             65 5F 70 6C 6C 5F 73
             65 74 74 6C 69 6E 67
      008159 00                   12430 	.db 0x00
      00815A                      12431 ___str_6:
      00815A 74 72 78 73 74 61 74 12432 	.ascii "trxstate_tx_xtalwait"
             65 5F 74 78 5F 78 74
             61 6C 77 61 69 74
      00816E 00                   12433 	.db 0x00
      00816F                      12434 ___str_7:
      00816F 74 72 78 73 74 61 74 12435 	.ascii "trxstate_tx_packet"
             65 5F 74 78 5F 70 61
             63 6B 65 74
      008181 00                   12436 	.db 0x00
      008182                      12437 ___str_8:
      008182 74 72 78 73 74 61 74 12438 	.ascii "trxstate_tx_waitdone"
             65 5F 74 78 5F 77 61
             69 74 64 6F 6E 65
      008196 00                   12439 	.db 0x00
      008197                      12440 ___str_9:
      008197 74 72 78 73 74 61 74 12441 	.ascii "trxstate_txcw_xtalwait"
             65 5F 74 78 63 77 5F
             78 74 61 6C 77 61 69
             74
      0081AD 00                   12442 	.db 0x00
      0081AE                      12443 ___str_10:
      0081AE 74 72 78 73 74 61 74 12444 	.ascii "trxstate_txstream_xtalwait"
             65 5F 74 78 73 74 72
             65 61 6D 5F 78 74 61
             6C 77 61 69 74
      0081C8 00                   12445 	.db 0x00
      0081C9                      12446 ___str_11:
      0081C9 74 72 78 73 74 61 74 12447 	.ascii "trxstate_txstream"
             65 5F 74 78 73 74 72
             65 61 6D
      0081DA 00                   12448 	.db 0x00
      0081DB                      12449 ___str_12:
      0081DB 74 72 78 73 74 61 74 12450 	.ascii "trxstate_rxwor"
             65 5F 72 78 77 6F 72
      0081E9 00                   12451 	.db 0x00
      0081EA                      12452 ___str_13:
      0081EA 74 72 78 73 74 61 74 12453 	.ascii "trxstate_rx"
             65 5F 72 78
      0081F5 00                   12454 	.db 0x00
      0081F6                      12455 ___str_14:
      0081F6 43 48 20             12456 	.ascii "CH "
      0081F9 00                   12457 	.db 0x00
      0081FA                      12458 ___str_15:
      0081FA 20 52 4E 47 20       12459 	.ascii " RNG "
      0081FF 00                   12460 	.db 0x00
      008200                      12461 ___str_16:
      008200 20 56 43 4F 49 20    12462 	.ascii " VCOI "
      008206 00                   12463 	.db 0x00
      008207                      12464 ___str_17:
      008207 20 2A                12465 	.ascii " *"
      008209 00                   12466 	.db 0x00
                                  12467 	.area XINIT   (CODE)
      008647                      12468 __xinit__f30_saved:
      008647 3F                   12469 	.db #0x3f	; 63
      008648                      12470 __xinit__f31_saved:
      008648 F0                   12471 	.db #0xf0	; 240
      008649                      12472 __xinit__f32_saved:
      008649 3F                   12473 	.db #0x3f	; 63
      00864A                      12474 __xinit__f33_saved:
      00864A F0                   12475 	.db #0xf0	; 240
                                  12476 	.area CABS    (ABS,CODE)
