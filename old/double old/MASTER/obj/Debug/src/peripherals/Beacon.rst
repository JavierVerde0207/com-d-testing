                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.6.0 #9615 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module Beacon
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _memcpy
                                     12 	.globl _BEACON_decoding_PARM_3
                                     13 	.globl _BEACON_decoding_PARM_2
                                     14 	.globl _BEACON_decoding
                                     15 ;--------------------------------------------------------
                                     16 ; special function registers
                                     17 ;--------------------------------------------------------
                                     18 	.area RSEG    (ABS,DATA)
      000000                         19 	.org 0x0000
                                     20 ;--------------------------------------------------------
                                     21 ; special function bits
                                     22 ;--------------------------------------------------------
                                     23 	.area RSEG    (ABS,DATA)
      000000                         24 	.org 0x0000
                                     25 ;--------------------------------------------------------
                                     26 ; overlayable register banks
                                     27 ;--------------------------------------------------------
                                     28 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                         29 	.ds 8
                                     30 ;--------------------------------------------------------
                                     31 ; internal ram data
                                     32 ;--------------------------------------------------------
                                     33 	.area DSEG    (DATA)
      00000D                         34 _BEACON_decoding_sloc0_1_0:
      00000D                         35 	.ds 2
      00000F                         36 _BEACON_decoding_sloc1_1_0:
      00000F                         37 	.ds 2
      000011                         38 _BEACON_decoding_sloc2_1_0:
      000011                         39 	.ds 3
      000014                         40 _BEACON_decoding_sloc3_1_0:
      000014                         41 	.ds 3
      000017                         42 _BEACON_decoding_sloc4_1_0:
      000017                         43 	.ds 2
      000019                         44 _BEACON_decoding_sloc5_1_0:
      000019                         45 	.ds 4
                                     46 ;--------------------------------------------------------
                                     47 ; overlayable items in internal ram 
                                     48 ;--------------------------------------------------------
                                     49 ;--------------------------------------------------------
                                     50 ; indirectly addressable internal ram data
                                     51 ;--------------------------------------------------------
                                     52 	.area ISEG    (DATA)
                                     53 ;--------------------------------------------------------
                                     54 ; absolute internal ram data
                                     55 ;--------------------------------------------------------
                                     56 	.area IABS    (ABS,DATA)
                                     57 	.area IABS    (ABS,DATA)
                                     58 ;--------------------------------------------------------
                                     59 ; bit data
                                     60 ;--------------------------------------------------------
                                     61 	.area BSEG    (BIT)
                                     62 ;--------------------------------------------------------
                                     63 ; paged external ram data
                                     64 ;--------------------------------------------------------
                                     65 	.area PSEG    (PAG,XDATA)
                                     66 ;--------------------------------------------------------
                                     67 ; external ram data
                                     68 ;--------------------------------------------------------
                                     69 	.area XSEG    (XDATA)
      000379                         70 _BEACON_decoding_PARM_2:
      000379                         71 	.ds 2
      00037B                         72 _BEACON_decoding_PARM_3:
      00037B                         73 	.ds 1
      00037C                         74 _BEACON_decoding_Rxbuffer_1_79:
      00037C                         75 	.ds 3
                                     76 ;--------------------------------------------------------
                                     77 ; absolute external ram data
                                     78 ;--------------------------------------------------------
                                     79 	.area XABS    (ABS,XDATA)
                                     80 ;--------------------------------------------------------
                                     81 ; external initialized ram data
                                     82 ;--------------------------------------------------------
                                     83 	.area XISEG   (XDATA)
                                     84 	.area HOME    (CODE)
                                     85 	.area GSINIT0 (CODE)
                                     86 	.area GSINIT1 (CODE)
                                     87 	.area GSINIT2 (CODE)
                                     88 	.area GSINIT3 (CODE)
                                     89 	.area GSINIT4 (CODE)
                                     90 	.area GSINIT5 (CODE)
                                     91 	.area GSINIT  (CODE)
                                     92 	.area GSFINAL (CODE)
                                     93 	.area CSEG    (CODE)
                                     94 ;--------------------------------------------------------
                                     95 ; global & static initialisations
                                     96 ;--------------------------------------------------------
                                     97 	.area HOME    (CODE)
                                     98 	.area GSINIT  (CODE)
                                     99 	.area GSFINAL (CODE)
                                    100 	.area GSINIT  (CODE)
                                    101 ;--------------------------------------------------------
                                    102 ; Home
                                    103 ;--------------------------------------------------------
                                    104 	.area HOME    (CODE)
                                    105 	.area HOME    (CODE)
                                    106 ;--------------------------------------------------------
                                    107 ; code
                                    108 ;--------------------------------------------------------
                                    109 	.area CSEG    (CODE)
                                    110 ;------------------------------------------------------------
                                    111 ;Allocation info for local variables in function 'BEACON_decoding'
                                    112 ;------------------------------------------------------------
                                    113 ;sloc0                     Allocated with name '_BEACON_decoding_sloc0_1_0'
                                    114 ;sloc1                     Allocated with name '_BEACON_decoding_sloc1_1_0'
                                    115 ;sloc2                     Allocated with name '_BEACON_decoding_sloc2_1_0'
                                    116 ;sloc3                     Allocated with name '_BEACON_decoding_sloc3_1_0'
                                    117 ;sloc4                     Allocated with name '_BEACON_decoding_sloc4_1_0'
                                    118 ;sloc5                     Allocated with name '_BEACON_decoding_sloc5_1_0'
                                    119 ;beacon                    Allocated with name '_BEACON_decoding_PARM_2'
                                    120 ;length                    Allocated with name '_BEACON_decoding_PARM_3'
                                    121 ;Rxbuffer                  Allocated with name '_BEACON_decoding_Rxbuffer_1_79'
                                    122 ;i                         Allocated with name '_BEACON_decoding_i_1_80'
                                    123 ;------------------------------------------------------------
                                    124 ;	..\src\peripherals\Beacon.c:29: __xdata void BEACON_decoding(uint8_t* Rxbuffer, RF_beacon_t* beacon, uint8_t length)
                                    125 ;	-----------------------------------------
                                    126 ;	 function BEACON_decoding
                                    127 ;	-----------------------------------------
      004593                        128 _BEACON_decoding:
                           000007   129 	ar7 = 0x07
                           000006   130 	ar6 = 0x06
                           000005   131 	ar5 = 0x05
                           000004   132 	ar4 = 0x04
                           000003   133 	ar3 = 0x03
                           000002   134 	ar2 = 0x02
                           000001   135 	ar1 = 0x01
                           000000   136 	ar0 = 0x00
      004593 AF F0            [24]  137 	mov	r7,b
      004595 AE 83            [24]  138 	mov	r6,dph
      004597 E5 82            [12]  139 	mov	a,dpl
      004599 90 03 7C         [24]  140 	mov	dptr,#_BEACON_decoding_Rxbuffer_1_79
      00459C F0               [24]  141 	movx	@dptr,a
      00459D EE               [12]  142 	mov	a,r6
      00459E A3               [24]  143 	inc	dptr
      00459F F0               [24]  144 	movx	@dptr,a
      0045A0 EF               [12]  145 	mov	a,r7
      0045A1 A3               [24]  146 	inc	dptr
      0045A2 F0               [24]  147 	movx	@dptr,a
                                    148 ;	..\src\peripherals\Beacon.c:32: beacon->FlagReceived = VALID_BEACON;
      0045A3 90 03 79         [24]  149 	mov	dptr,#_BEACON_decoding_PARM_2
      0045A6 E0               [24]  150 	movx	a,@dptr
      0045A7 F5 17            [12]  151 	mov	_BEACON_decoding_sloc4_1_0,a
      0045A9 A3               [24]  152 	inc	dptr
      0045AA E0               [24]  153 	movx	a,@dptr
      0045AB F5 18            [12]  154 	mov	(_BEACON_decoding_sloc4_1_0 + 1),a
      0045AD 85 17 82         [24]  155 	mov	dpl,_BEACON_decoding_sloc4_1_0
      0045B0 85 18 83         [24]  156 	mov	dph,(_BEACON_decoding_sloc4_1_0 + 1)
      0045B3 74 01            [12]  157 	mov	a,#0x01
      0045B5 F0               [24]  158 	movx	@dptr,a
                                    159 ;	..\src\peripherals\Beacon.c:33: if(length == 23)
      0045B6 90 03 7B         [24]  160 	mov	dptr,#_BEACON_decoding_PARM_3
      0045B9 E0               [24]  161 	movx	a,@dptr
      0045BA FD               [12]  162 	mov	r5,a
      0045BB BD 17 02         [24]  163 	cjne	r5,#0x17,00121$
      0045BE 80 03            [24]  164 	sjmp	00122$
      0045C0                        165 00121$:
      0045C0 02 4A A3         [24]  166 	ljmp	00108$
      0045C3                        167 00122$:
                                    168 ;	..\src\peripherals\Beacon.c:35: beacon->SatId = *Rxbuffer <<8 | *(Rxbuffer+1);
      0045C3 74 01            [12]  169 	mov	a,#0x01
      0045C5 25 17            [12]  170 	add	a,_BEACON_decoding_sloc4_1_0
      0045C7 F5 0D            [12]  171 	mov	_BEACON_decoding_sloc0_1_0,a
      0045C9 E4               [12]  172 	clr	a
      0045CA 35 18            [12]  173 	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
      0045CC F5 0E            [12]  174 	mov	(_BEACON_decoding_sloc0_1_0 + 1),a
      0045CE 90 03 7C         [24]  175 	mov	dptr,#_BEACON_decoding_Rxbuffer_1_79
      0045D1 E0               [24]  176 	movx	a,@dptr
      0045D2 F9               [12]  177 	mov	r1,a
      0045D3 A3               [24]  178 	inc	dptr
      0045D4 E0               [24]  179 	movx	a,@dptr
      0045D5 FA               [12]  180 	mov	r2,a
      0045D6 A3               [24]  181 	inc	dptr
      0045D7 E0               [24]  182 	movx	a,@dptr
      0045D8 FB               [12]  183 	mov	r3,a
      0045D9 89 82            [24]  184 	mov	dpl,r1
      0045DB 8A 83            [24]  185 	mov	dph,r2
      0045DD 8B F0            [24]  186 	mov	b,r3
      0045DF 12 79 65         [24]  187 	lcall	__gptrget
      0045E2 F8               [12]  188 	mov	r0,a
      0045E3 7D 00            [12]  189 	mov	r5,#0x00
      0045E5 88 10            [24]  190 	mov	(_BEACON_decoding_sloc1_1_0 + 1),r0
                                    191 ;	1-genFromRTrack replaced	mov	_BEACON_decoding_sloc1_1_0,#0x00
      0045E7 8D 0F            [24]  192 	mov	_BEACON_decoding_sloc1_1_0,r5
      0045E9 74 01            [12]  193 	mov	a,#0x01
      0045EB 29               [12]  194 	add	a,r1
      0045EC F8               [12]  195 	mov	r0,a
      0045ED E4               [12]  196 	clr	a
      0045EE 3A               [12]  197 	addc	a,r2
      0045EF FC               [12]  198 	mov	r4,a
      0045F0 8B 05            [24]  199 	mov	ar5,r3
      0045F2 88 82            [24]  200 	mov	dpl,r0
      0045F4 8C 83            [24]  201 	mov	dph,r4
      0045F6 8D F0            [24]  202 	mov	b,r5
      0045F8 12 79 65         [24]  203 	lcall	__gptrget
      0045FB F8               [12]  204 	mov	r0,a
      0045FC 7D 00            [12]  205 	mov	r5,#0x00
      0045FE E5 0F            [12]  206 	mov	a,_BEACON_decoding_sloc1_1_0
      004600 42 00            [12]  207 	orl	ar0,a
      004602 E5 10            [12]  208 	mov	a,(_BEACON_decoding_sloc1_1_0 + 1)
      004604 42 05            [12]  209 	orl	ar5,a
      004606 85 0D 82         [24]  210 	mov	dpl,_BEACON_decoding_sloc0_1_0
      004609 85 0E 83         [24]  211 	mov	dph,(_BEACON_decoding_sloc0_1_0 + 1)
      00460C E8               [12]  212 	mov	a,r0
      00460D F0               [24]  213 	movx	@dptr,a
      00460E ED               [12]  214 	mov	a,r5
      00460F A3               [24]  215 	inc	dptr
      004610 F0               [24]  216 	movx	@dptr,a
                                    217 ;	..\src\peripherals\Beacon.c:36: beacon->BeaconID = (*(Rxbuffer+POS_BEACONID) & MASK_BEACONID)>> 4;
      004611 74 03            [12]  218 	mov	a,#0x03
      004613 25 17            [12]  219 	add	a,_BEACON_decoding_sloc4_1_0
      004615 FC               [12]  220 	mov	r4,a
      004616 E4               [12]  221 	clr	a
      004617 35 18            [12]  222 	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
      004619 FD               [12]  223 	mov	r5,a
      00461A 74 02            [12]  224 	mov	a,#0x02
      00461C 29               [12]  225 	add	a,r1
      00461D F8               [12]  226 	mov	r0,a
      00461E E4               [12]  227 	clr	a
      00461F 3A               [12]  228 	addc	a,r2
      004620 FE               [12]  229 	mov	r6,a
      004621 8B 07            [24]  230 	mov	ar7,r3
      004623 88 82            [24]  231 	mov	dpl,r0
      004625 8E 83            [24]  232 	mov	dph,r6
      004627 8F F0            [24]  233 	mov	b,r7
      004629 12 79 65         [24]  234 	lcall	__gptrget
      00462C 54 F0            [12]  235 	anl	a,#0xf0
      00462E C4               [12]  236 	swap	a
      00462F 54 0F            [12]  237 	anl	a,#0x0f
      004631 F8               [12]  238 	mov	r0,a
      004632 8C 82            [24]  239 	mov	dpl,r4
      004634 8D 83            [24]  240 	mov	dph,r5
      004636 F0               [24]  241 	movx	@dptr,a
                                    242 ;	..\src\peripherals\Beacon.c:39: if(beacon->BeaconID == UPLINK_BEACON)
      004637 8C 82            [24]  243 	mov	dpl,r4
      004639 8D 83            [24]  244 	mov	dph,r5
      00463B E0               [24]  245 	movx	a,@dptr
      00463C F5 0F            [12]  246 	mov	_BEACON_decoding_sloc1_1_0,a
      00463E B8 0C 35         [24]  247 	cjne	r0,#0x0c,00105$
                                    248 ;	..\src\peripherals\Beacon.c:41: beacon->NumPremSlots = *(Rxbuffer+POS_NUMPREM);
      004641 74 04            [12]  249 	mov	a,#0x04
      004643 25 17            [12]  250 	add	a,_BEACON_decoding_sloc4_1_0
      004645 F5 0D            [12]  251 	mov	_BEACON_decoding_sloc0_1_0,a
      004647 E4               [12]  252 	clr	a
      004648 35 18            [12]  253 	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
      00464A F5 0E            [12]  254 	mov	(_BEACON_decoding_sloc0_1_0 + 1),a
      00464C 74 03            [12]  255 	mov	a,#0x03
      00464E 29               [12]  256 	add	a,r1
      00464F F8               [12]  257 	mov	r0,a
      004650 E4               [12]  258 	clr	a
      004651 3A               [12]  259 	addc	a,r2
      004652 FC               [12]  260 	mov	r4,a
      004653 8B 05            [24]  261 	mov	ar5,r3
      004655 88 82            [24]  262 	mov	dpl,r0
      004657 8C 83            [24]  263 	mov	dph,r4
      004659 8D F0            [24]  264 	mov	b,r5
      00465B 12 79 65         [24]  265 	lcall	__gptrget
      00465E F8               [12]  266 	mov	r0,a
      00465F 85 0D 82         [24]  267 	mov	dpl,_BEACON_decoding_sloc0_1_0
      004662 85 0E 83         [24]  268 	mov	dph,(_BEACON_decoding_sloc0_1_0 + 1)
      004665 F0               [24]  269 	movx	@dptr,a
                                    270 ;	..\src\peripherals\Beacon.c:42: beacon->NumACK = 0xFF; //invalid data
      004666 85 17 82         [24]  271 	mov	dpl,_BEACON_decoding_sloc4_1_0
      004669 85 18 83         [24]  272 	mov	dph,(_BEACON_decoding_sloc4_1_0 + 1)
      00466C A3               [24]  273 	inc	dptr
      00466D A3               [24]  274 	inc	dptr
      00466E A3               [24]  275 	inc	dptr
      00466F A3               [24]  276 	inc	dptr
      004670 A3               [24]  277 	inc	dptr
      004671 74 FF            [12]  278 	mov	a,#0xff
      004673 F0               [24]  279 	movx	@dptr,a
      004674 80 41            [24]  280 	sjmp	00106$
      004676                        281 00105$:
                                    282 ;	..\src\peripherals\Beacon.c:43: }else if(beacon->BeaconID == DOWNLINK_BEACON)
      004676 74 0A            [12]  283 	mov	a,#0x0a
      004678 B5 0F 34         [24]  284 	cjne	a,_BEACON_decoding_sloc1_1_0,00102$
                                    285 ;	..\src\peripherals\Beacon.c:45: beacon->NumACK = *(Rxbuffer+POS_NUMPREM);
      00467B 74 05            [12]  286 	mov	a,#0x05
      00467D 25 17            [12]  287 	add	a,_BEACON_decoding_sloc4_1_0
      00467F F5 0F            [12]  288 	mov	_BEACON_decoding_sloc1_1_0,a
      004681 E4               [12]  289 	clr	a
      004682 35 18            [12]  290 	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
      004684 F5 10            [12]  291 	mov	(_BEACON_decoding_sloc1_1_0 + 1),a
      004686 74 03            [12]  292 	mov	a,#0x03
      004688 29               [12]  293 	add	a,r1
      004689 F8               [12]  294 	mov	r0,a
      00468A E4               [12]  295 	clr	a
      00468B 3A               [12]  296 	addc	a,r2
      00468C FC               [12]  297 	mov	r4,a
      00468D 8B 05            [24]  298 	mov	ar5,r3
      00468F 88 82            [24]  299 	mov	dpl,r0
      004691 8C 83            [24]  300 	mov	dph,r4
      004693 8D F0            [24]  301 	mov	b,r5
      004695 12 79 65         [24]  302 	lcall	__gptrget
      004698 F8               [12]  303 	mov	r0,a
      004699 85 0F 82         [24]  304 	mov	dpl,_BEACON_decoding_sloc1_1_0
      00469C 85 10 83         [24]  305 	mov	dph,(_BEACON_decoding_sloc1_1_0 + 1)
      00469F F0               [24]  306 	movx	@dptr,a
                                    307 ;	..\src\peripherals\Beacon.c:46: beacon->NumPremSlots = 0xFF; //invalid data
      0046A0 85 17 82         [24]  308 	mov	dpl,_BEACON_decoding_sloc4_1_0
      0046A3 85 18 83         [24]  309 	mov	dph,(_BEACON_decoding_sloc4_1_0 + 1)
      0046A6 A3               [24]  310 	inc	dptr
      0046A7 A3               [24]  311 	inc	dptr
      0046A8 A3               [24]  312 	inc	dptr
      0046A9 A3               [24]  313 	inc	dptr
      0046AA 74 FF            [12]  314 	mov	a,#0xff
      0046AC F0               [24]  315 	movx	@dptr,a
      0046AD 80 08            [24]  316 	sjmp	00106$
      0046AF                        317 00102$:
                                    318 ;	..\src\peripherals\Beacon.c:49: else beacon->FlagReceived = INVALID_BEACON;
      0046AF 85 17 82         [24]  319 	mov	dpl,_BEACON_decoding_sloc4_1_0
      0046B2 85 18 83         [24]  320 	mov	dph,(_BEACON_decoding_sloc4_1_0 + 1)
      0046B5 E4               [12]  321 	clr	a
      0046B6 F0               [24]  322 	movx	@dptr,a
      0046B7                        323 00106$:
                                    324 ;	..\src\peripherals\Beacon.c:51: beacon->HMAC_flag =(*(Rxbuffer+POS_SECURITY) & MASK_HMAC_FLAG)>>7;
      0046B7 74 06            [12]  325 	mov	a,#0x06
      0046B9 25 17            [12]  326 	add	a,_BEACON_decoding_sloc4_1_0
      0046BB F5 0F            [12]  327 	mov	_BEACON_decoding_sloc1_1_0,a
      0046BD E4               [12]  328 	clr	a
      0046BE 35 18            [12]  329 	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
      0046C0 F5 10            [12]  330 	mov	(_BEACON_decoding_sloc1_1_0 + 1),a
      0046C2 74 04            [12]  331 	mov	a,#0x04
      0046C4 29               [12]  332 	add	a,r1
      0046C5 F5 11            [12]  333 	mov	_BEACON_decoding_sloc2_1_0,a
      0046C7 E4               [12]  334 	clr	a
      0046C8 3A               [12]  335 	addc	a,r2
      0046C9 F5 12            [12]  336 	mov	(_BEACON_decoding_sloc2_1_0 + 1),a
      0046CB 8B 13            [24]  337 	mov	(_BEACON_decoding_sloc2_1_0 + 2),r3
      0046CD 85 11 82         [24]  338 	mov	dpl,_BEACON_decoding_sloc2_1_0
      0046D0 85 12 83         [24]  339 	mov	dph,(_BEACON_decoding_sloc2_1_0 + 1)
      0046D3 85 13 F0         [24]  340 	mov	b,(_BEACON_decoding_sloc2_1_0 + 2)
      0046D6 12 79 65         [24]  341 	lcall	__gptrget
      0046D9 54 80            [12]  342 	anl	a,#0x80
      0046DB 23               [12]  343 	rl	a
      0046DC 54 01            [12]  344 	anl	a,#0x01
      0046DE 85 0F 82         [24]  345 	mov	dpl,_BEACON_decoding_sloc1_1_0
      0046E1 85 10 83         [24]  346 	mov	dph,(_BEACON_decoding_sloc1_1_0 + 1)
      0046E4 F0               [24]  347 	movx	@dptr,a
                                    348 ;	..\src\peripherals\Beacon.c:52: beacon->HMAC_type = (*(Rxbuffer+POS_SECURITY) & MASK_HMAC_TYPE)>>4;
      0046E5 74 07            [12]  349 	mov	a,#0x07
      0046E7 25 17            [12]  350 	add	a,_BEACON_decoding_sloc4_1_0
      0046E9 FC               [12]  351 	mov	r4,a
      0046EA E4               [12]  352 	clr	a
      0046EB 35 18            [12]  353 	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
      0046ED FD               [12]  354 	mov	r5,a
      0046EE 85 11 82         [24]  355 	mov	dpl,_BEACON_decoding_sloc2_1_0
      0046F1 85 12 83         [24]  356 	mov	dph,(_BEACON_decoding_sloc2_1_0 + 1)
      0046F4 85 13 F0         [24]  357 	mov	b,(_BEACON_decoding_sloc2_1_0 + 2)
      0046F7 12 79 65         [24]  358 	lcall	__gptrget
      0046FA 54 70            [12]  359 	anl	a,#0x70
      0046FC C4               [12]  360 	swap	a
      0046FD 54 0F            [12]  361 	anl	a,#0x0f
      0046FF 8C 82            [24]  362 	mov	dpl,r4
      004701 8D 83            [24]  363 	mov	dph,r5
      004703 F0               [24]  364 	movx	@dptr,a
                                    365 ;	..\src\peripherals\Beacon.c:53: beacon->Encryption_flag = (*(Rxbuffer+POS_SECURITY) & MASK_ENCRYPTION_FLAG)>>3;
      004704 74 08            [12]  366 	mov	a,#0x08
      004706 25 17            [12]  367 	add	a,_BEACON_decoding_sloc4_1_0
      004708 FC               [12]  368 	mov	r4,a
      004709 E4               [12]  369 	clr	a
      00470A 35 18            [12]  370 	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
      00470C FD               [12]  371 	mov	r5,a
      00470D 85 11 82         [24]  372 	mov	dpl,_BEACON_decoding_sloc2_1_0
      004710 85 12 83         [24]  373 	mov	dph,(_BEACON_decoding_sloc2_1_0 + 1)
      004713 85 13 F0         [24]  374 	mov	b,(_BEACON_decoding_sloc2_1_0 + 2)
      004716 12 79 65         [24]  375 	lcall	__gptrget
      004719 54 08            [12]  376 	anl	a,#0x08
      00471B C4               [12]  377 	swap	a
      00471C 23               [12]  378 	rl	a
      00471D 54 1F            [12]  379 	anl	a,#0x1f
      00471F 8C 82            [24]  380 	mov	dpl,r4
      004721 8D 83            [24]  381 	mov	dph,r5
      004723 F0               [24]  382 	movx	@dptr,a
                                    383 ;	..\src\peripherals\Beacon.c:54: beacon->Encryption_type = *(Rxbuffer+POS_SECURITY) & MASK_ENCRYPTION_TYPE;
      004724 74 09            [12]  384 	mov	a,#0x09
      004726 25 17            [12]  385 	add	a,_BEACON_decoding_sloc4_1_0
      004728 FC               [12]  386 	mov	r4,a
      004729 E4               [12]  387 	clr	a
      00472A 35 18            [12]  388 	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
      00472C FD               [12]  389 	mov	r5,a
      00472D 85 11 82         [24]  390 	mov	dpl,_BEACON_decoding_sloc2_1_0
      004730 85 12 83         [24]  391 	mov	dph,(_BEACON_decoding_sloc2_1_0 + 1)
      004733 85 13 F0         [24]  392 	mov	b,(_BEACON_decoding_sloc2_1_0 + 2)
      004736 12 79 65         [24]  393 	lcall	__gptrget
      004739 F8               [12]  394 	mov	r0,a
      00473A 53 00 07         [24]  395 	anl	ar0,#0x07
      00473D 8C 82            [24]  396 	mov	dpl,r4
      00473F 8D 83            [24]  397 	mov	dph,r5
      004741 E8               [12]  398 	mov	a,r0
      004742 F0               [24]  399 	movx	@dptr,a
                                    400 ;	..\src\peripherals\Beacon.c:56: *(Rxbuffer+POS_NONCE)   = *(Rxbuffer+POS_NONCE) + *(Rxbuffer+POS_NONCE+1);
      004743 74 05            [12]  401 	mov	a,#0x05
      004745 29               [12]  402 	add	a,r1
      004746 F5 11            [12]  403 	mov	_BEACON_decoding_sloc2_1_0,a
      004748 E4               [12]  404 	clr	a
      004749 3A               [12]  405 	addc	a,r2
      00474A F5 12            [12]  406 	mov	(_BEACON_decoding_sloc2_1_0 + 1),a
      00474C 8B 13            [24]  407 	mov	(_BEACON_decoding_sloc2_1_0 + 2),r3
      00474E 85 11 82         [24]  408 	mov	dpl,_BEACON_decoding_sloc2_1_0
      004751 85 12 83         [24]  409 	mov	dph,(_BEACON_decoding_sloc2_1_0 + 1)
      004754 85 13 F0         [24]  410 	mov	b,(_BEACON_decoding_sloc2_1_0 + 2)
      004757 12 79 65         [24]  411 	lcall	__gptrget
      00475A F5 0F            [12]  412 	mov	_BEACON_decoding_sloc1_1_0,a
      00475C 74 06            [12]  413 	mov	a,#0x06
      00475E 29               [12]  414 	add	a,r1
      00475F F5 14            [12]  415 	mov	_BEACON_decoding_sloc3_1_0,a
      004761 E4               [12]  416 	clr	a
      004762 3A               [12]  417 	addc	a,r2
      004763 F5 15            [12]  418 	mov	(_BEACON_decoding_sloc3_1_0 + 1),a
      004765 8B 16            [24]  419 	mov	(_BEACON_decoding_sloc3_1_0 + 2),r3
      004767 85 14 82         [24]  420 	mov	dpl,_BEACON_decoding_sloc3_1_0
      00476A 85 15 83         [24]  421 	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
      00476D 85 16 F0         [24]  422 	mov	b,(_BEACON_decoding_sloc3_1_0 + 2)
      004770 12 79 65         [24]  423 	lcall	__gptrget
      004773 25 0F            [12]  424 	add	a,_BEACON_decoding_sloc1_1_0
      004775 FD               [12]  425 	mov	r5,a
      004776 85 11 82         [24]  426 	mov	dpl,_BEACON_decoding_sloc2_1_0
      004779 85 12 83         [24]  427 	mov	dph,(_BEACON_decoding_sloc2_1_0 + 1)
      00477C 85 13 F0         [24]  428 	mov	b,(_BEACON_decoding_sloc2_1_0 + 2)
      00477F 12 6A 1E         [24]  429 	lcall	__gptrput
                                    430 ;	..\src\peripherals\Beacon.c:57: *(Rxbuffer+POS_NONCE+1) = *(Rxbuffer+POS_NONCE) - *(Rxbuffer+POS_NONCE+1);
      004782 85 14 82         [24]  431 	mov	dpl,_BEACON_decoding_sloc3_1_0
      004785 85 15 83         [24]  432 	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
      004788 85 16 F0         [24]  433 	mov	b,(_BEACON_decoding_sloc3_1_0 + 2)
      00478B 12 79 65         [24]  434 	lcall	__gptrget
      00478E FC               [12]  435 	mov	r4,a
      00478F ED               [12]  436 	mov	a,r5
      004790 C3               [12]  437 	clr	c
      004791 9C               [12]  438 	subb	a,r4
      004792 FC               [12]  439 	mov	r4,a
      004793 85 14 82         [24]  440 	mov	dpl,_BEACON_decoding_sloc3_1_0
      004796 85 15 83         [24]  441 	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
      004799 85 16 F0         [24]  442 	mov	b,(_BEACON_decoding_sloc3_1_0 + 2)
      00479C 12 6A 1E         [24]  443 	lcall	__gptrput
                                    444 ;	..\src\peripherals\Beacon.c:58: *(Rxbuffer+POS_NONCE)   = *(Rxbuffer+POS_NONCE) - *(Rxbuffer+POS_NONCE+1);
      00479F ED               [12]  445 	mov	a,r5
      0047A0 C3               [12]  446 	clr	c
      0047A1 9C               [12]  447 	subb	a,r4
      0047A2 85 11 82         [24]  448 	mov	dpl,_BEACON_decoding_sloc2_1_0
      0047A5 85 12 83         [24]  449 	mov	dph,(_BEACON_decoding_sloc2_1_0 + 1)
      0047A8 85 13 F0         [24]  450 	mov	b,(_BEACON_decoding_sloc2_1_0 + 2)
      0047AB 12 6A 1E         [24]  451 	lcall	__gptrput
                                    452 ;	..\src\peripherals\Beacon.c:60: memcpy(&beacon->Nonce,Rxbuffer+POS_NONCE,sizeof(uint16_t));
      0047AE 74 0A            [12]  453 	mov	a,#0x0a
      0047B0 25 17            [12]  454 	add	a,_BEACON_decoding_sloc4_1_0
      0047B2 FC               [12]  455 	mov	r4,a
      0047B3 E4               [12]  456 	clr	a
      0047B4 35 18            [12]  457 	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
      0047B6 FD               [12]  458 	mov	r5,a
      0047B7 8C 14            [24]  459 	mov	_BEACON_decoding_sloc3_1_0,r4
      0047B9 8D 15            [24]  460 	mov	(_BEACON_decoding_sloc3_1_0 + 1),r5
      0047BB 75 16 00         [24]  461 	mov	(_BEACON_decoding_sloc3_1_0 + 2),#0x00
      0047BE A8 11            [24]  462 	mov	r0,_BEACON_decoding_sloc2_1_0
      0047C0 AC 12            [24]  463 	mov	r4,(_BEACON_decoding_sloc2_1_0 + 1)
      0047C2 AD 13            [24]  464 	mov	r5,(_BEACON_decoding_sloc2_1_0 + 2)
      0047C4 90 03 E2         [24]  465 	mov	dptr,#_memcpy_PARM_2
      0047C7 E8               [12]  466 	mov	a,r0
      0047C8 F0               [24]  467 	movx	@dptr,a
      0047C9 EC               [12]  468 	mov	a,r4
      0047CA A3               [24]  469 	inc	dptr
      0047CB F0               [24]  470 	movx	@dptr,a
      0047CC ED               [12]  471 	mov	a,r5
      0047CD A3               [24]  472 	inc	dptr
      0047CE F0               [24]  473 	movx	@dptr,a
      0047CF 90 03 E5         [24]  474 	mov	dptr,#_memcpy_PARM_3
      0047D2 74 02            [12]  475 	mov	a,#0x02
      0047D4 F0               [24]  476 	movx	@dptr,a
      0047D5 E4               [12]  477 	clr	a
      0047D6 A3               [24]  478 	inc	dptr
      0047D7 F0               [24]  479 	movx	@dptr,a
      0047D8 85 14 82         [24]  480 	mov	dpl,_BEACON_decoding_sloc3_1_0
      0047DB 85 15 83         [24]  481 	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
      0047DE 85 16 F0         [24]  482 	mov	b,(_BEACON_decoding_sloc3_1_0 + 2)
      0047E1 C0 03            [24]  483 	push	ar3
      0047E3 C0 02            [24]  484 	push	ar2
      0047E5 C0 01            [24]  485 	push	ar1
      0047E7 12 65 97         [24]  486 	lcall	_memcpy
      0047EA D0 01            [24]  487 	pop	ar1
      0047EC D0 02            [24]  488 	pop	ar2
      0047EE D0 03            [24]  489 	pop	ar3
                                    490 ;	..\src\peripherals\Beacon.c:62: memcpy(&beacon->reserved,Rxbuffer+POS_RESERVED,sizeof(uint8_t)*5);
      0047F0 74 0C            [12]  491 	mov	a,#0x0c
      0047F2 25 17            [12]  492 	add	a,_BEACON_decoding_sloc4_1_0
      0047F4 FC               [12]  493 	mov	r4,a
      0047F5 E4               [12]  494 	clr	a
      0047F6 35 18            [12]  495 	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
      0047F8 FD               [12]  496 	mov	r5,a
      0047F9 8C 14            [24]  497 	mov	_BEACON_decoding_sloc3_1_0,r4
      0047FB 8D 15            [24]  498 	mov	(_BEACON_decoding_sloc3_1_0 + 1),r5
      0047FD 75 16 00         [24]  499 	mov	(_BEACON_decoding_sloc3_1_0 + 2),#0x00
      004800 74 07            [12]  500 	mov	a,#0x07
      004802 29               [12]  501 	add	a,r1
      004803 F8               [12]  502 	mov	r0,a
      004804 E4               [12]  503 	clr	a
      004805 3A               [12]  504 	addc	a,r2
      004806 FC               [12]  505 	mov	r4,a
      004807 8B 05            [24]  506 	mov	ar5,r3
      004809 90 03 E2         [24]  507 	mov	dptr,#_memcpy_PARM_2
      00480C E8               [12]  508 	mov	a,r0
      00480D F0               [24]  509 	movx	@dptr,a
      00480E EC               [12]  510 	mov	a,r4
      00480F A3               [24]  511 	inc	dptr
      004810 F0               [24]  512 	movx	@dptr,a
      004811 ED               [12]  513 	mov	a,r5
      004812 A3               [24]  514 	inc	dptr
      004813 F0               [24]  515 	movx	@dptr,a
      004814 90 03 E5         [24]  516 	mov	dptr,#_memcpy_PARM_3
      004817 74 05            [12]  517 	mov	a,#0x05
      004819 F0               [24]  518 	movx	@dptr,a
      00481A E4               [12]  519 	clr	a
      00481B A3               [24]  520 	inc	dptr
      00481C F0               [24]  521 	movx	@dptr,a
      00481D 85 14 82         [24]  522 	mov	dpl,_BEACON_decoding_sloc3_1_0
      004820 85 15 83         [24]  523 	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
      004823 85 16 F0         [24]  524 	mov	b,(_BEACON_decoding_sloc3_1_0 + 2)
      004826 C0 03            [24]  525 	push	ar3
      004828 C0 02            [24]  526 	push	ar2
      00482A C0 01            [24]  527 	push	ar1
      00482C 12 65 97         [24]  528 	lcall	_memcpy
      00482F D0 01            [24]  529 	pop	ar1
      004831 D0 02            [24]  530 	pop	ar2
      004833 D0 03            [24]  531 	pop	ar3
                                    532 ;	..\src\peripherals\Beacon.c:64: beacon->CRC =  *(Rxbuffer+POS_CRC) <<8 | *(Rxbuffer+POS_CRC+1);
      004835 74 11            [12]  533 	mov	a,#0x11
      004837 25 17            [12]  534 	add	a,_BEACON_decoding_sloc4_1_0
      004839 F5 14            [12]  535 	mov	_BEACON_decoding_sloc3_1_0,a
      00483B E4               [12]  536 	clr	a
      00483C 35 18            [12]  537 	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
      00483E F5 15            [12]  538 	mov	(_BEACON_decoding_sloc3_1_0 + 1),a
      004840 74 0C            [12]  539 	mov	a,#0x0c
      004842 29               [12]  540 	add	a,r1
      004843 F8               [12]  541 	mov	r0,a
      004844 E4               [12]  542 	clr	a
      004845 3A               [12]  543 	addc	a,r2
      004846 FC               [12]  544 	mov	r4,a
      004847 8B 05            [24]  545 	mov	ar5,r3
      004849 88 82            [24]  546 	mov	dpl,r0
      00484B 8C 83            [24]  547 	mov	dph,r4
      00484D 8D F0            [24]  548 	mov	b,r5
      00484F 12 79 65         [24]  549 	lcall	__gptrget
      004852 F8               [12]  550 	mov	r0,a
      004853 7D 00            [12]  551 	mov	r5,#0x00
      004855 88 12            [24]  552 	mov	(_BEACON_decoding_sloc2_1_0 + 1),r0
                                    553 ;	1-genFromRTrack replaced	mov	_BEACON_decoding_sloc2_1_0,#0x00
      004857 8D 11            [24]  554 	mov	_BEACON_decoding_sloc2_1_0,r5
      004859 74 0D            [12]  555 	mov	a,#0x0d
      00485B 29               [12]  556 	add	a,r1
      00485C F8               [12]  557 	mov	r0,a
      00485D E4               [12]  558 	clr	a
      00485E 3A               [12]  559 	addc	a,r2
      00485F FC               [12]  560 	mov	r4,a
      004860 8B 05            [24]  561 	mov	ar5,r3
      004862 88 82            [24]  562 	mov	dpl,r0
      004864 8C 83            [24]  563 	mov	dph,r4
      004866 8D F0            [24]  564 	mov	b,r5
      004868 12 79 65         [24]  565 	lcall	__gptrget
      00486B FD               [12]  566 	mov	r5,a
      00486C 78 00            [12]  567 	mov	r0,#0x00
      00486E E5 11            [12]  568 	mov	a,_BEACON_decoding_sloc2_1_0
      004870 42 05            [12]  569 	orl	ar5,a
      004872 E5 12            [12]  570 	mov	a,(_BEACON_decoding_sloc2_1_0 + 1)
      004874 42 00            [12]  571 	orl	ar0,a
      004876 8D 04            [24]  572 	mov	ar4,r5
      004878 E8               [12]  573 	mov	a,r0
      004879 33               [12]  574 	rlc	a
      00487A 95 E0            [12]  575 	subb	a,acc
      00487C FD               [12]  576 	mov	r5,a
      00487D FF               [12]  577 	mov	r7,a
      00487E 85 14 82         [24]  578 	mov	dpl,_BEACON_decoding_sloc3_1_0
      004881 85 15 83         [24]  579 	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
      004884 EC               [12]  580 	mov	a,r4
      004885 F0               [24]  581 	movx	@dptr,a
      004886 E8               [12]  582 	mov	a,r0
      004887 A3               [24]  583 	inc	dptr
      004888 F0               [24]  584 	movx	@dptr,a
      004889 ED               [12]  585 	mov	a,r5
      00488A A3               [24]  586 	inc	dptr
      00488B F0               [24]  587 	movx	@dptr,a
      00488C EF               [12]  588 	mov	a,r7
      00488D A3               [24]  589 	inc	dptr
      00488E F0               [24]  590 	movx	@dptr,a
                                    591 ;	..\src\peripherals\Beacon.c:65: beacon->CRC = beacon->CRC << 16;
      00488F 88 1C            [24]  592 	mov	(_BEACON_decoding_sloc5_1_0 + 3),r0
      004891 8C 1B            [24]  593 	mov	(_BEACON_decoding_sloc5_1_0 + 2),r4
      004893 75 1A 00         [24]  594 	mov	(_BEACON_decoding_sloc5_1_0 + 1),#0x00
      004896 75 19 00         [24]  595 	mov	_BEACON_decoding_sloc5_1_0,#0x00
      004899 85 14 82         [24]  596 	mov	dpl,_BEACON_decoding_sloc3_1_0
      00489C 85 15 83         [24]  597 	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
      00489F E5 19            [12]  598 	mov	a,_BEACON_decoding_sloc5_1_0
      0048A1 F0               [24]  599 	movx	@dptr,a
      0048A2 E5 1A            [12]  600 	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
      0048A4 A3               [24]  601 	inc	dptr
      0048A5 F0               [24]  602 	movx	@dptr,a
      0048A6 E5 1B            [12]  603 	mov	a,(_BEACON_decoding_sloc5_1_0 + 2)
      0048A8 A3               [24]  604 	inc	dptr
      0048A9 F0               [24]  605 	movx	@dptr,a
      0048AA E5 1C            [12]  606 	mov	a,(_BEACON_decoding_sloc5_1_0 + 3)
      0048AC A3               [24]  607 	inc	dptr
      0048AD F0               [24]  608 	movx	@dptr,a
                                    609 ;	..\src\peripherals\Beacon.c:66: beacon->CRC = beacon->CRC | *(Rxbuffer+POS_CRC+2)<<8 | *(Rxbuffer+POS_CRC+3);
      0048AE 74 0E            [12]  610 	mov	a,#0x0e
      0048B0 29               [12]  611 	add	a,r1
      0048B1 FD               [12]  612 	mov	r5,a
      0048B2 E4               [12]  613 	clr	a
      0048B3 3A               [12]  614 	addc	a,r2
      0048B4 FE               [12]  615 	mov	r6,a
      0048B5 8B 07            [24]  616 	mov	ar7,r3
      0048B7 8D 82            [24]  617 	mov	dpl,r5
      0048B9 8E 83            [24]  618 	mov	dph,r6
      0048BB 8F F0            [24]  619 	mov	b,r7
      0048BD 12 79 65         [24]  620 	lcall	__gptrget
      0048C0 FF               [12]  621 	mov	r7,a
      0048C1 7D 00            [12]  622 	mov	r5,#0x00
      0048C3 33               [12]  623 	rlc	a
      0048C4 95 E0            [12]  624 	subb	a,acc
      0048C6 FE               [12]  625 	mov	r6,a
      0048C7 FC               [12]  626 	mov	r4,a
      0048C8 ED               [12]  627 	mov	a,r5
      0048C9 42 19            [12]  628 	orl	_BEACON_decoding_sloc5_1_0,a
      0048CB EF               [12]  629 	mov	a,r7
      0048CC 42 1A            [12]  630 	orl	(_BEACON_decoding_sloc5_1_0 + 1),a
      0048CE EE               [12]  631 	mov	a,r6
      0048CF 42 1B            [12]  632 	orl	(_BEACON_decoding_sloc5_1_0 + 2),a
      0048D1 EC               [12]  633 	mov	a,r4
      0048D2 42 1C            [12]  634 	orl	(_BEACON_decoding_sloc5_1_0 + 3),a
      0048D4 74 0F            [12]  635 	mov	a,#0x0f
      0048D6 29               [12]  636 	add	a,r1
      0048D7 F8               [12]  637 	mov	r0,a
      0048D8 E4               [12]  638 	clr	a
      0048D9 3A               [12]  639 	addc	a,r2
      0048DA FE               [12]  640 	mov	r6,a
      0048DB 8B 07            [24]  641 	mov	ar7,r3
      0048DD 88 82            [24]  642 	mov	dpl,r0
      0048DF 8E 83            [24]  643 	mov	dph,r6
      0048E1 8F F0            [24]  644 	mov	b,r7
      0048E3 12 79 65         [24]  645 	lcall	__gptrget
      0048E6 F8               [12]  646 	mov	r0,a
      0048E7 E4               [12]  647 	clr	a
      0048E8 FF               [12]  648 	mov	r7,a
      0048E9 FE               [12]  649 	mov	r6,a
      0048EA FD               [12]  650 	mov	r5,a
      0048EB E5 19            [12]  651 	mov	a,_BEACON_decoding_sloc5_1_0
      0048ED 42 00            [12]  652 	orl	ar0,a
      0048EF E5 1A            [12]  653 	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
      0048F1 42 07            [12]  654 	orl	ar7,a
      0048F3 E5 1B            [12]  655 	mov	a,(_BEACON_decoding_sloc5_1_0 + 2)
      0048F5 42 06            [12]  656 	orl	ar6,a
      0048F7 E5 1C            [12]  657 	mov	a,(_BEACON_decoding_sloc5_1_0 + 3)
      0048F9 42 05            [12]  658 	orl	ar5,a
      0048FB 85 14 82         [24]  659 	mov	dpl,_BEACON_decoding_sloc3_1_0
      0048FE 85 15 83         [24]  660 	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
      004901 E8               [12]  661 	mov	a,r0
      004902 F0               [24]  662 	movx	@dptr,a
      004903 EF               [12]  663 	mov	a,r7
      004904 A3               [24]  664 	inc	dptr
      004905 F0               [24]  665 	movx	@dptr,a
      004906 EE               [12]  666 	mov	a,r6
      004907 A3               [24]  667 	inc	dptr
      004908 F0               [24]  668 	movx	@dptr,a
      004909 ED               [12]  669 	mov	a,r5
      00490A A3               [24]  670 	inc	dptr
      00490B F0               [24]  671 	movx	@dptr,a
                                    672 ;	..\src\peripherals\Beacon.c:68: beacon->HMAC_Msb = *(Rxbuffer+POS_HMAC_MSB)<<8 | *(Rxbuffer+POS_HMAC_MSB+1);
      00490C 74 15            [12]  673 	mov	a,#0x15
      00490E 25 17            [12]  674 	add	a,_BEACON_decoding_sloc4_1_0
      004910 F5 14            [12]  675 	mov	_BEACON_decoding_sloc3_1_0,a
      004912 E4               [12]  676 	clr	a
      004913 35 18            [12]  677 	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
      004915 F5 15            [12]  678 	mov	(_BEACON_decoding_sloc3_1_0 + 1),a
      004917 74 10            [12]  679 	mov	a,#0x10
      004919 29               [12]  680 	add	a,r1
      00491A F8               [12]  681 	mov	r0,a
      00491B E4               [12]  682 	clr	a
      00491C 3A               [12]  683 	addc	a,r2
      00491D FC               [12]  684 	mov	r4,a
      00491E 8B 05            [24]  685 	mov	ar5,r3
      004920 88 82            [24]  686 	mov	dpl,r0
      004922 8C 83            [24]  687 	mov	dph,r4
      004924 8D F0            [24]  688 	mov	b,r5
      004926 12 79 65         [24]  689 	lcall	__gptrget
      004929 F8               [12]  690 	mov	r0,a
      00492A 7D 00            [12]  691 	mov	r5,#0x00
      00492C 88 1A            [24]  692 	mov	(_BEACON_decoding_sloc5_1_0 + 1),r0
                                    693 ;	1-genFromRTrack replaced	mov	_BEACON_decoding_sloc5_1_0,#0x00
      00492E 8D 19            [24]  694 	mov	_BEACON_decoding_sloc5_1_0,r5
      004930 74 11            [12]  695 	mov	a,#0x11
      004932 29               [12]  696 	add	a,r1
      004933 F8               [12]  697 	mov	r0,a
      004934 E4               [12]  698 	clr	a
      004935 3A               [12]  699 	addc	a,r2
      004936 FC               [12]  700 	mov	r4,a
      004937 8B 05            [24]  701 	mov	ar5,r3
      004939 88 82            [24]  702 	mov	dpl,r0
      00493B 8C 83            [24]  703 	mov	dph,r4
      00493D 8D F0            [24]  704 	mov	b,r5
      00493F 12 79 65         [24]  705 	lcall	__gptrget
      004942 F8               [12]  706 	mov	r0,a
      004943 7D 00            [12]  707 	mov	r5,#0x00
      004945 E5 19            [12]  708 	mov	a,_BEACON_decoding_sloc5_1_0
      004947 42 00            [12]  709 	orl	ar0,a
      004949 E5 1A            [12]  710 	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
      00494B 42 05            [12]  711 	orl	ar5,a
      00494D ED               [12]  712 	mov	a,r5
      00494E FC               [12]  713 	mov	r4,a
      00494F 33               [12]  714 	rlc	a
      004950 95 E0            [12]  715 	subb	a,acc
      004952 FD               [12]  716 	mov	r5,a
      004953 FF               [12]  717 	mov	r7,a
      004954 85 14 82         [24]  718 	mov	dpl,_BEACON_decoding_sloc3_1_0
      004957 85 15 83         [24]  719 	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
      00495A E8               [12]  720 	mov	a,r0
      00495B F0               [24]  721 	movx	@dptr,a
      00495C EC               [12]  722 	mov	a,r4
      00495D A3               [24]  723 	inc	dptr
      00495E F0               [24]  724 	movx	@dptr,a
      00495F ED               [12]  725 	mov	a,r5
      004960 A3               [24]  726 	inc	dptr
      004961 F0               [24]  727 	movx	@dptr,a
      004962 EF               [12]  728 	mov	a,r7
      004963 A3               [24]  729 	inc	dptr
      004964 F0               [24]  730 	movx	@dptr,a
                                    731 ;	..\src\peripherals\Beacon.c:69: beacon->HMAC_Msb = beacon->HMAC_Msb << 16;
      004965 8C 1C            [24]  732 	mov	(_BEACON_decoding_sloc5_1_0 + 3),r4
      004967 88 1B            [24]  733 	mov	(_BEACON_decoding_sloc5_1_0 + 2),r0
      004969 75 1A 00         [24]  734 	mov	(_BEACON_decoding_sloc5_1_0 + 1),#0x00
      00496C 75 19 00         [24]  735 	mov	_BEACON_decoding_sloc5_1_0,#0x00
      00496F 85 14 82         [24]  736 	mov	dpl,_BEACON_decoding_sloc3_1_0
      004972 85 15 83         [24]  737 	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
      004975 E5 19            [12]  738 	mov	a,_BEACON_decoding_sloc5_1_0
      004977 F0               [24]  739 	movx	@dptr,a
      004978 E5 1A            [12]  740 	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
      00497A A3               [24]  741 	inc	dptr
      00497B F0               [24]  742 	movx	@dptr,a
      00497C E5 1B            [12]  743 	mov	a,(_BEACON_decoding_sloc5_1_0 + 2)
      00497E A3               [24]  744 	inc	dptr
      00497F F0               [24]  745 	movx	@dptr,a
      004980 E5 1C            [12]  746 	mov	a,(_BEACON_decoding_sloc5_1_0 + 3)
      004982 A3               [24]  747 	inc	dptr
      004983 F0               [24]  748 	movx	@dptr,a
                                    749 ;	..\src\peripherals\Beacon.c:70: beacon->HMAC_Msb = beacon->HMAC_Msb | (*(Rxbuffer+POS_HMAC_MSB+2)<<8 | *(Rxbuffer+POS_HMAC_MSB+3) & 0x0000FFFF);
      004984 74 12            [12]  750 	mov	a,#0x12
      004986 29               [12]  751 	add	a,r1
      004987 FD               [12]  752 	mov	r5,a
      004988 E4               [12]  753 	clr	a
      004989 3A               [12]  754 	addc	a,r2
      00498A FE               [12]  755 	mov	r6,a
      00498B 8B 07            [24]  756 	mov	ar7,r3
      00498D 8D 82            [24]  757 	mov	dpl,r5
      00498F 8E 83            [24]  758 	mov	dph,r6
      004991 8F F0            [24]  759 	mov	b,r7
      004993 12 79 65         [24]  760 	lcall	__gptrget
      004996 FF               [12]  761 	mov	r7,a
      004997 7D 00            [12]  762 	mov	r5,#0x00
      004999 74 13            [12]  763 	mov	a,#0x13
      00499B 29               [12]  764 	add	a,r1
      00499C F8               [12]  765 	mov	r0,a
      00499D E4               [12]  766 	clr	a
      00499E 3A               [12]  767 	addc	a,r2
      00499F FC               [12]  768 	mov	r4,a
      0049A0 8B 06            [24]  769 	mov	ar6,r3
      0049A2 88 82            [24]  770 	mov	dpl,r0
      0049A4 8C 83            [24]  771 	mov	dph,r4
      0049A6 8E F0            [24]  772 	mov	b,r6
      0049A8 12 79 65         [24]  773 	lcall	__gptrget
      0049AB FE               [12]  774 	mov	r6,a
      0049AC 78 00            [12]  775 	mov	r0,#0x00
      0049AE ED               [12]  776 	mov	a,r5
      0049AF 42 06            [12]  777 	orl	ar6,a
      0049B1 EF               [12]  778 	mov	a,r7
      0049B2 42 00            [12]  779 	orl	ar0,a
      0049B4 E4               [12]  780 	clr	a
      0049B5 FF               [12]  781 	mov	r7,a
      0049B6 FD               [12]  782 	mov	r5,a
      0049B7 E5 19            [12]  783 	mov	a,_BEACON_decoding_sloc5_1_0
      0049B9 42 06            [12]  784 	orl	ar6,a
      0049BB E5 1A            [12]  785 	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
      0049BD 42 00            [12]  786 	orl	ar0,a
      0049BF E5 1B            [12]  787 	mov	a,(_BEACON_decoding_sloc5_1_0 + 2)
      0049C1 42 07            [12]  788 	orl	ar7,a
      0049C3 E5 1C            [12]  789 	mov	a,(_BEACON_decoding_sloc5_1_0 + 3)
      0049C5 42 05            [12]  790 	orl	ar5,a
      0049C7 85 14 82         [24]  791 	mov	dpl,_BEACON_decoding_sloc3_1_0
      0049CA 85 15 83         [24]  792 	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
      0049CD EE               [12]  793 	mov	a,r6
      0049CE F0               [24]  794 	movx	@dptr,a
      0049CF E8               [12]  795 	mov	a,r0
      0049D0 A3               [24]  796 	inc	dptr
      0049D1 F0               [24]  797 	movx	@dptr,a
      0049D2 EF               [12]  798 	mov	a,r7
      0049D3 A3               [24]  799 	inc	dptr
      0049D4 F0               [24]  800 	movx	@dptr,a
      0049D5 ED               [12]  801 	mov	a,r5
      0049D6 A3               [24]  802 	inc	dptr
      0049D7 F0               [24]  803 	movx	@dptr,a
                                    804 ;	..\src\peripherals\Beacon.c:72: beacon->HMAC_Lsb = *(Rxbuffer+POS_HMAC_LSB)<<8 | *(Rxbuffer+POS_HMAC_LSB+1);
      0049D8 74 19            [12]  805 	mov	a,#0x19
      0049DA 25 17            [12]  806 	add	a,_BEACON_decoding_sloc4_1_0
      0049DC F5 14            [12]  807 	mov	_BEACON_decoding_sloc3_1_0,a
      0049DE E4               [12]  808 	clr	a
      0049DF 35 18            [12]  809 	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
      0049E1 F5 15            [12]  810 	mov	(_BEACON_decoding_sloc3_1_0 + 1),a
      0049E3 74 14            [12]  811 	mov	a,#0x14
      0049E5 29               [12]  812 	add	a,r1
      0049E6 F8               [12]  813 	mov	r0,a
      0049E7 E4               [12]  814 	clr	a
      0049E8 3A               [12]  815 	addc	a,r2
      0049E9 FC               [12]  816 	mov	r4,a
      0049EA 8B 05            [24]  817 	mov	ar5,r3
      0049EC 88 82            [24]  818 	mov	dpl,r0
      0049EE 8C 83            [24]  819 	mov	dph,r4
      0049F0 8D F0            [24]  820 	mov	b,r5
      0049F2 12 79 65         [24]  821 	lcall	__gptrget
      0049F5 F8               [12]  822 	mov	r0,a
      0049F6 7D 00            [12]  823 	mov	r5,#0x00
      0049F8 88 1A            [24]  824 	mov	(_BEACON_decoding_sloc5_1_0 + 1),r0
                                    825 ;	1-genFromRTrack replaced	mov	_BEACON_decoding_sloc5_1_0,#0x00
      0049FA 8D 19            [24]  826 	mov	_BEACON_decoding_sloc5_1_0,r5
      0049FC 74 15            [12]  827 	mov	a,#0x15
      0049FE 29               [12]  828 	add	a,r1
      0049FF F8               [12]  829 	mov	r0,a
      004A00 E4               [12]  830 	clr	a
      004A01 3A               [12]  831 	addc	a,r2
      004A02 FC               [12]  832 	mov	r4,a
      004A03 8B 05            [24]  833 	mov	ar5,r3
      004A05 88 82            [24]  834 	mov	dpl,r0
      004A07 8C 83            [24]  835 	mov	dph,r4
      004A09 8D F0            [24]  836 	mov	b,r5
      004A0B 12 79 65         [24]  837 	lcall	__gptrget
      004A0E F8               [12]  838 	mov	r0,a
      004A0F 7D 00            [12]  839 	mov	r5,#0x00
      004A11 E5 19            [12]  840 	mov	a,_BEACON_decoding_sloc5_1_0
      004A13 42 00            [12]  841 	orl	ar0,a
      004A15 E5 1A            [12]  842 	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
      004A17 42 05            [12]  843 	orl	ar5,a
      004A19 ED               [12]  844 	mov	a,r5
      004A1A FC               [12]  845 	mov	r4,a
      004A1B 33               [12]  846 	rlc	a
      004A1C 95 E0            [12]  847 	subb	a,acc
      004A1E FD               [12]  848 	mov	r5,a
      004A1F FF               [12]  849 	mov	r7,a
      004A20 85 14 82         [24]  850 	mov	dpl,_BEACON_decoding_sloc3_1_0
      004A23 85 15 83         [24]  851 	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
      004A26 E8               [12]  852 	mov	a,r0
      004A27 F0               [24]  853 	movx	@dptr,a
      004A28 EC               [12]  854 	mov	a,r4
      004A29 A3               [24]  855 	inc	dptr
      004A2A F0               [24]  856 	movx	@dptr,a
      004A2B ED               [12]  857 	mov	a,r5
      004A2C A3               [24]  858 	inc	dptr
      004A2D F0               [24]  859 	movx	@dptr,a
      004A2E EF               [12]  860 	mov	a,r7
      004A2F A3               [24]  861 	inc	dptr
      004A30 F0               [24]  862 	movx	@dptr,a
                                    863 ;	..\src\peripherals\Beacon.c:73: beacon->HMAC_Lsb = beacon->HMAC_Lsb << 16;
      004A31 8C 1C            [24]  864 	mov	(_BEACON_decoding_sloc5_1_0 + 3),r4
      004A33 88 1B            [24]  865 	mov	(_BEACON_decoding_sloc5_1_0 + 2),r0
      004A35 75 1A 00         [24]  866 	mov	(_BEACON_decoding_sloc5_1_0 + 1),#0x00
      004A38 75 19 00         [24]  867 	mov	_BEACON_decoding_sloc5_1_0,#0x00
      004A3B 85 14 82         [24]  868 	mov	dpl,_BEACON_decoding_sloc3_1_0
      004A3E 85 15 83         [24]  869 	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
      004A41 E5 19            [12]  870 	mov	a,_BEACON_decoding_sloc5_1_0
      004A43 F0               [24]  871 	movx	@dptr,a
      004A44 E5 1A            [12]  872 	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
      004A46 A3               [24]  873 	inc	dptr
      004A47 F0               [24]  874 	movx	@dptr,a
      004A48 E5 1B            [12]  875 	mov	a,(_BEACON_decoding_sloc5_1_0 + 2)
      004A4A A3               [24]  876 	inc	dptr
      004A4B F0               [24]  877 	movx	@dptr,a
      004A4C E5 1C            [12]  878 	mov	a,(_BEACON_decoding_sloc5_1_0 + 3)
      004A4E A3               [24]  879 	inc	dptr
      004A4F F0               [24]  880 	movx	@dptr,a
                                    881 ;	..\src\peripherals\Beacon.c:74: beacon->HMAC_Lsb = beacon->HMAC_Lsb | (*(Rxbuffer+POS_HMAC_LSB+2)<<8 | *(Rxbuffer+POS_HMAC_LSB+3) & 0x0000FFFF);
      004A50 74 16            [12]  882 	mov	a,#0x16
      004A52 29               [12]  883 	add	a,r1
      004A53 FD               [12]  884 	mov	r5,a
      004A54 E4               [12]  885 	clr	a
      004A55 3A               [12]  886 	addc	a,r2
      004A56 FE               [12]  887 	mov	r6,a
      004A57 8B 07            [24]  888 	mov	ar7,r3
      004A59 8D 82            [24]  889 	mov	dpl,r5
      004A5B 8E 83            [24]  890 	mov	dph,r6
      004A5D 8F F0            [24]  891 	mov	b,r7
      004A5F 12 79 65         [24]  892 	lcall	__gptrget
      004A62 FF               [12]  893 	mov	r7,a
      004A63 7D 00            [12]  894 	mov	r5,#0x00
      004A65 74 17            [12]  895 	mov	a,#0x17
      004A67 29               [12]  896 	add	a,r1
      004A68 F9               [12]  897 	mov	r1,a
      004A69 E4               [12]  898 	clr	a
      004A6A 3A               [12]  899 	addc	a,r2
      004A6B FA               [12]  900 	mov	r2,a
      004A6C 89 82            [24]  901 	mov	dpl,r1
      004A6E 8A 83            [24]  902 	mov	dph,r2
      004A70 8B F0            [24]  903 	mov	b,r3
      004A72 12 79 65         [24]  904 	lcall	__gptrget
      004A75 FE               [12]  905 	mov	r6,a
      004A76 79 00            [12]  906 	mov	r1,#0x00
      004A78 ED               [12]  907 	mov	a,r5
      004A79 42 06            [12]  908 	orl	ar6,a
      004A7B EF               [12]  909 	mov	a,r7
      004A7C 42 01            [12]  910 	orl	ar1,a
      004A7E E4               [12]  911 	clr	a
      004A7F FF               [12]  912 	mov	r7,a
      004A80 FD               [12]  913 	mov	r5,a
      004A81 E5 19            [12]  914 	mov	a,_BEACON_decoding_sloc5_1_0
      004A83 42 06            [12]  915 	orl	ar6,a
      004A85 E5 1A            [12]  916 	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
      004A87 42 01            [12]  917 	orl	ar1,a
      004A89 E5 1B            [12]  918 	mov	a,(_BEACON_decoding_sloc5_1_0 + 2)
      004A8B 42 07            [12]  919 	orl	ar7,a
      004A8D E5 1C            [12]  920 	mov	a,(_BEACON_decoding_sloc5_1_0 + 3)
      004A8F 42 05            [12]  921 	orl	ar5,a
      004A91 85 14 82         [24]  922 	mov	dpl,_BEACON_decoding_sloc3_1_0
      004A94 85 15 83         [24]  923 	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
      004A97 EE               [12]  924 	mov	a,r6
      004A98 F0               [24]  925 	movx	@dptr,a
      004A99 E9               [12]  926 	mov	a,r1
      004A9A A3               [24]  927 	inc	dptr
      004A9B F0               [24]  928 	movx	@dptr,a
      004A9C EF               [12]  929 	mov	a,r7
      004A9D A3               [24]  930 	inc	dptr
      004A9E F0               [24]  931 	movx	@dptr,a
      004A9F ED               [12]  932 	mov	a,r5
      004AA0 A3               [24]  933 	inc	dptr
      004AA1 F0               [24]  934 	movx	@dptr,a
      004AA2 22               [24]  935 	ret
      004AA3                        936 00108$:
                                    937 ;	..\src\peripherals\Beacon.c:75: }else beacon->FlagReceived = INVALID_BEACON;
      004AA3 85 17 82         [24]  938 	mov	dpl,_BEACON_decoding_sloc4_1_0
      004AA6 85 18 83         [24]  939 	mov	dph,(_BEACON_decoding_sloc4_1_0 + 1)
      004AA9 E4               [12]  940 	clr	a
      004AAA F0               [24]  941 	movx	@dptr,a
      004AAB 22               [24]  942 	ret
                                    943 	.area CSEG    (CODE)
                                    944 	.area CONST   (CODE)
                                    945 	.area XINIT   (CODE)
                                    946 	.area CABS    (ABS,CODE)
