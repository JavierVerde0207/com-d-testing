                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.6.0 #9615 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module GoldSequence
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _flash_read
                                     12 	.globl _flash_write
                                     13 	.globl _flash_lock
                                     14 	.globl _flash_unlock
                                     15 	.globl _random
                                     16 	.globl _PORTC_7
                                     17 	.globl _PORTC_6
                                     18 	.globl _PORTC_5
                                     19 	.globl _PORTC_4
                                     20 	.globl _PORTC_3
                                     21 	.globl _PORTC_2
                                     22 	.globl _PORTC_1
                                     23 	.globl _PORTC_0
                                     24 	.globl _PORTB_7
                                     25 	.globl _PORTB_6
                                     26 	.globl _PORTB_5
                                     27 	.globl _PORTB_4
                                     28 	.globl _PORTB_3
                                     29 	.globl _PORTB_2
                                     30 	.globl _PORTB_1
                                     31 	.globl _PORTB_0
                                     32 	.globl _PORTA_7
                                     33 	.globl _PORTA_6
                                     34 	.globl _PORTA_5
                                     35 	.globl _PORTA_4
                                     36 	.globl _PORTA_3
                                     37 	.globl _PORTA_2
                                     38 	.globl _PORTA_1
                                     39 	.globl _PORTA_0
                                     40 	.globl _PINC_7
                                     41 	.globl _PINC_6
                                     42 	.globl _PINC_5
                                     43 	.globl _PINC_4
                                     44 	.globl _PINC_3
                                     45 	.globl _PINC_2
                                     46 	.globl _PINC_1
                                     47 	.globl _PINC_0
                                     48 	.globl _PINB_7
                                     49 	.globl _PINB_6
                                     50 	.globl _PINB_5
                                     51 	.globl _PINB_4
                                     52 	.globl _PINB_3
                                     53 	.globl _PINB_2
                                     54 	.globl _PINB_1
                                     55 	.globl _PINB_0
                                     56 	.globl _PINA_7
                                     57 	.globl _PINA_6
                                     58 	.globl _PINA_5
                                     59 	.globl _PINA_4
                                     60 	.globl _PINA_3
                                     61 	.globl _PINA_2
                                     62 	.globl _PINA_1
                                     63 	.globl _PINA_0
                                     64 	.globl _CY
                                     65 	.globl _AC
                                     66 	.globl _F0
                                     67 	.globl _RS1
                                     68 	.globl _RS0
                                     69 	.globl _OV
                                     70 	.globl _F1
                                     71 	.globl _P
                                     72 	.globl _IP_7
                                     73 	.globl _IP_6
                                     74 	.globl _IP_5
                                     75 	.globl _IP_4
                                     76 	.globl _IP_3
                                     77 	.globl _IP_2
                                     78 	.globl _IP_1
                                     79 	.globl _IP_0
                                     80 	.globl _EA
                                     81 	.globl _IE_7
                                     82 	.globl _IE_6
                                     83 	.globl _IE_5
                                     84 	.globl _IE_4
                                     85 	.globl _IE_3
                                     86 	.globl _IE_2
                                     87 	.globl _IE_1
                                     88 	.globl _IE_0
                                     89 	.globl _EIP_7
                                     90 	.globl _EIP_6
                                     91 	.globl _EIP_5
                                     92 	.globl _EIP_4
                                     93 	.globl _EIP_3
                                     94 	.globl _EIP_2
                                     95 	.globl _EIP_1
                                     96 	.globl _EIP_0
                                     97 	.globl _EIE_7
                                     98 	.globl _EIE_6
                                     99 	.globl _EIE_5
                                    100 	.globl _EIE_4
                                    101 	.globl _EIE_3
                                    102 	.globl _EIE_2
                                    103 	.globl _EIE_1
                                    104 	.globl _EIE_0
                                    105 	.globl _E2IP_7
                                    106 	.globl _E2IP_6
                                    107 	.globl _E2IP_5
                                    108 	.globl _E2IP_4
                                    109 	.globl _E2IP_3
                                    110 	.globl _E2IP_2
                                    111 	.globl _E2IP_1
                                    112 	.globl _E2IP_0
                                    113 	.globl _E2IE_7
                                    114 	.globl _E2IE_6
                                    115 	.globl _E2IE_5
                                    116 	.globl _E2IE_4
                                    117 	.globl _E2IE_3
                                    118 	.globl _E2IE_2
                                    119 	.globl _E2IE_1
                                    120 	.globl _E2IE_0
                                    121 	.globl _B_7
                                    122 	.globl _B_6
                                    123 	.globl _B_5
                                    124 	.globl _B_4
                                    125 	.globl _B_3
                                    126 	.globl _B_2
                                    127 	.globl _B_1
                                    128 	.globl _B_0
                                    129 	.globl _ACC_7
                                    130 	.globl _ACC_6
                                    131 	.globl _ACC_5
                                    132 	.globl _ACC_4
                                    133 	.globl _ACC_3
                                    134 	.globl _ACC_2
                                    135 	.globl _ACC_1
                                    136 	.globl _ACC_0
                                    137 	.globl _WTSTAT
                                    138 	.globl _WTIRQEN
                                    139 	.globl _WTEVTD
                                    140 	.globl _WTEVTD1
                                    141 	.globl _WTEVTD0
                                    142 	.globl _WTEVTC
                                    143 	.globl _WTEVTC1
                                    144 	.globl _WTEVTC0
                                    145 	.globl _WTEVTB
                                    146 	.globl _WTEVTB1
                                    147 	.globl _WTEVTB0
                                    148 	.globl _WTEVTA
                                    149 	.globl _WTEVTA1
                                    150 	.globl _WTEVTA0
                                    151 	.globl _WTCNTR1
                                    152 	.globl _WTCNTB
                                    153 	.globl _WTCNTB1
                                    154 	.globl _WTCNTB0
                                    155 	.globl _WTCNTA
                                    156 	.globl _WTCNTA1
                                    157 	.globl _WTCNTA0
                                    158 	.globl _WTCFGB
                                    159 	.globl _WTCFGA
                                    160 	.globl _WDTRESET
                                    161 	.globl _WDTCFG
                                    162 	.globl _U1STATUS
                                    163 	.globl _U1SHREG
                                    164 	.globl _U1MODE
                                    165 	.globl _U1CTRL
                                    166 	.globl _U0STATUS
                                    167 	.globl _U0SHREG
                                    168 	.globl _U0MODE
                                    169 	.globl _U0CTRL
                                    170 	.globl _T2STATUS
                                    171 	.globl _T2PERIOD
                                    172 	.globl _T2PERIOD1
                                    173 	.globl _T2PERIOD0
                                    174 	.globl _T2MODE
                                    175 	.globl _T2CNT
                                    176 	.globl _T2CNT1
                                    177 	.globl _T2CNT0
                                    178 	.globl _T2CLKSRC
                                    179 	.globl _T1STATUS
                                    180 	.globl _T1PERIOD
                                    181 	.globl _T1PERIOD1
                                    182 	.globl _T1PERIOD0
                                    183 	.globl _T1MODE
                                    184 	.globl _T1CNT
                                    185 	.globl _T1CNT1
                                    186 	.globl _T1CNT0
                                    187 	.globl _T1CLKSRC
                                    188 	.globl _T0STATUS
                                    189 	.globl _T0PERIOD
                                    190 	.globl _T0PERIOD1
                                    191 	.globl _T0PERIOD0
                                    192 	.globl _T0MODE
                                    193 	.globl _T0CNT
                                    194 	.globl _T0CNT1
                                    195 	.globl _T0CNT0
                                    196 	.globl _T0CLKSRC
                                    197 	.globl _SPSTATUS
                                    198 	.globl _SPSHREG
                                    199 	.globl _SPMODE
                                    200 	.globl _SPCLKSRC
                                    201 	.globl _RADIOSTAT
                                    202 	.globl _RADIOSTAT1
                                    203 	.globl _RADIOSTAT0
                                    204 	.globl _RADIODATA
                                    205 	.globl _RADIODATA3
                                    206 	.globl _RADIODATA2
                                    207 	.globl _RADIODATA1
                                    208 	.globl _RADIODATA0
                                    209 	.globl _RADIOADDR
                                    210 	.globl _RADIOADDR1
                                    211 	.globl _RADIOADDR0
                                    212 	.globl _RADIOACC
                                    213 	.globl _OC1STATUS
                                    214 	.globl _OC1PIN
                                    215 	.globl _OC1MODE
                                    216 	.globl _OC1COMP
                                    217 	.globl _OC1COMP1
                                    218 	.globl _OC1COMP0
                                    219 	.globl _OC0STATUS
                                    220 	.globl _OC0PIN
                                    221 	.globl _OC0MODE
                                    222 	.globl _OC0COMP
                                    223 	.globl _OC0COMP1
                                    224 	.globl _OC0COMP0
                                    225 	.globl _NVSTATUS
                                    226 	.globl _NVKEY
                                    227 	.globl _NVDATA
                                    228 	.globl _NVDATA1
                                    229 	.globl _NVDATA0
                                    230 	.globl _NVADDR
                                    231 	.globl _NVADDR1
                                    232 	.globl _NVADDR0
                                    233 	.globl _IC1STATUS
                                    234 	.globl _IC1MODE
                                    235 	.globl _IC1CAPT
                                    236 	.globl _IC1CAPT1
                                    237 	.globl _IC1CAPT0
                                    238 	.globl _IC0STATUS
                                    239 	.globl _IC0MODE
                                    240 	.globl _IC0CAPT
                                    241 	.globl _IC0CAPT1
                                    242 	.globl _IC0CAPT0
                                    243 	.globl _PORTR
                                    244 	.globl _PORTC
                                    245 	.globl _PORTB
                                    246 	.globl _PORTA
                                    247 	.globl _PINR
                                    248 	.globl _PINC
                                    249 	.globl _PINB
                                    250 	.globl _PINA
                                    251 	.globl _DIRR
                                    252 	.globl _DIRC
                                    253 	.globl _DIRB
                                    254 	.globl _DIRA
                                    255 	.globl _DBGLNKSTAT
                                    256 	.globl _DBGLNKBUF
                                    257 	.globl _CODECONFIG
                                    258 	.globl _CLKSTAT
                                    259 	.globl _CLKCON
                                    260 	.globl _ANALOGCOMP
                                    261 	.globl _ADCCONV
                                    262 	.globl _ADCCLKSRC
                                    263 	.globl _ADCCH3CONFIG
                                    264 	.globl _ADCCH2CONFIG
                                    265 	.globl _ADCCH1CONFIG
                                    266 	.globl _ADCCH0CONFIG
                                    267 	.globl __XPAGE
                                    268 	.globl _XPAGE
                                    269 	.globl _SP
                                    270 	.globl _PSW
                                    271 	.globl _PCON
                                    272 	.globl _IP
                                    273 	.globl _IE
                                    274 	.globl _EIP
                                    275 	.globl _EIE
                                    276 	.globl _E2IP
                                    277 	.globl _E2IE
                                    278 	.globl _DPS
                                    279 	.globl _DPTR1
                                    280 	.globl _DPTR0
                                    281 	.globl _DPL1
                                    282 	.globl _DPL
                                    283 	.globl _DPH1
                                    284 	.globl _DPH
                                    285 	.globl _B
                                    286 	.globl _ACC
                                    287 	.globl _XTALREADY
                                    288 	.globl _XTALOSC
                                    289 	.globl _XTALAMPL
                                    290 	.globl _SILICONREV
                                    291 	.globl _SCRATCH3
                                    292 	.globl _SCRATCH2
                                    293 	.globl _SCRATCH1
                                    294 	.globl _SCRATCH0
                                    295 	.globl _RADIOMUX
                                    296 	.globl _RADIOFSTATADDR
                                    297 	.globl _RADIOFSTATADDR1
                                    298 	.globl _RADIOFSTATADDR0
                                    299 	.globl _RADIOFDATAADDR
                                    300 	.globl _RADIOFDATAADDR1
                                    301 	.globl _RADIOFDATAADDR0
                                    302 	.globl _OSCRUN
                                    303 	.globl _OSCREADY
                                    304 	.globl _OSCFORCERUN
                                    305 	.globl _OSCCALIB
                                    306 	.globl _MISCCTRL
                                    307 	.globl _LPXOSCGM
                                    308 	.globl _LPOSCREF
                                    309 	.globl _LPOSCREF1
                                    310 	.globl _LPOSCREF0
                                    311 	.globl _LPOSCPER
                                    312 	.globl _LPOSCPER1
                                    313 	.globl _LPOSCPER0
                                    314 	.globl _LPOSCKFILT
                                    315 	.globl _LPOSCKFILT1
                                    316 	.globl _LPOSCKFILT0
                                    317 	.globl _LPOSCFREQ
                                    318 	.globl _LPOSCFREQ1
                                    319 	.globl _LPOSCFREQ0
                                    320 	.globl _LPOSCCONFIG
                                    321 	.globl _PINSEL
                                    322 	.globl _PINCHGC
                                    323 	.globl _PINCHGB
                                    324 	.globl _PINCHGA
                                    325 	.globl _PALTRADIO
                                    326 	.globl _PALTC
                                    327 	.globl _PALTB
                                    328 	.globl _PALTA
                                    329 	.globl _INTCHGC
                                    330 	.globl _INTCHGB
                                    331 	.globl _INTCHGA
                                    332 	.globl _EXTIRQ
                                    333 	.globl _GPIOENABLE
                                    334 	.globl _ANALOGA
                                    335 	.globl _FRCOSCREF
                                    336 	.globl _FRCOSCREF1
                                    337 	.globl _FRCOSCREF0
                                    338 	.globl _FRCOSCPER
                                    339 	.globl _FRCOSCPER1
                                    340 	.globl _FRCOSCPER0
                                    341 	.globl _FRCOSCKFILT
                                    342 	.globl _FRCOSCKFILT1
                                    343 	.globl _FRCOSCKFILT0
                                    344 	.globl _FRCOSCFREQ
                                    345 	.globl _FRCOSCFREQ1
                                    346 	.globl _FRCOSCFREQ0
                                    347 	.globl _FRCOSCCTRL
                                    348 	.globl _FRCOSCCONFIG
                                    349 	.globl _DMA1CONFIG
                                    350 	.globl _DMA1ADDR
                                    351 	.globl _DMA1ADDR1
                                    352 	.globl _DMA1ADDR0
                                    353 	.globl _DMA0CONFIG
                                    354 	.globl _DMA0ADDR
                                    355 	.globl _DMA0ADDR1
                                    356 	.globl _DMA0ADDR0
                                    357 	.globl _ADCTUNE2
                                    358 	.globl _ADCTUNE1
                                    359 	.globl _ADCTUNE0
                                    360 	.globl _ADCCH3VAL
                                    361 	.globl _ADCCH3VAL1
                                    362 	.globl _ADCCH3VAL0
                                    363 	.globl _ADCCH2VAL
                                    364 	.globl _ADCCH2VAL1
                                    365 	.globl _ADCCH2VAL0
                                    366 	.globl _ADCCH1VAL
                                    367 	.globl _ADCCH1VAL1
                                    368 	.globl _ADCCH1VAL0
                                    369 	.globl _ADCCH0VAL
                                    370 	.globl _ADCCH0VAL1
                                    371 	.globl _ADCCH0VAL0
                                    372 	.globl _aligned_alloc_PARM_2
                                    373 	.globl _GOLDSEQUENCE_Init
                                    374 	.globl _GOLDSEQUENCE_Obtain
                                    375 ;--------------------------------------------------------
                                    376 ; special function registers
                                    377 ;--------------------------------------------------------
                                    378 	.area RSEG    (ABS,DATA)
      000000                        379 	.org 0x0000
                           0000E0   380 _ACC	=	0x00e0
                           0000F0   381 _B	=	0x00f0
                           000083   382 _DPH	=	0x0083
                           000085   383 _DPH1	=	0x0085
                           000082   384 _DPL	=	0x0082
                           000084   385 _DPL1	=	0x0084
                           008382   386 _DPTR0	=	0x8382
                           008584   387 _DPTR1	=	0x8584
                           000086   388 _DPS	=	0x0086
                           0000A0   389 _E2IE	=	0x00a0
                           0000C0   390 _E2IP	=	0x00c0
                           000098   391 _EIE	=	0x0098
                           0000B0   392 _EIP	=	0x00b0
                           0000A8   393 _IE	=	0x00a8
                           0000B8   394 _IP	=	0x00b8
                           000087   395 _PCON	=	0x0087
                           0000D0   396 _PSW	=	0x00d0
                           000081   397 _SP	=	0x0081
                           0000D9   398 _XPAGE	=	0x00d9
                           0000D9   399 __XPAGE	=	0x00d9
                           0000CA   400 _ADCCH0CONFIG	=	0x00ca
                           0000CB   401 _ADCCH1CONFIG	=	0x00cb
                           0000D2   402 _ADCCH2CONFIG	=	0x00d2
                           0000D3   403 _ADCCH3CONFIG	=	0x00d3
                           0000D1   404 _ADCCLKSRC	=	0x00d1
                           0000C9   405 _ADCCONV	=	0x00c9
                           0000E1   406 _ANALOGCOMP	=	0x00e1
                           0000C6   407 _CLKCON	=	0x00c6
                           0000C7   408 _CLKSTAT	=	0x00c7
                           000097   409 _CODECONFIG	=	0x0097
                           0000E3   410 _DBGLNKBUF	=	0x00e3
                           0000E2   411 _DBGLNKSTAT	=	0x00e2
                           000089   412 _DIRA	=	0x0089
                           00008A   413 _DIRB	=	0x008a
                           00008B   414 _DIRC	=	0x008b
                           00008E   415 _DIRR	=	0x008e
                           0000C8   416 _PINA	=	0x00c8
                           0000E8   417 _PINB	=	0x00e8
                           0000F8   418 _PINC	=	0x00f8
                           00008D   419 _PINR	=	0x008d
                           000080   420 _PORTA	=	0x0080
                           000088   421 _PORTB	=	0x0088
                           000090   422 _PORTC	=	0x0090
                           00008C   423 _PORTR	=	0x008c
                           0000CE   424 _IC0CAPT0	=	0x00ce
                           0000CF   425 _IC0CAPT1	=	0x00cf
                           00CFCE   426 _IC0CAPT	=	0xcfce
                           0000CC   427 _IC0MODE	=	0x00cc
                           0000CD   428 _IC0STATUS	=	0x00cd
                           0000D6   429 _IC1CAPT0	=	0x00d6
                           0000D7   430 _IC1CAPT1	=	0x00d7
                           00D7D6   431 _IC1CAPT	=	0xd7d6
                           0000D4   432 _IC1MODE	=	0x00d4
                           0000D5   433 _IC1STATUS	=	0x00d5
                           000092   434 _NVADDR0	=	0x0092
                           000093   435 _NVADDR1	=	0x0093
                           009392   436 _NVADDR	=	0x9392
                           000094   437 _NVDATA0	=	0x0094
                           000095   438 _NVDATA1	=	0x0095
                           009594   439 _NVDATA	=	0x9594
                           000096   440 _NVKEY	=	0x0096
                           000091   441 _NVSTATUS	=	0x0091
                           0000BC   442 _OC0COMP0	=	0x00bc
                           0000BD   443 _OC0COMP1	=	0x00bd
                           00BDBC   444 _OC0COMP	=	0xbdbc
                           0000B9   445 _OC0MODE	=	0x00b9
                           0000BA   446 _OC0PIN	=	0x00ba
                           0000BB   447 _OC0STATUS	=	0x00bb
                           0000C4   448 _OC1COMP0	=	0x00c4
                           0000C5   449 _OC1COMP1	=	0x00c5
                           00C5C4   450 _OC1COMP	=	0xc5c4
                           0000C1   451 _OC1MODE	=	0x00c1
                           0000C2   452 _OC1PIN	=	0x00c2
                           0000C3   453 _OC1STATUS	=	0x00c3
                           0000B1   454 _RADIOACC	=	0x00b1
                           0000B3   455 _RADIOADDR0	=	0x00b3
                           0000B2   456 _RADIOADDR1	=	0x00b2
                           00B2B3   457 _RADIOADDR	=	0xb2b3
                           0000B7   458 _RADIODATA0	=	0x00b7
                           0000B6   459 _RADIODATA1	=	0x00b6
                           0000B5   460 _RADIODATA2	=	0x00b5
                           0000B4   461 _RADIODATA3	=	0x00b4
                           B4B5B6B7   462 _RADIODATA	=	0xb4b5b6b7
                           0000BE   463 _RADIOSTAT0	=	0x00be
                           0000BF   464 _RADIOSTAT1	=	0x00bf
                           00BFBE   465 _RADIOSTAT	=	0xbfbe
                           0000DF   466 _SPCLKSRC	=	0x00df
                           0000DC   467 _SPMODE	=	0x00dc
                           0000DE   468 _SPSHREG	=	0x00de
                           0000DD   469 _SPSTATUS	=	0x00dd
                           00009A   470 _T0CLKSRC	=	0x009a
                           00009C   471 _T0CNT0	=	0x009c
                           00009D   472 _T0CNT1	=	0x009d
                           009D9C   473 _T0CNT	=	0x9d9c
                           000099   474 _T0MODE	=	0x0099
                           00009E   475 _T0PERIOD0	=	0x009e
                           00009F   476 _T0PERIOD1	=	0x009f
                           009F9E   477 _T0PERIOD	=	0x9f9e
                           00009B   478 _T0STATUS	=	0x009b
                           0000A2   479 _T1CLKSRC	=	0x00a2
                           0000A4   480 _T1CNT0	=	0x00a4
                           0000A5   481 _T1CNT1	=	0x00a5
                           00A5A4   482 _T1CNT	=	0xa5a4
                           0000A1   483 _T1MODE	=	0x00a1
                           0000A6   484 _T1PERIOD0	=	0x00a6
                           0000A7   485 _T1PERIOD1	=	0x00a7
                           00A7A6   486 _T1PERIOD	=	0xa7a6
                           0000A3   487 _T1STATUS	=	0x00a3
                           0000AA   488 _T2CLKSRC	=	0x00aa
                           0000AC   489 _T2CNT0	=	0x00ac
                           0000AD   490 _T2CNT1	=	0x00ad
                           00ADAC   491 _T2CNT	=	0xadac
                           0000A9   492 _T2MODE	=	0x00a9
                           0000AE   493 _T2PERIOD0	=	0x00ae
                           0000AF   494 _T2PERIOD1	=	0x00af
                           00AFAE   495 _T2PERIOD	=	0xafae
                           0000AB   496 _T2STATUS	=	0x00ab
                           0000E4   497 _U0CTRL	=	0x00e4
                           0000E7   498 _U0MODE	=	0x00e7
                           0000E6   499 _U0SHREG	=	0x00e6
                           0000E5   500 _U0STATUS	=	0x00e5
                           0000EC   501 _U1CTRL	=	0x00ec
                           0000EF   502 _U1MODE	=	0x00ef
                           0000EE   503 _U1SHREG	=	0x00ee
                           0000ED   504 _U1STATUS	=	0x00ed
                           0000DA   505 _WDTCFG	=	0x00da
                           0000DB   506 _WDTRESET	=	0x00db
                           0000F1   507 _WTCFGA	=	0x00f1
                           0000F9   508 _WTCFGB	=	0x00f9
                           0000F2   509 _WTCNTA0	=	0x00f2
                           0000F3   510 _WTCNTA1	=	0x00f3
                           00F3F2   511 _WTCNTA	=	0xf3f2
                           0000FA   512 _WTCNTB0	=	0x00fa
                           0000FB   513 _WTCNTB1	=	0x00fb
                           00FBFA   514 _WTCNTB	=	0xfbfa
                           0000EB   515 _WTCNTR1	=	0x00eb
                           0000F4   516 _WTEVTA0	=	0x00f4
                           0000F5   517 _WTEVTA1	=	0x00f5
                           00F5F4   518 _WTEVTA	=	0xf5f4
                           0000F6   519 _WTEVTB0	=	0x00f6
                           0000F7   520 _WTEVTB1	=	0x00f7
                           00F7F6   521 _WTEVTB	=	0xf7f6
                           0000FC   522 _WTEVTC0	=	0x00fc
                           0000FD   523 _WTEVTC1	=	0x00fd
                           00FDFC   524 _WTEVTC	=	0xfdfc
                           0000FE   525 _WTEVTD0	=	0x00fe
                           0000FF   526 _WTEVTD1	=	0x00ff
                           00FFFE   527 _WTEVTD	=	0xfffe
                           0000E9   528 _WTIRQEN	=	0x00e9
                           0000EA   529 _WTSTAT	=	0x00ea
                                    530 ;--------------------------------------------------------
                                    531 ; special function bits
                                    532 ;--------------------------------------------------------
                                    533 	.area RSEG    (ABS,DATA)
      000000                        534 	.org 0x0000
                           0000E0   535 _ACC_0	=	0x00e0
                           0000E1   536 _ACC_1	=	0x00e1
                           0000E2   537 _ACC_2	=	0x00e2
                           0000E3   538 _ACC_3	=	0x00e3
                           0000E4   539 _ACC_4	=	0x00e4
                           0000E5   540 _ACC_5	=	0x00e5
                           0000E6   541 _ACC_6	=	0x00e6
                           0000E7   542 _ACC_7	=	0x00e7
                           0000F0   543 _B_0	=	0x00f0
                           0000F1   544 _B_1	=	0x00f1
                           0000F2   545 _B_2	=	0x00f2
                           0000F3   546 _B_3	=	0x00f3
                           0000F4   547 _B_4	=	0x00f4
                           0000F5   548 _B_5	=	0x00f5
                           0000F6   549 _B_6	=	0x00f6
                           0000F7   550 _B_7	=	0x00f7
                           0000A0   551 _E2IE_0	=	0x00a0
                           0000A1   552 _E2IE_1	=	0x00a1
                           0000A2   553 _E2IE_2	=	0x00a2
                           0000A3   554 _E2IE_3	=	0x00a3
                           0000A4   555 _E2IE_4	=	0x00a4
                           0000A5   556 _E2IE_5	=	0x00a5
                           0000A6   557 _E2IE_6	=	0x00a6
                           0000A7   558 _E2IE_7	=	0x00a7
                           0000C0   559 _E2IP_0	=	0x00c0
                           0000C1   560 _E2IP_1	=	0x00c1
                           0000C2   561 _E2IP_2	=	0x00c2
                           0000C3   562 _E2IP_3	=	0x00c3
                           0000C4   563 _E2IP_4	=	0x00c4
                           0000C5   564 _E2IP_5	=	0x00c5
                           0000C6   565 _E2IP_6	=	0x00c6
                           0000C7   566 _E2IP_7	=	0x00c7
                           000098   567 _EIE_0	=	0x0098
                           000099   568 _EIE_1	=	0x0099
                           00009A   569 _EIE_2	=	0x009a
                           00009B   570 _EIE_3	=	0x009b
                           00009C   571 _EIE_4	=	0x009c
                           00009D   572 _EIE_5	=	0x009d
                           00009E   573 _EIE_6	=	0x009e
                           00009F   574 _EIE_7	=	0x009f
                           0000B0   575 _EIP_0	=	0x00b0
                           0000B1   576 _EIP_1	=	0x00b1
                           0000B2   577 _EIP_2	=	0x00b2
                           0000B3   578 _EIP_3	=	0x00b3
                           0000B4   579 _EIP_4	=	0x00b4
                           0000B5   580 _EIP_5	=	0x00b5
                           0000B6   581 _EIP_6	=	0x00b6
                           0000B7   582 _EIP_7	=	0x00b7
                           0000A8   583 _IE_0	=	0x00a8
                           0000A9   584 _IE_1	=	0x00a9
                           0000AA   585 _IE_2	=	0x00aa
                           0000AB   586 _IE_3	=	0x00ab
                           0000AC   587 _IE_4	=	0x00ac
                           0000AD   588 _IE_5	=	0x00ad
                           0000AE   589 _IE_6	=	0x00ae
                           0000AF   590 _IE_7	=	0x00af
                           0000AF   591 _EA	=	0x00af
                           0000B8   592 _IP_0	=	0x00b8
                           0000B9   593 _IP_1	=	0x00b9
                           0000BA   594 _IP_2	=	0x00ba
                           0000BB   595 _IP_3	=	0x00bb
                           0000BC   596 _IP_4	=	0x00bc
                           0000BD   597 _IP_5	=	0x00bd
                           0000BE   598 _IP_6	=	0x00be
                           0000BF   599 _IP_7	=	0x00bf
                           0000D0   600 _P	=	0x00d0
                           0000D1   601 _F1	=	0x00d1
                           0000D2   602 _OV	=	0x00d2
                           0000D3   603 _RS0	=	0x00d3
                           0000D4   604 _RS1	=	0x00d4
                           0000D5   605 _F0	=	0x00d5
                           0000D6   606 _AC	=	0x00d6
                           0000D7   607 _CY	=	0x00d7
                           0000C8   608 _PINA_0	=	0x00c8
                           0000C9   609 _PINA_1	=	0x00c9
                           0000CA   610 _PINA_2	=	0x00ca
                           0000CB   611 _PINA_3	=	0x00cb
                           0000CC   612 _PINA_4	=	0x00cc
                           0000CD   613 _PINA_5	=	0x00cd
                           0000CE   614 _PINA_6	=	0x00ce
                           0000CF   615 _PINA_7	=	0x00cf
                           0000E8   616 _PINB_0	=	0x00e8
                           0000E9   617 _PINB_1	=	0x00e9
                           0000EA   618 _PINB_2	=	0x00ea
                           0000EB   619 _PINB_3	=	0x00eb
                           0000EC   620 _PINB_4	=	0x00ec
                           0000ED   621 _PINB_5	=	0x00ed
                           0000EE   622 _PINB_6	=	0x00ee
                           0000EF   623 _PINB_7	=	0x00ef
                           0000F8   624 _PINC_0	=	0x00f8
                           0000F9   625 _PINC_1	=	0x00f9
                           0000FA   626 _PINC_2	=	0x00fa
                           0000FB   627 _PINC_3	=	0x00fb
                           0000FC   628 _PINC_4	=	0x00fc
                           0000FD   629 _PINC_5	=	0x00fd
                           0000FE   630 _PINC_6	=	0x00fe
                           0000FF   631 _PINC_7	=	0x00ff
                           000080   632 _PORTA_0	=	0x0080
                           000081   633 _PORTA_1	=	0x0081
                           000082   634 _PORTA_2	=	0x0082
                           000083   635 _PORTA_3	=	0x0083
                           000084   636 _PORTA_4	=	0x0084
                           000085   637 _PORTA_5	=	0x0085
                           000086   638 _PORTA_6	=	0x0086
                           000087   639 _PORTA_7	=	0x0087
                           000088   640 _PORTB_0	=	0x0088
                           000089   641 _PORTB_1	=	0x0089
                           00008A   642 _PORTB_2	=	0x008a
                           00008B   643 _PORTB_3	=	0x008b
                           00008C   644 _PORTB_4	=	0x008c
                           00008D   645 _PORTB_5	=	0x008d
                           00008E   646 _PORTB_6	=	0x008e
                           00008F   647 _PORTB_7	=	0x008f
                           000090   648 _PORTC_0	=	0x0090
                           000091   649 _PORTC_1	=	0x0091
                           000092   650 _PORTC_2	=	0x0092
                           000093   651 _PORTC_3	=	0x0093
                           000094   652 _PORTC_4	=	0x0094
                           000095   653 _PORTC_5	=	0x0095
                           000096   654 _PORTC_6	=	0x0096
                           000097   655 _PORTC_7	=	0x0097
                                    656 ;--------------------------------------------------------
                                    657 ; overlayable register banks
                                    658 ;--------------------------------------------------------
                                    659 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        660 	.ds 8
                                    661 ;--------------------------------------------------------
                                    662 ; internal ram data
                                    663 ;--------------------------------------------------------
                                    664 	.area DSEG    (DATA)
                                    665 ;--------------------------------------------------------
                                    666 ; overlayable items in internal ram 
                                    667 ;--------------------------------------------------------
                                    668 ;--------------------------------------------------------
                                    669 ; indirectly addressable internal ram data
                                    670 ;--------------------------------------------------------
                                    671 	.area ISEG    (DATA)
                                    672 ;--------------------------------------------------------
                                    673 ; absolute internal ram data
                                    674 ;--------------------------------------------------------
                                    675 	.area IABS    (ABS,DATA)
                                    676 	.area IABS    (ABS,DATA)
                                    677 ;--------------------------------------------------------
                                    678 ; bit data
                                    679 ;--------------------------------------------------------
                                    680 	.area BSEG    (BIT)
                                    681 ;--------------------------------------------------------
                                    682 ; paged external ram data
                                    683 ;--------------------------------------------------------
                                    684 	.area PSEG    (PAG,XDATA)
                                    685 ;--------------------------------------------------------
                                    686 ; external ram data
                                    687 ;--------------------------------------------------------
                                    688 	.area XSEG    (XDATA)
      00037F                        689 _aligned_alloc_PARM_2:
      00037F                        690 	.ds 2
                           00FC06   691 _flash_deviceid	=	0xfc06
                           007020   692 _ADCCH0VAL0	=	0x7020
                           007021   693 _ADCCH0VAL1	=	0x7021
                           007020   694 _ADCCH0VAL	=	0x7020
                           007022   695 _ADCCH1VAL0	=	0x7022
                           007023   696 _ADCCH1VAL1	=	0x7023
                           007022   697 _ADCCH1VAL	=	0x7022
                           007024   698 _ADCCH2VAL0	=	0x7024
                           007025   699 _ADCCH2VAL1	=	0x7025
                           007024   700 _ADCCH2VAL	=	0x7024
                           007026   701 _ADCCH3VAL0	=	0x7026
                           007027   702 _ADCCH3VAL1	=	0x7027
                           007026   703 _ADCCH3VAL	=	0x7026
                           007028   704 _ADCTUNE0	=	0x7028
                           007029   705 _ADCTUNE1	=	0x7029
                           00702A   706 _ADCTUNE2	=	0x702a
                           007010   707 _DMA0ADDR0	=	0x7010
                           007011   708 _DMA0ADDR1	=	0x7011
                           007010   709 _DMA0ADDR	=	0x7010
                           007014   710 _DMA0CONFIG	=	0x7014
                           007012   711 _DMA1ADDR0	=	0x7012
                           007013   712 _DMA1ADDR1	=	0x7013
                           007012   713 _DMA1ADDR	=	0x7012
                           007015   714 _DMA1CONFIG	=	0x7015
                           007070   715 _FRCOSCCONFIG	=	0x7070
                           007071   716 _FRCOSCCTRL	=	0x7071
                           007076   717 _FRCOSCFREQ0	=	0x7076
                           007077   718 _FRCOSCFREQ1	=	0x7077
                           007076   719 _FRCOSCFREQ	=	0x7076
                           007072   720 _FRCOSCKFILT0	=	0x7072
                           007073   721 _FRCOSCKFILT1	=	0x7073
                           007072   722 _FRCOSCKFILT	=	0x7072
                           007078   723 _FRCOSCPER0	=	0x7078
                           007079   724 _FRCOSCPER1	=	0x7079
                           007078   725 _FRCOSCPER	=	0x7078
                           007074   726 _FRCOSCREF0	=	0x7074
                           007075   727 _FRCOSCREF1	=	0x7075
                           007074   728 _FRCOSCREF	=	0x7074
                           007007   729 _ANALOGA	=	0x7007
                           00700C   730 _GPIOENABLE	=	0x700c
                           007003   731 _EXTIRQ	=	0x7003
                           007000   732 _INTCHGA	=	0x7000
                           007001   733 _INTCHGB	=	0x7001
                           007002   734 _INTCHGC	=	0x7002
                           007008   735 _PALTA	=	0x7008
                           007009   736 _PALTB	=	0x7009
                           00700A   737 _PALTC	=	0x700a
                           007046   738 _PALTRADIO	=	0x7046
                           007004   739 _PINCHGA	=	0x7004
                           007005   740 _PINCHGB	=	0x7005
                           007006   741 _PINCHGC	=	0x7006
                           00700B   742 _PINSEL	=	0x700b
                           007060   743 _LPOSCCONFIG	=	0x7060
                           007066   744 _LPOSCFREQ0	=	0x7066
                           007067   745 _LPOSCFREQ1	=	0x7067
                           007066   746 _LPOSCFREQ	=	0x7066
                           007062   747 _LPOSCKFILT0	=	0x7062
                           007063   748 _LPOSCKFILT1	=	0x7063
                           007062   749 _LPOSCKFILT	=	0x7062
                           007068   750 _LPOSCPER0	=	0x7068
                           007069   751 _LPOSCPER1	=	0x7069
                           007068   752 _LPOSCPER	=	0x7068
                           007064   753 _LPOSCREF0	=	0x7064
                           007065   754 _LPOSCREF1	=	0x7065
                           007064   755 _LPOSCREF	=	0x7064
                           007054   756 _LPXOSCGM	=	0x7054
                           007F01   757 _MISCCTRL	=	0x7f01
                           007053   758 _OSCCALIB	=	0x7053
                           007050   759 _OSCFORCERUN	=	0x7050
                           007052   760 _OSCREADY	=	0x7052
                           007051   761 _OSCRUN	=	0x7051
                           007040   762 _RADIOFDATAADDR0	=	0x7040
                           007041   763 _RADIOFDATAADDR1	=	0x7041
                           007040   764 _RADIOFDATAADDR	=	0x7040
                           007042   765 _RADIOFSTATADDR0	=	0x7042
                           007043   766 _RADIOFSTATADDR1	=	0x7043
                           007042   767 _RADIOFSTATADDR	=	0x7042
                           007044   768 _RADIOMUX	=	0x7044
                           007084   769 _SCRATCH0	=	0x7084
                           007085   770 _SCRATCH1	=	0x7085
                           007086   771 _SCRATCH2	=	0x7086
                           007087   772 _SCRATCH3	=	0x7087
                           007F00   773 _SILICONREV	=	0x7f00
                           007F19   774 _XTALAMPL	=	0x7f19
                           007F18   775 _XTALOSC	=	0x7f18
                           007F1A   776 _XTALREADY	=	0x7f1a
                                    777 ;--------------------------------------------------------
                                    778 ; absolute external ram data
                                    779 ;--------------------------------------------------------
                                    780 	.area XABS    (ABS,XDATA)
                                    781 ;--------------------------------------------------------
                                    782 ; external initialized ram data
                                    783 ;--------------------------------------------------------
                                    784 	.area XISEG   (XDATA)
                                    785 	.area HOME    (CODE)
                                    786 	.area GSINIT0 (CODE)
                                    787 	.area GSINIT1 (CODE)
                                    788 	.area GSINIT2 (CODE)
                                    789 	.area GSINIT3 (CODE)
                                    790 	.area GSINIT4 (CODE)
                                    791 	.area GSINIT5 (CODE)
                                    792 	.area GSINIT  (CODE)
                                    793 	.area GSFINAL (CODE)
                                    794 	.area CSEG    (CODE)
                                    795 ;--------------------------------------------------------
                                    796 ; global & static initialisations
                                    797 ;--------------------------------------------------------
                                    798 	.area HOME    (CODE)
                                    799 	.area GSINIT  (CODE)
                                    800 	.area GSFINAL (CODE)
                                    801 	.area GSINIT  (CODE)
                                    802 ;--------------------------------------------------------
                                    803 ; Home
                                    804 ;--------------------------------------------------------
                                    805 	.area HOME    (CODE)
                                    806 	.area HOME    (CODE)
                                    807 ;--------------------------------------------------------
                                    808 ; code
                                    809 ;--------------------------------------------------------
                                    810 	.area CSEG    (CODE)
                                    811 ;------------------------------------------------------------
                                    812 ;Allocation info for local variables in function 'GOLDSEQUENCE_Init'
                                    813 ;------------------------------------------------------------
                                    814 ;	..\src\peripherals\GoldSequence.c:28: void GOLDSEQUENCE_Init(void)
                                    815 ;	-----------------------------------------
                                    816 ;	 function GOLDSEQUENCE_Init
                                    817 ;	-----------------------------------------
      004AAC                        818 _GOLDSEQUENCE_Init:
                           000007   819 	ar7 = 0x07
                           000006   820 	ar6 = 0x06
                           000005   821 	ar5 = 0x05
                           000004   822 	ar4 = 0x04
                           000003   823 	ar3 = 0x03
                           000002   824 	ar2 = 0x02
                           000001   825 	ar1 = 0x01
                           000000   826 	ar0 = 0x00
                                    827 ;	..\src\peripherals\GoldSequence.c:30: flash_unlock();
      004AAC 12 6D 2D         [24]  828 	lcall	_flash_unlock
                                    829 ;	..\src\peripherals\GoldSequence.c:33: flash_write(MEM_GOLD_SEQUENCES_START+0x0000,0b1111100110100100);
      004AAF 90 04 6B         [24]  830 	mov	dptr,#_flash_write_PARM_2
      004AB2 74 A4            [12]  831 	mov	a,#0xa4
      004AB4 F0               [24]  832 	movx	@dptr,a
      004AB5 74 F9            [12]  833 	mov	a,#0xf9
      004AB7 A3               [24]  834 	inc	dptr
      004AB8 F0               [24]  835 	movx	@dptr,a
      004AB9 90 0C 00         [24]  836 	mov	dptr,#0x0c00
      004ABC 12 6B 66         [24]  837 	lcall	_flash_write
                                    838 ;	..\src\peripherals\GoldSequence.c:35: flash_write(MEM_GOLD_SEQUENCES_START+0x0002,0b001010111011000);
      004ABF 90 04 6B         [24]  839 	mov	dptr,#_flash_write_PARM_2
      004AC2 74 D8            [12]  840 	mov	a,#0xd8
      004AC4 F0               [24]  841 	movx	@dptr,a
      004AC5 74 15            [12]  842 	mov	a,#0x15
      004AC7 A3               [24]  843 	inc	dptr
      004AC8 F0               [24]  844 	movx	@dptr,a
      004AC9 90 0C 02         [24]  845 	mov	dptr,#0x0c02
      004ACC 12 6B 66         [24]  846 	lcall	_flash_write
                                    847 ;	..\src\peripherals\GoldSequence.c:38: flash_write(MEM_GOLD_SEQUENCES_START+0x0004,0b1111100100110000);
      004ACF 90 04 6B         [24]  848 	mov	dptr,#_flash_write_PARM_2
      004AD2 74 30            [12]  849 	mov	a,#0x30
      004AD4 F0               [24]  850 	movx	@dptr,a
      004AD5 74 F9            [12]  851 	mov	a,#0xf9
      004AD7 A3               [24]  852 	inc	dptr
      004AD8 F0               [24]  853 	movx	@dptr,a
      004AD9 90 0C 04         [24]  854 	mov	dptr,#0x0c04
      004ADC 12 6B 66         [24]  855 	lcall	_flash_write
                                    856 ;	..\src\peripherals\GoldSequence.c:40: flash_write(MEM_GOLD_SEQUENCES_START+0x0006,0b101101010001110);
      004ADF 90 04 6B         [24]  857 	mov	dptr,#_flash_write_PARM_2
      004AE2 74 8E            [12]  858 	mov	a,#0x8e
      004AE4 F0               [24]  859 	movx	@dptr,a
      004AE5 74 5A            [12]  860 	mov	a,#0x5a
      004AE7 A3               [24]  861 	inc	dptr
      004AE8 F0               [24]  862 	movx	@dptr,a
      004AE9 90 0C 06         [24]  863 	mov	dptr,#0x0c06
      004AEC 12 6B 66         [24]  864 	lcall	_flash_write
                                    865 ;	..\src\peripherals\GoldSequence.c:43: flash_write(MEM_GOLD_SEQUENCES_START+0x0008,0b0000000010010100);
      004AEF 90 04 6B         [24]  866 	mov	dptr,#_flash_write_PARM_2
      004AF2 74 94            [12]  867 	mov	a,#0x94
      004AF4 F0               [24]  868 	movx	@dptr,a
      004AF5 E4               [12]  869 	clr	a
      004AF6 A3               [24]  870 	inc	dptr
      004AF7 F0               [24]  871 	movx	@dptr,a
      004AF8 90 0C 08         [24]  872 	mov	dptr,#0x0c08
      004AFB 12 6B 66         [24]  873 	lcall	_flash_write
                                    874 ;	..\src\peripherals\GoldSequence.c:45: flash_write(MEM_GOLD_SEQUENCES_START+0x000A,0b100111101010110);
      004AFE 90 04 6B         [24]  875 	mov	dptr,#_flash_write_PARM_2
      004B01 74 56            [12]  876 	mov	a,#0x56
      004B03 F0               [24]  877 	movx	@dptr,a
      004B04 74 4F            [12]  878 	mov	a,#0x4f
      004B06 A3               [24]  879 	inc	dptr
      004B07 F0               [24]  880 	movx	@dptr,a
      004B08 90 0C 0A         [24]  881 	mov	dptr,#0x0c0a
      004B0B 12 6B 66         [24]  882 	lcall	_flash_write
                                    883 ;	..\src\peripherals\GoldSequence.c:48: flash_write(MEM_GOLD_SEQUENCES_START+0x000C,0b1000010100111100);
      004B0E 90 04 6B         [24]  884 	mov	dptr,#_flash_write_PARM_2
      004B11 74 3C            [12]  885 	mov	a,#0x3c
      004B13 F0               [24]  886 	movx	@dptr,a
      004B14 74 85            [12]  887 	mov	a,#0x85
      004B16 A3               [24]  888 	inc	dptr
      004B17 F0               [24]  889 	movx	@dptr,a
      004B18 90 0C 0C         [24]  890 	mov	dptr,#0x0c0c
      004B1B 12 6B 66         [24]  891 	lcall	_flash_write
                                    892 ;	..\src\peripherals\GoldSequence.c:50: flash_write(MEM_GOLD_SEQUENCES_START+0x000E,0b011100010011111);
      004B1E 90 04 6B         [24]  893 	mov	dptr,#_flash_write_PARM_2
      004B21 74 9F            [12]  894 	mov	a,#0x9f
      004B23 F0               [24]  895 	movx	@dptr,a
      004B24 74 38            [12]  896 	mov	a,#0x38
      004B26 A3               [24]  897 	inc	dptr
      004B27 F0               [24]  898 	movx	@dptr,a
      004B28 90 0C 0E         [24]  899 	mov	dptr,#0x0c0e
      004B2B 12 6B 66         [24]  900 	lcall	_flash_write
                                    901 ;	..\src\peripherals\GoldSequence.c:53: flash_write(MEM_GOLD_SEQUENCES_START+0x0010,0b0100011111101000);
      004B2E 90 04 6B         [24]  902 	mov	dptr,#_flash_write_PARM_2
      004B31 74 E8            [12]  903 	mov	a,#0xe8
      004B33 F0               [24]  904 	movx	@dptr,a
      004B34 74 47            [12]  905 	mov	a,#0x47
      004B36 A3               [24]  906 	inc	dptr
      004B37 F0               [24]  907 	movx	@dptr,a
      004B38 90 0C 10         [24]  908 	mov	dptr,#0x0c10
      004B3B 12 6B 66         [24]  909 	lcall	_flash_write
                                    910 ;	..\src\peripherals\GoldSequence.c:55: flash_write(MEM_GOLD_SEQUENCES_START+0x0012,0b000001101111011);
      004B3E 90 04 6B         [24]  911 	mov	dptr,#_flash_write_PARM_2
      004B41 74 7B            [12]  912 	mov	a,#0x7b
      004B43 F0               [24]  913 	movx	@dptr,a
      004B44 74 03            [12]  914 	mov	a,#0x03
      004B46 A3               [24]  915 	inc	dptr
      004B47 F0               [24]  916 	movx	@dptr,a
      004B48 90 0C 12         [24]  917 	mov	dptr,#0x0c12
      004B4B 12 6B 66         [24]  918 	lcall	_flash_write
                                    919 ;	..\src\peripherals\GoldSequence.c:58: flash_write(MEM_GOLD_SEQUENCES_START+0x0014,0b0010011010000010);
      004B4E 90 04 6B         [24]  920 	mov	dptr,#_flash_write_PARM_2
      004B51 74 82            [12]  921 	mov	a,#0x82
      004B53 F0               [24]  922 	movx	@dptr,a
      004B54 74 26            [12]  923 	mov	a,#0x26
      004B56 A3               [24]  924 	inc	dptr
      004B57 F0               [24]  925 	movx	@dptr,a
      004B58 90 0C 14         [24]  926 	mov	dptr,#0x0c14
      004B5B 12 6B 66         [24]  927 	lcall	_flash_write
                                    928 ;	..\src\peripherals\GoldSequence.c:60: flash_write(MEM_GOLD_SEQUENCES_START+0x0016,0b001111010001001);
      004B5E 90 04 6B         [24]  929 	mov	dptr,#_flash_write_PARM_2
      004B61 74 89            [12]  930 	mov	a,#0x89
      004B63 F0               [24]  931 	movx	@dptr,a
      004B64 74 1E            [12]  932 	mov	a,#0x1e
      004B66 A3               [24]  933 	inc	dptr
      004B67 F0               [24]  934 	movx	@dptr,a
      004B68 90 0C 16         [24]  935 	mov	dptr,#0x0c16
      004B6B 12 6B 66         [24]  936 	lcall	_flash_write
                                    937 ;	..\src\peripherals\GoldSequence.c:63: flash_write(MEM_GOLD_SEQUENCES_START+0x0018,0b0001011000110111);
      004B6E 90 04 6B         [24]  938 	mov	dptr,#_flash_write_PARM_2
      004B71 74 37            [12]  939 	mov	a,#0x37
      004B73 F0               [24]  940 	movx	@dptr,a
      004B74 74 16            [12]  941 	mov	a,#0x16
      004B76 A3               [24]  942 	inc	dptr
      004B77 F0               [24]  943 	movx	@dptr,a
      004B78 90 0C 18         [24]  944 	mov	dptr,#0x0c18
      004B7B 12 6B 66         [24]  945 	lcall	_flash_write
                                    946 ;	..\src\peripherals\GoldSequence.c:65: flash_write(MEM_GOLD_SEQUENCES_START+0x001A,0b001000001110000);
      004B7E 90 04 6B         [24]  947 	mov	dptr,#_flash_write_PARM_2
      004B81 74 70            [12]  948 	mov	a,#0x70
      004B83 F0               [24]  949 	movx	@dptr,a
      004B84 74 10            [12]  950 	mov	a,#0x10
      004B86 A3               [24]  951 	inc	dptr
      004B87 F0               [24]  952 	movx	@dptr,a
      004B88 90 0C 1A         [24]  953 	mov	dptr,#0x0c1a
      004B8B 12 6B 66         [24]  954 	lcall	_flash_write
                                    955 ;	..\src\peripherals\GoldSequence.c:68: flash_write(MEM_GOLD_SEQUENCES_START+0x001C,0b1000111001101101);
      004B8E 90 04 6B         [24]  956 	mov	dptr,#_flash_write_PARM_2
      004B91 74 6D            [12]  957 	mov	a,#0x6d
      004B93 F0               [24]  958 	movx	@dptr,a
      004B94 74 8E            [12]  959 	mov	a,#0x8e
      004B96 A3               [24]  960 	inc	dptr
      004B97 F0               [24]  961 	movx	@dptr,a
      004B98 90 0C 1C         [24]  962 	mov	dptr,#0x0c1c
      004B9B 12 6B 66         [24]  963 	lcall	_flash_write
                                    964 ;	..\src\peripherals\GoldSequence.c:70: flash_write(MEM_GOLD_SEQUENCES_START+0x001E,0b101011100001100);
      004B9E 90 04 6B         [24]  965 	mov	dptr,#_flash_write_PARM_2
      004BA1 74 0C            [12]  966 	mov	a,#0x0c
      004BA3 F0               [24]  967 	movx	@dptr,a
      004BA4 74 57            [12]  968 	mov	a,#0x57
      004BA6 A3               [24]  969 	inc	dptr
      004BA7 F0               [24]  970 	movx	@dptr,a
      004BA8 90 0C 1E         [24]  971 	mov	dptr,#0x0c1e
      004BAB 12 6B 66         [24]  972 	lcall	_flash_write
                                    973 ;	..\src\peripherals\GoldSequence.c:73: flash_write(MEM_GOLD_SEQUENCES_START+0x0020,0b1100001001000000);
      004BAE 90 04 6B         [24]  974 	mov	dptr,#_flash_write_PARM_2
      004BB1 74 40            [12]  975 	mov	a,#0x40
      004BB3 F0               [24]  976 	movx	@dptr,a
      004BB4 74 C2            [12]  977 	mov	a,#0xc2
      004BB6 A3               [24]  978 	inc	dptr
      004BB7 F0               [24]  979 	movx	@dptr,a
      004BB8 90 0C 20         [24]  980 	mov	dptr,#0x0c20
      004BBB 12 6B 66         [24]  981 	lcall	_flash_write
                                    982 ;	..\src\peripherals\GoldSequence.c:75: flash_write(MEM_GOLD_SEQUENCES_START+0x0022,0b111010010110010);
      004BBE 90 04 6B         [24]  983 	mov	dptr,#_flash_write_PARM_2
      004BC1 74 B2            [12]  984 	mov	a,#0xb2
      004BC3 F0               [24]  985 	movx	@dptr,a
      004BC4 74 74            [12]  986 	mov	a,#0x74
      004BC6 A3               [24]  987 	inc	dptr
      004BC7 F0               [24]  988 	movx	@dptr,a
      004BC8 90 0C 22         [24]  989 	mov	dptr,#0x0c22
      004BCB 12 6B 66         [24]  990 	lcall	_flash_write
                                    991 ;	..\src\peripherals\GoldSequence.c:78: flash_write(MEM_GOLD_SEQUENCES_START+0x0024,0b1110010001010110);
      004BCE 90 04 6B         [24]  992 	mov	dptr,#_flash_write_PARM_2
      004BD1 74 56            [12]  993 	mov	a,#0x56
      004BD3 F0               [24]  994 	movx	@dptr,a
      004BD4 74 E4            [12]  995 	mov	a,#0xe4
      004BD6 A3               [24]  996 	inc	dptr
      004BD7 F0               [24]  997 	movx	@dptr,a
      004BD8 90 0C 24         [24]  998 	mov	dptr,#0x0c24
      004BDB 12 6B 66         [24]  999 	lcall	_flash_write
                                   1000 ;	..\src\peripherals\GoldSequence.c:80: flash_write(MEM_GOLD_SEQUENCES_START+0x0026,0b010010101101101);
      004BDE 90 04 6B         [24] 1001 	mov	dptr,#_flash_write_PARM_2
      004BE1 74 6D            [12] 1002 	mov	a,#0x6d
      004BE3 F0               [24] 1003 	movx	@dptr,a
      004BE4 74 25            [12] 1004 	mov	a,#0x25
      004BE6 A3               [24] 1005 	inc	dptr
      004BE7 F0               [24] 1006 	movx	@dptr,a
      004BE8 90 0C 26         [24] 1007 	mov	dptr,#0x0c26
      004BEB 12 6B 66         [24] 1008 	lcall	_flash_write
                                   1009 ;	..\src\peripherals\GoldSequence.c:83: flash_write(MEM_GOLD_SEQUENCES_START+0x0028,0b0111011101011101);
      004BEE 90 04 6B         [24] 1010 	mov	dptr,#_flash_write_PARM_2
      004BF1 74 5D            [12] 1011 	mov	a,#0x5d
      004BF3 F0               [24] 1012 	movx	@dptr,a
      004BF4 74 77            [12] 1013 	mov	a,#0x77
      004BF6 A3               [24] 1014 	inc	dptr
      004BF7 F0               [24] 1015 	movx	@dptr,a
      004BF8 90 0C 28         [24] 1016 	mov	dptr,#0x0c28
      004BFB 12 6B 66         [24] 1017 	lcall	_flash_write
                                   1018 ;	..\src\peripherals\GoldSequence.c:85: flash_write(MEM_GOLD_SEQUENCES_START+0x002A,0b000110110000010);
      004BFE 90 04 6B         [24] 1019 	mov	dptr,#_flash_write_PARM_2
      004C01 74 82            [12] 1020 	mov	a,#0x82
      004C03 F0               [24] 1021 	movx	@dptr,a
      004C04 74 0D            [12] 1022 	mov	a,#0x0d
      004C06 A3               [24] 1023 	inc	dptr
      004C07 F0               [24] 1024 	movx	@dptr,a
      004C08 90 0C 2A         [24] 1025 	mov	dptr,#0x0c2a
      004C0B 12 6B 66         [24] 1026 	lcall	_flash_write
                                   1027 ;	..\src\peripherals\GoldSequence.c:88: flash_write(MEM_GOLD_SEQUENCES_START+0x002C,0b1011111011011000);
      004C0E 90 04 6B         [24] 1028 	mov	dptr,#_flash_write_PARM_2
      004C11 74 D8            [12] 1029 	mov	a,#0xd8
      004C13 F0               [24] 1030 	movx	@dptr,a
      004C14 74 BE            [12] 1031 	mov	a,#0xbe
      004C16 A3               [24] 1032 	inc	dptr
      004C17 F0               [24] 1033 	movx	@dptr,a
      004C18 90 0C 2C         [24] 1034 	mov	dptr,#0x0c2c
      004C1B 12 6B 66         [24] 1035 	lcall	_flash_write
                                   1036 ;	..\src\peripherals\GoldSequence.c:90: flash_write(MEM_GOLD_SEQUENCES_START+0x002E,0b101100111110101);
      004C1E 90 04 6B         [24] 1037 	mov	dptr,#_flash_write_PARM_2
      004C21 74 F5            [12] 1038 	mov	a,#0xf5
      004C23 F0               [24] 1039 	movx	@dptr,a
      004C24 74 59            [12] 1040 	mov	a,#0x59
      004C26 A3               [24] 1041 	inc	dptr
      004C27 F0               [24] 1042 	movx	@dptr,a
      004C28 90 0C 2E         [24] 1043 	mov	dptr,#0x0c2e
      004C2B 12 6B 66         [24] 1044 	lcall	_flash_write
                                   1045 ;	..\src\peripherals\GoldSequence.c:93: flash_write(MEM_GOLD_SEQUENCES_START+0x0030,0b0101101000011010);
      004C2E 90 04 6B         [24] 1046 	mov	dptr,#_flash_write_PARM_2
      004C31 74 1A            [12] 1047 	mov	a,#0x1a
      004C33 F0               [24] 1048 	movx	@dptr,a
      004C34 74 5A            [12] 1049 	mov	a,#0x5a
      004C36 A3               [24] 1050 	inc	dptr
      004C37 F0               [24] 1051 	movx	@dptr,a
      004C38 90 0C 30         [24] 1052 	mov	dptr,#0x0c30
      004C3B 12 6B 66         [24] 1053 	lcall	_flash_write
                                   1054 ;	..\src\peripherals\GoldSequence.c:95: flash_write(MEM_GOLD_SEQUENCES_START+0x0032,0b011001111001110);
      004C3E 90 04 6B         [24] 1055 	mov	dptr,#_flash_write_PARM_2
      004C41 74 CE            [12] 1056 	mov	a,#0xce
      004C43 F0               [24] 1057 	movx	@dptr,a
      004C44 74 33            [12] 1058 	mov	a,#0x33
      004C46 A3               [24] 1059 	inc	dptr
      004C47 F0               [24] 1060 	movx	@dptr,a
      004C48 90 0C 32         [24] 1061 	mov	dptr,#0x0c32
      004C4B 12 6B 66         [24] 1062 	lcall	_flash_write
                                   1063 ;	..\src\peripherals\GoldSequence.c:98: flash_write(MEM_GOLD_SEQUENCES_START+0x0034,0b1010100001111011);
      004C4E 90 04 6B         [24] 1064 	mov	dptr,#_flash_write_PARM_2
      004C51 74 7B            [12] 1065 	mov	a,#0x7b
      004C53 F0               [24] 1066 	movx	@dptr,a
      004C54 74 A8            [12] 1067 	mov	a,#0xa8
      004C56 A3               [24] 1068 	inc	dptr
      004C57 F0               [24] 1069 	movx	@dptr,a
      004C58 90 0C 34         [24] 1070 	mov	dptr,#0x0c34
      004C5B 12 6B 66         [24] 1071 	lcall	_flash_write
                                   1072 ;	..\src\peripherals\GoldSequence.c:100: flash_write(MEM_GOLD_SEQUENCES_START+0x0036,0b000011011010011);
      004C5E 90 04 6B         [24] 1073 	mov	dptr,#_flash_write_PARM_2
      004C61 74 D3            [12] 1074 	mov	a,#0xd3
      004C63 F0               [24] 1075 	movx	@dptr,a
      004C64 74 06            [12] 1076 	mov	a,#0x06
      004C66 A3               [24] 1077 	inc	dptr
      004C67 F0               [24] 1078 	movx	@dptr,a
      004C68 90 0C 36         [24] 1079 	mov	dptr,#0x0c36
      004C6B 12 6B 66         [24] 1080 	lcall	_flash_write
                                   1081 ;	..\src\peripherals\GoldSequence.c:103: flash_write(MEM_GOLD_SEQUENCES_START+0x0038,0b0101000101001011);
      004C6E 90 04 6B         [24] 1082 	mov	dptr,#_flash_write_PARM_2
      004C71 74 4B            [12] 1083 	mov	a,#0x4b
      004C73 F0               [24] 1084 	movx	@dptr,a
      004C74 74 51            [12] 1085 	mov	a,#0x51
      004C76 A3               [24] 1086 	inc	dptr
      004C77 F0               [24] 1087 	movx	@dptr,a
      004C78 90 0C 38         [24] 1088 	mov	dptr,#0x0c38
      004C7B 12 6B 66         [24] 1089 	lcall	_flash_write
                                   1090 ;	..\src\peripherals\GoldSequence.c:105: flash_write(MEM_GOLD_SEQUENCES_START+0x003A,0b101110001011101);
      004C7E 90 04 6B         [24] 1091 	mov	dptr,#_flash_write_PARM_2
      004C81 74 5D            [12] 1092 	mov	a,#0x5d
      004C83 F0               [24] 1093 	movx	@dptr,a
      004C84 14               [12] 1094 	dec	a
      004C85 A3               [24] 1095 	inc	dptr
      004C86 F0               [24] 1096 	movx	@dptr,a
      004C87 90 0C 3A         [24] 1097 	mov	dptr,#0x0c3a
      004C8A 12 6B 66         [24] 1098 	lcall	_flash_write
                                   1099 ;	..\src\peripherals\GoldSequence.c:108: flash_write(MEM_GOLD_SEQUENCES_START+0x003C,0b0010110111010011);
      004C8D 90 04 6B         [24] 1100 	mov	dptr,#_flash_write_PARM_2
      004C90 74 D3            [12] 1101 	mov	a,#0xd3
      004C92 F0               [24] 1102 	movx	@dptr,a
      004C93 74 2D            [12] 1103 	mov	a,#0x2d
      004C95 A3               [24] 1104 	inc	dptr
      004C96 F0               [24] 1105 	movx	@dptr,a
      004C97 90 0C 3C         [24] 1106 	mov	dptr,#0x0c3c
      004C9A 12 6B 66         [24] 1107 	lcall	_flash_write
                                   1108 ;	..\src\peripherals\GoldSequence.c:110: flash_write(MEM_GOLD_SEQUENCES_START+0x003E,0b111000100011010);
      004C9D 90 04 6B         [24] 1109 	mov	dptr,#_flash_write_PARM_2
      004CA0 74 1A            [12] 1110 	mov	a,#0x1a
      004CA2 F0               [24] 1111 	movx	@dptr,a
      004CA3 74 71            [12] 1112 	mov	a,#0x71
      004CA5 A3               [24] 1113 	inc	dptr
      004CA6 F0               [24] 1114 	movx	@dptr,a
      004CA7 90 0C 3E         [24] 1115 	mov	dptr,#0x0c3e
      004CAA 12 6B 66         [24] 1116 	lcall	_flash_write
                                   1117 ;	..\src\peripherals\GoldSequence.c:113: flash_write(MEM_GOLD_SEQUENCES_START+0x0040,0b1001001110011111);
      004CAD 90 04 6B         [24] 1118 	mov	dptr,#_flash_write_PARM_2
      004CB0 74 9F            [12] 1119 	mov	a,#0x9f
      004CB2 F0               [24] 1120 	movx	@dptr,a
      004CB3 74 93            [12] 1121 	mov	a,#0x93
      004CB5 A3               [24] 1122 	inc	dptr
      004CB6 F0               [24] 1123 	movx	@dptr,a
      004CB7 90 0C 40         [24] 1124 	mov	dptr,#0x0c40
      004CBA 12 6B 66         [24] 1125 	lcall	_flash_write
                                   1126 ;	..\src\peripherals\GoldSequence.c:115: flash_write(MEM_GOLD_SEQUENCES_START+0x0042,0b110011110111001);
      004CBD 90 04 6B         [24] 1127 	mov	dptr,#_flash_write_PARM_2
      004CC0 74 B9            [12] 1128 	mov	a,#0xb9
      004CC2 F0               [24] 1129 	movx	@dptr,a
      004CC3 74 67            [12] 1130 	mov	a,#0x67
      004CC5 A3               [24] 1131 	inc	dptr
      004CC6 F0               [24] 1132 	movx	@dptr,a
      004CC7 90 0C 42         [24] 1133 	mov	dptr,#0x0c42
      004CCA 12 6B 66         [24] 1134 	lcall	_flash_write
                                   1135 ;	..\src\peripherals\GoldSequence.c:118: flash_write(MEM_GOLD_SEQUENCES_START+0x0044,0b0100110010111001);
      004CCD 90 04 6B         [24] 1136 	mov	dptr,#_flash_write_PARM_2
      004CD0 74 B9            [12] 1137 	mov	a,#0xb9
      004CD2 F0               [24] 1138 	movx	@dptr,a
      004CD3 74 4C            [12] 1139 	mov	a,#0x4c
      004CD5 A3               [24] 1140 	inc	dptr
      004CD6 F0               [24] 1141 	movx	@dptr,a
      004CD7 90 0C 44         [24] 1142 	mov	dptr,#0x0c44
      004CDA 12 6B 66         [24] 1143 	lcall	_flash_write
                                   1144 ;	..\src\peripherals\GoldSequence.c:120: flash_write(MEM_GOLD_SEQUENCES_START+0x0046,0b110110011101000);
      004CDD 90 04 6B         [24] 1145 	mov	dptr,#_flash_write_PARM_2
      004CE0 74 E8            [12] 1146 	mov	a,#0xe8
      004CE2 F0               [24] 1147 	movx	@dptr,a
      004CE3 74 6C            [12] 1148 	mov	a,#0x6c
      004CE5 A3               [24] 1149 	inc	dptr
      004CE6 F0               [24] 1150 	movx	@dptr,a
      004CE7 90 0C 46         [24] 1151 	mov	dptr,#0x0c46
      004CEA 12 6B 66         [24] 1152 	lcall	_flash_write
                                   1153 ;	..\src\peripherals\GoldSequence.c:123: flash_write(MEM_GOLD_SEQUENCES_START+0x0048,0b1010001100101010);
      004CED 90 04 6B         [24] 1154 	mov	dptr,#_flash_write_PARM_2
      004CF0 74 2A            [12] 1155 	mov	a,#0x2a
      004CF2 F0               [24] 1156 	movx	@dptr,a
      004CF3 74 A3            [12] 1157 	mov	a,#0xa3
      004CF5 A3               [24] 1158 	inc	dptr
      004CF6 F0               [24] 1159 	movx	@dptr,a
      004CF7 90 0C 48         [24] 1160 	mov	dptr,#0x0c48
      004CFA 12 6B 66         [24] 1161 	lcall	_flash_write
                                   1162 ;	..\src\peripherals\GoldSequence.c:125: flash_write(MEM_GOLD_SEQUENCES_START+0x004A,0b110100101000000);
      004CFD 90 04 6B         [24] 1163 	mov	dptr,#_flash_write_PARM_2
      004D00 74 40            [12] 1164 	mov	a,#0x40
      004D02 F0               [24] 1165 	movx	@dptr,a
      004D03 74 69            [12] 1166 	mov	a,#0x69
      004D05 A3               [24] 1167 	inc	dptr
      004D06 F0               [24] 1168 	movx	@dptr,a
      004D07 90 0C 4A         [24] 1169 	mov	dptr,#0x0c4a
      004D0A 12 6B 66         [24] 1170 	lcall	_flash_write
                                   1171 ;	..\src\peripherals\GoldSequence.c:128: flash_write(MEM_GOLD_SEQUENCES_START+0x004C,0b1101010011100011);
      004D0D 90 04 6B         [24] 1172 	mov	dptr,#_flash_write_PARM_2
      004D10 74 E3            [12] 1173 	mov	a,#0xe3
      004D12 F0               [24] 1174 	movx	@dptr,a
      004D13 74 D4            [12] 1175 	mov	a,#0xd4
      004D15 A3               [24] 1176 	inc	dptr
      004D16 F0               [24] 1177 	movx	@dptr,a
      004D17 90 0C 4C         [24] 1178 	mov	dptr,#0x0c4c
      004D1A 12 6B 66         [24] 1179 	lcall	_flash_write
                                   1180 ;	..\src\peripherals\GoldSequence.c:130: flash_write(MEM_GOLD_SEQUENCES_START+0x004E,0b010101110010100);
      004D1D 90 04 6B         [24] 1181 	mov	dptr,#_flash_write_PARM_2
      004D20 74 94            [12] 1182 	mov	a,#0x94
      004D22 F0               [24] 1183 	movx	@dptr,a
      004D23 74 2B            [12] 1184 	mov	a,#0x2b
      004D25 A3               [24] 1185 	inc	dptr
      004D26 F0               [24] 1186 	movx	@dptr,a
      004D27 90 0C 4E         [24] 1187 	mov	dptr,#0x0c4e
      004D2A 12 6B 66         [24] 1188 	lcall	_flash_write
                                   1189 ;	..\src\peripherals\GoldSequence.c:133: flash_write(MEM_GOLD_SEQUENCES_START+0x0050,0b1110111100000111);
      004D2D 90 04 6B         [24] 1190 	mov	dptr,#_flash_write_PARM_2
      004D30 74 07            [12] 1191 	mov	a,#0x07
      004D32 F0               [24] 1192 	movx	@dptr,a
      004D33 74 EF            [12] 1193 	mov	a,#0xef
      004D35 A3               [24] 1194 	inc	dptr
      004D36 F0               [24] 1195 	movx	@dptr,a
      004D37 90 0C 50         [24] 1196 	mov	dptr,#0x0c50
      004D3A 12 6B 66         [24] 1197 	lcall	_flash_write
                                   1198 ;	..\src\peripherals\GoldSequence.c:135: flash_write(MEM_GOLD_SEQUENCES_START+0x0052,0b100101011111110);
      004D3D 90 04 6B         [24] 1199 	mov	dptr,#_flash_write_PARM_2
      004D40 74 FE            [12] 1200 	mov	a,#0xfe
      004D42 F0               [24] 1201 	movx	@dptr,a
      004D43 74 4A            [12] 1202 	mov	a,#0x4a
      004D45 A3               [24] 1203 	inc	dptr
      004D46 F0               [24] 1204 	movx	@dptr,a
      004D47 90 0C 52         [24] 1205 	mov	dptr,#0x0c52
      004D4A 12 6B 66         [24] 1206 	lcall	_flash_write
                                   1207 ;	..\src\peripherals\GoldSequence.c:138: flash_write(MEM_GOLD_SEQUENCES_START+0x0054,0b1111001011110101);
      004D4D 90 04 6B         [24] 1208 	mov	dptr,#_flash_write_PARM_2
      004D50 74 F5            [12] 1209 	mov	a,#0xf5
      004D52 F0               [24] 1210 	movx	@dptr,a
      004D53 74 F2            [12] 1211 	mov	a,#0xf2
      004D55 A3               [24] 1212 	inc	dptr
      004D56 F0               [24] 1213 	movx	@dptr,a
      004D57 90 0C 54         [24] 1214 	mov	dptr,#0x0c54
      004D5A 12 6B 66         [24] 1215 	lcall	_flash_write
                                   1216 ;	..\src\peripherals\GoldSequence.c:140: flash_write(MEM_GOLD_SEQUENCES_START+0x0056,0b111101001001011);
      004D5D 90 04 6B         [24] 1217 	mov	dptr,#_flash_write_PARM_2
      004D60 74 4B            [12] 1218 	mov	a,#0x4b
      004D62 F0               [24] 1219 	movx	@dptr,a
      004D63 74 7A            [12] 1220 	mov	a,#0x7a
      004D65 A3               [24] 1221 	inc	dptr
      004D66 F0               [24] 1222 	movx	@dptr,a
      004D67 90 0C 56         [24] 1223 	mov	dptr,#0x0c56
      004D6A 12 6B 66         [24] 1224 	lcall	_flash_write
                                   1225 ;	..\src\peripherals\GoldSequence.c:142: flash_write(MEM_GOLD_SEQUENCES_START+0x0058,0b0111110000001100);
      004D6D 90 04 6B         [24] 1226 	mov	dptr,#_flash_write_PARM_2
      004D70 74 0C            [12] 1227 	mov	a,#0x0c
      004D72 F0               [24] 1228 	movx	@dptr,a
      004D73 74 7C            [12] 1229 	mov	a,#0x7c
      004D75 A3               [24] 1230 	inc	dptr
      004D76 F0               [24] 1231 	movx	@dptr,a
      004D77 90 0C 58         [24] 1232 	mov	dptr,#0x0c58
      004D7A 12 6B 66         [24] 1233 	lcall	_flash_write
                                   1234 ;	..\src\peripherals\GoldSequence.c:144: flash_write(MEM_GOLD_SEQUENCES_START+0x005A,0b110001000010001);
      004D7D 90 04 6B         [24] 1235 	mov	dptr,#_flash_write_PARM_2
      004D80 74 11            [12] 1236 	mov	a,#0x11
      004D82 F0               [24] 1237 	movx	@dptr,a
      004D83 74 62            [12] 1238 	mov	a,#0x62
      004D85 A3               [24] 1239 	inc	dptr
      004D86 F0               [24] 1240 	movx	@dptr,a
      004D87 90 0C 5A         [24] 1241 	mov	dptr,#0x0c5a
      004D8A 12 6B 66         [24] 1242 	lcall	_flash_write
                                   1243 ;	..\src\peripherals\GoldSequence.c:147: flash_write(MEM_GOLD_SEQUENCES_START+0x005C,0b0011101101110000);
      004D8D 90 04 6B         [24] 1244 	mov	dptr,#_flash_write_PARM_2
      004D90 74 70            [12] 1245 	mov	a,#0x70
      004D92 F0               [24] 1246 	movx	@dptr,a
      004D93 74 3B            [12] 1247 	mov	a,#0x3b
      004D95 A3               [24] 1248 	inc	dptr
      004D96 F0               [24] 1249 	movx	@dptr,a
      004D97 90 0C 5C         [24] 1250 	mov	dptr,#0x0c5c
      004D9A 12 6B 66         [24] 1251 	lcall	_flash_write
                                   1252 ;	..\src\peripherals\GoldSequence.c:149: flash_write(MEM_GOLD_SEQUENCES_START+0x005E,0b010111000111100);
      004D9D 90 04 6B         [24] 1253 	mov	dptr,#_flash_write_PARM_2
      004DA0 74 3C            [12] 1254 	mov	a,#0x3c
      004DA2 F0               [24] 1255 	movx	@dptr,a
      004DA3 74 2E            [12] 1256 	mov	a,#0x2e
      004DA5 A3               [24] 1257 	inc	dptr
      004DA6 F0               [24] 1258 	movx	@dptr,a
      004DA7 90 0C 5E         [24] 1259 	mov	dptr,#0x0c5e
      004DAA 12 6B 66         [24] 1260 	lcall	_flash_write
                                   1261 ;	..\src\peripherals\GoldSequence.c:152: flash_write(MEM_GOLD_SEQUENCES_START+0x0060,0b1001100011001110);
      004DAD 90 04 6B         [24] 1262 	mov	dptr,#_flash_write_PARM_2
      004DB0 74 CE            [12] 1263 	mov	a,#0xce
      004DB2 F0               [24] 1264 	movx	@dptr,a
      004DB3 74 98            [12] 1265 	mov	a,#0x98
      004DB5 A3               [24] 1266 	inc	dptr
      004DB6 F0               [24] 1267 	movx	@dptr,a
      004DB7 90 0C 60         [24] 1268 	mov	dptr,#0x0c60
      004DBA 12 6B 66         [24] 1269 	lcall	_flash_write
                                   1270 ;	..\src\peripherals\GoldSequence.c:154: flash_write(MEM_GOLD_SEQUENCES_START+0x0062,0b000100000101010);
      004DBD 90 04 6B         [24] 1271 	mov	dptr,#_flash_write_PARM_2
      004DC0 74 2A            [12] 1272 	mov	a,#0x2a
      004DC2 F0               [24] 1273 	movx	@dptr,a
      004DC3 74 08            [12] 1274 	mov	a,#0x08
      004DC5 A3               [24] 1275 	inc	dptr
      004DC6 F0               [24] 1276 	movx	@dptr,a
      004DC7 90 0C 62         [24] 1277 	mov	dptr,#0x0c62
      004DCA 12 6B 66         [24] 1278 	lcall	_flash_write
                                   1279 ;	..\src\peripherals\GoldSequence.c:157: flash_write(MEM_GOLD_SEQUENCES_START+0x0064,0b1100100100010001);
      004DCD 90 04 6B         [24] 1280 	mov	dptr,#_flash_write_PARM_2
      004DD0 74 11            [12] 1281 	mov	a,#0x11
      004DD2 F0               [24] 1282 	movx	@dptr,a
      004DD3 74 C9            [12] 1283 	mov	a,#0xc9
      004DD5 A3               [24] 1284 	inc	dptr
      004DD6 F0               [24] 1285 	movx	@dptr,a
      004DD7 90 0C 64         [24] 1286 	mov	dptr,#0x0c64
      004DDA 12 6B 66         [24] 1287 	lcall	_flash_write
                                   1288 ;	..\src\peripherals\GoldSequence.c:159: flash_write(MEM_GOLD_SEQUENCES_START+0x0066,0b001101100100001);
      004DDD 90 04 6B         [24] 1289 	mov	dptr,#_flash_write_PARM_2
      004DE0 74 21            [12] 1290 	mov	a,#0x21
      004DE2 F0               [24] 1291 	movx	@dptr,a
      004DE3 74 1B            [12] 1292 	mov	a,#0x1b
      004DE5 A3               [24] 1293 	inc	dptr
      004DE6 F0               [24] 1294 	movx	@dptr,a
      004DE7 90 0C 66         [24] 1295 	mov	dptr,#0x0c66
      004DEA 12 6B 66         [24] 1296 	lcall	_flash_write
                                   1297 ;	..\src\peripherals\GoldSequence.c:162: flash_write(MEM_GOLD_SEQUENCES_START+0x0068,0b0110000111111110);
      004DED 90 04 6B         [24] 1298 	mov	dptr,#_flash_write_PARM_2
      004DF0 74 FE            [12] 1299 	mov	a,#0xfe
      004DF2 F0               [24] 1300 	movx	@dptr,a
      004DF3 74 61            [12] 1301 	mov	a,#0x61
      004DF5 A3               [24] 1302 	inc	dptr
      004DF6 F0               [24] 1303 	movx	@dptr,a
      004DF7 90 0C 68         [24] 1304 	mov	dptr,#0x0c68
      004DFA 12 6B 66         [24] 1305 	lcall	_flash_write
                                   1306 ;	..\src\peripherals\GoldSequence.c:164: flash_write(MEM_GOLD_SEQUENCES_START+0x006A,0b101001010100100);
      004DFD 90 04 6B         [24] 1307 	mov	dptr,#_flash_write_PARM_2
      004E00 74 A4            [12] 1308 	mov	a,#0xa4
      004E02 F0               [24] 1309 	movx	@dptr,a
      004E03 03               [12] 1310 	rr	a
      004E04 A3               [24] 1311 	inc	dptr
      004E05 F0               [24] 1312 	movx	@dptr,a
      004E06 90 0C 6A         [24] 1313 	mov	dptr,#0x0c6a
      004E09 12 6B 66         [24] 1314 	lcall	_flash_write
                                   1315 ;	..\src\peripherals\GoldSequence.c:167: flash_write(MEM_GOLD_SEQUENCES_START+0x006C,0b1011010110001001);
      004E0C 90 04 6B         [24] 1316 	mov	dptr,#_flash_write_PARM_2
      004E0F 74 89            [12] 1317 	mov	a,#0x89
      004E11 F0               [24] 1318 	movx	@dptr,a
      004E12 74 B5            [12] 1319 	mov	a,#0xb5
      004E14 A3               [24] 1320 	inc	dptr
      004E15 F0               [24] 1321 	movx	@dptr,a
      004E16 90 0C 6C         [24] 1322 	mov	dptr,#0x0c6c
      004E19 12 6B 66         [24] 1323 	lcall	_flash_write
                                   1324 ;	..\src\peripherals\GoldSequence.c:169: flash_write(MEM_GOLD_SEQUENCES_START+0x006E,0b011011001100110);
      004E1C 90 04 6B         [24] 1325 	mov	dptr,#_flash_write_PARM_2
      004E1F 74 66            [12] 1326 	mov	a,#0x66
      004E21 F0               [24] 1327 	movx	@dptr,a
      004E22 74 36            [12] 1328 	mov	a,#0x36
      004E24 A3               [24] 1329 	inc	dptr
      004E25 F0               [24] 1330 	movx	@dptr,a
      004E26 90 0C 6E         [24] 1331 	mov	dptr,#0x0c6e
      004E29 12 6B 66         [24] 1332 	lcall	_flash_write
                                   1333 ;	..\src\peripherals\GoldSequence.c:172: flash_write(MEM_GOLD_SEQUENCES_START+0x0070,0b1101111110110010);
      004E2C 90 04 6B         [24] 1334 	mov	dptr,#_flash_write_PARM_2
      004E2F 74 B2            [12] 1335 	mov	a,#0xb2
      004E31 F0               [24] 1336 	movx	@dptr,a
      004E32 74 DF            [12] 1337 	mov	a,#0xdf
      004E34 A3               [24] 1338 	inc	dptr
      004E35 F0               [24] 1339 	movx	@dptr,a
      004E36 90 0C 70         [24] 1340 	mov	dptr,#0x0c70
      004E39 12 6B 66         [24] 1341 	lcall	_flash_write
                                   1342 ;	..\src\peripherals\GoldSequence.c:174: flash_write(MEM_GOLD_SEQUENCES_START+0x0072,0b100010000000111);
      004E3C 90 04 6B         [24] 1343 	mov	dptr,#_flash_write_PARM_2
      004E3F 74 07            [12] 1344 	mov	a,#0x07
      004E41 F0               [24] 1345 	movx	@dptr,a
      004E42 74 44            [12] 1346 	mov	a,#0x44
      004E44 A3               [24] 1347 	inc	dptr
      004E45 F0               [24] 1348 	movx	@dptr,a
      004E46 90 0C 72         [24] 1349 	mov	dptr,#0x0c72
      004E49 12 6B 66         [24] 1350 	lcall	_flash_write
                                   1351 ;	..\src\peripherals\GoldSequence.c:177: flash_write(MEM_GOLD_SEQUENCES_START+0x0074,0b0110101010101111);
      004E4C 90 04 6B         [24] 1352 	mov	dptr,#_flash_write_PARM_2
      004E4F 74 AF            [12] 1353 	mov	a,#0xaf
      004E51 F0               [24] 1354 	movx	@dptr,a
      004E52 74 6A            [12] 1355 	mov	a,#0x6a
      004E54 A3               [24] 1356 	inc	dptr
      004E55 F0               [24] 1357 	movx	@dptr,a
      004E56 90 0C 74         [24] 1358 	mov	dptr,#0x0c74
      004E59 12 6B 66         [24] 1359 	lcall	_flash_write
                                   1360 ;	..\src\peripherals\GoldSequence.c:179: flash_write(MEM_GOLD_SEQUENCES_START+0x0076,0b011110100110111);
      004E5C 90 04 6B         [24] 1361 	mov	dptr,#_flash_write_PARM_2
      004E5F 74 37            [12] 1362 	mov	a,#0x37
      004E61 F0               [24] 1363 	movx	@dptr,a
      004E62 74 3D            [12] 1364 	mov	a,#0x3d
      004E64 A3               [24] 1365 	inc	dptr
      004E65 F0               [24] 1366 	movx	@dptr,a
      004E66 90 0C 76         [24] 1367 	mov	dptr,#0x0c76
      004E69 12 6B 66         [24] 1368 	lcall	_flash_write
                                   1369 ;	..\src\peripherals\GoldSequence.c:182: flash_write(MEM_GOLD_SEQUENCES_START+0x0078,0b0011000000100001);
      004E6C 90 04 6B         [24] 1370 	mov	dptr,#_flash_write_PARM_2
      004E6F 74 21            [12] 1371 	mov	a,#0x21
      004E71 F0               [24] 1372 	movx	@dptr,a
      004E72 74 30            [12] 1373 	mov	a,#0x30
      004E74 A3               [24] 1374 	inc	dptr
      004E75 F0               [24] 1375 	movx	@dptr,a
      004E76 90 0C 78         [24] 1376 	mov	dptr,#0x0c78
      004E79 12 6B 66         [24] 1377 	lcall	_flash_write
                                   1378 ;	..\src\peripherals\GoldSequence.c:184: flash_write(MEM_GOLD_SEQUENCES_START+0x007A,0b100000110101111);
      004E7C 90 04 6B         [24] 1379 	mov	dptr,#_flash_write_PARM_2
      004E7F 74 AF            [12] 1380 	mov	a,#0xaf
      004E81 F0               [24] 1381 	movx	@dptr,a
      004E82 74 41            [12] 1382 	mov	a,#0x41
      004E84 A3               [24] 1383 	inc	dptr
      004E85 F0               [24] 1384 	movx	@dptr,a
      004E86 90 0C 7A         [24] 1385 	mov	dptr,#0x0c7a
      004E89 12 6B 66         [24] 1386 	lcall	_flash_write
                                   1387 ;	..\src\peripherals\GoldSequence.c:187: flash_write(MEM_GOLD_SEQUENCES_START+0x007C,0b0001110101100110);
      004E8C 90 04 6B         [24] 1388 	mov	dptr,#_flash_write_PARM_2
      004E8F 74 66            [12] 1389 	mov	a,#0x66
      004E91 F0               [24] 1390 	movx	@dptr,a
      004E92 74 1D            [12] 1391 	mov	a,#0x1d
      004E94 A3               [24] 1392 	inc	dptr
      004E95 F0               [24] 1393 	movx	@dptr,a
      004E96 90 0C 7C         [24] 1394 	mov	dptr,#0x0c7c
      004E99 12 6B 66         [24] 1395 	lcall	_flash_write
                                   1396 ;	..\src\peripherals\GoldSequence.c:189: flash_write(MEM_GOLD_SEQUENCES_START+0x007E,0b111111111100011);
      004E9C 90 04 6B         [24] 1397 	mov	dptr,#_flash_write_PARM_2
      004E9F 74 E3            [12] 1398 	mov	a,#0xe3
      004EA1 F0               [24] 1399 	movx	@dptr,a
      004EA2 74 7F            [12] 1400 	mov	a,#0x7f
      004EA4 A3               [24] 1401 	inc	dptr
      004EA5 F0               [24] 1402 	movx	@dptr,a
      004EA6 90 0C 7E         [24] 1403 	mov	dptr,#0x0c7e
      004EA9 12 6B 66         [24] 1404 	lcall	_flash_write
                                   1405 ;	..\src\peripherals\GoldSequence.c:192: flash_write(MEM_GOLD_SEQUENCES_START+0x0080,0b0000101111000101);
      004EAC 90 04 6B         [24] 1406 	mov	dptr,#_flash_write_PARM_2
      004EAF 74 C5            [12] 1407 	mov	a,#0xc5
      004EB1 F0               [24] 1408 	movx	@dptr,a
      004EB2 74 0B            [12] 1409 	mov	a,#0x0b
      004EB4 A3               [24] 1410 	inc	dptr
      004EB5 F0               [24] 1411 	movx	@dptr,a
      004EB6 90 0C 80         [24] 1412 	mov	dptr,#0x0c80
      004EB9 12 6B 66         [24] 1413 	lcall	_flash_write
                                   1414 ;	..\src\peripherals\GoldSequence.c:194: flash_write(MEM_GOLD_SEQUENCES_START+0x0082,0b010000011000101);
      004EBC 90 04 6B         [24] 1415 	mov	dptr,#_flash_write_PARM_2
      004EBF 74 C5            [12] 1416 	mov	a,#0xc5
      004EC1 F0               [24] 1417 	movx	@dptr,a
      004EC2 74 20            [12] 1418 	mov	a,#0x20
      004EC4 A3               [24] 1419 	inc	dptr
      004EC5 F0               [24] 1420 	movx	@dptr,a
      004EC6 90 0C 82         [24] 1421 	mov	dptr,#0x0c82
      004EC9 12 6B 66         [24] 1422 	lcall	_flash_write
                                   1423 ;	..\src\peripherals\GoldSequence.c:196: flash_lock();
      004ECC 02 77 0A         [24] 1424 	ljmp	_flash_lock
                                   1425 ;------------------------------------------------------------
                                   1426 ;Allocation info for local variables in function 'GOLDSEQUENCE_Obtain'
                                   1427 ;------------------------------------------------------------
                                   1428 ;Rnd                       Allocated with name '_GOLDSEQUENCE_Obtain_Rnd_1_123'
                                   1429 ;RetVal                    Allocated with name '_GOLDSEQUENCE_Obtain_RetVal_1_123'
                                   1430 ;------------------------------------------------------------
                                   1431 ;	..\src\peripherals\GoldSequence.c:213: __xdata uint32_t GOLDSEQUENCE_Obtain(void)
                                   1432 ;	-----------------------------------------
                                   1433 ;	 function GOLDSEQUENCE_Obtain
                                   1434 ;	-----------------------------------------
      004ECF                       1435 _GOLDSEQUENCE_Obtain:
                                   1436 ;	..\src\peripherals\GoldSequence.c:218: Rnd = random();
      004ECF 12 70 DD         [24] 1437 	lcall	_random
      004ED2 AE 82            [24] 1438 	mov	r6,dpl
      004ED4 AF 83            [24] 1439 	mov	r7,dph
                                   1440 ;	..\src\peripherals\GoldSequence.c:219: flash_unlock();
      004ED6 C0 07            [24] 1441 	push	ar7
      004ED8 C0 06            [24] 1442 	push	ar6
      004EDA 12 6D 2D         [24] 1443 	lcall	_flash_unlock
      004EDD D0 06            [24] 1444 	pop	ar6
      004EDF D0 07            [24] 1445 	pop	ar7
                                   1446 ;	..\src\peripherals\GoldSequence.c:221: Rnd = ((Rnd % 33)*4);  // MOD operation is done so that the number is between 0 and 33, once a number between 0 and 32 has been obtained, its multiplied by 4 to match the memory address
      004EE1 90 04 77         [24] 1447 	mov	dptr,#__moduint_PARM_2
      004EE4 74 21            [12] 1448 	mov	a,#0x21
      004EE6 F0               [24] 1449 	movx	@dptr,a
      004EE7 E4               [12] 1450 	clr	a
      004EE8 A3               [24] 1451 	inc	dptr
      004EE9 F0               [24] 1452 	movx	@dptr,a
      004EEA 8E 82            [24] 1453 	mov	dpl,r6
      004EEC 8F 83            [24] 1454 	mov	dph,r7
      004EEE 12 6F 71         [24] 1455 	lcall	__moduint
      004EF1 AE 82            [24] 1456 	mov	r6,dpl
      004EF3 E5 83            [12] 1457 	mov	a,dph
      004EF5 CE               [12] 1458 	xch	a,r6
      004EF6 25 E0            [12] 1459 	add	a,acc
      004EF8 CE               [12] 1460 	xch	a,r6
      004EF9 33               [12] 1461 	rlc	a
      004EFA CE               [12] 1462 	xch	a,r6
      004EFB 25 E0            [12] 1463 	add	a,acc
      004EFD CE               [12] 1464 	xch	a,r6
      004EFE 33               [12] 1465 	rlc	a
      004EFF FF               [12] 1466 	mov	r7,a
                                   1467 ;	..\src\peripherals\GoldSequence.c:222: RetVal = ((uint32_t)flash_read(Rnd))<<16;
      004F00 8E 82            [24] 1468 	mov	dpl,r6
      004F02 8F 83            [24] 1469 	mov	dph,r7
      004F04 C0 07            [24] 1470 	push	ar7
      004F06 C0 06            [24] 1471 	push	ar6
      004F08 12 72 E9         [24] 1472 	lcall	_flash_read
      004F0B AC 82            [24] 1473 	mov	r4,dpl
      004F0D AD 83            [24] 1474 	mov	r5,dph
      004F0F D0 06            [24] 1475 	pop	ar6
      004F11 D0 07            [24] 1476 	pop	ar7
      004F13 8D 02            [24] 1477 	mov	ar2,r5
      004F15 8C 03            [24] 1478 	mov	ar3,r4
                                   1479 ;	..\src\peripherals\GoldSequence.c:223: RetVal |= (((uint32_t)flash_read(Rnd+2)) & 0x0000FFFF)<<1;
      004F17 E4               [12] 1480 	clr	a
      004F18 FD               [12] 1481 	mov	r5,a
      004F19 FC               [12] 1482 	mov	r4,a
      004F1A 74 02            [12] 1483 	mov	a,#0x02
      004F1C 2E               [12] 1484 	add	a,r6
      004F1D FE               [12] 1485 	mov	r6,a
      004F1E E4               [12] 1486 	clr	a
      004F1F 3F               [12] 1487 	addc	a,r7
      004F20 FF               [12] 1488 	mov	r7,a
      004F21 8E 82            [24] 1489 	mov	dpl,r6
      004F23 8F 83            [24] 1490 	mov	dph,r7
      004F25 C0 05            [24] 1491 	push	ar5
      004F27 C0 04            [24] 1492 	push	ar4
      004F29 C0 03            [24] 1493 	push	ar3
      004F2B C0 02            [24] 1494 	push	ar2
      004F2D 12 72 E9         [24] 1495 	lcall	_flash_read
      004F30 AE 82            [24] 1496 	mov	r6,dpl
      004F32 AF 83            [24] 1497 	mov	r7,dph
      004F34 D0 02            [24] 1498 	pop	ar2
      004F36 D0 03            [24] 1499 	pop	ar3
      004F38 D0 04            [24] 1500 	pop	ar4
      004F3A D0 05            [24] 1501 	pop	ar5
      004F3C 8E 00            [24] 1502 	mov	ar0,r6
      004F3E 8F 01            [24] 1503 	mov	ar1,r7
      004F40 E4               [12] 1504 	clr	a
      004F41 FE               [12] 1505 	mov	r6,a
      004F42 FF               [12] 1506 	mov	r7,a
      004F43 E8               [12] 1507 	mov	a,r0
      004F44 28               [12] 1508 	add	a,r0
      004F45 F8               [12] 1509 	mov	r0,a
      004F46 E9               [12] 1510 	mov	a,r1
      004F47 33               [12] 1511 	rlc	a
      004F48 F9               [12] 1512 	mov	r1,a
      004F49 EE               [12] 1513 	mov	a,r6
      004F4A 33               [12] 1514 	rlc	a
      004F4B FE               [12] 1515 	mov	r6,a
      004F4C EF               [12] 1516 	mov	a,r7
      004F4D 33               [12] 1517 	rlc	a
      004F4E FF               [12] 1518 	mov	r7,a
      004F4F E8               [12] 1519 	mov	a,r0
      004F50 42 04            [12] 1520 	orl	ar4,a
      004F52 E9               [12] 1521 	mov	a,r1
      004F53 42 05            [12] 1522 	orl	ar5,a
      004F55 EE               [12] 1523 	mov	a,r6
      004F56 42 03            [12] 1524 	orl	ar3,a
      004F58 EF               [12] 1525 	mov	a,r7
      004F59 42 02            [12] 1526 	orl	ar2,a
                                   1527 ;	..\src\peripherals\GoldSequence.c:225: flash_lock();
      004F5B C0 05            [24] 1528 	push	ar5
      004F5D C0 04            [24] 1529 	push	ar4
      004F5F C0 03            [24] 1530 	push	ar3
      004F61 C0 02            [24] 1531 	push	ar2
      004F63 12 77 0A         [24] 1532 	lcall	_flash_lock
      004F66 D0 02            [24] 1533 	pop	ar2
      004F68 D0 03            [24] 1534 	pop	ar3
      004F6A D0 04            [24] 1535 	pop	ar4
      004F6C D0 05            [24] 1536 	pop	ar5
                                   1537 ;	..\src\peripherals\GoldSequence.c:226: return(RetVal);
      004F6E 8C 82            [24] 1538 	mov	dpl,r4
      004F70 8D 83            [24] 1539 	mov	dph,r5
      004F72 8B F0            [24] 1540 	mov	b,r3
      004F74 EA               [12] 1541 	mov	a,r2
      004F75 22               [24] 1542 	ret
                                   1543 	.area CSEG    (CODE)
                                   1544 	.area CONST   (CODE)
                                   1545 	.area XINIT   (CODE)
                                   1546 	.area CABS    (ABS,CODE)
