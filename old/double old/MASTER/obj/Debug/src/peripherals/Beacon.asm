;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.6.0 #9615 (MINGW64)
;--------------------------------------------------------
	.module Beacon
	.optsdcc -mmcs51 --model-large
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _memcpy
	.globl _BEACON_decoding_PARM_3
	.globl _BEACON_decoding_PARM_2
	.globl _BEACON_decoding
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
;--------------------------------------------------------
; special function bits
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
;--------------------------------------------------------
; overlayable register banks
;--------------------------------------------------------
	.area REG_BANK_0	(REL,OVR,DATA)
	.ds 8
;--------------------------------------------------------
; internal ram data
;--------------------------------------------------------
	.area DSEG    (DATA)
_BEACON_decoding_sloc0_1_0:
	.ds 2
_BEACON_decoding_sloc1_1_0:
	.ds 2
_BEACON_decoding_sloc2_1_0:
	.ds 3
_BEACON_decoding_sloc3_1_0:
	.ds 3
_BEACON_decoding_sloc4_1_0:
	.ds 2
_BEACON_decoding_sloc5_1_0:
	.ds 4
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;--------------------------------------------------------
; indirectly addressable internal ram data
;--------------------------------------------------------
	.area ISEG    (DATA)
;--------------------------------------------------------
; absolute internal ram data
;--------------------------------------------------------
	.area IABS    (ABS,DATA)
	.area IABS    (ABS,DATA)
;--------------------------------------------------------
; bit data
;--------------------------------------------------------
	.area BSEG    (BIT)
;--------------------------------------------------------
; paged external ram data
;--------------------------------------------------------
	.area PSEG    (PAG,XDATA)
;--------------------------------------------------------
; external ram data
;--------------------------------------------------------
	.area XSEG    (XDATA)
_BEACON_decoding_PARM_2:
	.ds 2
_BEACON_decoding_PARM_3:
	.ds 1
_BEACON_decoding_Rxbuffer_1_79:
	.ds 3
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area XABS    (ABS,XDATA)
;--------------------------------------------------------
; external initialized ram data
;--------------------------------------------------------
	.area XISEG   (XDATA)
	.area HOME    (CODE)
	.area GSINIT0 (CODE)
	.area GSINIT1 (CODE)
	.area GSINIT2 (CODE)
	.area GSINIT3 (CODE)
	.area GSINIT4 (CODE)
	.area GSINIT5 (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area CSEG    (CODE)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME    (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area GSINIT  (CODE)
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME    (CODE)
	.area HOME    (CODE)
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CSEG    (CODE)
;------------------------------------------------------------
;Allocation info for local variables in function 'BEACON_decoding'
;------------------------------------------------------------
;sloc0                     Allocated with name '_BEACON_decoding_sloc0_1_0'
;sloc1                     Allocated with name '_BEACON_decoding_sloc1_1_0'
;sloc2                     Allocated with name '_BEACON_decoding_sloc2_1_0'
;sloc3                     Allocated with name '_BEACON_decoding_sloc3_1_0'
;sloc4                     Allocated with name '_BEACON_decoding_sloc4_1_0'
;sloc5                     Allocated with name '_BEACON_decoding_sloc5_1_0'
;beacon                    Allocated with name '_BEACON_decoding_PARM_2'
;length                    Allocated with name '_BEACON_decoding_PARM_3'
;Rxbuffer                  Allocated with name '_BEACON_decoding_Rxbuffer_1_79'
;i                         Allocated with name '_BEACON_decoding_i_1_80'
;------------------------------------------------------------
;	..\src\peripherals\Beacon.c:29: __xdata void BEACON_decoding(uint8_t* Rxbuffer, RF_beacon_t* beacon, uint8_t length)
;	-----------------------------------------
;	 function BEACON_decoding
;	-----------------------------------------
_BEACON_decoding:
	ar7 = 0x07
	ar6 = 0x06
	ar5 = 0x05
	ar4 = 0x04
	ar3 = 0x03
	ar2 = 0x02
	ar1 = 0x01
	ar0 = 0x00
	mov	r7,b
	mov	r6,dph
	mov	a,dpl
	mov	dptr,#_BEACON_decoding_Rxbuffer_1_79
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:32: beacon->FlagReceived = VALID_BEACON;
	mov	dptr,#_BEACON_decoding_PARM_2
	movx	a,@dptr
	mov	_BEACON_decoding_sloc4_1_0,a
	inc	dptr
	movx	a,@dptr
	mov	(_BEACON_decoding_sloc4_1_0 + 1),a
	mov	dpl,_BEACON_decoding_sloc4_1_0
	mov	dph,(_BEACON_decoding_sloc4_1_0 + 1)
	mov	a,#0x01
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:33: if(length == 23)
	mov	dptr,#_BEACON_decoding_PARM_3
	movx	a,@dptr
	mov	r5,a
	cjne	r5,#0x17,00121$
	sjmp	00122$
00121$:
	ljmp	00108$
00122$:
;	..\src\peripherals\Beacon.c:35: beacon->SatId = *Rxbuffer <<8 | *(Rxbuffer+1);
	mov	a,#0x01
	add	a,_BEACON_decoding_sloc4_1_0
	mov	_BEACON_decoding_sloc0_1_0,a
	clr	a
	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
	mov	(_BEACON_decoding_sloc0_1_0 + 1),a
	mov	dptr,#_BEACON_decoding_Rxbuffer_1_79
	movx	a,@dptr
	mov	r1,a
	inc	dptr
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	mov	dpl,r1
	mov	dph,r2
	mov	b,r3
	lcall	__gptrget
	mov	r0,a
	mov	r5,#0x00
	mov	(_BEACON_decoding_sloc1_1_0 + 1),r0
;	1-genFromRTrack replaced	mov	_BEACON_decoding_sloc1_1_0,#0x00
	mov	_BEACON_decoding_sloc1_1_0,r5
	mov	a,#0x01
	add	a,r1
	mov	r0,a
	clr	a
	addc	a,r2
	mov	r4,a
	mov	ar5,r3
	mov	dpl,r0
	mov	dph,r4
	mov	b,r5
	lcall	__gptrget
	mov	r0,a
	mov	r5,#0x00
	mov	a,_BEACON_decoding_sloc1_1_0
	orl	ar0,a
	mov	a,(_BEACON_decoding_sloc1_1_0 + 1)
	orl	ar5,a
	mov	dpl,_BEACON_decoding_sloc0_1_0
	mov	dph,(_BEACON_decoding_sloc0_1_0 + 1)
	mov	a,r0
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:36: beacon->BeaconID = (*(Rxbuffer+POS_BEACONID) & MASK_BEACONID)>> 4;
	mov	a,#0x03
	add	a,_BEACON_decoding_sloc4_1_0
	mov	r4,a
	clr	a
	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
	mov	r5,a
	mov	a,#0x02
	add	a,r1
	mov	r0,a
	clr	a
	addc	a,r2
	mov	r6,a
	mov	ar7,r3
	mov	dpl,r0
	mov	dph,r6
	mov	b,r7
	lcall	__gptrget
	anl	a,#0xf0
	swap	a
	anl	a,#0x0f
	mov	r0,a
	mov	dpl,r4
	mov	dph,r5
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:39: if(beacon->BeaconID == UPLINK_BEACON)
	mov	dpl,r4
	mov	dph,r5
	movx	a,@dptr
	mov	_BEACON_decoding_sloc1_1_0,a
	cjne	r0,#0x0c,00105$
;	..\src\peripherals\Beacon.c:41: beacon->NumPremSlots = *(Rxbuffer+POS_NUMPREM);
	mov	a,#0x04
	add	a,_BEACON_decoding_sloc4_1_0
	mov	_BEACON_decoding_sloc0_1_0,a
	clr	a
	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
	mov	(_BEACON_decoding_sloc0_1_0 + 1),a
	mov	a,#0x03
	add	a,r1
	mov	r0,a
	clr	a
	addc	a,r2
	mov	r4,a
	mov	ar5,r3
	mov	dpl,r0
	mov	dph,r4
	mov	b,r5
	lcall	__gptrget
	mov	r0,a
	mov	dpl,_BEACON_decoding_sloc0_1_0
	mov	dph,(_BEACON_decoding_sloc0_1_0 + 1)
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:42: beacon->NumACK = 0xFF; //invalid data
	mov	dpl,_BEACON_decoding_sloc4_1_0
	mov	dph,(_BEACON_decoding_sloc4_1_0 + 1)
	inc	dptr
	inc	dptr
	inc	dptr
	inc	dptr
	inc	dptr
	mov	a,#0xff
	movx	@dptr,a
	sjmp	00106$
00105$:
;	..\src\peripherals\Beacon.c:43: }else if(beacon->BeaconID == DOWNLINK_BEACON)
	mov	a,#0x0a
	cjne	a,_BEACON_decoding_sloc1_1_0,00102$
;	..\src\peripherals\Beacon.c:45: beacon->NumACK = *(Rxbuffer+POS_NUMPREM);
	mov	a,#0x05
	add	a,_BEACON_decoding_sloc4_1_0
	mov	_BEACON_decoding_sloc1_1_0,a
	clr	a
	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
	mov	(_BEACON_decoding_sloc1_1_0 + 1),a
	mov	a,#0x03
	add	a,r1
	mov	r0,a
	clr	a
	addc	a,r2
	mov	r4,a
	mov	ar5,r3
	mov	dpl,r0
	mov	dph,r4
	mov	b,r5
	lcall	__gptrget
	mov	r0,a
	mov	dpl,_BEACON_decoding_sloc1_1_0
	mov	dph,(_BEACON_decoding_sloc1_1_0 + 1)
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:46: beacon->NumPremSlots = 0xFF; //invalid data
	mov	dpl,_BEACON_decoding_sloc4_1_0
	mov	dph,(_BEACON_decoding_sloc4_1_0 + 1)
	inc	dptr
	inc	dptr
	inc	dptr
	inc	dptr
	mov	a,#0xff
	movx	@dptr,a
	sjmp	00106$
00102$:
;	..\src\peripherals\Beacon.c:49: else beacon->FlagReceived = INVALID_BEACON;
	mov	dpl,_BEACON_decoding_sloc4_1_0
	mov	dph,(_BEACON_decoding_sloc4_1_0 + 1)
	clr	a
	movx	@dptr,a
00106$:
;	..\src\peripherals\Beacon.c:51: beacon->HMAC_flag =(*(Rxbuffer+POS_SECURITY) & MASK_HMAC_FLAG)>>7;
	mov	a,#0x06
	add	a,_BEACON_decoding_sloc4_1_0
	mov	_BEACON_decoding_sloc1_1_0,a
	clr	a
	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
	mov	(_BEACON_decoding_sloc1_1_0 + 1),a
	mov	a,#0x04
	add	a,r1
	mov	_BEACON_decoding_sloc2_1_0,a
	clr	a
	addc	a,r2
	mov	(_BEACON_decoding_sloc2_1_0 + 1),a
	mov	(_BEACON_decoding_sloc2_1_0 + 2),r3
	mov	dpl,_BEACON_decoding_sloc2_1_0
	mov	dph,(_BEACON_decoding_sloc2_1_0 + 1)
	mov	b,(_BEACON_decoding_sloc2_1_0 + 2)
	lcall	__gptrget
	anl	a,#0x80
	rl	a
	anl	a,#0x01
	mov	dpl,_BEACON_decoding_sloc1_1_0
	mov	dph,(_BEACON_decoding_sloc1_1_0 + 1)
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:52: beacon->HMAC_type = (*(Rxbuffer+POS_SECURITY) & MASK_HMAC_TYPE)>>4;
	mov	a,#0x07
	add	a,_BEACON_decoding_sloc4_1_0
	mov	r4,a
	clr	a
	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
	mov	r5,a
	mov	dpl,_BEACON_decoding_sloc2_1_0
	mov	dph,(_BEACON_decoding_sloc2_1_0 + 1)
	mov	b,(_BEACON_decoding_sloc2_1_0 + 2)
	lcall	__gptrget
	anl	a,#0x70
	swap	a
	anl	a,#0x0f
	mov	dpl,r4
	mov	dph,r5
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:53: beacon->Encryption_flag = (*(Rxbuffer+POS_SECURITY) & MASK_ENCRYPTION_FLAG)>>3;
	mov	a,#0x08
	add	a,_BEACON_decoding_sloc4_1_0
	mov	r4,a
	clr	a
	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
	mov	r5,a
	mov	dpl,_BEACON_decoding_sloc2_1_0
	mov	dph,(_BEACON_decoding_sloc2_1_0 + 1)
	mov	b,(_BEACON_decoding_sloc2_1_0 + 2)
	lcall	__gptrget
	anl	a,#0x08
	swap	a
	rl	a
	anl	a,#0x1f
	mov	dpl,r4
	mov	dph,r5
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:54: beacon->Encryption_type = *(Rxbuffer+POS_SECURITY) & MASK_ENCRYPTION_TYPE;
	mov	a,#0x09
	add	a,_BEACON_decoding_sloc4_1_0
	mov	r4,a
	clr	a
	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
	mov	r5,a
	mov	dpl,_BEACON_decoding_sloc2_1_0
	mov	dph,(_BEACON_decoding_sloc2_1_0 + 1)
	mov	b,(_BEACON_decoding_sloc2_1_0 + 2)
	lcall	__gptrget
	mov	r0,a
	anl	ar0,#0x07
	mov	dpl,r4
	mov	dph,r5
	mov	a,r0
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:56: *(Rxbuffer+POS_NONCE)   = *(Rxbuffer+POS_NONCE) + *(Rxbuffer+POS_NONCE+1);
	mov	a,#0x05
	add	a,r1
	mov	_BEACON_decoding_sloc2_1_0,a
	clr	a
	addc	a,r2
	mov	(_BEACON_decoding_sloc2_1_0 + 1),a
	mov	(_BEACON_decoding_sloc2_1_0 + 2),r3
	mov	dpl,_BEACON_decoding_sloc2_1_0
	mov	dph,(_BEACON_decoding_sloc2_1_0 + 1)
	mov	b,(_BEACON_decoding_sloc2_1_0 + 2)
	lcall	__gptrget
	mov	_BEACON_decoding_sloc1_1_0,a
	mov	a,#0x06
	add	a,r1
	mov	_BEACON_decoding_sloc3_1_0,a
	clr	a
	addc	a,r2
	mov	(_BEACON_decoding_sloc3_1_0 + 1),a
	mov	(_BEACON_decoding_sloc3_1_0 + 2),r3
	mov	dpl,_BEACON_decoding_sloc3_1_0
	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
	mov	b,(_BEACON_decoding_sloc3_1_0 + 2)
	lcall	__gptrget
	add	a,_BEACON_decoding_sloc1_1_0
	mov	r5,a
	mov	dpl,_BEACON_decoding_sloc2_1_0
	mov	dph,(_BEACON_decoding_sloc2_1_0 + 1)
	mov	b,(_BEACON_decoding_sloc2_1_0 + 2)
	lcall	__gptrput
;	..\src\peripherals\Beacon.c:57: *(Rxbuffer+POS_NONCE+1) = *(Rxbuffer+POS_NONCE) - *(Rxbuffer+POS_NONCE+1);
	mov	dpl,_BEACON_decoding_sloc3_1_0
	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
	mov	b,(_BEACON_decoding_sloc3_1_0 + 2)
	lcall	__gptrget
	mov	r4,a
	mov	a,r5
	clr	c
	subb	a,r4
	mov	r4,a
	mov	dpl,_BEACON_decoding_sloc3_1_0
	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
	mov	b,(_BEACON_decoding_sloc3_1_0 + 2)
	lcall	__gptrput
;	..\src\peripherals\Beacon.c:58: *(Rxbuffer+POS_NONCE)   = *(Rxbuffer+POS_NONCE) - *(Rxbuffer+POS_NONCE+1);
	mov	a,r5
	clr	c
	subb	a,r4
	mov	dpl,_BEACON_decoding_sloc2_1_0
	mov	dph,(_BEACON_decoding_sloc2_1_0 + 1)
	mov	b,(_BEACON_decoding_sloc2_1_0 + 2)
	lcall	__gptrput
;	..\src\peripherals\Beacon.c:60: memcpy(&beacon->Nonce,Rxbuffer+POS_NONCE,sizeof(uint16_t));
	mov	a,#0x0a
	add	a,_BEACON_decoding_sloc4_1_0
	mov	r4,a
	clr	a
	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
	mov	r5,a
	mov	_BEACON_decoding_sloc3_1_0,r4
	mov	(_BEACON_decoding_sloc3_1_0 + 1),r5
	mov	(_BEACON_decoding_sloc3_1_0 + 2),#0x00
	mov	r0,_BEACON_decoding_sloc2_1_0
	mov	r4,(_BEACON_decoding_sloc2_1_0 + 1)
	mov	r5,(_BEACON_decoding_sloc2_1_0 + 2)
	mov	dptr,#_memcpy_PARM_2
	mov	a,r0
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_memcpy_PARM_3
	mov	a,#0x02
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
	mov	dpl,_BEACON_decoding_sloc3_1_0
	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
	mov	b,(_BEACON_decoding_sloc3_1_0 + 2)
	push	ar3
	push	ar2
	push	ar1
	lcall	_memcpy
	pop	ar1
	pop	ar2
	pop	ar3
;	..\src\peripherals\Beacon.c:62: memcpy(&beacon->reserved,Rxbuffer+POS_RESERVED,sizeof(uint8_t)*5);
	mov	a,#0x0c
	add	a,_BEACON_decoding_sloc4_1_0
	mov	r4,a
	clr	a
	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
	mov	r5,a
	mov	_BEACON_decoding_sloc3_1_0,r4
	mov	(_BEACON_decoding_sloc3_1_0 + 1),r5
	mov	(_BEACON_decoding_sloc3_1_0 + 2),#0x00
	mov	a,#0x07
	add	a,r1
	mov	r0,a
	clr	a
	addc	a,r2
	mov	r4,a
	mov	ar5,r3
	mov	dptr,#_memcpy_PARM_2
	mov	a,r0
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_memcpy_PARM_3
	mov	a,#0x05
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
	mov	dpl,_BEACON_decoding_sloc3_1_0
	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
	mov	b,(_BEACON_decoding_sloc3_1_0 + 2)
	push	ar3
	push	ar2
	push	ar1
	lcall	_memcpy
	pop	ar1
	pop	ar2
	pop	ar3
;	..\src\peripherals\Beacon.c:64: beacon->CRC =  *(Rxbuffer+POS_CRC) <<8 | *(Rxbuffer+POS_CRC+1);
	mov	a,#0x11
	add	a,_BEACON_decoding_sloc4_1_0
	mov	_BEACON_decoding_sloc3_1_0,a
	clr	a
	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
	mov	(_BEACON_decoding_sloc3_1_0 + 1),a
	mov	a,#0x0c
	add	a,r1
	mov	r0,a
	clr	a
	addc	a,r2
	mov	r4,a
	mov	ar5,r3
	mov	dpl,r0
	mov	dph,r4
	mov	b,r5
	lcall	__gptrget
	mov	r0,a
	mov	r5,#0x00
	mov	(_BEACON_decoding_sloc2_1_0 + 1),r0
;	1-genFromRTrack replaced	mov	_BEACON_decoding_sloc2_1_0,#0x00
	mov	_BEACON_decoding_sloc2_1_0,r5
	mov	a,#0x0d
	add	a,r1
	mov	r0,a
	clr	a
	addc	a,r2
	mov	r4,a
	mov	ar5,r3
	mov	dpl,r0
	mov	dph,r4
	mov	b,r5
	lcall	__gptrget
	mov	r5,a
	mov	r0,#0x00
	mov	a,_BEACON_decoding_sloc2_1_0
	orl	ar5,a
	mov	a,(_BEACON_decoding_sloc2_1_0 + 1)
	orl	ar0,a
	mov	ar4,r5
	mov	a,r0
	rlc	a
	subb	a,acc
	mov	r5,a
	mov	r7,a
	mov	dpl,_BEACON_decoding_sloc3_1_0
	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
	mov	a,r4
	movx	@dptr,a
	mov	a,r0
	inc	dptr
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:65: beacon->CRC = beacon->CRC << 16;
	mov	(_BEACON_decoding_sloc5_1_0 + 3),r0
	mov	(_BEACON_decoding_sloc5_1_0 + 2),r4
	mov	(_BEACON_decoding_sloc5_1_0 + 1),#0x00
	mov	_BEACON_decoding_sloc5_1_0,#0x00
	mov	dpl,_BEACON_decoding_sloc3_1_0
	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
	mov	a,_BEACON_decoding_sloc5_1_0
	movx	@dptr,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
	inc	dptr
	movx	@dptr,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 2)
	inc	dptr
	movx	@dptr,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 3)
	inc	dptr
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:66: beacon->CRC = beacon->CRC | *(Rxbuffer+POS_CRC+2)<<8 | *(Rxbuffer+POS_CRC+3);
	mov	a,#0x0e
	add	a,r1
	mov	r5,a
	clr	a
	addc	a,r2
	mov	r6,a
	mov	ar7,r3
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	lcall	__gptrget
	mov	r7,a
	mov	r5,#0x00
	rlc	a
	subb	a,acc
	mov	r6,a
	mov	r4,a
	mov	a,r5
	orl	_BEACON_decoding_sloc5_1_0,a
	mov	a,r7
	orl	(_BEACON_decoding_sloc5_1_0 + 1),a
	mov	a,r6
	orl	(_BEACON_decoding_sloc5_1_0 + 2),a
	mov	a,r4
	orl	(_BEACON_decoding_sloc5_1_0 + 3),a
	mov	a,#0x0f
	add	a,r1
	mov	r0,a
	clr	a
	addc	a,r2
	mov	r6,a
	mov	ar7,r3
	mov	dpl,r0
	mov	dph,r6
	mov	b,r7
	lcall	__gptrget
	mov	r0,a
	clr	a
	mov	r7,a
	mov	r6,a
	mov	r5,a
	mov	a,_BEACON_decoding_sloc5_1_0
	orl	ar0,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
	orl	ar7,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 2)
	orl	ar6,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 3)
	orl	ar5,a
	mov	dpl,_BEACON_decoding_sloc3_1_0
	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
	mov	a,r0
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:68: beacon->HMAC_Msb = *(Rxbuffer+POS_HMAC_MSB)<<8 | *(Rxbuffer+POS_HMAC_MSB+1);
	mov	a,#0x15
	add	a,_BEACON_decoding_sloc4_1_0
	mov	_BEACON_decoding_sloc3_1_0,a
	clr	a
	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
	mov	(_BEACON_decoding_sloc3_1_0 + 1),a
	mov	a,#0x10
	add	a,r1
	mov	r0,a
	clr	a
	addc	a,r2
	mov	r4,a
	mov	ar5,r3
	mov	dpl,r0
	mov	dph,r4
	mov	b,r5
	lcall	__gptrget
	mov	r0,a
	mov	r5,#0x00
	mov	(_BEACON_decoding_sloc5_1_0 + 1),r0
;	1-genFromRTrack replaced	mov	_BEACON_decoding_sloc5_1_0,#0x00
	mov	_BEACON_decoding_sloc5_1_0,r5
	mov	a,#0x11
	add	a,r1
	mov	r0,a
	clr	a
	addc	a,r2
	mov	r4,a
	mov	ar5,r3
	mov	dpl,r0
	mov	dph,r4
	mov	b,r5
	lcall	__gptrget
	mov	r0,a
	mov	r5,#0x00
	mov	a,_BEACON_decoding_sloc5_1_0
	orl	ar0,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
	orl	ar5,a
	mov	a,r5
	mov	r4,a
	rlc	a
	subb	a,acc
	mov	r5,a
	mov	r7,a
	mov	dpl,_BEACON_decoding_sloc3_1_0
	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
	mov	a,r0
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:69: beacon->HMAC_Msb = beacon->HMAC_Msb << 16;
	mov	(_BEACON_decoding_sloc5_1_0 + 3),r4
	mov	(_BEACON_decoding_sloc5_1_0 + 2),r0
	mov	(_BEACON_decoding_sloc5_1_0 + 1),#0x00
	mov	_BEACON_decoding_sloc5_1_0,#0x00
	mov	dpl,_BEACON_decoding_sloc3_1_0
	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
	mov	a,_BEACON_decoding_sloc5_1_0
	movx	@dptr,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
	inc	dptr
	movx	@dptr,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 2)
	inc	dptr
	movx	@dptr,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 3)
	inc	dptr
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:70: beacon->HMAC_Msb = beacon->HMAC_Msb | (*(Rxbuffer+POS_HMAC_MSB+2)<<8 | *(Rxbuffer+POS_HMAC_MSB+3) & 0x0000FFFF);
	mov	a,#0x12
	add	a,r1
	mov	r5,a
	clr	a
	addc	a,r2
	mov	r6,a
	mov	ar7,r3
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	lcall	__gptrget
	mov	r7,a
	mov	r5,#0x00
	mov	a,#0x13
	add	a,r1
	mov	r0,a
	clr	a
	addc	a,r2
	mov	r4,a
	mov	ar6,r3
	mov	dpl,r0
	mov	dph,r4
	mov	b,r6
	lcall	__gptrget
	mov	r6,a
	mov	r0,#0x00
	mov	a,r5
	orl	ar6,a
	mov	a,r7
	orl	ar0,a
	clr	a
	mov	r7,a
	mov	r5,a
	mov	a,_BEACON_decoding_sloc5_1_0
	orl	ar6,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
	orl	ar0,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 2)
	orl	ar7,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 3)
	orl	ar5,a
	mov	dpl,_BEACON_decoding_sloc3_1_0
	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
	mov	a,r6
	movx	@dptr,a
	mov	a,r0
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:72: beacon->HMAC_Lsb = *(Rxbuffer+POS_HMAC_LSB)<<8 | *(Rxbuffer+POS_HMAC_LSB+1);
	mov	a,#0x19
	add	a,_BEACON_decoding_sloc4_1_0
	mov	_BEACON_decoding_sloc3_1_0,a
	clr	a
	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
	mov	(_BEACON_decoding_sloc3_1_0 + 1),a
	mov	a,#0x14
	add	a,r1
	mov	r0,a
	clr	a
	addc	a,r2
	mov	r4,a
	mov	ar5,r3
	mov	dpl,r0
	mov	dph,r4
	mov	b,r5
	lcall	__gptrget
	mov	r0,a
	mov	r5,#0x00
	mov	(_BEACON_decoding_sloc5_1_0 + 1),r0
;	1-genFromRTrack replaced	mov	_BEACON_decoding_sloc5_1_0,#0x00
	mov	_BEACON_decoding_sloc5_1_0,r5
	mov	a,#0x15
	add	a,r1
	mov	r0,a
	clr	a
	addc	a,r2
	mov	r4,a
	mov	ar5,r3
	mov	dpl,r0
	mov	dph,r4
	mov	b,r5
	lcall	__gptrget
	mov	r0,a
	mov	r5,#0x00
	mov	a,_BEACON_decoding_sloc5_1_0
	orl	ar0,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
	orl	ar5,a
	mov	a,r5
	mov	r4,a
	rlc	a
	subb	a,acc
	mov	r5,a
	mov	r7,a
	mov	dpl,_BEACON_decoding_sloc3_1_0
	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
	mov	a,r0
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:73: beacon->HMAC_Lsb = beacon->HMAC_Lsb << 16;
	mov	(_BEACON_decoding_sloc5_1_0 + 3),r4
	mov	(_BEACON_decoding_sloc5_1_0 + 2),r0
	mov	(_BEACON_decoding_sloc5_1_0 + 1),#0x00
	mov	_BEACON_decoding_sloc5_1_0,#0x00
	mov	dpl,_BEACON_decoding_sloc3_1_0
	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
	mov	a,_BEACON_decoding_sloc5_1_0
	movx	@dptr,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
	inc	dptr
	movx	@dptr,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 2)
	inc	dptr
	movx	@dptr,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 3)
	inc	dptr
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:74: beacon->HMAC_Lsb = beacon->HMAC_Lsb | (*(Rxbuffer+POS_HMAC_LSB+2)<<8 | *(Rxbuffer+POS_HMAC_LSB+3) & 0x0000FFFF);
	mov	a,#0x16
	add	a,r1
	mov	r5,a
	clr	a
	addc	a,r2
	mov	r6,a
	mov	ar7,r3
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	lcall	__gptrget
	mov	r7,a
	mov	r5,#0x00
	mov	a,#0x17
	add	a,r1
	mov	r1,a
	clr	a
	addc	a,r2
	mov	r2,a
	mov	dpl,r1
	mov	dph,r2
	mov	b,r3
	lcall	__gptrget
	mov	r6,a
	mov	r1,#0x00
	mov	a,r5
	orl	ar6,a
	mov	a,r7
	orl	ar1,a
	clr	a
	mov	r7,a
	mov	r5,a
	mov	a,_BEACON_decoding_sloc5_1_0
	orl	ar6,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
	orl	ar1,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 2)
	orl	ar7,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 3)
	orl	ar5,a
	mov	dpl,_BEACON_decoding_sloc3_1_0
	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
	mov	a,r6
	movx	@dptr,a
	mov	a,r1
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	ret
00108$:
;	..\src\peripherals\Beacon.c:75: }else beacon->FlagReceived = INVALID_BEACON;
	mov	dpl,_BEACON_decoding_sloc4_1_0
	mov	dph,(_BEACON_decoding_sloc4_1_0 + 1)
	clr	a
	movx	@dptr,a
	ret
	.area CSEG    (CODE)
	.area CONST   (CODE)
	.area XINIT   (CODE)
	.area CABS    (ABS,CODE)
