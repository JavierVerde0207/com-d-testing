                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.6.0 #9615 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module UartComProc
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _delay_ms
                                     12 	.globl _memcpy
                                     13 	.globl _free
                                     14 	.globl _malloc
                                     15 	.globl _uart_timer1_baud
                                     16 	.globl _uart0_tx
                                     17 	.globl _uart0_rx
                                     18 	.globl _uart0_init
                                     19 	.globl _uart0_rxcount
                                     20 	.globl _PORTC_7
                                     21 	.globl _PORTC_6
                                     22 	.globl _PORTC_5
                                     23 	.globl _PORTC_4
                                     24 	.globl _PORTC_3
                                     25 	.globl _PORTC_2
                                     26 	.globl _PORTC_1
                                     27 	.globl _PORTC_0
                                     28 	.globl _PORTB_7
                                     29 	.globl _PORTB_6
                                     30 	.globl _PORTB_5
                                     31 	.globl _PORTB_4
                                     32 	.globl _PORTB_3
                                     33 	.globl _PORTB_2
                                     34 	.globl _PORTB_1
                                     35 	.globl _PORTB_0
                                     36 	.globl _PORTA_7
                                     37 	.globl _PORTA_6
                                     38 	.globl _PORTA_5
                                     39 	.globl _PORTA_4
                                     40 	.globl _PORTA_3
                                     41 	.globl _PORTA_2
                                     42 	.globl _PORTA_1
                                     43 	.globl _PORTA_0
                                     44 	.globl _PINC_7
                                     45 	.globl _PINC_6
                                     46 	.globl _PINC_5
                                     47 	.globl _PINC_4
                                     48 	.globl _PINC_3
                                     49 	.globl _PINC_2
                                     50 	.globl _PINC_1
                                     51 	.globl _PINC_0
                                     52 	.globl _PINB_7
                                     53 	.globl _PINB_6
                                     54 	.globl _PINB_5
                                     55 	.globl _PINB_4
                                     56 	.globl _PINB_3
                                     57 	.globl _PINB_2
                                     58 	.globl _PINB_1
                                     59 	.globl _PINB_0
                                     60 	.globl _PINA_7
                                     61 	.globl _PINA_6
                                     62 	.globl _PINA_5
                                     63 	.globl _PINA_4
                                     64 	.globl _PINA_3
                                     65 	.globl _PINA_2
                                     66 	.globl _PINA_1
                                     67 	.globl _PINA_0
                                     68 	.globl _CY
                                     69 	.globl _AC
                                     70 	.globl _F0
                                     71 	.globl _RS1
                                     72 	.globl _RS0
                                     73 	.globl _OV
                                     74 	.globl _F1
                                     75 	.globl _P
                                     76 	.globl _IP_7
                                     77 	.globl _IP_6
                                     78 	.globl _IP_5
                                     79 	.globl _IP_4
                                     80 	.globl _IP_3
                                     81 	.globl _IP_2
                                     82 	.globl _IP_1
                                     83 	.globl _IP_0
                                     84 	.globl _EA
                                     85 	.globl _IE_7
                                     86 	.globl _IE_6
                                     87 	.globl _IE_5
                                     88 	.globl _IE_4
                                     89 	.globl _IE_3
                                     90 	.globl _IE_2
                                     91 	.globl _IE_1
                                     92 	.globl _IE_0
                                     93 	.globl _EIP_7
                                     94 	.globl _EIP_6
                                     95 	.globl _EIP_5
                                     96 	.globl _EIP_4
                                     97 	.globl _EIP_3
                                     98 	.globl _EIP_2
                                     99 	.globl _EIP_1
                                    100 	.globl _EIP_0
                                    101 	.globl _EIE_7
                                    102 	.globl _EIE_6
                                    103 	.globl _EIE_5
                                    104 	.globl _EIE_4
                                    105 	.globl _EIE_3
                                    106 	.globl _EIE_2
                                    107 	.globl _EIE_1
                                    108 	.globl _EIE_0
                                    109 	.globl _E2IP_7
                                    110 	.globl _E2IP_6
                                    111 	.globl _E2IP_5
                                    112 	.globl _E2IP_4
                                    113 	.globl _E2IP_3
                                    114 	.globl _E2IP_2
                                    115 	.globl _E2IP_1
                                    116 	.globl _E2IP_0
                                    117 	.globl _E2IE_7
                                    118 	.globl _E2IE_6
                                    119 	.globl _E2IE_5
                                    120 	.globl _E2IE_4
                                    121 	.globl _E2IE_3
                                    122 	.globl _E2IE_2
                                    123 	.globl _E2IE_1
                                    124 	.globl _E2IE_0
                                    125 	.globl _B_7
                                    126 	.globl _B_6
                                    127 	.globl _B_5
                                    128 	.globl _B_4
                                    129 	.globl _B_3
                                    130 	.globl _B_2
                                    131 	.globl _B_1
                                    132 	.globl _B_0
                                    133 	.globl _ACC_7
                                    134 	.globl _ACC_6
                                    135 	.globl _ACC_5
                                    136 	.globl _ACC_4
                                    137 	.globl _ACC_3
                                    138 	.globl _ACC_2
                                    139 	.globl _ACC_1
                                    140 	.globl _ACC_0
                                    141 	.globl _WTSTAT
                                    142 	.globl _WTIRQEN
                                    143 	.globl _WTEVTD
                                    144 	.globl _WTEVTD1
                                    145 	.globl _WTEVTD0
                                    146 	.globl _WTEVTC
                                    147 	.globl _WTEVTC1
                                    148 	.globl _WTEVTC0
                                    149 	.globl _WTEVTB
                                    150 	.globl _WTEVTB1
                                    151 	.globl _WTEVTB0
                                    152 	.globl _WTEVTA
                                    153 	.globl _WTEVTA1
                                    154 	.globl _WTEVTA0
                                    155 	.globl _WTCNTR1
                                    156 	.globl _WTCNTB
                                    157 	.globl _WTCNTB1
                                    158 	.globl _WTCNTB0
                                    159 	.globl _WTCNTA
                                    160 	.globl _WTCNTA1
                                    161 	.globl _WTCNTA0
                                    162 	.globl _WTCFGB
                                    163 	.globl _WTCFGA
                                    164 	.globl _WDTRESET
                                    165 	.globl _WDTCFG
                                    166 	.globl _U1STATUS
                                    167 	.globl _U1SHREG
                                    168 	.globl _U1MODE
                                    169 	.globl _U1CTRL
                                    170 	.globl _U0STATUS
                                    171 	.globl _U0SHREG
                                    172 	.globl _U0MODE
                                    173 	.globl _U0CTRL
                                    174 	.globl _T2STATUS
                                    175 	.globl _T2PERIOD
                                    176 	.globl _T2PERIOD1
                                    177 	.globl _T2PERIOD0
                                    178 	.globl _T2MODE
                                    179 	.globl _T2CNT
                                    180 	.globl _T2CNT1
                                    181 	.globl _T2CNT0
                                    182 	.globl _T2CLKSRC
                                    183 	.globl _T1STATUS
                                    184 	.globl _T1PERIOD
                                    185 	.globl _T1PERIOD1
                                    186 	.globl _T1PERIOD0
                                    187 	.globl _T1MODE
                                    188 	.globl _T1CNT
                                    189 	.globl _T1CNT1
                                    190 	.globl _T1CNT0
                                    191 	.globl _T1CLKSRC
                                    192 	.globl _T0STATUS
                                    193 	.globl _T0PERIOD
                                    194 	.globl _T0PERIOD1
                                    195 	.globl _T0PERIOD0
                                    196 	.globl _T0MODE
                                    197 	.globl _T0CNT
                                    198 	.globl _T0CNT1
                                    199 	.globl _T0CNT0
                                    200 	.globl _T0CLKSRC
                                    201 	.globl _SPSTATUS
                                    202 	.globl _SPSHREG
                                    203 	.globl _SPMODE
                                    204 	.globl _SPCLKSRC
                                    205 	.globl _RADIOSTAT
                                    206 	.globl _RADIOSTAT1
                                    207 	.globl _RADIOSTAT0
                                    208 	.globl _RADIODATA
                                    209 	.globl _RADIODATA3
                                    210 	.globl _RADIODATA2
                                    211 	.globl _RADIODATA1
                                    212 	.globl _RADIODATA0
                                    213 	.globl _RADIOADDR
                                    214 	.globl _RADIOADDR1
                                    215 	.globl _RADIOADDR0
                                    216 	.globl _RADIOACC
                                    217 	.globl _OC1STATUS
                                    218 	.globl _OC1PIN
                                    219 	.globl _OC1MODE
                                    220 	.globl _OC1COMP
                                    221 	.globl _OC1COMP1
                                    222 	.globl _OC1COMP0
                                    223 	.globl _OC0STATUS
                                    224 	.globl _OC0PIN
                                    225 	.globl _OC0MODE
                                    226 	.globl _OC0COMP
                                    227 	.globl _OC0COMP1
                                    228 	.globl _OC0COMP0
                                    229 	.globl _NVSTATUS
                                    230 	.globl _NVKEY
                                    231 	.globl _NVDATA
                                    232 	.globl _NVDATA1
                                    233 	.globl _NVDATA0
                                    234 	.globl _NVADDR
                                    235 	.globl _NVADDR1
                                    236 	.globl _NVADDR0
                                    237 	.globl _IC1STATUS
                                    238 	.globl _IC1MODE
                                    239 	.globl _IC1CAPT
                                    240 	.globl _IC1CAPT1
                                    241 	.globl _IC1CAPT0
                                    242 	.globl _IC0STATUS
                                    243 	.globl _IC0MODE
                                    244 	.globl _IC0CAPT
                                    245 	.globl _IC0CAPT1
                                    246 	.globl _IC0CAPT0
                                    247 	.globl _PORTR
                                    248 	.globl _PORTC
                                    249 	.globl _PORTB
                                    250 	.globl _PORTA
                                    251 	.globl _PINR
                                    252 	.globl _PINC
                                    253 	.globl _PINB
                                    254 	.globl _PINA
                                    255 	.globl _DIRR
                                    256 	.globl _DIRC
                                    257 	.globl _DIRB
                                    258 	.globl _DIRA
                                    259 	.globl _DBGLNKSTAT
                                    260 	.globl _DBGLNKBUF
                                    261 	.globl _CODECONFIG
                                    262 	.globl _CLKSTAT
                                    263 	.globl _CLKCON
                                    264 	.globl _ANALOGCOMP
                                    265 	.globl _ADCCONV
                                    266 	.globl _ADCCLKSRC
                                    267 	.globl _ADCCH3CONFIG
                                    268 	.globl _ADCCH2CONFIG
                                    269 	.globl _ADCCH1CONFIG
                                    270 	.globl _ADCCH0CONFIG
                                    271 	.globl __XPAGE
                                    272 	.globl _XPAGE
                                    273 	.globl _SP
                                    274 	.globl _PSW
                                    275 	.globl _PCON
                                    276 	.globl _IP
                                    277 	.globl _IE
                                    278 	.globl _EIP
                                    279 	.globl _EIE
                                    280 	.globl _E2IP
                                    281 	.globl _E2IE
                                    282 	.globl _DPS
                                    283 	.globl _DPTR1
                                    284 	.globl _DPTR0
                                    285 	.globl _DPL1
                                    286 	.globl _DPL
                                    287 	.globl _DPH1
                                    288 	.globl _DPH
                                    289 	.globl _B
                                    290 	.globl _ACC
                                    291 	.globl _UART_Calc_CRC32_PARM_3
                                    292 	.globl _UART_Calc_CRC32_PARM_2
                                    293 	.globl _UART_Proc_SendMessage_PARM_3
                                    294 	.globl _UART_Proc_SendMessage_PARM_2
                                    295 	.globl _UART_Proc_ModifiyKissSpecialCharacters_PARM_2
                                    296 	.globl _UART_Proc_VerifyIncomingMsg_PARM_3
                                    297 	.globl _UART_Proc_VerifyIncomingMsg_PARM_2
                                    298 	.globl _AX5043_TIMEGAIN3NB
                                    299 	.globl _AX5043_TIMEGAIN2NB
                                    300 	.globl _AX5043_TIMEGAIN1NB
                                    301 	.globl _AX5043_TIMEGAIN0NB
                                    302 	.globl _AX5043_RXPARAMSETSNB
                                    303 	.globl _AX5043_RXPARAMCURSETNB
                                    304 	.globl _AX5043_PKTMAXLENNB
                                    305 	.globl _AX5043_PKTLENOFFSETNB
                                    306 	.globl _AX5043_PKTLENCFGNB
                                    307 	.globl _AX5043_PKTADDRMASK3NB
                                    308 	.globl _AX5043_PKTADDRMASK2NB
                                    309 	.globl _AX5043_PKTADDRMASK1NB
                                    310 	.globl _AX5043_PKTADDRMASK0NB
                                    311 	.globl _AX5043_PKTADDRCFGNB
                                    312 	.globl _AX5043_PKTADDR3NB
                                    313 	.globl _AX5043_PKTADDR2NB
                                    314 	.globl _AX5043_PKTADDR1NB
                                    315 	.globl _AX5043_PKTADDR0NB
                                    316 	.globl _AX5043_PHASEGAIN3NB
                                    317 	.globl _AX5043_PHASEGAIN2NB
                                    318 	.globl _AX5043_PHASEGAIN1NB
                                    319 	.globl _AX5043_PHASEGAIN0NB
                                    320 	.globl _AX5043_FREQUENCYLEAKNB
                                    321 	.globl _AX5043_FREQUENCYGAIND3NB
                                    322 	.globl _AX5043_FREQUENCYGAIND2NB
                                    323 	.globl _AX5043_FREQUENCYGAIND1NB
                                    324 	.globl _AX5043_FREQUENCYGAIND0NB
                                    325 	.globl _AX5043_FREQUENCYGAINC3NB
                                    326 	.globl _AX5043_FREQUENCYGAINC2NB
                                    327 	.globl _AX5043_FREQUENCYGAINC1NB
                                    328 	.globl _AX5043_FREQUENCYGAINC0NB
                                    329 	.globl _AX5043_FREQUENCYGAINB3NB
                                    330 	.globl _AX5043_FREQUENCYGAINB2NB
                                    331 	.globl _AX5043_FREQUENCYGAINB1NB
                                    332 	.globl _AX5043_FREQUENCYGAINB0NB
                                    333 	.globl _AX5043_FREQUENCYGAINA3NB
                                    334 	.globl _AX5043_FREQUENCYGAINA2NB
                                    335 	.globl _AX5043_FREQUENCYGAINA1NB
                                    336 	.globl _AX5043_FREQUENCYGAINA0NB
                                    337 	.globl _AX5043_FREQDEV13NB
                                    338 	.globl _AX5043_FREQDEV12NB
                                    339 	.globl _AX5043_FREQDEV11NB
                                    340 	.globl _AX5043_FREQDEV10NB
                                    341 	.globl _AX5043_FREQDEV03NB
                                    342 	.globl _AX5043_FREQDEV02NB
                                    343 	.globl _AX5043_FREQDEV01NB
                                    344 	.globl _AX5043_FREQDEV00NB
                                    345 	.globl _AX5043_FOURFSK3NB
                                    346 	.globl _AX5043_FOURFSK2NB
                                    347 	.globl _AX5043_FOURFSK1NB
                                    348 	.globl _AX5043_FOURFSK0NB
                                    349 	.globl _AX5043_DRGAIN3NB
                                    350 	.globl _AX5043_DRGAIN2NB
                                    351 	.globl _AX5043_DRGAIN1NB
                                    352 	.globl _AX5043_DRGAIN0NB
                                    353 	.globl _AX5043_BBOFFSRES3NB
                                    354 	.globl _AX5043_BBOFFSRES2NB
                                    355 	.globl _AX5043_BBOFFSRES1NB
                                    356 	.globl _AX5043_BBOFFSRES0NB
                                    357 	.globl _AX5043_AMPLITUDEGAIN3NB
                                    358 	.globl _AX5043_AMPLITUDEGAIN2NB
                                    359 	.globl _AX5043_AMPLITUDEGAIN1NB
                                    360 	.globl _AX5043_AMPLITUDEGAIN0NB
                                    361 	.globl _AX5043_AGCTARGET3NB
                                    362 	.globl _AX5043_AGCTARGET2NB
                                    363 	.globl _AX5043_AGCTARGET1NB
                                    364 	.globl _AX5043_AGCTARGET0NB
                                    365 	.globl _AX5043_AGCMINMAX3NB
                                    366 	.globl _AX5043_AGCMINMAX2NB
                                    367 	.globl _AX5043_AGCMINMAX1NB
                                    368 	.globl _AX5043_AGCMINMAX0NB
                                    369 	.globl _AX5043_AGCGAIN3NB
                                    370 	.globl _AX5043_AGCGAIN2NB
                                    371 	.globl _AX5043_AGCGAIN1NB
                                    372 	.globl _AX5043_AGCGAIN0NB
                                    373 	.globl _AX5043_AGCAHYST3NB
                                    374 	.globl _AX5043_AGCAHYST2NB
                                    375 	.globl _AX5043_AGCAHYST1NB
                                    376 	.globl _AX5043_AGCAHYST0NB
                                    377 	.globl _AX5043_0xF44NB
                                    378 	.globl _AX5043_0xF35NB
                                    379 	.globl _AX5043_0xF34NB
                                    380 	.globl _AX5043_0xF33NB
                                    381 	.globl _AX5043_0xF32NB
                                    382 	.globl _AX5043_0xF31NB
                                    383 	.globl _AX5043_0xF30NB
                                    384 	.globl _AX5043_0xF26NB
                                    385 	.globl _AX5043_0xF23NB
                                    386 	.globl _AX5043_0xF22NB
                                    387 	.globl _AX5043_0xF21NB
                                    388 	.globl _AX5043_0xF1CNB
                                    389 	.globl _AX5043_0xF18NB
                                    390 	.globl _AX5043_0xF0CNB
                                    391 	.globl _AX5043_0xF00NB
                                    392 	.globl _AX5043_XTALSTATUSNB
                                    393 	.globl _AX5043_XTALOSCNB
                                    394 	.globl _AX5043_XTALCAPNB
                                    395 	.globl _AX5043_XTALAMPLNB
                                    396 	.globl _AX5043_WAKEUPXOEARLYNB
                                    397 	.globl _AX5043_WAKEUPTIMER1NB
                                    398 	.globl _AX5043_WAKEUPTIMER0NB
                                    399 	.globl _AX5043_WAKEUPFREQ1NB
                                    400 	.globl _AX5043_WAKEUPFREQ0NB
                                    401 	.globl _AX5043_WAKEUP1NB
                                    402 	.globl _AX5043_WAKEUP0NB
                                    403 	.globl _AX5043_TXRATE2NB
                                    404 	.globl _AX5043_TXRATE1NB
                                    405 	.globl _AX5043_TXRATE0NB
                                    406 	.globl _AX5043_TXPWRCOEFFE1NB
                                    407 	.globl _AX5043_TXPWRCOEFFE0NB
                                    408 	.globl _AX5043_TXPWRCOEFFD1NB
                                    409 	.globl _AX5043_TXPWRCOEFFD0NB
                                    410 	.globl _AX5043_TXPWRCOEFFC1NB
                                    411 	.globl _AX5043_TXPWRCOEFFC0NB
                                    412 	.globl _AX5043_TXPWRCOEFFB1NB
                                    413 	.globl _AX5043_TXPWRCOEFFB0NB
                                    414 	.globl _AX5043_TXPWRCOEFFA1NB
                                    415 	.globl _AX5043_TXPWRCOEFFA0NB
                                    416 	.globl _AX5043_TRKRFFREQ2NB
                                    417 	.globl _AX5043_TRKRFFREQ1NB
                                    418 	.globl _AX5043_TRKRFFREQ0NB
                                    419 	.globl _AX5043_TRKPHASE1NB
                                    420 	.globl _AX5043_TRKPHASE0NB
                                    421 	.globl _AX5043_TRKFSKDEMOD1NB
                                    422 	.globl _AX5043_TRKFSKDEMOD0NB
                                    423 	.globl _AX5043_TRKFREQ1NB
                                    424 	.globl _AX5043_TRKFREQ0NB
                                    425 	.globl _AX5043_TRKDATARATE2NB
                                    426 	.globl _AX5043_TRKDATARATE1NB
                                    427 	.globl _AX5043_TRKDATARATE0NB
                                    428 	.globl _AX5043_TRKAMPLITUDE1NB
                                    429 	.globl _AX5043_TRKAMPLITUDE0NB
                                    430 	.globl _AX5043_TRKAFSKDEMOD1NB
                                    431 	.globl _AX5043_TRKAFSKDEMOD0NB
                                    432 	.globl _AX5043_TMGTXSETTLENB
                                    433 	.globl _AX5043_TMGTXBOOSTNB
                                    434 	.globl _AX5043_TMGRXSETTLENB
                                    435 	.globl _AX5043_TMGRXRSSINB
                                    436 	.globl _AX5043_TMGRXPREAMBLE3NB
                                    437 	.globl _AX5043_TMGRXPREAMBLE2NB
                                    438 	.globl _AX5043_TMGRXPREAMBLE1NB
                                    439 	.globl _AX5043_TMGRXOFFSACQNB
                                    440 	.globl _AX5043_TMGRXCOARSEAGCNB
                                    441 	.globl _AX5043_TMGRXBOOSTNB
                                    442 	.globl _AX5043_TMGRXAGCNB
                                    443 	.globl _AX5043_TIMER2NB
                                    444 	.globl _AX5043_TIMER1NB
                                    445 	.globl _AX5043_TIMER0NB
                                    446 	.globl _AX5043_SILICONREVISIONNB
                                    447 	.globl _AX5043_SCRATCHNB
                                    448 	.globl _AX5043_RXDATARATE2NB
                                    449 	.globl _AX5043_RXDATARATE1NB
                                    450 	.globl _AX5043_RXDATARATE0NB
                                    451 	.globl _AX5043_RSSIREFERENCENB
                                    452 	.globl _AX5043_RSSIABSTHRNB
                                    453 	.globl _AX5043_RSSINB
                                    454 	.globl _AX5043_REFNB
                                    455 	.globl _AX5043_RADIOSTATENB
                                    456 	.globl _AX5043_RADIOEVENTREQ1NB
                                    457 	.globl _AX5043_RADIOEVENTREQ0NB
                                    458 	.globl _AX5043_RADIOEVENTMASK1NB
                                    459 	.globl _AX5043_RADIOEVENTMASK0NB
                                    460 	.globl _AX5043_PWRMODENB
                                    461 	.globl _AX5043_PWRAMPNB
                                    462 	.globl _AX5043_POWSTICKYSTATNB
                                    463 	.globl _AX5043_POWSTATNB
                                    464 	.globl _AX5043_POWIRQMASKNB
                                    465 	.globl _AX5043_POWCTRL1NB
                                    466 	.globl _AX5043_PLLVCOIRNB
                                    467 	.globl _AX5043_PLLVCOINB
                                    468 	.globl _AX5043_PLLVCODIVNB
                                    469 	.globl _AX5043_PLLRNGCLKNB
                                    470 	.globl _AX5043_PLLRANGINGBNB
                                    471 	.globl _AX5043_PLLRANGINGANB
                                    472 	.globl _AX5043_PLLLOOPBOOSTNB
                                    473 	.globl _AX5043_PLLLOOPNB
                                    474 	.globl _AX5043_PLLLOCKDETNB
                                    475 	.globl _AX5043_PLLCPIBOOSTNB
                                    476 	.globl _AX5043_PLLCPINB
                                    477 	.globl _AX5043_PKTSTOREFLAGSNB
                                    478 	.globl _AX5043_PKTMISCFLAGSNB
                                    479 	.globl _AX5043_PKTCHUNKSIZENB
                                    480 	.globl _AX5043_PKTACCEPTFLAGSNB
                                    481 	.globl _AX5043_PINSTATENB
                                    482 	.globl _AX5043_PINFUNCSYSCLKNB
                                    483 	.globl _AX5043_PINFUNCPWRAMPNB
                                    484 	.globl _AX5043_PINFUNCIRQNB
                                    485 	.globl _AX5043_PINFUNCDCLKNB
                                    486 	.globl _AX5043_PINFUNCDATANB
                                    487 	.globl _AX5043_PINFUNCANTSELNB
                                    488 	.globl _AX5043_MODULATIONNB
                                    489 	.globl _AX5043_MODCFGPNB
                                    490 	.globl _AX5043_MODCFGFNB
                                    491 	.globl _AX5043_MODCFGANB
                                    492 	.globl _AX5043_MAXRFOFFSET2NB
                                    493 	.globl _AX5043_MAXRFOFFSET1NB
                                    494 	.globl _AX5043_MAXRFOFFSET0NB
                                    495 	.globl _AX5043_MAXDROFFSET2NB
                                    496 	.globl _AX5043_MAXDROFFSET1NB
                                    497 	.globl _AX5043_MAXDROFFSET0NB
                                    498 	.globl _AX5043_MATCH1PAT1NB
                                    499 	.globl _AX5043_MATCH1PAT0NB
                                    500 	.globl _AX5043_MATCH1MINNB
                                    501 	.globl _AX5043_MATCH1MAXNB
                                    502 	.globl _AX5043_MATCH1LENNB
                                    503 	.globl _AX5043_MATCH0PAT3NB
                                    504 	.globl _AX5043_MATCH0PAT2NB
                                    505 	.globl _AX5043_MATCH0PAT1NB
                                    506 	.globl _AX5043_MATCH0PAT0NB
                                    507 	.globl _AX5043_MATCH0MINNB
                                    508 	.globl _AX5043_MATCH0MAXNB
                                    509 	.globl _AX5043_MATCH0LENNB
                                    510 	.globl _AX5043_LPOSCSTATUSNB
                                    511 	.globl _AX5043_LPOSCREF1NB
                                    512 	.globl _AX5043_LPOSCREF0NB
                                    513 	.globl _AX5043_LPOSCPER1NB
                                    514 	.globl _AX5043_LPOSCPER0NB
                                    515 	.globl _AX5043_LPOSCKFILT1NB
                                    516 	.globl _AX5043_LPOSCKFILT0NB
                                    517 	.globl _AX5043_LPOSCFREQ1NB
                                    518 	.globl _AX5043_LPOSCFREQ0NB
                                    519 	.globl _AX5043_LPOSCCONFIGNB
                                    520 	.globl _AX5043_IRQREQUEST1NB
                                    521 	.globl _AX5043_IRQREQUEST0NB
                                    522 	.globl _AX5043_IRQMASK1NB
                                    523 	.globl _AX5043_IRQMASK0NB
                                    524 	.globl _AX5043_IRQINVERSION1NB
                                    525 	.globl _AX5043_IRQINVERSION0NB
                                    526 	.globl _AX5043_IFFREQ1NB
                                    527 	.globl _AX5043_IFFREQ0NB
                                    528 	.globl _AX5043_GPADCPERIODNB
                                    529 	.globl _AX5043_GPADCCTRLNB
                                    530 	.globl _AX5043_GPADC13VALUE1NB
                                    531 	.globl _AX5043_GPADC13VALUE0NB
                                    532 	.globl _AX5043_FSKDMIN1NB
                                    533 	.globl _AX5043_FSKDMIN0NB
                                    534 	.globl _AX5043_FSKDMAX1NB
                                    535 	.globl _AX5043_FSKDMAX0NB
                                    536 	.globl _AX5043_FSKDEV2NB
                                    537 	.globl _AX5043_FSKDEV1NB
                                    538 	.globl _AX5043_FSKDEV0NB
                                    539 	.globl _AX5043_FREQB3NB
                                    540 	.globl _AX5043_FREQB2NB
                                    541 	.globl _AX5043_FREQB1NB
                                    542 	.globl _AX5043_FREQB0NB
                                    543 	.globl _AX5043_FREQA3NB
                                    544 	.globl _AX5043_FREQA2NB
                                    545 	.globl _AX5043_FREQA1NB
                                    546 	.globl _AX5043_FREQA0NB
                                    547 	.globl _AX5043_FRAMINGNB
                                    548 	.globl _AX5043_FIFOTHRESH1NB
                                    549 	.globl _AX5043_FIFOTHRESH0NB
                                    550 	.globl _AX5043_FIFOSTATNB
                                    551 	.globl _AX5043_FIFOFREE1NB
                                    552 	.globl _AX5043_FIFOFREE0NB
                                    553 	.globl _AX5043_FIFODATANB
                                    554 	.globl _AX5043_FIFOCOUNT1NB
                                    555 	.globl _AX5043_FIFOCOUNT0NB
                                    556 	.globl _AX5043_FECSYNCNB
                                    557 	.globl _AX5043_FECSTATUSNB
                                    558 	.globl _AX5043_FECNB
                                    559 	.globl _AX5043_ENCODINGNB
                                    560 	.globl _AX5043_DIVERSITYNB
                                    561 	.globl _AX5043_DECIMATIONNB
                                    562 	.globl _AX5043_DACVALUE1NB
                                    563 	.globl _AX5043_DACVALUE0NB
                                    564 	.globl _AX5043_DACCONFIGNB
                                    565 	.globl _AX5043_CRCINIT3NB
                                    566 	.globl _AX5043_CRCINIT2NB
                                    567 	.globl _AX5043_CRCINIT1NB
                                    568 	.globl _AX5043_CRCINIT0NB
                                    569 	.globl _AX5043_BGNDRSSITHRNB
                                    570 	.globl _AX5043_BGNDRSSIGAINNB
                                    571 	.globl _AX5043_BGNDRSSINB
                                    572 	.globl _AX5043_BBTUNENB
                                    573 	.globl _AX5043_BBOFFSCAPNB
                                    574 	.globl _AX5043_AMPLFILTERNB
                                    575 	.globl _AX5043_AGCCOUNTERNB
                                    576 	.globl _AX5043_AFSKSPACE1NB
                                    577 	.globl _AX5043_AFSKSPACE0NB
                                    578 	.globl _AX5043_AFSKMARK1NB
                                    579 	.globl _AX5043_AFSKMARK0NB
                                    580 	.globl _AX5043_AFSKCTRLNB
                                    581 	.globl _AX5043_TIMEGAIN3
                                    582 	.globl _AX5043_TIMEGAIN2
                                    583 	.globl _AX5043_TIMEGAIN1
                                    584 	.globl _AX5043_TIMEGAIN0
                                    585 	.globl _AX5043_RXPARAMSETS
                                    586 	.globl _AX5043_RXPARAMCURSET
                                    587 	.globl _AX5043_PKTMAXLEN
                                    588 	.globl _AX5043_PKTLENOFFSET
                                    589 	.globl _AX5043_PKTLENCFG
                                    590 	.globl _AX5043_PKTADDRMASK3
                                    591 	.globl _AX5043_PKTADDRMASK2
                                    592 	.globl _AX5043_PKTADDRMASK1
                                    593 	.globl _AX5043_PKTADDRMASK0
                                    594 	.globl _AX5043_PKTADDRCFG
                                    595 	.globl _AX5043_PKTADDR3
                                    596 	.globl _AX5043_PKTADDR2
                                    597 	.globl _AX5043_PKTADDR1
                                    598 	.globl _AX5043_PKTADDR0
                                    599 	.globl _AX5043_PHASEGAIN3
                                    600 	.globl _AX5043_PHASEGAIN2
                                    601 	.globl _AX5043_PHASEGAIN1
                                    602 	.globl _AX5043_PHASEGAIN0
                                    603 	.globl _AX5043_FREQUENCYLEAK
                                    604 	.globl _AX5043_FREQUENCYGAIND3
                                    605 	.globl _AX5043_FREQUENCYGAIND2
                                    606 	.globl _AX5043_FREQUENCYGAIND1
                                    607 	.globl _AX5043_FREQUENCYGAIND0
                                    608 	.globl _AX5043_FREQUENCYGAINC3
                                    609 	.globl _AX5043_FREQUENCYGAINC2
                                    610 	.globl _AX5043_FREQUENCYGAINC1
                                    611 	.globl _AX5043_FREQUENCYGAINC0
                                    612 	.globl _AX5043_FREQUENCYGAINB3
                                    613 	.globl _AX5043_FREQUENCYGAINB2
                                    614 	.globl _AX5043_FREQUENCYGAINB1
                                    615 	.globl _AX5043_FREQUENCYGAINB0
                                    616 	.globl _AX5043_FREQUENCYGAINA3
                                    617 	.globl _AX5043_FREQUENCYGAINA2
                                    618 	.globl _AX5043_FREQUENCYGAINA1
                                    619 	.globl _AX5043_FREQUENCYGAINA0
                                    620 	.globl _AX5043_FREQDEV13
                                    621 	.globl _AX5043_FREQDEV12
                                    622 	.globl _AX5043_FREQDEV11
                                    623 	.globl _AX5043_FREQDEV10
                                    624 	.globl _AX5043_FREQDEV03
                                    625 	.globl _AX5043_FREQDEV02
                                    626 	.globl _AX5043_FREQDEV01
                                    627 	.globl _AX5043_FREQDEV00
                                    628 	.globl _AX5043_FOURFSK3
                                    629 	.globl _AX5043_FOURFSK2
                                    630 	.globl _AX5043_FOURFSK1
                                    631 	.globl _AX5043_FOURFSK0
                                    632 	.globl _AX5043_DRGAIN3
                                    633 	.globl _AX5043_DRGAIN2
                                    634 	.globl _AX5043_DRGAIN1
                                    635 	.globl _AX5043_DRGAIN0
                                    636 	.globl _AX5043_BBOFFSRES3
                                    637 	.globl _AX5043_BBOFFSRES2
                                    638 	.globl _AX5043_BBOFFSRES1
                                    639 	.globl _AX5043_BBOFFSRES0
                                    640 	.globl _AX5043_AMPLITUDEGAIN3
                                    641 	.globl _AX5043_AMPLITUDEGAIN2
                                    642 	.globl _AX5043_AMPLITUDEGAIN1
                                    643 	.globl _AX5043_AMPLITUDEGAIN0
                                    644 	.globl _AX5043_AGCTARGET3
                                    645 	.globl _AX5043_AGCTARGET2
                                    646 	.globl _AX5043_AGCTARGET1
                                    647 	.globl _AX5043_AGCTARGET0
                                    648 	.globl _AX5043_AGCMINMAX3
                                    649 	.globl _AX5043_AGCMINMAX2
                                    650 	.globl _AX5043_AGCMINMAX1
                                    651 	.globl _AX5043_AGCMINMAX0
                                    652 	.globl _AX5043_AGCGAIN3
                                    653 	.globl _AX5043_AGCGAIN2
                                    654 	.globl _AX5043_AGCGAIN1
                                    655 	.globl _AX5043_AGCGAIN0
                                    656 	.globl _AX5043_AGCAHYST3
                                    657 	.globl _AX5043_AGCAHYST2
                                    658 	.globl _AX5043_AGCAHYST1
                                    659 	.globl _AX5043_AGCAHYST0
                                    660 	.globl _AX5043_0xF44
                                    661 	.globl _AX5043_0xF35
                                    662 	.globl _AX5043_0xF34
                                    663 	.globl _AX5043_0xF33
                                    664 	.globl _AX5043_0xF32
                                    665 	.globl _AX5043_0xF31
                                    666 	.globl _AX5043_0xF30
                                    667 	.globl _AX5043_0xF26
                                    668 	.globl _AX5043_0xF23
                                    669 	.globl _AX5043_0xF22
                                    670 	.globl _AX5043_0xF21
                                    671 	.globl _AX5043_0xF1C
                                    672 	.globl _AX5043_0xF18
                                    673 	.globl _AX5043_0xF0C
                                    674 	.globl _AX5043_0xF00
                                    675 	.globl _AX5043_XTALSTATUS
                                    676 	.globl _AX5043_XTALOSC
                                    677 	.globl _AX5043_XTALCAP
                                    678 	.globl _AX5043_XTALAMPL
                                    679 	.globl _AX5043_WAKEUPXOEARLY
                                    680 	.globl _AX5043_WAKEUPTIMER1
                                    681 	.globl _AX5043_WAKEUPTIMER0
                                    682 	.globl _AX5043_WAKEUPFREQ1
                                    683 	.globl _AX5043_WAKEUPFREQ0
                                    684 	.globl _AX5043_WAKEUP1
                                    685 	.globl _AX5043_WAKEUP0
                                    686 	.globl _AX5043_TXRATE2
                                    687 	.globl _AX5043_TXRATE1
                                    688 	.globl _AX5043_TXRATE0
                                    689 	.globl _AX5043_TXPWRCOEFFE1
                                    690 	.globl _AX5043_TXPWRCOEFFE0
                                    691 	.globl _AX5043_TXPWRCOEFFD1
                                    692 	.globl _AX5043_TXPWRCOEFFD0
                                    693 	.globl _AX5043_TXPWRCOEFFC1
                                    694 	.globl _AX5043_TXPWRCOEFFC0
                                    695 	.globl _AX5043_TXPWRCOEFFB1
                                    696 	.globl _AX5043_TXPWRCOEFFB0
                                    697 	.globl _AX5043_TXPWRCOEFFA1
                                    698 	.globl _AX5043_TXPWRCOEFFA0
                                    699 	.globl _AX5043_TRKRFFREQ2
                                    700 	.globl _AX5043_TRKRFFREQ1
                                    701 	.globl _AX5043_TRKRFFREQ0
                                    702 	.globl _AX5043_TRKPHASE1
                                    703 	.globl _AX5043_TRKPHASE0
                                    704 	.globl _AX5043_TRKFSKDEMOD1
                                    705 	.globl _AX5043_TRKFSKDEMOD0
                                    706 	.globl _AX5043_TRKFREQ1
                                    707 	.globl _AX5043_TRKFREQ0
                                    708 	.globl _AX5043_TRKDATARATE2
                                    709 	.globl _AX5043_TRKDATARATE1
                                    710 	.globl _AX5043_TRKDATARATE0
                                    711 	.globl _AX5043_TRKAMPLITUDE1
                                    712 	.globl _AX5043_TRKAMPLITUDE0
                                    713 	.globl _AX5043_TRKAFSKDEMOD1
                                    714 	.globl _AX5043_TRKAFSKDEMOD0
                                    715 	.globl _AX5043_TMGTXSETTLE
                                    716 	.globl _AX5043_TMGTXBOOST
                                    717 	.globl _AX5043_TMGRXSETTLE
                                    718 	.globl _AX5043_TMGRXRSSI
                                    719 	.globl _AX5043_TMGRXPREAMBLE3
                                    720 	.globl _AX5043_TMGRXPREAMBLE2
                                    721 	.globl _AX5043_TMGRXPREAMBLE1
                                    722 	.globl _AX5043_TMGRXOFFSACQ
                                    723 	.globl _AX5043_TMGRXCOARSEAGC
                                    724 	.globl _AX5043_TMGRXBOOST
                                    725 	.globl _AX5043_TMGRXAGC
                                    726 	.globl _AX5043_TIMER2
                                    727 	.globl _AX5043_TIMER1
                                    728 	.globl _AX5043_TIMER0
                                    729 	.globl _AX5043_SILICONREVISION
                                    730 	.globl _AX5043_SCRATCH
                                    731 	.globl _AX5043_RXDATARATE2
                                    732 	.globl _AX5043_RXDATARATE1
                                    733 	.globl _AX5043_RXDATARATE0
                                    734 	.globl _AX5043_RSSIREFERENCE
                                    735 	.globl _AX5043_RSSIABSTHR
                                    736 	.globl _AX5043_RSSI
                                    737 	.globl _AX5043_REF
                                    738 	.globl _AX5043_RADIOSTATE
                                    739 	.globl _AX5043_RADIOEVENTREQ1
                                    740 	.globl _AX5043_RADIOEVENTREQ0
                                    741 	.globl _AX5043_RADIOEVENTMASK1
                                    742 	.globl _AX5043_RADIOEVENTMASK0
                                    743 	.globl _AX5043_PWRMODE
                                    744 	.globl _AX5043_PWRAMP
                                    745 	.globl _AX5043_POWSTICKYSTAT
                                    746 	.globl _AX5043_POWSTAT
                                    747 	.globl _AX5043_POWIRQMASK
                                    748 	.globl _AX5043_POWCTRL1
                                    749 	.globl _AX5043_PLLVCOIR
                                    750 	.globl _AX5043_PLLVCOI
                                    751 	.globl _AX5043_PLLVCODIV
                                    752 	.globl _AX5043_PLLRNGCLK
                                    753 	.globl _AX5043_PLLRANGINGB
                                    754 	.globl _AX5043_PLLRANGINGA
                                    755 	.globl _AX5043_PLLLOOPBOOST
                                    756 	.globl _AX5043_PLLLOOP
                                    757 	.globl _AX5043_PLLLOCKDET
                                    758 	.globl _AX5043_PLLCPIBOOST
                                    759 	.globl _AX5043_PLLCPI
                                    760 	.globl _AX5043_PKTSTOREFLAGS
                                    761 	.globl _AX5043_PKTMISCFLAGS
                                    762 	.globl _AX5043_PKTCHUNKSIZE
                                    763 	.globl _AX5043_PKTACCEPTFLAGS
                                    764 	.globl _AX5043_PINSTATE
                                    765 	.globl _AX5043_PINFUNCSYSCLK
                                    766 	.globl _AX5043_PINFUNCPWRAMP
                                    767 	.globl _AX5043_PINFUNCIRQ
                                    768 	.globl _AX5043_PINFUNCDCLK
                                    769 	.globl _AX5043_PINFUNCDATA
                                    770 	.globl _AX5043_PINFUNCANTSEL
                                    771 	.globl _AX5043_MODULATION
                                    772 	.globl _AX5043_MODCFGP
                                    773 	.globl _AX5043_MODCFGF
                                    774 	.globl _AX5043_MODCFGA
                                    775 	.globl _AX5043_MAXRFOFFSET2
                                    776 	.globl _AX5043_MAXRFOFFSET1
                                    777 	.globl _AX5043_MAXRFOFFSET0
                                    778 	.globl _AX5043_MAXDROFFSET2
                                    779 	.globl _AX5043_MAXDROFFSET1
                                    780 	.globl _AX5043_MAXDROFFSET0
                                    781 	.globl _AX5043_MATCH1PAT1
                                    782 	.globl _AX5043_MATCH1PAT0
                                    783 	.globl _AX5043_MATCH1MIN
                                    784 	.globl _AX5043_MATCH1MAX
                                    785 	.globl _AX5043_MATCH1LEN
                                    786 	.globl _AX5043_MATCH0PAT3
                                    787 	.globl _AX5043_MATCH0PAT2
                                    788 	.globl _AX5043_MATCH0PAT1
                                    789 	.globl _AX5043_MATCH0PAT0
                                    790 	.globl _AX5043_MATCH0MIN
                                    791 	.globl _AX5043_MATCH0MAX
                                    792 	.globl _AX5043_MATCH0LEN
                                    793 	.globl _AX5043_LPOSCSTATUS
                                    794 	.globl _AX5043_LPOSCREF1
                                    795 	.globl _AX5043_LPOSCREF0
                                    796 	.globl _AX5043_LPOSCPER1
                                    797 	.globl _AX5043_LPOSCPER0
                                    798 	.globl _AX5043_LPOSCKFILT1
                                    799 	.globl _AX5043_LPOSCKFILT0
                                    800 	.globl _AX5043_LPOSCFREQ1
                                    801 	.globl _AX5043_LPOSCFREQ0
                                    802 	.globl _AX5043_LPOSCCONFIG
                                    803 	.globl _AX5043_IRQREQUEST1
                                    804 	.globl _AX5043_IRQREQUEST0
                                    805 	.globl _AX5043_IRQMASK1
                                    806 	.globl _AX5043_IRQMASK0
                                    807 	.globl _AX5043_IRQINVERSION1
                                    808 	.globl _AX5043_IRQINVERSION0
                                    809 	.globl _AX5043_IFFREQ1
                                    810 	.globl _AX5043_IFFREQ0
                                    811 	.globl _AX5043_GPADCPERIOD
                                    812 	.globl _AX5043_GPADCCTRL
                                    813 	.globl _AX5043_GPADC13VALUE1
                                    814 	.globl _AX5043_GPADC13VALUE0
                                    815 	.globl _AX5043_FSKDMIN1
                                    816 	.globl _AX5043_FSKDMIN0
                                    817 	.globl _AX5043_FSKDMAX1
                                    818 	.globl _AX5043_FSKDMAX0
                                    819 	.globl _AX5043_FSKDEV2
                                    820 	.globl _AX5043_FSKDEV1
                                    821 	.globl _AX5043_FSKDEV0
                                    822 	.globl _AX5043_FREQB3
                                    823 	.globl _AX5043_FREQB2
                                    824 	.globl _AX5043_FREQB1
                                    825 	.globl _AX5043_FREQB0
                                    826 	.globl _AX5043_FREQA3
                                    827 	.globl _AX5043_FREQA2
                                    828 	.globl _AX5043_FREQA1
                                    829 	.globl _AX5043_FREQA0
                                    830 	.globl _AX5043_FRAMING
                                    831 	.globl _AX5043_FIFOTHRESH1
                                    832 	.globl _AX5043_FIFOTHRESH0
                                    833 	.globl _AX5043_FIFOSTAT
                                    834 	.globl _AX5043_FIFOFREE1
                                    835 	.globl _AX5043_FIFOFREE0
                                    836 	.globl _AX5043_FIFODATA
                                    837 	.globl _AX5043_FIFOCOUNT1
                                    838 	.globl _AX5043_FIFOCOUNT0
                                    839 	.globl _AX5043_FECSYNC
                                    840 	.globl _AX5043_FECSTATUS
                                    841 	.globl _AX5043_FEC
                                    842 	.globl _AX5043_ENCODING
                                    843 	.globl _AX5043_DIVERSITY
                                    844 	.globl _AX5043_DECIMATION
                                    845 	.globl _AX5043_DACVALUE1
                                    846 	.globl _AX5043_DACVALUE0
                                    847 	.globl _AX5043_DACCONFIG
                                    848 	.globl _AX5043_CRCINIT3
                                    849 	.globl _AX5043_CRCINIT2
                                    850 	.globl _AX5043_CRCINIT1
                                    851 	.globl _AX5043_CRCINIT0
                                    852 	.globl _AX5043_BGNDRSSITHR
                                    853 	.globl _AX5043_BGNDRSSIGAIN
                                    854 	.globl _AX5043_BGNDRSSI
                                    855 	.globl _AX5043_BBTUNE
                                    856 	.globl _AX5043_BBOFFSCAP
                                    857 	.globl _AX5043_AMPLFILTER
                                    858 	.globl _AX5043_AGCCOUNTER
                                    859 	.globl _AX5043_AFSKSPACE1
                                    860 	.globl _AX5043_AFSKSPACE0
                                    861 	.globl _AX5043_AFSKMARK1
                                    862 	.globl _AX5043_AFSKMARK0
                                    863 	.globl _AX5043_AFSKCTRL
                                    864 	.globl _XWTSTAT
                                    865 	.globl _XWTIRQEN
                                    866 	.globl _XWTEVTD
                                    867 	.globl _XWTEVTD1
                                    868 	.globl _XWTEVTD0
                                    869 	.globl _XWTEVTC
                                    870 	.globl _XWTEVTC1
                                    871 	.globl _XWTEVTC0
                                    872 	.globl _XWTEVTB
                                    873 	.globl _XWTEVTB1
                                    874 	.globl _XWTEVTB0
                                    875 	.globl _XWTEVTA
                                    876 	.globl _XWTEVTA1
                                    877 	.globl _XWTEVTA0
                                    878 	.globl _XWTCNTR1
                                    879 	.globl _XWTCNTB
                                    880 	.globl _XWTCNTB1
                                    881 	.globl _XWTCNTB0
                                    882 	.globl _XWTCNTA
                                    883 	.globl _XWTCNTA1
                                    884 	.globl _XWTCNTA0
                                    885 	.globl _XWTCFGB
                                    886 	.globl _XWTCFGA
                                    887 	.globl _XWDTRESET
                                    888 	.globl _XWDTCFG
                                    889 	.globl _XU1STATUS
                                    890 	.globl _XU1SHREG
                                    891 	.globl _XU1MODE
                                    892 	.globl _XU1CTRL
                                    893 	.globl _XU0STATUS
                                    894 	.globl _XU0SHREG
                                    895 	.globl _XU0MODE
                                    896 	.globl _XU0CTRL
                                    897 	.globl _XT2STATUS
                                    898 	.globl _XT2PERIOD
                                    899 	.globl _XT2PERIOD1
                                    900 	.globl _XT2PERIOD0
                                    901 	.globl _XT2MODE
                                    902 	.globl _XT2CNT
                                    903 	.globl _XT2CNT1
                                    904 	.globl _XT2CNT0
                                    905 	.globl _XT2CLKSRC
                                    906 	.globl _XT1STATUS
                                    907 	.globl _XT1PERIOD
                                    908 	.globl _XT1PERIOD1
                                    909 	.globl _XT1PERIOD0
                                    910 	.globl _XT1MODE
                                    911 	.globl _XT1CNT
                                    912 	.globl _XT1CNT1
                                    913 	.globl _XT1CNT0
                                    914 	.globl _XT1CLKSRC
                                    915 	.globl _XT0STATUS
                                    916 	.globl _XT0PERIOD
                                    917 	.globl _XT0PERIOD1
                                    918 	.globl _XT0PERIOD0
                                    919 	.globl _XT0MODE
                                    920 	.globl _XT0CNT
                                    921 	.globl _XT0CNT1
                                    922 	.globl _XT0CNT0
                                    923 	.globl _XT0CLKSRC
                                    924 	.globl _XSPSTATUS
                                    925 	.globl _XSPSHREG
                                    926 	.globl _XSPMODE
                                    927 	.globl _XSPCLKSRC
                                    928 	.globl _XRADIOSTAT
                                    929 	.globl _XRADIOSTAT1
                                    930 	.globl _XRADIOSTAT0
                                    931 	.globl _XRADIODATA3
                                    932 	.globl _XRADIODATA2
                                    933 	.globl _XRADIODATA1
                                    934 	.globl _XRADIODATA0
                                    935 	.globl _XRADIOADDR1
                                    936 	.globl _XRADIOADDR0
                                    937 	.globl _XRADIOACC
                                    938 	.globl _XOC1STATUS
                                    939 	.globl _XOC1PIN
                                    940 	.globl _XOC1MODE
                                    941 	.globl _XOC1COMP
                                    942 	.globl _XOC1COMP1
                                    943 	.globl _XOC1COMP0
                                    944 	.globl _XOC0STATUS
                                    945 	.globl _XOC0PIN
                                    946 	.globl _XOC0MODE
                                    947 	.globl _XOC0COMP
                                    948 	.globl _XOC0COMP1
                                    949 	.globl _XOC0COMP0
                                    950 	.globl _XNVSTATUS
                                    951 	.globl _XNVKEY
                                    952 	.globl _XNVDATA
                                    953 	.globl _XNVDATA1
                                    954 	.globl _XNVDATA0
                                    955 	.globl _XNVADDR
                                    956 	.globl _XNVADDR1
                                    957 	.globl _XNVADDR0
                                    958 	.globl _XIC1STATUS
                                    959 	.globl _XIC1MODE
                                    960 	.globl _XIC1CAPT
                                    961 	.globl _XIC1CAPT1
                                    962 	.globl _XIC1CAPT0
                                    963 	.globl _XIC0STATUS
                                    964 	.globl _XIC0MODE
                                    965 	.globl _XIC0CAPT
                                    966 	.globl _XIC0CAPT1
                                    967 	.globl _XIC0CAPT0
                                    968 	.globl _XPORTR
                                    969 	.globl _XPORTC
                                    970 	.globl _XPORTB
                                    971 	.globl _XPORTA
                                    972 	.globl _XPINR
                                    973 	.globl _XPINC
                                    974 	.globl _XPINB
                                    975 	.globl _XPINA
                                    976 	.globl _XDIRR
                                    977 	.globl _XDIRC
                                    978 	.globl _XDIRB
                                    979 	.globl _XDIRA
                                    980 	.globl _XDBGLNKSTAT
                                    981 	.globl _XDBGLNKBUF
                                    982 	.globl _XCODECONFIG
                                    983 	.globl _XCLKSTAT
                                    984 	.globl _XCLKCON
                                    985 	.globl _XANALOGCOMP
                                    986 	.globl _XADCCONV
                                    987 	.globl _XADCCLKSRC
                                    988 	.globl _XADCCH3CONFIG
                                    989 	.globl _XADCCH2CONFIG
                                    990 	.globl _XADCCH1CONFIG
                                    991 	.globl _XADCCH0CONFIG
                                    992 	.globl _XPCON
                                    993 	.globl _XIP
                                    994 	.globl _XIE
                                    995 	.globl _XDPTR1
                                    996 	.globl _XDPTR0
                                    997 	.globl _XTALREADY
                                    998 	.globl _XTALOSC
                                    999 	.globl _XTALAMPL
                                   1000 	.globl _SILICONREV
                                   1001 	.globl _SCRATCH3
                                   1002 	.globl _SCRATCH2
                                   1003 	.globl _SCRATCH1
                                   1004 	.globl _SCRATCH0
                                   1005 	.globl _RADIOMUX
                                   1006 	.globl _RADIOFSTATADDR
                                   1007 	.globl _RADIOFSTATADDR1
                                   1008 	.globl _RADIOFSTATADDR0
                                   1009 	.globl _RADIOFDATAADDR
                                   1010 	.globl _RADIOFDATAADDR1
                                   1011 	.globl _RADIOFDATAADDR0
                                   1012 	.globl _OSCRUN
                                   1013 	.globl _OSCREADY
                                   1014 	.globl _OSCFORCERUN
                                   1015 	.globl _OSCCALIB
                                   1016 	.globl _MISCCTRL
                                   1017 	.globl _LPXOSCGM
                                   1018 	.globl _LPOSCREF
                                   1019 	.globl _LPOSCREF1
                                   1020 	.globl _LPOSCREF0
                                   1021 	.globl _LPOSCPER
                                   1022 	.globl _LPOSCPER1
                                   1023 	.globl _LPOSCPER0
                                   1024 	.globl _LPOSCKFILT
                                   1025 	.globl _LPOSCKFILT1
                                   1026 	.globl _LPOSCKFILT0
                                   1027 	.globl _LPOSCFREQ
                                   1028 	.globl _LPOSCFREQ1
                                   1029 	.globl _LPOSCFREQ0
                                   1030 	.globl _LPOSCCONFIG
                                   1031 	.globl _PINSEL
                                   1032 	.globl _PINCHGC
                                   1033 	.globl _PINCHGB
                                   1034 	.globl _PINCHGA
                                   1035 	.globl _PALTRADIO
                                   1036 	.globl _PALTC
                                   1037 	.globl _PALTB
                                   1038 	.globl _PALTA
                                   1039 	.globl _INTCHGC
                                   1040 	.globl _INTCHGB
                                   1041 	.globl _INTCHGA
                                   1042 	.globl _EXTIRQ
                                   1043 	.globl _GPIOENABLE
                                   1044 	.globl _ANALOGA
                                   1045 	.globl _FRCOSCREF
                                   1046 	.globl _FRCOSCREF1
                                   1047 	.globl _FRCOSCREF0
                                   1048 	.globl _FRCOSCPER
                                   1049 	.globl _FRCOSCPER1
                                   1050 	.globl _FRCOSCPER0
                                   1051 	.globl _FRCOSCKFILT
                                   1052 	.globl _FRCOSCKFILT1
                                   1053 	.globl _FRCOSCKFILT0
                                   1054 	.globl _FRCOSCFREQ
                                   1055 	.globl _FRCOSCFREQ1
                                   1056 	.globl _FRCOSCFREQ0
                                   1057 	.globl _FRCOSCCTRL
                                   1058 	.globl _FRCOSCCONFIG
                                   1059 	.globl _DMA1CONFIG
                                   1060 	.globl _DMA1ADDR
                                   1061 	.globl _DMA1ADDR1
                                   1062 	.globl _DMA1ADDR0
                                   1063 	.globl _DMA0CONFIG
                                   1064 	.globl _DMA0ADDR
                                   1065 	.globl _DMA0ADDR1
                                   1066 	.globl _DMA0ADDR0
                                   1067 	.globl _ADCTUNE2
                                   1068 	.globl _ADCTUNE1
                                   1069 	.globl _ADCTUNE0
                                   1070 	.globl _ADCCH3VAL
                                   1071 	.globl _ADCCH3VAL1
                                   1072 	.globl _ADCCH3VAL0
                                   1073 	.globl _ADCCH2VAL
                                   1074 	.globl _ADCCH2VAL1
                                   1075 	.globl _ADCCH2VAL0
                                   1076 	.globl _ADCCH1VAL
                                   1077 	.globl _ADCCH1VAL1
                                   1078 	.globl _ADCCH1VAL0
                                   1079 	.globl _ADCCH0VAL
                                   1080 	.globl _ADCCH0VAL1
                                   1081 	.globl _ADCCH0VAL0
                                   1082 	.globl _aligned_alloc_PARM_2
                                   1083 	.globl _UART_Proc_PortInit
                                   1084 	.globl _UART_Proc_VerifyIncomingMsg
                                   1085 	.globl _UART_Proc_ModifiyKissSpecialCharacters
                                   1086 	.globl _UART_Proc_SendMessage
                                   1087 	.globl _UART_Calc_CRC32
                                   1088 ;--------------------------------------------------------
                                   1089 ; special function registers
                                   1090 ;--------------------------------------------------------
                                   1091 	.area RSEG    (ABS,DATA)
      000000                       1092 	.org 0x0000
                           0000E0  1093 _ACC	=	0x00e0
                           0000F0  1094 _B	=	0x00f0
                           000083  1095 _DPH	=	0x0083
                           000085  1096 _DPH1	=	0x0085
                           000082  1097 _DPL	=	0x0082
                           000084  1098 _DPL1	=	0x0084
                           008382  1099 _DPTR0	=	0x8382
                           008584  1100 _DPTR1	=	0x8584
                           000086  1101 _DPS	=	0x0086
                           0000A0  1102 _E2IE	=	0x00a0
                           0000C0  1103 _E2IP	=	0x00c0
                           000098  1104 _EIE	=	0x0098
                           0000B0  1105 _EIP	=	0x00b0
                           0000A8  1106 _IE	=	0x00a8
                           0000B8  1107 _IP	=	0x00b8
                           000087  1108 _PCON	=	0x0087
                           0000D0  1109 _PSW	=	0x00d0
                           000081  1110 _SP	=	0x0081
                           0000D9  1111 _XPAGE	=	0x00d9
                           0000D9  1112 __XPAGE	=	0x00d9
                           0000CA  1113 _ADCCH0CONFIG	=	0x00ca
                           0000CB  1114 _ADCCH1CONFIG	=	0x00cb
                           0000D2  1115 _ADCCH2CONFIG	=	0x00d2
                           0000D3  1116 _ADCCH3CONFIG	=	0x00d3
                           0000D1  1117 _ADCCLKSRC	=	0x00d1
                           0000C9  1118 _ADCCONV	=	0x00c9
                           0000E1  1119 _ANALOGCOMP	=	0x00e1
                           0000C6  1120 _CLKCON	=	0x00c6
                           0000C7  1121 _CLKSTAT	=	0x00c7
                           000097  1122 _CODECONFIG	=	0x0097
                           0000E3  1123 _DBGLNKBUF	=	0x00e3
                           0000E2  1124 _DBGLNKSTAT	=	0x00e2
                           000089  1125 _DIRA	=	0x0089
                           00008A  1126 _DIRB	=	0x008a
                           00008B  1127 _DIRC	=	0x008b
                           00008E  1128 _DIRR	=	0x008e
                           0000C8  1129 _PINA	=	0x00c8
                           0000E8  1130 _PINB	=	0x00e8
                           0000F8  1131 _PINC	=	0x00f8
                           00008D  1132 _PINR	=	0x008d
                           000080  1133 _PORTA	=	0x0080
                           000088  1134 _PORTB	=	0x0088
                           000090  1135 _PORTC	=	0x0090
                           00008C  1136 _PORTR	=	0x008c
                           0000CE  1137 _IC0CAPT0	=	0x00ce
                           0000CF  1138 _IC0CAPT1	=	0x00cf
                           00CFCE  1139 _IC0CAPT	=	0xcfce
                           0000CC  1140 _IC0MODE	=	0x00cc
                           0000CD  1141 _IC0STATUS	=	0x00cd
                           0000D6  1142 _IC1CAPT0	=	0x00d6
                           0000D7  1143 _IC1CAPT1	=	0x00d7
                           00D7D6  1144 _IC1CAPT	=	0xd7d6
                           0000D4  1145 _IC1MODE	=	0x00d4
                           0000D5  1146 _IC1STATUS	=	0x00d5
                           000092  1147 _NVADDR0	=	0x0092
                           000093  1148 _NVADDR1	=	0x0093
                           009392  1149 _NVADDR	=	0x9392
                           000094  1150 _NVDATA0	=	0x0094
                           000095  1151 _NVDATA1	=	0x0095
                           009594  1152 _NVDATA	=	0x9594
                           000096  1153 _NVKEY	=	0x0096
                           000091  1154 _NVSTATUS	=	0x0091
                           0000BC  1155 _OC0COMP0	=	0x00bc
                           0000BD  1156 _OC0COMP1	=	0x00bd
                           00BDBC  1157 _OC0COMP	=	0xbdbc
                           0000B9  1158 _OC0MODE	=	0x00b9
                           0000BA  1159 _OC0PIN	=	0x00ba
                           0000BB  1160 _OC0STATUS	=	0x00bb
                           0000C4  1161 _OC1COMP0	=	0x00c4
                           0000C5  1162 _OC1COMP1	=	0x00c5
                           00C5C4  1163 _OC1COMP	=	0xc5c4
                           0000C1  1164 _OC1MODE	=	0x00c1
                           0000C2  1165 _OC1PIN	=	0x00c2
                           0000C3  1166 _OC1STATUS	=	0x00c3
                           0000B1  1167 _RADIOACC	=	0x00b1
                           0000B3  1168 _RADIOADDR0	=	0x00b3
                           0000B2  1169 _RADIOADDR1	=	0x00b2
                           00B2B3  1170 _RADIOADDR	=	0xb2b3
                           0000B7  1171 _RADIODATA0	=	0x00b7
                           0000B6  1172 _RADIODATA1	=	0x00b6
                           0000B5  1173 _RADIODATA2	=	0x00b5
                           0000B4  1174 _RADIODATA3	=	0x00b4
                           B4B5B6B7  1175 _RADIODATA	=	0xb4b5b6b7
                           0000BE  1176 _RADIOSTAT0	=	0x00be
                           0000BF  1177 _RADIOSTAT1	=	0x00bf
                           00BFBE  1178 _RADIOSTAT	=	0xbfbe
                           0000DF  1179 _SPCLKSRC	=	0x00df
                           0000DC  1180 _SPMODE	=	0x00dc
                           0000DE  1181 _SPSHREG	=	0x00de
                           0000DD  1182 _SPSTATUS	=	0x00dd
                           00009A  1183 _T0CLKSRC	=	0x009a
                           00009C  1184 _T0CNT0	=	0x009c
                           00009D  1185 _T0CNT1	=	0x009d
                           009D9C  1186 _T0CNT	=	0x9d9c
                           000099  1187 _T0MODE	=	0x0099
                           00009E  1188 _T0PERIOD0	=	0x009e
                           00009F  1189 _T0PERIOD1	=	0x009f
                           009F9E  1190 _T0PERIOD	=	0x9f9e
                           00009B  1191 _T0STATUS	=	0x009b
                           0000A2  1192 _T1CLKSRC	=	0x00a2
                           0000A4  1193 _T1CNT0	=	0x00a4
                           0000A5  1194 _T1CNT1	=	0x00a5
                           00A5A4  1195 _T1CNT	=	0xa5a4
                           0000A1  1196 _T1MODE	=	0x00a1
                           0000A6  1197 _T1PERIOD0	=	0x00a6
                           0000A7  1198 _T1PERIOD1	=	0x00a7
                           00A7A6  1199 _T1PERIOD	=	0xa7a6
                           0000A3  1200 _T1STATUS	=	0x00a3
                           0000AA  1201 _T2CLKSRC	=	0x00aa
                           0000AC  1202 _T2CNT0	=	0x00ac
                           0000AD  1203 _T2CNT1	=	0x00ad
                           00ADAC  1204 _T2CNT	=	0xadac
                           0000A9  1205 _T2MODE	=	0x00a9
                           0000AE  1206 _T2PERIOD0	=	0x00ae
                           0000AF  1207 _T2PERIOD1	=	0x00af
                           00AFAE  1208 _T2PERIOD	=	0xafae
                           0000AB  1209 _T2STATUS	=	0x00ab
                           0000E4  1210 _U0CTRL	=	0x00e4
                           0000E7  1211 _U0MODE	=	0x00e7
                           0000E6  1212 _U0SHREG	=	0x00e6
                           0000E5  1213 _U0STATUS	=	0x00e5
                           0000EC  1214 _U1CTRL	=	0x00ec
                           0000EF  1215 _U1MODE	=	0x00ef
                           0000EE  1216 _U1SHREG	=	0x00ee
                           0000ED  1217 _U1STATUS	=	0x00ed
                           0000DA  1218 _WDTCFG	=	0x00da
                           0000DB  1219 _WDTRESET	=	0x00db
                           0000F1  1220 _WTCFGA	=	0x00f1
                           0000F9  1221 _WTCFGB	=	0x00f9
                           0000F2  1222 _WTCNTA0	=	0x00f2
                           0000F3  1223 _WTCNTA1	=	0x00f3
                           00F3F2  1224 _WTCNTA	=	0xf3f2
                           0000FA  1225 _WTCNTB0	=	0x00fa
                           0000FB  1226 _WTCNTB1	=	0x00fb
                           00FBFA  1227 _WTCNTB	=	0xfbfa
                           0000EB  1228 _WTCNTR1	=	0x00eb
                           0000F4  1229 _WTEVTA0	=	0x00f4
                           0000F5  1230 _WTEVTA1	=	0x00f5
                           00F5F4  1231 _WTEVTA	=	0xf5f4
                           0000F6  1232 _WTEVTB0	=	0x00f6
                           0000F7  1233 _WTEVTB1	=	0x00f7
                           00F7F6  1234 _WTEVTB	=	0xf7f6
                           0000FC  1235 _WTEVTC0	=	0x00fc
                           0000FD  1236 _WTEVTC1	=	0x00fd
                           00FDFC  1237 _WTEVTC	=	0xfdfc
                           0000FE  1238 _WTEVTD0	=	0x00fe
                           0000FF  1239 _WTEVTD1	=	0x00ff
                           00FFFE  1240 _WTEVTD	=	0xfffe
                           0000E9  1241 _WTIRQEN	=	0x00e9
                           0000EA  1242 _WTSTAT	=	0x00ea
                                   1243 ;--------------------------------------------------------
                                   1244 ; special function bits
                                   1245 ;--------------------------------------------------------
                                   1246 	.area RSEG    (ABS,DATA)
      000000                       1247 	.org 0x0000
                           0000E0  1248 _ACC_0	=	0x00e0
                           0000E1  1249 _ACC_1	=	0x00e1
                           0000E2  1250 _ACC_2	=	0x00e2
                           0000E3  1251 _ACC_3	=	0x00e3
                           0000E4  1252 _ACC_4	=	0x00e4
                           0000E5  1253 _ACC_5	=	0x00e5
                           0000E6  1254 _ACC_6	=	0x00e6
                           0000E7  1255 _ACC_7	=	0x00e7
                           0000F0  1256 _B_0	=	0x00f0
                           0000F1  1257 _B_1	=	0x00f1
                           0000F2  1258 _B_2	=	0x00f2
                           0000F3  1259 _B_3	=	0x00f3
                           0000F4  1260 _B_4	=	0x00f4
                           0000F5  1261 _B_5	=	0x00f5
                           0000F6  1262 _B_6	=	0x00f6
                           0000F7  1263 _B_7	=	0x00f7
                           0000A0  1264 _E2IE_0	=	0x00a0
                           0000A1  1265 _E2IE_1	=	0x00a1
                           0000A2  1266 _E2IE_2	=	0x00a2
                           0000A3  1267 _E2IE_3	=	0x00a3
                           0000A4  1268 _E2IE_4	=	0x00a4
                           0000A5  1269 _E2IE_5	=	0x00a5
                           0000A6  1270 _E2IE_6	=	0x00a6
                           0000A7  1271 _E2IE_7	=	0x00a7
                           0000C0  1272 _E2IP_0	=	0x00c0
                           0000C1  1273 _E2IP_1	=	0x00c1
                           0000C2  1274 _E2IP_2	=	0x00c2
                           0000C3  1275 _E2IP_3	=	0x00c3
                           0000C4  1276 _E2IP_4	=	0x00c4
                           0000C5  1277 _E2IP_5	=	0x00c5
                           0000C6  1278 _E2IP_6	=	0x00c6
                           0000C7  1279 _E2IP_7	=	0x00c7
                           000098  1280 _EIE_0	=	0x0098
                           000099  1281 _EIE_1	=	0x0099
                           00009A  1282 _EIE_2	=	0x009a
                           00009B  1283 _EIE_3	=	0x009b
                           00009C  1284 _EIE_4	=	0x009c
                           00009D  1285 _EIE_5	=	0x009d
                           00009E  1286 _EIE_6	=	0x009e
                           00009F  1287 _EIE_7	=	0x009f
                           0000B0  1288 _EIP_0	=	0x00b0
                           0000B1  1289 _EIP_1	=	0x00b1
                           0000B2  1290 _EIP_2	=	0x00b2
                           0000B3  1291 _EIP_3	=	0x00b3
                           0000B4  1292 _EIP_4	=	0x00b4
                           0000B5  1293 _EIP_5	=	0x00b5
                           0000B6  1294 _EIP_6	=	0x00b6
                           0000B7  1295 _EIP_7	=	0x00b7
                           0000A8  1296 _IE_0	=	0x00a8
                           0000A9  1297 _IE_1	=	0x00a9
                           0000AA  1298 _IE_2	=	0x00aa
                           0000AB  1299 _IE_3	=	0x00ab
                           0000AC  1300 _IE_4	=	0x00ac
                           0000AD  1301 _IE_5	=	0x00ad
                           0000AE  1302 _IE_6	=	0x00ae
                           0000AF  1303 _IE_7	=	0x00af
                           0000AF  1304 _EA	=	0x00af
                           0000B8  1305 _IP_0	=	0x00b8
                           0000B9  1306 _IP_1	=	0x00b9
                           0000BA  1307 _IP_2	=	0x00ba
                           0000BB  1308 _IP_3	=	0x00bb
                           0000BC  1309 _IP_4	=	0x00bc
                           0000BD  1310 _IP_5	=	0x00bd
                           0000BE  1311 _IP_6	=	0x00be
                           0000BF  1312 _IP_7	=	0x00bf
                           0000D0  1313 _P	=	0x00d0
                           0000D1  1314 _F1	=	0x00d1
                           0000D2  1315 _OV	=	0x00d2
                           0000D3  1316 _RS0	=	0x00d3
                           0000D4  1317 _RS1	=	0x00d4
                           0000D5  1318 _F0	=	0x00d5
                           0000D6  1319 _AC	=	0x00d6
                           0000D7  1320 _CY	=	0x00d7
                           0000C8  1321 _PINA_0	=	0x00c8
                           0000C9  1322 _PINA_1	=	0x00c9
                           0000CA  1323 _PINA_2	=	0x00ca
                           0000CB  1324 _PINA_3	=	0x00cb
                           0000CC  1325 _PINA_4	=	0x00cc
                           0000CD  1326 _PINA_5	=	0x00cd
                           0000CE  1327 _PINA_6	=	0x00ce
                           0000CF  1328 _PINA_7	=	0x00cf
                           0000E8  1329 _PINB_0	=	0x00e8
                           0000E9  1330 _PINB_1	=	0x00e9
                           0000EA  1331 _PINB_2	=	0x00ea
                           0000EB  1332 _PINB_3	=	0x00eb
                           0000EC  1333 _PINB_4	=	0x00ec
                           0000ED  1334 _PINB_5	=	0x00ed
                           0000EE  1335 _PINB_6	=	0x00ee
                           0000EF  1336 _PINB_7	=	0x00ef
                           0000F8  1337 _PINC_0	=	0x00f8
                           0000F9  1338 _PINC_1	=	0x00f9
                           0000FA  1339 _PINC_2	=	0x00fa
                           0000FB  1340 _PINC_3	=	0x00fb
                           0000FC  1341 _PINC_4	=	0x00fc
                           0000FD  1342 _PINC_5	=	0x00fd
                           0000FE  1343 _PINC_6	=	0x00fe
                           0000FF  1344 _PINC_7	=	0x00ff
                           000080  1345 _PORTA_0	=	0x0080
                           000081  1346 _PORTA_1	=	0x0081
                           000082  1347 _PORTA_2	=	0x0082
                           000083  1348 _PORTA_3	=	0x0083
                           000084  1349 _PORTA_4	=	0x0084
                           000085  1350 _PORTA_5	=	0x0085
                           000086  1351 _PORTA_6	=	0x0086
                           000087  1352 _PORTA_7	=	0x0087
                           000088  1353 _PORTB_0	=	0x0088
                           000089  1354 _PORTB_1	=	0x0089
                           00008A  1355 _PORTB_2	=	0x008a
                           00008B  1356 _PORTB_3	=	0x008b
                           00008C  1357 _PORTB_4	=	0x008c
                           00008D  1358 _PORTB_5	=	0x008d
                           00008E  1359 _PORTB_6	=	0x008e
                           00008F  1360 _PORTB_7	=	0x008f
                           000090  1361 _PORTC_0	=	0x0090
                           000091  1362 _PORTC_1	=	0x0091
                           000092  1363 _PORTC_2	=	0x0092
                           000093  1364 _PORTC_3	=	0x0093
                           000094  1365 _PORTC_4	=	0x0094
                           000095  1366 _PORTC_5	=	0x0095
                           000096  1367 _PORTC_6	=	0x0096
                           000097  1368 _PORTC_7	=	0x0097
                                   1369 ;--------------------------------------------------------
                                   1370 ; overlayable register banks
                                   1371 ;--------------------------------------------------------
                                   1372 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                       1373 	.ds 8
                                   1374 ;--------------------------------------------------------
                                   1375 ; internal ram data
                                   1376 ;--------------------------------------------------------
                                   1377 	.area DSEG    (DATA)
      000022                       1378 _UART_Proc_VerifyIncomingMsg_sloc0_1_0:
      000022                       1379 	.ds 4
      000026                       1380 _UART_Proc_SendMessage_sloc0_1_0:
      000026                       1381 	.ds 3
                                   1382 ;--------------------------------------------------------
                                   1383 ; overlayable items in internal ram 
                                   1384 ;--------------------------------------------------------
                                   1385 	.area	OSEG    (OVR,DATA)
      000043                       1386 _UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0:
      000043                       1387 	.ds 3
      000046                       1388 _UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0:
      000046                       1389 	.ds 3
      000049                       1390 _UART_Proc_ModifiyKissSpecialCharacters_sloc2_1_0:
      000049                       1391 	.ds 1
      00004A                       1392 _UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0:
      00004A                       1393 	.ds 3
                                   1394 	.area	OSEG    (OVR,DATA)
      000043                       1395 _UART_Calc_CRC32_sloc0_1_0:
      000043                       1396 	.ds 3
      000046                       1397 _UART_Calc_CRC32_sloc1_1_0:
      000046                       1398 	.ds 4
      00004A                       1399 _UART_Calc_CRC32_sloc2_1_0:
      00004A                       1400 	.ds 2
                                   1401 ;--------------------------------------------------------
                                   1402 ; indirectly addressable internal ram data
                                   1403 ;--------------------------------------------------------
                                   1404 	.area ISEG    (DATA)
                                   1405 ;--------------------------------------------------------
                                   1406 ; absolute internal ram data
                                   1407 ;--------------------------------------------------------
                                   1408 	.area IABS    (ABS,DATA)
                                   1409 	.area IABS    (ABS,DATA)
                                   1410 ;--------------------------------------------------------
                                   1411 ; bit data
                                   1412 ;--------------------------------------------------------
                                   1413 	.area BSEG    (BIT)
                                   1414 ;--------------------------------------------------------
                                   1415 ; paged external ram data
                                   1416 ;--------------------------------------------------------
                                   1417 	.area PSEG    (PAG,XDATA)
                                   1418 ;--------------------------------------------------------
                                   1419 ; external ram data
                                   1420 ;--------------------------------------------------------
                                   1421 	.area XSEG    (XDATA)
      000381                       1422 _aligned_alloc_PARM_2:
      000381                       1423 	.ds 2
                           007020  1424 _ADCCH0VAL0	=	0x7020
                           007021  1425 _ADCCH0VAL1	=	0x7021
                           007020  1426 _ADCCH0VAL	=	0x7020
                           007022  1427 _ADCCH1VAL0	=	0x7022
                           007023  1428 _ADCCH1VAL1	=	0x7023
                           007022  1429 _ADCCH1VAL	=	0x7022
                           007024  1430 _ADCCH2VAL0	=	0x7024
                           007025  1431 _ADCCH2VAL1	=	0x7025
                           007024  1432 _ADCCH2VAL	=	0x7024
                           007026  1433 _ADCCH3VAL0	=	0x7026
                           007027  1434 _ADCCH3VAL1	=	0x7027
                           007026  1435 _ADCCH3VAL	=	0x7026
                           007028  1436 _ADCTUNE0	=	0x7028
                           007029  1437 _ADCTUNE1	=	0x7029
                           00702A  1438 _ADCTUNE2	=	0x702a
                           007010  1439 _DMA0ADDR0	=	0x7010
                           007011  1440 _DMA0ADDR1	=	0x7011
                           007010  1441 _DMA0ADDR	=	0x7010
                           007014  1442 _DMA0CONFIG	=	0x7014
                           007012  1443 _DMA1ADDR0	=	0x7012
                           007013  1444 _DMA1ADDR1	=	0x7013
                           007012  1445 _DMA1ADDR	=	0x7012
                           007015  1446 _DMA1CONFIG	=	0x7015
                           007070  1447 _FRCOSCCONFIG	=	0x7070
                           007071  1448 _FRCOSCCTRL	=	0x7071
                           007076  1449 _FRCOSCFREQ0	=	0x7076
                           007077  1450 _FRCOSCFREQ1	=	0x7077
                           007076  1451 _FRCOSCFREQ	=	0x7076
                           007072  1452 _FRCOSCKFILT0	=	0x7072
                           007073  1453 _FRCOSCKFILT1	=	0x7073
                           007072  1454 _FRCOSCKFILT	=	0x7072
                           007078  1455 _FRCOSCPER0	=	0x7078
                           007079  1456 _FRCOSCPER1	=	0x7079
                           007078  1457 _FRCOSCPER	=	0x7078
                           007074  1458 _FRCOSCREF0	=	0x7074
                           007075  1459 _FRCOSCREF1	=	0x7075
                           007074  1460 _FRCOSCREF	=	0x7074
                           007007  1461 _ANALOGA	=	0x7007
                           00700C  1462 _GPIOENABLE	=	0x700c
                           007003  1463 _EXTIRQ	=	0x7003
                           007000  1464 _INTCHGA	=	0x7000
                           007001  1465 _INTCHGB	=	0x7001
                           007002  1466 _INTCHGC	=	0x7002
                           007008  1467 _PALTA	=	0x7008
                           007009  1468 _PALTB	=	0x7009
                           00700A  1469 _PALTC	=	0x700a
                           007046  1470 _PALTRADIO	=	0x7046
                           007004  1471 _PINCHGA	=	0x7004
                           007005  1472 _PINCHGB	=	0x7005
                           007006  1473 _PINCHGC	=	0x7006
                           00700B  1474 _PINSEL	=	0x700b
                           007060  1475 _LPOSCCONFIG	=	0x7060
                           007066  1476 _LPOSCFREQ0	=	0x7066
                           007067  1477 _LPOSCFREQ1	=	0x7067
                           007066  1478 _LPOSCFREQ	=	0x7066
                           007062  1479 _LPOSCKFILT0	=	0x7062
                           007063  1480 _LPOSCKFILT1	=	0x7063
                           007062  1481 _LPOSCKFILT	=	0x7062
                           007068  1482 _LPOSCPER0	=	0x7068
                           007069  1483 _LPOSCPER1	=	0x7069
                           007068  1484 _LPOSCPER	=	0x7068
                           007064  1485 _LPOSCREF0	=	0x7064
                           007065  1486 _LPOSCREF1	=	0x7065
                           007064  1487 _LPOSCREF	=	0x7064
                           007054  1488 _LPXOSCGM	=	0x7054
                           007F01  1489 _MISCCTRL	=	0x7f01
                           007053  1490 _OSCCALIB	=	0x7053
                           007050  1491 _OSCFORCERUN	=	0x7050
                           007052  1492 _OSCREADY	=	0x7052
                           007051  1493 _OSCRUN	=	0x7051
                           007040  1494 _RADIOFDATAADDR0	=	0x7040
                           007041  1495 _RADIOFDATAADDR1	=	0x7041
                           007040  1496 _RADIOFDATAADDR	=	0x7040
                           007042  1497 _RADIOFSTATADDR0	=	0x7042
                           007043  1498 _RADIOFSTATADDR1	=	0x7043
                           007042  1499 _RADIOFSTATADDR	=	0x7042
                           007044  1500 _RADIOMUX	=	0x7044
                           007084  1501 _SCRATCH0	=	0x7084
                           007085  1502 _SCRATCH1	=	0x7085
                           007086  1503 _SCRATCH2	=	0x7086
                           007087  1504 _SCRATCH3	=	0x7087
                           007F00  1505 _SILICONREV	=	0x7f00
                           007F19  1506 _XTALAMPL	=	0x7f19
                           007F18  1507 _XTALOSC	=	0x7f18
                           007F1A  1508 _XTALREADY	=	0x7f1a
                           003F82  1509 _XDPTR0	=	0x3f82
                           003F84  1510 _XDPTR1	=	0x3f84
                           003FA8  1511 _XIE	=	0x3fa8
                           003FB8  1512 _XIP	=	0x3fb8
                           003F87  1513 _XPCON	=	0x3f87
                           003FCA  1514 _XADCCH0CONFIG	=	0x3fca
                           003FCB  1515 _XADCCH1CONFIG	=	0x3fcb
                           003FD2  1516 _XADCCH2CONFIG	=	0x3fd2
                           003FD3  1517 _XADCCH3CONFIG	=	0x3fd3
                           003FD1  1518 _XADCCLKSRC	=	0x3fd1
                           003FC9  1519 _XADCCONV	=	0x3fc9
                           003FE1  1520 _XANALOGCOMP	=	0x3fe1
                           003FC6  1521 _XCLKCON	=	0x3fc6
                           003FC7  1522 _XCLKSTAT	=	0x3fc7
                           003F97  1523 _XCODECONFIG	=	0x3f97
                           003FE3  1524 _XDBGLNKBUF	=	0x3fe3
                           003FE2  1525 _XDBGLNKSTAT	=	0x3fe2
                           003F89  1526 _XDIRA	=	0x3f89
                           003F8A  1527 _XDIRB	=	0x3f8a
                           003F8B  1528 _XDIRC	=	0x3f8b
                           003F8E  1529 _XDIRR	=	0x3f8e
                           003FC8  1530 _XPINA	=	0x3fc8
                           003FE8  1531 _XPINB	=	0x3fe8
                           003FF8  1532 _XPINC	=	0x3ff8
                           003F8D  1533 _XPINR	=	0x3f8d
                           003F80  1534 _XPORTA	=	0x3f80
                           003F88  1535 _XPORTB	=	0x3f88
                           003F90  1536 _XPORTC	=	0x3f90
                           003F8C  1537 _XPORTR	=	0x3f8c
                           003FCE  1538 _XIC0CAPT0	=	0x3fce
                           003FCF  1539 _XIC0CAPT1	=	0x3fcf
                           003FCE  1540 _XIC0CAPT	=	0x3fce
                           003FCC  1541 _XIC0MODE	=	0x3fcc
                           003FCD  1542 _XIC0STATUS	=	0x3fcd
                           003FD6  1543 _XIC1CAPT0	=	0x3fd6
                           003FD7  1544 _XIC1CAPT1	=	0x3fd7
                           003FD6  1545 _XIC1CAPT	=	0x3fd6
                           003FD4  1546 _XIC1MODE	=	0x3fd4
                           003FD5  1547 _XIC1STATUS	=	0x3fd5
                           003F92  1548 _XNVADDR0	=	0x3f92
                           003F93  1549 _XNVADDR1	=	0x3f93
                           003F92  1550 _XNVADDR	=	0x3f92
                           003F94  1551 _XNVDATA0	=	0x3f94
                           003F95  1552 _XNVDATA1	=	0x3f95
                           003F94  1553 _XNVDATA	=	0x3f94
                           003F96  1554 _XNVKEY	=	0x3f96
                           003F91  1555 _XNVSTATUS	=	0x3f91
                           003FBC  1556 _XOC0COMP0	=	0x3fbc
                           003FBD  1557 _XOC0COMP1	=	0x3fbd
                           003FBC  1558 _XOC0COMP	=	0x3fbc
                           003FB9  1559 _XOC0MODE	=	0x3fb9
                           003FBA  1560 _XOC0PIN	=	0x3fba
                           003FBB  1561 _XOC0STATUS	=	0x3fbb
                           003FC4  1562 _XOC1COMP0	=	0x3fc4
                           003FC5  1563 _XOC1COMP1	=	0x3fc5
                           003FC4  1564 _XOC1COMP	=	0x3fc4
                           003FC1  1565 _XOC1MODE	=	0x3fc1
                           003FC2  1566 _XOC1PIN	=	0x3fc2
                           003FC3  1567 _XOC1STATUS	=	0x3fc3
                           003FB1  1568 _XRADIOACC	=	0x3fb1
                           003FB3  1569 _XRADIOADDR0	=	0x3fb3
                           003FB2  1570 _XRADIOADDR1	=	0x3fb2
                           003FB7  1571 _XRADIODATA0	=	0x3fb7
                           003FB6  1572 _XRADIODATA1	=	0x3fb6
                           003FB5  1573 _XRADIODATA2	=	0x3fb5
                           003FB4  1574 _XRADIODATA3	=	0x3fb4
                           003FBE  1575 _XRADIOSTAT0	=	0x3fbe
                           003FBF  1576 _XRADIOSTAT1	=	0x3fbf
                           003FBE  1577 _XRADIOSTAT	=	0x3fbe
                           003FDF  1578 _XSPCLKSRC	=	0x3fdf
                           003FDC  1579 _XSPMODE	=	0x3fdc
                           003FDE  1580 _XSPSHREG	=	0x3fde
                           003FDD  1581 _XSPSTATUS	=	0x3fdd
                           003F9A  1582 _XT0CLKSRC	=	0x3f9a
                           003F9C  1583 _XT0CNT0	=	0x3f9c
                           003F9D  1584 _XT0CNT1	=	0x3f9d
                           003F9C  1585 _XT0CNT	=	0x3f9c
                           003F99  1586 _XT0MODE	=	0x3f99
                           003F9E  1587 _XT0PERIOD0	=	0x3f9e
                           003F9F  1588 _XT0PERIOD1	=	0x3f9f
                           003F9E  1589 _XT0PERIOD	=	0x3f9e
                           003F9B  1590 _XT0STATUS	=	0x3f9b
                           003FA2  1591 _XT1CLKSRC	=	0x3fa2
                           003FA4  1592 _XT1CNT0	=	0x3fa4
                           003FA5  1593 _XT1CNT1	=	0x3fa5
                           003FA4  1594 _XT1CNT	=	0x3fa4
                           003FA1  1595 _XT1MODE	=	0x3fa1
                           003FA6  1596 _XT1PERIOD0	=	0x3fa6
                           003FA7  1597 _XT1PERIOD1	=	0x3fa7
                           003FA6  1598 _XT1PERIOD	=	0x3fa6
                           003FA3  1599 _XT1STATUS	=	0x3fa3
                           003FAA  1600 _XT2CLKSRC	=	0x3faa
                           003FAC  1601 _XT2CNT0	=	0x3fac
                           003FAD  1602 _XT2CNT1	=	0x3fad
                           003FAC  1603 _XT2CNT	=	0x3fac
                           003FA9  1604 _XT2MODE	=	0x3fa9
                           003FAE  1605 _XT2PERIOD0	=	0x3fae
                           003FAF  1606 _XT2PERIOD1	=	0x3faf
                           003FAE  1607 _XT2PERIOD	=	0x3fae
                           003FAB  1608 _XT2STATUS	=	0x3fab
                           003FE4  1609 _XU0CTRL	=	0x3fe4
                           003FE7  1610 _XU0MODE	=	0x3fe7
                           003FE6  1611 _XU0SHREG	=	0x3fe6
                           003FE5  1612 _XU0STATUS	=	0x3fe5
                           003FEC  1613 _XU1CTRL	=	0x3fec
                           003FEF  1614 _XU1MODE	=	0x3fef
                           003FEE  1615 _XU1SHREG	=	0x3fee
                           003FED  1616 _XU1STATUS	=	0x3fed
                           003FDA  1617 _XWDTCFG	=	0x3fda
                           003FDB  1618 _XWDTRESET	=	0x3fdb
                           003FF1  1619 _XWTCFGA	=	0x3ff1
                           003FF9  1620 _XWTCFGB	=	0x3ff9
                           003FF2  1621 _XWTCNTA0	=	0x3ff2
                           003FF3  1622 _XWTCNTA1	=	0x3ff3
                           003FF2  1623 _XWTCNTA	=	0x3ff2
                           003FFA  1624 _XWTCNTB0	=	0x3ffa
                           003FFB  1625 _XWTCNTB1	=	0x3ffb
                           003FFA  1626 _XWTCNTB	=	0x3ffa
                           003FEB  1627 _XWTCNTR1	=	0x3feb
                           003FF4  1628 _XWTEVTA0	=	0x3ff4
                           003FF5  1629 _XWTEVTA1	=	0x3ff5
                           003FF4  1630 _XWTEVTA	=	0x3ff4
                           003FF6  1631 _XWTEVTB0	=	0x3ff6
                           003FF7  1632 _XWTEVTB1	=	0x3ff7
                           003FF6  1633 _XWTEVTB	=	0x3ff6
                           003FFC  1634 _XWTEVTC0	=	0x3ffc
                           003FFD  1635 _XWTEVTC1	=	0x3ffd
                           003FFC  1636 _XWTEVTC	=	0x3ffc
                           003FFE  1637 _XWTEVTD0	=	0x3ffe
                           003FFF  1638 _XWTEVTD1	=	0x3fff
                           003FFE  1639 _XWTEVTD	=	0x3ffe
                           003FE9  1640 _XWTIRQEN	=	0x3fe9
                           003FEA  1641 _XWTSTAT	=	0x3fea
                           004114  1642 _AX5043_AFSKCTRL	=	0x4114
                           004113  1643 _AX5043_AFSKMARK0	=	0x4113
                           004112  1644 _AX5043_AFSKMARK1	=	0x4112
                           004111  1645 _AX5043_AFSKSPACE0	=	0x4111
                           004110  1646 _AX5043_AFSKSPACE1	=	0x4110
                           004043  1647 _AX5043_AGCCOUNTER	=	0x4043
                           004115  1648 _AX5043_AMPLFILTER	=	0x4115
                           004189  1649 _AX5043_BBOFFSCAP	=	0x4189
                           004188  1650 _AX5043_BBTUNE	=	0x4188
                           004041  1651 _AX5043_BGNDRSSI	=	0x4041
                           00422E  1652 _AX5043_BGNDRSSIGAIN	=	0x422e
                           00422F  1653 _AX5043_BGNDRSSITHR	=	0x422f
                           004017  1654 _AX5043_CRCINIT0	=	0x4017
                           004016  1655 _AX5043_CRCINIT1	=	0x4016
                           004015  1656 _AX5043_CRCINIT2	=	0x4015
                           004014  1657 _AX5043_CRCINIT3	=	0x4014
                           004332  1658 _AX5043_DACCONFIG	=	0x4332
                           004331  1659 _AX5043_DACVALUE0	=	0x4331
                           004330  1660 _AX5043_DACVALUE1	=	0x4330
                           004102  1661 _AX5043_DECIMATION	=	0x4102
                           004042  1662 _AX5043_DIVERSITY	=	0x4042
                           004011  1663 _AX5043_ENCODING	=	0x4011
                           004018  1664 _AX5043_FEC	=	0x4018
                           00401A  1665 _AX5043_FECSTATUS	=	0x401a
                           004019  1666 _AX5043_FECSYNC	=	0x4019
                           00402B  1667 _AX5043_FIFOCOUNT0	=	0x402b
                           00402A  1668 _AX5043_FIFOCOUNT1	=	0x402a
                           004029  1669 _AX5043_FIFODATA	=	0x4029
                           00402D  1670 _AX5043_FIFOFREE0	=	0x402d
                           00402C  1671 _AX5043_FIFOFREE1	=	0x402c
                           004028  1672 _AX5043_FIFOSTAT	=	0x4028
                           00402F  1673 _AX5043_FIFOTHRESH0	=	0x402f
                           00402E  1674 _AX5043_FIFOTHRESH1	=	0x402e
                           004012  1675 _AX5043_FRAMING	=	0x4012
                           004037  1676 _AX5043_FREQA0	=	0x4037
                           004036  1677 _AX5043_FREQA1	=	0x4036
                           004035  1678 _AX5043_FREQA2	=	0x4035
                           004034  1679 _AX5043_FREQA3	=	0x4034
                           00403F  1680 _AX5043_FREQB0	=	0x403f
                           00403E  1681 _AX5043_FREQB1	=	0x403e
                           00403D  1682 _AX5043_FREQB2	=	0x403d
                           00403C  1683 _AX5043_FREQB3	=	0x403c
                           004163  1684 _AX5043_FSKDEV0	=	0x4163
                           004162  1685 _AX5043_FSKDEV1	=	0x4162
                           004161  1686 _AX5043_FSKDEV2	=	0x4161
                           00410D  1687 _AX5043_FSKDMAX0	=	0x410d
                           00410C  1688 _AX5043_FSKDMAX1	=	0x410c
                           00410F  1689 _AX5043_FSKDMIN0	=	0x410f
                           00410E  1690 _AX5043_FSKDMIN1	=	0x410e
                           004309  1691 _AX5043_GPADC13VALUE0	=	0x4309
                           004308  1692 _AX5043_GPADC13VALUE1	=	0x4308
                           004300  1693 _AX5043_GPADCCTRL	=	0x4300
                           004301  1694 _AX5043_GPADCPERIOD	=	0x4301
                           004101  1695 _AX5043_IFFREQ0	=	0x4101
                           004100  1696 _AX5043_IFFREQ1	=	0x4100
                           00400B  1697 _AX5043_IRQINVERSION0	=	0x400b
                           00400A  1698 _AX5043_IRQINVERSION1	=	0x400a
                           004007  1699 _AX5043_IRQMASK0	=	0x4007
                           004006  1700 _AX5043_IRQMASK1	=	0x4006
                           00400D  1701 _AX5043_IRQREQUEST0	=	0x400d
                           00400C  1702 _AX5043_IRQREQUEST1	=	0x400c
                           004310  1703 _AX5043_LPOSCCONFIG	=	0x4310
                           004317  1704 _AX5043_LPOSCFREQ0	=	0x4317
                           004316  1705 _AX5043_LPOSCFREQ1	=	0x4316
                           004313  1706 _AX5043_LPOSCKFILT0	=	0x4313
                           004312  1707 _AX5043_LPOSCKFILT1	=	0x4312
                           004319  1708 _AX5043_LPOSCPER0	=	0x4319
                           004318  1709 _AX5043_LPOSCPER1	=	0x4318
                           004315  1710 _AX5043_LPOSCREF0	=	0x4315
                           004314  1711 _AX5043_LPOSCREF1	=	0x4314
                           004311  1712 _AX5043_LPOSCSTATUS	=	0x4311
                           004214  1713 _AX5043_MATCH0LEN	=	0x4214
                           004216  1714 _AX5043_MATCH0MAX	=	0x4216
                           004215  1715 _AX5043_MATCH0MIN	=	0x4215
                           004213  1716 _AX5043_MATCH0PAT0	=	0x4213
                           004212  1717 _AX5043_MATCH0PAT1	=	0x4212
                           004211  1718 _AX5043_MATCH0PAT2	=	0x4211
                           004210  1719 _AX5043_MATCH0PAT3	=	0x4210
                           00421C  1720 _AX5043_MATCH1LEN	=	0x421c
                           00421E  1721 _AX5043_MATCH1MAX	=	0x421e
                           00421D  1722 _AX5043_MATCH1MIN	=	0x421d
                           004219  1723 _AX5043_MATCH1PAT0	=	0x4219
                           004218  1724 _AX5043_MATCH1PAT1	=	0x4218
                           004108  1725 _AX5043_MAXDROFFSET0	=	0x4108
                           004107  1726 _AX5043_MAXDROFFSET1	=	0x4107
                           004106  1727 _AX5043_MAXDROFFSET2	=	0x4106
                           00410B  1728 _AX5043_MAXRFOFFSET0	=	0x410b
                           00410A  1729 _AX5043_MAXRFOFFSET1	=	0x410a
                           004109  1730 _AX5043_MAXRFOFFSET2	=	0x4109
                           004164  1731 _AX5043_MODCFGA	=	0x4164
                           004160  1732 _AX5043_MODCFGF	=	0x4160
                           004F5F  1733 _AX5043_MODCFGP	=	0x4f5f
                           004010  1734 _AX5043_MODULATION	=	0x4010
                           004025  1735 _AX5043_PINFUNCANTSEL	=	0x4025
                           004023  1736 _AX5043_PINFUNCDATA	=	0x4023
                           004022  1737 _AX5043_PINFUNCDCLK	=	0x4022
                           004024  1738 _AX5043_PINFUNCIRQ	=	0x4024
                           004026  1739 _AX5043_PINFUNCPWRAMP	=	0x4026
                           004021  1740 _AX5043_PINFUNCSYSCLK	=	0x4021
                           004020  1741 _AX5043_PINSTATE	=	0x4020
                           004233  1742 _AX5043_PKTACCEPTFLAGS	=	0x4233
                           004230  1743 _AX5043_PKTCHUNKSIZE	=	0x4230
                           004231  1744 _AX5043_PKTMISCFLAGS	=	0x4231
                           004232  1745 _AX5043_PKTSTOREFLAGS	=	0x4232
                           004031  1746 _AX5043_PLLCPI	=	0x4031
                           004039  1747 _AX5043_PLLCPIBOOST	=	0x4039
                           004182  1748 _AX5043_PLLLOCKDET	=	0x4182
                           004030  1749 _AX5043_PLLLOOP	=	0x4030
                           004038  1750 _AX5043_PLLLOOPBOOST	=	0x4038
                           004033  1751 _AX5043_PLLRANGINGA	=	0x4033
                           00403B  1752 _AX5043_PLLRANGINGB	=	0x403b
                           004183  1753 _AX5043_PLLRNGCLK	=	0x4183
                           004032  1754 _AX5043_PLLVCODIV	=	0x4032
                           004180  1755 _AX5043_PLLVCOI	=	0x4180
                           004181  1756 _AX5043_PLLVCOIR	=	0x4181
                           004F08  1757 _AX5043_POWCTRL1	=	0x4f08
                           004005  1758 _AX5043_POWIRQMASK	=	0x4005
                           004003  1759 _AX5043_POWSTAT	=	0x4003
                           004004  1760 _AX5043_POWSTICKYSTAT	=	0x4004
                           004027  1761 _AX5043_PWRAMP	=	0x4027
                           004002  1762 _AX5043_PWRMODE	=	0x4002
                           004009  1763 _AX5043_RADIOEVENTMASK0	=	0x4009
                           004008  1764 _AX5043_RADIOEVENTMASK1	=	0x4008
                           00400F  1765 _AX5043_RADIOEVENTREQ0	=	0x400f
                           00400E  1766 _AX5043_RADIOEVENTREQ1	=	0x400e
                           00401C  1767 _AX5043_RADIOSTATE	=	0x401c
                           004F0D  1768 _AX5043_REF	=	0x4f0d
                           004040  1769 _AX5043_RSSI	=	0x4040
                           00422D  1770 _AX5043_RSSIABSTHR	=	0x422d
                           00422C  1771 _AX5043_RSSIREFERENCE	=	0x422c
                           004105  1772 _AX5043_RXDATARATE0	=	0x4105
                           004104  1773 _AX5043_RXDATARATE1	=	0x4104
                           004103  1774 _AX5043_RXDATARATE2	=	0x4103
                           004001  1775 _AX5043_SCRATCH	=	0x4001
                           004000  1776 _AX5043_SILICONREVISION	=	0x4000
                           00405B  1777 _AX5043_TIMER0	=	0x405b
                           00405A  1778 _AX5043_TIMER1	=	0x405a
                           004059  1779 _AX5043_TIMER2	=	0x4059
                           004227  1780 _AX5043_TMGRXAGC	=	0x4227
                           004223  1781 _AX5043_TMGRXBOOST	=	0x4223
                           004226  1782 _AX5043_TMGRXCOARSEAGC	=	0x4226
                           004225  1783 _AX5043_TMGRXOFFSACQ	=	0x4225
                           004229  1784 _AX5043_TMGRXPREAMBLE1	=	0x4229
                           00422A  1785 _AX5043_TMGRXPREAMBLE2	=	0x422a
                           00422B  1786 _AX5043_TMGRXPREAMBLE3	=	0x422b
                           004228  1787 _AX5043_TMGRXRSSI	=	0x4228
                           004224  1788 _AX5043_TMGRXSETTLE	=	0x4224
                           004220  1789 _AX5043_TMGTXBOOST	=	0x4220
                           004221  1790 _AX5043_TMGTXSETTLE	=	0x4221
                           004055  1791 _AX5043_TRKAFSKDEMOD0	=	0x4055
                           004054  1792 _AX5043_TRKAFSKDEMOD1	=	0x4054
                           004049  1793 _AX5043_TRKAMPLITUDE0	=	0x4049
                           004048  1794 _AX5043_TRKAMPLITUDE1	=	0x4048
                           004047  1795 _AX5043_TRKDATARATE0	=	0x4047
                           004046  1796 _AX5043_TRKDATARATE1	=	0x4046
                           004045  1797 _AX5043_TRKDATARATE2	=	0x4045
                           004051  1798 _AX5043_TRKFREQ0	=	0x4051
                           004050  1799 _AX5043_TRKFREQ1	=	0x4050
                           004053  1800 _AX5043_TRKFSKDEMOD0	=	0x4053
                           004052  1801 _AX5043_TRKFSKDEMOD1	=	0x4052
                           00404B  1802 _AX5043_TRKPHASE0	=	0x404b
                           00404A  1803 _AX5043_TRKPHASE1	=	0x404a
                           00404F  1804 _AX5043_TRKRFFREQ0	=	0x404f
                           00404E  1805 _AX5043_TRKRFFREQ1	=	0x404e
                           00404D  1806 _AX5043_TRKRFFREQ2	=	0x404d
                           004169  1807 _AX5043_TXPWRCOEFFA0	=	0x4169
                           004168  1808 _AX5043_TXPWRCOEFFA1	=	0x4168
                           00416B  1809 _AX5043_TXPWRCOEFFB0	=	0x416b
                           00416A  1810 _AX5043_TXPWRCOEFFB1	=	0x416a
                           00416D  1811 _AX5043_TXPWRCOEFFC0	=	0x416d
                           00416C  1812 _AX5043_TXPWRCOEFFC1	=	0x416c
                           00416F  1813 _AX5043_TXPWRCOEFFD0	=	0x416f
                           00416E  1814 _AX5043_TXPWRCOEFFD1	=	0x416e
                           004171  1815 _AX5043_TXPWRCOEFFE0	=	0x4171
                           004170  1816 _AX5043_TXPWRCOEFFE1	=	0x4170
                           004167  1817 _AX5043_TXRATE0	=	0x4167
                           004166  1818 _AX5043_TXRATE1	=	0x4166
                           004165  1819 _AX5043_TXRATE2	=	0x4165
                           00406B  1820 _AX5043_WAKEUP0	=	0x406b
                           00406A  1821 _AX5043_WAKEUP1	=	0x406a
                           00406D  1822 _AX5043_WAKEUPFREQ0	=	0x406d
                           00406C  1823 _AX5043_WAKEUPFREQ1	=	0x406c
                           004069  1824 _AX5043_WAKEUPTIMER0	=	0x4069
                           004068  1825 _AX5043_WAKEUPTIMER1	=	0x4068
                           00406E  1826 _AX5043_WAKEUPXOEARLY	=	0x406e
                           004F11  1827 _AX5043_XTALAMPL	=	0x4f11
                           004184  1828 _AX5043_XTALCAP	=	0x4184
                           004F10  1829 _AX5043_XTALOSC	=	0x4f10
                           00401D  1830 _AX5043_XTALSTATUS	=	0x401d
                           004F00  1831 _AX5043_0xF00	=	0x4f00
                           004F0C  1832 _AX5043_0xF0C	=	0x4f0c
                           004F18  1833 _AX5043_0xF18	=	0x4f18
                           004F1C  1834 _AX5043_0xF1C	=	0x4f1c
                           004F21  1835 _AX5043_0xF21	=	0x4f21
                           004F22  1836 _AX5043_0xF22	=	0x4f22
                           004F23  1837 _AX5043_0xF23	=	0x4f23
                           004F26  1838 _AX5043_0xF26	=	0x4f26
                           004F30  1839 _AX5043_0xF30	=	0x4f30
                           004F31  1840 _AX5043_0xF31	=	0x4f31
                           004F32  1841 _AX5043_0xF32	=	0x4f32
                           004F33  1842 _AX5043_0xF33	=	0x4f33
                           004F34  1843 _AX5043_0xF34	=	0x4f34
                           004F35  1844 _AX5043_0xF35	=	0x4f35
                           004F44  1845 _AX5043_0xF44	=	0x4f44
                           004122  1846 _AX5043_AGCAHYST0	=	0x4122
                           004132  1847 _AX5043_AGCAHYST1	=	0x4132
                           004142  1848 _AX5043_AGCAHYST2	=	0x4142
                           004152  1849 _AX5043_AGCAHYST3	=	0x4152
                           004120  1850 _AX5043_AGCGAIN0	=	0x4120
                           004130  1851 _AX5043_AGCGAIN1	=	0x4130
                           004140  1852 _AX5043_AGCGAIN2	=	0x4140
                           004150  1853 _AX5043_AGCGAIN3	=	0x4150
                           004123  1854 _AX5043_AGCMINMAX0	=	0x4123
                           004133  1855 _AX5043_AGCMINMAX1	=	0x4133
                           004143  1856 _AX5043_AGCMINMAX2	=	0x4143
                           004153  1857 _AX5043_AGCMINMAX3	=	0x4153
                           004121  1858 _AX5043_AGCTARGET0	=	0x4121
                           004131  1859 _AX5043_AGCTARGET1	=	0x4131
                           004141  1860 _AX5043_AGCTARGET2	=	0x4141
                           004151  1861 _AX5043_AGCTARGET3	=	0x4151
                           00412B  1862 _AX5043_AMPLITUDEGAIN0	=	0x412b
                           00413B  1863 _AX5043_AMPLITUDEGAIN1	=	0x413b
                           00414B  1864 _AX5043_AMPLITUDEGAIN2	=	0x414b
                           00415B  1865 _AX5043_AMPLITUDEGAIN3	=	0x415b
                           00412F  1866 _AX5043_BBOFFSRES0	=	0x412f
                           00413F  1867 _AX5043_BBOFFSRES1	=	0x413f
                           00414F  1868 _AX5043_BBOFFSRES2	=	0x414f
                           00415F  1869 _AX5043_BBOFFSRES3	=	0x415f
                           004125  1870 _AX5043_DRGAIN0	=	0x4125
                           004135  1871 _AX5043_DRGAIN1	=	0x4135
                           004145  1872 _AX5043_DRGAIN2	=	0x4145
                           004155  1873 _AX5043_DRGAIN3	=	0x4155
                           00412E  1874 _AX5043_FOURFSK0	=	0x412e
                           00413E  1875 _AX5043_FOURFSK1	=	0x413e
                           00414E  1876 _AX5043_FOURFSK2	=	0x414e
                           00415E  1877 _AX5043_FOURFSK3	=	0x415e
                           00412D  1878 _AX5043_FREQDEV00	=	0x412d
                           00413D  1879 _AX5043_FREQDEV01	=	0x413d
                           00414D  1880 _AX5043_FREQDEV02	=	0x414d
                           00415D  1881 _AX5043_FREQDEV03	=	0x415d
                           00412C  1882 _AX5043_FREQDEV10	=	0x412c
                           00413C  1883 _AX5043_FREQDEV11	=	0x413c
                           00414C  1884 _AX5043_FREQDEV12	=	0x414c
                           00415C  1885 _AX5043_FREQDEV13	=	0x415c
                           004127  1886 _AX5043_FREQUENCYGAINA0	=	0x4127
                           004137  1887 _AX5043_FREQUENCYGAINA1	=	0x4137
                           004147  1888 _AX5043_FREQUENCYGAINA2	=	0x4147
                           004157  1889 _AX5043_FREQUENCYGAINA3	=	0x4157
                           004128  1890 _AX5043_FREQUENCYGAINB0	=	0x4128
                           004138  1891 _AX5043_FREQUENCYGAINB1	=	0x4138
                           004148  1892 _AX5043_FREQUENCYGAINB2	=	0x4148
                           004158  1893 _AX5043_FREQUENCYGAINB3	=	0x4158
                           004129  1894 _AX5043_FREQUENCYGAINC0	=	0x4129
                           004139  1895 _AX5043_FREQUENCYGAINC1	=	0x4139
                           004149  1896 _AX5043_FREQUENCYGAINC2	=	0x4149
                           004159  1897 _AX5043_FREQUENCYGAINC3	=	0x4159
                           00412A  1898 _AX5043_FREQUENCYGAIND0	=	0x412a
                           00413A  1899 _AX5043_FREQUENCYGAIND1	=	0x413a
                           00414A  1900 _AX5043_FREQUENCYGAIND2	=	0x414a
                           00415A  1901 _AX5043_FREQUENCYGAIND3	=	0x415a
                           004116  1902 _AX5043_FREQUENCYLEAK	=	0x4116
                           004126  1903 _AX5043_PHASEGAIN0	=	0x4126
                           004136  1904 _AX5043_PHASEGAIN1	=	0x4136
                           004146  1905 _AX5043_PHASEGAIN2	=	0x4146
                           004156  1906 _AX5043_PHASEGAIN3	=	0x4156
                           004207  1907 _AX5043_PKTADDR0	=	0x4207
                           004206  1908 _AX5043_PKTADDR1	=	0x4206
                           004205  1909 _AX5043_PKTADDR2	=	0x4205
                           004204  1910 _AX5043_PKTADDR3	=	0x4204
                           004200  1911 _AX5043_PKTADDRCFG	=	0x4200
                           00420B  1912 _AX5043_PKTADDRMASK0	=	0x420b
                           00420A  1913 _AX5043_PKTADDRMASK1	=	0x420a
                           004209  1914 _AX5043_PKTADDRMASK2	=	0x4209
                           004208  1915 _AX5043_PKTADDRMASK3	=	0x4208
                           004201  1916 _AX5043_PKTLENCFG	=	0x4201
                           004202  1917 _AX5043_PKTLENOFFSET	=	0x4202
                           004203  1918 _AX5043_PKTMAXLEN	=	0x4203
                           004118  1919 _AX5043_RXPARAMCURSET	=	0x4118
                           004117  1920 _AX5043_RXPARAMSETS	=	0x4117
                           004124  1921 _AX5043_TIMEGAIN0	=	0x4124
                           004134  1922 _AX5043_TIMEGAIN1	=	0x4134
                           004144  1923 _AX5043_TIMEGAIN2	=	0x4144
                           004154  1924 _AX5043_TIMEGAIN3	=	0x4154
                           005114  1925 _AX5043_AFSKCTRLNB	=	0x5114
                           005113  1926 _AX5043_AFSKMARK0NB	=	0x5113
                           005112  1927 _AX5043_AFSKMARK1NB	=	0x5112
                           005111  1928 _AX5043_AFSKSPACE0NB	=	0x5111
                           005110  1929 _AX5043_AFSKSPACE1NB	=	0x5110
                           005043  1930 _AX5043_AGCCOUNTERNB	=	0x5043
                           005115  1931 _AX5043_AMPLFILTERNB	=	0x5115
                           005189  1932 _AX5043_BBOFFSCAPNB	=	0x5189
                           005188  1933 _AX5043_BBTUNENB	=	0x5188
                           005041  1934 _AX5043_BGNDRSSINB	=	0x5041
                           00522E  1935 _AX5043_BGNDRSSIGAINNB	=	0x522e
                           00522F  1936 _AX5043_BGNDRSSITHRNB	=	0x522f
                           005017  1937 _AX5043_CRCINIT0NB	=	0x5017
                           005016  1938 _AX5043_CRCINIT1NB	=	0x5016
                           005015  1939 _AX5043_CRCINIT2NB	=	0x5015
                           005014  1940 _AX5043_CRCINIT3NB	=	0x5014
                           005332  1941 _AX5043_DACCONFIGNB	=	0x5332
                           005331  1942 _AX5043_DACVALUE0NB	=	0x5331
                           005330  1943 _AX5043_DACVALUE1NB	=	0x5330
                           005102  1944 _AX5043_DECIMATIONNB	=	0x5102
                           005042  1945 _AX5043_DIVERSITYNB	=	0x5042
                           005011  1946 _AX5043_ENCODINGNB	=	0x5011
                           005018  1947 _AX5043_FECNB	=	0x5018
                           00501A  1948 _AX5043_FECSTATUSNB	=	0x501a
                           005019  1949 _AX5043_FECSYNCNB	=	0x5019
                           00502B  1950 _AX5043_FIFOCOUNT0NB	=	0x502b
                           00502A  1951 _AX5043_FIFOCOUNT1NB	=	0x502a
                           005029  1952 _AX5043_FIFODATANB	=	0x5029
                           00502D  1953 _AX5043_FIFOFREE0NB	=	0x502d
                           00502C  1954 _AX5043_FIFOFREE1NB	=	0x502c
                           005028  1955 _AX5043_FIFOSTATNB	=	0x5028
                           00502F  1956 _AX5043_FIFOTHRESH0NB	=	0x502f
                           00502E  1957 _AX5043_FIFOTHRESH1NB	=	0x502e
                           005012  1958 _AX5043_FRAMINGNB	=	0x5012
                           005037  1959 _AX5043_FREQA0NB	=	0x5037
                           005036  1960 _AX5043_FREQA1NB	=	0x5036
                           005035  1961 _AX5043_FREQA2NB	=	0x5035
                           005034  1962 _AX5043_FREQA3NB	=	0x5034
                           00503F  1963 _AX5043_FREQB0NB	=	0x503f
                           00503E  1964 _AX5043_FREQB1NB	=	0x503e
                           00503D  1965 _AX5043_FREQB2NB	=	0x503d
                           00503C  1966 _AX5043_FREQB3NB	=	0x503c
                           005163  1967 _AX5043_FSKDEV0NB	=	0x5163
                           005162  1968 _AX5043_FSKDEV1NB	=	0x5162
                           005161  1969 _AX5043_FSKDEV2NB	=	0x5161
                           00510D  1970 _AX5043_FSKDMAX0NB	=	0x510d
                           00510C  1971 _AX5043_FSKDMAX1NB	=	0x510c
                           00510F  1972 _AX5043_FSKDMIN0NB	=	0x510f
                           00510E  1973 _AX5043_FSKDMIN1NB	=	0x510e
                           005309  1974 _AX5043_GPADC13VALUE0NB	=	0x5309
                           005308  1975 _AX5043_GPADC13VALUE1NB	=	0x5308
                           005300  1976 _AX5043_GPADCCTRLNB	=	0x5300
                           005301  1977 _AX5043_GPADCPERIODNB	=	0x5301
                           005101  1978 _AX5043_IFFREQ0NB	=	0x5101
                           005100  1979 _AX5043_IFFREQ1NB	=	0x5100
                           00500B  1980 _AX5043_IRQINVERSION0NB	=	0x500b
                           00500A  1981 _AX5043_IRQINVERSION1NB	=	0x500a
                           005007  1982 _AX5043_IRQMASK0NB	=	0x5007
                           005006  1983 _AX5043_IRQMASK1NB	=	0x5006
                           00500D  1984 _AX5043_IRQREQUEST0NB	=	0x500d
                           00500C  1985 _AX5043_IRQREQUEST1NB	=	0x500c
                           005310  1986 _AX5043_LPOSCCONFIGNB	=	0x5310
                           005317  1987 _AX5043_LPOSCFREQ0NB	=	0x5317
                           005316  1988 _AX5043_LPOSCFREQ1NB	=	0x5316
                           005313  1989 _AX5043_LPOSCKFILT0NB	=	0x5313
                           005312  1990 _AX5043_LPOSCKFILT1NB	=	0x5312
                           005319  1991 _AX5043_LPOSCPER0NB	=	0x5319
                           005318  1992 _AX5043_LPOSCPER1NB	=	0x5318
                           005315  1993 _AX5043_LPOSCREF0NB	=	0x5315
                           005314  1994 _AX5043_LPOSCREF1NB	=	0x5314
                           005311  1995 _AX5043_LPOSCSTATUSNB	=	0x5311
                           005214  1996 _AX5043_MATCH0LENNB	=	0x5214
                           005216  1997 _AX5043_MATCH0MAXNB	=	0x5216
                           005215  1998 _AX5043_MATCH0MINNB	=	0x5215
                           005213  1999 _AX5043_MATCH0PAT0NB	=	0x5213
                           005212  2000 _AX5043_MATCH0PAT1NB	=	0x5212
                           005211  2001 _AX5043_MATCH0PAT2NB	=	0x5211
                           005210  2002 _AX5043_MATCH0PAT3NB	=	0x5210
                           00521C  2003 _AX5043_MATCH1LENNB	=	0x521c
                           00521E  2004 _AX5043_MATCH1MAXNB	=	0x521e
                           00521D  2005 _AX5043_MATCH1MINNB	=	0x521d
                           005219  2006 _AX5043_MATCH1PAT0NB	=	0x5219
                           005218  2007 _AX5043_MATCH1PAT1NB	=	0x5218
                           005108  2008 _AX5043_MAXDROFFSET0NB	=	0x5108
                           005107  2009 _AX5043_MAXDROFFSET1NB	=	0x5107
                           005106  2010 _AX5043_MAXDROFFSET2NB	=	0x5106
                           00510B  2011 _AX5043_MAXRFOFFSET0NB	=	0x510b
                           00510A  2012 _AX5043_MAXRFOFFSET1NB	=	0x510a
                           005109  2013 _AX5043_MAXRFOFFSET2NB	=	0x5109
                           005164  2014 _AX5043_MODCFGANB	=	0x5164
                           005160  2015 _AX5043_MODCFGFNB	=	0x5160
                           005F5F  2016 _AX5043_MODCFGPNB	=	0x5f5f
                           005010  2017 _AX5043_MODULATIONNB	=	0x5010
                           005025  2018 _AX5043_PINFUNCANTSELNB	=	0x5025
                           005023  2019 _AX5043_PINFUNCDATANB	=	0x5023
                           005022  2020 _AX5043_PINFUNCDCLKNB	=	0x5022
                           005024  2021 _AX5043_PINFUNCIRQNB	=	0x5024
                           005026  2022 _AX5043_PINFUNCPWRAMPNB	=	0x5026
                           005021  2023 _AX5043_PINFUNCSYSCLKNB	=	0x5021
                           005020  2024 _AX5043_PINSTATENB	=	0x5020
                           005233  2025 _AX5043_PKTACCEPTFLAGSNB	=	0x5233
                           005230  2026 _AX5043_PKTCHUNKSIZENB	=	0x5230
                           005231  2027 _AX5043_PKTMISCFLAGSNB	=	0x5231
                           005232  2028 _AX5043_PKTSTOREFLAGSNB	=	0x5232
                           005031  2029 _AX5043_PLLCPINB	=	0x5031
                           005039  2030 _AX5043_PLLCPIBOOSTNB	=	0x5039
                           005182  2031 _AX5043_PLLLOCKDETNB	=	0x5182
                           005030  2032 _AX5043_PLLLOOPNB	=	0x5030
                           005038  2033 _AX5043_PLLLOOPBOOSTNB	=	0x5038
                           005033  2034 _AX5043_PLLRANGINGANB	=	0x5033
                           00503B  2035 _AX5043_PLLRANGINGBNB	=	0x503b
                           005183  2036 _AX5043_PLLRNGCLKNB	=	0x5183
                           005032  2037 _AX5043_PLLVCODIVNB	=	0x5032
                           005180  2038 _AX5043_PLLVCOINB	=	0x5180
                           005181  2039 _AX5043_PLLVCOIRNB	=	0x5181
                           005F08  2040 _AX5043_POWCTRL1NB	=	0x5f08
                           005005  2041 _AX5043_POWIRQMASKNB	=	0x5005
                           005003  2042 _AX5043_POWSTATNB	=	0x5003
                           005004  2043 _AX5043_POWSTICKYSTATNB	=	0x5004
                           005027  2044 _AX5043_PWRAMPNB	=	0x5027
                           005002  2045 _AX5043_PWRMODENB	=	0x5002
                           005009  2046 _AX5043_RADIOEVENTMASK0NB	=	0x5009
                           005008  2047 _AX5043_RADIOEVENTMASK1NB	=	0x5008
                           00500F  2048 _AX5043_RADIOEVENTREQ0NB	=	0x500f
                           00500E  2049 _AX5043_RADIOEVENTREQ1NB	=	0x500e
                           00501C  2050 _AX5043_RADIOSTATENB	=	0x501c
                           005F0D  2051 _AX5043_REFNB	=	0x5f0d
                           005040  2052 _AX5043_RSSINB	=	0x5040
                           00522D  2053 _AX5043_RSSIABSTHRNB	=	0x522d
                           00522C  2054 _AX5043_RSSIREFERENCENB	=	0x522c
                           005105  2055 _AX5043_RXDATARATE0NB	=	0x5105
                           005104  2056 _AX5043_RXDATARATE1NB	=	0x5104
                           005103  2057 _AX5043_RXDATARATE2NB	=	0x5103
                           005001  2058 _AX5043_SCRATCHNB	=	0x5001
                           005000  2059 _AX5043_SILICONREVISIONNB	=	0x5000
                           00505B  2060 _AX5043_TIMER0NB	=	0x505b
                           00505A  2061 _AX5043_TIMER1NB	=	0x505a
                           005059  2062 _AX5043_TIMER2NB	=	0x5059
                           005227  2063 _AX5043_TMGRXAGCNB	=	0x5227
                           005223  2064 _AX5043_TMGRXBOOSTNB	=	0x5223
                           005226  2065 _AX5043_TMGRXCOARSEAGCNB	=	0x5226
                           005225  2066 _AX5043_TMGRXOFFSACQNB	=	0x5225
                           005229  2067 _AX5043_TMGRXPREAMBLE1NB	=	0x5229
                           00522A  2068 _AX5043_TMGRXPREAMBLE2NB	=	0x522a
                           00522B  2069 _AX5043_TMGRXPREAMBLE3NB	=	0x522b
                           005228  2070 _AX5043_TMGRXRSSINB	=	0x5228
                           005224  2071 _AX5043_TMGRXSETTLENB	=	0x5224
                           005220  2072 _AX5043_TMGTXBOOSTNB	=	0x5220
                           005221  2073 _AX5043_TMGTXSETTLENB	=	0x5221
                           005055  2074 _AX5043_TRKAFSKDEMOD0NB	=	0x5055
                           005054  2075 _AX5043_TRKAFSKDEMOD1NB	=	0x5054
                           005049  2076 _AX5043_TRKAMPLITUDE0NB	=	0x5049
                           005048  2077 _AX5043_TRKAMPLITUDE1NB	=	0x5048
                           005047  2078 _AX5043_TRKDATARATE0NB	=	0x5047
                           005046  2079 _AX5043_TRKDATARATE1NB	=	0x5046
                           005045  2080 _AX5043_TRKDATARATE2NB	=	0x5045
                           005051  2081 _AX5043_TRKFREQ0NB	=	0x5051
                           005050  2082 _AX5043_TRKFREQ1NB	=	0x5050
                           005053  2083 _AX5043_TRKFSKDEMOD0NB	=	0x5053
                           005052  2084 _AX5043_TRKFSKDEMOD1NB	=	0x5052
                           00504B  2085 _AX5043_TRKPHASE0NB	=	0x504b
                           00504A  2086 _AX5043_TRKPHASE1NB	=	0x504a
                           00504F  2087 _AX5043_TRKRFFREQ0NB	=	0x504f
                           00504E  2088 _AX5043_TRKRFFREQ1NB	=	0x504e
                           00504D  2089 _AX5043_TRKRFFREQ2NB	=	0x504d
                           005169  2090 _AX5043_TXPWRCOEFFA0NB	=	0x5169
                           005168  2091 _AX5043_TXPWRCOEFFA1NB	=	0x5168
                           00516B  2092 _AX5043_TXPWRCOEFFB0NB	=	0x516b
                           00516A  2093 _AX5043_TXPWRCOEFFB1NB	=	0x516a
                           00516D  2094 _AX5043_TXPWRCOEFFC0NB	=	0x516d
                           00516C  2095 _AX5043_TXPWRCOEFFC1NB	=	0x516c
                           00516F  2096 _AX5043_TXPWRCOEFFD0NB	=	0x516f
                           00516E  2097 _AX5043_TXPWRCOEFFD1NB	=	0x516e
                           005171  2098 _AX5043_TXPWRCOEFFE0NB	=	0x5171
                           005170  2099 _AX5043_TXPWRCOEFFE1NB	=	0x5170
                           005167  2100 _AX5043_TXRATE0NB	=	0x5167
                           005166  2101 _AX5043_TXRATE1NB	=	0x5166
                           005165  2102 _AX5043_TXRATE2NB	=	0x5165
                           00506B  2103 _AX5043_WAKEUP0NB	=	0x506b
                           00506A  2104 _AX5043_WAKEUP1NB	=	0x506a
                           00506D  2105 _AX5043_WAKEUPFREQ0NB	=	0x506d
                           00506C  2106 _AX5043_WAKEUPFREQ1NB	=	0x506c
                           005069  2107 _AX5043_WAKEUPTIMER0NB	=	0x5069
                           005068  2108 _AX5043_WAKEUPTIMER1NB	=	0x5068
                           00506E  2109 _AX5043_WAKEUPXOEARLYNB	=	0x506e
                           005F11  2110 _AX5043_XTALAMPLNB	=	0x5f11
                           005184  2111 _AX5043_XTALCAPNB	=	0x5184
                           005F10  2112 _AX5043_XTALOSCNB	=	0x5f10
                           00501D  2113 _AX5043_XTALSTATUSNB	=	0x501d
                           005F00  2114 _AX5043_0xF00NB	=	0x5f00
                           005F0C  2115 _AX5043_0xF0CNB	=	0x5f0c
                           005F18  2116 _AX5043_0xF18NB	=	0x5f18
                           005F1C  2117 _AX5043_0xF1CNB	=	0x5f1c
                           005F21  2118 _AX5043_0xF21NB	=	0x5f21
                           005F22  2119 _AX5043_0xF22NB	=	0x5f22
                           005F23  2120 _AX5043_0xF23NB	=	0x5f23
                           005F26  2121 _AX5043_0xF26NB	=	0x5f26
                           005F30  2122 _AX5043_0xF30NB	=	0x5f30
                           005F31  2123 _AX5043_0xF31NB	=	0x5f31
                           005F32  2124 _AX5043_0xF32NB	=	0x5f32
                           005F33  2125 _AX5043_0xF33NB	=	0x5f33
                           005F34  2126 _AX5043_0xF34NB	=	0x5f34
                           005F35  2127 _AX5043_0xF35NB	=	0x5f35
                           005F44  2128 _AX5043_0xF44NB	=	0x5f44
                           005122  2129 _AX5043_AGCAHYST0NB	=	0x5122
                           005132  2130 _AX5043_AGCAHYST1NB	=	0x5132
                           005142  2131 _AX5043_AGCAHYST2NB	=	0x5142
                           005152  2132 _AX5043_AGCAHYST3NB	=	0x5152
                           005120  2133 _AX5043_AGCGAIN0NB	=	0x5120
                           005130  2134 _AX5043_AGCGAIN1NB	=	0x5130
                           005140  2135 _AX5043_AGCGAIN2NB	=	0x5140
                           005150  2136 _AX5043_AGCGAIN3NB	=	0x5150
                           005123  2137 _AX5043_AGCMINMAX0NB	=	0x5123
                           005133  2138 _AX5043_AGCMINMAX1NB	=	0x5133
                           005143  2139 _AX5043_AGCMINMAX2NB	=	0x5143
                           005153  2140 _AX5043_AGCMINMAX3NB	=	0x5153
                           005121  2141 _AX5043_AGCTARGET0NB	=	0x5121
                           005131  2142 _AX5043_AGCTARGET1NB	=	0x5131
                           005141  2143 _AX5043_AGCTARGET2NB	=	0x5141
                           005151  2144 _AX5043_AGCTARGET3NB	=	0x5151
                           00512B  2145 _AX5043_AMPLITUDEGAIN0NB	=	0x512b
                           00513B  2146 _AX5043_AMPLITUDEGAIN1NB	=	0x513b
                           00514B  2147 _AX5043_AMPLITUDEGAIN2NB	=	0x514b
                           00515B  2148 _AX5043_AMPLITUDEGAIN3NB	=	0x515b
                           00512F  2149 _AX5043_BBOFFSRES0NB	=	0x512f
                           00513F  2150 _AX5043_BBOFFSRES1NB	=	0x513f
                           00514F  2151 _AX5043_BBOFFSRES2NB	=	0x514f
                           00515F  2152 _AX5043_BBOFFSRES3NB	=	0x515f
                           005125  2153 _AX5043_DRGAIN0NB	=	0x5125
                           005135  2154 _AX5043_DRGAIN1NB	=	0x5135
                           005145  2155 _AX5043_DRGAIN2NB	=	0x5145
                           005155  2156 _AX5043_DRGAIN3NB	=	0x5155
                           00512E  2157 _AX5043_FOURFSK0NB	=	0x512e
                           00513E  2158 _AX5043_FOURFSK1NB	=	0x513e
                           00514E  2159 _AX5043_FOURFSK2NB	=	0x514e
                           00515E  2160 _AX5043_FOURFSK3NB	=	0x515e
                           00512D  2161 _AX5043_FREQDEV00NB	=	0x512d
                           00513D  2162 _AX5043_FREQDEV01NB	=	0x513d
                           00514D  2163 _AX5043_FREQDEV02NB	=	0x514d
                           00515D  2164 _AX5043_FREQDEV03NB	=	0x515d
                           00512C  2165 _AX5043_FREQDEV10NB	=	0x512c
                           00513C  2166 _AX5043_FREQDEV11NB	=	0x513c
                           00514C  2167 _AX5043_FREQDEV12NB	=	0x514c
                           00515C  2168 _AX5043_FREQDEV13NB	=	0x515c
                           005127  2169 _AX5043_FREQUENCYGAINA0NB	=	0x5127
                           005137  2170 _AX5043_FREQUENCYGAINA1NB	=	0x5137
                           005147  2171 _AX5043_FREQUENCYGAINA2NB	=	0x5147
                           005157  2172 _AX5043_FREQUENCYGAINA3NB	=	0x5157
                           005128  2173 _AX5043_FREQUENCYGAINB0NB	=	0x5128
                           005138  2174 _AX5043_FREQUENCYGAINB1NB	=	0x5138
                           005148  2175 _AX5043_FREQUENCYGAINB2NB	=	0x5148
                           005158  2176 _AX5043_FREQUENCYGAINB3NB	=	0x5158
                           005129  2177 _AX5043_FREQUENCYGAINC0NB	=	0x5129
                           005139  2178 _AX5043_FREQUENCYGAINC1NB	=	0x5139
                           005149  2179 _AX5043_FREQUENCYGAINC2NB	=	0x5149
                           005159  2180 _AX5043_FREQUENCYGAINC3NB	=	0x5159
                           00512A  2181 _AX5043_FREQUENCYGAIND0NB	=	0x512a
                           00513A  2182 _AX5043_FREQUENCYGAIND1NB	=	0x513a
                           00514A  2183 _AX5043_FREQUENCYGAIND2NB	=	0x514a
                           00515A  2184 _AX5043_FREQUENCYGAIND3NB	=	0x515a
                           005116  2185 _AX5043_FREQUENCYLEAKNB	=	0x5116
                           005126  2186 _AX5043_PHASEGAIN0NB	=	0x5126
                           005136  2187 _AX5043_PHASEGAIN1NB	=	0x5136
                           005146  2188 _AX5043_PHASEGAIN2NB	=	0x5146
                           005156  2189 _AX5043_PHASEGAIN3NB	=	0x5156
                           005207  2190 _AX5043_PKTADDR0NB	=	0x5207
                           005206  2191 _AX5043_PKTADDR1NB	=	0x5206
                           005205  2192 _AX5043_PKTADDR2NB	=	0x5205
                           005204  2193 _AX5043_PKTADDR3NB	=	0x5204
                           005200  2194 _AX5043_PKTADDRCFGNB	=	0x5200
                           00520B  2195 _AX5043_PKTADDRMASK0NB	=	0x520b
                           00520A  2196 _AX5043_PKTADDRMASK1NB	=	0x520a
                           005209  2197 _AX5043_PKTADDRMASK2NB	=	0x5209
                           005208  2198 _AX5043_PKTADDRMASK3NB	=	0x5208
                           005201  2199 _AX5043_PKTLENCFGNB	=	0x5201
                           005202  2200 _AX5043_PKTLENOFFSETNB	=	0x5202
                           005203  2201 _AX5043_PKTMAXLENNB	=	0x5203
                           005118  2202 _AX5043_RXPARAMCURSETNB	=	0x5118
                           005117  2203 _AX5043_RXPARAMSETSNB	=	0x5117
                           005124  2204 _AX5043_TIMEGAIN0NB	=	0x5124
                           005134  2205 _AX5043_TIMEGAIN1NB	=	0x5134
                           005144  2206 _AX5043_TIMEGAIN2NB	=	0x5144
                           005154  2207 _AX5043_TIMEGAIN3NB	=	0x5154
      000383                       2208 _UART_Proc_VerifyIncomingMsg_PARM_2:
      000383                       2209 	.ds 3
      000386                       2210 _UART_Proc_VerifyIncomingMsg_PARM_3:
      000386                       2211 	.ds 3
      000389                       2212 _UART_Proc_VerifyIncomingMsg_cmd_1_238:
      000389                       2213 	.ds 3
      00038C                       2214 _UART_Proc_VerifyIncomingMsg_RetVal_1_239:
      00038C                       2215 	.ds 1
      00038D                       2216 _UART_Proc_VerifyIncomingMsg_RxLength_1_239:
      00038D                       2217 	.ds 1
      00038E                       2218 _UART_Proc_VerifyIncomingMsg_CRC32Rx_1_239:
      00038E                       2219 	.ds 4
      000392                       2220 _UART_Proc_ModifiyKissSpecialCharacters_PARM_2:
      000392                       2221 	.ds 3
      000395                       2222 _UART_Proc_ModifiyKissSpecialCharacters_UartRxBuffer_1_250:
      000395                       2223 	.ds 3
      000398                       2224 _UART_Proc_SendMessage_PARM_2:
      000398                       2225 	.ds 1
      000399                       2226 _UART_Proc_SendMessage_PARM_3:
      000399                       2227 	.ds 1
      00039A                       2228 _UART_Proc_SendMessage_payload_1_258:
      00039A                       2229 	.ds 3
      00039D                       2230 _UART_Calc_CRC32_PARM_2:
      00039D                       2231 	.ds 1
      00039E                       2232 _UART_Calc_CRC32_PARM_3:
      00039E                       2233 	.ds 1
      00039F                       2234 _UART_Calc_CRC32_message_1_261:
      00039F                       2235 	.ds 3
      0003A2                       2236 _UART_Calc_CRC32_crc_1_262:
      0003A2                       2237 	.ds 4
                                   2238 ;--------------------------------------------------------
                                   2239 ; absolute external ram data
                                   2240 ;--------------------------------------------------------
                                   2241 	.area XABS    (ABS,XDATA)
                                   2242 ;--------------------------------------------------------
                                   2243 ; external initialized ram data
                                   2244 ;--------------------------------------------------------
                                   2245 	.area XISEG   (XDATA)
                                   2246 	.area HOME    (CODE)
                                   2247 	.area GSINIT0 (CODE)
                                   2248 	.area GSINIT1 (CODE)
                                   2249 	.area GSINIT2 (CODE)
                                   2250 	.area GSINIT3 (CODE)
                                   2251 	.area GSINIT4 (CODE)
                                   2252 	.area GSINIT5 (CODE)
                                   2253 	.area GSINIT  (CODE)
                                   2254 	.area GSFINAL (CODE)
                                   2255 	.area CSEG    (CODE)
                                   2256 ;--------------------------------------------------------
                                   2257 ; global & static initialisations
                                   2258 ;--------------------------------------------------------
                                   2259 	.area HOME    (CODE)
                                   2260 	.area GSINIT  (CODE)
                                   2261 	.area GSFINAL (CODE)
                                   2262 	.area GSINIT  (CODE)
                                   2263 ;--------------------------------------------------------
                                   2264 ; Home
                                   2265 ;--------------------------------------------------------
                                   2266 	.area HOME    (CODE)
                                   2267 	.area HOME    (CODE)
                                   2268 ;--------------------------------------------------------
                                   2269 ; code
                                   2270 ;--------------------------------------------------------
                                   2271 	.area CSEG    (CODE)
                                   2272 ;------------------------------------------------------------
                                   2273 ;Allocation info for local variables in function 'UART_Proc_PortInit'
                                   2274 ;------------------------------------------------------------
                                   2275 ;	..\src\peripherals\UartComProc.c:24: __reentrantb void UART_Proc_PortInit(void) __reentrant
                                   2276 ;	-----------------------------------------
                                   2277 ;	 function UART_Proc_PortInit
                                   2278 ;	-----------------------------------------
      004F76                       2279 _UART_Proc_PortInit:
                           000007  2280 	ar7 = 0x07
                           000006  2281 	ar6 = 0x06
                           000005  2282 	ar5 = 0x05
                           000004  2283 	ar4 = 0x04
                           000003  2284 	ar3 = 0x03
                           000002  2285 	ar2 = 0x02
                           000001  2286 	ar1 = 0x01
                           000000  2287 	ar0 = 0x00
                                   2288 ;	..\src\peripherals\UartComProc.c:26: PALTB |= 0x11;
      004F76 90 70 09         [24] 2289 	mov	dptr,#_PALTB
      004F79 E0               [24] 2290 	movx	a,@dptr
      004F7A FF               [12] 2291 	mov	r7,a
      004F7B 74 11            [12] 2292 	mov	a,#0x11
      004F7D 4F               [12] 2293 	orl	a,r7
      004F7E F0               [24] 2294 	movx	@dptr,a
                                   2295 ;	..\src\peripherals\UartComProc.c:27: DIRB |= (1<<0) | (1<<4);
      004F7F 43 8A 11         [24] 2296 	orl	_DIRB,#0x11
                                   2297 ;	..\src\peripherals\UartComProc.c:28: DIRB &= (uint8_t)~(1<<5);
      004F82 53 8A DF         [24] 2298 	anl	_DIRB,#0xdf
                                   2299 ;	..\src\peripherals\UartComProc.c:29: DIRB &= (uint8_t)~(1<<1);
      004F85 53 8A FD         [24] 2300 	anl	_DIRB,#0xfd
                                   2301 ;	..\src\peripherals\UartComProc.c:30: PINSEL &= (uint8_t)~((1<<0) | (1<<1));
      004F88 90 70 0B         [24] 2302 	mov	dptr,#_PINSEL
      004F8B E0               [24] 2303 	movx	a,@dptr
      004F8C FF               [12] 2304 	mov	r7,a
      004F8D 74 FC            [12] 2305 	mov	a,#0xfc
      004F8F 5F               [12] 2306 	anl	a,r7
      004F90 F0               [24] 2307 	movx	@dptr,a
                                   2308 ;	..\src\peripherals\UartComProc.c:31: uart_timer1_baud(CLKSRC_SYSCLK, 9600, 20000000UL);
      004F91 E4               [12] 2309 	clr	a
      004F92 C0 E0            [24] 2310 	push	acc
      004F94 74 2D            [12] 2311 	mov	a,#0x2d
      004F96 C0 E0            [24] 2312 	push	acc
      004F98 74 31            [12] 2313 	mov	a,#0x31
      004F9A C0 E0            [24] 2314 	push	acc
      004F9C 74 01            [12] 2315 	mov	a,#0x01
      004F9E C0 E0            [24] 2316 	push	acc
      004FA0 03               [12] 2317 	rr	a
      004FA1 C0 E0            [24] 2318 	push	acc
      004FA3 74 25            [12] 2319 	mov	a,#0x25
      004FA5 C0 E0            [24] 2320 	push	acc
      004FA7 E4               [12] 2321 	clr	a
      004FA8 C0 E0            [24] 2322 	push	acc
      004FAA C0 E0            [24] 2323 	push	acc
      004FAC 75 82 06         [24] 2324 	mov	dpl,#0x06
      004FAF 12 60 A6         [24] 2325 	lcall	_uart_timer1_baud
      004FB2 E5 81            [12] 2326 	mov	a,sp
      004FB4 24 F8            [12] 2327 	add	a,#0xf8
      004FB6 F5 81            [12] 2328 	mov	sp,a
                                   2329 ;	..\src\peripherals\UartComProc.c:32: uart0_init(1, 8, 1);
      004FB8 90 04 7C         [24] 2330 	mov	dptr,#_uart0_init_PARM_2
      004FBB 74 08            [12] 2331 	mov	a,#0x08
      004FBD F0               [24] 2332 	movx	@dptr,a
      004FBE 90 04 7D         [24] 2333 	mov	dptr,#_uart0_init_PARM_3
      004FC1 74 01            [12] 2334 	mov	a,#0x01
      004FC3 F0               [24] 2335 	movx	@dptr,a
      004FC4 75 82 01         [24] 2336 	mov	dpl,#0x01
      004FC7 02 70 61         [24] 2337 	ljmp	_uart0_init
                                   2338 ;------------------------------------------------------------
                                   2339 ;Allocation info for local variables in function 'UART_Proc_VerifyIncomingMsg'
                                   2340 ;------------------------------------------------------------
                                   2341 ;sloc0                     Allocated with name '_UART_Proc_VerifyIncomingMsg_sloc0_1_0'
                                   2342 ;payload                   Allocated with name '_UART_Proc_VerifyIncomingMsg_PARM_2'
                                   2343 ;PayloadLength             Allocated with name '_UART_Proc_VerifyIncomingMsg_PARM_3'
                                   2344 ;cmd                       Allocated with name '_UART_Proc_VerifyIncomingMsg_cmd_1_238'
                                   2345 ;RetVal                    Allocated with name '_UART_Proc_VerifyIncomingMsg_RetVal_1_239'
                                   2346 ;scape                     Allocated with name '_UART_Proc_VerifyIncomingMsg_scape_1_239'
                                   2347 ;UartRxBuffer              Allocated with name '_UART_Proc_VerifyIncomingMsg_UartRxBuffer_1_239'
                                   2348 ;RxLength                  Allocated with name '_UART_Proc_VerifyIncomingMsg_RxLength_1_239'
                                   2349 ;CRC32                     Allocated with name '_UART_Proc_VerifyIncomingMsg_CRC32_1_239'
                                   2350 ;CRC32Rx                   Allocated with name '_UART_Proc_VerifyIncomingMsg_CRC32Rx_1_239'
                                   2351 ;------------------------------------------------------------
                                   2352 ;	..\src\peripherals\UartComProc.c:53: uint8_t  UART_Proc_VerifyIncomingMsg(uint8_t *cmd, uint8_t *payload,uint8_t *PayloadLength)
                                   2353 ;	-----------------------------------------
                                   2354 ;	 function UART_Proc_VerifyIncomingMsg
                                   2355 ;	-----------------------------------------
      004FCA                       2356 _UART_Proc_VerifyIncomingMsg:
      004FCA AF F0            [24] 2357 	mov	r7,b
      004FCC AE 83            [24] 2358 	mov	r6,dph
      004FCE E5 82            [12] 2359 	mov	a,dpl
      004FD0 90 03 89         [24] 2360 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_cmd_1_238
      004FD3 F0               [24] 2361 	movx	@dptr,a
      004FD4 EE               [12] 2362 	mov	a,r6
      004FD5 A3               [24] 2363 	inc	dptr
      004FD6 F0               [24] 2364 	movx	@dptr,a
      004FD7 EF               [12] 2365 	mov	a,r7
      004FD8 A3               [24] 2366 	inc	dptr
      004FD9 F0               [24] 2367 	movx	@dptr,a
                                   2368 ;	..\src\peripherals\UartComProc.c:55: uint8_t RetVal = ERR_NO_DATA;
      004FDA 90 03 8C         [24] 2369 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RetVal_1_239
      004FDD E4               [12] 2370 	clr	a
      004FDE F0               [24] 2371 	movx	@dptr,a
                                   2372 ;	..\src\peripherals\UartComProc.c:61: UartRxBuffer  =(uint8_t*) malloc(sizeof(uint8_t)*40);
      004FDF 90 00 28         [24] 2373 	mov	dptr,#0x0028
      004FE2 12 6B C4         [24] 2374 	lcall	_malloc
      004FE5 AE 82            [24] 2375 	mov	r6,dpl
      004FE7 AF 83            [24] 2376 	mov	r7,dph
                                   2377 ;	..\src\peripherals\UartComProc.c:62: if(uart0_rxcount())
      004FE9 12 85 95         [24] 2378 	lcall	_uart0_rxcount
      004FEC E5 82            [12] 2379 	mov	a,dpl
      004FEE 70 03            [24] 2380 	jnz	00149$
      004FF0 02 52 10         [24] 2381 	ljmp	00119$
      004FF3                       2382 00149$:
                                   2383 ;	..\src\peripherals\UartComProc.c:64: *UartRxBuffer = uart0_rx();
      004FF3 12 6A 03         [24] 2384 	lcall	_uart0_rx
      004FF6 AD 82            [24] 2385 	mov	r5,dpl
      004FF8 8E 82            [24] 2386 	mov	dpl,r6
      004FFA 8F 83            [24] 2387 	mov	dph,r7
      004FFC ED               [12] 2388 	mov	a,r5
      004FFD F0               [24] 2389 	movx	@dptr,a
                                   2390 ;	..\src\peripherals\UartComProc.c:65: RetVal = ERR_BAD_FORMAT; // Indico que se recibio algo, pero de volver con este valor, no se recibio una trama KISS
      004FFE 90 03 8C         [24] 2391 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RetVal_1_239
      005001 74 02            [12] 2392 	mov	a,#0x02
      005003 F0               [24] 2393 	movx	@dptr,a
                                   2394 ;	..\src\peripherals\UartComProc.c:66: delay_ms(500);
      005004 90 01 F4         [24] 2395 	mov	dptr,#0x01f4
      005007 C0 07            [24] 2396 	push	ar7
      005009 C0 06            [24] 2397 	push	ar6
      00500B 12 10 7A         [24] 2398 	lcall	_delay_ms
      00500E D0 06            [24] 2399 	pop	ar6
      005010 D0 07            [24] 2400 	pop	ar7
                                   2401 ;	..\src\peripherals\UartComProc.c:67: RxLength = 1;
      005012 90 03 8D         [24] 2402 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RxLength_1_239
      005015 74 01            [12] 2403 	mov	a,#0x01
      005017 F0               [24] 2404 	movx	@dptr,a
                                   2405 ;	..\src\peripherals\UartComProc.c:68: if(*UartRxBuffer == 0xC0)
      005018 8E 82            [24] 2406 	mov	dpl,r6
      00501A 8F 83            [24] 2407 	mov	dph,r7
      00501C E0               [24] 2408 	movx	a,@dptr
      00501D FD               [12] 2409 	mov	r5,a
      00501E BD C0 02         [24] 2410 	cjne	r5,#0xc0,00150$
      005021 80 03            [24] 2411 	sjmp	00151$
      005023                       2412 00150$:
      005023 02 52 05         [24] 2413 	ljmp	00117$
      005026                       2414 00151$:
                                   2415 ;	..\src\peripherals\UartComProc.c:70: do{
      005026 7D 00            [12] 2416 	mov	r5,#0x00
      005028 7C 01            [12] 2417 	mov	r4,#0x01
      00502A                       2418 00104$:
                                   2419 ;	..\src\peripherals\UartComProc.c:71: if(uart0_rxcount())
      00502A 12 85 95         [24] 2420 	lcall	_uart0_rxcount
      00502D E5 82            [12] 2421 	mov	a,dpl
      00502F 60 19            [24] 2422 	jz	00102$
                                   2423 ;	..\src\peripherals\UartComProc.c:73: *(UartRxBuffer+RxLength)=uart0_rx();
      005031 EC               [12] 2424 	mov	a,r4
      005032 2E               [12] 2425 	add	a,r6
      005033 FA               [12] 2426 	mov	r2,a
      005034 E4               [12] 2427 	clr	a
      005035 3F               [12] 2428 	addc	a,r7
      005036 FB               [12] 2429 	mov	r3,a
      005037 12 6A 03         [24] 2430 	lcall	_uart0_rx
      00503A A9 82            [24] 2431 	mov	r1,dpl
      00503C 8A 82            [24] 2432 	mov	dpl,r2
      00503E 8B 83            [24] 2433 	mov	dph,r3
      005040 E9               [12] 2434 	mov	a,r1
      005041 F0               [24] 2435 	movx	@dptr,a
                                   2436 ;	..\src\peripherals\UartComProc.c:74: RxLength=(RxLength)+1;
      005042 0C               [12] 2437 	inc	r4
      005043 90 03 8D         [24] 2438 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RxLength_1_239
      005046 EC               [12] 2439 	mov	a,r4
      005047 F0               [24] 2440 	movx	@dptr,a
      005048 80 17            [24] 2441 	sjmp	00105$
      00504A                       2442 00102$:
                                   2443 ;	..\src\peripherals\UartComProc.c:79: scape ++;
      00504A 0D               [12] 2444 	inc	r5
                                   2445 ;	..\src\peripherals\UartComProc.c:80: delay_ms(1);
      00504B 90 00 01         [24] 2446 	mov	dptr,#0x0001
      00504E C0 07            [24] 2447 	push	ar7
      005050 C0 06            [24] 2448 	push	ar6
      005052 C0 05            [24] 2449 	push	ar5
      005054 C0 04            [24] 2450 	push	ar4
      005056 12 10 7A         [24] 2451 	lcall	_delay_ms
      005059 D0 04            [24] 2452 	pop	ar4
      00505B D0 05            [24] 2453 	pop	ar5
      00505D D0 06            [24] 2454 	pop	ar6
      00505F D0 07            [24] 2455 	pop	ar7
      005061                       2456 00105$:
                                   2457 ;	..\src\peripherals\UartComProc.c:82: }while(*(UartRxBuffer+(RxLength-1)) != 0xC0);
      005061 8C 02            [24] 2458 	mov	ar2,r4
      005063 7B 00            [12] 2459 	mov	r3,#0x00
      005065 1A               [12] 2460 	dec	r2
      005066 BA FF 01         [24] 2461 	cjne	r2,#0xff,00153$
      005069 1B               [12] 2462 	dec	r3
      00506A                       2463 00153$:
      00506A EA               [12] 2464 	mov	a,r2
      00506B 2E               [12] 2465 	add	a,r6
      00506C F5 82            [12] 2466 	mov	dpl,a
      00506E EB               [12] 2467 	mov	a,r3
      00506F 3F               [12] 2468 	addc	a,r7
      005070 F5 83            [12] 2469 	mov	dph,a
      005072 E0               [24] 2470 	movx	a,@dptr
      005073 FB               [12] 2471 	mov	r3,a
      005074 BB C0 B3         [24] 2472 	cjne	r3,#0xc0,00104$
                                   2473 ;	..\src\peripherals\UartComProc.c:83: if(scape == 250)
      005077 90 03 8D         [24] 2474 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RxLength_1_239
      00507A EC               [12] 2475 	mov	a,r4
      00507B F0               [24] 2476 	movx	@dptr,a
      00507C BD FA 09         [24] 2477 	cjne	r5,#0xfa,00114$
                                   2478 ;	..\src\peripherals\UartComProc.c:85: RetVal = ERR_BAD_ENDING;// indico que no se recibio el paquete correctamente
      00507F 90 03 8C         [24] 2479 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RetVal_1_239
      005082 74 03            [12] 2480 	mov	a,#0x03
      005084 F0               [24] 2481 	movx	@dptr,a
      005085 02 52 05         [24] 2482 	ljmp	00117$
      005088                       2483 00114$:
                                   2484 ;	..\src\peripherals\UartComProc.c:90: UART_Proc_ModifiyKissSpecialCharacters(UartRxBuffer,&RxLength);
      005088 8E 04            [24] 2485 	mov	ar4,r6
      00508A 8F 05            [24] 2486 	mov	ar5,r7
      00508C 7B 00            [12] 2487 	mov	r3,#0x00
      00508E 90 03 92         [24] 2488 	mov	dptr,#_UART_Proc_ModifiyKissSpecialCharacters_PARM_2
      005091 74 8D            [12] 2489 	mov	a,#_UART_Proc_VerifyIncomingMsg_RxLength_1_239
      005093 F0               [24] 2490 	movx	@dptr,a
      005094 74 03            [12] 2491 	mov	a,#(_UART_Proc_VerifyIncomingMsg_RxLength_1_239 >> 8)
      005096 A3               [24] 2492 	inc	dptr
      005097 F0               [24] 2493 	movx	@dptr,a
      005098 E4               [12] 2494 	clr	a
      005099 A3               [24] 2495 	inc	dptr
      00509A F0               [24] 2496 	movx	@dptr,a
      00509B 8C 82            [24] 2497 	mov	dpl,r4
      00509D 8D 83            [24] 2498 	mov	dph,r5
      00509F 8B F0            [24] 2499 	mov	b,r3
      0050A1 C0 07            [24] 2500 	push	ar7
      0050A3 C0 06            [24] 2501 	push	ar6
      0050A5 12 52 17         [24] 2502 	lcall	_UART_Proc_ModifiyKissSpecialCharacters
      0050A8 D0 06            [24] 2503 	pop	ar6
      0050AA D0 07            [24] 2504 	pop	ar7
                                   2505 ;	..\src\peripherals\UartComProc.c:92: if(*UartRxBuffer == 0xC0 && *(UartRxBuffer+RxLength-1) == 0xC0)
      0050AC 8E 82            [24] 2506 	mov	dpl,r6
      0050AE 8F 83            [24] 2507 	mov	dph,r7
      0050B0 E0               [24] 2508 	movx	a,@dptr
      0050B1 FD               [12] 2509 	mov	r5,a
      0050B2 BD C0 02         [24] 2510 	cjne	r5,#0xc0,00158$
      0050B5 80 03            [24] 2511 	sjmp	00159$
      0050B7                       2512 00158$:
      0050B7 02 52 05         [24] 2513 	ljmp	00117$
      0050BA                       2514 00159$:
      0050BA 90 03 8D         [24] 2515 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RxLength_1_239
      0050BD E0               [24] 2516 	movx	a,@dptr
      0050BE FD               [12] 2517 	mov	r5,a
      0050BF 2E               [12] 2518 	add	a,r6
      0050C0 FB               [12] 2519 	mov	r3,a
      0050C1 E4               [12] 2520 	clr	a
      0050C2 3F               [12] 2521 	addc	a,r7
      0050C3 FC               [12] 2522 	mov	r4,a
      0050C4 EB               [12] 2523 	mov	a,r3
      0050C5 24 FF            [12] 2524 	add	a,#0xff
      0050C7 F5 82            [12] 2525 	mov	dpl,a
      0050C9 EC               [12] 2526 	mov	a,r4
      0050CA 34 FF            [12] 2527 	addc	a,#0xff
      0050CC F5 83            [12] 2528 	mov	dph,a
      0050CE E0               [24] 2529 	movx	a,@dptr
      0050CF FC               [12] 2530 	mov	r4,a
      0050D0 BC C0 02         [24] 2531 	cjne	r4,#0xc0,00160$
      0050D3 80 03            [24] 2532 	sjmp	00161$
      0050D5                       2533 00160$:
      0050D5 02 52 05         [24] 2534 	ljmp	00117$
      0050D8                       2535 00161$:
                                   2536 ;	..\src\peripherals\UartComProc.c:94: CRC32 = UART_Calc_CRC32(UartRxBuffer+1,RxLength-START_CHARACTER_LENGTH-END_CHARACTER_LENGTH-CRC_LENGTH,1);// lo pongo en little endian, ya que el memcpy lo pasa asi
      0050D8 74 01            [12] 2537 	mov	a,#0x01
      0050DA 2E               [12] 2538 	add	a,r6
      0050DB FB               [12] 2539 	mov	r3,a
      0050DC E4               [12] 2540 	clr	a
      0050DD 3F               [12] 2541 	addc	a,r7
      0050DE FC               [12] 2542 	mov	r4,a
      0050DF 8B 00            [24] 2543 	mov	ar0,r3
      0050E1 8C 01            [24] 2544 	mov	ar1,r4
      0050E3 7A 00            [12] 2545 	mov	r2,#0x00
      0050E5 ED               [12] 2546 	mov	a,r5
      0050E6 24 FA            [12] 2547 	add	a,#0xfa
      0050E8 90 03 9D         [24] 2548 	mov	dptr,#_UART_Calc_CRC32_PARM_2
      0050EB F0               [24] 2549 	movx	@dptr,a
      0050EC 90 03 9E         [24] 2550 	mov	dptr,#_UART_Calc_CRC32_PARM_3
      0050EF 74 01            [12] 2551 	mov	a,#0x01
      0050F1 F0               [24] 2552 	movx	@dptr,a
      0050F2 88 82            [24] 2553 	mov	dpl,r0
      0050F4 89 83            [24] 2554 	mov	dph,r1
      0050F6 8A F0            [24] 2555 	mov	b,r2
      0050F8 C0 07            [24] 2556 	push	ar7
      0050FA C0 06            [24] 2557 	push	ar6
      0050FC C0 04            [24] 2558 	push	ar4
      0050FE C0 03            [24] 2559 	push	ar3
      005100 12 54 6D         [24] 2560 	lcall	_UART_Calc_CRC32
      005103 85 82 22         [24] 2561 	mov	_UART_Proc_VerifyIncomingMsg_sloc0_1_0,dpl
      005106 85 83 23         [24] 2562 	mov	(_UART_Proc_VerifyIncomingMsg_sloc0_1_0 + 1),dph
      005109 85 F0 24         [24] 2563 	mov	(_UART_Proc_VerifyIncomingMsg_sloc0_1_0 + 2),b
      00510C F5 25            [12] 2564 	mov	(_UART_Proc_VerifyIncomingMsg_sloc0_1_0 + 3),a
      00510E D0 03            [24] 2565 	pop	ar3
      005110 D0 04            [24] 2566 	pop	ar4
      005112 D0 06            [24] 2567 	pop	ar6
      005114 D0 07            [24] 2568 	pop	ar7
                                   2569 ;	..\src\peripherals\UartComProc.c:95: memcpy(&CRC32Rx,UartRxBuffer+RxLength-START_CHARACTER_LENGTH-CRC_LENGTH,CRC_LENGTH);
      005116 90 03 8D         [24] 2570 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RxLength_1_239
      005119 E0               [24] 2571 	movx	a,@dptr
      00511A 2E               [12] 2572 	add	a,r6
      00511B FD               [12] 2573 	mov	r5,a
      00511C E4               [12] 2574 	clr	a
      00511D 3F               [12] 2575 	addc	a,r7
      00511E FA               [12] 2576 	mov	r2,a
      00511F ED               [12] 2577 	mov	a,r5
      005120 24 FB            [12] 2578 	add	a,#0xfb
      005122 FD               [12] 2579 	mov	r5,a
      005123 EA               [12] 2580 	mov	a,r2
      005124 34 FF            [12] 2581 	addc	a,#0xff
      005126 FA               [12] 2582 	mov	r2,a
      005127 90 03 E2         [24] 2583 	mov	dptr,#_memcpy_PARM_2
      00512A ED               [12] 2584 	mov	a,r5
      00512B F0               [24] 2585 	movx	@dptr,a
      00512C EA               [12] 2586 	mov	a,r2
      00512D A3               [24] 2587 	inc	dptr
      00512E F0               [24] 2588 	movx	@dptr,a
      00512F E4               [12] 2589 	clr	a
      005130 A3               [24] 2590 	inc	dptr
      005131 F0               [24] 2591 	movx	@dptr,a
      005132 90 03 E5         [24] 2592 	mov	dptr,#_memcpy_PARM_3
      005135 74 04            [12] 2593 	mov	a,#0x04
      005137 F0               [24] 2594 	movx	@dptr,a
      005138 E4               [12] 2595 	clr	a
      005139 A3               [24] 2596 	inc	dptr
      00513A F0               [24] 2597 	movx	@dptr,a
      00513B 90 03 8E         [24] 2598 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_CRC32Rx_1_239
      00513E 75 F0 00         [24] 2599 	mov	b,#0x00
      005141 C0 07            [24] 2600 	push	ar7
      005143 C0 06            [24] 2601 	push	ar6
      005145 C0 04            [24] 2602 	push	ar4
      005147 C0 03            [24] 2603 	push	ar3
      005149 12 65 97         [24] 2604 	lcall	_memcpy
      00514C D0 03            [24] 2605 	pop	ar3
      00514E D0 04            [24] 2606 	pop	ar4
      005150 D0 06            [24] 2607 	pop	ar6
      005152 D0 07            [24] 2608 	pop	ar7
                                   2609 ;	..\src\peripherals\UartComProc.c:97: if(CRC32Rx == CRC32)
      005154 90 03 8E         [24] 2610 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_CRC32Rx_1_239
      005157 E0               [24] 2611 	movx	a,@dptr
      005158 F8               [12] 2612 	mov	r0,a
      005159 A3               [24] 2613 	inc	dptr
      00515A E0               [24] 2614 	movx	a,@dptr
      00515B F9               [12] 2615 	mov	r1,a
      00515C A3               [24] 2616 	inc	dptr
      00515D E0               [24] 2617 	movx	a,@dptr
      00515E FA               [12] 2618 	mov	r2,a
      00515F A3               [24] 2619 	inc	dptr
      005160 E0               [24] 2620 	movx	a,@dptr
      005161 FD               [12] 2621 	mov	r5,a
      005162 E8               [12] 2622 	mov	a,r0
      005163 B5 22 0E         [24] 2623 	cjne	a,_UART_Proc_VerifyIncomingMsg_sloc0_1_0,00162$
      005166 E9               [12] 2624 	mov	a,r1
      005167 B5 23 0A         [24] 2625 	cjne	a,(_UART_Proc_VerifyIncomingMsg_sloc0_1_0 + 1),00162$
      00516A EA               [12] 2626 	mov	a,r2
      00516B B5 24 06         [24] 2627 	cjne	a,(_UART_Proc_VerifyIncomingMsg_sloc0_1_0 + 2),00162$
      00516E ED               [12] 2628 	mov	a,r5
      00516F B5 25 02         [24] 2629 	cjne	a,(_UART_Proc_VerifyIncomingMsg_sloc0_1_0 + 3),00162$
      005172 80 02            [24] 2630 	sjmp	00163$
      005174                       2631 00162$:
      005174 80 6E            [24] 2632 	sjmp	00108$
      005176                       2633 00163$:
                                   2634 ;	..\src\peripherals\UartComProc.c:100: RetVal = RX_SUCCESFULL;
      005176 90 03 8C         [24] 2635 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RetVal_1_239
      005179 74 04            [12] 2636 	mov	a,#0x04
      00517B F0               [24] 2637 	movx	@dptr,a
                                   2638 ;	..\src\peripherals\UartComProc.c:101: *cmd = *(UartRxBuffer+1);
      00517C 90 03 89         [24] 2639 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_cmd_1_238
      00517F E0               [24] 2640 	movx	a,@dptr
      005180 F9               [12] 2641 	mov	r1,a
      005181 A3               [24] 2642 	inc	dptr
      005182 E0               [24] 2643 	movx	a,@dptr
      005183 FA               [12] 2644 	mov	r2,a
      005184 A3               [24] 2645 	inc	dptr
      005185 E0               [24] 2646 	movx	a,@dptr
      005186 FD               [12] 2647 	mov	r5,a
      005187 8B 82            [24] 2648 	mov	dpl,r3
      005189 8C 83            [24] 2649 	mov	dph,r4
      00518B E0               [24] 2650 	movx	a,@dptr
      00518C 89 82            [24] 2651 	mov	dpl,r1
      00518E 8A 83            [24] 2652 	mov	dph,r2
      005190 8D F0            [24] 2653 	mov	b,r5
      005192 12 6A 1E         [24] 2654 	lcall	__gptrput
                                   2655 ;	..\src\peripherals\UartComProc.c:102: memcpy(payload,UartRxBuffer+START_CHARACTER_LENGTH+COMMAND_LENGTH,RxLength-START_CHARACTER_LENGTH-CRC_LENGTH-END_CHARACTER_LENGTH); // porq esta linea me adelanta !!
      005195 90 03 83         [24] 2656 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_PARM_2
      005198 E0               [24] 2657 	movx	a,@dptr
      005199 FB               [12] 2658 	mov	r3,a
      00519A A3               [24] 2659 	inc	dptr
      00519B E0               [24] 2660 	movx	a,@dptr
      00519C FC               [12] 2661 	mov	r4,a
      00519D A3               [24] 2662 	inc	dptr
      00519E E0               [24] 2663 	movx	a,@dptr
      00519F FD               [12] 2664 	mov	r5,a
      0051A0 74 02            [12] 2665 	mov	a,#0x02
      0051A2 2E               [12] 2666 	add	a,r6
      0051A3 F9               [12] 2667 	mov	r1,a
      0051A4 E4               [12] 2668 	clr	a
      0051A5 3F               [12] 2669 	addc	a,r7
      0051A6 F8               [12] 2670 	mov	r0,a
      0051A7 7A 00            [12] 2671 	mov	r2,#0x00
      0051A9 C0 06            [24] 2672 	push	ar6
      0051AB C0 07            [24] 2673 	push	ar7
      0051AD 90 03 8D         [24] 2674 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RxLength_1_239
      0051B0 E0               [24] 2675 	movx	a,@dptr
      0051B1 7E 00            [12] 2676 	mov	r6,#0x00
      0051B3 24 FA            [12] 2677 	add	a,#0xfa
      0051B5 FF               [12] 2678 	mov	r7,a
      0051B6 EE               [12] 2679 	mov	a,r6
      0051B7 34 FF            [12] 2680 	addc	a,#0xff
      0051B9 FE               [12] 2681 	mov	r6,a
      0051BA 90 03 E2         [24] 2682 	mov	dptr,#_memcpy_PARM_2
      0051BD E9               [12] 2683 	mov	a,r1
      0051BE F0               [24] 2684 	movx	@dptr,a
      0051BF E8               [12] 2685 	mov	a,r0
      0051C0 A3               [24] 2686 	inc	dptr
      0051C1 F0               [24] 2687 	movx	@dptr,a
      0051C2 EA               [12] 2688 	mov	a,r2
      0051C3 A3               [24] 2689 	inc	dptr
      0051C4 F0               [24] 2690 	movx	@dptr,a
      0051C5 90 03 E5         [24] 2691 	mov	dptr,#_memcpy_PARM_3
      0051C8 EF               [12] 2692 	mov	a,r7
      0051C9 F0               [24] 2693 	movx	@dptr,a
      0051CA EE               [12] 2694 	mov	a,r6
      0051CB A3               [24] 2695 	inc	dptr
      0051CC F0               [24] 2696 	movx	@dptr,a
      0051CD 8B 82            [24] 2697 	mov	dpl,r3
      0051CF 8C 83            [24] 2698 	mov	dph,r4
      0051D1 8D F0            [24] 2699 	mov	b,r5
      0051D3 C0 07            [24] 2700 	push	ar7
      0051D5 C0 06            [24] 2701 	push	ar6
      0051D7 12 65 97         [24] 2702 	lcall	_memcpy
      0051DA D0 06            [24] 2703 	pop	ar6
      0051DC D0 07            [24] 2704 	pop	ar7
      0051DE D0 07            [24] 2705 	pop	ar7
      0051E0 D0 06            [24] 2706 	pop	ar6
      0051E2 80 06            [24] 2707 	sjmp	00109$
      0051E4                       2708 00108$:
                                   2709 ;	..\src\peripherals\UartComProc.c:106: RetVal = ERR_BAD_CRC;
      0051E4 90 03 8C         [24] 2710 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RetVal_1_239
      0051E7 74 01            [12] 2711 	mov	a,#0x01
      0051E9 F0               [24] 2712 	movx	@dptr,a
      0051EA                       2713 00109$:
                                   2714 ;	..\src\peripherals\UartComProc.c:108: *PayloadLength = RxLength -START_CHARACTER_LENGTH-COMMAND_LENGTH-CRC_LENGTH-END_CHARACTER_LENGTH;
      0051EA 90 03 86         [24] 2715 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_PARM_3
      0051ED E0               [24] 2716 	movx	a,@dptr
      0051EE FB               [12] 2717 	mov	r3,a
      0051EF A3               [24] 2718 	inc	dptr
      0051F0 E0               [24] 2719 	movx	a,@dptr
      0051F1 FC               [12] 2720 	mov	r4,a
      0051F2 A3               [24] 2721 	inc	dptr
      0051F3 E0               [24] 2722 	movx	a,@dptr
      0051F4 FD               [12] 2723 	mov	r5,a
      0051F5 90 03 8D         [24] 2724 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RxLength_1_239
      0051F8 E0               [24] 2725 	movx	a,@dptr
      0051F9 24 F9            [12] 2726 	add	a,#0xf9
      0051FB FA               [12] 2727 	mov	r2,a
      0051FC 8B 82            [24] 2728 	mov	dpl,r3
      0051FE 8C 83            [24] 2729 	mov	dph,r4
      005200 8D F0            [24] 2730 	mov	b,r5
      005202 12 6A 1E         [24] 2731 	lcall	__gptrput
      005205                       2732 00117$:
                                   2733 ;	..\src\peripherals\UartComProc.c:112: free(UartRxBuffer);
      005205 7D 00            [12] 2734 	mov	r5,#0x00
      005207 8E 82            [24] 2735 	mov	dpl,r6
      005209 8F 83            [24] 2736 	mov	dph,r7
      00520B 8D F0            [24] 2737 	mov	b,r5
      00520D 12 5B 4D         [24] 2738 	lcall	_free
      005210                       2739 00119$:
                                   2740 ;	..\src\peripherals\UartComProc.c:114: return RetVal;
      005210 90 03 8C         [24] 2741 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RetVal_1_239
      005213 E0               [24] 2742 	movx	a,@dptr
      005214 F5 82            [12] 2743 	mov	dpl,a
      005216 22               [24] 2744 	ret
                                   2745 ;------------------------------------------------------------
                                   2746 ;Allocation info for local variables in function 'UART_Proc_ModifiyKissSpecialCharacters'
                                   2747 ;------------------------------------------------------------
                                   2748 ;RxLength                  Allocated with name '_UART_Proc_ModifiyKissSpecialCharacters_PARM_2'
                                   2749 ;UartRxBuffer              Allocated with name '_UART_Proc_ModifiyKissSpecialCharacters_UartRxBuffer_1_250'
                                   2750 ;i                         Allocated with name '_UART_Proc_ModifiyKissSpecialCharacters_i_1_251'
                                   2751 ;j                         Allocated with name '_UART_Proc_ModifiyKissSpecialCharacters_j_1_251'
                                   2752 ;sloc0                     Allocated with name '_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0'
                                   2753 ;sloc1                     Allocated with name '_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0'
                                   2754 ;sloc2                     Allocated with name '_UART_Proc_ModifiyKissSpecialCharacters_sloc2_1_0'
                                   2755 ;sloc3                     Allocated with name '_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0'
                                   2756 ;------------------------------------------------------------
                                   2757 ;	..\src\peripherals\UartComProc.c:130: void UART_Proc_ModifiyKissSpecialCharacters(uint8_t *UartRxBuffer, uint8_t *RxLength)
                                   2758 ;	-----------------------------------------
                                   2759 ;	 function UART_Proc_ModifiyKissSpecialCharacters
                                   2760 ;	-----------------------------------------
      005217                       2761 _UART_Proc_ModifiyKissSpecialCharacters:
      005217 AF F0            [24] 2762 	mov	r7,b
      005219 AE 83            [24] 2763 	mov	r6,dph
      00521B E5 82            [12] 2764 	mov	a,dpl
      00521D 90 03 95         [24] 2765 	mov	dptr,#_UART_Proc_ModifiyKissSpecialCharacters_UartRxBuffer_1_250
      005220 F0               [24] 2766 	movx	@dptr,a
      005221 EE               [12] 2767 	mov	a,r6
      005222 A3               [24] 2768 	inc	dptr
      005223 F0               [24] 2769 	movx	@dptr,a
      005224 EF               [12] 2770 	mov	a,r7
      005225 A3               [24] 2771 	inc	dptr
      005226 F0               [24] 2772 	movx	@dptr,a
                                   2773 ;	..\src\peripherals\UartComProc.c:134: for(i=0;i<*RxLength;i++)
      005227 90 03 95         [24] 2774 	mov	dptr,#_UART_Proc_ModifiyKissSpecialCharacters_UartRxBuffer_1_250
      00522A E0               [24] 2775 	movx	a,@dptr
      00522B F5 46            [12] 2776 	mov	_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0,a
      00522D A3               [24] 2777 	inc	dptr
      00522E E0               [24] 2778 	movx	a,@dptr
      00522F F5 47            [12] 2779 	mov	(_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0 + 1),a
      005231 A3               [24] 2780 	inc	dptr
      005232 E0               [24] 2781 	movx	a,@dptr
      005233 F5 48            [12] 2782 	mov	(_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0 + 2),a
      005235 7C 00            [12] 2783 	mov	r4,#0x00
      005237                       2784 00118$:
      005237 90 03 92         [24] 2785 	mov	dptr,#_UART_Proc_ModifiyKissSpecialCharacters_PARM_2
      00523A E0               [24] 2786 	movx	a,@dptr
      00523B F9               [12] 2787 	mov	r1,a
      00523C A3               [24] 2788 	inc	dptr
      00523D E0               [24] 2789 	movx	a,@dptr
      00523E FA               [12] 2790 	mov	r2,a
      00523F A3               [24] 2791 	inc	dptr
      005240 E0               [24] 2792 	movx	a,@dptr
      005241 FB               [12] 2793 	mov	r3,a
      005242 89 82            [24] 2794 	mov	dpl,r1
      005244 8A 83            [24] 2795 	mov	dph,r2
      005246 8B F0            [24] 2796 	mov	b,r3
      005248 12 79 65         [24] 2797 	lcall	__gptrget
      00524B F8               [12] 2798 	mov	r0,a
      00524C C3               [12] 2799 	clr	c
      00524D EC               [12] 2800 	mov	a,r4
      00524E 98               [12] 2801 	subb	a,r0
      00524F 40 01            [24] 2802 	jc	00152$
      005251 22               [24] 2803 	ret
      005252                       2804 00152$:
                                   2805 ;	..\src\peripherals\UartComProc.c:136: if (*(UartRxBuffer+i) == FRAME_SCAPE)
      005252 EC               [12] 2806 	mov	a,r4
      005253 25 46            [12] 2807 	add	a,_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0
      005255 F5 43            [12] 2808 	mov	_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0,a
      005257 E4               [12] 2809 	clr	a
      005258 35 47            [12] 2810 	addc	a,(_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0 + 1)
      00525A F5 44            [12] 2811 	mov	(_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0 + 1),a
      00525C 85 48 45         [24] 2812 	mov	(_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0 + 2),(_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0 + 2)
      00525F 85 43 82         [24] 2813 	mov	dpl,_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0
      005262 85 44 83         [24] 2814 	mov	dph,(_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0 + 1)
      005265 85 45 F0         [24] 2815 	mov	b,(_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0 + 2)
      005268 12 79 65         [24] 2816 	lcall	__gptrget
      00526B F8               [12] 2817 	mov	r0,a
      00526C B8 DB 02         [24] 2818 	cjne	r0,#0xdb,00153$
      00526F 80 03            [24] 2819 	sjmp	00154$
      005271                       2820 00153$:
      005271 02 53 60         [24] 2821 	ljmp	00119$
      005274                       2822 00154$:
                                   2823 ;	..\src\peripherals\UartComProc.c:138: if(*(UartRxBuffer+i+1)== TRANSPOSE_FRAME_END)
      005274 C0 04            [24] 2824 	push	ar4
      005276 74 01            [12] 2825 	mov	a,#0x01
      005278 25 43            [12] 2826 	add	a,_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0
      00527A F8               [12] 2827 	mov	r0,a
      00527B E4               [12] 2828 	clr	a
      00527C 35 44            [12] 2829 	addc	a,(_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0 + 1)
      00527E FC               [12] 2830 	mov	r4,a
      00527F AF 45            [24] 2831 	mov	r7,(_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0 + 2)
      005281 88 82            [24] 2832 	mov	dpl,r0
      005283 8C 83            [24] 2833 	mov	dph,r4
      005285 8F F0            [24] 2834 	mov	b,r7
      005287 12 79 65         [24] 2835 	lcall	__gptrget
      00528A F5 49            [12] 2836 	mov	_UART_Proc_ModifiyKissSpecialCharacters_sloc2_1_0,a
      00528C 74 DC            [12] 2837 	mov	a,#0xdc
      00528E B5 49 02         [24] 2838 	cjne	a,_UART_Proc_ModifiyKissSpecialCharacters_sloc2_1_0,00155$
      005291 80 04            [24] 2839 	sjmp	00156$
      005293                       2840 00155$:
      005293 D0 04            [24] 2841 	pop	ar4
      005295 80 64            [24] 2842 	sjmp	00106$
      005297                       2843 00156$:
      005297 D0 04            [24] 2844 	pop	ar4
                                   2845 ;	..\src\peripherals\UartComProc.c:140: *(UartRxBuffer+i) = FRAME_END;
      005299 85 43 82         [24] 2846 	mov	dpl,_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0
      00529C 85 44 83         [24] 2847 	mov	dph,(_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0 + 1)
      00529F 85 45 F0         [24] 2848 	mov	b,(_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0 + 2)
      0052A2 74 C0            [12] 2849 	mov	a,#0xc0
      0052A4 12 6A 1E         [24] 2850 	lcall	__gptrput
                                   2851 ;	..\src\peripherals\UartComProc.c:141: for(j=i+1;j<*RxLength;j++)
      0052A7 EC               [12] 2852 	mov	a,r4
      0052A8 04               [12] 2853 	inc	a
      0052A9 FF               [12] 2854 	mov	r7,a
      0052AA                       2855 00112$:
      0052AA 89 82            [24] 2856 	mov	dpl,r1
      0052AC 8A 83            [24] 2857 	mov	dph,r2
      0052AE 8B F0            [24] 2858 	mov	b,r3
      0052B0 12 79 65         [24] 2859 	lcall	__gptrget
      0052B3 FE               [12] 2860 	mov	r6,a
      0052B4 C3               [12] 2861 	clr	c
      0052B5 EF               [12] 2862 	mov	a,r7
      0052B6 9E               [12] 2863 	subb	a,r6
      0052B7 50 35            [24] 2864 	jnc	00101$
                                   2865 ;	..\src\peripherals\UartComProc.c:143: *(UartRxBuffer+j) = *(UartRxBuffer+j+1);
      0052B9 C0 04            [24] 2866 	push	ar4
      0052BB EF               [12] 2867 	mov	a,r7
      0052BC 25 46            [12] 2868 	add	a,_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0
      0052BE F5 4A            [12] 2869 	mov	_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0,a
      0052C0 E4               [12] 2870 	clr	a
      0052C1 35 47            [12] 2871 	addc	a,(_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0 + 1)
      0052C3 F5 4B            [12] 2872 	mov	(_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0 + 1),a
      0052C5 85 48 4C         [24] 2873 	mov	(_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0 + 2),(_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0 + 2)
      0052C8 74 01            [12] 2874 	mov	a,#0x01
      0052CA 25 4A            [12] 2875 	add	a,_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0
      0052CC F8               [12] 2876 	mov	r0,a
      0052CD E4               [12] 2877 	clr	a
      0052CE 35 4B            [12] 2878 	addc	a,(_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0 + 1)
      0052D0 FC               [12] 2879 	mov	r4,a
      0052D1 AD 4C            [24] 2880 	mov	r5,(_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0 + 2)
      0052D3 88 82            [24] 2881 	mov	dpl,r0
      0052D5 8C 83            [24] 2882 	mov	dph,r4
      0052D7 8D F0            [24] 2883 	mov	b,r5
      0052D9 12 79 65         [24] 2884 	lcall	__gptrget
      0052DC F8               [12] 2885 	mov	r0,a
      0052DD 85 4A 82         [24] 2886 	mov	dpl,_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0
      0052E0 85 4B 83         [24] 2887 	mov	dph,(_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0 + 1)
      0052E3 85 4C F0         [24] 2888 	mov	b,(_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0 + 2)
      0052E6 12 6A 1E         [24] 2889 	lcall	__gptrput
                                   2890 ;	..\src\peripherals\UartComProc.c:141: for(j=i+1;j<*RxLength;j++)
      0052E9 0F               [12] 2891 	inc	r7
      0052EA D0 04            [24] 2892 	pop	ar4
      0052EC 80 BC            [24] 2893 	sjmp	00112$
      0052EE                       2894 00101$:
                                   2895 ;	..\src\peripherals\UartComProc.c:145: *RxLength= *RxLength -1; //suprimi un caracter por lo cual el mensaje se disminuye su tama�o en 1
      0052EE 1E               [12] 2896 	dec	r6
      0052EF 89 82            [24] 2897 	mov	dpl,r1
      0052F1 8A 83            [24] 2898 	mov	dph,r2
      0052F3 8B F0            [24] 2899 	mov	b,r3
      0052F5 EE               [12] 2900 	mov	a,r6
      0052F6 12 6A 1E         [24] 2901 	lcall	__gptrput
      0052F9 80 65            [24] 2902 	sjmp	00119$
      0052FB                       2903 00106$:
                                   2904 ;	..\src\peripherals\UartComProc.c:147: else if(*(UartRxBuffer+i+1) == TRANSPOSE_FRAME_ESCAPE)
      0052FB 74 DD            [12] 2905 	mov	a,#0xdd
      0052FD B5 49 60         [24] 2906 	cjne	a,_UART_Proc_ModifiyKissSpecialCharacters_sloc2_1_0,00119$
                                   2907 ;	..\src\peripherals\UartComProc.c:149: *(UartRxBuffer+i) = FRAME_SCAPE;
      005300 85 43 82         [24] 2908 	mov	dpl,_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0
      005303 85 44 83         [24] 2909 	mov	dph,(_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0 + 1)
      005306 85 45 F0         [24] 2910 	mov	b,(_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0 + 2)
      005309 74 DB            [12] 2911 	mov	a,#0xdb
      00530B 12 6A 1E         [24] 2912 	lcall	__gptrput
                                   2913 ;	..\src\peripherals\UartComProc.c:150: for(j=i+1;j<*RxLength;j++)
      00530E EC               [12] 2914 	mov	a,r4
      00530F 04               [12] 2915 	inc	a
      005310 FF               [12] 2916 	mov	r7,a
      005311                       2917 00115$:
      005311 89 82            [24] 2918 	mov	dpl,r1
      005313 8A 83            [24] 2919 	mov	dph,r2
      005315 8B F0            [24] 2920 	mov	b,r3
      005317 12 79 65         [24] 2921 	lcall	__gptrget
      00531A FE               [12] 2922 	mov	r6,a
      00531B C3               [12] 2923 	clr	c
      00531C EF               [12] 2924 	mov	a,r7
      00531D 9E               [12] 2925 	subb	a,r6
      00531E 50 35            [24] 2926 	jnc	00102$
                                   2927 ;	..\src\peripherals\UartComProc.c:152: *(UartRxBuffer+j) = *(UartRxBuffer+j+1);
      005320 C0 04            [24] 2928 	push	ar4
      005322 EF               [12] 2929 	mov	a,r7
      005323 25 46            [12] 2930 	add	a,_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0
      005325 F5 4A            [12] 2931 	mov	_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0,a
      005327 E4               [12] 2932 	clr	a
      005328 35 47            [12] 2933 	addc	a,(_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0 + 1)
      00532A F5 4B            [12] 2934 	mov	(_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0 + 1),a
      00532C 85 48 4C         [24] 2935 	mov	(_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0 + 2),(_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0 + 2)
      00532F 74 01            [12] 2936 	mov	a,#0x01
      005331 25 4A            [12] 2937 	add	a,_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0
      005333 F8               [12] 2938 	mov	r0,a
      005334 E4               [12] 2939 	clr	a
      005335 35 4B            [12] 2940 	addc	a,(_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0 + 1)
      005337 FC               [12] 2941 	mov	r4,a
      005338 AD 4C            [24] 2942 	mov	r5,(_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0 + 2)
      00533A 88 82            [24] 2943 	mov	dpl,r0
      00533C 8C 83            [24] 2944 	mov	dph,r4
      00533E 8D F0            [24] 2945 	mov	b,r5
      005340 12 79 65         [24] 2946 	lcall	__gptrget
      005343 F8               [12] 2947 	mov	r0,a
      005344 85 4A 82         [24] 2948 	mov	dpl,_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0
      005347 85 4B 83         [24] 2949 	mov	dph,(_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0 + 1)
      00534A 85 4C F0         [24] 2950 	mov	b,(_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0 + 2)
      00534D 12 6A 1E         [24] 2951 	lcall	__gptrput
                                   2952 ;	..\src\peripherals\UartComProc.c:150: for(j=i+1;j<*RxLength;j++)
      005350 0F               [12] 2953 	inc	r7
      005351 D0 04            [24] 2954 	pop	ar4
      005353 80 BC            [24] 2955 	sjmp	00115$
      005355                       2956 00102$:
                                   2957 ;	..\src\peripherals\UartComProc.c:154: *RxLength = *RxLength -1; //suprimi un caracter por lo cual el mensaje se disminuye su tama�o en 1
      005355 1E               [12] 2958 	dec	r6
      005356 89 82            [24] 2959 	mov	dpl,r1
      005358 8A 83            [24] 2960 	mov	dph,r2
      00535A 8B F0            [24] 2961 	mov	b,r3
      00535C EE               [12] 2962 	mov	a,r6
      00535D 12 6A 1E         [24] 2963 	lcall	__gptrput
      005360                       2964 00119$:
                                   2965 ;	..\src\peripherals\UartComProc.c:134: for(i=0;i<*RxLength;i++)
      005360 0C               [12] 2966 	inc	r4
      005361 02 52 37         [24] 2967 	ljmp	00118$
                                   2968 ;------------------------------------------------------------
                                   2969 ;Allocation info for local variables in function 'UART_Proc_SendMessage'
                                   2970 ;------------------------------------------------------------
                                   2971 ;sloc0                     Allocated with name '_UART_Proc_SendMessage_sloc0_1_0'
                                   2972 ;length                    Allocated with name '_UART_Proc_SendMessage_PARM_2'
                                   2973 ;command                   Allocated with name '_UART_Proc_SendMessage_PARM_3'
                                   2974 ;payload                   Allocated with name '_UART_Proc_SendMessage_payload_1_258'
                                   2975 ;i                         Allocated with name '_UART_Proc_SendMessage_i_1_259'
                                   2976 ;RetVal                    Allocated with name '_UART_Proc_SendMessage_RetVal_1_259'
                                   2977 ;CrcBuffer                 Allocated with name '_UART_Proc_SendMessage_CrcBuffer_1_259'
                                   2978 ;CRC32                     Allocated with name '_UART_Proc_SendMessage_CRC32_1_259'
                                   2979 ;------------------------------------------------------------
                                   2980 ;	..\src\peripherals\UartComProc.c:180: uint8_t UART_Proc_SendMessage(uint8_t *payload, uint8_t length, uint8_t command)
                                   2981 ;	-----------------------------------------
                                   2982 ;	 function UART_Proc_SendMessage
                                   2983 ;	-----------------------------------------
      005364                       2984 _UART_Proc_SendMessage:
      005364 AF F0            [24] 2985 	mov	r7,b
      005366 AE 83            [24] 2986 	mov	r6,dph
      005368 E5 82            [12] 2987 	mov	a,dpl
      00536A 90 03 9A         [24] 2988 	mov	dptr,#_UART_Proc_SendMessage_payload_1_258
      00536D F0               [24] 2989 	movx	@dptr,a
      00536E EE               [12] 2990 	mov	a,r6
      00536F A3               [24] 2991 	inc	dptr
      005370 F0               [24] 2992 	movx	@dptr,a
      005371 EF               [12] 2993 	mov	a,r7
      005372 A3               [24] 2994 	inc	dptr
      005373 F0               [24] 2995 	movx	@dptr,a
                                   2996 ;	..\src\peripherals\UartComProc.c:186: CrcBuffer = (uint8_t*) malloc(sizeof(uint8_t)*(length+1));
      005374 90 03 98         [24] 2997 	mov	dptr,#_UART_Proc_SendMessage_PARM_2
      005377 E0               [24] 2998 	movx	a,@dptr
      005378 FF               [12] 2999 	mov	r7,a
      005379 FD               [12] 3000 	mov	r5,a
      00537A 7E 00            [12] 3001 	mov	r6,#0x00
      00537C 0D               [12] 3002 	inc	r5
      00537D BD 00 01         [24] 3003 	cjne	r5,#0x00,00114$
      005380 0E               [12] 3004 	inc	r6
      005381                       3005 00114$:
      005381 8D 82            [24] 3006 	mov	dpl,r5
      005383 8E 83            [24] 3007 	mov	dph,r6
      005385 C0 07            [24] 3008 	push	ar7
      005387 12 6B C4         [24] 3009 	lcall	_malloc
      00538A AD 82            [24] 3010 	mov	r5,dpl
      00538C AE 83            [24] 3011 	mov	r6,dph
      00538E D0 07            [24] 3012 	pop	ar7
                                   3013 ;	..\src\peripherals\UartComProc.c:187: *CrcBuffer = command;
      005390 90 03 99         [24] 3014 	mov	dptr,#_UART_Proc_SendMessage_PARM_3
      005393 E0               [24] 3015 	movx	a,@dptr
      005394 FC               [12] 3016 	mov	r4,a
      005395 8D 82            [24] 3017 	mov	dpl,r5
      005397 8E 83            [24] 3018 	mov	dph,r6
      005399 F0               [24] 3019 	movx	@dptr,a
                                   3020 ;	..\src\peripherals\UartComProc.c:189: memcpy(CrcBuffer+1,payload,length);
      00539A 74 01            [12] 3021 	mov	a,#0x01
      00539C 2D               [12] 3022 	add	a,r5
      00539D FA               [12] 3023 	mov	r2,a
      00539E E4               [12] 3024 	clr	a
      00539F 3E               [12] 3025 	addc	a,r6
      0053A0 F9               [12] 3026 	mov	r1,a
      0053A1 7B 00            [12] 3027 	mov	r3,#0x00
      0053A3 90 03 9A         [24] 3028 	mov	dptr,#_UART_Proc_SendMessage_payload_1_258
      0053A6 E0               [24] 3029 	movx	a,@dptr
      0053A7 F5 26            [12] 3030 	mov	_UART_Proc_SendMessage_sloc0_1_0,a
      0053A9 A3               [24] 3031 	inc	dptr
      0053AA E0               [24] 3032 	movx	a,@dptr
      0053AB F5 27            [12] 3033 	mov	(_UART_Proc_SendMessage_sloc0_1_0 + 1),a
      0053AD A3               [24] 3034 	inc	dptr
      0053AE E0               [24] 3035 	movx	a,@dptr
      0053AF F5 28            [12] 3036 	mov	(_UART_Proc_SendMessage_sloc0_1_0 + 2),a
      0053B1 90 03 E2         [24] 3037 	mov	dptr,#_memcpy_PARM_2
      0053B4 E5 26            [12] 3038 	mov	a,_UART_Proc_SendMessage_sloc0_1_0
      0053B6 F0               [24] 3039 	movx	@dptr,a
      0053B7 E5 27            [12] 3040 	mov	a,(_UART_Proc_SendMessage_sloc0_1_0 + 1)
      0053B9 A3               [24] 3041 	inc	dptr
      0053BA F0               [24] 3042 	movx	@dptr,a
      0053BB E5 28            [12] 3043 	mov	a,(_UART_Proc_SendMessage_sloc0_1_0 + 2)
      0053BD A3               [24] 3044 	inc	dptr
      0053BE F0               [24] 3045 	movx	@dptr,a
      0053BF 90 03 E5         [24] 3046 	mov	dptr,#_memcpy_PARM_3
      0053C2 EF               [12] 3047 	mov	a,r7
      0053C3 F0               [24] 3048 	movx	@dptr,a
      0053C4 E4               [12] 3049 	clr	a
      0053C5 A3               [24] 3050 	inc	dptr
      0053C6 F0               [24] 3051 	movx	@dptr,a
      0053C7 8A 82            [24] 3052 	mov	dpl,r2
      0053C9 89 83            [24] 3053 	mov	dph,r1
      0053CB 8B F0            [24] 3054 	mov	b,r3
      0053CD C0 07            [24] 3055 	push	ar7
      0053CF C0 06            [24] 3056 	push	ar6
      0053D1 C0 05            [24] 3057 	push	ar5
      0053D3 C0 04            [24] 3058 	push	ar4
      0053D5 12 65 97         [24] 3059 	lcall	_memcpy
      0053D8 D0 04            [24] 3060 	pop	ar4
      0053DA D0 05            [24] 3061 	pop	ar5
      0053DC D0 06            [24] 3062 	pop	ar6
      0053DE D0 07            [24] 3063 	pop	ar7
                                   3064 ;	..\src\peripherals\UartComProc.c:190: CRC32 = UART_Calc_CRC32(CrcBuffer,length+1,0);
      0053E0 7B 00            [12] 3065 	mov	r3,#0x00
      0053E2 90 03 9D         [24] 3066 	mov	dptr,#_UART_Calc_CRC32_PARM_2
      0053E5 EF               [12] 3067 	mov	a,r7
      0053E6 04               [12] 3068 	inc	a
      0053E7 F0               [24] 3069 	movx	@dptr,a
      0053E8 90 03 9E         [24] 3070 	mov	dptr,#_UART_Calc_CRC32_PARM_3
      0053EB E4               [12] 3071 	clr	a
      0053EC F0               [24] 3072 	movx	@dptr,a
      0053ED 8D 82            [24] 3073 	mov	dpl,r5
      0053EF 8E 83            [24] 3074 	mov	dph,r6
      0053F1 8B F0            [24] 3075 	mov	b,r3
      0053F3 C0 07            [24] 3076 	push	ar7
      0053F5 C0 04            [24] 3077 	push	ar4
      0053F7 12 54 6D         [24] 3078 	lcall	_UART_Calc_CRC32
      0053FA AA 82            [24] 3079 	mov	r2,dpl
      0053FC AB 83            [24] 3080 	mov	r3,dph
      0053FE AD F0            [24] 3081 	mov	r5,b
      005400 FE               [12] 3082 	mov	r6,a
      005401 D0 04            [24] 3083 	pop	ar4
      005403 D0 07            [24] 3084 	pop	ar7
                                   3085 ;	..\src\peripherals\UartComProc.c:191: uart0_tx(FRAME_START);
      005405 75 82 C0         [24] 3086 	mov	dpl,#0xc0
      005408 12 6A D9         [24] 3087 	lcall	_uart0_tx
                                   3088 ;	..\src\peripherals\UartComProc.c:192: uart0_tx(command);
      00540B 8C 82            [24] 3089 	mov	dpl,r4
      00540D 12 6A D9         [24] 3090 	lcall	_uart0_tx
                                   3091 ;	..\src\peripherals\UartComProc.c:193: for(i=0;i<length;i++)
      005410 7C 00            [12] 3092 	mov	r4,#0x00
      005412                       3093 00103$:
      005412 C3               [12] 3094 	clr	c
      005413 EC               [12] 3095 	mov	a,r4
      005414 9F               [12] 3096 	subb	a,r7
      005415 50 2C            [24] 3097 	jnc	00101$
                                   3098 ;	..\src\peripherals\UartComProc.c:195: uart0_tx(*(payload+i));
      005417 C0 02            [24] 3099 	push	ar2
      005419 C0 03            [24] 3100 	push	ar3
      00541B C0 05            [24] 3101 	push	ar5
      00541D C0 06            [24] 3102 	push	ar6
      00541F EC               [12] 3103 	mov	a,r4
      005420 25 26            [12] 3104 	add	a,_UART_Proc_SendMessage_sloc0_1_0
      005422 F8               [12] 3105 	mov	r0,a
      005423 E4               [12] 3106 	clr	a
      005424 35 27            [12] 3107 	addc	a,(_UART_Proc_SendMessage_sloc0_1_0 + 1)
      005426 F9               [12] 3108 	mov	r1,a
      005427 AE 28            [24] 3109 	mov	r6,(_UART_Proc_SendMessage_sloc0_1_0 + 2)
      005429 88 82            [24] 3110 	mov	dpl,r0
      00542B 89 83            [24] 3111 	mov	dph,r1
      00542D 8E F0            [24] 3112 	mov	b,r6
      00542F 12 79 65         [24] 3113 	lcall	__gptrget
      005432 F8               [12] 3114 	mov	r0,a
      005433 F5 82            [12] 3115 	mov	dpl,a
      005435 12 6A D9         [24] 3116 	lcall	_uart0_tx
                                   3117 ;	..\src\peripherals\UartComProc.c:193: for(i=0;i<length;i++)
      005438 0C               [12] 3118 	inc	r4
      005439 D0 06            [24] 3119 	pop	ar6
      00543B D0 05            [24] 3120 	pop	ar5
      00543D D0 03            [24] 3121 	pop	ar3
      00543F D0 02            [24] 3122 	pop	ar2
      005441 80 CF            [24] 3123 	sjmp	00103$
      005443                       3124 00101$:
                                   3125 ;	..\src\peripherals\UartComProc.c:197: uart0_tx((uint8_t)((CRC32 & 0xFF000000) >>24));
      005443 8E 07            [24] 3126 	mov	ar7,r6
      005445 8F 00            [24] 3127 	mov	ar0,r7
      005447 88 82            [24] 3128 	mov	dpl,r0
      005449 12 6A D9         [24] 3129 	lcall	_uart0_tx
                                   3130 ;	..\src\peripherals\UartComProc.c:198: uart0_tx((uint8_t)((CRC32 & 0x00FF0000) >>16));
      00544C 8D 04            [24] 3131 	mov	ar4,r5
      00544E 8C 00            [24] 3132 	mov	ar0,r4
      005450 88 82            [24] 3133 	mov	dpl,r0
      005452 12 6A D9         [24] 3134 	lcall	_uart0_tx
                                   3135 ;	..\src\peripherals\UartComProc.c:199: uart0_tx((uint8_t)((CRC32 & 0x0000FF00) >>8));
      005455 8B 01            [24] 3136 	mov	ar1,r3
      005457 89 00            [24] 3137 	mov	ar0,r1
      005459 88 82            [24] 3138 	mov	dpl,r0
      00545B 12 6A D9         [24] 3139 	lcall	_uart0_tx
                                   3140 ;	..\src\peripherals\UartComProc.c:200: uart0_tx((uint8_t)(CRC32 & 0x000000FF));
      00545E 8A 82            [24] 3141 	mov	dpl,r2
      005460 12 6A D9         [24] 3142 	lcall	_uart0_tx
                                   3143 ;	..\src\peripherals\UartComProc.c:202: uart0_tx(FRAME_END);
      005463 75 82 C0         [24] 3144 	mov	dpl,#0xc0
      005466 12 6A D9         [24] 3145 	lcall	_uart0_tx
                                   3146 ;	..\src\peripherals\UartComProc.c:210: return RetVal;
      005469 75 82 00         [24] 3147 	mov	dpl,#0x00
      00546C 22               [24] 3148 	ret
                                   3149 ;------------------------------------------------------------
                                   3150 ;Allocation info for local variables in function 'UART_Calc_CRC32'
                                   3151 ;------------------------------------------------------------
                                   3152 ;length                    Allocated with name '_UART_Calc_CRC32_PARM_2'
                                   3153 ;littleEndian              Allocated with name '_UART_Calc_CRC32_PARM_3'
                                   3154 ;message                   Allocated with name '_UART_Calc_CRC32_message_1_261'
                                   3155 ;i                         Allocated with name '_UART_Calc_CRC32_i_1_262'
                                   3156 ;j                         Allocated with name '_UART_Calc_CRC32_j_1_262'
                                   3157 ;byte                      Allocated with name '_UART_Calc_CRC32_byte_1_262'
                                   3158 ;crc                       Allocated with name '_UART_Calc_CRC32_crc_1_262'
                                   3159 ;mask                      Allocated with name '_UART_Calc_CRC32_mask_1_262'
                                   3160 ;sloc0                     Allocated with name '_UART_Calc_CRC32_sloc0_1_0'
                                   3161 ;sloc1                     Allocated with name '_UART_Calc_CRC32_sloc1_1_0'
                                   3162 ;sloc2                     Allocated with name '_UART_Calc_CRC32_sloc2_1_0'
                                   3163 ;------------------------------------------------------------
                                   3164 ;	..\src\peripherals\UartComProc.c:234: uint32_t UART_Calc_CRC32(uint8_t *message,uint8_t length,uint8_t littleEndian) {
                                   3165 ;	-----------------------------------------
                                   3166 ;	 function UART_Calc_CRC32
                                   3167 ;	-----------------------------------------
      00546D                       3168 _UART_Calc_CRC32:
      00546D AF F0            [24] 3169 	mov	r7,b
      00546F AE 83            [24] 3170 	mov	r6,dph
      005471 E5 82            [12] 3171 	mov	a,dpl
      005473 90 03 9F         [24] 3172 	mov	dptr,#_UART_Calc_CRC32_message_1_261
      005476 F0               [24] 3173 	movx	@dptr,a
      005477 EE               [12] 3174 	mov	a,r6
      005478 A3               [24] 3175 	inc	dptr
      005479 F0               [24] 3176 	movx	@dptr,a
      00547A EF               [12] 3177 	mov	a,r7
      00547B A3               [24] 3178 	inc	dptr
      00547C F0               [24] 3179 	movx	@dptr,a
                                   3180 ;	..\src\peripherals\UartComProc.c:239: crc = 0xFFFFFFFF;
      00547D 90 03 A2         [24] 3181 	mov	dptr,#_UART_Calc_CRC32_crc_1_262
      005480 74 FF            [12] 3182 	mov	a,#0xff
      005482 F0               [24] 3183 	movx	@dptr,a
      005483 A3               [24] 3184 	inc	dptr
      005484 F0               [24] 3185 	movx	@dptr,a
      005485 A3               [24] 3186 	inc	dptr
      005486 F0               [24] 3187 	movx	@dptr,a
      005487 A3               [24] 3188 	inc	dptr
      005488 F0               [24] 3189 	movx	@dptr,a
                                   3190 ;	..\src\peripherals\UartComProc.c:240: while (i < length) {
      005489 90 03 9F         [24] 3191 	mov	dptr,#_UART_Calc_CRC32_message_1_261
      00548C E0               [24] 3192 	movx	a,@dptr
      00548D F5 43            [12] 3193 	mov	_UART_Calc_CRC32_sloc0_1_0,a
      00548F A3               [24] 3194 	inc	dptr
      005490 E0               [24] 3195 	movx	a,@dptr
      005491 F5 44            [12] 3196 	mov	(_UART_Calc_CRC32_sloc0_1_0 + 1),a
      005493 A3               [24] 3197 	inc	dptr
      005494 E0               [24] 3198 	movx	a,@dptr
      005495 F5 45            [12] 3199 	mov	(_UART_Calc_CRC32_sloc0_1_0 + 2),a
      005497 90 03 9D         [24] 3200 	mov	dptr,#_UART_Calc_CRC32_PARM_2
      00549A E0               [24] 3201 	movx	a,@dptr
      00549B FC               [12] 3202 	mov	r4,a
      00549C E4               [12] 3203 	clr	a
      00549D F5 4A            [12] 3204 	mov	_UART_Calc_CRC32_sloc2_1_0,a
      00549F F5 4B            [12] 3205 	mov	(_UART_Calc_CRC32_sloc2_1_0 + 1),a
      0054A1                       3206 00102$:
      0054A1 8C 00            [24] 3207 	mov	ar0,r4
      0054A3 79 00            [12] 3208 	mov	r1,#0x00
      0054A5 C3               [12] 3209 	clr	c
      0054A6 E5 4A            [12] 3210 	mov	a,_UART_Calc_CRC32_sloc2_1_0
      0054A8 98               [12] 3211 	subb	a,r0
      0054A9 E5 4B            [12] 3212 	mov	a,(_UART_Calc_CRC32_sloc2_1_0 + 1)
      0054AB 64 80            [12] 3213 	xrl	a,#0x80
      0054AD 89 F0            [24] 3214 	mov	b,r1
      0054AF 63 F0 80         [24] 3215 	xrl	b,#0x80
      0054B2 95 F0            [12] 3216 	subb	a,b
      0054B4 40 03            [24] 3217 	jc	00128$
      0054B6 02 55 75         [24] 3218 	ljmp	00104$
      0054B9                       3219 00128$:
                                   3220 ;	..\src\peripherals\UartComProc.c:241: byte = message[i];
      0054B9 C0 04            [24] 3221 	push	ar4
      0054BB E5 4A            [12] 3222 	mov	a,_UART_Calc_CRC32_sloc2_1_0
      0054BD 25 43            [12] 3223 	add	a,_UART_Calc_CRC32_sloc0_1_0
      0054BF F8               [12] 3224 	mov	r0,a
      0054C0 E5 4B            [12] 3225 	mov	a,(_UART_Calc_CRC32_sloc2_1_0 + 1)
      0054C2 35 44            [12] 3226 	addc	a,(_UART_Calc_CRC32_sloc0_1_0 + 1)
      0054C4 F9               [12] 3227 	mov	r1,a
      0054C5 AC 45            [24] 3228 	mov	r4,(_UART_Calc_CRC32_sloc0_1_0 + 2)
      0054C7 88 82            [24] 3229 	mov	dpl,r0
      0054C9 89 83            [24] 3230 	mov	dph,r1
      0054CB 8C F0            [24] 3231 	mov	b,r4
      0054CD 12 79 65         [24] 3232 	lcall	__gptrget
      0054D0 F8               [12] 3233 	mov	r0,a
      0054D1 88 46            [24] 3234 	mov	_UART_Calc_CRC32_sloc1_1_0,r0
      0054D3 75 47 00         [24] 3235 	mov	(_UART_Calc_CRC32_sloc1_1_0 + 1),#0x00
      0054D6 75 48 00         [24] 3236 	mov	(_UART_Calc_CRC32_sloc1_1_0 + 2),#0x00
      0054D9 75 49 00         [24] 3237 	mov	(_UART_Calc_CRC32_sloc1_1_0 + 3),#0x00
                                   3238 ;	..\src\peripherals\UartComProc.c:243: crc = crc ^ byte;
      0054DC 90 03 A2         [24] 3239 	mov	dptr,#_UART_Calc_CRC32_crc_1_262
      0054DF E0               [24] 3240 	movx	a,@dptr
      0054E0 FC               [12] 3241 	mov	r4,a
      0054E1 A3               [24] 3242 	inc	dptr
      0054E2 E0               [24] 3243 	movx	a,@dptr
      0054E3 FD               [12] 3244 	mov	r5,a
      0054E4 A3               [24] 3245 	inc	dptr
      0054E5 E0               [24] 3246 	movx	a,@dptr
      0054E6 FE               [12] 3247 	mov	r6,a
      0054E7 A3               [24] 3248 	inc	dptr
      0054E8 E0               [24] 3249 	movx	a,@dptr
      0054E9 FF               [12] 3250 	mov	r7,a
      0054EA 90 03 A2         [24] 3251 	mov	dptr,#_UART_Calc_CRC32_crc_1_262
      0054ED E5 46            [12] 3252 	mov	a,_UART_Calc_CRC32_sloc1_1_0
      0054EF 6C               [12] 3253 	xrl	a,r4
      0054F0 F0               [24] 3254 	movx	@dptr,a
      0054F1 E5 47            [12] 3255 	mov	a,(_UART_Calc_CRC32_sloc1_1_0 + 1)
      0054F3 6D               [12] 3256 	xrl	a,r5
      0054F4 A3               [24] 3257 	inc	dptr
      0054F5 F0               [24] 3258 	movx	@dptr,a
      0054F6 E5 48            [12] 3259 	mov	a,(_UART_Calc_CRC32_sloc1_1_0 + 2)
      0054F8 6E               [12] 3260 	xrl	a,r6
      0054F9 A3               [24] 3261 	inc	dptr
      0054FA F0               [24] 3262 	movx	@dptr,a
      0054FB E5 49            [12] 3263 	mov	a,(_UART_Calc_CRC32_sloc1_1_0 + 3)
      0054FD 6F               [12] 3264 	xrl	a,r7
      0054FE A3               [24] 3265 	inc	dptr
      0054FF F0               [24] 3266 	movx	@dptr,a
                                   3267 ;	..\src\peripherals\UartComProc.c:244: for (j = 7; j >= 0; j--) {    // Do eight times.
      005500 75 46 07         [24] 3268 	mov	_UART_Calc_CRC32_sloc1_1_0,#0x07
      005503 75 47 00         [24] 3269 	mov	(_UART_Calc_CRC32_sloc1_1_0 + 1),#0x00
                                   3270 ;	..\src\peripherals\UartComProc.c:252: return crc;
      005506 D0 04            [24] 3271 	pop	ar4
                                   3272 ;	..\src\peripherals\UartComProc.c:244: for (j = 7; j >= 0; j--) {    // Do eight times.
      005508                       3273 00107$:
                                   3274 ;	..\src\peripherals\UartComProc.c:245: mask = -(crc & 1);
      005508 C0 04            [24] 3275 	push	ar4
      00550A 90 03 A2         [24] 3276 	mov	dptr,#_UART_Calc_CRC32_crc_1_262
      00550D E0               [24] 3277 	movx	a,@dptr
      00550E F9               [12] 3278 	mov	r1,a
      00550F A3               [24] 3279 	inc	dptr
      005510 E0               [24] 3280 	movx	a,@dptr
      005511 FD               [12] 3281 	mov	r5,a
      005512 A3               [24] 3282 	inc	dptr
      005513 E0               [24] 3283 	movx	a,@dptr
      005514 FE               [12] 3284 	mov	r6,a
      005515 A3               [24] 3285 	inc	dptr
      005516 E0               [24] 3286 	movx	a,@dptr
      005517 FF               [12] 3287 	mov	r7,a
      005518 74 01            [12] 3288 	mov	a,#0x01
      00551A 59               [12] 3289 	anl	a,r1
      00551B F8               [12] 3290 	mov	r0,a
      00551C 7A 00            [12] 3291 	mov	r2,#0x00
      00551E 7B 00            [12] 3292 	mov	r3,#0x00
      005520 7C 00            [12] 3293 	mov	r4,#0x00
      005522 C3               [12] 3294 	clr	c
      005523 E4               [12] 3295 	clr	a
      005524 98               [12] 3296 	subb	a,r0
      005525 F8               [12] 3297 	mov	r0,a
      005526 E4               [12] 3298 	clr	a
      005527 9A               [12] 3299 	subb	a,r2
      005528 FA               [12] 3300 	mov	r2,a
      005529 E4               [12] 3301 	clr	a
      00552A 9B               [12] 3302 	subb	a,r3
      00552B FB               [12] 3303 	mov	r3,a
      00552C E4               [12] 3304 	clr	a
      00552D 9C               [12] 3305 	subb	a,r4
      00552E FC               [12] 3306 	mov	r4,a
                                   3307 ;	..\src\peripherals\UartComProc.c:246: crc = (crc >> 1) ^ (0xEDB88320 & mask);
      00552F EF               [12] 3308 	mov	a,r7
      005530 C3               [12] 3309 	clr	c
      005531 13               [12] 3310 	rrc	a
      005532 FF               [12] 3311 	mov	r7,a
      005533 EE               [12] 3312 	mov	a,r6
      005534 13               [12] 3313 	rrc	a
      005535 FE               [12] 3314 	mov	r6,a
      005536 ED               [12] 3315 	mov	a,r5
      005537 13               [12] 3316 	rrc	a
      005538 FD               [12] 3317 	mov	r5,a
      005539 E9               [12] 3318 	mov	a,r1
      00553A 13               [12] 3319 	rrc	a
      00553B F9               [12] 3320 	mov	r1,a
      00553C 53 00 20         [24] 3321 	anl	ar0,#0x20
      00553F 53 02 83         [24] 3322 	anl	ar2,#0x83
      005542 53 03 B8         [24] 3323 	anl	ar3,#0xb8
      005545 53 04 ED         [24] 3324 	anl	ar4,#0xed
      005548 90 03 A2         [24] 3325 	mov	dptr,#_UART_Calc_CRC32_crc_1_262
      00554B E8               [12] 3326 	mov	a,r0
      00554C 69               [12] 3327 	xrl	a,r1
      00554D F0               [24] 3328 	movx	@dptr,a
      00554E EA               [12] 3329 	mov	a,r2
      00554F 6D               [12] 3330 	xrl	a,r5
      005550 A3               [24] 3331 	inc	dptr
      005551 F0               [24] 3332 	movx	@dptr,a
      005552 EB               [12] 3333 	mov	a,r3
      005553 6E               [12] 3334 	xrl	a,r6
      005554 A3               [24] 3335 	inc	dptr
      005555 F0               [24] 3336 	movx	@dptr,a
      005556 EC               [12] 3337 	mov	a,r4
      005557 6F               [12] 3338 	xrl	a,r7
      005558 A3               [24] 3339 	inc	dptr
      005559 F0               [24] 3340 	movx	@dptr,a
                                   3341 ;	..\src\peripherals\UartComProc.c:244: for (j = 7; j >= 0; j--) {    // Do eight times.
      00555A 15 46            [12] 3342 	dec	_UART_Calc_CRC32_sloc1_1_0
      00555C 74 FF            [12] 3343 	mov	a,#0xff
      00555E B5 46 02         [24] 3344 	cjne	a,_UART_Calc_CRC32_sloc1_1_0,00129$
      005561 15 47            [12] 3345 	dec	(_UART_Calc_CRC32_sloc1_1_0 + 1)
      005563                       3346 00129$:
      005563 E5 47            [12] 3347 	mov	a,(_UART_Calc_CRC32_sloc1_1_0 + 1)
      005565 D0 04            [24] 3348 	pop	ar4
      005567 30 E7 9E         [24] 3349 	jnb	acc.7,00107$
                                   3350 ;	..\src\peripherals\UartComProc.c:248: i = i + 1;
      00556A 05 4A            [12] 3351 	inc	_UART_Calc_CRC32_sloc2_1_0
      00556C E4               [12] 3352 	clr	a
      00556D B5 4A 02         [24] 3353 	cjne	a,_UART_Calc_CRC32_sloc2_1_0,00131$
      005570 05 4B            [12] 3354 	inc	(_UART_Calc_CRC32_sloc2_1_0 + 1)
      005572                       3355 00131$:
      005572 02 54 A1         [24] 3356 	ljmp	00102$
      005575                       3357 00104$:
                                   3358 ;	..\src\peripherals\UartComProc.c:250: crc = ~crc;
      005575 90 03 A2         [24] 3359 	mov	dptr,#_UART_Calc_CRC32_crc_1_262
      005578 E0               [24] 3360 	movx	a,@dptr
      005579 FC               [12] 3361 	mov	r4,a
      00557A A3               [24] 3362 	inc	dptr
      00557B E0               [24] 3363 	movx	a,@dptr
      00557C FD               [12] 3364 	mov	r5,a
      00557D A3               [24] 3365 	inc	dptr
      00557E E0               [24] 3366 	movx	a,@dptr
      00557F FE               [12] 3367 	mov	r6,a
      005580 A3               [24] 3368 	inc	dptr
      005581 E0               [24] 3369 	movx	a,@dptr
      005582 FF               [12] 3370 	mov	r7,a
      005583 90 03 A2         [24] 3371 	mov	dptr,#_UART_Calc_CRC32_crc_1_262
      005586 EC               [12] 3372 	mov	a,r4
      005587 F4               [12] 3373 	cpl	a
      005588 F0               [24] 3374 	movx	@dptr,a
      005589 ED               [12] 3375 	mov	a,r5
      00558A F4               [12] 3376 	cpl	a
      00558B A3               [24] 3377 	inc	dptr
      00558C F0               [24] 3378 	movx	@dptr,a
      00558D EE               [12] 3379 	mov	a,r6
      00558E F4               [12] 3380 	cpl	a
      00558F A3               [24] 3381 	inc	dptr
      005590 F0               [24] 3382 	movx	@dptr,a
      005591 EF               [12] 3383 	mov	a,r7
      005592 F4               [12] 3384 	cpl	a
      005593 A3               [24] 3385 	inc	dptr
      005594 F0               [24] 3386 	movx	@dptr,a
                                   3387 ;	..\src\peripherals\UartComProc.c:251: if(littleEndian)crc=(((crc & 0x000000FF) << 24)+((crc & 0x0000FF00) << 8)+ ((crc & 0x00FF0000) >> 8)+((crc & 0xFF000000) >> 24));//swap little endian big endian
      005595 90 03 9E         [24] 3388 	mov	dptr,#_UART_Calc_CRC32_PARM_3
      005598 E0               [24] 3389 	movx	a,@dptr
      005599 70 03            [24] 3390 	jnz	00132$
      00559B 02 56 10         [24] 3391 	ljmp	00106$
      00559E                       3392 00132$:
      00559E 90 03 A2         [24] 3393 	mov	dptr,#_UART_Calc_CRC32_crc_1_262
      0055A1 E0               [24] 3394 	movx	a,@dptr
      0055A2 FC               [12] 3395 	mov	r4,a
      0055A3 A3               [24] 3396 	inc	dptr
      0055A4 E0               [24] 3397 	movx	a,@dptr
      0055A5 FD               [12] 3398 	mov	r5,a
      0055A6 A3               [24] 3399 	inc	dptr
      0055A7 E0               [24] 3400 	movx	a,@dptr
      0055A8 FE               [12] 3401 	mov	r6,a
      0055A9 A3               [24] 3402 	inc	dptr
      0055AA E0               [24] 3403 	movx	a,@dptr
      0055AB FF               [12] 3404 	mov	r7,a
      0055AC 8C 00            [24] 3405 	mov	ar0,r4
      0055AE 7B 00            [12] 3406 	mov	r3,#0x00
      0055B0 88 49            [24] 3407 	mov	(_UART_Calc_CRC32_sloc1_1_0 + 3),r0
                                   3408 ;	1-genFromRTrack replaced	mov	_UART_Calc_CRC32_sloc1_1_0,#0x00
      0055B2 8B 46            [24] 3409 	mov	_UART_Calc_CRC32_sloc1_1_0,r3
                                   3410 ;	1-genFromRTrack replaced	mov	(_UART_Calc_CRC32_sloc1_1_0 + 1),#0x00
      0055B4 8B 47            [24] 3411 	mov	(_UART_Calc_CRC32_sloc1_1_0 + 1),r3
                                   3412 ;	1-genFromRTrack replaced	mov	(_UART_Calc_CRC32_sloc1_1_0 + 2),#0x00
      0055B6 8B 48            [24] 3413 	mov	(_UART_Calc_CRC32_sloc1_1_0 + 2),r3
      0055B8 78 00            [12] 3414 	mov	r0,#0x00
      0055BA 8D 01            [24] 3415 	mov	ar1,r5
      0055BC 7A 00            [12] 3416 	mov	r2,#0x00
      0055BE 8A 03            [24] 3417 	mov	ar3,r2
      0055C0 89 02            [24] 3418 	mov	ar2,r1
      0055C2 88 01            [24] 3419 	mov	ar1,r0
      0055C4 E4               [12] 3420 	clr	a
      0055C5 25 46            [12] 3421 	add	a,_UART_Calc_CRC32_sloc1_1_0
      0055C7 F5 46            [12] 3422 	mov	_UART_Calc_CRC32_sloc1_1_0,a
      0055C9 E9               [12] 3423 	mov	a,r1
      0055CA 35 47            [12] 3424 	addc	a,(_UART_Calc_CRC32_sloc1_1_0 + 1)
      0055CC F5 47            [12] 3425 	mov	(_UART_Calc_CRC32_sloc1_1_0 + 1),a
      0055CE EA               [12] 3426 	mov	a,r2
      0055CF 35 48            [12] 3427 	addc	a,(_UART_Calc_CRC32_sloc1_1_0 + 2)
      0055D1 F5 48            [12] 3428 	mov	(_UART_Calc_CRC32_sloc1_1_0 + 2),a
      0055D3 EB               [12] 3429 	mov	a,r3
      0055D4 35 49            [12] 3430 	addc	a,(_UART_Calc_CRC32_sloc1_1_0 + 3)
      0055D6 F5 49            [12] 3431 	mov	(_UART_Calc_CRC32_sloc1_1_0 + 3),a
      0055D8 79 00            [12] 3432 	mov	r1,#0x00
      0055DA 8E 02            [24] 3433 	mov	ar2,r6
      0055DC 7B 00            [12] 3434 	mov	r3,#0x00
      0055DE 89 00            [24] 3435 	mov	ar0,r1
      0055E0 8A 01            [24] 3436 	mov	ar1,r2
      0055E2 8B 02            [24] 3437 	mov	ar2,r3
      0055E4 7B 00            [12] 3438 	mov	r3,#0x00
      0055E6 E8               [12] 3439 	mov	a,r0
      0055E7 25 46            [12] 3440 	add	a,_UART_Calc_CRC32_sloc1_1_0
      0055E9 F8               [12] 3441 	mov	r0,a
      0055EA E9               [12] 3442 	mov	a,r1
      0055EB 35 47            [12] 3443 	addc	a,(_UART_Calc_CRC32_sloc1_1_0 + 1)
      0055ED F9               [12] 3444 	mov	r1,a
      0055EE EA               [12] 3445 	mov	a,r2
      0055EF 35 48            [12] 3446 	addc	a,(_UART_Calc_CRC32_sloc1_1_0 + 2)
      0055F1 FA               [12] 3447 	mov	r2,a
      0055F2 EB               [12] 3448 	mov	a,r3
      0055F3 35 49            [12] 3449 	addc	a,(_UART_Calc_CRC32_sloc1_1_0 + 3)
      0055F5 FB               [12] 3450 	mov	r3,a
      0055F6 8F 04            [24] 3451 	mov	ar4,r7
      0055F8 7D 00            [12] 3452 	mov	r5,#0x00
      0055FA 7E 00            [12] 3453 	mov	r6,#0x00
      0055FC 7F 00            [12] 3454 	mov	r7,#0x00
      0055FE 90 03 A2         [24] 3455 	mov	dptr,#_UART_Calc_CRC32_crc_1_262
      005601 EC               [12] 3456 	mov	a,r4
      005602 28               [12] 3457 	add	a,r0
      005603 F0               [24] 3458 	movx	@dptr,a
      005604 ED               [12] 3459 	mov	a,r5
      005605 39               [12] 3460 	addc	a,r1
      005606 A3               [24] 3461 	inc	dptr
      005607 F0               [24] 3462 	movx	@dptr,a
      005608 EE               [12] 3463 	mov	a,r6
      005609 3A               [12] 3464 	addc	a,r2
      00560A A3               [24] 3465 	inc	dptr
      00560B F0               [24] 3466 	movx	@dptr,a
      00560C EF               [12] 3467 	mov	a,r7
      00560D 3B               [12] 3468 	addc	a,r3
      00560E A3               [24] 3469 	inc	dptr
      00560F F0               [24] 3470 	movx	@dptr,a
      005610                       3471 00106$:
                                   3472 ;	..\src\peripherals\UartComProc.c:252: return crc;
      005610 90 03 A2         [24] 3473 	mov	dptr,#_UART_Calc_CRC32_crc_1_262
      005613 E0               [24] 3474 	movx	a,@dptr
      005614 FC               [12] 3475 	mov	r4,a
      005615 A3               [24] 3476 	inc	dptr
      005616 E0               [24] 3477 	movx	a,@dptr
      005617 FD               [12] 3478 	mov	r5,a
      005618 A3               [24] 3479 	inc	dptr
      005619 E0               [24] 3480 	movx	a,@dptr
      00561A FE               [12] 3481 	mov	r6,a
      00561B A3               [24] 3482 	inc	dptr
      00561C E0               [24] 3483 	movx	a,@dptr
      00561D 8C 82            [24] 3484 	mov	dpl,r4
      00561F 8D 83            [24] 3485 	mov	dph,r5
      005621 8E F0            [24] 3486 	mov	b,r6
      005623 22               [24] 3487 	ret
                                   3488 	.area CSEG    (CODE)
                                   3489 	.area CONST   (CODE)
                                   3490 	.area XINIT   (CODE)
                                   3491 	.area CABS    (ABS,CODE)
