                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.6.0 #9615 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module UBLOX
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _wtimer_runcallbacks
                                     12 	.globl _memcpy
                                     13 	.globl _free
                                     14 	.globl _malloc
                                     15 	.globl _uart_timer1_baud
                                     16 	.globl _uart1_tx
                                     17 	.globl _uart1_rx
                                     18 	.globl _uart1_init
                                     19 	.globl _uart1_rxcount
                                     20 	.globl _delay
                                     21 	.globl _PORTC_7
                                     22 	.globl _PORTC_6
                                     23 	.globl _PORTC_5
                                     24 	.globl _PORTC_4
                                     25 	.globl _PORTC_3
                                     26 	.globl _PORTC_2
                                     27 	.globl _PORTC_1
                                     28 	.globl _PORTC_0
                                     29 	.globl _PORTB_7
                                     30 	.globl _PORTB_6
                                     31 	.globl _PORTB_5
                                     32 	.globl _PORTB_4
                                     33 	.globl _PORTB_3
                                     34 	.globl _PORTB_2
                                     35 	.globl _PORTB_1
                                     36 	.globl _PORTB_0
                                     37 	.globl _PORTA_7
                                     38 	.globl _PORTA_6
                                     39 	.globl _PORTA_5
                                     40 	.globl _PORTA_4
                                     41 	.globl _PORTA_3
                                     42 	.globl _PORTA_2
                                     43 	.globl _PORTA_1
                                     44 	.globl _PORTA_0
                                     45 	.globl _PINC_7
                                     46 	.globl _PINC_6
                                     47 	.globl _PINC_5
                                     48 	.globl _PINC_4
                                     49 	.globl _PINC_3
                                     50 	.globl _PINC_2
                                     51 	.globl _PINC_1
                                     52 	.globl _PINC_0
                                     53 	.globl _PINB_7
                                     54 	.globl _PINB_6
                                     55 	.globl _PINB_5
                                     56 	.globl _PINB_4
                                     57 	.globl _PINB_3
                                     58 	.globl _PINB_2
                                     59 	.globl _PINB_1
                                     60 	.globl _PINB_0
                                     61 	.globl _PINA_7
                                     62 	.globl _PINA_6
                                     63 	.globl _PINA_5
                                     64 	.globl _PINA_4
                                     65 	.globl _PINA_3
                                     66 	.globl _PINA_2
                                     67 	.globl _PINA_1
                                     68 	.globl _PINA_0
                                     69 	.globl _CY
                                     70 	.globl _AC
                                     71 	.globl _F0
                                     72 	.globl _RS1
                                     73 	.globl _RS0
                                     74 	.globl _OV
                                     75 	.globl _F1
                                     76 	.globl _P
                                     77 	.globl _IP_7
                                     78 	.globl _IP_6
                                     79 	.globl _IP_5
                                     80 	.globl _IP_4
                                     81 	.globl _IP_3
                                     82 	.globl _IP_2
                                     83 	.globl _IP_1
                                     84 	.globl _IP_0
                                     85 	.globl _EA
                                     86 	.globl _IE_7
                                     87 	.globl _IE_6
                                     88 	.globl _IE_5
                                     89 	.globl _IE_4
                                     90 	.globl _IE_3
                                     91 	.globl _IE_2
                                     92 	.globl _IE_1
                                     93 	.globl _IE_0
                                     94 	.globl _EIP_7
                                     95 	.globl _EIP_6
                                     96 	.globl _EIP_5
                                     97 	.globl _EIP_4
                                     98 	.globl _EIP_3
                                     99 	.globl _EIP_2
                                    100 	.globl _EIP_1
                                    101 	.globl _EIP_0
                                    102 	.globl _EIE_7
                                    103 	.globl _EIE_6
                                    104 	.globl _EIE_5
                                    105 	.globl _EIE_4
                                    106 	.globl _EIE_3
                                    107 	.globl _EIE_2
                                    108 	.globl _EIE_1
                                    109 	.globl _EIE_0
                                    110 	.globl _E2IP_7
                                    111 	.globl _E2IP_6
                                    112 	.globl _E2IP_5
                                    113 	.globl _E2IP_4
                                    114 	.globl _E2IP_3
                                    115 	.globl _E2IP_2
                                    116 	.globl _E2IP_1
                                    117 	.globl _E2IP_0
                                    118 	.globl _E2IE_7
                                    119 	.globl _E2IE_6
                                    120 	.globl _E2IE_5
                                    121 	.globl _E2IE_4
                                    122 	.globl _E2IE_3
                                    123 	.globl _E2IE_2
                                    124 	.globl _E2IE_1
                                    125 	.globl _E2IE_0
                                    126 	.globl _B_7
                                    127 	.globl _B_6
                                    128 	.globl _B_5
                                    129 	.globl _B_4
                                    130 	.globl _B_3
                                    131 	.globl _B_2
                                    132 	.globl _B_1
                                    133 	.globl _B_0
                                    134 	.globl _ACC_7
                                    135 	.globl _ACC_6
                                    136 	.globl _ACC_5
                                    137 	.globl _ACC_4
                                    138 	.globl _ACC_3
                                    139 	.globl _ACC_2
                                    140 	.globl _ACC_1
                                    141 	.globl _ACC_0
                                    142 	.globl _WTSTAT
                                    143 	.globl _WTIRQEN
                                    144 	.globl _WTEVTD
                                    145 	.globl _WTEVTD1
                                    146 	.globl _WTEVTD0
                                    147 	.globl _WTEVTC
                                    148 	.globl _WTEVTC1
                                    149 	.globl _WTEVTC0
                                    150 	.globl _WTEVTB
                                    151 	.globl _WTEVTB1
                                    152 	.globl _WTEVTB0
                                    153 	.globl _WTEVTA
                                    154 	.globl _WTEVTA1
                                    155 	.globl _WTEVTA0
                                    156 	.globl _WTCNTR1
                                    157 	.globl _WTCNTB
                                    158 	.globl _WTCNTB1
                                    159 	.globl _WTCNTB0
                                    160 	.globl _WTCNTA
                                    161 	.globl _WTCNTA1
                                    162 	.globl _WTCNTA0
                                    163 	.globl _WTCFGB
                                    164 	.globl _WTCFGA
                                    165 	.globl _WDTRESET
                                    166 	.globl _WDTCFG
                                    167 	.globl _U1STATUS
                                    168 	.globl _U1SHREG
                                    169 	.globl _U1MODE
                                    170 	.globl _U1CTRL
                                    171 	.globl _U0STATUS
                                    172 	.globl _U0SHREG
                                    173 	.globl _U0MODE
                                    174 	.globl _U0CTRL
                                    175 	.globl _T2STATUS
                                    176 	.globl _T2PERIOD
                                    177 	.globl _T2PERIOD1
                                    178 	.globl _T2PERIOD0
                                    179 	.globl _T2MODE
                                    180 	.globl _T2CNT
                                    181 	.globl _T2CNT1
                                    182 	.globl _T2CNT0
                                    183 	.globl _T2CLKSRC
                                    184 	.globl _T1STATUS
                                    185 	.globl _T1PERIOD
                                    186 	.globl _T1PERIOD1
                                    187 	.globl _T1PERIOD0
                                    188 	.globl _T1MODE
                                    189 	.globl _T1CNT
                                    190 	.globl _T1CNT1
                                    191 	.globl _T1CNT0
                                    192 	.globl _T1CLKSRC
                                    193 	.globl _T0STATUS
                                    194 	.globl _T0PERIOD
                                    195 	.globl _T0PERIOD1
                                    196 	.globl _T0PERIOD0
                                    197 	.globl _T0MODE
                                    198 	.globl _T0CNT
                                    199 	.globl _T0CNT1
                                    200 	.globl _T0CNT0
                                    201 	.globl _T0CLKSRC
                                    202 	.globl _SPSTATUS
                                    203 	.globl _SPSHREG
                                    204 	.globl _SPMODE
                                    205 	.globl _SPCLKSRC
                                    206 	.globl _RADIOSTAT
                                    207 	.globl _RADIOSTAT1
                                    208 	.globl _RADIOSTAT0
                                    209 	.globl _RADIODATA
                                    210 	.globl _RADIODATA3
                                    211 	.globl _RADIODATA2
                                    212 	.globl _RADIODATA1
                                    213 	.globl _RADIODATA0
                                    214 	.globl _RADIOADDR
                                    215 	.globl _RADIOADDR1
                                    216 	.globl _RADIOADDR0
                                    217 	.globl _RADIOACC
                                    218 	.globl _OC1STATUS
                                    219 	.globl _OC1PIN
                                    220 	.globl _OC1MODE
                                    221 	.globl _OC1COMP
                                    222 	.globl _OC1COMP1
                                    223 	.globl _OC1COMP0
                                    224 	.globl _OC0STATUS
                                    225 	.globl _OC0PIN
                                    226 	.globl _OC0MODE
                                    227 	.globl _OC0COMP
                                    228 	.globl _OC0COMP1
                                    229 	.globl _OC0COMP0
                                    230 	.globl _NVSTATUS
                                    231 	.globl _NVKEY
                                    232 	.globl _NVDATA
                                    233 	.globl _NVDATA1
                                    234 	.globl _NVDATA0
                                    235 	.globl _NVADDR
                                    236 	.globl _NVADDR1
                                    237 	.globl _NVADDR0
                                    238 	.globl _IC1STATUS
                                    239 	.globl _IC1MODE
                                    240 	.globl _IC1CAPT
                                    241 	.globl _IC1CAPT1
                                    242 	.globl _IC1CAPT0
                                    243 	.globl _IC0STATUS
                                    244 	.globl _IC0MODE
                                    245 	.globl _IC0CAPT
                                    246 	.globl _IC0CAPT1
                                    247 	.globl _IC0CAPT0
                                    248 	.globl _PORTR
                                    249 	.globl _PORTC
                                    250 	.globl _PORTB
                                    251 	.globl _PORTA
                                    252 	.globl _PINR
                                    253 	.globl _PINC
                                    254 	.globl _PINB
                                    255 	.globl _PINA
                                    256 	.globl _DIRR
                                    257 	.globl _DIRC
                                    258 	.globl _DIRB
                                    259 	.globl _DIRA
                                    260 	.globl _DBGLNKSTAT
                                    261 	.globl _DBGLNKBUF
                                    262 	.globl _CODECONFIG
                                    263 	.globl _CLKSTAT
                                    264 	.globl _CLKCON
                                    265 	.globl _ANALOGCOMP
                                    266 	.globl _ADCCONV
                                    267 	.globl _ADCCLKSRC
                                    268 	.globl _ADCCH3CONFIG
                                    269 	.globl _ADCCH2CONFIG
                                    270 	.globl _ADCCH1CONFIG
                                    271 	.globl _ADCCH0CONFIG
                                    272 	.globl __XPAGE
                                    273 	.globl _XPAGE
                                    274 	.globl _SP
                                    275 	.globl _PSW
                                    276 	.globl _PCON
                                    277 	.globl _IP
                                    278 	.globl _IE
                                    279 	.globl _EIP
                                    280 	.globl _EIE
                                    281 	.globl _E2IP
                                    282 	.globl _E2IE
                                    283 	.globl _DPS
                                    284 	.globl _DPTR1
                                    285 	.globl _DPTR0
                                    286 	.globl _DPL1
                                    287 	.globl _DPL
                                    288 	.globl _DPH1
                                    289 	.globl _DPH
                                    290 	.globl _B
                                    291 	.globl _ACC
                                    292 	.globl _UBLOX_GPS_SendCommand_WaitACK_PARM_6
                                    293 	.globl _UBLOX_GPS_SendCommand_WaitACK_PARM_5
                                    294 	.globl _UBLOX_GPS_SendCommand_WaitACK_PARM_4
                                    295 	.globl _UBLOX_GPS_SendCommand_WaitACK_PARM_3
                                    296 	.globl _UBLOX_GPS_SendCommand_WaitACK_PARM_2
                                    297 	.globl _UBLOX_GPS_FletcherChecksum8_PARM_4
                                    298 	.globl _UBLOX_GPS_FletcherChecksum8_PARM_3
                                    299 	.globl _UBLOX_GPS_FletcherChecksum8_PARM_2
                                    300 	.globl _AX5043_TIMEGAIN3NB
                                    301 	.globl _AX5043_TIMEGAIN2NB
                                    302 	.globl _AX5043_TIMEGAIN1NB
                                    303 	.globl _AX5043_TIMEGAIN0NB
                                    304 	.globl _AX5043_RXPARAMSETSNB
                                    305 	.globl _AX5043_RXPARAMCURSETNB
                                    306 	.globl _AX5043_PKTMAXLENNB
                                    307 	.globl _AX5043_PKTLENOFFSETNB
                                    308 	.globl _AX5043_PKTLENCFGNB
                                    309 	.globl _AX5043_PKTADDRMASK3NB
                                    310 	.globl _AX5043_PKTADDRMASK2NB
                                    311 	.globl _AX5043_PKTADDRMASK1NB
                                    312 	.globl _AX5043_PKTADDRMASK0NB
                                    313 	.globl _AX5043_PKTADDRCFGNB
                                    314 	.globl _AX5043_PKTADDR3NB
                                    315 	.globl _AX5043_PKTADDR2NB
                                    316 	.globl _AX5043_PKTADDR1NB
                                    317 	.globl _AX5043_PKTADDR0NB
                                    318 	.globl _AX5043_PHASEGAIN3NB
                                    319 	.globl _AX5043_PHASEGAIN2NB
                                    320 	.globl _AX5043_PHASEGAIN1NB
                                    321 	.globl _AX5043_PHASEGAIN0NB
                                    322 	.globl _AX5043_FREQUENCYLEAKNB
                                    323 	.globl _AX5043_FREQUENCYGAIND3NB
                                    324 	.globl _AX5043_FREQUENCYGAIND2NB
                                    325 	.globl _AX5043_FREQUENCYGAIND1NB
                                    326 	.globl _AX5043_FREQUENCYGAIND0NB
                                    327 	.globl _AX5043_FREQUENCYGAINC3NB
                                    328 	.globl _AX5043_FREQUENCYGAINC2NB
                                    329 	.globl _AX5043_FREQUENCYGAINC1NB
                                    330 	.globl _AX5043_FREQUENCYGAINC0NB
                                    331 	.globl _AX5043_FREQUENCYGAINB3NB
                                    332 	.globl _AX5043_FREQUENCYGAINB2NB
                                    333 	.globl _AX5043_FREQUENCYGAINB1NB
                                    334 	.globl _AX5043_FREQUENCYGAINB0NB
                                    335 	.globl _AX5043_FREQUENCYGAINA3NB
                                    336 	.globl _AX5043_FREQUENCYGAINA2NB
                                    337 	.globl _AX5043_FREQUENCYGAINA1NB
                                    338 	.globl _AX5043_FREQUENCYGAINA0NB
                                    339 	.globl _AX5043_FREQDEV13NB
                                    340 	.globl _AX5043_FREQDEV12NB
                                    341 	.globl _AX5043_FREQDEV11NB
                                    342 	.globl _AX5043_FREQDEV10NB
                                    343 	.globl _AX5043_FREQDEV03NB
                                    344 	.globl _AX5043_FREQDEV02NB
                                    345 	.globl _AX5043_FREQDEV01NB
                                    346 	.globl _AX5043_FREQDEV00NB
                                    347 	.globl _AX5043_FOURFSK3NB
                                    348 	.globl _AX5043_FOURFSK2NB
                                    349 	.globl _AX5043_FOURFSK1NB
                                    350 	.globl _AX5043_FOURFSK0NB
                                    351 	.globl _AX5043_DRGAIN3NB
                                    352 	.globl _AX5043_DRGAIN2NB
                                    353 	.globl _AX5043_DRGAIN1NB
                                    354 	.globl _AX5043_DRGAIN0NB
                                    355 	.globl _AX5043_BBOFFSRES3NB
                                    356 	.globl _AX5043_BBOFFSRES2NB
                                    357 	.globl _AX5043_BBOFFSRES1NB
                                    358 	.globl _AX5043_BBOFFSRES0NB
                                    359 	.globl _AX5043_AMPLITUDEGAIN3NB
                                    360 	.globl _AX5043_AMPLITUDEGAIN2NB
                                    361 	.globl _AX5043_AMPLITUDEGAIN1NB
                                    362 	.globl _AX5043_AMPLITUDEGAIN0NB
                                    363 	.globl _AX5043_AGCTARGET3NB
                                    364 	.globl _AX5043_AGCTARGET2NB
                                    365 	.globl _AX5043_AGCTARGET1NB
                                    366 	.globl _AX5043_AGCTARGET0NB
                                    367 	.globl _AX5043_AGCMINMAX3NB
                                    368 	.globl _AX5043_AGCMINMAX2NB
                                    369 	.globl _AX5043_AGCMINMAX1NB
                                    370 	.globl _AX5043_AGCMINMAX0NB
                                    371 	.globl _AX5043_AGCGAIN3NB
                                    372 	.globl _AX5043_AGCGAIN2NB
                                    373 	.globl _AX5043_AGCGAIN1NB
                                    374 	.globl _AX5043_AGCGAIN0NB
                                    375 	.globl _AX5043_AGCAHYST3NB
                                    376 	.globl _AX5043_AGCAHYST2NB
                                    377 	.globl _AX5043_AGCAHYST1NB
                                    378 	.globl _AX5043_AGCAHYST0NB
                                    379 	.globl _AX5043_0xF44NB
                                    380 	.globl _AX5043_0xF35NB
                                    381 	.globl _AX5043_0xF34NB
                                    382 	.globl _AX5043_0xF33NB
                                    383 	.globl _AX5043_0xF32NB
                                    384 	.globl _AX5043_0xF31NB
                                    385 	.globl _AX5043_0xF30NB
                                    386 	.globl _AX5043_0xF26NB
                                    387 	.globl _AX5043_0xF23NB
                                    388 	.globl _AX5043_0xF22NB
                                    389 	.globl _AX5043_0xF21NB
                                    390 	.globl _AX5043_0xF1CNB
                                    391 	.globl _AX5043_0xF18NB
                                    392 	.globl _AX5043_0xF0CNB
                                    393 	.globl _AX5043_0xF00NB
                                    394 	.globl _AX5043_XTALSTATUSNB
                                    395 	.globl _AX5043_XTALOSCNB
                                    396 	.globl _AX5043_XTALCAPNB
                                    397 	.globl _AX5043_XTALAMPLNB
                                    398 	.globl _AX5043_WAKEUPXOEARLYNB
                                    399 	.globl _AX5043_WAKEUPTIMER1NB
                                    400 	.globl _AX5043_WAKEUPTIMER0NB
                                    401 	.globl _AX5043_WAKEUPFREQ1NB
                                    402 	.globl _AX5043_WAKEUPFREQ0NB
                                    403 	.globl _AX5043_WAKEUP1NB
                                    404 	.globl _AX5043_WAKEUP0NB
                                    405 	.globl _AX5043_TXRATE2NB
                                    406 	.globl _AX5043_TXRATE1NB
                                    407 	.globl _AX5043_TXRATE0NB
                                    408 	.globl _AX5043_TXPWRCOEFFE1NB
                                    409 	.globl _AX5043_TXPWRCOEFFE0NB
                                    410 	.globl _AX5043_TXPWRCOEFFD1NB
                                    411 	.globl _AX5043_TXPWRCOEFFD0NB
                                    412 	.globl _AX5043_TXPWRCOEFFC1NB
                                    413 	.globl _AX5043_TXPWRCOEFFC0NB
                                    414 	.globl _AX5043_TXPWRCOEFFB1NB
                                    415 	.globl _AX5043_TXPWRCOEFFB0NB
                                    416 	.globl _AX5043_TXPWRCOEFFA1NB
                                    417 	.globl _AX5043_TXPWRCOEFFA0NB
                                    418 	.globl _AX5043_TRKRFFREQ2NB
                                    419 	.globl _AX5043_TRKRFFREQ1NB
                                    420 	.globl _AX5043_TRKRFFREQ0NB
                                    421 	.globl _AX5043_TRKPHASE1NB
                                    422 	.globl _AX5043_TRKPHASE0NB
                                    423 	.globl _AX5043_TRKFSKDEMOD1NB
                                    424 	.globl _AX5043_TRKFSKDEMOD0NB
                                    425 	.globl _AX5043_TRKFREQ1NB
                                    426 	.globl _AX5043_TRKFREQ0NB
                                    427 	.globl _AX5043_TRKDATARATE2NB
                                    428 	.globl _AX5043_TRKDATARATE1NB
                                    429 	.globl _AX5043_TRKDATARATE0NB
                                    430 	.globl _AX5043_TRKAMPLITUDE1NB
                                    431 	.globl _AX5043_TRKAMPLITUDE0NB
                                    432 	.globl _AX5043_TRKAFSKDEMOD1NB
                                    433 	.globl _AX5043_TRKAFSKDEMOD0NB
                                    434 	.globl _AX5043_TMGTXSETTLENB
                                    435 	.globl _AX5043_TMGTXBOOSTNB
                                    436 	.globl _AX5043_TMGRXSETTLENB
                                    437 	.globl _AX5043_TMGRXRSSINB
                                    438 	.globl _AX5043_TMGRXPREAMBLE3NB
                                    439 	.globl _AX5043_TMGRXPREAMBLE2NB
                                    440 	.globl _AX5043_TMGRXPREAMBLE1NB
                                    441 	.globl _AX5043_TMGRXOFFSACQNB
                                    442 	.globl _AX5043_TMGRXCOARSEAGCNB
                                    443 	.globl _AX5043_TMGRXBOOSTNB
                                    444 	.globl _AX5043_TMGRXAGCNB
                                    445 	.globl _AX5043_TIMER2NB
                                    446 	.globl _AX5043_TIMER1NB
                                    447 	.globl _AX5043_TIMER0NB
                                    448 	.globl _AX5043_SILICONREVISIONNB
                                    449 	.globl _AX5043_SCRATCHNB
                                    450 	.globl _AX5043_RXDATARATE2NB
                                    451 	.globl _AX5043_RXDATARATE1NB
                                    452 	.globl _AX5043_RXDATARATE0NB
                                    453 	.globl _AX5043_RSSIREFERENCENB
                                    454 	.globl _AX5043_RSSIABSTHRNB
                                    455 	.globl _AX5043_RSSINB
                                    456 	.globl _AX5043_REFNB
                                    457 	.globl _AX5043_RADIOSTATENB
                                    458 	.globl _AX5043_RADIOEVENTREQ1NB
                                    459 	.globl _AX5043_RADIOEVENTREQ0NB
                                    460 	.globl _AX5043_RADIOEVENTMASK1NB
                                    461 	.globl _AX5043_RADIOEVENTMASK0NB
                                    462 	.globl _AX5043_PWRMODENB
                                    463 	.globl _AX5043_PWRAMPNB
                                    464 	.globl _AX5043_POWSTICKYSTATNB
                                    465 	.globl _AX5043_POWSTATNB
                                    466 	.globl _AX5043_POWIRQMASKNB
                                    467 	.globl _AX5043_POWCTRL1NB
                                    468 	.globl _AX5043_PLLVCOIRNB
                                    469 	.globl _AX5043_PLLVCOINB
                                    470 	.globl _AX5043_PLLVCODIVNB
                                    471 	.globl _AX5043_PLLRNGCLKNB
                                    472 	.globl _AX5043_PLLRANGINGBNB
                                    473 	.globl _AX5043_PLLRANGINGANB
                                    474 	.globl _AX5043_PLLLOOPBOOSTNB
                                    475 	.globl _AX5043_PLLLOOPNB
                                    476 	.globl _AX5043_PLLLOCKDETNB
                                    477 	.globl _AX5043_PLLCPIBOOSTNB
                                    478 	.globl _AX5043_PLLCPINB
                                    479 	.globl _AX5043_PKTSTOREFLAGSNB
                                    480 	.globl _AX5043_PKTMISCFLAGSNB
                                    481 	.globl _AX5043_PKTCHUNKSIZENB
                                    482 	.globl _AX5043_PKTACCEPTFLAGSNB
                                    483 	.globl _AX5043_PINSTATENB
                                    484 	.globl _AX5043_PINFUNCSYSCLKNB
                                    485 	.globl _AX5043_PINFUNCPWRAMPNB
                                    486 	.globl _AX5043_PINFUNCIRQNB
                                    487 	.globl _AX5043_PINFUNCDCLKNB
                                    488 	.globl _AX5043_PINFUNCDATANB
                                    489 	.globl _AX5043_PINFUNCANTSELNB
                                    490 	.globl _AX5043_MODULATIONNB
                                    491 	.globl _AX5043_MODCFGPNB
                                    492 	.globl _AX5043_MODCFGFNB
                                    493 	.globl _AX5043_MODCFGANB
                                    494 	.globl _AX5043_MAXRFOFFSET2NB
                                    495 	.globl _AX5043_MAXRFOFFSET1NB
                                    496 	.globl _AX5043_MAXRFOFFSET0NB
                                    497 	.globl _AX5043_MAXDROFFSET2NB
                                    498 	.globl _AX5043_MAXDROFFSET1NB
                                    499 	.globl _AX5043_MAXDROFFSET0NB
                                    500 	.globl _AX5043_MATCH1PAT1NB
                                    501 	.globl _AX5043_MATCH1PAT0NB
                                    502 	.globl _AX5043_MATCH1MINNB
                                    503 	.globl _AX5043_MATCH1MAXNB
                                    504 	.globl _AX5043_MATCH1LENNB
                                    505 	.globl _AX5043_MATCH0PAT3NB
                                    506 	.globl _AX5043_MATCH0PAT2NB
                                    507 	.globl _AX5043_MATCH0PAT1NB
                                    508 	.globl _AX5043_MATCH0PAT0NB
                                    509 	.globl _AX5043_MATCH0MINNB
                                    510 	.globl _AX5043_MATCH0MAXNB
                                    511 	.globl _AX5043_MATCH0LENNB
                                    512 	.globl _AX5043_LPOSCSTATUSNB
                                    513 	.globl _AX5043_LPOSCREF1NB
                                    514 	.globl _AX5043_LPOSCREF0NB
                                    515 	.globl _AX5043_LPOSCPER1NB
                                    516 	.globl _AX5043_LPOSCPER0NB
                                    517 	.globl _AX5043_LPOSCKFILT1NB
                                    518 	.globl _AX5043_LPOSCKFILT0NB
                                    519 	.globl _AX5043_LPOSCFREQ1NB
                                    520 	.globl _AX5043_LPOSCFREQ0NB
                                    521 	.globl _AX5043_LPOSCCONFIGNB
                                    522 	.globl _AX5043_IRQREQUEST1NB
                                    523 	.globl _AX5043_IRQREQUEST0NB
                                    524 	.globl _AX5043_IRQMASK1NB
                                    525 	.globl _AX5043_IRQMASK0NB
                                    526 	.globl _AX5043_IRQINVERSION1NB
                                    527 	.globl _AX5043_IRQINVERSION0NB
                                    528 	.globl _AX5043_IFFREQ1NB
                                    529 	.globl _AX5043_IFFREQ0NB
                                    530 	.globl _AX5043_GPADCPERIODNB
                                    531 	.globl _AX5043_GPADCCTRLNB
                                    532 	.globl _AX5043_GPADC13VALUE1NB
                                    533 	.globl _AX5043_GPADC13VALUE0NB
                                    534 	.globl _AX5043_FSKDMIN1NB
                                    535 	.globl _AX5043_FSKDMIN0NB
                                    536 	.globl _AX5043_FSKDMAX1NB
                                    537 	.globl _AX5043_FSKDMAX0NB
                                    538 	.globl _AX5043_FSKDEV2NB
                                    539 	.globl _AX5043_FSKDEV1NB
                                    540 	.globl _AX5043_FSKDEV0NB
                                    541 	.globl _AX5043_FREQB3NB
                                    542 	.globl _AX5043_FREQB2NB
                                    543 	.globl _AX5043_FREQB1NB
                                    544 	.globl _AX5043_FREQB0NB
                                    545 	.globl _AX5043_FREQA3NB
                                    546 	.globl _AX5043_FREQA2NB
                                    547 	.globl _AX5043_FREQA1NB
                                    548 	.globl _AX5043_FREQA0NB
                                    549 	.globl _AX5043_FRAMINGNB
                                    550 	.globl _AX5043_FIFOTHRESH1NB
                                    551 	.globl _AX5043_FIFOTHRESH0NB
                                    552 	.globl _AX5043_FIFOSTATNB
                                    553 	.globl _AX5043_FIFOFREE1NB
                                    554 	.globl _AX5043_FIFOFREE0NB
                                    555 	.globl _AX5043_FIFODATANB
                                    556 	.globl _AX5043_FIFOCOUNT1NB
                                    557 	.globl _AX5043_FIFOCOUNT0NB
                                    558 	.globl _AX5043_FECSYNCNB
                                    559 	.globl _AX5043_FECSTATUSNB
                                    560 	.globl _AX5043_FECNB
                                    561 	.globl _AX5043_ENCODINGNB
                                    562 	.globl _AX5043_DIVERSITYNB
                                    563 	.globl _AX5043_DECIMATIONNB
                                    564 	.globl _AX5043_DACVALUE1NB
                                    565 	.globl _AX5043_DACVALUE0NB
                                    566 	.globl _AX5043_DACCONFIGNB
                                    567 	.globl _AX5043_CRCINIT3NB
                                    568 	.globl _AX5043_CRCINIT2NB
                                    569 	.globl _AX5043_CRCINIT1NB
                                    570 	.globl _AX5043_CRCINIT0NB
                                    571 	.globl _AX5043_BGNDRSSITHRNB
                                    572 	.globl _AX5043_BGNDRSSIGAINNB
                                    573 	.globl _AX5043_BGNDRSSINB
                                    574 	.globl _AX5043_BBTUNENB
                                    575 	.globl _AX5043_BBOFFSCAPNB
                                    576 	.globl _AX5043_AMPLFILTERNB
                                    577 	.globl _AX5043_AGCCOUNTERNB
                                    578 	.globl _AX5043_AFSKSPACE1NB
                                    579 	.globl _AX5043_AFSKSPACE0NB
                                    580 	.globl _AX5043_AFSKMARK1NB
                                    581 	.globl _AX5043_AFSKMARK0NB
                                    582 	.globl _AX5043_AFSKCTRLNB
                                    583 	.globl _AX5043_TIMEGAIN3
                                    584 	.globl _AX5043_TIMEGAIN2
                                    585 	.globl _AX5043_TIMEGAIN1
                                    586 	.globl _AX5043_TIMEGAIN0
                                    587 	.globl _AX5043_RXPARAMSETS
                                    588 	.globl _AX5043_RXPARAMCURSET
                                    589 	.globl _AX5043_PKTMAXLEN
                                    590 	.globl _AX5043_PKTLENOFFSET
                                    591 	.globl _AX5043_PKTLENCFG
                                    592 	.globl _AX5043_PKTADDRMASK3
                                    593 	.globl _AX5043_PKTADDRMASK2
                                    594 	.globl _AX5043_PKTADDRMASK1
                                    595 	.globl _AX5043_PKTADDRMASK0
                                    596 	.globl _AX5043_PKTADDRCFG
                                    597 	.globl _AX5043_PKTADDR3
                                    598 	.globl _AX5043_PKTADDR2
                                    599 	.globl _AX5043_PKTADDR1
                                    600 	.globl _AX5043_PKTADDR0
                                    601 	.globl _AX5043_PHASEGAIN3
                                    602 	.globl _AX5043_PHASEGAIN2
                                    603 	.globl _AX5043_PHASEGAIN1
                                    604 	.globl _AX5043_PHASEGAIN0
                                    605 	.globl _AX5043_FREQUENCYLEAK
                                    606 	.globl _AX5043_FREQUENCYGAIND3
                                    607 	.globl _AX5043_FREQUENCYGAIND2
                                    608 	.globl _AX5043_FREQUENCYGAIND1
                                    609 	.globl _AX5043_FREQUENCYGAIND0
                                    610 	.globl _AX5043_FREQUENCYGAINC3
                                    611 	.globl _AX5043_FREQUENCYGAINC2
                                    612 	.globl _AX5043_FREQUENCYGAINC1
                                    613 	.globl _AX5043_FREQUENCYGAINC0
                                    614 	.globl _AX5043_FREQUENCYGAINB3
                                    615 	.globl _AX5043_FREQUENCYGAINB2
                                    616 	.globl _AX5043_FREQUENCYGAINB1
                                    617 	.globl _AX5043_FREQUENCYGAINB0
                                    618 	.globl _AX5043_FREQUENCYGAINA3
                                    619 	.globl _AX5043_FREQUENCYGAINA2
                                    620 	.globl _AX5043_FREQUENCYGAINA1
                                    621 	.globl _AX5043_FREQUENCYGAINA0
                                    622 	.globl _AX5043_FREQDEV13
                                    623 	.globl _AX5043_FREQDEV12
                                    624 	.globl _AX5043_FREQDEV11
                                    625 	.globl _AX5043_FREQDEV10
                                    626 	.globl _AX5043_FREQDEV03
                                    627 	.globl _AX5043_FREQDEV02
                                    628 	.globl _AX5043_FREQDEV01
                                    629 	.globl _AX5043_FREQDEV00
                                    630 	.globl _AX5043_FOURFSK3
                                    631 	.globl _AX5043_FOURFSK2
                                    632 	.globl _AX5043_FOURFSK1
                                    633 	.globl _AX5043_FOURFSK0
                                    634 	.globl _AX5043_DRGAIN3
                                    635 	.globl _AX5043_DRGAIN2
                                    636 	.globl _AX5043_DRGAIN1
                                    637 	.globl _AX5043_DRGAIN0
                                    638 	.globl _AX5043_BBOFFSRES3
                                    639 	.globl _AX5043_BBOFFSRES2
                                    640 	.globl _AX5043_BBOFFSRES1
                                    641 	.globl _AX5043_BBOFFSRES0
                                    642 	.globl _AX5043_AMPLITUDEGAIN3
                                    643 	.globl _AX5043_AMPLITUDEGAIN2
                                    644 	.globl _AX5043_AMPLITUDEGAIN1
                                    645 	.globl _AX5043_AMPLITUDEGAIN0
                                    646 	.globl _AX5043_AGCTARGET3
                                    647 	.globl _AX5043_AGCTARGET2
                                    648 	.globl _AX5043_AGCTARGET1
                                    649 	.globl _AX5043_AGCTARGET0
                                    650 	.globl _AX5043_AGCMINMAX3
                                    651 	.globl _AX5043_AGCMINMAX2
                                    652 	.globl _AX5043_AGCMINMAX1
                                    653 	.globl _AX5043_AGCMINMAX0
                                    654 	.globl _AX5043_AGCGAIN3
                                    655 	.globl _AX5043_AGCGAIN2
                                    656 	.globl _AX5043_AGCGAIN1
                                    657 	.globl _AX5043_AGCGAIN0
                                    658 	.globl _AX5043_AGCAHYST3
                                    659 	.globl _AX5043_AGCAHYST2
                                    660 	.globl _AX5043_AGCAHYST1
                                    661 	.globl _AX5043_AGCAHYST0
                                    662 	.globl _AX5043_0xF44
                                    663 	.globl _AX5043_0xF35
                                    664 	.globl _AX5043_0xF34
                                    665 	.globl _AX5043_0xF33
                                    666 	.globl _AX5043_0xF32
                                    667 	.globl _AX5043_0xF31
                                    668 	.globl _AX5043_0xF30
                                    669 	.globl _AX5043_0xF26
                                    670 	.globl _AX5043_0xF23
                                    671 	.globl _AX5043_0xF22
                                    672 	.globl _AX5043_0xF21
                                    673 	.globl _AX5043_0xF1C
                                    674 	.globl _AX5043_0xF18
                                    675 	.globl _AX5043_0xF0C
                                    676 	.globl _AX5043_0xF00
                                    677 	.globl _AX5043_XTALSTATUS
                                    678 	.globl _AX5043_XTALOSC
                                    679 	.globl _AX5043_XTALCAP
                                    680 	.globl _AX5043_XTALAMPL
                                    681 	.globl _AX5043_WAKEUPXOEARLY
                                    682 	.globl _AX5043_WAKEUPTIMER1
                                    683 	.globl _AX5043_WAKEUPTIMER0
                                    684 	.globl _AX5043_WAKEUPFREQ1
                                    685 	.globl _AX5043_WAKEUPFREQ0
                                    686 	.globl _AX5043_WAKEUP1
                                    687 	.globl _AX5043_WAKEUP0
                                    688 	.globl _AX5043_TXRATE2
                                    689 	.globl _AX5043_TXRATE1
                                    690 	.globl _AX5043_TXRATE0
                                    691 	.globl _AX5043_TXPWRCOEFFE1
                                    692 	.globl _AX5043_TXPWRCOEFFE0
                                    693 	.globl _AX5043_TXPWRCOEFFD1
                                    694 	.globl _AX5043_TXPWRCOEFFD0
                                    695 	.globl _AX5043_TXPWRCOEFFC1
                                    696 	.globl _AX5043_TXPWRCOEFFC0
                                    697 	.globl _AX5043_TXPWRCOEFFB1
                                    698 	.globl _AX5043_TXPWRCOEFFB0
                                    699 	.globl _AX5043_TXPWRCOEFFA1
                                    700 	.globl _AX5043_TXPWRCOEFFA0
                                    701 	.globl _AX5043_TRKRFFREQ2
                                    702 	.globl _AX5043_TRKRFFREQ1
                                    703 	.globl _AX5043_TRKRFFREQ0
                                    704 	.globl _AX5043_TRKPHASE1
                                    705 	.globl _AX5043_TRKPHASE0
                                    706 	.globl _AX5043_TRKFSKDEMOD1
                                    707 	.globl _AX5043_TRKFSKDEMOD0
                                    708 	.globl _AX5043_TRKFREQ1
                                    709 	.globl _AX5043_TRKFREQ0
                                    710 	.globl _AX5043_TRKDATARATE2
                                    711 	.globl _AX5043_TRKDATARATE1
                                    712 	.globl _AX5043_TRKDATARATE0
                                    713 	.globl _AX5043_TRKAMPLITUDE1
                                    714 	.globl _AX5043_TRKAMPLITUDE0
                                    715 	.globl _AX5043_TRKAFSKDEMOD1
                                    716 	.globl _AX5043_TRKAFSKDEMOD0
                                    717 	.globl _AX5043_TMGTXSETTLE
                                    718 	.globl _AX5043_TMGTXBOOST
                                    719 	.globl _AX5043_TMGRXSETTLE
                                    720 	.globl _AX5043_TMGRXRSSI
                                    721 	.globl _AX5043_TMGRXPREAMBLE3
                                    722 	.globl _AX5043_TMGRXPREAMBLE2
                                    723 	.globl _AX5043_TMGRXPREAMBLE1
                                    724 	.globl _AX5043_TMGRXOFFSACQ
                                    725 	.globl _AX5043_TMGRXCOARSEAGC
                                    726 	.globl _AX5043_TMGRXBOOST
                                    727 	.globl _AX5043_TMGRXAGC
                                    728 	.globl _AX5043_TIMER2
                                    729 	.globl _AX5043_TIMER1
                                    730 	.globl _AX5043_TIMER0
                                    731 	.globl _AX5043_SILICONREVISION
                                    732 	.globl _AX5043_SCRATCH
                                    733 	.globl _AX5043_RXDATARATE2
                                    734 	.globl _AX5043_RXDATARATE1
                                    735 	.globl _AX5043_RXDATARATE0
                                    736 	.globl _AX5043_RSSIREFERENCE
                                    737 	.globl _AX5043_RSSIABSTHR
                                    738 	.globl _AX5043_RSSI
                                    739 	.globl _AX5043_REF
                                    740 	.globl _AX5043_RADIOSTATE
                                    741 	.globl _AX5043_RADIOEVENTREQ1
                                    742 	.globl _AX5043_RADIOEVENTREQ0
                                    743 	.globl _AX5043_RADIOEVENTMASK1
                                    744 	.globl _AX5043_RADIOEVENTMASK0
                                    745 	.globl _AX5043_PWRMODE
                                    746 	.globl _AX5043_PWRAMP
                                    747 	.globl _AX5043_POWSTICKYSTAT
                                    748 	.globl _AX5043_POWSTAT
                                    749 	.globl _AX5043_POWIRQMASK
                                    750 	.globl _AX5043_POWCTRL1
                                    751 	.globl _AX5043_PLLVCOIR
                                    752 	.globl _AX5043_PLLVCOI
                                    753 	.globl _AX5043_PLLVCODIV
                                    754 	.globl _AX5043_PLLRNGCLK
                                    755 	.globl _AX5043_PLLRANGINGB
                                    756 	.globl _AX5043_PLLRANGINGA
                                    757 	.globl _AX5043_PLLLOOPBOOST
                                    758 	.globl _AX5043_PLLLOOP
                                    759 	.globl _AX5043_PLLLOCKDET
                                    760 	.globl _AX5043_PLLCPIBOOST
                                    761 	.globl _AX5043_PLLCPI
                                    762 	.globl _AX5043_PKTSTOREFLAGS
                                    763 	.globl _AX5043_PKTMISCFLAGS
                                    764 	.globl _AX5043_PKTCHUNKSIZE
                                    765 	.globl _AX5043_PKTACCEPTFLAGS
                                    766 	.globl _AX5043_PINSTATE
                                    767 	.globl _AX5043_PINFUNCSYSCLK
                                    768 	.globl _AX5043_PINFUNCPWRAMP
                                    769 	.globl _AX5043_PINFUNCIRQ
                                    770 	.globl _AX5043_PINFUNCDCLK
                                    771 	.globl _AX5043_PINFUNCDATA
                                    772 	.globl _AX5043_PINFUNCANTSEL
                                    773 	.globl _AX5043_MODULATION
                                    774 	.globl _AX5043_MODCFGP
                                    775 	.globl _AX5043_MODCFGF
                                    776 	.globl _AX5043_MODCFGA
                                    777 	.globl _AX5043_MAXRFOFFSET2
                                    778 	.globl _AX5043_MAXRFOFFSET1
                                    779 	.globl _AX5043_MAXRFOFFSET0
                                    780 	.globl _AX5043_MAXDROFFSET2
                                    781 	.globl _AX5043_MAXDROFFSET1
                                    782 	.globl _AX5043_MAXDROFFSET0
                                    783 	.globl _AX5043_MATCH1PAT1
                                    784 	.globl _AX5043_MATCH1PAT0
                                    785 	.globl _AX5043_MATCH1MIN
                                    786 	.globl _AX5043_MATCH1MAX
                                    787 	.globl _AX5043_MATCH1LEN
                                    788 	.globl _AX5043_MATCH0PAT3
                                    789 	.globl _AX5043_MATCH0PAT2
                                    790 	.globl _AX5043_MATCH0PAT1
                                    791 	.globl _AX5043_MATCH0PAT0
                                    792 	.globl _AX5043_MATCH0MIN
                                    793 	.globl _AX5043_MATCH0MAX
                                    794 	.globl _AX5043_MATCH0LEN
                                    795 	.globl _AX5043_LPOSCSTATUS
                                    796 	.globl _AX5043_LPOSCREF1
                                    797 	.globl _AX5043_LPOSCREF0
                                    798 	.globl _AX5043_LPOSCPER1
                                    799 	.globl _AX5043_LPOSCPER0
                                    800 	.globl _AX5043_LPOSCKFILT1
                                    801 	.globl _AX5043_LPOSCKFILT0
                                    802 	.globl _AX5043_LPOSCFREQ1
                                    803 	.globl _AX5043_LPOSCFREQ0
                                    804 	.globl _AX5043_LPOSCCONFIG
                                    805 	.globl _AX5043_IRQREQUEST1
                                    806 	.globl _AX5043_IRQREQUEST0
                                    807 	.globl _AX5043_IRQMASK1
                                    808 	.globl _AX5043_IRQMASK0
                                    809 	.globl _AX5043_IRQINVERSION1
                                    810 	.globl _AX5043_IRQINVERSION0
                                    811 	.globl _AX5043_IFFREQ1
                                    812 	.globl _AX5043_IFFREQ0
                                    813 	.globl _AX5043_GPADCPERIOD
                                    814 	.globl _AX5043_GPADCCTRL
                                    815 	.globl _AX5043_GPADC13VALUE1
                                    816 	.globl _AX5043_GPADC13VALUE0
                                    817 	.globl _AX5043_FSKDMIN1
                                    818 	.globl _AX5043_FSKDMIN0
                                    819 	.globl _AX5043_FSKDMAX1
                                    820 	.globl _AX5043_FSKDMAX0
                                    821 	.globl _AX5043_FSKDEV2
                                    822 	.globl _AX5043_FSKDEV1
                                    823 	.globl _AX5043_FSKDEV0
                                    824 	.globl _AX5043_FREQB3
                                    825 	.globl _AX5043_FREQB2
                                    826 	.globl _AX5043_FREQB1
                                    827 	.globl _AX5043_FREQB0
                                    828 	.globl _AX5043_FREQA3
                                    829 	.globl _AX5043_FREQA2
                                    830 	.globl _AX5043_FREQA1
                                    831 	.globl _AX5043_FREQA0
                                    832 	.globl _AX5043_FRAMING
                                    833 	.globl _AX5043_FIFOTHRESH1
                                    834 	.globl _AX5043_FIFOTHRESH0
                                    835 	.globl _AX5043_FIFOSTAT
                                    836 	.globl _AX5043_FIFOFREE1
                                    837 	.globl _AX5043_FIFOFREE0
                                    838 	.globl _AX5043_FIFODATA
                                    839 	.globl _AX5043_FIFOCOUNT1
                                    840 	.globl _AX5043_FIFOCOUNT0
                                    841 	.globl _AX5043_FECSYNC
                                    842 	.globl _AX5043_FECSTATUS
                                    843 	.globl _AX5043_FEC
                                    844 	.globl _AX5043_ENCODING
                                    845 	.globl _AX5043_DIVERSITY
                                    846 	.globl _AX5043_DECIMATION
                                    847 	.globl _AX5043_DACVALUE1
                                    848 	.globl _AX5043_DACVALUE0
                                    849 	.globl _AX5043_DACCONFIG
                                    850 	.globl _AX5043_CRCINIT3
                                    851 	.globl _AX5043_CRCINIT2
                                    852 	.globl _AX5043_CRCINIT1
                                    853 	.globl _AX5043_CRCINIT0
                                    854 	.globl _AX5043_BGNDRSSITHR
                                    855 	.globl _AX5043_BGNDRSSIGAIN
                                    856 	.globl _AX5043_BGNDRSSI
                                    857 	.globl _AX5043_BBTUNE
                                    858 	.globl _AX5043_BBOFFSCAP
                                    859 	.globl _AX5043_AMPLFILTER
                                    860 	.globl _AX5043_AGCCOUNTER
                                    861 	.globl _AX5043_AFSKSPACE1
                                    862 	.globl _AX5043_AFSKSPACE0
                                    863 	.globl _AX5043_AFSKMARK1
                                    864 	.globl _AX5043_AFSKMARK0
                                    865 	.globl _AX5043_AFSKCTRL
                                    866 	.globl _XWTSTAT
                                    867 	.globl _XWTIRQEN
                                    868 	.globl _XWTEVTD
                                    869 	.globl _XWTEVTD1
                                    870 	.globl _XWTEVTD0
                                    871 	.globl _XWTEVTC
                                    872 	.globl _XWTEVTC1
                                    873 	.globl _XWTEVTC0
                                    874 	.globl _XWTEVTB
                                    875 	.globl _XWTEVTB1
                                    876 	.globl _XWTEVTB0
                                    877 	.globl _XWTEVTA
                                    878 	.globl _XWTEVTA1
                                    879 	.globl _XWTEVTA0
                                    880 	.globl _XWTCNTR1
                                    881 	.globl _XWTCNTB
                                    882 	.globl _XWTCNTB1
                                    883 	.globl _XWTCNTB0
                                    884 	.globl _XWTCNTA
                                    885 	.globl _XWTCNTA1
                                    886 	.globl _XWTCNTA0
                                    887 	.globl _XWTCFGB
                                    888 	.globl _XWTCFGA
                                    889 	.globl _XWDTRESET
                                    890 	.globl _XWDTCFG
                                    891 	.globl _XU1STATUS
                                    892 	.globl _XU1SHREG
                                    893 	.globl _XU1MODE
                                    894 	.globl _XU1CTRL
                                    895 	.globl _XU0STATUS
                                    896 	.globl _XU0SHREG
                                    897 	.globl _XU0MODE
                                    898 	.globl _XU0CTRL
                                    899 	.globl _XT2STATUS
                                    900 	.globl _XT2PERIOD
                                    901 	.globl _XT2PERIOD1
                                    902 	.globl _XT2PERIOD0
                                    903 	.globl _XT2MODE
                                    904 	.globl _XT2CNT
                                    905 	.globl _XT2CNT1
                                    906 	.globl _XT2CNT0
                                    907 	.globl _XT2CLKSRC
                                    908 	.globl _XT1STATUS
                                    909 	.globl _XT1PERIOD
                                    910 	.globl _XT1PERIOD1
                                    911 	.globl _XT1PERIOD0
                                    912 	.globl _XT1MODE
                                    913 	.globl _XT1CNT
                                    914 	.globl _XT1CNT1
                                    915 	.globl _XT1CNT0
                                    916 	.globl _XT1CLKSRC
                                    917 	.globl _XT0STATUS
                                    918 	.globl _XT0PERIOD
                                    919 	.globl _XT0PERIOD1
                                    920 	.globl _XT0PERIOD0
                                    921 	.globl _XT0MODE
                                    922 	.globl _XT0CNT
                                    923 	.globl _XT0CNT1
                                    924 	.globl _XT0CNT0
                                    925 	.globl _XT0CLKSRC
                                    926 	.globl _XSPSTATUS
                                    927 	.globl _XSPSHREG
                                    928 	.globl _XSPMODE
                                    929 	.globl _XSPCLKSRC
                                    930 	.globl _XRADIOSTAT
                                    931 	.globl _XRADIOSTAT1
                                    932 	.globl _XRADIOSTAT0
                                    933 	.globl _XRADIODATA3
                                    934 	.globl _XRADIODATA2
                                    935 	.globl _XRADIODATA1
                                    936 	.globl _XRADIODATA0
                                    937 	.globl _XRADIOADDR1
                                    938 	.globl _XRADIOADDR0
                                    939 	.globl _XRADIOACC
                                    940 	.globl _XOC1STATUS
                                    941 	.globl _XOC1PIN
                                    942 	.globl _XOC1MODE
                                    943 	.globl _XOC1COMP
                                    944 	.globl _XOC1COMP1
                                    945 	.globl _XOC1COMP0
                                    946 	.globl _XOC0STATUS
                                    947 	.globl _XOC0PIN
                                    948 	.globl _XOC0MODE
                                    949 	.globl _XOC0COMP
                                    950 	.globl _XOC0COMP1
                                    951 	.globl _XOC0COMP0
                                    952 	.globl _XNVSTATUS
                                    953 	.globl _XNVKEY
                                    954 	.globl _XNVDATA
                                    955 	.globl _XNVDATA1
                                    956 	.globl _XNVDATA0
                                    957 	.globl _XNVADDR
                                    958 	.globl _XNVADDR1
                                    959 	.globl _XNVADDR0
                                    960 	.globl _XIC1STATUS
                                    961 	.globl _XIC1MODE
                                    962 	.globl _XIC1CAPT
                                    963 	.globl _XIC1CAPT1
                                    964 	.globl _XIC1CAPT0
                                    965 	.globl _XIC0STATUS
                                    966 	.globl _XIC0MODE
                                    967 	.globl _XIC0CAPT
                                    968 	.globl _XIC0CAPT1
                                    969 	.globl _XIC0CAPT0
                                    970 	.globl _XPORTR
                                    971 	.globl _XPORTC
                                    972 	.globl _XPORTB
                                    973 	.globl _XPORTA
                                    974 	.globl _XPINR
                                    975 	.globl _XPINC
                                    976 	.globl _XPINB
                                    977 	.globl _XPINA
                                    978 	.globl _XDIRR
                                    979 	.globl _XDIRC
                                    980 	.globl _XDIRB
                                    981 	.globl _XDIRA
                                    982 	.globl _XDBGLNKSTAT
                                    983 	.globl _XDBGLNKBUF
                                    984 	.globl _XCODECONFIG
                                    985 	.globl _XCLKSTAT
                                    986 	.globl _XCLKCON
                                    987 	.globl _XANALOGCOMP
                                    988 	.globl _XADCCONV
                                    989 	.globl _XADCCLKSRC
                                    990 	.globl _XADCCH3CONFIG
                                    991 	.globl _XADCCH2CONFIG
                                    992 	.globl _XADCCH1CONFIG
                                    993 	.globl _XADCCH0CONFIG
                                    994 	.globl _XPCON
                                    995 	.globl _XIP
                                    996 	.globl _XIE
                                    997 	.globl _XDPTR1
                                    998 	.globl _XDPTR0
                                    999 	.globl _XTALREADY
                                   1000 	.globl _XTALOSC
                                   1001 	.globl _XTALAMPL
                                   1002 	.globl _SILICONREV
                                   1003 	.globl _SCRATCH3
                                   1004 	.globl _SCRATCH2
                                   1005 	.globl _SCRATCH1
                                   1006 	.globl _SCRATCH0
                                   1007 	.globl _RADIOMUX
                                   1008 	.globl _RADIOFSTATADDR
                                   1009 	.globl _RADIOFSTATADDR1
                                   1010 	.globl _RADIOFSTATADDR0
                                   1011 	.globl _RADIOFDATAADDR
                                   1012 	.globl _RADIOFDATAADDR1
                                   1013 	.globl _RADIOFDATAADDR0
                                   1014 	.globl _OSCRUN
                                   1015 	.globl _OSCREADY
                                   1016 	.globl _OSCFORCERUN
                                   1017 	.globl _OSCCALIB
                                   1018 	.globl _MISCCTRL
                                   1019 	.globl _LPXOSCGM
                                   1020 	.globl _LPOSCREF
                                   1021 	.globl _LPOSCREF1
                                   1022 	.globl _LPOSCREF0
                                   1023 	.globl _LPOSCPER
                                   1024 	.globl _LPOSCPER1
                                   1025 	.globl _LPOSCPER0
                                   1026 	.globl _LPOSCKFILT
                                   1027 	.globl _LPOSCKFILT1
                                   1028 	.globl _LPOSCKFILT0
                                   1029 	.globl _LPOSCFREQ
                                   1030 	.globl _LPOSCFREQ1
                                   1031 	.globl _LPOSCFREQ0
                                   1032 	.globl _LPOSCCONFIG
                                   1033 	.globl _PINSEL
                                   1034 	.globl _PINCHGC
                                   1035 	.globl _PINCHGB
                                   1036 	.globl _PINCHGA
                                   1037 	.globl _PALTRADIO
                                   1038 	.globl _PALTC
                                   1039 	.globl _PALTB
                                   1040 	.globl _PALTA
                                   1041 	.globl _INTCHGC
                                   1042 	.globl _INTCHGB
                                   1043 	.globl _INTCHGA
                                   1044 	.globl _EXTIRQ
                                   1045 	.globl _GPIOENABLE
                                   1046 	.globl _ANALOGA
                                   1047 	.globl _FRCOSCREF
                                   1048 	.globl _FRCOSCREF1
                                   1049 	.globl _FRCOSCREF0
                                   1050 	.globl _FRCOSCPER
                                   1051 	.globl _FRCOSCPER1
                                   1052 	.globl _FRCOSCPER0
                                   1053 	.globl _FRCOSCKFILT
                                   1054 	.globl _FRCOSCKFILT1
                                   1055 	.globl _FRCOSCKFILT0
                                   1056 	.globl _FRCOSCFREQ
                                   1057 	.globl _FRCOSCFREQ1
                                   1058 	.globl _FRCOSCFREQ0
                                   1059 	.globl _FRCOSCCTRL
                                   1060 	.globl _FRCOSCCONFIG
                                   1061 	.globl _DMA1CONFIG
                                   1062 	.globl _DMA1ADDR
                                   1063 	.globl _DMA1ADDR1
                                   1064 	.globl _DMA1ADDR0
                                   1065 	.globl _DMA0CONFIG
                                   1066 	.globl _DMA0ADDR
                                   1067 	.globl _DMA0ADDR1
                                   1068 	.globl _DMA0ADDR0
                                   1069 	.globl _ADCTUNE2
                                   1070 	.globl _ADCTUNE1
                                   1071 	.globl _ADCTUNE0
                                   1072 	.globl _ADCCH3VAL
                                   1073 	.globl _ADCCH3VAL1
                                   1074 	.globl _ADCCH3VAL0
                                   1075 	.globl _ADCCH2VAL
                                   1076 	.globl _ADCCH2VAL1
                                   1077 	.globl _ADCCH2VAL0
                                   1078 	.globl _ADCCH1VAL
                                   1079 	.globl _ADCCH1VAL1
                                   1080 	.globl _ADCCH1VAL0
                                   1081 	.globl _ADCCH0VAL
                                   1082 	.globl _ADCCH0VAL1
                                   1083 	.globl _ADCCH0VAL0
                                   1084 	.globl _aligned_alloc_PARM_2
                                   1085 	.globl _UBLOX_GPS_PortInit
                                   1086 	.globl _UBLOX_GPS_FletcherChecksum8
                                   1087 	.globl _UBLOX_GPS_SendCommand_WaitACK
                                   1088 ;--------------------------------------------------------
                                   1089 ; special function registers
                                   1090 ;--------------------------------------------------------
                                   1091 	.area RSEG    (ABS,DATA)
      000000                       1092 	.org 0x0000
                           0000E0  1093 _ACC	=	0x00e0
                           0000F0  1094 _B	=	0x00f0
                           000083  1095 _DPH	=	0x0083
                           000085  1096 _DPH1	=	0x0085
                           000082  1097 _DPL	=	0x0082
                           000084  1098 _DPL1	=	0x0084
                           008382  1099 _DPTR0	=	0x8382
                           008584  1100 _DPTR1	=	0x8584
                           000086  1101 _DPS	=	0x0086
                           0000A0  1102 _E2IE	=	0x00a0
                           0000C0  1103 _E2IP	=	0x00c0
                           000098  1104 _EIE	=	0x0098
                           0000B0  1105 _EIP	=	0x00b0
                           0000A8  1106 _IE	=	0x00a8
                           0000B8  1107 _IP	=	0x00b8
                           000087  1108 _PCON	=	0x0087
                           0000D0  1109 _PSW	=	0x00d0
                           000081  1110 _SP	=	0x0081
                           0000D9  1111 _XPAGE	=	0x00d9
                           0000D9  1112 __XPAGE	=	0x00d9
                           0000CA  1113 _ADCCH0CONFIG	=	0x00ca
                           0000CB  1114 _ADCCH1CONFIG	=	0x00cb
                           0000D2  1115 _ADCCH2CONFIG	=	0x00d2
                           0000D3  1116 _ADCCH3CONFIG	=	0x00d3
                           0000D1  1117 _ADCCLKSRC	=	0x00d1
                           0000C9  1118 _ADCCONV	=	0x00c9
                           0000E1  1119 _ANALOGCOMP	=	0x00e1
                           0000C6  1120 _CLKCON	=	0x00c6
                           0000C7  1121 _CLKSTAT	=	0x00c7
                           000097  1122 _CODECONFIG	=	0x0097
                           0000E3  1123 _DBGLNKBUF	=	0x00e3
                           0000E2  1124 _DBGLNKSTAT	=	0x00e2
                           000089  1125 _DIRA	=	0x0089
                           00008A  1126 _DIRB	=	0x008a
                           00008B  1127 _DIRC	=	0x008b
                           00008E  1128 _DIRR	=	0x008e
                           0000C8  1129 _PINA	=	0x00c8
                           0000E8  1130 _PINB	=	0x00e8
                           0000F8  1131 _PINC	=	0x00f8
                           00008D  1132 _PINR	=	0x008d
                           000080  1133 _PORTA	=	0x0080
                           000088  1134 _PORTB	=	0x0088
                           000090  1135 _PORTC	=	0x0090
                           00008C  1136 _PORTR	=	0x008c
                           0000CE  1137 _IC0CAPT0	=	0x00ce
                           0000CF  1138 _IC0CAPT1	=	0x00cf
                           00CFCE  1139 _IC0CAPT	=	0xcfce
                           0000CC  1140 _IC0MODE	=	0x00cc
                           0000CD  1141 _IC0STATUS	=	0x00cd
                           0000D6  1142 _IC1CAPT0	=	0x00d6
                           0000D7  1143 _IC1CAPT1	=	0x00d7
                           00D7D6  1144 _IC1CAPT	=	0xd7d6
                           0000D4  1145 _IC1MODE	=	0x00d4
                           0000D5  1146 _IC1STATUS	=	0x00d5
                           000092  1147 _NVADDR0	=	0x0092
                           000093  1148 _NVADDR1	=	0x0093
                           009392  1149 _NVADDR	=	0x9392
                           000094  1150 _NVDATA0	=	0x0094
                           000095  1151 _NVDATA1	=	0x0095
                           009594  1152 _NVDATA	=	0x9594
                           000096  1153 _NVKEY	=	0x0096
                           000091  1154 _NVSTATUS	=	0x0091
                           0000BC  1155 _OC0COMP0	=	0x00bc
                           0000BD  1156 _OC0COMP1	=	0x00bd
                           00BDBC  1157 _OC0COMP	=	0xbdbc
                           0000B9  1158 _OC0MODE	=	0x00b9
                           0000BA  1159 _OC0PIN	=	0x00ba
                           0000BB  1160 _OC0STATUS	=	0x00bb
                           0000C4  1161 _OC1COMP0	=	0x00c4
                           0000C5  1162 _OC1COMP1	=	0x00c5
                           00C5C4  1163 _OC1COMP	=	0xc5c4
                           0000C1  1164 _OC1MODE	=	0x00c1
                           0000C2  1165 _OC1PIN	=	0x00c2
                           0000C3  1166 _OC1STATUS	=	0x00c3
                           0000B1  1167 _RADIOACC	=	0x00b1
                           0000B3  1168 _RADIOADDR0	=	0x00b3
                           0000B2  1169 _RADIOADDR1	=	0x00b2
                           00B2B3  1170 _RADIOADDR	=	0xb2b3
                           0000B7  1171 _RADIODATA0	=	0x00b7
                           0000B6  1172 _RADIODATA1	=	0x00b6
                           0000B5  1173 _RADIODATA2	=	0x00b5
                           0000B4  1174 _RADIODATA3	=	0x00b4
                           B4B5B6B7  1175 _RADIODATA	=	0xb4b5b6b7
                           0000BE  1176 _RADIOSTAT0	=	0x00be
                           0000BF  1177 _RADIOSTAT1	=	0x00bf
                           00BFBE  1178 _RADIOSTAT	=	0xbfbe
                           0000DF  1179 _SPCLKSRC	=	0x00df
                           0000DC  1180 _SPMODE	=	0x00dc
                           0000DE  1181 _SPSHREG	=	0x00de
                           0000DD  1182 _SPSTATUS	=	0x00dd
                           00009A  1183 _T0CLKSRC	=	0x009a
                           00009C  1184 _T0CNT0	=	0x009c
                           00009D  1185 _T0CNT1	=	0x009d
                           009D9C  1186 _T0CNT	=	0x9d9c
                           000099  1187 _T0MODE	=	0x0099
                           00009E  1188 _T0PERIOD0	=	0x009e
                           00009F  1189 _T0PERIOD1	=	0x009f
                           009F9E  1190 _T0PERIOD	=	0x9f9e
                           00009B  1191 _T0STATUS	=	0x009b
                           0000A2  1192 _T1CLKSRC	=	0x00a2
                           0000A4  1193 _T1CNT0	=	0x00a4
                           0000A5  1194 _T1CNT1	=	0x00a5
                           00A5A4  1195 _T1CNT	=	0xa5a4
                           0000A1  1196 _T1MODE	=	0x00a1
                           0000A6  1197 _T1PERIOD0	=	0x00a6
                           0000A7  1198 _T1PERIOD1	=	0x00a7
                           00A7A6  1199 _T1PERIOD	=	0xa7a6
                           0000A3  1200 _T1STATUS	=	0x00a3
                           0000AA  1201 _T2CLKSRC	=	0x00aa
                           0000AC  1202 _T2CNT0	=	0x00ac
                           0000AD  1203 _T2CNT1	=	0x00ad
                           00ADAC  1204 _T2CNT	=	0xadac
                           0000A9  1205 _T2MODE	=	0x00a9
                           0000AE  1206 _T2PERIOD0	=	0x00ae
                           0000AF  1207 _T2PERIOD1	=	0x00af
                           00AFAE  1208 _T2PERIOD	=	0xafae
                           0000AB  1209 _T2STATUS	=	0x00ab
                           0000E4  1210 _U0CTRL	=	0x00e4
                           0000E7  1211 _U0MODE	=	0x00e7
                           0000E6  1212 _U0SHREG	=	0x00e6
                           0000E5  1213 _U0STATUS	=	0x00e5
                           0000EC  1214 _U1CTRL	=	0x00ec
                           0000EF  1215 _U1MODE	=	0x00ef
                           0000EE  1216 _U1SHREG	=	0x00ee
                           0000ED  1217 _U1STATUS	=	0x00ed
                           0000DA  1218 _WDTCFG	=	0x00da
                           0000DB  1219 _WDTRESET	=	0x00db
                           0000F1  1220 _WTCFGA	=	0x00f1
                           0000F9  1221 _WTCFGB	=	0x00f9
                           0000F2  1222 _WTCNTA0	=	0x00f2
                           0000F3  1223 _WTCNTA1	=	0x00f3
                           00F3F2  1224 _WTCNTA	=	0xf3f2
                           0000FA  1225 _WTCNTB0	=	0x00fa
                           0000FB  1226 _WTCNTB1	=	0x00fb
                           00FBFA  1227 _WTCNTB	=	0xfbfa
                           0000EB  1228 _WTCNTR1	=	0x00eb
                           0000F4  1229 _WTEVTA0	=	0x00f4
                           0000F5  1230 _WTEVTA1	=	0x00f5
                           00F5F4  1231 _WTEVTA	=	0xf5f4
                           0000F6  1232 _WTEVTB0	=	0x00f6
                           0000F7  1233 _WTEVTB1	=	0x00f7
                           00F7F6  1234 _WTEVTB	=	0xf7f6
                           0000FC  1235 _WTEVTC0	=	0x00fc
                           0000FD  1236 _WTEVTC1	=	0x00fd
                           00FDFC  1237 _WTEVTC	=	0xfdfc
                           0000FE  1238 _WTEVTD0	=	0x00fe
                           0000FF  1239 _WTEVTD1	=	0x00ff
                           00FFFE  1240 _WTEVTD	=	0xfffe
                           0000E9  1241 _WTIRQEN	=	0x00e9
                           0000EA  1242 _WTSTAT	=	0x00ea
                                   1243 ;--------------------------------------------------------
                                   1244 ; special function bits
                                   1245 ;--------------------------------------------------------
                                   1246 	.area RSEG    (ABS,DATA)
      000000                       1247 	.org 0x0000
                           0000E0  1248 _ACC_0	=	0x00e0
                           0000E1  1249 _ACC_1	=	0x00e1
                           0000E2  1250 _ACC_2	=	0x00e2
                           0000E3  1251 _ACC_3	=	0x00e3
                           0000E4  1252 _ACC_4	=	0x00e4
                           0000E5  1253 _ACC_5	=	0x00e5
                           0000E6  1254 _ACC_6	=	0x00e6
                           0000E7  1255 _ACC_7	=	0x00e7
                           0000F0  1256 _B_0	=	0x00f0
                           0000F1  1257 _B_1	=	0x00f1
                           0000F2  1258 _B_2	=	0x00f2
                           0000F3  1259 _B_3	=	0x00f3
                           0000F4  1260 _B_4	=	0x00f4
                           0000F5  1261 _B_5	=	0x00f5
                           0000F6  1262 _B_6	=	0x00f6
                           0000F7  1263 _B_7	=	0x00f7
                           0000A0  1264 _E2IE_0	=	0x00a0
                           0000A1  1265 _E2IE_1	=	0x00a1
                           0000A2  1266 _E2IE_2	=	0x00a2
                           0000A3  1267 _E2IE_3	=	0x00a3
                           0000A4  1268 _E2IE_4	=	0x00a4
                           0000A5  1269 _E2IE_5	=	0x00a5
                           0000A6  1270 _E2IE_6	=	0x00a6
                           0000A7  1271 _E2IE_7	=	0x00a7
                           0000C0  1272 _E2IP_0	=	0x00c0
                           0000C1  1273 _E2IP_1	=	0x00c1
                           0000C2  1274 _E2IP_2	=	0x00c2
                           0000C3  1275 _E2IP_3	=	0x00c3
                           0000C4  1276 _E2IP_4	=	0x00c4
                           0000C5  1277 _E2IP_5	=	0x00c5
                           0000C6  1278 _E2IP_6	=	0x00c6
                           0000C7  1279 _E2IP_7	=	0x00c7
                           000098  1280 _EIE_0	=	0x0098
                           000099  1281 _EIE_1	=	0x0099
                           00009A  1282 _EIE_2	=	0x009a
                           00009B  1283 _EIE_3	=	0x009b
                           00009C  1284 _EIE_4	=	0x009c
                           00009D  1285 _EIE_5	=	0x009d
                           00009E  1286 _EIE_6	=	0x009e
                           00009F  1287 _EIE_7	=	0x009f
                           0000B0  1288 _EIP_0	=	0x00b0
                           0000B1  1289 _EIP_1	=	0x00b1
                           0000B2  1290 _EIP_2	=	0x00b2
                           0000B3  1291 _EIP_3	=	0x00b3
                           0000B4  1292 _EIP_4	=	0x00b4
                           0000B5  1293 _EIP_5	=	0x00b5
                           0000B6  1294 _EIP_6	=	0x00b6
                           0000B7  1295 _EIP_7	=	0x00b7
                           0000A8  1296 _IE_0	=	0x00a8
                           0000A9  1297 _IE_1	=	0x00a9
                           0000AA  1298 _IE_2	=	0x00aa
                           0000AB  1299 _IE_3	=	0x00ab
                           0000AC  1300 _IE_4	=	0x00ac
                           0000AD  1301 _IE_5	=	0x00ad
                           0000AE  1302 _IE_6	=	0x00ae
                           0000AF  1303 _IE_7	=	0x00af
                           0000AF  1304 _EA	=	0x00af
                           0000B8  1305 _IP_0	=	0x00b8
                           0000B9  1306 _IP_1	=	0x00b9
                           0000BA  1307 _IP_2	=	0x00ba
                           0000BB  1308 _IP_3	=	0x00bb
                           0000BC  1309 _IP_4	=	0x00bc
                           0000BD  1310 _IP_5	=	0x00bd
                           0000BE  1311 _IP_6	=	0x00be
                           0000BF  1312 _IP_7	=	0x00bf
                           0000D0  1313 _P	=	0x00d0
                           0000D1  1314 _F1	=	0x00d1
                           0000D2  1315 _OV	=	0x00d2
                           0000D3  1316 _RS0	=	0x00d3
                           0000D4  1317 _RS1	=	0x00d4
                           0000D5  1318 _F0	=	0x00d5
                           0000D6  1319 _AC	=	0x00d6
                           0000D7  1320 _CY	=	0x00d7
                           0000C8  1321 _PINA_0	=	0x00c8
                           0000C9  1322 _PINA_1	=	0x00c9
                           0000CA  1323 _PINA_2	=	0x00ca
                           0000CB  1324 _PINA_3	=	0x00cb
                           0000CC  1325 _PINA_4	=	0x00cc
                           0000CD  1326 _PINA_5	=	0x00cd
                           0000CE  1327 _PINA_6	=	0x00ce
                           0000CF  1328 _PINA_7	=	0x00cf
                           0000E8  1329 _PINB_0	=	0x00e8
                           0000E9  1330 _PINB_1	=	0x00e9
                           0000EA  1331 _PINB_2	=	0x00ea
                           0000EB  1332 _PINB_3	=	0x00eb
                           0000EC  1333 _PINB_4	=	0x00ec
                           0000ED  1334 _PINB_5	=	0x00ed
                           0000EE  1335 _PINB_6	=	0x00ee
                           0000EF  1336 _PINB_7	=	0x00ef
                           0000F8  1337 _PINC_0	=	0x00f8
                           0000F9  1338 _PINC_1	=	0x00f9
                           0000FA  1339 _PINC_2	=	0x00fa
                           0000FB  1340 _PINC_3	=	0x00fb
                           0000FC  1341 _PINC_4	=	0x00fc
                           0000FD  1342 _PINC_5	=	0x00fd
                           0000FE  1343 _PINC_6	=	0x00fe
                           0000FF  1344 _PINC_7	=	0x00ff
                           000080  1345 _PORTA_0	=	0x0080
                           000081  1346 _PORTA_1	=	0x0081
                           000082  1347 _PORTA_2	=	0x0082
                           000083  1348 _PORTA_3	=	0x0083
                           000084  1349 _PORTA_4	=	0x0084
                           000085  1350 _PORTA_5	=	0x0085
                           000086  1351 _PORTA_6	=	0x0086
                           000087  1352 _PORTA_7	=	0x0087
                           000088  1353 _PORTB_0	=	0x0088
                           000089  1354 _PORTB_1	=	0x0089
                           00008A  1355 _PORTB_2	=	0x008a
                           00008B  1356 _PORTB_3	=	0x008b
                           00008C  1357 _PORTB_4	=	0x008c
                           00008D  1358 _PORTB_5	=	0x008d
                           00008E  1359 _PORTB_6	=	0x008e
                           00008F  1360 _PORTB_7	=	0x008f
                           000090  1361 _PORTC_0	=	0x0090
                           000091  1362 _PORTC_1	=	0x0091
                           000092  1363 _PORTC_2	=	0x0092
                           000093  1364 _PORTC_3	=	0x0093
                           000094  1365 _PORTC_4	=	0x0094
                           000095  1366 _PORTC_5	=	0x0095
                           000096  1367 _PORTC_6	=	0x0096
                           000097  1368 _PORTC_7	=	0x0097
                                   1369 ;--------------------------------------------------------
                                   1370 ; overlayable register banks
                                   1371 ;--------------------------------------------------------
                                   1372 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                       1373 	.ds 8
                                   1374 ;--------------------------------------------------------
                                   1375 ; internal ram data
                                   1376 ;--------------------------------------------------------
                                   1377 	.area DSEG    (DATA)
      000029                       1378 _UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0:
      000029                       1379 	.ds 1
      00002A                       1380 _UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0:
      00002A                       1381 	.ds 3
      00002D                       1382 _UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0:
      00002D                       1383 	.ds 3
      000030                       1384 _UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0:
      000030                       1385 	.ds 2
      000032                       1386 _UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0:
      000032                       1387 	.ds 3
                                   1388 ;--------------------------------------------------------
                                   1389 ; overlayable items in internal ram 
                                   1390 ;--------------------------------------------------------
                                   1391 	.area	OSEG    (OVR,DATA)
      000043                       1392 _UBLOX_GPS_FletcherChecksum8_sloc0_1_0:
      000043                       1393 	.ds 3
      000046                       1394 _UBLOX_GPS_FletcherChecksum8_sloc1_1_0:
      000046                       1395 	.ds 1
      000047                       1396 _UBLOX_GPS_FletcherChecksum8_sloc2_1_0:
      000047                       1397 	.ds 1
      000048                       1398 _UBLOX_GPS_FletcherChecksum8_sloc3_1_0:
      000048                       1399 	.ds 3
                                   1400 ;--------------------------------------------------------
                                   1401 ; indirectly addressable internal ram data
                                   1402 ;--------------------------------------------------------
                                   1403 	.area ISEG    (DATA)
                                   1404 ;--------------------------------------------------------
                                   1405 ; absolute internal ram data
                                   1406 ;--------------------------------------------------------
                                   1407 	.area IABS    (ABS,DATA)
                                   1408 	.area IABS    (ABS,DATA)
                                   1409 ;--------------------------------------------------------
                                   1410 ; bit data
                                   1411 ;--------------------------------------------------------
                                   1412 	.area BSEG    (BIT)
                                   1413 ;--------------------------------------------------------
                                   1414 ; paged external ram data
                                   1415 ;--------------------------------------------------------
                                   1416 	.area PSEG    (PAG,XDATA)
                                   1417 ;--------------------------------------------------------
                                   1418 ; external ram data
                                   1419 ;--------------------------------------------------------
                                   1420 	.area XSEG    (XDATA)
      0003A6                       1421 _aligned_alloc_PARM_2:
      0003A6                       1422 	.ds 2
                           007020  1423 _ADCCH0VAL0	=	0x7020
                           007021  1424 _ADCCH0VAL1	=	0x7021
                           007020  1425 _ADCCH0VAL	=	0x7020
                           007022  1426 _ADCCH1VAL0	=	0x7022
                           007023  1427 _ADCCH1VAL1	=	0x7023
                           007022  1428 _ADCCH1VAL	=	0x7022
                           007024  1429 _ADCCH2VAL0	=	0x7024
                           007025  1430 _ADCCH2VAL1	=	0x7025
                           007024  1431 _ADCCH2VAL	=	0x7024
                           007026  1432 _ADCCH3VAL0	=	0x7026
                           007027  1433 _ADCCH3VAL1	=	0x7027
                           007026  1434 _ADCCH3VAL	=	0x7026
                           007028  1435 _ADCTUNE0	=	0x7028
                           007029  1436 _ADCTUNE1	=	0x7029
                           00702A  1437 _ADCTUNE2	=	0x702a
                           007010  1438 _DMA0ADDR0	=	0x7010
                           007011  1439 _DMA0ADDR1	=	0x7011
                           007010  1440 _DMA0ADDR	=	0x7010
                           007014  1441 _DMA0CONFIG	=	0x7014
                           007012  1442 _DMA1ADDR0	=	0x7012
                           007013  1443 _DMA1ADDR1	=	0x7013
                           007012  1444 _DMA1ADDR	=	0x7012
                           007015  1445 _DMA1CONFIG	=	0x7015
                           007070  1446 _FRCOSCCONFIG	=	0x7070
                           007071  1447 _FRCOSCCTRL	=	0x7071
                           007076  1448 _FRCOSCFREQ0	=	0x7076
                           007077  1449 _FRCOSCFREQ1	=	0x7077
                           007076  1450 _FRCOSCFREQ	=	0x7076
                           007072  1451 _FRCOSCKFILT0	=	0x7072
                           007073  1452 _FRCOSCKFILT1	=	0x7073
                           007072  1453 _FRCOSCKFILT	=	0x7072
                           007078  1454 _FRCOSCPER0	=	0x7078
                           007079  1455 _FRCOSCPER1	=	0x7079
                           007078  1456 _FRCOSCPER	=	0x7078
                           007074  1457 _FRCOSCREF0	=	0x7074
                           007075  1458 _FRCOSCREF1	=	0x7075
                           007074  1459 _FRCOSCREF	=	0x7074
                           007007  1460 _ANALOGA	=	0x7007
                           00700C  1461 _GPIOENABLE	=	0x700c
                           007003  1462 _EXTIRQ	=	0x7003
                           007000  1463 _INTCHGA	=	0x7000
                           007001  1464 _INTCHGB	=	0x7001
                           007002  1465 _INTCHGC	=	0x7002
                           007008  1466 _PALTA	=	0x7008
                           007009  1467 _PALTB	=	0x7009
                           00700A  1468 _PALTC	=	0x700a
                           007046  1469 _PALTRADIO	=	0x7046
                           007004  1470 _PINCHGA	=	0x7004
                           007005  1471 _PINCHGB	=	0x7005
                           007006  1472 _PINCHGC	=	0x7006
                           00700B  1473 _PINSEL	=	0x700b
                           007060  1474 _LPOSCCONFIG	=	0x7060
                           007066  1475 _LPOSCFREQ0	=	0x7066
                           007067  1476 _LPOSCFREQ1	=	0x7067
                           007066  1477 _LPOSCFREQ	=	0x7066
                           007062  1478 _LPOSCKFILT0	=	0x7062
                           007063  1479 _LPOSCKFILT1	=	0x7063
                           007062  1480 _LPOSCKFILT	=	0x7062
                           007068  1481 _LPOSCPER0	=	0x7068
                           007069  1482 _LPOSCPER1	=	0x7069
                           007068  1483 _LPOSCPER	=	0x7068
                           007064  1484 _LPOSCREF0	=	0x7064
                           007065  1485 _LPOSCREF1	=	0x7065
                           007064  1486 _LPOSCREF	=	0x7064
                           007054  1487 _LPXOSCGM	=	0x7054
                           007F01  1488 _MISCCTRL	=	0x7f01
                           007053  1489 _OSCCALIB	=	0x7053
                           007050  1490 _OSCFORCERUN	=	0x7050
                           007052  1491 _OSCREADY	=	0x7052
                           007051  1492 _OSCRUN	=	0x7051
                           007040  1493 _RADIOFDATAADDR0	=	0x7040
                           007041  1494 _RADIOFDATAADDR1	=	0x7041
                           007040  1495 _RADIOFDATAADDR	=	0x7040
                           007042  1496 _RADIOFSTATADDR0	=	0x7042
                           007043  1497 _RADIOFSTATADDR1	=	0x7043
                           007042  1498 _RADIOFSTATADDR	=	0x7042
                           007044  1499 _RADIOMUX	=	0x7044
                           007084  1500 _SCRATCH0	=	0x7084
                           007085  1501 _SCRATCH1	=	0x7085
                           007086  1502 _SCRATCH2	=	0x7086
                           007087  1503 _SCRATCH3	=	0x7087
                           007F00  1504 _SILICONREV	=	0x7f00
                           007F19  1505 _XTALAMPL	=	0x7f19
                           007F18  1506 _XTALOSC	=	0x7f18
                           007F1A  1507 _XTALREADY	=	0x7f1a
                           003F82  1508 _XDPTR0	=	0x3f82
                           003F84  1509 _XDPTR1	=	0x3f84
                           003FA8  1510 _XIE	=	0x3fa8
                           003FB8  1511 _XIP	=	0x3fb8
                           003F87  1512 _XPCON	=	0x3f87
                           003FCA  1513 _XADCCH0CONFIG	=	0x3fca
                           003FCB  1514 _XADCCH1CONFIG	=	0x3fcb
                           003FD2  1515 _XADCCH2CONFIG	=	0x3fd2
                           003FD3  1516 _XADCCH3CONFIG	=	0x3fd3
                           003FD1  1517 _XADCCLKSRC	=	0x3fd1
                           003FC9  1518 _XADCCONV	=	0x3fc9
                           003FE1  1519 _XANALOGCOMP	=	0x3fe1
                           003FC6  1520 _XCLKCON	=	0x3fc6
                           003FC7  1521 _XCLKSTAT	=	0x3fc7
                           003F97  1522 _XCODECONFIG	=	0x3f97
                           003FE3  1523 _XDBGLNKBUF	=	0x3fe3
                           003FE2  1524 _XDBGLNKSTAT	=	0x3fe2
                           003F89  1525 _XDIRA	=	0x3f89
                           003F8A  1526 _XDIRB	=	0x3f8a
                           003F8B  1527 _XDIRC	=	0x3f8b
                           003F8E  1528 _XDIRR	=	0x3f8e
                           003FC8  1529 _XPINA	=	0x3fc8
                           003FE8  1530 _XPINB	=	0x3fe8
                           003FF8  1531 _XPINC	=	0x3ff8
                           003F8D  1532 _XPINR	=	0x3f8d
                           003F80  1533 _XPORTA	=	0x3f80
                           003F88  1534 _XPORTB	=	0x3f88
                           003F90  1535 _XPORTC	=	0x3f90
                           003F8C  1536 _XPORTR	=	0x3f8c
                           003FCE  1537 _XIC0CAPT0	=	0x3fce
                           003FCF  1538 _XIC0CAPT1	=	0x3fcf
                           003FCE  1539 _XIC0CAPT	=	0x3fce
                           003FCC  1540 _XIC0MODE	=	0x3fcc
                           003FCD  1541 _XIC0STATUS	=	0x3fcd
                           003FD6  1542 _XIC1CAPT0	=	0x3fd6
                           003FD7  1543 _XIC1CAPT1	=	0x3fd7
                           003FD6  1544 _XIC1CAPT	=	0x3fd6
                           003FD4  1545 _XIC1MODE	=	0x3fd4
                           003FD5  1546 _XIC1STATUS	=	0x3fd5
                           003F92  1547 _XNVADDR0	=	0x3f92
                           003F93  1548 _XNVADDR1	=	0x3f93
                           003F92  1549 _XNVADDR	=	0x3f92
                           003F94  1550 _XNVDATA0	=	0x3f94
                           003F95  1551 _XNVDATA1	=	0x3f95
                           003F94  1552 _XNVDATA	=	0x3f94
                           003F96  1553 _XNVKEY	=	0x3f96
                           003F91  1554 _XNVSTATUS	=	0x3f91
                           003FBC  1555 _XOC0COMP0	=	0x3fbc
                           003FBD  1556 _XOC0COMP1	=	0x3fbd
                           003FBC  1557 _XOC0COMP	=	0x3fbc
                           003FB9  1558 _XOC0MODE	=	0x3fb9
                           003FBA  1559 _XOC0PIN	=	0x3fba
                           003FBB  1560 _XOC0STATUS	=	0x3fbb
                           003FC4  1561 _XOC1COMP0	=	0x3fc4
                           003FC5  1562 _XOC1COMP1	=	0x3fc5
                           003FC4  1563 _XOC1COMP	=	0x3fc4
                           003FC1  1564 _XOC1MODE	=	0x3fc1
                           003FC2  1565 _XOC1PIN	=	0x3fc2
                           003FC3  1566 _XOC1STATUS	=	0x3fc3
                           003FB1  1567 _XRADIOACC	=	0x3fb1
                           003FB3  1568 _XRADIOADDR0	=	0x3fb3
                           003FB2  1569 _XRADIOADDR1	=	0x3fb2
                           003FB7  1570 _XRADIODATA0	=	0x3fb7
                           003FB6  1571 _XRADIODATA1	=	0x3fb6
                           003FB5  1572 _XRADIODATA2	=	0x3fb5
                           003FB4  1573 _XRADIODATA3	=	0x3fb4
                           003FBE  1574 _XRADIOSTAT0	=	0x3fbe
                           003FBF  1575 _XRADIOSTAT1	=	0x3fbf
                           003FBE  1576 _XRADIOSTAT	=	0x3fbe
                           003FDF  1577 _XSPCLKSRC	=	0x3fdf
                           003FDC  1578 _XSPMODE	=	0x3fdc
                           003FDE  1579 _XSPSHREG	=	0x3fde
                           003FDD  1580 _XSPSTATUS	=	0x3fdd
                           003F9A  1581 _XT0CLKSRC	=	0x3f9a
                           003F9C  1582 _XT0CNT0	=	0x3f9c
                           003F9D  1583 _XT0CNT1	=	0x3f9d
                           003F9C  1584 _XT0CNT	=	0x3f9c
                           003F99  1585 _XT0MODE	=	0x3f99
                           003F9E  1586 _XT0PERIOD0	=	0x3f9e
                           003F9F  1587 _XT0PERIOD1	=	0x3f9f
                           003F9E  1588 _XT0PERIOD	=	0x3f9e
                           003F9B  1589 _XT0STATUS	=	0x3f9b
                           003FA2  1590 _XT1CLKSRC	=	0x3fa2
                           003FA4  1591 _XT1CNT0	=	0x3fa4
                           003FA5  1592 _XT1CNT1	=	0x3fa5
                           003FA4  1593 _XT1CNT	=	0x3fa4
                           003FA1  1594 _XT1MODE	=	0x3fa1
                           003FA6  1595 _XT1PERIOD0	=	0x3fa6
                           003FA7  1596 _XT1PERIOD1	=	0x3fa7
                           003FA6  1597 _XT1PERIOD	=	0x3fa6
                           003FA3  1598 _XT1STATUS	=	0x3fa3
                           003FAA  1599 _XT2CLKSRC	=	0x3faa
                           003FAC  1600 _XT2CNT0	=	0x3fac
                           003FAD  1601 _XT2CNT1	=	0x3fad
                           003FAC  1602 _XT2CNT	=	0x3fac
                           003FA9  1603 _XT2MODE	=	0x3fa9
                           003FAE  1604 _XT2PERIOD0	=	0x3fae
                           003FAF  1605 _XT2PERIOD1	=	0x3faf
                           003FAE  1606 _XT2PERIOD	=	0x3fae
                           003FAB  1607 _XT2STATUS	=	0x3fab
                           003FE4  1608 _XU0CTRL	=	0x3fe4
                           003FE7  1609 _XU0MODE	=	0x3fe7
                           003FE6  1610 _XU0SHREG	=	0x3fe6
                           003FE5  1611 _XU0STATUS	=	0x3fe5
                           003FEC  1612 _XU1CTRL	=	0x3fec
                           003FEF  1613 _XU1MODE	=	0x3fef
                           003FEE  1614 _XU1SHREG	=	0x3fee
                           003FED  1615 _XU1STATUS	=	0x3fed
                           003FDA  1616 _XWDTCFG	=	0x3fda
                           003FDB  1617 _XWDTRESET	=	0x3fdb
                           003FF1  1618 _XWTCFGA	=	0x3ff1
                           003FF9  1619 _XWTCFGB	=	0x3ff9
                           003FF2  1620 _XWTCNTA0	=	0x3ff2
                           003FF3  1621 _XWTCNTA1	=	0x3ff3
                           003FF2  1622 _XWTCNTA	=	0x3ff2
                           003FFA  1623 _XWTCNTB0	=	0x3ffa
                           003FFB  1624 _XWTCNTB1	=	0x3ffb
                           003FFA  1625 _XWTCNTB	=	0x3ffa
                           003FEB  1626 _XWTCNTR1	=	0x3feb
                           003FF4  1627 _XWTEVTA0	=	0x3ff4
                           003FF5  1628 _XWTEVTA1	=	0x3ff5
                           003FF4  1629 _XWTEVTA	=	0x3ff4
                           003FF6  1630 _XWTEVTB0	=	0x3ff6
                           003FF7  1631 _XWTEVTB1	=	0x3ff7
                           003FF6  1632 _XWTEVTB	=	0x3ff6
                           003FFC  1633 _XWTEVTC0	=	0x3ffc
                           003FFD  1634 _XWTEVTC1	=	0x3ffd
                           003FFC  1635 _XWTEVTC	=	0x3ffc
                           003FFE  1636 _XWTEVTD0	=	0x3ffe
                           003FFF  1637 _XWTEVTD1	=	0x3fff
                           003FFE  1638 _XWTEVTD	=	0x3ffe
                           003FE9  1639 _XWTIRQEN	=	0x3fe9
                           003FEA  1640 _XWTSTAT	=	0x3fea
                           004114  1641 _AX5043_AFSKCTRL	=	0x4114
                           004113  1642 _AX5043_AFSKMARK0	=	0x4113
                           004112  1643 _AX5043_AFSKMARK1	=	0x4112
                           004111  1644 _AX5043_AFSKSPACE0	=	0x4111
                           004110  1645 _AX5043_AFSKSPACE1	=	0x4110
                           004043  1646 _AX5043_AGCCOUNTER	=	0x4043
                           004115  1647 _AX5043_AMPLFILTER	=	0x4115
                           004189  1648 _AX5043_BBOFFSCAP	=	0x4189
                           004188  1649 _AX5043_BBTUNE	=	0x4188
                           004041  1650 _AX5043_BGNDRSSI	=	0x4041
                           00422E  1651 _AX5043_BGNDRSSIGAIN	=	0x422e
                           00422F  1652 _AX5043_BGNDRSSITHR	=	0x422f
                           004017  1653 _AX5043_CRCINIT0	=	0x4017
                           004016  1654 _AX5043_CRCINIT1	=	0x4016
                           004015  1655 _AX5043_CRCINIT2	=	0x4015
                           004014  1656 _AX5043_CRCINIT3	=	0x4014
                           004332  1657 _AX5043_DACCONFIG	=	0x4332
                           004331  1658 _AX5043_DACVALUE0	=	0x4331
                           004330  1659 _AX5043_DACVALUE1	=	0x4330
                           004102  1660 _AX5043_DECIMATION	=	0x4102
                           004042  1661 _AX5043_DIVERSITY	=	0x4042
                           004011  1662 _AX5043_ENCODING	=	0x4011
                           004018  1663 _AX5043_FEC	=	0x4018
                           00401A  1664 _AX5043_FECSTATUS	=	0x401a
                           004019  1665 _AX5043_FECSYNC	=	0x4019
                           00402B  1666 _AX5043_FIFOCOUNT0	=	0x402b
                           00402A  1667 _AX5043_FIFOCOUNT1	=	0x402a
                           004029  1668 _AX5043_FIFODATA	=	0x4029
                           00402D  1669 _AX5043_FIFOFREE0	=	0x402d
                           00402C  1670 _AX5043_FIFOFREE1	=	0x402c
                           004028  1671 _AX5043_FIFOSTAT	=	0x4028
                           00402F  1672 _AX5043_FIFOTHRESH0	=	0x402f
                           00402E  1673 _AX5043_FIFOTHRESH1	=	0x402e
                           004012  1674 _AX5043_FRAMING	=	0x4012
                           004037  1675 _AX5043_FREQA0	=	0x4037
                           004036  1676 _AX5043_FREQA1	=	0x4036
                           004035  1677 _AX5043_FREQA2	=	0x4035
                           004034  1678 _AX5043_FREQA3	=	0x4034
                           00403F  1679 _AX5043_FREQB0	=	0x403f
                           00403E  1680 _AX5043_FREQB1	=	0x403e
                           00403D  1681 _AX5043_FREQB2	=	0x403d
                           00403C  1682 _AX5043_FREQB3	=	0x403c
                           004163  1683 _AX5043_FSKDEV0	=	0x4163
                           004162  1684 _AX5043_FSKDEV1	=	0x4162
                           004161  1685 _AX5043_FSKDEV2	=	0x4161
                           00410D  1686 _AX5043_FSKDMAX0	=	0x410d
                           00410C  1687 _AX5043_FSKDMAX1	=	0x410c
                           00410F  1688 _AX5043_FSKDMIN0	=	0x410f
                           00410E  1689 _AX5043_FSKDMIN1	=	0x410e
                           004309  1690 _AX5043_GPADC13VALUE0	=	0x4309
                           004308  1691 _AX5043_GPADC13VALUE1	=	0x4308
                           004300  1692 _AX5043_GPADCCTRL	=	0x4300
                           004301  1693 _AX5043_GPADCPERIOD	=	0x4301
                           004101  1694 _AX5043_IFFREQ0	=	0x4101
                           004100  1695 _AX5043_IFFREQ1	=	0x4100
                           00400B  1696 _AX5043_IRQINVERSION0	=	0x400b
                           00400A  1697 _AX5043_IRQINVERSION1	=	0x400a
                           004007  1698 _AX5043_IRQMASK0	=	0x4007
                           004006  1699 _AX5043_IRQMASK1	=	0x4006
                           00400D  1700 _AX5043_IRQREQUEST0	=	0x400d
                           00400C  1701 _AX5043_IRQREQUEST1	=	0x400c
                           004310  1702 _AX5043_LPOSCCONFIG	=	0x4310
                           004317  1703 _AX5043_LPOSCFREQ0	=	0x4317
                           004316  1704 _AX5043_LPOSCFREQ1	=	0x4316
                           004313  1705 _AX5043_LPOSCKFILT0	=	0x4313
                           004312  1706 _AX5043_LPOSCKFILT1	=	0x4312
                           004319  1707 _AX5043_LPOSCPER0	=	0x4319
                           004318  1708 _AX5043_LPOSCPER1	=	0x4318
                           004315  1709 _AX5043_LPOSCREF0	=	0x4315
                           004314  1710 _AX5043_LPOSCREF1	=	0x4314
                           004311  1711 _AX5043_LPOSCSTATUS	=	0x4311
                           004214  1712 _AX5043_MATCH0LEN	=	0x4214
                           004216  1713 _AX5043_MATCH0MAX	=	0x4216
                           004215  1714 _AX5043_MATCH0MIN	=	0x4215
                           004213  1715 _AX5043_MATCH0PAT0	=	0x4213
                           004212  1716 _AX5043_MATCH0PAT1	=	0x4212
                           004211  1717 _AX5043_MATCH0PAT2	=	0x4211
                           004210  1718 _AX5043_MATCH0PAT3	=	0x4210
                           00421C  1719 _AX5043_MATCH1LEN	=	0x421c
                           00421E  1720 _AX5043_MATCH1MAX	=	0x421e
                           00421D  1721 _AX5043_MATCH1MIN	=	0x421d
                           004219  1722 _AX5043_MATCH1PAT0	=	0x4219
                           004218  1723 _AX5043_MATCH1PAT1	=	0x4218
                           004108  1724 _AX5043_MAXDROFFSET0	=	0x4108
                           004107  1725 _AX5043_MAXDROFFSET1	=	0x4107
                           004106  1726 _AX5043_MAXDROFFSET2	=	0x4106
                           00410B  1727 _AX5043_MAXRFOFFSET0	=	0x410b
                           00410A  1728 _AX5043_MAXRFOFFSET1	=	0x410a
                           004109  1729 _AX5043_MAXRFOFFSET2	=	0x4109
                           004164  1730 _AX5043_MODCFGA	=	0x4164
                           004160  1731 _AX5043_MODCFGF	=	0x4160
                           004F5F  1732 _AX5043_MODCFGP	=	0x4f5f
                           004010  1733 _AX5043_MODULATION	=	0x4010
                           004025  1734 _AX5043_PINFUNCANTSEL	=	0x4025
                           004023  1735 _AX5043_PINFUNCDATA	=	0x4023
                           004022  1736 _AX5043_PINFUNCDCLK	=	0x4022
                           004024  1737 _AX5043_PINFUNCIRQ	=	0x4024
                           004026  1738 _AX5043_PINFUNCPWRAMP	=	0x4026
                           004021  1739 _AX5043_PINFUNCSYSCLK	=	0x4021
                           004020  1740 _AX5043_PINSTATE	=	0x4020
                           004233  1741 _AX5043_PKTACCEPTFLAGS	=	0x4233
                           004230  1742 _AX5043_PKTCHUNKSIZE	=	0x4230
                           004231  1743 _AX5043_PKTMISCFLAGS	=	0x4231
                           004232  1744 _AX5043_PKTSTOREFLAGS	=	0x4232
                           004031  1745 _AX5043_PLLCPI	=	0x4031
                           004039  1746 _AX5043_PLLCPIBOOST	=	0x4039
                           004182  1747 _AX5043_PLLLOCKDET	=	0x4182
                           004030  1748 _AX5043_PLLLOOP	=	0x4030
                           004038  1749 _AX5043_PLLLOOPBOOST	=	0x4038
                           004033  1750 _AX5043_PLLRANGINGA	=	0x4033
                           00403B  1751 _AX5043_PLLRANGINGB	=	0x403b
                           004183  1752 _AX5043_PLLRNGCLK	=	0x4183
                           004032  1753 _AX5043_PLLVCODIV	=	0x4032
                           004180  1754 _AX5043_PLLVCOI	=	0x4180
                           004181  1755 _AX5043_PLLVCOIR	=	0x4181
                           004F08  1756 _AX5043_POWCTRL1	=	0x4f08
                           004005  1757 _AX5043_POWIRQMASK	=	0x4005
                           004003  1758 _AX5043_POWSTAT	=	0x4003
                           004004  1759 _AX5043_POWSTICKYSTAT	=	0x4004
                           004027  1760 _AX5043_PWRAMP	=	0x4027
                           004002  1761 _AX5043_PWRMODE	=	0x4002
                           004009  1762 _AX5043_RADIOEVENTMASK0	=	0x4009
                           004008  1763 _AX5043_RADIOEVENTMASK1	=	0x4008
                           00400F  1764 _AX5043_RADIOEVENTREQ0	=	0x400f
                           00400E  1765 _AX5043_RADIOEVENTREQ1	=	0x400e
                           00401C  1766 _AX5043_RADIOSTATE	=	0x401c
                           004F0D  1767 _AX5043_REF	=	0x4f0d
                           004040  1768 _AX5043_RSSI	=	0x4040
                           00422D  1769 _AX5043_RSSIABSTHR	=	0x422d
                           00422C  1770 _AX5043_RSSIREFERENCE	=	0x422c
                           004105  1771 _AX5043_RXDATARATE0	=	0x4105
                           004104  1772 _AX5043_RXDATARATE1	=	0x4104
                           004103  1773 _AX5043_RXDATARATE2	=	0x4103
                           004001  1774 _AX5043_SCRATCH	=	0x4001
                           004000  1775 _AX5043_SILICONREVISION	=	0x4000
                           00405B  1776 _AX5043_TIMER0	=	0x405b
                           00405A  1777 _AX5043_TIMER1	=	0x405a
                           004059  1778 _AX5043_TIMER2	=	0x4059
                           004227  1779 _AX5043_TMGRXAGC	=	0x4227
                           004223  1780 _AX5043_TMGRXBOOST	=	0x4223
                           004226  1781 _AX5043_TMGRXCOARSEAGC	=	0x4226
                           004225  1782 _AX5043_TMGRXOFFSACQ	=	0x4225
                           004229  1783 _AX5043_TMGRXPREAMBLE1	=	0x4229
                           00422A  1784 _AX5043_TMGRXPREAMBLE2	=	0x422a
                           00422B  1785 _AX5043_TMGRXPREAMBLE3	=	0x422b
                           004228  1786 _AX5043_TMGRXRSSI	=	0x4228
                           004224  1787 _AX5043_TMGRXSETTLE	=	0x4224
                           004220  1788 _AX5043_TMGTXBOOST	=	0x4220
                           004221  1789 _AX5043_TMGTXSETTLE	=	0x4221
                           004055  1790 _AX5043_TRKAFSKDEMOD0	=	0x4055
                           004054  1791 _AX5043_TRKAFSKDEMOD1	=	0x4054
                           004049  1792 _AX5043_TRKAMPLITUDE0	=	0x4049
                           004048  1793 _AX5043_TRKAMPLITUDE1	=	0x4048
                           004047  1794 _AX5043_TRKDATARATE0	=	0x4047
                           004046  1795 _AX5043_TRKDATARATE1	=	0x4046
                           004045  1796 _AX5043_TRKDATARATE2	=	0x4045
                           004051  1797 _AX5043_TRKFREQ0	=	0x4051
                           004050  1798 _AX5043_TRKFREQ1	=	0x4050
                           004053  1799 _AX5043_TRKFSKDEMOD0	=	0x4053
                           004052  1800 _AX5043_TRKFSKDEMOD1	=	0x4052
                           00404B  1801 _AX5043_TRKPHASE0	=	0x404b
                           00404A  1802 _AX5043_TRKPHASE1	=	0x404a
                           00404F  1803 _AX5043_TRKRFFREQ0	=	0x404f
                           00404E  1804 _AX5043_TRKRFFREQ1	=	0x404e
                           00404D  1805 _AX5043_TRKRFFREQ2	=	0x404d
                           004169  1806 _AX5043_TXPWRCOEFFA0	=	0x4169
                           004168  1807 _AX5043_TXPWRCOEFFA1	=	0x4168
                           00416B  1808 _AX5043_TXPWRCOEFFB0	=	0x416b
                           00416A  1809 _AX5043_TXPWRCOEFFB1	=	0x416a
                           00416D  1810 _AX5043_TXPWRCOEFFC0	=	0x416d
                           00416C  1811 _AX5043_TXPWRCOEFFC1	=	0x416c
                           00416F  1812 _AX5043_TXPWRCOEFFD0	=	0x416f
                           00416E  1813 _AX5043_TXPWRCOEFFD1	=	0x416e
                           004171  1814 _AX5043_TXPWRCOEFFE0	=	0x4171
                           004170  1815 _AX5043_TXPWRCOEFFE1	=	0x4170
                           004167  1816 _AX5043_TXRATE0	=	0x4167
                           004166  1817 _AX5043_TXRATE1	=	0x4166
                           004165  1818 _AX5043_TXRATE2	=	0x4165
                           00406B  1819 _AX5043_WAKEUP0	=	0x406b
                           00406A  1820 _AX5043_WAKEUP1	=	0x406a
                           00406D  1821 _AX5043_WAKEUPFREQ0	=	0x406d
                           00406C  1822 _AX5043_WAKEUPFREQ1	=	0x406c
                           004069  1823 _AX5043_WAKEUPTIMER0	=	0x4069
                           004068  1824 _AX5043_WAKEUPTIMER1	=	0x4068
                           00406E  1825 _AX5043_WAKEUPXOEARLY	=	0x406e
                           004F11  1826 _AX5043_XTALAMPL	=	0x4f11
                           004184  1827 _AX5043_XTALCAP	=	0x4184
                           004F10  1828 _AX5043_XTALOSC	=	0x4f10
                           00401D  1829 _AX5043_XTALSTATUS	=	0x401d
                           004F00  1830 _AX5043_0xF00	=	0x4f00
                           004F0C  1831 _AX5043_0xF0C	=	0x4f0c
                           004F18  1832 _AX5043_0xF18	=	0x4f18
                           004F1C  1833 _AX5043_0xF1C	=	0x4f1c
                           004F21  1834 _AX5043_0xF21	=	0x4f21
                           004F22  1835 _AX5043_0xF22	=	0x4f22
                           004F23  1836 _AX5043_0xF23	=	0x4f23
                           004F26  1837 _AX5043_0xF26	=	0x4f26
                           004F30  1838 _AX5043_0xF30	=	0x4f30
                           004F31  1839 _AX5043_0xF31	=	0x4f31
                           004F32  1840 _AX5043_0xF32	=	0x4f32
                           004F33  1841 _AX5043_0xF33	=	0x4f33
                           004F34  1842 _AX5043_0xF34	=	0x4f34
                           004F35  1843 _AX5043_0xF35	=	0x4f35
                           004F44  1844 _AX5043_0xF44	=	0x4f44
                           004122  1845 _AX5043_AGCAHYST0	=	0x4122
                           004132  1846 _AX5043_AGCAHYST1	=	0x4132
                           004142  1847 _AX5043_AGCAHYST2	=	0x4142
                           004152  1848 _AX5043_AGCAHYST3	=	0x4152
                           004120  1849 _AX5043_AGCGAIN0	=	0x4120
                           004130  1850 _AX5043_AGCGAIN1	=	0x4130
                           004140  1851 _AX5043_AGCGAIN2	=	0x4140
                           004150  1852 _AX5043_AGCGAIN3	=	0x4150
                           004123  1853 _AX5043_AGCMINMAX0	=	0x4123
                           004133  1854 _AX5043_AGCMINMAX1	=	0x4133
                           004143  1855 _AX5043_AGCMINMAX2	=	0x4143
                           004153  1856 _AX5043_AGCMINMAX3	=	0x4153
                           004121  1857 _AX5043_AGCTARGET0	=	0x4121
                           004131  1858 _AX5043_AGCTARGET1	=	0x4131
                           004141  1859 _AX5043_AGCTARGET2	=	0x4141
                           004151  1860 _AX5043_AGCTARGET3	=	0x4151
                           00412B  1861 _AX5043_AMPLITUDEGAIN0	=	0x412b
                           00413B  1862 _AX5043_AMPLITUDEGAIN1	=	0x413b
                           00414B  1863 _AX5043_AMPLITUDEGAIN2	=	0x414b
                           00415B  1864 _AX5043_AMPLITUDEGAIN3	=	0x415b
                           00412F  1865 _AX5043_BBOFFSRES0	=	0x412f
                           00413F  1866 _AX5043_BBOFFSRES1	=	0x413f
                           00414F  1867 _AX5043_BBOFFSRES2	=	0x414f
                           00415F  1868 _AX5043_BBOFFSRES3	=	0x415f
                           004125  1869 _AX5043_DRGAIN0	=	0x4125
                           004135  1870 _AX5043_DRGAIN1	=	0x4135
                           004145  1871 _AX5043_DRGAIN2	=	0x4145
                           004155  1872 _AX5043_DRGAIN3	=	0x4155
                           00412E  1873 _AX5043_FOURFSK0	=	0x412e
                           00413E  1874 _AX5043_FOURFSK1	=	0x413e
                           00414E  1875 _AX5043_FOURFSK2	=	0x414e
                           00415E  1876 _AX5043_FOURFSK3	=	0x415e
                           00412D  1877 _AX5043_FREQDEV00	=	0x412d
                           00413D  1878 _AX5043_FREQDEV01	=	0x413d
                           00414D  1879 _AX5043_FREQDEV02	=	0x414d
                           00415D  1880 _AX5043_FREQDEV03	=	0x415d
                           00412C  1881 _AX5043_FREQDEV10	=	0x412c
                           00413C  1882 _AX5043_FREQDEV11	=	0x413c
                           00414C  1883 _AX5043_FREQDEV12	=	0x414c
                           00415C  1884 _AX5043_FREQDEV13	=	0x415c
                           004127  1885 _AX5043_FREQUENCYGAINA0	=	0x4127
                           004137  1886 _AX5043_FREQUENCYGAINA1	=	0x4137
                           004147  1887 _AX5043_FREQUENCYGAINA2	=	0x4147
                           004157  1888 _AX5043_FREQUENCYGAINA3	=	0x4157
                           004128  1889 _AX5043_FREQUENCYGAINB0	=	0x4128
                           004138  1890 _AX5043_FREQUENCYGAINB1	=	0x4138
                           004148  1891 _AX5043_FREQUENCYGAINB2	=	0x4148
                           004158  1892 _AX5043_FREQUENCYGAINB3	=	0x4158
                           004129  1893 _AX5043_FREQUENCYGAINC0	=	0x4129
                           004139  1894 _AX5043_FREQUENCYGAINC1	=	0x4139
                           004149  1895 _AX5043_FREQUENCYGAINC2	=	0x4149
                           004159  1896 _AX5043_FREQUENCYGAINC3	=	0x4159
                           00412A  1897 _AX5043_FREQUENCYGAIND0	=	0x412a
                           00413A  1898 _AX5043_FREQUENCYGAIND1	=	0x413a
                           00414A  1899 _AX5043_FREQUENCYGAIND2	=	0x414a
                           00415A  1900 _AX5043_FREQUENCYGAIND3	=	0x415a
                           004116  1901 _AX5043_FREQUENCYLEAK	=	0x4116
                           004126  1902 _AX5043_PHASEGAIN0	=	0x4126
                           004136  1903 _AX5043_PHASEGAIN1	=	0x4136
                           004146  1904 _AX5043_PHASEGAIN2	=	0x4146
                           004156  1905 _AX5043_PHASEGAIN3	=	0x4156
                           004207  1906 _AX5043_PKTADDR0	=	0x4207
                           004206  1907 _AX5043_PKTADDR1	=	0x4206
                           004205  1908 _AX5043_PKTADDR2	=	0x4205
                           004204  1909 _AX5043_PKTADDR3	=	0x4204
                           004200  1910 _AX5043_PKTADDRCFG	=	0x4200
                           00420B  1911 _AX5043_PKTADDRMASK0	=	0x420b
                           00420A  1912 _AX5043_PKTADDRMASK1	=	0x420a
                           004209  1913 _AX5043_PKTADDRMASK2	=	0x4209
                           004208  1914 _AX5043_PKTADDRMASK3	=	0x4208
                           004201  1915 _AX5043_PKTLENCFG	=	0x4201
                           004202  1916 _AX5043_PKTLENOFFSET	=	0x4202
                           004203  1917 _AX5043_PKTMAXLEN	=	0x4203
                           004118  1918 _AX5043_RXPARAMCURSET	=	0x4118
                           004117  1919 _AX5043_RXPARAMSETS	=	0x4117
                           004124  1920 _AX5043_TIMEGAIN0	=	0x4124
                           004134  1921 _AX5043_TIMEGAIN1	=	0x4134
                           004144  1922 _AX5043_TIMEGAIN2	=	0x4144
                           004154  1923 _AX5043_TIMEGAIN3	=	0x4154
                           005114  1924 _AX5043_AFSKCTRLNB	=	0x5114
                           005113  1925 _AX5043_AFSKMARK0NB	=	0x5113
                           005112  1926 _AX5043_AFSKMARK1NB	=	0x5112
                           005111  1927 _AX5043_AFSKSPACE0NB	=	0x5111
                           005110  1928 _AX5043_AFSKSPACE1NB	=	0x5110
                           005043  1929 _AX5043_AGCCOUNTERNB	=	0x5043
                           005115  1930 _AX5043_AMPLFILTERNB	=	0x5115
                           005189  1931 _AX5043_BBOFFSCAPNB	=	0x5189
                           005188  1932 _AX5043_BBTUNENB	=	0x5188
                           005041  1933 _AX5043_BGNDRSSINB	=	0x5041
                           00522E  1934 _AX5043_BGNDRSSIGAINNB	=	0x522e
                           00522F  1935 _AX5043_BGNDRSSITHRNB	=	0x522f
                           005017  1936 _AX5043_CRCINIT0NB	=	0x5017
                           005016  1937 _AX5043_CRCINIT1NB	=	0x5016
                           005015  1938 _AX5043_CRCINIT2NB	=	0x5015
                           005014  1939 _AX5043_CRCINIT3NB	=	0x5014
                           005332  1940 _AX5043_DACCONFIGNB	=	0x5332
                           005331  1941 _AX5043_DACVALUE0NB	=	0x5331
                           005330  1942 _AX5043_DACVALUE1NB	=	0x5330
                           005102  1943 _AX5043_DECIMATIONNB	=	0x5102
                           005042  1944 _AX5043_DIVERSITYNB	=	0x5042
                           005011  1945 _AX5043_ENCODINGNB	=	0x5011
                           005018  1946 _AX5043_FECNB	=	0x5018
                           00501A  1947 _AX5043_FECSTATUSNB	=	0x501a
                           005019  1948 _AX5043_FECSYNCNB	=	0x5019
                           00502B  1949 _AX5043_FIFOCOUNT0NB	=	0x502b
                           00502A  1950 _AX5043_FIFOCOUNT1NB	=	0x502a
                           005029  1951 _AX5043_FIFODATANB	=	0x5029
                           00502D  1952 _AX5043_FIFOFREE0NB	=	0x502d
                           00502C  1953 _AX5043_FIFOFREE1NB	=	0x502c
                           005028  1954 _AX5043_FIFOSTATNB	=	0x5028
                           00502F  1955 _AX5043_FIFOTHRESH0NB	=	0x502f
                           00502E  1956 _AX5043_FIFOTHRESH1NB	=	0x502e
                           005012  1957 _AX5043_FRAMINGNB	=	0x5012
                           005037  1958 _AX5043_FREQA0NB	=	0x5037
                           005036  1959 _AX5043_FREQA1NB	=	0x5036
                           005035  1960 _AX5043_FREQA2NB	=	0x5035
                           005034  1961 _AX5043_FREQA3NB	=	0x5034
                           00503F  1962 _AX5043_FREQB0NB	=	0x503f
                           00503E  1963 _AX5043_FREQB1NB	=	0x503e
                           00503D  1964 _AX5043_FREQB2NB	=	0x503d
                           00503C  1965 _AX5043_FREQB3NB	=	0x503c
                           005163  1966 _AX5043_FSKDEV0NB	=	0x5163
                           005162  1967 _AX5043_FSKDEV1NB	=	0x5162
                           005161  1968 _AX5043_FSKDEV2NB	=	0x5161
                           00510D  1969 _AX5043_FSKDMAX0NB	=	0x510d
                           00510C  1970 _AX5043_FSKDMAX1NB	=	0x510c
                           00510F  1971 _AX5043_FSKDMIN0NB	=	0x510f
                           00510E  1972 _AX5043_FSKDMIN1NB	=	0x510e
                           005309  1973 _AX5043_GPADC13VALUE0NB	=	0x5309
                           005308  1974 _AX5043_GPADC13VALUE1NB	=	0x5308
                           005300  1975 _AX5043_GPADCCTRLNB	=	0x5300
                           005301  1976 _AX5043_GPADCPERIODNB	=	0x5301
                           005101  1977 _AX5043_IFFREQ0NB	=	0x5101
                           005100  1978 _AX5043_IFFREQ1NB	=	0x5100
                           00500B  1979 _AX5043_IRQINVERSION0NB	=	0x500b
                           00500A  1980 _AX5043_IRQINVERSION1NB	=	0x500a
                           005007  1981 _AX5043_IRQMASK0NB	=	0x5007
                           005006  1982 _AX5043_IRQMASK1NB	=	0x5006
                           00500D  1983 _AX5043_IRQREQUEST0NB	=	0x500d
                           00500C  1984 _AX5043_IRQREQUEST1NB	=	0x500c
                           005310  1985 _AX5043_LPOSCCONFIGNB	=	0x5310
                           005317  1986 _AX5043_LPOSCFREQ0NB	=	0x5317
                           005316  1987 _AX5043_LPOSCFREQ1NB	=	0x5316
                           005313  1988 _AX5043_LPOSCKFILT0NB	=	0x5313
                           005312  1989 _AX5043_LPOSCKFILT1NB	=	0x5312
                           005319  1990 _AX5043_LPOSCPER0NB	=	0x5319
                           005318  1991 _AX5043_LPOSCPER1NB	=	0x5318
                           005315  1992 _AX5043_LPOSCREF0NB	=	0x5315
                           005314  1993 _AX5043_LPOSCREF1NB	=	0x5314
                           005311  1994 _AX5043_LPOSCSTATUSNB	=	0x5311
                           005214  1995 _AX5043_MATCH0LENNB	=	0x5214
                           005216  1996 _AX5043_MATCH0MAXNB	=	0x5216
                           005215  1997 _AX5043_MATCH0MINNB	=	0x5215
                           005213  1998 _AX5043_MATCH0PAT0NB	=	0x5213
                           005212  1999 _AX5043_MATCH0PAT1NB	=	0x5212
                           005211  2000 _AX5043_MATCH0PAT2NB	=	0x5211
                           005210  2001 _AX5043_MATCH0PAT3NB	=	0x5210
                           00521C  2002 _AX5043_MATCH1LENNB	=	0x521c
                           00521E  2003 _AX5043_MATCH1MAXNB	=	0x521e
                           00521D  2004 _AX5043_MATCH1MINNB	=	0x521d
                           005219  2005 _AX5043_MATCH1PAT0NB	=	0x5219
                           005218  2006 _AX5043_MATCH1PAT1NB	=	0x5218
                           005108  2007 _AX5043_MAXDROFFSET0NB	=	0x5108
                           005107  2008 _AX5043_MAXDROFFSET1NB	=	0x5107
                           005106  2009 _AX5043_MAXDROFFSET2NB	=	0x5106
                           00510B  2010 _AX5043_MAXRFOFFSET0NB	=	0x510b
                           00510A  2011 _AX5043_MAXRFOFFSET1NB	=	0x510a
                           005109  2012 _AX5043_MAXRFOFFSET2NB	=	0x5109
                           005164  2013 _AX5043_MODCFGANB	=	0x5164
                           005160  2014 _AX5043_MODCFGFNB	=	0x5160
                           005F5F  2015 _AX5043_MODCFGPNB	=	0x5f5f
                           005010  2016 _AX5043_MODULATIONNB	=	0x5010
                           005025  2017 _AX5043_PINFUNCANTSELNB	=	0x5025
                           005023  2018 _AX5043_PINFUNCDATANB	=	0x5023
                           005022  2019 _AX5043_PINFUNCDCLKNB	=	0x5022
                           005024  2020 _AX5043_PINFUNCIRQNB	=	0x5024
                           005026  2021 _AX5043_PINFUNCPWRAMPNB	=	0x5026
                           005021  2022 _AX5043_PINFUNCSYSCLKNB	=	0x5021
                           005020  2023 _AX5043_PINSTATENB	=	0x5020
                           005233  2024 _AX5043_PKTACCEPTFLAGSNB	=	0x5233
                           005230  2025 _AX5043_PKTCHUNKSIZENB	=	0x5230
                           005231  2026 _AX5043_PKTMISCFLAGSNB	=	0x5231
                           005232  2027 _AX5043_PKTSTOREFLAGSNB	=	0x5232
                           005031  2028 _AX5043_PLLCPINB	=	0x5031
                           005039  2029 _AX5043_PLLCPIBOOSTNB	=	0x5039
                           005182  2030 _AX5043_PLLLOCKDETNB	=	0x5182
                           005030  2031 _AX5043_PLLLOOPNB	=	0x5030
                           005038  2032 _AX5043_PLLLOOPBOOSTNB	=	0x5038
                           005033  2033 _AX5043_PLLRANGINGANB	=	0x5033
                           00503B  2034 _AX5043_PLLRANGINGBNB	=	0x503b
                           005183  2035 _AX5043_PLLRNGCLKNB	=	0x5183
                           005032  2036 _AX5043_PLLVCODIVNB	=	0x5032
                           005180  2037 _AX5043_PLLVCOINB	=	0x5180
                           005181  2038 _AX5043_PLLVCOIRNB	=	0x5181
                           005F08  2039 _AX5043_POWCTRL1NB	=	0x5f08
                           005005  2040 _AX5043_POWIRQMASKNB	=	0x5005
                           005003  2041 _AX5043_POWSTATNB	=	0x5003
                           005004  2042 _AX5043_POWSTICKYSTATNB	=	0x5004
                           005027  2043 _AX5043_PWRAMPNB	=	0x5027
                           005002  2044 _AX5043_PWRMODENB	=	0x5002
                           005009  2045 _AX5043_RADIOEVENTMASK0NB	=	0x5009
                           005008  2046 _AX5043_RADIOEVENTMASK1NB	=	0x5008
                           00500F  2047 _AX5043_RADIOEVENTREQ0NB	=	0x500f
                           00500E  2048 _AX5043_RADIOEVENTREQ1NB	=	0x500e
                           00501C  2049 _AX5043_RADIOSTATENB	=	0x501c
                           005F0D  2050 _AX5043_REFNB	=	0x5f0d
                           005040  2051 _AX5043_RSSINB	=	0x5040
                           00522D  2052 _AX5043_RSSIABSTHRNB	=	0x522d
                           00522C  2053 _AX5043_RSSIREFERENCENB	=	0x522c
                           005105  2054 _AX5043_RXDATARATE0NB	=	0x5105
                           005104  2055 _AX5043_RXDATARATE1NB	=	0x5104
                           005103  2056 _AX5043_RXDATARATE2NB	=	0x5103
                           005001  2057 _AX5043_SCRATCHNB	=	0x5001
                           005000  2058 _AX5043_SILICONREVISIONNB	=	0x5000
                           00505B  2059 _AX5043_TIMER0NB	=	0x505b
                           00505A  2060 _AX5043_TIMER1NB	=	0x505a
                           005059  2061 _AX5043_TIMER2NB	=	0x5059
                           005227  2062 _AX5043_TMGRXAGCNB	=	0x5227
                           005223  2063 _AX5043_TMGRXBOOSTNB	=	0x5223
                           005226  2064 _AX5043_TMGRXCOARSEAGCNB	=	0x5226
                           005225  2065 _AX5043_TMGRXOFFSACQNB	=	0x5225
                           005229  2066 _AX5043_TMGRXPREAMBLE1NB	=	0x5229
                           00522A  2067 _AX5043_TMGRXPREAMBLE2NB	=	0x522a
                           00522B  2068 _AX5043_TMGRXPREAMBLE3NB	=	0x522b
                           005228  2069 _AX5043_TMGRXRSSINB	=	0x5228
                           005224  2070 _AX5043_TMGRXSETTLENB	=	0x5224
                           005220  2071 _AX5043_TMGTXBOOSTNB	=	0x5220
                           005221  2072 _AX5043_TMGTXSETTLENB	=	0x5221
                           005055  2073 _AX5043_TRKAFSKDEMOD0NB	=	0x5055
                           005054  2074 _AX5043_TRKAFSKDEMOD1NB	=	0x5054
                           005049  2075 _AX5043_TRKAMPLITUDE0NB	=	0x5049
                           005048  2076 _AX5043_TRKAMPLITUDE1NB	=	0x5048
                           005047  2077 _AX5043_TRKDATARATE0NB	=	0x5047
                           005046  2078 _AX5043_TRKDATARATE1NB	=	0x5046
                           005045  2079 _AX5043_TRKDATARATE2NB	=	0x5045
                           005051  2080 _AX5043_TRKFREQ0NB	=	0x5051
                           005050  2081 _AX5043_TRKFREQ1NB	=	0x5050
                           005053  2082 _AX5043_TRKFSKDEMOD0NB	=	0x5053
                           005052  2083 _AX5043_TRKFSKDEMOD1NB	=	0x5052
                           00504B  2084 _AX5043_TRKPHASE0NB	=	0x504b
                           00504A  2085 _AX5043_TRKPHASE1NB	=	0x504a
                           00504F  2086 _AX5043_TRKRFFREQ0NB	=	0x504f
                           00504E  2087 _AX5043_TRKRFFREQ1NB	=	0x504e
                           00504D  2088 _AX5043_TRKRFFREQ2NB	=	0x504d
                           005169  2089 _AX5043_TXPWRCOEFFA0NB	=	0x5169
                           005168  2090 _AX5043_TXPWRCOEFFA1NB	=	0x5168
                           00516B  2091 _AX5043_TXPWRCOEFFB0NB	=	0x516b
                           00516A  2092 _AX5043_TXPWRCOEFFB1NB	=	0x516a
                           00516D  2093 _AX5043_TXPWRCOEFFC0NB	=	0x516d
                           00516C  2094 _AX5043_TXPWRCOEFFC1NB	=	0x516c
                           00516F  2095 _AX5043_TXPWRCOEFFD0NB	=	0x516f
                           00516E  2096 _AX5043_TXPWRCOEFFD1NB	=	0x516e
                           005171  2097 _AX5043_TXPWRCOEFFE0NB	=	0x5171
                           005170  2098 _AX5043_TXPWRCOEFFE1NB	=	0x5170
                           005167  2099 _AX5043_TXRATE0NB	=	0x5167
                           005166  2100 _AX5043_TXRATE1NB	=	0x5166
                           005165  2101 _AX5043_TXRATE2NB	=	0x5165
                           00506B  2102 _AX5043_WAKEUP0NB	=	0x506b
                           00506A  2103 _AX5043_WAKEUP1NB	=	0x506a
                           00506D  2104 _AX5043_WAKEUPFREQ0NB	=	0x506d
                           00506C  2105 _AX5043_WAKEUPFREQ1NB	=	0x506c
                           005069  2106 _AX5043_WAKEUPTIMER0NB	=	0x5069
                           005068  2107 _AX5043_WAKEUPTIMER1NB	=	0x5068
                           00506E  2108 _AX5043_WAKEUPXOEARLYNB	=	0x506e
                           005F11  2109 _AX5043_XTALAMPLNB	=	0x5f11
                           005184  2110 _AX5043_XTALCAPNB	=	0x5184
                           005F10  2111 _AX5043_XTALOSCNB	=	0x5f10
                           00501D  2112 _AX5043_XTALSTATUSNB	=	0x501d
                           005F00  2113 _AX5043_0xF00NB	=	0x5f00
                           005F0C  2114 _AX5043_0xF0CNB	=	0x5f0c
                           005F18  2115 _AX5043_0xF18NB	=	0x5f18
                           005F1C  2116 _AX5043_0xF1CNB	=	0x5f1c
                           005F21  2117 _AX5043_0xF21NB	=	0x5f21
                           005F22  2118 _AX5043_0xF22NB	=	0x5f22
                           005F23  2119 _AX5043_0xF23NB	=	0x5f23
                           005F26  2120 _AX5043_0xF26NB	=	0x5f26
                           005F30  2121 _AX5043_0xF30NB	=	0x5f30
                           005F31  2122 _AX5043_0xF31NB	=	0x5f31
                           005F32  2123 _AX5043_0xF32NB	=	0x5f32
                           005F33  2124 _AX5043_0xF33NB	=	0x5f33
                           005F34  2125 _AX5043_0xF34NB	=	0x5f34
                           005F35  2126 _AX5043_0xF35NB	=	0x5f35
                           005F44  2127 _AX5043_0xF44NB	=	0x5f44
                           005122  2128 _AX5043_AGCAHYST0NB	=	0x5122
                           005132  2129 _AX5043_AGCAHYST1NB	=	0x5132
                           005142  2130 _AX5043_AGCAHYST2NB	=	0x5142
                           005152  2131 _AX5043_AGCAHYST3NB	=	0x5152
                           005120  2132 _AX5043_AGCGAIN0NB	=	0x5120
                           005130  2133 _AX5043_AGCGAIN1NB	=	0x5130
                           005140  2134 _AX5043_AGCGAIN2NB	=	0x5140
                           005150  2135 _AX5043_AGCGAIN3NB	=	0x5150
                           005123  2136 _AX5043_AGCMINMAX0NB	=	0x5123
                           005133  2137 _AX5043_AGCMINMAX1NB	=	0x5133
                           005143  2138 _AX5043_AGCMINMAX2NB	=	0x5143
                           005153  2139 _AX5043_AGCMINMAX3NB	=	0x5153
                           005121  2140 _AX5043_AGCTARGET0NB	=	0x5121
                           005131  2141 _AX5043_AGCTARGET1NB	=	0x5131
                           005141  2142 _AX5043_AGCTARGET2NB	=	0x5141
                           005151  2143 _AX5043_AGCTARGET3NB	=	0x5151
                           00512B  2144 _AX5043_AMPLITUDEGAIN0NB	=	0x512b
                           00513B  2145 _AX5043_AMPLITUDEGAIN1NB	=	0x513b
                           00514B  2146 _AX5043_AMPLITUDEGAIN2NB	=	0x514b
                           00515B  2147 _AX5043_AMPLITUDEGAIN3NB	=	0x515b
                           00512F  2148 _AX5043_BBOFFSRES0NB	=	0x512f
                           00513F  2149 _AX5043_BBOFFSRES1NB	=	0x513f
                           00514F  2150 _AX5043_BBOFFSRES2NB	=	0x514f
                           00515F  2151 _AX5043_BBOFFSRES3NB	=	0x515f
                           005125  2152 _AX5043_DRGAIN0NB	=	0x5125
                           005135  2153 _AX5043_DRGAIN1NB	=	0x5135
                           005145  2154 _AX5043_DRGAIN2NB	=	0x5145
                           005155  2155 _AX5043_DRGAIN3NB	=	0x5155
                           00512E  2156 _AX5043_FOURFSK0NB	=	0x512e
                           00513E  2157 _AX5043_FOURFSK1NB	=	0x513e
                           00514E  2158 _AX5043_FOURFSK2NB	=	0x514e
                           00515E  2159 _AX5043_FOURFSK3NB	=	0x515e
                           00512D  2160 _AX5043_FREQDEV00NB	=	0x512d
                           00513D  2161 _AX5043_FREQDEV01NB	=	0x513d
                           00514D  2162 _AX5043_FREQDEV02NB	=	0x514d
                           00515D  2163 _AX5043_FREQDEV03NB	=	0x515d
                           00512C  2164 _AX5043_FREQDEV10NB	=	0x512c
                           00513C  2165 _AX5043_FREQDEV11NB	=	0x513c
                           00514C  2166 _AX5043_FREQDEV12NB	=	0x514c
                           00515C  2167 _AX5043_FREQDEV13NB	=	0x515c
                           005127  2168 _AX5043_FREQUENCYGAINA0NB	=	0x5127
                           005137  2169 _AX5043_FREQUENCYGAINA1NB	=	0x5137
                           005147  2170 _AX5043_FREQUENCYGAINA2NB	=	0x5147
                           005157  2171 _AX5043_FREQUENCYGAINA3NB	=	0x5157
                           005128  2172 _AX5043_FREQUENCYGAINB0NB	=	0x5128
                           005138  2173 _AX5043_FREQUENCYGAINB1NB	=	0x5138
                           005148  2174 _AX5043_FREQUENCYGAINB2NB	=	0x5148
                           005158  2175 _AX5043_FREQUENCYGAINB3NB	=	0x5158
                           005129  2176 _AX5043_FREQUENCYGAINC0NB	=	0x5129
                           005139  2177 _AX5043_FREQUENCYGAINC1NB	=	0x5139
                           005149  2178 _AX5043_FREQUENCYGAINC2NB	=	0x5149
                           005159  2179 _AX5043_FREQUENCYGAINC3NB	=	0x5159
                           00512A  2180 _AX5043_FREQUENCYGAIND0NB	=	0x512a
                           00513A  2181 _AX5043_FREQUENCYGAIND1NB	=	0x513a
                           00514A  2182 _AX5043_FREQUENCYGAIND2NB	=	0x514a
                           00515A  2183 _AX5043_FREQUENCYGAIND3NB	=	0x515a
                           005116  2184 _AX5043_FREQUENCYLEAKNB	=	0x5116
                           005126  2185 _AX5043_PHASEGAIN0NB	=	0x5126
                           005136  2186 _AX5043_PHASEGAIN1NB	=	0x5136
                           005146  2187 _AX5043_PHASEGAIN2NB	=	0x5146
                           005156  2188 _AX5043_PHASEGAIN3NB	=	0x5156
                           005207  2189 _AX5043_PKTADDR0NB	=	0x5207
                           005206  2190 _AX5043_PKTADDR1NB	=	0x5206
                           005205  2191 _AX5043_PKTADDR2NB	=	0x5205
                           005204  2192 _AX5043_PKTADDR3NB	=	0x5204
                           005200  2193 _AX5043_PKTADDRCFGNB	=	0x5200
                           00520B  2194 _AX5043_PKTADDRMASK0NB	=	0x520b
                           00520A  2195 _AX5043_PKTADDRMASK1NB	=	0x520a
                           005209  2196 _AX5043_PKTADDRMASK2NB	=	0x5209
                           005208  2197 _AX5043_PKTADDRMASK3NB	=	0x5208
                           005201  2198 _AX5043_PKTLENCFGNB	=	0x5201
                           005202  2199 _AX5043_PKTLENOFFSETNB	=	0x5202
                           005203  2200 _AX5043_PKTMAXLENNB	=	0x5203
                           005118  2201 _AX5043_RXPARAMCURSETNB	=	0x5118
                           005117  2202 _AX5043_RXPARAMSETSNB	=	0x5117
                           005124  2203 _AX5043_TIMEGAIN0NB	=	0x5124
                           005134  2204 _AX5043_TIMEGAIN1NB	=	0x5134
                           005144  2205 _AX5043_TIMEGAIN2NB	=	0x5144
                           005154  2206 _AX5043_TIMEGAIN3NB	=	0x5154
      0003A8                       2207 _UBLOX_GPS_FletcherChecksum8_PARM_2:
      0003A8                       2208 	.ds 3
      0003AB                       2209 _UBLOX_GPS_FletcherChecksum8_PARM_3:
      0003AB                       2210 	.ds 3
      0003AE                       2211 _UBLOX_GPS_FletcherChecksum8_PARM_4:
      0003AE                       2212 	.ds 2
      0003B0                       2213 _UBLOX_GPS_FletcherChecksum8_buffer_1_191:
      0003B0                       2214 	.ds 3
      0003B3                       2215 _UBLOX_GPS_SendCommand_WaitACK_PARM_2:
      0003B3                       2216 	.ds 1
      0003B4                       2217 _UBLOX_GPS_SendCommand_WaitACK_PARM_3:
      0003B4                       2218 	.ds 2
      0003B6                       2219 _UBLOX_GPS_SendCommand_WaitACK_PARM_4:
      0003B6                       2220 	.ds 3
      0003B9                       2221 _UBLOX_GPS_SendCommand_WaitACK_PARM_5:
      0003B9                       2222 	.ds 1
      0003BA                       2223 _UBLOX_GPS_SendCommand_WaitACK_PARM_6:
      0003BA                       2224 	.ds 3
      0003BD                       2225 _UBLOX_GPS_SendCommand_WaitACK_msg_class_1_194:
      0003BD                       2226 	.ds 1
      0003BE                       2227 _UBLOX_GPS_SendCommand_WaitACK_RetVal_1_195:
      0003BE                       2228 	.ds 1
      0003BF                       2229 _UBLOX_GPS_SendCommand_WaitACK_CK_A_1_195:
      0003BF                       2230 	.ds 1
      0003C0                       2231 _UBLOX_GPS_SendCommand_WaitACK_CK_B_1_195:
      0003C0                       2232 	.ds 1
                                   2233 ;--------------------------------------------------------
                                   2234 ; absolute external ram data
                                   2235 ;--------------------------------------------------------
                                   2236 	.area XABS    (ABS,XDATA)
                                   2237 ;--------------------------------------------------------
                                   2238 ; external initialized ram data
                                   2239 ;--------------------------------------------------------
                                   2240 	.area XISEG   (XDATA)
                                   2241 	.area HOME    (CODE)
                                   2242 	.area GSINIT0 (CODE)
                                   2243 	.area GSINIT1 (CODE)
                                   2244 	.area GSINIT2 (CODE)
                                   2245 	.area GSINIT3 (CODE)
                                   2246 	.area GSINIT4 (CODE)
                                   2247 	.area GSINIT5 (CODE)
                                   2248 	.area GSINIT  (CODE)
                                   2249 	.area GSFINAL (CODE)
                                   2250 	.area CSEG    (CODE)
                                   2251 ;--------------------------------------------------------
                                   2252 ; global & static initialisations
                                   2253 ;--------------------------------------------------------
                                   2254 	.area HOME    (CODE)
                                   2255 	.area GSINIT  (CODE)
                                   2256 	.area GSFINAL (CODE)
                                   2257 	.area GSINIT  (CODE)
                                   2258 ;--------------------------------------------------------
                                   2259 ; Home
                                   2260 ;--------------------------------------------------------
                                   2261 	.area HOME    (CODE)
                                   2262 	.area HOME    (CODE)
                                   2263 ;--------------------------------------------------------
                                   2264 ; code
                                   2265 ;--------------------------------------------------------
                                   2266 	.area CSEG    (CODE)
                                   2267 ;------------------------------------------------------------
                                   2268 ;Allocation info for local variables in function 'UBLOX_GPS_PortInit'
                                   2269 ;------------------------------------------------------------
                                   2270 ;	..\src\peripherals\UBLOX.c:30: __reentrantb void UBLOX_GPS_PortInit(void) __reentrant
                                   2271 ;	-----------------------------------------
                                   2272 ;	 function UBLOX_GPS_PortInit
                                   2273 ;	-----------------------------------------
      005624                       2274 _UBLOX_GPS_PortInit:
                           000007  2275 	ar7 = 0x07
                           000006  2276 	ar6 = 0x06
                           000005  2277 	ar5 = 0x05
                           000004  2278 	ar4 = 0x04
                           000003  2279 	ar3 = 0x03
                           000002  2280 	ar2 = 0x02
                           000001  2281 	ar1 = 0x01
                           000000  2282 	ar0 = 0x00
                                   2283 ;	..\src\peripherals\UBLOX.c:32: uart_timer1_baud(CLKSRC_SYSCLK, 9600, 20000000UL);
      005624 E4               [12] 2284 	clr	a
      005625 C0 E0            [24] 2285 	push	acc
      005627 74 2D            [12] 2286 	mov	a,#0x2d
      005629 C0 E0            [24] 2287 	push	acc
      00562B 74 31            [12] 2288 	mov	a,#0x31
      00562D C0 E0            [24] 2289 	push	acc
      00562F 74 01            [12] 2290 	mov	a,#0x01
      005631 C0 E0            [24] 2291 	push	acc
      005633 03               [12] 2292 	rr	a
      005634 C0 E0            [24] 2293 	push	acc
      005636 74 25            [12] 2294 	mov	a,#0x25
      005638 C0 E0            [24] 2295 	push	acc
      00563A E4               [12] 2296 	clr	a
      00563B C0 E0            [24] 2297 	push	acc
      00563D C0 E0            [24] 2298 	push	acc
      00563F 75 82 06         [24] 2299 	mov	dpl,#0x06
      005642 12 60 A6         [24] 2300 	lcall	_uart_timer1_baud
      005645 E5 81            [12] 2301 	mov	a,sp
      005647 24 F8            [12] 2302 	add	a,#0xf8
      005649 F5 81            [12] 2303 	mov	sp,a
                                   2304 ;	..\src\peripherals\UBLOX.c:33: uart1_init(1, 8, 1);
      00564B 90 03 C1         [24] 2305 	mov	dptr,#_uart1_init_PARM_2
      00564E 74 08            [12] 2306 	mov	a,#0x08
      005650 F0               [24] 2307 	movx	@dptr,a
      005651 90 03 C2         [24] 2308 	mov	dptr,#_uart1_init_PARM_3
      005654 74 01            [12] 2309 	mov	a,#0x01
      005656 F0               [24] 2310 	movx	@dptr,a
      005657 75 82 01         [24] 2311 	mov	dpl,#0x01
      00565A 02 5B 08         [24] 2312 	ljmp	_uart1_init
                                   2313 ;------------------------------------------------------------
                                   2314 ;Allocation info for local variables in function 'UBLOX_GPS_FletcherChecksum8'
                                   2315 ;------------------------------------------------------------
                                   2316 ;CK_A                      Allocated with name '_UBLOX_GPS_FletcherChecksum8_PARM_2'
                                   2317 ;CK_B                      Allocated with name '_UBLOX_GPS_FletcherChecksum8_PARM_3'
                                   2318 ;length                    Allocated with name '_UBLOX_GPS_FletcherChecksum8_PARM_4'
                                   2319 ;buffer                    Allocated with name '_UBLOX_GPS_FletcherChecksum8_buffer_1_191'
                                   2320 ;i                         Allocated with name '_UBLOX_GPS_FletcherChecksum8_i_1_192'
                                   2321 ;sloc0                     Allocated with name '_UBLOX_GPS_FletcherChecksum8_sloc0_1_0'
                                   2322 ;sloc1                     Allocated with name '_UBLOX_GPS_FletcherChecksum8_sloc1_1_0'
                                   2323 ;sloc2                     Allocated with name '_UBLOX_GPS_FletcherChecksum8_sloc2_1_0'
                                   2324 ;sloc3                     Allocated with name '_UBLOX_GPS_FletcherChecksum8_sloc3_1_0'
                                   2325 ;------------------------------------------------------------
                                   2326 ;	..\src\peripherals\UBLOX.c:52: void UBLOX_GPS_FletcherChecksum8 ( uint8_t * buffer, uint8_t *CK_A, uint8_t *CK_B, uint16_t length)
                                   2327 ;	-----------------------------------------
                                   2328 ;	 function UBLOX_GPS_FletcherChecksum8
                                   2329 ;	-----------------------------------------
      00565D                       2330 _UBLOX_GPS_FletcherChecksum8:
      00565D AF F0            [24] 2331 	mov	r7,b
      00565F AE 83            [24] 2332 	mov	r6,dph
      005661 E5 82            [12] 2333 	mov	a,dpl
      005663 90 03 B0         [24] 2334 	mov	dptr,#_UBLOX_GPS_FletcherChecksum8_buffer_1_191
      005666 F0               [24] 2335 	movx	@dptr,a
      005667 EE               [12] 2336 	mov	a,r6
      005668 A3               [24] 2337 	inc	dptr
      005669 F0               [24] 2338 	movx	@dptr,a
      00566A EF               [12] 2339 	mov	a,r7
      00566B A3               [24] 2340 	inc	dptr
      00566C F0               [24] 2341 	movx	@dptr,a
                                   2342 ;	..\src\peripherals\UBLOX.c:55: *CK_A = 0;
      00566D 90 03 A8         [24] 2343 	mov	dptr,#_UBLOX_GPS_FletcherChecksum8_PARM_2
      005670 E0               [24] 2344 	movx	a,@dptr
      005671 FD               [12] 2345 	mov	r5,a
      005672 A3               [24] 2346 	inc	dptr
      005673 E0               [24] 2347 	movx	a,@dptr
      005674 FE               [12] 2348 	mov	r6,a
      005675 A3               [24] 2349 	inc	dptr
      005676 E0               [24] 2350 	movx	a,@dptr
      005677 FF               [12] 2351 	mov	r7,a
      005678 8D 82            [24] 2352 	mov	dpl,r5
      00567A 8E 83            [24] 2353 	mov	dph,r6
      00567C 8F F0            [24] 2354 	mov	b,r7
      00567E E4               [12] 2355 	clr	a
      00567F 12 6A 1E         [24] 2356 	lcall	__gptrput
                                   2357 ;	..\src\peripherals\UBLOX.c:56: *CK_B = 0;
      005682 90 03 AB         [24] 2358 	mov	dptr,#_UBLOX_GPS_FletcherChecksum8_PARM_3
      005685 E0               [24] 2359 	movx	a,@dptr
      005686 F5 48            [12] 2360 	mov	_UBLOX_GPS_FletcherChecksum8_sloc3_1_0,a
      005688 A3               [24] 2361 	inc	dptr
      005689 E0               [24] 2362 	movx	a,@dptr
      00568A F5 49            [12] 2363 	mov	(_UBLOX_GPS_FletcherChecksum8_sloc3_1_0 + 1),a
      00568C A3               [24] 2364 	inc	dptr
      00568D E0               [24] 2365 	movx	a,@dptr
      00568E F5 4A            [12] 2366 	mov	(_UBLOX_GPS_FletcherChecksum8_sloc3_1_0 + 2),a
      005690 85 48 82         [24] 2367 	mov	dpl,_UBLOX_GPS_FletcherChecksum8_sloc3_1_0
      005693 85 49 83         [24] 2368 	mov	dph,(_UBLOX_GPS_FletcherChecksum8_sloc3_1_0 + 1)
      005696 85 4A F0         [24] 2369 	mov	b,(_UBLOX_GPS_FletcherChecksum8_sloc3_1_0 + 2)
      005699 E4               [12] 2370 	clr	a
      00569A 12 6A 1E         [24] 2371 	lcall	__gptrput
                                   2372 ;	..\src\peripherals\UBLOX.c:57: for (i = 0; i < length; i++)
      00569D 90 03 B0         [24] 2373 	mov	dptr,#_UBLOX_GPS_FletcherChecksum8_buffer_1_191
      0056A0 E0               [24] 2374 	movx	a,@dptr
      0056A1 F5 43            [12] 2375 	mov	_UBLOX_GPS_FletcherChecksum8_sloc0_1_0,a
      0056A3 A3               [24] 2376 	inc	dptr
      0056A4 E0               [24] 2377 	movx	a,@dptr
      0056A5 F5 44            [12] 2378 	mov	(_UBLOX_GPS_FletcherChecksum8_sloc0_1_0 + 1),a
      0056A7 A3               [24] 2379 	inc	dptr
      0056A8 E0               [24] 2380 	movx	a,@dptr
      0056A9 F5 45            [12] 2381 	mov	(_UBLOX_GPS_FletcherChecksum8_sloc0_1_0 + 2),a
      0056AB 90 03 AE         [24] 2382 	mov	dptr,#_UBLOX_GPS_FletcherChecksum8_PARM_4
      0056AE E0               [24] 2383 	movx	a,@dptr
      0056AF F8               [12] 2384 	mov	r0,a
      0056B0 A3               [24] 2385 	inc	dptr
      0056B1 E0               [24] 2386 	movx	a,@dptr
      0056B2 F9               [12] 2387 	mov	r1,a
      0056B3 75 46 00         [24] 2388 	mov	_UBLOX_GPS_FletcherChecksum8_sloc1_1_0,#0x00
      0056B6                       2389 00103$:
      0056B6 AB 46            [24] 2390 	mov	r3,_UBLOX_GPS_FletcherChecksum8_sloc1_1_0
      0056B8 7C 00            [12] 2391 	mov	r4,#0x00
      0056BA C3               [12] 2392 	clr	c
      0056BB EB               [12] 2393 	mov	a,r3
      0056BC 98               [12] 2394 	subb	a,r0
      0056BD EC               [12] 2395 	mov	a,r4
      0056BE 99               [12] 2396 	subb	a,r1
      0056BF 50 51            [24] 2397 	jnc	00101$
                                   2398 ;	..\src\peripherals\UBLOX.c:59: *CK_A += buffer[i];
      0056C1 C0 00            [24] 2399 	push	ar0
      0056C3 C0 01            [24] 2400 	push	ar1
      0056C5 8D 82            [24] 2401 	mov	dpl,r5
      0056C7 8E 83            [24] 2402 	mov	dph,r6
      0056C9 8F F0            [24] 2403 	mov	b,r7
      0056CB 12 79 65         [24] 2404 	lcall	__gptrget
      0056CE F5 47            [12] 2405 	mov	_UBLOX_GPS_FletcherChecksum8_sloc2_1_0,a
      0056D0 E5 46            [12] 2406 	mov	a,_UBLOX_GPS_FletcherChecksum8_sloc1_1_0
      0056D2 25 43            [12] 2407 	add	a,_UBLOX_GPS_FletcherChecksum8_sloc0_1_0
      0056D4 F8               [12] 2408 	mov	r0,a
      0056D5 E4               [12] 2409 	clr	a
      0056D6 35 44            [12] 2410 	addc	a,(_UBLOX_GPS_FletcherChecksum8_sloc0_1_0 + 1)
      0056D8 F9               [12] 2411 	mov	r1,a
      0056D9 AC 45            [24] 2412 	mov	r4,(_UBLOX_GPS_FletcherChecksum8_sloc0_1_0 + 2)
      0056DB 88 82            [24] 2413 	mov	dpl,r0
      0056DD 89 83            [24] 2414 	mov	dph,r1
      0056DF 8C F0            [24] 2415 	mov	b,r4
      0056E1 12 79 65         [24] 2416 	lcall	__gptrget
      0056E4 25 47            [12] 2417 	add	a,_UBLOX_GPS_FletcherChecksum8_sloc2_1_0
      0056E6 F8               [12] 2418 	mov	r0,a
      0056E7 8D 82            [24] 2419 	mov	dpl,r5
      0056E9 8E 83            [24] 2420 	mov	dph,r6
      0056EB 8F F0            [24] 2421 	mov	b,r7
      0056ED 12 6A 1E         [24] 2422 	lcall	__gptrput
                                   2423 ;	..\src\peripherals\UBLOX.c:61: *CK_B += *CK_A;
      0056F0 85 48 82         [24] 2424 	mov	dpl,_UBLOX_GPS_FletcherChecksum8_sloc3_1_0
      0056F3 85 49 83         [24] 2425 	mov	dph,(_UBLOX_GPS_FletcherChecksum8_sloc3_1_0 + 1)
      0056F6 85 4A F0         [24] 2426 	mov	b,(_UBLOX_GPS_FletcherChecksum8_sloc3_1_0 + 2)
      0056F9 12 79 65         [24] 2427 	lcall	__gptrget
      0056FC FC               [12] 2428 	mov	r4,a
      0056FD 28               [12] 2429 	add	a,r0
      0056FE 85 48 82         [24] 2430 	mov	dpl,_UBLOX_GPS_FletcherChecksum8_sloc3_1_0
      005701 85 49 83         [24] 2431 	mov	dph,(_UBLOX_GPS_FletcherChecksum8_sloc3_1_0 + 1)
      005704 85 4A F0         [24] 2432 	mov	b,(_UBLOX_GPS_FletcherChecksum8_sloc3_1_0 + 2)
      005707 12 6A 1E         [24] 2433 	lcall	__gptrput
                                   2434 ;	..\src\peripherals\UBLOX.c:57: for (i = 0; i < length; i++)
      00570A 05 46            [12] 2435 	inc	_UBLOX_GPS_FletcherChecksum8_sloc1_1_0
      00570C D0 01            [24] 2436 	pop	ar1
      00570E D0 00            [24] 2437 	pop	ar0
      005710 80 A4            [24] 2438 	sjmp	00103$
      005712                       2439 00101$:
                                   2440 ;	..\src\peripherals\UBLOX.c:63: return;
      005712 22               [24] 2441 	ret
                                   2442 ;------------------------------------------------------------
                                   2443 ;Allocation info for local variables in function 'UBLOX_GPS_SendCommand_WaitACK'
                                   2444 ;------------------------------------------------------------
                                   2445 ;sloc0                     Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0'
                                   2446 ;sloc1                     Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0'
                                   2447 ;sloc2                     Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0'
                                   2448 ;sloc3                     Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0'
                                   2449 ;sloc4                     Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0'
                                   2450 ;msg_id                    Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_PARM_2'
                                   2451 ;msg_length                Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_PARM_3'
                                   2452 ;payload                   Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_PARM_4'
                                   2453 ;AnsOrAck                  Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_PARM_5'
                                   2454 ;RxLength                  Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_PARM_6'
                                   2455 ;msg_class                 Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_msg_class_1_194'
                                   2456 ;i                         Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_i_1_195'
                                   2457 ;k                         Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_k_1_195'
                                   2458 ;RetVal                    Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_RetVal_1_195'
                                   2459 ;buffer_aux                Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_195'
                                   2460 ;rx_buffer                 Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_195'
                                   2461 ;CK_A                      Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_CK_A_1_195'
                                   2462 ;CK_B                      Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_CK_B_1_195'
                                   2463 ;------------------------------------------------------------
                                   2464 ;	..\src\peripherals\UBLOX.c:132: uint8_t UBLOX_GPS_SendCommand_WaitACK(uint8_t msg_class, uint8_t msg_id, uint16_t msg_length,uint8_t *payload,uint8_t AnsOrAck,uint16_t *RxLength)
                                   2465 ;	-----------------------------------------
                                   2466 ;	 function UBLOX_GPS_SendCommand_WaitACK
                                   2467 ;	-----------------------------------------
      005713                       2468 _UBLOX_GPS_SendCommand_WaitACK:
      005713 E5 82            [12] 2469 	mov	a,dpl
      005715 90 03 BD         [24] 2470 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_msg_class_1_194
      005718 F0               [24] 2471 	movx	@dptr,a
                                   2472 ;	..\src\peripherals\UBLOX.c:135: uint8_t RetVal = 0;
      005719 90 03 BE         [24] 2473 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_RetVal_1_195
      00571C E4               [12] 2474 	clr	a
      00571D F0               [24] 2475 	movx	@dptr,a
                                   2476 ;	..\src\peripherals\UBLOX.c:139: buffer_aux =(uint8_t *) malloc((msg_length)+10);
      00571E 90 03 B4         [24] 2477 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_3
      005721 E0               [24] 2478 	movx	a,@dptr
      005722 FE               [12] 2479 	mov	r6,a
      005723 A3               [24] 2480 	inc	dptr
      005724 E0               [24] 2481 	movx	a,@dptr
      005725 FF               [12] 2482 	mov	r7,a
      005726 74 0A            [12] 2483 	mov	a,#0x0a
      005728 2E               [12] 2484 	add	a,r6
      005729 FC               [12] 2485 	mov	r4,a
      00572A E4               [12] 2486 	clr	a
      00572B 3F               [12] 2487 	addc	a,r7
      00572C FD               [12] 2488 	mov	r5,a
      00572D 8C 82            [24] 2489 	mov	dpl,r4
      00572F 8D 83            [24] 2490 	mov	dph,r5
      005731 C0 07            [24] 2491 	push	ar7
      005733 C0 06            [24] 2492 	push	ar6
      005735 12 6B C4         [24] 2493 	lcall	_malloc
      005738 AC 82            [24] 2494 	mov	r4,dpl
      00573A AD 83            [24] 2495 	mov	r5,dph
      00573C 8C 30            [24] 2496 	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0,r4
      00573E 8D 31            [24] 2497 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0 + 1),r5
                                   2498 ;	..\src\peripherals\UBLOX.c:140: rx_buffer =(uint8_t *) malloc(30);
      005740 90 00 1E         [24] 2499 	mov	dptr,#0x001e
      005743 12 6B C4         [24] 2500 	lcall	_malloc
      005746 AA 82            [24] 2501 	mov	r2,dpl
      005748 AB 83            [24] 2502 	mov	r3,dph
      00574A D0 06            [24] 2503 	pop	ar6
      00574C D0 07            [24] 2504 	pop	ar7
                                   2505 ;	..\src\peripherals\UBLOX.c:141: for(i = 0; i<40;i++)
      00574E 79 00            [12] 2506 	mov	r1,#0x00
      005750                       2507 00125$:
                                   2508 ;	..\src\peripherals\UBLOX.c:143: *(rx_buffer+i)=0;
      005750 E9               [12] 2509 	mov	a,r1
      005751 2A               [12] 2510 	add	a,r2
      005752 F5 82            [12] 2511 	mov	dpl,a
      005754 E4               [12] 2512 	clr	a
      005755 3B               [12] 2513 	addc	a,r3
      005756 F5 83            [12] 2514 	mov	dph,a
      005758 E4               [12] 2515 	clr	a
      005759 F0               [24] 2516 	movx	@dptr,a
                                   2517 ;	..\src\peripherals\UBLOX.c:141: for(i = 0; i<40;i++)
      00575A 09               [12] 2518 	inc	r1
      00575B B9 28 00         [24] 2519 	cjne	r1,#0x28,00186$
      00575E                       2520 00186$:
      00575E 40 F0            [24] 2521 	jc	00125$
                                   2522 ;	..\src\peripherals\UBLOX.c:146: *buffer_aux = msg_class;
      005760 C0 02            [24] 2523 	push	ar2
      005762 C0 03            [24] 2524 	push	ar3
      005764 90 03 BD         [24] 2525 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_msg_class_1_194
      005767 E0               [24] 2526 	movx	a,@dptr
      005768 F9               [12] 2527 	mov	r1,a
      005769 85 30 82         [24] 2528 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0
      00576C 85 31 83         [24] 2529 	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0 + 1)
      00576F F0               [24] 2530 	movx	@dptr,a
                                   2531 ;	..\src\peripherals\UBLOX.c:147: buffer_aux++;
      005770 74 01            [12] 2532 	mov	a,#0x01
      005772 25 30            [12] 2533 	add	a,_UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0
      005774 F8               [12] 2534 	mov	r0,a
      005775 E4               [12] 2535 	clr	a
      005776 35 31            [12] 2536 	addc	a,(_UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0 + 1)
      005778 FB               [12] 2537 	mov	r3,a
                                   2538 ;	..\src\peripherals\UBLOX.c:148: *buffer_aux = msg_id;
      005779 90 03 B3         [24] 2539 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_2
      00577C E0               [24] 2540 	movx	a,@dptr
      00577D F5 29            [12] 2541 	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0,a
      00577F 88 82            [24] 2542 	mov	dpl,r0
      005781 8B 83            [24] 2543 	mov	dph,r3
      005783 F0               [24] 2544 	movx	@dptr,a
                                   2545 ;	..\src\peripherals\UBLOX.c:149: buffer_aux++;
      005784 85 30 82         [24] 2546 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0
      005787 85 31 83         [24] 2547 	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0 + 1)
      00578A A3               [24] 2548 	inc	dptr
      00578B A3               [24] 2549 	inc	dptr
                                   2550 ;	..\src\peripherals\UBLOX.c:150: *buffer_aux = msg_length;
      00578C 8E 03            [24] 2551 	mov	ar3,r6
      00578E EB               [12] 2552 	mov	a,r3
      00578F F0               [24] 2553 	movx	@dptr,a
                                   2554 ;	..\src\peripherals\UBLOX.c:151: buffer_aux +=2;
      005790 74 04            [12] 2555 	mov	a,#0x04
      005792 25 30            [12] 2556 	add	a,_UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0
      005794 FA               [12] 2557 	mov	r2,a
      005795 E4               [12] 2558 	clr	a
      005796 35 31            [12] 2559 	addc	a,(_UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0 + 1)
                                   2560 ;	..\src\peripherals\UBLOX.c:152: memcpy(buffer_aux,payload,msg_length);
      005798 F8               [12] 2561 	mov	r0,a
      005799 7B 00            [12] 2562 	mov	r3,#0x00
      00579B 90 03 B6         [24] 2563 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_4
      00579E E0               [24] 2564 	movx	a,@dptr
      00579F F5 2A            [12] 2565 	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0,a
      0057A1 A3               [24] 2566 	inc	dptr
      0057A2 E0               [24] 2567 	movx	a,@dptr
      0057A3 F5 2B            [12] 2568 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0 + 1),a
      0057A5 A3               [24] 2569 	inc	dptr
      0057A6 E0               [24] 2570 	movx	a,@dptr
      0057A7 F5 2C            [12] 2571 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0 + 2),a
      0057A9 90 03 E2         [24] 2572 	mov	dptr,#_memcpy_PARM_2
      0057AC E5 2A            [12] 2573 	mov	a,_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0
      0057AE F0               [24] 2574 	movx	@dptr,a
      0057AF E5 2B            [12] 2575 	mov	a,(_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0 + 1)
      0057B1 A3               [24] 2576 	inc	dptr
      0057B2 F0               [24] 2577 	movx	@dptr,a
      0057B3 E5 2C            [12] 2578 	mov	a,(_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0 + 2)
      0057B5 A3               [24] 2579 	inc	dptr
      0057B6 F0               [24] 2580 	movx	@dptr,a
      0057B7 90 03 E5         [24] 2581 	mov	dptr,#_memcpy_PARM_3
      0057BA EE               [12] 2582 	mov	a,r6
      0057BB F0               [24] 2583 	movx	@dptr,a
      0057BC EF               [12] 2584 	mov	a,r7
      0057BD A3               [24] 2585 	inc	dptr
      0057BE F0               [24] 2586 	movx	@dptr,a
      0057BF 8A 82            [24] 2587 	mov	dpl,r2
      0057C1 88 83            [24] 2588 	mov	dph,r0
      0057C3 8B F0            [24] 2589 	mov	b,r3
      0057C5 C0 07            [24] 2590 	push	ar7
      0057C7 C0 06            [24] 2591 	push	ar6
      0057C9 C0 03            [24] 2592 	push	ar3
      0057CB C0 02            [24] 2593 	push	ar2
      0057CD C0 01            [24] 2594 	push	ar1
      0057CF 12 65 97         [24] 2595 	lcall	_memcpy
      0057D2 D0 01            [24] 2596 	pop	ar1
      0057D4 D0 02            [24] 2597 	pop	ar2
      0057D6 D0 03            [24] 2598 	pop	ar3
      0057D8 D0 06            [24] 2599 	pop	ar6
      0057DA D0 07            [24] 2600 	pop	ar7
                                   2601 ;	..\src\peripherals\UBLOX.c:154: UBLOX_GPS_FletcherChecksum8(buffer_aux,&CK_A,&CK_B,(msg_length)+4);
      0057DC AA 30            [24] 2602 	mov	r2,_UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0
      0057DE AB 31            [24] 2603 	mov	r3,(_UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0 + 1)
      0057E0 8A 2D            [24] 2604 	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0,r2
      0057E2 8B 2E            [24] 2605 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0 + 1),r3
      0057E4 75 2F 00         [24] 2606 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0 + 2),#0x00
      0057E7 74 04            [12] 2607 	mov	a,#0x04
      0057E9 2E               [12] 2608 	add	a,r6
      0057EA FA               [12] 2609 	mov	r2,a
      0057EB E4               [12] 2610 	clr	a
      0057EC 3F               [12] 2611 	addc	a,r7
      0057ED FB               [12] 2612 	mov	r3,a
      0057EE 90 03 A8         [24] 2613 	mov	dptr,#_UBLOX_GPS_FletcherChecksum8_PARM_2
      0057F1 74 BF            [12] 2614 	mov	a,#_UBLOX_GPS_SendCommand_WaitACK_CK_A_1_195
      0057F3 F0               [24] 2615 	movx	@dptr,a
      0057F4 74 03            [12] 2616 	mov	a,#(_UBLOX_GPS_SendCommand_WaitACK_CK_A_1_195 >> 8)
      0057F6 A3               [24] 2617 	inc	dptr
      0057F7 F0               [24] 2618 	movx	@dptr,a
      0057F8 E4               [12] 2619 	clr	a
      0057F9 A3               [24] 2620 	inc	dptr
      0057FA F0               [24] 2621 	movx	@dptr,a
      0057FB 90 03 AB         [24] 2622 	mov	dptr,#_UBLOX_GPS_FletcherChecksum8_PARM_3
      0057FE 74 C0            [12] 2623 	mov	a,#_UBLOX_GPS_SendCommand_WaitACK_CK_B_1_195
      005800 F0               [24] 2624 	movx	@dptr,a
      005801 74 03            [12] 2625 	mov	a,#(_UBLOX_GPS_SendCommand_WaitACK_CK_B_1_195 >> 8)
      005803 A3               [24] 2626 	inc	dptr
      005804 F0               [24] 2627 	movx	@dptr,a
      005805 E4               [12] 2628 	clr	a
      005806 A3               [24] 2629 	inc	dptr
      005807 F0               [24] 2630 	movx	@dptr,a
      005808 90 03 AE         [24] 2631 	mov	dptr,#_UBLOX_GPS_FletcherChecksum8_PARM_4
      00580B EA               [12] 2632 	mov	a,r2
      00580C F0               [24] 2633 	movx	@dptr,a
      00580D EB               [12] 2634 	mov	a,r3
      00580E A3               [24] 2635 	inc	dptr
      00580F F0               [24] 2636 	movx	@dptr,a
      005810 85 2D 82         [24] 2637 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0
      005813 85 2E 83         [24] 2638 	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0 + 1)
      005816 85 2F F0         [24] 2639 	mov	b,(_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0 + 2)
      005819 C0 07            [24] 2640 	push	ar7
      00581B C0 06            [24] 2641 	push	ar6
      00581D C0 03            [24] 2642 	push	ar3
      00581F C0 02            [24] 2643 	push	ar2
      005821 C0 01            [24] 2644 	push	ar1
      005823 12 56 5D         [24] 2645 	lcall	_UBLOX_GPS_FletcherChecksum8
      005826 D0 01            [24] 2646 	pop	ar1
      005828 D0 02            [24] 2647 	pop	ar2
      00582A D0 03            [24] 2648 	pop	ar3
      00582C D0 06            [24] 2649 	pop	ar6
      00582E D0 07            [24] 2650 	pop	ar7
                                   2651 ;	..\src\peripherals\UBLOX.c:156: uart1_tx(UBX_HEADER1_VAL);
      005830 75 82 B5         [24] 2652 	mov	dpl,#0xb5
      005833 12 6B 49         [24] 2653 	lcall	_uart1_tx
                                   2654 ;	..\src\peripherals\UBLOX.c:157: uart1_tx(UBX_HEADER2_VAL);
      005836 75 82 62         [24] 2655 	mov	dpl,#0x62
      005839 12 6B 49         [24] 2656 	lcall	_uart1_tx
                                   2657 ;	..\src\peripherals\UBLOX.c:158: uart1_tx(msg_class);
      00583C 89 82            [24] 2658 	mov	dpl,r1
      00583E 12 6B 49         [24] 2659 	lcall	_uart1_tx
                                   2660 ;	..\src\peripherals\UBLOX.c:159: uart1_tx(msg_id);
      005841 85 29 82         [24] 2661 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0
      005844 12 6B 49         [24] 2662 	lcall	_uart1_tx
                                   2663 ;	..\src\peripherals\UBLOX.c:160: uart1_tx((uint8_t)(msg_length & 0x00FF));//LSB
      005847 8E 02            [24] 2664 	mov	ar2,r6
      005849 8A 82            [24] 2665 	mov	dpl,r2
      00584B 12 6B 49         [24] 2666 	lcall	_uart1_tx
                                   2667 ;	..\src\peripherals\UBLOX.c:161: uart1_tx((uint8_t)((msg_length & 0xFF00)>>8));//MSB
      00584E 8F 03            [24] 2668 	mov	ar3,r7
      005850 8B 02            [24] 2669 	mov	ar2,r3
      005852 8A 82            [24] 2670 	mov	dpl,r2
      005854 12 6B 49         [24] 2671 	lcall	_uart1_tx
                                   2672 ;	..\src\peripherals\UBLOX.c:164: for(i = 0;i<msg_length;i++)
      005857 75 2D 00         [24] 2673 	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0,#0x00
                                   2674 ;	..\src\peripherals\UBLOX.c:216: return RetVal;
      00585A D0 03            [24] 2675 	pop	ar3
      00585C D0 02            [24] 2676 	pop	ar2
                                   2677 ;	..\src\peripherals\UBLOX.c:164: for(i = 0;i<msg_length;i++)
      00585E                       2678 00128$:
      00585E C0 01            [24] 2679 	push	ar1
      005860 A8 2D            [24] 2680 	mov	r0,_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0
      005862 79 00            [12] 2681 	mov	r1,#0x00
      005864 C3               [12] 2682 	clr	c
      005865 E8               [12] 2683 	mov	a,r0
      005866 9E               [12] 2684 	subb	a,r6
      005867 E9               [12] 2685 	mov	a,r1
      005868 9F               [12] 2686 	subb	a,r7
      005869 D0 01            [24] 2687 	pop	ar1
      00586B 50 22            [24] 2688 	jnc	00102$
                                   2689 ;	..\src\peripherals\UBLOX.c:166: uart1_tx(*(payload+i));
      00586D C0 01            [24] 2690 	push	ar1
      00586F E5 2D            [12] 2691 	mov	a,_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0
      005871 25 2A            [12] 2692 	add	a,_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0
      005873 F8               [12] 2693 	mov	r0,a
      005874 E4               [12] 2694 	clr	a
      005875 35 2B            [12] 2695 	addc	a,(_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0 + 1)
      005877 F9               [12] 2696 	mov	r1,a
      005878 AD 2C            [24] 2697 	mov	r5,(_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0 + 2)
      00587A 88 82            [24] 2698 	mov	dpl,r0
      00587C 89 83            [24] 2699 	mov	dph,r1
      00587E 8D F0            [24] 2700 	mov	b,r5
      005880 12 79 65         [24] 2701 	lcall	__gptrget
      005883 F8               [12] 2702 	mov	r0,a
      005884 F5 82            [12] 2703 	mov	dpl,a
      005886 12 6B 49         [24] 2704 	lcall	_uart1_tx
                                   2705 ;	..\src\peripherals\UBLOX.c:164: for(i = 0;i<msg_length;i++)
      005889 05 2D            [12] 2706 	inc	_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0
      00588B D0 01            [24] 2707 	pop	ar1
      00588D 80 CF            [24] 2708 	sjmp	00128$
      00588F                       2709 00102$:
                                   2710 ;	..\src\peripherals\UBLOX.c:169: uart1_tx(CK_A);
      00588F 90 03 BF         [24] 2711 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_CK_A_1_195
      005892 E0               [24] 2712 	movx	a,@dptr
      005893 F5 82            [12] 2713 	mov	dpl,a
      005895 12 6B 49         [24] 2714 	lcall	_uart1_tx
                                   2715 ;	..\src\peripherals\UBLOX.c:170: uart1_tx(CK_B);
      005898 90 03 C0         [24] 2716 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_CK_B_1_195
      00589B E0               [24] 2717 	movx	a,@dptr
      00589C FF               [12] 2718 	mov	r7,a
      00589D F5 82            [12] 2719 	mov	dpl,a
      00589F 12 6B 49         [24] 2720 	lcall	_uart1_tx
                                   2721 ;	..\src\peripherals\UBLOX.c:173: do{
      0058A2                       2722 00103$:
                                   2723 ;	..\src\peripherals\UBLOX.c:174: delay(2500);
      0058A2 90 09 C4         [24] 2724 	mov	dptr,#0x09c4
      0058A5 12 73 0E         [24] 2725 	lcall	_delay
                                   2726 ;	..\src\peripherals\UBLOX.c:175: }while(!uart1_rxcount());
      0058A8 12 82 C7         [24] 2727 	lcall	_uart1_rxcount
      0058AB E5 82            [12] 2728 	mov	a,dpl
      0058AD 60 F3            [24] 2729 	jz	00103$
                                   2730 ;	..\src\peripherals\UBLOX.c:176: delay(25000);
      0058AF 90 61 A8         [24] 2731 	mov	dptr,#0x61a8
      0058B2 12 73 0E         [24] 2732 	lcall	_delay
                                   2733 ;	..\src\peripherals\UBLOX.c:179: do
      0058B5 7F 00            [12] 2734 	mov	r7,#0x00
      0058B7                       2735 00106$:
                                   2736 ;	..\src\peripherals\UBLOX.c:181: wtimer_runcallbacks(); // si no pongo esto por algun motivo se pierden los eventos de timer , cuidado con los delays !!
      0058B7 C0 07            [24] 2737 	push	ar7
      0058B9 C0 03            [24] 2738 	push	ar3
      0058BB C0 02            [24] 2739 	push	ar2
      0058BD C0 01            [24] 2740 	push	ar1
      0058BF 12 64 92         [24] 2741 	lcall	_wtimer_runcallbacks
      0058C2 D0 01            [24] 2742 	pop	ar1
      0058C4 D0 02            [24] 2743 	pop	ar2
      0058C6 D0 03            [24] 2744 	pop	ar3
      0058C8 D0 07            [24] 2745 	pop	ar7
                                   2746 ;	..\src\peripherals\UBLOX.c:182: *(rx_buffer+k)=uart1_rx();
      0058CA EF               [12] 2747 	mov	a,r7
      0058CB 2A               [12] 2748 	add	a,r2
      0058CC FD               [12] 2749 	mov	r5,a
      0058CD E4               [12] 2750 	clr	a
      0058CE 3B               [12] 2751 	addc	a,r3
      0058CF FE               [12] 2752 	mov	r6,a
      0058D0 12 6A 6B         [24] 2753 	lcall	_uart1_rx
      0058D3 AC 82            [24] 2754 	mov	r4,dpl
      0058D5 8D 82            [24] 2755 	mov	dpl,r5
      0058D7 8E 83            [24] 2756 	mov	dph,r6
      0058D9 EC               [12] 2757 	mov	a,r4
      0058DA F0               [24] 2758 	movx	@dptr,a
                                   2759 ;	..\src\peripherals\UBLOX.c:183: k++;
      0058DB 0F               [12] 2760 	inc	r7
                                   2761 ;	..\src\peripherals\UBLOX.c:184: delay(2500);
      0058DC 90 09 C4         [24] 2762 	mov	dptr,#0x09c4
      0058DF 12 73 0E         [24] 2763 	lcall	_delay
                                   2764 ;	..\src\peripherals\UBLOX.c:185: }while(uart1_rxcount());
      0058E2 12 82 C7         [24] 2765 	lcall	_uart1_rxcount
      0058E5 E5 82            [12] 2766 	mov	a,dpl
      0058E7 70 CE            [24] 2767 	jnz	00106$
                                   2768 ;	..\src\peripherals\UBLOX.c:188: if(rx_buffer[UBX_HEADER1_POS] == UBX_HEADER1_VAL && rx_buffer[UBX_HEADER2_POS] == UBX_HEADER2_VAL)
      0058E9 8A 82            [24] 2769 	mov	dpl,r2
      0058EB 8B 83            [24] 2770 	mov	dph,r3
      0058ED E0               [24] 2771 	movx	a,@dptr
      0058EE FE               [12] 2772 	mov	r6,a
      0058EF BE B5 02         [24] 2773 	cjne	r6,#0xb5,00191$
      0058F2 80 03            [24] 2774 	sjmp	00192$
      0058F4                       2775 00191$:
      0058F4 02 5A 9C         [24] 2776 	ljmp	00123$
      0058F7                       2777 00192$:
      0058F7 8A 82            [24] 2778 	mov	dpl,r2
      0058F9 8B 83            [24] 2779 	mov	dph,r3
      0058FB A3               [24] 2780 	inc	dptr
      0058FC E0               [24] 2781 	movx	a,@dptr
      0058FD FE               [12] 2782 	mov	r6,a
      0058FE BE 62 02         [24] 2783 	cjne	r6,#0x62,00193$
      005901 80 03            [24] 2784 	sjmp	00194$
      005903                       2785 00193$:
      005903 02 5A 9C         [24] 2786 	ljmp	00123$
      005906                       2787 00194$:
                                   2788 ;	..\src\peripherals\UBLOX.c:190: CK_A = 0;
      005906 C0 01            [24] 2789 	push	ar1
      005908 90 03 BF         [24] 2790 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_CK_A_1_195
      00590B E4               [12] 2791 	clr	a
      00590C F0               [24] 2792 	movx	@dptr,a
                                   2793 ;	..\src\peripherals\UBLOX.c:191: CK_B = 0;
      00590D 90 03 C0         [24] 2794 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_CK_B_1_195
      005910 F0               [24] 2795 	movx	@dptr,a
                                   2796 ;	..\src\peripherals\UBLOX.c:192: UBLOX_GPS_FletcherChecksum8(&rx_buffer[UBX_MESSAGE_CLASS],&CK_A,&CK_B,k-4);
      005911 74 02            [12] 2797 	mov	a,#0x02
      005913 2A               [12] 2798 	add	a,r2
      005914 FD               [12] 2799 	mov	r5,a
      005915 E4               [12] 2800 	clr	a
      005916 3B               [12] 2801 	addc	a,r3
      005917 FE               [12] 2802 	mov	r6,a
      005918 8D 32            [24] 2803 	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0,r5
      00591A 8E 33            [24] 2804 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0 + 1),r6
      00591C 75 34 00         [24] 2805 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0 + 2),#0x00
      00591F 8F 2D            [24] 2806 	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0,r7
      005921 75 2E 00         [24] 2807 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0 + 1),#0x00
      005924 E5 2D            [12] 2808 	mov	a,_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0
      005926 24 FC            [12] 2809 	add	a,#0xfc
      005928 FC               [12] 2810 	mov	r4,a
      005929 E5 2E            [12] 2811 	mov	a,(_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0 + 1)
      00592B 34 FF            [12] 2812 	addc	a,#0xff
      00592D FF               [12] 2813 	mov	r7,a
      00592E 90 03 A8         [24] 2814 	mov	dptr,#_UBLOX_GPS_FletcherChecksum8_PARM_2
      005931 74 BF            [12] 2815 	mov	a,#_UBLOX_GPS_SendCommand_WaitACK_CK_A_1_195
      005933 F0               [24] 2816 	movx	@dptr,a
      005934 74 03            [12] 2817 	mov	a,#(_UBLOX_GPS_SendCommand_WaitACK_CK_A_1_195 >> 8)
      005936 A3               [24] 2818 	inc	dptr
      005937 F0               [24] 2819 	movx	@dptr,a
      005938 E4               [12] 2820 	clr	a
      005939 A3               [24] 2821 	inc	dptr
      00593A F0               [24] 2822 	movx	@dptr,a
      00593B 90 03 AB         [24] 2823 	mov	dptr,#_UBLOX_GPS_FletcherChecksum8_PARM_3
      00593E 74 C0            [12] 2824 	mov	a,#_UBLOX_GPS_SendCommand_WaitACK_CK_B_1_195
      005940 F0               [24] 2825 	movx	@dptr,a
      005941 74 03            [12] 2826 	mov	a,#(_UBLOX_GPS_SendCommand_WaitACK_CK_B_1_195 >> 8)
      005943 A3               [24] 2827 	inc	dptr
      005944 F0               [24] 2828 	movx	@dptr,a
      005945 E4               [12] 2829 	clr	a
      005946 A3               [24] 2830 	inc	dptr
      005947 F0               [24] 2831 	movx	@dptr,a
      005948 90 03 AE         [24] 2832 	mov	dptr,#_UBLOX_GPS_FletcherChecksum8_PARM_4
      00594B EC               [12] 2833 	mov	a,r4
      00594C F0               [24] 2834 	movx	@dptr,a
      00594D EF               [12] 2835 	mov	a,r7
      00594E A3               [24] 2836 	inc	dptr
      00594F F0               [24] 2837 	movx	@dptr,a
      005950 85 32 82         [24] 2838 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0
      005953 85 33 83         [24] 2839 	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0 + 1)
      005956 85 34 F0         [24] 2840 	mov	b,(_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0 + 2)
      005959 C0 06            [24] 2841 	push	ar6
      00595B C0 05            [24] 2842 	push	ar5
      00595D C0 03            [24] 2843 	push	ar3
      00595F C0 02            [24] 2844 	push	ar2
      005961 C0 01            [24] 2845 	push	ar1
      005963 12 56 5D         [24] 2846 	lcall	_UBLOX_GPS_FletcherChecksum8
      005966 D0 01            [24] 2847 	pop	ar1
      005968 D0 02            [24] 2848 	pop	ar2
      00596A D0 03            [24] 2849 	pop	ar3
      00596C D0 05            [24] 2850 	pop	ar5
      00596E D0 06            [24] 2851 	pop	ar6
                                   2852 ;	..\src\peripherals\UBLOX.c:193: if(CK_A == rx_buffer[k-2] && CK_B == rx_buffer[k-1]) // verifico el checksum calculado
      005970 E5 2D            [12] 2853 	mov	a,_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0
      005972 24 FE            [12] 2854 	add	a,#0xfe
      005974 FC               [12] 2855 	mov	r4,a
      005975 E5 2E            [12] 2856 	mov	a,(_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0 + 1)
      005977 34 FF            [12] 2857 	addc	a,#0xff
      005979 FF               [12] 2858 	mov	r7,a
      00597A EC               [12] 2859 	mov	a,r4
      00597B 2A               [12] 2860 	add	a,r2
      00597C F5 82            [12] 2861 	mov	dpl,a
      00597E EF               [12] 2862 	mov	a,r7
      00597F 3B               [12] 2863 	addc	a,r3
      005980 F5 83            [12] 2864 	mov	dph,a
      005982 E0               [24] 2865 	movx	a,@dptr
      005983 FF               [12] 2866 	mov	r7,a
      005984 90 03 BF         [24] 2867 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_CK_A_1_195
      005987 E0               [24] 2868 	movx	a,@dptr
      005988 FC               [12] 2869 	mov	r4,a
      005989 B5 07 02         [24] 2870 	cjne	a,ar7,00195$
      00598C 80 05            [24] 2871 	sjmp	00196$
      00598E                       2872 00195$:
      00598E D0 01            [24] 2873 	pop	ar1
      005990 02 5A 9C         [24] 2874 	ljmp	00123$
      005993                       2875 00196$:
      005993 D0 01            [24] 2876 	pop	ar1
      005995 E5 2D            [12] 2877 	mov	a,_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0
      005997 24 FF            [12] 2878 	add	a,#0xff
      005999 FC               [12] 2879 	mov	r4,a
      00599A E5 2E            [12] 2880 	mov	a,(_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0 + 1)
      00599C 34 FF            [12] 2881 	addc	a,#0xff
      00599E FF               [12] 2882 	mov	r7,a
      00599F EC               [12] 2883 	mov	a,r4
      0059A0 2A               [12] 2884 	add	a,r2
      0059A1 F5 82            [12] 2885 	mov	dpl,a
      0059A3 EF               [12] 2886 	mov	a,r7
      0059A4 3B               [12] 2887 	addc	a,r3
      0059A5 F5 83            [12] 2888 	mov	dph,a
      0059A7 E0               [24] 2889 	movx	a,@dptr
      0059A8 FF               [12] 2890 	mov	r7,a
      0059A9 90 03 C0         [24] 2891 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_CK_B_1_195
      0059AC E0               [24] 2892 	movx	a,@dptr
      0059AD FC               [12] 2893 	mov	r4,a
      0059AE B5 07 02         [24] 2894 	cjne	a,ar7,00197$
      0059B1 80 03            [24] 2895 	sjmp	00198$
      0059B3                       2896 00197$:
      0059B3 02 5A 9C         [24] 2897 	ljmp	00123$
      0059B6                       2898 00198$:
                                   2899 ;	..\src\peripherals\UBLOX.c:196: if(!AnsOrAck)
      0059B6 90 03 B9         [24] 2900 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_5
      0059B9 E0               [24] 2901 	movx	a,@dptr
      0059BA 60 03            [24] 2902 	jz	00199$
      0059BC 02 5A 7A         [24] 2903 	ljmp	00117$
      0059BF                       2904 00199$:
                                   2905 ;	..\src\peripherals\UBLOX.c:199: if(msg_class == rx_buffer[UBX_MESSAGE_CLASS] && msg_id == rx_buffer[UBX_MESSAGE_ID])
      0059BF 8D 82            [24] 2906 	mov	dpl,r5
      0059C1 8E 83            [24] 2907 	mov	dph,r6
      0059C3 E0               [24] 2908 	movx	a,@dptr
      0059C4 FF               [12] 2909 	mov	r7,a
      0059C5 E9               [12] 2910 	mov	a,r1
      0059C6 B5 07 02         [24] 2911 	cjne	a,ar7,00200$
      0059C9 80 03            [24] 2912 	sjmp	00201$
      0059CB                       2913 00200$:
      0059CB 02 5A 9C         [24] 2914 	ljmp	00123$
      0059CE                       2915 00201$:
      0059CE 8A 82            [24] 2916 	mov	dpl,r2
      0059D0 8B 83            [24] 2917 	mov	dph,r3
      0059D2 A3               [24] 2918 	inc	dptr
      0059D3 A3               [24] 2919 	inc	dptr
      0059D4 A3               [24] 2920 	inc	dptr
      0059D5 E0               [24] 2921 	movx	a,@dptr
      0059D6 FF               [12] 2922 	mov	r7,a
      0059D7 B5 29 02         [24] 2923 	cjne	a,_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0,00202$
      0059DA 80 03            [24] 2924 	sjmp	00203$
      0059DC                       2925 00202$:
      0059DC 02 5A 9C         [24] 2926 	ljmp	00123$
      0059DF                       2927 00203$:
                                   2928 ;	..\src\peripherals\UBLOX.c:201: *RxLength = rx_buffer[UBX_MESSAGE_LENGTH_MSB]<<8 | rx_buffer[UBX_MESSAGE_LENGTH_LSB];
      0059DF 90 03 BA         [24] 2929 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_6
      0059E2 E0               [24] 2930 	movx	a,@dptr
      0059E3 F5 32            [12] 2931 	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0,a
      0059E5 A3               [24] 2932 	inc	dptr
      0059E6 E0               [24] 2933 	movx	a,@dptr
      0059E7 F5 33            [12] 2934 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0 + 1),a
      0059E9 A3               [24] 2935 	inc	dptr
      0059EA E0               [24] 2936 	movx	a,@dptr
      0059EB F5 34            [12] 2937 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0 + 2),a
      0059ED 8A 82            [24] 2938 	mov	dpl,r2
      0059EF 8B 83            [24] 2939 	mov	dph,r3
      0059F1 A3               [24] 2940 	inc	dptr
      0059F2 A3               [24] 2941 	inc	dptr
      0059F3 A3               [24] 2942 	inc	dptr
      0059F4 A3               [24] 2943 	inc	dptr
      0059F5 A3               [24] 2944 	inc	dptr
      0059F6 E0               [24] 2945 	movx	a,@dptr
      0059F7 FF               [12] 2946 	mov	r7,a
      0059F8 78 00            [12] 2947 	mov	r0,#0x00
      0059FA 8A 82            [24] 2948 	mov	dpl,r2
      0059FC 8B 83            [24] 2949 	mov	dph,r3
      0059FE A3               [24] 2950 	inc	dptr
      0059FF A3               [24] 2951 	inc	dptr
      005A00 A3               [24] 2952 	inc	dptr
      005A01 A3               [24] 2953 	inc	dptr
      005A02 E0               [24] 2954 	movx	a,@dptr
      005A03 7C 00            [12] 2955 	mov	r4,#0x00
      005A05 42 00            [12] 2956 	orl	ar0,a
      005A07 EC               [12] 2957 	mov	a,r4
      005A08 42 07            [12] 2958 	orl	ar7,a
      005A0A 85 32 82         [24] 2959 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0
      005A0D 85 33 83         [24] 2960 	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0 + 1)
      005A10 85 34 F0         [24] 2961 	mov	b,(_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0 + 2)
      005A13 E8               [12] 2962 	mov	a,r0
      005A14 12 6A 1E         [24] 2963 	lcall	__gptrput
      005A17 A3               [24] 2964 	inc	dptr
      005A18 EF               [12] 2965 	mov	a,r7
      005A19 12 6A 1E         [24] 2966 	lcall	__gptrput
                                   2967 ;	..\src\peripherals\UBLOX.c:202: memcpy(payload,&rx_buffer[UBX_MESSAGE_PAYLOAD],*RxLength);
      005A1C A9 2A            [24] 2968 	mov	r1,_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0
      005A1E AC 2B            [24] 2969 	mov	r4,(_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0 + 1)
      005A20 AF 2C            [24] 2970 	mov	r7,(_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0 + 2)
      005A22 89 2D            [24] 2971 	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0,r1
      005A24 8C 2E            [24] 2972 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0 + 1),r4
      005A26 8F 2F            [24] 2973 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0 + 2),r7
      005A28 74 06            [12] 2974 	mov	a,#0x06
      005A2A 2A               [12] 2975 	add	a,r2
      005A2B F8               [12] 2976 	mov	r0,a
      005A2C E4               [12] 2977 	clr	a
      005A2D 3B               [12] 2978 	addc	a,r3
      005A2E FF               [12] 2979 	mov	r7,a
      005A2F 7C 00            [12] 2980 	mov	r4,#0x00
      005A31 C0 02            [24] 2981 	push	ar2
      005A33 C0 03            [24] 2982 	push	ar3
      005A35 85 32 82         [24] 2983 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0
      005A38 85 33 83         [24] 2984 	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0 + 1)
      005A3B 85 34 F0         [24] 2985 	mov	b,(_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0 + 2)
      005A3E 12 79 65         [24] 2986 	lcall	__gptrget
      005A41 F9               [12] 2987 	mov	r1,a
      005A42 A3               [24] 2988 	inc	dptr
      005A43 12 79 65         [24] 2989 	lcall	__gptrget
      005A46 FB               [12] 2990 	mov	r3,a
      005A47 90 03 E2         [24] 2991 	mov	dptr,#_memcpy_PARM_2
      005A4A E8               [12] 2992 	mov	a,r0
      005A4B F0               [24] 2993 	movx	@dptr,a
      005A4C EF               [12] 2994 	mov	a,r7
      005A4D A3               [24] 2995 	inc	dptr
      005A4E F0               [24] 2996 	movx	@dptr,a
      005A4F EC               [12] 2997 	mov	a,r4
      005A50 A3               [24] 2998 	inc	dptr
      005A51 F0               [24] 2999 	movx	@dptr,a
      005A52 90 03 E5         [24] 3000 	mov	dptr,#_memcpy_PARM_3
      005A55 E9               [12] 3001 	mov	a,r1
      005A56 F0               [24] 3002 	movx	@dptr,a
      005A57 EB               [12] 3003 	mov	a,r3
      005A58 A3               [24] 3004 	inc	dptr
      005A59 F0               [24] 3005 	movx	@dptr,a
      005A5A 85 2D 82         [24] 3006 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0
      005A5D 85 2E 83         [24] 3007 	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0 + 1)
      005A60 85 2F F0         [24] 3008 	mov	b,(_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0 + 2)
      005A63 C0 03            [24] 3009 	push	ar3
      005A65 C0 02            [24] 3010 	push	ar2
      005A67 12 65 97         [24] 3011 	lcall	_memcpy
      005A6A D0 02            [24] 3012 	pop	ar2
      005A6C D0 03            [24] 3013 	pop	ar3
                                   3014 ;	..\src\peripherals\UBLOX.c:203: RetVal = OK;
      005A6E 90 03 BE         [24] 3015 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_RetVal_1_195
      005A71 74 01            [12] 3016 	mov	a,#0x01
      005A73 F0               [24] 3017 	movx	@dptr,a
      005A74 D0 03            [24] 3018 	pop	ar3
      005A76 D0 02            [24] 3019 	pop	ar2
      005A78 80 22            [24] 3020 	sjmp	00123$
      005A7A                       3021 00117$:
                                   3022 ;	..\src\peripherals\UBLOX.c:209: if(rx_buffer[UBX_MESSAGE_CLASS] == eACK && rx_buffer[UBX_MESSAGE_ID] == ACK) RetVal = ACK;
      005A7A 8D 82            [24] 3023 	mov	dpl,r5
      005A7C 8E 83            [24] 3024 	mov	dph,r6
      005A7E E0               [24] 3025 	movx	a,@dptr
      005A7F FD               [12] 3026 	mov	r5,a
      005A80 BD 05 14         [24] 3027 	cjne	r5,#0x05,00113$
      005A83 8A 82            [24] 3028 	mov	dpl,r2
      005A85 8B 83            [24] 3029 	mov	dph,r3
      005A87 A3               [24] 3030 	inc	dptr
      005A88 A3               [24] 3031 	inc	dptr
      005A89 A3               [24] 3032 	inc	dptr
      005A8A E0               [24] 3033 	movx	a,@dptr
      005A8B FF               [12] 3034 	mov	r7,a
      005A8C BF 01 08         [24] 3035 	cjne	r7,#0x01,00113$
      005A8F 90 03 BE         [24] 3036 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_RetVal_1_195
      005A92 74 01            [12] 3037 	mov	a,#0x01
      005A94 F0               [24] 3038 	movx	@dptr,a
      005A95 80 05            [24] 3039 	sjmp	00123$
      005A97                       3040 00113$:
                                   3041 ;	..\src\peripherals\UBLOX.c:210: else RetVal = NAK;
      005A97 90 03 BE         [24] 3042 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_RetVal_1_195
      005A9A E4               [12] 3043 	clr	a
      005A9B F0               [24] 3044 	movx	@dptr,a
      005A9C                       3045 00123$:
                                   3046 ;	..\src\peripherals\UBLOX.c:214: free(buffer_aux);
      005A9C AE 30            [24] 3047 	mov	r6,_UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0
      005A9E AF 31            [24] 3048 	mov	r7,(_UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0 + 1)
      005AA0 7D 00            [12] 3049 	mov	r5,#0x00
      005AA2 8E 82            [24] 3050 	mov	dpl,r6
      005AA4 8F 83            [24] 3051 	mov	dph,r7
      005AA6 8D F0            [24] 3052 	mov	b,r5
      005AA8 C0 03            [24] 3053 	push	ar3
      005AAA C0 02            [24] 3054 	push	ar2
      005AAC 12 5B 4D         [24] 3055 	lcall	_free
      005AAF D0 02            [24] 3056 	pop	ar2
      005AB1 D0 03            [24] 3057 	pop	ar3
                                   3058 ;	..\src\peripherals\UBLOX.c:215: free(rx_buffer);
      005AB3 7F 00            [12] 3059 	mov	r7,#0x00
      005AB5 8A 82            [24] 3060 	mov	dpl,r2
      005AB7 8B 83            [24] 3061 	mov	dph,r3
      005AB9 8F F0            [24] 3062 	mov	b,r7
      005ABB 12 5B 4D         [24] 3063 	lcall	_free
                                   3064 ;	..\src\peripherals\UBLOX.c:216: return RetVal;
      005ABE 90 03 BE         [24] 3065 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_RetVal_1_195
      005AC1 E0               [24] 3066 	movx	a,@dptr
      005AC2 F5 82            [12] 3067 	mov	dpl,a
      005AC4 22               [24] 3068 	ret
                                   3069 	.area CSEG    (CODE)
                                   3070 	.area CONST   (CODE)
                                   3071 	.area XINIT   (CODE)
                                   3072 	.area CABS    (ABS,CODE)
