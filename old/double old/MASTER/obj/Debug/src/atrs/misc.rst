                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.6.0 #9615 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module misc
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _dbglink_writehex32
                                     12 	.globl _dbglink_writestr
                                     13 	.globl _transmit_packet
                                     14 	.globl _wtimer_remove
                                     15 	.globl _wtimer1_addrelative
                                     16 	.globl _wtimer0_addabsolute
                                     17 	.globl _wtimer_runcallbacks
                                     18 	.globl _wtimer_idle
                                     19 	.globl _PORTC_7
                                     20 	.globl _PORTC_6
                                     21 	.globl _PORTC_5
                                     22 	.globl _PORTC_4
                                     23 	.globl _PORTC_3
                                     24 	.globl _PORTC_2
                                     25 	.globl _PORTC_1
                                     26 	.globl _PORTC_0
                                     27 	.globl _PORTB_7
                                     28 	.globl _PORTB_6
                                     29 	.globl _PORTB_5
                                     30 	.globl _PORTB_4
                                     31 	.globl _PORTB_3
                                     32 	.globl _PORTB_2
                                     33 	.globl _PORTB_1
                                     34 	.globl _PORTB_0
                                     35 	.globl _PORTA_7
                                     36 	.globl _PORTA_6
                                     37 	.globl _PORTA_5
                                     38 	.globl _PORTA_4
                                     39 	.globl _PORTA_3
                                     40 	.globl _PORTA_2
                                     41 	.globl _PORTA_1
                                     42 	.globl _PORTA_0
                                     43 	.globl _PINC_7
                                     44 	.globl _PINC_6
                                     45 	.globl _PINC_5
                                     46 	.globl _PINC_4
                                     47 	.globl _PINC_3
                                     48 	.globl _PINC_2
                                     49 	.globl _PINC_1
                                     50 	.globl _PINC_0
                                     51 	.globl _PINB_7
                                     52 	.globl _PINB_6
                                     53 	.globl _PINB_5
                                     54 	.globl _PINB_4
                                     55 	.globl _PINB_3
                                     56 	.globl _PINB_2
                                     57 	.globl _PINB_1
                                     58 	.globl _PINB_0
                                     59 	.globl _PINA_7
                                     60 	.globl _PINA_6
                                     61 	.globl _PINA_5
                                     62 	.globl _PINA_4
                                     63 	.globl _PINA_3
                                     64 	.globl _PINA_2
                                     65 	.globl _PINA_1
                                     66 	.globl _PINA_0
                                     67 	.globl _CY
                                     68 	.globl _AC
                                     69 	.globl _F0
                                     70 	.globl _RS1
                                     71 	.globl _RS0
                                     72 	.globl _OV
                                     73 	.globl _F1
                                     74 	.globl _P
                                     75 	.globl _IP_7
                                     76 	.globl _IP_6
                                     77 	.globl _IP_5
                                     78 	.globl _IP_4
                                     79 	.globl _IP_3
                                     80 	.globl _IP_2
                                     81 	.globl _IP_1
                                     82 	.globl _IP_0
                                     83 	.globl _EA
                                     84 	.globl _IE_7
                                     85 	.globl _IE_6
                                     86 	.globl _IE_5
                                     87 	.globl _IE_4
                                     88 	.globl _IE_3
                                     89 	.globl _IE_2
                                     90 	.globl _IE_1
                                     91 	.globl _IE_0
                                     92 	.globl _EIP_7
                                     93 	.globl _EIP_6
                                     94 	.globl _EIP_5
                                     95 	.globl _EIP_4
                                     96 	.globl _EIP_3
                                     97 	.globl _EIP_2
                                     98 	.globl _EIP_1
                                     99 	.globl _EIP_0
                                    100 	.globl _EIE_7
                                    101 	.globl _EIE_6
                                    102 	.globl _EIE_5
                                    103 	.globl _EIE_4
                                    104 	.globl _EIE_3
                                    105 	.globl _EIE_2
                                    106 	.globl _EIE_1
                                    107 	.globl _EIE_0
                                    108 	.globl _E2IP_7
                                    109 	.globl _E2IP_6
                                    110 	.globl _E2IP_5
                                    111 	.globl _E2IP_4
                                    112 	.globl _E2IP_3
                                    113 	.globl _E2IP_2
                                    114 	.globl _E2IP_1
                                    115 	.globl _E2IP_0
                                    116 	.globl _E2IE_7
                                    117 	.globl _E2IE_6
                                    118 	.globl _E2IE_5
                                    119 	.globl _E2IE_4
                                    120 	.globl _E2IE_3
                                    121 	.globl _E2IE_2
                                    122 	.globl _E2IE_1
                                    123 	.globl _E2IE_0
                                    124 	.globl _B_7
                                    125 	.globl _B_6
                                    126 	.globl _B_5
                                    127 	.globl _B_4
                                    128 	.globl _B_3
                                    129 	.globl _B_2
                                    130 	.globl _B_1
                                    131 	.globl _B_0
                                    132 	.globl _ACC_7
                                    133 	.globl _ACC_6
                                    134 	.globl _ACC_5
                                    135 	.globl _ACC_4
                                    136 	.globl _ACC_3
                                    137 	.globl _ACC_2
                                    138 	.globl _ACC_1
                                    139 	.globl _ACC_0
                                    140 	.globl _WTSTAT
                                    141 	.globl _WTIRQEN
                                    142 	.globl _WTEVTD
                                    143 	.globl _WTEVTD1
                                    144 	.globl _WTEVTD0
                                    145 	.globl _WTEVTC
                                    146 	.globl _WTEVTC1
                                    147 	.globl _WTEVTC0
                                    148 	.globl _WTEVTB
                                    149 	.globl _WTEVTB1
                                    150 	.globl _WTEVTB0
                                    151 	.globl _WTEVTA
                                    152 	.globl _WTEVTA1
                                    153 	.globl _WTEVTA0
                                    154 	.globl _WTCNTR1
                                    155 	.globl _WTCNTB
                                    156 	.globl _WTCNTB1
                                    157 	.globl _WTCNTB0
                                    158 	.globl _WTCNTA
                                    159 	.globl _WTCNTA1
                                    160 	.globl _WTCNTA0
                                    161 	.globl _WTCFGB
                                    162 	.globl _WTCFGA
                                    163 	.globl _WDTRESET
                                    164 	.globl _WDTCFG
                                    165 	.globl _U1STATUS
                                    166 	.globl _U1SHREG
                                    167 	.globl _U1MODE
                                    168 	.globl _U1CTRL
                                    169 	.globl _U0STATUS
                                    170 	.globl _U0SHREG
                                    171 	.globl _U0MODE
                                    172 	.globl _U0CTRL
                                    173 	.globl _T2STATUS
                                    174 	.globl _T2PERIOD
                                    175 	.globl _T2PERIOD1
                                    176 	.globl _T2PERIOD0
                                    177 	.globl _T2MODE
                                    178 	.globl _T2CNT
                                    179 	.globl _T2CNT1
                                    180 	.globl _T2CNT0
                                    181 	.globl _T2CLKSRC
                                    182 	.globl _T1STATUS
                                    183 	.globl _T1PERIOD
                                    184 	.globl _T1PERIOD1
                                    185 	.globl _T1PERIOD0
                                    186 	.globl _T1MODE
                                    187 	.globl _T1CNT
                                    188 	.globl _T1CNT1
                                    189 	.globl _T1CNT0
                                    190 	.globl _T1CLKSRC
                                    191 	.globl _T0STATUS
                                    192 	.globl _T0PERIOD
                                    193 	.globl _T0PERIOD1
                                    194 	.globl _T0PERIOD0
                                    195 	.globl _T0MODE
                                    196 	.globl _T0CNT
                                    197 	.globl _T0CNT1
                                    198 	.globl _T0CNT0
                                    199 	.globl _T0CLKSRC
                                    200 	.globl _SPSTATUS
                                    201 	.globl _SPSHREG
                                    202 	.globl _SPMODE
                                    203 	.globl _SPCLKSRC
                                    204 	.globl _RADIOSTAT
                                    205 	.globl _RADIOSTAT1
                                    206 	.globl _RADIOSTAT0
                                    207 	.globl _RADIODATA
                                    208 	.globl _RADIODATA3
                                    209 	.globl _RADIODATA2
                                    210 	.globl _RADIODATA1
                                    211 	.globl _RADIODATA0
                                    212 	.globl _RADIOADDR
                                    213 	.globl _RADIOADDR1
                                    214 	.globl _RADIOADDR0
                                    215 	.globl _RADIOACC
                                    216 	.globl _OC1STATUS
                                    217 	.globl _OC1PIN
                                    218 	.globl _OC1MODE
                                    219 	.globl _OC1COMP
                                    220 	.globl _OC1COMP1
                                    221 	.globl _OC1COMP0
                                    222 	.globl _OC0STATUS
                                    223 	.globl _OC0PIN
                                    224 	.globl _OC0MODE
                                    225 	.globl _OC0COMP
                                    226 	.globl _OC0COMP1
                                    227 	.globl _OC0COMP0
                                    228 	.globl _NVSTATUS
                                    229 	.globl _NVKEY
                                    230 	.globl _NVDATA
                                    231 	.globl _NVDATA1
                                    232 	.globl _NVDATA0
                                    233 	.globl _NVADDR
                                    234 	.globl _NVADDR1
                                    235 	.globl _NVADDR0
                                    236 	.globl _IC1STATUS
                                    237 	.globl _IC1MODE
                                    238 	.globl _IC1CAPT
                                    239 	.globl _IC1CAPT1
                                    240 	.globl _IC1CAPT0
                                    241 	.globl _IC0STATUS
                                    242 	.globl _IC0MODE
                                    243 	.globl _IC0CAPT
                                    244 	.globl _IC0CAPT1
                                    245 	.globl _IC0CAPT0
                                    246 	.globl _PORTR
                                    247 	.globl _PORTC
                                    248 	.globl _PORTB
                                    249 	.globl _PORTA
                                    250 	.globl _PINR
                                    251 	.globl _PINC
                                    252 	.globl _PINB
                                    253 	.globl _PINA
                                    254 	.globl _DIRR
                                    255 	.globl _DIRC
                                    256 	.globl _DIRB
                                    257 	.globl _DIRA
                                    258 	.globl _DBGLNKSTAT
                                    259 	.globl _DBGLNKBUF
                                    260 	.globl _CODECONFIG
                                    261 	.globl _CLKSTAT
                                    262 	.globl _CLKCON
                                    263 	.globl _ANALOGCOMP
                                    264 	.globl _ADCCONV
                                    265 	.globl _ADCCLKSRC
                                    266 	.globl _ADCCH3CONFIG
                                    267 	.globl _ADCCH2CONFIG
                                    268 	.globl _ADCCH1CONFIG
                                    269 	.globl _ADCCH0CONFIG
                                    270 	.globl __XPAGE
                                    271 	.globl _XPAGE
                                    272 	.globl _SP
                                    273 	.globl _PSW
                                    274 	.globl _PCON
                                    275 	.globl _IP
                                    276 	.globl _IE
                                    277 	.globl _EIP
                                    278 	.globl _EIE
                                    279 	.globl _E2IP
                                    280 	.globl _E2IE
                                    281 	.globl _DPS
                                    282 	.globl _DPTR1
                                    283 	.globl _DPTR0
                                    284 	.globl _DPL1
                                    285 	.globl _DPL
                                    286 	.globl _DPH1
                                    287 	.globl _DPH
                                    288 	.globl _B
                                    289 	.globl _ACC
                                    290 	.globl _delay_tx_PARM_3
                                    291 	.globl _delay_tx_PARM_2
                                    292 	.globl _wakeup_tmr3
                                    293 	.globl _wakeup_tmr2
                                    294 	.globl _wakeup_tmr1
                                    295 	.globl _XTALREADY
                                    296 	.globl _XTALOSC
                                    297 	.globl _XTALAMPL
                                    298 	.globl _SILICONREV
                                    299 	.globl _SCRATCH3
                                    300 	.globl _SCRATCH2
                                    301 	.globl _SCRATCH1
                                    302 	.globl _SCRATCH0
                                    303 	.globl _RADIOMUX
                                    304 	.globl _RADIOFSTATADDR
                                    305 	.globl _RADIOFSTATADDR1
                                    306 	.globl _RADIOFSTATADDR0
                                    307 	.globl _RADIOFDATAADDR
                                    308 	.globl _RADIOFDATAADDR1
                                    309 	.globl _RADIOFDATAADDR0
                                    310 	.globl _OSCRUN
                                    311 	.globl _OSCREADY
                                    312 	.globl _OSCFORCERUN
                                    313 	.globl _OSCCALIB
                                    314 	.globl _MISCCTRL
                                    315 	.globl _LPXOSCGM
                                    316 	.globl _LPOSCREF
                                    317 	.globl _LPOSCREF1
                                    318 	.globl _LPOSCREF0
                                    319 	.globl _LPOSCPER
                                    320 	.globl _LPOSCPER1
                                    321 	.globl _LPOSCPER0
                                    322 	.globl _LPOSCKFILT
                                    323 	.globl _LPOSCKFILT1
                                    324 	.globl _LPOSCKFILT0
                                    325 	.globl _LPOSCFREQ
                                    326 	.globl _LPOSCFREQ1
                                    327 	.globl _LPOSCFREQ0
                                    328 	.globl _LPOSCCONFIG
                                    329 	.globl _PINSEL
                                    330 	.globl _PINCHGC
                                    331 	.globl _PINCHGB
                                    332 	.globl _PINCHGA
                                    333 	.globl _PALTRADIO
                                    334 	.globl _PALTC
                                    335 	.globl _PALTB
                                    336 	.globl _PALTA
                                    337 	.globl _INTCHGC
                                    338 	.globl _INTCHGB
                                    339 	.globl _INTCHGA
                                    340 	.globl _EXTIRQ
                                    341 	.globl _GPIOENABLE
                                    342 	.globl _ANALOGA
                                    343 	.globl _FRCOSCREF
                                    344 	.globl _FRCOSCREF1
                                    345 	.globl _FRCOSCREF0
                                    346 	.globl _FRCOSCPER
                                    347 	.globl _FRCOSCPER1
                                    348 	.globl _FRCOSCPER0
                                    349 	.globl _FRCOSCKFILT
                                    350 	.globl _FRCOSCKFILT1
                                    351 	.globl _FRCOSCKFILT0
                                    352 	.globl _FRCOSCFREQ
                                    353 	.globl _FRCOSCFREQ1
                                    354 	.globl _FRCOSCFREQ0
                                    355 	.globl _FRCOSCCTRL
                                    356 	.globl _FRCOSCCONFIG
                                    357 	.globl _DMA1CONFIG
                                    358 	.globl _DMA1ADDR
                                    359 	.globl _DMA1ADDR1
                                    360 	.globl _DMA1ADDR0
                                    361 	.globl _DMA0CONFIG
                                    362 	.globl _DMA0ADDR
                                    363 	.globl _DMA0ADDR1
                                    364 	.globl _DMA0ADDR0
                                    365 	.globl _ADCTUNE2
                                    366 	.globl _ADCTUNE1
                                    367 	.globl _ADCTUNE0
                                    368 	.globl _ADCCH3VAL
                                    369 	.globl _ADCCH3VAL1
                                    370 	.globl _ADCCH3VAL0
                                    371 	.globl _ADCCH2VAL
                                    372 	.globl _ADCCH2VAL1
                                    373 	.globl _ADCCH2VAL0
                                    374 	.globl _ADCCH1VAL
                                    375 	.globl _ADCCH1VAL1
                                    376 	.globl _ADCCH1VAL0
                                    377 	.globl _ADCCH0VAL
                                    378 	.globl _ADCCH0VAL1
                                    379 	.globl _ADCCH0VAL0
                                    380 	.globl _delay_ms
                                    381 	.globl _delay_tx
                                    382 ;--------------------------------------------------------
                                    383 ; special function registers
                                    384 ;--------------------------------------------------------
                                    385 	.area RSEG    (ABS,DATA)
      000000                        386 	.org 0x0000
                           0000E0   387 _ACC	=	0x00e0
                           0000F0   388 _B	=	0x00f0
                           000083   389 _DPH	=	0x0083
                           000085   390 _DPH1	=	0x0085
                           000082   391 _DPL	=	0x0082
                           000084   392 _DPL1	=	0x0084
                           008382   393 _DPTR0	=	0x8382
                           008584   394 _DPTR1	=	0x8584
                           000086   395 _DPS	=	0x0086
                           0000A0   396 _E2IE	=	0x00a0
                           0000C0   397 _E2IP	=	0x00c0
                           000098   398 _EIE	=	0x0098
                           0000B0   399 _EIP	=	0x00b0
                           0000A8   400 _IE	=	0x00a8
                           0000B8   401 _IP	=	0x00b8
                           000087   402 _PCON	=	0x0087
                           0000D0   403 _PSW	=	0x00d0
                           000081   404 _SP	=	0x0081
                           0000D9   405 _XPAGE	=	0x00d9
                           0000D9   406 __XPAGE	=	0x00d9
                           0000CA   407 _ADCCH0CONFIG	=	0x00ca
                           0000CB   408 _ADCCH1CONFIG	=	0x00cb
                           0000D2   409 _ADCCH2CONFIG	=	0x00d2
                           0000D3   410 _ADCCH3CONFIG	=	0x00d3
                           0000D1   411 _ADCCLKSRC	=	0x00d1
                           0000C9   412 _ADCCONV	=	0x00c9
                           0000E1   413 _ANALOGCOMP	=	0x00e1
                           0000C6   414 _CLKCON	=	0x00c6
                           0000C7   415 _CLKSTAT	=	0x00c7
                           000097   416 _CODECONFIG	=	0x0097
                           0000E3   417 _DBGLNKBUF	=	0x00e3
                           0000E2   418 _DBGLNKSTAT	=	0x00e2
                           000089   419 _DIRA	=	0x0089
                           00008A   420 _DIRB	=	0x008a
                           00008B   421 _DIRC	=	0x008b
                           00008E   422 _DIRR	=	0x008e
                           0000C8   423 _PINA	=	0x00c8
                           0000E8   424 _PINB	=	0x00e8
                           0000F8   425 _PINC	=	0x00f8
                           00008D   426 _PINR	=	0x008d
                           000080   427 _PORTA	=	0x0080
                           000088   428 _PORTB	=	0x0088
                           000090   429 _PORTC	=	0x0090
                           00008C   430 _PORTR	=	0x008c
                           0000CE   431 _IC0CAPT0	=	0x00ce
                           0000CF   432 _IC0CAPT1	=	0x00cf
                           00CFCE   433 _IC0CAPT	=	0xcfce
                           0000CC   434 _IC0MODE	=	0x00cc
                           0000CD   435 _IC0STATUS	=	0x00cd
                           0000D6   436 _IC1CAPT0	=	0x00d6
                           0000D7   437 _IC1CAPT1	=	0x00d7
                           00D7D6   438 _IC1CAPT	=	0xd7d6
                           0000D4   439 _IC1MODE	=	0x00d4
                           0000D5   440 _IC1STATUS	=	0x00d5
                           000092   441 _NVADDR0	=	0x0092
                           000093   442 _NVADDR1	=	0x0093
                           009392   443 _NVADDR	=	0x9392
                           000094   444 _NVDATA0	=	0x0094
                           000095   445 _NVDATA1	=	0x0095
                           009594   446 _NVDATA	=	0x9594
                           000096   447 _NVKEY	=	0x0096
                           000091   448 _NVSTATUS	=	0x0091
                           0000BC   449 _OC0COMP0	=	0x00bc
                           0000BD   450 _OC0COMP1	=	0x00bd
                           00BDBC   451 _OC0COMP	=	0xbdbc
                           0000B9   452 _OC0MODE	=	0x00b9
                           0000BA   453 _OC0PIN	=	0x00ba
                           0000BB   454 _OC0STATUS	=	0x00bb
                           0000C4   455 _OC1COMP0	=	0x00c4
                           0000C5   456 _OC1COMP1	=	0x00c5
                           00C5C4   457 _OC1COMP	=	0xc5c4
                           0000C1   458 _OC1MODE	=	0x00c1
                           0000C2   459 _OC1PIN	=	0x00c2
                           0000C3   460 _OC1STATUS	=	0x00c3
                           0000B1   461 _RADIOACC	=	0x00b1
                           0000B3   462 _RADIOADDR0	=	0x00b3
                           0000B2   463 _RADIOADDR1	=	0x00b2
                           00B2B3   464 _RADIOADDR	=	0xb2b3
                           0000B7   465 _RADIODATA0	=	0x00b7
                           0000B6   466 _RADIODATA1	=	0x00b6
                           0000B5   467 _RADIODATA2	=	0x00b5
                           0000B4   468 _RADIODATA3	=	0x00b4
                           B4B5B6B7   469 _RADIODATA	=	0xb4b5b6b7
                           0000BE   470 _RADIOSTAT0	=	0x00be
                           0000BF   471 _RADIOSTAT1	=	0x00bf
                           00BFBE   472 _RADIOSTAT	=	0xbfbe
                           0000DF   473 _SPCLKSRC	=	0x00df
                           0000DC   474 _SPMODE	=	0x00dc
                           0000DE   475 _SPSHREG	=	0x00de
                           0000DD   476 _SPSTATUS	=	0x00dd
                           00009A   477 _T0CLKSRC	=	0x009a
                           00009C   478 _T0CNT0	=	0x009c
                           00009D   479 _T0CNT1	=	0x009d
                           009D9C   480 _T0CNT	=	0x9d9c
                           000099   481 _T0MODE	=	0x0099
                           00009E   482 _T0PERIOD0	=	0x009e
                           00009F   483 _T0PERIOD1	=	0x009f
                           009F9E   484 _T0PERIOD	=	0x9f9e
                           00009B   485 _T0STATUS	=	0x009b
                           0000A2   486 _T1CLKSRC	=	0x00a2
                           0000A4   487 _T1CNT0	=	0x00a4
                           0000A5   488 _T1CNT1	=	0x00a5
                           00A5A4   489 _T1CNT	=	0xa5a4
                           0000A1   490 _T1MODE	=	0x00a1
                           0000A6   491 _T1PERIOD0	=	0x00a6
                           0000A7   492 _T1PERIOD1	=	0x00a7
                           00A7A6   493 _T1PERIOD	=	0xa7a6
                           0000A3   494 _T1STATUS	=	0x00a3
                           0000AA   495 _T2CLKSRC	=	0x00aa
                           0000AC   496 _T2CNT0	=	0x00ac
                           0000AD   497 _T2CNT1	=	0x00ad
                           00ADAC   498 _T2CNT	=	0xadac
                           0000A9   499 _T2MODE	=	0x00a9
                           0000AE   500 _T2PERIOD0	=	0x00ae
                           0000AF   501 _T2PERIOD1	=	0x00af
                           00AFAE   502 _T2PERIOD	=	0xafae
                           0000AB   503 _T2STATUS	=	0x00ab
                           0000E4   504 _U0CTRL	=	0x00e4
                           0000E7   505 _U0MODE	=	0x00e7
                           0000E6   506 _U0SHREG	=	0x00e6
                           0000E5   507 _U0STATUS	=	0x00e5
                           0000EC   508 _U1CTRL	=	0x00ec
                           0000EF   509 _U1MODE	=	0x00ef
                           0000EE   510 _U1SHREG	=	0x00ee
                           0000ED   511 _U1STATUS	=	0x00ed
                           0000DA   512 _WDTCFG	=	0x00da
                           0000DB   513 _WDTRESET	=	0x00db
                           0000F1   514 _WTCFGA	=	0x00f1
                           0000F9   515 _WTCFGB	=	0x00f9
                           0000F2   516 _WTCNTA0	=	0x00f2
                           0000F3   517 _WTCNTA1	=	0x00f3
                           00F3F2   518 _WTCNTA	=	0xf3f2
                           0000FA   519 _WTCNTB0	=	0x00fa
                           0000FB   520 _WTCNTB1	=	0x00fb
                           00FBFA   521 _WTCNTB	=	0xfbfa
                           0000EB   522 _WTCNTR1	=	0x00eb
                           0000F4   523 _WTEVTA0	=	0x00f4
                           0000F5   524 _WTEVTA1	=	0x00f5
                           00F5F4   525 _WTEVTA	=	0xf5f4
                           0000F6   526 _WTEVTB0	=	0x00f6
                           0000F7   527 _WTEVTB1	=	0x00f7
                           00F7F6   528 _WTEVTB	=	0xf7f6
                           0000FC   529 _WTEVTC0	=	0x00fc
                           0000FD   530 _WTEVTC1	=	0x00fd
                           00FDFC   531 _WTEVTC	=	0xfdfc
                           0000FE   532 _WTEVTD0	=	0x00fe
                           0000FF   533 _WTEVTD1	=	0x00ff
                           00FFFE   534 _WTEVTD	=	0xfffe
                           0000E9   535 _WTIRQEN	=	0x00e9
                           0000EA   536 _WTSTAT	=	0x00ea
                                    537 ;--------------------------------------------------------
                                    538 ; special function bits
                                    539 ;--------------------------------------------------------
                                    540 	.area RSEG    (ABS,DATA)
      000000                        541 	.org 0x0000
                           0000E0   542 _ACC_0	=	0x00e0
                           0000E1   543 _ACC_1	=	0x00e1
                           0000E2   544 _ACC_2	=	0x00e2
                           0000E3   545 _ACC_3	=	0x00e3
                           0000E4   546 _ACC_4	=	0x00e4
                           0000E5   547 _ACC_5	=	0x00e5
                           0000E6   548 _ACC_6	=	0x00e6
                           0000E7   549 _ACC_7	=	0x00e7
                           0000F0   550 _B_0	=	0x00f0
                           0000F1   551 _B_1	=	0x00f1
                           0000F2   552 _B_2	=	0x00f2
                           0000F3   553 _B_3	=	0x00f3
                           0000F4   554 _B_4	=	0x00f4
                           0000F5   555 _B_5	=	0x00f5
                           0000F6   556 _B_6	=	0x00f6
                           0000F7   557 _B_7	=	0x00f7
                           0000A0   558 _E2IE_0	=	0x00a0
                           0000A1   559 _E2IE_1	=	0x00a1
                           0000A2   560 _E2IE_2	=	0x00a2
                           0000A3   561 _E2IE_3	=	0x00a3
                           0000A4   562 _E2IE_4	=	0x00a4
                           0000A5   563 _E2IE_5	=	0x00a5
                           0000A6   564 _E2IE_6	=	0x00a6
                           0000A7   565 _E2IE_7	=	0x00a7
                           0000C0   566 _E2IP_0	=	0x00c0
                           0000C1   567 _E2IP_1	=	0x00c1
                           0000C2   568 _E2IP_2	=	0x00c2
                           0000C3   569 _E2IP_3	=	0x00c3
                           0000C4   570 _E2IP_4	=	0x00c4
                           0000C5   571 _E2IP_5	=	0x00c5
                           0000C6   572 _E2IP_6	=	0x00c6
                           0000C7   573 _E2IP_7	=	0x00c7
                           000098   574 _EIE_0	=	0x0098
                           000099   575 _EIE_1	=	0x0099
                           00009A   576 _EIE_2	=	0x009a
                           00009B   577 _EIE_3	=	0x009b
                           00009C   578 _EIE_4	=	0x009c
                           00009D   579 _EIE_5	=	0x009d
                           00009E   580 _EIE_6	=	0x009e
                           00009F   581 _EIE_7	=	0x009f
                           0000B0   582 _EIP_0	=	0x00b0
                           0000B1   583 _EIP_1	=	0x00b1
                           0000B2   584 _EIP_2	=	0x00b2
                           0000B3   585 _EIP_3	=	0x00b3
                           0000B4   586 _EIP_4	=	0x00b4
                           0000B5   587 _EIP_5	=	0x00b5
                           0000B6   588 _EIP_6	=	0x00b6
                           0000B7   589 _EIP_7	=	0x00b7
                           0000A8   590 _IE_0	=	0x00a8
                           0000A9   591 _IE_1	=	0x00a9
                           0000AA   592 _IE_2	=	0x00aa
                           0000AB   593 _IE_3	=	0x00ab
                           0000AC   594 _IE_4	=	0x00ac
                           0000AD   595 _IE_5	=	0x00ad
                           0000AE   596 _IE_6	=	0x00ae
                           0000AF   597 _IE_7	=	0x00af
                           0000AF   598 _EA	=	0x00af
                           0000B8   599 _IP_0	=	0x00b8
                           0000B9   600 _IP_1	=	0x00b9
                           0000BA   601 _IP_2	=	0x00ba
                           0000BB   602 _IP_3	=	0x00bb
                           0000BC   603 _IP_4	=	0x00bc
                           0000BD   604 _IP_5	=	0x00bd
                           0000BE   605 _IP_6	=	0x00be
                           0000BF   606 _IP_7	=	0x00bf
                           0000D0   607 _P	=	0x00d0
                           0000D1   608 _F1	=	0x00d1
                           0000D2   609 _OV	=	0x00d2
                           0000D3   610 _RS0	=	0x00d3
                           0000D4   611 _RS1	=	0x00d4
                           0000D5   612 _F0	=	0x00d5
                           0000D6   613 _AC	=	0x00d6
                           0000D7   614 _CY	=	0x00d7
                           0000C8   615 _PINA_0	=	0x00c8
                           0000C9   616 _PINA_1	=	0x00c9
                           0000CA   617 _PINA_2	=	0x00ca
                           0000CB   618 _PINA_3	=	0x00cb
                           0000CC   619 _PINA_4	=	0x00cc
                           0000CD   620 _PINA_5	=	0x00cd
                           0000CE   621 _PINA_6	=	0x00ce
                           0000CF   622 _PINA_7	=	0x00cf
                           0000E8   623 _PINB_0	=	0x00e8
                           0000E9   624 _PINB_1	=	0x00e9
                           0000EA   625 _PINB_2	=	0x00ea
                           0000EB   626 _PINB_3	=	0x00eb
                           0000EC   627 _PINB_4	=	0x00ec
                           0000ED   628 _PINB_5	=	0x00ed
                           0000EE   629 _PINB_6	=	0x00ee
                           0000EF   630 _PINB_7	=	0x00ef
                           0000F8   631 _PINC_0	=	0x00f8
                           0000F9   632 _PINC_1	=	0x00f9
                           0000FA   633 _PINC_2	=	0x00fa
                           0000FB   634 _PINC_3	=	0x00fb
                           0000FC   635 _PINC_4	=	0x00fc
                           0000FD   636 _PINC_5	=	0x00fd
                           0000FE   637 _PINC_6	=	0x00fe
                           0000FF   638 _PINC_7	=	0x00ff
                           000080   639 _PORTA_0	=	0x0080
                           000081   640 _PORTA_1	=	0x0081
                           000082   641 _PORTA_2	=	0x0082
                           000083   642 _PORTA_3	=	0x0083
                           000084   643 _PORTA_4	=	0x0084
                           000085   644 _PORTA_5	=	0x0085
                           000086   645 _PORTA_6	=	0x0086
                           000087   646 _PORTA_7	=	0x0087
                           000088   647 _PORTB_0	=	0x0088
                           000089   648 _PORTB_1	=	0x0089
                           00008A   649 _PORTB_2	=	0x008a
                           00008B   650 _PORTB_3	=	0x008b
                           00008C   651 _PORTB_4	=	0x008c
                           00008D   652 _PORTB_5	=	0x008d
                           00008E   653 _PORTB_6	=	0x008e
                           00008F   654 _PORTB_7	=	0x008f
                           000090   655 _PORTC_0	=	0x0090
                           000091   656 _PORTC_1	=	0x0091
                           000092   657 _PORTC_2	=	0x0092
                           000093   658 _PORTC_3	=	0x0093
                           000094   659 _PORTC_4	=	0x0094
                           000095   660 _PORTC_5	=	0x0095
                           000096   661 _PORTC_6	=	0x0096
                           000097   662 _PORTC_7	=	0x0097
                                    663 ;--------------------------------------------------------
                                    664 ; overlayable register banks
                                    665 ;--------------------------------------------------------
                                    666 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        667 	.ds 8
                                    668 ;--------------------------------------------------------
                                    669 ; internal ram data
                                    670 ;--------------------------------------------------------
                                    671 	.area DSEG    (DATA)
                                    672 ;--------------------------------------------------------
                                    673 ; overlayable items in internal ram 
                                    674 ;--------------------------------------------------------
                                    675 ;--------------------------------------------------------
                                    676 ; indirectly addressable internal ram data
                                    677 ;--------------------------------------------------------
                                    678 	.area ISEG    (DATA)
                                    679 ;--------------------------------------------------------
                                    680 ; absolute internal ram data
                                    681 ;--------------------------------------------------------
                                    682 	.area IABS    (ABS,DATA)
                                    683 	.area IABS    (ABS,DATA)
                                    684 ;--------------------------------------------------------
                                    685 ; bit data
                                    686 ;--------------------------------------------------------
                                    687 	.area BSEG    (BIT)
                                    688 ;--------------------------------------------------------
                                    689 ; paged external ram data
                                    690 ;--------------------------------------------------------
                                    691 	.area PSEG    (PAG,XDATA)
                                    692 ;--------------------------------------------------------
                                    693 ; external ram data
                                    694 ;--------------------------------------------------------
                                    695 	.area XSEG    (XDATA)
                           007020   696 _ADCCH0VAL0	=	0x7020
                           007021   697 _ADCCH0VAL1	=	0x7021
                           007020   698 _ADCCH0VAL	=	0x7020
                           007022   699 _ADCCH1VAL0	=	0x7022
                           007023   700 _ADCCH1VAL1	=	0x7023
                           007022   701 _ADCCH1VAL	=	0x7022
                           007024   702 _ADCCH2VAL0	=	0x7024
                           007025   703 _ADCCH2VAL1	=	0x7025
                           007024   704 _ADCCH2VAL	=	0x7024
                           007026   705 _ADCCH3VAL0	=	0x7026
                           007027   706 _ADCCH3VAL1	=	0x7027
                           007026   707 _ADCCH3VAL	=	0x7026
                           007028   708 _ADCTUNE0	=	0x7028
                           007029   709 _ADCTUNE1	=	0x7029
                           00702A   710 _ADCTUNE2	=	0x702a
                           007010   711 _DMA0ADDR0	=	0x7010
                           007011   712 _DMA0ADDR1	=	0x7011
                           007010   713 _DMA0ADDR	=	0x7010
                           007014   714 _DMA0CONFIG	=	0x7014
                           007012   715 _DMA1ADDR0	=	0x7012
                           007013   716 _DMA1ADDR1	=	0x7013
                           007012   717 _DMA1ADDR	=	0x7012
                           007015   718 _DMA1CONFIG	=	0x7015
                           007070   719 _FRCOSCCONFIG	=	0x7070
                           007071   720 _FRCOSCCTRL	=	0x7071
                           007076   721 _FRCOSCFREQ0	=	0x7076
                           007077   722 _FRCOSCFREQ1	=	0x7077
                           007076   723 _FRCOSCFREQ	=	0x7076
                           007072   724 _FRCOSCKFILT0	=	0x7072
                           007073   725 _FRCOSCKFILT1	=	0x7073
                           007072   726 _FRCOSCKFILT	=	0x7072
                           007078   727 _FRCOSCPER0	=	0x7078
                           007079   728 _FRCOSCPER1	=	0x7079
                           007078   729 _FRCOSCPER	=	0x7078
                           007074   730 _FRCOSCREF0	=	0x7074
                           007075   731 _FRCOSCREF1	=	0x7075
                           007074   732 _FRCOSCREF	=	0x7074
                           007007   733 _ANALOGA	=	0x7007
                           00700C   734 _GPIOENABLE	=	0x700c
                           007003   735 _EXTIRQ	=	0x7003
                           007000   736 _INTCHGA	=	0x7000
                           007001   737 _INTCHGB	=	0x7001
                           007002   738 _INTCHGC	=	0x7002
                           007008   739 _PALTA	=	0x7008
                           007009   740 _PALTB	=	0x7009
                           00700A   741 _PALTC	=	0x700a
                           007046   742 _PALTRADIO	=	0x7046
                           007004   743 _PINCHGA	=	0x7004
                           007005   744 _PINCHGB	=	0x7005
                           007006   745 _PINCHGC	=	0x7006
                           00700B   746 _PINSEL	=	0x700b
                           007060   747 _LPOSCCONFIG	=	0x7060
                           007066   748 _LPOSCFREQ0	=	0x7066
                           007067   749 _LPOSCFREQ1	=	0x7067
                           007066   750 _LPOSCFREQ	=	0x7066
                           007062   751 _LPOSCKFILT0	=	0x7062
                           007063   752 _LPOSCKFILT1	=	0x7063
                           007062   753 _LPOSCKFILT	=	0x7062
                           007068   754 _LPOSCPER0	=	0x7068
                           007069   755 _LPOSCPER1	=	0x7069
                           007068   756 _LPOSCPER	=	0x7068
                           007064   757 _LPOSCREF0	=	0x7064
                           007065   758 _LPOSCREF1	=	0x7065
                           007064   759 _LPOSCREF	=	0x7064
                           007054   760 _LPXOSCGM	=	0x7054
                           007F01   761 _MISCCTRL	=	0x7f01
                           007053   762 _OSCCALIB	=	0x7053
                           007050   763 _OSCFORCERUN	=	0x7050
                           007052   764 _OSCREADY	=	0x7052
                           007051   765 _OSCRUN	=	0x7051
                           007040   766 _RADIOFDATAADDR0	=	0x7040
                           007041   767 _RADIOFDATAADDR1	=	0x7041
                           007040   768 _RADIOFDATAADDR	=	0x7040
                           007042   769 _RADIOFSTATADDR0	=	0x7042
                           007043   770 _RADIOFSTATADDR1	=	0x7043
                           007042   771 _RADIOFSTATADDR	=	0x7042
                           007044   772 _RADIOMUX	=	0x7044
                           007084   773 _SCRATCH0	=	0x7084
                           007085   774 _SCRATCH1	=	0x7085
                           007086   775 _SCRATCH2	=	0x7086
                           007087   776 _SCRATCH3	=	0x7087
                           007F00   777 _SILICONREV	=	0x7f00
                           007F19   778 _XTALAMPL	=	0x7f19
                           007F18   779 _XTALOSC	=	0x7f18
                           007F1A   780 _XTALREADY	=	0x7f1a
                           00FC06   781 _flash_deviceid	=	0xfc06
                           00FC00   782 _flash_calsector	=	0xfc00
      000087                        783 _delaymstimer:
      000087                        784 	.ds 8
      00008F                        785 _wakeup_tmr1::
      00008F                        786 	.ds 8
      000097                        787 _wakeup_tmr2::
      000097                        788 	.ds 8
      00009F                        789 _wakeup_tmr3::
      00009F                        790 	.ds 8
      0000A7                        791 _delay_tx_PARM_2:
      0000A7                        792 	.ds 4
      0000AB                        793 _delay_tx_PARM_3:
      0000AB                        794 	.ds 4
      0000AF                        795 _delay_tx_time1_ms_1_222:
      0000AF                        796 	.ds 4
                                    797 ;--------------------------------------------------------
                                    798 ; absolute external ram data
                                    799 ;--------------------------------------------------------
                                    800 	.area XABS    (ABS,XDATA)
                                    801 ;--------------------------------------------------------
                                    802 ; external initialized ram data
                                    803 ;--------------------------------------------------------
                                    804 	.area XISEG   (XDATA)
                                    805 	.area HOME    (CODE)
                                    806 	.area GSINIT0 (CODE)
                                    807 	.area GSINIT1 (CODE)
                                    808 	.area GSINIT2 (CODE)
                                    809 	.area GSINIT3 (CODE)
                                    810 	.area GSINIT4 (CODE)
                                    811 	.area GSINIT5 (CODE)
                                    812 	.area GSINIT  (CODE)
                                    813 	.area GSFINAL (CODE)
                                    814 	.area CSEG    (CODE)
                                    815 ;--------------------------------------------------------
                                    816 ; global & static initialisations
                                    817 ;--------------------------------------------------------
                                    818 	.area HOME    (CODE)
                                    819 	.area GSINIT  (CODE)
                                    820 	.area GSFINAL (CODE)
                                    821 	.area GSINIT  (CODE)
                                    822 ;--------------------------------------------------------
                                    823 ; Home
                                    824 ;--------------------------------------------------------
                                    825 	.area HOME    (CODE)
                                    826 	.area HOME    (CODE)
                                    827 ;--------------------------------------------------------
                                    828 ; code
                                    829 ;--------------------------------------------------------
                                    830 	.area CSEG    (CODE)
                                    831 ;------------------------------------------------------------
                                    832 ;Allocation info for local variables in function 'delayms_callback'
                                    833 ;------------------------------------------------------------
                                    834 ;desc                      Allocated with name '_delayms_callback_desc_1_211'
                                    835 ;------------------------------------------------------------
                                    836 ;	..\src\atrs\misc.c:67: static void delayms_callback(struct wtimer_desc __xdata *desc)
                                    837 ;	-----------------------------------------
                                    838 ;	 function delayms_callback
                                    839 ;	-----------------------------------------
      001072                        840 _delayms_callback:
                           000007   841 	ar7 = 0x07
                           000006   842 	ar6 = 0x06
                           000005   843 	ar5 = 0x05
                           000004   844 	ar4 = 0x04
                           000003   845 	ar3 = 0x03
                           000002   846 	ar2 = 0x02
                           000001   847 	ar1 = 0x01
                           000000   848 	ar0 = 0x00
                                    849 ;	..\src\atrs\misc.c:70: delaymstimer.handler = 0;
      001072 90 00 89         [24]  850 	mov	dptr,#(_delaymstimer + 0x0002)
      001075 E4               [12]  851 	clr	a
      001076 F0               [24]  852 	movx	@dptr,a
      001077 A3               [24]  853 	inc	dptr
      001078 F0               [24]  854 	movx	@dptr,a
      001079 22               [24]  855 	ret
                                    856 ;------------------------------------------------------------
                                    857 ;Allocation info for local variables in function 'delay_ms'
                                    858 ;------------------------------------------------------------
                                    859 ;ms                        Allocated to registers r6 r7 
                                    860 ;x                         Allocated to stack - _bp +1
                                    861 ;------------------------------------------------------------
                                    862 ;	..\src\atrs\misc.c:77: __reentrantb void delay_ms(uint16_t ms) __reentrant
                                    863 ;	-----------------------------------------
                                    864 ;	 function delay_ms
                                    865 ;	-----------------------------------------
      00107A                        866 _delay_ms:
      00107A C0 1F            [24]  867 	push	_bp
      00107C E5 81            [12]  868 	mov	a,sp
      00107E F5 1F            [12]  869 	mov	_bp,a
      001080 24 04            [12]  870 	add	a,#0x04
      001082 F5 81            [12]  871 	mov	sp,a
      001084 AE 82            [24]  872 	mov	r6,dpl
      001086 AF 83            [24]  873 	mov	r7,dph
                                    874 ;	..\src\atrs\misc.c:81: wtimer_remove(&delaymstimer);
      001088 90 00 87         [24]  875 	mov	dptr,#_delaymstimer
      00108B C0 07            [24]  876 	push	ar7
      00108D C0 06            [24]  877 	push	ar6
      00108F 12 71 84         [24]  878 	lcall	_wtimer_remove
      001092 D0 06            [24]  879 	pop	ar6
      001094 D0 07            [24]  880 	pop	ar7
                                    881 ;	..\src\atrs\misc.c:82: x = ms;
      001096 A8 1F            [24]  882 	mov	r0,_bp
      001098 08               [12]  883 	inc	r0
      001099 A6 06            [24]  884 	mov	@r0,ar6
      00109B 08               [12]  885 	inc	r0
      00109C A6 07            [24]  886 	mov	@r0,ar7
      00109E 08               [12]  887 	inc	r0
      00109F 76 00            [12]  888 	mov	@r0,#0x00
      0010A1 08               [12]  889 	inc	r0
      0010A2 76 00            [12]  890 	mov	@r0,#0x00
                                    891 ;	..\src\atrs\misc.c:83: delaymstimer.time = ms >> 1;
      0010A4 EF               [12]  892 	mov	a,r7
      0010A5 C3               [12]  893 	clr	c
      0010A6 13               [12]  894 	rrc	a
      0010A7 CE               [12]  895 	xch	a,r6
      0010A8 13               [12]  896 	rrc	a
      0010A9 CE               [12]  897 	xch	a,r6
      0010AA FF               [12]  898 	mov	r7,a
      0010AB 8E 04            [24]  899 	mov	ar4,r6
      0010AD 8F 05            [24]  900 	mov	ar5,r7
      0010AF 7E 00            [12]  901 	mov	r6,#0x00
      0010B1 7F 00            [12]  902 	mov	r7,#0x00
      0010B3 90 00 8B         [24]  903 	mov	dptr,#(_delaymstimer + 0x0004)
      0010B6 EC               [12]  904 	mov	a,r4
      0010B7 F0               [24]  905 	movx	@dptr,a
      0010B8 ED               [12]  906 	mov	a,r5
      0010B9 A3               [24]  907 	inc	dptr
      0010BA F0               [24]  908 	movx	@dptr,a
      0010BB EE               [12]  909 	mov	a,r6
      0010BC A3               [24]  910 	inc	dptr
      0010BD F0               [24]  911 	movx	@dptr,a
      0010BE EF               [12]  912 	mov	a,r7
      0010BF A3               [24]  913 	inc	dptr
      0010C0 F0               [24]  914 	movx	@dptr,a
                                    915 ;	..\src\atrs\misc.c:84: x <<= 3;
      0010C1 A8 1F            [24]  916 	mov	r0,_bp
      0010C3 08               [12]  917 	inc	r0
      0010C4 08               [12]  918 	inc	r0
      0010C5 08               [12]  919 	inc	r0
      0010C6 08               [12]  920 	inc	r0
      0010C7 E6               [12]  921 	mov	a,@r0
      0010C8 18               [12]  922 	dec	r0
      0010C9 C4               [12]  923 	swap	a
      0010CA 03               [12]  924 	rr	a
      0010CB 54 F8            [12]  925 	anl	a,#0xf8
      0010CD C6               [12]  926 	xch	a,@r0
      0010CE C4               [12]  927 	swap	a
      0010CF 03               [12]  928 	rr	a
      0010D0 C6               [12]  929 	xch	a,@r0
      0010D1 66               [12]  930 	xrl	a,@r0
      0010D2 C6               [12]  931 	xch	a,@r0
      0010D3 54 F8            [12]  932 	anl	a,#0xf8
      0010D5 C6               [12]  933 	xch	a,@r0
      0010D6 66               [12]  934 	xrl	a,@r0
      0010D7 08               [12]  935 	inc	r0
      0010D8 F6               [12]  936 	mov	@r0,a
      0010D9 18               [12]  937 	dec	r0
      0010DA 18               [12]  938 	dec	r0
      0010DB E6               [12]  939 	mov	a,@r0
      0010DC C4               [12]  940 	swap	a
      0010DD 03               [12]  941 	rr	a
      0010DE 54 07            [12]  942 	anl	a,#0x07
      0010E0 08               [12]  943 	inc	r0
      0010E1 46               [12]  944 	orl	a,@r0
      0010E2 F6               [12]  945 	mov	@r0,a
      0010E3 18               [12]  946 	dec	r0
      0010E4 E6               [12]  947 	mov	a,@r0
      0010E5 18               [12]  948 	dec	r0
      0010E6 C4               [12]  949 	swap	a
      0010E7 03               [12]  950 	rr	a
      0010E8 54 F8            [12]  951 	anl	a,#0xf8
      0010EA C6               [12]  952 	xch	a,@r0
      0010EB C4               [12]  953 	swap	a
      0010EC 03               [12]  954 	rr	a
      0010ED C6               [12]  955 	xch	a,@r0
      0010EE 66               [12]  956 	xrl	a,@r0
      0010EF C6               [12]  957 	xch	a,@r0
      0010F0 54 F8            [12]  958 	anl	a,#0xf8
      0010F2 C6               [12]  959 	xch	a,@r0
      0010F3 66               [12]  960 	xrl	a,@r0
      0010F4 08               [12]  961 	inc	r0
      0010F5 F6               [12]  962 	mov	@r0,a
                                    963 ;	..\src\atrs\misc.c:85: delaymstimer.time -= x;
      0010F6 A8 1F            [24]  964 	mov	r0,_bp
      0010F8 08               [12]  965 	inc	r0
      0010F9 EC               [12]  966 	mov	a,r4
      0010FA C3               [12]  967 	clr	c
      0010FB 96               [12]  968 	subb	a,@r0
      0010FC FC               [12]  969 	mov	r4,a
      0010FD ED               [12]  970 	mov	a,r5
      0010FE 08               [12]  971 	inc	r0
      0010FF 96               [12]  972 	subb	a,@r0
      001100 FD               [12]  973 	mov	r5,a
      001101 EE               [12]  974 	mov	a,r6
      001102 08               [12]  975 	inc	r0
      001103 96               [12]  976 	subb	a,@r0
      001104 FE               [12]  977 	mov	r6,a
      001105 EF               [12]  978 	mov	a,r7
      001106 08               [12]  979 	inc	r0
      001107 96               [12]  980 	subb	a,@r0
      001108 FF               [12]  981 	mov	r7,a
      001109 90 00 8B         [24]  982 	mov	dptr,#(_delaymstimer + 0x0004)
      00110C EC               [12]  983 	mov	a,r4
      00110D F0               [24]  984 	movx	@dptr,a
      00110E ED               [12]  985 	mov	a,r5
      00110F A3               [24]  986 	inc	dptr
      001110 F0               [24]  987 	movx	@dptr,a
      001111 EE               [12]  988 	mov	a,r6
      001112 A3               [24]  989 	inc	dptr
      001113 F0               [24]  990 	movx	@dptr,a
      001114 EF               [12]  991 	mov	a,r7
      001115 A3               [24]  992 	inc	dptr
      001116 F0               [24]  993 	movx	@dptr,a
                                    994 ;	..\src\atrs\misc.c:86: x <<= 3;
      001117 A8 1F            [24]  995 	mov	r0,_bp
      001119 08               [12]  996 	inc	r0
      00111A 08               [12]  997 	inc	r0
      00111B 08               [12]  998 	inc	r0
      00111C 08               [12]  999 	inc	r0
      00111D E6               [12] 1000 	mov	a,@r0
      00111E 18               [12] 1001 	dec	r0
      00111F C4               [12] 1002 	swap	a
      001120 03               [12] 1003 	rr	a
      001121 54 F8            [12] 1004 	anl	a,#0xf8
      001123 C6               [12] 1005 	xch	a,@r0
      001124 C4               [12] 1006 	swap	a
      001125 03               [12] 1007 	rr	a
      001126 C6               [12] 1008 	xch	a,@r0
      001127 66               [12] 1009 	xrl	a,@r0
      001128 C6               [12] 1010 	xch	a,@r0
      001129 54 F8            [12] 1011 	anl	a,#0xf8
      00112B C6               [12] 1012 	xch	a,@r0
      00112C 66               [12] 1013 	xrl	a,@r0
      00112D 08               [12] 1014 	inc	r0
      00112E F6               [12] 1015 	mov	@r0,a
      00112F 18               [12] 1016 	dec	r0
      001130 18               [12] 1017 	dec	r0
      001131 E6               [12] 1018 	mov	a,@r0
      001132 C4               [12] 1019 	swap	a
      001133 03               [12] 1020 	rr	a
      001134 54 07            [12] 1021 	anl	a,#0x07
      001136 08               [12] 1022 	inc	r0
      001137 46               [12] 1023 	orl	a,@r0
      001138 F6               [12] 1024 	mov	@r0,a
      001139 18               [12] 1025 	dec	r0
      00113A E6               [12] 1026 	mov	a,@r0
      00113B 18               [12] 1027 	dec	r0
      00113C C4               [12] 1028 	swap	a
      00113D 03               [12] 1029 	rr	a
      00113E 54 F8            [12] 1030 	anl	a,#0xf8
      001140 C6               [12] 1031 	xch	a,@r0
      001141 C4               [12] 1032 	swap	a
      001142 03               [12] 1033 	rr	a
      001143 C6               [12] 1034 	xch	a,@r0
      001144 66               [12] 1035 	xrl	a,@r0
      001145 C6               [12] 1036 	xch	a,@r0
      001146 54 F8            [12] 1037 	anl	a,#0xf8
      001148 C6               [12] 1038 	xch	a,@r0
      001149 66               [12] 1039 	xrl	a,@r0
      00114A 08               [12] 1040 	inc	r0
      00114B F6               [12] 1041 	mov	@r0,a
                                   1042 ;	..\src\atrs\misc.c:87: delaymstimer.time += x;
      00114C A8 1F            [24] 1043 	mov	r0,_bp
      00114E 08               [12] 1044 	inc	r0
      00114F E6               [12] 1045 	mov	a,@r0
      001150 2C               [12] 1046 	add	a,r4
      001151 FC               [12] 1047 	mov	r4,a
      001152 08               [12] 1048 	inc	r0
      001153 E6               [12] 1049 	mov	a,@r0
      001154 3D               [12] 1050 	addc	a,r5
      001155 FD               [12] 1051 	mov	r5,a
      001156 08               [12] 1052 	inc	r0
      001157 E6               [12] 1053 	mov	a,@r0
      001158 3E               [12] 1054 	addc	a,r6
      001159 FE               [12] 1055 	mov	r6,a
      00115A 08               [12] 1056 	inc	r0
      00115B E6               [12] 1057 	mov	a,@r0
      00115C 3F               [12] 1058 	addc	a,r7
      00115D FF               [12] 1059 	mov	r7,a
      00115E 90 00 8B         [24] 1060 	mov	dptr,#(_delaymstimer + 0x0004)
      001161 EC               [12] 1061 	mov	a,r4
      001162 F0               [24] 1062 	movx	@dptr,a
      001163 ED               [12] 1063 	mov	a,r5
      001164 A3               [24] 1064 	inc	dptr
      001165 F0               [24] 1065 	movx	@dptr,a
      001166 EE               [12] 1066 	mov	a,r6
      001167 A3               [24] 1067 	inc	dptr
      001168 F0               [24] 1068 	movx	@dptr,a
      001169 EF               [12] 1069 	mov	a,r7
      00116A A3               [24] 1070 	inc	dptr
      00116B F0               [24] 1071 	movx	@dptr,a
                                   1072 ;	..\src\atrs\misc.c:88: x <<= 2;
      00116C A8 1F            [24] 1073 	mov	r0,_bp
      00116E 08               [12] 1074 	inc	r0
      00116F E6               [12] 1075 	mov	a,@r0
      001170 25 E0            [12] 1076 	add	a,acc
      001172 F6               [12] 1077 	mov	@r0,a
      001173 08               [12] 1078 	inc	r0
      001174 E6               [12] 1079 	mov	a,@r0
      001175 33               [12] 1080 	rlc	a
      001176 F6               [12] 1081 	mov	@r0,a
      001177 08               [12] 1082 	inc	r0
      001178 E6               [12] 1083 	mov	a,@r0
      001179 33               [12] 1084 	rlc	a
      00117A F6               [12] 1085 	mov	@r0,a
      00117B 08               [12] 1086 	inc	r0
      00117C E6               [12] 1087 	mov	a,@r0
      00117D 33               [12] 1088 	rlc	a
      00117E F6               [12] 1089 	mov	@r0,a
      00117F 18               [12] 1090 	dec	r0
      001180 18               [12] 1091 	dec	r0
      001181 18               [12] 1092 	dec	r0
      001182 E6               [12] 1093 	mov	a,@r0
      001183 25 E0            [12] 1094 	add	a,acc
      001185 F6               [12] 1095 	mov	@r0,a
      001186 08               [12] 1096 	inc	r0
      001187 E6               [12] 1097 	mov	a,@r0
      001188 33               [12] 1098 	rlc	a
      001189 F6               [12] 1099 	mov	@r0,a
      00118A 08               [12] 1100 	inc	r0
      00118B E6               [12] 1101 	mov	a,@r0
      00118C 33               [12] 1102 	rlc	a
      00118D F6               [12] 1103 	mov	@r0,a
      00118E 08               [12] 1104 	inc	r0
      00118F E6               [12] 1105 	mov	a,@r0
      001190 33               [12] 1106 	rlc	a
      001191 F6               [12] 1107 	mov	@r0,a
                                   1108 ;	..\src\atrs\misc.c:89: delaymstimer.time += x;
      001192 A8 1F            [24] 1109 	mov	r0,_bp
      001194 08               [12] 1110 	inc	r0
      001195 E6               [12] 1111 	mov	a,@r0
      001196 2C               [12] 1112 	add	a,r4
      001197 FC               [12] 1113 	mov	r4,a
      001198 08               [12] 1114 	inc	r0
      001199 E6               [12] 1115 	mov	a,@r0
      00119A 3D               [12] 1116 	addc	a,r5
      00119B FD               [12] 1117 	mov	r5,a
      00119C 08               [12] 1118 	inc	r0
      00119D E6               [12] 1119 	mov	a,@r0
      00119E 3E               [12] 1120 	addc	a,r6
      00119F FE               [12] 1121 	mov	r6,a
      0011A0 08               [12] 1122 	inc	r0
      0011A1 E6               [12] 1123 	mov	a,@r0
      0011A2 3F               [12] 1124 	addc	a,r7
      0011A3 FF               [12] 1125 	mov	r7,a
      0011A4 90 00 8B         [24] 1126 	mov	dptr,#(_delaymstimer + 0x0004)
      0011A7 EC               [12] 1127 	mov	a,r4
      0011A8 F0               [24] 1128 	movx	@dptr,a
      0011A9 ED               [12] 1129 	mov	a,r5
      0011AA A3               [24] 1130 	inc	dptr
      0011AB F0               [24] 1131 	movx	@dptr,a
      0011AC EE               [12] 1132 	mov	a,r6
      0011AD A3               [24] 1133 	inc	dptr
      0011AE F0               [24] 1134 	movx	@dptr,a
      0011AF EF               [12] 1135 	mov	a,r7
      0011B0 A3               [24] 1136 	inc	dptr
      0011B1 F0               [24] 1137 	movx	@dptr,a
                                   1138 ;	..\src\atrs\misc.c:90: delaymstimer.handler = delayms_callback;
      0011B2 90 00 89         [24] 1139 	mov	dptr,#(_delaymstimer + 0x0002)
      0011B5 74 72            [12] 1140 	mov	a,#_delayms_callback
      0011B7 F0               [24] 1141 	movx	@dptr,a
      0011B8 74 10            [12] 1142 	mov	a,#(_delayms_callback >> 8)
      0011BA A3               [24] 1143 	inc	dptr
      0011BB F0               [24] 1144 	movx	@dptr,a
                                   1145 ;	..\src\atrs\misc.c:91: wtimer1_addrelative(&delaymstimer);
      0011BC 90 00 87         [24] 1146 	mov	dptr,#_delaymstimer
      0011BF 12 66 6C         [24] 1147 	lcall	_wtimer1_addrelative
                                   1148 ;	..\src\atrs\misc.c:92: wtimer_runcallbacks();
      0011C2 12 64 92         [24] 1149 	lcall	_wtimer_runcallbacks
                                   1150 ;	..\src\atrs\misc.c:93: do {
      0011C5                       1151 00101$:
                                   1152 ;	..\src\atrs\misc.c:94: wtimer_idle(WTFLAG_CANSTANDBY);
      0011C5 75 82 02         [24] 1153 	mov	dpl,#0x02
      0011C8 12 65 13         [24] 1154 	lcall	_wtimer_idle
                                   1155 ;	..\src\atrs\misc.c:95: wtimer_runcallbacks();
      0011CB 12 64 92         [24] 1156 	lcall	_wtimer_runcallbacks
                                   1157 ;	..\src\atrs\misc.c:96: } while (delaymstimer.handler);
      0011CE 90 00 89         [24] 1158 	mov	dptr,#(_delaymstimer + 0x0002)
      0011D1 E0               [24] 1159 	movx	a,@dptr
      0011D2 FE               [12] 1160 	mov	r6,a
      0011D3 A3               [24] 1161 	inc	dptr
      0011D4 E0               [24] 1162 	movx	a,@dptr
      0011D5 FF               [12] 1163 	mov	r7,a
      0011D6 4E               [12] 1164 	orl	a,r6
      0011D7 70 EC            [24] 1165 	jnz	00101$
      0011D9 85 1F 81         [24] 1166 	mov	sp,_bp
      0011DC D0 1F            [24] 1167 	pop	_bp
      0011DE 22               [24] 1168 	ret
                                   1169 ;------------------------------------------------------------
                                   1170 ;Allocation info for local variables in function 'wakeup_callback1'
                                   1171 ;------------------------------------------------------------
                                   1172 ;desc                      Allocated with name '_wakeup_callback1_desc_1_216'
                                   1173 ;------------------------------------------------------------
                                   1174 ;	..\src\atrs\misc.c:100: static void wakeup_callback1(struct wtimer_desc __xdata *desc)
                                   1175 ;	-----------------------------------------
                                   1176 ;	 function wakeup_callback1
                                   1177 ;	-----------------------------------------
      0011DF                       1178 _wakeup_callback1:
                                   1179 ;	..\src\atrs\misc.c:105: dbglink_writestr(" timer1 ");
      0011DF 90 7B 57         [24] 1180 	mov	dptr,#___str_0
      0011E2 75 F0 80         [24] 1181 	mov	b,#0x80
      0011E5 12 6E FA         [24] 1182 	lcall	_dbglink_writestr
                                   1183 ;	..\src\atrs\misc.c:109: transmit_packet();
      0011E8 02 0B 91         [24] 1184 	ljmp	_transmit_packet
                                   1185 ;------------------------------------------------------------
                                   1186 ;Allocation info for local variables in function 'wakeup_callback2'
                                   1187 ;------------------------------------------------------------
                                   1188 ;desc                      Allocated with name '_wakeup_callback2_desc_1_218'
                                   1189 ;------------------------------------------------------------
                                   1190 ;	..\src\atrs\misc.c:113: static void wakeup_callback2(struct wtimer_desc __xdata *desc)
                                   1191 ;	-----------------------------------------
                                   1192 ;	 function wakeup_callback2
                                   1193 ;	-----------------------------------------
      0011EB                       1194 _wakeup_callback2:
                                   1195 ;	..\src\atrs\misc.c:118: dbglink_writestr(" timer2 ");
      0011EB 90 7B 60         [24] 1196 	mov	dptr,#___str_1
      0011EE 75 F0 80         [24] 1197 	mov	b,#0x80
      0011F1 12 6E FA         [24] 1198 	lcall	_dbglink_writestr
                                   1199 ;	..\src\atrs\misc.c:122: transmit_packet();
      0011F4 02 0B 91         [24] 1200 	ljmp	_transmit_packet
                                   1201 ;------------------------------------------------------------
                                   1202 ;Allocation info for local variables in function 'wakeup_callback3'
                                   1203 ;------------------------------------------------------------
                                   1204 ;desc                      Allocated with name '_wakeup_callback3_desc_1_220'
                                   1205 ;------------------------------------------------------------
                                   1206 ;	..\src\atrs\misc.c:126: static void wakeup_callback3(struct wtimer_desc __xdata *desc)
                                   1207 ;	-----------------------------------------
                                   1208 ;	 function wakeup_callback3
                                   1209 ;	-----------------------------------------
      0011F7                       1210 _wakeup_callback3:
                                   1211 ;	..\src\atrs\misc.c:131: dbglink_writestr(" timer3 ");
      0011F7 90 7B 69         [24] 1212 	mov	dptr,#___str_2
      0011FA 75 F0 80         [24] 1213 	mov	b,#0x80
      0011FD 12 6E FA         [24] 1214 	lcall	_dbglink_writestr
                                   1215 ;	..\src\atrs\misc.c:135: transmit_packet();
      001200 02 0B 91         [24] 1216 	ljmp	_transmit_packet
                                   1217 ;------------------------------------------------------------
                                   1218 ;Allocation info for local variables in function 'delay_tx'
                                   1219 ;------------------------------------------------------------
                                   1220 ;time2_ms                  Allocated with name '_delay_tx_PARM_2'
                                   1221 ;time3_ms                  Allocated with name '_delay_tx_PARM_3'
                                   1222 ;time1_ms                  Allocated with name '_delay_tx_time1_ms_1_222'
                                   1223 ;------------------------------------------------------------
                                   1224 ;	..\src\atrs\misc.c:152: void delay_tx(uint32_t time1_ms, uint32_t time2_ms, uint32_t time3_ms)
                                   1225 ;	-----------------------------------------
                                   1226 ;	 function delay_tx
                                   1227 ;	-----------------------------------------
      001203                       1228 _delay_tx:
      001203 AF 82            [24] 1229 	mov	r7,dpl
      001205 AE 83            [24] 1230 	mov	r6,dph
      001207 AD F0            [24] 1231 	mov	r5,b
      001209 FC               [12] 1232 	mov	r4,a
      00120A 90 00 AF         [24] 1233 	mov	dptr,#_delay_tx_time1_ms_1_222
      00120D EF               [12] 1234 	mov	a,r7
      00120E F0               [24] 1235 	movx	@dptr,a
      00120F EE               [12] 1236 	mov	a,r6
      001210 A3               [24] 1237 	inc	dptr
      001211 F0               [24] 1238 	movx	@dptr,a
      001212 ED               [12] 1239 	mov	a,r5
      001213 A3               [24] 1240 	inc	dptr
      001214 F0               [24] 1241 	movx	@dptr,a
      001215 EC               [12] 1242 	mov	a,r4
      001216 A3               [24] 1243 	inc	dptr
      001217 F0               [24] 1244 	movx	@dptr,a
                                   1245 ;	..\src\atrs\misc.c:157: wakeup_tmr1.handler = wakeup_callback1;//Handler for timer 0
      001218 90 00 91         [24] 1246 	mov	dptr,#(_wakeup_tmr1 + 0x0002)
      00121B 74 DF            [12] 1247 	mov	a,#_wakeup_callback1
      00121D F0               [24] 1248 	movx	@dptr,a
      00121E 74 11            [12] 1249 	mov	a,#(_wakeup_callback1 >> 8)
      001220 A3               [24] 1250 	inc	dptr
      001221 F0               [24] 1251 	movx	@dptr,a
                                   1252 ;	..\src\atrs\misc.c:158: time1_ms = (uint32_t) TIME_FOR_TIMER_MSEC(time1_ms);
      001222 90 00 AF         [24] 1253 	mov	dptr,#_delay_tx_time1_ms_1_222
      001225 E0               [24] 1254 	movx	a,@dptr
      001226 FC               [12] 1255 	mov	r4,a
      001227 A3               [24] 1256 	inc	dptr
      001228 E0               [24] 1257 	movx	a,@dptr
      001229 FD               [12] 1258 	mov	r5,a
      00122A A3               [24] 1259 	inc	dptr
      00122B E0               [24] 1260 	movx	a,@dptr
      00122C FE               [12] 1261 	mov	r6,a
      00122D A3               [24] 1262 	inc	dptr
      00122E E0               [24] 1263 	movx	a,@dptr
      00122F 8C 82            [24] 1264 	mov	dpl,r4
      001231 8D 83            [24] 1265 	mov	dph,r5
      001233 8E F0            [24] 1266 	mov	b,r6
      001235 12 66 B3         [24] 1267 	lcall	___ulong2fs
      001238 AC 82            [24] 1268 	mov	r4,dpl
      00123A AD 83            [24] 1269 	mov	r5,dph
      00123C AE F0            [24] 1270 	mov	r6,b
      00123E FF               [12] 1271 	mov	r7,a
      00123F C0 04            [24] 1272 	push	ar4
      001241 C0 05            [24] 1273 	push	ar5
      001243 C0 06            [24] 1274 	push	ar6
      001245 C0 07            [24] 1275 	push	ar7
      001247 90 00 00         [24] 1276 	mov	dptr,#0x0000
      00124A 75 F0 20         [24] 1277 	mov	b,#0x20
      00124D 74 44            [12] 1278 	mov	a,#0x44
      00124F 12 5C 9F         [24] 1279 	lcall	___fsmul
      001252 AC 82            [24] 1280 	mov	r4,dpl
      001254 AD 83            [24] 1281 	mov	r5,dph
      001256 AE F0            [24] 1282 	mov	r6,b
      001258 FF               [12] 1283 	mov	r7,a
      001259 E5 81            [12] 1284 	mov	a,sp
      00125B 24 FC            [12] 1285 	add	a,#0xfc
      00125D F5 81            [12] 1286 	mov	sp,a
      00125F E4               [12] 1287 	clr	a
      001260 C0 E0            [24] 1288 	push	acc
      001262 C0 E0            [24] 1289 	push	acc
      001264 74 7A            [12] 1290 	mov	a,#0x7a
      001266 C0 E0            [24] 1291 	push	acc
      001268 74 44            [12] 1292 	mov	a,#0x44
      00126A C0 E0            [24] 1293 	push	acc
      00126C 8C 82            [24] 1294 	mov	dpl,r4
      00126E 8D 83            [24] 1295 	mov	dph,r5
      001270 8E F0            [24] 1296 	mov	b,r6
      001272 EF               [12] 1297 	mov	a,r7
      001273 12 77 66         [24] 1298 	lcall	___fsdiv
      001276 AC 82            [24] 1299 	mov	r4,dpl
      001278 AD 83            [24] 1300 	mov	r5,dph
      00127A AE F0            [24] 1301 	mov	r6,b
      00127C FF               [12] 1302 	mov	r7,a
      00127D E5 81            [12] 1303 	mov	a,sp
      00127F 24 FC            [12] 1304 	add	a,#0xfc
      001281 F5 81            [12] 1305 	mov	sp,a
      001283 8C 82            [24] 1306 	mov	dpl,r4
      001285 8D 83            [24] 1307 	mov	dph,r5
      001287 8E F0            [24] 1308 	mov	b,r6
      001289 EF               [12] 1309 	mov	a,r7
      00128A 12 66 C6         [24] 1310 	lcall	___fs2ulong
      00128D AC 82            [24] 1311 	mov	r4,dpl
      00128F AD 83            [24] 1312 	mov	r5,dph
      001291 AE F0            [24] 1313 	mov	r6,b
      001293 FF               [12] 1314 	mov	r7,a
      001294 90 00 AF         [24] 1315 	mov	dptr,#_delay_tx_time1_ms_1_222
      001297 EC               [12] 1316 	mov	a,r4
      001298 F0               [24] 1317 	movx	@dptr,a
      001299 ED               [12] 1318 	mov	a,r5
      00129A A3               [24] 1319 	inc	dptr
      00129B F0               [24] 1320 	movx	@dptr,a
      00129C EE               [12] 1321 	mov	a,r6
      00129D A3               [24] 1322 	inc	dptr
      00129E F0               [24] 1323 	movx	@dptr,a
      00129F EF               [12] 1324 	mov	a,r7
      0012A0 A3               [24] 1325 	inc	dptr
      0012A1 F0               [24] 1326 	movx	@dptr,a
                                   1327 ;	..\src\atrs\misc.c:159: dbglink_writestr("timer1: ");
      0012A2 90 7B 72         [24] 1328 	mov	dptr,#___str_3
      0012A5 75 F0 80         [24] 1329 	mov	b,#0x80
      0012A8 12 6E FA         [24] 1330 	lcall	_dbglink_writestr
                                   1331 ;	..\src\atrs\misc.c:160: dbglink_writehex32(time1_ms,1,WRNUM_PADZERO);
      0012AB 90 00 AF         [24] 1332 	mov	dptr,#_delay_tx_time1_ms_1_222
      0012AE E0               [24] 1333 	movx	a,@dptr
      0012AF FC               [12] 1334 	mov	r4,a
      0012B0 A3               [24] 1335 	inc	dptr
      0012B1 E0               [24] 1336 	movx	a,@dptr
      0012B2 FD               [12] 1337 	mov	r5,a
      0012B3 A3               [24] 1338 	inc	dptr
      0012B4 E0               [24] 1339 	movx	a,@dptr
      0012B5 FE               [12] 1340 	mov	r6,a
      0012B6 A3               [24] 1341 	inc	dptr
      0012B7 E0               [24] 1342 	movx	a,@dptr
      0012B8 FF               [12] 1343 	mov	r7,a
      0012B9 C0 07            [24] 1344 	push	ar7
      0012BB C0 06            [24] 1345 	push	ar6
      0012BD C0 05            [24] 1346 	push	ar5
      0012BF C0 04            [24] 1347 	push	ar4
      0012C1 74 08            [12] 1348 	mov	a,#0x08
      0012C3 C0 E0            [24] 1349 	push	acc
      0012C5 74 01            [12] 1350 	mov	a,#0x01
      0012C7 C0 E0            [24] 1351 	push	acc
      0012C9 8C 82            [24] 1352 	mov	dpl,r4
      0012CB 8D 83            [24] 1353 	mov	dph,r5
      0012CD 8E F0            [24] 1354 	mov	b,r6
      0012CF EF               [12] 1355 	mov	a,r7
      0012D0 12 73 99         [24] 1356 	lcall	_dbglink_writehex32
      0012D3 15 81            [12] 1357 	dec	sp
      0012D5 15 81            [12] 1358 	dec	sp
      0012D7 D0 04            [24] 1359 	pop	ar4
      0012D9 D0 05            [24] 1360 	pop	ar5
      0012DB D0 06            [24] 1361 	pop	ar6
      0012DD D0 07            [24] 1362 	pop	ar7
                                   1363 ;	..\src\atrs\misc.c:161: wakeup_tmr1.time = time1_ms; //+= ? o solo = ? verificar esto
      0012DF 90 00 93         [24] 1364 	mov	dptr,#(_wakeup_tmr1 + 0x0004)
      0012E2 EC               [12] 1365 	mov	a,r4
      0012E3 F0               [24] 1366 	movx	@dptr,a
      0012E4 ED               [12] 1367 	mov	a,r5
      0012E5 A3               [24] 1368 	inc	dptr
      0012E6 F0               [24] 1369 	movx	@dptr,a
      0012E7 EE               [12] 1370 	mov	a,r6
      0012E8 A3               [24] 1371 	inc	dptr
      0012E9 F0               [24] 1372 	movx	@dptr,a
      0012EA EF               [12] 1373 	mov	a,r7
      0012EB A3               [24] 1374 	inc	dptr
      0012EC F0               [24] 1375 	movx	@dptr,a
                                   1376 ;	..\src\atrs\misc.c:162: wtimer0_addabsolute(&wakeup_tmr1);
      0012ED 90 00 8F         [24] 1377 	mov	dptr,#_wakeup_tmr1
      0012F0 12 67 3B         [24] 1378 	lcall	_wtimer0_addabsolute
                                   1379 ;	..\src\atrs\misc.c:164: wakeup_tmr2.handler = wakeup_callback2;//Handler for timer 0
      0012F3 90 00 99         [24] 1380 	mov	dptr,#(_wakeup_tmr2 + 0x0002)
      0012F6 74 EB            [12] 1381 	mov	a,#_wakeup_callback2
      0012F8 F0               [24] 1382 	movx	@dptr,a
      0012F9 74 11            [12] 1383 	mov	a,#(_wakeup_callback2 >> 8)
      0012FB A3               [24] 1384 	inc	dptr
      0012FC F0               [24] 1385 	movx	@dptr,a
                                   1386 ;	..\src\atrs\misc.c:165: time2_ms = (uint32_t) TIME_FOR_TIMER_MSEC(time2_ms);
      0012FD 90 00 A7         [24] 1387 	mov	dptr,#_delay_tx_PARM_2
      001300 E0               [24] 1388 	movx	a,@dptr
      001301 FC               [12] 1389 	mov	r4,a
      001302 A3               [24] 1390 	inc	dptr
      001303 E0               [24] 1391 	movx	a,@dptr
      001304 FD               [12] 1392 	mov	r5,a
      001305 A3               [24] 1393 	inc	dptr
      001306 E0               [24] 1394 	movx	a,@dptr
      001307 FE               [12] 1395 	mov	r6,a
      001308 A3               [24] 1396 	inc	dptr
      001309 E0               [24] 1397 	movx	a,@dptr
      00130A 8C 82            [24] 1398 	mov	dpl,r4
      00130C 8D 83            [24] 1399 	mov	dph,r5
      00130E 8E F0            [24] 1400 	mov	b,r6
      001310 12 66 B3         [24] 1401 	lcall	___ulong2fs
      001313 AC 82            [24] 1402 	mov	r4,dpl
      001315 AD 83            [24] 1403 	mov	r5,dph
      001317 AE F0            [24] 1404 	mov	r6,b
      001319 FF               [12] 1405 	mov	r7,a
      00131A C0 04            [24] 1406 	push	ar4
      00131C C0 05            [24] 1407 	push	ar5
      00131E C0 06            [24] 1408 	push	ar6
      001320 C0 07            [24] 1409 	push	ar7
      001322 90 00 00         [24] 1410 	mov	dptr,#0x0000
      001325 75 F0 20         [24] 1411 	mov	b,#0x20
      001328 74 44            [12] 1412 	mov	a,#0x44
      00132A 12 5C 9F         [24] 1413 	lcall	___fsmul
      00132D AC 82            [24] 1414 	mov	r4,dpl
      00132F AD 83            [24] 1415 	mov	r5,dph
      001331 AE F0            [24] 1416 	mov	r6,b
      001333 FF               [12] 1417 	mov	r7,a
      001334 E5 81            [12] 1418 	mov	a,sp
      001336 24 FC            [12] 1419 	add	a,#0xfc
      001338 F5 81            [12] 1420 	mov	sp,a
      00133A E4               [12] 1421 	clr	a
      00133B C0 E0            [24] 1422 	push	acc
      00133D C0 E0            [24] 1423 	push	acc
      00133F 74 7A            [12] 1424 	mov	a,#0x7a
      001341 C0 E0            [24] 1425 	push	acc
      001343 74 44            [12] 1426 	mov	a,#0x44
      001345 C0 E0            [24] 1427 	push	acc
      001347 8C 82            [24] 1428 	mov	dpl,r4
      001349 8D 83            [24] 1429 	mov	dph,r5
      00134B 8E F0            [24] 1430 	mov	b,r6
      00134D EF               [12] 1431 	mov	a,r7
      00134E 12 77 66         [24] 1432 	lcall	___fsdiv
      001351 AC 82            [24] 1433 	mov	r4,dpl
      001353 AD 83            [24] 1434 	mov	r5,dph
      001355 AE F0            [24] 1435 	mov	r6,b
      001357 FF               [12] 1436 	mov	r7,a
      001358 E5 81            [12] 1437 	mov	a,sp
      00135A 24 FC            [12] 1438 	add	a,#0xfc
      00135C F5 81            [12] 1439 	mov	sp,a
      00135E 8C 82            [24] 1440 	mov	dpl,r4
      001360 8D 83            [24] 1441 	mov	dph,r5
      001362 8E F0            [24] 1442 	mov	b,r6
      001364 EF               [12] 1443 	mov	a,r7
      001365 12 66 C6         [24] 1444 	lcall	___fs2ulong
      001368 AC 82            [24] 1445 	mov	r4,dpl
      00136A AD 83            [24] 1446 	mov	r5,dph
      00136C AE F0            [24] 1447 	mov	r6,b
      00136E FF               [12] 1448 	mov	r7,a
      00136F 90 00 A7         [24] 1449 	mov	dptr,#_delay_tx_PARM_2
      001372 EC               [12] 1450 	mov	a,r4
      001373 F0               [24] 1451 	movx	@dptr,a
      001374 ED               [12] 1452 	mov	a,r5
      001375 A3               [24] 1453 	inc	dptr
      001376 F0               [24] 1454 	movx	@dptr,a
      001377 EE               [12] 1455 	mov	a,r6
      001378 A3               [24] 1456 	inc	dptr
      001379 F0               [24] 1457 	movx	@dptr,a
      00137A EF               [12] 1458 	mov	a,r7
      00137B A3               [24] 1459 	inc	dptr
      00137C F0               [24] 1460 	movx	@dptr,a
                                   1461 ;	..\src\atrs\misc.c:166: dbglink_writestr("timer2: ");
      00137D 90 7B 7B         [24] 1462 	mov	dptr,#___str_4
      001380 75 F0 80         [24] 1463 	mov	b,#0x80
      001383 12 6E FA         [24] 1464 	lcall	_dbglink_writestr
                                   1465 ;	..\src\atrs\misc.c:167: dbglink_writehex32(time2_ms,1,WRNUM_PADZERO);
      001386 90 00 A7         [24] 1466 	mov	dptr,#_delay_tx_PARM_2
      001389 E0               [24] 1467 	movx	a,@dptr
      00138A FC               [12] 1468 	mov	r4,a
      00138B A3               [24] 1469 	inc	dptr
      00138C E0               [24] 1470 	movx	a,@dptr
      00138D FD               [12] 1471 	mov	r5,a
      00138E A3               [24] 1472 	inc	dptr
      00138F E0               [24] 1473 	movx	a,@dptr
      001390 FE               [12] 1474 	mov	r6,a
      001391 A3               [24] 1475 	inc	dptr
      001392 E0               [24] 1476 	movx	a,@dptr
      001393 FF               [12] 1477 	mov	r7,a
      001394 C0 07            [24] 1478 	push	ar7
      001396 C0 06            [24] 1479 	push	ar6
      001398 C0 05            [24] 1480 	push	ar5
      00139A C0 04            [24] 1481 	push	ar4
      00139C 74 08            [12] 1482 	mov	a,#0x08
      00139E C0 E0            [24] 1483 	push	acc
      0013A0 74 01            [12] 1484 	mov	a,#0x01
      0013A2 C0 E0            [24] 1485 	push	acc
      0013A4 8C 82            [24] 1486 	mov	dpl,r4
      0013A6 8D 83            [24] 1487 	mov	dph,r5
      0013A8 8E F0            [24] 1488 	mov	b,r6
      0013AA EF               [12] 1489 	mov	a,r7
      0013AB 12 73 99         [24] 1490 	lcall	_dbglink_writehex32
      0013AE 15 81            [12] 1491 	dec	sp
      0013B0 15 81            [12] 1492 	dec	sp
      0013B2 D0 04            [24] 1493 	pop	ar4
      0013B4 D0 05            [24] 1494 	pop	ar5
      0013B6 D0 06            [24] 1495 	pop	ar6
      0013B8 D0 07            [24] 1496 	pop	ar7
                                   1497 ;	..\src\atrs\misc.c:168: wakeup_tmr2.time = time2_ms; //+= ? o solo = ? verificar esto
      0013BA 90 00 9B         [24] 1498 	mov	dptr,#(_wakeup_tmr2 + 0x0004)
      0013BD EC               [12] 1499 	mov	a,r4
      0013BE F0               [24] 1500 	movx	@dptr,a
      0013BF ED               [12] 1501 	mov	a,r5
      0013C0 A3               [24] 1502 	inc	dptr
      0013C1 F0               [24] 1503 	movx	@dptr,a
      0013C2 EE               [12] 1504 	mov	a,r6
      0013C3 A3               [24] 1505 	inc	dptr
      0013C4 F0               [24] 1506 	movx	@dptr,a
      0013C5 EF               [12] 1507 	mov	a,r7
      0013C6 A3               [24] 1508 	inc	dptr
      0013C7 F0               [24] 1509 	movx	@dptr,a
                                   1510 ;	..\src\atrs\misc.c:169: wtimer0_addabsolute(&wakeup_tmr2);
      0013C8 90 00 97         [24] 1511 	mov	dptr,#_wakeup_tmr2
      0013CB 12 67 3B         [24] 1512 	lcall	_wtimer0_addabsolute
                                   1513 ;	..\src\atrs\misc.c:171: wakeup_tmr3.handler = wakeup_callback3;//Handler for timer 0
      0013CE 90 00 A1         [24] 1514 	mov	dptr,#(_wakeup_tmr3 + 0x0002)
      0013D1 74 F7            [12] 1515 	mov	a,#_wakeup_callback3
      0013D3 F0               [24] 1516 	movx	@dptr,a
      0013D4 74 11            [12] 1517 	mov	a,#(_wakeup_callback3 >> 8)
      0013D6 A3               [24] 1518 	inc	dptr
      0013D7 F0               [24] 1519 	movx	@dptr,a
                                   1520 ;	..\src\atrs\misc.c:172: time3_ms = (uint32_t) TIME_FOR_TIMER_MSEC(time3_ms);
      0013D8 90 00 AB         [24] 1521 	mov	dptr,#_delay_tx_PARM_3
      0013DB E0               [24] 1522 	movx	a,@dptr
      0013DC FC               [12] 1523 	mov	r4,a
      0013DD A3               [24] 1524 	inc	dptr
      0013DE E0               [24] 1525 	movx	a,@dptr
      0013DF FD               [12] 1526 	mov	r5,a
      0013E0 A3               [24] 1527 	inc	dptr
      0013E1 E0               [24] 1528 	movx	a,@dptr
      0013E2 FE               [12] 1529 	mov	r6,a
      0013E3 A3               [24] 1530 	inc	dptr
      0013E4 E0               [24] 1531 	movx	a,@dptr
      0013E5 8C 82            [24] 1532 	mov	dpl,r4
      0013E7 8D 83            [24] 1533 	mov	dph,r5
      0013E9 8E F0            [24] 1534 	mov	b,r6
      0013EB 12 66 B3         [24] 1535 	lcall	___ulong2fs
      0013EE AC 82            [24] 1536 	mov	r4,dpl
      0013F0 AD 83            [24] 1537 	mov	r5,dph
      0013F2 AE F0            [24] 1538 	mov	r6,b
      0013F4 FF               [12] 1539 	mov	r7,a
      0013F5 C0 04            [24] 1540 	push	ar4
      0013F7 C0 05            [24] 1541 	push	ar5
      0013F9 C0 06            [24] 1542 	push	ar6
      0013FB C0 07            [24] 1543 	push	ar7
      0013FD 90 00 00         [24] 1544 	mov	dptr,#0x0000
      001400 75 F0 20         [24] 1545 	mov	b,#0x20
      001403 74 44            [12] 1546 	mov	a,#0x44
      001405 12 5C 9F         [24] 1547 	lcall	___fsmul
      001408 AC 82            [24] 1548 	mov	r4,dpl
      00140A AD 83            [24] 1549 	mov	r5,dph
      00140C AE F0            [24] 1550 	mov	r6,b
      00140E FF               [12] 1551 	mov	r7,a
      00140F E5 81            [12] 1552 	mov	a,sp
      001411 24 FC            [12] 1553 	add	a,#0xfc
      001413 F5 81            [12] 1554 	mov	sp,a
      001415 E4               [12] 1555 	clr	a
      001416 C0 E0            [24] 1556 	push	acc
      001418 C0 E0            [24] 1557 	push	acc
      00141A 74 7A            [12] 1558 	mov	a,#0x7a
      00141C C0 E0            [24] 1559 	push	acc
      00141E 74 44            [12] 1560 	mov	a,#0x44
      001420 C0 E0            [24] 1561 	push	acc
      001422 8C 82            [24] 1562 	mov	dpl,r4
      001424 8D 83            [24] 1563 	mov	dph,r5
      001426 8E F0            [24] 1564 	mov	b,r6
      001428 EF               [12] 1565 	mov	a,r7
      001429 12 77 66         [24] 1566 	lcall	___fsdiv
      00142C AC 82            [24] 1567 	mov	r4,dpl
      00142E AD 83            [24] 1568 	mov	r5,dph
      001430 AE F0            [24] 1569 	mov	r6,b
      001432 FF               [12] 1570 	mov	r7,a
      001433 E5 81            [12] 1571 	mov	a,sp
      001435 24 FC            [12] 1572 	add	a,#0xfc
      001437 F5 81            [12] 1573 	mov	sp,a
      001439 8C 82            [24] 1574 	mov	dpl,r4
      00143B 8D 83            [24] 1575 	mov	dph,r5
      00143D 8E F0            [24] 1576 	mov	b,r6
      00143F EF               [12] 1577 	mov	a,r7
      001440 12 66 C6         [24] 1578 	lcall	___fs2ulong
      001443 AC 82            [24] 1579 	mov	r4,dpl
      001445 AD 83            [24] 1580 	mov	r5,dph
      001447 AE F0            [24] 1581 	mov	r6,b
      001449 FF               [12] 1582 	mov	r7,a
      00144A 90 00 AB         [24] 1583 	mov	dptr,#_delay_tx_PARM_3
      00144D EC               [12] 1584 	mov	a,r4
      00144E F0               [24] 1585 	movx	@dptr,a
      00144F ED               [12] 1586 	mov	a,r5
      001450 A3               [24] 1587 	inc	dptr
      001451 F0               [24] 1588 	movx	@dptr,a
      001452 EE               [12] 1589 	mov	a,r6
      001453 A3               [24] 1590 	inc	dptr
      001454 F0               [24] 1591 	movx	@dptr,a
      001455 EF               [12] 1592 	mov	a,r7
      001456 A3               [24] 1593 	inc	dptr
      001457 F0               [24] 1594 	movx	@dptr,a
                                   1595 ;	..\src\atrs\misc.c:173: dbglink_writestr("timer3: ");
      001458 90 7B 84         [24] 1596 	mov	dptr,#___str_5
      00145B 75 F0 80         [24] 1597 	mov	b,#0x80
      00145E 12 6E FA         [24] 1598 	lcall	_dbglink_writestr
                                   1599 ;	..\src\atrs\misc.c:174: dbglink_writehex32(time3_ms,1,WRNUM_PADZERO);
      001461 90 00 AB         [24] 1600 	mov	dptr,#_delay_tx_PARM_3
      001464 E0               [24] 1601 	movx	a,@dptr
      001465 FC               [12] 1602 	mov	r4,a
      001466 A3               [24] 1603 	inc	dptr
      001467 E0               [24] 1604 	movx	a,@dptr
      001468 FD               [12] 1605 	mov	r5,a
      001469 A3               [24] 1606 	inc	dptr
      00146A E0               [24] 1607 	movx	a,@dptr
      00146B FE               [12] 1608 	mov	r6,a
      00146C A3               [24] 1609 	inc	dptr
      00146D E0               [24] 1610 	movx	a,@dptr
      00146E FF               [12] 1611 	mov	r7,a
      00146F C0 07            [24] 1612 	push	ar7
      001471 C0 06            [24] 1613 	push	ar6
      001473 C0 05            [24] 1614 	push	ar5
      001475 C0 04            [24] 1615 	push	ar4
      001477 74 08            [12] 1616 	mov	a,#0x08
      001479 C0 E0            [24] 1617 	push	acc
      00147B 74 01            [12] 1618 	mov	a,#0x01
      00147D C0 E0            [24] 1619 	push	acc
      00147F 8C 82            [24] 1620 	mov	dpl,r4
      001481 8D 83            [24] 1621 	mov	dph,r5
      001483 8E F0            [24] 1622 	mov	b,r6
      001485 EF               [12] 1623 	mov	a,r7
      001486 12 73 99         [24] 1624 	lcall	_dbglink_writehex32
      001489 15 81            [12] 1625 	dec	sp
      00148B 15 81            [12] 1626 	dec	sp
      00148D D0 04            [24] 1627 	pop	ar4
      00148F D0 05            [24] 1628 	pop	ar5
      001491 D0 06            [24] 1629 	pop	ar6
      001493 D0 07            [24] 1630 	pop	ar7
                                   1631 ;	..\src\atrs\misc.c:175: wakeup_tmr3.time = time3_ms; //+= ? o solo = ? verificar esto
      001495 90 00 A3         [24] 1632 	mov	dptr,#(_wakeup_tmr3 + 0x0004)
      001498 EC               [12] 1633 	mov	a,r4
      001499 F0               [24] 1634 	movx	@dptr,a
      00149A ED               [12] 1635 	mov	a,r5
      00149B A3               [24] 1636 	inc	dptr
      00149C F0               [24] 1637 	movx	@dptr,a
      00149D EE               [12] 1638 	mov	a,r6
      00149E A3               [24] 1639 	inc	dptr
      00149F F0               [24] 1640 	movx	@dptr,a
      0014A0 EF               [12] 1641 	mov	a,r7
      0014A1 A3               [24] 1642 	inc	dptr
      0014A2 F0               [24] 1643 	movx	@dptr,a
                                   1644 ;	..\src\atrs\misc.c:176: wtimer0_addabsolute(&wakeup_tmr3);
      0014A3 90 00 9F         [24] 1645 	mov	dptr,#_wakeup_tmr3
      0014A6 02 67 3B         [24] 1646 	ljmp	_wtimer0_addabsolute
                                   1647 	.area CSEG    (CODE)
                                   1648 	.area CONST   (CODE)
      007B57                       1649 ___str_0:
      007B57 20 74 69 6D 65 72 31  1650 	.ascii " timer1 "
             20
      007B5F 00                    1651 	.db 0x00
      007B60                       1652 ___str_1:
      007B60 20 74 69 6D 65 72 32  1653 	.ascii " timer2 "
             20
      007B68 00                    1654 	.db 0x00
      007B69                       1655 ___str_2:
      007B69 20 74 69 6D 65 72 33  1656 	.ascii " timer3 "
             20
      007B71 00                    1657 	.db 0x00
      007B72                       1658 ___str_3:
      007B72 74 69 6D 65 72 31 3A  1659 	.ascii "timer1: "
             20
      007B7A 00                    1660 	.db 0x00
      007B7B                       1661 ___str_4:
      007B7B 74 69 6D 65 72 32 3A  1662 	.ascii "timer2: "
             20
      007B83 00                    1663 	.db 0x00
      007B84                       1664 ___str_5:
      007B84 74 69 6D 65 72 33 3A  1665 	.ascii "timer3: "
             20
      007B8C 00                    1666 	.db 0x00
                                   1667 	.area XINIT   (CODE)
                                   1668 	.area CABS    (ABS,CODE)
