                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.6.0 #9615 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module main
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _main
                                     12 	.globl __sdcc_external_startup
                                     13 	.globl _UART_Proc_VerifyIncomingMsg
                                     14 	.globl _UART_Proc_PortInit
                                     15 	.globl _UBLOX_GPS_SendCommand_WaitACK
                                     16 	.globl _UBLOX_GPS_PortInit
                                     17 	.globl _delay_tx
                                     18 	.globl _delay_ms
                                     19 	.globl _axradio_setup_pincfg2
                                     20 	.globl _axradio_setup_pincfg1
                                     21 	.globl _axradio_transmit
                                     22 	.globl _axradio_set_default_remote_address
                                     23 	.globl _axradio_set_local_address
                                     24 	.globl _axradio_set_mode
                                     25 	.globl _axradio_init
                                     26 	.globl _dbglink_writehex16
                                     27 	.globl _dbglink_writestr
                                     28 	.globl _dbglink_init
                                     29 	.globl _free
                                     30 	.globl _malloc
                                     31 	.globl _memcpy
                                     32 	.globl _wtimer_runcallbacks
                                     33 	.globl _wtimer_idle
                                     34 	.globl _wtimer_init
                                     35 	.globl _wtimer1_setconfig
                                     36 	.globl _wtimer0_setconfig
                                     37 	.globl _flash_apply_calibration
                                     38 	.globl _ax5043_commsleepexit
                                     39 	.globl _PORTC_7
                                     40 	.globl _PORTC_6
                                     41 	.globl _PORTC_5
                                     42 	.globl _PORTC_4
                                     43 	.globl _PORTC_3
                                     44 	.globl _PORTC_2
                                     45 	.globl _PORTC_1
                                     46 	.globl _PORTC_0
                                     47 	.globl _PORTB_7
                                     48 	.globl _PORTB_6
                                     49 	.globl _PORTB_5
                                     50 	.globl _PORTB_4
                                     51 	.globl _PORTB_3
                                     52 	.globl _PORTB_2
                                     53 	.globl _PORTB_1
                                     54 	.globl _PORTB_0
                                     55 	.globl _PORTA_7
                                     56 	.globl _PORTA_6
                                     57 	.globl _PORTA_5
                                     58 	.globl _PORTA_4
                                     59 	.globl _PORTA_3
                                     60 	.globl _PORTA_2
                                     61 	.globl _PORTA_1
                                     62 	.globl _PORTA_0
                                     63 	.globl _PINC_7
                                     64 	.globl _PINC_6
                                     65 	.globl _PINC_5
                                     66 	.globl _PINC_4
                                     67 	.globl _PINC_3
                                     68 	.globl _PINC_2
                                     69 	.globl _PINC_1
                                     70 	.globl _PINC_0
                                     71 	.globl _PINB_7
                                     72 	.globl _PINB_6
                                     73 	.globl _PINB_5
                                     74 	.globl _PINB_4
                                     75 	.globl _PINB_3
                                     76 	.globl _PINB_2
                                     77 	.globl _PINB_1
                                     78 	.globl _PINB_0
                                     79 	.globl _PINA_7
                                     80 	.globl _PINA_6
                                     81 	.globl _PINA_5
                                     82 	.globl _PINA_4
                                     83 	.globl _PINA_3
                                     84 	.globl _PINA_2
                                     85 	.globl _PINA_1
                                     86 	.globl _PINA_0
                                     87 	.globl _CY
                                     88 	.globl _AC
                                     89 	.globl _F0
                                     90 	.globl _RS1
                                     91 	.globl _RS0
                                     92 	.globl _OV
                                     93 	.globl _F1
                                     94 	.globl _P
                                     95 	.globl _IP_7
                                     96 	.globl _IP_6
                                     97 	.globl _IP_5
                                     98 	.globl _IP_4
                                     99 	.globl _IP_3
                                    100 	.globl _IP_2
                                    101 	.globl _IP_1
                                    102 	.globl _IP_0
                                    103 	.globl _EA
                                    104 	.globl _IE_7
                                    105 	.globl _IE_6
                                    106 	.globl _IE_5
                                    107 	.globl _IE_4
                                    108 	.globl _IE_3
                                    109 	.globl _IE_2
                                    110 	.globl _IE_1
                                    111 	.globl _IE_0
                                    112 	.globl _EIP_7
                                    113 	.globl _EIP_6
                                    114 	.globl _EIP_5
                                    115 	.globl _EIP_4
                                    116 	.globl _EIP_3
                                    117 	.globl _EIP_2
                                    118 	.globl _EIP_1
                                    119 	.globl _EIP_0
                                    120 	.globl _EIE_7
                                    121 	.globl _EIE_6
                                    122 	.globl _EIE_5
                                    123 	.globl _EIE_4
                                    124 	.globl _EIE_3
                                    125 	.globl _EIE_2
                                    126 	.globl _EIE_1
                                    127 	.globl _EIE_0
                                    128 	.globl _E2IP_7
                                    129 	.globl _E2IP_6
                                    130 	.globl _E2IP_5
                                    131 	.globl _E2IP_4
                                    132 	.globl _E2IP_3
                                    133 	.globl _E2IP_2
                                    134 	.globl _E2IP_1
                                    135 	.globl _E2IP_0
                                    136 	.globl _E2IE_7
                                    137 	.globl _E2IE_6
                                    138 	.globl _E2IE_5
                                    139 	.globl _E2IE_4
                                    140 	.globl _E2IE_3
                                    141 	.globl _E2IE_2
                                    142 	.globl _E2IE_1
                                    143 	.globl _E2IE_0
                                    144 	.globl _B_7
                                    145 	.globl _B_6
                                    146 	.globl _B_5
                                    147 	.globl _B_4
                                    148 	.globl _B_3
                                    149 	.globl _B_2
                                    150 	.globl _B_1
                                    151 	.globl _B_0
                                    152 	.globl _ACC_7
                                    153 	.globl _ACC_6
                                    154 	.globl _ACC_5
                                    155 	.globl _ACC_4
                                    156 	.globl _ACC_3
                                    157 	.globl _ACC_2
                                    158 	.globl _ACC_1
                                    159 	.globl _ACC_0
                                    160 	.globl _WTSTAT
                                    161 	.globl _WTIRQEN
                                    162 	.globl _WTEVTD
                                    163 	.globl _WTEVTD1
                                    164 	.globl _WTEVTD0
                                    165 	.globl _WTEVTC
                                    166 	.globl _WTEVTC1
                                    167 	.globl _WTEVTC0
                                    168 	.globl _WTEVTB
                                    169 	.globl _WTEVTB1
                                    170 	.globl _WTEVTB0
                                    171 	.globl _WTEVTA
                                    172 	.globl _WTEVTA1
                                    173 	.globl _WTEVTA0
                                    174 	.globl _WTCNTR1
                                    175 	.globl _WTCNTB
                                    176 	.globl _WTCNTB1
                                    177 	.globl _WTCNTB0
                                    178 	.globl _WTCNTA
                                    179 	.globl _WTCNTA1
                                    180 	.globl _WTCNTA0
                                    181 	.globl _WTCFGB
                                    182 	.globl _WTCFGA
                                    183 	.globl _WDTRESET
                                    184 	.globl _WDTCFG
                                    185 	.globl _U1STATUS
                                    186 	.globl _U1SHREG
                                    187 	.globl _U1MODE
                                    188 	.globl _U1CTRL
                                    189 	.globl _U0STATUS
                                    190 	.globl _U0SHREG
                                    191 	.globl _U0MODE
                                    192 	.globl _U0CTRL
                                    193 	.globl _T2STATUS
                                    194 	.globl _T2PERIOD
                                    195 	.globl _T2PERIOD1
                                    196 	.globl _T2PERIOD0
                                    197 	.globl _T2MODE
                                    198 	.globl _T2CNT
                                    199 	.globl _T2CNT1
                                    200 	.globl _T2CNT0
                                    201 	.globl _T2CLKSRC
                                    202 	.globl _T1STATUS
                                    203 	.globl _T1PERIOD
                                    204 	.globl _T1PERIOD1
                                    205 	.globl _T1PERIOD0
                                    206 	.globl _T1MODE
                                    207 	.globl _T1CNT
                                    208 	.globl _T1CNT1
                                    209 	.globl _T1CNT0
                                    210 	.globl _T1CLKSRC
                                    211 	.globl _T0STATUS
                                    212 	.globl _T0PERIOD
                                    213 	.globl _T0PERIOD1
                                    214 	.globl _T0PERIOD0
                                    215 	.globl _T0MODE
                                    216 	.globl _T0CNT
                                    217 	.globl _T0CNT1
                                    218 	.globl _T0CNT0
                                    219 	.globl _T0CLKSRC
                                    220 	.globl _SPSTATUS
                                    221 	.globl _SPSHREG
                                    222 	.globl _SPMODE
                                    223 	.globl _SPCLKSRC
                                    224 	.globl _RADIOSTAT
                                    225 	.globl _RADIOSTAT1
                                    226 	.globl _RADIOSTAT0
                                    227 	.globl _RADIODATA
                                    228 	.globl _RADIODATA3
                                    229 	.globl _RADIODATA2
                                    230 	.globl _RADIODATA1
                                    231 	.globl _RADIODATA0
                                    232 	.globl _RADIOADDR
                                    233 	.globl _RADIOADDR1
                                    234 	.globl _RADIOADDR0
                                    235 	.globl _RADIOACC
                                    236 	.globl _OC1STATUS
                                    237 	.globl _OC1PIN
                                    238 	.globl _OC1MODE
                                    239 	.globl _OC1COMP
                                    240 	.globl _OC1COMP1
                                    241 	.globl _OC1COMP0
                                    242 	.globl _OC0STATUS
                                    243 	.globl _OC0PIN
                                    244 	.globl _OC0MODE
                                    245 	.globl _OC0COMP
                                    246 	.globl _OC0COMP1
                                    247 	.globl _OC0COMP0
                                    248 	.globl _NVSTATUS
                                    249 	.globl _NVKEY
                                    250 	.globl _NVDATA
                                    251 	.globl _NVDATA1
                                    252 	.globl _NVDATA0
                                    253 	.globl _NVADDR
                                    254 	.globl _NVADDR1
                                    255 	.globl _NVADDR0
                                    256 	.globl _IC1STATUS
                                    257 	.globl _IC1MODE
                                    258 	.globl _IC1CAPT
                                    259 	.globl _IC1CAPT1
                                    260 	.globl _IC1CAPT0
                                    261 	.globl _IC0STATUS
                                    262 	.globl _IC0MODE
                                    263 	.globl _IC0CAPT
                                    264 	.globl _IC0CAPT1
                                    265 	.globl _IC0CAPT0
                                    266 	.globl _PORTR
                                    267 	.globl _PORTC
                                    268 	.globl _PORTB
                                    269 	.globl _PORTA
                                    270 	.globl _PINR
                                    271 	.globl _PINC
                                    272 	.globl _PINB
                                    273 	.globl _PINA
                                    274 	.globl _DIRR
                                    275 	.globl _DIRC
                                    276 	.globl _DIRB
                                    277 	.globl _DIRA
                                    278 	.globl _DBGLNKSTAT
                                    279 	.globl _DBGLNKBUF
                                    280 	.globl _CODECONFIG
                                    281 	.globl _CLKSTAT
                                    282 	.globl _CLKCON
                                    283 	.globl _ANALOGCOMP
                                    284 	.globl _ADCCONV
                                    285 	.globl _ADCCLKSRC
                                    286 	.globl _ADCCH3CONFIG
                                    287 	.globl _ADCCH2CONFIG
                                    288 	.globl _ADCCH1CONFIG
                                    289 	.globl _ADCCH0CONFIG
                                    290 	.globl __XPAGE
                                    291 	.globl _XPAGE
                                    292 	.globl _SP
                                    293 	.globl _PSW
                                    294 	.globl _PCON
                                    295 	.globl _IP
                                    296 	.globl _IE
                                    297 	.globl _EIP
                                    298 	.globl _EIE
                                    299 	.globl _E2IP
                                    300 	.globl _E2IE
                                    301 	.globl _DPS
                                    302 	.globl _DPTR1
                                    303 	.globl _DPTR0
                                    304 	.globl _DPL1
                                    305 	.globl _DPL
                                    306 	.globl _DPH1
                                    307 	.globl _DPH
                                    308 	.globl _B
                                    309 	.globl _ACC
                                    310 	.globl _counter
                                    311 	.globl _Tmr0Flg
                                    312 	.globl _RfStateMachine
                                    313 	.globl _i
                                    314 	.globl _RxBuffer
                                    315 	.globl _BeaconRx
                                    316 	.globl _RxIsrFlag
                                    317 	.globl _AX5043_XTALAMPL
                                    318 	.globl _AX5043_XTALOSC
                                    319 	.globl _AX5043_MODCFGP
                                    320 	.globl _AX5043_POWCTRL1
                                    321 	.globl _AX5043_REF
                                    322 	.globl _AX5043_0xF44
                                    323 	.globl _AX5043_0xF35
                                    324 	.globl _AX5043_0xF34
                                    325 	.globl _AX5043_0xF33
                                    326 	.globl _AX5043_0xF32
                                    327 	.globl _AX5043_0xF31
                                    328 	.globl _AX5043_0xF30
                                    329 	.globl _AX5043_0xF26
                                    330 	.globl _AX5043_0xF23
                                    331 	.globl _AX5043_0xF22
                                    332 	.globl _AX5043_0xF21
                                    333 	.globl _AX5043_0xF1C
                                    334 	.globl _AX5043_0xF18
                                    335 	.globl _AX5043_0xF11
                                    336 	.globl _AX5043_0xF10
                                    337 	.globl _AX5043_0xF0C
                                    338 	.globl _AX5043_0xF00
                                    339 	.globl _aligned_alloc_PARM_2
                                    340 	.globl _AX5043_TIMEGAIN3NB
                                    341 	.globl _AX5043_TIMEGAIN2NB
                                    342 	.globl _AX5043_TIMEGAIN1NB
                                    343 	.globl _AX5043_TIMEGAIN0NB
                                    344 	.globl _AX5043_RXPARAMSETSNB
                                    345 	.globl _AX5043_RXPARAMCURSETNB
                                    346 	.globl _AX5043_PKTMAXLENNB
                                    347 	.globl _AX5043_PKTLENOFFSETNB
                                    348 	.globl _AX5043_PKTLENCFGNB
                                    349 	.globl _AX5043_PKTADDRMASK3NB
                                    350 	.globl _AX5043_PKTADDRMASK2NB
                                    351 	.globl _AX5043_PKTADDRMASK1NB
                                    352 	.globl _AX5043_PKTADDRMASK0NB
                                    353 	.globl _AX5043_PKTADDRCFGNB
                                    354 	.globl _AX5043_PKTADDR3NB
                                    355 	.globl _AX5043_PKTADDR2NB
                                    356 	.globl _AX5043_PKTADDR1NB
                                    357 	.globl _AX5043_PKTADDR0NB
                                    358 	.globl _AX5043_PHASEGAIN3NB
                                    359 	.globl _AX5043_PHASEGAIN2NB
                                    360 	.globl _AX5043_PHASEGAIN1NB
                                    361 	.globl _AX5043_PHASEGAIN0NB
                                    362 	.globl _AX5043_FREQUENCYLEAKNB
                                    363 	.globl _AX5043_FREQUENCYGAIND3NB
                                    364 	.globl _AX5043_FREQUENCYGAIND2NB
                                    365 	.globl _AX5043_FREQUENCYGAIND1NB
                                    366 	.globl _AX5043_FREQUENCYGAIND0NB
                                    367 	.globl _AX5043_FREQUENCYGAINC3NB
                                    368 	.globl _AX5043_FREQUENCYGAINC2NB
                                    369 	.globl _AX5043_FREQUENCYGAINC1NB
                                    370 	.globl _AX5043_FREQUENCYGAINC0NB
                                    371 	.globl _AX5043_FREQUENCYGAINB3NB
                                    372 	.globl _AX5043_FREQUENCYGAINB2NB
                                    373 	.globl _AX5043_FREQUENCYGAINB1NB
                                    374 	.globl _AX5043_FREQUENCYGAINB0NB
                                    375 	.globl _AX5043_FREQUENCYGAINA3NB
                                    376 	.globl _AX5043_FREQUENCYGAINA2NB
                                    377 	.globl _AX5043_FREQUENCYGAINA1NB
                                    378 	.globl _AX5043_FREQUENCYGAINA0NB
                                    379 	.globl _AX5043_FREQDEV13NB
                                    380 	.globl _AX5043_FREQDEV12NB
                                    381 	.globl _AX5043_FREQDEV11NB
                                    382 	.globl _AX5043_FREQDEV10NB
                                    383 	.globl _AX5043_FREQDEV03NB
                                    384 	.globl _AX5043_FREQDEV02NB
                                    385 	.globl _AX5043_FREQDEV01NB
                                    386 	.globl _AX5043_FREQDEV00NB
                                    387 	.globl _AX5043_FOURFSK3NB
                                    388 	.globl _AX5043_FOURFSK2NB
                                    389 	.globl _AX5043_FOURFSK1NB
                                    390 	.globl _AX5043_FOURFSK0NB
                                    391 	.globl _AX5043_DRGAIN3NB
                                    392 	.globl _AX5043_DRGAIN2NB
                                    393 	.globl _AX5043_DRGAIN1NB
                                    394 	.globl _AX5043_DRGAIN0NB
                                    395 	.globl _AX5043_BBOFFSRES3NB
                                    396 	.globl _AX5043_BBOFFSRES2NB
                                    397 	.globl _AX5043_BBOFFSRES1NB
                                    398 	.globl _AX5043_BBOFFSRES0NB
                                    399 	.globl _AX5043_AMPLITUDEGAIN3NB
                                    400 	.globl _AX5043_AMPLITUDEGAIN2NB
                                    401 	.globl _AX5043_AMPLITUDEGAIN1NB
                                    402 	.globl _AX5043_AMPLITUDEGAIN0NB
                                    403 	.globl _AX5043_AGCTARGET3NB
                                    404 	.globl _AX5043_AGCTARGET2NB
                                    405 	.globl _AX5043_AGCTARGET1NB
                                    406 	.globl _AX5043_AGCTARGET0NB
                                    407 	.globl _AX5043_AGCMINMAX3NB
                                    408 	.globl _AX5043_AGCMINMAX2NB
                                    409 	.globl _AX5043_AGCMINMAX1NB
                                    410 	.globl _AX5043_AGCMINMAX0NB
                                    411 	.globl _AX5043_AGCGAIN3NB
                                    412 	.globl _AX5043_AGCGAIN2NB
                                    413 	.globl _AX5043_AGCGAIN1NB
                                    414 	.globl _AX5043_AGCGAIN0NB
                                    415 	.globl _AX5043_AGCAHYST3NB
                                    416 	.globl _AX5043_AGCAHYST2NB
                                    417 	.globl _AX5043_AGCAHYST1NB
                                    418 	.globl _AX5043_AGCAHYST0NB
                                    419 	.globl _AX5043_0xF44NB
                                    420 	.globl _AX5043_0xF35NB
                                    421 	.globl _AX5043_0xF34NB
                                    422 	.globl _AX5043_0xF33NB
                                    423 	.globl _AX5043_0xF32NB
                                    424 	.globl _AX5043_0xF31NB
                                    425 	.globl _AX5043_0xF30NB
                                    426 	.globl _AX5043_0xF26NB
                                    427 	.globl _AX5043_0xF23NB
                                    428 	.globl _AX5043_0xF22NB
                                    429 	.globl _AX5043_0xF21NB
                                    430 	.globl _AX5043_0xF1CNB
                                    431 	.globl _AX5043_0xF18NB
                                    432 	.globl _AX5043_0xF0CNB
                                    433 	.globl _AX5043_0xF00NB
                                    434 	.globl _AX5043_XTALSTATUSNB
                                    435 	.globl _AX5043_XTALOSCNB
                                    436 	.globl _AX5043_XTALCAPNB
                                    437 	.globl _AX5043_XTALAMPLNB
                                    438 	.globl _AX5043_WAKEUPXOEARLYNB
                                    439 	.globl _AX5043_WAKEUPTIMER1NB
                                    440 	.globl _AX5043_WAKEUPTIMER0NB
                                    441 	.globl _AX5043_WAKEUPFREQ1NB
                                    442 	.globl _AX5043_WAKEUPFREQ0NB
                                    443 	.globl _AX5043_WAKEUP1NB
                                    444 	.globl _AX5043_WAKEUP0NB
                                    445 	.globl _AX5043_TXRATE2NB
                                    446 	.globl _AX5043_TXRATE1NB
                                    447 	.globl _AX5043_TXRATE0NB
                                    448 	.globl _AX5043_TXPWRCOEFFE1NB
                                    449 	.globl _AX5043_TXPWRCOEFFE0NB
                                    450 	.globl _AX5043_TXPWRCOEFFD1NB
                                    451 	.globl _AX5043_TXPWRCOEFFD0NB
                                    452 	.globl _AX5043_TXPWRCOEFFC1NB
                                    453 	.globl _AX5043_TXPWRCOEFFC0NB
                                    454 	.globl _AX5043_TXPWRCOEFFB1NB
                                    455 	.globl _AX5043_TXPWRCOEFFB0NB
                                    456 	.globl _AX5043_TXPWRCOEFFA1NB
                                    457 	.globl _AX5043_TXPWRCOEFFA0NB
                                    458 	.globl _AX5043_TRKRFFREQ2NB
                                    459 	.globl _AX5043_TRKRFFREQ1NB
                                    460 	.globl _AX5043_TRKRFFREQ0NB
                                    461 	.globl _AX5043_TRKPHASE1NB
                                    462 	.globl _AX5043_TRKPHASE0NB
                                    463 	.globl _AX5043_TRKFSKDEMOD1NB
                                    464 	.globl _AX5043_TRKFSKDEMOD0NB
                                    465 	.globl _AX5043_TRKFREQ1NB
                                    466 	.globl _AX5043_TRKFREQ0NB
                                    467 	.globl _AX5043_TRKDATARATE2NB
                                    468 	.globl _AX5043_TRKDATARATE1NB
                                    469 	.globl _AX5043_TRKDATARATE0NB
                                    470 	.globl _AX5043_TRKAMPLITUDE1NB
                                    471 	.globl _AX5043_TRKAMPLITUDE0NB
                                    472 	.globl _AX5043_TRKAFSKDEMOD1NB
                                    473 	.globl _AX5043_TRKAFSKDEMOD0NB
                                    474 	.globl _AX5043_TMGTXSETTLENB
                                    475 	.globl _AX5043_TMGTXBOOSTNB
                                    476 	.globl _AX5043_TMGRXSETTLENB
                                    477 	.globl _AX5043_TMGRXRSSINB
                                    478 	.globl _AX5043_TMGRXPREAMBLE3NB
                                    479 	.globl _AX5043_TMGRXPREAMBLE2NB
                                    480 	.globl _AX5043_TMGRXPREAMBLE1NB
                                    481 	.globl _AX5043_TMGRXOFFSACQNB
                                    482 	.globl _AX5043_TMGRXCOARSEAGCNB
                                    483 	.globl _AX5043_TMGRXBOOSTNB
                                    484 	.globl _AX5043_TMGRXAGCNB
                                    485 	.globl _AX5043_TIMER2NB
                                    486 	.globl _AX5043_TIMER1NB
                                    487 	.globl _AX5043_TIMER0NB
                                    488 	.globl _AX5043_SILICONREVISIONNB
                                    489 	.globl _AX5043_SCRATCHNB
                                    490 	.globl _AX5043_RXDATARATE2NB
                                    491 	.globl _AX5043_RXDATARATE1NB
                                    492 	.globl _AX5043_RXDATARATE0NB
                                    493 	.globl _AX5043_RSSIREFERENCENB
                                    494 	.globl _AX5043_RSSIABSTHRNB
                                    495 	.globl _AX5043_RSSINB
                                    496 	.globl _AX5043_REFNB
                                    497 	.globl _AX5043_RADIOSTATENB
                                    498 	.globl _AX5043_RADIOEVENTREQ1NB
                                    499 	.globl _AX5043_RADIOEVENTREQ0NB
                                    500 	.globl _AX5043_RADIOEVENTMASK1NB
                                    501 	.globl _AX5043_RADIOEVENTMASK0NB
                                    502 	.globl _AX5043_PWRMODENB
                                    503 	.globl _AX5043_PWRAMPNB
                                    504 	.globl _AX5043_POWSTICKYSTATNB
                                    505 	.globl _AX5043_POWSTATNB
                                    506 	.globl _AX5043_POWIRQMASKNB
                                    507 	.globl _AX5043_POWCTRL1NB
                                    508 	.globl _AX5043_PLLVCOIRNB
                                    509 	.globl _AX5043_PLLVCOINB
                                    510 	.globl _AX5043_PLLVCODIVNB
                                    511 	.globl _AX5043_PLLRNGCLKNB
                                    512 	.globl _AX5043_PLLRANGINGBNB
                                    513 	.globl _AX5043_PLLRANGINGANB
                                    514 	.globl _AX5043_PLLLOOPBOOSTNB
                                    515 	.globl _AX5043_PLLLOOPNB
                                    516 	.globl _AX5043_PLLLOCKDETNB
                                    517 	.globl _AX5043_PLLCPIBOOSTNB
                                    518 	.globl _AX5043_PLLCPINB
                                    519 	.globl _AX5043_PKTSTOREFLAGSNB
                                    520 	.globl _AX5043_PKTMISCFLAGSNB
                                    521 	.globl _AX5043_PKTCHUNKSIZENB
                                    522 	.globl _AX5043_PKTACCEPTFLAGSNB
                                    523 	.globl _AX5043_PINSTATENB
                                    524 	.globl _AX5043_PINFUNCSYSCLKNB
                                    525 	.globl _AX5043_PINFUNCPWRAMPNB
                                    526 	.globl _AX5043_PINFUNCIRQNB
                                    527 	.globl _AX5043_PINFUNCDCLKNB
                                    528 	.globl _AX5043_PINFUNCDATANB
                                    529 	.globl _AX5043_PINFUNCANTSELNB
                                    530 	.globl _AX5043_MODULATIONNB
                                    531 	.globl _AX5043_MODCFGPNB
                                    532 	.globl _AX5043_MODCFGFNB
                                    533 	.globl _AX5043_MODCFGANB
                                    534 	.globl _AX5043_MAXRFOFFSET2NB
                                    535 	.globl _AX5043_MAXRFOFFSET1NB
                                    536 	.globl _AX5043_MAXRFOFFSET0NB
                                    537 	.globl _AX5043_MAXDROFFSET2NB
                                    538 	.globl _AX5043_MAXDROFFSET1NB
                                    539 	.globl _AX5043_MAXDROFFSET0NB
                                    540 	.globl _AX5043_MATCH1PAT1NB
                                    541 	.globl _AX5043_MATCH1PAT0NB
                                    542 	.globl _AX5043_MATCH1MINNB
                                    543 	.globl _AX5043_MATCH1MAXNB
                                    544 	.globl _AX5043_MATCH1LENNB
                                    545 	.globl _AX5043_MATCH0PAT3NB
                                    546 	.globl _AX5043_MATCH0PAT2NB
                                    547 	.globl _AX5043_MATCH0PAT1NB
                                    548 	.globl _AX5043_MATCH0PAT0NB
                                    549 	.globl _AX5043_MATCH0MINNB
                                    550 	.globl _AX5043_MATCH0MAXNB
                                    551 	.globl _AX5043_MATCH0LENNB
                                    552 	.globl _AX5043_LPOSCSTATUSNB
                                    553 	.globl _AX5043_LPOSCREF1NB
                                    554 	.globl _AX5043_LPOSCREF0NB
                                    555 	.globl _AX5043_LPOSCPER1NB
                                    556 	.globl _AX5043_LPOSCPER0NB
                                    557 	.globl _AX5043_LPOSCKFILT1NB
                                    558 	.globl _AX5043_LPOSCKFILT0NB
                                    559 	.globl _AX5043_LPOSCFREQ1NB
                                    560 	.globl _AX5043_LPOSCFREQ0NB
                                    561 	.globl _AX5043_LPOSCCONFIGNB
                                    562 	.globl _AX5043_IRQREQUEST1NB
                                    563 	.globl _AX5043_IRQREQUEST0NB
                                    564 	.globl _AX5043_IRQMASK1NB
                                    565 	.globl _AX5043_IRQMASK0NB
                                    566 	.globl _AX5043_IRQINVERSION1NB
                                    567 	.globl _AX5043_IRQINVERSION0NB
                                    568 	.globl _AX5043_IFFREQ1NB
                                    569 	.globl _AX5043_IFFREQ0NB
                                    570 	.globl _AX5043_GPADCPERIODNB
                                    571 	.globl _AX5043_GPADCCTRLNB
                                    572 	.globl _AX5043_GPADC13VALUE1NB
                                    573 	.globl _AX5043_GPADC13VALUE0NB
                                    574 	.globl _AX5043_FSKDMIN1NB
                                    575 	.globl _AX5043_FSKDMIN0NB
                                    576 	.globl _AX5043_FSKDMAX1NB
                                    577 	.globl _AX5043_FSKDMAX0NB
                                    578 	.globl _AX5043_FSKDEV2NB
                                    579 	.globl _AX5043_FSKDEV1NB
                                    580 	.globl _AX5043_FSKDEV0NB
                                    581 	.globl _AX5043_FREQB3NB
                                    582 	.globl _AX5043_FREQB2NB
                                    583 	.globl _AX5043_FREQB1NB
                                    584 	.globl _AX5043_FREQB0NB
                                    585 	.globl _AX5043_FREQA3NB
                                    586 	.globl _AX5043_FREQA2NB
                                    587 	.globl _AX5043_FREQA1NB
                                    588 	.globl _AX5043_FREQA0NB
                                    589 	.globl _AX5043_FRAMINGNB
                                    590 	.globl _AX5043_FIFOTHRESH1NB
                                    591 	.globl _AX5043_FIFOTHRESH0NB
                                    592 	.globl _AX5043_FIFOSTATNB
                                    593 	.globl _AX5043_FIFOFREE1NB
                                    594 	.globl _AX5043_FIFOFREE0NB
                                    595 	.globl _AX5043_FIFODATANB
                                    596 	.globl _AX5043_FIFOCOUNT1NB
                                    597 	.globl _AX5043_FIFOCOUNT0NB
                                    598 	.globl _AX5043_FECSYNCNB
                                    599 	.globl _AX5043_FECSTATUSNB
                                    600 	.globl _AX5043_FECNB
                                    601 	.globl _AX5043_ENCODINGNB
                                    602 	.globl _AX5043_DIVERSITYNB
                                    603 	.globl _AX5043_DECIMATIONNB
                                    604 	.globl _AX5043_DACVALUE1NB
                                    605 	.globl _AX5043_DACVALUE0NB
                                    606 	.globl _AX5043_DACCONFIGNB
                                    607 	.globl _AX5043_CRCINIT3NB
                                    608 	.globl _AX5043_CRCINIT2NB
                                    609 	.globl _AX5043_CRCINIT1NB
                                    610 	.globl _AX5043_CRCINIT0NB
                                    611 	.globl _AX5043_BGNDRSSITHRNB
                                    612 	.globl _AX5043_BGNDRSSIGAINNB
                                    613 	.globl _AX5043_BGNDRSSINB
                                    614 	.globl _AX5043_BBTUNENB
                                    615 	.globl _AX5043_BBOFFSCAPNB
                                    616 	.globl _AX5043_AMPLFILTERNB
                                    617 	.globl _AX5043_AGCCOUNTERNB
                                    618 	.globl _AX5043_AFSKSPACE1NB
                                    619 	.globl _AX5043_AFSKSPACE0NB
                                    620 	.globl _AX5043_AFSKMARK1NB
                                    621 	.globl _AX5043_AFSKMARK0NB
                                    622 	.globl _AX5043_AFSKCTRLNB
                                    623 	.globl _AX5043_TIMEGAIN3
                                    624 	.globl _AX5043_TIMEGAIN2
                                    625 	.globl _AX5043_TIMEGAIN1
                                    626 	.globl _AX5043_TIMEGAIN0
                                    627 	.globl _AX5043_RXPARAMSETS
                                    628 	.globl _AX5043_RXPARAMCURSET
                                    629 	.globl _AX5043_PKTMAXLEN
                                    630 	.globl _AX5043_PKTLENOFFSET
                                    631 	.globl _AX5043_PKTLENCFG
                                    632 	.globl _AX5043_PKTADDRMASK3
                                    633 	.globl _AX5043_PKTADDRMASK2
                                    634 	.globl _AX5043_PKTADDRMASK1
                                    635 	.globl _AX5043_PKTADDRMASK0
                                    636 	.globl _AX5043_PKTADDRCFG
                                    637 	.globl _AX5043_PKTADDR3
                                    638 	.globl _AX5043_PKTADDR2
                                    639 	.globl _AX5043_PKTADDR1
                                    640 	.globl _AX5043_PKTADDR0
                                    641 	.globl _AX5043_PHASEGAIN3
                                    642 	.globl _AX5043_PHASEGAIN2
                                    643 	.globl _AX5043_PHASEGAIN1
                                    644 	.globl _AX5043_PHASEGAIN0
                                    645 	.globl _AX5043_FREQUENCYLEAK
                                    646 	.globl _AX5043_FREQUENCYGAIND3
                                    647 	.globl _AX5043_FREQUENCYGAIND2
                                    648 	.globl _AX5043_FREQUENCYGAIND1
                                    649 	.globl _AX5043_FREQUENCYGAIND0
                                    650 	.globl _AX5043_FREQUENCYGAINC3
                                    651 	.globl _AX5043_FREQUENCYGAINC2
                                    652 	.globl _AX5043_FREQUENCYGAINC1
                                    653 	.globl _AX5043_FREQUENCYGAINC0
                                    654 	.globl _AX5043_FREQUENCYGAINB3
                                    655 	.globl _AX5043_FREQUENCYGAINB2
                                    656 	.globl _AX5043_FREQUENCYGAINB1
                                    657 	.globl _AX5043_FREQUENCYGAINB0
                                    658 	.globl _AX5043_FREQUENCYGAINA3
                                    659 	.globl _AX5043_FREQUENCYGAINA2
                                    660 	.globl _AX5043_FREQUENCYGAINA1
                                    661 	.globl _AX5043_FREQUENCYGAINA0
                                    662 	.globl _AX5043_FREQDEV13
                                    663 	.globl _AX5043_FREQDEV12
                                    664 	.globl _AX5043_FREQDEV11
                                    665 	.globl _AX5043_FREQDEV10
                                    666 	.globl _AX5043_FREQDEV03
                                    667 	.globl _AX5043_FREQDEV02
                                    668 	.globl _AX5043_FREQDEV01
                                    669 	.globl _AX5043_FREQDEV00
                                    670 	.globl _AX5043_FOURFSK3
                                    671 	.globl _AX5043_FOURFSK2
                                    672 	.globl _AX5043_FOURFSK1
                                    673 	.globl _AX5043_FOURFSK0
                                    674 	.globl _AX5043_DRGAIN3
                                    675 	.globl _AX5043_DRGAIN2
                                    676 	.globl _AX5043_DRGAIN1
                                    677 	.globl _AX5043_DRGAIN0
                                    678 	.globl _AX5043_BBOFFSRES3
                                    679 	.globl _AX5043_BBOFFSRES2
                                    680 	.globl _AX5043_BBOFFSRES1
                                    681 	.globl _AX5043_BBOFFSRES0
                                    682 	.globl _AX5043_AMPLITUDEGAIN3
                                    683 	.globl _AX5043_AMPLITUDEGAIN2
                                    684 	.globl _AX5043_AMPLITUDEGAIN1
                                    685 	.globl _AX5043_AMPLITUDEGAIN0
                                    686 	.globl _AX5043_AGCTARGET3
                                    687 	.globl _AX5043_AGCTARGET2
                                    688 	.globl _AX5043_AGCTARGET1
                                    689 	.globl _AX5043_AGCTARGET0
                                    690 	.globl _AX5043_AGCMINMAX3
                                    691 	.globl _AX5043_AGCMINMAX2
                                    692 	.globl _AX5043_AGCMINMAX1
                                    693 	.globl _AX5043_AGCMINMAX0
                                    694 	.globl _AX5043_AGCGAIN3
                                    695 	.globl _AX5043_AGCGAIN2
                                    696 	.globl _AX5043_AGCGAIN1
                                    697 	.globl _AX5043_AGCGAIN0
                                    698 	.globl _AX5043_AGCAHYST3
                                    699 	.globl _AX5043_AGCAHYST2
                                    700 	.globl _AX5043_AGCAHYST1
                                    701 	.globl _AX5043_AGCAHYST0
                                    702 	.globl _AX5043_XTALSTATUS
                                    703 	.globl _AX5043_XTALCAP
                                    704 	.globl _AX5043_WAKEUPXOEARLY
                                    705 	.globl _AX5043_WAKEUPTIMER1
                                    706 	.globl _AX5043_WAKEUPTIMER0
                                    707 	.globl _AX5043_WAKEUPFREQ1
                                    708 	.globl _AX5043_WAKEUPFREQ0
                                    709 	.globl _AX5043_WAKEUP1
                                    710 	.globl _AX5043_WAKEUP0
                                    711 	.globl _AX5043_TXRATE2
                                    712 	.globl _AX5043_TXRATE1
                                    713 	.globl _AX5043_TXRATE0
                                    714 	.globl _AX5043_TXPWRCOEFFE1
                                    715 	.globl _AX5043_TXPWRCOEFFE0
                                    716 	.globl _AX5043_TXPWRCOEFFD1
                                    717 	.globl _AX5043_TXPWRCOEFFD0
                                    718 	.globl _AX5043_TXPWRCOEFFC1
                                    719 	.globl _AX5043_TXPWRCOEFFC0
                                    720 	.globl _AX5043_TXPWRCOEFFB1
                                    721 	.globl _AX5043_TXPWRCOEFFB0
                                    722 	.globl _AX5043_TXPWRCOEFFA1
                                    723 	.globl _AX5043_TXPWRCOEFFA0
                                    724 	.globl _AX5043_TRKRFFREQ2
                                    725 	.globl _AX5043_TRKRFFREQ1
                                    726 	.globl _AX5043_TRKRFFREQ0
                                    727 	.globl _AX5043_TRKPHASE1
                                    728 	.globl _AX5043_TRKPHASE0
                                    729 	.globl _AX5043_TRKFSKDEMOD1
                                    730 	.globl _AX5043_TRKFSKDEMOD0
                                    731 	.globl _AX5043_TRKFREQ1
                                    732 	.globl _AX5043_TRKFREQ0
                                    733 	.globl _AX5043_TRKDATARATE2
                                    734 	.globl _AX5043_TRKDATARATE1
                                    735 	.globl _AX5043_TRKDATARATE0
                                    736 	.globl _AX5043_TRKAMPLITUDE1
                                    737 	.globl _AX5043_TRKAMPLITUDE0
                                    738 	.globl _AX5043_TRKAFSKDEMOD1
                                    739 	.globl _AX5043_TRKAFSKDEMOD0
                                    740 	.globl _AX5043_TMGTXSETTLE
                                    741 	.globl _AX5043_TMGTXBOOST
                                    742 	.globl _AX5043_TMGRXSETTLE
                                    743 	.globl _AX5043_TMGRXRSSI
                                    744 	.globl _AX5043_TMGRXPREAMBLE3
                                    745 	.globl _AX5043_TMGRXPREAMBLE2
                                    746 	.globl _AX5043_TMGRXPREAMBLE1
                                    747 	.globl _AX5043_TMGRXOFFSACQ
                                    748 	.globl _AX5043_TMGRXCOARSEAGC
                                    749 	.globl _AX5043_TMGRXBOOST
                                    750 	.globl _AX5043_TMGRXAGC
                                    751 	.globl _AX5043_TIMER2
                                    752 	.globl _AX5043_TIMER1
                                    753 	.globl _AX5043_TIMER0
                                    754 	.globl _AX5043_SILICONREVISION
                                    755 	.globl _AX5043_SCRATCH
                                    756 	.globl _AX5043_RXDATARATE2
                                    757 	.globl _AX5043_RXDATARATE1
                                    758 	.globl _AX5043_RXDATARATE0
                                    759 	.globl _AX5043_RSSIREFERENCE
                                    760 	.globl _AX5043_RSSIABSTHR
                                    761 	.globl _AX5043_RSSI
                                    762 	.globl _AX5043_RADIOSTATE
                                    763 	.globl _AX5043_RADIOEVENTREQ1
                                    764 	.globl _AX5043_RADIOEVENTREQ0
                                    765 	.globl _AX5043_RADIOEVENTMASK1
                                    766 	.globl _AX5043_RADIOEVENTMASK0
                                    767 	.globl _AX5043_PWRMODE
                                    768 	.globl _AX5043_PWRAMP
                                    769 	.globl _AX5043_POWSTICKYSTAT
                                    770 	.globl _AX5043_POWSTAT
                                    771 	.globl _AX5043_POWIRQMASK
                                    772 	.globl _AX5043_PLLVCOIR
                                    773 	.globl _AX5043_PLLVCOI
                                    774 	.globl _AX5043_PLLVCODIV
                                    775 	.globl _AX5043_PLLRNGCLK
                                    776 	.globl _AX5043_PLLRANGINGB
                                    777 	.globl _AX5043_PLLRANGINGA
                                    778 	.globl _AX5043_PLLLOOPBOOST
                                    779 	.globl _AX5043_PLLLOOP
                                    780 	.globl _AX5043_PLLLOCKDET
                                    781 	.globl _AX5043_PLLCPIBOOST
                                    782 	.globl _AX5043_PLLCPI
                                    783 	.globl _AX5043_PKTSTOREFLAGS
                                    784 	.globl _AX5043_PKTMISCFLAGS
                                    785 	.globl _AX5043_PKTCHUNKSIZE
                                    786 	.globl _AX5043_PKTACCEPTFLAGS
                                    787 	.globl _AX5043_PINSTATE
                                    788 	.globl _AX5043_PINFUNCSYSCLK
                                    789 	.globl _AX5043_PINFUNCPWRAMP
                                    790 	.globl _AX5043_PINFUNCIRQ
                                    791 	.globl _AX5043_PINFUNCDCLK
                                    792 	.globl _AX5043_PINFUNCDATA
                                    793 	.globl _AX5043_PINFUNCANTSEL
                                    794 	.globl _AX5043_MODULATION
                                    795 	.globl _AX5043_MODCFGF
                                    796 	.globl _AX5043_MODCFGA
                                    797 	.globl _AX5043_MAXRFOFFSET2
                                    798 	.globl _AX5043_MAXRFOFFSET1
                                    799 	.globl _AX5043_MAXRFOFFSET0
                                    800 	.globl _AX5043_MAXDROFFSET2
                                    801 	.globl _AX5043_MAXDROFFSET1
                                    802 	.globl _AX5043_MAXDROFFSET0
                                    803 	.globl _AX5043_MATCH1PAT1
                                    804 	.globl _AX5043_MATCH1PAT0
                                    805 	.globl _AX5043_MATCH1MIN
                                    806 	.globl _AX5043_MATCH1MAX
                                    807 	.globl _AX5043_MATCH1LEN
                                    808 	.globl _AX5043_MATCH0PAT3
                                    809 	.globl _AX5043_MATCH0PAT2
                                    810 	.globl _AX5043_MATCH0PAT1
                                    811 	.globl _AX5043_MATCH0PAT0
                                    812 	.globl _AX5043_MATCH0MIN
                                    813 	.globl _AX5043_MATCH0MAX
                                    814 	.globl _AX5043_MATCH0LEN
                                    815 	.globl _AX5043_LPOSCSTATUS
                                    816 	.globl _AX5043_LPOSCREF1
                                    817 	.globl _AX5043_LPOSCREF0
                                    818 	.globl _AX5043_LPOSCPER1
                                    819 	.globl _AX5043_LPOSCPER0
                                    820 	.globl _AX5043_LPOSCKFILT1
                                    821 	.globl _AX5043_LPOSCKFILT0
                                    822 	.globl _AX5043_LPOSCFREQ1
                                    823 	.globl _AX5043_LPOSCFREQ0
                                    824 	.globl _AX5043_LPOSCCONFIG
                                    825 	.globl _AX5043_IRQREQUEST1
                                    826 	.globl _AX5043_IRQREQUEST0
                                    827 	.globl _AX5043_IRQMASK1
                                    828 	.globl _AX5043_IRQMASK0
                                    829 	.globl _AX5043_IRQINVERSION1
                                    830 	.globl _AX5043_IRQINVERSION0
                                    831 	.globl _AX5043_IFFREQ1
                                    832 	.globl _AX5043_IFFREQ0
                                    833 	.globl _AX5043_GPADCPERIOD
                                    834 	.globl _AX5043_GPADCCTRL
                                    835 	.globl _AX5043_GPADC13VALUE1
                                    836 	.globl _AX5043_GPADC13VALUE0
                                    837 	.globl _AX5043_FSKDMIN1
                                    838 	.globl _AX5043_FSKDMIN0
                                    839 	.globl _AX5043_FSKDMAX1
                                    840 	.globl _AX5043_FSKDMAX0
                                    841 	.globl _AX5043_FSKDEV2
                                    842 	.globl _AX5043_FSKDEV1
                                    843 	.globl _AX5043_FSKDEV0
                                    844 	.globl _AX5043_FREQB3
                                    845 	.globl _AX5043_FREQB2
                                    846 	.globl _AX5043_FREQB1
                                    847 	.globl _AX5043_FREQB0
                                    848 	.globl _AX5043_FREQA3
                                    849 	.globl _AX5043_FREQA2
                                    850 	.globl _AX5043_FREQA1
                                    851 	.globl _AX5043_FREQA0
                                    852 	.globl _AX5043_FRAMING
                                    853 	.globl _AX5043_FIFOTHRESH1
                                    854 	.globl _AX5043_FIFOTHRESH0
                                    855 	.globl _AX5043_FIFOSTAT
                                    856 	.globl _AX5043_FIFOFREE1
                                    857 	.globl _AX5043_FIFOFREE0
                                    858 	.globl _AX5043_FIFODATA
                                    859 	.globl _AX5043_FIFOCOUNT1
                                    860 	.globl _AX5043_FIFOCOUNT0
                                    861 	.globl _AX5043_FECSYNC
                                    862 	.globl _AX5043_FECSTATUS
                                    863 	.globl _AX5043_FEC
                                    864 	.globl _AX5043_ENCODING
                                    865 	.globl _AX5043_DIVERSITY
                                    866 	.globl _AX5043_DECIMATION
                                    867 	.globl _AX5043_DACVALUE1
                                    868 	.globl _AX5043_DACVALUE0
                                    869 	.globl _AX5043_DACCONFIG
                                    870 	.globl _AX5043_CRCINIT3
                                    871 	.globl _AX5043_CRCINIT2
                                    872 	.globl _AX5043_CRCINIT1
                                    873 	.globl _AX5043_CRCINIT0
                                    874 	.globl _AX5043_BGNDRSSITHR
                                    875 	.globl _AX5043_BGNDRSSIGAIN
                                    876 	.globl _AX5043_BGNDRSSI
                                    877 	.globl _AX5043_BBTUNE
                                    878 	.globl _AX5043_BBOFFSCAP
                                    879 	.globl _AX5043_AMPLFILTER
                                    880 	.globl _AX5043_AGCCOUNTER
                                    881 	.globl _AX5043_AFSKSPACE1
                                    882 	.globl _AX5043_AFSKSPACE0
                                    883 	.globl _AX5043_AFSKMARK1
                                    884 	.globl _AX5043_AFSKMARK0
                                    885 	.globl _AX5043_AFSKCTRL
                                    886 	.globl _XWTSTAT
                                    887 	.globl _XWTIRQEN
                                    888 	.globl _XWTEVTD
                                    889 	.globl _XWTEVTD1
                                    890 	.globl _XWTEVTD0
                                    891 	.globl _XWTEVTC
                                    892 	.globl _XWTEVTC1
                                    893 	.globl _XWTEVTC0
                                    894 	.globl _XWTEVTB
                                    895 	.globl _XWTEVTB1
                                    896 	.globl _XWTEVTB0
                                    897 	.globl _XWTEVTA
                                    898 	.globl _XWTEVTA1
                                    899 	.globl _XWTEVTA0
                                    900 	.globl _XWTCNTR1
                                    901 	.globl _XWTCNTB
                                    902 	.globl _XWTCNTB1
                                    903 	.globl _XWTCNTB0
                                    904 	.globl _XWTCNTA
                                    905 	.globl _XWTCNTA1
                                    906 	.globl _XWTCNTA0
                                    907 	.globl _XWTCFGB
                                    908 	.globl _XWTCFGA
                                    909 	.globl _XWDTRESET
                                    910 	.globl _XWDTCFG
                                    911 	.globl _XU1STATUS
                                    912 	.globl _XU1SHREG
                                    913 	.globl _XU1MODE
                                    914 	.globl _XU1CTRL
                                    915 	.globl _XU0STATUS
                                    916 	.globl _XU0SHREG
                                    917 	.globl _XU0MODE
                                    918 	.globl _XU0CTRL
                                    919 	.globl _XT2STATUS
                                    920 	.globl _XT2PERIOD
                                    921 	.globl _XT2PERIOD1
                                    922 	.globl _XT2PERIOD0
                                    923 	.globl _XT2MODE
                                    924 	.globl _XT2CNT
                                    925 	.globl _XT2CNT1
                                    926 	.globl _XT2CNT0
                                    927 	.globl _XT2CLKSRC
                                    928 	.globl _XT1STATUS
                                    929 	.globl _XT1PERIOD
                                    930 	.globl _XT1PERIOD1
                                    931 	.globl _XT1PERIOD0
                                    932 	.globl _XT1MODE
                                    933 	.globl _XT1CNT
                                    934 	.globl _XT1CNT1
                                    935 	.globl _XT1CNT0
                                    936 	.globl _XT1CLKSRC
                                    937 	.globl _XT0STATUS
                                    938 	.globl _XT0PERIOD
                                    939 	.globl _XT0PERIOD1
                                    940 	.globl _XT0PERIOD0
                                    941 	.globl _XT0MODE
                                    942 	.globl _XT0CNT
                                    943 	.globl _XT0CNT1
                                    944 	.globl _XT0CNT0
                                    945 	.globl _XT0CLKSRC
                                    946 	.globl _XSPSTATUS
                                    947 	.globl _XSPSHREG
                                    948 	.globl _XSPMODE
                                    949 	.globl _XSPCLKSRC
                                    950 	.globl _XRADIOSTAT
                                    951 	.globl _XRADIOSTAT1
                                    952 	.globl _XRADIOSTAT0
                                    953 	.globl _XRADIODATA3
                                    954 	.globl _XRADIODATA2
                                    955 	.globl _XRADIODATA1
                                    956 	.globl _XRADIODATA0
                                    957 	.globl _XRADIOADDR1
                                    958 	.globl _XRADIOADDR0
                                    959 	.globl _XRADIOACC
                                    960 	.globl _XOC1STATUS
                                    961 	.globl _XOC1PIN
                                    962 	.globl _XOC1MODE
                                    963 	.globl _XOC1COMP
                                    964 	.globl _XOC1COMP1
                                    965 	.globl _XOC1COMP0
                                    966 	.globl _XOC0STATUS
                                    967 	.globl _XOC0PIN
                                    968 	.globl _XOC0MODE
                                    969 	.globl _XOC0COMP
                                    970 	.globl _XOC0COMP1
                                    971 	.globl _XOC0COMP0
                                    972 	.globl _XNVSTATUS
                                    973 	.globl _XNVKEY
                                    974 	.globl _XNVDATA
                                    975 	.globl _XNVDATA1
                                    976 	.globl _XNVDATA0
                                    977 	.globl _XNVADDR
                                    978 	.globl _XNVADDR1
                                    979 	.globl _XNVADDR0
                                    980 	.globl _XIC1STATUS
                                    981 	.globl _XIC1MODE
                                    982 	.globl _XIC1CAPT
                                    983 	.globl _XIC1CAPT1
                                    984 	.globl _XIC1CAPT0
                                    985 	.globl _XIC0STATUS
                                    986 	.globl _XIC0MODE
                                    987 	.globl _XIC0CAPT
                                    988 	.globl _XIC0CAPT1
                                    989 	.globl _XIC0CAPT0
                                    990 	.globl _XPORTR
                                    991 	.globl _XPORTC
                                    992 	.globl _XPORTB
                                    993 	.globl _XPORTA
                                    994 	.globl _XPINR
                                    995 	.globl _XPINC
                                    996 	.globl _XPINB
                                    997 	.globl _XPINA
                                    998 	.globl _XDIRR
                                    999 	.globl _XDIRC
                                   1000 	.globl _XDIRB
                                   1001 	.globl _XDIRA
                                   1002 	.globl _XDBGLNKSTAT
                                   1003 	.globl _XDBGLNKBUF
                                   1004 	.globl _XCODECONFIG
                                   1005 	.globl _XCLKSTAT
                                   1006 	.globl _XCLKCON
                                   1007 	.globl _XANALOGCOMP
                                   1008 	.globl _XADCCONV
                                   1009 	.globl _XADCCLKSRC
                                   1010 	.globl _XADCCH3CONFIG
                                   1011 	.globl _XADCCH2CONFIG
                                   1012 	.globl _XADCCH1CONFIG
                                   1013 	.globl _XADCCH0CONFIG
                                   1014 	.globl _XPCON
                                   1015 	.globl _XIP
                                   1016 	.globl _XIE
                                   1017 	.globl _XDPTR1
                                   1018 	.globl _XDPTR0
                                   1019 	.globl _XTALREADY
                                   1020 	.globl _XTALOSC
                                   1021 	.globl _XTALAMPL
                                   1022 	.globl _SILICONREV
                                   1023 	.globl _SCRATCH3
                                   1024 	.globl _SCRATCH2
                                   1025 	.globl _SCRATCH1
                                   1026 	.globl _SCRATCH0
                                   1027 	.globl _RADIOMUX
                                   1028 	.globl _RADIOFSTATADDR
                                   1029 	.globl _RADIOFSTATADDR1
                                   1030 	.globl _RADIOFSTATADDR0
                                   1031 	.globl _RADIOFDATAADDR
                                   1032 	.globl _RADIOFDATAADDR1
                                   1033 	.globl _RADIOFDATAADDR0
                                   1034 	.globl _OSCRUN
                                   1035 	.globl _OSCREADY
                                   1036 	.globl _OSCFORCERUN
                                   1037 	.globl _OSCCALIB
                                   1038 	.globl _MISCCTRL
                                   1039 	.globl _LPXOSCGM
                                   1040 	.globl _LPOSCREF
                                   1041 	.globl _LPOSCREF1
                                   1042 	.globl _LPOSCREF0
                                   1043 	.globl _LPOSCPER
                                   1044 	.globl _LPOSCPER1
                                   1045 	.globl _LPOSCPER0
                                   1046 	.globl _LPOSCKFILT
                                   1047 	.globl _LPOSCKFILT1
                                   1048 	.globl _LPOSCKFILT0
                                   1049 	.globl _LPOSCFREQ
                                   1050 	.globl _LPOSCFREQ1
                                   1051 	.globl _LPOSCFREQ0
                                   1052 	.globl _LPOSCCONFIG
                                   1053 	.globl _PINSEL
                                   1054 	.globl _PINCHGC
                                   1055 	.globl _PINCHGB
                                   1056 	.globl _PINCHGA
                                   1057 	.globl _PALTRADIO
                                   1058 	.globl _PALTC
                                   1059 	.globl _PALTB
                                   1060 	.globl _PALTA
                                   1061 	.globl _INTCHGC
                                   1062 	.globl _INTCHGB
                                   1063 	.globl _INTCHGA
                                   1064 	.globl _EXTIRQ
                                   1065 	.globl _GPIOENABLE
                                   1066 	.globl _ANALOGA
                                   1067 	.globl _FRCOSCREF
                                   1068 	.globl _FRCOSCREF1
                                   1069 	.globl _FRCOSCREF0
                                   1070 	.globl _FRCOSCPER
                                   1071 	.globl _FRCOSCPER1
                                   1072 	.globl _FRCOSCPER0
                                   1073 	.globl _FRCOSCKFILT
                                   1074 	.globl _FRCOSCKFILT1
                                   1075 	.globl _FRCOSCKFILT0
                                   1076 	.globl _FRCOSCFREQ
                                   1077 	.globl _FRCOSCFREQ1
                                   1078 	.globl _FRCOSCFREQ0
                                   1079 	.globl _FRCOSCCTRL
                                   1080 	.globl _FRCOSCCONFIG
                                   1081 	.globl _DMA1CONFIG
                                   1082 	.globl _DMA1ADDR
                                   1083 	.globl _DMA1ADDR1
                                   1084 	.globl _DMA1ADDR0
                                   1085 	.globl _DMA0CONFIG
                                   1086 	.globl _DMA0ADDR
                                   1087 	.globl _DMA0ADDR1
                                   1088 	.globl _DMA0ADDR0
                                   1089 	.globl _ADCTUNE2
                                   1090 	.globl _ADCTUNE1
                                   1091 	.globl _ADCTUNE0
                                   1092 	.globl _ADCCH3VAL
                                   1093 	.globl _ADCCH3VAL1
                                   1094 	.globl _ADCCH3VAL0
                                   1095 	.globl _ADCCH2VAL
                                   1096 	.globl _ADCCH2VAL1
                                   1097 	.globl _ADCCH2VAL0
                                   1098 	.globl _ADCCH1VAL
                                   1099 	.globl _ADCCH1VAL1
                                   1100 	.globl _ADCCH1VAL0
                                   1101 	.globl _ADCCH0VAL
                                   1102 	.globl _ADCCH0VAL1
                                   1103 	.globl _ADCCH0VAL0
                                   1104 	.globl _coldstart
                                   1105 	.globl _pkt_counter
                                   1106 	.globl _transmit_packet
                                   1107 	.globl _axradio_statuschange
                                   1108 ;--------------------------------------------------------
                                   1109 ; special function registers
                                   1110 ;--------------------------------------------------------
                                   1111 	.area RSEG    (ABS,DATA)
      000000                       1112 	.org 0x0000
                           0000E0  1113 _ACC	=	0x00e0
                           0000F0  1114 _B	=	0x00f0
                           000083  1115 _DPH	=	0x0083
                           000085  1116 _DPH1	=	0x0085
                           000082  1117 _DPL	=	0x0082
                           000084  1118 _DPL1	=	0x0084
                           008382  1119 _DPTR0	=	0x8382
                           008584  1120 _DPTR1	=	0x8584
                           000086  1121 _DPS	=	0x0086
                           0000A0  1122 _E2IE	=	0x00a0
                           0000C0  1123 _E2IP	=	0x00c0
                           000098  1124 _EIE	=	0x0098
                           0000B0  1125 _EIP	=	0x00b0
                           0000A8  1126 _IE	=	0x00a8
                           0000B8  1127 _IP	=	0x00b8
                           000087  1128 _PCON	=	0x0087
                           0000D0  1129 _PSW	=	0x00d0
                           000081  1130 _SP	=	0x0081
                           0000D9  1131 _XPAGE	=	0x00d9
                           0000D9  1132 __XPAGE	=	0x00d9
                           0000CA  1133 _ADCCH0CONFIG	=	0x00ca
                           0000CB  1134 _ADCCH1CONFIG	=	0x00cb
                           0000D2  1135 _ADCCH2CONFIG	=	0x00d2
                           0000D3  1136 _ADCCH3CONFIG	=	0x00d3
                           0000D1  1137 _ADCCLKSRC	=	0x00d1
                           0000C9  1138 _ADCCONV	=	0x00c9
                           0000E1  1139 _ANALOGCOMP	=	0x00e1
                           0000C6  1140 _CLKCON	=	0x00c6
                           0000C7  1141 _CLKSTAT	=	0x00c7
                           000097  1142 _CODECONFIG	=	0x0097
                           0000E3  1143 _DBGLNKBUF	=	0x00e3
                           0000E2  1144 _DBGLNKSTAT	=	0x00e2
                           000089  1145 _DIRA	=	0x0089
                           00008A  1146 _DIRB	=	0x008a
                           00008B  1147 _DIRC	=	0x008b
                           00008E  1148 _DIRR	=	0x008e
                           0000C8  1149 _PINA	=	0x00c8
                           0000E8  1150 _PINB	=	0x00e8
                           0000F8  1151 _PINC	=	0x00f8
                           00008D  1152 _PINR	=	0x008d
                           000080  1153 _PORTA	=	0x0080
                           000088  1154 _PORTB	=	0x0088
                           000090  1155 _PORTC	=	0x0090
                           00008C  1156 _PORTR	=	0x008c
                           0000CE  1157 _IC0CAPT0	=	0x00ce
                           0000CF  1158 _IC0CAPT1	=	0x00cf
                           00CFCE  1159 _IC0CAPT	=	0xcfce
                           0000CC  1160 _IC0MODE	=	0x00cc
                           0000CD  1161 _IC0STATUS	=	0x00cd
                           0000D6  1162 _IC1CAPT0	=	0x00d6
                           0000D7  1163 _IC1CAPT1	=	0x00d7
                           00D7D6  1164 _IC1CAPT	=	0xd7d6
                           0000D4  1165 _IC1MODE	=	0x00d4
                           0000D5  1166 _IC1STATUS	=	0x00d5
                           000092  1167 _NVADDR0	=	0x0092
                           000093  1168 _NVADDR1	=	0x0093
                           009392  1169 _NVADDR	=	0x9392
                           000094  1170 _NVDATA0	=	0x0094
                           000095  1171 _NVDATA1	=	0x0095
                           009594  1172 _NVDATA	=	0x9594
                           000096  1173 _NVKEY	=	0x0096
                           000091  1174 _NVSTATUS	=	0x0091
                           0000BC  1175 _OC0COMP0	=	0x00bc
                           0000BD  1176 _OC0COMP1	=	0x00bd
                           00BDBC  1177 _OC0COMP	=	0xbdbc
                           0000B9  1178 _OC0MODE	=	0x00b9
                           0000BA  1179 _OC0PIN	=	0x00ba
                           0000BB  1180 _OC0STATUS	=	0x00bb
                           0000C4  1181 _OC1COMP0	=	0x00c4
                           0000C5  1182 _OC1COMP1	=	0x00c5
                           00C5C4  1183 _OC1COMP	=	0xc5c4
                           0000C1  1184 _OC1MODE	=	0x00c1
                           0000C2  1185 _OC1PIN	=	0x00c2
                           0000C3  1186 _OC1STATUS	=	0x00c3
                           0000B1  1187 _RADIOACC	=	0x00b1
                           0000B3  1188 _RADIOADDR0	=	0x00b3
                           0000B2  1189 _RADIOADDR1	=	0x00b2
                           00B2B3  1190 _RADIOADDR	=	0xb2b3
                           0000B7  1191 _RADIODATA0	=	0x00b7
                           0000B6  1192 _RADIODATA1	=	0x00b6
                           0000B5  1193 _RADIODATA2	=	0x00b5
                           0000B4  1194 _RADIODATA3	=	0x00b4
                           B4B5B6B7  1195 _RADIODATA	=	0xb4b5b6b7
                           0000BE  1196 _RADIOSTAT0	=	0x00be
                           0000BF  1197 _RADIOSTAT1	=	0x00bf
                           00BFBE  1198 _RADIOSTAT	=	0xbfbe
                           0000DF  1199 _SPCLKSRC	=	0x00df
                           0000DC  1200 _SPMODE	=	0x00dc
                           0000DE  1201 _SPSHREG	=	0x00de
                           0000DD  1202 _SPSTATUS	=	0x00dd
                           00009A  1203 _T0CLKSRC	=	0x009a
                           00009C  1204 _T0CNT0	=	0x009c
                           00009D  1205 _T0CNT1	=	0x009d
                           009D9C  1206 _T0CNT	=	0x9d9c
                           000099  1207 _T0MODE	=	0x0099
                           00009E  1208 _T0PERIOD0	=	0x009e
                           00009F  1209 _T0PERIOD1	=	0x009f
                           009F9E  1210 _T0PERIOD	=	0x9f9e
                           00009B  1211 _T0STATUS	=	0x009b
                           0000A2  1212 _T1CLKSRC	=	0x00a2
                           0000A4  1213 _T1CNT0	=	0x00a4
                           0000A5  1214 _T1CNT1	=	0x00a5
                           00A5A4  1215 _T1CNT	=	0xa5a4
                           0000A1  1216 _T1MODE	=	0x00a1
                           0000A6  1217 _T1PERIOD0	=	0x00a6
                           0000A7  1218 _T1PERIOD1	=	0x00a7
                           00A7A6  1219 _T1PERIOD	=	0xa7a6
                           0000A3  1220 _T1STATUS	=	0x00a3
                           0000AA  1221 _T2CLKSRC	=	0x00aa
                           0000AC  1222 _T2CNT0	=	0x00ac
                           0000AD  1223 _T2CNT1	=	0x00ad
                           00ADAC  1224 _T2CNT	=	0xadac
                           0000A9  1225 _T2MODE	=	0x00a9
                           0000AE  1226 _T2PERIOD0	=	0x00ae
                           0000AF  1227 _T2PERIOD1	=	0x00af
                           00AFAE  1228 _T2PERIOD	=	0xafae
                           0000AB  1229 _T2STATUS	=	0x00ab
                           0000E4  1230 _U0CTRL	=	0x00e4
                           0000E7  1231 _U0MODE	=	0x00e7
                           0000E6  1232 _U0SHREG	=	0x00e6
                           0000E5  1233 _U0STATUS	=	0x00e5
                           0000EC  1234 _U1CTRL	=	0x00ec
                           0000EF  1235 _U1MODE	=	0x00ef
                           0000EE  1236 _U1SHREG	=	0x00ee
                           0000ED  1237 _U1STATUS	=	0x00ed
                           0000DA  1238 _WDTCFG	=	0x00da
                           0000DB  1239 _WDTRESET	=	0x00db
                           0000F1  1240 _WTCFGA	=	0x00f1
                           0000F9  1241 _WTCFGB	=	0x00f9
                           0000F2  1242 _WTCNTA0	=	0x00f2
                           0000F3  1243 _WTCNTA1	=	0x00f3
                           00F3F2  1244 _WTCNTA	=	0xf3f2
                           0000FA  1245 _WTCNTB0	=	0x00fa
                           0000FB  1246 _WTCNTB1	=	0x00fb
                           00FBFA  1247 _WTCNTB	=	0xfbfa
                           0000EB  1248 _WTCNTR1	=	0x00eb
                           0000F4  1249 _WTEVTA0	=	0x00f4
                           0000F5  1250 _WTEVTA1	=	0x00f5
                           00F5F4  1251 _WTEVTA	=	0xf5f4
                           0000F6  1252 _WTEVTB0	=	0x00f6
                           0000F7  1253 _WTEVTB1	=	0x00f7
                           00F7F6  1254 _WTEVTB	=	0xf7f6
                           0000FC  1255 _WTEVTC0	=	0x00fc
                           0000FD  1256 _WTEVTC1	=	0x00fd
                           00FDFC  1257 _WTEVTC	=	0xfdfc
                           0000FE  1258 _WTEVTD0	=	0x00fe
                           0000FF  1259 _WTEVTD1	=	0x00ff
                           00FFFE  1260 _WTEVTD	=	0xfffe
                           0000E9  1261 _WTIRQEN	=	0x00e9
                           0000EA  1262 _WTSTAT	=	0x00ea
                                   1263 ;--------------------------------------------------------
                                   1264 ; special function bits
                                   1265 ;--------------------------------------------------------
                                   1266 	.area RSEG    (ABS,DATA)
      000000                       1267 	.org 0x0000
                           0000E0  1268 _ACC_0	=	0x00e0
                           0000E1  1269 _ACC_1	=	0x00e1
                           0000E2  1270 _ACC_2	=	0x00e2
                           0000E3  1271 _ACC_3	=	0x00e3
                           0000E4  1272 _ACC_4	=	0x00e4
                           0000E5  1273 _ACC_5	=	0x00e5
                           0000E6  1274 _ACC_6	=	0x00e6
                           0000E7  1275 _ACC_7	=	0x00e7
                           0000F0  1276 _B_0	=	0x00f0
                           0000F1  1277 _B_1	=	0x00f1
                           0000F2  1278 _B_2	=	0x00f2
                           0000F3  1279 _B_3	=	0x00f3
                           0000F4  1280 _B_4	=	0x00f4
                           0000F5  1281 _B_5	=	0x00f5
                           0000F6  1282 _B_6	=	0x00f6
                           0000F7  1283 _B_7	=	0x00f7
                           0000A0  1284 _E2IE_0	=	0x00a0
                           0000A1  1285 _E2IE_1	=	0x00a1
                           0000A2  1286 _E2IE_2	=	0x00a2
                           0000A3  1287 _E2IE_3	=	0x00a3
                           0000A4  1288 _E2IE_4	=	0x00a4
                           0000A5  1289 _E2IE_5	=	0x00a5
                           0000A6  1290 _E2IE_6	=	0x00a6
                           0000A7  1291 _E2IE_7	=	0x00a7
                           0000C0  1292 _E2IP_0	=	0x00c0
                           0000C1  1293 _E2IP_1	=	0x00c1
                           0000C2  1294 _E2IP_2	=	0x00c2
                           0000C3  1295 _E2IP_3	=	0x00c3
                           0000C4  1296 _E2IP_4	=	0x00c4
                           0000C5  1297 _E2IP_5	=	0x00c5
                           0000C6  1298 _E2IP_6	=	0x00c6
                           0000C7  1299 _E2IP_7	=	0x00c7
                           000098  1300 _EIE_0	=	0x0098
                           000099  1301 _EIE_1	=	0x0099
                           00009A  1302 _EIE_2	=	0x009a
                           00009B  1303 _EIE_3	=	0x009b
                           00009C  1304 _EIE_4	=	0x009c
                           00009D  1305 _EIE_5	=	0x009d
                           00009E  1306 _EIE_6	=	0x009e
                           00009F  1307 _EIE_7	=	0x009f
                           0000B0  1308 _EIP_0	=	0x00b0
                           0000B1  1309 _EIP_1	=	0x00b1
                           0000B2  1310 _EIP_2	=	0x00b2
                           0000B3  1311 _EIP_3	=	0x00b3
                           0000B4  1312 _EIP_4	=	0x00b4
                           0000B5  1313 _EIP_5	=	0x00b5
                           0000B6  1314 _EIP_6	=	0x00b6
                           0000B7  1315 _EIP_7	=	0x00b7
                           0000A8  1316 _IE_0	=	0x00a8
                           0000A9  1317 _IE_1	=	0x00a9
                           0000AA  1318 _IE_2	=	0x00aa
                           0000AB  1319 _IE_3	=	0x00ab
                           0000AC  1320 _IE_4	=	0x00ac
                           0000AD  1321 _IE_5	=	0x00ad
                           0000AE  1322 _IE_6	=	0x00ae
                           0000AF  1323 _IE_7	=	0x00af
                           0000AF  1324 _EA	=	0x00af
                           0000B8  1325 _IP_0	=	0x00b8
                           0000B9  1326 _IP_1	=	0x00b9
                           0000BA  1327 _IP_2	=	0x00ba
                           0000BB  1328 _IP_3	=	0x00bb
                           0000BC  1329 _IP_4	=	0x00bc
                           0000BD  1330 _IP_5	=	0x00bd
                           0000BE  1331 _IP_6	=	0x00be
                           0000BF  1332 _IP_7	=	0x00bf
                           0000D0  1333 _P	=	0x00d0
                           0000D1  1334 _F1	=	0x00d1
                           0000D2  1335 _OV	=	0x00d2
                           0000D3  1336 _RS0	=	0x00d3
                           0000D4  1337 _RS1	=	0x00d4
                           0000D5  1338 _F0	=	0x00d5
                           0000D6  1339 _AC	=	0x00d6
                           0000D7  1340 _CY	=	0x00d7
                           0000C8  1341 _PINA_0	=	0x00c8
                           0000C9  1342 _PINA_1	=	0x00c9
                           0000CA  1343 _PINA_2	=	0x00ca
                           0000CB  1344 _PINA_3	=	0x00cb
                           0000CC  1345 _PINA_4	=	0x00cc
                           0000CD  1346 _PINA_5	=	0x00cd
                           0000CE  1347 _PINA_6	=	0x00ce
                           0000CF  1348 _PINA_7	=	0x00cf
                           0000E8  1349 _PINB_0	=	0x00e8
                           0000E9  1350 _PINB_1	=	0x00e9
                           0000EA  1351 _PINB_2	=	0x00ea
                           0000EB  1352 _PINB_3	=	0x00eb
                           0000EC  1353 _PINB_4	=	0x00ec
                           0000ED  1354 _PINB_5	=	0x00ed
                           0000EE  1355 _PINB_6	=	0x00ee
                           0000EF  1356 _PINB_7	=	0x00ef
                           0000F8  1357 _PINC_0	=	0x00f8
                           0000F9  1358 _PINC_1	=	0x00f9
                           0000FA  1359 _PINC_2	=	0x00fa
                           0000FB  1360 _PINC_3	=	0x00fb
                           0000FC  1361 _PINC_4	=	0x00fc
                           0000FD  1362 _PINC_5	=	0x00fd
                           0000FE  1363 _PINC_6	=	0x00fe
                           0000FF  1364 _PINC_7	=	0x00ff
                           000080  1365 _PORTA_0	=	0x0080
                           000081  1366 _PORTA_1	=	0x0081
                           000082  1367 _PORTA_2	=	0x0082
                           000083  1368 _PORTA_3	=	0x0083
                           000084  1369 _PORTA_4	=	0x0084
                           000085  1370 _PORTA_5	=	0x0085
                           000086  1371 _PORTA_6	=	0x0086
                           000087  1372 _PORTA_7	=	0x0087
                           000088  1373 _PORTB_0	=	0x0088
                           000089  1374 _PORTB_1	=	0x0089
                           00008A  1375 _PORTB_2	=	0x008a
                           00008B  1376 _PORTB_3	=	0x008b
                           00008C  1377 _PORTB_4	=	0x008c
                           00008D  1378 _PORTB_5	=	0x008d
                           00008E  1379 _PORTB_6	=	0x008e
                           00008F  1380 _PORTB_7	=	0x008f
                           000090  1381 _PORTC_0	=	0x0090
                           000091  1382 _PORTC_1	=	0x0091
                           000092  1383 _PORTC_2	=	0x0092
                           000093  1384 _PORTC_3	=	0x0093
                           000094  1385 _PORTC_4	=	0x0094
                           000095  1386 _PORTC_5	=	0x0095
                           000096  1387 _PORTC_6	=	0x0096
                           000097  1388 _PORTC_7	=	0x0097
                                   1389 ;--------------------------------------------------------
                                   1390 ; overlayable register banks
                                   1391 ;--------------------------------------------------------
                                   1392 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                       1393 	.ds 8
                                   1394 ;--------------------------------------------------------
                                   1395 ; internal ram data
                                   1396 ;--------------------------------------------------------
                                   1397 	.area DSEG    (DATA)
      000008                       1398 _pkt_counter::
      000008                       1399 	.ds 2
      00000A                       1400 _coldstart::
      00000A                       1401 	.ds 1
                                   1402 ;--------------------------------------------------------
                                   1403 ; overlayable items in internal ram 
                                   1404 ;--------------------------------------------------------
                                   1405 ;--------------------------------------------------------
                                   1406 ; Stack segment in internal ram 
                                   1407 ;--------------------------------------------------------
                                   1408 	.area	SSEG
      00004D                       1409 __start__stack:
      00004D                       1410 	.ds	1
                                   1411 
                                   1412 ;--------------------------------------------------------
                                   1413 ; indirectly addressable internal ram data
                                   1414 ;--------------------------------------------------------
                                   1415 	.area ISEG    (DATA)
                                   1416 ;--------------------------------------------------------
                                   1417 ; absolute internal ram data
                                   1418 ;--------------------------------------------------------
                                   1419 	.area IABS    (ABS,DATA)
                                   1420 	.area IABS    (ABS,DATA)
                                   1421 ;--------------------------------------------------------
                                   1422 ; bit data
                                   1423 ;--------------------------------------------------------
                                   1424 	.area BSEG    (BIT)
      000000                       1425 __sdcc_external_startup_sloc0_1_0:
      000000                       1426 	.ds 1
                                   1427 ;--------------------------------------------------------
                                   1428 ; paged external ram data
                                   1429 ;--------------------------------------------------------
                                   1430 	.area PSEG    (PAG,XDATA)
                                   1431 ;--------------------------------------------------------
                                   1432 ; external ram data
                                   1433 ;--------------------------------------------------------
                                   1434 	.area XSEG    (XDATA)
                           007020  1435 _ADCCH0VAL0	=	0x7020
                           007021  1436 _ADCCH0VAL1	=	0x7021
                           007020  1437 _ADCCH0VAL	=	0x7020
                           007022  1438 _ADCCH1VAL0	=	0x7022
                           007023  1439 _ADCCH1VAL1	=	0x7023
                           007022  1440 _ADCCH1VAL	=	0x7022
                           007024  1441 _ADCCH2VAL0	=	0x7024
                           007025  1442 _ADCCH2VAL1	=	0x7025
                           007024  1443 _ADCCH2VAL	=	0x7024
                           007026  1444 _ADCCH3VAL0	=	0x7026
                           007027  1445 _ADCCH3VAL1	=	0x7027
                           007026  1446 _ADCCH3VAL	=	0x7026
                           007028  1447 _ADCTUNE0	=	0x7028
                           007029  1448 _ADCTUNE1	=	0x7029
                           00702A  1449 _ADCTUNE2	=	0x702a
                           007010  1450 _DMA0ADDR0	=	0x7010
                           007011  1451 _DMA0ADDR1	=	0x7011
                           007010  1452 _DMA0ADDR	=	0x7010
                           007014  1453 _DMA0CONFIG	=	0x7014
                           007012  1454 _DMA1ADDR0	=	0x7012
                           007013  1455 _DMA1ADDR1	=	0x7013
                           007012  1456 _DMA1ADDR	=	0x7012
                           007015  1457 _DMA1CONFIG	=	0x7015
                           007070  1458 _FRCOSCCONFIG	=	0x7070
                           007071  1459 _FRCOSCCTRL	=	0x7071
                           007076  1460 _FRCOSCFREQ0	=	0x7076
                           007077  1461 _FRCOSCFREQ1	=	0x7077
                           007076  1462 _FRCOSCFREQ	=	0x7076
                           007072  1463 _FRCOSCKFILT0	=	0x7072
                           007073  1464 _FRCOSCKFILT1	=	0x7073
                           007072  1465 _FRCOSCKFILT	=	0x7072
                           007078  1466 _FRCOSCPER0	=	0x7078
                           007079  1467 _FRCOSCPER1	=	0x7079
                           007078  1468 _FRCOSCPER	=	0x7078
                           007074  1469 _FRCOSCREF0	=	0x7074
                           007075  1470 _FRCOSCREF1	=	0x7075
                           007074  1471 _FRCOSCREF	=	0x7074
                           007007  1472 _ANALOGA	=	0x7007
                           00700C  1473 _GPIOENABLE	=	0x700c
                           007003  1474 _EXTIRQ	=	0x7003
                           007000  1475 _INTCHGA	=	0x7000
                           007001  1476 _INTCHGB	=	0x7001
                           007002  1477 _INTCHGC	=	0x7002
                           007008  1478 _PALTA	=	0x7008
                           007009  1479 _PALTB	=	0x7009
                           00700A  1480 _PALTC	=	0x700a
                           007046  1481 _PALTRADIO	=	0x7046
                           007004  1482 _PINCHGA	=	0x7004
                           007005  1483 _PINCHGB	=	0x7005
                           007006  1484 _PINCHGC	=	0x7006
                           00700B  1485 _PINSEL	=	0x700b
                           007060  1486 _LPOSCCONFIG	=	0x7060
                           007066  1487 _LPOSCFREQ0	=	0x7066
                           007067  1488 _LPOSCFREQ1	=	0x7067
                           007066  1489 _LPOSCFREQ	=	0x7066
                           007062  1490 _LPOSCKFILT0	=	0x7062
                           007063  1491 _LPOSCKFILT1	=	0x7063
                           007062  1492 _LPOSCKFILT	=	0x7062
                           007068  1493 _LPOSCPER0	=	0x7068
                           007069  1494 _LPOSCPER1	=	0x7069
                           007068  1495 _LPOSCPER	=	0x7068
                           007064  1496 _LPOSCREF0	=	0x7064
                           007065  1497 _LPOSCREF1	=	0x7065
                           007064  1498 _LPOSCREF	=	0x7064
                           007054  1499 _LPXOSCGM	=	0x7054
                           007F01  1500 _MISCCTRL	=	0x7f01
                           007053  1501 _OSCCALIB	=	0x7053
                           007050  1502 _OSCFORCERUN	=	0x7050
                           007052  1503 _OSCREADY	=	0x7052
                           007051  1504 _OSCRUN	=	0x7051
                           007040  1505 _RADIOFDATAADDR0	=	0x7040
                           007041  1506 _RADIOFDATAADDR1	=	0x7041
                           007040  1507 _RADIOFDATAADDR	=	0x7040
                           007042  1508 _RADIOFSTATADDR0	=	0x7042
                           007043  1509 _RADIOFSTATADDR1	=	0x7043
                           007042  1510 _RADIOFSTATADDR	=	0x7042
                           007044  1511 _RADIOMUX	=	0x7044
                           007084  1512 _SCRATCH0	=	0x7084
                           007085  1513 _SCRATCH1	=	0x7085
                           007086  1514 _SCRATCH2	=	0x7086
                           007087  1515 _SCRATCH3	=	0x7087
                           007F00  1516 _SILICONREV	=	0x7f00
                           007F19  1517 _XTALAMPL	=	0x7f19
                           007F18  1518 _XTALOSC	=	0x7f18
                           007F1A  1519 _XTALREADY	=	0x7f1a
                           003F82  1520 _XDPTR0	=	0x3f82
                           003F84  1521 _XDPTR1	=	0x3f84
                           003FA8  1522 _XIE	=	0x3fa8
                           003FB8  1523 _XIP	=	0x3fb8
                           003F87  1524 _XPCON	=	0x3f87
                           003FCA  1525 _XADCCH0CONFIG	=	0x3fca
                           003FCB  1526 _XADCCH1CONFIG	=	0x3fcb
                           003FD2  1527 _XADCCH2CONFIG	=	0x3fd2
                           003FD3  1528 _XADCCH3CONFIG	=	0x3fd3
                           003FD1  1529 _XADCCLKSRC	=	0x3fd1
                           003FC9  1530 _XADCCONV	=	0x3fc9
                           003FE1  1531 _XANALOGCOMP	=	0x3fe1
                           003FC6  1532 _XCLKCON	=	0x3fc6
                           003FC7  1533 _XCLKSTAT	=	0x3fc7
                           003F97  1534 _XCODECONFIG	=	0x3f97
                           003FE3  1535 _XDBGLNKBUF	=	0x3fe3
                           003FE2  1536 _XDBGLNKSTAT	=	0x3fe2
                           003F89  1537 _XDIRA	=	0x3f89
                           003F8A  1538 _XDIRB	=	0x3f8a
                           003F8B  1539 _XDIRC	=	0x3f8b
                           003F8E  1540 _XDIRR	=	0x3f8e
                           003FC8  1541 _XPINA	=	0x3fc8
                           003FE8  1542 _XPINB	=	0x3fe8
                           003FF8  1543 _XPINC	=	0x3ff8
                           003F8D  1544 _XPINR	=	0x3f8d
                           003F80  1545 _XPORTA	=	0x3f80
                           003F88  1546 _XPORTB	=	0x3f88
                           003F90  1547 _XPORTC	=	0x3f90
                           003F8C  1548 _XPORTR	=	0x3f8c
                           003FCE  1549 _XIC0CAPT0	=	0x3fce
                           003FCF  1550 _XIC0CAPT1	=	0x3fcf
                           003FCE  1551 _XIC0CAPT	=	0x3fce
                           003FCC  1552 _XIC0MODE	=	0x3fcc
                           003FCD  1553 _XIC0STATUS	=	0x3fcd
                           003FD6  1554 _XIC1CAPT0	=	0x3fd6
                           003FD7  1555 _XIC1CAPT1	=	0x3fd7
                           003FD6  1556 _XIC1CAPT	=	0x3fd6
                           003FD4  1557 _XIC1MODE	=	0x3fd4
                           003FD5  1558 _XIC1STATUS	=	0x3fd5
                           003F92  1559 _XNVADDR0	=	0x3f92
                           003F93  1560 _XNVADDR1	=	0x3f93
                           003F92  1561 _XNVADDR	=	0x3f92
                           003F94  1562 _XNVDATA0	=	0x3f94
                           003F95  1563 _XNVDATA1	=	0x3f95
                           003F94  1564 _XNVDATA	=	0x3f94
                           003F96  1565 _XNVKEY	=	0x3f96
                           003F91  1566 _XNVSTATUS	=	0x3f91
                           003FBC  1567 _XOC0COMP0	=	0x3fbc
                           003FBD  1568 _XOC0COMP1	=	0x3fbd
                           003FBC  1569 _XOC0COMP	=	0x3fbc
                           003FB9  1570 _XOC0MODE	=	0x3fb9
                           003FBA  1571 _XOC0PIN	=	0x3fba
                           003FBB  1572 _XOC0STATUS	=	0x3fbb
                           003FC4  1573 _XOC1COMP0	=	0x3fc4
                           003FC5  1574 _XOC1COMP1	=	0x3fc5
                           003FC4  1575 _XOC1COMP	=	0x3fc4
                           003FC1  1576 _XOC1MODE	=	0x3fc1
                           003FC2  1577 _XOC1PIN	=	0x3fc2
                           003FC3  1578 _XOC1STATUS	=	0x3fc3
                           003FB1  1579 _XRADIOACC	=	0x3fb1
                           003FB3  1580 _XRADIOADDR0	=	0x3fb3
                           003FB2  1581 _XRADIOADDR1	=	0x3fb2
                           003FB7  1582 _XRADIODATA0	=	0x3fb7
                           003FB6  1583 _XRADIODATA1	=	0x3fb6
                           003FB5  1584 _XRADIODATA2	=	0x3fb5
                           003FB4  1585 _XRADIODATA3	=	0x3fb4
                           003FBE  1586 _XRADIOSTAT0	=	0x3fbe
                           003FBF  1587 _XRADIOSTAT1	=	0x3fbf
                           003FBE  1588 _XRADIOSTAT	=	0x3fbe
                           003FDF  1589 _XSPCLKSRC	=	0x3fdf
                           003FDC  1590 _XSPMODE	=	0x3fdc
                           003FDE  1591 _XSPSHREG	=	0x3fde
                           003FDD  1592 _XSPSTATUS	=	0x3fdd
                           003F9A  1593 _XT0CLKSRC	=	0x3f9a
                           003F9C  1594 _XT0CNT0	=	0x3f9c
                           003F9D  1595 _XT0CNT1	=	0x3f9d
                           003F9C  1596 _XT0CNT	=	0x3f9c
                           003F99  1597 _XT0MODE	=	0x3f99
                           003F9E  1598 _XT0PERIOD0	=	0x3f9e
                           003F9F  1599 _XT0PERIOD1	=	0x3f9f
                           003F9E  1600 _XT0PERIOD	=	0x3f9e
                           003F9B  1601 _XT0STATUS	=	0x3f9b
                           003FA2  1602 _XT1CLKSRC	=	0x3fa2
                           003FA4  1603 _XT1CNT0	=	0x3fa4
                           003FA5  1604 _XT1CNT1	=	0x3fa5
                           003FA4  1605 _XT1CNT	=	0x3fa4
                           003FA1  1606 _XT1MODE	=	0x3fa1
                           003FA6  1607 _XT1PERIOD0	=	0x3fa6
                           003FA7  1608 _XT1PERIOD1	=	0x3fa7
                           003FA6  1609 _XT1PERIOD	=	0x3fa6
                           003FA3  1610 _XT1STATUS	=	0x3fa3
                           003FAA  1611 _XT2CLKSRC	=	0x3faa
                           003FAC  1612 _XT2CNT0	=	0x3fac
                           003FAD  1613 _XT2CNT1	=	0x3fad
                           003FAC  1614 _XT2CNT	=	0x3fac
                           003FA9  1615 _XT2MODE	=	0x3fa9
                           003FAE  1616 _XT2PERIOD0	=	0x3fae
                           003FAF  1617 _XT2PERIOD1	=	0x3faf
                           003FAE  1618 _XT2PERIOD	=	0x3fae
                           003FAB  1619 _XT2STATUS	=	0x3fab
                           003FE4  1620 _XU0CTRL	=	0x3fe4
                           003FE7  1621 _XU0MODE	=	0x3fe7
                           003FE6  1622 _XU0SHREG	=	0x3fe6
                           003FE5  1623 _XU0STATUS	=	0x3fe5
                           003FEC  1624 _XU1CTRL	=	0x3fec
                           003FEF  1625 _XU1MODE	=	0x3fef
                           003FEE  1626 _XU1SHREG	=	0x3fee
                           003FED  1627 _XU1STATUS	=	0x3fed
                           003FDA  1628 _XWDTCFG	=	0x3fda
                           003FDB  1629 _XWDTRESET	=	0x3fdb
                           003FF1  1630 _XWTCFGA	=	0x3ff1
                           003FF9  1631 _XWTCFGB	=	0x3ff9
                           003FF2  1632 _XWTCNTA0	=	0x3ff2
                           003FF3  1633 _XWTCNTA1	=	0x3ff3
                           003FF2  1634 _XWTCNTA	=	0x3ff2
                           003FFA  1635 _XWTCNTB0	=	0x3ffa
                           003FFB  1636 _XWTCNTB1	=	0x3ffb
                           003FFA  1637 _XWTCNTB	=	0x3ffa
                           003FEB  1638 _XWTCNTR1	=	0x3feb
                           003FF4  1639 _XWTEVTA0	=	0x3ff4
                           003FF5  1640 _XWTEVTA1	=	0x3ff5
                           003FF4  1641 _XWTEVTA	=	0x3ff4
                           003FF6  1642 _XWTEVTB0	=	0x3ff6
                           003FF7  1643 _XWTEVTB1	=	0x3ff7
                           003FF6  1644 _XWTEVTB	=	0x3ff6
                           003FFC  1645 _XWTEVTC0	=	0x3ffc
                           003FFD  1646 _XWTEVTC1	=	0x3ffd
                           003FFC  1647 _XWTEVTC	=	0x3ffc
                           003FFE  1648 _XWTEVTD0	=	0x3ffe
                           003FFF  1649 _XWTEVTD1	=	0x3fff
                           003FFE  1650 _XWTEVTD	=	0x3ffe
                           003FE9  1651 _XWTIRQEN	=	0x3fe9
                           003FEA  1652 _XWTSTAT	=	0x3fea
                           004114  1653 _AX5043_AFSKCTRL	=	0x4114
                           004113  1654 _AX5043_AFSKMARK0	=	0x4113
                           004112  1655 _AX5043_AFSKMARK1	=	0x4112
                           004111  1656 _AX5043_AFSKSPACE0	=	0x4111
                           004110  1657 _AX5043_AFSKSPACE1	=	0x4110
                           004043  1658 _AX5043_AGCCOUNTER	=	0x4043
                           004115  1659 _AX5043_AMPLFILTER	=	0x4115
                           004189  1660 _AX5043_BBOFFSCAP	=	0x4189
                           004188  1661 _AX5043_BBTUNE	=	0x4188
                           004041  1662 _AX5043_BGNDRSSI	=	0x4041
                           00422E  1663 _AX5043_BGNDRSSIGAIN	=	0x422e
                           00422F  1664 _AX5043_BGNDRSSITHR	=	0x422f
                           004017  1665 _AX5043_CRCINIT0	=	0x4017
                           004016  1666 _AX5043_CRCINIT1	=	0x4016
                           004015  1667 _AX5043_CRCINIT2	=	0x4015
                           004014  1668 _AX5043_CRCINIT3	=	0x4014
                           004332  1669 _AX5043_DACCONFIG	=	0x4332
                           004331  1670 _AX5043_DACVALUE0	=	0x4331
                           004330  1671 _AX5043_DACVALUE1	=	0x4330
                           004102  1672 _AX5043_DECIMATION	=	0x4102
                           004042  1673 _AX5043_DIVERSITY	=	0x4042
                           004011  1674 _AX5043_ENCODING	=	0x4011
                           004018  1675 _AX5043_FEC	=	0x4018
                           00401A  1676 _AX5043_FECSTATUS	=	0x401a
                           004019  1677 _AX5043_FECSYNC	=	0x4019
                           00402B  1678 _AX5043_FIFOCOUNT0	=	0x402b
                           00402A  1679 _AX5043_FIFOCOUNT1	=	0x402a
                           004029  1680 _AX5043_FIFODATA	=	0x4029
                           00402D  1681 _AX5043_FIFOFREE0	=	0x402d
                           00402C  1682 _AX5043_FIFOFREE1	=	0x402c
                           004028  1683 _AX5043_FIFOSTAT	=	0x4028
                           00402F  1684 _AX5043_FIFOTHRESH0	=	0x402f
                           00402E  1685 _AX5043_FIFOTHRESH1	=	0x402e
                           004012  1686 _AX5043_FRAMING	=	0x4012
                           004037  1687 _AX5043_FREQA0	=	0x4037
                           004036  1688 _AX5043_FREQA1	=	0x4036
                           004035  1689 _AX5043_FREQA2	=	0x4035
                           004034  1690 _AX5043_FREQA3	=	0x4034
                           00403F  1691 _AX5043_FREQB0	=	0x403f
                           00403E  1692 _AX5043_FREQB1	=	0x403e
                           00403D  1693 _AX5043_FREQB2	=	0x403d
                           00403C  1694 _AX5043_FREQB3	=	0x403c
                           004163  1695 _AX5043_FSKDEV0	=	0x4163
                           004162  1696 _AX5043_FSKDEV1	=	0x4162
                           004161  1697 _AX5043_FSKDEV2	=	0x4161
                           00410D  1698 _AX5043_FSKDMAX0	=	0x410d
                           00410C  1699 _AX5043_FSKDMAX1	=	0x410c
                           00410F  1700 _AX5043_FSKDMIN0	=	0x410f
                           00410E  1701 _AX5043_FSKDMIN1	=	0x410e
                           004309  1702 _AX5043_GPADC13VALUE0	=	0x4309
                           004308  1703 _AX5043_GPADC13VALUE1	=	0x4308
                           004300  1704 _AX5043_GPADCCTRL	=	0x4300
                           004301  1705 _AX5043_GPADCPERIOD	=	0x4301
                           004101  1706 _AX5043_IFFREQ0	=	0x4101
                           004100  1707 _AX5043_IFFREQ1	=	0x4100
                           00400B  1708 _AX5043_IRQINVERSION0	=	0x400b
                           00400A  1709 _AX5043_IRQINVERSION1	=	0x400a
                           004007  1710 _AX5043_IRQMASK0	=	0x4007
                           004006  1711 _AX5043_IRQMASK1	=	0x4006
                           00400D  1712 _AX5043_IRQREQUEST0	=	0x400d
                           00400C  1713 _AX5043_IRQREQUEST1	=	0x400c
                           004310  1714 _AX5043_LPOSCCONFIG	=	0x4310
                           004317  1715 _AX5043_LPOSCFREQ0	=	0x4317
                           004316  1716 _AX5043_LPOSCFREQ1	=	0x4316
                           004313  1717 _AX5043_LPOSCKFILT0	=	0x4313
                           004312  1718 _AX5043_LPOSCKFILT1	=	0x4312
                           004319  1719 _AX5043_LPOSCPER0	=	0x4319
                           004318  1720 _AX5043_LPOSCPER1	=	0x4318
                           004315  1721 _AX5043_LPOSCREF0	=	0x4315
                           004314  1722 _AX5043_LPOSCREF1	=	0x4314
                           004311  1723 _AX5043_LPOSCSTATUS	=	0x4311
                           004214  1724 _AX5043_MATCH0LEN	=	0x4214
                           004216  1725 _AX5043_MATCH0MAX	=	0x4216
                           004215  1726 _AX5043_MATCH0MIN	=	0x4215
                           004213  1727 _AX5043_MATCH0PAT0	=	0x4213
                           004212  1728 _AX5043_MATCH0PAT1	=	0x4212
                           004211  1729 _AX5043_MATCH0PAT2	=	0x4211
                           004210  1730 _AX5043_MATCH0PAT3	=	0x4210
                           00421C  1731 _AX5043_MATCH1LEN	=	0x421c
                           00421E  1732 _AX5043_MATCH1MAX	=	0x421e
                           00421D  1733 _AX5043_MATCH1MIN	=	0x421d
                           004219  1734 _AX5043_MATCH1PAT0	=	0x4219
                           004218  1735 _AX5043_MATCH1PAT1	=	0x4218
                           004108  1736 _AX5043_MAXDROFFSET0	=	0x4108
                           004107  1737 _AX5043_MAXDROFFSET1	=	0x4107
                           004106  1738 _AX5043_MAXDROFFSET2	=	0x4106
                           00410B  1739 _AX5043_MAXRFOFFSET0	=	0x410b
                           00410A  1740 _AX5043_MAXRFOFFSET1	=	0x410a
                           004109  1741 _AX5043_MAXRFOFFSET2	=	0x4109
                           004164  1742 _AX5043_MODCFGA	=	0x4164
                           004160  1743 _AX5043_MODCFGF	=	0x4160
                           004010  1744 _AX5043_MODULATION	=	0x4010
                           004025  1745 _AX5043_PINFUNCANTSEL	=	0x4025
                           004023  1746 _AX5043_PINFUNCDATA	=	0x4023
                           004022  1747 _AX5043_PINFUNCDCLK	=	0x4022
                           004024  1748 _AX5043_PINFUNCIRQ	=	0x4024
                           004026  1749 _AX5043_PINFUNCPWRAMP	=	0x4026
                           004021  1750 _AX5043_PINFUNCSYSCLK	=	0x4021
                           004020  1751 _AX5043_PINSTATE	=	0x4020
                           004233  1752 _AX5043_PKTACCEPTFLAGS	=	0x4233
                           004230  1753 _AX5043_PKTCHUNKSIZE	=	0x4230
                           004231  1754 _AX5043_PKTMISCFLAGS	=	0x4231
                           004232  1755 _AX5043_PKTSTOREFLAGS	=	0x4232
                           004031  1756 _AX5043_PLLCPI	=	0x4031
                           004039  1757 _AX5043_PLLCPIBOOST	=	0x4039
                           004182  1758 _AX5043_PLLLOCKDET	=	0x4182
                           004030  1759 _AX5043_PLLLOOP	=	0x4030
                           004038  1760 _AX5043_PLLLOOPBOOST	=	0x4038
                           004033  1761 _AX5043_PLLRANGINGA	=	0x4033
                           00403B  1762 _AX5043_PLLRANGINGB	=	0x403b
                           004183  1763 _AX5043_PLLRNGCLK	=	0x4183
                           004032  1764 _AX5043_PLLVCODIV	=	0x4032
                           004180  1765 _AX5043_PLLVCOI	=	0x4180
                           004181  1766 _AX5043_PLLVCOIR	=	0x4181
                           004005  1767 _AX5043_POWIRQMASK	=	0x4005
                           004003  1768 _AX5043_POWSTAT	=	0x4003
                           004004  1769 _AX5043_POWSTICKYSTAT	=	0x4004
                           004027  1770 _AX5043_PWRAMP	=	0x4027
                           004002  1771 _AX5043_PWRMODE	=	0x4002
                           004009  1772 _AX5043_RADIOEVENTMASK0	=	0x4009
                           004008  1773 _AX5043_RADIOEVENTMASK1	=	0x4008
                           00400F  1774 _AX5043_RADIOEVENTREQ0	=	0x400f
                           00400E  1775 _AX5043_RADIOEVENTREQ1	=	0x400e
                           00401C  1776 _AX5043_RADIOSTATE	=	0x401c
                           004040  1777 _AX5043_RSSI	=	0x4040
                           00422D  1778 _AX5043_RSSIABSTHR	=	0x422d
                           00422C  1779 _AX5043_RSSIREFERENCE	=	0x422c
                           004105  1780 _AX5043_RXDATARATE0	=	0x4105
                           004104  1781 _AX5043_RXDATARATE1	=	0x4104
                           004103  1782 _AX5043_RXDATARATE2	=	0x4103
                           004001  1783 _AX5043_SCRATCH	=	0x4001
                           004000  1784 _AX5043_SILICONREVISION	=	0x4000
                           00405B  1785 _AX5043_TIMER0	=	0x405b
                           00405A  1786 _AX5043_TIMER1	=	0x405a
                           004059  1787 _AX5043_TIMER2	=	0x4059
                           004227  1788 _AX5043_TMGRXAGC	=	0x4227
                           004223  1789 _AX5043_TMGRXBOOST	=	0x4223
                           004226  1790 _AX5043_TMGRXCOARSEAGC	=	0x4226
                           004225  1791 _AX5043_TMGRXOFFSACQ	=	0x4225
                           004229  1792 _AX5043_TMGRXPREAMBLE1	=	0x4229
                           00422A  1793 _AX5043_TMGRXPREAMBLE2	=	0x422a
                           00422B  1794 _AX5043_TMGRXPREAMBLE3	=	0x422b
                           004228  1795 _AX5043_TMGRXRSSI	=	0x4228
                           004224  1796 _AX5043_TMGRXSETTLE	=	0x4224
                           004220  1797 _AX5043_TMGTXBOOST	=	0x4220
                           004221  1798 _AX5043_TMGTXSETTLE	=	0x4221
                           004055  1799 _AX5043_TRKAFSKDEMOD0	=	0x4055
                           004054  1800 _AX5043_TRKAFSKDEMOD1	=	0x4054
                           004049  1801 _AX5043_TRKAMPLITUDE0	=	0x4049
                           004048  1802 _AX5043_TRKAMPLITUDE1	=	0x4048
                           004047  1803 _AX5043_TRKDATARATE0	=	0x4047
                           004046  1804 _AX5043_TRKDATARATE1	=	0x4046
                           004045  1805 _AX5043_TRKDATARATE2	=	0x4045
                           004051  1806 _AX5043_TRKFREQ0	=	0x4051
                           004050  1807 _AX5043_TRKFREQ1	=	0x4050
                           004053  1808 _AX5043_TRKFSKDEMOD0	=	0x4053
                           004052  1809 _AX5043_TRKFSKDEMOD1	=	0x4052
                           00404B  1810 _AX5043_TRKPHASE0	=	0x404b
                           00404A  1811 _AX5043_TRKPHASE1	=	0x404a
                           00404F  1812 _AX5043_TRKRFFREQ0	=	0x404f
                           00404E  1813 _AX5043_TRKRFFREQ1	=	0x404e
                           00404D  1814 _AX5043_TRKRFFREQ2	=	0x404d
                           004169  1815 _AX5043_TXPWRCOEFFA0	=	0x4169
                           004168  1816 _AX5043_TXPWRCOEFFA1	=	0x4168
                           00416B  1817 _AX5043_TXPWRCOEFFB0	=	0x416b
                           00416A  1818 _AX5043_TXPWRCOEFFB1	=	0x416a
                           00416D  1819 _AX5043_TXPWRCOEFFC0	=	0x416d
                           00416C  1820 _AX5043_TXPWRCOEFFC1	=	0x416c
                           00416F  1821 _AX5043_TXPWRCOEFFD0	=	0x416f
                           00416E  1822 _AX5043_TXPWRCOEFFD1	=	0x416e
                           004171  1823 _AX5043_TXPWRCOEFFE0	=	0x4171
                           004170  1824 _AX5043_TXPWRCOEFFE1	=	0x4170
                           004167  1825 _AX5043_TXRATE0	=	0x4167
                           004166  1826 _AX5043_TXRATE1	=	0x4166
                           004165  1827 _AX5043_TXRATE2	=	0x4165
                           00406B  1828 _AX5043_WAKEUP0	=	0x406b
                           00406A  1829 _AX5043_WAKEUP1	=	0x406a
                           00406D  1830 _AX5043_WAKEUPFREQ0	=	0x406d
                           00406C  1831 _AX5043_WAKEUPFREQ1	=	0x406c
                           004069  1832 _AX5043_WAKEUPTIMER0	=	0x4069
                           004068  1833 _AX5043_WAKEUPTIMER1	=	0x4068
                           00406E  1834 _AX5043_WAKEUPXOEARLY	=	0x406e
                           004184  1835 _AX5043_XTALCAP	=	0x4184
                           00401D  1836 _AX5043_XTALSTATUS	=	0x401d
                           004122  1837 _AX5043_AGCAHYST0	=	0x4122
                           004132  1838 _AX5043_AGCAHYST1	=	0x4132
                           004142  1839 _AX5043_AGCAHYST2	=	0x4142
                           004152  1840 _AX5043_AGCAHYST3	=	0x4152
                           004120  1841 _AX5043_AGCGAIN0	=	0x4120
                           004130  1842 _AX5043_AGCGAIN1	=	0x4130
                           004140  1843 _AX5043_AGCGAIN2	=	0x4140
                           004150  1844 _AX5043_AGCGAIN3	=	0x4150
                           004123  1845 _AX5043_AGCMINMAX0	=	0x4123
                           004133  1846 _AX5043_AGCMINMAX1	=	0x4133
                           004143  1847 _AX5043_AGCMINMAX2	=	0x4143
                           004153  1848 _AX5043_AGCMINMAX3	=	0x4153
                           004121  1849 _AX5043_AGCTARGET0	=	0x4121
                           004131  1850 _AX5043_AGCTARGET1	=	0x4131
                           004141  1851 _AX5043_AGCTARGET2	=	0x4141
                           004151  1852 _AX5043_AGCTARGET3	=	0x4151
                           00412B  1853 _AX5043_AMPLITUDEGAIN0	=	0x412b
                           00413B  1854 _AX5043_AMPLITUDEGAIN1	=	0x413b
                           00414B  1855 _AX5043_AMPLITUDEGAIN2	=	0x414b
                           00415B  1856 _AX5043_AMPLITUDEGAIN3	=	0x415b
                           00412F  1857 _AX5043_BBOFFSRES0	=	0x412f
                           00413F  1858 _AX5043_BBOFFSRES1	=	0x413f
                           00414F  1859 _AX5043_BBOFFSRES2	=	0x414f
                           00415F  1860 _AX5043_BBOFFSRES3	=	0x415f
                           004125  1861 _AX5043_DRGAIN0	=	0x4125
                           004135  1862 _AX5043_DRGAIN1	=	0x4135
                           004145  1863 _AX5043_DRGAIN2	=	0x4145
                           004155  1864 _AX5043_DRGAIN3	=	0x4155
                           00412E  1865 _AX5043_FOURFSK0	=	0x412e
                           00413E  1866 _AX5043_FOURFSK1	=	0x413e
                           00414E  1867 _AX5043_FOURFSK2	=	0x414e
                           00415E  1868 _AX5043_FOURFSK3	=	0x415e
                           00412D  1869 _AX5043_FREQDEV00	=	0x412d
                           00413D  1870 _AX5043_FREQDEV01	=	0x413d
                           00414D  1871 _AX5043_FREQDEV02	=	0x414d
                           00415D  1872 _AX5043_FREQDEV03	=	0x415d
                           00412C  1873 _AX5043_FREQDEV10	=	0x412c
                           00413C  1874 _AX5043_FREQDEV11	=	0x413c
                           00414C  1875 _AX5043_FREQDEV12	=	0x414c
                           00415C  1876 _AX5043_FREQDEV13	=	0x415c
                           004127  1877 _AX5043_FREQUENCYGAINA0	=	0x4127
                           004137  1878 _AX5043_FREQUENCYGAINA1	=	0x4137
                           004147  1879 _AX5043_FREQUENCYGAINA2	=	0x4147
                           004157  1880 _AX5043_FREQUENCYGAINA3	=	0x4157
                           004128  1881 _AX5043_FREQUENCYGAINB0	=	0x4128
                           004138  1882 _AX5043_FREQUENCYGAINB1	=	0x4138
                           004148  1883 _AX5043_FREQUENCYGAINB2	=	0x4148
                           004158  1884 _AX5043_FREQUENCYGAINB3	=	0x4158
                           004129  1885 _AX5043_FREQUENCYGAINC0	=	0x4129
                           004139  1886 _AX5043_FREQUENCYGAINC1	=	0x4139
                           004149  1887 _AX5043_FREQUENCYGAINC2	=	0x4149
                           004159  1888 _AX5043_FREQUENCYGAINC3	=	0x4159
                           00412A  1889 _AX5043_FREQUENCYGAIND0	=	0x412a
                           00413A  1890 _AX5043_FREQUENCYGAIND1	=	0x413a
                           00414A  1891 _AX5043_FREQUENCYGAIND2	=	0x414a
                           00415A  1892 _AX5043_FREQUENCYGAIND3	=	0x415a
                           004116  1893 _AX5043_FREQUENCYLEAK	=	0x4116
                           004126  1894 _AX5043_PHASEGAIN0	=	0x4126
                           004136  1895 _AX5043_PHASEGAIN1	=	0x4136
                           004146  1896 _AX5043_PHASEGAIN2	=	0x4146
                           004156  1897 _AX5043_PHASEGAIN3	=	0x4156
                           004207  1898 _AX5043_PKTADDR0	=	0x4207
                           004206  1899 _AX5043_PKTADDR1	=	0x4206
                           004205  1900 _AX5043_PKTADDR2	=	0x4205
                           004204  1901 _AX5043_PKTADDR3	=	0x4204
                           004200  1902 _AX5043_PKTADDRCFG	=	0x4200
                           00420B  1903 _AX5043_PKTADDRMASK0	=	0x420b
                           00420A  1904 _AX5043_PKTADDRMASK1	=	0x420a
                           004209  1905 _AX5043_PKTADDRMASK2	=	0x4209
                           004208  1906 _AX5043_PKTADDRMASK3	=	0x4208
                           004201  1907 _AX5043_PKTLENCFG	=	0x4201
                           004202  1908 _AX5043_PKTLENOFFSET	=	0x4202
                           004203  1909 _AX5043_PKTMAXLEN	=	0x4203
                           004118  1910 _AX5043_RXPARAMCURSET	=	0x4118
                           004117  1911 _AX5043_RXPARAMSETS	=	0x4117
                           004124  1912 _AX5043_TIMEGAIN0	=	0x4124
                           004134  1913 _AX5043_TIMEGAIN1	=	0x4134
                           004144  1914 _AX5043_TIMEGAIN2	=	0x4144
                           004154  1915 _AX5043_TIMEGAIN3	=	0x4154
                           005114  1916 _AX5043_AFSKCTRLNB	=	0x5114
                           005113  1917 _AX5043_AFSKMARK0NB	=	0x5113
                           005112  1918 _AX5043_AFSKMARK1NB	=	0x5112
                           005111  1919 _AX5043_AFSKSPACE0NB	=	0x5111
                           005110  1920 _AX5043_AFSKSPACE1NB	=	0x5110
                           005043  1921 _AX5043_AGCCOUNTERNB	=	0x5043
                           005115  1922 _AX5043_AMPLFILTERNB	=	0x5115
                           005189  1923 _AX5043_BBOFFSCAPNB	=	0x5189
                           005188  1924 _AX5043_BBTUNENB	=	0x5188
                           005041  1925 _AX5043_BGNDRSSINB	=	0x5041
                           00522E  1926 _AX5043_BGNDRSSIGAINNB	=	0x522e
                           00522F  1927 _AX5043_BGNDRSSITHRNB	=	0x522f
                           005017  1928 _AX5043_CRCINIT0NB	=	0x5017
                           005016  1929 _AX5043_CRCINIT1NB	=	0x5016
                           005015  1930 _AX5043_CRCINIT2NB	=	0x5015
                           005014  1931 _AX5043_CRCINIT3NB	=	0x5014
                           005332  1932 _AX5043_DACCONFIGNB	=	0x5332
                           005331  1933 _AX5043_DACVALUE0NB	=	0x5331
                           005330  1934 _AX5043_DACVALUE1NB	=	0x5330
                           005102  1935 _AX5043_DECIMATIONNB	=	0x5102
                           005042  1936 _AX5043_DIVERSITYNB	=	0x5042
                           005011  1937 _AX5043_ENCODINGNB	=	0x5011
                           005018  1938 _AX5043_FECNB	=	0x5018
                           00501A  1939 _AX5043_FECSTATUSNB	=	0x501a
                           005019  1940 _AX5043_FECSYNCNB	=	0x5019
                           00502B  1941 _AX5043_FIFOCOUNT0NB	=	0x502b
                           00502A  1942 _AX5043_FIFOCOUNT1NB	=	0x502a
                           005029  1943 _AX5043_FIFODATANB	=	0x5029
                           00502D  1944 _AX5043_FIFOFREE0NB	=	0x502d
                           00502C  1945 _AX5043_FIFOFREE1NB	=	0x502c
                           005028  1946 _AX5043_FIFOSTATNB	=	0x5028
                           00502F  1947 _AX5043_FIFOTHRESH0NB	=	0x502f
                           00502E  1948 _AX5043_FIFOTHRESH1NB	=	0x502e
                           005012  1949 _AX5043_FRAMINGNB	=	0x5012
                           005037  1950 _AX5043_FREQA0NB	=	0x5037
                           005036  1951 _AX5043_FREQA1NB	=	0x5036
                           005035  1952 _AX5043_FREQA2NB	=	0x5035
                           005034  1953 _AX5043_FREQA3NB	=	0x5034
                           00503F  1954 _AX5043_FREQB0NB	=	0x503f
                           00503E  1955 _AX5043_FREQB1NB	=	0x503e
                           00503D  1956 _AX5043_FREQB2NB	=	0x503d
                           00503C  1957 _AX5043_FREQB3NB	=	0x503c
                           005163  1958 _AX5043_FSKDEV0NB	=	0x5163
                           005162  1959 _AX5043_FSKDEV1NB	=	0x5162
                           005161  1960 _AX5043_FSKDEV2NB	=	0x5161
                           00510D  1961 _AX5043_FSKDMAX0NB	=	0x510d
                           00510C  1962 _AX5043_FSKDMAX1NB	=	0x510c
                           00510F  1963 _AX5043_FSKDMIN0NB	=	0x510f
                           00510E  1964 _AX5043_FSKDMIN1NB	=	0x510e
                           005309  1965 _AX5043_GPADC13VALUE0NB	=	0x5309
                           005308  1966 _AX5043_GPADC13VALUE1NB	=	0x5308
                           005300  1967 _AX5043_GPADCCTRLNB	=	0x5300
                           005301  1968 _AX5043_GPADCPERIODNB	=	0x5301
                           005101  1969 _AX5043_IFFREQ0NB	=	0x5101
                           005100  1970 _AX5043_IFFREQ1NB	=	0x5100
                           00500B  1971 _AX5043_IRQINVERSION0NB	=	0x500b
                           00500A  1972 _AX5043_IRQINVERSION1NB	=	0x500a
                           005007  1973 _AX5043_IRQMASK0NB	=	0x5007
                           005006  1974 _AX5043_IRQMASK1NB	=	0x5006
                           00500D  1975 _AX5043_IRQREQUEST0NB	=	0x500d
                           00500C  1976 _AX5043_IRQREQUEST1NB	=	0x500c
                           005310  1977 _AX5043_LPOSCCONFIGNB	=	0x5310
                           005317  1978 _AX5043_LPOSCFREQ0NB	=	0x5317
                           005316  1979 _AX5043_LPOSCFREQ1NB	=	0x5316
                           005313  1980 _AX5043_LPOSCKFILT0NB	=	0x5313
                           005312  1981 _AX5043_LPOSCKFILT1NB	=	0x5312
                           005319  1982 _AX5043_LPOSCPER0NB	=	0x5319
                           005318  1983 _AX5043_LPOSCPER1NB	=	0x5318
                           005315  1984 _AX5043_LPOSCREF0NB	=	0x5315
                           005314  1985 _AX5043_LPOSCREF1NB	=	0x5314
                           005311  1986 _AX5043_LPOSCSTATUSNB	=	0x5311
                           005214  1987 _AX5043_MATCH0LENNB	=	0x5214
                           005216  1988 _AX5043_MATCH0MAXNB	=	0x5216
                           005215  1989 _AX5043_MATCH0MINNB	=	0x5215
                           005213  1990 _AX5043_MATCH0PAT0NB	=	0x5213
                           005212  1991 _AX5043_MATCH0PAT1NB	=	0x5212
                           005211  1992 _AX5043_MATCH0PAT2NB	=	0x5211
                           005210  1993 _AX5043_MATCH0PAT3NB	=	0x5210
                           00521C  1994 _AX5043_MATCH1LENNB	=	0x521c
                           00521E  1995 _AX5043_MATCH1MAXNB	=	0x521e
                           00521D  1996 _AX5043_MATCH1MINNB	=	0x521d
                           005219  1997 _AX5043_MATCH1PAT0NB	=	0x5219
                           005218  1998 _AX5043_MATCH1PAT1NB	=	0x5218
                           005108  1999 _AX5043_MAXDROFFSET0NB	=	0x5108
                           005107  2000 _AX5043_MAXDROFFSET1NB	=	0x5107
                           005106  2001 _AX5043_MAXDROFFSET2NB	=	0x5106
                           00510B  2002 _AX5043_MAXRFOFFSET0NB	=	0x510b
                           00510A  2003 _AX5043_MAXRFOFFSET1NB	=	0x510a
                           005109  2004 _AX5043_MAXRFOFFSET2NB	=	0x5109
                           005164  2005 _AX5043_MODCFGANB	=	0x5164
                           005160  2006 _AX5043_MODCFGFNB	=	0x5160
                           005F5F  2007 _AX5043_MODCFGPNB	=	0x5f5f
                           005010  2008 _AX5043_MODULATIONNB	=	0x5010
                           005025  2009 _AX5043_PINFUNCANTSELNB	=	0x5025
                           005023  2010 _AX5043_PINFUNCDATANB	=	0x5023
                           005022  2011 _AX5043_PINFUNCDCLKNB	=	0x5022
                           005024  2012 _AX5043_PINFUNCIRQNB	=	0x5024
                           005026  2013 _AX5043_PINFUNCPWRAMPNB	=	0x5026
                           005021  2014 _AX5043_PINFUNCSYSCLKNB	=	0x5021
                           005020  2015 _AX5043_PINSTATENB	=	0x5020
                           005233  2016 _AX5043_PKTACCEPTFLAGSNB	=	0x5233
                           005230  2017 _AX5043_PKTCHUNKSIZENB	=	0x5230
                           005231  2018 _AX5043_PKTMISCFLAGSNB	=	0x5231
                           005232  2019 _AX5043_PKTSTOREFLAGSNB	=	0x5232
                           005031  2020 _AX5043_PLLCPINB	=	0x5031
                           005039  2021 _AX5043_PLLCPIBOOSTNB	=	0x5039
                           005182  2022 _AX5043_PLLLOCKDETNB	=	0x5182
                           005030  2023 _AX5043_PLLLOOPNB	=	0x5030
                           005038  2024 _AX5043_PLLLOOPBOOSTNB	=	0x5038
                           005033  2025 _AX5043_PLLRANGINGANB	=	0x5033
                           00503B  2026 _AX5043_PLLRANGINGBNB	=	0x503b
                           005183  2027 _AX5043_PLLRNGCLKNB	=	0x5183
                           005032  2028 _AX5043_PLLVCODIVNB	=	0x5032
                           005180  2029 _AX5043_PLLVCOINB	=	0x5180
                           005181  2030 _AX5043_PLLVCOIRNB	=	0x5181
                           005F08  2031 _AX5043_POWCTRL1NB	=	0x5f08
                           005005  2032 _AX5043_POWIRQMASKNB	=	0x5005
                           005003  2033 _AX5043_POWSTATNB	=	0x5003
                           005004  2034 _AX5043_POWSTICKYSTATNB	=	0x5004
                           005027  2035 _AX5043_PWRAMPNB	=	0x5027
                           005002  2036 _AX5043_PWRMODENB	=	0x5002
                           005009  2037 _AX5043_RADIOEVENTMASK0NB	=	0x5009
                           005008  2038 _AX5043_RADIOEVENTMASK1NB	=	0x5008
                           00500F  2039 _AX5043_RADIOEVENTREQ0NB	=	0x500f
                           00500E  2040 _AX5043_RADIOEVENTREQ1NB	=	0x500e
                           00501C  2041 _AX5043_RADIOSTATENB	=	0x501c
                           005F0D  2042 _AX5043_REFNB	=	0x5f0d
                           005040  2043 _AX5043_RSSINB	=	0x5040
                           00522D  2044 _AX5043_RSSIABSTHRNB	=	0x522d
                           00522C  2045 _AX5043_RSSIREFERENCENB	=	0x522c
                           005105  2046 _AX5043_RXDATARATE0NB	=	0x5105
                           005104  2047 _AX5043_RXDATARATE1NB	=	0x5104
                           005103  2048 _AX5043_RXDATARATE2NB	=	0x5103
                           005001  2049 _AX5043_SCRATCHNB	=	0x5001
                           005000  2050 _AX5043_SILICONREVISIONNB	=	0x5000
                           00505B  2051 _AX5043_TIMER0NB	=	0x505b
                           00505A  2052 _AX5043_TIMER1NB	=	0x505a
                           005059  2053 _AX5043_TIMER2NB	=	0x5059
                           005227  2054 _AX5043_TMGRXAGCNB	=	0x5227
                           005223  2055 _AX5043_TMGRXBOOSTNB	=	0x5223
                           005226  2056 _AX5043_TMGRXCOARSEAGCNB	=	0x5226
                           005225  2057 _AX5043_TMGRXOFFSACQNB	=	0x5225
                           005229  2058 _AX5043_TMGRXPREAMBLE1NB	=	0x5229
                           00522A  2059 _AX5043_TMGRXPREAMBLE2NB	=	0x522a
                           00522B  2060 _AX5043_TMGRXPREAMBLE3NB	=	0x522b
                           005228  2061 _AX5043_TMGRXRSSINB	=	0x5228
                           005224  2062 _AX5043_TMGRXSETTLENB	=	0x5224
                           005220  2063 _AX5043_TMGTXBOOSTNB	=	0x5220
                           005221  2064 _AX5043_TMGTXSETTLENB	=	0x5221
                           005055  2065 _AX5043_TRKAFSKDEMOD0NB	=	0x5055
                           005054  2066 _AX5043_TRKAFSKDEMOD1NB	=	0x5054
                           005049  2067 _AX5043_TRKAMPLITUDE0NB	=	0x5049
                           005048  2068 _AX5043_TRKAMPLITUDE1NB	=	0x5048
                           005047  2069 _AX5043_TRKDATARATE0NB	=	0x5047
                           005046  2070 _AX5043_TRKDATARATE1NB	=	0x5046
                           005045  2071 _AX5043_TRKDATARATE2NB	=	0x5045
                           005051  2072 _AX5043_TRKFREQ0NB	=	0x5051
                           005050  2073 _AX5043_TRKFREQ1NB	=	0x5050
                           005053  2074 _AX5043_TRKFSKDEMOD0NB	=	0x5053
                           005052  2075 _AX5043_TRKFSKDEMOD1NB	=	0x5052
                           00504B  2076 _AX5043_TRKPHASE0NB	=	0x504b
                           00504A  2077 _AX5043_TRKPHASE1NB	=	0x504a
                           00504F  2078 _AX5043_TRKRFFREQ0NB	=	0x504f
                           00504E  2079 _AX5043_TRKRFFREQ1NB	=	0x504e
                           00504D  2080 _AX5043_TRKRFFREQ2NB	=	0x504d
                           005169  2081 _AX5043_TXPWRCOEFFA0NB	=	0x5169
                           005168  2082 _AX5043_TXPWRCOEFFA1NB	=	0x5168
                           00516B  2083 _AX5043_TXPWRCOEFFB0NB	=	0x516b
                           00516A  2084 _AX5043_TXPWRCOEFFB1NB	=	0x516a
                           00516D  2085 _AX5043_TXPWRCOEFFC0NB	=	0x516d
                           00516C  2086 _AX5043_TXPWRCOEFFC1NB	=	0x516c
                           00516F  2087 _AX5043_TXPWRCOEFFD0NB	=	0x516f
                           00516E  2088 _AX5043_TXPWRCOEFFD1NB	=	0x516e
                           005171  2089 _AX5043_TXPWRCOEFFE0NB	=	0x5171
                           005170  2090 _AX5043_TXPWRCOEFFE1NB	=	0x5170
                           005167  2091 _AX5043_TXRATE0NB	=	0x5167
                           005166  2092 _AX5043_TXRATE1NB	=	0x5166
                           005165  2093 _AX5043_TXRATE2NB	=	0x5165
                           00506B  2094 _AX5043_WAKEUP0NB	=	0x506b
                           00506A  2095 _AX5043_WAKEUP1NB	=	0x506a
                           00506D  2096 _AX5043_WAKEUPFREQ0NB	=	0x506d
                           00506C  2097 _AX5043_WAKEUPFREQ1NB	=	0x506c
                           005069  2098 _AX5043_WAKEUPTIMER0NB	=	0x5069
                           005068  2099 _AX5043_WAKEUPTIMER1NB	=	0x5068
                           00506E  2100 _AX5043_WAKEUPXOEARLYNB	=	0x506e
                           005F11  2101 _AX5043_XTALAMPLNB	=	0x5f11
                           005184  2102 _AX5043_XTALCAPNB	=	0x5184
                           005F10  2103 _AX5043_XTALOSCNB	=	0x5f10
                           00501D  2104 _AX5043_XTALSTATUSNB	=	0x501d
                           005F00  2105 _AX5043_0xF00NB	=	0x5f00
                           005F0C  2106 _AX5043_0xF0CNB	=	0x5f0c
                           005F18  2107 _AX5043_0xF18NB	=	0x5f18
                           005F1C  2108 _AX5043_0xF1CNB	=	0x5f1c
                           005F21  2109 _AX5043_0xF21NB	=	0x5f21
                           005F22  2110 _AX5043_0xF22NB	=	0x5f22
                           005F23  2111 _AX5043_0xF23NB	=	0x5f23
                           005F26  2112 _AX5043_0xF26NB	=	0x5f26
                           005F30  2113 _AX5043_0xF30NB	=	0x5f30
                           005F31  2114 _AX5043_0xF31NB	=	0x5f31
                           005F32  2115 _AX5043_0xF32NB	=	0x5f32
                           005F33  2116 _AX5043_0xF33NB	=	0x5f33
                           005F34  2117 _AX5043_0xF34NB	=	0x5f34
                           005F35  2118 _AX5043_0xF35NB	=	0x5f35
                           005F44  2119 _AX5043_0xF44NB	=	0x5f44
                           005122  2120 _AX5043_AGCAHYST0NB	=	0x5122
                           005132  2121 _AX5043_AGCAHYST1NB	=	0x5132
                           005142  2122 _AX5043_AGCAHYST2NB	=	0x5142
                           005152  2123 _AX5043_AGCAHYST3NB	=	0x5152
                           005120  2124 _AX5043_AGCGAIN0NB	=	0x5120
                           005130  2125 _AX5043_AGCGAIN1NB	=	0x5130
                           005140  2126 _AX5043_AGCGAIN2NB	=	0x5140
                           005150  2127 _AX5043_AGCGAIN3NB	=	0x5150
                           005123  2128 _AX5043_AGCMINMAX0NB	=	0x5123
                           005133  2129 _AX5043_AGCMINMAX1NB	=	0x5133
                           005143  2130 _AX5043_AGCMINMAX2NB	=	0x5143
                           005153  2131 _AX5043_AGCMINMAX3NB	=	0x5153
                           005121  2132 _AX5043_AGCTARGET0NB	=	0x5121
                           005131  2133 _AX5043_AGCTARGET1NB	=	0x5131
                           005141  2134 _AX5043_AGCTARGET2NB	=	0x5141
                           005151  2135 _AX5043_AGCTARGET3NB	=	0x5151
                           00512B  2136 _AX5043_AMPLITUDEGAIN0NB	=	0x512b
                           00513B  2137 _AX5043_AMPLITUDEGAIN1NB	=	0x513b
                           00514B  2138 _AX5043_AMPLITUDEGAIN2NB	=	0x514b
                           00515B  2139 _AX5043_AMPLITUDEGAIN3NB	=	0x515b
                           00512F  2140 _AX5043_BBOFFSRES0NB	=	0x512f
                           00513F  2141 _AX5043_BBOFFSRES1NB	=	0x513f
                           00514F  2142 _AX5043_BBOFFSRES2NB	=	0x514f
                           00515F  2143 _AX5043_BBOFFSRES3NB	=	0x515f
                           005125  2144 _AX5043_DRGAIN0NB	=	0x5125
                           005135  2145 _AX5043_DRGAIN1NB	=	0x5135
                           005145  2146 _AX5043_DRGAIN2NB	=	0x5145
                           005155  2147 _AX5043_DRGAIN3NB	=	0x5155
                           00512E  2148 _AX5043_FOURFSK0NB	=	0x512e
                           00513E  2149 _AX5043_FOURFSK1NB	=	0x513e
                           00514E  2150 _AX5043_FOURFSK2NB	=	0x514e
                           00515E  2151 _AX5043_FOURFSK3NB	=	0x515e
                           00512D  2152 _AX5043_FREQDEV00NB	=	0x512d
                           00513D  2153 _AX5043_FREQDEV01NB	=	0x513d
                           00514D  2154 _AX5043_FREQDEV02NB	=	0x514d
                           00515D  2155 _AX5043_FREQDEV03NB	=	0x515d
                           00512C  2156 _AX5043_FREQDEV10NB	=	0x512c
                           00513C  2157 _AX5043_FREQDEV11NB	=	0x513c
                           00514C  2158 _AX5043_FREQDEV12NB	=	0x514c
                           00515C  2159 _AX5043_FREQDEV13NB	=	0x515c
                           005127  2160 _AX5043_FREQUENCYGAINA0NB	=	0x5127
                           005137  2161 _AX5043_FREQUENCYGAINA1NB	=	0x5137
                           005147  2162 _AX5043_FREQUENCYGAINA2NB	=	0x5147
                           005157  2163 _AX5043_FREQUENCYGAINA3NB	=	0x5157
                           005128  2164 _AX5043_FREQUENCYGAINB0NB	=	0x5128
                           005138  2165 _AX5043_FREQUENCYGAINB1NB	=	0x5138
                           005148  2166 _AX5043_FREQUENCYGAINB2NB	=	0x5148
                           005158  2167 _AX5043_FREQUENCYGAINB3NB	=	0x5158
                           005129  2168 _AX5043_FREQUENCYGAINC0NB	=	0x5129
                           005139  2169 _AX5043_FREQUENCYGAINC1NB	=	0x5139
                           005149  2170 _AX5043_FREQUENCYGAINC2NB	=	0x5149
                           005159  2171 _AX5043_FREQUENCYGAINC3NB	=	0x5159
                           00512A  2172 _AX5043_FREQUENCYGAIND0NB	=	0x512a
                           00513A  2173 _AX5043_FREQUENCYGAIND1NB	=	0x513a
                           00514A  2174 _AX5043_FREQUENCYGAIND2NB	=	0x514a
                           00515A  2175 _AX5043_FREQUENCYGAIND3NB	=	0x515a
                           005116  2176 _AX5043_FREQUENCYLEAKNB	=	0x5116
                           005126  2177 _AX5043_PHASEGAIN0NB	=	0x5126
                           005136  2178 _AX5043_PHASEGAIN1NB	=	0x5136
                           005146  2179 _AX5043_PHASEGAIN2NB	=	0x5146
                           005156  2180 _AX5043_PHASEGAIN3NB	=	0x5156
                           005207  2181 _AX5043_PKTADDR0NB	=	0x5207
                           005206  2182 _AX5043_PKTADDR1NB	=	0x5206
                           005205  2183 _AX5043_PKTADDR2NB	=	0x5205
                           005204  2184 _AX5043_PKTADDR3NB	=	0x5204
                           005200  2185 _AX5043_PKTADDRCFGNB	=	0x5200
                           00520B  2186 _AX5043_PKTADDRMASK0NB	=	0x520b
                           00520A  2187 _AX5043_PKTADDRMASK1NB	=	0x520a
                           005209  2188 _AX5043_PKTADDRMASK2NB	=	0x5209
                           005208  2189 _AX5043_PKTADDRMASK3NB	=	0x5208
                           005201  2190 _AX5043_PKTLENCFGNB	=	0x5201
                           005202  2191 _AX5043_PKTLENOFFSETNB	=	0x5202
                           005203  2192 _AX5043_PKTMAXLENNB	=	0x5203
                           005118  2193 _AX5043_RXPARAMCURSETNB	=	0x5118
                           005117  2194 _AX5043_RXPARAMSETSNB	=	0x5117
                           005124  2195 _AX5043_TIMEGAIN0NB	=	0x5124
                           005134  2196 _AX5043_TIMEGAIN1NB	=	0x5134
                           005144  2197 _AX5043_TIMEGAIN2NB	=	0x5144
                           005154  2198 _AX5043_TIMEGAIN3NB	=	0x5154
                           00FC06  2199 _flash_deviceid	=	0xfc06
      00000B                       2200 _aligned_alloc_PARM_2:
      00000B                       2201 	.ds 2
                           004F00  2202 _AX5043_0xF00	=	0x4f00
                           004F0C  2203 _AX5043_0xF0C	=	0x4f0c
                           004F10  2204 _AX5043_0xF10	=	0x4f10
                           004F11  2205 _AX5043_0xF11	=	0x4f11
                           004F18  2206 _AX5043_0xF18	=	0x4f18
                           004F1C  2207 _AX5043_0xF1C	=	0x4f1c
                           004F21  2208 _AX5043_0xF21	=	0x4f21
                           004F22  2209 _AX5043_0xF22	=	0x4f22
                           004F23  2210 _AX5043_0xF23	=	0x4f23
                           004F26  2211 _AX5043_0xF26	=	0x4f26
                           004F30  2212 _AX5043_0xF30	=	0x4f30
                           004F31  2213 _AX5043_0xF31	=	0x4f31
                           004F32  2214 _AX5043_0xF32	=	0x4f32
                           004F33  2215 _AX5043_0xF33	=	0x4f33
                           004F34  2216 _AX5043_0xF34	=	0x4f34
                           004F35  2217 _AX5043_0xF35	=	0x4f35
                           004F44  2218 _AX5043_0xF44	=	0x4f44
                           004F0D  2219 _AX5043_REF	=	0x4f0d
                           004F08  2220 _AX5043_POWCTRL1	=	0x4f08
                           004F5F  2221 _AX5043_MODCFGP	=	0x4f5f
                           004F10  2222 _AX5043_XTALOSC	=	0x4f10
                           004F11  2223 _AX5043_XTALAMPL	=	0x4f11
                           00FC00  2224 _flash_calsector	=	0xfc00
      00000D                       2225 _RxIsrFlag::
      00000D                       2226 	.ds 2
      00000F                       2227 _BeaconRx::
      00000F                       2228 	.ds 29
      00002C                       2229 _RxBuffer::
      00002C                       2230 	.ds 2
      00002E                       2231 _i::
      00002E                       2232 	.ds 1
      00002F                       2233 _pwrmgmt_irq_pc_1_350:
      00002F                       2234 	.ds 1
      000030                       2235 _transmit_packet_i_1_352:
      000030                       2236 	.ds 1
      000031                       2237 _transmit_packet_demo_packet__1_352:
      000031                       2238 	.ds 23
      000048                       2239 _axradio_statuschange_st_1_354:
      000048                       2240 	.ds 2
      00004A                       2241 _main_PROCD_PayloadLength_1_382:
      00004A                       2242 	.ds 1
      00004B                       2243 _main_PROCD_UartRxBuffer_1_382:
      00004B                       2244 	.ds 2
      00004D                       2245 _main_PROCD_Command_1_382:
      00004D                       2246 	.ds 1
      00004E                       2247 _main_UBLOX_setNav1_1_382:
      00004E                       2248 	.ds 3
      000051                       2249 _main_UBLOX_setNav2_1_382:
      000051                       2250 	.ds 8
      000059                       2251 _main_UBLOX_setNav3_1_382:
      000059                       2252 	.ds 8
      000061                       2253 _main_UBLOX_setNav5_1_382:
      000061                       2254 	.ds 36
      000085                       2255 _main_UBLOX_Rx_Length_1_382:
      000085                       2256 	.ds 2
                                   2257 ;--------------------------------------------------------
                                   2258 ; absolute external ram data
                                   2259 ;--------------------------------------------------------
                                   2260 	.area XABS    (ABS,XDATA)
                                   2261 ;--------------------------------------------------------
                                   2262 ; external initialized ram data
                                   2263 ;--------------------------------------------------------
                                   2264 	.area XISEG   (XDATA)
      00098F                       2265 _RfStateMachine::
      00098F                       2266 	.ds 1
      000990                       2267 _Tmr0Flg::
      000990                       2268 	.ds 1
      000991                       2269 _counter::
      000991                       2270 	.ds 1
                                   2271 	.area HOME    (CODE)
                                   2272 	.area GSINIT0 (CODE)
                                   2273 	.area GSINIT1 (CODE)
                                   2274 	.area GSINIT2 (CODE)
                                   2275 	.area GSINIT3 (CODE)
                                   2276 	.area GSINIT4 (CODE)
                                   2277 	.area GSINIT5 (CODE)
                                   2278 	.area GSINIT  (CODE)
                                   2279 	.area GSFINAL (CODE)
                                   2280 	.area CSEG    (CODE)
                                   2281 ;--------------------------------------------------------
                                   2282 ; interrupt vector 
                                   2283 ;--------------------------------------------------------
                                   2284 	.area HOME    (CODE)
      000000                       2285 __interrupt_vect:
      000000 02 03 11         [24] 2286 	ljmp	__sdcc_gsinit_startup
      000003 32               [24] 2287 	reti
      000004                       2288 	.ds	7
      00000B 02 00 E8         [24] 2289 	ljmp	_wtimer_irq
      00000E                       2290 	.ds	5
      000013 32               [24] 2291 	reti
      000014                       2292 	.ds	7
      00001B 32               [24] 2293 	reti
      00001C                       2294 	.ds	7
      000023 02 1D 7C         [24] 2295 	ljmp	_axradio_isr
      000026                       2296 	.ds	5
      00002B 32               [24] 2297 	reti
      00002C                       2298 	.ds	7
      000033 02 0B 5C         [24] 2299 	ljmp	_pwrmgmt_irq
      000036                       2300 	.ds	5
      00003B 32               [24] 2301 	reti
      00003C                       2302 	.ds	7
      000043 32               [24] 2303 	reti
      000044                       2304 	.ds	7
      00004B 32               [24] 2305 	reti
      00004C                       2306 	.ds	7
      000053 32               [24] 2307 	reti
      000054                       2308 	.ds	7
      00005B 02 02 DA         [24] 2309 	ljmp	_uart0_irq
      00005E                       2310 	.ds	5
      000063 02 00 B1         [24] 2311 	ljmp	_uart1_irq
      000066                       2312 	.ds	5
      00006B 32               [24] 2313 	reti
      00006C                       2314 	.ds	7
      000073 32               [24] 2315 	reti
      000074                       2316 	.ds	7
      00007B 32               [24] 2317 	reti
      00007C                       2318 	.ds	7
      000083 32               [24] 2319 	reti
      000084                       2320 	.ds	7
      00008B 32               [24] 2321 	reti
      00008C                       2322 	.ds	7
      000093 32               [24] 2323 	reti
      000094                       2324 	.ds	7
      00009B 32               [24] 2325 	reti
      00009C                       2326 	.ds	7
      0000A3 32               [24] 2327 	reti
      0000A4                       2328 	.ds	7
      0000AB 02 02 A3         [24] 2329 	ljmp	_dbglink_irq
                                   2330 ;--------------------------------------------------------
                                   2331 ; global & static initialisations
                                   2332 ;--------------------------------------------------------
                                   2333 	.area HOME    (CODE)
                                   2334 	.area GSINIT  (CODE)
                                   2335 	.area GSFINAL (CODE)
                                   2336 	.area GSINIT  (CODE)
                                   2337 	.globl __sdcc_gsinit_startup
                                   2338 	.globl __sdcc_program_startup
                                   2339 	.globl __start__stack
                                   2340 	.globl __mcs51_genXINIT
                                   2341 	.globl __mcs51_genXRAMCLEAR
                                   2342 	.globl __mcs51_genRAMCLEAR
                                   2343 ;------------------------------------------------------------
                                   2344 ;Allocation info for local variables in function 'transmit_packet'
                                   2345 ;------------------------------------------------------------
                                   2346 ;i                         Allocated with name '_transmit_packet_i_1_352'
                                   2347 ;demo_packet_              Allocated with name '_transmit_packet_demo_packet__1_352'
                                   2348 ;------------------------------------------------------------
                                   2349 ;	..\src\atrs\main.c:70: static uint8_t i = 0;
      000384 90 00 30         [24] 2350 	mov	dptr,#_transmit_packet_i_1_352
      000387 E4               [12] 2351 	clr	a
      000388 F0               [24] 2352 	movx	@dptr,a
                                   2353 ;	..\src\atrs\main.c:52: uint16_t __data pkt_counter = 0;/**< \brief amount of packets received */
      000389 E4               [12] 2354 	clr	a
      00038A F5 08            [12] 2355 	mov	_pkt_counter,a
      00038C F5 09            [12] 2356 	mov	(_pkt_counter + 1),a
                                   2357 ;	..\src\atrs\main.c:53: uint8_t __data coldstart = 1; /**< \brief Cold start indicator*/ // caution: initialization with 1 is necessary! Variables are initialized upon _sdcc_external_startup returning 0 -> the coldstart value returned from _sdcc_external startup does not survive in the coldstart case
      00038E 75 0A 01         [24] 2358 	mov	_coldstart,#0x01
                                   2359 	.area GSFINAL (CODE)
      000397 02 00 AE         [24] 2360 	ljmp	__sdcc_program_startup
                                   2361 ;--------------------------------------------------------
                                   2362 ; Home
                                   2363 ;--------------------------------------------------------
                                   2364 	.area HOME    (CODE)
                                   2365 	.area HOME    (CODE)
      0000AE                       2366 __sdcc_program_startup:
      0000AE 02 0C E1         [24] 2367 	ljmp	_main
                                   2368 ;	return from main will return to caller
                                   2369 ;--------------------------------------------------------
                                   2370 ; code
                                   2371 ;--------------------------------------------------------
                                   2372 	.area CSEG    (CODE)
                                   2373 ;------------------------------------------------------------
                                   2374 ;Allocation info for local variables in function 'pwrmgmt_irq'
                                   2375 ;------------------------------------------------------------
                                   2376 ;pc                        Allocated with name '_pwrmgmt_irq_pc_1_350'
                                   2377 ;------------------------------------------------------------
                                   2378 ;	..\src\atrs\main.c:57: static void pwrmgmt_irq(void) __interrupt(INT_POWERMGMT)
                                   2379 ;	-----------------------------------------
                                   2380 ;	 function pwrmgmt_irq
                                   2381 ;	-----------------------------------------
      000B5C                       2382 _pwrmgmt_irq:
                           000007  2383 	ar7 = 0x07
                           000006  2384 	ar6 = 0x06
                           000005  2385 	ar5 = 0x05
                           000004  2386 	ar4 = 0x04
                           000003  2387 	ar3 = 0x03
                           000002  2388 	ar2 = 0x02
                           000001  2389 	ar1 = 0x01
                           000000  2390 	ar0 = 0x00
      000B5C C0 E0            [24] 2391 	push	acc
      000B5E C0 82            [24] 2392 	push	dpl
      000B60 C0 83            [24] 2393 	push	dph
      000B62 C0 07            [24] 2394 	push	ar7
      000B64 C0 D0            [24] 2395 	push	psw
      000B66 75 D0 00         [24] 2396 	mov	psw,#0x00
                                   2397 ;	..\src\atrs\main.c:59: uint8_t pc = PCON;
      000B69 90 00 2F         [24] 2398 	mov	dptr,#_pwrmgmt_irq_pc_1_350
      000B6C E5 87            [12] 2399 	mov	a,_PCON
      000B6E F0               [24] 2400 	movx	@dptr,a
                                   2401 ;	..\src\atrs\main.c:60: if (!(pc & 0x80))
      000B6F E0               [24] 2402 	movx	a,@dptr
      000B70 FF               [12] 2403 	mov	r7,a
      000B71 20 E7 02         [24] 2404 	jb	acc.7,00102$
                                   2405 ;	..\src\atrs\main.c:61: return;
      000B74 80 10            [24] 2406 	sjmp	00106$
      000B76                       2407 00102$:
                                   2408 ;	..\src\atrs\main.c:62: GPIOENABLE = 0;
      000B76 90 70 0C         [24] 2409 	mov	dptr,#_GPIOENABLE
      000B79 E4               [12] 2410 	clr	a
      000B7A F0               [24] 2411 	movx	@dptr,a
                                   2412 ;	..\src\atrs\main.c:63: IE = EIE = E2IE = 0;
                                   2413 ;	1-genFromRTrack replaced	mov	_E2IE,#0x00
      000B7B F5 A0            [12] 2414 	mov	_E2IE,a
                                   2415 ;	1-genFromRTrack replaced	mov	_EIE,#0x00
      000B7D F5 98            [12] 2416 	mov	_EIE,a
                                   2417 ;	1-genFromRTrack replaced	mov	_IE,#0x00
      000B7F F5 A8            [12] 2418 	mov	_IE,a
      000B81                       2419 00104$:
                                   2420 ;	..\src\atrs\main.c:65: PCON |= 0x01;
      000B81 43 87 01         [24] 2421 	orl	_PCON,#0x01
      000B84 80 FB            [24] 2422 	sjmp	00104$
      000B86                       2423 00106$:
      000B86 D0 D0            [24] 2424 	pop	psw
      000B88 D0 07            [24] 2425 	pop	ar7
      000B8A D0 83            [24] 2426 	pop	dph
      000B8C D0 82            [24] 2427 	pop	dpl
      000B8E D0 E0            [24] 2428 	pop	acc
      000B90 32               [24] 2429 	reti
                                   2430 ;	eliminated unneeded push/pop b
                                   2431 ;------------------------------------------------------------
                                   2432 ;Allocation info for local variables in function 'transmit_packet'
                                   2433 ;------------------------------------------------------------
                                   2434 ;i                         Allocated with name '_transmit_packet_i_1_352'
                                   2435 ;demo_packet_              Allocated with name '_transmit_packet_demo_packet__1_352'
                                   2436 ;------------------------------------------------------------
                                   2437 ;	..\src\atrs\main.c:68: void transmit_packet(void)
                                   2438 ;	-----------------------------------------
                                   2439 ;	 function transmit_packet
                                   2440 ;	-----------------------------------------
      000B91                       2441 _transmit_packet:
                                   2442 ;	..\src\atrs\main.c:72: axradio_set_mode(AXRADIO_MODE_ASYNC_TRANSMIT);
      000B91 75 82 10         [24] 2443 	mov	dpl,#0x10
      000B94 12 3B 2A         [24] 2444 	lcall	_axradio_set_mode
                                   2445 ;	..\src\atrs\main.c:73: ++pkt_counter;
      000B97 05 08            [12] 2446 	inc	_pkt_counter
      000B99 E4               [12] 2447 	clr	a
      000B9A B5 08 02         [24] 2448 	cjne	a,_pkt_counter,00108$
      000B9D 05 09            [12] 2449 	inc	(_pkt_counter + 1)
      000B9F                       2450 00108$:
                                   2451 ;	..\src\atrs\main.c:74: memcpy(demo_packet_, demo_packet, sizeof(demo_packet));
      000B9F 90 03 E2         [24] 2452 	mov	dptr,#_memcpy_PARM_2
      000BA2 74 92            [12] 2453 	mov	a,#_demo_packet
      000BA4 F0               [24] 2454 	movx	@dptr,a
      000BA5 74 09            [12] 2455 	mov	a,#(_demo_packet >> 8)
      000BA7 A3               [24] 2456 	inc	dptr
      000BA8 F0               [24] 2457 	movx	@dptr,a
      000BA9 E4               [12] 2458 	clr	a
      000BAA A3               [24] 2459 	inc	dptr
      000BAB F0               [24] 2460 	movx	@dptr,a
      000BAC 90 03 E5         [24] 2461 	mov	dptr,#_memcpy_PARM_3
      000BAF 74 17            [12] 2462 	mov	a,#0x17
      000BB1 F0               [24] 2463 	movx	@dptr,a
      000BB2 E4               [12] 2464 	clr	a
      000BB3 A3               [24] 2465 	inc	dptr
      000BB4 F0               [24] 2466 	movx	@dptr,a
      000BB5 90 00 31         [24] 2467 	mov	dptr,#_transmit_packet_demo_packet__1_352
      000BB8 75 F0 00         [24] 2468 	mov	b,#0x00
      000BBB 12 65 97         [24] 2469 	lcall	_memcpy
                                   2470 ;	..\src\atrs\main.c:75: if (framing_insert_counter) {
      000BBE 90 7B 99         [24] 2471 	mov	dptr,#_framing_insert_counter
      000BC1 E4               [12] 2472 	clr	a
      000BC2 93               [24] 2473 	movc	a,@a+dptr
      000BC3 60 24            [24] 2474 	jz	00102$
                                   2475 ;	..\src\atrs\main.c:76: demo_packet_[framing_counter_pos] = pkt_counter & 0xFF ;
      000BC5 90 7B 9A         [24] 2476 	mov	dptr,#_framing_counter_pos
      000BC8 E4               [12] 2477 	clr	a
      000BC9 93               [24] 2478 	movc	a,@a+dptr
      000BCA FF               [12] 2479 	mov	r7,a
      000BCB 24 31            [12] 2480 	add	a,#_transmit_packet_demo_packet__1_352
      000BCD F5 82            [12] 2481 	mov	dpl,a
      000BCF E4               [12] 2482 	clr	a
      000BD0 34 00            [12] 2483 	addc	a,#(_transmit_packet_demo_packet__1_352 >> 8)
      000BD2 F5 83            [12] 2484 	mov	dph,a
      000BD4 AD 08            [24] 2485 	mov	r5,_pkt_counter
      000BD6 7E 00            [12] 2486 	mov	r6,#0x00
      000BD8 ED               [12] 2487 	mov	a,r5
      000BD9 F0               [24] 2488 	movx	@dptr,a
                                   2489 ;	..\src\atrs\main.c:77: demo_packet_[framing_counter_pos+1] = (pkt_counter>>8) & 0xFF;
      000BDA EF               [12] 2490 	mov	a,r7
      000BDB 04               [12] 2491 	inc	a
      000BDC 24 31            [12] 2492 	add	a,#_transmit_packet_demo_packet__1_352
      000BDE F5 82            [12] 2493 	mov	dpl,a
      000BE0 E4               [12] 2494 	clr	a
      000BE1 34 00            [12] 2495 	addc	a,#(_transmit_packet_demo_packet__1_352 >> 8)
      000BE3 F5 83            [12] 2496 	mov	dph,a
      000BE5 E5 09            [12] 2497 	mov	a,(_pkt_counter + 1)
      000BE7 FF               [12] 2498 	mov	r7,a
      000BE8 F0               [24] 2499 	movx	@dptr,a
      000BE9                       2500 00102$:
                                   2501 ;	..\src\atrs\main.c:79: axradio_transmit(&remoteaddr, demo_packet_, sizeof(demo_packet));
      000BE9 90 03 71         [24] 2502 	mov	dptr,#_axradio_transmit_PARM_2
      000BEC 74 31            [12] 2503 	mov	a,#_transmit_packet_demo_packet__1_352
      000BEE F0               [24] 2504 	movx	@dptr,a
      000BEF 74 00            [12] 2505 	mov	a,#(_transmit_packet_demo_packet__1_352 >> 8)
      000BF1 A3               [24] 2506 	inc	dptr
      000BF2 F0               [24] 2507 	movx	@dptr,a
      000BF3 E4               [12] 2508 	clr	a
      000BF4 A3               [24] 2509 	inc	dptr
      000BF5 F0               [24] 2510 	movx	@dptr,a
      000BF6 90 03 74         [24] 2511 	mov	dptr,#_axradio_transmit_PARM_3
      000BF9 74 17            [12] 2512 	mov	a,#0x17
      000BFB F0               [24] 2513 	movx	@dptr,a
      000BFC E4               [12] 2514 	clr	a
      000BFD A3               [24] 2515 	inc	dptr
      000BFE F0               [24] 2516 	movx	@dptr,a
      000BFF 90 7B 8D         [24] 2517 	mov	dptr,#_remoteaddr
      000C02 75 F0 80         [24] 2518 	mov	b,#0x80
      000C05 02 41 D6         [24] 2519 	ljmp	_axradio_transmit
                                   2520 ;------------------------------------------------------------
                                   2521 ;Allocation info for local variables in function 'axradio_statuschange'
                                   2522 ;------------------------------------------------------------
                                   2523 ;st                        Allocated with name '_axradio_statuschange_st_1_354'
                                   2524 ;i                         Allocated with name '_axradio_statuschange_i_1_355'
                                   2525 ;------------------------------------------------------------
                                   2526 ;	..\src\atrs\main.c:84: void axradio_statuschange(struct axradio_status __xdata *st)
                                   2527 ;	-----------------------------------------
                                   2528 ;	 function axradio_statuschange
                                   2529 ;	-----------------------------------------
      000C08                       2530 _axradio_statuschange:
      000C08 AF 83            [24] 2531 	mov	r7,dph
      000C0A E5 82            [12] 2532 	mov	a,dpl
      000C0C 90 00 48         [24] 2533 	mov	dptr,#_axradio_statuschange_st_1_354
      000C0F F0               [24] 2534 	movx	@dptr,a
      000C10 EF               [12] 2535 	mov	a,r7
      000C11 A3               [24] 2536 	inc	dptr
      000C12 F0               [24] 2537 	movx	@dptr,a
                                   2538 ;	..\src\atrs\main.c:88: switch (st->status)
      000C13 90 00 48         [24] 2539 	mov	dptr,#_axradio_statuschange_st_1_354
      000C16 E0               [24] 2540 	movx	a,@dptr
      000C17 FE               [12] 2541 	mov	r6,a
      000C18 A3               [24] 2542 	inc	dptr
      000C19 E0               [24] 2543 	movx	a,@dptr
      000C1A FF               [12] 2544 	mov	r7,a
      000C1B 8E 82            [24] 2545 	mov	dpl,r6
      000C1D 8F 83            [24] 2546 	mov	dph,r7
      000C1F E0               [24] 2547 	movx	a,@dptr
      000C20 FD               [12] 2548 	mov	r5,a
      000C21 BD 02 02         [24] 2549 	cjne	r5,#0x02,00199$
      000C24 80 36            [24] 2550 	sjmp	00155$
      000C26                       2551 00199$:
      000C26 BD 03 02         [24] 2552 	cjne	r5,#0x03,00200$
      000C29 80 05            [24] 2553 	sjmp	00105$
      000C2B                       2554 00200$:
                                   2555 ;	..\src\atrs\main.c:91: led0_on();
      000C2B BD 04 41         [24] 2556 	cjne	r5,#0x04,00173$
      000C2E 80 0F            [24] 2557 	sjmp	00120$
      000C30                       2558 00105$:
      000C30 D2 93            [12] 2559 	setb	_PORTC_3
                                   2560 ;	..\src\atrs\main.c:92: if (st->error == AXRADIO_ERR_RETRANSMISSION)
      000C32 8E 82            [24] 2561 	mov	dpl,r6
      000C34 8F 83            [24] 2562 	mov	dph,r7
      000C36 A3               [24] 2563 	inc	dptr
      000C37 E0               [24] 2564 	movx	a,@dptr
      000C38 FD               [12] 2565 	mov	r5,a
      000C39 BD 08 33         [24] 2566 	cjne	r5,#0x08,00173$
                                   2567 ;	..\src\atrs\main.c:93: led2_on();
      000C3C D2 82            [12] 2568 	setb	_PORTA_2
                                   2569 ;	..\src\atrs\main.c:94: break;
                                   2570 ;	..\src\atrs\main.c:97: led0_off();
      000C3E 22               [24] 2571 	ret
      000C3F                       2572 00120$:
      000C3F C2 93            [12] 2573 	clr	_PORTC_3
                                   2574 ;	..\src\atrs\main.c:98: if (st->error == AXRADIO_ERR_NOERROR) {
      000C41 8E 82            [24] 2575 	mov	dpl,r6
      000C43 8F 83            [24] 2576 	mov	dph,r7
      000C45 A3               [24] 2577 	inc	dptr
      000C46 E0               [24] 2578 	movx	a,@dptr
      000C47 FD               [12] 2579 	mov	r5,a
      000C48 70 04            [24] 2580 	jnz	00138$
                                   2581 ;	..\src\atrs\main.c:99: led2_off();
      000C4A C2 82            [12] 2582 	clr	_PORTA_2
      000C4C 80 05            [24] 2583 	sjmp	00139$
      000C4E                       2584 00138$:
                                   2585 ;	..\src\atrs\main.c:101: } else if (st->error == AXRADIO_ERR_TIMEOUT) {
      000C4E BD 03 02         [24] 2586 	cjne	r5,#0x03,00139$
                                   2587 ;	..\src\atrs\main.c:102: led2_on();
      000C51 D2 82            [12] 2588 	setb	_PORTA_2
      000C53                       2589 00139$:
                                   2590 ;	..\src\atrs\main.c:105: if (st->error == AXRADIO_ERR_BUSY)
      000C53 BD 02 03         [24] 2591 	cjne	r5,#0x02,00149$
                                   2592 ;	..\src\atrs\main.c:106: led3_on();
      000C56 D2 85            [12] 2593 	setb	_PORTA_5
                                   2594 ;	..\src\atrs\main.c:108: led3_off();
      000C58 22               [24] 2595 	ret
      000C59                       2596 00149$:
      000C59 C2 85            [12] 2597 	clr	_PORTA_5
                                   2598 ;	..\src\atrs\main.c:109: break;
                                   2599 ;	..\src\atrs\main.c:119: case AXRADIO_STAT_CHANNELSTATE:
      000C5B 22               [24] 2600 	ret
      000C5C                       2601 00155$:
                                   2602 ;	..\src\atrs\main.c:120: if (st->u.cs.busy)
      000C5C 74 08            [12] 2603 	mov	a,#0x08
      000C5E 2E               [12] 2604 	add	a,r6
      000C5F FE               [12] 2605 	mov	r6,a
      000C60 E4               [12] 2606 	clr	a
      000C61 3F               [12] 2607 	addc	a,r7
      000C62 FF               [12] 2608 	mov	r7,a
      000C63 8E 82            [24] 2609 	mov	dpl,r6
      000C65 8F 83            [24] 2610 	mov	dph,r7
      000C67 E0               [24] 2611 	movx	a,@dptr
      000C68 60 03            [24] 2612 	jz	00165$
                                   2613 ;	..\src\atrs\main.c:121: led3_on();
      000C6A D2 85            [12] 2614 	setb	_PORTA_5
                                   2615 ;	..\src\atrs\main.c:123: led3_off();
      000C6C 22               [24] 2616 	ret
      000C6D                       2617 00165$:
      000C6D C2 85            [12] 2618 	clr	_PORTA_5
                                   2619 ;	..\src\atrs\main.c:128: }
      000C6F                       2620 00173$:
      000C6F 22               [24] 2621 	ret
                                   2622 ;------------------------------------------------------------
                                   2623 ;Allocation info for local variables in function '_sdcc_external_startup'
                                   2624 ;------------------------------------------------------------
                                   2625 ;c                         Allocated with name '__sdcc_external_startup_c_2_379'
                                   2626 ;p                         Allocated with name '__sdcc_external_startup_p_2_379'
                                   2627 ;c                         Allocated with name '__sdcc_external_startup_c_2_380'
                                   2628 ;p                         Allocated with name '__sdcc_external_startup_p_2_380'
                                   2629 ;------------------------------------------------------------
                                   2630 ;	..\src\atrs\main.c:134: uint8_t _sdcc_external_startup(void)
                                   2631 ;	-----------------------------------------
                                   2632 ;	 function _sdcc_external_startup
                                   2633 ;	-----------------------------------------
      000C70                       2634 __sdcc_external_startup:
                                   2635 ;	..\src\atrs\main.c:136: LPXOSCGM = 0x8A;
      000C70 90 70 54         [24] 2636 	mov	dptr,#_LPXOSCGM
      000C73 74 8A            [12] 2637 	mov	a,#0x8a
      000C75 F0               [24] 2638 	movx	@dptr,a
                                   2639 ;	..\src\atrs\main.c:137: wtimer0_setclksrc(WTIMER0_CLKSRC, WTIMER0_PRESCALER);
      000C76 75 82 09         [24] 2640 	mov	dpl,#0x09
      000C79 12 62 32         [24] 2641 	lcall	_wtimer0_setconfig
                                   2642 ;	..\src\atrs\main.c:138: wtimer1_setclksrc(CLKSRC_FRCOSC, 7);
      000C7C 75 82 38         [24] 2643 	mov	dpl,#0x38
      000C7F 12 62 83         [24] 2644 	lcall	_wtimer1_setconfig
                                   2645 ;	..\src\atrs\main.c:140: LPOSCCONFIG = 0x09; // Slow, PRESC /1, no cal. Does NOT enable LPOSC. LPOSC is enabled upon configuring WTCFGA (MODE_TX_PERIODIC and receive_ack() )
      000C82 90 70 60         [24] 2646 	mov	dptr,#_LPOSCCONFIG
      000C85 74 09            [12] 2647 	mov	a,#0x09
      000C87 F0               [24] 2648 	movx	@dptr,a
                                   2649 ;	..\src\atrs\main.c:142: coldstart = !(PCON & 0x40);
      000C88 E5 87            [12] 2650 	mov	a,_PCON
      000C8A A2 E6            [12] 2651 	mov	c,acc[6]
      000C8C B3               [12] 2652 	cpl	c
      000C8D 92 00            [24] 2653 	mov	__sdcc_external_startup_sloc0_1_0,c
      000C8F E4               [12] 2654 	clr	a
      000C90 33               [12] 2655 	rlc	a
      000C91 F5 0A            [12] 2656 	mov	_coldstart,a
                                   2657 ;	..\src\atrs\main.c:144: ANALOGA = 0x18; // PA[3,4] LPXOSC, other PA are used as digital pins
      000C93 90 70 07         [24] 2658 	mov	dptr,#_ANALOGA
      000C96 74 18            [12] 2659 	mov	a,#0x18
      000C98 F0               [24] 2660 	movx	@dptr,a
                                   2661 ;	..\src\atrs\main.c:145: PORTA = 0xFF; //
      000C99 75 80 FF         [24] 2662 	mov	_PORTA,#0xff
                                   2663 ;	..\src\atrs\main.c:146: PORTB = 0xFD | (PINB & 0x02); // init LEDs to previous (frozen) state
      000C9C 74 02            [12] 2664 	mov	a,#0x02
      000C9E 55 E8            [12] 2665 	anl	a,_PINB
      000CA0 44 FD            [12] 2666 	orl	a,#0xfd
      000CA2 F5 88            [12] 2667 	mov	_PORTB,a
                                   2668 ;	..\src\atrs\main.c:147: PORTC = 0xFF; //
      000CA4 75 90 FF         [24] 2669 	mov	_PORTC,#0xff
                                   2670 ;	..\src\atrs\main.c:148: PORTR = 0x0B; //
      000CA7 75 8C 0B         [24] 2671 	mov	_PORTR,#0x0b
                                   2672 ;	..\src\atrs\main.c:149: DIRA = 0x00; //
      000CAA 75 89 00         [24] 2673 	mov	_DIRA,#0x00
                                   2674 ;	..\src\atrs\main.c:150: DIRB = 0x0e; //  PB1 = LED; PB2 / PB3 are outputs (in case PWRAMP / ANSTSEL are used)
      000CAD 75 8A 0E         [24] 2675 	mov	_DIRB,#0x0e
                                   2676 ;	..\src\atrs\main.c:151: DIRC = 0x00; //  PC4 = button
      000CB0 75 8B 00         [24] 2677 	mov	_DIRC,#0x00
                                   2678 ;	..\src\atrs\main.c:152: DIRR = 0x15; //
      000CB3 75 8E 15         [24] 2679 	mov	_DIRR,#0x15
                                   2680 ;	..\src\atrs\main.c:157: axradio_setup_pincfg1();
      000CB6 12 06 D5         [24] 2681 	lcall	_axradio_setup_pincfg1
                                   2682 ;	..\src\atrs\main.c:159: DPS = 0;
      000CB9 75 86 00         [24] 2683 	mov	_DPS,#0x00
                                   2684 ;	..\src\atrs\main.c:160: IE = 0x40;
      000CBC 75 A8 40         [24] 2685 	mov	_IE,#0x40
                                   2686 ;	..\src\atrs\main.c:161: EIE = 0x13;//habilito interrupcion de UART + timer0 + timer 1
      000CBF 75 98 13         [24] 2687 	mov	_EIE,#0x13
                                   2688 ;	..\src\atrs\main.c:162: IP_4 = 1; // radio interrupt high priority
      000CC2 D2 BC            [12] 2689 	setb	_IP_4
                                   2690 ;	..\src\atrs\main.c:163: EIP_0 = 1; //timer0 interrupt high priority
      000CC4 D2 B0            [12] 2691 	setb	_EIP_0
                                   2692 ;	..\src\atrs\main.c:164: E2IE = 0x00;// Keep random number generation interrupt disabled, if needed a random number just check the variable, and wait for it
      000CC6 75 A0 00         [24] 2693 	mov	_E2IE,#0x00
                                   2694 ;	..\src\atrs\main.c:165: UBLOX_GPS_PortInit();
      000CC9 12 56 24         [24] 2695 	lcall	_UBLOX_GPS_PortInit
                                   2696 ;	..\src\atrs\main.c:166: UART_Proc_PortInit();
      000CCC 12 4F 76         [24] 2697 	lcall	_UART_Proc_PortInit
                                   2698 ;	..\src\atrs\main.c:171: GPIOENABLE = 1; // unfreeze GPIO
      000CCF 90 70 0C         [24] 2699 	mov	dptr,#_GPIOENABLE
      000CD2 74 01            [12] 2700 	mov	a,#0x01
      000CD4 F0               [24] 2701 	movx	@dptr,a
                                   2702 ;	..\src\atrs\main.c:175: return !coldstart; // coldstart -> return 0 -> var initialization; start from sleep -> return 1 -> no var initialization
      000CD5 E5 0A            [12] 2703 	mov	a,_coldstart
      000CD7 B4 01 00         [24] 2704 	cjne	a,#0x01,00109$
      000CDA                       2705 00109$:
      000CDA 92 00            [24] 2706 	mov  __sdcc_external_startup_sloc0_1_0,c
      000CDC E4               [12] 2707 	clr	a
      000CDD 33               [12] 2708 	rlc	a
      000CDE F5 82            [12] 2709 	mov	dpl,a
      000CE0 22               [24] 2710 	ret
                                   2711 ;------------------------------------------------------------
                                   2712 ;Allocation info for local variables in function 'main'
                                   2713 ;------------------------------------------------------------
                                   2714 ;PROCD_PayloadLength       Allocated with name '_main_PROCD_PayloadLength_1_382'
                                   2715 ;PROCD_UartRxBuffer        Allocated with name '_main_PROCD_UartRxBuffer_1_382'
                                   2716 ;PROCD_Command             Allocated with name '_main_PROCD_Command_1_382'
                                   2717 ;c                         Allocated with name '_main_c_1_382'
                                   2718 ;UBLOX_setNav1             Allocated with name '_main_UBLOX_setNav1_1_382'
                                   2719 ;UBLOX_setNav2             Allocated with name '_main_UBLOX_setNav2_1_382'
                                   2720 ;UBLOX_setNav3             Allocated with name '_main_UBLOX_setNav3_1_382'
                                   2721 ;UBLOX_setNav5             Allocated with name '_main_UBLOX_setNav5_1_382'
                                   2722 ;UBLOX_Rx_Length           Allocated with name '_main_UBLOX_Rx_Length_1_382'
                                   2723 ;UBLOX_Rx_Payload          Allocated with name '_main_UBLOX_Rx_Payload_1_382'
                                   2724 ;------------------------------------------------------------
                                   2725 ;	..\src\atrs\main.c:182: void main(void)
                                   2726 ;	-----------------------------------------
                                   2727 ;	 function main
                                   2728 ;	-----------------------------------------
      000CE1                       2729 _main:
                                   2730 ;	..\src\atrs\main.c:187: volatile uint8_t    PROCD_PayloadLength= 0;     /**< \var length of the payload received from PROC-D*/
      000CE1 90 00 4A         [24] 2731 	mov	dptr,#_main_PROCD_PayloadLength_1_382
      000CE4 E4               [12] 2732 	clr	a
      000CE5 F0               [24] 2733 	movx	@dptr,a
                                   2734 ;	..\src\atrs\main.c:193: __xdata uint8_t  UBLOX_setNav1[]= {0xF0 ,0x04 ,0x00};/**< \brief Initial cofiguration for UBLOX GPS*/
      000CE6 90 00 4E         [24] 2735 	mov	dptr,#_main_UBLOX_setNav1_1_382
      000CE9 74 F0            [12] 2736 	mov	a,#0xf0
      000CEB F0               [24] 2737 	movx	@dptr,a
      000CEC 90 00 4F         [24] 2738 	mov	dptr,#(_main_UBLOX_setNav1_1_382 + 0x0001)
      000CEF 74 04            [12] 2739 	mov	a,#0x04
      000CF1 F0               [24] 2740 	movx	@dptr,a
      000CF2 90 00 50         [24] 2741 	mov	dptr,#(_main_UBLOX_setNav1_1_382 + 0x0002)
      000CF5 E4               [12] 2742 	clr	a
      000CF6 F0               [24] 2743 	movx	@dptr,a
                                   2744 ;	..\src\atrs\main.c:194: __xdata uint8_t  UBLOX_setNav2[]= {0xF0, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01}; /**< \brief Initial cofiguration for UBLOX GPS*/
      000CF7 90 00 51         [24] 2745 	mov	dptr,#_main_UBLOX_setNav2_1_382
      000CFA 74 F0            [12] 2746 	mov	a,#0xf0
      000CFC F0               [24] 2747 	movx	@dptr,a
      000CFD 90 00 52         [24] 2748 	mov	dptr,#(_main_UBLOX_setNav2_1_382 + 0x0001)
      000D00 74 01            [12] 2749 	mov	a,#0x01
      000D02 F0               [24] 2750 	movx	@dptr,a
      000D03 90 00 53         [24] 2751 	mov	dptr,#(_main_UBLOX_setNav2_1_382 + 0x0002)
      000D06 E4               [12] 2752 	clr	a
      000D07 F0               [24] 2753 	movx	@dptr,a
      000D08 90 00 54         [24] 2754 	mov	dptr,#(_main_UBLOX_setNav2_1_382 + 0x0003)
      000D0B F0               [24] 2755 	movx	@dptr,a
      000D0C 90 00 55         [24] 2756 	mov	dptr,#(_main_UBLOX_setNav2_1_382 + 0x0004)
      000D0F F0               [24] 2757 	movx	@dptr,a
      000D10 90 00 56         [24] 2758 	mov	dptr,#(_main_UBLOX_setNav2_1_382 + 0x0005)
      000D13 F0               [24] 2759 	movx	@dptr,a
      000D14 90 00 57         [24] 2760 	mov	dptr,#(_main_UBLOX_setNav2_1_382 + 0x0006)
      000D17 F0               [24] 2761 	movx	@dptr,a
      000D18 90 00 58         [24] 2762 	mov	dptr,#(_main_UBLOX_setNav2_1_382 + 0x0007)
      000D1B 04               [12] 2763 	inc	a
      000D1C F0               [24] 2764 	movx	@dptr,a
                                   2765 ;	..\src\atrs\main.c:195: __xdata uint8_t  UBLOX_setNav3[] = {0xF0, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01};/**< \brief Initial cofiguration for UBLOX GPS*/
      000D1D 90 00 59         [24] 2766 	mov	dptr,#_main_UBLOX_setNav3_1_382
      000D20 74 F0            [12] 2767 	mov	a,#0xf0
      000D22 F0               [24] 2768 	movx	@dptr,a
      000D23 90 00 5A         [24] 2769 	mov	dptr,#(_main_UBLOX_setNav3_1_382 + 0x0001)
      000D26 74 04            [12] 2770 	mov	a,#0x04
      000D28 F0               [24] 2771 	movx	@dptr,a
      000D29 90 00 5B         [24] 2772 	mov	dptr,#(_main_UBLOX_setNav3_1_382 + 0x0002)
      000D2C E4               [12] 2773 	clr	a
      000D2D F0               [24] 2774 	movx	@dptr,a
      000D2E 90 00 5C         [24] 2775 	mov	dptr,#(_main_UBLOX_setNav3_1_382 + 0x0003)
      000D31 F0               [24] 2776 	movx	@dptr,a
      000D32 90 00 5D         [24] 2777 	mov	dptr,#(_main_UBLOX_setNav3_1_382 + 0x0004)
      000D35 F0               [24] 2778 	movx	@dptr,a
      000D36 90 00 5E         [24] 2779 	mov	dptr,#(_main_UBLOX_setNav3_1_382 + 0x0005)
      000D39 F0               [24] 2780 	movx	@dptr,a
      000D3A 90 00 5F         [24] 2781 	mov	dptr,#(_main_UBLOX_setNav3_1_382 + 0x0006)
      000D3D F0               [24] 2782 	movx	@dptr,a
      000D3E 90 00 60         [24] 2783 	mov	dptr,#(_main_UBLOX_setNav3_1_382 + 0x0007)
      000D41 04               [12] 2784 	inc	a
      000D42 F0               [24] 2785 	movx	@dptr,a
                                   2786 ;	..\src\atrs\main.c:196: __xdata uint8_t  UBLOX_setNav5[] ={0xFF, 0xFF, 0x06, 0x03, 0x00, 0x00, 0x00, 0x00, 0x10, 0x27, 0x00, 0x00,
      000D43 90 00 61         [24] 2787 	mov	dptr,#_main_UBLOX_setNav5_1_382
      000D46 74 FF            [12] 2788 	mov	a,#0xff
      000D48 F0               [24] 2789 	movx	@dptr,a
      000D49 90 00 62         [24] 2790 	mov	dptr,#(_main_UBLOX_setNav5_1_382 + 0x0001)
      000D4C F0               [24] 2791 	movx	@dptr,a
      000D4D 90 00 63         [24] 2792 	mov	dptr,#(_main_UBLOX_setNav5_1_382 + 0x0002)
      000D50 74 06            [12] 2793 	mov	a,#0x06
      000D52 F0               [24] 2794 	movx	@dptr,a
      000D53 90 00 64         [24] 2795 	mov	dptr,#(_main_UBLOX_setNav5_1_382 + 0x0003)
      000D56 03               [12] 2796 	rr	a
      000D57 F0               [24] 2797 	movx	@dptr,a
      000D58 90 00 65         [24] 2798 	mov	dptr,#(_main_UBLOX_setNav5_1_382 + 0x0004)
      000D5B E4               [12] 2799 	clr	a
      000D5C F0               [24] 2800 	movx	@dptr,a
      000D5D 90 00 66         [24] 2801 	mov	dptr,#(_main_UBLOX_setNav5_1_382 + 0x0005)
      000D60 F0               [24] 2802 	movx	@dptr,a
      000D61 90 00 67         [24] 2803 	mov	dptr,#(_main_UBLOX_setNav5_1_382 + 0x0006)
      000D64 F0               [24] 2804 	movx	@dptr,a
      000D65 90 00 68         [24] 2805 	mov	dptr,#(_main_UBLOX_setNav5_1_382 + 0x0007)
      000D68 F0               [24] 2806 	movx	@dptr,a
      000D69 90 00 69         [24] 2807 	mov	dptr,#(_main_UBLOX_setNav5_1_382 + 0x0008)
      000D6C 74 10            [12] 2808 	mov	a,#0x10
      000D6E F0               [24] 2809 	movx	@dptr,a
      000D6F 90 00 6A         [24] 2810 	mov	dptr,#(_main_UBLOX_setNav5_1_382 + 0x0009)
      000D72 74 27            [12] 2811 	mov	a,#0x27
      000D74 F0               [24] 2812 	movx	@dptr,a
      000D75 90 00 6B         [24] 2813 	mov	dptr,#(_main_UBLOX_setNav5_1_382 + 0x000a)
      000D78 E4               [12] 2814 	clr	a
      000D79 F0               [24] 2815 	movx	@dptr,a
      000D7A 90 00 6C         [24] 2816 	mov	dptr,#(_main_UBLOX_setNav5_1_382 + 0x000b)
      000D7D F0               [24] 2817 	movx	@dptr,a
      000D7E 90 00 6D         [24] 2818 	mov	dptr,#(_main_UBLOX_setNav5_1_382 + 0x000c)
      000D81 74 05            [12] 2819 	mov	a,#0x05
      000D83 F0               [24] 2820 	movx	@dptr,a
      000D84 90 00 6E         [24] 2821 	mov	dptr,#(_main_UBLOX_setNav5_1_382 + 0x000d)
      000D87 E4               [12] 2822 	clr	a
      000D88 F0               [24] 2823 	movx	@dptr,a
      000D89 90 00 6F         [24] 2824 	mov	dptr,#(_main_UBLOX_setNav5_1_382 + 0x000e)
      000D8C 74 FA            [12] 2825 	mov	a,#0xfa
      000D8E F0               [24] 2826 	movx	@dptr,a
      000D8F 90 00 70         [24] 2827 	mov	dptr,#(_main_UBLOX_setNav5_1_382 + 0x000f)
      000D92 E4               [12] 2828 	clr	a
      000D93 F0               [24] 2829 	movx	@dptr,a
      000D94 90 00 71         [24] 2830 	mov	dptr,#(_main_UBLOX_setNav5_1_382 + 0x0010)
      000D97 74 FA            [12] 2831 	mov	a,#0xfa
      000D99 F0               [24] 2832 	movx	@dptr,a
      000D9A 90 00 72         [24] 2833 	mov	dptr,#(_main_UBLOX_setNav5_1_382 + 0x0011)
      000D9D E4               [12] 2834 	clr	a
      000D9E F0               [24] 2835 	movx	@dptr,a
      000D9F 90 00 73         [24] 2836 	mov	dptr,#(_main_UBLOX_setNav5_1_382 + 0x0012)
      000DA2 74 64            [12] 2837 	mov	a,#0x64
      000DA4 F0               [24] 2838 	movx	@dptr,a
      000DA5 90 00 74         [24] 2839 	mov	dptr,#(_main_UBLOX_setNav5_1_382 + 0x0013)
      000DA8 E4               [12] 2840 	clr	a
      000DA9 F0               [24] 2841 	movx	@dptr,a
      000DAA 90 00 75         [24] 2842 	mov	dptr,#(_main_UBLOX_setNav5_1_382 + 0x0014)
      000DAD 74 2C            [12] 2843 	mov	a,#0x2c
      000DAF F0               [24] 2844 	movx	@dptr,a
      000DB0 90 00 76         [24] 2845 	mov	dptr,#(_main_UBLOX_setNav5_1_382 + 0x0015)
      000DB3 74 01            [12] 2846 	mov	a,#0x01
      000DB5 F0               [24] 2847 	movx	@dptr,a
      000DB6 90 00 77         [24] 2848 	mov	dptr,#(_main_UBLOX_setNav5_1_382 + 0x0016)
      000DB9 E4               [12] 2849 	clr	a
      000DBA F0               [24] 2850 	movx	@dptr,a
      000DBB 90 00 78         [24] 2851 	mov	dptr,#(_main_UBLOX_setNav5_1_382 + 0x0017)
      000DBE F0               [24] 2852 	movx	@dptr,a
      000DBF 90 00 79         [24] 2853 	mov	dptr,#(_main_UBLOX_setNav5_1_382 + 0x0018)
      000DC2 F0               [24] 2854 	movx	@dptr,a
      000DC3 90 00 7A         [24] 2855 	mov	dptr,#(_main_UBLOX_setNav5_1_382 + 0x0019)
      000DC6 F0               [24] 2856 	movx	@dptr,a
      000DC7 90 00 7B         [24] 2857 	mov	dptr,#(_main_UBLOX_setNav5_1_382 + 0x001a)
      000DCA F0               [24] 2858 	movx	@dptr,a
      000DCB 90 00 7C         [24] 2859 	mov	dptr,#(_main_UBLOX_setNav5_1_382 + 0x001b)
      000DCE F0               [24] 2860 	movx	@dptr,a
      000DCF 90 00 7D         [24] 2861 	mov	dptr,#(_main_UBLOX_setNav5_1_382 + 0x001c)
      000DD2 F0               [24] 2862 	movx	@dptr,a
      000DD3 90 00 7E         [24] 2863 	mov	dptr,#(_main_UBLOX_setNav5_1_382 + 0x001d)
      000DD6 F0               [24] 2864 	movx	@dptr,a
      000DD7 90 00 7F         [24] 2865 	mov	dptr,#(_main_UBLOX_setNav5_1_382 + 0x001e)
      000DDA F0               [24] 2866 	movx	@dptr,a
      000DDB 90 00 80         [24] 2867 	mov	dptr,#(_main_UBLOX_setNav5_1_382 + 0x001f)
      000DDE F0               [24] 2868 	movx	@dptr,a
      000DDF 90 00 81         [24] 2869 	mov	dptr,#(_main_UBLOX_setNav5_1_382 + 0x0020)
      000DE2 F0               [24] 2870 	movx	@dptr,a
      000DE3 90 00 82         [24] 2871 	mov	dptr,#(_main_UBLOX_setNav5_1_382 + 0x0021)
      000DE6 F0               [24] 2872 	movx	@dptr,a
      000DE7 90 00 83         [24] 2873 	mov	dptr,#(_main_UBLOX_setNav5_1_382 + 0x0022)
      000DEA F0               [24] 2874 	movx	@dptr,a
      000DEB 90 00 84         [24] 2875 	mov	dptr,#(_main_UBLOX_setNav5_1_382 + 0x0023)
      000DEE F0               [24] 2876 	movx	@dptr,a
                                   2877 ;	..\src\atrs\main.c:199: __xdata uint16_t UBLOX_Rx_Length = 0;               /**< \brief UBLOX GPS received data length */
      000DEF 90 00 85         [24] 2878 	mov	dptr,#_main_UBLOX_Rx_Length_1_382
      000DF2 F0               [24] 2879 	movx	@dptr,a
      000DF3 A3               [24] 2880 	inc	dptr
      000DF4 F0               [24] 2881 	movx	@dptr,a
                                   2882 ;	..\src\atrs\main.c:213: __endasm;
                           000000  2883 	G$_start__stack$0$0	= __start__stack
                                   2884 	.globl	G$_start__stack$0$0
                                   2885 ;	..\src\atrs\main.c:215: RxBuffer =(uint16_t*) malloc(sizeof(uint16_t)*20);
      000DF5 90 00 28         [24] 2886 	mov	dptr,#0x0028
      000DF8 12 6B C4         [24] 2887 	lcall	_malloc
      000DFB AE 82            [24] 2888 	mov	r6,dpl
      000DFD AF 83            [24] 2889 	mov	r7,dph
      000DFF 90 00 2C         [24] 2890 	mov	dptr,#_RxBuffer
      000E02 EE               [12] 2891 	mov	a,r6
      000E03 F0               [24] 2892 	movx	@dptr,a
      000E04 EF               [12] 2893 	mov	a,r7
      000E05 A3               [24] 2894 	inc	dptr
      000E06 F0               [24] 2895 	movx	@dptr,a
                                   2896 ;	..\src\atrs\main.c:216: PROCD_UartRxBuffer = (uint8_t*) malloc(sizeof(uint8_t)*20);
      000E07 90 00 14         [24] 2897 	mov	dptr,#0x0014
      000E0A 12 6B C4         [24] 2898 	lcall	_malloc
      000E0D AE 82            [24] 2899 	mov	r6,dpl
      000E0F AF 83            [24] 2900 	mov	r7,dph
                                   2901 ;	..\src\atrs\main.c:217: EA = 1;
      000E11 D2 AF            [12] 2902 	setb	_EA
                                   2903 ;	..\src\atrs\main.c:218: flash_apply_calibration();
      000E13 C0 07            [24] 2904 	push	ar7
      000E15 C0 06            [24] 2905 	push	ar6
      000E17 12 6D 3E         [24] 2906 	lcall	_flash_apply_calibration
                                   2907 ;	..\src\atrs\main.c:220: CLKCON = 0x00;
      000E1A 75 C6 00         [24] 2908 	mov	_CLKCON,#0x00
                                   2909 ;	..\src\atrs\main.c:221: wtimer_init();
      000E1D 12 63 5E         [24] 2910 	lcall	_wtimer_init
                                   2911 ;	..\src\atrs\main.c:222: dbglink_init();
      000E20 12 67 28         [24] 2912 	lcall	_dbglink_init
                                   2913 ;	..\src\atrs\main.c:223: delay_ms(10);
      000E23 90 00 0A         [24] 2914 	mov	dptr,#0x000a
      000E26 12 10 7A         [24] 2915 	lcall	_delay_ms
      000E29 D0 06            [24] 2916 	pop	ar6
      000E2B D0 07            [24] 2917 	pop	ar7
                                   2918 ;	..\src\atrs\main.c:224: if (coldstart) {
      000E2D E5 0A            [12] 2919 	mov	a,_coldstart
      000E2F 60 46            [24] 2920 	jz	00126$
                                   2921 ;	..\src\atrs\main.c:226: led0_off();
      000E31 C2 93            [12] 2922 	clr	_PORTC_3
                                   2923 ;	..\src\atrs\main.c:227: led1_off();
      000E33 C2 80            [12] 2924 	clr	_PORTA_0
                                   2925 ;	..\src\atrs\main.c:228: led2_off();
      000E35 C2 82            [12] 2926 	clr	_PORTA_2
                                   2927 ;	..\src\atrs\main.c:229: led3_off();
      000E37 C2 85            [12] 2928 	clr	_PORTA_5
                                   2929 ;	..\src\atrs\main.c:231: axradio_init();
      000E39 C0 07            [24] 2930 	push	ar7
      000E3B C0 06            [24] 2931 	push	ar6
      000E3D 12 36 02         [24] 2932 	lcall	_axradio_init
                                   2933 ;	..\src\atrs\main.c:233: axradio_set_local_address(&localaddr);
      000E40 90 7B 91         [24] 2934 	mov	dptr,#_localaddr
      000E43 75 F0 80         [24] 2935 	mov	b,#0x80
      000E46 12 41 63         [24] 2936 	lcall	_axradio_set_local_address
                                   2937 ;	..\src\atrs\main.c:234: axradio_set_default_remote_address(&remoteaddr);
      000E49 90 7B 8D         [24] 2938 	mov	dptr,#_remoteaddr
      000E4C 75 F0 80         [24] 2939 	mov	b,#0x80
      000E4F 12 41 9E         [24] 2940 	lcall	_axradio_set_default_remote_address
                                   2941 ;	..\src\atrs\main.c:235: delay_ms(lpxosc_settlingtime+1500);//this delay is important to let the radio init
      000E52 90 7B 9B         [24] 2942 	mov	dptr,#_lpxosc_settlingtime
      000E55 E4               [12] 2943 	clr	a
      000E56 93               [24] 2944 	movc	a,@a+dptr
      000E57 FC               [12] 2945 	mov	r4,a
      000E58 74 01            [12] 2946 	mov	a,#0x01
      000E5A 93               [24] 2947 	movc	a,@a+dptr
      000E5B FD               [12] 2948 	mov	r5,a
      000E5C 74 DC            [12] 2949 	mov	a,#0xdc
      000E5E 2C               [12] 2950 	add	a,r4
      000E5F FC               [12] 2951 	mov	r4,a
      000E60 74 05            [12] 2952 	mov	a,#0x05
      000E62 3D               [12] 2953 	addc	a,r5
      000E63 FD               [12] 2954 	mov	r5,a
      000E64 8C 82            [24] 2955 	mov	dpl,r4
      000E66 8D 83            [24] 2956 	mov	dph,r5
      000E68 12 10 7A         [24] 2957 	lcall	_delay_ms
                                   2958 ;	..\src\atrs\main.c:238: axradio_set_mode(RADIO_MODE);
      000E6B 75 82 20         [24] 2959 	mov	dpl,#0x20
      000E6E 12 3B 2A         [24] 2960 	lcall	_axradio_set_mode
      000E71 D0 06            [24] 2961 	pop	ar6
      000E73 D0 07            [24] 2962 	pop	ar7
      000E75 80 0D            [24] 2963 	sjmp	00127$
      000E77                       2964 00126$:
                                   2965 ;	..\src\atrs\main.c:242: ax5043_commsleepexit();
      000E77 C0 07            [24] 2966 	push	ar7
      000E79 C0 06            [24] 2967 	push	ar6
      000E7B 12 71 08         [24] 2968 	lcall	_ax5043_commsleepexit
      000E7E D0 06            [24] 2969 	pop	ar6
      000E80 D0 07            [24] 2970 	pop	ar7
                                   2971 ;	..\src\atrs\main.c:243: IE_4 = 1;
      000E82 D2 AC            [12] 2972 	setb	_IE_4
      000E84                       2973 00127$:
                                   2974 ;	..\src\atrs\main.c:246: axradio_setup_pincfg2();
      000E84 C0 07            [24] 2975 	push	ar7
      000E86 C0 06            [24] 2976 	push	ar6
      000E88 12 06 DC         [24] 2977 	lcall	_axradio_setup_pincfg2
                                   2978 ;	..\src\atrs\main.c:256: delay_tx(5000,10000,15000);
      000E8B 90 00 A7         [24] 2979 	mov	dptr,#_delay_tx_PARM_2
      000E8E 74 10            [12] 2980 	mov	a,#0x10
      000E90 F0               [24] 2981 	movx	@dptr,a
      000E91 74 27            [12] 2982 	mov	a,#0x27
      000E93 A3               [24] 2983 	inc	dptr
      000E94 F0               [24] 2984 	movx	@dptr,a
      000E95 E4               [12] 2985 	clr	a
      000E96 A3               [24] 2986 	inc	dptr
      000E97 F0               [24] 2987 	movx	@dptr,a
      000E98 A3               [24] 2988 	inc	dptr
      000E99 F0               [24] 2989 	movx	@dptr,a
      000E9A 90 00 AB         [24] 2990 	mov	dptr,#_delay_tx_PARM_3
      000E9D 74 98            [12] 2991 	mov	a,#0x98
      000E9F F0               [24] 2992 	movx	@dptr,a
      000EA0 74 3A            [12] 2993 	mov	a,#0x3a
      000EA2 A3               [24] 2994 	inc	dptr
      000EA3 F0               [24] 2995 	movx	@dptr,a
      000EA4 E4               [12] 2996 	clr	a
      000EA5 A3               [24] 2997 	inc	dptr
      000EA6 F0               [24] 2998 	movx	@dptr,a
      000EA7 A3               [24] 2999 	inc	dptr
      000EA8 F0               [24] 3000 	movx	@dptr,a
      000EA9 90 13 88         [24] 3001 	mov	dptr,#0x1388
      000EAC E4               [12] 3002 	clr	a
      000EAD F5 F0            [12] 3003 	mov	b,a
      000EAF 12 12 03         [24] 3004 	lcall	_delay_tx
      000EB2 D0 06            [24] 3005 	pop	ar6
      000EB4 D0 07            [24] 3006 	pop	ar7
      000EB6                       3007 00146$:
                                   3008 ;	..\src\atrs\main.c:307: switch(RfStateMachine)
      000EB6 90 09 8F         [24] 3009 	mov	dptr,#_RfStateMachine
      000EB9 E0               [24] 3010 	movx	a,@dptr
      000EBA FD               [12] 3011 	mov	r5,a
      000EBB 60 05            [24] 3012 	jz	00128$
                                   3013 ;	..\src\atrs\main.c:309: case e_WaitTxMessage:
      000EBD BD 01 28         [24] 3014 	cjne	r5,#0x01,00134$
      000EC0 80 0B            [24] 3015 	sjmp	00129$
      000EC2                       3016 00128$:
                                   3017 ;	..\src\atrs\main.c:310: RfStateMachine ++;
      000EC2 90 09 8F         [24] 3018 	mov	dptr,#_RfStateMachine
      000EC5 ED               [12] 3019 	mov	a,r5
      000EC6 04               [12] 3020 	inc	a
      000EC7 F0               [24] 3021 	movx	@dptr,a
                                   3022 ;	..\src\atrs\main.c:311: RfStateMachine --;
      000EC8 E0               [24] 3023 	movx	a,@dptr
      000EC9 14               [12] 3024 	dec	a
      000ECA F0               [24] 3025 	movx	@dptr,a
                                   3026 ;	..\src\atrs\main.c:317: break;
                                   3027 ;	..\src\atrs\main.c:319: case e_WaitForBeaconAndMessageSend:
      000ECB 80 1B            [24] 3028 	sjmp	00134$
      000ECD                       3029 00129$:
                                   3030 ;	..\src\atrs\main.c:320: if(RxIsrFlag==0x01)
      000ECD 90 00 0D         [24] 3031 	mov	dptr,#_RxIsrFlag
      000ED0 E0               [24] 3032 	movx	a,@dptr
      000ED1 FC               [12] 3033 	mov	r4,a
      000ED2 A3               [24] 3034 	inc	dptr
      000ED3 E0               [24] 3035 	movx	a,@dptr
      000ED4 FD               [12] 3036 	mov	r5,a
      000ED5 BC 01 10         [24] 3037 	cjne	r4,#0x01,00134$
      000ED8 BD 00 0D         [24] 3038 	cjne	r5,#0x00,00134$
                                   3039 ;	..\src\atrs\main.c:323: RxIsrFlag=0x00;
      000EDB 90 00 0D         [24] 3040 	mov	dptr,#_RxIsrFlag
      000EDE E4               [12] 3041 	clr	a
      000EDF F0               [24] 3042 	movx	@dptr,a
      000EE0 A3               [24] 3043 	inc	dptr
      000EE1 F0               [24] 3044 	movx	@dptr,a
                                   3045 ;	..\src\atrs\main.c:329: RfStateMachine = e_WaitForACK;
      000EE2 90 09 8F         [24] 3046 	mov	dptr,#_RfStateMachine
      000EE5 74 02            [12] 3047 	mov	a,#0x02
      000EE7 F0               [24] 3048 	movx	@dptr,a
                                   3049 ;	..\src\atrs\main.c:341: }
      000EE8                       3050 00134$:
                                   3051 ;	..\src\atrs\main.c:347: if( UART_Proc_VerifyIncomingMsg(&PROCD_Command,PROCD_UartRxBuffer , &PROCD_PayloadLength) == RX_SUCCESFULL)
      000EE8 8E 04            [24] 3052 	mov	ar4,r6
      000EEA 8F 05            [24] 3053 	mov	ar5,r7
      000EEC 7B 00            [12] 3054 	mov	r3,#0x00
      000EEE 90 03 83         [24] 3055 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_PARM_2
      000EF1 EC               [12] 3056 	mov	a,r4
      000EF2 F0               [24] 3057 	movx	@dptr,a
      000EF3 ED               [12] 3058 	mov	a,r5
      000EF4 A3               [24] 3059 	inc	dptr
      000EF5 F0               [24] 3060 	movx	@dptr,a
      000EF6 EB               [12] 3061 	mov	a,r3
      000EF7 A3               [24] 3062 	inc	dptr
      000EF8 F0               [24] 3063 	movx	@dptr,a
      000EF9 90 03 86         [24] 3064 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_PARM_3
      000EFC 74 4A            [12] 3065 	mov	a,#_main_PROCD_PayloadLength_1_382
      000EFE F0               [24] 3066 	movx	@dptr,a
      000EFF 74 00            [12] 3067 	mov	a,#(_main_PROCD_PayloadLength_1_382 >> 8)
      000F01 A3               [24] 3068 	inc	dptr
      000F02 F0               [24] 3069 	movx	@dptr,a
      000F03 E4               [12] 3070 	clr	a
      000F04 A3               [24] 3071 	inc	dptr
      000F05 F0               [24] 3072 	movx	@dptr,a
      000F06 90 00 4D         [24] 3073 	mov	dptr,#_main_PROCD_Command_1_382
      000F09 75 F0 00         [24] 3074 	mov	b,#0x00
      000F0C C0 07            [24] 3075 	push	ar7
      000F0E C0 06            [24] 3076 	push	ar6
      000F10 12 4F CA         [24] 3077 	lcall	_UART_Proc_VerifyIncomingMsg
      000F13 AD 82            [24] 3078 	mov	r5,dpl
      000F15 D0 06            [24] 3079 	pop	ar6
      000F17 D0 07            [24] 3080 	pop	ar7
      000F19 BD 04 02         [24] 3081 	cjne	r5,#0x04,00186$
      000F1C 80 03            [24] 3082 	sjmp	00187$
      000F1E                       3083 00186$:
      000F1E 02 0F BC         [24] 3084 	ljmp	00137$
      000F21                       3085 00187$:
                                   3086 ;	..\src\atrs\main.c:350: dbglink_writestr("comando Rx: ");
      000F21 90 7B 2A         [24] 3087 	mov	dptr,#___str_0
      000F24 75 F0 80         [24] 3088 	mov	b,#0x80
      000F27 C0 07            [24] 3089 	push	ar7
      000F29 C0 06            [24] 3090 	push	ar6
      000F2B 12 6E FA         [24] 3091 	lcall	_dbglink_writestr
                                   3092 ;	..\src\atrs\main.c:351: dbglink_writehex16(PROCD_Command,1,WRNUM_PADZERO);
      000F2E 90 00 4D         [24] 3093 	mov	dptr,#_main_PROCD_Command_1_382
      000F31 E0               [24] 3094 	movx	a,@dptr
      000F32 FD               [12] 3095 	mov	r5,a
      000F33 7C 00            [12] 3096 	mov	r4,#0x00
      000F35 74 08            [12] 3097 	mov	a,#0x08
      000F37 C0 E0            [24] 3098 	push	acc
      000F39 74 01            [12] 3099 	mov	a,#0x01
      000F3B C0 E0            [24] 3100 	push	acc
      000F3D 8D 82            [24] 3101 	mov	dpl,r5
      000F3F 8C 83            [24] 3102 	mov	dph,r4
      000F41 12 74 DE         [24] 3103 	lcall	_dbglink_writehex16
      000F44 15 81            [12] 3104 	dec	sp
      000F46 15 81            [12] 3105 	dec	sp
                                   3106 ;	..\src\atrs\main.c:352: dbglink_writestr("\n Longitud Rx: ");
      000F48 90 7B 37         [24] 3107 	mov	dptr,#___str_1
      000F4B 75 F0 80         [24] 3108 	mov	b,#0x80
      000F4E 12 6E FA         [24] 3109 	lcall	_dbglink_writestr
                                   3110 ;	..\src\atrs\main.c:353: dbglink_writehex16(PROCD_PayloadLength,1,WRNUM_PADZERO);
      000F51 90 00 4A         [24] 3111 	mov	dptr,#_main_PROCD_PayloadLength_1_382
      000F54 E0               [24] 3112 	movx	a,@dptr
      000F55 FD               [12] 3113 	mov	r5,a
      000F56 7C 00            [12] 3114 	mov	r4,#0x00
      000F58 74 08            [12] 3115 	mov	a,#0x08
      000F5A C0 E0            [24] 3116 	push	acc
      000F5C 74 01            [12] 3117 	mov	a,#0x01
      000F5E C0 E0            [24] 3118 	push	acc
      000F60 8D 82            [24] 3119 	mov	dpl,r5
      000F62 8C 83            [24] 3120 	mov	dph,r4
      000F64 12 74 DE         [24] 3121 	lcall	_dbglink_writehex16
      000F67 15 81            [12] 3122 	dec	sp
      000F69 15 81            [12] 3123 	dec	sp
                                   3124 ;	..\src\atrs\main.c:354: dbglink_writestr("\n payload: ");
      000F6B 90 7B 47         [24] 3125 	mov	dptr,#___str_2
      000F6E 75 F0 80         [24] 3126 	mov	b,#0x80
      000F71 12 6E FA         [24] 3127 	lcall	_dbglink_writestr
      000F74 D0 06            [24] 3128 	pop	ar6
      000F76 D0 07            [24] 3129 	pop	ar7
                                   3130 ;	..\src\atrs\main.c:355: for(i=0;i<PROCD_PayloadLength;i++)
      000F78 90 00 2E         [24] 3131 	mov	dptr,#_i
      000F7B E4               [12] 3132 	clr	a
      000F7C F0               [24] 3133 	movx	@dptr,a
      000F7D                       3134 00144$:
      000F7D 90 00 2E         [24] 3135 	mov	dptr,#_i
      000F80 E0               [24] 3136 	movx	a,@dptr
      000F81 FD               [12] 3137 	mov	r5,a
      000F82 90 00 4A         [24] 3138 	mov	dptr,#_main_PROCD_PayloadLength_1_382
      000F85 E0               [24] 3139 	movx	a,@dptr
      000F86 FC               [12] 3140 	mov	r4,a
      000F87 C3               [12] 3141 	clr	c
      000F88 ED               [12] 3142 	mov	a,r5
      000F89 9C               [12] 3143 	subb	a,r4
      000F8A 50 30            [24] 3144 	jnc	00137$
                                   3145 ;	..\src\atrs\main.c:357: dbglink_writehex16(*(PROCD_UartRxBuffer+i),1,WRNUM_PADZERO);
      000F8C ED               [12] 3146 	mov	a,r5
      000F8D 2E               [12] 3147 	add	a,r6
      000F8E F5 82            [12] 3148 	mov	dpl,a
      000F90 E4               [12] 3149 	clr	a
      000F91 3F               [12] 3150 	addc	a,r7
      000F92 F5 83            [12] 3151 	mov	dph,a
      000F94 E0               [24] 3152 	movx	a,@dptr
      000F95 FD               [12] 3153 	mov	r5,a
      000F96 7C 00            [12] 3154 	mov	r4,#0x00
      000F98 C0 07            [24] 3155 	push	ar7
      000F9A C0 06            [24] 3156 	push	ar6
      000F9C 74 08            [12] 3157 	mov	a,#0x08
      000F9E C0 E0            [24] 3158 	push	acc
      000FA0 74 01            [12] 3159 	mov	a,#0x01
      000FA2 C0 E0            [24] 3160 	push	acc
      000FA4 8D 82            [24] 3161 	mov	dpl,r5
      000FA6 8C 83            [24] 3162 	mov	dph,r4
      000FA8 12 74 DE         [24] 3163 	lcall	_dbglink_writehex16
      000FAB 15 81            [12] 3164 	dec	sp
      000FAD 15 81            [12] 3165 	dec	sp
      000FAF D0 06            [24] 3166 	pop	ar6
      000FB1 D0 07            [24] 3167 	pop	ar7
                                   3168 ;	..\src\atrs\main.c:355: for(i=0;i<PROCD_PayloadLength;i++)
      000FB3 90 00 2E         [24] 3169 	mov	dptr,#_i
      000FB6 E0               [24] 3170 	movx	a,@dptr
      000FB7 24 01            [12] 3171 	add	a,#0x01
      000FB9 F0               [24] 3172 	movx	@dptr,a
      000FBA 80 C1            [24] 3173 	sjmp	00144$
      000FBC                       3174 00137$:
                                   3175 ;	..\src\atrs\main.c:361: if(Tmr0Flg == 0x01) //timer has been activated
      000FBC 90 09 90         [24] 3176 	mov	dptr,#_Tmr0Flg
      000FBF E0               [24] 3177 	movx	a,@dptr
      000FC0 FD               [12] 3178 	mov	r5,a
      000FC1 BD 01 61         [24] 3179 	cjne	r5,#0x01,00139$
                                   3180 ;	..\src\atrs\main.c:363: Tmr0Flg = 0x00;
      000FC4 90 09 90         [24] 3181 	mov	dptr,#_Tmr0Flg
      000FC7 E4               [12] 3182 	clr	a
      000FC8 F0               [24] 3183 	movx	@dptr,a
                                   3184 ;	..\src\atrs\main.c:364: UBLOX_Rx_Payload = malloc(30*sizeof(uint8_t));
      000FC9 90 00 1E         [24] 3185 	mov	dptr,#0x001e
      000FCC C0 07            [24] 3186 	push	ar7
      000FCE C0 06            [24] 3187 	push	ar6
      000FD0 12 6B C4         [24] 3188 	lcall	_malloc
      000FD3 AC 82            [24] 3189 	mov	r4,dpl
      000FD5 AD 83            [24] 3190 	mov	r5,dph
                                   3191 ;	..\src\atrs\main.c:365: UBLOX_GPS_SendCommand_WaitACK(eNAV,POSLLH,0x00,UBLOX_Rx_Payload,ANS,&UBLOX_Rx_Length);
      000FD7 8C 02            [24] 3192 	mov	ar2,r4
      000FD9 8D 03            [24] 3193 	mov	ar3,r5
      000FDB 8B 01            [24] 3194 	mov	ar1,r3
      000FDD 7B 00            [12] 3195 	mov	r3,#0x00
      000FDF 90 03 B3         [24] 3196 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_2
      000FE2 74 02            [12] 3197 	mov	a,#0x02
      000FE4 F0               [24] 3198 	movx	@dptr,a
      000FE5 90 03 B4         [24] 3199 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_3
      000FE8 E4               [12] 3200 	clr	a
      000FE9 F0               [24] 3201 	movx	@dptr,a
      000FEA A3               [24] 3202 	inc	dptr
      000FEB F0               [24] 3203 	movx	@dptr,a
      000FEC 90 03 B6         [24] 3204 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_4
      000FEF EA               [12] 3205 	mov	a,r2
      000FF0 F0               [24] 3206 	movx	@dptr,a
      000FF1 E9               [12] 3207 	mov	a,r1
      000FF2 A3               [24] 3208 	inc	dptr
      000FF3 F0               [24] 3209 	movx	@dptr,a
      000FF4 EB               [12] 3210 	mov	a,r3
      000FF5 A3               [24] 3211 	inc	dptr
      000FF6 F0               [24] 3212 	movx	@dptr,a
      000FF7 90 03 B9         [24] 3213 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_5
      000FFA F0               [24] 3214 	movx	@dptr,a
      000FFB 90 03 BA         [24] 3215 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_6
      000FFE 74 85            [12] 3216 	mov	a,#_main_UBLOX_Rx_Length_1_382
      001000 F0               [24] 3217 	movx	@dptr,a
      001001 74 00            [12] 3218 	mov	a,#(_main_UBLOX_Rx_Length_1_382 >> 8)
      001003 A3               [24] 3219 	inc	dptr
      001004 F0               [24] 3220 	movx	@dptr,a
      001005 E4               [12] 3221 	clr	a
      001006 A3               [24] 3222 	inc	dptr
      001007 F0               [24] 3223 	movx	@dptr,a
      001008 75 82 01         [24] 3224 	mov	dpl,#0x01
      00100B C0 05            [24] 3225 	push	ar5
      00100D C0 04            [24] 3226 	push	ar4
      00100F 12 57 13         [24] 3227 	lcall	_UBLOX_GPS_SendCommand_WaitACK
      001012 D0 04            [24] 3228 	pop	ar4
      001014 D0 05            [24] 3229 	pop	ar5
                                   3230 ;	..\src\atrs\main.c:366: free(UBLOX_Rx_Payload);
      001016 7B 00            [12] 3231 	mov	r3,#0x00
      001018 8C 82            [24] 3232 	mov	dpl,r4
      00101A 8D 83            [24] 3233 	mov	dph,r5
      00101C 8B F0            [24] 3234 	mov	b,r3
      00101E 12 5B 4D         [24] 3235 	lcall	_free
      001021 D0 06            [24] 3236 	pop	ar6
      001023 D0 07            [24] 3237 	pop	ar7
      001025                       3238 00139$:
                                   3239 ;	..\src\atrs\main.c:371: if(BeaconRx.FlagReceived == 0x01)
      001025 90 00 0F         [24] 3240 	mov	dptr,#_BeaconRx
      001028 E0               [24] 3241 	movx	a,@dptr
      001029 FD               [12] 3242 	mov	r5,a
      00102A BD 01 31         [24] 3243 	cjne	r5,#0x01,00141$
                                   3244 ;	..\src\atrs\main.c:373: dbglink_writestr(" 0x");
      00102D 90 7B 53         [24] 3245 	mov	dptr,#___str_3
      001030 75 F0 80         [24] 3246 	mov	b,#0x80
      001033 C0 07            [24] 3247 	push	ar7
      001035 C0 06            [24] 3248 	push	ar6
      001037 12 6E FA         [24] 3249 	lcall	_dbglink_writestr
                                   3250 ;	..\src\atrs\main.c:374: dbglink_writehex16(BeaconRx.SatId,1,WRNUM_PADZERO);
      00103A 90 00 10         [24] 3251 	mov	dptr,#(_BeaconRx + 0x0001)
      00103D E0               [24] 3252 	movx	a,@dptr
      00103E FC               [12] 3253 	mov	r4,a
      00103F A3               [24] 3254 	inc	dptr
      001040 E0               [24] 3255 	movx	a,@dptr
      001041 FD               [12] 3256 	mov	r5,a
      001042 74 08            [12] 3257 	mov	a,#0x08
      001044 C0 E0            [24] 3258 	push	acc
      001046 74 01            [12] 3259 	mov	a,#0x01
      001048 C0 E0            [24] 3260 	push	acc
      00104A 8C 82            [24] 3261 	mov	dpl,r4
      00104C 8D 83            [24] 3262 	mov	dph,r5
      00104E 12 74 DE         [24] 3263 	lcall	_dbglink_writehex16
      001051 15 81            [12] 3264 	dec	sp
      001053 15 81            [12] 3265 	dec	sp
      001055 D0 06            [24] 3266 	pop	ar6
      001057 D0 07            [24] 3267 	pop	ar7
                                   3268 ;	..\src\atrs\main.c:375: BeaconRx.FlagReceived = 0x00;
      001059 90 00 0F         [24] 3269 	mov	dptr,#_BeaconRx
      00105C E4               [12] 3270 	clr	a
      00105D F0               [24] 3271 	movx	@dptr,a
      00105E                       3272 00141$:
                                   3273 ;	..\src\atrs\main.c:432: wtimer_runcallbacks();
      00105E C0 07            [24] 3274 	push	ar7
      001060 C0 06            [24] 3275 	push	ar6
      001062 12 64 92         [24] 3276 	lcall	_wtimer_runcallbacks
                                   3277 ;	..\src\atrs\main.c:433: wtimer_idle(WTFLAG_CANSTANDBY);
      001065 75 82 02         [24] 3278 	mov	dpl,#0x02
      001068 12 65 13         [24] 3279 	lcall	_wtimer_idle
      00106B D0 06            [24] 3280 	pop	ar6
      00106D D0 07            [24] 3281 	pop	ar7
      00106F 02 0E B6         [24] 3282 	ljmp	00146$
                                   3283 	.area CSEG    (CODE)
                                   3284 	.area CONST   (CODE)
      007B2A                       3285 ___str_0:
      007B2A 63 6F 6D 61 6E 64 6F  3286 	.ascii "comando Rx: "
             20 52 78 3A 20
      007B36 00                    3287 	.db 0x00
      007B37                       3288 ___str_1:
      007B37 0A                    3289 	.db 0x0a
      007B38 20 4C 6F 6E 67 69 74  3290 	.ascii " Longitud Rx: "
             75 64 20 52 78 3A 20
      007B46 00                    3291 	.db 0x00
      007B47                       3292 ___str_2:
      007B47 0A                    3293 	.db 0x0a
      007B48 20 70 61 79 6C 6F 61  3294 	.ascii " payload: "
             64 3A 20
      007B52 00                    3295 	.db 0x00
      007B53                       3296 ___str_3:
      007B53 20 30 78              3297 	.ascii " 0x"
      007B56 00                    3298 	.db 0x00
                                   3299 	.area XINIT   (CODE)
      008192                       3300 __xinit__RfStateMachine:
      008192 01                    3301 	.db #0x01	; 1
      008193                       3302 __xinit__Tmr0Flg:
      008193 00                    3303 	.db #0x00	; 0
      008194                       3304 __xinit__counter:
      008194 00                    3305 	.db #0x00	; 0
                                   3306 	.area CABS    (ABS,CODE)
