                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.6.0 #9615 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module easyax5043
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _ax5043_init_registers_rx
                                     12 	.globl _ax5043_init_registers_tx
                                     13 	.globl _BEACON_decoding
                                     14 	.globl _dbglink_writenum16
                                     15 	.globl _dbglink_writestr
                                     16 	.globl _dbglink_tx
                                     17 	.globl _memset
                                     18 	.globl _memcpy
                                     19 	.globl _wtimer_remove_callback
                                     20 	.globl _wtimer_add_callback
                                     21 	.globl _wtimer_remove
                                     22 	.globl _wtimer1_addrelative
                                     23 	.globl _wtimer0_addrelative
                                     24 	.globl _wtimer0_addabsolute
                                     25 	.globl _wtimer0_curtime
                                     26 	.globl _wtimer_runcallbacks
                                     27 	.globl _wtimer_idle
                                     28 	.globl _ax5043_writefifo
                                     29 	.globl _ax5043_readfifo
                                     30 	.globl _ax5043_wakeup_deepsleep
                                     31 	.globl _ax5043_enter_deepsleep
                                     32 	.globl _ax5043_reset
                                     33 	.globl _radio_read24
                                     34 	.globl _radio_read16
                                     35 	.globl _pn9_buffer
                                     36 	.globl _pn9_advance_byte
                                     37 	.globl _pn9_advance_bits
                                     38 	.globl _axradio_framing_append_crc
                                     39 	.globl _axradio_framing_check_crc
                                     40 	.globl _ax5043_set_registers_rxcont_singleparamset
                                     41 	.globl _ax5043_set_registers_rxcont
                                     42 	.globl _ax5043_set_registers_rxwor
                                     43 	.globl _ax5043_set_registers_rx
                                     44 	.globl _ax5043_set_registers_tx
                                     45 	.globl _ax5043_set_registers
                                     46 	.globl _axradio_conv_freq_fromreg
                                     47 	.globl _axradio_statuschange
                                     48 	.globl _axradio_conv_timeinterval_totimer0
                                     49 	.globl _checksignedlimit32
                                     50 	.globl _checksignedlimit16
                                     51 	.globl _signedlimit16
                                     52 	.globl _signextend24
                                     53 	.globl _signextend20
                                     54 	.globl _signextend16
                                     55 	.globl _PORTC_7
                                     56 	.globl _PORTC_6
                                     57 	.globl _PORTC_5
                                     58 	.globl _PORTC_4
                                     59 	.globl _PORTC_3
                                     60 	.globl _PORTC_2
                                     61 	.globl _PORTC_1
                                     62 	.globl _PORTC_0
                                     63 	.globl _PORTB_7
                                     64 	.globl _PORTB_6
                                     65 	.globl _PORTB_5
                                     66 	.globl _PORTB_4
                                     67 	.globl _PORTB_3
                                     68 	.globl _PORTB_2
                                     69 	.globl _PORTB_1
                                     70 	.globl _PORTB_0
                                     71 	.globl _PORTA_7
                                     72 	.globl _PORTA_6
                                     73 	.globl _PORTA_5
                                     74 	.globl _PORTA_4
                                     75 	.globl _PORTA_3
                                     76 	.globl _PORTA_2
                                     77 	.globl _PORTA_1
                                     78 	.globl _PORTA_0
                                     79 	.globl _PINC_7
                                     80 	.globl _PINC_6
                                     81 	.globl _PINC_5
                                     82 	.globl _PINC_4
                                     83 	.globl _PINC_3
                                     84 	.globl _PINC_2
                                     85 	.globl _PINC_1
                                     86 	.globl _PINC_0
                                     87 	.globl _PINB_7
                                     88 	.globl _PINB_6
                                     89 	.globl _PINB_5
                                     90 	.globl _PINB_4
                                     91 	.globl _PINB_3
                                     92 	.globl _PINB_2
                                     93 	.globl _PINB_1
                                     94 	.globl _PINB_0
                                     95 	.globl _PINA_7
                                     96 	.globl _PINA_6
                                     97 	.globl _PINA_5
                                     98 	.globl _PINA_4
                                     99 	.globl _PINA_3
                                    100 	.globl _PINA_2
                                    101 	.globl _PINA_1
                                    102 	.globl _PINA_0
                                    103 	.globl _CY
                                    104 	.globl _AC
                                    105 	.globl _F0
                                    106 	.globl _RS1
                                    107 	.globl _RS0
                                    108 	.globl _OV
                                    109 	.globl _F1
                                    110 	.globl _P
                                    111 	.globl _IP_7
                                    112 	.globl _IP_6
                                    113 	.globl _IP_5
                                    114 	.globl _IP_4
                                    115 	.globl _IP_3
                                    116 	.globl _IP_2
                                    117 	.globl _IP_1
                                    118 	.globl _IP_0
                                    119 	.globl _EA
                                    120 	.globl _IE_7
                                    121 	.globl _IE_6
                                    122 	.globl _IE_5
                                    123 	.globl _IE_4
                                    124 	.globl _IE_3
                                    125 	.globl _IE_2
                                    126 	.globl _IE_1
                                    127 	.globl _IE_0
                                    128 	.globl _EIP_7
                                    129 	.globl _EIP_6
                                    130 	.globl _EIP_5
                                    131 	.globl _EIP_4
                                    132 	.globl _EIP_3
                                    133 	.globl _EIP_2
                                    134 	.globl _EIP_1
                                    135 	.globl _EIP_0
                                    136 	.globl _EIE_7
                                    137 	.globl _EIE_6
                                    138 	.globl _EIE_5
                                    139 	.globl _EIE_4
                                    140 	.globl _EIE_3
                                    141 	.globl _EIE_2
                                    142 	.globl _EIE_1
                                    143 	.globl _EIE_0
                                    144 	.globl _E2IP_7
                                    145 	.globl _E2IP_6
                                    146 	.globl _E2IP_5
                                    147 	.globl _E2IP_4
                                    148 	.globl _E2IP_3
                                    149 	.globl _E2IP_2
                                    150 	.globl _E2IP_1
                                    151 	.globl _E2IP_0
                                    152 	.globl _E2IE_7
                                    153 	.globl _E2IE_6
                                    154 	.globl _E2IE_5
                                    155 	.globl _E2IE_4
                                    156 	.globl _E2IE_3
                                    157 	.globl _E2IE_2
                                    158 	.globl _E2IE_1
                                    159 	.globl _E2IE_0
                                    160 	.globl _B_7
                                    161 	.globl _B_6
                                    162 	.globl _B_5
                                    163 	.globl _B_4
                                    164 	.globl _B_3
                                    165 	.globl _B_2
                                    166 	.globl _B_1
                                    167 	.globl _B_0
                                    168 	.globl _ACC_7
                                    169 	.globl _ACC_6
                                    170 	.globl _ACC_5
                                    171 	.globl _ACC_4
                                    172 	.globl _ACC_3
                                    173 	.globl _ACC_2
                                    174 	.globl _ACC_1
                                    175 	.globl _ACC_0
                                    176 	.globl _WTSTAT
                                    177 	.globl _WTIRQEN
                                    178 	.globl _WTEVTD
                                    179 	.globl _WTEVTD1
                                    180 	.globl _WTEVTD0
                                    181 	.globl _WTEVTC
                                    182 	.globl _WTEVTC1
                                    183 	.globl _WTEVTC0
                                    184 	.globl _WTEVTB
                                    185 	.globl _WTEVTB1
                                    186 	.globl _WTEVTB0
                                    187 	.globl _WTEVTA
                                    188 	.globl _WTEVTA1
                                    189 	.globl _WTEVTA0
                                    190 	.globl _WTCNTR1
                                    191 	.globl _WTCNTB
                                    192 	.globl _WTCNTB1
                                    193 	.globl _WTCNTB0
                                    194 	.globl _WTCNTA
                                    195 	.globl _WTCNTA1
                                    196 	.globl _WTCNTA0
                                    197 	.globl _WTCFGB
                                    198 	.globl _WTCFGA
                                    199 	.globl _WDTRESET
                                    200 	.globl _WDTCFG
                                    201 	.globl _U1STATUS
                                    202 	.globl _U1SHREG
                                    203 	.globl _U1MODE
                                    204 	.globl _U1CTRL
                                    205 	.globl _U0STATUS
                                    206 	.globl _U0SHREG
                                    207 	.globl _U0MODE
                                    208 	.globl _U0CTRL
                                    209 	.globl _T2STATUS
                                    210 	.globl _T2PERIOD
                                    211 	.globl _T2PERIOD1
                                    212 	.globl _T2PERIOD0
                                    213 	.globl _T2MODE
                                    214 	.globl _T2CNT
                                    215 	.globl _T2CNT1
                                    216 	.globl _T2CNT0
                                    217 	.globl _T2CLKSRC
                                    218 	.globl _T1STATUS
                                    219 	.globl _T1PERIOD
                                    220 	.globl _T1PERIOD1
                                    221 	.globl _T1PERIOD0
                                    222 	.globl _T1MODE
                                    223 	.globl _T1CNT
                                    224 	.globl _T1CNT1
                                    225 	.globl _T1CNT0
                                    226 	.globl _T1CLKSRC
                                    227 	.globl _T0STATUS
                                    228 	.globl _T0PERIOD
                                    229 	.globl _T0PERIOD1
                                    230 	.globl _T0PERIOD0
                                    231 	.globl _T0MODE
                                    232 	.globl _T0CNT
                                    233 	.globl _T0CNT1
                                    234 	.globl _T0CNT0
                                    235 	.globl _T0CLKSRC
                                    236 	.globl _SPSTATUS
                                    237 	.globl _SPSHREG
                                    238 	.globl _SPMODE
                                    239 	.globl _SPCLKSRC
                                    240 	.globl _RADIOSTAT
                                    241 	.globl _RADIOSTAT1
                                    242 	.globl _RADIOSTAT0
                                    243 	.globl _RADIODATA
                                    244 	.globl _RADIODATA3
                                    245 	.globl _RADIODATA2
                                    246 	.globl _RADIODATA1
                                    247 	.globl _RADIODATA0
                                    248 	.globl _RADIOADDR
                                    249 	.globl _RADIOADDR1
                                    250 	.globl _RADIOADDR0
                                    251 	.globl _RADIOACC
                                    252 	.globl _OC1STATUS
                                    253 	.globl _OC1PIN
                                    254 	.globl _OC1MODE
                                    255 	.globl _OC1COMP
                                    256 	.globl _OC1COMP1
                                    257 	.globl _OC1COMP0
                                    258 	.globl _OC0STATUS
                                    259 	.globl _OC0PIN
                                    260 	.globl _OC0MODE
                                    261 	.globl _OC0COMP
                                    262 	.globl _OC0COMP1
                                    263 	.globl _OC0COMP0
                                    264 	.globl _NVSTATUS
                                    265 	.globl _NVKEY
                                    266 	.globl _NVDATA
                                    267 	.globl _NVDATA1
                                    268 	.globl _NVDATA0
                                    269 	.globl _NVADDR
                                    270 	.globl _NVADDR1
                                    271 	.globl _NVADDR0
                                    272 	.globl _IC1STATUS
                                    273 	.globl _IC1MODE
                                    274 	.globl _IC1CAPT
                                    275 	.globl _IC1CAPT1
                                    276 	.globl _IC1CAPT0
                                    277 	.globl _IC0STATUS
                                    278 	.globl _IC0MODE
                                    279 	.globl _IC0CAPT
                                    280 	.globl _IC0CAPT1
                                    281 	.globl _IC0CAPT0
                                    282 	.globl _PORTR
                                    283 	.globl _PORTC
                                    284 	.globl _PORTB
                                    285 	.globl _PORTA
                                    286 	.globl _PINR
                                    287 	.globl _PINC
                                    288 	.globl _PINB
                                    289 	.globl _PINA
                                    290 	.globl _DIRR
                                    291 	.globl _DIRC
                                    292 	.globl _DIRB
                                    293 	.globl _DIRA
                                    294 	.globl _DBGLNKSTAT
                                    295 	.globl _DBGLNKBUF
                                    296 	.globl _CODECONFIG
                                    297 	.globl _CLKSTAT
                                    298 	.globl _CLKCON
                                    299 	.globl _ANALOGCOMP
                                    300 	.globl _ADCCONV
                                    301 	.globl _ADCCLKSRC
                                    302 	.globl _ADCCH3CONFIG
                                    303 	.globl _ADCCH2CONFIG
                                    304 	.globl _ADCCH1CONFIG
                                    305 	.globl _ADCCH0CONFIG
                                    306 	.globl __XPAGE
                                    307 	.globl _XPAGE
                                    308 	.globl _SP
                                    309 	.globl _PSW
                                    310 	.globl _PCON
                                    311 	.globl _IP
                                    312 	.globl _IE
                                    313 	.globl _EIP
                                    314 	.globl _EIE
                                    315 	.globl _E2IP
                                    316 	.globl _E2IE
                                    317 	.globl _DPS
                                    318 	.globl _DPTR1
                                    319 	.globl _DPTR0
                                    320 	.globl _DPL1
                                    321 	.globl _DPL
                                    322 	.globl _DPH1
                                    323 	.globl _DPH
                                    324 	.globl _B
                                    325 	.globl _ACC
                                    326 	.globl _f33_saved
                                    327 	.globl _f32_saved
                                    328 	.globl _f31_saved
                                    329 	.globl _f30_saved
                                    330 	.globl _axradio_transmit_PARM_3
                                    331 	.globl _axradio_transmit_PARM_2
                                    332 	.globl _axradio_timer
                                    333 	.globl _axradio_cb_transmitdata
                                    334 	.globl _axradio_cb_transmitend
                                    335 	.globl _axradio_cb_transmitstart
                                    336 	.globl _axradio_cb_channelstate
                                    337 	.globl _axradio_cb_receivesfd
                                    338 	.globl _axradio_cb_receive
                                    339 	.globl _axradio_rxbuffer
                                    340 	.globl _axradio_txbuffer
                                    341 	.globl _axradio_default_remoteaddr
                                    342 	.globl _axradio_localaddr
                                    343 	.globl _axradio_timeanchor
                                    344 	.globl _axradio_sync_periodcorr
                                    345 	.globl _axradio_sync_time
                                    346 	.globl _axradio_ack_seqnr
                                    347 	.globl _axradio_ack_count
                                    348 	.globl _axradio_curfreqoffset
                                    349 	.globl _axradio_curchannel
                                    350 	.globl _axradio_txbuffer_cnt
                                    351 	.globl _axradio_txbuffer_len
                                    352 	.globl _axradio_syncstate
                                    353 	.globl _aligned_alloc_PARM_2
                                    354 	.globl _AX5043_XTALAMPL
                                    355 	.globl _AX5043_XTALOSC
                                    356 	.globl _AX5043_MODCFGP
                                    357 	.globl _AX5043_POWCTRL1
                                    358 	.globl _AX5043_REF
                                    359 	.globl _AX5043_0xF44
                                    360 	.globl _AX5043_0xF35
                                    361 	.globl _AX5043_0xF34
                                    362 	.globl _AX5043_0xF33
                                    363 	.globl _AX5043_0xF32
                                    364 	.globl _AX5043_0xF31
                                    365 	.globl _AX5043_0xF30
                                    366 	.globl _AX5043_0xF26
                                    367 	.globl _AX5043_0xF23
                                    368 	.globl _AX5043_0xF22
                                    369 	.globl _AX5043_0xF21
                                    370 	.globl _AX5043_0xF1C
                                    371 	.globl _AX5043_0xF18
                                    372 	.globl _AX5043_0xF11
                                    373 	.globl _AX5043_0xF10
                                    374 	.globl _AX5043_0xF0C
                                    375 	.globl _AX5043_0xF00
                                    376 	.globl _AX5043_TIMEGAIN3NB
                                    377 	.globl _AX5043_TIMEGAIN2NB
                                    378 	.globl _AX5043_TIMEGAIN1NB
                                    379 	.globl _AX5043_TIMEGAIN0NB
                                    380 	.globl _AX5043_RXPARAMSETSNB
                                    381 	.globl _AX5043_RXPARAMCURSETNB
                                    382 	.globl _AX5043_PKTMAXLENNB
                                    383 	.globl _AX5043_PKTLENOFFSETNB
                                    384 	.globl _AX5043_PKTLENCFGNB
                                    385 	.globl _AX5043_PKTADDRMASK3NB
                                    386 	.globl _AX5043_PKTADDRMASK2NB
                                    387 	.globl _AX5043_PKTADDRMASK1NB
                                    388 	.globl _AX5043_PKTADDRMASK0NB
                                    389 	.globl _AX5043_PKTADDRCFGNB
                                    390 	.globl _AX5043_PKTADDR3NB
                                    391 	.globl _AX5043_PKTADDR2NB
                                    392 	.globl _AX5043_PKTADDR1NB
                                    393 	.globl _AX5043_PKTADDR0NB
                                    394 	.globl _AX5043_PHASEGAIN3NB
                                    395 	.globl _AX5043_PHASEGAIN2NB
                                    396 	.globl _AX5043_PHASEGAIN1NB
                                    397 	.globl _AX5043_PHASEGAIN0NB
                                    398 	.globl _AX5043_FREQUENCYLEAKNB
                                    399 	.globl _AX5043_FREQUENCYGAIND3NB
                                    400 	.globl _AX5043_FREQUENCYGAIND2NB
                                    401 	.globl _AX5043_FREQUENCYGAIND1NB
                                    402 	.globl _AX5043_FREQUENCYGAIND0NB
                                    403 	.globl _AX5043_FREQUENCYGAINC3NB
                                    404 	.globl _AX5043_FREQUENCYGAINC2NB
                                    405 	.globl _AX5043_FREQUENCYGAINC1NB
                                    406 	.globl _AX5043_FREQUENCYGAINC0NB
                                    407 	.globl _AX5043_FREQUENCYGAINB3NB
                                    408 	.globl _AX5043_FREQUENCYGAINB2NB
                                    409 	.globl _AX5043_FREQUENCYGAINB1NB
                                    410 	.globl _AX5043_FREQUENCYGAINB0NB
                                    411 	.globl _AX5043_FREQUENCYGAINA3NB
                                    412 	.globl _AX5043_FREQUENCYGAINA2NB
                                    413 	.globl _AX5043_FREQUENCYGAINA1NB
                                    414 	.globl _AX5043_FREQUENCYGAINA0NB
                                    415 	.globl _AX5043_FREQDEV13NB
                                    416 	.globl _AX5043_FREQDEV12NB
                                    417 	.globl _AX5043_FREQDEV11NB
                                    418 	.globl _AX5043_FREQDEV10NB
                                    419 	.globl _AX5043_FREQDEV03NB
                                    420 	.globl _AX5043_FREQDEV02NB
                                    421 	.globl _AX5043_FREQDEV01NB
                                    422 	.globl _AX5043_FREQDEV00NB
                                    423 	.globl _AX5043_FOURFSK3NB
                                    424 	.globl _AX5043_FOURFSK2NB
                                    425 	.globl _AX5043_FOURFSK1NB
                                    426 	.globl _AX5043_FOURFSK0NB
                                    427 	.globl _AX5043_DRGAIN3NB
                                    428 	.globl _AX5043_DRGAIN2NB
                                    429 	.globl _AX5043_DRGAIN1NB
                                    430 	.globl _AX5043_DRGAIN0NB
                                    431 	.globl _AX5043_BBOFFSRES3NB
                                    432 	.globl _AX5043_BBOFFSRES2NB
                                    433 	.globl _AX5043_BBOFFSRES1NB
                                    434 	.globl _AX5043_BBOFFSRES0NB
                                    435 	.globl _AX5043_AMPLITUDEGAIN3NB
                                    436 	.globl _AX5043_AMPLITUDEGAIN2NB
                                    437 	.globl _AX5043_AMPLITUDEGAIN1NB
                                    438 	.globl _AX5043_AMPLITUDEGAIN0NB
                                    439 	.globl _AX5043_AGCTARGET3NB
                                    440 	.globl _AX5043_AGCTARGET2NB
                                    441 	.globl _AX5043_AGCTARGET1NB
                                    442 	.globl _AX5043_AGCTARGET0NB
                                    443 	.globl _AX5043_AGCMINMAX3NB
                                    444 	.globl _AX5043_AGCMINMAX2NB
                                    445 	.globl _AX5043_AGCMINMAX1NB
                                    446 	.globl _AX5043_AGCMINMAX0NB
                                    447 	.globl _AX5043_AGCGAIN3NB
                                    448 	.globl _AX5043_AGCGAIN2NB
                                    449 	.globl _AX5043_AGCGAIN1NB
                                    450 	.globl _AX5043_AGCGAIN0NB
                                    451 	.globl _AX5043_AGCAHYST3NB
                                    452 	.globl _AX5043_AGCAHYST2NB
                                    453 	.globl _AX5043_AGCAHYST1NB
                                    454 	.globl _AX5043_AGCAHYST0NB
                                    455 	.globl _AX5043_0xF44NB
                                    456 	.globl _AX5043_0xF35NB
                                    457 	.globl _AX5043_0xF34NB
                                    458 	.globl _AX5043_0xF33NB
                                    459 	.globl _AX5043_0xF32NB
                                    460 	.globl _AX5043_0xF31NB
                                    461 	.globl _AX5043_0xF30NB
                                    462 	.globl _AX5043_0xF26NB
                                    463 	.globl _AX5043_0xF23NB
                                    464 	.globl _AX5043_0xF22NB
                                    465 	.globl _AX5043_0xF21NB
                                    466 	.globl _AX5043_0xF1CNB
                                    467 	.globl _AX5043_0xF18NB
                                    468 	.globl _AX5043_0xF0CNB
                                    469 	.globl _AX5043_0xF00NB
                                    470 	.globl _AX5043_XTALSTATUSNB
                                    471 	.globl _AX5043_XTALOSCNB
                                    472 	.globl _AX5043_XTALCAPNB
                                    473 	.globl _AX5043_XTALAMPLNB
                                    474 	.globl _AX5043_WAKEUPXOEARLYNB
                                    475 	.globl _AX5043_WAKEUPTIMER1NB
                                    476 	.globl _AX5043_WAKEUPTIMER0NB
                                    477 	.globl _AX5043_WAKEUPFREQ1NB
                                    478 	.globl _AX5043_WAKEUPFREQ0NB
                                    479 	.globl _AX5043_WAKEUP1NB
                                    480 	.globl _AX5043_WAKEUP0NB
                                    481 	.globl _AX5043_TXRATE2NB
                                    482 	.globl _AX5043_TXRATE1NB
                                    483 	.globl _AX5043_TXRATE0NB
                                    484 	.globl _AX5043_TXPWRCOEFFE1NB
                                    485 	.globl _AX5043_TXPWRCOEFFE0NB
                                    486 	.globl _AX5043_TXPWRCOEFFD1NB
                                    487 	.globl _AX5043_TXPWRCOEFFD0NB
                                    488 	.globl _AX5043_TXPWRCOEFFC1NB
                                    489 	.globl _AX5043_TXPWRCOEFFC0NB
                                    490 	.globl _AX5043_TXPWRCOEFFB1NB
                                    491 	.globl _AX5043_TXPWRCOEFFB0NB
                                    492 	.globl _AX5043_TXPWRCOEFFA1NB
                                    493 	.globl _AX5043_TXPWRCOEFFA0NB
                                    494 	.globl _AX5043_TRKRFFREQ2NB
                                    495 	.globl _AX5043_TRKRFFREQ1NB
                                    496 	.globl _AX5043_TRKRFFREQ0NB
                                    497 	.globl _AX5043_TRKPHASE1NB
                                    498 	.globl _AX5043_TRKPHASE0NB
                                    499 	.globl _AX5043_TRKFSKDEMOD1NB
                                    500 	.globl _AX5043_TRKFSKDEMOD0NB
                                    501 	.globl _AX5043_TRKFREQ1NB
                                    502 	.globl _AX5043_TRKFREQ0NB
                                    503 	.globl _AX5043_TRKDATARATE2NB
                                    504 	.globl _AX5043_TRKDATARATE1NB
                                    505 	.globl _AX5043_TRKDATARATE0NB
                                    506 	.globl _AX5043_TRKAMPLITUDE1NB
                                    507 	.globl _AX5043_TRKAMPLITUDE0NB
                                    508 	.globl _AX5043_TRKAFSKDEMOD1NB
                                    509 	.globl _AX5043_TRKAFSKDEMOD0NB
                                    510 	.globl _AX5043_TMGTXSETTLENB
                                    511 	.globl _AX5043_TMGTXBOOSTNB
                                    512 	.globl _AX5043_TMGRXSETTLENB
                                    513 	.globl _AX5043_TMGRXRSSINB
                                    514 	.globl _AX5043_TMGRXPREAMBLE3NB
                                    515 	.globl _AX5043_TMGRXPREAMBLE2NB
                                    516 	.globl _AX5043_TMGRXPREAMBLE1NB
                                    517 	.globl _AX5043_TMGRXOFFSACQNB
                                    518 	.globl _AX5043_TMGRXCOARSEAGCNB
                                    519 	.globl _AX5043_TMGRXBOOSTNB
                                    520 	.globl _AX5043_TMGRXAGCNB
                                    521 	.globl _AX5043_TIMER2NB
                                    522 	.globl _AX5043_TIMER1NB
                                    523 	.globl _AX5043_TIMER0NB
                                    524 	.globl _AX5043_SILICONREVISIONNB
                                    525 	.globl _AX5043_SCRATCHNB
                                    526 	.globl _AX5043_RXDATARATE2NB
                                    527 	.globl _AX5043_RXDATARATE1NB
                                    528 	.globl _AX5043_RXDATARATE0NB
                                    529 	.globl _AX5043_RSSIREFERENCENB
                                    530 	.globl _AX5043_RSSIABSTHRNB
                                    531 	.globl _AX5043_RSSINB
                                    532 	.globl _AX5043_REFNB
                                    533 	.globl _AX5043_RADIOSTATENB
                                    534 	.globl _AX5043_RADIOEVENTREQ1NB
                                    535 	.globl _AX5043_RADIOEVENTREQ0NB
                                    536 	.globl _AX5043_RADIOEVENTMASK1NB
                                    537 	.globl _AX5043_RADIOEVENTMASK0NB
                                    538 	.globl _AX5043_PWRMODENB
                                    539 	.globl _AX5043_PWRAMPNB
                                    540 	.globl _AX5043_POWSTICKYSTATNB
                                    541 	.globl _AX5043_POWSTATNB
                                    542 	.globl _AX5043_POWIRQMASKNB
                                    543 	.globl _AX5043_POWCTRL1NB
                                    544 	.globl _AX5043_PLLVCOIRNB
                                    545 	.globl _AX5043_PLLVCOINB
                                    546 	.globl _AX5043_PLLVCODIVNB
                                    547 	.globl _AX5043_PLLRNGCLKNB
                                    548 	.globl _AX5043_PLLRANGINGBNB
                                    549 	.globl _AX5043_PLLRANGINGANB
                                    550 	.globl _AX5043_PLLLOOPBOOSTNB
                                    551 	.globl _AX5043_PLLLOOPNB
                                    552 	.globl _AX5043_PLLLOCKDETNB
                                    553 	.globl _AX5043_PLLCPIBOOSTNB
                                    554 	.globl _AX5043_PLLCPINB
                                    555 	.globl _AX5043_PKTSTOREFLAGSNB
                                    556 	.globl _AX5043_PKTMISCFLAGSNB
                                    557 	.globl _AX5043_PKTCHUNKSIZENB
                                    558 	.globl _AX5043_PKTACCEPTFLAGSNB
                                    559 	.globl _AX5043_PINSTATENB
                                    560 	.globl _AX5043_PINFUNCSYSCLKNB
                                    561 	.globl _AX5043_PINFUNCPWRAMPNB
                                    562 	.globl _AX5043_PINFUNCIRQNB
                                    563 	.globl _AX5043_PINFUNCDCLKNB
                                    564 	.globl _AX5043_PINFUNCDATANB
                                    565 	.globl _AX5043_PINFUNCANTSELNB
                                    566 	.globl _AX5043_MODULATIONNB
                                    567 	.globl _AX5043_MODCFGPNB
                                    568 	.globl _AX5043_MODCFGFNB
                                    569 	.globl _AX5043_MODCFGANB
                                    570 	.globl _AX5043_MAXRFOFFSET2NB
                                    571 	.globl _AX5043_MAXRFOFFSET1NB
                                    572 	.globl _AX5043_MAXRFOFFSET0NB
                                    573 	.globl _AX5043_MAXDROFFSET2NB
                                    574 	.globl _AX5043_MAXDROFFSET1NB
                                    575 	.globl _AX5043_MAXDROFFSET0NB
                                    576 	.globl _AX5043_MATCH1PAT1NB
                                    577 	.globl _AX5043_MATCH1PAT0NB
                                    578 	.globl _AX5043_MATCH1MINNB
                                    579 	.globl _AX5043_MATCH1MAXNB
                                    580 	.globl _AX5043_MATCH1LENNB
                                    581 	.globl _AX5043_MATCH0PAT3NB
                                    582 	.globl _AX5043_MATCH0PAT2NB
                                    583 	.globl _AX5043_MATCH0PAT1NB
                                    584 	.globl _AX5043_MATCH0PAT0NB
                                    585 	.globl _AX5043_MATCH0MINNB
                                    586 	.globl _AX5043_MATCH0MAXNB
                                    587 	.globl _AX5043_MATCH0LENNB
                                    588 	.globl _AX5043_LPOSCSTATUSNB
                                    589 	.globl _AX5043_LPOSCREF1NB
                                    590 	.globl _AX5043_LPOSCREF0NB
                                    591 	.globl _AX5043_LPOSCPER1NB
                                    592 	.globl _AX5043_LPOSCPER0NB
                                    593 	.globl _AX5043_LPOSCKFILT1NB
                                    594 	.globl _AX5043_LPOSCKFILT0NB
                                    595 	.globl _AX5043_LPOSCFREQ1NB
                                    596 	.globl _AX5043_LPOSCFREQ0NB
                                    597 	.globl _AX5043_LPOSCCONFIGNB
                                    598 	.globl _AX5043_IRQREQUEST1NB
                                    599 	.globl _AX5043_IRQREQUEST0NB
                                    600 	.globl _AX5043_IRQMASK1NB
                                    601 	.globl _AX5043_IRQMASK0NB
                                    602 	.globl _AX5043_IRQINVERSION1NB
                                    603 	.globl _AX5043_IRQINVERSION0NB
                                    604 	.globl _AX5043_IFFREQ1NB
                                    605 	.globl _AX5043_IFFREQ0NB
                                    606 	.globl _AX5043_GPADCPERIODNB
                                    607 	.globl _AX5043_GPADCCTRLNB
                                    608 	.globl _AX5043_GPADC13VALUE1NB
                                    609 	.globl _AX5043_GPADC13VALUE0NB
                                    610 	.globl _AX5043_FSKDMIN1NB
                                    611 	.globl _AX5043_FSKDMIN0NB
                                    612 	.globl _AX5043_FSKDMAX1NB
                                    613 	.globl _AX5043_FSKDMAX0NB
                                    614 	.globl _AX5043_FSKDEV2NB
                                    615 	.globl _AX5043_FSKDEV1NB
                                    616 	.globl _AX5043_FSKDEV0NB
                                    617 	.globl _AX5043_FREQB3NB
                                    618 	.globl _AX5043_FREQB2NB
                                    619 	.globl _AX5043_FREQB1NB
                                    620 	.globl _AX5043_FREQB0NB
                                    621 	.globl _AX5043_FREQA3NB
                                    622 	.globl _AX5043_FREQA2NB
                                    623 	.globl _AX5043_FREQA1NB
                                    624 	.globl _AX5043_FREQA0NB
                                    625 	.globl _AX5043_FRAMINGNB
                                    626 	.globl _AX5043_FIFOTHRESH1NB
                                    627 	.globl _AX5043_FIFOTHRESH0NB
                                    628 	.globl _AX5043_FIFOSTATNB
                                    629 	.globl _AX5043_FIFOFREE1NB
                                    630 	.globl _AX5043_FIFOFREE0NB
                                    631 	.globl _AX5043_FIFODATANB
                                    632 	.globl _AX5043_FIFOCOUNT1NB
                                    633 	.globl _AX5043_FIFOCOUNT0NB
                                    634 	.globl _AX5043_FECSYNCNB
                                    635 	.globl _AX5043_FECSTATUSNB
                                    636 	.globl _AX5043_FECNB
                                    637 	.globl _AX5043_ENCODINGNB
                                    638 	.globl _AX5043_DIVERSITYNB
                                    639 	.globl _AX5043_DECIMATIONNB
                                    640 	.globl _AX5043_DACVALUE1NB
                                    641 	.globl _AX5043_DACVALUE0NB
                                    642 	.globl _AX5043_DACCONFIGNB
                                    643 	.globl _AX5043_CRCINIT3NB
                                    644 	.globl _AX5043_CRCINIT2NB
                                    645 	.globl _AX5043_CRCINIT1NB
                                    646 	.globl _AX5043_CRCINIT0NB
                                    647 	.globl _AX5043_BGNDRSSITHRNB
                                    648 	.globl _AX5043_BGNDRSSIGAINNB
                                    649 	.globl _AX5043_BGNDRSSINB
                                    650 	.globl _AX5043_BBTUNENB
                                    651 	.globl _AX5043_BBOFFSCAPNB
                                    652 	.globl _AX5043_AMPLFILTERNB
                                    653 	.globl _AX5043_AGCCOUNTERNB
                                    654 	.globl _AX5043_AFSKSPACE1NB
                                    655 	.globl _AX5043_AFSKSPACE0NB
                                    656 	.globl _AX5043_AFSKMARK1NB
                                    657 	.globl _AX5043_AFSKMARK0NB
                                    658 	.globl _AX5043_AFSKCTRLNB
                                    659 	.globl _AX5043_TIMEGAIN3
                                    660 	.globl _AX5043_TIMEGAIN2
                                    661 	.globl _AX5043_TIMEGAIN1
                                    662 	.globl _AX5043_TIMEGAIN0
                                    663 	.globl _AX5043_RXPARAMSETS
                                    664 	.globl _AX5043_RXPARAMCURSET
                                    665 	.globl _AX5043_PKTMAXLEN
                                    666 	.globl _AX5043_PKTLENOFFSET
                                    667 	.globl _AX5043_PKTLENCFG
                                    668 	.globl _AX5043_PKTADDRMASK3
                                    669 	.globl _AX5043_PKTADDRMASK2
                                    670 	.globl _AX5043_PKTADDRMASK1
                                    671 	.globl _AX5043_PKTADDRMASK0
                                    672 	.globl _AX5043_PKTADDRCFG
                                    673 	.globl _AX5043_PKTADDR3
                                    674 	.globl _AX5043_PKTADDR2
                                    675 	.globl _AX5043_PKTADDR1
                                    676 	.globl _AX5043_PKTADDR0
                                    677 	.globl _AX5043_PHASEGAIN3
                                    678 	.globl _AX5043_PHASEGAIN2
                                    679 	.globl _AX5043_PHASEGAIN1
                                    680 	.globl _AX5043_PHASEGAIN0
                                    681 	.globl _AX5043_FREQUENCYLEAK
                                    682 	.globl _AX5043_FREQUENCYGAIND3
                                    683 	.globl _AX5043_FREQUENCYGAIND2
                                    684 	.globl _AX5043_FREQUENCYGAIND1
                                    685 	.globl _AX5043_FREQUENCYGAIND0
                                    686 	.globl _AX5043_FREQUENCYGAINC3
                                    687 	.globl _AX5043_FREQUENCYGAINC2
                                    688 	.globl _AX5043_FREQUENCYGAINC1
                                    689 	.globl _AX5043_FREQUENCYGAINC0
                                    690 	.globl _AX5043_FREQUENCYGAINB3
                                    691 	.globl _AX5043_FREQUENCYGAINB2
                                    692 	.globl _AX5043_FREQUENCYGAINB1
                                    693 	.globl _AX5043_FREQUENCYGAINB0
                                    694 	.globl _AX5043_FREQUENCYGAINA3
                                    695 	.globl _AX5043_FREQUENCYGAINA2
                                    696 	.globl _AX5043_FREQUENCYGAINA1
                                    697 	.globl _AX5043_FREQUENCYGAINA0
                                    698 	.globl _AX5043_FREQDEV13
                                    699 	.globl _AX5043_FREQDEV12
                                    700 	.globl _AX5043_FREQDEV11
                                    701 	.globl _AX5043_FREQDEV10
                                    702 	.globl _AX5043_FREQDEV03
                                    703 	.globl _AX5043_FREQDEV02
                                    704 	.globl _AX5043_FREQDEV01
                                    705 	.globl _AX5043_FREQDEV00
                                    706 	.globl _AX5043_FOURFSK3
                                    707 	.globl _AX5043_FOURFSK2
                                    708 	.globl _AX5043_FOURFSK1
                                    709 	.globl _AX5043_FOURFSK0
                                    710 	.globl _AX5043_DRGAIN3
                                    711 	.globl _AX5043_DRGAIN2
                                    712 	.globl _AX5043_DRGAIN1
                                    713 	.globl _AX5043_DRGAIN0
                                    714 	.globl _AX5043_BBOFFSRES3
                                    715 	.globl _AX5043_BBOFFSRES2
                                    716 	.globl _AX5043_BBOFFSRES1
                                    717 	.globl _AX5043_BBOFFSRES0
                                    718 	.globl _AX5043_AMPLITUDEGAIN3
                                    719 	.globl _AX5043_AMPLITUDEGAIN2
                                    720 	.globl _AX5043_AMPLITUDEGAIN1
                                    721 	.globl _AX5043_AMPLITUDEGAIN0
                                    722 	.globl _AX5043_AGCTARGET3
                                    723 	.globl _AX5043_AGCTARGET2
                                    724 	.globl _AX5043_AGCTARGET1
                                    725 	.globl _AX5043_AGCTARGET0
                                    726 	.globl _AX5043_AGCMINMAX3
                                    727 	.globl _AX5043_AGCMINMAX2
                                    728 	.globl _AX5043_AGCMINMAX1
                                    729 	.globl _AX5043_AGCMINMAX0
                                    730 	.globl _AX5043_AGCGAIN3
                                    731 	.globl _AX5043_AGCGAIN2
                                    732 	.globl _AX5043_AGCGAIN1
                                    733 	.globl _AX5043_AGCGAIN0
                                    734 	.globl _AX5043_AGCAHYST3
                                    735 	.globl _AX5043_AGCAHYST2
                                    736 	.globl _AX5043_AGCAHYST1
                                    737 	.globl _AX5043_AGCAHYST0
                                    738 	.globl _AX5043_XTALSTATUS
                                    739 	.globl _AX5043_XTALCAP
                                    740 	.globl _AX5043_WAKEUPXOEARLY
                                    741 	.globl _AX5043_WAKEUPTIMER1
                                    742 	.globl _AX5043_WAKEUPTIMER0
                                    743 	.globl _AX5043_WAKEUPFREQ1
                                    744 	.globl _AX5043_WAKEUPFREQ0
                                    745 	.globl _AX5043_WAKEUP1
                                    746 	.globl _AX5043_WAKEUP0
                                    747 	.globl _AX5043_TXRATE2
                                    748 	.globl _AX5043_TXRATE1
                                    749 	.globl _AX5043_TXRATE0
                                    750 	.globl _AX5043_TXPWRCOEFFE1
                                    751 	.globl _AX5043_TXPWRCOEFFE0
                                    752 	.globl _AX5043_TXPWRCOEFFD1
                                    753 	.globl _AX5043_TXPWRCOEFFD0
                                    754 	.globl _AX5043_TXPWRCOEFFC1
                                    755 	.globl _AX5043_TXPWRCOEFFC0
                                    756 	.globl _AX5043_TXPWRCOEFFB1
                                    757 	.globl _AX5043_TXPWRCOEFFB0
                                    758 	.globl _AX5043_TXPWRCOEFFA1
                                    759 	.globl _AX5043_TXPWRCOEFFA0
                                    760 	.globl _AX5043_TRKRFFREQ2
                                    761 	.globl _AX5043_TRKRFFREQ1
                                    762 	.globl _AX5043_TRKRFFREQ0
                                    763 	.globl _AX5043_TRKPHASE1
                                    764 	.globl _AX5043_TRKPHASE0
                                    765 	.globl _AX5043_TRKFSKDEMOD1
                                    766 	.globl _AX5043_TRKFSKDEMOD0
                                    767 	.globl _AX5043_TRKFREQ1
                                    768 	.globl _AX5043_TRKFREQ0
                                    769 	.globl _AX5043_TRKDATARATE2
                                    770 	.globl _AX5043_TRKDATARATE1
                                    771 	.globl _AX5043_TRKDATARATE0
                                    772 	.globl _AX5043_TRKAMPLITUDE1
                                    773 	.globl _AX5043_TRKAMPLITUDE0
                                    774 	.globl _AX5043_TRKAFSKDEMOD1
                                    775 	.globl _AX5043_TRKAFSKDEMOD0
                                    776 	.globl _AX5043_TMGTXSETTLE
                                    777 	.globl _AX5043_TMGTXBOOST
                                    778 	.globl _AX5043_TMGRXSETTLE
                                    779 	.globl _AX5043_TMGRXRSSI
                                    780 	.globl _AX5043_TMGRXPREAMBLE3
                                    781 	.globl _AX5043_TMGRXPREAMBLE2
                                    782 	.globl _AX5043_TMGRXPREAMBLE1
                                    783 	.globl _AX5043_TMGRXOFFSACQ
                                    784 	.globl _AX5043_TMGRXCOARSEAGC
                                    785 	.globl _AX5043_TMGRXBOOST
                                    786 	.globl _AX5043_TMGRXAGC
                                    787 	.globl _AX5043_TIMER2
                                    788 	.globl _AX5043_TIMER1
                                    789 	.globl _AX5043_TIMER0
                                    790 	.globl _AX5043_SILICONREVISION
                                    791 	.globl _AX5043_SCRATCH
                                    792 	.globl _AX5043_RXDATARATE2
                                    793 	.globl _AX5043_RXDATARATE1
                                    794 	.globl _AX5043_RXDATARATE0
                                    795 	.globl _AX5043_RSSIREFERENCE
                                    796 	.globl _AX5043_RSSIABSTHR
                                    797 	.globl _AX5043_RSSI
                                    798 	.globl _AX5043_RADIOSTATE
                                    799 	.globl _AX5043_RADIOEVENTREQ1
                                    800 	.globl _AX5043_RADIOEVENTREQ0
                                    801 	.globl _AX5043_RADIOEVENTMASK1
                                    802 	.globl _AX5043_RADIOEVENTMASK0
                                    803 	.globl _AX5043_PWRMODE
                                    804 	.globl _AX5043_PWRAMP
                                    805 	.globl _AX5043_POWSTICKYSTAT
                                    806 	.globl _AX5043_POWSTAT
                                    807 	.globl _AX5043_POWIRQMASK
                                    808 	.globl _AX5043_PLLVCOIR
                                    809 	.globl _AX5043_PLLVCOI
                                    810 	.globl _AX5043_PLLVCODIV
                                    811 	.globl _AX5043_PLLRNGCLK
                                    812 	.globl _AX5043_PLLRANGINGB
                                    813 	.globl _AX5043_PLLRANGINGA
                                    814 	.globl _AX5043_PLLLOOPBOOST
                                    815 	.globl _AX5043_PLLLOOP
                                    816 	.globl _AX5043_PLLLOCKDET
                                    817 	.globl _AX5043_PLLCPIBOOST
                                    818 	.globl _AX5043_PLLCPI
                                    819 	.globl _AX5043_PKTSTOREFLAGS
                                    820 	.globl _AX5043_PKTMISCFLAGS
                                    821 	.globl _AX5043_PKTCHUNKSIZE
                                    822 	.globl _AX5043_PKTACCEPTFLAGS
                                    823 	.globl _AX5043_PINSTATE
                                    824 	.globl _AX5043_PINFUNCSYSCLK
                                    825 	.globl _AX5043_PINFUNCPWRAMP
                                    826 	.globl _AX5043_PINFUNCIRQ
                                    827 	.globl _AX5043_PINFUNCDCLK
                                    828 	.globl _AX5043_PINFUNCDATA
                                    829 	.globl _AX5043_PINFUNCANTSEL
                                    830 	.globl _AX5043_MODULATION
                                    831 	.globl _AX5043_MODCFGF
                                    832 	.globl _AX5043_MODCFGA
                                    833 	.globl _AX5043_MAXRFOFFSET2
                                    834 	.globl _AX5043_MAXRFOFFSET1
                                    835 	.globl _AX5043_MAXRFOFFSET0
                                    836 	.globl _AX5043_MAXDROFFSET2
                                    837 	.globl _AX5043_MAXDROFFSET1
                                    838 	.globl _AX5043_MAXDROFFSET0
                                    839 	.globl _AX5043_MATCH1PAT1
                                    840 	.globl _AX5043_MATCH1PAT0
                                    841 	.globl _AX5043_MATCH1MIN
                                    842 	.globl _AX5043_MATCH1MAX
                                    843 	.globl _AX5043_MATCH1LEN
                                    844 	.globl _AX5043_MATCH0PAT3
                                    845 	.globl _AX5043_MATCH0PAT2
                                    846 	.globl _AX5043_MATCH0PAT1
                                    847 	.globl _AX5043_MATCH0PAT0
                                    848 	.globl _AX5043_MATCH0MIN
                                    849 	.globl _AX5043_MATCH0MAX
                                    850 	.globl _AX5043_MATCH0LEN
                                    851 	.globl _AX5043_LPOSCSTATUS
                                    852 	.globl _AX5043_LPOSCREF1
                                    853 	.globl _AX5043_LPOSCREF0
                                    854 	.globl _AX5043_LPOSCPER1
                                    855 	.globl _AX5043_LPOSCPER0
                                    856 	.globl _AX5043_LPOSCKFILT1
                                    857 	.globl _AX5043_LPOSCKFILT0
                                    858 	.globl _AX5043_LPOSCFREQ1
                                    859 	.globl _AX5043_LPOSCFREQ0
                                    860 	.globl _AX5043_LPOSCCONFIG
                                    861 	.globl _AX5043_IRQREQUEST1
                                    862 	.globl _AX5043_IRQREQUEST0
                                    863 	.globl _AX5043_IRQMASK1
                                    864 	.globl _AX5043_IRQMASK0
                                    865 	.globl _AX5043_IRQINVERSION1
                                    866 	.globl _AX5043_IRQINVERSION0
                                    867 	.globl _AX5043_IFFREQ1
                                    868 	.globl _AX5043_IFFREQ0
                                    869 	.globl _AX5043_GPADCPERIOD
                                    870 	.globl _AX5043_GPADCCTRL
                                    871 	.globl _AX5043_GPADC13VALUE1
                                    872 	.globl _AX5043_GPADC13VALUE0
                                    873 	.globl _AX5043_FSKDMIN1
                                    874 	.globl _AX5043_FSKDMIN0
                                    875 	.globl _AX5043_FSKDMAX1
                                    876 	.globl _AX5043_FSKDMAX0
                                    877 	.globl _AX5043_FSKDEV2
                                    878 	.globl _AX5043_FSKDEV1
                                    879 	.globl _AX5043_FSKDEV0
                                    880 	.globl _AX5043_FREQB3
                                    881 	.globl _AX5043_FREQB2
                                    882 	.globl _AX5043_FREQB1
                                    883 	.globl _AX5043_FREQB0
                                    884 	.globl _AX5043_FREQA3
                                    885 	.globl _AX5043_FREQA2
                                    886 	.globl _AX5043_FREQA1
                                    887 	.globl _AX5043_FREQA0
                                    888 	.globl _AX5043_FRAMING
                                    889 	.globl _AX5043_FIFOTHRESH1
                                    890 	.globl _AX5043_FIFOTHRESH0
                                    891 	.globl _AX5043_FIFOSTAT
                                    892 	.globl _AX5043_FIFOFREE1
                                    893 	.globl _AX5043_FIFOFREE0
                                    894 	.globl _AX5043_FIFODATA
                                    895 	.globl _AX5043_FIFOCOUNT1
                                    896 	.globl _AX5043_FIFOCOUNT0
                                    897 	.globl _AX5043_FECSYNC
                                    898 	.globl _AX5043_FECSTATUS
                                    899 	.globl _AX5043_FEC
                                    900 	.globl _AX5043_ENCODING
                                    901 	.globl _AX5043_DIVERSITY
                                    902 	.globl _AX5043_DECIMATION
                                    903 	.globl _AX5043_DACVALUE1
                                    904 	.globl _AX5043_DACVALUE0
                                    905 	.globl _AX5043_DACCONFIG
                                    906 	.globl _AX5043_CRCINIT3
                                    907 	.globl _AX5043_CRCINIT2
                                    908 	.globl _AX5043_CRCINIT1
                                    909 	.globl _AX5043_CRCINIT0
                                    910 	.globl _AX5043_BGNDRSSITHR
                                    911 	.globl _AX5043_BGNDRSSIGAIN
                                    912 	.globl _AX5043_BGNDRSSI
                                    913 	.globl _AX5043_BBTUNE
                                    914 	.globl _AX5043_BBOFFSCAP
                                    915 	.globl _AX5043_AMPLFILTER
                                    916 	.globl _AX5043_AGCCOUNTER
                                    917 	.globl _AX5043_AFSKSPACE1
                                    918 	.globl _AX5043_AFSKSPACE0
                                    919 	.globl _AX5043_AFSKMARK1
                                    920 	.globl _AX5043_AFSKMARK0
                                    921 	.globl _AX5043_AFSKCTRL
                                    922 	.globl _XWTSTAT
                                    923 	.globl _XWTIRQEN
                                    924 	.globl _XWTEVTD
                                    925 	.globl _XWTEVTD1
                                    926 	.globl _XWTEVTD0
                                    927 	.globl _XWTEVTC
                                    928 	.globl _XWTEVTC1
                                    929 	.globl _XWTEVTC0
                                    930 	.globl _XWTEVTB
                                    931 	.globl _XWTEVTB1
                                    932 	.globl _XWTEVTB0
                                    933 	.globl _XWTEVTA
                                    934 	.globl _XWTEVTA1
                                    935 	.globl _XWTEVTA0
                                    936 	.globl _XWTCNTR1
                                    937 	.globl _XWTCNTB
                                    938 	.globl _XWTCNTB1
                                    939 	.globl _XWTCNTB0
                                    940 	.globl _XWTCNTA
                                    941 	.globl _XWTCNTA1
                                    942 	.globl _XWTCNTA0
                                    943 	.globl _XWTCFGB
                                    944 	.globl _XWTCFGA
                                    945 	.globl _XWDTRESET
                                    946 	.globl _XWDTCFG
                                    947 	.globl _XU1STATUS
                                    948 	.globl _XU1SHREG
                                    949 	.globl _XU1MODE
                                    950 	.globl _XU1CTRL
                                    951 	.globl _XU0STATUS
                                    952 	.globl _XU0SHREG
                                    953 	.globl _XU0MODE
                                    954 	.globl _XU0CTRL
                                    955 	.globl _XT2STATUS
                                    956 	.globl _XT2PERIOD
                                    957 	.globl _XT2PERIOD1
                                    958 	.globl _XT2PERIOD0
                                    959 	.globl _XT2MODE
                                    960 	.globl _XT2CNT
                                    961 	.globl _XT2CNT1
                                    962 	.globl _XT2CNT0
                                    963 	.globl _XT2CLKSRC
                                    964 	.globl _XT1STATUS
                                    965 	.globl _XT1PERIOD
                                    966 	.globl _XT1PERIOD1
                                    967 	.globl _XT1PERIOD0
                                    968 	.globl _XT1MODE
                                    969 	.globl _XT1CNT
                                    970 	.globl _XT1CNT1
                                    971 	.globl _XT1CNT0
                                    972 	.globl _XT1CLKSRC
                                    973 	.globl _XT0STATUS
                                    974 	.globl _XT0PERIOD
                                    975 	.globl _XT0PERIOD1
                                    976 	.globl _XT0PERIOD0
                                    977 	.globl _XT0MODE
                                    978 	.globl _XT0CNT
                                    979 	.globl _XT0CNT1
                                    980 	.globl _XT0CNT0
                                    981 	.globl _XT0CLKSRC
                                    982 	.globl _XSPSTATUS
                                    983 	.globl _XSPSHREG
                                    984 	.globl _XSPMODE
                                    985 	.globl _XSPCLKSRC
                                    986 	.globl _XRADIOSTAT
                                    987 	.globl _XRADIOSTAT1
                                    988 	.globl _XRADIOSTAT0
                                    989 	.globl _XRADIODATA3
                                    990 	.globl _XRADIODATA2
                                    991 	.globl _XRADIODATA1
                                    992 	.globl _XRADIODATA0
                                    993 	.globl _XRADIOADDR1
                                    994 	.globl _XRADIOADDR0
                                    995 	.globl _XRADIOACC
                                    996 	.globl _XOC1STATUS
                                    997 	.globl _XOC1PIN
                                    998 	.globl _XOC1MODE
                                    999 	.globl _XOC1COMP
                                   1000 	.globl _XOC1COMP1
                                   1001 	.globl _XOC1COMP0
                                   1002 	.globl _XOC0STATUS
                                   1003 	.globl _XOC0PIN
                                   1004 	.globl _XOC0MODE
                                   1005 	.globl _XOC0COMP
                                   1006 	.globl _XOC0COMP1
                                   1007 	.globl _XOC0COMP0
                                   1008 	.globl _XNVSTATUS
                                   1009 	.globl _XNVKEY
                                   1010 	.globl _XNVDATA
                                   1011 	.globl _XNVDATA1
                                   1012 	.globl _XNVDATA0
                                   1013 	.globl _XNVADDR
                                   1014 	.globl _XNVADDR1
                                   1015 	.globl _XNVADDR0
                                   1016 	.globl _XIC1STATUS
                                   1017 	.globl _XIC1MODE
                                   1018 	.globl _XIC1CAPT
                                   1019 	.globl _XIC1CAPT1
                                   1020 	.globl _XIC1CAPT0
                                   1021 	.globl _XIC0STATUS
                                   1022 	.globl _XIC0MODE
                                   1023 	.globl _XIC0CAPT
                                   1024 	.globl _XIC0CAPT1
                                   1025 	.globl _XIC0CAPT0
                                   1026 	.globl _XPORTR
                                   1027 	.globl _XPORTC
                                   1028 	.globl _XPORTB
                                   1029 	.globl _XPORTA
                                   1030 	.globl _XPINR
                                   1031 	.globl _XPINC
                                   1032 	.globl _XPINB
                                   1033 	.globl _XPINA
                                   1034 	.globl _XDIRR
                                   1035 	.globl _XDIRC
                                   1036 	.globl _XDIRB
                                   1037 	.globl _XDIRA
                                   1038 	.globl _XDBGLNKSTAT
                                   1039 	.globl _XDBGLNKBUF
                                   1040 	.globl _XCODECONFIG
                                   1041 	.globl _XCLKSTAT
                                   1042 	.globl _XCLKCON
                                   1043 	.globl _XANALOGCOMP
                                   1044 	.globl _XADCCONV
                                   1045 	.globl _XADCCLKSRC
                                   1046 	.globl _XADCCH3CONFIG
                                   1047 	.globl _XADCCH2CONFIG
                                   1048 	.globl _XADCCH1CONFIG
                                   1049 	.globl _XADCCH0CONFIG
                                   1050 	.globl _XPCON
                                   1051 	.globl _XIP
                                   1052 	.globl _XIE
                                   1053 	.globl _XDPTR1
                                   1054 	.globl _XDPTR0
                                   1055 	.globl _XTALREADY
                                   1056 	.globl _XTALOSC
                                   1057 	.globl _XTALAMPL
                                   1058 	.globl _SILICONREV
                                   1059 	.globl _SCRATCH3
                                   1060 	.globl _SCRATCH2
                                   1061 	.globl _SCRATCH1
                                   1062 	.globl _SCRATCH0
                                   1063 	.globl _RADIOMUX
                                   1064 	.globl _RADIOFSTATADDR
                                   1065 	.globl _RADIOFSTATADDR1
                                   1066 	.globl _RADIOFSTATADDR0
                                   1067 	.globl _RADIOFDATAADDR
                                   1068 	.globl _RADIOFDATAADDR1
                                   1069 	.globl _RADIOFDATAADDR0
                                   1070 	.globl _OSCRUN
                                   1071 	.globl _OSCREADY
                                   1072 	.globl _OSCFORCERUN
                                   1073 	.globl _OSCCALIB
                                   1074 	.globl _MISCCTRL
                                   1075 	.globl _LPXOSCGM
                                   1076 	.globl _LPOSCREF
                                   1077 	.globl _LPOSCREF1
                                   1078 	.globl _LPOSCREF0
                                   1079 	.globl _LPOSCPER
                                   1080 	.globl _LPOSCPER1
                                   1081 	.globl _LPOSCPER0
                                   1082 	.globl _LPOSCKFILT
                                   1083 	.globl _LPOSCKFILT1
                                   1084 	.globl _LPOSCKFILT0
                                   1085 	.globl _LPOSCFREQ
                                   1086 	.globl _LPOSCFREQ1
                                   1087 	.globl _LPOSCFREQ0
                                   1088 	.globl _LPOSCCONFIG
                                   1089 	.globl _PINSEL
                                   1090 	.globl _PINCHGC
                                   1091 	.globl _PINCHGB
                                   1092 	.globl _PINCHGA
                                   1093 	.globl _PALTRADIO
                                   1094 	.globl _PALTC
                                   1095 	.globl _PALTB
                                   1096 	.globl _PALTA
                                   1097 	.globl _INTCHGC
                                   1098 	.globl _INTCHGB
                                   1099 	.globl _INTCHGA
                                   1100 	.globl _EXTIRQ
                                   1101 	.globl _GPIOENABLE
                                   1102 	.globl _ANALOGA
                                   1103 	.globl _FRCOSCREF
                                   1104 	.globl _FRCOSCREF1
                                   1105 	.globl _FRCOSCREF0
                                   1106 	.globl _FRCOSCPER
                                   1107 	.globl _FRCOSCPER1
                                   1108 	.globl _FRCOSCPER0
                                   1109 	.globl _FRCOSCKFILT
                                   1110 	.globl _FRCOSCKFILT1
                                   1111 	.globl _FRCOSCKFILT0
                                   1112 	.globl _FRCOSCFREQ
                                   1113 	.globl _FRCOSCFREQ1
                                   1114 	.globl _FRCOSCFREQ0
                                   1115 	.globl _FRCOSCCTRL
                                   1116 	.globl _FRCOSCCONFIG
                                   1117 	.globl _DMA1CONFIG
                                   1118 	.globl _DMA1ADDR
                                   1119 	.globl _DMA1ADDR1
                                   1120 	.globl _DMA1ADDR0
                                   1121 	.globl _DMA0CONFIG
                                   1122 	.globl _DMA0ADDR
                                   1123 	.globl _DMA0ADDR1
                                   1124 	.globl _DMA0ADDR0
                                   1125 	.globl _ADCTUNE2
                                   1126 	.globl _ADCTUNE1
                                   1127 	.globl _ADCTUNE0
                                   1128 	.globl _ADCCH3VAL
                                   1129 	.globl _ADCCH3VAL1
                                   1130 	.globl _ADCCH3VAL0
                                   1131 	.globl _ADCCH2VAL
                                   1132 	.globl _ADCCH2VAL1
                                   1133 	.globl _ADCCH2VAL0
                                   1134 	.globl _ADCCH1VAL
                                   1135 	.globl _ADCCH1VAL1
                                   1136 	.globl _ADCCH1VAL0
                                   1137 	.globl _ADCCH0VAL
                                   1138 	.globl _ADCCH0VAL1
                                   1139 	.globl _ADCCH0VAL0
                                   1140 	.globl _axradio_trxstate
                                   1141 	.globl _axradio_mode
                                   1142 	.globl _axradio_conv_time_totimer0
                                   1143 	.globl _axradio_isr
                                   1144 	.globl _ax5043_receiver_on_continuous
                                   1145 	.globl _ax5043_receiver_on_wor
                                   1146 	.globl _ax5043_prepare_tx
                                   1147 	.globl _ax5043_off
                                   1148 	.globl _ax5043_off_xtal
                                   1149 	.globl _axradio_wait_for_xtal
                                   1150 	.globl _axradio_init
                                   1151 	.globl _axradio_cansleep
                                   1152 	.globl _axradio_set_mode
                                   1153 	.globl _axradio_set_channel
                                   1154 	.globl _axradio_get_pllvcoi
                                   1155 	.globl _axradio_set_local_address
                                   1156 	.globl _axradio_set_default_remote_address
                                   1157 	.globl _axradio_transmit
                                   1158 ;--------------------------------------------------------
                                   1159 ; special function registers
                                   1160 ;--------------------------------------------------------
                                   1161 	.area RSEG    (ABS,DATA)
      000000                       1162 	.org 0x0000
                           0000E0  1163 _ACC	=	0x00e0
                           0000F0  1164 _B	=	0x00f0
                           000083  1165 _DPH	=	0x0083
                           000085  1166 _DPH1	=	0x0085
                           000082  1167 _DPL	=	0x0082
                           000084  1168 _DPL1	=	0x0084
                           008382  1169 _DPTR0	=	0x8382
                           008584  1170 _DPTR1	=	0x8584
                           000086  1171 _DPS	=	0x0086
                           0000A0  1172 _E2IE	=	0x00a0
                           0000C0  1173 _E2IP	=	0x00c0
                           000098  1174 _EIE	=	0x0098
                           0000B0  1175 _EIP	=	0x00b0
                           0000A8  1176 _IE	=	0x00a8
                           0000B8  1177 _IP	=	0x00b8
                           000087  1178 _PCON	=	0x0087
                           0000D0  1179 _PSW	=	0x00d0
                           000081  1180 _SP	=	0x0081
                           0000D9  1181 _XPAGE	=	0x00d9
                           0000D9  1182 __XPAGE	=	0x00d9
                           0000CA  1183 _ADCCH0CONFIG	=	0x00ca
                           0000CB  1184 _ADCCH1CONFIG	=	0x00cb
                           0000D2  1185 _ADCCH2CONFIG	=	0x00d2
                           0000D3  1186 _ADCCH3CONFIG	=	0x00d3
                           0000D1  1187 _ADCCLKSRC	=	0x00d1
                           0000C9  1188 _ADCCONV	=	0x00c9
                           0000E1  1189 _ANALOGCOMP	=	0x00e1
                           0000C6  1190 _CLKCON	=	0x00c6
                           0000C7  1191 _CLKSTAT	=	0x00c7
                           000097  1192 _CODECONFIG	=	0x0097
                           0000E3  1193 _DBGLNKBUF	=	0x00e3
                           0000E2  1194 _DBGLNKSTAT	=	0x00e2
                           000089  1195 _DIRA	=	0x0089
                           00008A  1196 _DIRB	=	0x008a
                           00008B  1197 _DIRC	=	0x008b
                           00008E  1198 _DIRR	=	0x008e
                           0000C8  1199 _PINA	=	0x00c8
                           0000E8  1200 _PINB	=	0x00e8
                           0000F8  1201 _PINC	=	0x00f8
                           00008D  1202 _PINR	=	0x008d
                           000080  1203 _PORTA	=	0x0080
                           000088  1204 _PORTB	=	0x0088
                           000090  1205 _PORTC	=	0x0090
                           00008C  1206 _PORTR	=	0x008c
                           0000CE  1207 _IC0CAPT0	=	0x00ce
                           0000CF  1208 _IC0CAPT1	=	0x00cf
                           00CFCE  1209 _IC0CAPT	=	0xcfce
                           0000CC  1210 _IC0MODE	=	0x00cc
                           0000CD  1211 _IC0STATUS	=	0x00cd
                           0000D6  1212 _IC1CAPT0	=	0x00d6
                           0000D7  1213 _IC1CAPT1	=	0x00d7
                           00D7D6  1214 _IC1CAPT	=	0xd7d6
                           0000D4  1215 _IC1MODE	=	0x00d4
                           0000D5  1216 _IC1STATUS	=	0x00d5
                           000092  1217 _NVADDR0	=	0x0092
                           000093  1218 _NVADDR1	=	0x0093
                           009392  1219 _NVADDR	=	0x9392
                           000094  1220 _NVDATA0	=	0x0094
                           000095  1221 _NVDATA1	=	0x0095
                           009594  1222 _NVDATA	=	0x9594
                           000096  1223 _NVKEY	=	0x0096
                           000091  1224 _NVSTATUS	=	0x0091
                           0000BC  1225 _OC0COMP0	=	0x00bc
                           0000BD  1226 _OC0COMP1	=	0x00bd
                           00BDBC  1227 _OC0COMP	=	0xbdbc
                           0000B9  1228 _OC0MODE	=	0x00b9
                           0000BA  1229 _OC0PIN	=	0x00ba
                           0000BB  1230 _OC0STATUS	=	0x00bb
                           0000C4  1231 _OC1COMP0	=	0x00c4
                           0000C5  1232 _OC1COMP1	=	0x00c5
                           00C5C4  1233 _OC1COMP	=	0xc5c4
                           0000C1  1234 _OC1MODE	=	0x00c1
                           0000C2  1235 _OC1PIN	=	0x00c2
                           0000C3  1236 _OC1STATUS	=	0x00c3
                           0000B1  1237 _RADIOACC	=	0x00b1
                           0000B3  1238 _RADIOADDR0	=	0x00b3
                           0000B2  1239 _RADIOADDR1	=	0x00b2
                           00B2B3  1240 _RADIOADDR	=	0xb2b3
                           0000B7  1241 _RADIODATA0	=	0x00b7
                           0000B6  1242 _RADIODATA1	=	0x00b6
                           0000B5  1243 _RADIODATA2	=	0x00b5
                           0000B4  1244 _RADIODATA3	=	0x00b4
                           B4B5B6B7  1245 _RADIODATA	=	0xb4b5b6b7
                           0000BE  1246 _RADIOSTAT0	=	0x00be
                           0000BF  1247 _RADIOSTAT1	=	0x00bf
                           00BFBE  1248 _RADIOSTAT	=	0xbfbe
                           0000DF  1249 _SPCLKSRC	=	0x00df
                           0000DC  1250 _SPMODE	=	0x00dc
                           0000DE  1251 _SPSHREG	=	0x00de
                           0000DD  1252 _SPSTATUS	=	0x00dd
                           00009A  1253 _T0CLKSRC	=	0x009a
                           00009C  1254 _T0CNT0	=	0x009c
                           00009D  1255 _T0CNT1	=	0x009d
                           009D9C  1256 _T0CNT	=	0x9d9c
                           000099  1257 _T0MODE	=	0x0099
                           00009E  1258 _T0PERIOD0	=	0x009e
                           00009F  1259 _T0PERIOD1	=	0x009f
                           009F9E  1260 _T0PERIOD	=	0x9f9e
                           00009B  1261 _T0STATUS	=	0x009b
                           0000A2  1262 _T1CLKSRC	=	0x00a2
                           0000A4  1263 _T1CNT0	=	0x00a4
                           0000A5  1264 _T1CNT1	=	0x00a5
                           00A5A4  1265 _T1CNT	=	0xa5a4
                           0000A1  1266 _T1MODE	=	0x00a1
                           0000A6  1267 _T1PERIOD0	=	0x00a6
                           0000A7  1268 _T1PERIOD1	=	0x00a7
                           00A7A6  1269 _T1PERIOD	=	0xa7a6
                           0000A3  1270 _T1STATUS	=	0x00a3
                           0000AA  1271 _T2CLKSRC	=	0x00aa
                           0000AC  1272 _T2CNT0	=	0x00ac
                           0000AD  1273 _T2CNT1	=	0x00ad
                           00ADAC  1274 _T2CNT	=	0xadac
                           0000A9  1275 _T2MODE	=	0x00a9
                           0000AE  1276 _T2PERIOD0	=	0x00ae
                           0000AF  1277 _T2PERIOD1	=	0x00af
                           00AFAE  1278 _T2PERIOD	=	0xafae
                           0000AB  1279 _T2STATUS	=	0x00ab
                           0000E4  1280 _U0CTRL	=	0x00e4
                           0000E7  1281 _U0MODE	=	0x00e7
                           0000E6  1282 _U0SHREG	=	0x00e6
                           0000E5  1283 _U0STATUS	=	0x00e5
                           0000EC  1284 _U1CTRL	=	0x00ec
                           0000EF  1285 _U1MODE	=	0x00ef
                           0000EE  1286 _U1SHREG	=	0x00ee
                           0000ED  1287 _U1STATUS	=	0x00ed
                           0000DA  1288 _WDTCFG	=	0x00da
                           0000DB  1289 _WDTRESET	=	0x00db
                           0000F1  1290 _WTCFGA	=	0x00f1
                           0000F9  1291 _WTCFGB	=	0x00f9
                           0000F2  1292 _WTCNTA0	=	0x00f2
                           0000F3  1293 _WTCNTA1	=	0x00f3
                           00F3F2  1294 _WTCNTA	=	0xf3f2
                           0000FA  1295 _WTCNTB0	=	0x00fa
                           0000FB  1296 _WTCNTB1	=	0x00fb
                           00FBFA  1297 _WTCNTB	=	0xfbfa
                           0000EB  1298 _WTCNTR1	=	0x00eb
                           0000F4  1299 _WTEVTA0	=	0x00f4
                           0000F5  1300 _WTEVTA1	=	0x00f5
                           00F5F4  1301 _WTEVTA	=	0xf5f4
                           0000F6  1302 _WTEVTB0	=	0x00f6
                           0000F7  1303 _WTEVTB1	=	0x00f7
                           00F7F6  1304 _WTEVTB	=	0xf7f6
                           0000FC  1305 _WTEVTC0	=	0x00fc
                           0000FD  1306 _WTEVTC1	=	0x00fd
                           00FDFC  1307 _WTEVTC	=	0xfdfc
                           0000FE  1308 _WTEVTD0	=	0x00fe
                           0000FF  1309 _WTEVTD1	=	0x00ff
                           00FFFE  1310 _WTEVTD	=	0xfffe
                           0000E9  1311 _WTIRQEN	=	0x00e9
                           0000EA  1312 _WTSTAT	=	0x00ea
                                   1313 ;--------------------------------------------------------
                                   1314 ; special function bits
                                   1315 ;--------------------------------------------------------
                                   1316 	.area RSEG    (ABS,DATA)
      000000                       1317 	.org 0x0000
                           0000E0  1318 _ACC_0	=	0x00e0
                           0000E1  1319 _ACC_1	=	0x00e1
                           0000E2  1320 _ACC_2	=	0x00e2
                           0000E3  1321 _ACC_3	=	0x00e3
                           0000E4  1322 _ACC_4	=	0x00e4
                           0000E5  1323 _ACC_5	=	0x00e5
                           0000E6  1324 _ACC_6	=	0x00e6
                           0000E7  1325 _ACC_7	=	0x00e7
                           0000F0  1326 _B_0	=	0x00f0
                           0000F1  1327 _B_1	=	0x00f1
                           0000F2  1328 _B_2	=	0x00f2
                           0000F3  1329 _B_3	=	0x00f3
                           0000F4  1330 _B_4	=	0x00f4
                           0000F5  1331 _B_5	=	0x00f5
                           0000F6  1332 _B_6	=	0x00f6
                           0000F7  1333 _B_7	=	0x00f7
                           0000A0  1334 _E2IE_0	=	0x00a0
                           0000A1  1335 _E2IE_1	=	0x00a1
                           0000A2  1336 _E2IE_2	=	0x00a2
                           0000A3  1337 _E2IE_3	=	0x00a3
                           0000A4  1338 _E2IE_4	=	0x00a4
                           0000A5  1339 _E2IE_5	=	0x00a5
                           0000A6  1340 _E2IE_6	=	0x00a6
                           0000A7  1341 _E2IE_7	=	0x00a7
                           0000C0  1342 _E2IP_0	=	0x00c0
                           0000C1  1343 _E2IP_1	=	0x00c1
                           0000C2  1344 _E2IP_2	=	0x00c2
                           0000C3  1345 _E2IP_3	=	0x00c3
                           0000C4  1346 _E2IP_4	=	0x00c4
                           0000C5  1347 _E2IP_5	=	0x00c5
                           0000C6  1348 _E2IP_6	=	0x00c6
                           0000C7  1349 _E2IP_7	=	0x00c7
                           000098  1350 _EIE_0	=	0x0098
                           000099  1351 _EIE_1	=	0x0099
                           00009A  1352 _EIE_2	=	0x009a
                           00009B  1353 _EIE_3	=	0x009b
                           00009C  1354 _EIE_4	=	0x009c
                           00009D  1355 _EIE_5	=	0x009d
                           00009E  1356 _EIE_6	=	0x009e
                           00009F  1357 _EIE_7	=	0x009f
                           0000B0  1358 _EIP_0	=	0x00b0
                           0000B1  1359 _EIP_1	=	0x00b1
                           0000B2  1360 _EIP_2	=	0x00b2
                           0000B3  1361 _EIP_3	=	0x00b3
                           0000B4  1362 _EIP_4	=	0x00b4
                           0000B5  1363 _EIP_5	=	0x00b5
                           0000B6  1364 _EIP_6	=	0x00b6
                           0000B7  1365 _EIP_7	=	0x00b7
                           0000A8  1366 _IE_0	=	0x00a8
                           0000A9  1367 _IE_1	=	0x00a9
                           0000AA  1368 _IE_2	=	0x00aa
                           0000AB  1369 _IE_3	=	0x00ab
                           0000AC  1370 _IE_4	=	0x00ac
                           0000AD  1371 _IE_5	=	0x00ad
                           0000AE  1372 _IE_6	=	0x00ae
                           0000AF  1373 _IE_7	=	0x00af
                           0000AF  1374 _EA	=	0x00af
                           0000B8  1375 _IP_0	=	0x00b8
                           0000B9  1376 _IP_1	=	0x00b9
                           0000BA  1377 _IP_2	=	0x00ba
                           0000BB  1378 _IP_3	=	0x00bb
                           0000BC  1379 _IP_4	=	0x00bc
                           0000BD  1380 _IP_5	=	0x00bd
                           0000BE  1381 _IP_6	=	0x00be
                           0000BF  1382 _IP_7	=	0x00bf
                           0000D0  1383 _P	=	0x00d0
                           0000D1  1384 _F1	=	0x00d1
                           0000D2  1385 _OV	=	0x00d2
                           0000D3  1386 _RS0	=	0x00d3
                           0000D4  1387 _RS1	=	0x00d4
                           0000D5  1388 _F0	=	0x00d5
                           0000D6  1389 _AC	=	0x00d6
                           0000D7  1390 _CY	=	0x00d7
                           0000C8  1391 _PINA_0	=	0x00c8
                           0000C9  1392 _PINA_1	=	0x00c9
                           0000CA  1393 _PINA_2	=	0x00ca
                           0000CB  1394 _PINA_3	=	0x00cb
                           0000CC  1395 _PINA_4	=	0x00cc
                           0000CD  1396 _PINA_5	=	0x00cd
                           0000CE  1397 _PINA_6	=	0x00ce
                           0000CF  1398 _PINA_7	=	0x00cf
                           0000E8  1399 _PINB_0	=	0x00e8
                           0000E9  1400 _PINB_1	=	0x00e9
                           0000EA  1401 _PINB_2	=	0x00ea
                           0000EB  1402 _PINB_3	=	0x00eb
                           0000EC  1403 _PINB_4	=	0x00ec
                           0000ED  1404 _PINB_5	=	0x00ed
                           0000EE  1405 _PINB_6	=	0x00ee
                           0000EF  1406 _PINB_7	=	0x00ef
                           0000F8  1407 _PINC_0	=	0x00f8
                           0000F9  1408 _PINC_1	=	0x00f9
                           0000FA  1409 _PINC_2	=	0x00fa
                           0000FB  1410 _PINC_3	=	0x00fb
                           0000FC  1411 _PINC_4	=	0x00fc
                           0000FD  1412 _PINC_5	=	0x00fd
                           0000FE  1413 _PINC_6	=	0x00fe
                           0000FF  1414 _PINC_7	=	0x00ff
                           000080  1415 _PORTA_0	=	0x0080
                           000081  1416 _PORTA_1	=	0x0081
                           000082  1417 _PORTA_2	=	0x0082
                           000083  1418 _PORTA_3	=	0x0083
                           000084  1419 _PORTA_4	=	0x0084
                           000085  1420 _PORTA_5	=	0x0085
                           000086  1421 _PORTA_6	=	0x0086
                           000087  1422 _PORTA_7	=	0x0087
                           000088  1423 _PORTB_0	=	0x0088
                           000089  1424 _PORTB_1	=	0x0089
                           00008A  1425 _PORTB_2	=	0x008a
                           00008B  1426 _PORTB_3	=	0x008b
                           00008C  1427 _PORTB_4	=	0x008c
                           00008D  1428 _PORTB_5	=	0x008d
                           00008E  1429 _PORTB_6	=	0x008e
                           00008F  1430 _PORTB_7	=	0x008f
                           000090  1431 _PORTC_0	=	0x0090
                           000091  1432 _PORTC_1	=	0x0091
                           000092  1433 _PORTC_2	=	0x0092
                           000093  1434 _PORTC_3	=	0x0093
                           000094  1435 _PORTC_4	=	0x0094
                           000095  1436 _PORTC_5	=	0x0095
                           000096  1437 _PORTC_6	=	0x0096
                           000097  1438 _PORTC_7	=	0x0097
                                   1439 ;--------------------------------------------------------
                                   1440 ; overlayable register banks
                                   1441 ;--------------------------------------------------------
                                   1442 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                       1443 	.ds 8
                                   1444 ;--------------------------------------------------------
                                   1445 ; overlayable bit register bank
                                   1446 ;--------------------------------------------------------
                                   1447 	.area BIT_BANK	(REL,OVR,DATA)
      000021                       1448 bits:
      000021                       1449 	.ds 1
                           008000  1450 	b0 = bits[0]
                           008100  1451 	b1 = bits[1]
                           008200  1452 	b2 = bits[2]
                           008300  1453 	b3 = bits[3]
                           008400  1454 	b4 = bits[4]
                           008500  1455 	b5 = bits[5]
                           008600  1456 	b6 = bits[6]
                           008700  1457 	b7 = bits[7]
                                   1458 ;--------------------------------------------------------
                                   1459 ; internal ram data
                                   1460 ;--------------------------------------------------------
                                   1461 	.area DSEG    (DATA)
      00000B                       1462 _axradio_mode::
      00000B                       1463 	.ds 1
      00000C                       1464 _axradio_trxstate::
      00000C                       1465 	.ds 1
                                   1466 ;--------------------------------------------------------
                                   1467 ; overlayable items in internal ram 
                                   1468 ;--------------------------------------------------------
                                   1469 	.area	OSEG    (OVR,DATA)
                                   1470 	.area	OSEG    (OVR,DATA)
      000043                       1471 _axradio_set_channel_rng_1_422:
      000043                       1472 	.ds 1
                                   1473 ;--------------------------------------------------------
                                   1474 ; indirectly addressable internal ram data
                                   1475 ;--------------------------------------------------------
                                   1476 	.area ISEG    (DATA)
                                   1477 ;--------------------------------------------------------
                                   1478 ; absolute internal ram data
                                   1479 ;--------------------------------------------------------
                                   1480 	.area IABS    (ABS,DATA)
                                   1481 	.area IABS    (ABS,DATA)
                                   1482 ;--------------------------------------------------------
                                   1483 ; bit data
                                   1484 ;--------------------------------------------------------
                                   1485 	.area BSEG    (BIT)
      000001                       1486 _axradio_timer_callback_sloc0_1_0:
      000001                       1487 	.ds 1
      000002                       1488 _axradio_init_sloc0_1_0:
      000002                       1489 	.ds 1
                                   1490 ;--------------------------------------------------------
                                   1491 ; paged external ram data
                                   1492 ;--------------------------------------------------------
                                   1493 	.area PSEG    (PAG,XDATA)
                                   1494 ;--------------------------------------------------------
                                   1495 ; external ram data
                                   1496 ;--------------------------------------------------------
                                   1497 	.area XSEG    (XDATA)
                           007020  1498 _ADCCH0VAL0	=	0x7020
                           007021  1499 _ADCCH0VAL1	=	0x7021
                           007020  1500 _ADCCH0VAL	=	0x7020
                           007022  1501 _ADCCH1VAL0	=	0x7022
                           007023  1502 _ADCCH1VAL1	=	0x7023
                           007022  1503 _ADCCH1VAL	=	0x7022
                           007024  1504 _ADCCH2VAL0	=	0x7024
                           007025  1505 _ADCCH2VAL1	=	0x7025
                           007024  1506 _ADCCH2VAL	=	0x7024
                           007026  1507 _ADCCH3VAL0	=	0x7026
                           007027  1508 _ADCCH3VAL1	=	0x7027
                           007026  1509 _ADCCH3VAL	=	0x7026
                           007028  1510 _ADCTUNE0	=	0x7028
                           007029  1511 _ADCTUNE1	=	0x7029
                           00702A  1512 _ADCTUNE2	=	0x702a
                           007010  1513 _DMA0ADDR0	=	0x7010
                           007011  1514 _DMA0ADDR1	=	0x7011
                           007010  1515 _DMA0ADDR	=	0x7010
                           007014  1516 _DMA0CONFIG	=	0x7014
                           007012  1517 _DMA1ADDR0	=	0x7012
                           007013  1518 _DMA1ADDR1	=	0x7013
                           007012  1519 _DMA1ADDR	=	0x7012
                           007015  1520 _DMA1CONFIG	=	0x7015
                           007070  1521 _FRCOSCCONFIG	=	0x7070
                           007071  1522 _FRCOSCCTRL	=	0x7071
                           007076  1523 _FRCOSCFREQ0	=	0x7076
                           007077  1524 _FRCOSCFREQ1	=	0x7077
                           007076  1525 _FRCOSCFREQ	=	0x7076
                           007072  1526 _FRCOSCKFILT0	=	0x7072
                           007073  1527 _FRCOSCKFILT1	=	0x7073
                           007072  1528 _FRCOSCKFILT	=	0x7072
                           007078  1529 _FRCOSCPER0	=	0x7078
                           007079  1530 _FRCOSCPER1	=	0x7079
                           007078  1531 _FRCOSCPER	=	0x7078
                           007074  1532 _FRCOSCREF0	=	0x7074
                           007075  1533 _FRCOSCREF1	=	0x7075
                           007074  1534 _FRCOSCREF	=	0x7074
                           007007  1535 _ANALOGA	=	0x7007
                           00700C  1536 _GPIOENABLE	=	0x700c
                           007003  1537 _EXTIRQ	=	0x7003
                           007000  1538 _INTCHGA	=	0x7000
                           007001  1539 _INTCHGB	=	0x7001
                           007002  1540 _INTCHGC	=	0x7002
                           007008  1541 _PALTA	=	0x7008
                           007009  1542 _PALTB	=	0x7009
                           00700A  1543 _PALTC	=	0x700a
                           007046  1544 _PALTRADIO	=	0x7046
                           007004  1545 _PINCHGA	=	0x7004
                           007005  1546 _PINCHGB	=	0x7005
                           007006  1547 _PINCHGC	=	0x7006
                           00700B  1548 _PINSEL	=	0x700b
                           007060  1549 _LPOSCCONFIG	=	0x7060
                           007066  1550 _LPOSCFREQ0	=	0x7066
                           007067  1551 _LPOSCFREQ1	=	0x7067
                           007066  1552 _LPOSCFREQ	=	0x7066
                           007062  1553 _LPOSCKFILT0	=	0x7062
                           007063  1554 _LPOSCKFILT1	=	0x7063
                           007062  1555 _LPOSCKFILT	=	0x7062
                           007068  1556 _LPOSCPER0	=	0x7068
                           007069  1557 _LPOSCPER1	=	0x7069
                           007068  1558 _LPOSCPER	=	0x7068
                           007064  1559 _LPOSCREF0	=	0x7064
                           007065  1560 _LPOSCREF1	=	0x7065
                           007064  1561 _LPOSCREF	=	0x7064
                           007054  1562 _LPXOSCGM	=	0x7054
                           007F01  1563 _MISCCTRL	=	0x7f01
                           007053  1564 _OSCCALIB	=	0x7053
                           007050  1565 _OSCFORCERUN	=	0x7050
                           007052  1566 _OSCREADY	=	0x7052
                           007051  1567 _OSCRUN	=	0x7051
                           007040  1568 _RADIOFDATAADDR0	=	0x7040
                           007041  1569 _RADIOFDATAADDR1	=	0x7041
                           007040  1570 _RADIOFDATAADDR	=	0x7040
                           007042  1571 _RADIOFSTATADDR0	=	0x7042
                           007043  1572 _RADIOFSTATADDR1	=	0x7043
                           007042  1573 _RADIOFSTATADDR	=	0x7042
                           007044  1574 _RADIOMUX	=	0x7044
                           007084  1575 _SCRATCH0	=	0x7084
                           007085  1576 _SCRATCH1	=	0x7085
                           007086  1577 _SCRATCH2	=	0x7086
                           007087  1578 _SCRATCH3	=	0x7087
                           007F00  1579 _SILICONREV	=	0x7f00
                           007F19  1580 _XTALAMPL	=	0x7f19
                           007F18  1581 _XTALOSC	=	0x7f18
                           007F1A  1582 _XTALREADY	=	0x7f1a
                           003F82  1583 _XDPTR0	=	0x3f82
                           003F84  1584 _XDPTR1	=	0x3f84
                           003FA8  1585 _XIE	=	0x3fa8
                           003FB8  1586 _XIP	=	0x3fb8
                           003F87  1587 _XPCON	=	0x3f87
                           003FCA  1588 _XADCCH0CONFIG	=	0x3fca
                           003FCB  1589 _XADCCH1CONFIG	=	0x3fcb
                           003FD2  1590 _XADCCH2CONFIG	=	0x3fd2
                           003FD3  1591 _XADCCH3CONFIG	=	0x3fd3
                           003FD1  1592 _XADCCLKSRC	=	0x3fd1
                           003FC9  1593 _XADCCONV	=	0x3fc9
                           003FE1  1594 _XANALOGCOMP	=	0x3fe1
                           003FC6  1595 _XCLKCON	=	0x3fc6
                           003FC7  1596 _XCLKSTAT	=	0x3fc7
                           003F97  1597 _XCODECONFIG	=	0x3f97
                           003FE3  1598 _XDBGLNKBUF	=	0x3fe3
                           003FE2  1599 _XDBGLNKSTAT	=	0x3fe2
                           003F89  1600 _XDIRA	=	0x3f89
                           003F8A  1601 _XDIRB	=	0x3f8a
                           003F8B  1602 _XDIRC	=	0x3f8b
                           003F8E  1603 _XDIRR	=	0x3f8e
                           003FC8  1604 _XPINA	=	0x3fc8
                           003FE8  1605 _XPINB	=	0x3fe8
                           003FF8  1606 _XPINC	=	0x3ff8
                           003F8D  1607 _XPINR	=	0x3f8d
                           003F80  1608 _XPORTA	=	0x3f80
                           003F88  1609 _XPORTB	=	0x3f88
                           003F90  1610 _XPORTC	=	0x3f90
                           003F8C  1611 _XPORTR	=	0x3f8c
                           003FCE  1612 _XIC0CAPT0	=	0x3fce
                           003FCF  1613 _XIC0CAPT1	=	0x3fcf
                           003FCE  1614 _XIC0CAPT	=	0x3fce
                           003FCC  1615 _XIC0MODE	=	0x3fcc
                           003FCD  1616 _XIC0STATUS	=	0x3fcd
                           003FD6  1617 _XIC1CAPT0	=	0x3fd6
                           003FD7  1618 _XIC1CAPT1	=	0x3fd7
                           003FD6  1619 _XIC1CAPT	=	0x3fd6
                           003FD4  1620 _XIC1MODE	=	0x3fd4
                           003FD5  1621 _XIC1STATUS	=	0x3fd5
                           003F92  1622 _XNVADDR0	=	0x3f92
                           003F93  1623 _XNVADDR1	=	0x3f93
                           003F92  1624 _XNVADDR	=	0x3f92
                           003F94  1625 _XNVDATA0	=	0x3f94
                           003F95  1626 _XNVDATA1	=	0x3f95
                           003F94  1627 _XNVDATA	=	0x3f94
                           003F96  1628 _XNVKEY	=	0x3f96
                           003F91  1629 _XNVSTATUS	=	0x3f91
                           003FBC  1630 _XOC0COMP0	=	0x3fbc
                           003FBD  1631 _XOC0COMP1	=	0x3fbd
                           003FBC  1632 _XOC0COMP	=	0x3fbc
                           003FB9  1633 _XOC0MODE	=	0x3fb9
                           003FBA  1634 _XOC0PIN	=	0x3fba
                           003FBB  1635 _XOC0STATUS	=	0x3fbb
                           003FC4  1636 _XOC1COMP0	=	0x3fc4
                           003FC5  1637 _XOC1COMP1	=	0x3fc5
                           003FC4  1638 _XOC1COMP	=	0x3fc4
                           003FC1  1639 _XOC1MODE	=	0x3fc1
                           003FC2  1640 _XOC1PIN	=	0x3fc2
                           003FC3  1641 _XOC1STATUS	=	0x3fc3
                           003FB1  1642 _XRADIOACC	=	0x3fb1
                           003FB3  1643 _XRADIOADDR0	=	0x3fb3
                           003FB2  1644 _XRADIOADDR1	=	0x3fb2
                           003FB7  1645 _XRADIODATA0	=	0x3fb7
                           003FB6  1646 _XRADIODATA1	=	0x3fb6
                           003FB5  1647 _XRADIODATA2	=	0x3fb5
                           003FB4  1648 _XRADIODATA3	=	0x3fb4
                           003FBE  1649 _XRADIOSTAT0	=	0x3fbe
                           003FBF  1650 _XRADIOSTAT1	=	0x3fbf
                           003FBE  1651 _XRADIOSTAT	=	0x3fbe
                           003FDF  1652 _XSPCLKSRC	=	0x3fdf
                           003FDC  1653 _XSPMODE	=	0x3fdc
                           003FDE  1654 _XSPSHREG	=	0x3fde
                           003FDD  1655 _XSPSTATUS	=	0x3fdd
                           003F9A  1656 _XT0CLKSRC	=	0x3f9a
                           003F9C  1657 _XT0CNT0	=	0x3f9c
                           003F9D  1658 _XT0CNT1	=	0x3f9d
                           003F9C  1659 _XT0CNT	=	0x3f9c
                           003F99  1660 _XT0MODE	=	0x3f99
                           003F9E  1661 _XT0PERIOD0	=	0x3f9e
                           003F9F  1662 _XT0PERIOD1	=	0x3f9f
                           003F9E  1663 _XT0PERIOD	=	0x3f9e
                           003F9B  1664 _XT0STATUS	=	0x3f9b
                           003FA2  1665 _XT1CLKSRC	=	0x3fa2
                           003FA4  1666 _XT1CNT0	=	0x3fa4
                           003FA5  1667 _XT1CNT1	=	0x3fa5
                           003FA4  1668 _XT1CNT	=	0x3fa4
                           003FA1  1669 _XT1MODE	=	0x3fa1
                           003FA6  1670 _XT1PERIOD0	=	0x3fa6
                           003FA7  1671 _XT1PERIOD1	=	0x3fa7
                           003FA6  1672 _XT1PERIOD	=	0x3fa6
                           003FA3  1673 _XT1STATUS	=	0x3fa3
                           003FAA  1674 _XT2CLKSRC	=	0x3faa
                           003FAC  1675 _XT2CNT0	=	0x3fac
                           003FAD  1676 _XT2CNT1	=	0x3fad
                           003FAC  1677 _XT2CNT	=	0x3fac
                           003FA9  1678 _XT2MODE	=	0x3fa9
                           003FAE  1679 _XT2PERIOD0	=	0x3fae
                           003FAF  1680 _XT2PERIOD1	=	0x3faf
                           003FAE  1681 _XT2PERIOD	=	0x3fae
                           003FAB  1682 _XT2STATUS	=	0x3fab
                           003FE4  1683 _XU0CTRL	=	0x3fe4
                           003FE7  1684 _XU0MODE	=	0x3fe7
                           003FE6  1685 _XU0SHREG	=	0x3fe6
                           003FE5  1686 _XU0STATUS	=	0x3fe5
                           003FEC  1687 _XU1CTRL	=	0x3fec
                           003FEF  1688 _XU1MODE	=	0x3fef
                           003FEE  1689 _XU1SHREG	=	0x3fee
                           003FED  1690 _XU1STATUS	=	0x3fed
                           003FDA  1691 _XWDTCFG	=	0x3fda
                           003FDB  1692 _XWDTRESET	=	0x3fdb
                           003FF1  1693 _XWTCFGA	=	0x3ff1
                           003FF9  1694 _XWTCFGB	=	0x3ff9
                           003FF2  1695 _XWTCNTA0	=	0x3ff2
                           003FF3  1696 _XWTCNTA1	=	0x3ff3
                           003FF2  1697 _XWTCNTA	=	0x3ff2
                           003FFA  1698 _XWTCNTB0	=	0x3ffa
                           003FFB  1699 _XWTCNTB1	=	0x3ffb
                           003FFA  1700 _XWTCNTB	=	0x3ffa
                           003FEB  1701 _XWTCNTR1	=	0x3feb
                           003FF4  1702 _XWTEVTA0	=	0x3ff4
                           003FF5  1703 _XWTEVTA1	=	0x3ff5
                           003FF4  1704 _XWTEVTA	=	0x3ff4
                           003FF6  1705 _XWTEVTB0	=	0x3ff6
                           003FF7  1706 _XWTEVTB1	=	0x3ff7
                           003FF6  1707 _XWTEVTB	=	0x3ff6
                           003FFC  1708 _XWTEVTC0	=	0x3ffc
                           003FFD  1709 _XWTEVTC1	=	0x3ffd
                           003FFC  1710 _XWTEVTC	=	0x3ffc
                           003FFE  1711 _XWTEVTD0	=	0x3ffe
                           003FFF  1712 _XWTEVTD1	=	0x3fff
                           003FFE  1713 _XWTEVTD	=	0x3ffe
                           003FE9  1714 _XWTIRQEN	=	0x3fe9
                           003FEA  1715 _XWTSTAT	=	0x3fea
                           004114  1716 _AX5043_AFSKCTRL	=	0x4114
                           004113  1717 _AX5043_AFSKMARK0	=	0x4113
                           004112  1718 _AX5043_AFSKMARK1	=	0x4112
                           004111  1719 _AX5043_AFSKSPACE0	=	0x4111
                           004110  1720 _AX5043_AFSKSPACE1	=	0x4110
                           004043  1721 _AX5043_AGCCOUNTER	=	0x4043
                           004115  1722 _AX5043_AMPLFILTER	=	0x4115
                           004189  1723 _AX5043_BBOFFSCAP	=	0x4189
                           004188  1724 _AX5043_BBTUNE	=	0x4188
                           004041  1725 _AX5043_BGNDRSSI	=	0x4041
                           00422E  1726 _AX5043_BGNDRSSIGAIN	=	0x422e
                           00422F  1727 _AX5043_BGNDRSSITHR	=	0x422f
                           004017  1728 _AX5043_CRCINIT0	=	0x4017
                           004016  1729 _AX5043_CRCINIT1	=	0x4016
                           004015  1730 _AX5043_CRCINIT2	=	0x4015
                           004014  1731 _AX5043_CRCINIT3	=	0x4014
                           004332  1732 _AX5043_DACCONFIG	=	0x4332
                           004331  1733 _AX5043_DACVALUE0	=	0x4331
                           004330  1734 _AX5043_DACVALUE1	=	0x4330
                           004102  1735 _AX5043_DECIMATION	=	0x4102
                           004042  1736 _AX5043_DIVERSITY	=	0x4042
                           004011  1737 _AX5043_ENCODING	=	0x4011
                           004018  1738 _AX5043_FEC	=	0x4018
                           00401A  1739 _AX5043_FECSTATUS	=	0x401a
                           004019  1740 _AX5043_FECSYNC	=	0x4019
                           00402B  1741 _AX5043_FIFOCOUNT0	=	0x402b
                           00402A  1742 _AX5043_FIFOCOUNT1	=	0x402a
                           004029  1743 _AX5043_FIFODATA	=	0x4029
                           00402D  1744 _AX5043_FIFOFREE0	=	0x402d
                           00402C  1745 _AX5043_FIFOFREE1	=	0x402c
                           004028  1746 _AX5043_FIFOSTAT	=	0x4028
                           00402F  1747 _AX5043_FIFOTHRESH0	=	0x402f
                           00402E  1748 _AX5043_FIFOTHRESH1	=	0x402e
                           004012  1749 _AX5043_FRAMING	=	0x4012
                           004037  1750 _AX5043_FREQA0	=	0x4037
                           004036  1751 _AX5043_FREQA1	=	0x4036
                           004035  1752 _AX5043_FREQA2	=	0x4035
                           004034  1753 _AX5043_FREQA3	=	0x4034
                           00403F  1754 _AX5043_FREQB0	=	0x403f
                           00403E  1755 _AX5043_FREQB1	=	0x403e
                           00403D  1756 _AX5043_FREQB2	=	0x403d
                           00403C  1757 _AX5043_FREQB3	=	0x403c
                           004163  1758 _AX5043_FSKDEV0	=	0x4163
                           004162  1759 _AX5043_FSKDEV1	=	0x4162
                           004161  1760 _AX5043_FSKDEV2	=	0x4161
                           00410D  1761 _AX5043_FSKDMAX0	=	0x410d
                           00410C  1762 _AX5043_FSKDMAX1	=	0x410c
                           00410F  1763 _AX5043_FSKDMIN0	=	0x410f
                           00410E  1764 _AX5043_FSKDMIN1	=	0x410e
                           004309  1765 _AX5043_GPADC13VALUE0	=	0x4309
                           004308  1766 _AX5043_GPADC13VALUE1	=	0x4308
                           004300  1767 _AX5043_GPADCCTRL	=	0x4300
                           004301  1768 _AX5043_GPADCPERIOD	=	0x4301
                           004101  1769 _AX5043_IFFREQ0	=	0x4101
                           004100  1770 _AX5043_IFFREQ1	=	0x4100
                           00400B  1771 _AX5043_IRQINVERSION0	=	0x400b
                           00400A  1772 _AX5043_IRQINVERSION1	=	0x400a
                           004007  1773 _AX5043_IRQMASK0	=	0x4007
                           004006  1774 _AX5043_IRQMASK1	=	0x4006
                           00400D  1775 _AX5043_IRQREQUEST0	=	0x400d
                           00400C  1776 _AX5043_IRQREQUEST1	=	0x400c
                           004310  1777 _AX5043_LPOSCCONFIG	=	0x4310
                           004317  1778 _AX5043_LPOSCFREQ0	=	0x4317
                           004316  1779 _AX5043_LPOSCFREQ1	=	0x4316
                           004313  1780 _AX5043_LPOSCKFILT0	=	0x4313
                           004312  1781 _AX5043_LPOSCKFILT1	=	0x4312
                           004319  1782 _AX5043_LPOSCPER0	=	0x4319
                           004318  1783 _AX5043_LPOSCPER1	=	0x4318
                           004315  1784 _AX5043_LPOSCREF0	=	0x4315
                           004314  1785 _AX5043_LPOSCREF1	=	0x4314
                           004311  1786 _AX5043_LPOSCSTATUS	=	0x4311
                           004214  1787 _AX5043_MATCH0LEN	=	0x4214
                           004216  1788 _AX5043_MATCH0MAX	=	0x4216
                           004215  1789 _AX5043_MATCH0MIN	=	0x4215
                           004213  1790 _AX5043_MATCH0PAT0	=	0x4213
                           004212  1791 _AX5043_MATCH0PAT1	=	0x4212
                           004211  1792 _AX5043_MATCH0PAT2	=	0x4211
                           004210  1793 _AX5043_MATCH0PAT3	=	0x4210
                           00421C  1794 _AX5043_MATCH1LEN	=	0x421c
                           00421E  1795 _AX5043_MATCH1MAX	=	0x421e
                           00421D  1796 _AX5043_MATCH1MIN	=	0x421d
                           004219  1797 _AX5043_MATCH1PAT0	=	0x4219
                           004218  1798 _AX5043_MATCH1PAT1	=	0x4218
                           004108  1799 _AX5043_MAXDROFFSET0	=	0x4108
                           004107  1800 _AX5043_MAXDROFFSET1	=	0x4107
                           004106  1801 _AX5043_MAXDROFFSET2	=	0x4106
                           00410B  1802 _AX5043_MAXRFOFFSET0	=	0x410b
                           00410A  1803 _AX5043_MAXRFOFFSET1	=	0x410a
                           004109  1804 _AX5043_MAXRFOFFSET2	=	0x4109
                           004164  1805 _AX5043_MODCFGA	=	0x4164
                           004160  1806 _AX5043_MODCFGF	=	0x4160
                           004010  1807 _AX5043_MODULATION	=	0x4010
                           004025  1808 _AX5043_PINFUNCANTSEL	=	0x4025
                           004023  1809 _AX5043_PINFUNCDATA	=	0x4023
                           004022  1810 _AX5043_PINFUNCDCLK	=	0x4022
                           004024  1811 _AX5043_PINFUNCIRQ	=	0x4024
                           004026  1812 _AX5043_PINFUNCPWRAMP	=	0x4026
                           004021  1813 _AX5043_PINFUNCSYSCLK	=	0x4021
                           004020  1814 _AX5043_PINSTATE	=	0x4020
                           004233  1815 _AX5043_PKTACCEPTFLAGS	=	0x4233
                           004230  1816 _AX5043_PKTCHUNKSIZE	=	0x4230
                           004231  1817 _AX5043_PKTMISCFLAGS	=	0x4231
                           004232  1818 _AX5043_PKTSTOREFLAGS	=	0x4232
                           004031  1819 _AX5043_PLLCPI	=	0x4031
                           004039  1820 _AX5043_PLLCPIBOOST	=	0x4039
                           004182  1821 _AX5043_PLLLOCKDET	=	0x4182
                           004030  1822 _AX5043_PLLLOOP	=	0x4030
                           004038  1823 _AX5043_PLLLOOPBOOST	=	0x4038
                           004033  1824 _AX5043_PLLRANGINGA	=	0x4033
                           00403B  1825 _AX5043_PLLRANGINGB	=	0x403b
                           004183  1826 _AX5043_PLLRNGCLK	=	0x4183
                           004032  1827 _AX5043_PLLVCODIV	=	0x4032
                           004180  1828 _AX5043_PLLVCOI	=	0x4180
                           004181  1829 _AX5043_PLLVCOIR	=	0x4181
                           004005  1830 _AX5043_POWIRQMASK	=	0x4005
                           004003  1831 _AX5043_POWSTAT	=	0x4003
                           004004  1832 _AX5043_POWSTICKYSTAT	=	0x4004
                           004027  1833 _AX5043_PWRAMP	=	0x4027
                           004002  1834 _AX5043_PWRMODE	=	0x4002
                           004009  1835 _AX5043_RADIOEVENTMASK0	=	0x4009
                           004008  1836 _AX5043_RADIOEVENTMASK1	=	0x4008
                           00400F  1837 _AX5043_RADIOEVENTREQ0	=	0x400f
                           00400E  1838 _AX5043_RADIOEVENTREQ1	=	0x400e
                           00401C  1839 _AX5043_RADIOSTATE	=	0x401c
                           004040  1840 _AX5043_RSSI	=	0x4040
                           00422D  1841 _AX5043_RSSIABSTHR	=	0x422d
                           00422C  1842 _AX5043_RSSIREFERENCE	=	0x422c
                           004105  1843 _AX5043_RXDATARATE0	=	0x4105
                           004104  1844 _AX5043_RXDATARATE1	=	0x4104
                           004103  1845 _AX5043_RXDATARATE2	=	0x4103
                           004001  1846 _AX5043_SCRATCH	=	0x4001
                           004000  1847 _AX5043_SILICONREVISION	=	0x4000
                           00405B  1848 _AX5043_TIMER0	=	0x405b
                           00405A  1849 _AX5043_TIMER1	=	0x405a
                           004059  1850 _AX5043_TIMER2	=	0x4059
                           004227  1851 _AX5043_TMGRXAGC	=	0x4227
                           004223  1852 _AX5043_TMGRXBOOST	=	0x4223
                           004226  1853 _AX5043_TMGRXCOARSEAGC	=	0x4226
                           004225  1854 _AX5043_TMGRXOFFSACQ	=	0x4225
                           004229  1855 _AX5043_TMGRXPREAMBLE1	=	0x4229
                           00422A  1856 _AX5043_TMGRXPREAMBLE2	=	0x422a
                           00422B  1857 _AX5043_TMGRXPREAMBLE3	=	0x422b
                           004228  1858 _AX5043_TMGRXRSSI	=	0x4228
                           004224  1859 _AX5043_TMGRXSETTLE	=	0x4224
                           004220  1860 _AX5043_TMGTXBOOST	=	0x4220
                           004221  1861 _AX5043_TMGTXSETTLE	=	0x4221
                           004055  1862 _AX5043_TRKAFSKDEMOD0	=	0x4055
                           004054  1863 _AX5043_TRKAFSKDEMOD1	=	0x4054
                           004049  1864 _AX5043_TRKAMPLITUDE0	=	0x4049
                           004048  1865 _AX5043_TRKAMPLITUDE1	=	0x4048
                           004047  1866 _AX5043_TRKDATARATE0	=	0x4047
                           004046  1867 _AX5043_TRKDATARATE1	=	0x4046
                           004045  1868 _AX5043_TRKDATARATE2	=	0x4045
                           004051  1869 _AX5043_TRKFREQ0	=	0x4051
                           004050  1870 _AX5043_TRKFREQ1	=	0x4050
                           004053  1871 _AX5043_TRKFSKDEMOD0	=	0x4053
                           004052  1872 _AX5043_TRKFSKDEMOD1	=	0x4052
                           00404B  1873 _AX5043_TRKPHASE0	=	0x404b
                           00404A  1874 _AX5043_TRKPHASE1	=	0x404a
                           00404F  1875 _AX5043_TRKRFFREQ0	=	0x404f
                           00404E  1876 _AX5043_TRKRFFREQ1	=	0x404e
                           00404D  1877 _AX5043_TRKRFFREQ2	=	0x404d
                           004169  1878 _AX5043_TXPWRCOEFFA0	=	0x4169
                           004168  1879 _AX5043_TXPWRCOEFFA1	=	0x4168
                           00416B  1880 _AX5043_TXPWRCOEFFB0	=	0x416b
                           00416A  1881 _AX5043_TXPWRCOEFFB1	=	0x416a
                           00416D  1882 _AX5043_TXPWRCOEFFC0	=	0x416d
                           00416C  1883 _AX5043_TXPWRCOEFFC1	=	0x416c
                           00416F  1884 _AX5043_TXPWRCOEFFD0	=	0x416f
                           00416E  1885 _AX5043_TXPWRCOEFFD1	=	0x416e
                           004171  1886 _AX5043_TXPWRCOEFFE0	=	0x4171
                           004170  1887 _AX5043_TXPWRCOEFFE1	=	0x4170
                           004167  1888 _AX5043_TXRATE0	=	0x4167
                           004166  1889 _AX5043_TXRATE1	=	0x4166
                           004165  1890 _AX5043_TXRATE2	=	0x4165
                           00406B  1891 _AX5043_WAKEUP0	=	0x406b
                           00406A  1892 _AX5043_WAKEUP1	=	0x406a
                           00406D  1893 _AX5043_WAKEUPFREQ0	=	0x406d
                           00406C  1894 _AX5043_WAKEUPFREQ1	=	0x406c
                           004069  1895 _AX5043_WAKEUPTIMER0	=	0x4069
                           004068  1896 _AX5043_WAKEUPTIMER1	=	0x4068
                           00406E  1897 _AX5043_WAKEUPXOEARLY	=	0x406e
                           004184  1898 _AX5043_XTALCAP	=	0x4184
                           00401D  1899 _AX5043_XTALSTATUS	=	0x401d
                           004122  1900 _AX5043_AGCAHYST0	=	0x4122
                           004132  1901 _AX5043_AGCAHYST1	=	0x4132
                           004142  1902 _AX5043_AGCAHYST2	=	0x4142
                           004152  1903 _AX5043_AGCAHYST3	=	0x4152
                           004120  1904 _AX5043_AGCGAIN0	=	0x4120
                           004130  1905 _AX5043_AGCGAIN1	=	0x4130
                           004140  1906 _AX5043_AGCGAIN2	=	0x4140
                           004150  1907 _AX5043_AGCGAIN3	=	0x4150
                           004123  1908 _AX5043_AGCMINMAX0	=	0x4123
                           004133  1909 _AX5043_AGCMINMAX1	=	0x4133
                           004143  1910 _AX5043_AGCMINMAX2	=	0x4143
                           004153  1911 _AX5043_AGCMINMAX3	=	0x4153
                           004121  1912 _AX5043_AGCTARGET0	=	0x4121
                           004131  1913 _AX5043_AGCTARGET1	=	0x4131
                           004141  1914 _AX5043_AGCTARGET2	=	0x4141
                           004151  1915 _AX5043_AGCTARGET3	=	0x4151
                           00412B  1916 _AX5043_AMPLITUDEGAIN0	=	0x412b
                           00413B  1917 _AX5043_AMPLITUDEGAIN1	=	0x413b
                           00414B  1918 _AX5043_AMPLITUDEGAIN2	=	0x414b
                           00415B  1919 _AX5043_AMPLITUDEGAIN3	=	0x415b
                           00412F  1920 _AX5043_BBOFFSRES0	=	0x412f
                           00413F  1921 _AX5043_BBOFFSRES1	=	0x413f
                           00414F  1922 _AX5043_BBOFFSRES2	=	0x414f
                           00415F  1923 _AX5043_BBOFFSRES3	=	0x415f
                           004125  1924 _AX5043_DRGAIN0	=	0x4125
                           004135  1925 _AX5043_DRGAIN1	=	0x4135
                           004145  1926 _AX5043_DRGAIN2	=	0x4145
                           004155  1927 _AX5043_DRGAIN3	=	0x4155
                           00412E  1928 _AX5043_FOURFSK0	=	0x412e
                           00413E  1929 _AX5043_FOURFSK1	=	0x413e
                           00414E  1930 _AX5043_FOURFSK2	=	0x414e
                           00415E  1931 _AX5043_FOURFSK3	=	0x415e
                           00412D  1932 _AX5043_FREQDEV00	=	0x412d
                           00413D  1933 _AX5043_FREQDEV01	=	0x413d
                           00414D  1934 _AX5043_FREQDEV02	=	0x414d
                           00415D  1935 _AX5043_FREQDEV03	=	0x415d
                           00412C  1936 _AX5043_FREQDEV10	=	0x412c
                           00413C  1937 _AX5043_FREQDEV11	=	0x413c
                           00414C  1938 _AX5043_FREQDEV12	=	0x414c
                           00415C  1939 _AX5043_FREQDEV13	=	0x415c
                           004127  1940 _AX5043_FREQUENCYGAINA0	=	0x4127
                           004137  1941 _AX5043_FREQUENCYGAINA1	=	0x4137
                           004147  1942 _AX5043_FREQUENCYGAINA2	=	0x4147
                           004157  1943 _AX5043_FREQUENCYGAINA3	=	0x4157
                           004128  1944 _AX5043_FREQUENCYGAINB0	=	0x4128
                           004138  1945 _AX5043_FREQUENCYGAINB1	=	0x4138
                           004148  1946 _AX5043_FREQUENCYGAINB2	=	0x4148
                           004158  1947 _AX5043_FREQUENCYGAINB3	=	0x4158
                           004129  1948 _AX5043_FREQUENCYGAINC0	=	0x4129
                           004139  1949 _AX5043_FREQUENCYGAINC1	=	0x4139
                           004149  1950 _AX5043_FREQUENCYGAINC2	=	0x4149
                           004159  1951 _AX5043_FREQUENCYGAINC3	=	0x4159
                           00412A  1952 _AX5043_FREQUENCYGAIND0	=	0x412a
                           00413A  1953 _AX5043_FREQUENCYGAIND1	=	0x413a
                           00414A  1954 _AX5043_FREQUENCYGAIND2	=	0x414a
                           00415A  1955 _AX5043_FREQUENCYGAIND3	=	0x415a
                           004116  1956 _AX5043_FREQUENCYLEAK	=	0x4116
                           004126  1957 _AX5043_PHASEGAIN0	=	0x4126
                           004136  1958 _AX5043_PHASEGAIN1	=	0x4136
                           004146  1959 _AX5043_PHASEGAIN2	=	0x4146
                           004156  1960 _AX5043_PHASEGAIN3	=	0x4156
                           004207  1961 _AX5043_PKTADDR0	=	0x4207
                           004206  1962 _AX5043_PKTADDR1	=	0x4206
                           004205  1963 _AX5043_PKTADDR2	=	0x4205
                           004204  1964 _AX5043_PKTADDR3	=	0x4204
                           004200  1965 _AX5043_PKTADDRCFG	=	0x4200
                           00420B  1966 _AX5043_PKTADDRMASK0	=	0x420b
                           00420A  1967 _AX5043_PKTADDRMASK1	=	0x420a
                           004209  1968 _AX5043_PKTADDRMASK2	=	0x4209
                           004208  1969 _AX5043_PKTADDRMASK3	=	0x4208
                           004201  1970 _AX5043_PKTLENCFG	=	0x4201
                           004202  1971 _AX5043_PKTLENOFFSET	=	0x4202
                           004203  1972 _AX5043_PKTMAXLEN	=	0x4203
                           004118  1973 _AX5043_RXPARAMCURSET	=	0x4118
                           004117  1974 _AX5043_RXPARAMSETS	=	0x4117
                           004124  1975 _AX5043_TIMEGAIN0	=	0x4124
                           004134  1976 _AX5043_TIMEGAIN1	=	0x4134
                           004144  1977 _AX5043_TIMEGAIN2	=	0x4144
                           004154  1978 _AX5043_TIMEGAIN3	=	0x4154
                           005114  1979 _AX5043_AFSKCTRLNB	=	0x5114
                           005113  1980 _AX5043_AFSKMARK0NB	=	0x5113
                           005112  1981 _AX5043_AFSKMARK1NB	=	0x5112
                           005111  1982 _AX5043_AFSKSPACE0NB	=	0x5111
                           005110  1983 _AX5043_AFSKSPACE1NB	=	0x5110
                           005043  1984 _AX5043_AGCCOUNTERNB	=	0x5043
                           005115  1985 _AX5043_AMPLFILTERNB	=	0x5115
                           005189  1986 _AX5043_BBOFFSCAPNB	=	0x5189
                           005188  1987 _AX5043_BBTUNENB	=	0x5188
                           005041  1988 _AX5043_BGNDRSSINB	=	0x5041
                           00522E  1989 _AX5043_BGNDRSSIGAINNB	=	0x522e
                           00522F  1990 _AX5043_BGNDRSSITHRNB	=	0x522f
                           005017  1991 _AX5043_CRCINIT0NB	=	0x5017
                           005016  1992 _AX5043_CRCINIT1NB	=	0x5016
                           005015  1993 _AX5043_CRCINIT2NB	=	0x5015
                           005014  1994 _AX5043_CRCINIT3NB	=	0x5014
                           005332  1995 _AX5043_DACCONFIGNB	=	0x5332
                           005331  1996 _AX5043_DACVALUE0NB	=	0x5331
                           005330  1997 _AX5043_DACVALUE1NB	=	0x5330
                           005102  1998 _AX5043_DECIMATIONNB	=	0x5102
                           005042  1999 _AX5043_DIVERSITYNB	=	0x5042
                           005011  2000 _AX5043_ENCODINGNB	=	0x5011
                           005018  2001 _AX5043_FECNB	=	0x5018
                           00501A  2002 _AX5043_FECSTATUSNB	=	0x501a
                           005019  2003 _AX5043_FECSYNCNB	=	0x5019
                           00502B  2004 _AX5043_FIFOCOUNT0NB	=	0x502b
                           00502A  2005 _AX5043_FIFOCOUNT1NB	=	0x502a
                           005029  2006 _AX5043_FIFODATANB	=	0x5029
                           00502D  2007 _AX5043_FIFOFREE0NB	=	0x502d
                           00502C  2008 _AX5043_FIFOFREE1NB	=	0x502c
                           005028  2009 _AX5043_FIFOSTATNB	=	0x5028
                           00502F  2010 _AX5043_FIFOTHRESH0NB	=	0x502f
                           00502E  2011 _AX5043_FIFOTHRESH1NB	=	0x502e
                           005012  2012 _AX5043_FRAMINGNB	=	0x5012
                           005037  2013 _AX5043_FREQA0NB	=	0x5037
                           005036  2014 _AX5043_FREQA1NB	=	0x5036
                           005035  2015 _AX5043_FREQA2NB	=	0x5035
                           005034  2016 _AX5043_FREQA3NB	=	0x5034
                           00503F  2017 _AX5043_FREQB0NB	=	0x503f
                           00503E  2018 _AX5043_FREQB1NB	=	0x503e
                           00503D  2019 _AX5043_FREQB2NB	=	0x503d
                           00503C  2020 _AX5043_FREQB3NB	=	0x503c
                           005163  2021 _AX5043_FSKDEV0NB	=	0x5163
                           005162  2022 _AX5043_FSKDEV1NB	=	0x5162
                           005161  2023 _AX5043_FSKDEV2NB	=	0x5161
                           00510D  2024 _AX5043_FSKDMAX0NB	=	0x510d
                           00510C  2025 _AX5043_FSKDMAX1NB	=	0x510c
                           00510F  2026 _AX5043_FSKDMIN0NB	=	0x510f
                           00510E  2027 _AX5043_FSKDMIN1NB	=	0x510e
                           005309  2028 _AX5043_GPADC13VALUE0NB	=	0x5309
                           005308  2029 _AX5043_GPADC13VALUE1NB	=	0x5308
                           005300  2030 _AX5043_GPADCCTRLNB	=	0x5300
                           005301  2031 _AX5043_GPADCPERIODNB	=	0x5301
                           005101  2032 _AX5043_IFFREQ0NB	=	0x5101
                           005100  2033 _AX5043_IFFREQ1NB	=	0x5100
                           00500B  2034 _AX5043_IRQINVERSION0NB	=	0x500b
                           00500A  2035 _AX5043_IRQINVERSION1NB	=	0x500a
                           005007  2036 _AX5043_IRQMASK0NB	=	0x5007
                           005006  2037 _AX5043_IRQMASK1NB	=	0x5006
                           00500D  2038 _AX5043_IRQREQUEST0NB	=	0x500d
                           00500C  2039 _AX5043_IRQREQUEST1NB	=	0x500c
                           005310  2040 _AX5043_LPOSCCONFIGNB	=	0x5310
                           005317  2041 _AX5043_LPOSCFREQ0NB	=	0x5317
                           005316  2042 _AX5043_LPOSCFREQ1NB	=	0x5316
                           005313  2043 _AX5043_LPOSCKFILT0NB	=	0x5313
                           005312  2044 _AX5043_LPOSCKFILT1NB	=	0x5312
                           005319  2045 _AX5043_LPOSCPER0NB	=	0x5319
                           005318  2046 _AX5043_LPOSCPER1NB	=	0x5318
                           005315  2047 _AX5043_LPOSCREF0NB	=	0x5315
                           005314  2048 _AX5043_LPOSCREF1NB	=	0x5314
                           005311  2049 _AX5043_LPOSCSTATUSNB	=	0x5311
                           005214  2050 _AX5043_MATCH0LENNB	=	0x5214
                           005216  2051 _AX5043_MATCH0MAXNB	=	0x5216
                           005215  2052 _AX5043_MATCH0MINNB	=	0x5215
                           005213  2053 _AX5043_MATCH0PAT0NB	=	0x5213
                           005212  2054 _AX5043_MATCH0PAT1NB	=	0x5212
                           005211  2055 _AX5043_MATCH0PAT2NB	=	0x5211
                           005210  2056 _AX5043_MATCH0PAT3NB	=	0x5210
                           00521C  2057 _AX5043_MATCH1LENNB	=	0x521c
                           00521E  2058 _AX5043_MATCH1MAXNB	=	0x521e
                           00521D  2059 _AX5043_MATCH1MINNB	=	0x521d
                           005219  2060 _AX5043_MATCH1PAT0NB	=	0x5219
                           005218  2061 _AX5043_MATCH1PAT1NB	=	0x5218
                           005108  2062 _AX5043_MAXDROFFSET0NB	=	0x5108
                           005107  2063 _AX5043_MAXDROFFSET1NB	=	0x5107
                           005106  2064 _AX5043_MAXDROFFSET2NB	=	0x5106
                           00510B  2065 _AX5043_MAXRFOFFSET0NB	=	0x510b
                           00510A  2066 _AX5043_MAXRFOFFSET1NB	=	0x510a
                           005109  2067 _AX5043_MAXRFOFFSET2NB	=	0x5109
                           005164  2068 _AX5043_MODCFGANB	=	0x5164
                           005160  2069 _AX5043_MODCFGFNB	=	0x5160
                           005F5F  2070 _AX5043_MODCFGPNB	=	0x5f5f
                           005010  2071 _AX5043_MODULATIONNB	=	0x5010
                           005025  2072 _AX5043_PINFUNCANTSELNB	=	0x5025
                           005023  2073 _AX5043_PINFUNCDATANB	=	0x5023
                           005022  2074 _AX5043_PINFUNCDCLKNB	=	0x5022
                           005024  2075 _AX5043_PINFUNCIRQNB	=	0x5024
                           005026  2076 _AX5043_PINFUNCPWRAMPNB	=	0x5026
                           005021  2077 _AX5043_PINFUNCSYSCLKNB	=	0x5021
                           005020  2078 _AX5043_PINSTATENB	=	0x5020
                           005233  2079 _AX5043_PKTACCEPTFLAGSNB	=	0x5233
                           005230  2080 _AX5043_PKTCHUNKSIZENB	=	0x5230
                           005231  2081 _AX5043_PKTMISCFLAGSNB	=	0x5231
                           005232  2082 _AX5043_PKTSTOREFLAGSNB	=	0x5232
                           005031  2083 _AX5043_PLLCPINB	=	0x5031
                           005039  2084 _AX5043_PLLCPIBOOSTNB	=	0x5039
                           005182  2085 _AX5043_PLLLOCKDETNB	=	0x5182
                           005030  2086 _AX5043_PLLLOOPNB	=	0x5030
                           005038  2087 _AX5043_PLLLOOPBOOSTNB	=	0x5038
                           005033  2088 _AX5043_PLLRANGINGANB	=	0x5033
                           00503B  2089 _AX5043_PLLRANGINGBNB	=	0x503b
                           005183  2090 _AX5043_PLLRNGCLKNB	=	0x5183
                           005032  2091 _AX5043_PLLVCODIVNB	=	0x5032
                           005180  2092 _AX5043_PLLVCOINB	=	0x5180
                           005181  2093 _AX5043_PLLVCOIRNB	=	0x5181
                           005F08  2094 _AX5043_POWCTRL1NB	=	0x5f08
                           005005  2095 _AX5043_POWIRQMASKNB	=	0x5005
                           005003  2096 _AX5043_POWSTATNB	=	0x5003
                           005004  2097 _AX5043_POWSTICKYSTATNB	=	0x5004
                           005027  2098 _AX5043_PWRAMPNB	=	0x5027
                           005002  2099 _AX5043_PWRMODENB	=	0x5002
                           005009  2100 _AX5043_RADIOEVENTMASK0NB	=	0x5009
                           005008  2101 _AX5043_RADIOEVENTMASK1NB	=	0x5008
                           00500F  2102 _AX5043_RADIOEVENTREQ0NB	=	0x500f
                           00500E  2103 _AX5043_RADIOEVENTREQ1NB	=	0x500e
                           00501C  2104 _AX5043_RADIOSTATENB	=	0x501c
                           005F0D  2105 _AX5043_REFNB	=	0x5f0d
                           005040  2106 _AX5043_RSSINB	=	0x5040
                           00522D  2107 _AX5043_RSSIABSTHRNB	=	0x522d
                           00522C  2108 _AX5043_RSSIREFERENCENB	=	0x522c
                           005105  2109 _AX5043_RXDATARATE0NB	=	0x5105
                           005104  2110 _AX5043_RXDATARATE1NB	=	0x5104
                           005103  2111 _AX5043_RXDATARATE2NB	=	0x5103
                           005001  2112 _AX5043_SCRATCHNB	=	0x5001
                           005000  2113 _AX5043_SILICONREVISIONNB	=	0x5000
                           00505B  2114 _AX5043_TIMER0NB	=	0x505b
                           00505A  2115 _AX5043_TIMER1NB	=	0x505a
                           005059  2116 _AX5043_TIMER2NB	=	0x5059
                           005227  2117 _AX5043_TMGRXAGCNB	=	0x5227
                           005223  2118 _AX5043_TMGRXBOOSTNB	=	0x5223
                           005226  2119 _AX5043_TMGRXCOARSEAGCNB	=	0x5226
                           005225  2120 _AX5043_TMGRXOFFSACQNB	=	0x5225
                           005229  2121 _AX5043_TMGRXPREAMBLE1NB	=	0x5229
                           00522A  2122 _AX5043_TMGRXPREAMBLE2NB	=	0x522a
                           00522B  2123 _AX5043_TMGRXPREAMBLE3NB	=	0x522b
                           005228  2124 _AX5043_TMGRXRSSINB	=	0x5228
                           005224  2125 _AX5043_TMGRXSETTLENB	=	0x5224
                           005220  2126 _AX5043_TMGTXBOOSTNB	=	0x5220
                           005221  2127 _AX5043_TMGTXSETTLENB	=	0x5221
                           005055  2128 _AX5043_TRKAFSKDEMOD0NB	=	0x5055
                           005054  2129 _AX5043_TRKAFSKDEMOD1NB	=	0x5054
                           005049  2130 _AX5043_TRKAMPLITUDE0NB	=	0x5049
                           005048  2131 _AX5043_TRKAMPLITUDE1NB	=	0x5048
                           005047  2132 _AX5043_TRKDATARATE0NB	=	0x5047
                           005046  2133 _AX5043_TRKDATARATE1NB	=	0x5046
                           005045  2134 _AX5043_TRKDATARATE2NB	=	0x5045
                           005051  2135 _AX5043_TRKFREQ0NB	=	0x5051
                           005050  2136 _AX5043_TRKFREQ1NB	=	0x5050
                           005053  2137 _AX5043_TRKFSKDEMOD0NB	=	0x5053
                           005052  2138 _AX5043_TRKFSKDEMOD1NB	=	0x5052
                           00504B  2139 _AX5043_TRKPHASE0NB	=	0x504b
                           00504A  2140 _AX5043_TRKPHASE1NB	=	0x504a
                           00504F  2141 _AX5043_TRKRFFREQ0NB	=	0x504f
                           00504E  2142 _AX5043_TRKRFFREQ1NB	=	0x504e
                           00504D  2143 _AX5043_TRKRFFREQ2NB	=	0x504d
                           005169  2144 _AX5043_TXPWRCOEFFA0NB	=	0x5169
                           005168  2145 _AX5043_TXPWRCOEFFA1NB	=	0x5168
                           00516B  2146 _AX5043_TXPWRCOEFFB0NB	=	0x516b
                           00516A  2147 _AX5043_TXPWRCOEFFB1NB	=	0x516a
                           00516D  2148 _AX5043_TXPWRCOEFFC0NB	=	0x516d
                           00516C  2149 _AX5043_TXPWRCOEFFC1NB	=	0x516c
                           00516F  2150 _AX5043_TXPWRCOEFFD0NB	=	0x516f
                           00516E  2151 _AX5043_TXPWRCOEFFD1NB	=	0x516e
                           005171  2152 _AX5043_TXPWRCOEFFE0NB	=	0x5171
                           005170  2153 _AX5043_TXPWRCOEFFE1NB	=	0x5170
                           005167  2154 _AX5043_TXRATE0NB	=	0x5167
                           005166  2155 _AX5043_TXRATE1NB	=	0x5166
                           005165  2156 _AX5043_TXRATE2NB	=	0x5165
                           00506B  2157 _AX5043_WAKEUP0NB	=	0x506b
                           00506A  2158 _AX5043_WAKEUP1NB	=	0x506a
                           00506D  2159 _AX5043_WAKEUPFREQ0NB	=	0x506d
                           00506C  2160 _AX5043_WAKEUPFREQ1NB	=	0x506c
                           005069  2161 _AX5043_WAKEUPTIMER0NB	=	0x5069
                           005068  2162 _AX5043_WAKEUPTIMER1NB	=	0x5068
                           00506E  2163 _AX5043_WAKEUPXOEARLYNB	=	0x506e
                           005F11  2164 _AX5043_XTALAMPLNB	=	0x5f11
                           005184  2165 _AX5043_XTALCAPNB	=	0x5184
                           005F10  2166 _AX5043_XTALOSCNB	=	0x5f10
                           00501D  2167 _AX5043_XTALSTATUSNB	=	0x501d
                           005F00  2168 _AX5043_0xF00NB	=	0x5f00
                           005F0C  2169 _AX5043_0xF0CNB	=	0x5f0c
                           005F18  2170 _AX5043_0xF18NB	=	0x5f18
                           005F1C  2171 _AX5043_0xF1CNB	=	0x5f1c
                           005F21  2172 _AX5043_0xF21NB	=	0x5f21
                           005F22  2173 _AX5043_0xF22NB	=	0x5f22
                           005F23  2174 _AX5043_0xF23NB	=	0x5f23
                           005F26  2175 _AX5043_0xF26NB	=	0x5f26
                           005F30  2176 _AX5043_0xF30NB	=	0x5f30
                           005F31  2177 _AX5043_0xF31NB	=	0x5f31
                           005F32  2178 _AX5043_0xF32NB	=	0x5f32
                           005F33  2179 _AX5043_0xF33NB	=	0x5f33
                           005F34  2180 _AX5043_0xF34NB	=	0x5f34
                           005F35  2181 _AX5043_0xF35NB	=	0x5f35
                           005F44  2182 _AX5043_0xF44NB	=	0x5f44
                           005122  2183 _AX5043_AGCAHYST0NB	=	0x5122
                           005132  2184 _AX5043_AGCAHYST1NB	=	0x5132
                           005142  2185 _AX5043_AGCAHYST2NB	=	0x5142
                           005152  2186 _AX5043_AGCAHYST3NB	=	0x5152
                           005120  2187 _AX5043_AGCGAIN0NB	=	0x5120
                           005130  2188 _AX5043_AGCGAIN1NB	=	0x5130
                           005140  2189 _AX5043_AGCGAIN2NB	=	0x5140
                           005150  2190 _AX5043_AGCGAIN3NB	=	0x5150
                           005123  2191 _AX5043_AGCMINMAX0NB	=	0x5123
                           005133  2192 _AX5043_AGCMINMAX1NB	=	0x5133
                           005143  2193 _AX5043_AGCMINMAX2NB	=	0x5143
                           005153  2194 _AX5043_AGCMINMAX3NB	=	0x5153
                           005121  2195 _AX5043_AGCTARGET0NB	=	0x5121
                           005131  2196 _AX5043_AGCTARGET1NB	=	0x5131
                           005141  2197 _AX5043_AGCTARGET2NB	=	0x5141
                           005151  2198 _AX5043_AGCTARGET3NB	=	0x5151
                           00512B  2199 _AX5043_AMPLITUDEGAIN0NB	=	0x512b
                           00513B  2200 _AX5043_AMPLITUDEGAIN1NB	=	0x513b
                           00514B  2201 _AX5043_AMPLITUDEGAIN2NB	=	0x514b
                           00515B  2202 _AX5043_AMPLITUDEGAIN3NB	=	0x515b
                           00512F  2203 _AX5043_BBOFFSRES0NB	=	0x512f
                           00513F  2204 _AX5043_BBOFFSRES1NB	=	0x513f
                           00514F  2205 _AX5043_BBOFFSRES2NB	=	0x514f
                           00515F  2206 _AX5043_BBOFFSRES3NB	=	0x515f
                           005125  2207 _AX5043_DRGAIN0NB	=	0x5125
                           005135  2208 _AX5043_DRGAIN1NB	=	0x5135
                           005145  2209 _AX5043_DRGAIN2NB	=	0x5145
                           005155  2210 _AX5043_DRGAIN3NB	=	0x5155
                           00512E  2211 _AX5043_FOURFSK0NB	=	0x512e
                           00513E  2212 _AX5043_FOURFSK1NB	=	0x513e
                           00514E  2213 _AX5043_FOURFSK2NB	=	0x514e
                           00515E  2214 _AX5043_FOURFSK3NB	=	0x515e
                           00512D  2215 _AX5043_FREQDEV00NB	=	0x512d
                           00513D  2216 _AX5043_FREQDEV01NB	=	0x513d
                           00514D  2217 _AX5043_FREQDEV02NB	=	0x514d
                           00515D  2218 _AX5043_FREQDEV03NB	=	0x515d
                           00512C  2219 _AX5043_FREQDEV10NB	=	0x512c
                           00513C  2220 _AX5043_FREQDEV11NB	=	0x513c
                           00514C  2221 _AX5043_FREQDEV12NB	=	0x514c
                           00515C  2222 _AX5043_FREQDEV13NB	=	0x515c
                           005127  2223 _AX5043_FREQUENCYGAINA0NB	=	0x5127
                           005137  2224 _AX5043_FREQUENCYGAINA1NB	=	0x5137
                           005147  2225 _AX5043_FREQUENCYGAINA2NB	=	0x5147
                           005157  2226 _AX5043_FREQUENCYGAINA3NB	=	0x5157
                           005128  2227 _AX5043_FREQUENCYGAINB0NB	=	0x5128
                           005138  2228 _AX5043_FREQUENCYGAINB1NB	=	0x5138
                           005148  2229 _AX5043_FREQUENCYGAINB2NB	=	0x5148
                           005158  2230 _AX5043_FREQUENCYGAINB3NB	=	0x5158
                           005129  2231 _AX5043_FREQUENCYGAINC0NB	=	0x5129
                           005139  2232 _AX5043_FREQUENCYGAINC1NB	=	0x5139
                           005149  2233 _AX5043_FREQUENCYGAINC2NB	=	0x5149
                           005159  2234 _AX5043_FREQUENCYGAINC3NB	=	0x5159
                           00512A  2235 _AX5043_FREQUENCYGAIND0NB	=	0x512a
                           00513A  2236 _AX5043_FREQUENCYGAIND1NB	=	0x513a
                           00514A  2237 _AX5043_FREQUENCYGAIND2NB	=	0x514a
                           00515A  2238 _AX5043_FREQUENCYGAIND3NB	=	0x515a
                           005116  2239 _AX5043_FREQUENCYLEAKNB	=	0x5116
                           005126  2240 _AX5043_PHASEGAIN0NB	=	0x5126
                           005136  2241 _AX5043_PHASEGAIN1NB	=	0x5136
                           005146  2242 _AX5043_PHASEGAIN2NB	=	0x5146
                           005156  2243 _AX5043_PHASEGAIN3NB	=	0x5156
                           005207  2244 _AX5043_PKTADDR0NB	=	0x5207
                           005206  2245 _AX5043_PKTADDR1NB	=	0x5206
                           005205  2246 _AX5043_PKTADDR2NB	=	0x5205
                           005204  2247 _AX5043_PKTADDR3NB	=	0x5204
                           005200  2248 _AX5043_PKTADDRCFGNB	=	0x5200
                           00520B  2249 _AX5043_PKTADDRMASK0NB	=	0x520b
                           00520A  2250 _AX5043_PKTADDRMASK1NB	=	0x520a
                           005209  2251 _AX5043_PKTADDRMASK2NB	=	0x5209
                           005208  2252 _AX5043_PKTADDRMASK3NB	=	0x5208
                           005201  2253 _AX5043_PKTLENCFGNB	=	0x5201
                           005202  2254 _AX5043_PKTLENOFFSETNB	=	0x5202
                           005203  2255 _AX5043_PKTMAXLENNB	=	0x5203
                           005118  2256 _AX5043_RXPARAMCURSETNB	=	0x5118
                           005117  2257 _AX5043_RXPARAMSETSNB	=	0x5117
                           005124  2258 _AX5043_TIMEGAIN0NB	=	0x5124
                           005134  2259 _AX5043_TIMEGAIN1NB	=	0x5134
                           005144  2260 _AX5043_TIMEGAIN2NB	=	0x5144
                           005154  2261 _AX5043_TIMEGAIN3NB	=	0x5154
                           004F00  2262 _AX5043_0xF00	=	0x4f00
                           004F0C  2263 _AX5043_0xF0C	=	0x4f0c
                           004F10  2264 _AX5043_0xF10	=	0x4f10
                           004F11  2265 _AX5043_0xF11	=	0x4f11
                           004F18  2266 _AX5043_0xF18	=	0x4f18
                           004F1C  2267 _AX5043_0xF1C	=	0x4f1c
                           004F21  2268 _AX5043_0xF21	=	0x4f21
                           004F22  2269 _AX5043_0xF22	=	0x4f22
                           004F23  2270 _AX5043_0xF23	=	0x4f23
                           004F26  2271 _AX5043_0xF26	=	0x4f26
                           004F30  2272 _AX5043_0xF30	=	0x4f30
                           004F31  2273 _AX5043_0xF31	=	0x4f31
                           004F32  2274 _AX5043_0xF32	=	0x4f32
                           004F33  2275 _AX5043_0xF33	=	0x4f33
                           004F34  2276 _AX5043_0xF34	=	0x4f34
                           004F35  2277 _AX5043_0xF35	=	0x4f35
                           004F44  2278 _AX5043_0xF44	=	0x4f44
                           004F0D  2279 _AX5043_REF	=	0x4f0d
                           004F08  2280 _AX5043_POWCTRL1	=	0x4f08
                           004F5F  2281 _AX5043_MODCFGP	=	0x4f5f
                           004F10  2282 _AX5043_XTALOSC	=	0x4f10
                           004F11  2283 _AX5043_XTALAMPL	=	0x4f11
      0000B5                       2284 _aligned_alloc_PARM_2:
      0000B5                       2285 	.ds 2
      0000B7                       2286 _axradio_syncstate::
      0000B7                       2287 	.ds 1
      0000B8                       2288 _axradio_txbuffer_len::
      0000B8                       2289 	.ds 2
      0000BA                       2290 _axradio_txbuffer_cnt::
      0000BA                       2291 	.ds 2
      0000BC                       2292 _axradio_curchannel::
      0000BC                       2293 	.ds 1
      0000BD                       2294 _axradio_curfreqoffset::
      0000BD                       2295 	.ds 4
      0000C1                       2296 _axradio_ack_count::
      0000C1                       2297 	.ds 1
      0000C2                       2298 _axradio_ack_seqnr::
      0000C2                       2299 	.ds 1
      0000C3                       2300 _axradio_sync_time::
      0000C3                       2301 	.ds 4
      0000C7                       2302 _axradio_sync_periodcorr::
      0000C7                       2303 	.ds 2
      0000C9                       2304 _axradio_timeanchor::
      0000C9                       2305 	.ds 8
      0000D1                       2306 _axradio_localaddr::
      0000D1                       2307 	.ds 8
      0000D9                       2308 _axradio_default_remoteaddr::
      0000D9                       2309 	.ds 4
      0000DD                       2310 _axradio_txbuffer::
      0000DD                       2311 	.ds 260
      0001E1                       2312 _axradio_rxbuffer::
      0001E1                       2313 	.ds 260
      0002E5                       2314 _axradio_cb_receive::
      0002E5                       2315 	.ds 34
      000307                       2316 _axradio_cb_receivesfd::
      000307                       2317 	.ds 10
      000311                       2318 _axradio_cb_channelstate::
      000311                       2319 	.ds 13
      00031E                       2320 _axradio_cb_transmitstart::
      00031E                       2321 	.ds 10
      000328                       2322 _axradio_cb_transmitend::
      000328                       2323 	.ds 10
      000332                       2324 _axradio_cb_transmitdata::
      000332                       2325 	.ds 10
      00033C                       2326 _axradio_timer::
      00033C                       2327 	.ds 8
      000344                       2328 _receive_isr_BeaconRx_1_245:
      000344                       2329 	.ds 29
      000361                       2330 _axradio_init_x_3_396:
      000361                       2331 	.ds 1
      000362                       2332 _axradio_init_j_3_397:
      000362                       2333 	.ds 1
      000363                       2334 _axradio_init_x_6_401:
      000363                       2335 	.ds 1
      000364                       2336 _axradio_set_mode_mode_1_410:
      000364                       2337 	.ds 1
      000365                       2338 _axradio_set_channel_chnum_1_421:
      000365                       2339 	.ds 1
      000366                       2340 _axradio_get_pllvcoi_x_2_429:
      000366                       2341 	.ds 1
      000367                       2342 _axradio_set_curfreqoffset_offs_1_432:
      000367                       2343 	.ds 4
      00036B                       2344 _axradio_set_local_address_addr_1_434:
      00036B                       2345 	.ds 3
      00036E                       2346 _axradio_set_default_remote_address_addr_1_436:
      00036E                       2347 	.ds 3
      000371                       2348 _axradio_transmit_PARM_2:
      000371                       2349 	.ds 3
      000374                       2350 _axradio_transmit_PARM_3:
      000374                       2351 	.ds 2
      000376                       2352 _axradio_transmit_addr_1_438:
      000376                       2353 	.ds 3
                                   2354 ;--------------------------------------------------------
                                   2355 ; absolute external ram data
                                   2356 ;--------------------------------------------------------
                                   2357 	.area XABS    (ABS,XDATA)
                                   2358 ;--------------------------------------------------------
                                   2359 ; external initialized ram data
                                   2360 ;--------------------------------------------------------
                                   2361 	.area XISEG   (XDATA)
      0009AA                       2362 _f30_saved::
      0009AA                       2363 	.ds 1
      0009AB                       2364 _f31_saved::
      0009AB                       2365 	.ds 1
      0009AC                       2366 _f32_saved::
      0009AC                       2367 	.ds 1
      0009AD                       2368 _f33_saved::
      0009AD                       2369 	.ds 1
                                   2370 	.area HOME    (CODE)
                                   2371 	.area GSINIT0 (CODE)
                                   2372 	.area GSINIT1 (CODE)
                                   2373 	.area GSINIT2 (CODE)
                                   2374 	.area GSINIT3 (CODE)
                                   2375 	.area GSINIT4 (CODE)
                                   2376 	.area GSINIT5 (CODE)
                                   2377 	.area GSINIT  (CODE)
                                   2378 	.area GSFINAL (CODE)
                                   2379 	.area CSEG    (CODE)
                                   2380 ;--------------------------------------------------------
                                   2381 ; global & static initialisations
                                   2382 ;--------------------------------------------------------
                                   2383 	.area HOME    (CODE)
                                   2384 	.area GSINIT  (CODE)
                                   2385 	.area GSFINAL (CODE)
                                   2386 	.area GSINIT  (CODE)
                                   2387 ;	..\src\COMMON\easyax5043.c:60: volatile uint8_t __data axradio_mode = AXRADIO_MODE_UNINIT;
      000391 75 0B 00         [24] 2388 	mov	_axradio_mode,#0x00
                                   2389 ;	..\src\COMMON\easyax5043.c:61: volatile axradio_trxstate_t __data axradio_trxstate = trxstate_off;
      000394 75 0C 00         [24] 2390 	mov	_axradio_trxstate,#0x00
                                   2391 ;--------------------------------------------------------
                                   2392 ; Home
                                   2393 ;--------------------------------------------------------
                                   2394 	.area HOME    (CODE)
                                   2395 	.area HOME    (CODE)
                                   2396 ;--------------------------------------------------------
                                   2397 ; code
                                   2398 ;--------------------------------------------------------
                                   2399 	.area CSEG    (CODE)
                                   2400 ;------------------------------------------------------------
                                   2401 ;Allocation info for local variables in function 'update_timeanchor'
                                   2402 ;------------------------------------------------------------
                                   2403 ;iesave                    Allocated to registers r7 
                                   2404 ;------------------------------------------------------------
                                   2405 ;	..\src\COMMON\easyax5043.c:239: static __reentrantb void update_timeanchor(void) __reentrant
                                   2406 ;	-----------------------------------------
                                   2407 ;	 function update_timeanchor
                                   2408 ;	-----------------------------------------
      0015BA                       2409 _update_timeanchor:
                           000007  2410 	ar7 = 0x07
                           000006  2411 	ar6 = 0x06
                           000005  2412 	ar5 = 0x05
                           000004  2413 	ar4 = 0x04
                           000003  2414 	ar3 = 0x03
                           000002  2415 	ar2 = 0x02
                           000001  2416 	ar1 = 0x01
                           000000  2417 	ar0 = 0x00
                                   2418 ;	..\src\COMMON\easyax5043.c:241: uint8_t iesave = IE & 0x80;
      0015BA 74 80            [12] 2419 	mov	a,#0x80
      0015BC 55 A8            [12] 2420 	anl	a,_IE
      0015BE FF               [12] 2421 	mov	r7,a
                                   2422 ;	..\src\COMMON\easyax5043.c:242: EA = 0;
      0015BF C2 AF            [12] 2423 	clr	_EA
                                   2424 ;	..\src\COMMON\easyax5043.c:243: axradio_timeanchor.timer0 = wtimer0_curtime();
      0015C1 C0 07            [24] 2425 	push	ar7
      0015C3 12 79 87         [24] 2426 	lcall	_wtimer0_curtime
      0015C6 AB 82            [24] 2427 	mov	r3,dpl
      0015C8 AC 83            [24] 2428 	mov	r4,dph
      0015CA AD F0            [24] 2429 	mov	r5,b
      0015CC FE               [12] 2430 	mov	r6,a
      0015CD D0 07            [24] 2431 	pop	ar7
      0015CF 90 00 C9         [24] 2432 	mov	dptr,#_axradio_timeanchor
      0015D2 EB               [12] 2433 	mov	a,r3
      0015D3 F0               [24] 2434 	movx	@dptr,a
      0015D4 EC               [12] 2435 	mov	a,r4
      0015D5 A3               [24] 2436 	inc	dptr
      0015D6 F0               [24] 2437 	movx	@dptr,a
      0015D7 ED               [12] 2438 	mov	a,r5
      0015D8 A3               [24] 2439 	inc	dptr
      0015D9 F0               [24] 2440 	movx	@dptr,a
      0015DA EE               [12] 2441 	mov	a,r6
      0015DB A3               [24] 2442 	inc	dptr
      0015DC F0               [24] 2443 	movx	@dptr,a
                                   2444 ;	..\src\COMMON\easyax5043.c:244: axradio_timeanchor.radiotimer = radio_read24((uint16_t)&AX5043_TIMER2);
      0015DD 7D 59            [12] 2445 	mov	r5,#_AX5043_TIMER2
      0015DF 7E 40            [12] 2446 	mov	r6,#(_AX5043_TIMER2 >> 8)
      0015E1 8D 82            [24] 2447 	mov	dpl,r5
      0015E3 8E 83            [24] 2448 	mov	dph,r6
      0015E5 12 67 67         [24] 2449 	lcall	_radio_read24
      0015E8 AB 82            [24] 2450 	mov	r3,dpl
      0015EA AC 83            [24] 2451 	mov	r4,dph
      0015EC AD F0            [24] 2452 	mov	r5,b
      0015EE FE               [12] 2453 	mov	r6,a
      0015EF 90 00 CD         [24] 2454 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      0015F2 EB               [12] 2455 	mov	a,r3
      0015F3 F0               [24] 2456 	movx	@dptr,a
      0015F4 EC               [12] 2457 	mov	a,r4
      0015F5 A3               [24] 2458 	inc	dptr
      0015F6 F0               [24] 2459 	movx	@dptr,a
      0015F7 ED               [12] 2460 	mov	a,r5
      0015F8 A3               [24] 2461 	inc	dptr
      0015F9 F0               [24] 2462 	movx	@dptr,a
      0015FA EE               [12] 2463 	mov	a,r6
      0015FB A3               [24] 2464 	inc	dptr
      0015FC F0               [24] 2465 	movx	@dptr,a
                                   2466 ;	..\src\COMMON\easyax5043.c:245: IE |= iesave;
      0015FD EF               [12] 2467 	mov	a,r7
      0015FE 42 A8            [12] 2468 	orl	_IE,a
      001600 22               [24] 2469 	ret
                                   2470 ;------------------------------------------------------------
                                   2471 ;Allocation info for local variables in function 'axradio_conv_time_totimer0'
                                   2472 ;------------------------------------------------------------
                                   2473 ;dt                        Allocated to registers r4 r5 r6 r7 
                                   2474 ;------------------------------------------------------------
                                   2475 ;	..\src\COMMON\easyax5043.c:248: __reentrantb uint32_t axradio_conv_time_totimer0(uint32_t dt) __reentrant
                                   2476 ;	-----------------------------------------
                                   2477 ;	 function axradio_conv_time_totimer0
                                   2478 ;	-----------------------------------------
      001601                       2479 _axradio_conv_time_totimer0:
      001601 AC 82            [24] 2480 	mov	r4,dpl
      001603 AD 83            [24] 2481 	mov	r5,dph
      001605 AE F0            [24] 2482 	mov	r6,b
      001607 FF               [12] 2483 	mov	r7,a
                                   2484 ;	..\src\COMMON\easyax5043.c:250: dt -= axradio_timeanchor.radiotimer;
      001608 90 00 CD         [24] 2485 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      00160B E0               [24] 2486 	movx	a,@dptr
      00160C F8               [12] 2487 	mov	r0,a
      00160D A3               [24] 2488 	inc	dptr
      00160E E0               [24] 2489 	movx	a,@dptr
      00160F F9               [12] 2490 	mov	r1,a
      001610 A3               [24] 2491 	inc	dptr
      001611 E0               [24] 2492 	movx	a,@dptr
      001612 FA               [12] 2493 	mov	r2,a
      001613 A3               [24] 2494 	inc	dptr
      001614 E0               [24] 2495 	movx	a,@dptr
      001615 FB               [12] 2496 	mov	r3,a
      001616 EC               [12] 2497 	mov	a,r4
      001617 C3               [12] 2498 	clr	c
      001618 98               [12] 2499 	subb	a,r0
      001619 FC               [12] 2500 	mov	r4,a
      00161A ED               [12] 2501 	mov	a,r5
      00161B 99               [12] 2502 	subb	a,r1
      00161C FD               [12] 2503 	mov	r5,a
      00161D EE               [12] 2504 	mov	a,r6
      00161E 9A               [12] 2505 	subb	a,r2
      00161F FE               [12] 2506 	mov	r6,a
      001620 EF               [12] 2507 	mov	a,r7
      001621 9B               [12] 2508 	subb	a,r3
                                   2509 ;	..\src\COMMON\easyax5043.c:251: dt = axradio_conv_timeinterval_totimer0(signextend24(dt));
      001622 8C 82            [24] 2510 	mov	dpl,r4
      001624 8D 83            [24] 2511 	mov	dph,r5
      001626 8E F0            [24] 2512 	mov	b,r6
      001628 12 79 81         [24] 2513 	lcall	_signextend24
      00162B 12 08 AA         [24] 2514 	lcall	_axradio_conv_timeinterval_totimer0
      00162E AC 82            [24] 2515 	mov	r4,dpl
      001630 AD 83            [24] 2516 	mov	r5,dph
      001632 AE F0            [24] 2517 	mov	r6,b
      001634 FF               [12] 2518 	mov	r7,a
                                   2519 ;	..\src\COMMON\easyax5043.c:252: dt += axradio_timeanchor.timer0;
      001635 90 00 C9         [24] 2520 	mov	dptr,#_axradio_timeanchor
      001638 E0               [24] 2521 	movx	a,@dptr
      001639 F8               [12] 2522 	mov	r0,a
      00163A A3               [24] 2523 	inc	dptr
      00163B E0               [24] 2524 	movx	a,@dptr
      00163C F9               [12] 2525 	mov	r1,a
      00163D A3               [24] 2526 	inc	dptr
      00163E E0               [24] 2527 	movx	a,@dptr
      00163F FA               [12] 2528 	mov	r2,a
      001640 A3               [24] 2529 	inc	dptr
      001641 E0               [24] 2530 	movx	a,@dptr
      001642 FB               [12] 2531 	mov	r3,a
      001643 E8               [12] 2532 	mov	a,r0
      001644 2C               [12] 2533 	add	a,r4
      001645 FC               [12] 2534 	mov	r4,a
      001646 E9               [12] 2535 	mov	a,r1
      001647 3D               [12] 2536 	addc	a,r5
      001648 FD               [12] 2537 	mov	r5,a
      001649 EA               [12] 2538 	mov	a,r2
      00164A 3E               [12] 2539 	addc	a,r6
      00164B FE               [12] 2540 	mov	r6,a
      00164C EB               [12] 2541 	mov	a,r3
      00164D 3F               [12] 2542 	addc	a,r7
                                   2543 ;	..\src\COMMON\easyax5043.c:253: return dt;
      00164E 8C 82            [24] 2544 	mov	dpl,r4
      001650 8D 83            [24] 2545 	mov	dph,r5
      001652 8E F0            [24] 2546 	mov	b,r6
      001654 22               [24] 2547 	ret
                                   2548 ;------------------------------------------------------------
                                   2549 ;Allocation info for local variables in function 'ax5043_init_registers_common'
                                   2550 ;------------------------------------------------------------
                                   2551 ;rng                       Allocated to registers r7 
                                   2552 ;------------------------------------------------------------
                                   2553 ;	..\src\COMMON\easyax5043.c:256: static __reentrantb uint8_t ax5043_init_registers_common(void) __reentrant
                                   2554 ;	-----------------------------------------
                                   2555 ;	 function ax5043_init_registers_common
                                   2556 ;	-----------------------------------------
      001655                       2557 _ax5043_init_registers_common:
                                   2558 ;	..\src\COMMON\easyax5043.c:258: uint8_t rng = axradio_phy_chanpllrng[axradio_curchannel];
      001655 90 00 BC         [24] 2559 	mov	dptr,#_axradio_curchannel
      001658 E0               [24] 2560 	movx	a,@dptr
      001659 24 09            [12] 2561 	add	a,#_axradio_phy_chanpllrng
      00165B F5 82            [12] 2562 	mov	dpl,a
      00165D E4               [12] 2563 	clr	a
      00165E 34 00            [12] 2564 	addc	a,#(_axradio_phy_chanpllrng >> 8)
      001660 F5 83            [12] 2565 	mov	dph,a
      001662 E0               [24] 2566 	movx	a,@dptr
                                   2567 ;	..\src\COMMON\easyax5043.c:259: if (rng & 0x20)
      001663 FF               [12] 2568 	mov	r7,a
      001664 30 E5 04         [24] 2569 	jnb	acc.5,00102$
                                   2570 ;	..\src\COMMON\easyax5043.c:260: return AXRADIO_ERR_RANGING;
      001667 75 82 06         [24] 2571 	mov	dpl,#0x06
      00166A 22               [24] 2572 	ret
      00166B                       2573 00102$:
                                   2574 ;	..\src\COMMON\easyax5043.c:261: if (AX5043_PLLLOOP & 0x80) {
      00166B 90 40 30         [24] 2575 	mov	dptr,#_AX5043_PLLLOOP
      00166E E0               [24] 2576 	movx	a,@dptr
      00166F FE               [12] 2577 	mov	r6,a
      001670 30 E7 09         [24] 2578 	jnb	acc.7,00104$
                                   2579 ;	..\src\COMMON\easyax5043.c:262: AX5043_PLLRANGINGB = rng & 0x0F;
      001673 90 40 3B         [24] 2580 	mov	dptr,#_AX5043_PLLRANGINGB
      001676 74 0F            [12] 2581 	mov	a,#0x0f
      001678 5F               [12] 2582 	anl	a,r7
      001679 F0               [24] 2583 	movx	@dptr,a
      00167A 80 07            [24] 2584 	sjmp	00105$
      00167C                       2585 00104$:
                                   2586 ;	..\src\COMMON\easyax5043.c:264: AX5043_PLLRANGINGA = rng & 0x0F;
      00167C 90 40 33         [24] 2587 	mov	dptr,#_AX5043_PLLRANGINGA
      00167F 74 0F            [12] 2588 	mov	a,#0x0f
      001681 5F               [12] 2589 	anl	a,r7
      001682 F0               [24] 2590 	movx	@dptr,a
      001683                       2591 00105$:
                                   2592 ;	..\src\COMMON\easyax5043.c:266: rng = axradio_get_pllvcoi();
      001683 12 40 3B         [24] 2593 	lcall	_axradio_get_pllvcoi
      001686 AE 82            [24] 2594 	mov	r6,dpl
      001688 8E 07            [24] 2595 	mov	ar7,r6
                                   2596 ;	..\src\COMMON\easyax5043.c:267: if (rng & 0x80)
      00168A EF               [12] 2597 	mov	a,r7
      00168B 30 E7 05         [24] 2598 	jnb	acc.7,00107$
                                   2599 ;	..\src\COMMON\easyax5043.c:268: AX5043_PLLVCOI = rng;
      00168E 90 41 80         [24] 2600 	mov	dptr,#_AX5043_PLLVCOI
      001691 EF               [12] 2601 	mov	a,r7
      001692 F0               [24] 2602 	movx	@dptr,a
      001693                       2603 00107$:
                                   2604 ;	..\src\COMMON\easyax5043.c:269: return AXRADIO_ERR_NOERROR;
      001693 75 82 00         [24] 2605 	mov	dpl,#0x00
      001696 22               [24] 2606 	ret
                                   2607 ;------------------------------------------------------------
                                   2608 ;Allocation info for local variables in function 'ax5043_init_registers_tx'
                                   2609 ;------------------------------------------------------------
                                   2610 ;	..\src\COMMON\easyax5043.c:272: __reentrantb uint8_t ax5043_init_registers_tx(void) __reentrant
                                   2611 ;	-----------------------------------------
                                   2612 ;	 function ax5043_init_registers_tx
                                   2613 ;	-----------------------------------------
      001697                       2614 _ax5043_init_registers_tx:
                                   2615 ;	..\src\COMMON\easyax5043.c:274: ax5043_set_registers_tx();
      001697 12 06 56         [24] 2616 	lcall	_ax5043_set_registers_tx
                                   2617 ;	..\src\COMMON\easyax5043.c:275: return ax5043_init_registers_common();
      00169A 02 16 55         [24] 2618 	ljmp	_ax5043_init_registers_common
                                   2619 ;------------------------------------------------------------
                                   2620 ;Allocation info for local variables in function 'ax5043_init_registers_rx'
                                   2621 ;------------------------------------------------------------
                                   2622 ;	..\src\COMMON\easyax5043.c:278: __reentrantb uint8_t ax5043_init_registers_rx(void) __reentrant
                                   2623 ;	-----------------------------------------
                                   2624 ;	 function ax5043_init_registers_rx
                                   2625 ;	-----------------------------------------
      00169D                       2626 _ax5043_init_registers_rx:
                                   2627 ;	..\src\COMMON\easyax5043.c:280: ax5043_set_registers_rx();
      00169D 12 06 7A         [24] 2628 	lcall	_ax5043_set_registers_rx
                                   2629 ;	..\src\COMMON\easyax5043.c:281: return ax5043_init_registers_common();
      0016A0 02 16 55         [24] 2630 	ljmp	_ax5043_init_registers_common
                                   2631 ;------------------------------------------------------------
                                   2632 ;Allocation info for local variables in function 'receive_isr'
                                   2633 ;------------------------------------------------------------
                                   2634 ;fifo_cmd                  Allocated to registers r6 
                                   2635 ;flags                     Allocated to registers 
                                   2636 ;i                         Allocated to registers r6 
                                   2637 ;len                       Allocated to registers r7 
                                   2638 ;r                         Allocated to registers r6 
                                   2639 ;r                         Allocated to registers r6 
                                   2640 ;r                         Allocated to registers r6 
                                   2641 ;BeaconRx                  Allocated with name '_receive_isr_BeaconRx_1_245'
                                   2642 ;------------------------------------------------------------
                                   2643 ;	..\src\COMMON\easyax5043.c:284: static __reentrantb void receive_isr(void) __reentrant
                                   2644 ;	-----------------------------------------
                                   2645 ;	 function receive_isr
                                   2646 ;	-----------------------------------------
      0016A3                       2647 _receive_isr:
                                   2648 ;	..\src\COMMON\easyax5043.c:288: uint8_t len = AX5043_RADIOEVENTREQ0; // clear request so interrupt does not fire again. sync_rx enables interrupt on radio state changed in order to wake up on SDF detected
      0016A3 90 40 0F         [24] 2649 	mov	dptr,#_AX5043_RADIOEVENTREQ0
      0016A6 E0               [24] 2650 	movx	a,@dptr
      0016A7 FF               [12] 2651 	mov	r7,a
                                   2652 ;	..\src\COMMON\easyax5043.c:290: dbglink_writestr(" receive_isr ");
      0016A8 90 7C 2B         [24] 2653 	mov	dptr,#___str_0
      0016AB 75 F0 80         [24] 2654 	mov	b,#0x80
      0016AE C0 07            [24] 2655 	push	ar7
      0016B0 12 6E FA         [24] 2656 	lcall	_dbglink_writestr
      0016B3 D0 07            [24] 2657 	pop	ar7
                                   2658 ;	..\src\COMMON\easyax5043.c:291: if ((len & 0x04) && AX5043_RADIOSTATE == 0x0F) {
      0016B5 EF               [12] 2659 	mov	a,r7
      0016B6 30 E2 51         [24] 2660 	jnb	acc.2,00169$
      0016B9 90 40 1C         [24] 2661 	mov	dptr,#_AX5043_RADIOSTATE
      0016BC E0               [24] 2662 	movx	a,@dptr
      0016BD FE               [12] 2663 	mov	r6,a
      0016BE BE 0F 49         [24] 2664 	cjne	r6,#0x0f,00169$
                                   2665 ;	..\src\COMMON\easyax5043.c:293: dbglink_writestr(" time anchor ");
      0016C1 90 7C 39         [24] 2666 	mov	dptr,#___str_1
      0016C4 75 F0 80         [24] 2667 	mov	b,#0x80
      0016C7 12 6E FA         [24] 2668 	lcall	_dbglink_writestr
                                   2669 ;	..\src\COMMON\easyax5043.c:294: update_timeanchor();
      0016CA 12 15 BA         [24] 2670 	lcall	_update_timeanchor
                                   2671 ;	..\src\COMMON\easyax5043.c:295: if(axradio_framing_enable_sfdcallback)
      0016CD 90 7A E8         [24] 2672 	mov	dptr,#_axradio_framing_enable_sfdcallback
      0016D0 E4               [12] 2673 	clr	a
      0016D1 93               [24] 2674 	movc	a,@a+dptr
      0016D2 60 36            [24] 2675 	jz	00169$
                                   2676 ;	..\src\COMMON\easyax5043.c:297: dbglink_writestr(" enable_sfdcallback ");
      0016D4 90 7C 47         [24] 2677 	mov	dptr,#___str_2
      0016D7 75 F0 80         [24] 2678 	mov	b,#0x80
      0016DA 12 6E FA         [24] 2679 	lcall	_dbglink_writestr
                                   2680 ;	..\src\COMMON\easyax5043.c:298: wtimer_remove_callback(&axradio_cb_receivesfd.cb);
      0016DD 90 03 07         [24] 2681 	mov	dptr,#_axradio_cb_receivesfd
      0016E0 12 74 C0         [24] 2682 	lcall	_wtimer_remove_callback
                                   2683 ;	..\src\COMMON\easyax5043.c:299: axradio_cb_receivesfd.st.error = AXRADIO_ERR_NOERROR;
      0016E3 90 03 0C         [24] 2684 	mov	dptr,#(_axradio_cb_receivesfd + 0x0005)
      0016E6 E4               [12] 2685 	clr	a
      0016E7 F0               [24] 2686 	movx	@dptr,a
                                   2687 ;	..\src\COMMON\easyax5043.c:300: axradio_cb_receivesfd.st.time.t = axradio_timeanchor.radiotimer;
      0016E8 90 00 CD         [24] 2688 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      0016EB E0               [24] 2689 	movx	a,@dptr
      0016EC FB               [12] 2690 	mov	r3,a
      0016ED A3               [24] 2691 	inc	dptr
      0016EE E0               [24] 2692 	movx	a,@dptr
      0016EF FC               [12] 2693 	mov	r4,a
      0016F0 A3               [24] 2694 	inc	dptr
      0016F1 E0               [24] 2695 	movx	a,@dptr
      0016F2 FD               [12] 2696 	mov	r5,a
      0016F3 A3               [24] 2697 	inc	dptr
      0016F4 E0               [24] 2698 	movx	a,@dptr
      0016F5 FE               [12] 2699 	mov	r6,a
      0016F6 90 03 0D         [24] 2700 	mov	dptr,#(_axradio_cb_receivesfd + 0x0006)
      0016F9 EB               [12] 2701 	mov	a,r3
      0016FA F0               [24] 2702 	movx	@dptr,a
      0016FB EC               [12] 2703 	mov	a,r4
      0016FC A3               [24] 2704 	inc	dptr
      0016FD F0               [24] 2705 	movx	@dptr,a
      0016FE ED               [12] 2706 	mov	a,r5
      0016FF A3               [24] 2707 	inc	dptr
      001700 F0               [24] 2708 	movx	@dptr,a
      001701 EE               [12] 2709 	mov	a,r6
      001702 A3               [24] 2710 	inc	dptr
      001703 F0               [24] 2711 	movx	@dptr,a
                                   2712 ;	..\src\COMMON\easyax5043.c:301: wtimer_add_callback(&axradio_cb_receivesfd.cb);
      001704 90 03 07         [24] 2713 	mov	dptr,#_axradio_cb_receivesfd
      001707 12 66 0B         [24] 2714 	lcall	_wtimer_add_callback
                                   2715 ;	..\src\COMMON\easyax5043.c:313: while (AX5043_IRQREQUEST0 & 0x01) {    // while fifo not empty
      00170A                       2716 00169$:
      00170A                       2717 00153$:
      00170A 90 40 0D         [24] 2718 	mov	dptr,#_AX5043_IRQREQUEST0
      00170D E0               [24] 2719 	movx	a,@dptr
      00170E FE               [12] 2720 	mov	r6,a
      00170F 20 E0 01         [24] 2721 	jb	acc.0,00250$
      001712 22               [24] 2722 	ret
      001713                       2723 00250$:
                                   2724 ;	..\src\COMMON\easyax5043.c:315: fifo_cmd = AX5043_FIFODATA; // read command
      001713 90 40 29         [24] 2725 	mov	dptr,#_AX5043_FIFODATA
      001716 E0               [24] 2726 	movx	a,@dptr
      001717 FE               [12] 2727 	mov	r6,a
                                   2728 ;	..\src\COMMON\easyax5043.c:316: len = (fifo_cmd & 0xE0) >> 5; // top 3 bits encode payload len
      001718 74 E0            [12] 2729 	mov	a,#0xe0
      00171A 5E               [12] 2730 	anl	a,r6
      00171B FD               [12] 2731 	mov	r5,a
      00171C C4               [12] 2732 	swap	a
      00171D 03               [12] 2733 	rr	a
      00171E 54 07            [12] 2734 	anl	a,#0x07
      001720 FF               [12] 2735 	mov	r7,a
                                   2736 ;	..\src\COMMON\easyax5043.c:317: if (len == 7)
      001721 BF 07 06         [24] 2737 	cjne	r7,#0x07,00107$
                                   2738 ;	..\src\COMMON\easyax5043.c:318: len = AX5043_FIFODATA; // 7 means variable length, -> get length byte
      001724 90 40 29         [24] 2739 	mov	dptr,#_AX5043_FIFODATA
      001727 E0               [24] 2740 	movx	a,@dptr
      001728 FD               [12] 2741 	mov	r5,a
      001729 FF               [12] 2742 	mov	r7,a
      00172A                       2743 00107$:
                                   2744 ;	..\src\COMMON\easyax5043.c:319: fifo_cmd &= 0x1F;
      00172A 53 06 1F         [24] 2745 	anl	ar6,#0x1f
                                   2746 ;	..\src\COMMON\easyax5043.c:320: switch (fifo_cmd) {
      00172D BE 01 02         [24] 2747 	cjne	r6,#0x01,00253$
      001730 80 21            [24] 2748 	sjmp	00108$
      001732                       2749 00253$:
      001732 BE 10 03         [24] 2750 	cjne	r6,#0x10,00254$
      001735 02 19 9F         [24] 2751 	ljmp	00139$
      001738                       2752 00254$:
      001738 BE 11 03         [24] 2753 	cjne	r6,#0x11,00255$
      00173B 02 19 72         [24] 2754 	ljmp	00136$
      00173E                       2755 00255$:
      00173E BE 12 03         [24] 2756 	cjne	r6,#0x12,00256$
      001741 02 19 22         [24] 2757 	ljmp	00132$
      001744                       2758 00256$:
      001744 BE 13 03         [24] 2759 	cjne	r6,#0x13,00257$
      001747 02 18 DB         [24] 2760 	ljmp	00128$
      00174A                       2761 00257$:
      00174A BE 15 03         [24] 2762 	cjne	r6,#0x15,00258$
      00174D 02 19 C8         [24] 2763 	ljmp	00142$
      001750                       2764 00258$:
      001750 02 1A 40         [24] 2765 	ljmp	00146$
                                   2766 ;	..\src\COMMON\easyax5043.c:321: case AX5043_FIFOCMD_DATA:
      001753                       2767 00108$:
                                   2768 ;	..\src\COMMON\easyax5043.c:323: if (!len)
      001753 EF               [12] 2769 	mov	a,r7
      001754 60 B4            [24] 2770 	jz	00153$
                                   2771 ;	..\src\COMMON\easyax5043.c:326: flags = AX5043_FIFODATA;
      001756 90 40 29         [24] 2772 	mov	dptr,#_AX5043_FIFODATA
      001759 E0               [24] 2773 	movx	a,@dptr
                                   2774 ;	..\src\COMMON\easyax5043.c:327: --len;
      00175A 1F               [12] 2775 	dec	r7
                                   2776 ;	..\src\COMMON\easyax5043.c:328: ax5043_readfifo(axradio_rxbuffer, len);
      00175B C0 07            [24] 2777 	push	ar7
      00175D C0 07            [24] 2778 	push	ar7
      00175F 90 01 E1         [24] 2779 	mov	dptr,#_axradio_rxbuffer
      001762 75 F0 00         [24] 2780 	mov	b,#0x00
      001765 12 71 30         [24] 2781 	lcall	_ax5043_readfifo
      001768 15 81            [12] 2782 	dec	sp
      00176A D0 07            [24] 2783 	pop	ar7
                                   2784 ;	..\src\COMMON\easyax5043.c:329: if(axradio_mode == AXRADIO_MODE_WOR_RECEIVE || axradio_mode == AXRADIO_MODE_WOR_ACK_RECEIVE)
      00176C 74 21            [12] 2785 	mov	a,#0x21
      00176E B5 0B 02         [24] 2786 	cjne	a,_axradio_mode,00260$
      001771 80 05            [24] 2787 	sjmp	00111$
      001773                       2788 00260$:
      001773 74 23            [12] 2789 	mov	a,#0x23
      001775 B5 0B 21         [24] 2790 	cjne	a,_axradio_mode,00112$
      001778                       2791 00111$:
                                   2792 ;	..\src\COMMON\easyax5043.c:331: f30_saved = AX5043_0xF30;
      001778 90 4F 30         [24] 2793 	mov	dptr,#_AX5043_0xF30
      00177B E0               [24] 2794 	movx	a,@dptr
      00177C 90 09 AA         [24] 2795 	mov	dptr,#_f30_saved
      00177F F0               [24] 2796 	movx	@dptr,a
                                   2797 ;	..\src\COMMON\easyax5043.c:332: f31_saved = AX5043_0xF31;
      001780 90 4F 31         [24] 2798 	mov	dptr,#_AX5043_0xF31
      001783 E0               [24] 2799 	movx	a,@dptr
      001784 90 09 AB         [24] 2800 	mov	dptr,#_f31_saved
      001787 F0               [24] 2801 	movx	@dptr,a
                                   2802 ;	..\src\COMMON\easyax5043.c:333: f32_saved = AX5043_0xF32;
      001788 90 4F 32         [24] 2803 	mov	dptr,#_AX5043_0xF32
      00178B E0               [24] 2804 	movx	a,@dptr
      00178C 90 09 AC         [24] 2805 	mov	dptr,#_f32_saved
      00178F F0               [24] 2806 	movx	@dptr,a
                                   2807 ;	..\src\COMMON\easyax5043.c:334: f33_saved = AX5043_0xF33;
      001790 90 4F 33         [24] 2808 	mov	dptr,#_AX5043_0xF33
      001793 E0               [24] 2809 	movx	a,@dptr
      001794 FE               [12] 2810 	mov	r6,a
      001795 90 09 AD         [24] 2811 	mov	dptr,#_f33_saved
      001798 F0               [24] 2812 	movx	@dptr,a
      001799                       2813 00112$:
                                   2814 ;	..\src\COMMON\easyax5043.c:336: if (axradio_mode == AXRADIO_MODE_WOR_RECEIVE ||
      001799 74 21            [12] 2815 	mov	a,#0x21
      00179B B5 0B 02         [24] 2816 	cjne	a,_axradio_mode,00263$
      00179E 80 05            [24] 2817 	sjmp	00114$
      0017A0                       2818 00263$:
                                   2819 ;	..\src\COMMON\easyax5043.c:337: axradio_mode == AXRADIO_MODE_SYNC_SLAVE)
      0017A0 74 32            [12] 2820 	mov	a,#0x32
      0017A2 B5 0B 05         [24] 2821 	cjne	a,_axradio_mode,00115$
      0017A5                       2822 00114$:
                                   2823 ;	..\src\COMMON\easyax5043.c:338: AX5043_PWRMODE = AX5043_PWRSTATE_POWERDOWN;
      0017A5 90 40 02         [24] 2824 	mov	dptr,#_AX5043_PWRMODE
      0017A8 E4               [12] 2825 	clr	a
      0017A9 F0               [24] 2826 	movx	@dptr,a
      0017AA                       2827 00115$:
                                   2828 ;	..\src\COMMON\easyax5043.c:339: AX5043_IRQMASK0 &= (uint8_t)~0x01; // disable FIFO not empty irq
      0017AA 90 40 07         [24] 2829 	mov	dptr,#_AX5043_IRQMASK0
      0017AD E0               [24] 2830 	movx	a,@dptr
      0017AE FE               [12] 2831 	mov	r6,a
      0017AF 74 FE            [12] 2832 	mov	a,#0xfe
      0017B1 5E               [12] 2833 	anl	a,r6
      0017B2 F0               [24] 2834 	movx	@dptr,a
                                   2835 ;	..\src\COMMON\easyax5043.c:340: wtimer_remove_callback(&axradio_cb_receive.cb);
      0017B3 90 02 E5         [24] 2836 	mov	dptr,#_axradio_cb_receive
      0017B6 C0 07            [24] 2837 	push	ar7
      0017B8 12 74 C0         [24] 2838 	lcall	_wtimer_remove_callback
      0017BB D0 07            [24] 2839 	pop	ar7
                                   2840 ;	..\src\COMMON\easyax5043.c:341: axradio_cb_receive.st.error = AXRADIO_ERR_NOERROR;
      0017BD 90 02 EA         [24] 2841 	mov	dptr,#(_axradio_cb_receive + 0x0005)
      0017C0 E4               [12] 2842 	clr	a
      0017C1 F0               [24] 2843 	movx	@dptr,a
                                   2844 ;	..\src\COMMON\easyax5043.c:343: axradio_cb_receive.st.rx.mac.raw = axradio_rxbuffer;
      0017C2 90 03 01         [24] 2845 	mov	dptr,#(_axradio_cb_receive + 0x001c)
      0017C5 74 E1            [12] 2846 	mov	a,#_axradio_rxbuffer
      0017C7 F0               [24] 2847 	movx	@dptr,a
      0017C8 74 01            [12] 2848 	mov	a,#(_axradio_rxbuffer >> 8)
      0017CA A3               [24] 2849 	inc	dptr
      0017CB F0               [24] 2850 	movx	@dptr,a
                                   2851 ;	..\src\COMMON\easyax5043.c:344: if (AXRADIO_MODE_IS_STREAM_RECEIVE(axradio_mode)) {
      0017CC 74 F8            [12] 2852 	mov	a,#0xf8
      0017CE 55 0B            [12] 2853 	anl	a,_axradio_mode
      0017D0 FE               [12] 2854 	mov	r6,a
      0017D1 BE 28 02         [24] 2855 	cjne	r6,#0x28,00266$
      0017D4 80 03            [24] 2856 	sjmp	00267$
      0017D6                       2857 00266$:
      0017D6 02 18 6C         [24] 2858 	ljmp	00121$
      0017D9                       2859 00267$:
                                   2860 ;	..\src\COMMON\easyax5043.c:345: axradio_cb_receive.st.rx.pktdata = axradio_rxbuffer;
      0017D9 90 03 03         [24] 2861 	mov	dptr,#(_axradio_cb_receive + 0x001e)
      0017DC 74 E1            [12] 2862 	mov	a,#_axradio_rxbuffer
      0017DE F0               [24] 2863 	movx	@dptr,a
      0017DF 74 01            [12] 2864 	mov	a,#(_axradio_rxbuffer >> 8)
      0017E1 A3               [24] 2865 	inc	dptr
      0017E2 F0               [24] 2866 	movx	@dptr,a
                                   2867 ;	..\src\COMMON\easyax5043.c:346: axradio_cb_receive.st.rx.pktlen = len;
      0017E3 8F 05            [24] 2868 	mov	ar5,r7
      0017E5 7E 00            [12] 2869 	mov	r6,#0x00
      0017E7 90 03 05         [24] 2870 	mov	dptr,#(_axradio_cb_receive + 0x0020)
      0017EA ED               [12] 2871 	mov	a,r5
      0017EB F0               [24] 2872 	movx	@dptr,a
      0017EC EE               [12] 2873 	mov	a,r6
      0017ED A3               [24] 2874 	inc	dptr
      0017EE F0               [24] 2875 	movx	@dptr,a
                                   2876 ;	..\src\COMMON\easyax5043.c:348: int8_t r = AX5043_RSSI;
      0017EF 90 40 40         [24] 2877 	mov	dptr,#_AX5043_RSSI
      0017F2 E0               [24] 2878 	movx	a,@dptr
                                   2879 ;	..\src\COMMON\easyax5043.c:349: axradio_cb_receive.st.rx.phy.rssi = r - (int16_t)axradio_phy_rssioffset;
      0017F3 FE               [12] 2880 	mov	r6,a
      0017F4 33               [12] 2881 	rlc	a
      0017F5 95 E0            [12] 2882 	subb	a,acc
      0017F7 FD               [12] 2883 	mov	r5,a
      0017F8 90 7A C6         [24] 2884 	mov	dptr,#_axradio_phy_rssioffset
      0017FB E4               [12] 2885 	clr	a
      0017FC 93               [24] 2886 	movc	a,@a+dptr
      0017FD FC               [12] 2887 	mov	r4,a
      0017FE 33               [12] 2888 	rlc	a
      0017FF 95 E0            [12] 2889 	subb	a,acc
      001801 FB               [12] 2890 	mov	r3,a
      001802 EE               [12] 2891 	mov	a,r6
      001803 C3               [12] 2892 	clr	c
      001804 9C               [12] 2893 	subb	a,r4
      001805 FE               [12] 2894 	mov	r6,a
      001806 ED               [12] 2895 	mov	a,r5
      001807 9B               [12] 2896 	subb	a,r3
      001808 FD               [12] 2897 	mov	r5,a
      001809 90 02 EF         [24] 2898 	mov	dptr,#(_axradio_cb_receive + 0x000a)
      00180C EE               [12] 2899 	mov	a,r6
      00180D F0               [24] 2900 	movx	@dptr,a
      00180E ED               [12] 2901 	mov	a,r5
      00180F A3               [24] 2902 	inc	dptr
      001810 F0               [24] 2903 	movx	@dptr,a
                                   2904 ;	..\src\COMMON\easyax5043.c:351: if (axradio_phy_innerfreqloop)
      001811 90 7A B8         [24] 2905 	mov	dptr,#_axradio_phy_innerfreqloop
      001814 E4               [12] 2906 	clr	a
      001815 93               [24] 2907 	movc	a,@a+dptr
      001816 60 28            [24] 2908 	jz	00118$
                                   2909 ;	..\src\COMMON\easyax5043.c:352: axradio_cb_receive.st.rx.phy.offset.o = axradio_conv_freq_fromreg(signextend16(radio_read16((uint16_t)&AX5043_TRKFREQ1)));
      001818 7D 50            [12] 2910 	mov	r5,#_AX5043_TRKFREQ1
      00181A 7E 40            [12] 2911 	mov	r6,#(_AX5043_TRKFREQ1 >> 8)
      00181C 8D 82            [24] 2912 	mov	dpl,r5
      00181E 8E 83            [24] 2913 	mov	dph,r6
      001820 12 68 A0         [24] 2914 	lcall	_radio_read16
      001823 12 79 AE         [24] 2915 	lcall	_signextend16
      001826 12 08 58         [24] 2916 	lcall	_axradio_conv_freq_fromreg
      001829 AB 82            [24] 2917 	mov	r3,dpl
      00182B AC 83            [24] 2918 	mov	r4,dph
      00182D AD F0            [24] 2919 	mov	r5,b
      00182F FE               [12] 2920 	mov	r6,a
      001830 90 02 F1         [24] 2921 	mov	dptr,#(_axradio_cb_receive + 0x000c)
      001833 EB               [12] 2922 	mov	a,r3
      001834 F0               [24] 2923 	movx	@dptr,a
      001835 EC               [12] 2924 	mov	a,r4
      001836 A3               [24] 2925 	inc	dptr
      001837 F0               [24] 2926 	movx	@dptr,a
      001838 ED               [12] 2927 	mov	a,r5
      001839 A3               [24] 2928 	inc	dptr
      00183A F0               [24] 2929 	movx	@dptr,a
      00183B EE               [12] 2930 	mov	a,r6
      00183C A3               [24] 2931 	inc	dptr
      00183D F0               [24] 2932 	movx	@dptr,a
      00183E 80 23            [24] 2933 	sjmp	00119$
      001840                       2934 00118$:
                                   2935 ;	..\src\COMMON\easyax5043.c:354: axradio_cb_receive.st.rx.phy.offset.o = signextend20(radio_read24((uint16_t)&AX5043_TRKRFFREQ2));
      001840 7D 4D            [12] 2936 	mov	r5,#_AX5043_TRKRFFREQ2
      001842 7E 40            [12] 2937 	mov	r6,#(_AX5043_TRKRFFREQ2 >> 8)
      001844 8D 82            [24] 2938 	mov	dpl,r5
      001846 8E 83            [24] 2939 	mov	dph,r6
      001848 12 67 67         [24] 2940 	lcall	_radio_read24
      00184B 12 78 5D         [24] 2941 	lcall	_signextend20
      00184E AB 82            [24] 2942 	mov	r3,dpl
      001850 AC 83            [24] 2943 	mov	r4,dph
      001852 AD F0            [24] 2944 	mov	r5,b
      001854 FE               [12] 2945 	mov	r6,a
      001855 90 02 F1         [24] 2946 	mov	dptr,#(_axradio_cb_receive + 0x000c)
      001858 EB               [12] 2947 	mov	a,r3
      001859 F0               [24] 2948 	movx	@dptr,a
      00185A EC               [12] 2949 	mov	a,r4
      00185B A3               [24] 2950 	inc	dptr
      00185C F0               [24] 2951 	movx	@dptr,a
      00185D ED               [12] 2952 	mov	a,r5
      00185E A3               [24] 2953 	inc	dptr
      00185F F0               [24] 2954 	movx	@dptr,a
      001860 EE               [12] 2955 	mov	a,r6
      001861 A3               [24] 2956 	inc	dptr
      001862 F0               [24] 2957 	movx	@dptr,a
      001863                       2958 00119$:
                                   2959 ;	..\src\COMMON\easyax5043.c:355: wtimer_add_callback(&axradio_cb_receive.cb);
      001863 90 02 E5         [24] 2960 	mov	dptr,#_axradio_cb_receive
      001866 12 66 0B         [24] 2961 	lcall	_wtimer_add_callback
                                   2962 ;	..\src\COMMON\easyax5043.c:356: break;
      001869 02 17 0A         [24] 2963 	ljmp	00153$
      00186C                       2964 00121$:
                                   2965 ;	..\src\COMMON\easyax5043.c:358: axradio_cb_receive.st.rx.pktdata = &axradio_rxbuffer[axradio_framing_maclen];
      00186C 90 7A DA         [24] 2966 	mov	dptr,#_axradio_framing_maclen
      00186F E4               [12] 2967 	clr	a
      001870 93               [24] 2968 	movc	a,@a+dptr
      001871 FE               [12] 2969 	mov	r6,a
      001872 24 E1            [12] 2970 	add	a,#_axradio_rxbuffer
      001874 FC               [12] 2971 	mov	r4,a
      001875 E4               [12] 2972 	clr	a
      001876 34 01            [12] 2973 	addc	a,#(_axradio_rxbuffer >> 8)
      001878 FD               [12] 2974 	mov	r5,a
      001879 90 03 03         [24] 2975 	mov	dptr,#(_axradio_cb_receive + 0x001e)
      00187C EC               [12] 2976 	mov	a,r4
      00187D F0               [24] 2977 	movx	@dptr,a
      00187E ED               [12] 2978 	mov	a,r5
      00187F A3               [24] 2979 	inc	dptr
      001880 F0               [24] 2980 	movx	@dptr,a
                                   2981 ;	..\src\COMMON\easyax5043.c:359: if (len < axradio_framing_maclen) {
      001881 C3               [12] 2982 	clr	c
      001882 EF               [12] 2983 	mov	a,r7
      001883 9E               [12] 2984 	subb	a,r6
      001884 50 0B            [24] 2985 	jnc	00126$
                                   2986 ;	..\src\COMMON\easyax5043.c:360: len = 0;
      001886 7F 00            [12] 2987 	mov	r7,#0x00
                                   2988 ;	..\src\COMMON\easyax5043.c:361: axradio_cb_receive.st.rx.pktlen = 0;
      001888 90 03 05         [24] 2989 	mov	dptr,#(_axradio_cb_receive + 0x0020)
      00188B E4               [12] 2990 	clr	a
      00188C F0               [24] 2991 	movx	@dptr,a
      00188D A3               [24] 2992 	inc	dptr
      00188E F0               [24] 2993 	movx	@dptr,a
      00188F 80 2F            [24] 2994 	sjmp	00127$
      001891                       2995 00126$:
                                   2996 ;	..\src\COMMON\easyax5043.c:363: len -= axradio_framing_maclen;
      001891 EF               [12] 2997 	mov	a,r7
      001892 C3               [12] 2998 	clr	c
      001893 9E               [12] 2999 	subb	a,r6
                                   3000 ;	..\src\COMMON\easyax5043.c:364: axradio_cb_receive.st.rx.pktlen = len;
      001894 FF               [12] 3001 	mov	r7,a
      001895 FD               [12] 3002 	mov	r5,a
      001896 7E 00            [12] 3003 	mov	r6,#0x00
      001898 90 03 05         [24] 3004 	mov	dptr,#(_axradio_cb_receive + 0x0020)
      00189B ED               [12] 3005 	mov	a,r5
      00189C F0               [24] 3006 	movx	@dptr,a
      00189D EE               [12] 3007 	mov	a,r6
      00189E A3               [24] 3008 	inc	dptr
      00189F F0               [24] 3009 	movx	@dptr,a
                                   3010 ;	..\src\COMMON\easyax5043.c:365: wtimer_add_callback(&axradio_cb_receive.cb);
      0018A0 90 02 E5         [24] 3011 	mov	dptr,#_axradio_cb_receive
      0018A3 C0 07            [24] 3012 	push	ar7
      0018A5 12 66 0B         [24] 3013 	lcall	_wtimer_add_callback
      0018A8 D0 07            [24] 3014 	pop	ar7
                                   3015 ;	..\src\COMMON\easyax5043.c:366: if (axradio_mode == AXRADIO_MODE_SYNC_SLAVE ||
      0018AA 74 32            [12] 3016 	mov	a,#0x32
      0018AC B5 0B 02         [24] 3017 	cjne	a,_axradio_mode,00270$
      0018AF 80 05            [24] 3018 	sjmp	00122$
      0018B1                       3019 00270$:
                                   3020 ;	..\src\COMMON\easyax5043.c:367: axradio_mode == AXRADIO_MODE_SYNC_ACK_SLAVE)
      0018B1 74 33            [12] 3021 	mov	a,#0x33
      0018B3 B5 0B 0A         [24] 3022 	cjne	a,_axradio_mode,00127$
      0018B6                       3023 00122$:
                                   3024 ;	..\src\COMMON\easyax5043.c:368: wtimer_remove(&axradio_timer);
      0018B6 90 03 3C         [24] 3025 	mov	dptr,#_axradio_timer
      0018B9 C0 07            [24] 3026 	push	ar7
      0018BB 12 71 84         [24] 3027 	lcall	_wtimer_remove
      0018BE D0 07            [24] 3028 	pop	ar7
      0018C0                       3029 00127$:
                                   3030 ;	..\src\COMMON\easyax5043.c:370: BEACON_decoding(axradio_rxbuffer+3,&BeaconRx,len);
      0018C0 90 03 79         [24] 3031 	mov	dptr,#_BEACON_decoding_PARM_2
      0018C3 74 44            [12] 3032 	mov	a,#_receive_isr_BeaconRx_1_245
      0018C5 F0               [24] 3033 	movx	@dptr,a
      0018C6 74 03            [12] 3034 	mov	a,#(_receive_isr_BeaconRx_1_245 >> 8)
      0018C8 A3               [24] 3035 	inc	dptr
      0018C9 F0               [24] 3036 	movx	@dptr,a
      0018CA 90 03 7B         [24] 3037 	mov	dptr,#_BEACON_decoding_PARM_3
      0018CD EF               [12] 3038 	mov	a,r7
      0018CE F0               [24] 3039 	movx	@dptr,a
      0018CF 90 01 E4         [24] 3040 	mov	dptr,#(_axradio_rxbuffer + 0x0003)
      0018D2 75 F0 00         [24] 3041 	mov	b,#0x00
      0018D5 12 45 93         [24] 3042 	lcall	_BEACON_decoding
                                   3043 ;	..\src\COMMON\easyax5043.c:371: break;
      0018D8 02 17 0A         [24] 3044 	ljmp	00153$
                                   3045 ;	..\src\COMMON\easyax5043.c:373: case AX5043_FIFOCMD_RFFREQOFFS:
      0018DB                       3046 00128$:
                                   3047 ;	..\src\COMMON\easyax5043.c:374: if (axradio_phy_innerfreqloop || len != 3)
      0018DB 90 7A B8         [24] 3048 	mov	dptr,#_axradio_phy_innerfreqloop
      0018DE E4               [12] 3049 	clr	a
      0018DF 93               [24] 3050 	movc	a,@a+dptr
      0018E0 60 03            [24] 3051 	jz	00273$
      0018E2 02 1A 40         [24] 3052 	ljmp	00146$
      0018E5                       3053 00273$:
      0018E5 BF 03 02         [24] 3054 	cjne	r7,#0x03,00274$
      0018E8 80 03            [24] 3055 	sjmp	00275$
      0018EA                       3056 00274$:
      0018EA 02 1A 40         [24] 3057 	ljmp	00146$
      0018ED                       3058 00275$:
                                   3059 ;	..\src\COMMON\easyax5043.c:376: i = AX5043_FIFODATA;
      0018ED 90 40 29         [24] 3060 	mov	dptr,#_AX5043_FIFODATA
      0018F0 E0               [24] 3061 	movx	a,@dptr
      0018F1 FE               [12] 3062 	mov	r6,a
                                   3063 ;	..\src\COMMON\easyax5043.c:377: i &= 0x0F;
      0018F2 53 06 0F         [24] 3064 	anl	ar6,#0x0f
                                   3065 ;	..\src\COMMON\easyax5043.c:378: i |= 1 + (uint8_t)~(i & 0x08);
      0018F5 74 08            [12] 3066 	mov	a,#0x08
      0018F7 5E               [12] 3067 	anl	a,r6
      0018F8 F4               [12] 3068 	cpl	a
      0018F9 FD               [12] 3069 	mov	r5,a
      0018FA 0D               [12] 3070 	inc	r5
      0018FB ED               [12] 3071 	mov	a,r5
      0018FC 42 06            [12] 3072 	orl	ar6,a
                                   3073 ;	..\src\COMMON\easyax5043.c:379: axradio_cb_receive.st.rx.phy.offset.b.b3 = ((int8_t)i) >> 8;
      0018FE 8E 05            [24] 3074 	mov	ar5,r6
      001900 ED               [12] 3075 	mov	a,r5
      001901 33               [12] 3076 	rlc	a
      001902 95 E0            [12] 3077 	subb	a,acc
      001904 FD               [12] 3078 	mov	r5,a
      001905 90 02 F4         [24] 3079 	mov	dptr,#(_axradio_cb_receive + 0x000f)
      001908 F0               [24] 3080 	movx	@dptr,a
                                   3081 ;	..\src\COMMON\easyax5043.c:380: axradio_cb_receive.st.rx.phy.offset.b.b2 = i;
      001909 90 02 F3         [24] 3082 	mov	dptr,#(_axradio_cb_receive + 0x000e)
      00190C EE               [12] 3083 	mov	a,r6
      00190D F0               [24] 3084 	movx	@dptr,a
                                   3085 ;	..\src\COMMON\easyax5043.c:381: axradio_cb_receive.st.rx.phy.offset.b.b1 = AX5043_FIFODATA;
      00190E 90 40 29         [24] 3086 	mov	dptr,#_AX5043_FIFODATA
      001911 E0               [24] 3087 	movx	a,@dptr
      001912 90 02 F2         [24] 3088 	mov	dptr,#(_axradio_cb_receive + 0x000d)
      001915 F0               [24] 3089 	movx	@dptr,a
                                   3090 ;	..\src\COMMON\easyax5043.c:382: axradio_cb_receive.st.rx.phy.offset.b.b0 = AX5043_FIFODATA;
      001916 90 40 29         [24] 3091 	mov	dptr,#_AX5043_FIFODATA
      001919 E0               [24] 3092 	movx	a,@dptr
      00191A FE               [12] 3093 	mov	r6,a
      00191B 90 02 F1         [24] 3094 	mov	dptr,#(_axradio_cb_receive + 0x000c)
      00191E F0               [24] 3095 	movx	@dptr,a
                                   3096 ;	..\src\COMMON\easyax5043.c:383: break;
      00191F 02 17 0A         [24] 3097 	ljmp	00153$
                                   3098 ;	..\src\COMMON\easyax5043.c:385: case AX5043_FIFOCMD_FREQOFFS:
      001922                       3099 00132$:
                                   3100 ;	..\src\COMMON\easyax5043.c:386: if (!axradio_phy_innerfreqloop || len != 2)
      001922 90 7A B8         [24] 3101 	mov	dptr,#_axradio_phy_innerfreqloop
      001925 E4               [12] 3102 	clr	a
      001926 93               [24] 3103 	movc	a,@a+dptr
      001927 70 03            [24] 3104 	jnz	00276$
      001929 02 1A 40         [24] 3105 	ljmp	00146$
      00192C                       3106 00276$:
      00192C BF 02 02         [24] 3107 	cjne	r7,#0x02,00277$
      00192F 80 03            [24] 3108 	sjmp	00278$
      001931                       3109 00277$:
      001931 02 1A 40         [24] 3110 	ljmp	00146$
      001934                       3111 00278$:
                                   3112 ;	..\src\COMMON\easyax5043.c:388: axradio_cb_receive.st.rx.phy.offset.b.b1 = AX5043_FIFODATA;
      001934 90 40 29         [24] 3113 	mov	dptr,#_AX5043_FIFODATA
      001937 E0               [24] 3114 	movx	a,@dptr
      001938 90 02 F2         [24] 3115 	mov	dptr,#(_axradio_cb_receive + 0x000d)
      00193B F0               [24] 3116 	movx	@dptr,a
                                   3117 ;	..\src\COMMON\easyax5043.c:389: axradio_cb_receive.st.rx.phy.offset.b.b0 = AX5043_FIFODATA;
      00193C 90 40 29         [24] 3118 	mov	dptr,#_AX5043_FIFODATA
      00193F E0               [24] 3119 	movx	a,@dptr
      001940 90 02 F1         [24] 3120 	mov	dptr,#(_axradio_cb_receive + 0x000c)
      001943 F0               [24] 3121 	movx	@dptr,a
                                   3122 ;	..\src\COMMON\easyax5043.c:390: axradio_cb_receive.st.rx.phy.offset.o = axradio_conv_freq_fromreg(signextend16(axradio_cb_receive.st.rx.phy.offset.o));
      001944 90 02 F1         [24] 3123 	mov	dptr,#(_axradio_cb_receive + 0x000c)
      001947 E0               [24] 3124 	movx	a,@dptr
      001948 FB               [12] 3125 	mov	r3,a
      001949 A3               [24] 3126 	inc	dptr
      00194A E0               [24] 3127 	movx	a,@dptr
      00194B FC               [12] 3128 	mov	r4,a
      00194C A3               [24] 3129 	inc	dptr
      00194D E0               [24] 3130 	movx	a,@dptr
      00194E A3               [24] 3131 	inc	dptr
      00194F E0               [24] 3132 	movx	a,@dptr
      001950 8B 82            [24] 3133 	mov	dpl,r3
      001952 8C 83            [24] 3134 	mov	dph,r4
      001954 12 79 AE         [24] 3135 	lcall	_signextend16
      001957 12 08 58         [24] 3136 	lcall	_axradio_conv_freq_fromreg
      00195A AB 82            [24] 3137 	mov	r3,dpl
      00195C AC 83            [24] 3138 	mov	r4,dph
      00195E AD F0            [24] 3139 	mov	r5,b
      001960 FE               [12] 3140 	mov	r6,a
      001961 90 02 F1         [24] 3141 	mov	dptr,#(_axradio_cb_receive + 0x000c)
      001964 EB               [12] 3142 	mov	a,r3
      001965 F0               [24] 3143 	movx	@dptr,a
      001966 EC               [12] 3144 	mov	a,r4
      001967 A3               [24] 3145 	inc	dptr
      001968 F0               [24] 3146 	movx	@dptr,a
      001969 ED               [12] 3147 	mov	a,r5
      00196A A3               [24] 3148 	inc	dptr
      00196B F0               [24] 3149 	movx	@dptr,a
      00196C EE               [12] 3150 	mov	a,r6
      00196D A3               [24] 3151 	inc	dptr
      00196E F0               [24] 3152 	movx	@dptr,a
                                   3153 ;	..\src\COMMON\easyax5043.c:391: break;
      00196F 02 17 0A         [24] 3154 	ljmp	00153$
                                   3155 ;	..\src\COMMON\easyax5043.c:393: case AX5043_FIFOCMD_RSSI:
      001972                       3156 00136$:
                                   3157 ;	..\src\COMMON\easyax5043.c:394: if (len != 1)
      001972 BF 01 02         [24] 3158 	cjne	r7,#0x01,00279$
      001975 80 03            [24] 3159 	sjmp	00280$
      001977                       3160 00279$:
      001977 02 1A 40         [24] 3161 	ljmp	00146$
      00197A                       3162 00280$:
                                   3163 ;	..\src\COMMON\easyax5043.c:397: int8_t r = AX5043_FIFODATA;
      00197A 90 40 29         [24] 3164 	mov	dptr,#_AX5043_FIFODATA
      00197D E0               [24] 3165 	movx	a,@dptr
                                   3166 ;	..\src\COMMON\easyax5043.c:398: axradio_cb_receive.st.rx.phy.rssi = r - (int16_t)axradio_phy_rssioffset;
      00197E FE               [12] 3167 	mov	r6,a
      00197F 33               [12] 3168 	rlc	a
      001980 95 E0            [12] 3169 	subb	a,acc
      001982 FD               [12] 3170 	mov	r5,a
      001983 90 7A C6         [24] 3171 	mov	dptr,#_axradio_phy_rssioffset
      001986 E4               [12] 3172 	clr	a
      001987 93               [24] 3173 	movc	a,@a+dptr
      001988 FC               [12] 3174 	mov	r4,a
      001989 33               [12] 3175 	rlc	a
      00198A 95 E0            [12] 3176 	subb	a,acc
      00198C FB               [12] 3177 	mov	r3,a
      00198D EE               [12] 3178 	mov	a,r6
      00198E C3               [12] 3179 	clr	c
      00198F 9C               [12] 3180 	subb	a,r4
      001990 FE               [12] 3181 	mov	r6,a
      001991 ED               [12] 3182 	mov	a,r5
      001992 9B               [12] 3183 	subb	a,r3
      001993 FD               [12] 3184 	mov	r5,a
      001994 90 02 EF         [24] 3185 	mov	dptr,#(_axradio_cb_receive + 0x000a)
      001997 EE               [12] 3186 	mov	a,r6
      001998 F0               [24] 3187 	movx	@dptr,a
      001999 ED               [12] 3188 	mov	a,r5
      00199A A3               [24] 3189 	inc	dptr
      00199B F0               [24] 3190 	movx	@dptr,a
                                   3191 ;	..\src\COMMON\easyax5043.c:400: break;
      00199C 02 17 0A         [24] 3192 	ljmp	00153$
                                   3193 ;	..\src\COMMON\easyax5043.c:402: case AX5043_FIFOCMD_TIMER:
      00199F                       3194 00139$:
                                   3195 ;	..\src\COMMON\easyax5043.c:403: if (len != 3)
      00199F BF 03 02         [24] 3196 	cjne	r7,#0x03,00281$
      0019A2 80 03            [24] 3197 	sjmp	00282$
      0019A4                       3198 00281$:
      0019A4 02 1A 40         [24] 3199 	ljmp	00146$
      0019A7                       3200 00282$:
                                   3201 ;	..\src\COMMON\easyax5043.c:407: axradio_cb_receive.st.time.b.b3 = 0;
      0019A7 90 02 EE         [24] 3202 	mov	dptr,#(_axradio_cb_receive + 0x0009)
      0019AA E4               [12] 3203 	clr	a
      0019AB F0               [24] 3204 	movx	@dptr,a
                                   3205 ;	..\src\COMMON\easyax5043.c:408: axradio_cb_receive.st.time.b.b2 = AX5043_FIFODATA;
      0019AC 90 40 29         [24] 3206 	mov	dptr,#_AX5043_FIFODATA
      0019AF E0               [24] 3207 	movx	a,@dptr
      0019B0 90 02 ED         [24] 3208 	mov	dptr,#(_axradio_cb_receive + 0x0008)
      0019B3 F0               [24] 3209 	movx	@dptr,a
                                   3210 ;	..\src\COMMON\easyax5043.c:409: axradio_cb_receive.st.time.b.b1 = AX5043_FIFODATA;
      0019B4 90 40 29         [24] 3211 	mov	dptr,#_AX5043_FIFODATA
      0019B7 E0               [24] 3212 	movx	a,@dptr
      0019B8 90 02 EC         [24] 3213 	mov	dptr,#(_axradio_cb_receive + 0x0007)
      0019BB F0               [24] 3214 	movx	@dptr,a
                                   3215 ;	..\src\COMMON\easyax5043.c:410: axradio_cb_receive.st.time.b.b0 = AX5043_FIFODATA;
      0019BC 90 40 29         [24] 3216 	mov	dptr,#_AX5043_FIFODATA
      0019BF E0               [24] 3217 	movx	a,@dptr
      0019C0 FE               [12] 3218 	mov	r6,a
      0019C1 90 02 EB         [24] 3219 	mov	dptr,#(_axradio_cb_receive + 0x0006)
      0019C4 F0               [24] 3220 	movx	@dptr,a
                                   3221 ;	..\src\COMMON\easyax5043.c:411: break;
      0019C5 02 17 0A         [24] 3222 	ljmp	00153$
                                   3223 ;	..\src\COMMON\easyax5043.c:413: case AX5043_FIFOCMD_ANTRSSI:
      0019C8                       3224 00142$:
                                   3225 ;	..\src\COMMON\easyax5043.c:414: if (!len)
      0019C8 EF               [12] 3226 	mov	a,r7
      0019C9 70 03            [24] 3227 	jnz	00283$
      0019CB 02 17 0A         [24] 3228 	ljmp	00153$
      0019CE                       3229 00283$:
                                   3230 ;	..\src\COMMON\easyax5043.c:416: update_timeanchor();
      0019CE C0 07            [24] 3231 	push	ar7
      0019D0 12 15 BA         [24] 3232 	lcall	_update_timeanchor
                                   3233 ;	..\src\COMMON\easyax5043.c:417: wtimer_remove_callback(&axradio_cb_channelstate.cb);
      0019D3 90 03 11         [24] 3234 	mov	dptr,#_axradio_cb_channelstate
      0019D6 12 74 C0         [24] 3235 	lcall	_wtimer_remove_callback
                                   3236 ;	..\src\COMMON\easyax5043.c:418: axradio_cb_channelstate.st.error = AXRADIO_ERR_NOERROR;
      0019D9 90 03 16         [24] 3237 	mov	dptr,#(_axradio_cb_channelstate + 0x0005)
      0019DC E4               [12] 3238 	clr	a
      0019DD F0               [24] 3239 	movx	@dptr,a
                                   3240 ;	..\src\COMMON\easyax5043.c:420: int8_t r = AX5043_FIFODATA;
      0019DE 90 40 29         [24] 3241 	mov	dptr,#_AX5043_FIFODATA
      0019E1 E0               [24] 3242 	movx	a,@dptr
                                   3243 ;	..\src\COMMON\easyax5043.c:421: axradio_cb_channelstate.st.cs.rssi = r - (int16_t)axradio_phy_rssioffset;
      0019E2 FE               [12] 3244 	mov	r6,a
      0019E3 FC               [12] 3245 	mov	r4,a
      0019E4 33               [12] 3246 	rlc	a
      0019E5 95 E0            [12] 3247 	subb	a,acc
      0019E7 FD               [12] 3248 	mov	r5,a
      0019E8 90 7A C6         [24] 3249 	mov	dptr,#_axradio_phy_rssioffset
      0019EB E4               [12] 3250 	clr	a
      0019EC 93               [24] 3251 	movc	a,@a+dptr
      0019ED FB               [12] 3252 	mov	r3,a
      0019EE 33               [12] 3253 	rlc	a
      0019EF 95 E0            [12] 3254 	subb	a,acc
      0019F1 FA               [12] 3255 	mov	r2,a
      0019F2 EC               [12] 3256 	mov	a,r4
      0019F3 C3               [12] 3257 	clr	c
      0019F4 9B               [12] 3258 	subb	a,r3
      0019F5 FC               [12] 3259 	mov	r4,a
      0019F6 ED               [12] 3260 	mov	a,r5
      0019F7 9A               [12] 3261 	subb	a,r2
      0019F8 FD               [12] 3262 	mov	r5,a
      0019F9 90 03 1B         [24] 3263 	mov	dptr,#(_axradio_cb_channelstate + 0x000a)
      0019FC EC               [12] 3264 	mov	a,r4
      0019FD F0               [24] 3265 	movx	@dptr,a
      0019FE ED               [12] 3266 	mov	a,r5
      0019FF A3               [24] 3267 	inc	dptr
      001A00 F0               [24] 3268 	movx	@dptr,a
                                   3269 ;	..\src\COMMON\easyax5043.c:422: axradio_cb_channelstate.st.cs.busy = r >= axradio_phy_channelbusy;
      001A01 90 7A C8         [24] 3270 	mov	dptr,#_axradio_phy_channelbusy
      001A04 E4               [12] 3271 	clr	a
      001A05 93               [24] 3272 	movc	a,@a+dptr
      001A06 FD               [12] 3273 	mov	r5,a
      001A07 C3               [12] 3274 	clr	c
      001A08 EE               [12] 3275 	mov	a,r6
      001A09 64 80            [12] 3276 	xrl	a,#0x80
      001A0B 8D F0            [24] 3277 	mov	b,r5
      001A0D 63 F0 80         [24] 3278 	xrl	b,#0x80
      001A10 95 F0            [12] 3279 	subb	a,b
      001A12 B3               [12] 3280 	cpl	c
      001A13 92 08            [24] 3281 	mov	b0,c
      001A15 E4               [12] 3282 	clr	a
      001A16 33               [12] 3283 	rlc	a
      001A17 90 03 1D         [24] 3284 	mov	dptr,#(_axradio_cb_channelstate + 0x000c)
      001A1A F0               [24] 3285 	movx	@dptr,a
                                   3286 ;	..\src\COMMON\easyax5043.c:424: axradio_cb_channelstate.st.time.t = axradio_timeanchor.radiotimer;
      001A1B 90 00 CD         [24] 3287 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      001A1E E0               [24] 3288 	movx	a,@dptr
      001A1F FB               [12] 3289 	mov	r3,a
      001A20 A3               [24] 3290 	inc	dptr
      001A21 E0               [24] 3291 	movx	a,@dptr
      001A22 FC               [12] 3292 	mov	r4,a
      001A23 A3               [24] 3293 	inc	dptr
      001A24 E0               [24] 3294 	movx	a,@dptr
      001A25 FD               [12] 3295 	mov	r5,a
      001A26 A3               [24] 3296 	inc	dptr
      001A27 E0               [24] 3297 	movx	a,@dptr
      001A28 FE               [12] 3298 	mov	r6,a
      001A29 90 03 17         [24] 3299 	mov	dptr,#(_axradio_cb_channelstate + 0x0006)
      001A2C EB               [12] 3300 	mov	a,r3
      001A2D F0               [24] 3301 	movx	@dptr,a
      001A2E EC               [12] 3302 	mov	a,r4
      001A2F A3               [24] 3303 	inc	dptr
      001A30 F0               [24] 3304 	movx	@dptr,a
      001A31 ED               [12] 3305 	mov	a,r5
      001A32 A3               [24] 3306 	inc	dptr
      001A33 F0               [24] 3307 	movx	@dptr,a
      001A34 EE               [12] 3308 	mov	a,r6
      001A35 A3               [24] 3309 	inc	dptr
      001A36 F0               [24] 3310 	movx	@dptr,a
                                   3311 ;	..\src\COMMON\easyax5043.c:425: wtimer_add_callback(&axradio_cb_channelstate.cb);
      001A37 90 03 11         [24] 3312 	mov	dptr,#_axradio_cb_channelstate
      001A3A 12 66 0B         [24] 3313 	lcall	_wtimer_add_callback
      001A3D D0 07            [24] 3314 	pop	ar7
                                   3315 ;	..\src\COMMON\easyax5043.c:426: --len;
      001A3F 1F               [12] 3316 	dec	r7
                                   3317 ;	..\src\COMMON\easyax5043.c:431: dropchunk:
      001A40                       3318 00146$:
                                   3319 ;	..\src\COMMON\easyax5043.c:432: if (!len)
      001A40 EF               [12] 3320 	mov	a,r7
      001A41 70 03            [24] 3321 	jnz	00284$
      001A43 02 17 0A         [24] 3322 	ljmp	00153$
      001A46                       3323 00284$:
                                   3324 ;	..\src\COMMON\easyax5043.c:435: do {
      001A46                       3325 00149$:
                                   3326 ;	..\src\COMMON\easyax5043.c:436: AX5043_FIFODATA;        // purge FIFO
      001A46 90 40 29         [24] 3327 	mov	dptr,#_AX5043_FIFODATA
      001A49 E0               [24] 3328 	movx	a,@dptr
                                   3329 ;	..\src\COMMON\easyax5043.c:438: while (--i);
      001A4A DF FA            [24] 3330 	djnz	r7,00149$
                                   3331 ;	..\src\COMMON\easyax5043.c:440: } // end switch(fifo_cmd)
      001A4C 02 17 0A         [24] 3332 	ljmp	00153$
                                   3333 ;------------------------------------------------------------
                                   3334 ;Allocation info for local variables in function 'transmit_isr'
                                   3335 ;------------------------------------------------------------
                                   3336 ;cnt                       Allocated to registers r7 
                                   3337 ;byte                      Allocated to registers r7 
                                   3338 ;len_byte                  Allocated to registers r4 
                                   3339 ;i                         Allocated to registers r3 
                                   3340 ;byte                      Allocated to registers r6 
                                   3341 ;flags                     Allocated to registers r6 
                                   3342 ;len                       Allocated to registers r4 r5 
                                   3343 ;------------------------------------------------------------
                                   3344 ;	..\src\COMMON\easyax5043.c:444: static __reentrantb void transmit_isr(void) __reentrant
                                   3345 ;	-----------------------------------------
                                   3346 ;	 function transmit_isr
                                   3347 ;	-----------------------------------------
      001A4F                       3348 _transmit_isr:
                                   3349 ;	..\src\COMMON\easyax5043.c:583: axradio_trxstate = trxstate_tx_waitdone;
      001A4F                       3350 00157$:
                                   3351 ;	..\src\COMMON\easyax5043.c:447: uint8_t cnt = AX5043_FIFOFREE0;
      001A4F 90 40 2D         [24] 3352 	mov	dptr,#_AX5043_FIFOFREE0
      001A52 E0               [24] 3353 	movx	a,@dptr
      001A53 FF               [12] 3354 	mov	r7,a
                                   3355 ;	..\src\COMMON\easyax5043.c:448: if (AX5043_FIFOFREE1)
      001A54 90 40 2C         [24] 3356 	mov	dptr,#_AX5043_FIFOFREE1
      001A57 E0               [24] 3357 	movx	a,@dptr
      001A58 E0               [24] 3358 	movx	a,@dptr
      001A59 60 02            [24] 3359 	jz	00102$
                                   3360 ;	..\src\COMMON\easyax5043.c:449: cnt = 0xff;
      001A5B 7F FF            [12] 3361 	mov	r7,#0xff
      001A5D                       3362 00102$:
                                   3363 ;	..\src\COMMON\easyax5043.c:450: switch (axradio_trxstate) {
      001A5D AE 0C            [24] 3364 	mov	r6,_axradio_trxstate
      001A5F BE 0A 02         [24] 3365 	cjne	r6,#0x0a,00246$
      001A62 80 0D            [24] 3366 	sjmp	00103$
      001A64                       3367 00246$:
      001A64 BE 0B 03         [24] 3368 	cjne	r6,#0x0b,00247$
      001A67 02 1B 03         [24] 3369 	ljmp	00115$
      001A6A                       3370 00247$:
      001A6A BE 0C 03         [24] 3371 	cjne	r6,#0x0c,00248$
      001A6D 02 1C D4         [24] 3372 	ljmp	00138$
      001A70                       3373 00248$:
      001A70 22               [24] 3374 	ret
                                   3375 ;	..\src\COMMON\easyax5043.c:451: case trxstate_tx_longpreamble:
      001A71                       3376 00103$:
                                   3377 ;	..\src\COMMON\easyax5043.c:452: if (!axradio_txbuffer_cnt) {
      001A71 90 00 BA         [24] 3378 	mov	dptr,#_axradio_txbuffer_cnt
      001A74 E0               [24] 3379 	movx	a,@dptr
      001A75 FD               [12] 3380 	mov	r5,a
      001A76 A3               [24] 3381 	inc	dptr
      001A77 E0               [24] 3382 	movx	a,@dptr
      001A78 FE               [12] 3383 	mov	r6,a
      001A79 4D               [12] 3384 	orl	a,r5
      001A7A 70 37            [24] 3385 	jnz	00109$
                                   3386 ;	..\src\COMMON\easyax5043.c:453: axradio_trxstate = trxstate_tx_shortpreamble;
      001A7C 75 0C 0B         [24] 3387 	mov	_axradio_trxstate,#0x0b
                                   3388 ;	..\src\COMMON\easyax5043.c:454: if( axradio_mode == AXRADIO_MODE_WOR_TRANSMIT || axradio_mode == AXRADIO_MODE_WOR_ACK_TRANSMIT )
      001A7F 74 11            [12] 3389 	mov	a,#0x11
      001A81 B5 0B 02         [24] 3390 	cjne	a,_axradio_mode,00250$
      001A84 80 05            [24] 3391 	sjmp	00104$
      001A86                       3392 00250$:
      001A86 74 13            [12] 3393 	mov	a,#0x13
      001A88 B5 0B 14         [24] 3394 	cjne	a,_axradio_mode,00105$
      001A8B                       3395 00104$:
                                   3396 ;	..\src\COMMON\easyax5043.c:455: axradio_txbuffer_cnt = axradio_phy_preamble_wor_len;
      001A8B 90 7A D0         [24] 3397 	mov	dptr,#_axradio_phy_preamble_wor_len
      001A8E E4               [12] 3398 	clr	a
      001A8F 93               [24] 3399 	movc	a,@a+dptr
      001A90 FB               [12] 3400 	mov	r3,a
      001A91 74 01            [12] 3401 	mov	a,#0x01
      001A93 93               [24] 3402 	movc	a,@a+dptr
      001A94 FC               [12] 3403 	mov	r4,a
      001A95 90 00 BA         [24] 3404 	mov	dptr,#_axradio_txbuffer_cnt
      001A98 EB               [12] 3405 	mov	a,r3
      001A99 F0               [24] 3406 	movx	@dptr,a
      001A9A EC               [12] 3407 	mov	a,r4
      001A9B A3               [24] 3408 	inc	dptr
      001A9C F0               [24] 3409 	movx	@dptr,a
      001A9D 80 64            [24] 3410 	sjmp	00115$
      001A9F                       3411 00105$:
                                   3412 ;	..\src\COMMON\easyax5043.c:457: axradio_txbuffer_cnt = axradio_phy_preamble_len;
      001A9F 90 7A D4         [24] 3413 	mov	dptr,#_axradio_phy_preamble_len
      001AA2 E4               [12] 3414 	clr	a
      001AA3 93               [24] 3415 	movc	a,@a+dptr
      001AA4 FB               [12] 3416 	mov	r3,a
      001AA5 74 01            [12] 3417 	mov	a,#0x01
      001AA7 93               [24] 3418 	movc	a,@a+dptr
      001AA8 FC               [12] 3419 	mov	r4,a
      001AA9 90 00 BA         [24] 3420 	mov	dptr,#_axradio_txbuffer_cnt
      001AAC EB               [12] 3421 	mov	a,r3
      001AAD F0               [24] 3422 	movx	@dptr,a
      001AAE EC               [12] 3423 	mov	a,r4
      001AAF A3               [24] 3424 	inc	dptr
      001AB0 F0               [24] 3425 	movx	@dptr,a
                                   3426 ;	..\src\COMMON\easyax5043.c:458: goto shortpreamble;
      001AB1 80 50            [24] 3427 	sjmp	00115$
      001AB3                       3428 00109$:
                                   3429 ;	..\src\COMMON\easyax5043.c:460: if (cnt < 4)
      001AB3 BF 04 00         [24] 3430 	cjne	r7,#0x04,00253$
      001AB6                       3431 00253$:
      001AB6 50 03            [24] 3432 	jnc	00254$
      001AB8 02 1D 75         [24] 3433 	ljmp	00153$
      001ABB                       3434 00254$:
                                   3435 ;	..\src\COMMON\easyax5043.c:462: cnt = 7;
      001ABB 7F 07            [12] 3436 	mov	r7,#0x07
                                   3437 ;	..\src\COMMON\easyax5043.c:463: if (axradio_txbuffer_cnt < 7)
      001ABD C3               [12] 3438 	clr	c
      001ABE ED               [12] 3439 	mov	a,r5
      001ABF 94 07            [12] 3440 	subb	a,#0x07
      001AC1 EE               [12] 3441 	mov	a,r6
      001AC2 94 00            [12] 3442 	subb	a,#0x00
      001AC4 50 02            [24] 3443 	jnc	00113$
                                   3444 ;	..\src\COMMON\easyax5043.c:464: cnt = axradio_txbuffer_cnt;
      001AC6 8D 07            [24] 3445 	mov	ar7,r5
      001AC8                       3446 00113$:
                                   3447 ;	..\src\COMMON\easyax5043.c:465: axradio_txbuffer_cnt -= cnt;
      001AC8 8F 05            [24] 3448 	mov	ar5,r7
      001ACA 7E 00            [12] 3449 	mov	r6,#0x00
      001ACC 90 00 BA         [24] 3450 	mov	dptr,#_axradio_txbuffer_cnt
      001ACF E0               [24] 3451 	movx	a,@dptr
      001AD0 FB               [12] 3452 	mov	r3,a
      001AD1 A3               [24] 3453 	inc	dptr
      001AD2 E0               [24] 3454 	movx	a,@dptr
      001AD3 FC               [12] 3455 	mov	r4,a
      001AD4 90 00 BA         [24] 3456 	mov	dptr,#_axradio_txbuffer_cnt
      001AD7 EB               [12] 3457 	mov	a,r3
      001AD8 C3               [12] 3458 	clr	c
      001AD9 9D               [12] 3459 	subb	a,r5
      001ADA F0               [24] 3460 	movx	@dptr,a
      001ADB EC               [12] 3461 	mov	a,r4
      001ADC 9E               [12] 3462 	subb	a,r6
      001ADD A3               [24] 3463 	inc	dptr
      001ADE F0               [24] 3464 	movx	@dptr,a
                                   3465 ;	..\src\COMMON\easyax5043.c:466: cnt <<= 5;
      001ADF EF               [12] 3466 	mov	a,r7
      001AE0 C4               [12] 3467 	swap	a
      001AE1 23               [12] 3468 	rl	a
      001AE2 54 E0            [12] 3469 	anl	a,#0xe0
      001AE4 FF               [12] 3470 	mov	r7,a
                                   3471 ;	..\src\COMMON\easyax5043.c:467: AX5043_FIFODATA = AX5043_FIFOCMD_REPEATDATA | (3 << 5);
      001AE5 90 40 29         [24] 3472 	mov	dptr,#_AX5043_FIFODATA
      001AE8 74 62            [12] 3473 	mov	a,#0x62
      001AEA F0               [24] 3474 	movx	@dptr,a
                                   3475 ;	..\src\COMMON\easyax5043.c:468: AX5043_FIFODATA = axradio_phy_preamble_flags;
      001AEB 90 7A D7         [24] 3476 	mov	dptr,#_axradio_phy_preamble_flags
      001AEE E4               [12] 3477 	clr	a
      001AEF 93               [24] 3478 	movc	a,@a+dptr
      001AF0 90 40 29         [24] 3479 	mov	dptr,#_AX5043_FIFODATA
      001AF3 F0               [24] 3480 	movx	@dptr,a
                                   3481 ;	..\src\COMMON\easyax5043.c:469: AX5043_FIFODATA = cnt;
      001AF4 EF               [12] 3482 	mov	a,r7
      001AF5 F0               [24] 3483 	movx	@dptr,a
                                   3484 ;	..\src\COMMON\easyax5043.c:470: AX5043_FIFODATA = axradio_phy_preamble_byte;
      001AF6 90 7A D6         [24] 3485 	mov	dptr,#_axradio_phy_preamble_byte
      001AF9 E4               [12] 3486 	clr	a
      001AFA 93               [24] 3487 	movc	a,@a+dptr
      001AFB FE               [12] 3488 	mov	r6,a
      001AFC 90 40 29         [24] 3489 	mov	dptr,#_AX5043_FIFODATA
      001AFF F0               [24] 3490 	movx	@dptr,a
                                   3491 ;	..\src\COMMON\easyax5043.c:471: break;
      001B00 02 1A 4F         [24] 3492 	ljmp	00157$
                                   3493 ;	..\src\COMMON\easyax5043.c:474: shortpreamble:
      001B03                       3494 00115$:
                                   3495 ;	..\src\COMMON\easyax5043.c:475: if (!axradio_txbuffer_cnt) {
      001B03 90 00 BA         [24] 3496 	mov	dptr,#_axradio_txbuffer_cnt
      001B06 E0               [24] 3497 	movx	a,@dptr
      001B07 FD               [12] 3498 	mov	r5,a
      001B08 A3               [24] 3499 	inc	dptr
      001B09 E0               [24] 3500 	movx	a,@dptr
      001B0A FE               [12] 3501 	mov	r6,a
      001B0B 4D               [12] 3502 	orl	a,r5
      001B0C 60 03            [24] 3503 	jz	00256$
      001B0E 02 1B E8         [24] 3504 	ljmp	00128$
      001B11                       3505 00256$:
                                   3506 ;	..\src\COMMON\easyax5043.c:476: if (cnt < 15)
      001B11 BF 0F 00         [24] 3507 	cjne	r7,#0x0f,00257$
      001B14                       3508 00257$:
      001B14 50 03            [24] 3509 	jnc	00258$
      001B16 02 1D 75         [24] 3510 	ljmp	00153$
      001B19                       3511 00258$:
                                   3512 ;	..\src\COMMON\easyax5043.c:478: if (axradio_phy_preamble_appendbits) {
      001B19 90 7A D8         [24] 3513 	mov	dptr,#_axradio_phy_preamble_appendbits
      001B1C E4               [12] 3514 	clr	a
      001B1D 93               [24] 3515 	movc	a,@a+dptr
      001B1E FC               [12] 3516 	mov	r4,a
      001B1F 60 6F            [24] 3517 	jz	00122$
                                   3518 ;	..\src\COMMON\easyax5043.c:480: AX5043_FIFODATA = AX5043_FIFOCMD_DATA | (2 << 5);
      001B21 90 40 29         [24] 3519 	mov	dptr,#_AX5043_FIFODATA
      001B24 74 41            [12] 3520 	mov	a,#0x41
      001B26 F0               [24] 3521 	movx	@dptr,a
                                   3522 ;	..\src\COMMON\easyax5043.c:481: AX5043_FIFODATA = 0x1C;
      001B27 74 1C            [12] 3523 	mov	a,#0x1c
      001B29 F0               [24] 3524 	movx	@dptr,a
                                   3525 ;	..\src\COMMON\easyax5043.c:482: byte = axradio_phy_preamble_appendpattern;
      001B2A 90 7A D9         [24] 3526 	mov	dptr,#_axradio_phy_preamble_appendpattern
      001B2D E4               [12] 3527 	clr	a
      001B2E 93               [24] 3528 	movc	a,@a+dptr
      001B2F FB               [12] 3529 	mov	r3,a
      001B30 FF               [12] 3530 	mov	r7,a
                                   3531 ;	..\src\COMMON\easyax5043.c:483: if (AX5043_PKTADDRCFG & 0x80) {
      001B31 90 42 00         [24] 3532 	mov	dptr,#_AX5043_PKTADDRCFG
      001B34 E0               [24] 3533 	movx	a,@dptr
      001B35 FA               [12] 3534 	mov	r2,a
      001B36 30 E7 26         [24] 3535 	jnb	acc.7,00119$
                                   3536 ;	..\src\COMMON\easyax5043.c:485: byte &= 0xFF << (8-axradio_phy_preamble_appendbits);
      001B39 74 08            [12] 3537 	mov	a,#0x08
      001B3B C3               [12] 3538 	clr	c
      001B3C 9C               [12] 3539 	subb	a,r4
      001B3D F5 F0            [12] 3540 	mov	b,a
      001B3F 05 F0            [12] 3541 	inc	b
      001B41 74 FF            [12] 3542 	mov	a,#0xff
      001B43 80 02            [24] 3543 	sjmp	00263$
      001B45                       3544 00261$:
      001B45 25 E0            [12] 3545 	add	a,acc
      001B47                       3546 00263$:
      001B47 D5 F0 FB         [24] 3547 	djnz	b,00261$
      001B4A FA               [12] 3548 	mov	r2,a
      001B4B 52 07            [12] 3549 	anl	ar7,a
                                   3550 ;	..\src\COMMON\easyax5043.c:486: byte |= 0x80 >> axradio_phy_preamble_appendbits;
      001B4D 8C F0            [24] 3551 	mov	b,r4
      001B4F 05 F0            [12] 3552 	inc	b
      001B51 74 80            [12] 3553 	mov	a,#0x80
      001B53 80 02            [24] 3554 	sjmp	00265$
      001B55                       3555 00264$:
      001B55 C3               [12] 3556 	clr	c
      001B56 13               [12] 3557 	rrc	a
      001B57                       3558 00265$:
      001B57 D5 F0 FB         [24] 3559 	djnz	b,00264$
      001B5A FA               [12] 3560 	mov	r2,a
      001B5B 42 07            [12] 3561 	orl	ar7,a
      001B5D 80 2C            [24] 3562 	sjmp	00120$
      001B5F                       3563 00119$:
                                   3564 ;	..\src\COMMON\easyax5043.c:489: byte &= 0xFF >> (8-axradio_phy_preamble_appendbits);
      001B5F 8C 02            [24] 3565 	mov	ar2,r4
      001B61 7B 00            [12] 3566 	mov	r3,#0x00
      001B63 74 08            [12] 3567 	mov	a,#0x08
      001B65 C3               [12] 3568 	clr	c
      001B66 9A               [12] 3569 	subb	a,r2
      001B67 FA               [12] 3570 	mov	r2,a
      001B68 E4               [12] 3571 	clr	a
      001B69 9B               [12] 3572 	subb	a,r3
      001B6A FB               [12] 3573 	mov	r3,a
      001B6B 8A F0            [24] 3574 	mov	b,r2
      001B6D 05 F0            [12] 3575 	inc	b
      001B6F 74 FF            [12] 3576 	mov	a,#0xff
      001B71 80 02            [24] 3577 	sjmp	00267$
      001B73                       3578 00266$:
      001B73 C3               [12] 3579 	clr	c
      001B74 13               [12] 3580 	rrc	a
      001B75                       3581 00267$:
      001B75 D5 F0 FB         [24] 3582 	djnz	b,00266$
      001B78 FA               [12] 3583 	mov	r2,a
      001B79 52 07            [12] 3584 	anl	ar7,a
                                   3585 ;	..\src\COMMON\easyax5043.c:490: byte |= 0x01 << axradio_phy_preamble_appendbits;
      001B7B 8C F0            [24] 3586 	mov	b,r4
      001B7D 05 F0            [12] 3587 	inc	b
      001B7F 74 01            [12] 3588 	mov	a,#0x01
      001B81 80 02            [24] 3589 	sjmp	00270$
      001B83                       3590 00268$:
      001B83 25 E0            [12] 3591 	add	a,acc
      001B85                       3592 00270$:
      001B85 D5 F0 FB         [24] 3593 	djnz	b,00268$
      001B88 FC               [12] 3594 	mov	r4,a
      001B89 42 07            [12] 3595 	orl	ar7,a
      001B8B                       3596 00120$:
                                   3597 ;	..\src\COMMON\easyax5043.c:492: AX5043_FIFODATA = byte;
      001B8B 90 40 29         [24] 3598 	mov	dptr,#_AX5043_FIFODATA
      001B8E EF               [12] 3599 	mov	a,r7
      001B8F F0               [24] 3600 	movx	@dptr,a
      001B90                       3601 00122$:
                                   3602 ;	..\src\COMMON\easyax5043.c:498: if ((AX5043_FRAMING & 0x0E) == 0x06 && axradio_framing_synclen) {
      001B90 90 40 12         [24] 3603 	mov	dptr,#_AX5043_FRAMING
      001B93 E0               [24] 3604 	movx	a,@dptr
      001B94 FC               [12] 3605 	mov	r4,a
      001B95 53 04 0E         [24] 3606 	anl	ar4,#0x0e
      001B98 BC 06 47         [24] 3607 	cjne	r4,#0x06,00125$
      001B9B 90 7A E2         [24] 3608 	mov	dptr,#_axradio_framing_synclen
      001B9E E4               [12] 3609 	clr	a
      001B9F 93               [24] 3610 	movc	a,@a+dptr
      001BA0 FC               [12] 3611 	mov	r4,a
      001BA1 E4               [12] 3612 	clr	a
      001BA2 93               [24] 3613 	movc	a,@a+dptr
      001BA3 60 3D            [24] 3614 	jz	00125$
                                   3615 ;	..\src\COMMON\easyax5043.c:500: uint8_t len_byte = axradio_framing_synclen;
                                   3616 ;	..\src\COMMON\easyax5043.c:501: uint8_t i = (len_byte & 0x07) ? 0x04 : 0;
      001BA5 EC               [12] 3617 	mov	a,r4
      001BA6 54 07            [12] 3618 	anl	a,#0x07
      001BA8 60 02            [24] 3619 	jz	00161$
      001BAA 74 04            [12] 3620 	mov	a,#0x04
      001BAC                       3621 00161$:
      001BAC FB               [12] 3622 	mov	r3,a
                                   3623 ;	..\src\COMMON\easyax5043.c:503: len_byte += 7;
      001BAD 74 07            [12] 3624 	mov	a,#0x07
      001BAF 2C               [12] 3625 	add	a,r4
                                   3626 ;	..\src\COMMON\easyax5043.c:504: len_byte >>= 3;
      001BB0 C4               [12] 3627 	swap	a
      001BB1 23               [12] 3628 	rl	a
      001BB2 54 1F            [12] 3629 	anl	a,#0x1f
                                   3630 ;	..\src\COMMON\easyax5043.c:505: AX5043_FIFODATA = AX5043_FIFOCMD_DATA | ((len_byte + 1) << 5);
      001BB4 FC               [12] 3631 	mov	r4,a
      001BB5 04               [12] 3632 	inc	a
      001BB6 C4               [12] 3633 	swap	a
      001BB7 23               [12] 3634 	rl	a
      001BB8 54 E0            [12] 3635 	anl	a,#0xe0
      001BBA FA               [12] 3636 	mov	r2,a
      001BBB 90 40 29         [24] 3637 	mov	dptr,#_AX5043_FIFODATA
      001BBE 74 01            [12] 3638 	mov	a,#0x01
      001BC0 4A               [12] 3639 	orl	a,r2
      001BC1 F0               [24] 3640 	movx	@dptr,a
                                   3641 ;	..\src\COMMON\easyax5043.c:506: AX5043_FIFODATA = axradio_framing_syncflags | i;
      001BC2 90 7A E7         [24] 3642 	mov	dptr,#_axradio_framing_syncflags
      001BC5 E4               [12] 3643 	clr	a
      001BC6 93               [24] 3644 	movc	a,@a+dptr
      001BC7 FA               [12] 3645 	mov	r2,a
      001BC8 90 40 29         [24] 3646 	mov	dptr,#_AX5043_FIFODATA
      001BCB EB               [12] 3647 	mov	a,r3
      001BCC 4A               [12] 3648 	orl	a,r2
      001BCD F0               [24] 3649 	movx	@dptr,a
                                   3650 ;	..\src\COMMON\easyax5043.c:507: for (i = 0; i < len_byte; ++i) {
      001BCE 7B 00            [12] 3651 	mov	r3,#0x00
      001BD0                       3652 00155$:
      001BD0 C3               [12] 3653 	clr	c
      001BD1 EB               [12] 3654 	mov	a,r3
      001BD2 9C               [12] 3655 	subb	a,r4
      001BD3 50 0D            [24] 3656 	jnc	00125$
                                   3657 ;	..\src\COMMON\easyax5043.c:509: AX5043_FIFODATA = axradio_framing_syncword[i];
      001BD5 EB               [12] 3658 	mov	a,r3
      001BD6 90 7A E3         [24] 3659 	mov	dptr,#_axradio_framing_syncword
      001BD9 93               [24] 3660 	movc	a,@a+dptr
      001BDA FA               [12] 3661 	mov	r2,a
      001BDB 90 40 29         [24] 3662 	mov	dptr,#_AX5043_FIFODATA
      001BDE F0               [24] 3663 	movx	@dptr,a
                                   3664 ;	..\src\COMMON\easyax5043.c:507: for (i = 0; i < len_byte; ++i) {
      001BDF 0B               [12] 3665 	inc	r3
      001BE0 80 EE            [24] 3666 	sjmp	00155$
      001BE2                       3667 00125$:
                                   3668 ;	..\src\COMMON\easyax5043.c:516: axradio_trxstate = trxstate_tx_packet;
      001BE2 75 0C 0C         [24] 3669 	mov	_axradio_trxstate,#0x0c
                                   3670 ;	..\src\COMMON\easyax5043.c:517: break;
      001BE5 02 1A 4F         [24] 3671 	ljmp	00157$
      001BE8                       3672 00128$:
                                   3673 ;	..\src\COMMON\easyax5043.c:519: if (cnt < 4)
      001BE8 BF 04 00         [24] 3674 	cjne	r7,#0x04,00276$
      001BEB                       3675 00276$:
      001BEB 50 03            [24] 3676 	jnc	00277$
      001BED 02 1D 75         [24] 3677 	ljmp	00153$
      001BF0                       3678 00277$:
                                   3679 ;	..\src\COMMON\easyax5043.c:521: cnt = 255;
      001BF0 7F FF            [12] 3680 	mov	r7,#0xff
                                   3681 ;	..\src\COMMON\easyax5043.c:522: if (axradio_txbuffer_cnt < 255*8)
      001BF2 C3               [12] 3682 	clr	c
      001BF3 ED               [12] 3683 	mov	a,r5
      001BF4 94 F8            [12] 3684 	subb	a,#0xf8
      001BF6 EE               [12] 3685 	mov	a,r6
      001BF7 94 07            [12] 3686 	subb	a,#0x07
      001BF9 50 12            [24] 3687 	jnc	00132$
                                   3688 ;	..\src\COMMON\easyax5043.c:523: cnt = axradio_txbuffer_cnt >> 3;
      001BFB EE               [12] 3689 	mov	a,r6
      001BFC C4               [12] 3690 	swap	a
      001BFD 23               [12] 3691 	rl	a
      001BFE CD               [12] 3692 	xch	a,r5
      001BFF C4               [12] 3693 	swap	a
      001C00 23               [12] 3694 	rl	a
      001C01 54 1F            [12] 3695 	anl	a,#0x1f
      001C03 6D               [12] 3696 	xrl	a,r5
      001C04 CD               [12] 3697 	xch	a,r5
      001C05 54 1F            [12] 3698 	anl	a,#0x1f
      001C07 CD               [12] 3699 	xch	a,r5
      001C08 6D               [12] 3700 	xrl	a,r5
      001C09 CD               [12] 3701 	xch	a,r5
      001C0A FE               [12] 3702 	mov	r6,a
      001C0B 8D 07            [24] 3703 	mov	ar7,r5
      001C0D                       3704 00132$:
                                   3705 ;	..\src\COMMON\easyax5043.c:524: if (cnt) {
      001C0D EF               [12] 3706 	mov	a,r7
      001C0E 60 42            [24] 3707 	jz	00134$
                                   3708 ;	..\src\COMMON\easyax5043.c:525: axradio_txbuffer_cnt -= ((uint16_t)cnt) << 3;
      001C10 8F 05            [24] 3709 	mov	ar5,r7
      001C12 E4               [12] 3710 	clr	a
      001C13 03               [12] 3711 	rr	a
      001C14 54 F8            [12] 3712 	anl	a,#0xf8
      001C16 CD               [12] 3713 	xch	a,r5
      001C17 C4               [12] 3714 	swap	a
      001C18 03               [12] 3715 	rr	a
      001C19 CD               [12] 3716 	xch	a,r5
      001C1A 6D               [12] 3717 	xrl	a,r5
      001C1B CD               [12] 3718 	xch	a,r5
      001C1C 54 F8            [12] 3719 	anl	a,#0xf8
      001C1E CD               [12] 3720 	xch	a,r5
      001C1F 6D               [12] 3721 	xrl	a,r5
      001C20 FE               [12] 3722 	mov	r6,a
      001C21 90 00 BA         [24] 3723 	mov	dptr,#_axradio_txbuffer_cnt
      001C24 E0               [24] 3724 	movx	a,@dptr
      001C25 FB               [12] 3725 	mov	r3,a
      001C26 A3               [24] 3726 	inc	dptr
      001C27 E0               [24] 3727 	movx	a,@dptr
      001C28 FC               [12] 3728 	mov	r4,a
      001C29 90 00 BA         [24] 3729 	mov	dptr,#_axradio_txbuffer_cnt
      001C2C EB               [12] 3730 	mov	a,r3
      001C2D C3               [12] 3731 	clr	c
      001C2E 9D               [12] 3732 	subb	a,r5
      001C2F F0               [24] 3733 	movx	@dptr,a
      001C30 EC               [12] 3734 	mov	a,r4
      001C31 9E               [12] 3735 	subb	a,r6
      001C32 A3               [24] 3736 	inc	dptr
      001C33 F0               [24] 3737 	movx	@dptr,a
                                   3738 ;	..\src\COMMON\easyax5043.c:526: AX5043_FIFODATA = AX5043_FIFOCMD_REPEATDATA | (3 << 5);
      001C34 90 40 29         [24] 3739 	mov	dptr,#_AX5043_FIFODATA
      001C37 74 62            [12] 3740 	mov	a,#0x62
      001C39 F0               [24] 3741 	movx	@dptr,a
                                   3742 ;	..\src\COMMON\easyax5043.c:527: AX5043_FIFODATA = axradio_phy_preamble_flags;
      001C3A 90 7A D7         [24] 3743 	mov	dptr,#_axradio_phy_preamble_flags
      001C3D E4               [12] 3744 	clr	a
      001C3E 93               [24] 3745 	movc	a,@a+dptr
      001C3F 90 40 29         [24] 3746 	mov	dptr,#_AX5043_FIFODATA
      001C42 F0               [24] 3747 	movx	@dptr,a
                                   3748 ;	..\src\COMMON\easyax5043.c:528: AX5043_FIFODATA = cnt;
      001C43 EF               [12] 3749 	mov	a,r7
      001C44 F0               [24] 3750 	movx	@dptr,a
                                   3751 ;	..\src\COMMON\easyax5043.c:529: AX5043_FIFODATA = axradio_phy_preamble_byte;
      001C45 90 7A D6         [24] 3752 	mov	dptr,#_axradio_phy_preamble_byte
      001C48 E4               [12] 3753 	clr	a
      001C49 93               [24] 3754 	movc	a,@a+dptr
      001C4A FE               [12] 3755 	mov	r6,a
      001C4B 90 40 29         [24] 3756 	mov	dptr,#_AX5043_FIFODATA
      001C4E F0               [24] 3757 	movx	@dptr,a
                                   3758 ;	..\src\COMMON\easyax5043.c:530: break;
      001C4F 02 1A 4F         [24] 3759 	ljmp	00157$
      001C52                       3760 00134$:
                                   3761 ;	..\src\COMMON\easyax5043.c:533: uint8_t byte = axradio_phy_preamble_byte;
      001C52 90 7A D6         [24] 3762 	mov	dptr,#_axradio_phy_preamble_byte
      001C55 E4               [12] 3763 	clr	a
      001C56 93               [24] 3764 	movc	a,@a+dptr
      001C57 FE               [12] 3765 	mov	r6,a
                                   3766 ;	..\src\COMMON\easyax5043.c:534: cnt = axradio_txbuffer_cnt;
      001C58 90 00 BA         [24] 3767 	mov	dptr,#_axradio_txbuffer_cnt
      001C5B E0               [24] 3768 	movx	a,@dptr
      001C5C FC               [12] 3769 	mov	r4,a
      001C5D A3               [24] 3770 	inc	dptr
      001C5E E0               [24] 3771 	movx	a,@dptr
      001C5F 8C 07            [24] 3772 	mov	ar7,r4
                                   3773 ;	..\src\COMMON\easyax5043.c:535: axradio_txbuffer_cnt = 0;
      001C61 90 00 BA         [24] 3774 	mov	dptr,#_axradio_txbuffer_cnt
      001C64 E4               [12] 3775 	clr	a
      001C65 F0               [24] 3776 	movx	@dptr,a
      001C66 A3               [24] 3777 	inc	dptr
      001C67 F0               [24] 3778 	movx	@dptr,a
                                   3779 ;	..\src\COMMON\easyax5043.c:536: AX5043_FIFODATA = AX5043_FIFOCMD_DATA | (2 << 5);
      001C68 90 40 29         [24] 3780 	mov	dptr,#_AX5043_FIFODATA
      001C6B 74 41            [12] 3781 	mov	a,#0x41
      001C6D F0               [24] 3782 	movx	@dptr,a
                                   3783 ;	..\src\COMMON\easyax5043.c:537: AX5043_FIFODATA = 0x1C;
      001C6E 74 1C            [12] 3784 	mov	a,#0x1c
      001C70 F0               [24] 3785 	movx	@dptr,a
                                   3786 ;	..\src\COMMON\easyax5043.c:538: if (AX5043_PKTADDRCFG & 0x80) {
      001C71 90 42 00         [24] 3787 	mov	dptr,#_AX5043_PKTADDRCFG
      001C74 E0               [24] 3788 	movx	a,@dptr
      001C75 FD               [12] 3789 	mov	r5,a
      001C76 30 E7 27         [24] 3790 	jnb	acc.7,00136$
                                   3791 ;	..\src\COMMON\easyax5043.c:540: byte &= 0xFF << (8-cnt);
      001C79 74 08            [12] 3792 	mov	a,#0x08
      001C7B C3               [12] 3793 	clr	c
      001C7C 9F               [12] 3794 	subb	a,r7
      001C7D FD               [12] 3795 	mov	r5,a
      001C7E 8D F0            [24] 3796 	mov	b,r5
      001C80 05 F0            [12] 3797 	inc	b
      001C82 74 FF            [12] 3798 	mov	a,#0xff
      001C84 80 02            [24] 3799 	sjmp	00283$
      001C86                       3800 00281$:
      001C86 25 E0            [12] 3801 	add	a,acc
      001C88                       3802 00283$:
      001C88 D5 F0 FB         [24] 3803 	djnz	b,00281$
      001C8B FD               [12] 3804 	mov	r5,a
      001C8C 52 06            [12] 3805 	anl	ar6,a
                                   3806 ;	..\src\COMMON\easyax5043.c:541: byte |= 0x80 >> cnt;
      001C8E 8F F0            [24] 3807 	mov	b,r7
      001C90 05 F0            [12] 3808 	inc	b
      001C92 74 80            [12] 3809 	mov	a,#0x80
      001C94 80 02            [24] 3810 	sjmp	00285$
      001C96                       3811 00284$:
      001C96 C3               [12] 3812 	clr	c
      001C97 13               [12] 3813 	rrc	a
      001C98                       3814 00285$:
      001C98 D5 F0 FB         [24] 3815 	djnz	b,00284$
      001C9B FD               [12] 3816 	mov	r5,a
      001C9C 42 06            [12] 3817 	orl	ar6,a
      001C9E 80 2C            [24] 3818 	sjmp	00137$
      001CA0                       3819 00136$:
                                   3820 ;	..\src\COMMON\easyax5043.c:544: byte &= 0xFF >> (8-cnt);
      001CA0 8F 04            [24] 3821 	mov	ar4,r7
      001CA2 7D 00            [12] 3822 	mov	r5,#0x00
      001CA4 74 08            [12] 3823 	mov	a,#0x08
      001CA6 C3               [12] 3824 	clr	c
      001CA7 9C               [12] 3825 	subb	a,r4
      001CA8 FC               [12] 3826 	mov	r4,a
      001CA9 E4               [12] 3827 	clr	a
      001CAA 9D               [12] 3828 	subb	a,r5
      001CAB FD               [12] 3829 	mov	r5,a
      001CAC 8C F0            [24] 3830 	mov	b,r4
      001CAE 05 F0            [12] 3831 	inc	b
      001CB0 74 FF            [12] 3832 	mov	a,#0xff
      001CB2 80 02            [24] 3833 	sjmp	00287$
      001CB4                       3834 00286$:
      001CB4 C3               [12] 3835 	clr	c
      001CB5 13               [12] 3836 	rrc	a
      001CB6                       3837 00287$:
      001CB6 D5 F0 FB         [24] 3838 	djnz	b,00286$
      001CB9 FC               [12] 3839 	mov	r4,a
      001CBA 52 06            [12] 3840 	anl	ar6,a
                                   3841 ;	..\src\COMMON\easyax5043.c:545: byte |= 0x01 << cnt;
      001CBC 8F F0            [24] 3842 	mov	b,r7
      001CBE 05 F0            [12] 3843 	inc	b
      001CC0 74 01            [12] 3844 	mov	a,#0x01
      001CC2 80 02            [24] 3845 	sjmp	00290$
      001CC4                       3846 00288$:
      001CC4 25 E0            [12] 3847 	add	a,acc
      001CC6                       3848 00290$:
      001CC6 D5 F0 FB         [24] 3849 	djnz	b,00288$
      001CC9 FD               [12] 3850 	mov	r5,a
      001CCA 42 06            [12] 3851 	orl	ar6,a
      001CCC                       3852 00137$:
                                   3853 ;	..\src\COMMON\easyax5043.c:547: AX5043_FIFODATA = byte;
      001CCC 90 40 29         [24] 3854 	mov	dptr,#_AX5043_FIFODATA
      001CCF EE               [12] 3855 	mov	a,r6
      001CD0 F0               [24] 3856 	movx	@dptr,a
                                   3857 ;	..\src\COMMON\easyax5043.c:549: break;
      001CD1 02 1A 4F         [24] 3858 	ljmp	00157$
                                   3859 ;	..\src\COMMON\easyax5043.c:551: case trxstate_tx_packet:
      001CD4                       3860 00138$:
                                   3861 ;	..\src\COMMON\easyax5043.c:552: if (cnt < 11)
      001CD4 BF 0B 00         [24] 3862 	cjne	r7,#0x0b,00291$
      001CD7                       3863 00291$:
      001CD7 50 03            [24] 3864 	jnc	00292$
      001CD9 02 1D 75         [24] 3865 	ljmp	00153$
      001CDC                       3866 00292$:
                                   3867 ;	..\src\COMMON\easyax5043.c:555: uint8_t flags = 0;
      001CDC 7E 00            [12] 3868 	mov	r6,#0x00
                                   3869 ;	..\src\COMMON\easyax5043.c:556: if (!axradio_txbuffer_cnt)
      001CDE 90 00 BA         [24] 3870 	mov	dptr,#_axradio_txbuffer_cnt
      001CE1 E0               [24] 3871 	movx	a,@dptr
      001CE2 F5 F0            [12] 3872 	mov	b,a
      001CE4 A3               [24] 3873 	inc	dptr
      001CE5 E0               [24] 3874 	movx	a,@dptr
      001CE6 45 F0            [12] 3875 	orl	a,b
      001CE8 70 02            [24] 3876 	jnz	00142$
                                   3877 ;	..\src\COMMON\easyax5043.c:557: flags |= 0x01; // flag byte: pkt_start
      001CEA 7E 01            [12] 3878 	mov	r6,#0x01
      001CEC                       3879 00142$:
                                   3880 ;	..\src\COMMON\easyax5043.c:559: uint16_t len = axradio_txbuffer_len - axradio_txbuffer_cnt;
      001CEC 90 00 BA         [24] 3881 	mov	dptr,#_axradio_txbuffer_cnt
      001CEF E0               [24] 3882 	movx	a,@dptr
      001CF0 FC               [12] 3883 	mov	r4,a
      001CF1 A3               [24] 3884 	inc	dptr
      001CF2 E0               [24] 3885 	movx	a,@dptr
      001CF3 FD               [12] 3886 	mov	r5,a
      001CF4 90 00 B8         [24] 3887 	mov	dptr,#_axradio_txbuffer_len
      001CF7 E0               [24] 3888 	movx	a,@dptr
      001CF8 FA               [12] 3889 	mov	r2,a
      001CF9 A3               [24] 3890 	inc	dptr
      001CFA E0               [24] 3891 	movx	a,@dptr
      001CFB FB               [12] 3892 	mov	r3,a
      001CFC EA               [12] 3893 	mov	a,r2
      001CFD C3               [12] 3894 	clr	c
      001CFE 9C               [12] 3895 	subb	a,r4
      001CFF FC               [12] 3896 	mov	r4,a
      001D00 EB               [12] 3897 	mov	a,r3
      001D01 9D               [12] 3898 	subb	a,r5
      001D02 FD               [12] 3899 	mov	r5,a
                                   3900 ;	..\src\COMMON\easyax5043.c:560: cnt -= 3;
      001D03 1F               [12] 3901 	dec	r7
      001D04 1F               [12] 3902 	dec	r7
      001D05 1F               [12] 3903 	dec	r7
                                   3904 ;	..\src\COMMON\easyax5043.c:561: if (cnt >= len) {
      001D06 8F 02            [24] 3905 	mov	ar2,r7
      001D08 7B 00            [12] 3906 	mov	r3,#0x00
      001D0A C3               [12] 3907 	clr	c
      001D0B EA               [12] 3908 	mov	a,r2
      001D0C 9C               [12] 3909 	subb	a,r4
      001D0D EB               [12] 3910 	mov	a,r3
      001D0E 9D               [12] 3911 	subb	a,r5
      001D0F 40 05            [24] 3912 	jc	00144$
                                   3913 ;	..\src\COMMON\easyax5043.c:562: cnt = len;
      001D11 8C 07            [24] 3914 	mov	ar7,r4
                                   3915 ;	..\src\COMMON\easyax5043.c:563: flags |= 0x02; // flag byte: pkt_end
      001D13 43 06 02         [24] 3916 	orl	ar6,#0x02
      001D16                       3917 00144$:
                                   3918 ;	..\src\COMMON\easyax5043.c:566: if (!cnt)
      001D16 EF               [12] 3919 	mov	a,r7
      001D17 60 4D            [24] 3920 	jz	00152$
                                   3921 ;	..\src\COMMON\easyax5043.c:568: AX5043_FIFODATA = AX5043_FIFOCMD_DATA | (7 << 5);
      001D19 90 40 29         [24] 3922 	mov	dptr,#_AX5043_FIFODATA
      001D1C 74 E1            [12] 3923 	mov	a,#0xe1
      001D1E F0               [24] 3924 	movx	@dptr,a
                                   3925 ;	..\src\COMMON\easyax5043.c:569: AX5043_FIFODATA = cnt + 1; // write FIFO chunk length byte (length includes the flag byte, thus the +1)
      001D1F EF               [12] 3926 	mov	a,r7
      001D20 04               [12] 3927 	inc	a
      001D21 F0               [24] 3928 	movx	@dptr,a
                                   3929 ;	..\src\COMMON\easyax5043.c:570: AX5043_FIFODATA = flags;
      001D22 EE               [12] 3930 	mov	a,r6
      001D23 F0               [24] 3931 	movx	@dptr,a
                                   3932 ;	..\src\COMMON\easyax5043.c:571: ax5043_writefifo(&axradio_txbuffer[axradio_txbuffer_cnt], cnt);
      001D24 90 00 BA         [24] 3933 	mov	dptr,#_axradio_txbuffer_cnt
      001D27 E0               [24] 3934 	movx	a,@dptr
      001D28 FC               [12] 3935 	mov	r4,a
      001D29 A3               [24] 3936 	inc	dptr
      001D2A E0               [24] 3937 	movx	a,@dptr
      001D2B FD               [12] 3938 	mov	r5,a
      001D2C EC               [12] 3939 	mov	a,r4
      001D2D 24 DD            [12] 3940 	add	a,#_axradio_txbuffer
      001D2F FC               [12] 3941 	mov	r4,a
      001D30 ED               [12] 3942 	mov	a,r5
      001D31 34 00            [12] 3943 	addc	a,#(_axradio_txbuffer >> 8)
      001D33 FD               [12] 3944 	mov	r5,a
      001D34 7B 00            [12] 3945 	mov	r3,#0x00
      001D36 C0 07            [24] 3946 	push	ar7
      001D38 C0 06            [24] 3947 	push	ar6
      001D3A C0 07            [24] 3948 	push	ar7
      001D3C 8C 82            [24] 3949 	mov	dpl,r4
      001D3E 8D 83            [24] 3950 	mov	dph,r5
      001D40 8B F0            [24] 3951 	mov	b,r3
      001D42 12 76 8F         [24] 3952 	lcall	_ax5043_writefifo
      001D45 15 81            [12] 3953 	dec	sp
      001D47 D0 06            [24] 3954 	pop	ar6
      001D49 D0 07            [24] 3955 	pop	ar7
                                   3956 ;	..\src\COMMON\easyax5043.c:572: axradio_txbuffer_cnt += cnt;
      001D4B 7D 00            [12] 3957 	mov	r5,#0x00
      001D4D 90 00 BA         [24] 3958 	mov	dptr,#_axradio_txbuffer_cnt
      001D50 E0               [24] 3959 	movx	a,@dptr
      001D51 FB               [12] 3960 	mov	r3,a
      001D52 A3               [24] 3961 	inc	dptr
      001D53 E0               [24] 3962 	movx	a,@dptr
      001D54 FC               [12] 3963 	mov	r4,a
      001D55 90 00 BA         [24] 3964 	mov	dptr,#_axradio_txbuffer_cnt
      001D58 EF               [12] 3965 	mov	a,r7
      001D59 2B               [12] 3966 	add	a,r3
      001D5A F0               [24] 3967 	movx	@dptr,a
      001D5B ED               [12] 3968 	mov	a,r5
      001D5C 3C               [12] 3969 	addc	a,r4
      001D5D A3               [24] 3970 	inc	dptr
      001D5E F0               [24] 3971 	movx	@dptr,a
                                   3972 ;	..\src\COMMON\easyax5043.c:573: if (flags & 0x02)
      001D5F EE               [12] 3973 	mov	a,r6
      001D60 20 E1 03         [24] 3974 	jb	acc.1,00152$
                                   3975 ;	..\src\COMMON\easyax5043.c:574: goto pktend;
                                   3976 ;	..\src\COMMON\easyax5043.c:578: default:
                                   3977 ;	..\src\COMMON\easyax5043.c:579: return;
                                   3978 ;	..\src\COMMON\easyax5043.c:582: pktend:
      001D63 02 1A 4F         [24] 3979 	ljmp	00157$
      001D66                       3980 00152$:
                                   3981 ;	..\src\COMMON\easyax5043.c:583: axradio_trxstate = trxstate_tx_waitdone;
      001D66 75 0C 0D         [24] 3982 	mov	_axradio_trxstate,#0x0d
                                   3983 ;	..\src\COMMON\easyax5043.c:584: AX5043_RADIOEVENTMASK0 = 0x01; // enable REVRDONE event
      001D69 90 40 09         [24] 3984 	mov	dptr,#_AX5043_RADIOEVENTMASK0
      001D6C 74 01            [12] 3985 	mov	a,#0x01
      001D6E F0               [24] 3986 	movx	@dptr,a
                                   3987 ;	..\src\COMMON\easyax5043.c:585: AX5043_IRQMASK0 = 0x40; // enable radio controller irq
      001D6F 90 40 07         [24] 3988 	mov	dptr,#_AX5043_IRQMASK0
      001D72 74 40            [12] 3989 	mov	a,#0x40
      001D74 F0               [24] 3990 	movx	@dptr,a
                                   3991 ;	..\src\COMMON\easyax5043.c:586: fifocommit:
      001D75                       3992 00153$:
                                   3993 ;	..\src\COMMON\easyax5043.c:587: AX5043_FIFOSTAT = 4; // commit
      001D75 90 40 28         [24] 3994 	mov	dptr,#_AX5043_FIFOSTAT
      001D78 74 04            [12] 3995 	mov	a,#0x04
      001D7A F0               [24] 3996 	movx	@dptr,a
      001D7B 22               [24] 3997 	ret
                                   3998 ;------------------------------------------------------------
                                   3999 ;Allocation info for local variables in function 'axradio_isr'
                                   4000 ;------------------------------------------------------------
                                   4001 ;evt                       Allocated to registers r7 
                                   4002 ;------------------------------------------------------------
                                   4003 ;	..\src\COMMON\easyax5043.c:591: void axradio_isr(void) __interrupt INT_RADIO
                                   4004 ;	-----------------------------------------
                                   4005 ;	 function axradio_isr
                                   4006 ;	-----------------------------------------
      001D7C                       4007 _axradio_isr:
      001D7C C0 21            [24] 4008 	push	bits
      001D7E C0 E0            [24] 4009 	push	acc
      001D80 C0 F0            [24] 4010 	push	b
      001D82 C0 82            [24] 4011 	push	dpl
      001D84 C0 83            [24] 4012 	push	dph
      001D86 C0 07            [24] 4013 	push	(0+7)
      001D88 C0 06            [24] 4014 	push	(0+6)
      001D8A C0 05            [24] 4015 	push	(0+5)
      001D8C C0 04            [24] 4016 	push	(0+4)
      001D8E C0 03            [24] 4017 	push	(0+3)
      001D90 C0 02            [24] 4018 	push	(0+2)
      001D92 C0 01            [24] 4019 	push	(0+1)
      001D94 C0 00            [24] 4020 	push	(0+0)
      001D96 C0 D0            [24] 4021 	push	psw
      001D98 75 D0 00         [24] 4022 	mov	psw,#0x00
                                   4023 ;	..\src\COMMON\easyax5043.c:601: switch (axradio_trxstate) {
      001D9B E5 0C            [12] 4024 	mov	a,_axradio_trxstate
      001D9D FF               [12] 4025 	mov	r7,a
      001D9E 24 EF            [12] 4026 	add	a,#0xff - 0x10
      001DA0 50 03            [24] 4027 	jnc	00256$
      001DA2 02 1D D8         [24] 4028 	ljmp	00101$
      001DA5                       4029 00256$:
      001DA5 EF               [12] 4030 	mov	a,r7
      001DA6 F5 F0            [12] 4031 	mov	b,a
      001DA8 24 0B            [12] 4032 	add	a,#(00257$-3-.)
      001DAA 83               [24] 4033 	movc	a,@a+pc
      001DAB F5 82            [12] 4034 	mov	dpl,a
      001DAD E5 F0            [12] 4035 	mov	a,b
      001DAF 24 15            [12] 4036 	add	a,#(00258$-3-.)
      001DB1 83               [24] 4037 	movc	a,@a+pc
      001DB2 F5 83            [12] 4038 	mov	dph,a
      001DB4 E4               [12] 4039 	clr	a
      001DB5 73               [24] 4040 	jmp	@a+dptr
      001DB6                       4041 00257$:
      001DB6 D8                    4042 	.db	00101$
      001DB7 57                    4043 	.db	00165$
      001DB8 FE                    4044 	.db	00158$
      001DB9 E4                    4045 	.db	00102$
      001DBA D8                    4046 	.db	00101$
      001DBB EF                    4047 	.db	00103$
      001DBC D8                    4048 	.db	00101$
      001DBD FA                    4049 	.db	00104$
      001DBE D8                    4050 	.db	00101$
      001DBF 05                    4051 	.db	00105$
      001DC0 95                    4052 	.db	00114$
      001DC1 95                    4053 	.db	00115$
      001DC2 95                    4054 	.db	00116$
      001DC3 9B                    4055 	.db	00117$
      001DC4 D0                    4056 	.db	00144$
      001DC5 15                    4057 	.db	00145$
      001DC6 3C                    4058 	.db	00148$
      001DC7                       4059 00258$:
      001DC7 1D                    4060 	.db	00101$>>8
      001DC8 21                    4061 	.db	00165$>>8
      001DC9 20                    4062 	.db	00158$>>8
      001DCA 1D                    4063 	.db	00102$>>8
      001DCB 1D                    4064 	.db	00101$>>8
      001DCC 1D                    4065 	.db	00103$>>8
      001DCD 1D                    4066 	.db	00101$>>8
      001DCE 1D                    4067 	.db	00104$>>8
      001DCF 1D                    4068 	.db	00101$>>8
      001DD0 1E                    4069 	.db	00105$>>8
      001DD1 1E                    4070 	.db	00114$>>8
      001DD2 1E                    4071 	.db	00115$>>8
      001DD3 1E                    4072 	.db	00116$>>8
      001DD4 1E                    4073 	.db	00117$>>8
      001DD5 1F                    4074 	.db	00144$>>8
      001DD6 20                    4075 	.db	00145$>>8
      001DD7 20                    4076 	.db	00148$>>8
                                   4077 ;	..\src\COMMON\easyax5043.c:602: default:
      001DD8                       4078 00101$:
                                   4079 ;	..\src\COMMON\easyax5043.c:603: AX5043_IRQMASK1 = 0x00;
      001DD8 90 40 06         [24] 4080 	mov	dptr,#_AX5043_IRQMASK1
      001DDB E4               [12] 4081 	clr	a
      001DDC F0               [24] 4082 	movx	@dptr,a
                                   4083 ;	..\src\COMMON\easyax5043.c:604: AX5043_IRQMASK0 = 0x00;
      001DDD 90 40 07         [24] 4084 	mov	dptr,#_AX5043_IRQMASK0
      001DE0 F0               [24] 4085 	movx	@dptr,a
                                   4086 ;	..\src\COMMON\easyax5043.c:605: break;
      001DE1 02 21 5A         [24] 4087 	ljmp	00167$
                                   4088 ;	..\src\COMMON\easyax5043.c:607: case trxstate_wait_xtal:
      001DE4                       4089 00102$:
                                   4090 ;	..\src\COMMON\easyax5043.c:608: AX5043_IRQMASK1 = 0x00; // otherwise crystal ready will fire all over again
      001DE4 90 40 06         [24] 4091 	mov	dptr,#_AX5043_IRQMASK1
      001DE7 E4               [12] 4092 	clr	a
      001DE8 F0               [24] 4093 	movx	@dptr,a
                                   4094 ;	..\src\COMMON\easyax5043.c:609: axradio_trxstate = trxstate_xtal_ready;
      001DE9 75 0C 04         [24] 4095 	mov	_axradio_trxstate,#0x04
                                   4096 ;	..\src\COMMON\easyax5043.c:610: break;
      001DEC 02 21 5A         [24] 4097 	ljmp	00167$
                                   4098 ;	..\src\COMMON\easyax5043.c:612: case trxstate_pll_ranging:
      001DEF                       4099 00103$:
                                   4100 ;	..\src\COMMON\easyax5043.c:613: AX5043_IRQMASK1 = 0x00; // otherwise autoranging done will fire all over again
      001DEF 90 40 06         [24] 4101 	mov	dptr,#_AX5043_IRQMASK1
      001DF2 E4               [12] 4102 	clr	a
      001DF3 F0               [24] 4103 	movx	@dptr,a
                                   4104 ;	..\src\COMMON\easyax5043.c:614: axradio_trxstate = trxstate_pll_ranging_done;
      001DF4 75 0C 06         [24] 4105 	mov	_axradio_trxstate,#0x06
                                   4106 ;	..\src\COMMON\easyax5043.c:615: break;
      001DF7 02 21 5A         [24] 4107 	ljmp	00167$
                                   4108 ;	..\src\COMMON\easyax5043.c:617: case trxstate_pll_settling:
      001DFA                       4109 00104$:
                                   4110 ;	..\src\COMMON\easyax5043.c:618: AX5043_RADIOEVENTMASK0 = 0x00;
      001DFA 90 40 09         [24] 4111 	mov	dptr,#_AX5043_RADIOEVENTMASK0
      001DFD E4               [12] 4112 	clr	a
      001DFE F0               [24] 4113 	movx	@dptr,a
                                   4114 ;	..\src\COMMON\easyax5043.c:619: axradio_trxstate = trxstate_pll_settled;
      001DFF 75 0C 08         [24] 4115 	mov	_axradio_trxstate,#0x08
                                   4116 ;	..\src\COMMON\easyax5043.c:620: break;
      001E02 02 21 5A         [24] 4117 	ljmp	00167$
                                   4118 ;	..\src\COMMON\easyax5043.c:622: case trxstate_tx_xtalwait:
      001E05                       4119 00105$:
                                   4120 ;	..\src\COMMON\easyax5043.c:623: AX5043_RADIOEVENTREQ0; // make sure REVRDONE bit is cleared, so it is a reliable indicator that the packet is out
      001E05 90 40 0F         [24] 4121 	mov	dptr,#_AX5043_RADIOEVENTREQ0
      001E08 E0               [24] 4122 	movx	a,@dptr
                                   4123 ;	..\src\COMMON\easyax5043.c:624: AX5043_FIFOSTAT = 3; // clear FIFO data & flags (prevent transmitting anything left over in the FIFO, this has no effect if the FIFO is not powerered, in this case it is reset any way)
      001E09 90 40 28         [24] 4124 	mov	dptr,#_AX5043_FIFOSTAT
      001E0C 74 03            [12] 4125 	mov	a,#0x03
      001E0E F0               [24] 4126 	movx	@dptr,a
                                   4127 ;	..\src\COMMON\easyax5043.c:625: AX5043_IRQMASK1 = 0x00;
      001E0F 90 40 06         [24] 4128 	mov	dptr,#_AX5043_IRQMASK1
      001E12 E4               [12] 4129 	clr	a
      001E13 F0               [24] 4130 	movx	@dptr,a
                                   4131 ;	..\src\COMMON\easyax5043.c:626: AX5043_IRQMASK0 = 0x08; // enable fifo free threshold
      001E14 90 40 07         [24] 4132 	mov	dptr,#_AX5043_IRQMASK0
      001E17 74 08            [12] 4133 	mov	a,#0x08
      001E19 F0               [24] 4134 	movx	@dptr,a
                                   4135 ;	..\src\COMMON\easyax5043.c:627: axradio_trxstate = trxstate_tx_longpreamble;
      001E1A 75 0C 0A         [24] 4136 	mov	_axradio_trxstate,#0x0a
                                   4137 ;	..\src\COMMON\easyax5043.c:629: if ((AX5043_MODULATION & 0x0F) == 9) { // 4-FSK
      001E1D 90 40 10         [24] 4138 	mov	dptr,#_AX5043_MODULATION
      001E20 E0               [24] 4139 	movx	a,@dptr
      001E21 FF               [12] 4140 	mov	r7,a
      001E22 53 07 0F         [24] 4141 	anl	ar7,#0x0f
      001E25 BF 09 0E         [24] 4142 	cjne	r7,#0x09,00107$
                                   4143 ;	..\src\COMMON\easyax5043.c:630: AX5043_FIFODATA = AX5043_FIFOCMD_DATA | (7 << 5);
      001E28 90 40 29         [24] 4144 	mov	dptr,#_AX5043_FIFODATA
      001E2B 74 E1            [12] 4145 	mov	a,#0xe1
      001E2D F0               [24] 4146 	movx	@dptr,a
                                   4147 ;	..\src\COMMON\easyax5043.c:631: AX5043_FIFODATA = 2;  // length (including flags)
      001E2E 74 02            [12] 4148 	mov	a,#0x02
      001E30 F0               [24] 4149 	movx	@dptr,a
                                   4150 ;	..\src\COMMON\easyax5043.c:632: AX5043_FIFODATA = 0x01;  // flag PKTSTART -> dibit sync
      001E31 14               [12] 4151 	dec	a
      001E32 F0               [24] 4152 	movx	@dptr,a
                                   4153 ;	..\src\COMMON\easyax5043.c:633: AX5043_FIFODATA = 0x11; // dummy byte for forcing dibit sync
      001E33 74 11            [12] 4154 	mov	a,#0x11
      001E35 F0               [24] 4155 	movx	@dptr,a
      001E36                       4156 00107$:
                                   4157 ;	..\src\COMMON\easyax5043.c:640: transmit_isr();
      001E36 12 1A 4F         [24] 4158 	lcall	_transmit_isr
                                   4159 ;	..\src\COMMON\easyax5043.c:641: AX5043_PWRMODE = AX5043_PWRSTATE_FULL_TX;
      001E39 90 40 02         [24] 4160 	mov	dptr,#_AX5043_PWRMODE
      001E3C 74 0D            [12] 4161 	mov	a,#0x0d
      001E3E F0               [24] 4162 	movx	@dptr,a
                                   4163 ;	..\src\COMMON\easyax5043.c:642: update_timeanchor();
      001E3F 12 15 BA         [24] 4164 	lcall	_update_timeanchor
                                   4165 ;	..\src\COMMON\easyax5043.c:643: wtimer_remove_callback(&axradio_cb_transmitstart.cb);
      001E42 90 03 1E         [24] 4166 	mov	dptr,#_axradio_cb_transmitstart
      001E45 12 74 C0         [24] 4167 	lcall	_wtimer_remove_callback
                                   4168 ;	..\src\COMMON\easyax5043.c:644: switch (axradio_mode) {
      001E48 AF 0B            [24] 4169 	mov	r7,_axradio_mode
      001E4A BF 12 02         [24] 4170 	cjne	r7,#0x12,00261$
      001E4D 80 03            [24] 4171 	sjmp	00109$
      001E4F                       4172 00261$:
      001E4F BF 13 19         [24] 4173 	cjne	r7,#0x13,00112$
                                   4174 ;	..\src\COMMON\easyax5043.c:646: case AXRADIO_MODE_WOR_ACK_TRANSMIT:
      001E52                       4175 00109$:
                                   4176 ;	..\src\COMMON\easyax5043.c:647: if (axradio_ack_count != axradio_framing_ack_retransmissions) {
      001E52 90 00 C1         [24] 4177 	mov	dptr,#_axradio_ack_count
      001E55 E0               [24] 4178 	movx	a,@dptr
      001E56 FF               [12] 4179 	mov	r7,a
      001E57 90 7A F1         [24] 4180 	mov	dptr,#_axradio_framing_ack_retransmissions
      001E5A E4               [12] 4181 	clr	a
      001E5B 93               [24] 4182 	movc	a,@a+dptr
      001E5C FE               [12] 4183 	mov	r6,a
      001E5D EF               [12] 4184 	mov	a,r7
      001E5E B5 06 02         [24] 4185 	cjne	a,ar6,00264$
      001E61 80 08            [24] 4186 	sjmp	00112$
      001E63                       4187 00264$:
                                   4188 ;	..\src\COMMON\easyax5043.c:648: axradio_cb_transmitstart.st.error = AXRADIO_ERR_RETRANSMISSION;
      001E63 90 03 23         [24] 4189 	mov	dptr,#(_axradio_cb_transmitstart + 0x0005)
      001E66 74 08            [12] 4190 	mov	a,#0x08
      001E68 F0               [24] 4191 	movx	@dptr,a
                                   4192 ;	..\src\COMMON\easyax5043.c:649: break;
                                   4193 ;	..\src\COMMON\easyax5043.c:652: default:
      001E69 80 05            [24] 4194 	sjmp	00113$
      001E6B                       4195 00112$:
                                   4196 ;	..\src\COMMON\easyax5043.c:653: axradio_cb_transmitstart.st.error = AXRADIO_ERR_NOERROR;
      001E6B 90 03 23         [24] 4197 	mov	dptr,#(_axradio_cb_transmitstart + 0x0005)
      001E6E E4               [12] 4198 	clr	a
      001E6F F0               [24] 4199 	movx	@dptr,a
                                   4200 ;	..\src\COMMON\easyax5043.c:655: }
      001E70                       4201 00113$:
                                   4202 ;	..\src\COMMON\easyax5043.c:656: axradio_cb_transmitstart.st.time.t = axradio_timeanchor.radiotimer;
      001E70 90 00 CD         [24] 4203 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      001E73 E0               [24] 4204 	movx	a,@dptr
      001E74 FC               [12] 4205 	mov	r4,a
      001E75 A3               [24] 4206 	inc	dptr
      001E76 E0               [24] 4207 	movx	a,@dptr
      001E77 FD               [12] 4208 	mov	r5,a
      001E78 A3               [24] 4209 	inc	dptr
      001E79 E0               [24] 4210 	movx	a,@dptr
      001E7A FE               [12] 4211 	mov	r6,a
      001E7B A3               [24] 4212 	inc	dptr
      001E7C E0               [24] 4213 	movx	a,@dptr
      001E7D FF               [12] 4214 	mov	r7,a
      001E7E 90 03 24         [24] 4215 	mov	dptr,#(_axradio_cb_transmitstart + 0x0006)
      001E81 EC               [12] 4216 	mov	a,r4
      001E82 F0               [24] 4217 	movx	@dptr,a
      001E83 ED               [12] 4218 	mov	a,r5
      001E84 A3               [24] 4219 	inc	dptr
      001E85 F0               [24] 4220 	movx	@dptr,a
      001E86 EE               [12] 4221 	mov	a,r6
      001E87 A3               [24] 4222 	inc	dptr
      001E88 F0               [24] 4223 	movx	@dptr,a
      001E89 EF               [12] 4224 	mov	a,r7
      001E8A A3               [24] 4225 	inc	dptr
      001E8B F0               [24] 4226 	movx	@dptr,a
                                   4227 ;	..\src\COMMON\easyax5043.c:657: wtimer_add_callback(&axradio_cb_transmitstart.cb);
      001E8C 90 03 1E         [24] 4228 	mov	dptr,#_axradio_cb_transmitstart
      001E8F 12 66 0B         [24] 4229 	lcall	_wtimer_add_callback
                                   4230 ;	..\src\COMMON\easyax5043.c:658: break;
      001E92 02 21 5A         [24] 4231 	ljmp	00167$
                                   4232 ;	..\src\COMMON\easyax5043.c:660: case trxstate_tx_longpreamble:
      001E95                       4233 00114$:
                                   4234 ;	..\src\COMMON\easyax5043.c:661: case trxstate_tx_shortpreamble:
      001E95                       4235 00115$:
                                   4236 ;	..\src\COMMON\easyax5043.c:662: case trxstate_tx_packet:
      001E95                       4237 00116$:
                                   4238 ;	..\src\COMMON\easyax5043.c:663: transmit_isr();
      001E95 12 1A 4F         [24] 4239 	lcall	_transmit_isr
                                   4240 ;	..\src\COMMON\easyax5043.c:664: break;
      001E98 02 21 5A         [24] 4241 	ljmp	00167$
                                   4242 ;	..\src\COMMON\easyax5043.c:666: case trxstate_tx_waitdone:
      001E9B                       4243 00117$:
                                   4244 ;	..\src\COMMON\easyax5043.c:667: AX5043_RADIOEVENTREQ0;
      001E9B 90 40 0F         [24] 4245 	mov	dptr,#_AX5043_RADIOEVENTREQ0
      001E9E E0               [24] 4246 	movx	a,@dptr
                                   4247 ;	..\src\COMMON\easyax5043.c:668: if (AX5043_RADIOSTATE != 0)
      001E9F 90 40 1C         [24] 4248 	mov	dptr,#_AX5043_RADIOSTATE
      001EA2 E0               [24] 4249 	movx	a,@dptr
      001EA3 E0               [24] 4250 	movx	a,@dptr
      001EA4 60 03            [24] 4251 	jz	00265$
      001EA6 02 21 5A         [24] 4252 	ljmp	00167$
      001EA9                       4253 00265$:
                                   4254 ;	..\src\COMMON\easyax5043.c:670: AX5043_RADIOEVENTMASK0 = 0x00;
      001EA9 90 40 09         [24] 4255 	mov	dptr,#_AX5043_RADIOEVENTMASK0
      001EAC E4               [12] 4256 	clr	a
      001EAD F0               [24] 4257 	movx	@dptr,a
                                   4258 ;	..\src\COMMON\easyax5043.c:671: switch (axradio_mode) {
      001EAE AF 0B            [24] 4259 	mov	r7,_axradio_mode
      001EB0 BF 12 02         [24] 4260 	cjne	r7,#0x12,00266$
      001EB3 80 6A            [24] 4261 	sjmp	00131$
      001EB5                       4262 00266$:
      001EB5 BF 13 02         [24] 4263 	cjne	r7,#0x13,00267$
      001EB8 80 65            [24] 4264 	sjmp	00131$
      001EBA                       4265 00267$:
      001EBA BF 20 02         [24] 4266 	cjne	r7,#0x20,00268$
      001EBD 80 1D            [24] 4267 	sjmp	00120$
      001EBF                       4268 00268$:
      001EBF BF 21 02         [24] 4269 	cjne	r7,#0x21,00269$
      001EC2 80 36            [24] 4270 	sjmp	00125$
      001EC4                       4271 00269$:
      001EC4 BF 22 02         [24] 4272 	cjne	r7,#0x22,00270$
      001EC7 80 1C            [24] 4273 	sjmp	00121$
      001EC9                       4274 00270$:
      001EC9 BF 23 02         [24] 4275 	cjne	r7,#0x23,00271$
      001ECC 80 3C            [24] 4276 	sjmp	00128$
      001ECE                       4277 00271$:
      001ECE BF 30 03         [24] 4278 	cjne	r7,#0x30,00272$
      001ED1 02 1F 53         [24] 4279 	ljmp	00132$
      001ED4                       4280 00272$:
      001ED4 BF 31 02         [24] 4281 	cjne	r7,#0x31,00273$
      001ED7 80 39            [24] 4282 	sjmp	00129$
      001ED9                       4283 00273$:
      001ED9 02 1F 60         [24] 4284 	ljmp	00133$
                                   4285 ;	..\src\COMMON\easyax5043.c:672: case AXRADIO_MODE_ASYNC_RECEIVE:
      001EDC                       4286 00120$:
                                   4287 ;	..\src\COMMON\easyax5043.c:673: ax5043_init_registers_rx();
      001EDC 12 16 9D         [24] 4288 	lcall	_ax5043_init_registers_rx
                                   4289 ;	..\src\COMMON\easyax5043.c:674: ax5043_receiver_on_continuous();
      001EDF 12 21 77         [24] 4290 	lcall	_ax5043_receiver_on_continuous
                                   4291 ;	..\src\COMMON\easyax5043.c:675: break;
      001EE2 02 1F 63         [24] 4292 	ljmp	00134$
                                   4293 ;	..\src\COMMON\easyax5043.c:677: case AXRADIO_MODE_ACK_RECEIVE:
      001EE5                       4294 00121$:
                                   4295 ;	..\src\COMMON\easyax5043.c:678: if (axradio_cb_receive.st.error == AXRADIO_ERR_PACKETDONE) {
      001EE5 90 02 EA         [24] 4296 	mov	dptr,#(_axradio_cb_receive + 0x0005)
      001EE8 E0               [24] 4297 	movx	a,@dptr
      001EE9 FF               [12] 4298 	mov	r7,a
      001EEA BF F0 08         [24] 4299 	cjne	r7,#0xf0,00124$
                                   4300 ;	..\src\COMMON\easyax5043.c:679: ax5043_init_registers_rx();
      001EED 12 16 9D         [24] 4301 	lcall	_ax5043_init_registers_rx
                                   4302 ;	..\src\COMMON\easyax5043.c:680: ax5043_receiver_on_continuous();
      001EF0 12 21 77         [24] 4303 	lcall	_ax5043_receiver_on_continuous
                                   4304 ;	..\src\COMMON\easyax5043.c:681: break;
                                   4305 ;	..\src\COMMON\easyax5043.c:683: offxtal:
      001EF3 80 6E            [24] 4306 	sjmp	00134$
      001EF5                       4307 00124$:
                                   4308 ;	..\src\COMMON\easyax5043.c:684: ax5043_off_xtal();
      001EF5 12 22 D7         [24] 4309 	lcall	_ax5043_off_xtal
                                   4310 ;	..\src\COMMON\easyax5043.c:685: break;
                                   4311 ;	..\src\COMMON\easyax5043.c:687: case AXRADIO_MODE_WOR_RECEIVE:
      001EF8 80 69            [24] 4312 	sjmp	00134$
      001EFA                       4313 00125$:
                                   4314 ;	..\src\COMMON\easyax5043.c:688: if (axradio_cb_receive.st.error == AXRADIO_ERR_PACKETDONE) {
      001EFA 90 02 EA         [24] 4315 	mov	dptr,#(_axradio_cb_receive + 0x0005)
      001EFD E0               [24] 4316 	movx	a,@dptr
      001EFE FF               [12] 4317 	mov	r7,a
      001EFF BF F0 F3         [24] 4318 	cjne	r7,#0xf0,00124$
                                   4319 ;	..\src\COMMON\easyax5043.c:689: ax5043_init_registers_rx();
      001F02 12 16 9D         [24] 4320 	lcall	_ax5043_init_registers_rx
                                   4321 ;	..\src\COMMON\easyax5043.c:690: ax5043_receiver_on_wor();
      001F05 12 21 DF         [24] 4322 	lcall	_ax5043_receiver_on_wor
                                   4323 ;	..\src\COMMON\easyax5043.c:691: break;
                                   4324 ;	..\src\COMMON\easyax5043.c:695: case AXRADIO_MODE_WOR_ACK_RECEIVE:
      001F08 80 59            [24] 4325 	sjmp	00134$
      001F0A                       4326 00128$:
                                   4327 ;	..\src\COMMON\easyax5043.c:696: ax5043_init_registers_rx();
      001F0A 12 16 9D         [24] 4328 	lcall	_ax5043_init_registers_rx
                                   4329 ;	..\src\COMMON\easyax5043.c:697: ax5043_receiver_on_wor();
      001F0D 12 21 DF         [24] 4330 	lcall	_ax5043_receiver_on_wor
                                   4331 ;	..\src\COMMON\easyax5043.c:698: break;
                                   4332 ;	..\src\COMMON\easyax5043.c:700: case AXRADIO_MODE_SYNC_ACK_MASTER:
      001F10 80 51            [24] 4333 	sjmp	00134$
      001F12                       4334 00129$:
                                   4335 ;	..\src\COMMON\easyax5043.c:701: axradio_txbuffer_len = axradio_framing_minpayloadlen;
      001F12 90 7A F3         [24] 4336 	mov	dptr,#_axradio_framing_minpayloadlen
      001F15 E4               [12] 4337 	clr	a
      001F16 93               [24] 4338 	movc	a,@a+dptr
      001F17 FF               [12] 4339 	mov	r7,a
      001F18 90 00 B8         [24] 4340 	mov	dptr,#_axradio_txbuffer_len
      001F1B F0               [24] 4341 	movx	@dptr,a
      001F1C E4               [12] 4342 	clr	a
      001F1D A3               [24] 4343 	inc	dptr
      001F1E F0               [24] 4344 	movx	@dptr,a
                                   4345 ;	..\src\COMMON\easyax5043.c:705: case AXRADIO_MODE_WOR_ACK_TRANSMIT:
      001F1F                       4346 00131$:
                                   4347 ;	..\src\COMMON\easyax5043.c:706: ax5043_init_registers_rx();
      001F1F 12 16 9D         [24] 4348 	lcall	_ax5043_init_registers_rx
                                   4349 ;	..\src\COMMON\easyax5043.c:707: ax5043_receiver_on_continuous();
      001F22 12 21 77         [24] 4350 	lcall	_ax5043_receiver_on_continuous
                                   4351 ;	..\src\COMMON\easyax5043.c:708: wtimer_remove(&axradio_timer);
      001F25 90 03 3C         [24] 4352 	mov	dptr,#_axradio_timer
      001F28 12 71 84         [24] 4353 	lcall	_wtimer_remove
                                   4354 ;	..\src\COMMON\easyax5043.c:709: axradio_timer.time = axradio_framing_ack_timeout;
      001F2B 90 7A E9         [24] 4355 	mov	dptr,#_axradio_framing_ack_timeout
      001F2E E4               [12] 4356 	clr	a
      001F2F 93               [24] 4357 	movc	a,@a+dptr
      001F30 FC               [12] 4358 	mov	r4,a
      001F31 74 01            [12] 4359 	mov	a,#0x01
      001F33 93               [24] 4360 	movc	a,@a+dptr
      001F34 FD               [12] 4361 	mov	r5,a
      001F35 74 02            [12] 4362 	mov	a,#0x02
      001F37 93               [24] 4363 	movc	a,@a+dptr
      001F38 FE               [12] 4364 	mov	r6,a
      001F39 74 03            [12] 4365 	mov	a,#0x03
      001F3B 93               [24] 4366 	movc	a,@a+dptr
      001F3C FF               [12] 4367 	mov	r7,a
      001F3D 90 03 40         [24] 4368 	mov	dptr,#(_axradio_timer + 0x0004)
      001F40 EC               [12] 4369 	mov	a,r4
      001F41 F0               [24] 4370 	movx	@dptr,a
      001F42 ED               [12] 4371 	mov	a,r5
      001F43 A3               [24] 4372 	inc	dptr
      001F44 F0               [24] 4373 	movx	@dptr,a
      001F45 EE               [12] 4374 	mov	a,r6
      001F46 A3               [24] 4375 	inc	dptr
      001F47 F0               [24] 4376 	movx	@dptr,a
      001F48 EF               [12] 4377 	mov	a,r7
      001F49 A3               [24] 4378 	inc	dptr
      001F4A F0               [24] 4379 	movx	@dptr,a
                                   4380 ;	..\src\COMMON\easyax5043.c:710: wtimer0_addrelative(&axradio_timer);
      001F4B 90 03 3C         [24] 4381 	mov	dptr,#_axradio_timer
      001F4E 12 66 25         [24] 4382 	lcall	_wtimer0_addrelative
                                   4383 ;	..\src\COMMON\easyax5043.c:711: break;
                                   4384 ;	..\src\COMMON\easyax5043.c:713: case AXRADIO_MODE_SYNC_MASTER:
      001F51 80 10            [24] 4385 	sjmp	00134$
      001F53                       4386 00132$:
                                   4387 ;	..\src\COMMON\easyax5043.c:714: axradio_txbuffer_len = axradio_framing_minpayloadlen;
      001F53 90 7A F3         [24] 4388 	mov	dptr,#_axradio_framing_minpayloadlen
      001F56 E4               [12] 4389 	clr	a
      001F57 93               [24] 4390 	movc	a,@a+dptr
      001F58 FF               [12] 4391 	mov	r7,a
      001F59 90 00 B8         [24] 4392 	mov	dptr,#_axradio_txbuffer_len
      001F5C F0               [24] 4393 	movx	@dptr,a
      001F5D E4               [12] 4394 	clr	a
      001F5E A3               [24] 4395 	inc	dptr
      001F5F F0               [24] 4396 	movx	@dptr,a
                                   4397 ;	..\src\COMMON\easyax5043.c:717: default:
      001F60                       4398 00133$:
                                   4399 ;	..\src\COMMON\easyax5043.c:718: ax5043_off();
      001F60 12 22 CE         [24] 4400 	lcall	_ax5043_off
                                   4401 ;	..\src\COMMON\easyax5043.c:720: }
      001F63                       4402 00134$:
                                   4403 ;	..\src\COMMON\easyax5043.c:721: if (axradio_mode != AXRADIO_MODE_SYNC_MASTER &&
      001F63 74 30            [12] 4404 	mov	a,#0x30
      001F65 B5 0B 02         [24] 4405 	cjne	a,_axradio_mode,00278$
      001F68 80 1A            [24] 4406 	sjmp	00136$
      001F6A                       4407 00278$:
                                   4408 ;	..\src\COMMON\easyax5043.c:722: axradio_mode != AXRADIO_MODE_SYNC_ACK_MASTER &&
      001F6A 74 31            [12] 4409 	mov	a,#0x31
      001F6C B5 0B 02         [24] 4410 	cjne	a,_axradio_mode,00279$
      001F6F 80 13            [24] 4411 	sjmp	00136$
      001F71                       4412 00279$:
                                   4413 ;	..\src\COMMON\easyax5043.c:723: axradio_mode != AXRADIO_MODE_SYNC_SLAVE &&
      001F71 74 32            [12] 4414 	mov	a,#0x32
      001F73 B5 0B 02         [24] 4415 	cjne	a,_axradio_mode,00280$
      001F76 80 0C            [24] 4416 	sjmp	00136$
      001F78                       4417 00280$:
                                   4418 ;	..\src\COMMON\easyax5043.c:724: axradio_mode != AXRADIO_MODE_SYNC_ACK_SLAVE)
      001F78 74 33            [12] 4419 	mov	a,#0x33
      001F7A B5 0B 02         [24] 4420 	cjne	a,_axradio_mode,00281$
      001F7D 80 05            [24] 4421 	sjmp	00136$
      001F7F                       4422 00281$:
                                   4423 ;	..\src\COMMON\easyax5043.c:725: axradio_syncstate = syncstate_off;
      001F7F 90 00 B7         [24] 4424 	mov	dptr,#_axradio_syncstate
      001F82 E4               [12] 4425 	clr	a
      001F83 F0               [24] 4426 	movx	@dptr,a
      001F84                       4427 00136$:
                                   4428 ;	..\src\COMMON\easyax5043.c:726: update_timeanchor();
      001F84 12 15 BA         [24] 4429 	lcall	_update_timeanchor
                                   4430 ;	..\src\COMMON\easyax5043.c:727: wtimer_remove_callback(&axradio_cb_transmitend.cb);
      001F87 90 03 28         [24] 4431 	mov	dptr,#_axradio_cb_transmitend
      001F8A 12 74 C0         [24] 4432 	lcall	_wtimer_remove_callback
                                   4433 ;	..\src\COMMON\easyax5043.c:728: axradio_cb_transmitend.st.error = AXRADIO_ERR_NOERROR;
      001F8D 90 03 2D         [24] 4434 	mov	dptr,#(_axradio_cb_transmitend + 0x0005)
      001F90 E4               [12] 4435 	clr	a
      001F91 F0               [24] 4436 	movx	@dptr,a
                                   4437 ;	..\src\COMMON\easyax5043.c:729: if (axradio_mode == AXRADIO_MODE_ACK_TRANSMIT ||
      001F92 74 12            [12] 4438 	mov	a,#0x12
      001F94 B5 0B 02         [24] 4439 	cjne	a,_axradio_mode,00282$
      001F97 80 0C            [24] 4440 	sjmp	00140$
      001F99                       4441 00282$:
                                   4442 ;	..\src\COMMON\easyax5043.c:730: axradio_mode == AXRADIO_MODE_WOR_ACK_TRANSMIT ||
      001F99 74 13            [12] 4443 	mov	a,#0x13
      001F9B B5 0B 02         [24] 4444 	cjne	a,_axradio_mode,00283$
      001F9E 80 05            [24] 4445 	sjmp	00140$
      001FA0                       4446 00283$:
                                   4447 ;	..\src\COMMON\easyax5043.c:731: axradio_mode == AXRADIO_MODE_SYNC_ACK_MASTER)
      001FA0 74 31            [12] 4448 	mov	a,#0x31
      001FA2 B5 0B 06         [24] 4449 	cjne	a,_axradio_mode,00141$
      001FA5                       4450 00140$:
                                   4451 ;	..\src\COMMON\easyax5043.c:732: axradio_cb_transmitend.st.error = AXRADIO_ERR_BUSY;
      001FA5 90 03 2D         [24] 4452 	mov	dptr,#(_axradio_cb_transmitend + 0x0005)
      001FA8 74 02            [12] 4453 	mov	a,#0x02
      001FAA F0               [24] 4454 	movx	@dptr,a
      001FAB                       4455 00141$:
                                   4456 ;	..\src\COMMON\easyax5043.c:733: axradio_cb_transmitend.st.time.t = axradio_timeanchor.radiotimer;
      001FAB 90 00 CD         [24] 4457 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      001FAE E0               [24] 4458 	movx	a,@dptr
      001FAF FC               [12] 4459 	mov	r4,a
      001FB0 A3               [24] 4460 	inc	dptr
      001FB1 E0               [24] 4461 	movx	a,@dptr
      001FB2 FD               [12] 4462 	mov	r5,a
      001FB3 A3               [24] 4463 	inc	dptr
      001FB4 E0               [24] 4464 	movx	a,@dptr
      001FB5 FE               [12] 4465 	mov	r6,a
      001FB6 A3               [24] 4466 	inc	dptr
      001FB7 E0               [24] 4467 	movx	a,@dptr
      001FB8 FF               [12] 4468 	mov	r7,a
      001FB9 90 03 2E         [24] 4469 	mov	dptr,#(_axradio_cb_transmitend + 0x0006)
      001FBC EC               [12] 4470 	mov	a,r4
      001FBD F0               [24] 4471 	movx	@dptr,a
      001FBE ED               [12] 4472 	mov	a,r5
      001FBF A3               [24] 4473 	inc	dptr
      001FC0 F0               [24] 4474 	movx	@dptr,a
      001FC1 EE               [12] 4475 	mov	a,r6
      001FC2 A3               [24] 4476 	inc	dptr
      001FC3 F0               [24] 4477 	movx	@dptr,a
      001FC4 EF               [12] 4478 	mov	a,r7
      001FC5 A3               [24] 4479 	inc	dptr
      001FC6 F0               [24] 4480 	movx	@dptr,a
                                   4481 ;	..\src\COMMON\easyax5043.c:734: wtimer_add_callback(&axradio_cb_transmitend.cb);
      001FC7 90 03 28         [24] 4482 	mov	dptr,#_axradio_cb_transmitend
      001FCA 12 66 0B         [24] 4483 	lcall	_wtimer_add_callback
                                   4484 ;	..\src\COMMON\easyax5043.c:735: break;
      001FCD 02 21 5A         [24] 4485 	ljmp	00167$
                                   4486 ;	..\src\COMMON\easyax5043.c:738: case trxstate_txcw_xtalwait:
      001FD0                       4487 00144$:
                                   4488 ;	..\src\COMMON\easyax5043.c:739: AX5043_IRQMASK1 = 0x00;
      001FD0 90 40 06         [24] 4489 	mov	dptr,#_AX5043_IRQMASK1
      001FD3 E4               [12] 4490 	clr	a
      001FD4 F0               [24] 4491 	movx	@dptr,a
                                   4492 ;	..\src\COMMON\easyax5043.c:740: AX5043_IRQMASK0 = 0x00;
      001FD5 90 40 07         [24] 4493 	mov	dptr,#_AX5043_IRQMASK0
      001FD8 F0               [24] 4494 	movx	@dptr,a
                                   4495 ;	..\src\COMMON\easyax5043.c:741: AX5043_PWRMODE = AX5043_PWRSTATE_FULL_TX;
      001FD9 90 40 02         [24] 4496 	mov	dptr,#_AX5043_PWRMODE
      001FDC 74 0D            [12] 4497 	mov	a,#0x0d
      001FDE F0               [24] 4498 	movx	@dptr,a
                                   4499 ;	..\src\COMMON\easyax5043.c:742: axradio_trxstate = trxstate_off;
      001FDF 75 0C 00         [24] 4500 	mov	_axradio_trxstate,#0x00
                                   4501 ;	..\src\COMMON\easyax5043.c:743: update_timeanchor();
      001FE2 12 15 BA         [24] 4502 	lcall	_update_timeanchor
                                   4503 ;	..\src\COMMON\easyax5043.c:744: wtimer_remove_callback(&axradio_cb_transmitstart.cb);
      001FE5 90 03 1E         [24] 4504 	mov	dptr,#_axradio_cb_transmitstart
      001FE8 12 74 C0         [24] 4505 	lcall	_wtimer_remove_callback
                                   4506 ;	..\src\COMMON\easyax5043.c:745: axradio_cb_transmitstart.st.error = AXRADIO_ERR_NOERROR;
      001FEB 90 03 23         [24] 4507 	mov	dptr,#(_axradio_cb_transmitstart + 0x0005)
      001FEE E4               [12] 4508 	clr	a
      001FEF F0               [24] 4509 	movx	@dptr,a
                                   4510 ;	..\src\COMMON\easyax5043.c:746: axradio_cb_transmitstart.st.time.t = axradio_timeanchor.radiotimer;
      001FF0 90 00 CD         [24] 4511 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      001FF3 E0               [24] 4512 	movx	a,@dptr
      001FF4 FC               [12] 4513 	mov	r4,a
      001FF5 A3               [24] 4514 	inc	dptr
      001FF6 E0               [24] 4515 	movx	a,@dptr
      001FF7 FD               [12] 4516 	mov	r5,a
      001FF8 A3               [24] 4517 	inc	dptr
      001FF9 E0               [24] 4518 	movx	a,@dptr
      001FFA FE               [12] 4519 	mov	r6,a
      001FFB A3               [24] 4520 	inc	dptr
      001FFC E0               [24] 4521 	movx	a,@dptr
      001FFD FF               [12] 4522 	mov	r7,a
      001FFE 90 03 24         [24] 4523 	mov	dptr,#(_axradio_cb_transmitstart + 0x0006)
      002001 EC               [12] 4524 	mov	a,r4
      002002 F0               [24] 4525 	movx	@dptr,a
      002003 ED               [12] 4526 	mov	a,r5
      002004 A3               [24] 4527 	inc	dptr
      002005 F0               [24] 4528 	movx	@dptr,a
      002006 EE               [12] 4529 	mov	a,r6
      002007 A3               [24] 4530 	inc	dptr
      002008 F0               [24] 4531 	movx	@dptr,a
      002009 EF               [12] 4532 	mov	a,r7
      00200A A3               [24] 4533 	inc	dptr
      00200B F0               [24] 4534 	movx	@dptr,a
                                   4535 ;	..\src\COMMON\easyax5043.c:747: wtimer_add_callback(&axradio_cb_transmitstart.cb);
      00200C 90 03 1E         [24] 4536 	mov	dptr,#_axradio_cb_transmitstart
      00200F 12 66 0B         [24] 4537 	lcall	_wtimer_add_callback
                                   4538 ;	..\src\COMMON\easyax5043.c:748: break;
      002012 02 21 5A         [24] 4539 	ljmp	00167$
                                   4540 ;	..\src\COMMON\easyax5043.c:750: case trxstate_txstream_xtalwait:
      002015                       4541 00145$:
                                   4542 ;	..\src\COMMON\easyax5043.c:751: if (AX5043_IRQREQUEST1 & 0x01) {
      002015 90 40 0C         [24] 4543 	mov	dptr,#_AX5043_IRQREQUEST1
      002018 E0               [24] 4544 	movx	a,@dptr
      002019 FF               [12] 4545 	mov	r7,a
      00201A 20 E0 03         [24] 4546 	jb	acc.0,00286$
      00201D 02 20 B2         [24] 4547 	ljmp	00155$
      002020                       4548 00286$:
                                   4549 ;	..\src\COMMON\easyax5043.c:752: AX5043_RADIOEVENTMASK0 = 0x03; // enable PLL settled and done event
      002020 90 40 09         [24] 4550 	mov	dptr,#_AX5043_RADIOEVENTMASK0
      002023 74 03            [12] 4551 	mov	a,#0x03
      002025 F0               [24] 4552 	movx	@dptr,a
                                   4553 ;	..\src\COMMON\easyax5043.c:753: AX5043_IRQMASK1 = 0x00;
      002026 90 40 06         [24] 4554 	mov	dptr,#_AX5043_IRQMASK1
      002029 E4               [12] 4555 	clr	a
      00202A F0               [24] 4556 	movx	@dptr,a
                                   4557 ;	..\src\COMMON\easyax5043.c:754: AX5043_IRQMASK0 = 0x40; // enable radio controller irq
      00202B 90 40 07         [24] 4558 	mov	dptr,#_AX5043_IRQMASK0
      00202E 74 40            [12] 4559 	mov	a,#0x40
      002030 F0               [24] 4560 	movx	@dptr,a
                                   4561 ;	..\src\COMMON\easyax5043.c:755: AX5043_PWRMODE = AX5043_PWRSTATE_FULL_TX;
      002031 90 40 02         [24] 4562 	mov	dptr,#_AX5043_PWRMODE
      002034 74 0D            [12] 4563 	mov	a,#0x0d
      002036 F0               [24] 4564 	movx	@dptr,a
                                   4565 ;	..\src\COMMON\easyax5043.c:756: axradio_trxstate = trxstate_txstream;
      002037 75 0C 10         [24] 4566 	mov	_axradio_trxstate,#0x10
                                   4567 ;	..\src\COMMON\easyax5043.c:758: goto txstreamdatacb;
                                   4568 ;	..\src\COMMON\easyax5043.c:760: case trxstate_txstream:
      00203A 80 76            [24] 4569 	sjmp	00155$
      00203C                       4570 00148$:
                                   4571 ;	..\src\COMMON\easyax5043.c:762: uint8_t __autodata evt = AX5043_RADIOEVENTREQ0;
      00203C 90 40 0F         [24] 4572 	mov	dptr,#_AX5043_RADIOEVENTREQ0
      00203F E0               [24] 4573 	movx	a,@dptr
                                   4574 ;	..\src\COMMON\easyax5043.c:763: if (evt & 0x03)
      002040 FF               [12] 4575 	mov	r7,a
      002041 54 03            [12] 4576 	anl	a,#0x03
      002043 60 07            [24] 4577 	jz	00150$
                                   4578 ;	..\src\COMMON\easyax5043.c:764: update_timeanchor();
      002045 C0 07            [24] 4579 	push	ar7
      002047 12 15 BA         [24] 4580 	lcall	_update_timeanchor
      00204A D0 07            [24] 4581 	pop	ar7
      00204C                       4582 00150$:
                                   4583 ;	..\src\COMMON\easyax5043.c:765: if (evt & 0x01) {
      00204C EF               [12] 4584 	mov	a,r7
      00204D 30 E0 31         [24] 4585 	jnb	acc.0,00152$
                                   4586 ;	..\src\COMMON\easyax5043.c:766: wtimer_remove_callback(&axradio_cb_transmitend.cb);
      002050 90 03 28         [24] 4587 	mov	dptr,#_axradio_cb_transmitend
      002053 C0 07            [24] 4588 	push	ar7
      002055 12 74 C0         [24] 4589 	lcall	_wtimer_remove_callback
                                   4590 ;	..\src\COMMON\easyax5043.c:767: axradio_cb_transmitend.st.error = AXRADIO_ERR_NOERROR;
      002058 90 03 2D         [24] 4591 	mov	dptr,#(_axradio_cb_transmitend + 0x0005)
      00205B E4               [12] 4592 	clr	a
      00205C F0               [24] 4593 	movx	@dptr,a
                                   4594 ;	..\src\COMMON\easyax5043.c:768: axradio_cb_transmitend.st.time.t = axradio_timeanchor.radiotimer;
      00205D 90 00 CD         [24] 4595 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      002060 E0               [24] 4596 	movx	a,@dptr
      002061 FB               [12] 4597 	mov	r3,a
      002062 A3               [24] 4598 	inc	dptr
      002063 E0               [24] 4599 	movx	a,@dptr
      002064 FC               [12] 4600 	mov	r4,a
      002065 A3               [24] 4601 	inc	dptr
      002066 E0               [24] 4602 	movx	a,@dptr
      002067 FD               [12] 4603 	mov	r5,a
      002068 A3               [24] 4604 	inc	dptr
      002069 E0               [24] 4605 	movx	a,@dptr
      00206A FE               [12] 4606 	mov	r6,a
      00206B 90 03 2E         [24] 4607 	mov	dptr,#(_axradio_cb_transmitend + 0x0006)
      00206E EB               [12] 4608 	mov	a,r3
      00206F F0               [24] 4609 	movx	@dptr,a
      002070 EC               [12] 4610 	mov	a,r4
      002071 A3               [24] 4611 	inc	dptr
      002072 F0               [24] 4612 	movx	@dptr,a
      002073 ED               [12] 4613 	mov	a,r5
      002074 A3               [24] 4614 	inc	dptr
      002075 F0               [24] 4615 	movx	@dptr,a
      002076 EE               [12] 4616 	mov	a,r6
      002077 A3               [24] 4617 	inc	dptr
      002078 F0               [24] 4618 	movx	@dptr,a
                                   4619 ;	..\src\COMMON\easyax5043.c:769: wtimer_add_callback(&axradio_cb_transmitend.cb);
      002079 90 03 28         [24] 4620 	mov	dptr,#_axradio_cb_transmitend
      00207C 12 66 0B         [24] 4621 	lcall	_wtimer_add_callback
      00207F D0 07            [24] 4622 	pop	ar7
      002081                       4623 00152$:
                                   4624 ;	..\src\COMMON\easyax5043.c:771: if (evt & 0x02) {
      002081 EF               [12] 4625 	mov	a,r7
      002082 30 E1 2D         [24] 4626 	jnb	acc.1,00155$
                                   4627 ;	..\src\COMMON\easyax5043.c:772: wtimer_remove_callback(&axradio_cb_transmitstart.cb);
      002085 90 03 1E         [24] 4628 	mov	dptr,#_axradio_cb_transmitstart
      002088 12 74 C0         [24] 4629 	lcall	_wtimer_remove_callback
                                   4630 ;	..\src\COMMON\easyax5043.c:773: axradio_cb_transmitstart.st.error = AXRADIO_ERR_NOERROR;
      00208B 90 03 23         [24] 4631 	mov	dptr,#(_axradio_cb_transmitstart + 0x0005)
      00208E E4               [12] 4632 	clr	a
      00208F F0               [24] 4633 	movx	@dptr,a
                                   4634 ;	..\src\COMMON\easyax5043.c:774: axradio_cb_transmitstart.st.time.t = axradio_timeanchor.radiotimer;
      002090 90 00 CD         [24] 4635 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      002093 E0               [24] 4636 	movx	a,@dptr
      002094 FC               [12] 4637 	mov	r4,a
      002095 A3               [24] 4638 	inc	dptr
      002096 E0               [24] 4639 	movx	a,@dptr
      002097 FD               [12] 4640 	mov	r5,a
      002098 A3               [24] 4641 	inc	dptr
      002099 E0               [24] 4642 	movx	a,@dptr
      00209A FE               [12] 4643 	mov	r6,a
      00209B A3               [24] 4644 	inc	dptr
      00209C E0               [24] 4645 	movx	a,@dptr
      00209D FF               [12] 4646 	mov	r7,a
      00209E 90 03 24         [24] 4647 	mov	dptr,#(_axradio_cb_transmitstart + 0x0006)
      0020A1 EC               [12] 4648 	mov	a,r4
      0020A2 F0               [24] 4649 	movx	@dptr,a
      0020A3 ED               [12] 4650 	mov	a,r5
      0020A4 A3               [24] 4651 	inc	dptr
      0020A5 F0               [24] 4652 	movx	@dptr,a
      0020A6 EE               [12] 4653 	mov	a,r6
      0020A7 A3               [24] 4654 	inc	dptr
      0020A8 F0               [24] 4655 	movx	@dptr,a
      0020A9 EF               [12] 4656 	mov	a,r7
      0020AA A3               [24] 4657 	inc	dptr
      0020AB F0               [24] 4658 	movx	@dptr,a
                                   4659 ;	..\src\COMMON\easyax5043.c:775: wtimer_add_callback(&axradio_cb_transmitstart.cb);
      0020AC 90 03 1E         [24] 4660 	mov	dptr,#_axradio_cb_transmitstart
      0020AF 12 66 0B         [24] 4661 	lcall	_wtimer_add_callback
                                   4662 ;	..\src\COMMON\easyax5043.c:778: txstreamdatacb:
      0020B2                       4663 00155$:
                                   4664 ;	..\src\COMMON\easyax5043.c:779: if (AX5043_IRQREQUEST0 & AX5043_IRQMASK0 & 0x08) {
      0020B2 90 40 0D         [24] 4665 	mov	dptr,#_AX5043_IRQREQUEST0
      0020B5 E0               [24] 4666 	movx	a,@dptr
      0020B6 FF               [12] 4667 	mov	r7,a
      0020B7 90 40 07         [24] 4668 	mov	dptr,#_AX5043_IRQMASK0
      0020BA E0               [24] 4669 	movx	a,@dptr
      0020BB FE               [12] 4670 	mov	r6,a
      0020BC 5F               [12] 4671 	anl	a,r7
      0020BD 20 E3 03         [24] 4672 	jb	acc.3,00290$
      0020C0 02 21 5A         [24] 4673 	ljmp	00167$
      0020C3                       4674 00290$:
                                   4675 ;	..\src\COMMON\easyax5043.c:780: AX5043_IRQMASK0 &= (uint8_t)~0x08;
      0020C3 90 40 07         [24] 4676 	mov	dptr,#_AX5043_IRQMASK0
      0020C6 E0               [24] 4677 	movx	a,@dptr
      0020C7 FF               [12] 4678 	mov	r7,a
      0020C8 74 F7            [12] 4679 	mov	a,#0xf7
      0020CA 5F               [12] 4680 	anl	a,r7
      0020CB F0               [24] 4681 	movx	@dptr,a
                                   4682 ;	..\src\COMMON\easyax5043.c:781: update_timeanchor();
      0020CC 12 15 BA         [24] 4683 	lcall	_update_timeanchor
                                   4684 ;	..\src\COMMON\easyax5043.c:782: wtimer_remove_callback(&axradio_cb_transmitdata.cb);
      0020CF 90 03 32         [24] 4685 	mov	dptr,#_axradio_cb_transmitdata
      0020D2 12 74 C0         [24] 4686 	lcall	_wtimer_remove_callback
                                   4687 ;	..\src\COMMON\easyax5043.c:783: axradio_cb_transmitdata.st.error = AXRADIO_ERR_NOERROR;
      0020D5 90 03 37         [24] 4688 	mov	dptr,#(_axradio_cb_transmitdata + 0x0005)
      0020D8 E4               [12] 4689 	clr	a
      0020D9 F0               [24] 4690 	movx	@dptr,a
                                   4691 ;	..\src\COMMON\easyax5043.c:784: axradio_cb_transmitdata.st.time.t = axradio_timeanchor.radiotimer;
      0020DA 90 00 CD         [24] 4692 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      0020DD E0               [24] 4693 	movx	a,@dptr
      0020DE FC               [12] 4694 	mov	r4,a
      0020DF A3               [24] 4695 	inc	dptr
      0020E0 E0               [24] 4696 	movx	a,@dptr
      0020E1 FD               [12] 4697 	mov	r5,a
      0020E2 A3               [24] 4698 	inc	dptr
      0020E3 E0               [24] 4699 	movx	a,@dptr
      0020E4 FE               [12] 4700 	mov	r6,a
      0020E5 A3               [24] 4701 	inc	dptr
      0020E6 E0               [24] 4702 	movx	a,@dptr
      0020E7 FF               [12] 4703 	mov	r7,a
      0020E8 90 03 38         [24] 4704 	mov	dptr,#(_axradio_cb_transmitdata + 0x0006)
      0020EB EC               [12] 4705 	mov	a,r4
      0020EC F0               [24] 4706 	movx	@dptr,a
      0020ED ED               [12] 4707 	mov	a,r5
      0020EE A3               [24] 4708 	inc	dptr
      0020EF F0               [24] 4709 	movx	@dptr,a
      0020F0 EE               [12] 4710 	mov	a,r6
      0020F1 A3               [24] 4711 	inc	dptr
      0020F2 F0               [24] 4712 	movx	@dptr,a
      0020F3 EF               [12] 4713 	mov	a,r7
      0020F4 A3               [24] 4714 	inc	dptr
      0020F5 F0               [24] 4715 	movx	@dptr,a
                                   4716 ;	..\src\COMMON\easyax5043.c:785: wtimer_add_callback(&axradio_cb_transmitdata.cb);
      0020F6 90 03 32         [24] 4717 	mov	dptr,#_axradio_cb_transmitdata
      0020F9 12 66 0B         [24] 4718 	lcall	_wtimer_add_callback
                                   4719 ;	..\src\COMMON\easyax5043.c:787: break;
                                   4720 ;	..\src\COMMON\easyax5043.c:789: case trxstate_rxwor:
      0020FC 80 5C            [24] 4721 	sjmp	00167$
      0020FE                       4722 00158$:
                                   4723 ;	..\src\COMMON\easyax5043.c:792: if( AX5043_IRQREQUEST0 & 0x80 ) // vdda ready (note irqinversion does not act upon AX5043_IRQREQUEST0)
      0020FE 90 40 0D         [24] 4724 	mov	dptr,#_AX5043_IRQREQUEST0
      002101 E0               [24] 4725 	movx	a,@dptr
      002102 FF               [12] 4726 	mov	r7,a
      002103 30 E7 0B         [24] 4727 	jnb	acc.7,00160$
                                   4728 ;	..\src\COMMON\easyax5043.c:794: AX5043_IRQINVERSION0 |= 0x80; // invert pwr irq, so it does not fire continuously
      002106 90 40 0B         [24] 4729 	mov	dptr,#_AX5043_IRQINVERSION0
      002109 E0               [24] 4730 	movx	a,@dptr
      00210A FF               [12] 4731 	mov	r7,a
      00210B 74 80            [12] 4732 	mov	a,#0x80
      00210D 4F               [12] 4733 	orl	a,r7
      00210E F0               [24] 4734 	movx	@dptr,a
      00210F 80 09            [24] 4735 	sjmp	00161$
      002111                       4736 00160$:
                                   4737 ;	..\src\COMMON\easyax5043.c:798: AX5043_IRQINVERSION0 &= (uint8_t)~0x80; // drop pwr irq inversion --> armed again
      002111 90 40 0B         [24] 4738 	mov	dptr,#_AX5043_IRQINVERSION0
      002114 E0               [24] 4739 	movx	a,@dptr
      002115 FF               [12] 4740 	mov	r7,a
      002116 74 7F            [12] 4741 	mov	a,#0x7f
      002118 5F               [12] 4742 	anl	a,r7
      002119 F0               [24] 4743 	movx	@dptr,a
      00211A                       4744 00161$:
                                   4745 ;	..\src\COMMON\easyax5043.c:802: if( AX5043_IRQREQUEST1 & 0x01 ) // XTAL ready
      00211A 90 40 0C         [24] 4746 	mov	dptr,#_AX5043_IRQREQUEST1
      00211D E0               [24] 4747 	movx	a,@dptr
      00211E FF               [12] 4748 	mov	r7,a
      00211F 30 E0 0B         [24] 4749 	jnb	acc.0,00163$
                                   4750 ;	..\src\COMMON\easyax5043.c:804: AX5043_IRQINVERSION1 |= 0x01; // invert the xtal ready irq so it does not fire continuously
      002122 90 40 0A         [24] 4751 	mov	dptr,#_AX5043_IRQINVERSION1
      002125 E0               [24] 4752 	movx	a,@dptr
      002126 FF               [12] 4753 	mov	r7,a
      002127 74 01            [12] 4754 	mov	a,#0x01
      002129 4F               [12] 4755 	orl	a,r7
      00212A F0               [24] 4756 	movx	@dptr,a
      00212B 80 2A            [24] 4757 	sjmp	00165$
      00212D                       4758 00163$:
                                   4759 ;	..\src\COMMON\easyax5043.c:808: AX5043_IRQINVERSION1 &= (uint8_t)~0x01; // drop xtal ready irq inversion --> armed again for next wake-up
      00212D 90 40 0A         [24] 4760 	mov	dptr,#_AX5043_IRQINVERSION1
      002130 E0               [24] 4761 	movx	a,@dptr
      002131 FF               [12] 4762 	mov	r7,a
      002132 74 FE            [12] 4763 	mov	a,#0xfe
      002134 5F               [12] 4764 	anl	a,r7
      002135 F0               [24] 4765 	movx	@dptr,a
                                   4766 ;	..\src\COMMON\easyax5043.c:809: AX5043_0xF30 = f30_saved;
      002136 90 09 AA         [24] 4767 	mov	dptr,#_f30_saved
      002139 E0               [24] 4768 	movx	a,@dptr
      00213A 90 4F 30         [24] 4769 	mov	dptr,#_AX5043_0xF30
      00213D F0               [24] 4770 	movx	@dptr,a
                                   4771 ;	..\src\COMMON\easyax5043.c:810: AX5043_0xF31 = f31_saved;
      00213E 90 09 AB         [24] 4772 	mov	dptr,#_f31_saved
      002141 E0               [24] 4773 	movx	a,@dptr
      002142 90 4F 31         [24] 4774 	mov	dptr,#_AX5043_0xF31
      002145 F0               [24] 4775 	movx	@dptr,a
                                   4776 ;	..\src\COMMON\easyax5043.c:811: AX5043_0xF32 = f32_saved;
      002146 90 09 AC         [24] 4777 	mov	dptr,#_f32_saved
      002149 E0               [24] 4778 	movx	a,@dptr
      00214A 90 4F 32         [24] 4779 	mov	dptr,#_AX5043_0xF32
      00214D F0               [24] 4780 	movx	@dptr,a
                                   4781 ;	..\src\COMMON\easyax5043.c:812: AX5043_0xF33 = f33_saved;
      00214E 90 09 AD         [24] 4782 	mov	dptr,#_f33_saved
      002151 E0               [24] 4783 	movx	a,@dptr
      002152 FF               [12] 4784 	mov	r7,a
      002153 90 4F 33         [24] 4785 	mov	dptr,#_AX5043_0xF33
      002156 F0               [24] 4786 	movx	@dptr,a
                                   4787 ;	..\src\COMMON\easyax5043.c:816: case trxstate_rx:
      002157                       4788 00165$:
                                   4789 ;	..\src\COMMON\easyax5043.c:817: receive_isr();
      002157 12 16 A3         [24] 4790 	lcall	_receive_isr
                                   4791 ;	..\src\COMMON\easyax5043.c:820: } // end switch(axradio_trxstate)
      00215A                       4792 00167$:
      00215A D0 D0            [24] 4793 	pop	psw
      00215C D0 00            [24] 4794 	pop	(0+0)
      00215E D0 01            [24] 4795 	pop	(0+1)
      002160 D0 02            [24] 4796 	pop	(0+2)
      002162 D0 03            [24] 4797 	pop	(0+3)
      002164 D0 04            [24] 4798 	pop	(0+4)
      002166 D0 05            [24] 4799 	pop	(0+5)
      002168 D0 06            [24] 4800 	pop	(0+6)
      00216A D0 07            [24] 4801 	pop	(0+7)
      00216C D0 83            [24] 4802 	pop	dph
      00216E D0 82            [24] 4803 	pop	dpl
      002170 D0 F0            [24] 4804 	pop	b
      002172 D0 E0            [24] 4805 	pop	acc
      002174 D0 21            [24] 4806 	pop	bits
      002176 32               [24] 4807 	reti
                                   4808 ;------------------------------------------------------------
                                   4809 ;Allocation info for local variables in function 'ax5043_receiver_on_continuous'
                                   4810 ;------------------------------------------------------------
                                   4811 ;rschanged_int             Allocated to registers r6 
                                   4812 ;------------------------------------------------------------
                                   4813 ;	..\src\COMMON\easyax5043.c:824: __reentrantb void ax5043_receiver_on_continuous(void) __reentrant
                                   4814 ;	-----------------------------------------
                                   4815 ;	 function ax5043_receiver_on_continuous
                                   4816 ;	-----------------------------------------
      002177                       4817 _ax5043_receiver_on_continuous:
                                   4818 ;	..\src\COMMON\easyax5043.c:826: uint8_t rschanged_int = (axradio_framing_enable_sfdcallback | (axradio_mode == AXRADIO_MODE_SYNC_ACK_SLAVE) | (axradio_mode == AXRADIO_MODE_SYNC_SLAVE) );
      002177 74 33            [12] 4819 	mov	a,#0x33
      002179 B5 0B 04         [24] 4820 	cjne	a,_axradio_mode,00114$
      00217C 74 01            [12] 4821 	mov	a,#0x01
      00217E 80 01            [24] 4822 	sjmp	00115$
      002180                       4823 00114$:
      002180 E4               [12] 4824 	clr	a
      002181                       4825 00115$:
      002181 FF               [12] 4826 	mov	r7,a
      002182 90 7A E8         [24] 4827 	mov	dptr,#_axradio_framing_enable_sfdcallback
      002185 E4               [12] 4828 	clr	a
      002186 93               [24] 4829 	movc	a,@a+dptr
      002187 FE               [12] 4830 	mov	r6,a
      002188 42 07            [12] 4831 	orl	ar7,a
      00218A 74 32            [12] 4832 	mov	a,#0x32
      00218C B5 0B 04         [24] 4833 	cjne	a,_axradio_mode,00116$
      00218F 74 01            [12] 4834 	mov	a,#0x01
      002191 80 01            [24] 4835 	sjmp	00117$
      002193                       4836 00116$:
      002193 E4               [12] 4837 	clr	a
      002194                       4838 00117$:
      002194 42 07            [12] 4839 	orl	ar7,a
                                   4840 ;	..\src\COMMON\easyax5043.c:827: if(rschanged_int)
      002196 EF               [12] 4841 	mov	a,r7
      002197 FE               [12] 4842 	mov	r6,a
      002198 60 06            [24] 4843 	jz	00102$
                                   4844 ;	..\src\COMMON\easyax5043.c:828: AX5043_RADIOEVENTMASK0 = 0x04;
      00219A 90 40 09         [24] 4845 	mov	dptr,#_AX5043_RADIOEVENTMASK0
      00219D 74 04            [12] 4846 	mov	a,#0x04
      00219F F0               [24] 4847 	movx	@dptr,a
      0021A0                       4848 00102$:
                                   4849 ;	..\src\COMMON\easyax5043.c:829: AX5043_RSSIREFERENCE = axradio_phy_rssireference;
      0021A0 90 7A C7         [24] 4850 	mov	dptr,#_axradio_phy_rssireference
      0021A3 E4               [12] 4851 	clr	a
      0021A4 93               [24] 4852 	movc	a,@a+dptr
      0021A5 90 42 2C         [24] 4853 	mov	dptr,#_AX5043_RSSIREFERENCE
      0021A8 F0               [24] 4854 	movx	@dptr,a
                                   4855 ;	..\src\COMMON\easyax5043.c:830: ax5043_set_registers_rxcont();
      0021A9 C0 06            [24] 4856 	push	ar6
      0021AB 12 06 B1         [24] 4857 	lcall	_ax5043_set_registers_rxcont
      0021AE D0 06            [24] 4858 	pop	ar6
                                   4859 ;	..\src\COMMON\easyax5043.c:843: AX5043_PKTSTOREFLAGS &= (uint8_t)~0x40;
      0021B0 90 42 32         [24] 4860 	mov	dptr,#_AX5043_PKTSTOREFLAGS
      0021B3 E0               [24] 4861 	movx	a,@dptr
      0021B4 FF               [12] 4862 	mov	r7,a
      0021B5 74 BF            [12] 4863 	mov	a,#0xbf
      0021B7 5F               [12] 4864 	anl	a,r7
      0021B8 F0               [24] 4865 	movx	@dptr,a
                                   4866 ;	..\src\COMMON\easyax5043.c:846: AX5043_FIFOSTAT = 3; // clear FIFO data & flags
      0021B9 90 40 28         [24] 4867 	mov	dptr,#_AX5043_FIFOSTAT
      0021BC 74 03            [12] 4868 	mov	a,#0x03
      0021BE F0               [24] 4869 	movx	@dptr,a
                                   4870 ;	..\src\COMMON\easyax5043.c:847: AX5043_PWRMODE = AX5043_PWRSTATE_FULL_RX;
      0021BF 90 40 02         [24] 4871 	mov	dptr,#_AX5043_PWRMODE
      0021C2 74 09            [12] 4872 	mov	a,#0x09
      0021C4 F0               [24] 4873 	movx	@dptr,a
                                   4874 ;	..\src\COMMON\easyax5043.c:848: axradio_trxstate = trxstate_rx;
      0021C5 75 0C 01         [24] 4875 	mov	_axradio_trxstate,#0x01
                                   4876 ;	..\src\COMMON\easyax5043.c:849: if(rschanged_int)
      0021C8 EE               [12] 4877 	mov	a,r6
      0021C9 60 08            [24] 4878 	jz	00104$
                                   4879 ;	..\src\COMMON\easyax5043.c:850: AX5043_IRQMASK0 = 0x41; //  enable FIFO not empty / radio controller irq
      0021CB 90 40 07         [24] 4880 	mov	dptr,#_AX5043_IRQMASK0
      0021CE 74 41            [12] 4881 	mov	a,#0x41
      0021D0 F0               [24] 4882 	movx	@dptr,a
      0021D1 80 06            [24] 4883 	sjmp	00105$
      0021D3                       4884 00104$:
                                   4885 ;	..\src\COMMON\easyax5043.c:852: AX5043_IRQMASK0 = 0x01; //  enable FIFO not empty
      0021D3 90 40 07         [24] 4886 	mov	dptr,#_AX5043_IRQMASK0
      0021D6 74 01            [12] 4887 	mov	a,#0x01
      0021D8 F0               [24] 4888 	movx	@dptr,a
      0021D9                       4889 00105$:
                                   4890 ;	..\src\COMMON\easyax5043.c:853: AX5043_IRQMASK1 = 0x00;
      0021D9 90 40 06         [24] 4891 	mov	dptr,#_AX5043_IRQMASK1
      0021DC E4               [12] 4892 	clr	a
      0021DD F0               [24] 4893 	movx	@dptr,a
      0021DE 22               [24] 4894 	ret
                                   4895 ;------------------------------------------------------------
                                   4896 ;Allocation info for local variables in function 'ax5043_receiver_on_wor'
                                   4897 ;------------------------------------------------------------
                                   4898 ;wp                        Allocated to registers r6 r7 
                                   4899 ;------------------------------------------------------------
                                   4900 ;	..\src\COMMON\easyax5043.c:856: __reentrantb void ax5043_receiver_on_wor(void) __reentrant
                                   4901 ;	-----------------------------------------
                                   4902 ;	 function ax5043_receiver_on_wor
                                   4903 ;	-----------------------------------------
      0021DF                       4904 _ax5043_receiver_on_wor:
                                   4905 ;	..\src\COMMON\easyax5043.c:858: AX5043_BGNDRSSIGAIN = 0x02;
      0021DF 90 42 2E         [24] 4906 	mov	dptr,#_AX5043_BGNDRSSIGAIN
      0021E2 74 02            [12] 4907 	mov	a,#0x02
      0021E4 F0               [24] 4908 	movx	@dptr,a
                                   4909 ;	..\src\COMMON\easyax5043.c:859: if(axradio_framing_enable_sfdcallback)
      0021E5 90 7A E8         [24] 4910 	mov	dptr,#_axradio_framing_enable_sfdcallback
      0021E8 E4               [12] 4911 	clr	a
      0021E9 93               [24] 4912 	movc	a,@a+dptr
      0021EA 60 06            [24] 4913 	jz	00102$
                                   4914 ;	..\src\COMMON\easyax5043.c:860: AX5043_RADIOEVENTMASK0 = 0x04;
      0021EC 90 40 09         [24] 4915 	mov	dptr,#_AX5043_RADIOEVENTMASK0
      0021EF 74 04            [12] 4916 	mov	a,#0x04
      0021F1 F0               [24] 4917 	movx	@dptr,a
      0021F2                       4918 00102$:
                                   4919 ;	..\src\COMMON\easyax5043.c:861: AX5043_FIFOSTAT = 3; // clear FIFO data & flags
      0021F2 90 40 28         [24] 4920 	mov	dptr,#_AX5043_FIFOSTAT
      0021F5 74 03            [12] 4921 	mov	a,#0x03
      0021F7 F0               [24] 4922 	movx	@dptr,a
                                   4923 ;	..\src\COMMON\easyax5043.c:862: AX5043_LPOSCCONFIG = 0x01; // start LPOSC, slow mode
      0021F8 90 43 10         [24] 4924 	mov	dptr,#_AX5043_LPOSCCONFIG
      0021FB 74 01            [12] 4925 	mov	a,#0x01
      0021FD F0               [24] 4926 	movx	@dptr,a
                                   4927 ;	..\src\COMMON\easyax5043.c:863: AX5043_RSSIREFERENCE = axradio_phy_rssireference;
      0021FE 90 7A C7         [24] 4928 	mov	dptr,#_axradio_phy_rssireference
      002201 E4               [12] 4929 	clr	a
      002202 93               [24] 4930 	movc	a,@a+dptr
      002203 90 42 2C         [24] 4931 	mov	dptr,#_AX5043_RSSIREFERENCE
      002206 F0               [24] 4932 	movx	@dptr,a
                                   4933 ;	..\src\COMMON\easyax5043.c:864: ax5043_set_registers_rxwor();
      002207 12 06 9E         [24] 4934 	lcall	_ax5043_set_registers_rxwor
                                   4935 ;	..\src\COMMON\easyax5043.c:865: AX5043_PKTSTOREFLAGS &= (uint8_t)~0x40;
      00220A 90 42 32         [24] 4936 	mov	dptr,#_AX5043_PKTSTOREFLAGS
      00220D E0               [24] 4937 	movx	a,@dptr
      00220E FF               [12] 4938 	mov	r7,a
      00220F 74 BF            [12] 4939 	mov	a,#0xbf
      002211 5F               [12] 4940 	anl	a,r7
      002212 F0               [24] 4941 	movx	@dptr,a
                                   4942 ;	..\src\COMMON\easyax5043.c:867: AX5043_PWRMODE = AX5043_PWRSTATE_WOR_RX;
      002213 90 40 02         [24] 4943 	mov	dptr,#_AX5043_PWRMODE
      002216 74 0B            [12] 4944 	mov	a,#0x0b
      002218 F0               [24] 4945 	movx	@dptr,a
                                   4946 ;	..\src\COMMON\easyax5043.c:868: axradio_trxstate = trxstate_rxwor;
      002219 75 0C 02         [24] 4947 	mov	_axradio_trxstate,#0x02
                                   4948 ;	..\src\COMMON\easyax5043.c:869: if(axradio_framing_enable_sfdcallback)
      00221C 90 7A E8         [24] 4949 	mov	dptr,#_axradio_framing_enable_sfdcallback
      00221F E4               [12] 4950 	clr	a
      002220 93               [24] 4951 	movc	a,@a+dptr
      002221 60 08            [24] 4952 	jz	00104$
                                   4953 ;	..\src\COMMON\easyax5043.c:870: AX5043_IRQMASK0 = 0x41; //  enable FIFO not empty / radio controller irq
      002223 90 40 07         [24] 4954 	mov	dptr,#_AX5043_IRQMASK0
      002226 74 41            [12] 4955 	mov	a,#0x41
      002228 F0               [24] 4956 	movx	@dptr,a
      002229 80 06            [24] 4957 	sjmp	00105$
      00222B                       4958 00104$:
                                   4959 ;	..\src\COMMON\easyax5043.c:872: AX5043_IRQMASK0 = 0x01; //  enable FIFO not empty
      00222B 90 40 07         [24] 4960 	mov	dptr,#_AX5043_IRQMASK0
      00222E 74 01            [12] 4961 	mov	a,#0x01
      002230 F0               [24] 4962 	movx	@dptr,a
      002231                       4963 00105$:
                                   4964 ;	..\src\COMMON\easyax5043.c:874: if( ( (PALTRADIO & 0x40) && ((AX5043_PINFUNCPWRAMP & 0x0F) == 0x07) ) || ( (PALTRADIO & 0x80) && ( (AX5043_PINFUNCANTSEL & 0x07 ) == 0x04 ) ) ) // pass through of TCXO_EN
      002231 90 70 46         [24] 4965 	mov	dptr,#_PALTRADIO
      002234 E0               [24] 4966 	movx	a,@dptr
      002235 FF               [12] 4967 	mov	r7,a
      002236 30 E6 0D         [24] 4968 	jnb	acc.6,00110$
      002239 90 40 26         [24] 4969 	mov	dptr,#_AX5043_PINFUNCPWRAMP
      00223C E0               [24] 4970 	movx	a,@dptr
      00223D FF               [12] 4971 	mov	r7,a
      00223E 53 07 0F         [24] 4972 	anl	ar7,#0x0f
      002241 BF 07 02         [24] 4973 	cjne	r7,#0x07,00128$
      002244 80 13            [24] 4974 	sjmp	00106$
      002246                       4975 00128$:
      002246                       4976 00110$:
      002246 90 70 46         [24] 4977 	mov	dptr,#_PALTRADIO
      002249 E0               [24] 4978 	movx	a,@dptr
      00224A FF               [12] 4979 	mov	r7,a
      00224B 30 E7 1A         [24] 4980 	jnb	acc.7,00107$
      00224E 90 40 25         [24] 4981 	mov	dptr,#_AX5043_PINFUNCANTSEL
      002251 E0               [24] 4982 	movx	a,@dptr
      002252 FF               [12] 4983 	mov	r7,a
      002253 53 07 07         [24] 4984 	anl	ar7,#0x07
      002256 BF 04 0F         [24] 4985 	cjne	r7,#0x04,00107$
      002259                       4986 00106$:
                                   4987 ;	..\src\COMMON\easyax5043.c:877: AX5043_IRQMASK0 |= 0x80; // power irq (AX8052F143 WOR with TCXO)
      002259 90 40 07         [24] 4988 	mov	dptr,#_AX5043_IRQMASK0
      00225C E0               [24] 4989 	movx	a,@dptr
      00225D FF               [12] 4990 	mov	r7,a
      00225E 74 80            [12] 4991 	mov	a,#0x80
      002260 4F               [12] 4992 	orl	a,r7
      002261 F0               [24] 4993 	movx	@dptr,a
                                   4994 ;	..\src\COMMON\easyax5043.c:878: AX5043_POWIRQMASK = 0x90; // interrupt when vddana ready (AX8052F143 WOR with TCXO)
      002262 90 40 05         [24] 4995 	mov	dptr,#_AX5043_POWIRQMASK
      002265 74 90            [12] 4996 	mov	a,#0x90
      002267 F0               [24] 4997 	movx	@dptr,a
      002268                       4998 00107$:
                                   4999 ;	..\src\COMMON\easyax5043.c:881: AX5043_IRQMASK1 = 0x01; // xtal ready
      002268 90 40 06         [24] 5000 	mov	dptr,#_AX5043_IRQMASK1
      00226B 74 01            [12] 5001 	mov	a,#0x01
      00226D F0               [24] 5002 	movx	@dptr,a
                                   5003 ;	..\src\COMMON\easyax5043.c:883: uint16_t wp = axradio_wor_period;
      00226E 90 7A F4         [24] 5004 	mov	dptr,#_axradio_wor_period
      002271 E4               [12] 5005 	clr	a
      002272 93               [24] 5006 	movc	a,@a+dptr
      002273 FE               [12] 5007 	mov	r6,a
      002274 74 01            [12] 5008 	mov	a,#0x01
      002276 93               [24] 5009 	movc	a,@a+dptr
                                   5010 ;	..\src\COMMON\easyax5043.c:884: AX5043_WAKEUPFREQ1 = (wp >> 8) & 0xFF;
      002277 FF               [12] 5011 	mov	r7,a
      002278 FD               [12] 5012 	mov	r5,a
      002279 90 40 6C         [24] 5013 	mov	dptr,#_AX5043_WAKEUPFREQ1
      00227C ED               [12] 5014 	mov	a,r5
      00227D F0               [24] 5015 	movx	@dptr,a
                                   5016 ;	..\src\COMMON\easyax5043.c:885: AX5043_WAKEUPFREQ0 = (wp >> 0) & 0xFF;  // actually wakeup period measured in LP OSC cycles
      00227E 8E 05            [24] 5017 	mov	ar5,r6
      002280 90 40 6D         [24] 5018 	mov	dptr,#_AX5043_WAKEUPFREQ0
      002283 ED               [12] 5019 	mov	a,r5
      002284 F0               [24] 5020 	movx	@dptr,a
                                   5021 ;	..\src\COMMON\easyax5043.c:886: wp += radio_read16((uint16_t)&AX5043_WAKEUPTIMER1);
      002285 7C 68            [12] 5022 	mov	r4,#_AX5043_WAKEUPTIMER1
      002287 7D 40            [12] 5023 	mov	r5,#(_AX5043_WAKEUPTIMER1 >> 8)
      002289 8C 82            [24] 5024 	mov	dpl,r4
      00228B 8D 83            [24] 5025 	mov	dph,r5
      00228D 12 68 A0         [24] 5026 	lcall	_radio_read16
      002290 AC 82            [24] 5027 	mov	r4,dpl
      002292 AD 83            [24] 5028 	mov	r5,dph
      002294 EC               [12] 5029 	mov	a,r4
      002295 2E               [12] 5030 	add	a,r6
      002296 FE               [12] 5031 	mov	r6,a
      002297 ED               [12] 5032 	mov	a,r5
      002298 3F               [12] 5033 	addc	a,r7
                                   5034 ;	..\src\COMMON\easyax5043.c:887: AX5043_WAKEUP1 = (wp >> 8) & 0xFF;
      002299 FD               [12] 5035 	mov	r5,a
      00229A 90 40 6A         [24] 5036 	mov	dptr,#_AX5043_WAKEUP1
      00229D ED               [12] 5037 	mov	a,r5
      00229E F0               [24] 5038 	movx	@dptr,a
                                   5039 ;	..\src\COMMON\easyax5043.c:888: AX5043_WAKEUP0 = (wp >> 0) & 0xFF;
      00229F 90 40 6B         [24] 5040 	mov	dptr,#_AX5043_WAKEUP0
      0022A2 EE               [12] 5041 	mov	a,r6
      0022A3 F0               [24] 5042 	movx	@dptr,a
      0022A4 22               [24] 5043 	ret
                                   5044 ;------------------------------------------------------------
                                   5045 ;Allocation info for local variables in function 'ax5043_prepare_tx'
                                   5046 ;------------------------------------------------------------
                                   5047 ;	..\src\COMMON\easyax5043.c:891: __reentrantb void ax5043_prepare_tx(void) __reentrant
                                   5048 ;	-----------------------------------------
                                   5049 ;	 function ax5043_prepare_tx
                                   5050 ;	-----------------------------------------
      0022A5                       5051 _ax5043_prepare_tx:
                                   5052 ;	..\src\COMMON\easyax5043.c:893: AX5043_PWRMODE = AX5043_PWRSTATE_XTAL_ON;
      0022A5 90 40 02         [24] 5053 	mov	dptr,#_AX5043_PWRMODE
      0022A8 74 05            [12] 5054 	mov	a,#0x05
      0022AA F0               [24] 5055 	movx	@dptr,a
                                   5056 ;	..\src\COMMON\easyax5043.c:894: AX5043_PWRMODE = AX5043_PWRSTATE_FIFO_ON;
      0022AB 74 07            [12] 5057 	mov	a,#0x07
      0022AD F0               [24] 5058 	movx	@dptr,a
                                   5059 ;	..\src\COMMON\easyax5043.c:895: ax5043_init_registers_tx();
      0022AE 12 16 97         [24] 5060 	lcall	_ax5043_init_registers_tx
                                   5061 ;	..\src\COMMON\easyax5043.c:896: AX5043_FIFOTHRESH1 = 0;
      0022B1 90 40 2E         [24] 5062 	mov	dptr,#_AX5043_FIFOTHRESH1
      0022B4 E4               [12] 5063 	clr	a
      0022B5 F0               [24] 5064 	movx	@dptr,a
                                   5065 ;	..\src\COMMON\easyax5043.c:897: AX5043_FIFOTHRESH0 = 0x80;
      0022B6 90 40 2F         [24] 5066 	mov	dptr,#_AX5043_FIFOTHRESH0
      0022B9 74 80            [12] 5067 	mov	a,#0x80
      0022BB F0               [24] 5068 	movx	@dptr,a
                                   5069 ;	..\src\COMMON\easyax5043.c:898: axradio_trxstate = trxstate_tx_xtalwait;
      0022BC 75 0C 09         [24] 5070 	mov	_axradio_trxstate,#0x09
                                   5071 ;	..\src\COMMON\easyax5043.c:899: AX5043_IRQMASK0 = 0x00;
      0022BF 90 40 07         [24] 5072 	mov	dptr,#_AX5043_IRQMASK0
      0022C2 E4               [12] 5073 	clr	a
      0022C3 F0               [24] 5074 	movx	@dptr,a
                                   5075 ;	..\src\COMMON\easyax5043.c:900: AX5043_IRQMASK1 = 0x01; // enable xtal ready interrupt
      0022C4 90 40 06         [24] 5076 	mov	dptr,#_AX5043_IRQMASK1
      0022C7 04               [12] 5077 	inc	a
      0022C8 F0               [24] 5078 	movx	@dptr,a
                                   5079 ;	..\src\COMMON\easyax5043.c:901: AX5043_POWSTICKYSTAT; // clear pwr management sticky status --> brownout gate works
      0022C9 90 40 04         [24] 5080 	mov	dptr,#_AX5043_POWSTICKYSTAT
      0022CC E0               [24] 5081 	movx	a,@dptr
      0022CD 22               [24] 5082 	ret
                                   5083 ;------------------------------------------------------------
                                   5084 ;Allocation info for local variables in function 'ax5043_off'
                                   5085 ;------------------------------------------------------------
                                   5086 ;	..\src\COMMON\easyax5043.c:904: __reentrantb void ax5043_off(void) __reentrant
                                   5087 ;	-----------------------------------------
                                   5088 ;	 function ax5043_off
                                   5089 ;	-----------------------------------------
      0022CE                       5090 _ax5043_off:
                                   5091 ;	..\src\COMMON\easyax5043.c:906: ax5043_off_xtal();
      0022CE 12 22 D7         [24] 5092 	lcall	_ax5043_off_xtal
                                   5093 ;	..\src\COMMON\easyax5043.c:907: AX5043_PWRMODE = AX5043_PWRSTATE_POWERDOWN;
      0022D1 90 40 02         [24] 5094 	mov	dptr,#_AX5043_PWRMODE
      0022D4 E4               [12] 5095 	clr	a
      0022D5 F0               [24] 5096 	movx	@dptr,a
      0022D6 22               [24] 5097 	ret
                                   5098 ;------------------------------------------------------------
                                   5099 ;Allocation info for local variables in function 'ax5043_off_xtal'
                                   5100 ;------------------------------------------------------------
                                   5101 ;	..\src\COMMON\easyax5043.c:910: __reentrantb void ax5043_off_xtal(void) __reentrant
                                   5102 ;	-----------------------------------------
                                   5103 ;	 function ax5043_off_xtal
                                   5104 ;	-----------------------------------------
      0022D7                       5105 _ax5043_off_xtal:
                                   5106 ;	..\src\COMMON\easyax5043.c:912: AX5043_IRQMASK0 = 0x00; // IRQ off
      0022D7 90 40 07         [24] 5107 	mov	dptr,#_AX5043_IRQMASK0
      0022DA E4               [12] 5108 	clr	a
      0022DB F0               [24] 5109 	movx	@dptr,a
                                   5110 ;	..\src\COMMON\easyax5043.c:913: AX5043_IRQMASK1 = 0x00;
      0022DC 90 40 06         [24] 5111 	mov	dptr,#_AX5043_IRQMASK1
      0022DF F0               [24] 5112 	movx	@dptr,a
                                   5113 ;	..\src\COMMON\easyax5043.c:914: AX5043_PWRMODE = AX5043_PWRSTATE_XTAL_ON;
      0022E0 90 40 02         [24] 5114 	mov	dptr,#_AX5043_PWRMODE
      0022E3 74 05            [12] 5115 	mov	a,#0x05
      0022E5 F0               [24] 5116 	movx	@dptr,a
                                   5117 ;	..\src\COMMON\easyax5043.c:915: AX5043_LPOSCCONFIG = 0x00; // LPOSC off
      0022E6 90 43 10         [24] 5118 	mov	dptr,#_AX5043_LPOSCCONFIG
      0022E9 E4               [12] 5119 	clr	a
      0022EA F0               [24] 5120 	movx	@dptr,a
                                   5121 ;	..\src\COMMON\easyax5043.c:916: axradio_trxstate = trxstate_off;
                                   5122 ;	1-genFromRTrack replaced	mov	_axradio_trxstate,#0x00
      0022EB F5 0C            [12] 5123 	mov	_axradio_trxstate,a
      0022ED 22               [24] 5124 	ret
                                   5125 ;------------------------------------------------------------
                                   5126 ;Allocation info for local variables in function 'axradio_wait_for_xtal'
                                   5127 ;------------------------------------------------------------
                                   5128 ;iesave                    Allocated to registers r7 
                                   5129 ;------------------------------------------------------------
                                   5130 ;	..\src\COMMON\easyax5043.c:919: void axradio_wait_for_xtal(void)
                                   5131 ;	-----------------------------------------
                                   5132 ;	 function axradio_wait_for_xtal
                                   5133 ;	-----------------------------------------
      0022EE                       5134 _axradio_wait_for_xtal:
                                   5135 ;	..\src\COMMON\easyax5043.c:921: uint8_t __autodata iesave = IE & 0x80;
      0022EE 74 80            [12] 5136 	mov	a,#0x80
      0022F0 55 A8            [12] 5137 	anl	a,_IE
      0022F2 FF               [12] 5138 	mov	r7,a
                                   5139 ;	..\src\COMMON\easyax5043.c:922: EA = 0;
      0022F3 C2 AF            [12] 5140 	clr	_EA
                                   5141 ;	..\src\COMMON\easyax5043.c:923: axradio_trxstate = trxstate_wait_xtal;
      0022F5 75 0C 03         [24] 5142 	mov	_axradio_trxstate,#0x03
                                   5143 ;	..\src\COMMON\easyax5043.c:924: AX5043_IRQMASK1 |= 0x01; // enable xtal ready interrupt
      0022F8 90 40 06         [24] 5144 	mov	dptr,#_AX5043_IRQMASK1
      0022FB E0               [24] 5145 	movx	a,@dptr
      0022FC FE               [12] 5146 	mov	r6,a
      0022FD 74 01            [12] 5147 	mov	a,#0x01
      0022FF 4E               [12] 5148 	orl	a,r6
      002300 F0               [24] 5149 	movx	@dptr,a
      002301                       5150 00104$:
                                   5151 ;	..\src\COMMON\easyax5043.c:926: EA = 0;
      002301 C2 AF            [12] 5152 	clr	_EA
                                   5153 ;	..\src\COMMON\easyax5043.c:927: if (axradio_trxstate == trxstate_xtal_ready)
      002303 74 04            [12] 5154 	mov	a,#0x04
      002305 B5 0C 02         [24] 5155 	cjne	a,_axradio_trxstate,00114$
      002308 80 11            [24] 5156 	sjmp	00103$
      00230A                       5157 00114$:
                                   5158 ;	..\src\COMMON\easyax5043.c:929: wtimer_idle(WTFLAG_CANSTANDBY);
      00230A 75 82 02         [24] 5159 	mov	dpl,#0x02
      00230D C0 07            [24] 5160 	push	ar7
      00230F 12 65 13         [24] 5161 	lcall	_wtimer_idle
                                   5162 ;	..\src\COMMON\easyax5043.c:930: EA = 1;
      002312 D2 AF            [12] 5163 	setb	_EA
                                   5164 ;	..\src\COMMON\easyax5043.c:931: wtimer_runcallbacks();
      002314 12 64 92         [24] 5165 	lcall	_wtimer_runcallbacks
      002317 D0 07            [24] 5166 	pop	ar7
      002319 80 E6            [24] 5167 	sjmp	00104$
      00231B                       5168 00103$:
                                   5169 ;	..\src\COMMON\easyax5043.c:933: IE |= iesave;
      00231B EF               [12] 5170 	mov	a,r7
      00231C 42 A8            [12] 5171 	orl	_IE,a
      00231E 22               [24] 5172 	ret
                                   5173 ;------------------------------------------------------------
                                   5174 ;Allocation info for local variables in function 'axradio_setaddrregs'
                                   5175 ;------------------------------------------------------------
                                   5176 ;pn                        Allocated to registers r6 r7 
                                   5177 ;inv                       Allocated to registers r5 
                                   5178 ;------------------------------------------------------------
                                   5179 ;	..\src\COMMON\easyax5043.c:936: static void axradio_setaddrregs(void)
                                   5180 ;	-----------------------------------------
                                   5181 ;	 function axradio_setaddrregs
                                   5182 ;	-----------------------------------------
      00231F                       5183 _axradio_setaddrregs:
                                   5184 ;	..\src\COMMON\easyax5043.c:938: AX5043_PKTADDR0 = axradio_localaddr.addr[0];
      00231F 90 00 D1         [24] 5185 	mov	dptr,#_axradio_localaddr
      002322 E0               [24] 5186 	movx	a,@dptr
      002323 90 42 07         [24] 5187 	mov	dptr,#_AX5043_PKTADDR0
      002326 F0               [24] 5188 	movx	@dptr,a
                                   5189 ;	..\src\COMMON\easyax5043.c:939: AX5043_PKTADDR1 = axradio_localaddr.addr[1];
      002327 90 00 D2         [24] 5190 	mov	dptr,#(_axradio_localaddr + 0x0001)
      00232A E0               [24] 5191 	movx	a,@dptr
      00232B 90 42 06         [24] 5192 	mov	dptr,#_AX5043_PKTADDR1
      00232E F0               [24] 5193 	movx	@dptr,a
                                   5194 ;	..\src\COMMON\easyax5043.c:940: AX5043_PKTADDR2 = axradio_localaddr.addr[2];
      00232F 90 00 D3         [24] 5195 	mov	dptr,#(_axradio_localaddr + 0x0002)
      002332 E0               [24] 5196 	movx	a,@dptr
      002333 90 42 05         [24] 5197 	mov	dptr,#_AX5043_PKTADDR2
      002336 F0               [24] 5198 	movx	@dptr,a
                                   5199 ;	..\src\COMMON\easyax5043.c:941: AX5043_PKTADDR3 = axradio_localaddr.addr[3];
      002337 90 00 D4         [24] 5200 	mov	dptr,#(_axradio_localaddr + 0x0003)
      00233A E0               [24] 5201 	movx	a,@dptr
      00233B 90 42 04         [24] 5202 	mov	dptr,#_AX5043_PKTADDR3
      00233E F0               [24] 5203 	movx	@dptr,a
                                   5204 ;	..\src\COMMON\easyax5043.c:943: AX5043_PKTADDRMASK0 = axradio_localaddr.mask[0];
      00233F 90 00 D5         [24] 5205 	mov	dptr,#(_axradio_localaddr + 0x0004)
      002342 E0               [24] 5206 	movx	a,@dptr
      002343 90 42 0B         [24] 5207 	mov	dptr,#_AX5043_PKTADDRMASK0
      002346 F0               [24] 5208 	movx	@dptr,a
                                   5209 ;	..\src\COMMON\easyax5043.c:944: AX5043_PKTADDRMASK1 = axradio_localaddr.mask[1];
      002347 90 00 D6         [24] 5210 	mov	dptr,#(_axradio_localaddr + 0x0005)
      00234A E0               [24] 5211 	movx	a,@dptr
      00234B 90 42 0A         [24] 5212 	mov	dptr,#_AX5043_PKTADDRMASK1
      00234E F0               [24] 5213 	movx	@dptr,a
                                   5214 ;	..\src\COMMON\easyax5043.c:945: AX5043_PKTADDRMASK2 = axradio_localaddr.mask[2];
      00234F 90 00 D7         [24] 5215 	mov	dptr,#(_axradio_localaddr + 0x0006)
      002352 E0               [24] 5216 	movx	a,@dptr
      002353 90 42 09         [24] 5217 	mov	dptr,#_AX5043_PKTADDRMASK2
      002356 F0               [24] 5218 	movx	@dptr,a
                                   5219 ;	..\src\COMMON\easyax5043.c:946: AX5043_PKTADDRMASK3 = axradio_localaddr.mask[3];
      002357 90 00 D8         [24] 5220 	mov	dptr,#(_axradio_localaddr + 0x0007)
      00235A E0               [24] 5221 	movx	a,@dptr
      00235B FF               [12] 5222 	mov	r7,a
      00235C 90 42 08         [24] 5223 	mov	dptr,#_AX5043_PKTADDRMASK3
      00235F F0               [24] 5224 	movx	@dptr,a
                                   5225 ;	..\src\COMMON\easyax5043.c:948: if (axradio_phy_pn9 && axradio_framing_addrlen) {
      002360 90 7A B9         [24] 5226 	mov	dptr,#_axradio_phy_pn9
      002363 E4               [12] 5227 	clr	a
      002364 93               [24] 5228 	movc	a,@a+dptr
      002365 70 01            [24] 5229 	jnz	00117$
      002367 22               [24] 5230 	ret
      002368                       5231 00117$:
      002368 90 7A DB         [24] 5232 	mov	dptr,#_axradio_framing_addrlen
      00236B E4               [12] 5233 	clr	a
      00236C 93               [24] 5234 	movc	a,@a+dptr
      00236D 70 01            [24] 5235 	jnz	00118$
      00236F 22               [24] 5236 	ret
      002370                       5237 00118$:
                                   5238 ;	..\src\COMMON\easyax5043.c:949: uint16_t __autodata pn = 0x1ff;
      002370 7E FF            [12] 5239 	mov	r6,#0xff
      002372 7F 01            [12] 5240 	mov	r7,#0x01
                                   5241 ;	..\src\COMMON\easyax5043.c:950: uint8_t __autodata inv = -(AX5043_ENCODING & 0x01);
      002374 90 40 11         [24] 5242 	mov	dptr,#_AX5043_ENCODING
      002377 E0               [24] 5243 	movx	a,@dptr
      002378 FD               [12] 5244 	mov	r5,a
      002379 53 05 01         [24] 5245 	anl	ar5,#0x01
      00237C C3               [12] 5246 	clr	c
      00237D E4               [12] 5247 	clr	a
      00237E 9D               [12] 5248 	subb	a,r5
      00237F FD               [12] 5249 	mov	r5,a
                                   5250 ;	..\src\COMMON\easyax5043.c:951: if (axradio_framing_destaddrpos != 0xff)
      002380 90 7A DC         [24] 5251 	mov	dptr,#_axradio_framing_destaddrpos
      002383 E4               [12] 5252 	clr	a
      002384 93               [24] 5253 	movc	a,@a+dptr
      002385 FC               [12] 5254 	mov	r4,a
      002386 BC FF 02         [24] 5255 	cjne	r4,#0xff,00119$
      002389 80 25            [24] 5256 	sjmp	00102$
      00238B                       5257 00119$:
                                   5258 ;	..\src\COMMON\easyax5043.c:952: pn = pn9_advance_bits(pn, axradio_framing_destaddrpos << 3);
      00238B E4               [12] 5259 	clr	a
      00238C 03               [12] 5260 	rr	a
      00238D 54 F8            [12] 5261 	anl	a,#0xf8
      00238F CC               [12] 5262 	xch	a,r4
      002390 C4               [12] 5263 	swap	a
      002391 03               [12] 5264 	rr	a
      002392 CC               [12] 5265 	xch	a,r4
      002393 6C               [12] 5266 	xrl	a,r4
      002394 CC               [12] 5267 	xch	a,r4
      002395 54 F8            [12] 5268 	anl	a,#0xf8
      002397 CC               [12] 5269 	xch	a,r4
      002398 6C               [12] 5270 	xrl	a,r4
      002399 FB               [12] 5271 	mov	r3,a
      00239A C0 05            [24] 5272 	push	ar5
      00239C C0 04            [24] 5273 	push	ar4
      00239E C0 03            [24] 5274 	push	ar3
      0023A0 90 01 FF         [24] 5275 	mov	dptr,#0x01ff
      0023A3 12 77 40         [24] 5276 	lcall	_pn9_advance_bits
      0023A6 AE 82            [24] 5277 	mov	r6,dpl
      0023A8 AF 83            [24] 5278 	mov	r7,dph
      0023AA 15 81            [12] 5279 	dec	sp
      0023AC 15 81            [12] 5280 	dec	sp
      0023AE D0 05            [24] 5281 	pop	ar5
      0023B0                       5282 00102$:
                                   5283 ;	..\src\COMMON\easyax5043.c:953: AX5043_PKTADDR0 ^= pn ^ inv;
      0023B0 7C 00            [12] 5284 	mov	r4,#0x00
      0023B2 ED               [12] 5285 	mov	a,r5
      0023B3 6E               [12] 5286 	xrl	a,r6
      0023B4 FA               [12] 5287 	mov	r2,a
      0023B5 EC               [12] 5288 	mov	a,r4
      0023B6 6F               [12] 5289 	xrl	a,r7
      0023B7 FB               [12] 5290 	mov	r3,a
      0023B8 90 42 07         [24] 5291 	mov	dptr,#_AX5043_PKTADDR0
      0023BB E0               [24] 5292 	movx	a,@dptr
      0023BC 79 00            [12] 5293 	mov	r1,#0x00
      0023BE 62 02            [12] 5294 	xrl	ar2,a
      0023C0 E9               [12] 5295 	mov	a,r1
      0023C1 62 03            [12] 5296 	xrl	ar3,a
      0023C3 90 42 07         [24] 5297 	mov	dptr,#_AX5043_PKTADDR0
      0023C6 EA               [12] 5298 	mov	a,r2
      0023C7 F0               [24] 5299 	movx	@dptr,a
                                   5300 ;	..\src\COMMON\easyax5043.c:954: pn = pn9_advance_byte(pn);
      0023C8 8E 82            [24] 5301 	mov	dpl,r6
      0023CA 8F 83            [24] 5302 	mov	dph,r7
      0023CC C0 05            [24] 5303 	push	ar5
      0023CE C0 04            [24] 5304 	push	ar4
      0023D0 12 78 29         [24] 5305 	lcall	_pn9_advance_byte
      0023D3 AE 82            [24] 5306 	mov	r6,dpl
      0023D5 AF 83            [24] 5307 	mov	r7,dph
      0023D7 D0 04            [24] 5308 	pop	ar4
      0023D9 D0 05            [24] 5309 	pop	ar5
                                   5310 ;	..\src\COMMON\easyax5043.c:955: AX5043_PKTADDR1 ^= pn ^ inv;
      0023DB ED               [12] 5311 	mov	a,r5
      0023DC 6E               [12] 5312 	xrl	a,r6
      0023DD FA               [12] 5313 	mov	r2,a
      0023DE EC               [12] 5314 	mov	a,r4
      0023DF 6F               [12] 5315 	xrl	a,r7
      0023E0 FB               [12] 5316 	mov	r3,a
      0023E1 90 42 06         [24] 5317 	mov	dptr,#_AX5043_PKTADDR1
      0023E4 E0               [24] 5318 	movx	a,@dptr
      0023E5 79 00            [12] 5319 	mov	r1,#0x00
      0023E7 62 02            [12] 5320 	xrl	ar2,a
      0023E9 E9               [12] 5321 	mov	a,r1
      0023EA 62 03            [12] 5322 	xrl	ar3,a
      0023EC 90 42 06         [24] 5323 	mov	dptr,#_AX5043_PKTADDR1
      0023EF EA               [12] 5324 	mov	a,r2
      0023F0 F0               [24] 5325 	movx	@dptr,a
                                   5326 ;	..\src\COMMON\easyax5043.c:956: pn = pn9_advance_byte(pn);
      0023F1 8E 82            [24] 5327 	mov	dpl,r6
      0023F3 8F 83            [24] 5328 	mov	dph,r7
      0023F5 C0 05            [24] 5329 	push	ar5
      0023F7 C0 04            [24] 5330 	push	ar4
      0023F9 12 78 29         [24] 5331 	lcall	_pn9_advance_byte
      0023FC AE 82            [24] 5332 	mov	r6,dpl
      0023FE AF 83            [24] 5333 	mov	r7,dph
      002400 D0 04            [24] 5334 	pop	ar4
      002402 D0 05            [24] 5335 	pop	ar5
                                   5336 ;	..\src\COMMON\easyax5043.c:957: AX5043_PKTADDR2 ^= pn ^ inv;
      002404 ED               [12] 5337 	mov	a,r5
      002405 6E               [12] 5338 	xrl	a,r6
      002406 FA               [12] 5339 	mov	r2,a
      002407 EC               [12] 5340 	mov	a,r4
      002408 6F               [12] 5341 	xrl	a,r7
      002409 FB               [12] 5342 	mov	r3,a
      00240A 90 42 05         [24] 5343 	mov	dptr,#_AX5043_PKTADDR2
      00240D E0               [24] 5344 	movx	a,@dptr
      00240E 79 00            [12] 5345 	mov	r1,#0x00
      002410 62 02            [12] 5346 	xrl	ar2,a
      002412 E9               [12] 5347 	mov	a,r1
      002413 62 03            [12] 5348 	xrl	ar3,a
      002415 90 42 05         [24] 5349 	mov	dptr,#_AX5043_PKTADDR2
      002418 EA               [12] 5350 	mov	a,r2
      002419 F0               [24] 5351 	movx	@dptr,a
                                   5352 ;	..\src\COMMON\easyax5043.c:958: pn = pn9_advance_byte(pn);
      00241A 8E 82            [24] 5353 	mov	dpl,r6
      00241C 8F 83            [24] 5354 	mov	dph,r7
      00241E C0 05            [24] 5355 	push	ar5
      002420 C0 04            [24] 5356 	push	ar4
      002422 12 78 29         [24] 5357 	lcall	_pn9_advance_byte
      002425 AE 82            [24] 5358 	mov	r6,dpl
      002427 AF 83            [24] 5359 	mov	r7,dph
      002429 D0 04            [24] 5360 	pop	ar4
      00242B D0 05            [24] 5361 	pop	ar5
                                   5362 ;	..\src\COMMON\easyax5043.c:959: AX5043_PKTADDR3 ^= pn ^ inv;
      00242D ED               [12] 5363 	mov	a,r5
      00242E 62 06            [12] 5364 	xrl	ar6,a
      002430 EC               [12] 5365 	mov	a,r4
      002431 62 07            [12] 5366 	xrl	ar7,a
      002433 90 42 04         [24] 5367 	mov	dptr,#_AX5043_PKTADDR3
      002436 E0               [24] 5368 	movx	a,@dptr
      002437 7C 00            [12] 5369 	mov	r4,#0x00
      002439 62 06            [12] 5370 	xrl	ar6,a
      00243B EC               [12] 5371 	mov	a,r4
      00243C 62 07            [12] 5372 	xrl	ar7,a
      00243E 90 42 04         [24] 5373 	mov	dptr,#_AX5043_PKTADDR3
      002441 EE               [12] 5374 	mov	a,r6
      002442 F0               [24] 5375 	movx	@dptr,a
      002443 22               [24] 5376 	ret
                                   5377 ;------------------------------------------------------------
                                   5378 ;Allocation info for local variables in function 'ax5043_init_registers'
                                   5379 ;------------------------------------------------------------
                                   5380 ;	..\src\COMMON\easyax5043.c:963: static void ax5043_init_registers(void)
                                   5381 ;	-----------------------------------------
                                   5382 ;	 function ax5043_init_registers
                                   5383 ;	-----------------------------------------
      002444                       5384 _ax5043_init_registers:
                                   5385 ;	..\src\COMMON\easyax5043.c:965: ax5043_set_registers();
      002444 12 03 9A         [24] 5386 	lcall	_ax5043_set_registers
                                   5387 ;	..\src\COMMON\easyax5043.c:970: AX5043_PKTLENOFFSET += axradio_framing_swcrclen; // add len offs for software CRC16 (used for both, fixed and variable length packets
      002447 90 7A E1         [24] 5388 	mov	dptr,#_axradio_framing_swcrclen
      00244A E4               [12] 5389 	clr	a
      00244B 93               [24] 5390 	movc	a,@a+dptr
      00244C FF               [12] 5391 	mov	r7,a
      00244D 90 42 02         [24] 5392 	mov	dptr,#_AX5043_PKTLENOFFSET
      002450 E0               [24] 5393 	movx	a,@dptr
      002451 FE               [12] 5394 	mov	r6,a
      002452 2F               [12] 5395 	add	a,r7
      002453 F0               [24] 5396 	movx	@dptr,a
                                   5397 ;	..\src\COMMON\easyax5043.c:971: AX5043_PINFUNCIRQ = 0x03; // use as IRQ pin
      002454 90 40 24         [24] 5398 	mov	dptr,#_AX5043_PINFUNCIRQ
      002457 74 03            [12] 5399 	mov	a,#0x03
      002459 F0               [24] 5400 	movx	@dptr,a
                                   5401 ;	..\src\COMMON\easyax5043.c:972: AX5043_PKTSTOREFLAGS = axradio_phy_innerfreqloop ? 0x13 : 0x15; // store RF offset, RSSI and delimiter timing
      00245A 90 7A B8         [24] 5402 	mov	dptr,#_axradio_phy_innerfreqloop
      00245D E4               [12] 5403 	clr	a
      00245E 93               [24] 5404 	movc	a,@a+dptr
      00245F FF               [12] 5405 	mov	r7,a
      002460 60 04            [24] 5406 	jz	00103$
      002462 7F 13            [12] 5407 	mov	r7,#0x13
      002464 80 02            [24] 5408 	sjmp	00104$
      002466                       5409 00103$:
      002466 7F 15            [12] 5410 	mov	r7,#0x15
      002468                       5411 00104$:
      002468 90 42 32         [24] 5412 	mov	dptr,#_AX5043_PKTSTOREFLAGS
      00246B EF               [12] 5413 	mov	a,r7
      00246C F0               [24] 5414 	movx	@dptr,a
                                   5415 ;	..\src\COMMON\easyax5043.c:973: axradio_setaddrregs();
      00246D 02 23 1F         [24] 5416 	ljmp	_axradio_setaddrregs
                                   5417 ;------------------------------------------------------------
                                   5418 ;Allocation info for local variables in function 'axradio_sync_addtime'
                                   5419 ;------------------------------------------------------------
                                   5420 ;dt                        Allocated to registers r4 r5 r6 r7 
                                   5421 ;------------------------------------------------------------
                                   5422 ;	..\src\COMMON\easyax5043.c:980: static __reentrantb void axradio_sync_addtime(uint32_t dt) __reentrant
                                   5423 ;	-----------------------------------------
                                   5424 ;	 function axradio_sync_addtime
                                   5425 ;	-----------------------------------------
      002470                       5426 _axradio_sync_addtime:
      002470 AC 82            [24] 5427 	mov	r4,dpl
      002472 AD 83            [24] 5428 	mov	r5,dph
      002474 AE F0            [24] 5429 	mov	r6,b
      002476 FF               [12] 5430 	mov	r7,a
                                   5431 ;	..\src\COMMON\easyax5043.c:982: axradio_sync_time += dt;
      002477 90 00 C3         [24] 5432 	mov	dptr,#_axradio_sync_time
      00247A E0               [24] 5433 	movx	a,@dptr
      00247B F8               [12] 5434 	mov	r0,a
      00247C A3               [24] 5435 	inc	dptr
      00247D E0               [24] 5436 	movx	a,@dptr
      00247E F9               [12] 5437 	mov	r1,a
      00247F A3               [24] 5438 	inc	dptr
      002480 E0               [24] 5439 	movx	a,@dptr
      002481 FA               [12] 5440 	mov	r2,a
      002482 A3               [24] 5441 	inc	dptr
      002483 E0               [24] 5442 	movx	a,@dptr
      002484 FB               [12] 5443 	mov	r3,a
      002485 90 00 C3         [24] 5444 	mov	dptr,#_axradio_sync_time
      002488 EC               [12] 5445 	mov	a,r4
      002489 28               [12] 5446 	add	a,r0
      00248A F0               [24] 5447 	movx	@dptr,a
      00248B ED               [12] 5448 	mov	a,r5
      00248C 39               [12] 5449 	addc	a,r1
      00248D A3               [24] 5450 	inc	dptr
      00248E F0               [24] 5451 	movx	@dptr,a
      00248F EE               [12] 5452 	mov	a,r6
      002490 3A               [12] 5453 	addc	a,r2
      002491 A3               [24] 5454 	inc	dptr
      002492 F0               [24] 5455 	movx	@dptr,a
      002493 EF               [12] 5456 	mov	a,r7
      002494 3B               [12] 5457 	addc	a,r3
      002495 A3               [24] 5458 	inc	dptr
      002496 F0               [24] 5459 	movx	@dptr,a
      002497 22               [24] 5460 	ret
                                   5461 ;------------------------------------------------------------
                                   5462 ;Allocation info for local variables in function 'axradio_sync_subtime'
                                   5463 ;------------------------------------------------------------
                                   5464 ;dt                        Allocated to registers r4 r5 r6 r7 
                                   5465 ;------------------------------------------------------------
                                   5466 ;	..\src\COMMON\easyax5043.c:985: static __reentrantb void axradio_sync_subtime(uint32_t dt) __reentrant
                                   5467 ;	-----------------------------------------
                                   5468 ;	 function axradio_sync_subtime
                                   5469 ;	-----------------------------------------
      002498                       5470 _axradio_sync_subtime:
      002498 AC 82            [24] 5471 	mov	r4,dpl
      00249A AD 83            [24] 5472 	mov	r5,dph
      00249C AE F0            [24] 5473 	mov	r6,b
      00249E FF               [12] 5474 	mov	r7,a
                                   5475 ;	..\src\COMMON\easyax5043.c:987: axradio_sync_time -= dt;
      00249F 90 00 C3         [24] 5476 	mov	dptr,#_axradio_sync_time
      0024A2 E0               [24] 5477 	movx	a,@dptr
      0024A3 F8               [12] 5478 	mov	r0,a
      0024A4 A3               [24] 5479 	inc	dptr
      0024A5 E0               [24] 5480 	movx	a,@dptr
      0024A6 F9               [12] 5481 	mov	r1,a
      0024A7 A3               [24] 5482 	inc	dptr
      0024A8 E0               [24] 5483 	movx	a,@dptr
      0024A9 FA               [12] 5484 	mov	r2,a
      0024AA A3               [24] 5485 	inc	dptr
      0024AB E0               [24] 5486 	movx	a,@dptr
      0024AC FB               [12] 5487 	mov	r3,a
      0024AD 90 00 C3         [24] 5488 	mov	dptr,#_axradio_sync_time
      0024B0 E8               [12] 5489 	mov	a,r0
      0024B1 C3               [12] 5490 	clr	c
      0024B2 9C               [12] 5491 	subb	a,r4
      0024B3 F0               [24] 5492 	movx	@dptr,a
      0024B4 E9               [12] 5493 	mov	a,r1
      0024B5 9D               [12] 5494 	subb	a,r5
      0024B6 A3               [24] 5495 	inc	dptr
      0024B7 F0               [24] 5496 	movx	@dptr,a
      0024B8 EA               [12] 5497 	mov	a,r2
      0024B9 9E               [12] 5498 	subb	a,r6
      0024BA A3               [24] 5499 	inc	dptr
      0024BB F0               [24] 5500 	movx	@dptr,a
      0024BC EB               [12] 5501 	mov	a,r3
      0024BD 9F               [12] 5502 	subb	a,r7
      0024BE A3               [24] 5503 	inc	dptr
      0024BF F0               [24] 5504 	movx	@dptr,a
      0024C0 22               [24] 5505 	ret
                                   5506 ;------------------------------------------------------------
                                   5507 ;Allocation info for local variables in function 'axradio_sync_settimeradv'
                                   5508 ;------------------------------------------------------------
                                   5509 ;dt                        Allocated to registers r4 r5 r6 r7 
                                   5510 ;------------------------------------------------------------
                                   5511 ;	..\src\COMMON\easyax5043.c:990: static __reentrantb void axradio_sync_settimeradv(uint32_t dt) __reentrant
                                   5512 ;	-----------------------------------------
                                   5513 ;	 function axradio_sync_settimeradv
                                   5514 ;	-----------------------------------------
      0024C1                       5515 _axradio_sync_settimeradv:
      0024C1 AC 82            [24] 5516 	mov	r4,dpl
      0024C3 AD 83            [24] 5517 	mov	r5,dph
      0024C5 AE F0            [24] 5518 	mov	r6,b
      0024C7 FF               [12] 5519 	mov	r7,a
                                   5520 ;	..\src\COMMON\easyax5043.c:992: axradio_timer.time = axradio_sync_time;
      0024C8 90 00 C3         [24] 5521 	mov	dptr,#_axradio_sync_time
      0024CB E0               [24] 5522 	movx	a,@dptr
      0024CC F8               [12] 5523 	mov	r0,a
      0024CD A3               [24] 5524 	inc	dptr
      0024CE E0               [24] 5525 	movx	a,@dptr
      0024CF F9               [12] 5526 	mov	r1,a
      0024D0 A3               [24] 5527 	inc	dptr
      0024D1 E0               [24] 5528 	movx	a,@dptr
      0024D2 FA               [12] 5529 	mov	r2,a
      0024D3 A3               [24] 5530 	inc	dptr
      0024D4 E0               [24] 5531 	movx	a,@dptr
      0024D5 FB               [12] 5532 	mov	r3,a
      0024D6 90 03 40         [24] 5533 	mov	dptr,#(_axradio_timer + 0x0004)
      0024D9 E8               [12] 5534 	mov	a,r0
      0024DA F0               [24] 5535 	movx	@dptr,a
      0024DB E9               [12] 5536 	mov	a,r1
      0024DC A3               [24] 5537 	inc	dptr
      0024DD F0               [24] 5538 	movx	@dptr,a
      0024DE EA               [12] 5539 	mov	a,r2
      0024DF A3               [24] 5540 	inc	dptr
      0024E0 F0               [24] 5541 	movx	@dptr,a
      0024E1 EB               [12] 5542 	mov	a,r3
      0024E2 A3               [24] 5543 	inc	dptr
      0024E3 F0               [24] 5544 	movx	@dptr,a
                                   5545 ;	..\src\COMMON\easyax5043.c:993: axradio_timer.time -= dt;
      0024E4 E8               [12] 5546 	mov	a,r0
      0024E5 C3               [12] 5547 	clr	c
      0024E6 9C               [12] 5548 	subb	a,r4
      0024E7 FC               [12] 5549 	mov	r4,a
      0024E8 E9               [12] 5550 	mov	a,r1
      0024E9 9D               [12] 5551 	subb	a,r5
      0024EA FD               [12] 5552 	mov	r5,a
      0024EB EA               [12] 5553 	mov	a,r2
      0024EC 9E               [12] 5554 	subb	a,r6
      0024ED FE               [12] 5555 	mov	r6,a
      0024EE EB               [12] 5556 	mov	a,r3
      0024EF 9F               [12] 5557 	subb	a,r7
      0024F0 FF               [12] 5558 	mov	r7,a
      0024F1 90 03 40         [24] 5559 	mov	dptr,#(_axradio_timer + 0x0004)
      0024F4 EC               [12] 5560 	mov	a,r4
      0024F5 F0               [24] 5561 	movx	@dptr,a
      0024F6 ED               [12] 5562 	mov	a,r5
      0024F7 A3               [24] 5563 	inc	dptr
      0024F8 F0               [24] 5564 	movx	@dptr,a
      0024F9 EE               [12] 5565 	mov	a,r6
      0024FA A3               [24] 5566 	inc	dptr
      0024FB F0               [24] 5567 	movx	@dptr,a
      0024FC EF               [12] 5568 	mov	a,r7
      0024FD A3               [24] 5569 	inc	dptr
      0024FE F0               [24] 5570 	movx	@dptr,a
      0024FF 22               [24] 5571 	ret
                                   5572 ;------------------------------------------------------------
                                   5573 ;Allocation info for local variables in function 'axradio_sync_adjustperiodcorr'
                                   5574 ;------------------------------------------------------------
                                   5575 ;dt                        Allocated to registers r4 r5 r6 r7 
                                   5576 ;------------------------------------------------------------
                                   5577 ;	..\src\COMMON\easyax5043.c:996: static void axradio_sync_adjustperiodcorr(void)
                                   5578 ;	-----------------------------------------
                                   5579 ;	 function axradio_sync_adjustperiodcorr
                                   5580 ;	-----------------------------------------
      002500                       5581 _axradio_sync_adjustperiodcorr:
                                   5582 ;	..\src\COMMON\easyax5043.c:998: int32_t __autodata dt = axradio_conv_time_totimer0(axradio_cb_receive.st.time.t) - axradio_sync_time;
      002500 90 02 EB         [24] 5583 	mov	dptr,#(_axradio_cb_receive + 0x0006)
      002503 E0               [24] 5584 	movx	a,@dptr
      002504 FC               [12] 5585 	mov	r4,a
      002505 A3               [24] 5586 	inc	dptr
      002506 E0               [24] 5587 	movx	a,@dptr
      002507 FD               [12] 5588 	mov	r5,a
      002508 A3               [24] 5589 	inc	dptr
      002509 E0               [24] 5590 	movx	a,@dptr
      00250A FE               [12] 5591 	mov	r6,a
      00250B A3               [24] 5592 	inc	dptr
      00250C E0               [24] 5593 	movx	a,@dptr
      00250D 8C 82            [24] 5594 	mov	dpl,r4
      00250F 8D 83            [24] 5595 	mov	dph,r5
      002511 8E F0            [24] 5596 	mov	b,r6
      002513 12 16 01         [24] 5597 	lcall	_axradio_conv_time_totimer0
      002516 AC 82            [24] 5598 	mov	r4,dpl
      002518 AD 83            [24] 5599 	mov	r5,dph
      00251A AE F0            [24] 5600 	mov	r6,b
      00251C FF               [12] 5601 	mov	r7,a
      00251D 90 00 C3         [24] 5602 	mov	dptr,#_axradio_sync_time
      002520 E0               [24] 5603 	movx	a,@dptr
      002521 F8               [12] 5604 	mov	r0,a
      002522 A3               [24] 5605 	inc	dptr
      002523 E0               [24] 5606 	movx	a,@dptr
      002524 F9               [12] 5607 	mov	r1,a
      002525 A3               [24] 5608 	inc	dptr
      002526 E0               [24] 5609 	movx	a,@dptr
      002527 FA               [12] 5610 	mov	r2,a
      002528 A3               [24] 5611 	inc	dptr
      002529 E0               [24] 5612 	movx	a,@dptr
      00252A FB               [12] 5613 	mov	r3,a
      00252B EC               [12] 5614 	mov	a,r4
      00252C C3               [12] 5615 	clr	c
      00252D 98               [12] 5616 	subb	a,r0
      00252E FC               [12] 5617 	mov	r4,a
      00252F ED               [12] 5618 	mov	a,r5
      002530 99               [12] 5619 	subb	a,r1
      002531 FD               [12] 5620 	mov	r5,a
      002532 EE               [12] 5621 	mov	a,r6
      002533 9A               [12] 5622 	subb	a,r2
      002534 FE               [12] 5623 	mov	r6,a
      002535 EF               [12] 5624 	mov	a,r7
      002536 9B               [12] 5625 	subb	a,r3
      002537 FF               [12] 5626 	mov	r7,a
                                   5627 ;	..\src\COMMON\easyax5043.c:999: axradio_cb_receive.st.rx.phy.timeoffset = dt;
      002538 8C 02            [24] 5628 	mov	ar2,r4
      00253A 8D 03            [24] 5629 	mov	ar3,r5
      00253C 90 02 F5         [24] 5630 	mov	dptr,#(_axradio_cb_receive + 0x0010)
      00253F EA               [12] 5631 	mov	a,r2
      002540 F0               [24] 5632 	movx	@dptr,a
      002541 EB               [12] 5633 	mov	a,r3
      002542 A3               [24] 5634 	inc	dptr
      002543 F0               [24] 5635 	movx	@dptr,a
                                   5636 ;	..\src\COMMON\easyax5043.c:1000: if (!checksignedlimit16(axradio_sync_periodcorr, axradio_sync_slave_maxperiod)) {
      002544 90 00 C7         [24] 5637 	mov	dptr,#_axradio_sync_periodcorr
      002547 E0               [24] 5638 	movx	a,@dptr
      002548 FA               [12] 5639 	mov	r2,a
      002549 A3               [24] 5640 	inc	dptr
      00254A E0               [24] 5641 	movx	a,@dptr
      00254B FB               [12] 5642 	mov	r3,a
      00254C 90 7B 0A         [24] 5643 	mov	dptr,#_axradio_sync_slave_maxperiod
      00254F E4               [12] 5644 	clr	a
      002550 93               [24] 5645 	movc	a,@a+dptr
      002551 C0 E0            [24] 5646 	push	acc
      002553 74 01            [12] 5647 	mov	a,#0x01
      002555 93               [24] 5648 	movc	a,@a+dptr
      002556 C0 E0            [24] 5649 	push	acc
      002558 8A 82            [24] 5650 	mov	dpl,r2
      00255A 8B 83            [24] 5651 	mov	dph,r3
      00255C 12 6E D3         [24] 5652 	lcall	_checksignedlimit16
      00255F AB 82            [24] 5653 	mov	r3,dpl
      002561 15 81            [12] 5654 	dec	sp
      002563 15 81            [12] 5655 	dec	sp
      002565 EB               [12] 5656 	mov	a,r3
      002566 70 4B            [24] 5657 	jnz	00102$
                                   5658 ;	..\src\COMMON\easyax5043.c:1001: axradio_sync_addtime(dt);
      002568 8C 82            [24] 5659 	mov	dpl,r4
      00256A 8D 83            [24] 5660 	mov	dph,r5
      00256C 8E F0            [24] 5661 	mov	b,r6
      00256E EF               [12] 5662 	mov	a,r7
      00256F C0 07            [24] 5663 	push	ar7
      002571 C0 06            [24] 5664 	push	ar6
      002573 C0 05            [24] 5665 	push	ar5
      002575 C0 04            [24] 5666 	push	ar4
      002577 12 24 70         [24] 5667 	lcall	_axradio_sync_addtime
      00257A D0 04            [24] 5668 	pop	ar4
      00257C D0 05            [24] 5669 	pop	ar5
      00257E D0 06            [24] 5670 	pop	ar6
      002580 D0 07            [24] 5671 	pop	ar7
                                   5672 ;	..\src\COMMON\easyax5043.c:1002: dt <<= SYNC_K1;
      002582 EF               [12] 5673 	mov	a,r7
      002583 C4               [12] 5674 	swap	a
      002584 23               [12] 5675 	rl	a
      002585 54 E0            [12] 5676 	anl	a,#0xe0
      002587 CE               [12] 5677 	xch	a,r6
      002588 C4               [12] 5678 	swap	a
      002589 23               [12] 5679 	rl	a
      00258A CE               [12] 5680 	xch	a,r6
      00258B 6E               [12] 5681 	xrl	a,r6
      00258C CE               [12] 5682 	xch	a,r6
      00258D 54 E0            [12] 5683 	anl	a,#0xe0
      00258F CE               [12] 5684 	xch	a,r6
      002590 6E               [12] 5685 	xrl	a,r6
      002591 FF               [12] 5686 	mov	r7,a
      002592 ED               [12] 5687 	mov	a,r5
      002593 C4               [12] 5688 	swap	a
      002594 23               [12] 5689 	rl	a
      002595 54 1F            [12] 5690 	anl	a,#0x1f
      002597 4E               [12] 5691 	orl	a,r6
      002598 FE               [12] 5692 	mov	r6,a
      002599 ED               [12] 5693 	mov	a,r5
      00259A C4               [12] 5694 	swap	a
      00259B 23               [12] 5695 	rl	a
      00259C 54 E0            [12] 5696 	anl	a,#0xe0
      00259E CC               [12] 5697 	xch	a,r4
      00259F C4               [12] 5698 	swap	a
      0025A0 23               [12] 5699 	rl	a
      0025A1 CC               [12] 5700 	xch	a,r4
      0025A2 6C               [12] 5701 	xrl	a,r4
      0025A3 CC               [12] 5702 	xch	a,r4
      0025A4 54 E0            [12] 5703 	anl	a,#0xe0
      0025A6 CC               [12] 5704 	xch	a,r4
      0025A7 6C               [12] 5705 	xrl	a,r4
      0025A8 FD               [12] 5706 	mov	r5,a
                                   5707 ;	..\src\COMMON\easyax5043.c:1003: axradio_sync_periodcorr = dt;
      0025A9 90 00 C7         [24] 5708 	mov	dptr,#_axradio_sync_periodcorr
      0025AC EC               [12] 5709 	mov	a,r4
      0025AD F0               [24] 5710 	movx	@dptr,a
      0025AE ED               [12] 5711 	mov	a,r5
      0025AF A3               [24] 5712 	inc	dptr
      0025B0 F0               [24] 5713 	movx	@dptr,a
      0025B1 80 48            [24] 5714 	sjmp	00103$
      0025B3                       5715 00102$:
                                   5716 ;	..\src\COMMON\easyax5043.c:1005: axradio_sync_periodcorr += dt;
      0025B3 90 00 C7         [24] 5717 	mov	dptr,#_axradio_sync_periodcorr
      0025B6 E0               [24] 5718 	movx	a,@dptr
      0025B7 FA               [12] 5719 	mov	r2,a
      0025B8 A3               [24] 5720 	inc	dptr
      0025B9 E0               [24] 5721 	movx	a,@dptr
      0025BA FB               [12] 5722 	mov	r3,a
      0025BB 8A 00            [24] 5723 	mov	ar0,r2
      0025BD EB               [12] 5724 	mov	a,r3
      0025BE F9               [12] 5725 	mov	r1,a
      0025BF 33               [12] 5726 	rlc	a
      0025C0 95 E0            [12] 5727 	subb	a,acc
      0025C2 FA               [12] 5728 	mov	r2,a
      0025C3 FB               [12] 5729 	mov	r3,a
      0025C4 EC               [12] 5730 	mov	a,r4
      0025C5 28               [12] 5731 	add	a,r0
      0025C6 F8               [12] 5732 	mov	r0,a
      0025C7 ED               [12] 5733 	mov	a,r5
      0025C8 39               [12] 5734 	addc	a,r1
      0025C9 F9               [12] 5735 	mov	r1,a
      0025CA EE               [12] 5736 	mov	a,r6
      0025CB 3A               [12] 5737 	addc	a,r2
      0025CC EF               [12] 5738 	mov	a,r7
      0025CD 3B               [12] 5739 	addc	a,r3
      0025CE 90 00 C7         [24] 5740 	mov	dptr,#_axradio_sync_periodcorr
      0025D1 E8               [12] 5741 	mov	a,r0
      0025D2 F0               [24] 5742 	movx	@dptr,a
      0025D3 E9               [12] 5743 	mov	a,r1
      0025D4 A3               [24] 5744 	inc	dptr
      0025D5 F0               [24] 5745 	movx	@dptr,a
                                   5746 ;	..\src\COMMON\easyax5043.c:1006: dt >>= SYNC_K0;
      0025D6 EF               [12] 5747 	mov	a,r7
      0025D7 A2 E7            [12] 5748 	mov	c,acc.7
      0025D9 13               [12] 5749 	rrc	a
      0025DA FF               [12] 5750 	mov	r7,a
      0025DB EE               [12] 5751 	mov	a,r6
      0025DC 13               [12] 5752 	rrc	a
      0025DD FE               [12] 5753 	mov	r6,a
      0025DE ED               [12] 5754 	mov	a,r5
      0025DF 13               [12] 5755 	rrc	a
      0025E0 FD               [12] 5756 	mov	r5,a
      0025E1 EC               [12] 5757 	mov	a,r4
      0025E2 13               [12] 5758 	rrc	a
      0025E3 FC               [12] 5759 	mov	r4,a
      0025E4 EF               [12] 5760 	mov	a,r7
      0025E5 A2 E7            [12] 5761 	mov	c,acc.7
      0025E7 13               [12] 5762 	rrc	a
      0025E8 FF               [12] 5763 	mov	r7,a
      0025E9 EE               [12] 5764 	mov	a,r6
      0025EA 13               [12] 5765 	rrc	a
      0025EB FE               [12] 5766 	mov	r6,a
      0025EC ED               [12] 5767 	mov	a,r5
      0025ED 13               [12] 5768 	rrc	a
      0025EE FD               [12] 5769 	mov	r5,a
      0025EF EC               [12] 5770 	mov	a,r4
      0025F0 13               [12] 5771 	rrc	a
                                   5772 ;	..\src\COMMON\easyax5043.c:1007: axradio_sync_addtime(dt);
      0025F1 F5 82            [12] 5773 	mov	dpl,a
      0025F3 8D 83            [24] 5774 	mov	dph,r5
      0025F5 8E F0            [24] 5775 	mov	b,r6
      0025F7 EF               [12] 5776 	mov	a,r7
      0025F8 12 24 70         [24] 5777 	lcall	_axradio_sync_addtime
      0025FB                       5778 00103$:
                                   5779 ;	..\src\COMMON\easyax5043.c:1009: axradio_sync_periodcorr = signedlimit16(axradio_sync_periodcorr, axradio_sync_slave_maxperiod);
      0025FB 90 00 C7         [24] 5780 	mov	dptr,#_axradio_sync_periodcorr
      0025FE E0               [24] 5781 	movx	a,@dptr
      0025FF FE               [12] 5782 	mov	r6,a
      002600 A3               [24] 5783 	inc	dptr
      002601 E0               [24] 5784 	movx	a,@dptr
      002602 FF               [12] 5785 	mov	r7,a
      002603 90 7B 0A         [24] 5786 	mov	dptr,#_axradio_sync_slave_maxperiod
      002606 E4               [12] 5787 	clr	a
      002607 93               [24] 5788 	movc	a,@a+dptr
      002608 C0 E0            [24] 5789 	push	acc
      00260A 74 01            [12] 5790 	mov	a,#0x01
      00260C 93               [24] 5791 	movc	a,@a+dptr
      00260D C0 E0            [24] 5792 	push	acc
      00260F 8E 82            [24] 5793 	mov	dpl,r6
      002611 8F 83            [24] 5794 	mov	dph,r7
      002613 12 70 A6         [24] 5795 	lcall	_signedlimit16
      002616 AE 82            [24] 5796 	mov	r6,dpl
      002618 AF 83            [24] 5797 	mov	r7,dph
      00261A 15 81            [12] 5798 	dec	sp
      00261C 15 81            [12] 5799 	dec	sp
      00261E 90 00 C7         [24] 5800 	mov	dptr,#_axradio_sync_periodcorr
      002621 EE               [12] 5801 	mov	a,r6
      002622 F0               [24] 5802 	movx	@dptr,a
      002623 EF               [12] 5803 	mov	a,r7
      002624 A3               [24] 5804 	inc	dptr
      002625 F0               [24] 5805 	movx	@dptr,a
      002626 22               [24] 5806 	ret
                                   5807 ;------------------------------------------------------------
                                   5808 ;Allocation info for local variables in function 'axradio_sync_slave_nextperiod'
                                   5809 ;------------------------------------------------------------
                                   5810 ;c                         Allocated to registers r6 r7 
                                   5811 ;------------------------------------------------------------
                                   5812 ;	..\src\COMMON\easyax5043.c:1012: static void axradio_sync_slave_nextperiod()
                                   5813 ;	-----------------------------------------
                                   5814 ;	 function axradio_sync_slave_nextperiod
                                   5815 ;	-----------------------------------------
      002627                       5816 _axradio_sync_slave_nextperiod:
                                   5817 ;	..\src\COMMON\easyax5043.c:1014: axradio_sync_addtime(axradio_sync_period);
      002627 90 7A F6         [24] 5818 	mov	dptr,#_axradio_sync_period
      00262A E4               [12] 5819 	clr	a
      00262B 93               [24] 5820 	movc	a,@a+dptr
      00262C FC               [12] 5821 	mov	r4,a
      00262D 74 01            [12] 5822 	mov	a,#0x01
      00262F 93               [24] 5823 	movc	a,@a+dptr
      002630 FD               [12] 5824 	mov	r5,a
      002631 74 02            [12] 5825 	mov	a,#0x02
      002633 93               [24] 5826 	movc	a,@a+dptr
      002634 FE               [12] 5827 	mov	r6,a
      002635 74 03            [12] 5828 	mov	a,#0x03
      002637 93               [24] 5829 	movc	a,@a+dptr
      002638 8C 82            [24] 5830 	mov	dpl,r4
      00263A 8D 83            [24] 5831 	mov	dph,r5
      00263C 8E F0            [24] 5832 	mov	b,r6
      00263E 12 24 70         [24] 5833 	lcall	_axradio_sync_addtime
                                   5834 ;	..\src\COMMON\easyax5043.c:1015: if (!checksignedlimit16(axradio_sync_periodcorr, axradio_sync_slave_maxperiod))
      002641 90 00 C7         [24] 5835 	mov	dptr,#_axradio_sync_periodcorr
      002644 E0               [24] 5836 	movx	a,@dptr
      002645 FE               [12] 5837 	mov	r6,a
      002646 A3               [24] 5838 	inc	dptr
      002647 E0               [24] 5839 	movx	a,@dptr
      002648 FF               [12] 5840 	mov	r7,a
      002649 90 7B 0A         [24] 5841 	mov	dptr,#_axradio_sync_slave_maxperiod
      00264C E4               [12] 5842 	clr	a
      00264D 93               [24] 5843 	movc	a,@a+dptr
      00264E C0 E0            [24] 5844 	push	acc
      002650 74 01            [12] 5845 	mov	a,#0x01
      002652 93               [24] 5846 	movc	a,@a+dptr
      002653 C0 E0            [24] 5847 	push	acc
      002655 8E 82            [24] 5848 	mov	dpl,r6
      002657 8F 83            [24] 5849 	mov	dph,r7
      002659 12 6E D3         [24] 5850 	lcall	_checksignedlimit16
      00265C AF 82            [24] 5851 	mov	r7,dpl
      00265E 15 81            [12] 5852 	dec	sp
      002660 15 81            [12] 5853 	dec	sp
      002662 EF               [12] 5854 	mov	a,r7
      002663 70 01            [24] 5855 	jnz	00102$
                                   5856 ;	..\src\COMMON\easyax5043.c:1016: return;
      002665 22               [24] 5857 	ret
      002666                       5858 00102$:
                                   5859 ;	..\src\COMMON\easyax5043.c:1018: int16_t __autodata c = axradio_sync_periodcorr;
      002666 90 00 C7         [24] 5860 	mov	dptr,#_axradio_sync_periodcorr
      002669 E0               [24] 5861 	movx	a,@dptr
      00266A FE               [12] 5862 	mov	r6,a
      00266B A3               [24] 5863 	inc	dptr
      00266C E0               [24] 5864 	movx	a,@dptr
                                   5865 ;	..\src\COMMON\easyax5043.c:1019: axradio_sync_addtime(c >> SYNC_K1);
      00266D FF               [12] 5866 	mov	r7,a
      00266E C4               [12] 5867 	swap	a
      00266F 03               [12] 5868 	rr	a
      002670 CE               [12] 5869 	xch	a,r6
      002671 C4               [12] 5870 	swap	a
      002672 03               [12] 5871 	rr	a
      002673 54 07            [12] 5872 	anl	a,#0x07
      002675 6E               [12] 5873 	xrl	a,r6
      002676 CE               [12] 5874 	xch	a,r6
      002677 54 07            [12] 5875 	anl	a,#0x07
      002679 CE               [12] 5876 	xch	a,r6
      00267A 6E               [12] 5877 	xrl	a,r6
      00267B CE               [12] 5878 	xch	a,r6
      00267C 30 E2 02         [24] 5879 	jnb	acc.2,00109$
      00267F 44 F8            [12] 5880 	orl	a,#0xf8
      002681                       5881 00109$:
      002681 FF               [12] 5882 	mov	r7,a
      002682 33               [12] 5883 	rlc	a
      002683 95 E0            [12] 5884 	subb	a,acc
      002685 FD               [12] 5885 	mov	r5,a
      002686 8E 82            [24] 5886 	mov	dpl,r6
      002688 8F 83            [24] 5887 	mov	dph,r7
      00268A 8D F0            [24] 5888 	mov	b,r5
      00268C 02 24 70         [24] 5889 	ljmp	_axradio_sync_addtime
                                   5890 ;------------------------------------------------------------
                                   5891 ;Allocation info for local variables in function 'axradio_timer_callback'
                                   5892 ;------------------------------------------------------------
                                   5893 ;r                         Allocated to registers r7 
                                   5894 ;idx                       Allocated to registers r7 
                                   5895 ;rs                        Allocated to registers r6 
                                   5896 ;idx                       Allocated to registers r7 
                                   5897 ;desc                      Allocated with name '_axradio_timer_callback_desc_1_326'
                                   5898 ;------------------------------------------------------------
                                   5899 ;	..\src\COMMON\easyax5043.c:1025: static void axradio_timer_callback(struct wtimer_desc __xdata *desc)
                                   5900 ;	-----------------------------------------
                                   5901 ;	 function axradio_timer_callback
                                   5902 ;	-----------------------------------------
      00268F                       5903 _axradio_timer_callback:
                                   5904 ;	..\src\COMMON\easyax5043.c:1028: switch (axradio_mode) {
      00268F AF 0B            [24] 5905 	mov	r7,_axradio_mode
      002691 BF 10 00         [24] 5906 	cjne	r7,#0x10,00266$
      002694                       5907 00266$:
      002694 50 01            [24] 5908 	jnc	00267$
      002696 22               [24] 5909 	ret
      002697                       5910 00267$:
      002697 EF               [12] 5911 	mov	a,r7
      002698 24 CC            [12] 5912 	add	a,#0xff - 0x33
      00269A 50 01            [24] 5913 	jnc	00268$
      00269C 22               [24] 5914 	ret
      00269D                       5915 00268$:
      00269D EF               [12] 5916 	mov	a,r7
      00269E 24 F0            [12] 5917 	add	a,#0xf0
      0026A0 FF               [12] 5918 	mov	r7,a
      0026A1 24 0A            [12] 5919 	add	a,#(00269$-3-.)
      0026A3 83               [24] 5920 	movc	a,@a+pc
      0026A4 F5 82            [12] 5921 	mov	dpl,a
      0026A6 EF               [12] 5922 	mov	a,r7
      0026A7 24 28            [12] 5923 	add	a,#(00270$-3-.)
      0026A9 83               [24] 5924 	movc	a,@a+pc
      0026AA F5 83            [12] 5925 	mov	dph,a
      0026AC E4               [12] 5926 	clr	a
      0026AD 73               [24] 5927 	jmp	@a+dptr
      0026AE                       5928 00269$:
      0026AE 96                    5929 	.db	00112$
      0026AF 96                    5930 	.db	00113$
      0026B0 2C                    5931 	.db	00123$
      0026B1 2C                    5932 	.db	00124$
      0026B2 C7                    5933 	.db	00175$
      0026B3 C7                    5934 	.db	00175$
      0026B4 C7                    5935 	.db	00175$
      0026B5 C7                    5936 	.db	00175$
      0026B6 C7                    5937 	.db	00175$
      0026B7 C7                    5938 	.db	00175$
      0026B8 C7                    5939 	.db	00175$
      0026B9 C7                    5940 	.db	00175$
      0026BA C7                    5941 	.db	00175$
      0026BB C7                    5942 	.db	00175$
      0026BC C7                    5943 	.db	00175$
      0026BD C7                    5944 	.db	00175$
      0026BE F6                    5945 	.db	00106$
      0026BF F6                    5946 	.db	00107$
      0026C0 8E                    5947 	.db	00129$
      0026C1 8E                    5948 	.db	00130$
      0026C2 C7                    5949 	.db	00175$
      0026C3 C7                    5950 	.db	00175$
      0026C4 C7                    5951 	.db	00175$
      0026C5 C7                    5952 	.db	00175$
      0026C6 F6                    5953 	.db	00102$
      0026C7 F6                    5954 	.db	00103$
      0026C8 F6                    5955 	.db	00104$
      0026C9 F6                    5956 	.db	00105$
      0026CA F6                    5957 	.db	00101$
      0026CB C7                    5958 	.db	00175$
      0026CC C7                    5959 	.db	00175$
      0026CD C7                    5960 	.db	00175$
      0026CE 23                    5961 	.db	00139$
      0026CF 23                    5962 	.db	00140$
      0026D0 BF                    5963 	.db	00152$
      0026D1 BF                    5964 	.db	00153$
      0026D2                       5965 00270$:
      0026D2 27                    5966 	.db	00112$>>8
      0026D3 27                    5967 	.db	00113$>>8
      0026D4 28                    5968 	.db	00123$>>8
      0026D5 28                    5969 	.db	00124$>>8
      0026D6 2E                    5970 	.db	00175$>>8
      0026D7 2E                    5971 	.db	00175$>>8
      0026D8 2E                    5972 	.db	00175$>>8
      0026D9 2E                    5973 	.db	00175$>>8
      0026DA 2E                    5974 	.db	00175$>>8
      0026DB 2E                    5975 	.db	00175$>>8
      0026DC 2E                    5976 	.db	00175$>>8
      0026DD 2E                    5977 	.db	00175$>>8
      0026DE 2E                    5978 	.db	00175$>>8
      0026DF 2E                    5979 	.db	00175$>>8
      0026E0 2E                    5980 	.db	00175$>>8
      0026E1 2E                    5981 	.db	00175$>>8
      0026E2 26                    5982 	.db	00106$>>8
      0026E3 26                    5983 	.db	00107$>>8
      0026E4 28                    5984 	.db	00129$>>8
      0026E5 28                    5985 	.db	00130$>>8
      0026E6 2E                    5986 	.db	00175$>>8
      0026E7 2E                    5987 	.db	00175$>>8
      0026E8 2E                    5988 	.db	00175$>>8
      0026E9 2E                    5989 	.db	00175$>>8
      0026EA 26                    5990 	.db	00102$>>8
      0026EB 26                    5991 	.db	00103$>>8
      0026EC 26                    5992 	.db	00104$>>8
      0026ED 26                    5993 	.db	00105$>>8
      0026EE 26                    5994 	.db	00101$>>8
      0026EF 2E                    5995 	.db	00175$>>8
      0026F0 2E                    5996 	.db	00175$>>8
      0026F1 2E                    5997 	.db	00175$>>8
      0026F2 29                    5998 	.db	00139$>>8
      0026F3 29                    5999 	.db	00140$>>8
      0026F4 2A                    6000 	.db	00152$>>8
      0026F5 2A                    6001 	.db	00153$>>8
                                   6002 ;	..\src\COMMON\easyax5043.c:1029: case AXRADIO_MODE_STREAM_RECEIVE:
      0026F6                       6003 00101$:
                                   6004 ;	..\src\COMMON\easyax5043.c:1030: case AXRADIO_MODE_STREAM_RECEIVE_UNENC:
      0026F6                       6005 00102$:
                                   6006 ;	..\src\COMMON\easyax5043.c:1031: case AXRADIO_MODE_STREAM_RECEIVE_SCRAM:
      0026F6                       6007 00103$:
                                   6008 ;	..\src\COMMON\easyax5043.c:1032: case AXRADIO_MODE_STREAM_RECEIVE_UNENC_LSB:
      0026F6                       6009 00104$:
                                   6010 ;	..\src\COMMON\easyax5043.c:1033: case AXRADIO_MODE_STREAM_RECEIVE_SCRAM_LSB:
      0026F6                       6011 00105$:
                                   6012 ;	..\src\COMMON\easyax5043.c:1034: case AXRADIO_MODE_ASYNC_RECEIVE:
      0026F6                       6013 00106$:
                                   6014 ;	..\src\COMMON\easyax5043.c:1035: case AXRADIO_MODE_WOR_RECEIVE:
      0026F6                       6015 00107$:
                                   6016 ;	..\src\COMMON\easyax5043.c:1036: if (axradio_syncstate == syncstate_asynctx)
      0026F6 90 00 B7         [24] 6017 	mov	dptr,#_axradio_syncstate
      0026F9 E0               [24] 6018 	movx	a,@dptr
      0026FA FF               [12] 6019 	mov	r7,a
      0026FB BF 02 03         [24] 6020 	cjne	r7,#0x02,00271$
      0026FE 02 27 96         [24] 6021 	ljmp	00114$
      002701                       6022 00271$:
                                   6023 ;	..\src\COMMON\easyax5043.c:1038: wtimer_remove(&axradio_timer);
      002701 90 03 3C         [24] 6024 	mov	dptr,#_axradio_timer
      002704 12 71 84         [24] 6025 	lcall	_wtimer_remove
                                   6026 ;	..\src\COMMON\easyax5043.c:1039: rearmcstimer:
      002707                       6027 00110$:
                                   6028 ;	..\src\COMMON\easyax5043.c:1040: axradio_timer.time = axradio_phy_cs_period;
      002707 90 7A C9         [24] 6029 	mov	dptr,#_axradio_phy_cs_period
      00270A E4               [12] 6030 	clr	a
      00270B 93               [24] 6031 	movc	a,@a+dptr
      00270C FE               [12] 6032 	mov	r6,a
      00270D 74 01            [12] 6033 	mov	a,#0x01
      00270F 93               [24] 6034 	movc	a,@a+dptr
      002710 FF               [12] 6035 	mov	r7,a
      002711 7D 00            [12] 6036 	mov	r5,#0x00
      002713 7C 00            [12] 6037 	mov	r4,#0x00
      002715 90 03 40         [24] 6038 	mov	dptr,#(_axradio_timer + 0x0004)
      002718 EE               [12] 6039 	mov	a,r6
      002719 F0               [24] 6040 	movx	@dptr,a
      00271A EF               [12] 6041 	mov	a,r7
      00271B A3               [24] 6042 	inc	dptr
      00271C F0               [24] 6043 	movx	@dptr,a
      00271D ED               [12] 6044 	mov	a,r5
      00271E A3               [24] 6045 	inc	dptr
      00271F F0               [24] 6046 	movx	@dptr,a
      002720 EC               [12] 6047 	mov	a,r4
      002721 A3               [24] 6048 	inc	dptr
      002722 F0               [24] 6049 	movx	@dptr,a
                                   6050 ;	..\src\COMMON\easyax5043.c:1041: wtimer0_addrelative(&axradio_timer);
      002723 90 03 3C         [24] 6051 	mov	dptr,#_axradio_timer
      002726 12 66 25         [24] 6052 	lcall	_wtimer0_addrelative
                                   6053 ;	..\src\COMMON\easyax5043.c:1042: chanstatecb:
      002729                       6054 00111$:
                                   6055 ;	..\src\COMMON\easyax5043.c:1043: update_timeanchor();
      002729 12 15 BA         [24] 6056 	lcall	_update_timeanchor
                                   6057 ;	..\src\COMMON\easyax5043.c:1044: wtimer_remove_callback(&axradio_cb_channelstate.cb);
      00272C 90 03 11         [24] 6058 	mov	dptr,#_axradio_cb_channelstate
      00272F 12 74 C0         [24] 6059 	lcall	_wtimer_remove_callback
                                   6060 ;	..\src\COMMON\easyax5043.c:1045: axradio_cb_channelstate.st.error = AXRADIO_ERR_NOERROR;
      002732 90 03 16         [24] 6061 	mov	dptr,#(_axradio_cb_channelstate + 0x0005)
      002735 E4               [12] 6062 	clr	a
      002736 F0               [24] 6063 	movx	@dptr,a
                                   6064 ;	..\src\COMMON\easyax5043.c:1047: int8_t __autodata r = AX5043_RSSI;
      002737 90 40 40         [24] 6065 	mov	dptr,#_AX5043_RSSI
      00273A E0               [24] 6066 	movx	a,@dptr
                                   6067 ;	..\src\COMMON\easyax5043.c:1048: axradio_cb_channelstate.st.cs.rssi = r - (int16_t)axradio_phy_rssioffset;
      00273B FF               [12] 6068 	mov	r7,a
      00273C FD               [12] 6069 	mov	r5,a
      00273D 33               [12] 6070 	rlc	a
      00273E 95 E0            [12] 6071 	subb	a,acc
      002740 FE               [12] 6072 	mov	r6,a
      002741 90 7A C6         [24] 6073 	mov	dptr,#_axradio_phy_rssioffset
      002744 E4               [12] 6074 	clr	a
      002745 93               [24] 6075 	movc	a,@a+dptr
      002746 FC               [12] 6076 	mov	r4,a
      002747 33               [12] 6077 	rlc	a
      002748 95 E0            [12] 6078 	subb	a,acc
      00274A FB               [12] 6079 	mov	r3,a
      00274B ED               [12] 6080 	mov	a,r5
      00274C C3               [12] 6081 	clr	c
      00274D 9C               [12] 6082 	subb	a,r4
      00274E FD               [12] 6083 	mov	r5,a
      00274F EE               [12] 6084 	mov	a,r6
      002750 9B               [12] 6085 	subb	a,r3
      002751 FE               [12] 6086 	mov	r6,a
      002752 90 03 1B         [24] 6087 	mov	dptr,#(_axradio_cb_channelstate + 0x000a)
      002755 ED               [12] 6088 	mov	a,r5
      002756 F0               [24] 6089 	movx	@dptr,a
      002757 EE               [12] 6090 	mov	a,r6
      002758 A3               [24] 6091 	inc	dptr
      002759 F0               [24] 6092 	movx	@dptr,a
                                   6093 ;	..\src\COMMON\easyax5043.c:1049: axradio_cb_channelstate.st.cs.busy = r >= axradio_phy_channelbusy;
      00275A 90 7A C8         [24] 6094 	mov	dptr,#_axradio_phy_channelbusy
      00275D E4               [12] 6095 	clr	a
      00275E 93               [24] 6096 	movc	a,@a+dptr
      00275F FE               [12] 6097 	mov	r6,a
      002760 C3               [12] 6098 	clr	c
      002761 EF               [12] 6099 	mov	a,r7
      002762 64 80            [12] 6100 	xrl	a,#0x80
      002764 8E F0            [24] 6101 	mov	b,r6
      002766 63 F0 80         [24] 6102 	xrl	b,#0x80
      002769 95 F0            [12] 6103 	subb	a,b
      00276B B3               [12] 6104 	cpl	c
      00276C 92 01            [24] 6105 	mov	_axradio_timer_callback_sloc0_1_0,c
      00276E E4               [12] 6106 	clr	a
      00276F 33               [12] 6107 	rlc	a
      002770 90 03 1D         [24] 6108 	mov	dptr,#(_axradio_cb_channelstate + 0x000c)
      002773 F0               [24] 6109 	movx	@dptr,a
                                   6110 ;	..\src\COMMON\easyax5043.c:1051: axradio_cb_channelstate.st.time.t = axradio_timeanchor.radiotimer;
      002774 90 00 CD         [24] 6111 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      002777 E0               [24] 6112 	movx	a,@dptr
      002778 FC               [12] 6113 	mov	r4,a
      002779 A3               [24] 6114 	inc	dptr
      00277A E0               [24] 6115 	movx	a,@dptr
      00277B FD               [12] 6116 	mov	r5,a
      00277C A3               [24] 6117 	inc	dptr
      00277D E0               [24] 6118 	movx	a,@dptr
      00277E FE               [12] 6119 	mov	r6,a
      00277F A3               [24] 6120 	inc	dptr
      002780 E0               [24] 6121 	movx	a,@dptr
      002781 FF               [12] 6122 	mov	r7,a
      002782 90 03 17         [24] 6123 	mov	dptr,#(_axradio_cb_channelstate + 0x0006)
      002785 EC               [12] 6124 	mov	a,r4
      002786 F0               [24] 6125 	movx	@dptr,a
      002787 ED               [12] 6126 	mov	a,r5
      002788 A3               [24] 6127 	inc	dptr
      002789 F0               [24] 6128 	movx	@dptr,a
      00278A EE               [12] 6129 	mov	a,r6
      00278B A3               [24] 6130 	inc	dptr
      00278C F0               [24] 6131 	movx	@dptr,a
      00278D EF               [12] 6132 	mov	a,r7
      00278E A3               [24] 6133 	inc	dptr
      00278F F0               [24] 6134 	movx	@dptr,a
                                   6135 ;	..\src\COMMON\easyax5043.c:1052: wtimer_add_callback(&axradio_cb_channelstate.cb);
      002790 90 03 11         [24] 6136 	mov	dptr,#_axradio_cb_channelstate
                                   6137 ;	..\src\COMMON\easyax5043.c:1053: break;
      002793 02 66 0B         [24] 6138 	ljmp	_wtimer_add_callback
                                   6139 ;	..\src\COMMON\easyax5043.c:1055: case AXRADIO_MODE_ASYNC_TRANSMIT:
      002796                       6140 00112$:
                                   6141 ;	..\src\COMMON\easyax5043.c:1056: case AXRADIO_MODE_WOR_TRANSMIT:
      002796                       6142 00113$:
                                   6143 ;	..\src\COMMON\easyax5043.c:1057: transmitcs:
      002796                       6144 00114$:
                                   6145 ;	..\src\COMMON\easyax5043.c:1058: if (axradio_ack_count)
      002796 90 00 C1         [24] 6146 	mov	dptr,#_axradio_ack_count
      002799 E0               [24] 6147 	movx	a,@dptr
      00279A FF               [12] 6148 	mov	r7,a
      00279B E0               [24] 6149 	movx	a,@dptr
      00279C 60 06            [24] 6150 	jz	00116$
                                   6151 ;	..\src\COMMON\easyax5043.c:1059: --axradio_ack_count;
      00279E EF               [12] 6152 	mov	a,r7
      00279F 14               [12] 6153 	dec	a
      0027A0 90 00 C1         [24] 6154 	mov	dptr,#_axradio_ack_count
      0027A3 F0               [24] 6155 	movx	@dptr,a
      0027A4                       6156 00116$:
                                   6157 ;	..\src\COMMON\easyax5043.c:1060: wtimer_remove(&axradio_timer);
      0027A4 90 03 3C         [24] 6158 	mov	dptr,#_axradio_timer
      0027A7 12 71 84         [24] 6159 	lcall	_wtimer_remove
                                   6160 ;	..\src\COMMON\easyax5043.c:1061: if ((int8_t)AX5043_RSSI < axradio_phy_channelbusy ||
      0027AA 90 40 40         [24] 6161 	mov	dptr,#_AX5043_RSSI
      0027AD E0               [24] 6162 	movx	a,@dptr
      0027AE FF               [12] 6163 	mov	r7,a
      0027AF 90 7A C8         [24] 6164 	mov	dptr,#_axradio_phy_channelbusy
      0027B2 E4               [12] 6165 	clr	a
      0027B3 93               [24] 6166 	movc	a,@a+dptr
      0027B4 FE               [12] 6167 	mov	r6,a
      0027B5 C3               [12] 6168 	clr	c
      0027B6 EF               [12] 6169 	mov	a,r7
      0027B7 64 80            [12] 6170 	xrl	a,#0x80
      0027B9 8E F0            [24] 6171 	mov	b,r6
      0027BB 63 F0 80         [24] 6172 	xrl	b,#0x80
      0027BE 95 F0            [12] 6173 	subb	a,b
      0027C0 40 0F            [24] 6174 	jc	00117$
                                   6175 ;	..\src\COMMON\easyax5043.c:1062: (!axradio_ack_count && axradio_phy_lbt_forcetx)) {
      0027C2 90 00 C1         [24] 6176 	mov	dptr,#_axradio_ack_count
      0027C5 E0               [24] 6177 	movx	a,@dptr
      0027C6 FF               [12] 6178 	mov	r7,a
      0027C7 E0               [24] 6179 	movx	a,@dptr
      0027C8 70 23            [24] 6180 	jnz	00118$
      0027CA 90 7A CD         [24] 6181 	mov	dptr,#_axradio_phy_lbt_forcetx
      0027CD E4               [12] 6182 	clr	a
      0027CE 93               [24] 6183 	movc	a,@a+dptr
      0027CF 60 1C            [24] 6184 	jz	00118$
      0027D1                       6185 00117$:
                                   6186 ;	..\src\COMMON\easyax5043.c:1063: axradio_syncstate = syncstate_off;
      0027D1 90 00 B7         [24] 6187 	mov	dptr,#_axradio_syncstate
      0027D4 E4               [12] 6188 	clr	a
      0027D5 F0               [24] 6189 	movx	@dptr,a
                                   6190 ;	..\src\COMMON\easyax5043.c:1064: axradio_txbuffer_cnt = axradio_phy_preamble_longlen;
      0027D6 90 7A D2         [24] 6191 	mov	dptr,#_axradio_phy_preamble_longlen
                                   6192 ;	genFromRTrack removed	clr	a
      0027D9 93               [24] 6193 	movc	a,@a+dptr
      0027DA FD               [12] 6194 	mov	r5,a
      0027DB 74 01            [12] 6195 	mov	a,#0x01
      0027DD 93               [24] 6196 	movc	a,@a+dptr
      0027DE FE               [12] 6197 	mov	r6,a
      0027DF 90 00 BA         [24] 6198 	mov	dptr,#_axradio_txbuffer_cnt
      0027E2 ED               [12] 6199 	mov	a,r5
      0027E3 F0               [24] 6200 	movx	@dptr,a
      0027E4 EE               [12] 6201 	mov	a,r6
      0027E5 A3               [24] 6202 	inc	dptr
      0027E6 F0               [24] 6203 	movx	@dptr,a
                                   6204 ;	..\src\COMMON\easyax5043.c:1065: ax5043_prepare_tx();
      0027E7 12 22 A5         [24] 6205 	lcall	_ax5043_prepare_tx
                                   6206 ;	..\src\COMMON\easyax5043.c:1066: goto chanstatecb;
      0027EA 02 27 29         [24] 6207 	ljmp	00111$
      0027ED                       6208 00118$:
                                   6209 ;	..\src\COMMON\easyax5043.c:1068: if (axradio_ack_count)
      0027ED EF               [12] 6210 	mov	a,r7
      0027EE 60 03            [24] 6211 	jz	00276$
      0027F0 02 27 07         [24] 6212 	ljmp	00110$
      0027F3                       6213 00276$:
                                   6214 ;	..\src\COMMON\easyax5043.c:1070: update_timeanchor();
      0027F3 12 15 BA         [24] 6215 	lcall	_update_timeanchor
                                   6216 ;	..\src\COMMON\easyax5043.c:1071: axradio_syncstate = syncstate_off;
      0027F6 90 00 B7         [24] 6217 	mov	dptr,#_axradio_syncstate
      0027F9 E4               [12] 6218 	clr	a
      0027FA F0               [24] 6219 	movx	@dptr,a
                                   6220 ;	..\src\COMMON\easyax5043.c:1072: ax5043_off();
      0027FB 12 22 CE         [24] 6221 	lcall	_ax5043_off
                                   6222 ;	..\src\COMMON\easyax5043.c:1073: wtimer_remove_callback(&axradio_cb_transmitstart.cb);
      0027FE 90 03 1E         [24] 6223 	mov	dptr,#_axradio_cb_transmitstart
      002801 12 74 C0         [24] 6224 	lcall	_wtimer_remove_callback
                                   6225 ;	..\src\COMMON\easyax5043.c:1074: axradio_cb_transmitstart.st.error = AXRADIO_ERR_TIMEOUT;
      002804 90 03 23         [24] 6226 	mov	dptr,#(_axradio_cb_transmitstart + 0x0005)
      002807 74 03            [12] 6227 	mov	a,#0x03
      002809 F0               [24] 6228 	movx	@dptr,a
                                   6229 ;	..\src\COMMON\easyax5043.c:1075: axradio_cb_transmitstart.st.time.t = axradio_timeanchor.radiotimer;
      00280A 90 00 CD         [24] 6230 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      00280D E0               [24] 6231 	movx	a,@dptr
      00280E FC               [12] 6232 	mov	r4,a
      00280F A3               [24] 6233 	inc	dptr
      002810 E0               [24] 6234 	movx	a,@dptr
      002811 FD               [12] 6235 	mov	r5,a
      002812 A3               [24] 6236 	inc	dptr
      002813 E0               [24] 6237 	movx	a,@dptr
      002814 FE               [12] 6238 	mov	r6,a
      002815 A3               [24] 6239 	inc	dptr
      002816 E0               [24] 6240 	movx	a,@dptr
      002817 FF               [12] 6241 	mov	r7,a
      002818 90 03 24         [24] 6242 	mov	dptr,#(_axradio_cb_transmitstart + 0x0006)
      00281B EC               [12] 6243 	mov	a,r4
      00281C F0               [24] 6244 	movx	@dptr,a
      00281D ED               [12] 6245 	mov	a,r5
      00281E A3               [24] 6246 	inc	dptr
      00281F F0               [24] 6247 	movx	@dptr,a
      002820 EE               [12] 6248 	mov	a,r6
      002821 A3               [24] 6249 	inc	dptr
      002822 F0               [24] 6250 	movx	@dptr,a
      002823 EF               [12] 6251 	mov	a,r7
      002824 A3               [24] 6252 	inc	dptr
      002825 F0               [24] 6253 	movx	@dptr,a
                                   6254 ;	..\src\COMMON\easyax5043.c:1076: wtimer_add_callback(&axradio_cb_transmitstart.cb);
      002826 90 03 1E         [24] 6255 	mov	dptr,#_axradio_cb_transmitstart
                                   6256 ;	..\src\COMMON\easyax5043.c:1077: break;
      002829 02 66 0B         [24] 6257 	ljmp	_wtimer_add_callback
                                   6258 ;	..\src\COMMON\easyax5043.c:1079: case AXRADIO_MODE_ACK_TRANSMIT:
      00282C                       6259 00123$:
                                   6260 ;	..\src\COMMON\easyax5043.c:1080: case AXRADIO_MODE_WOR_ACK_TRANSMIT:
      00282C                       6261 00124$:
                                   6262 ;	..\src\COMMON\easyax5043.c:1081: if (axradio_syncstate == syncstate_lbt)
      00282C 90 00 B7         [24] 6263 	mov	dptr,#_axradio_syncstate
      00282F E0               [24] 6264 	movx	a,@dptr
      002830 FF               [12] 6265 	mov	r7,a
      002831 BF 01 03         [24] 6266 	cjne	r7,#0x01,00277$
      002834 02 27 96         [24] 6267 	ljmp	00114$
      002837                       6268 00277$:
                                   6269 ;	..\src\COMMON\easyax5043.c:1083: ax5043_off();
      002837 12 22 CE         [24] 6270 	lcall	_ax5043_off
                                   6271 ;	..\src\COMMON\easyax5043.c:1084: if (!axradio_ack_count) {
      00283A 90 00 C1         [24] 6272 	mov	dptr,#_axradio_ack_count
      00283D E0               [24] 6273 	movx	a,@dptr
      00283E FF               [12] 6274 	mov	r7,a
      00283F E0               [24] 6275 	movx	a,@dptr
      002840 70 31            [24] 6276 	jnz	00128$
                                   6277 ;	..\src\COMMON\easyax5043.c:1085: update_timeanchor();
      002842 12 15 BA         [24] 6278 	lcall	_update_timeanchor
                                   6279 ;	..\src\COMMON\easyax5043.c:1086: wtimer_remove_callback(&axradio_cb_transmitend.cb);
      002845 90 03 28         [24] 6280 	mov	dptr,#_axradio_cb_transmitend
      002848 12 74 C0         [24] 6281 	lcall	_wtimer_remove_callback
                                   6282 ;	..\src\COMMON\easyax5043.c:1087: axradio_cb_transmitend.st.error = AXRADIO_ERR_TIMEOUT;
      00284B 90 03 2D         [24] 6283 	mov	dptr,#(_axradio_cb_transmitend + 0x0005)
      00284E 74 03            [12] 6284 	mov	a,#0x03
      002850 F0               [24] 6285 	movx	@dptr,a
                                   6286 ;	..\src\COMMON\easyax5043.c:1088: axradio_cb_transmitend.st.time.t = axradio_timeanchor.radiotimer;
      002851 90 00 CD         [24] 6287 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      002854 E0               [24] 6288 	movx	a,@dptr
      002855 FB               [12] 6289 	mov	r3,a
      002856 A3               [24] 6290 	inc	dptr
      002857 E0               [24] 6291 	movx	a,@dptr
      002858 FC               [12] 6292 	mov	r4,a
      002859 A3               [24] 6293 	inc	dptr
      00285A E0               [24] 6294 	movx	a,@dptr
      00285B FD               [12] 6295 	mov	r5,a
      00285C A3               [24] 6296 	inc	dptr
      00285D E0               [24] 6297 	movx	a,@dptr
      00285E FE               [12] 6298 	mov	r6,a
      00285F 90 03 2E         [24] 6299 	mov	dptr,#(_axradio_cb_transmitend + 0x0006)
      002862 EB               [12] 6300 	mov	a,r3
      002863 F0               [24] 6301 	movx	@dptr,a
      002864 EC               [12] 6302 	mov	a,r4
      002865 A3               [24] 6303 	inc	dptr
      002866 F0               [24] 6304 	movx	@dptr,a
      002867 ED               [12] 6305 	mov	a,r5
      002868 A3               [24] 6306 	inc	dptr
      002869 F0               [24] 6307 	movx	@dptr,a
      00286A EE               [12] 6308 	mov	a,r6
      00286B A3               [24] 6309 	inc	dptr
      00286C F0               [24] 6310 	movx	@dptr,a
                                   6311 ;	..\src\COMMON\easyax5043.c:1089: wtimer_add_callback(&axradio_cb_transmitend.cb);
      00286D 90 03 28         [24] 6312 	mov	dptr,#_axradio_cb_transmitend
                                   6313 ;	..\src\COMMON\easyax5043.c:1090: break;
      002870 02 66 0B         [24] 6314 	ljmp	_wtimer_add_callback
      002873                       6315 00128$:
                                   6316 ;	..\src\COMMON\easyax5043.c:1092: --axradio_ack_count;
      002873 EF               [12] 6317 	mov	a,r7
      002874 14               [12] 6318 	dec	a
      002875 90 00 C1         [24] 6319 	mov	dptr,#_axradio_ack_count
      002878 F0               [24] 6320 	movx	@dptr,a
                                   6321 ;	..\src\COMMON\easyax5043.c:1093: axradio_txbuffer_cnt = axradio_phy_preamble_longlen;
      002879 90 7A D2         [24] 6322 	mov	dptr,#_axradio_phy_preamble_longlen
      00287C E4               [12] 6323 	clr	a
      00287D 93               [24] 6324 	movc	a,@a+dptr
      00287E FE               [12] 6325 	mov	r6,a
      00287F 74 01            [12] 6326 	mov	a,#0x01
      002881 93               [24] 6327 	movc	a,@a+dptr
      002882 FF               [12] 6328 	mov	r7,a
      002883 90 00 BA         [24] 6329 	mov	dptr,#_axradio_txbuffer_cnt
      002886 EE               [12] 6330 	mov	a,r6
      002887 F0               [24] 6331 	movx	@dptr,a
      002888 EF               [12] 6332 	mov	a,r7
      002889 A3               [24] 6333 	inc	dptr
      00288A F0               [24] 6334 	movx	@dptr,a
                                   6335 ;	..\src\COMMON\easyax5043.c:1094: ax5043_prepare_tx();
                                   6336 ;	..\src\COMMON\easyax5043.c:1095: break;
      00288B 02 22 A5         [24] 6337 	ljmp	_ax5043_prepare_tx
                                   6338 ;	..\src\COMMON\easyax5043.c:1097: case AXRADIO_MODE_ACK_RECEIVE:
      00288E                       6339 00129$:
                                   6340 ;	..\src\COMMON\easyax5043.c:1098: case AXRADIO_MODE_WOR_ACK_RECEIVE:
      00288E                       6341 00130$:
                                   6342 ;	..\src\COMMON\easyax5043.c:1099: if (axradio_syncstate == syncstate_lbt)
      00288E 90 00 B7         [24] 6343 	mov	dptr,#_axradio_syncstate
      002891 E0               [24] 6344 	movx	a,@dptr
      002892 FF               [12] 6345 	mov	r7,a
      002893 BF 01 03         [24] 6346 	cjne	r7,#0x01,00279$
      002896 02 27 96         [24] 6347 	ljmp	00114$
      002899                       6348 00279$:
                                   6349 ;	..\src\COMMON\easyax5043.c:1101: transmitack:
      002899                       6350 00133$:
                                   6351 ;	..\src\COMMON\easyax5043.c:1102: AX5043_FIFOSTAT = 3;
      002899 90 40 28         [24] 6352 	mov	dptr,#_AX5043_FIFOSTAT
      00289C 74 03            [12] 6353 	mov	a,#0x03
      00289E F0               [24] 6354 	movx	@dptr,a
                                   6355 ;	..\src\COMMON\easyax5043.c:1103: AX5043_PWRMODE = AX5043_PWRSTATE_FULL_TX;
      00289F 90 40 02         [24] 6356 	mov	dptr,#_AX5043_PWRMODE
      0028A2 74 0D            [12] 6357 	mov	a,#0x0d
      0028A4 F0               [24] 6358 	movx	@dptr,a
                                   6359 ;	..\src\COMMON\easyax5043.c:1104: while (!(AX5043_POWSTAT & 0x08)); // wait for modem vdd so writing the FIFO is safe
      0028A5                       6360 00134$:
      0028A5 90 40 03         [24] 6361 	mov	dptr,#_AX5043_POWSTAT
      0028A8 E0               [24] 6362 	movx	a,@dptr
      0028A9 FF               [12] 6363 	mov	r7,a
      0028AA 30 E3 F8         [24] 6364 	jnb	acc.3,00134$
                                   6365 ;	..\src\COMMON\easyax5043.c:1105: ax5043_init_registers_tx();
      0028AD 12 16 97         [24] 6366 	lcall	_ax5043_init_registers_tx
                                   6367 ;	..\src\COMMON\easyax5043.c:1106: AX5043_RADIOEVENTREQ0; // make sure REVRDONE bit is cleared, so it is a reliable indicator that the packet is out
      0028B0 90 40 0F         [24] 6368 	mov	dptr,#_AX5043_RADIOEVENTREQ0
      0028B3 E0               [24] 6369 	movx	a,@dptr
                                   6370 ;	..\src\COMMON\easyax5043.c:1107: AX5043_FIFOTHRESH1 = 0;
      0028B4 90 40 2E         [24] 6371 	mov	dptr,#_AX5043_FIFOTHRESH1
      0028B7 E4               [12] 6372 	clr	a
      0028B8 F0               [24] 6373 	movx	@dptr,a
                                   6374 ;	..\src\COMMON\easyax5043.c:1108: AX5043_FIFOTHRESH0 = 0x80;
      0028B9 90 40 2F         [24] 6375 	mov	dptr,#_AX5043_FIFOTHRESH0
      0028BC 74 80            [12] 6376 	mov	a,#0x80
      0028BE F0               [24] 6377 	movx	@dptr,a
                                   6378 ;	..\src\COMMON\easyax5043.c:1109: axradio_trxstate = trxstate_tx_longpreamble;
      0028BF 75 0C 0A         [24] 6379 	mov	_axradio_trxstate,#0x0a
                                   6380 ;	..\src\COMMON\easyax5043.c:1110: axradio_txbuffer_cnt = axradio_phy_preamble_longlen;
      0028C2 90 7A D2         [24] 6381 	mov	dptr,#_axradio_phy_preamble_longlen
      0028C5 E4               [12] 6382 	clr	a
      0028C6 93               [24] 6383 	movc	a,@a+dptr
      0028C7 FE               [12] 6384 	mov	r6,a
      0028C8 74 01            [12] 6385 	mov	a,#0x01
      0028CA 93               [24] 6386 	movc	a,@a+dptr
      0028CB FF               [12] 6387 	mov	r7,a
      0028CC 90 00 BA         [24] 6388 	mov	dptr,#_axradio_txbuffer_cnt
      0028CF EE               [12] 6389 	mov	a,r6
      0028D0 F0               [24] 6390 	movx	@dptr,a
      0028D1 EF               [12] 6391 	mov	a,r7
      0028D2 A3               [24] 6392 	inc	dptr
      0028D3 F0               [24] 6393 	movx	@dptr,a
                                   6394 ;	..\src\COMMON\easyax5043.c:1112: if ((AX5043_MODULATION & 0x0F) == 9) { // 4-FSK
      0028D4 90 40 10         [24] 6395 	mov	dptr,#_AX5043_MODULATION
      0028D7 E0               [24] 6396 	movx	a,@dptr
      0028D8 FF               [12] 6397 	mov	r7,a
      0028D9 53 07 0F         [24] 6398 	anl	ar7,#0x0f
      0028DC BF 09 0E         [24] 6399 	cjne	r7,#0x09,00138$
                                   6400 ;	..\src\COMMON\easyax5043.c:1113: AX5043_FIFODATA = AX5043_FIFOCMD_DATA | (7 << 5);
      0028DF 90 40 29         [24] 6401 	mov	dptr,#_AX5043_FIFODATA
      0028E2 74 E1            [12] 6402 	mov	a,#0xe1
      0028E4 F0               [24] 6403 	movx	@dptr,a
                                   6404 ;	..\src\COMMON\easyax5043.c:1114: AX5043_FIFODATA = 2;  // length (including flags)
      0028E5 74 02            [12] 6405 	mov	a,#0x02
      0028E7 F0               [24] 6406 	movx	@dptr,a
                                   6407 ;	..\src\COMMON\easyax5043.c:1115: AX5043_FIFODATA = 0x01;  // flag PKTSTART -> dibit sync
      0028E8 14               [12] 6408 	dec	a
      0028E9 F0               [24] 6409 	movx	@dptr,a
                                   6410 ;	..\src\COMMON\easyax5043.c:1116: AX5043_FIFODATA = 0x11; // dummy byte for forcing dibit sync
      0028EA 74 11            [12] 6411 	mov	a,#0x11
      0028EC F0               [24] 6412 	movx	@dptr,a
      0028ED                       6413 00138$:
                                   6414 ;	..\src\COMMON\easyax5043.c:1123: AX5043_IRQMASK0 = 0x08; // enable fifo free threshold
      0028ED 90 40 07         [24] 6415 	mov	dptr,#_AX5043_IRQMASK0
      0028F0 74 08            [12] 6416 	mov	a,#0x08
      0028F2 F0               [24] 6417 	movx	@dptr,a
                                   6418 ;	..\src\COMMON\easyax5043.c:1124: update_timeanchor();
      0028F3 12 15 BA         [24] 6419 	lcall	_update_timeanchor
                                   6420 ;	..\src\COMMON\easyax5043.c:1125: wtimer_remove_callback(&axradio_cb_transmitstart.cb);
      0028F6 90 03 1E         [24] 6421 	mov	dptr,#_axradio_cb_transmitstart
      0028F9 12 74 C0         [24] 6422 	lcall	_wtimer_remove_callback
                                   6423 ;	..\src\COMMON\easyax5043.c:1126: axradio_cb_transmitstart.st.error = AXRADIO_ERR_NOERROR;
      0028FC 90 03 23         [24] 6424 	mov	dptr,#(_axradio_cb_transmitstart + 0x0005)
      0028FF E4               [12] 6425 	clr	a
      002900 F0               [24] 6426 	movx	@dptr,a
                                   6427 ;	..\src\COMMON\easyax5043.c:1127: axradio_cb_transmitstart.st.time.t = axradio_timeanchor.radiotimer;
      002901 90 00 CD         [24] 6428 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      002904 E0               [24] 6429 	movx	a,@dptr
      002905 FC               [12] 6430 	mov	r4,a
      002906 A3               [24] 6431 	inc	dptr
      002907 E0               [24] 6432 	movx	a,@dptr
      002908 FD               [12] 6433 	mov	r5,a
      002909 A3               [24] 6434 	inc	dptr
      00290A E0               [24] 6435 	movx	a,@dptr
      00290B FE               [12] 6436 	mov	r6,a
      00290C A3               [24] 6437 	inc	dptr
      00290D E0               [24] 6438 	movx	a,@dptr
      00290E FF               [12] 6439 	mov	r7,a
      00290F 90 03 24         [24] 6440 	mov	dptr,#(_axradio_cb_transmitstart + 0x0006)
      002912 EC               [12] 6441 	mov	a,r4
      002913 F0               [24] 6442 	movx	@dptr,a
      002914 ED               [12] 6443 	mov	a,r5
      002915 A3               [24] 6444 	inc	dptr
      002916 F0               [24] 6445 	movx	@dptr,a
      002917 EE               [12] 6446 	mov	a,r6
      002918 A3               [24] 6447 	inc	dptr
      002919 F0               [24] 6448 	movx	@dptr,a
      00291A EF               [12] 6449 	mov	a,r7
      00291B A3               [24] 6450 	inc	dptr
      00291C F0               [24] 6451 	movx	@dptr,a
                                   6452 ;	..\src\COMMON\easyax5043.c:1128: wtimer_add_callback(&axradio_cb_transmitstart.cb);
      00291D 90 03 1E         [24] 6453 	mov	dptr,#_axradio_cb_transmitstart
                                   6454 ;	..\src\COMMON\easyax5043.c:1129: break;
      002920 02 66 0B         [24] 6455 	ljmp	_wtimer_add_callback
                                   6456 ;	..\src\COMMON\easyax5043.c:1131: case AXRADIO_MODE_SYNC_MASTER:
      002923                       6457 00139$:
                                   6458 ;	..\src\COMMON\easyax5043.c:1132: case AXRADIO_MODE_SYNC_ACK_MASTER:
      002923                       6459 00140$:
                                   6460 ;	..\src\COMMON\easyax5043.c:1133: switch (axradio_syncstate) {
      002923 90 00 B7         [24] 6461 	mov	dptr,#_axradio_syncstate
      002926 E0               [24] 6462 	movx	a,@dptr
      002927 FF               [12] 6463 	mov	r7,a
      002928 BF 04 02         [24] 6464 	cjne	r7,#0x04,00283$
      00292B 80 58            [24] 6465 	sjmp	00142$
      00292D                       6466 00283$:
      00292D BF 05 03         [24] 6467 	cjne	r7,#0x05,00284$
      002930 02 2A 5F         [24] 6468 	ljmp	00150$
      002933                       6469 00284$:
                                   6470 ;	..\src\COMMON\easyax5043.c:1135: AX5043_PWRMODE = AX5043_PWRSTATE_XTAL_ON;
      002933 90 40 02         [24] 6471 	mov	dptr,#_AX5043_PWRMODE
      002936 74 05            [12] 6472 	mov	a,#0x05
      002938 F0               [24] 6473 	movx	@dptr,a
                                   6474 ;	..\src\COMMON\easyax5043.c:1136: ax5043_init_registers_tx();
      002939 12 16 97         [24] 6475 	lcall	_ax5043_init_registers_tx
                                   6476 ;	..\src\COMMON\easyax5043.c:1137: axradio_syncstate = syncstate_master_xostartup;
      00293C 90 00 B7         [24] 6477 	mov	dptr,#_axradio_syncstate
      00293F 74 04            [12] 6478 	mov	a,#0x04
      002941 F0               [24] 6479 	movx	@dptr,a
                                   6480 ;	..\src\COMMON\easyax5043.c:1138: wtimer_remove_callback(&axradio_cb_transmitdata.cb);
      002942 90 03 32         [24] 6481 	mov	dptr,#_axradio_cb_transmitdata
      002945 12 74 C0         [24] 6482 	lcall	_wtimer_remove_callback
                                   6483 ;	..\src\COMMON\easyax5043.c:1139: axradio_cb_transmitdata.st.error = AXRADIO_ERR_NOERROR;
      002948 90 03 37         [24] 6484 	mov	dptr,#(_axradio_cb_transmitdata + 0x0005)
      00294B E4               [12] 6485 	clr	a
      00294C F0               [24] 6486 	movx	@dptr,a
                                   6487 ;	..\src\COMMON\easyax5043.c:1140: axradio_cb_transmitdata.st.time.t = 0;
      00294D 90 03 38         [24] 6488 	mov	dptr,#(_axradio_cb_transmitdata + 0x0006)
      002950 F0               [24] 6489 	movx	@dptr,a
      002951 A3               [24] 6490 	inc	dptr
      002952 F0               [24] 6491 	movx	@dptr,a
      002953 A3               [24] 6492 	inc	dptr
      002954 F0               [24] 6493 	movx	@dptr,a
      002955 A3               [24] 6494 	inc	dptr
      002956 F0               [24] 6495 	movx	@dptr,a
                                   6496 ;	..\src\COMMON\easyax5043.c:1141: wtimer_add_callback(&axradio_cb_transmitdata.cb);
      002957 90 03 32         [24] 6497 	mov	dptr,#_axradio_cb_transmitdata
      00295A 12 66 0B         [24] 6498 	lcall	_wtimer_add_callback
                                   6499 ;	..\src\COMMON\easyax5043.c:1142: wtimer_remove(&axradio_timer);
      00295D 90 03 3C         [24] 6500 	mov	dptr,#_axradio_timer
      002960 12 71 84         [24] 6501 	lcall	_wtimer_remove
                                   6502 ;	..\src\COMMON\easyax5043.c:1143: axradio_timer.time = axradio_sync_time;
      002963 90 00 C3         [24] 6503 	mov	dptr,#_axradio_sync_time
      002966 E0               [24] 6504 	movx	a,@dptr
      002967 FC               [12] 6505 	mov	r4,a
      002968 A3               [24] 6506 	inc	dptr
      002969 E0               [24] 6507 	movx	a,@dptr
      00296A FD               [12] 6508 	mov	r5,a
      00296B A3               [24] 6509 	inc	dptr
      00296C E0               [24] 6510 	movx	a,@dptr
      00296D FE               [12] 6511 	mov	r6,a
      00296E A3               [24] 6512 	inc	dptr
      00296F E0               [24] 6513 	movx	a,@dptr
      002970 FF               [12] 6514 	mov	r7,a
      002971 90 03 40         [24] 6515 	mov	dptr,#(_axradio_timer + 0x0004)
      002974 EC               [12] 6516 	mov	a,r4
      002975 F0               [24] 6517 	movx	@dptr,a
      002976 ED               [12] 6518 	mov	a,r5
      002977 A3               [24] 6519 	inc	dptr
      002978 F0               [24] 6520 	movx	@dptr,a
      002979 EE               [12] 6521 	mov	a,r6
      00297A A3               [24] 6522 	inc	dptr
      00297B F0               [24] 6523 	movx	@dptr,a
      00297C EF               [12] 6524 	mov	a,r7
      00297D A3               [24] 6525 	inc	dptr
      00297E F0               [24] 6526 	movx	@dptr,a
                                   6527 ;	..\src\COMMON\easyax5043.c:1144: wtimer0_addabsolute(&axradio_timer);
      00297F 90 03 3C         [24] 6528 	mov	dptr,#_axradio_timer
                                   6529 ;	..\src\COMMON\easyax5043.c:1145: break;
      002982 02 67 3B         [24] 6530 	ljmp	_wtimer0_addabsolute
                                   6531 ;	..\src\COMMON\easyax5043.c:1147: case syncstate_master_xostartup:
      002985                       6532 00142$:
                                   6533 ;	..\src\COMMON\easyax5043.c:1148: AX5043_FIFOSTAT = 3;
      002985 90 40 28         [24] 6534 	mov	dptr,#_AX5043_FIFOSTAT
      002988 74 03            [12] 6535 	mov	a,#0x03
      00298A F0               [24] 6536 	movx	@dptr,a
                                   6537 ;	..\src\COMMON\easyax5043.c:1149: AX5043_PWRMODE = AX5043_PWRSTATE_FULL_TX;
      00298B 90 40 02         [24] 6538 	mov	dptr,#_AX5043_PWRMODE
      00298E 74 0D            [12] 6539 	mov	a,#0x0d
      002990 F0               [24] 6540 	movx	@dptr,a
                                   6541 ;	..\src\COMMON\easyax5043.c:1150: while (!(AX5043_POWSTAT & 0x08)); // wait for modem vdd so writing the FIFO is safe
      002991                       6542 00143$:
      002991 90 40 03         [24] 6543 	mov	dptr,#_AX5043_POWSTAT
      002994 E0               [24] 6544 	movx	a,@dptr
      002995 FF               [12] 6545 	mov	r7,a
      002996 30 E3 F8         [24] 6546 	jnb	acc.3,00143$
                                   6547 ;	..\src\COMMON\easyax5043.c:1151: AX5043_RADIOEVENTREQ0; // make sure REVRDONE bit is cleared, so it is a reliable indicator that the packet is out
      002999 90 40 0F         [24] 6548 	mov	dptr,#_AX5043_RADIOEVENTREQ0
      00299C E0               [24] 6549 	movx	a,@dptr
                                   6550 ;	..\src\COMMON\easyax5043.c:1152: AX5043_FIFOTHRESH1 = 0;
      00299D 90 40 2E         [24] 6551 	mov	dptr,#_AX5043_FIFOTHRESH1
      0029A0 E4               [12] 6552 	clr	a
      0029A1 F0               [24] 6553 	movx	@dptr,a
                                   6554 ;	..\src\COMMON\easyax5043.c:1153: AX5043_FIFOTHRESH0 = 0x80;
      0029A2 90 40 2F         [24] 6555 	mov	dptr,#_AX5043_FIFOTHRESH0
      0029A5 74 80            [12] 6556 	mov	a,#0x80
      0029A7 F0               [24] 6557 	movx	@dptr,a
                                   6558 ;	..\src\COMMON\easyax5043.c:1154: axradio_trxstate = trxstate_tx_longpreamble;
      0029A8 75 0C 0A         [24] 6559 	mov	_axradio_trxstate,#0x0a
                                   6560 ;	..\src\COMMON\easyax5043.c:1155: axradio_txbuffer_cnt = axradio_phy_preamble_longlen;
      0029AB 90 7A D2         [24] 6561 	mov	dptr,#_axradio_phy_preamble_longlen
      0029AE E4               [12] 6562 	clr	a
      0029AF 93               [24] 6563 	movc	a,@a+dptr
      0029B0 FE               [12] 6564 	mov	r6,a
      0029B1 74 01            [12] 6565 	mov	a,#0x01
      0029B3 93               [24] 6566 	movc	a,@a+dptr
      0029B4 FF               [12] 6567 	mov	r7,a
      0029B5 90 00 BA         [24] 6568 	mov	dptr,#_axradio_txbuffer_cnt
      0029B8 EE               [12] 6569 	mov	a,r6
      0029B9 F0               [24] 6570 	movx	@dptr,a
      0029BA EF               [12] 6571 	mov	a,r7
      0029BB A3               [24] 6572 	inc	dptr
      0029BC F0               [24] 6573 	movx	@dptr,a
                                   6574 ;	..\src\COMMON\easyax5043.c:1157: if ((AX5043_MODULATION & 0x0F) == 9) { // 4-FSK
      0029BD 90 40 10         [24] 6575 	mov	dptr,#_AX5043_MODULATION
      0029C0 E0               [24] 6576 	movx	a,@dptr
      0029C1 FF               [12] 6577 	mov	r7,a
      0029C2 53 07 0F         [24] 6578 	anl	ar7,#0x0f
      0029C5 BF 09 0E         [24] 6579 	cjne	r7,#0x09,00147$
                                   6580 ;	..\src\COMMON\easyax5043.c:1158: AX5043_FIFODATA = AX5043_FIFOCMD_DATA | (7 << 5);
      0029C8 90 40 29         [24] 6581 	mov	dptr,#_AX5043_FIFODATA
      0029CB 74 E1            [12] 6582 	mov	a,#0xe1
      0029CD F0               [24] 6583 	movx	@dptr,a
                                   6584 ;	..\src\COMMON\easyax5043.c:1159: AX5043_FIFODATA = 2;  // length (including flags)
      0029CE 74 02            [12] 6585 	mov	a,#0x02
      0029D0 F0               [24] 6586 	movx	@dptr,a
                                   6587 ;	..\src\COMMON\easyax5043.c:1160: AX5043_FIFODATA = 0x01;  // flag PKTSTART -> dibit sync
      0029D1 14               [12] 6588 	dec	a
      0029D2 F0               [24] 6589 	movx	@dptr,a
                                   6590 ;	..\src\COMMON\easyax5043.c:1161: AX5043_FIFODATA = 0x11; // dummy byte for forcing dibit sync
      0029D3 74 11            [12] 6591 	mov	a,#0x11
      0029D5 F0               [24] 6592 	movx	@dptr,a
      0029D6                       6593 00147$:
                                   6594 ;	..\src\COMMON\easyax5043.c:1168: wtimer_remove(&axradio_timer);
      0029D6 90 03 3C         [24] 6595 	mov	dptr,#_axradio_timer
      0029D9 12 71 84         [24] 6596 	lcall	_wtimer_remove
                                   6597 ;	..\src\COMMON\easyax5043.c:1169: update_timeanchor();
      0029DC 12 15 BA         [24] 6598 	lcall	_update_timeanchor
                                   6599 ;	..\src\COMMON\easyax5043.c:1170: AX5043_IRQMASK0 = 0x08; // enable fifo free threshold
      0029DF 90 40 07         [24] 6600 	mov	dptr,#_AX5043_IRQMASK0
      0029E2 74 08            [12] 6601 	mov	a,#0x08
      0029E4 F0               [24] 6602 	movx	@dptr,a
                                   6603 ;	..\src\COMMON\easyax5043.c:1171: axradio_sync_addtime(axradio_sync_period);
      0029E5 90 7A F6         [24] 6604 	mov	dptr,#_axradio_sync_period
      0029E8 E4               [12] 6605 	clr	a
      0029E9 93               [24] 6606 	movc	a,@a+dptr
      0029EA FC               [12] 6607 	mov	r4,a
      0029EB 74 01            [12] 6608 	mov	a,#0x01
      0029ED 93               [24] 6609 	movc	a,@a+dptr
      0029EE FD               [12] 6610 	mov	r5,a
      0029EF 74 02            [12] 6611 	mov	a,#0x02
      0029F1 93               [24] 6612 	movc	a,@a+dptr
      0029F2 FE               [12] 6613 	mov	r6,a
      0029F3 74 03            [12] 6614 	mov	a,#0x03
      0029F5 93               [24] 6615 	movc	a,@a+dptr
      0029F6 8C 82            [24] 6616 	mov	dpl,r4
      0029F8 8D 83            [24] 6617 	mov	dph,r5
      0029FA 8E F0            [24] 6618 	mov	b,r6
      0029FC 12 24 70         [24] 6619 	lcall	_axradio_sync_addtime
                                   6620 ;	..\src\COMMON\easyax5043.c:1172: axradio_syncstate = syncstate_master_waitack;
      0029FF 90 00 B7         [24] 6621 	mov	dptr,#_axradio_syncstate
      002A02 74 05            [12] 6622 	mov	a,#0x05
      002A04 F0               [24] 6623 	movx	@dptr,a
                                   6624 ;	..\src\COMMON\easyax5043.c:1173: if (axradio_mode != AXRADIO_MODE_SYNC_ACK_MASTER) {
      002A05 74 31            [12] 6625 	mov	a,#0x31
      002A07 B5 0B 02         [24] 6626 	cjne	a,_axradio_mode,00288$
      002A0A 80 26            [24] 6627 	sjmp	00149$
      002A0C                       6628 00288$:
                                   6629 ;	..\src\COMMON\easyax5043.c:1174: axradio_syncstate = syncstate_master_normal;
      002A0C 90 00 B7         [24] 6630 	mov	dptr,#_axradio_syncstate
      002A0F 74 03            [12] 6631 	mov	a,#0x03
      002A11 F0               [24] 6632 	movx	@dptr,a
                                   6633 ;	..\src\COMMON\easyax5043.c:1175: axradio_sync_settimeradv(axradio_sync_xoscstartup);
      002A12 90 7A FA         [24] 6634 	mov	dptr,#_axradio_sync_xoscstartup
      002A15 E4               [12] 6635 	clr	a
      002A16 93               [24] 6636 	movc	a,@a+dptr
      002A17 FC               [12] 6637 	mov	r4,a
      002A18 74 01            [12] 6638 	mov	a,#0x01
      002A1A 93               [24] 6639 	movc	a,@a+dptr
      002A1B FD               [12] 6640 	mov	r5,a
      002A1C 74 02            [12] 6641 	mov	a,#0x02
      002A1E 93               [24] 6642 	movc	a,@a+dptr
      002A1F FE               [12] 6643 	mov	r6,a
      002A20 74 03            [12] 6644 	mov	a,#0x03
      002A22 93               [24] 6645 	movc	a,@a+dptr
      002A23 8C 82            [24] 6646 	mov	dpl,r4
      002A25 8D 83            [24] 6647 	mov	dph,r5
      002A27 8E F0            [24] 6648 	mov	b,r6
      002A29 12 24 C1         [24] 6649 	lcall	_axradio_sync_settimeradv
                                   6650 ;	..\src\COMMON\easyax5043.c:1176: wtimer0_addabsolute(&axradio_timer);
      002A2C 90 03 3C         [24] 6651 	mov	dptr,#_axradio_timer
      002A2F 12 67 3B         [24] 6652 	lcall	_wtimer0_addabsolute
      002A32                       6653 00149$:
                                   6654 ;	..\src\COMMON\easyax5043.c:1178: wtimer_remove_callback(&axradio_cb_transmitstart.cb);
      002A32 90 03 1E         [24] 6655 	mov	dptr,#_axradio_cb_transmitstart
      002A35 12 74 C0         [24] 6656 	lcall	_wtimer_remove_callback
                                   6657 ;	..\src\COMMON\easyax5043.c:1179: axradio_cb_transmitstart.st.error = AXRADIO_ERR_NOERROR;
      002A38 90 03 23         [24] 6658 	mov	dptr,#(_axradio_cb_transmitstart + 0x0005)
      002A3B E4               [12] 6659 	clr	a
      002A3C F0               [24] 6660 	movx	@dptr,a
                                   6661 ;	..\src\COMMON\easyax5043.c:1180: axradio_cb_transmitstart.st.time.t = axradio_timeanchor.radiotimer;
      002A3D 90 00 CD         [24] 6662 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      002A40 E0               [24] 6663 	movx	a,@dptr
      002A41 FC               [12] 6664 	mov	r4,a
      002A42 A3               [24] 6665 	inc	dptr
      002A43 E0               [24] 6666 	movx	a,@dptr
      002A44 FD               [12] 6667 	mov	r5,a
      002A45 A3               [24] 6668 	inc	dptr
      002A46 E0               [24] 6669 	movx	a,@dptr
      002A47 FE               [12] 6670 	mov	r6,a
      002A48 A3               [24] 6671 	inc	dptr
      002A49 E0               [24] 6672 	movx	a,@dptr
      002A4A FF               [12] 6673 	mov	r7,a
      002A4B 90 03 24         [24] 6674 	mov	dptr,#(_axradio_cb_transmitstart + 0x0006)
      002A4E EC               [12] 6675 	mov	a,r4
      002A4F F0               [24] 6676 	movx	@dptr,a
      002A50 ED               [12] 6677 	mov	a,r5
      002A51 A3               [24] 6678 	inc	dptr
      002A52 F0               [24] 6679 	movx	@dptr,a
      002A53 EE               [12] 6680 	mov	a,r6
      002A54 A3               [24] 6681 	inc	dptr
      002A55 F0               [24] 6682 	movx	@dptr,a
      002A56 EF               [12] 6683 	mov	a,r7
      002A57 A3               [24] 6684 	inc	dptr
      002A58 F0               [24] 6685 	movx	@dptr,a
                                   6686 ;	..\src\COMMON\easyax5043.c:1181: wtimer_add_callback(&axradio_cb_transmitstart.cb);
      002A59 90 03 1E         [24] 6687 	mov	dptr,#_axradio_cb_transmitstart
                                   6688 ;	..\src\COMMON\easyax5043.c:1182: break;
      002A5C 02 66 0B         [24] 6689 	ljmp	_wtimer_add_callback
                                   6690 ;	..\src\COMMON\easyax5043.c:1184: case syncstate_master_waitack:
      002A5F                       6691 00150$:
                                   6692 ;	..\src\COMMON\easyax5043.c:1185: ax5043_off();
      002A5F 12 22 CE         [24] 6693 	lcall	_ax5043_off
                                   6694 ;	..\src\COMMON\easyax5043.c:1186: axradio_syncstate = syncstate_master_normal;
      002A62 90 00 B7         [24] 6695 	mov	dptr,#_axradio_syncstate
      002A65 74 03            [12] 6696 	mov	a,#0x03
      002A67 F0               [24] 6697 	movx	@dptr,a
                                   6698 ;	..\src\COMMON\easyax5043.c:1187: wtimer_remove(&axradio_timer);
      002A68 90 03 3C         [24] 6699 	mov	dptr,#_axradio_timer
      002A6B 12 71 84         [24] 6700 	lcall	_wtimer_remove
                                   6701 ;	..\src\COMMON\easyax5043.c:1188: axradio_sync_settimeradv(axradio_sync_xoscstartup);
      002A6E 90 7A FA         [24] 6702 	mov	dptr,#_axradio_sync_xoscstartup
      002A71 E4               [12] 6703 	clr	a
      002A72 93               [24] 6704 	movc	a,@a+dptr
      002A73 FC               [12] 6705 	mov	r4,a
      002A74 74 01            [12] 6706 	mov	a,#0x01
      002A76 93               [24] 6707 	movc	a,@a+dptr
      002A77 FD               [12] 6708 	mov	r5,a
      002A78 74 02            [12] 6709 	mov	a,#0x02
      002A7A 93               [24] 6710 	movc	a,@a+dptr
      002A7B FE               [12] 6711 	mov	r6,a
      002A7C 74 03            [12] 6712 	mov	a,#0x03
      002A7E 93               [24] 6713 	movc	a,@a+dptr
      002A7F 8C 82            [24] 6714 	mov	dpl,r4
      002A81 8D 83            [24] 6715 	mov	dph,r5
      002A83 8E F0            [24] 6716 	mov	b,r6
      002A85 12 24 C1         [24] 6717 	lcall	_axradio_sync_settimeradv
                                   6718 ;	..\src\COMMON\easyax5043.c:1189: wtimer0_addabsolute(&axradio_timer);
      002A88 90 03 3C         [24] 6719 	mov	dptr,#_axradio_timer
      002A8B 12 67 3B         [24] 6720 	lcall	_wtimer0_addabsolute
                                   6721 ;	..\src\COMMON\easyax5043.c:1190: update_timeanchor();
      002A8E 12 15 BA         [24] 6722 	lcall	_update_timeanchor
                                   6723 ;	..\src\COMMON\easyax5043.c:1191: wtimer_remove_callback(&axradio_cb_transmitend.cb);
      002A91 90 03 28         [24] 6724 	mov	dptr,#_axradio_cb_transmitend
      002A94 12 74 C0         [24] 6725 	lcall	_wtimer_remove_callback
                                   6726 ;	..\src\COMMON\easyax5043.c:1192: axradio_cb_transmitend.st.error = AXRADIO_ERR_TIMEOUT;
      002A97 90 03 2D         [24] 6727 	mov	dptr,#(_axradio_cb_transmitend + 0x0005)
      002A9A 74 03            [12] 6728 	mov	a,#0x03
      002A9C F0               [24] 6729 	movx	@dptr,a
                                   6730 ;	..\src\COMMON\easyax5043.c:1193: axradio_cb_transmitend.st.time.t = axradio_timeanchor.radiotimer;
      002A9D 90 00 CD         [24] 6731 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      002AA0 E0               [24] 6732 	movx	a,@dptr
      002AA1 FC               [12] 6733 	mov	r4,a
      002AA2 A3               [24] 6734 	inc	dptr
      002AA3 E0               [24] 6735 	movx	a,@dptr
      002AA4 FD               [12] 6736 	mov	r5,a
      002AA5 A3               [24] 6737 	inc	dptr
      002AA6 E0               [24] 6738 	movx	a,@dptr
      002AA7 FE               [12] 6739 	mov	r6,a
      002AA8 A3               [24] 6740 	inc	dptr
      002AA9 E0               [24] 6741 	movx	a,@dptr
      002AAA FF               [12] 6742 	mov	r7,a
      002AAB 90 03 2E         [24] 6743 	mov	dptr,#(_axradio_cb_transmitend + 0x0006)
      002AAE EC               [12] 6744 	mov	a,r4
      002AAF F0               [24] 6745 	movx	@dptr,a
      002AB0 ED               [12] 6746 	mov	a,r5
      002AB1 A3               [24] 6747 	inc	dptr
      002AB2 F0               [24] 6748 	movx	@dptr,a
      002AB3 EE               [12] 6749 	mov	a,r6
      002AB4 A3               [24] 6750 	inc	dptr
      002AB5 F0               [24] 6751 	movx	@dptr,a
      002AB6 EF               [12] 6752 	mov	a,r7
      002AB7 A3               [24] 6753 	inc	dptr
      002AB8 F0               [24] 6754 	movx	@dptr,a
                                   6755 ;	..\src\COMMON\easyax5043.c:1194: wtimer_add_callback(&axradio_cb_transmitend.cb);
      002AB9 90 03 28         [24] 6756 	mov	dptr,#_axradio_cb_transmitend
                                   6757 ;	..\src\COMMON\easyax5043.c:1197: break;
      002ABC 02 66 0B         [24] 6758 	ljmp	_wtimer_add_callback
                                   6759 ;	..\src\COMMON\easyax5043.c:1199: case AXRADIO_MODE_SYNC_SLAVE:
      002ABF                       6760 00152$:
                                   6761 ;	..\src\COMMON\easyax5043.c:1200: case AXRADIO_MODE_SYNC_ACK_SLAVE:
      002ABF                       6762 00153$:
                                   6763 ;	..\src\COMMON\easyax5043.c:1201: switch (axradio_syncstate) {
      002ABF 90 00 B7         [24] 6764 	mov	dptr,#_axradio_syncstate
      002AC2 E0               [24] 6765 	movx	a,@dptr
      002AC3 FF               [12] 6766 	mov  r7,a
      002AC4 24 F3            [12] 6767 	add	a,#0xff - 0x0c
      002AC6 50 03            [24] 6768 	jnc	00289$
      002AC8 02 2A F6         [24] 6769 	ljmp	00155$
      002ACB                       6770 00289$:
      002ACB EF               [12] 6771 	mov	a,r7
      002ACC F5 F0            [12] 6772 	mov	b,a
      002ACE 24 0B            [12] 6773 	add	a,#(00290$-3-.)
      002AD0 83               [24] 6774 	movc	a,@a+pc
      002AD1 F5 82            [12] 6775 	mov	dpl,a
      002AD3 E5 F0            [12] 6776 	mov	a,b
      002AD5 24 11            [12] 6777 	add	a,#(00291$-3-.)
      002AD7 83               [24] 6778 	movc	a,@a+pc
      002AD8 F5 83            [12] 6779 	mov	dph,a
      002ADA E4               [12] 6780 	clr	a
      002ADB 73               [24] 6781 	jmp	@a+dptr
      002ADC                       6782 00290$:
      002ADC F6                    6783 	.db	00154$
      002ADD F6                    6784 	.db	00154$
      002ADE F6                    6785 	.db	00154$
      002ADF F6                    6786 	.db	00154$
      002AE0 F6                    6787 	.db	00154$
      002AE1 F6                    6788 	.db	00154$
      002AE2 F6                    6789 	.db	00155$
      002AE3 86                    6790 	.db	00156$
      002AE4 19                    6791 	.db	00157$
      002AE5 6B                    6792 	.db	00158$
      002AE6 21                    6793 	.db	00161$
      002AE7 7F                    6794 	.db	00166$
      002AE8 98                    6795 	.db	00173$
      002AE9                       6796 00291$:
      002AE9 2A                    6797 	.db	00154$>>8
      002AEA 2A                    6798 	.db	00154$>>8
      002AEB 2A                    6799 	.db	00154$>>8
      002AEC 2A                    6800 	.db	00154$>>8
      002AED 2A                    6801 	.db	00154$>>8
      002AEE 2A                    6802 	.db	00154$>>8
      002AEF 2A                    6803 	.db	00155$>>8
      002AF0 2B                    6804 	.db	00156$>>8
      002AF1 2C                    6805 	.db	00157$>>8
      002AF2 2C                    6806 	.db	00158$>>8
      002AF3 2D                    6807 	.db	00161$>>8
      002AF4 2D                    6808 	.db	00166$>>8
      002AF5 2E                    6809 	.db	00173$>>8
                                   6810 ;	..\src\COMMON\easyax5043.c:1202: default:
      002AF6                       6811 00154$:
                                   6812 ;	..\src\COMMON\easyax5043.c:1203: case syncstate_slave_synchunt:
      002AF6                       6813 00155$:
                                   6814 ;	..\src\COMMON\easyax5043.c:1204: ax5043_off();
      002AF6 12 22 CE         [24] 6815 	lcall	_ax5043_off
                                   6816 ;	..\src\COMMON\easyax5043.c:1205: axradio_syncstate = syncstate_slave_syncpause;
      002AF9 90 00 B7         [24] 6817 	mov	dptr,#_axradio_syncstate
      002AFC 74 07            [12] 6818 	mov	a,#0x07
      002AFE F0               [24] 6819 	movx	@dptr,a
                                   6820 ;	..\src\COMMON\easyax5043.c:1206: axradio_sync_addtime(axradio_sync_slave_syncpause);
      002AFF 90 7B 06         [24] 6821 	mov	dptr,#_axradio_sync_slave_syncpause
      002B02 E4               [12] 6822 	clr	a
      002B03 93               [24] 6823 	movc	a,@a+dptr
      002B04 FC               [12] 6824 	mov	r4,a
      002B05 74 01            [12] 6825 	mov	a,#0x01
      002B07 93               [24] 6826 	movc	a,@a+dptr
      002B08 FD               [12] 6827 	mov	r5,a
      002B09 74 02            [12] 6828 	mov	a,#0x02
      002B0B 93               [24] 6829 	movc	a,@a+dptr
      002B0C FE               [12] 6830 	mov	r6,a
      002B0D 74 03            [12] 6831 	mov	a,#0x03
      002B0F 93               [24] 6832 	movc	a,@a+dptr
      002B10 8C 82            [24] 6833 	mov	dpl,r4
      002B12 8D 83            [24] 6834 	mov	dph,r5
      002B14 8E F0            [24] 6835 	mov	b,r6
      002B16 12 24 70         [24] 6836 	lcall	_axradio_sync_addtime
                                   6837 ;	..\src\COMMON\easyax5043.c:1207: wtimer_remove(&axradio_timer);
      002B19 90 03 3C         [24] 6838 	mov	dptr,#_axradio_timer
      002B1C 12 71 84         [24] 6839 	lcall	_wtimer_remove
                                   6840 ;	..\src\COMMON\easyax5043.c:1208: axradio_timer.time = axradio_sync_time;
      002B1F 90 00 C3         [24] 6841 	mov	dptr,#_axradio_sync_time
      002B22 E0               [24] 6842 	movx	a,@dptr
      002B23 FC               [12] 6843 	mov	r4,a
      002B24 A3               [24] 6844 	inc	dptr
      002B25 E0               [24] 6845 	movx	a,@dptr
      002B26 FD               [12] 6846 	mov	r5,a
      002B27 A3               [24] 6847 	inc	dptr
      002B28 E0               [24] 6848 	movx	a,@dptr
      002B29 FE               [12] 6849 	mov	r6,a
      002B2A A3               [24] 6850 	inc	dptr
      002B2B E0               [24] 6851 	movx	a,@dptr
      002B2C FF               [12] 6852 	mov	r7,a
      002B2D 90 03 40         [24] 6853 	mov	dptr,#(_axradio_timer + 0x0004)
      002B30 EC               [12] 6854 	mov	a,r4
      002B31 F0               [24] 6855 	movx	@dptr,a
      002B32 ED               [12] 6856 	mov	a,r5
      002B33 A3               [24] 6857 	inc	dptr
      002B34 F0               [24] 6858 	movx	@dptr,a
      002B35 EE               [12] 6859 	mov	a,r6
      002B36 A3               [24] 6860 	inc	dptr
      002B37 F0               [24] 6861 	movx	@dptr,a
      002B38 EF               [12] 6862 	mov	a,r7
      002B39 A3               [24] 6863 	inc	dptr
      002B3A F0               [24] 6864 	movx	@dptr,a
                                   6865 ;	..\src\COMMON\easyax5043.c:1209: wtimer0_addabsolute(&axradio_timer);
      002B3B 90 03 3C         [24] 6866 	mov	dptr,#_axradio_timer
      002B3E 12 67 3B         [24] 6867 	lcall	_wtimer0_addabsolute
                                   6868 ;	..\src\COMMON\easyax5043.c:1210: wtimer_remove_callback(&axradio_cb_receive.cb);
      002B41 90 02 E5         [24] 6869 	mov	dptr,#_axradio_cb_receive
      002B44 12 74 C0         [24] 6870 	lcall	_wtimer_remove_callback
                                   6871 ;	..\src\COMMON\easyax5043.c:1211: memset_xdata(&axradio_cb_receive.st, 0, sizeof(axradio_cb_receive.st));
      002B47 90 03 CD         [24] 6872 	mov	dptr,#_memset_PARM_2
      002B4A E4               [12] 6873 	clr	a
      002B4B F0               [24] 6874 	movx	@dptr,a
      002B4C 90 03 CE         [24] 6875 	mov	dptr,#_memset_PARM_3
      002B4F 74 1E            [12] 6876 	mov	a,#0x1e
      002B51 F0               [24] 6877 	movx	@dptr,a
      002B52 E4               [12] 6878 	clr	a
      002B53 A3               [24] 6879 	inc	dptr
      002B54 F0               [24] 6880 	movx	@dptr,a
      002B55 90 02 E9         [24] 6881 	mov	dptr,#(_axradio_cb_receive + 0x0004)
      002B58 75 F0 00         [24] 6882 	mov	b,#0x00
      002B5B 12 62 A1         [24] 6883 	lcall	_memset
                                   6884 ;	..\src\COMMON\easyax5043.c:1212: axradio_cb_receive.st.time.t = axradio_timeanchor.radiotimer;
      002B5E 90 00 CD         [24] 6885 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      002B61 E0               [24] 6886 	movx	a,@dptr
      002B62 FC               [12] 6887 	mov	r4,a
      002B63 A3               [24] 6888 	inc	dptr
      002B64 E0               [24] 6889 	movx	a,@dptr
      002B65 FD               [12] 6890 	mov	r5,a
      002B66 A3               [24] 6891 	inc	dptr
      002B67 E0               [24] 6892 	movx	a,@dptr
      002B68 FE               [12] 6893 	mov	r6,a
      002B69 A3               [24] 6894 	inc	dptr
      002B6A E0               [24] 6895 	movx	a,@dptr
      002B6B FF               [12] 6896 	mov	r7,a
      002B6C 90 02 EB         [24] 6897 	mov	dptr,#(_axradio_cb_receive + 0x0006)
      002B6F EC               [12] 6898 	mov	a,r4
      002B70 F0               [24] 6899 	movx	@dptr,a
      002B71 ED               [12] 6900 	mov	a,r5
      002B72 A3               [24] 6901 	inc	dptr
      002B73 F0               [24] 6902 	movx	@dptr,a
      002B74 EE               [12] 6903 	mov	a,r6
      002B75 A3               [24] 6904 	inc	dptr
      002B76 F0               [24] 6905 	movx	@dptr,a
      002B77 EF               [12] 6906 	mov	a,r7
      002B78 A3               [24] 6907 	inc	dptr
      002B79 F0               [24] 6908 	movx	@dptr,a
                                   6909 ;	..\src\COMMON\easyax5043.c:1213: axradio_cb_receive.st.error = AXRADIO_ERR_RESYNCTIMEOUT;
      002B7A 90 02 EA         [24] 6910 	mov	dptr,#(_axradio_cb_receive + 0x0005)
      002B7D 74 0A            [12] 6911 	mov	a,#0x0a
      002B7F F0               [24] 6912 	movx	@dptr,a
                                   6913 ;	..\src\COMMON\easyax5043.c:1214: wtimer_add_callback(&axradio_cb_receive.cb);
      002B80 90 02 E5         [24] 6914 	mov	dptr,#_axradio_cb_receive
                                   6915 ;	..\src\COMMON\easyax5043.c:1215: break;
      002B83 02 66 0B         [24] 6916 	ljmp	_wtimer_add_callback
                                   6917 ;	..\src\COMMON\easyax5043.c:1217: case syncstate_slave_syncpause:
      002B86                       6918 00156$:
                                   6919 ;	..\src\COMMON\easyax5043.c:1218: ax5043_receiver_on_continuous();
      002B86 12 21 77         [24] 6920 	lcall	_ax5043_receiver_on_continuous
                                   6921 ;	..\src\COMMON\easyax5043.c:1219: axradio_syncstate = syncstate_slave_synchunt;
      002B89 90 00 B7         [24] 6922 	mov	dptr,#_axradio_syncstate
      002B8C 74 06            [12] 6923 	mov	a,#0x06
      002B8E F0               [24] 6924 	movx	@dptr,a
                                   6925 ;	..\src\COMMON\easyax5043.c:1220: axradio_sync_addtime(axradio_sync_slave_syncwindow);
      002B8F 90 7A FE         [24] 6926 	mov	dptr,#_axradio_sync_slave_syncwindow
      002B92 E4               [12] 6927 	clr	a
      002B93 93               [24] 6928 	movc	a,@a+dptr
      002B94 FC               [12] 6929 	mov	r4,a
      002B95 74 01            [12] 6930 	mov	a,#0x01
      002B97 93               [24] 6931 	movc	a,@a+dptr
      002B98 FD               [12] 6932 	mov	r5,a
      002B99 74 02            [12] 6933 	mov	a,#0x02
      002B9B 93               [24] 6934 	movc	a,@a+dptr
      002B9C FE               [12] 6935 	mov	r6,a
      002B9D 74 03            [12] 6936 	mov	a,#0x03
      002B9F 93               [24] 6937 	movc	a,@a+dptr
      002BA0 8C 82            [24] 6938 	mov	dpl,r4
      002BA2 8D 83            [24] 6939 	mov	dph,r5
      002BA4 8E F0            [24] 6940 	mov	b,r6
      002BA6 12 24 70         [24] 6941 	lcall	_axradio_sync_addtime
                                   6942 ;	..\src\COMMON\easyax5043.c:1221: wtimer_remove(&axradio_timer);
      002BA9 90 03 3C         [24] 6943 	mov	dptr,#_axradio_timer
      002BAC 12 71 84         [24] 6944 	lcall	_wtimer_remove
                                   6945 ;	..\src\COMMON\easyax5043.c:1222: axradio_timer.time = axradio_sync_time;
      002BAF 90 00 C3         [24] 6946 	mov	dptr,#_axradio_sync_time
      002BB2 E0               [24] 6947 	movx	a,@dptr
      002BB3 FC               [12] 6948 	mov	r4,a
      002BB4 A3               [24] 6949 	inc	dptr
      002BB5 E0               [24] 6950 	movx	a,@dptr
      002BB6 FD               [12] 6951 	mov	r5,a
      002BB7 A3               [24] 6952 	inc	dptr
      002BB8 E0               [24] 6953 	movx	a,@dptr
      002BB9 FE               [12] 6954 	mov	r6,a
      002BBA A3               [24] 6955 	inc	dptr
      002BBB E0               [24] 6956 	movx	a,@dptr
      002BBC FF               [12] 6957 	mov	r7,a
      002BBD 90 03 40         [24] 6958 	mov	dptr,#(_axradio_timer + 0x0004)
      002BC0 EC               [12] 6959 	mov	a,r4
      002BC1 F0               [24] 6960 	movx	@dptr,a
      002BC2 ED               [12] 6961 	mov	a,r5
      002BC3 A3               [24] 6962 	inc	dptr
      002BC4 F0               [24] 6963 	movx	@dptr,a
      002BC5 EE               [12] 6964 	mov	a,r6
      002BC6 A3               [24] 6965 	inc	dptr
      002BC7 F0               [24] 6966 	movx	@dptr,a
      002BC8 EF               [12] 6967 	mov	a,r7
      002BC9 A3               [24] 6968 	inc	dptr
      002BCA F0               [24] 6969 	movx	@dptr,a
                                   6970 ;	..\src\COMMON\easyax5043.c:1223: wtimer0_addabsolute(&axradio_timer);
      002BCB 90 03 3C         [24] 6971 	mov	dptr,#_axradio_timer
      002BCE 12 67 3B         [24] 6972 	lcall	_wtimer0_addabsolute
                                   6973 ;	..\src\COMMON\easyax5043.c:1224: update_timeanchor();
      002BD1 12 15 BA         [24] 6974 	lcall	_update_timeanchor
                                   6975 ;	..\src\COMMON\easyax5043.c:1225: wtimer_remove_callback(&axradio_cb_receive.cb);
      002BD4 90 02 E5         [24] 6976 	mov	dptr,#_axradio_cb_receive
      002BD7 12 74 C0         [24] 6977 	lcall	_wtimer_remove_callback
                                   6978 ;	..\src\COMMON\easyax5043.c:1226: memset_xdata(&axradio_cb_receive.st, 0, sizeof(axradio_cb_receive.st));
      002BDA 90 03 CD         [24] 6979 	mov	dptr,#_memset_PARM_2
      002BDD E4               [12] 6980 	clr	a
      002BDE F0               [24] 6981 	movx	@dptr,a
      002BDF 90 03 CE         [24] 6982 	mov	dptr,#_memset_PARM_3
      002BE2 74 1E            [12] 6983 	mov	a,#0x1e
      002BE4 F0               [24] 6984 	movx	@dptr,a
      002BE5 E4               [12] 6985 	clr	a
      002BE6 A3               [24] 6986 	inc	dptr
      002BE7 F0               [24] 6987 	movx	@dptr,a
      002BE8 90 02 E9         [24] 6988 	mov	dptr,#(_axradio_cb_receive + 0x0004)
      002BEB 75 F0 00         [24] 6989 	mov	b,#0x00
      002BEE 12 62 A1         [24] 6990 	lcall	_memset
                                   6991 ;	..\src\COMMON\easyax5043.c:1227: axradio_cb_receive.st.time.t = axradio_timeanchor.radiotimer;
      002BF1 90 00 CD         [24] 6992 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      002BF4 E0               [24] 6993 	movx	a,@dptr
      002BF5 FC               [12] 6994 	mov	r4,a
      002BF6 A3               [24] 6995 	inc	dptr
      002BF7 E0               [24] 6996 	movx	a,@dptr
      002BF8 FD               [12] 6997 	mov	r5,a
      002BF9 A3               [24] 6998 	inc	dptr
      002BFA E0               [24] 6999 	movx	a,@dptr
      002BFB FE               [12] 7000 	mov	r6,a
      002BFC A3               [24] 7001 	inc	dptr
      002BFD E0               [24] 7002 	movx	a,@dptr
      002BFE FF               [12] 7003 	mov	r7,a
      002BFF 90 02 EB         [24] 7004 	mov	dptr,#(_axradio_cb_receive + 0x0006)
      002C02 EC               [12] 7005 	mov	a,r4
      002C03 F0               [24] 7006 	movx	@dptr,a
      002C04 ED               [12] 7007 	mov	a,r5
      002C05 A3               [24] 7008 	inc	dptr
      002C06 F0               [24] 7009 	movx	@dptr,a
      002C07 EE               [12] 7010 	mov	a,r6
      002C08 A3               [24] 7011 	inc	dptr
      002C09 F0               [24] 7012 	movx	@dptr,a
      002C0A EF               [12] 7013 	mov	a,r7
      002C0B A3               [24] 7014 	inc	dptr
      002C0C F0               [24] 7015 	movx	@dptr,a
                                   7016 ;	..\src\COMMON\easyax5043.c:1228: axradio_cb_receive.st.error = AXRADIO_ERR_RESYNC;
      002C0D 90 02 EA         [24] 7017 	mov	dptr,#(_axradio_cb_receive + 0x0005)
      002C10 74 09            [12] 7018 	mov	a,#0x09
      002C12 F0               [24] 7019 	movx	@dptr,a
                                   7020 ;	..\src\COMMON\easyax5043.c:1229: wtimer_add_callback(&axradio_cb_receive.cb);
      002C13 90 02 E5         [24] 7021 	mov	dptr,#_axradio_cb_receive
                                   7022 ;	..\src\COMMON\easyax5043.c:1230: break;
      002C16 02 66 0B         [24] 7023 	ljmp	_wtimer_add_callback
                                   7024 ;	..\src\COMMON\easyax5043.c:1232: case syncstate_slave_rxidle:
      002C19                       7025 00157$:
                                   7026 ;	..\src\COMMON\easyax5043.c:1233: AX5043_PWRMODE = AX5043_PWRSTATE_XTAL_ON;
      002C19 90 40 02         [24] 7027 	mov	dptr,#_AX5043_PWRMODE
      002C1C 74 05            [12] 7028 	mov	a,#0x05
      002C1E F0               [24] 7029 	movx	@dptr,a
                                   7030 ;	..\src\COMMON\easyax5043.c:1234: axradio_syncstate = syncstate_slave_rxxosc;
      002C1F 90 00 B7         [24] 7031 	mov	dptr,#_axradio_syncstate
      002C22 74 09            [12] 7032 	mov	a,#0x09
      002C24 F0               [24] 7033 	movx	@dptr,a
                                   7034 ;	..\src\COMMON\easyax5043.c:1235: wtimer_remove(&axradio_timer);
      002C25 90 03 3C         [24] 7035 	mov	dptr,#_axradio_timer
      002C28 12 71 84         [24] 7036 	lcall	_wtimer_remove
                                   7037 ;	..\src\COMMON\easyax5043.c:1236: axradio_timer.time += axradio_sync_xoscstartup;
      002C2B 90 03 40         [24] 7038 	mov	dptr,#(_axradio_timer + 0x0004)
      002C2E E0               [24] 7039 	movx	a,@dptr
      002C2F FC               [12] 7040 	mov	r4,a
      002C30 A3               [24] 7041 	inc	dptr
      002C31 E0               [24] 7042 	movx	a,@dptr
      002C32 FD               [12] 7043 	mov	r5,a
      002C33 A3               [24] 7044 	inc	dptr
      002C34 E0               [24] 7045 	movx	a,@dptr
      002C35 FE               [12] 7046 	mov	r6,a
      002C36 A3               [24] 7047 	inc	dptr
      002C37 E0               [24] 7048 	movx	a,@dptr
      002C38 FF               [12] 7049 	mov	r7,a
      002C39 90 7A FA         [24] 7050 	mov	dptr,#_axradio_sync_xoscstartup
      002C3C E4               [12] 7051 	clr	a
      002C3D 93               [24] 7052 	movc	a,@a+dptr
      002C3E F8               [12] 7053 	mov	r0,a
      002C3F 74 01            [12] 7054 	mov	a,#0x01
      002C41 93               [24] 7055 	movc	a,@a+dptr
      002C42 F9               [12] 7056 	mov	r1,a
      002C43 74 02            [12] 7057 	mov	a,#0x02
      002C45 93               [24] 7058 	movc	a,@a+dptr
      002C46 FA               [12] 7059 	mov	r2,a
      002C47 74 03            [12] 7060 	mov	a,#0x03
      002C49 93               [24] 7061 	movc	a,@a+dptr
      002C4A FB               [12] 7062 	mov	r3,a
      002C4B E8               [12] 7063 	mov	a,r0
      002C4C 2C               [12] 7064 	add	a,r4
      002C4D FC               [12] 7065 	mov	r4,a
      002C4E E9               [12] 7066 	mov	a,r1
      002C4F 3D               [12] 7067 	addc	a,r5
      002C50 FD               [12] 7068 	mov	r5,a
      002C51 EA               [12] 7069 	mov	a,r2
      002C52 3E               [12] 7070 	addc	a,r6
      002C53 FE               [12] 7071 	mov	r6,a
      002C54 EB               [12] 7072 	mov	a,r3
      002C55 3F               [12] 7073 	addc	a,r7
      002C56 FF               [12] 7074 	mov	r7,a
      002C57 90 03 40         [24] 7075 	mov	dptr,#(_axradio_timer + 0x0004)
      002C5A EC               [12] 7076 	mov	a,r4
      002C5B F0               [24] 7077 	movx	@dptr,a
      002C5C ED               [12] 7078 	mov	a,r5
      002C5D A3               [24] 7079 	inc	dptr
      002C5E F0               [24] 7080 	movx	@dptr,a
      002C5F EE               [12] 7081 	mov	a,r6
      002C60 A3               [24] 7082 	inc	dptr
      002C61 F0               [24] 7083 	movx	@dptr,a
      002C62 EF               [12] 7084 	mov	a,r7
      002C63 A3               [24] 7085 	inc	dptr
      002C64 F0               [24] 7086 	movx	@dptr,a
                                   7087 ;	..\src\COMMON\easyax5043.c:1237: wtimer0_addabsolute(&axradio_timer);
      002C65 90 03 3C         [24] 7088 	mov	dptr,#_axradio_timer
                                   7089 ;	..\src\COMMON\easyax5043.c:1238: break;
      002C68 02 67 3B         [24] 7090 	ljmp	_wtimer0_addabsolute
                                   7091 ;	..\src\COMMON\easyax5043.c:1240: case syncstate_slave_rxxosc:
      002C6B                       7092 00158$:
                                   7093 ;	..\src\COMMON\easyax5043.c:1241: ax5043_receiver_on_continuous();
      002C6B 12 21 77         [24] 7094 	lcall	_ax5043_receiver_on_continuous
                                   7095 ;	..\src\COMMON\easyax5043.c:1242: axradio_syncstate = syncstate_slave_rxsfdwindow;
      002C6E 90 00 B7         [24] 7096 	mov	dptr,#_axradio_syncstate
      002C71 74 0A            [12] 7097 	mov	a,#0x0a
      002C73 F0               [24] 7098 	movx	@dptr,a
                                   7099 ;	..\src\COMMON\easyax5043.c:1243: update_timeanchor();
      002C74 12 15 BA         [24] 7100 	lcall	_update_timeanchor
                                   7101 ;	..\src\COMMON\easyax5043.c:1244: wtimer_remove_callback(&axradio_cb_receive.cb);
      002C77 90 02 E5         [24] 7102 	mov	dptr,#_axradio_cb_receive
      002C7A 12 74 C0         [24] 7103 	lcall	_wtimer_remove_callback
                                   7104 ;	..\src\COMMON\easyax5043.c:1245: memset_xdata(&axradio_cb_receive.st, 0, sizeof(axradio_cb_receive.st));
      002C7D 90 03 CD         [24] 7105 	mov	dptr,#_memset_PARM_2
      002C80 E4               [12] 7106 	clr	a
      002C81 F0               [24] 7107 	movx	@dptr,a
      002C82 90 03 CE         [24] 7108 	mov	dptr,#_memset_PARM_3
      002C85 74 1E            [12] 7109 	mov	a,#0x1e
      002C87 F0               [24] 7110 	movx	@dptr,a
      002C88 E4               [12] 7111 	clr	a
      002C89 A3               [24] 7112 	inc	dptr
      002C8A F0               [24] 7113 	movx	@dptr,a
      002C8B 90 02 E9         [24] 7114 	mov	dptr,#(_axradio_cb_receive + 0x0004)
      002C8E 75 F0 00         [24] 7115 	mov	b,#0x00
      002C91 12 62 A1         [24] 7116 	lcall	_memset
                                   7117 ;	..\src\COMMON\easyax5043.c:1246: axradio_cb_receive.st.time.t = axradio_timeanchor.radiotimer;
      002C94 90 00 CD         [24] 7118 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      002C97 E0               [24] 7119 	movx	a,@dptr
      002C98 FC               [12] 7120 	mov	r4,a
      002C99 A3               [24] 7121 	inc	dptr
      002C9A E0               [24] 7122 	movx	a,@dptr
      002C9B FD               [12] 7123 	mov	r5,a
      002C9C A3               [24] 7124 	inc	dptr
      002C9D E0               [24] 7125 	movx	a,@dptr
      002C9E FE               [12] 7126 	mov	r6,a
      002C9F A3               [24] 7127 	inc	dptr
      002CA0 E0               [24] 7128 	movx	a,@dptr
      002CA1 FF               [12] 7129 	mov	r7,a
      002CA2 90 02 EB         [24] 7130 	mov	dptr,#(_axradio_cb_receive + 0x0006)
      002CA5 EC               [12] 7131 	mov	a,r4
      002CA6 F0               [24] 7132 	movx	@dptr,a
      002CA7 ED               [12] 7133 	mov	a,r5
      002CA8 A3               [24] 7134 	inc	dptr
      002CA9 F0               [24] 7135 	movx	@dptr,a
      002CAA EE               [12] 7136 	mov	a,r6
      002CAB A3               [24] 7137 	inc	dptr
      002CAC F0               [24] 7138 	movx	@dptr,a
      002CAD EF               [12] 7139 	mov	a,r7
      002CAE A3               [24] 7140 	inc	dptr
      002CAF F0               [24] 7141 	movx	@dptr,a
                                   7142 ;	..\src\COMMON\easyax5043.c:1247: axradio_cb_receive.st.error = AXRADIO_ERR_RECEIVESTART;
      002CB0 90 02 EA         [24] 7143 	mov	dptr,#(_axradio_cb_receive + 0x0005)
      002CB3 74 0B            [12] 7144 	mov	a,#0x0b
      002CB5 F0               [24] 7145 	movx	@dptr,a
                                   7146 ;	..\src\COMMON\easyax5043.c:1248: wtimer_add_callback(&axradio_cb_receive.cb);
      002CB6 90 02 E5         [24] 7147 	mov	dptr,#_axradio_cb_receive
      002CB9 12 66 0B         [24] 7148 	lcall	_wtimer_add_callback
                                   7149 ;	..\src\COMMON\easyax5043.c:1249: wtimer_remove(&axradio_timer);
      002CBC 90 03 3C         [24] 7150 	mov	dptr,#_axradio_timer
      002CBF 12 71 84         [24] 7151 	lcall	_wtimer_remove
                                   7152 ;	..\src\COMMON\easyax5043.c:1251: uint8_t __autodata idx = axradio_sync_seqnr;
      002CC2 90 00 C2         [24] 7153 	mov	dptr,#_axradio_ack_seqnr
      002CC5 E0               [24] 7154 	movx	a,@dptr
      002CC6 FF               [12] 7155 	mov	r7,a
                                   7156 ;	..\src\COMMON\easyax5043.c:1252: if (idx >= axradio_sync_slave_nrrx)
      002CC7 90 7B 0D         [24] 7157 	mov	dptr,#_axradio_sync_slave_nrrx
      002CCA E4               [12] 7158 	clr	a
      002CCB 93               [24] 7159 	movc	a,@a+dptr
      002CCC FE               [12] 7160 	mov	r6,a
      002CCD C3               [12] 7161 	clr	c
      002CCE EF               [12] 7162 	mov	a,r7
      002CCF 9E               [12] 7163 	subb	a,r6
      002CD0 40 03            [24] 7164 	jc	00160$
                                   7165 ;	..\src\COMMON\easyax5043.c:1253: idx = axradio_sync_slave_nrrx - 1;
      002CD2 EE               [12] 7166 	mov	a,r6
      002CD3 14               [12] 7167 	dec	a
      002CD4 FF               [12] 7168 	mov	r7,a
      002CD5                       7169 00160$:
                                   7170 ;	..\src\COMMON\easyax5043.c:1254: axradio_timer.time += axradio_sync_slave_rxwindow[idx];
      002CD5 90 03 40         [24] 7171 	mov	dptr,#(_axradio_timer + 0x0004)
      002CD8 E0               [24] 7172 	movx	a,@dptr
      002CD9 FB               [12] 7173 	mov	r3,a
      002CDA A3               [24] 7174 	inc	dptr
      002CDB E0               [24] 7175 	movx	a,@dptr
      002CDC FC               [12] 7176 	mov	r4,a
      002CDD A3               [24] 7177 	inc	dptr
      002CDE E0               [24] 7178 	movx	a,@dptr
      002CDF FD               [12] 7179 	mov	r5,a
      002CE0 A3               [24] 7180 	inc	dptr
      002CE1 E0               [24] 7181 	movx	a,@dptr
      002CE2 FE               [12] 7182 	mov	r6,a
      002CE3 EF               [12] 7183 	mov	a,r7
      002CE4 75 F0 04         [24] 7184 	mov	b,#0x04
      002CE7 A4               [48] 7185 	mul	ab
      002CE8 24 1A            [12] 7186 	add	a,#_axradio_sync_slave_rxwindow
      002CEA F5 82            [12] 7187 	mov	dpl,a
      002CEC 74 7B            [12] 7188 	mov	a,#(_axradio_sync_slave_rxwindow >> 8)
      002CEE 35 F0            [12] 7189 	addc	a,b
      002CF0 F5 83            [12] 7190 	mov	dph,a
      002CF2 E4               [12] 7191 	clr	a
      002CF3 93               [24] 7192 	movc	a,@a+dptr
      002CF4 F8               [12] 7193 	mov	r0,a
      002CF5 A3               [24] 7194 	inc	dptr
      002CF6 E4               [12] 7195 	clr	a
      002CF7 93               [24] 7196 	movc	a,@a+dptr
      002CF8 F9               [12] 7197 	mov	r1,a
      002CF9 A3               [24] 7198 	inc	dptr
      002CFA E4               [12] 7199 	clr	a
      002CFB 93               [24] 7200 	movc	a,@a+dptr
      002CFC FA               [12] 7201 	mov	r2,a
      002CFD A3               [24] 7202 	inc	dptr
      002CFE E4               [12] 7203 	clr	a
      002CFF 93               [24] 7204 	movc	a,@a+dptr
      002D00 FF               [12] 7205 	mov	r7,a
      002D01 E8               [12] 7206 	mov	a,r0
      002D02 2B               [12] 7207 	add	a,r3
      002D03 FB               [12] 7208 	mov	r3,a
      002D04 E9               [12] 7209 	mov	a,r1
      002D05 3C               [12] 7210 	addc	a,r4
      002D06 FC               [12] 7211 	mov	r4,a
      002D07 EA               [12] 7212 	mov	a,r2
      002D08 3D               [12] 7213 	addc	a,r5
      002D09 FD               [12] 7214 	mov	r5,a
      002D0A EF               [12] 7215 	mov	a,r7
      002D0B 3E               [12] 7216 	addc	a,r6
      002D0C FE               [12] 7217 	mov	r6,a
      002D0D 90 03 40         [24] 7218 	mov	dptr,#(_axradio_timer + 0x0004)
      002D10 EB               [12] 7219 	mov	a,r3
      002D11 F0               [24] 7220 	movx	@dptr,a
      002D12 EC               [12] 7221 	mov	a,r4
      002D13 A3               [24] 7222 	inc	dptr
      002D14 F0               [24] 7223 	movx	@dptr,a
      002D15 ED               [12] 7224 	mov	a,r5
      002D16 A3               [24] 7225 	inc	dptr
      002D17 F0               [24] 7226 	movx	@dptr,a
      002D18 EE               [12] 7227 	mov	a,r6
      002D19 A3               [24] 7228 	inc	dptr
      002D1A F0               [24] 7229 	movx	@dptr,a
                                   7230 ;	..\src\COMMON\easyax5043.c:1256: wtimer0_addabsolute(&axradio_timer);
      002D1B 90 03 3C         [24] 7231 	mov	dptr,#_axradio_timer
                                   7232 ;	..\src\COMMON\easyax5043.c:1257: break;
      002D1E 02 67 3B         [24] 7233 	ljmp	_wtimer0_addabsolute
                                   7234 ;	..\src\COMMON\easyax5043.c:1259: case syncstate_slave_rxsfdwindow:
      002D21                       7235 00161$:
                                   7236 ;	..\src\COMMON\easyax5043.c:1261: uint8_t __autodata rs = AX5043_RADIOSTATE;
      002D21 90 40 1C         [24] 7237 	mov	dptr,#_AX5043_RADIOSTATE
      002D24 E0               [24] 7238 	movx	a,@dptr
                                   7239 ;	..\src\COMMON\easyax5043.c:1262: if( !rs )
      002D25 FF               [12] 7240 	mov	r7,a
      002D26 FE               [12] 7241 	mov	r6,a
      002D27 70 01            [24] 7242 	jnz	00293$
      002D29 22               [24] 7243 	ret
      002D2A                       7244 00293$:
                                   7245 ;	..\src\COMMON\easyax5043.c:1265: if (!(0x0F & (uint8_t)~rs)) {
      002D2A EE               [12] 7246 	mov	a,r6
      002D2B F4               [12] 7247 	cpl	a
      002D2C FE               [12] 7248 	mov	r6,a
      002D2D 54 0F            [12] 7249 	anl	a,#0x0f
      002D2F 60 02            [24] 7250 	jz	00295$
      002D31 80 4C            [24] 7251 	sjmp	00166$
      002D33                       7252 00295$:
                                   7253 ;	..\src\COMMON\easyax5043.c:1266: axradio_syncstate = syncstate_slave_rxpacket;
      002D33 90 00 B7         [24] 7254 	mov	dptr,#_axradio_syncstate
      002D36 74 0B            [12] 7255 	mov	a,#0x0b
      002D38 F0               [24] 7256 	movx	@dptr,a
                                   7257 ;	..\src\COMMON\easyax5043.c:1267: wtimer_remove(&axradio_timer);
      002D39 90 03 3C         [24] 7258 	mov	dptr,#_axradio_timer
      002D3C 12 71 84         [24] 7259 	lcall	_wtimer_remove
                                   7260 ;	..\src\COMMON\easyax5043.c:1268: axradio_timer.time += axradio_sync_slave_rxtimeout;
      002D3F 90 03 40         [24] 7261 	mov	dptr,#(_axradio_timer + 0x0004)
      002D42 E0               [24] 7262 	movx	a,@dptr
      002D43 FC               [12] 7263 	mov	r4,a
      002D44 A3               [24] 7264 	inc	dptr
      002D45 E0               [24] 7265 	movx	a,@dptr
      002D46 FD               [12] 7266 	mov	r5,a
      002D47 A3               [24] 7267 	inc	dptr
      002D48 E0               [24] 7268 	movx	a,@dptr
      002D49 FE               [12] 7269 	mov	r6,a
      002D4A A3               [24] 7270 	inc	dptr
      002D4B E0               [24] 7271 	movx	a,@dptr
      002D4C FF               [12] 7272 	mov	r7,a
      002D4D 90 7B 26         [24] 7273 	mov	dptr,#_axradio_sync_slave_rxtimeout
      002D50 E4               [12] 7274 	clr	a
      002D51 93               [24] 7275 	movc	a,@a+dptr
      002D52 F8               [12] 7276 	mov	r0,a
      002D53 74 01            [12] 7277 	mov	a,#0x01
      002D55 93               [24] 7278 	movc	a,@a+dptr
      002D56 F9               [12] 7279 	mov	r1,a
      002D57 74 02            [12] 7280 	mov	a,#0x02
      002D59 93               [24] 7281 	movc	a,@a+dptr
      002D5A FA               [12] 7282 	mov	r2,a
      002D5B 74 03            [12] 7283 	mov	a,#0x03
      002D5D 93               [24] 7284 	movc	a,@a+dptr
      002D5E FB               [12] 7285 	mov	r3,a
      002D5F E8               [12] 7286 	mov	a,r0
      002D60 2C               [12] 7287 	add	a,r4
      002D61 FC               [12] 7288 	mov	r4,a
      002D62 E9               [12] 7289 	mov	a,r1
      002D63 3D               [12] 7290 	addc	a,r5
      002D64 FD               [12] 7291 	mov	r5,a
      002D65 EA               [12] 7292 	mov	a,r2
      002D66 3E               [12] 7293 	addc	a,r6
      002D67 FE               [12] 7294 	mov	r6,a
      002D68 EB               [12] 7295 	mov	a,r3
      002D69 3F               [12] 7296 	addc	a,r7
      002D6A FF               [12] 7297 	mov	r7,a
      002D6B 90 03 40         [24] 7298 	mov	dptr,#(_axradio_timer + 0x0004)
      002D6E EC               [12] 7299 	mov	a,r4
      002D6F F0               [24] 7300 	movx	@dptr,a
      002D70 ED               [12] 7301 	mov	a,r5
      002D71 A3               [24] 7302 	inc	dptr
      002D72 F0               [24] 7303 	movx	@dptr,a
      002D73 EE               [12] 7304 	mov	a,r6
      002D74 A3               [24] 7305 	inc	dptr
      002D75 F0               [24] 7306 	movx	@dptr,a
      002D76 EF               [12] 7307 	mov	a,r7
      002D77 A3               [24] 7308 	inc	dptr
      002D78 F0               [24] 7309 	movx	@dptr,a
                                   7310 ;	..\src\COMMON\easyax5043.c:1269: wtimer0_addabsolute(&axradio_timer);
      002D79 90 03 3C         [24] 7311 	mov	dptr,#_axradio_timer
                                   7312 ;	..\src\COMMON\easyax5043.c:1270: break;
      002D7C 02 67 3B         [24] 7313 	ljmp	_wtimer0_addabsolute
                                   7314 ;	..\src\COMMON\easyax5043.c:1275: case syncstate_slave_rxpacket:
      002D7F                       7315 00166$:
                                   7316 ;	..\src\COMMON\easyax5043.c:1276: ax5043_off();
      002D7F 12 22 CE         [24] 7317 	lcall	_ax5043_off
                                   7318 ;	..\src\COMMON\easyax5043.c:1277: if (!axradio_sync_seqnr)
      002D82 90 00 C2         [24] 7319 	mov	dptr,#_axradio_ack_seqnr
      002D85 E0               [24] 7320 	movx	a,@dptr
      002D86 70 06            [24] 7321 	jnz	00168$
                                   7322 ;	..\src\COMMON\easyax5043.c:1278: axradio_sync_seqnr = 1;
      002D88 90 00 C2         [24] 7323 	mov	dptr,#_axradio_ack_seqnr
      002D8B 74 01            [12] 7324 	mov	a,#0x01
      002D8D F0               [24] 7325 	movx	@dptr,a
      002D8E                       7326 00168$:
                                   7327 ;	..\src\COMMON\easyax5043.c:1279: ++axradio_sync_seqnr;
      002D8E 90 00 C2         [24] 7328 	mov	dptr,#_axradio_ack_seqnr
      002D91 E0               [24] 7329 	movx	a,@dptr
      002D92 24 01            [12] 7330 	add	a,#0x01
      002D94 F0               [24] 7331 	movx	@dptr,a
                                   7332 ;	..\src\COMMON\easyax5043.c:1280: update_timeanchor();
      002D95 12 15 BA         [24] 7333 	lcall	_update_timeanchor
                                   7334 ;	..\src\COMMON\easyax5043.c:1281: wtimer_remove_callback(&axradio_cb_receive.cb);
      002D98 90 02 E5         [24] 7335 	mov	dptr,#_axradio_cb_receive
      002D9B 12 74 C0         [24] 7336 	lcall	_wtimer_remove_callback
                                   7337 ;	..\src\COMMON\easyax5043.c:1282: memset_xdata(&axradio_cb_receive.st, 0, sizeof(axradio_cb_receive.st));
      002D9E 90 03 CD         [24] 7338 	mov	dptr,#_memset_PARM_2
      002DA1 E4               [12] 7339 	clr	a
      002DA2 F0               [24] 7340 	movx	@dptr,a
      002DA3 90 03 CE         [24] 7341 	mov	dptr,#_memset_PARM_3
      002DA6 74 1E            [12] 7342 	mov	a,#0x1e
      002DA8 F0               [24] 7343 	movx	@dptr,a
      002DA9 E4               [12] 7344 	clr	a
      002DAA A3               [24] 7345 	inc	dptr
      002DAB F0               [24] 7346 	movx	@dptr,a
      002DAC 90 02 E9         [24] 7347 	mov	dptr,#(_axradio_cb_receive + 0x0004)
      002DAF 75 F0 00         [24] 7348 	mov	b,#0x00
      002DB2 12 62 A1         [24] 7349 	lcall	_memset
                                   7350 ;	..\src\COMMON\easyax5043.c:1283: axradio_cb_receive.st.time.t = axradio_timeanchor.radiotimer;
      002DB5 90 00 CD         [24] 7351 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      002DB8 E0               [24] 7352 	movx	a,@dptr
      002DB9 FC               [12] 7353 	mov	r4,a
      002DBA A3               [24] 7354 	inc	dptr
      002DBB E0               [24] 7355 	movx	a,@dptr
      002DBC FD               [12] 7356 	mov	r5,a
      002DBD A3               [24] 7357 	inc	dptr
      002DBE E0               [24] 7358 	movx	a,@dptr
      002DBF FE               [12] 7359 	mov	r6,a
      002DC0 A3               [24] 7360 	inc	dptr
      002DC1 E0               [24] 7361 	movx	a,@dptr
      002DC2 FF               [12] 7362 	mov	r7,a
      002DC3 90 02 EB         [24] 7363 	mov	dptr,#(_axradio_cb_receive + 0x0006)
      002DC6 EC               [12] 7364 	mov	a,r4
      002DC7 F0               [24] 7365 	movx	@dptr,a
      002DC8 ED               [12] 7366 	mov	a,r5
      002DC9 A3               [24] 7367 	inc	dptr
      002DCA F0               [24] 7368 	movx	@dptr,a
      002DCB EE               [12] 7369 	mov	a,r6
      002DCC A3               [24] 7370 	inc	dptr
      002DCD F0               [24] 7371 	movx	@dptr,a
      002DCE EF               [12] 7372 	mov	a,r7
      002DCF A3               [24] 7373 	inc	dptr
      002DD0 F0               [24] 7374 	movx	@dptr,a
                                   7375 ;	..\src\COMMON\easyax5043.c:1284: axradio_cb_receive.st.error = AXRADIO_ERR_TIMEOUT;
      002DD1 90 02 EA         [24] 7376 	mov	dptr,#(_axradio_cb_receive + 0x0005)
      002DD4 74 03            [12] 7377 	mov	a,#0x03
      002DD6 F0               [24] 7378 	movx	@dptr,a
                                   7379 ;	..\src\COMMON\easyax5043.c:1285: if (axradio_sync_seqnr <= axradio_sync_slave_resyncloss) {
      002DD7 90 00 C2         [24] 7380 	mov	dptr,#_axradio_ack_seqnr
      002DDA E0               [24] 7381 	movx	a,@dptr
      002DDB FF               [12] 7382 	mov	r7,a
      002DDC 90 7B 0C         [24] 7383 	mov	dptr,#_axradio_sync_slave_resyncloss
      002DDF E4               [12] 7384 	clr	a
      002DE0 93               [24] 7385 	movc	a,@a+dptr
      002DE1 FE               [12] 7386 	mov	r6,a
      002DE2 C3               [12] 7387 	clr	c
      002DE3 9F               [12] 7388 	subb	a,r7
      002DE4 40 54            [24] 7389 	jc	00172$
                                   7390 ;	..\src\COMMON\easyax5043.c:1286: wtimer_add_callback(&axradio_cb_receive.cb);
      002DE6 90 02 E5         [24] 7391 	mov	dptr,#_axradio_cb_receive
      002DE9 12 66 0B         [24] 7392 	lcall	_wtimer_add_callback
                                   7393 ;	..\src\COMMON\easyax5043.c:1287: axradio_sync_slave_nextperiod();
      002DEC 12 26 27         [24] 7394 	lcall	_axradio_sync_slave_nextperiod
                                   7395 ;	..\src\COMMON\easyax5043.c:1288: axradio_syncstate = syncstate_slave_rxidle;
      002DEF 90 00 B7         [24] 7396 	mov	dptr,#_axradio_syncstate
      002DF2 74 08            [12] 7397 	mov	a,#0x08
      002DF4 F0               [24] 7398 	movx	@dptr,a
                                   7399 ;	..\src\COMMON\easyax5043.c:1289: wtimer_remove(&axradio_timer);
      002DF5 90 03 3C         [24] 7400 	mov	dptr,#_axradio_timer
      002DF8 12 71 84         [24] 7401 	lcall	_wtimer_remove
                                   7402 ;	..\src\COMMON\easyax5043.c:1291: uint8_t __autodata idx = axradio_sync_seqnr;
      002DFB 90 00 C2         [24] 7403 	mov	dptr,#_axradio_ack_seqnr
      002DFE E0               [24] 7404 	movx	a,@dptr
      002DFF FF               [12] 7405 	mov	r7,a
                                   7406 ;	..\src\COMMON\easyax5043.c:1292: if (idx >= axradio_sync_slave_nrrx)
      002E00 90 7B 0D         [24] 7407 	mov	dptr,#_axradio_sync_slave_nrrx
      002E03 E4               [12] 7408 	clr	a
      002E04 93               [24] 7409 	movc	a,@a+dptr
      002E05 FE               [12] 7410 	mov	r6,a
      002E06 C3               [12] 7411 	clr	c
      002E07 EF               [12] 7412 	mov	a,r7
      002E08 9E               [12] 7413 	subb	a,r6
      002E09 40 03            [24] 7414 	jc	00170$
                                   7415 ;	..\src\COMMON\easyax5043.c:1293: idx = axradio_sync_slave_nrrx - 1;
      002E0B EE               [12] 7416 	mov	a,r6
      002E0C 14               [12] 7417 	dec	a
      002E0D FF               [12] 7418 	mov	r7,a
      002E0E                       7419 00170$:
                                   7420 ;	..\src\COMMON\easyax5043.c:1294: axradio_sync_settimeradv(axradio_sync_slave_rxadvance[idx]);
      002E0E EF               [12] 7421 	mov	a,r7
      002E0F 75 F0 04         [24] 7422 	mov	b,#0x04
      002E12 A4               [48] 7423 	mul	ab
      002E13 24 0E            [12] 7424 	add	a,#_axradio_sync_slave_rxadvance
      002E15 F5 82            [12] 7425 	mov	dpl,a
      002E17 74 7B            [12] 7426 	mov	a,#(_axradio_sync_slave_rxadvance >> 8)
      002E19 35 F0            [12] 7427 	addc	a,b
      002E1B F5 83            [12] 7428 	mov	dph,a
      002E1D E4               [12] 7429 	clr	a
      002E1E 93               [24] 7430 	movc	a,@a+dptr
      002E1F FC               [12] 7431 	mov	r4,a
      002E20 A3               [24] 7432 	inc	dptr
      002E21 E4               [12] 7433 	clr	a
      002E22 93               [24] 7434 	movc	a,@a+dptr
      002E23 FD               [12] 7435 	mov	r5,a
      002E24 A3               [24] 7436 	inc	dptr
      002E25 E4               [12] 7437 	clr	a
      002E26 93               [24] 7438 	movc	a,@a+dptr
      002E27 FE               [12] 7439 	mov	r6,a
      002E28 A3               [24] 7440 	inc	dptr
      002E29 E4               [12] 7441 	clr	a
      002E2A 93               [24] 7442 	movc	a,@a+dptr
      002E2B 8C 82            [24] 7443 	mov	dpl,r4
      002E2D 8D 83            [24] 7444 	mov	dph,r5
      002E2F 8E F0            [24] 7445 	mov	b,r6
      002E31 12 24 C1         [24] 7446 	lcall	_axradio_sync_settimeradv
                                   7447 ;	..\src\COMMON\easyax5043.c:1296: wtimer0_addabsolute(&axradio_timer);
      002E34 90 03 3C         [24] 7448 	mov	dptr,#_axradio_timer
                                   7449 ;	..\src\COMMON\easyax5043.c:1297: break;
      002E37 02 67 3B         [24] 7450 	ljmp	_wtimer0_addabsolute
      002E3A                       7451 00172$:
                                   7452 ;	..\src\COMMON\easyax5043.c:1299: axradio_cb_receive.st.error = AXRADIO_ERR_RESYNC;
      002E3A 90 02 EA         [24] 7453 	mov	dptr,#(_axradio_cb_receive + 0x0005)
      002E3D 74 09            [12] 7454 	mov	a,#0x09
      002E3F F0               [24] 7455 	movx	@dptr,a
                                   7456 ;	..\src\COMMON\easyax5043.c:1300: wtimer_add_callback(&axradio_cb_receive.cb);
      002E40 90 02 E5         [24] 7457 	mov	dptr,#_axradio_cb_receive
      002E43 12 66 0B         [24] 7458 	lcall	_wtimer_add_callback
                                   7459 ;	..\src\COMMON\easyax5043.c:1301: ax5043_receiver_on_continuous();
      002E46 12 21 77         [24] 7460 	lcall	_ax5043_receiver_on_continuous
                                   7461 ;	..\src\COMMON\easyax5043.c:1302: axradio_syncstate = syncstate_slave_synchunt;
      002E49 90 00 B7         [24] 7462 	mov	dptr,#_axradio_syncstate
      002E4C 74 06            [12] 7463 	mov	a,#0x06
      002E4E F0               [24] 7464 	movx	@dptr,a
                                   7465 ;	..\src\COMMON\easyax5043.c:1303: wtimer_remove(&axradio_timer);
      002E4F 90 03 3C         [24] 7466 	mov	dptr,#_axradio_timer
      002E52 12 71 84         [24] 7467 	lcall	_wtimer_remove
                                   7468 ;	..\src\COMMON\easyax5043.c:1304: axradio_timer.time = axradio_sync_slave_syncwindow;
      002E55 90 7A FE         [24] 7469 	mov	dptr,#_axradio_sync_slave_syncwindow
      002E58 E4               [12] 7470 	clr	a
      002E59 93               [24] 7471 	movc	a,@a+dptr
      002E5A FC               [12] 7472 	mov	r4,a
      002E5B 74 01            [12] 7473 	mov	a,#0x01
      002E5D 93               [24] 7474 	movc	a,@a+dptr
      002E5E FD               [12] 7475 	mov	r5,a
      002E5F 74 02            [12] 7476 	mov	a,#0x02
      002E61 93               [24] 7477 	movc	a,@a+dptr
      002E62 FE               [12] 7478 	mov	r6,a
      002E63 74 03            [12] 7479 	mov	a,#0x03
      002E65 93               [24] 7480 	movc	a,@a+dptr
      002E66 FF               [12] 7481 	mov	r7,a
      002E67 90 03 40         [24] 7482 	mov	dptr,#(_axradio_timer + 0x0004)
      002E6A EC               [12] 7483 	mov	a,r4
      002E6B F0               [24] 7484 	movx	@dptr,a
      002E6C ED               [12] 7485 	mov	a,r5
      002E6D A3               [24] 7486 	inc	dptr
      002E6E F0               [24] 7487 	movx	@dptr,a
      002E6F EE               [12] 7488 	mov	a,r6
      002E70 A3               [24] 7489 	inc	dptr
      002E71 F0               [24] 7490 	movx	@dptr,a
      002E72 EF               [12] 7491 	mov	a,r7
      002E73 A3               [24] 7492 	inc	dptr
      002E74 F0               [24] 7493 	movx	@dptr,a
                                   7494 ;	..\src\COMMON\easyax5043.c:1305: wtimer0_addrelative(&axradio_timer);
      002E75 90 03 3C         [24] 7495 	mov	dptr,#_axradio_timer
      002E78 12 66 25         [24] 7496 	lcall	_wtimer0_addrelative
                                   7497 ;	..\src\COMMON\easyax5043.c:1306: axradio_sync_time = axradio_timer.time;
      002E7B 90 03 40         [24] 7498 	mov	dptr,#(_axradio_timer + 0x0004)
      002E7E E0               [24] 7499 	movx	a,@dptr
      002E7F FC               [12] 7500 	mov	r4,a
      002E80 A3               [24] 7501 	inc	dptr
      002E81 E0               [24] 7502 	movx	a,@dptr
      002E82 FD               [12] 7503 	mov	r5,a
      002E83 A3               [24] 7504 	inc	dptr
      002E84 E0               [24] 7505 	movx	a,@dptr
      002E85 FE               [12] 7506 	mov	r6,a
      002E86 A3               [24] 7507 	inc	dptr
      002E87 E0               [24] 7508 	movx	a,@dptr
      002E88 FF               [12] 7509 	mov	r7,a
      002E89 90 00 C3         [24] 7510 	mov	dptr,#_axradio_sync_time
      002E8C EC               [12] 7511 	mov	a,r4
      002E8D F0               [24] 7512 	movx	@dptr,a
      002E8E ED               [12] 7513 	mov	a,r5
      002E8F A3               [24] 7514 	inc	dptr
      002E90 F0               [24] 7515 	movx	@dptr,a
      002E91 EE               [12] 7516 	mov	a,r6
      002E92 A3               [24] 7517 	inc	dptr
      002E93 F0               [24] 7518 	movx	@dptr,a
      002E94 EF               [12] 7519 	mov	a,r7
      002E95 A3               [24] 7520 	inc	dptr
      002E96 F0               [24] 7521 	movx	@dptr,a
                                   7522 ;	..\src\COMMON\easyax5043.c:1307: break;
                                   7523 ;	..\src\COMMON\easyax5043.c:1309: case syncstate_slave_rxack:
      002E97 22               [24] 7524 	ret
      002E98                       7525 00173$:
                                   7526 ;	..\src\COMMON\easyax5043.c:1310: axradio_syncstate = syncstate_slave_rxidle;
      002E98 90 00 B7         [24] 7527 	mov	dptr,#_axradio_syncstate
      002E9B 74 08            [12] 7528 	mov	a,#0x08
      002E9D F0               [24] 7529 	movx	@dptr,a
                                   7530 ;	..\src\COMMON\easyax5043.c:1311: wtimer_remove(&axradio_timer);
      002E9E 90 03 3C         [24] 7531 	mov	dptr,#_axradio_timer
      002EA1 12 71 84         [24] 7532 	lcall	_wtimer_remove
                                   7533 ;	..\src\COMMON\easyax5043.c:1312: axradio_sync_settimeradv(axradio_sync_slave_rxadvance[1]);
      002EA4 90 7B 12         [24] 7534 	mov	dptr,#(_axradio_sync_slave_rxadvance + 0x0004)
      002EA7 E4               [12] 7535 	clr	a
      002EA8 93               [24] 7536 	movc	a,@a+dptr
      002EA9 FC               [12] 7537 	mov	r4,a
      002EAA A3               [24] 7538 	inc	dptr
      002EAB E4               [12] 7539 	clr	a
      002EAC 93               [24] 7540 	movc	a,@a+dptr
      002EAD FD               [12] 7541 	mov	r5,a
      002EAE A3               [24] 7542 	inc	dptr
      002EAF E4               [12] 7543 	clr	a
      002EB0 93               [24] 7544 	movc	a,@a+dptr
      002EB1 FE               [12] 7545 	mov	r6,a
      002EB2 A3               [24] 7546 	inc	dptr
      002EB3 E4               [12] 7547 	clr	a
      002EB4 93               [24] 7548 	movc	a,@a+dptr
      002EB5 8C 82            [24] 7549 	mov	dpl,r4
      002EB7 8D 83            [24] 7550 	mov	dph,r5
      002EB9 8E F0            [24] 7551 	mov	b,r6
      002EBB 12 24 C1         [24] 7552 	lcall	_axradio_sync_settimeradv
                                   7553 ;	..\src\COMMON\easyax5043.c:1313: wtimer0_addabsolute(&axradio_timer);
      002EBE 90 03 3C         [24] 7554 	mov	dptr,#_axradio_timer
      002EC1 12 67 3B         [24] 7555 	lcall	_wtimer0_addabsolute
                                   7556 ;	..\src\COMMON\easyax5043.c:1314: goto transmitack;
      002EC4 02 28 99         [24] 7557 	ljmp	00133$
                                   7558 ;	..\src\COMMON\easyax5043.c:1318: default:
      002EC7                       7559 00175$:
                                   7560 ;	..\src\COMMON\easyax5043.c:1320: }
      002EC7 22               [24] 7561 	ret
                                   7562 ;------------------------------------------------------------
                                   7563 ;Allocation info for local variables in function 'axradio_callback_fwd'
                                   7564 ;------------------------------------------------------------
                                   7565 ;desc                      Allocated to registers r6 r7 
                                   7566 ;------------------------------------------------------------
                                   7567 ;	..\src\COMMON\easyax5043.c:1323: static __reentrantb void axradio_callback_fwd(struct wtimer_callback __xdata *desc) __reentrant
                                   7568 ;	-----------------------------------------
                                   7569 ;	 function axradio_callback_fwd
                                   7570 ;	-----------------------------------------
      002EC8                       7571 _axradio_callback_fwd:
      002EC8 AE 82            [24] 7572 	mov	r6,dpl
      002ECA AF 83            [24] 7573 	mov	r7,dph
                                   7574 ;	..\src\COMMON\easyax5043.c:1325: axradio_statuschange((struct axradio_status __xdata *)(desc + 1));
      002ECC 74 04            [12] 7575 	mov	a,#0x04
      002ECE 2E               [12] 7576 	add	a,r6
      002ECF FE               [12] 7577 	mov	r6,a
      002ED0 E4               [12] 7578 	clr	a
      002ED1 3F               [12] 7579 	addc	a,r7
      002ED2 FF               [12] 7580 	mov	r7,a
      002ED3 8E 82            [24] 7581 	mov	dpl,r6
      002ED5 8F 83            [24] 7582 	mov	dph,r7
      002ED7 02 0C 08         [24] 7583 	ljmp	_axradio_statuschange
                                   7584 ;------------------------------------------------------------
                                   7585 ;Allocation info for local variables in function 'axradio_receive_callback_fwd'
                                   7586 ;------------------------------------------------------------
                                   7587 ;len                       Allocated to registers r6 r7 
                                   7588 ;len                       Allocated to registers r6 r7 
                                   7589 ;trxst                     Allocated to registers r6 
                                   7590 ;iesave                    Allocated to registers r7 
                                   7591 ;desc                      Allocated with name '_axradio_receive_callback_fwd_desc_1_344'
                                   7592 ;seqnr                     Allocated with name '_axradio_receive_callback_fwd_seqnr_3_352'
                                   7593 ;len_byte                  Allocated with name '_axradio_receive_callback_fwd_len_byte_3_354'
                                   7594 ;------------------------------------------------------------
                                   7595 ;	..\src\COMMON\easyax5043.c:1328: static void axradio_receive_callback_fwd(struct wtimer_callback __xdata *desc)
                                   7596 ;	-----------------------------------------
                                   7597 ;	 function axradio_receive_callback_fwd
                                   7598 ;	-----------------------------------------
      002EDA                       7599 _axradio_receive_callback_fwd:
                                   7600 ;	..\src\COMMON\easyax5043.c:1332: if (axradio_cb_receive.st.error != AXRADIO_ERR_NOERROR) {
      002EDA 90 02 EA         [24] 7601 	mov	dptr,#(_axradio_cb_receive + 0x0005)
      002EDD E0               [24] 7602 	movx	a,@dptr
      002EDE 60 06            [24] 7603 	jz	00102$
                                   7604 ;	..\src\COMMON\easyax5043.c:1333: axradio_statuschange((struct axradio_status __xdata *)&axradio_cb_receive.st);
      002EE0 90 02 E9         [24] 7605 	mov	dptr,#(_axradio_cb_receive + 0x0004)
                                   7606 ;	..\src\COMMON\easyax5043.c:1334: return;
      002EE3 02 0C 08         [24] 7607 	ljmp	_axradio_statuschange
      002EE6                       7608 00102$:
                                   7609 ;	..\src\COMMON\easyax5043.c:1336: if (axradio_phy_pn9 && !AXRADIO_MODE_IS_STREAM_RECEIVE(axradio_mode)) {
      002EE6 90 7A B9         [24] 7610 	mov	dptr,#_axradio_phy_pn9
      002EE9 E4               [12] 7611 	clr	a
      002EEA 93               [24] 7612 	movc	a,@a+dptr
      002EEB 60 51            [24] 7613 	jz	00104$
      002EED 74 F8            [12] 7614 	mov	a,#0xf8
      002EEF 55 0B            [12] 7615 	anl	a,_axradio_mode
      002EF1 FF               [12] 7616 	mov	r7,a
      002EF2 BF 28 02         [24] 7617 	cjne	r7,#0x28,00282$
      002EF5 80 47            [24] 7618 	sjmp	00104$
      002EF7                       7619 00282$:
                                   7620 ;	..\src\COMMON\easyax5043.c:1337: uint16_t __autodata len = axradio_cb_receive.st.rx.pktlen;
      002EF7 90 03 05         [24] 7621 	mov	dptr,#(_axradio_cb_receive + 0x0020)
      002EFA E0               [24] 7622 	movx	a,@dptr
      002EFB FE               [12] 7623 	mov	r6,a
      002EFC A3               [24] 7624 	inc	dptr
      002EFD E0               [24] 7625 	movx	a,@dptr
      002EFE FF               [12] 7626 	mov	r7,a
                                   7627 ;	..\src\COMMON\easyax5043.c:1338: len += axradio_framing_maclen;
      002EFF 90 7A DA         [24] 7628 	mov	dptr,#_axradio_framing_maclen
      002F02 E4               [12] 7629 	clr	a
      002F03 93               [24] 7630 	movc	a,@a+dptr
      002F04 7C 00            [12] 7631 	mov	r4,#0x00
      002F06 2E               [12] 7632 	add	a,r6
      002F07 FE               [12] 7633 	mov	r6,a
      002F08 EC               [12] 7634 	mov	a,r4
      002F09 3F               [12] 7635 	addc	a,r7
      002F0A FF               [12] 7636 	mov	r7,a
                                   7637 ;	..\src\COMMON\easyax5043.c:1339: pn9_buffer((__xdata uint8_t *)axradio_cb_receive.st.rx.mac.raw, len, 0x1ff, -(AX5043_ENCODING & 0x01));
      002F0B 90 40 11         [24] 7638 	mov	dptr,#_AX5043_ENCODING
      002F0E E0               [24] 7639 	movx	a,@dptr
      002F0F FD               [12] 7640 	mov	r5,a
      002F10 53 05 01         [24] 7641 	anl	ar5,#0x01
      002F13 C3               [12] 7642 	clr	c
      002F14 E4               [12] 7643 	clr	a
      002F15 9D               [12] 7644 	subb	a,r5
      002F16 FD               [12] 7645 	mov	r5,a
      002F17 90 03 01         [24] 7646 	mov	dptr,#(_axradio_cb_receive + 0x001c)
      002F1A E0               [24] 7647 	movx	a,@dptr
      002F1B FB               [12] 7648 	mov	r3,a
      002F1C A3               [24] 7649 	inc	dptr
      002F1D E0               [24] 7650 	movx	a,@dptr
      002F1E FC               [12] 7651 	mov	r4,a
      002F1F 7A 00            [12] 7652 	mov	r2,#0x00
      002F21 C0 05            [24] 7653 	push	ar5
      002F23 74 FF            [12] 7654 	mov	a,#0xff
      002F25 C0 E0            [24] 7655 	push	acc
      002F27 74 01            [12] 7656 	mov	a,#0x01
      002F29 C0 E0            [24] 7657 	push	acc
      002F2B C0 06            [24] 7658 	push	ar6
      002F2D C0 07            [24] 7659 	push	ar7
      002F2F 8B 82            [24] 7660 	mov	dpl,r3
      002F31 8C 83            [24] 7661 	mov	dph,r4
      002F33 8A F0            [24] 7662 	mov	b,r2
      002F35 12 67 8E         [24] 7663 	lcall	_pn9_buffer
      002F38 E5 81            [12] 7664 	mov	a,sp
      002F3A 24 FB            [12] 7665 	add	a,#0xfb
      002F3C F5 81            [12] 7666 	mov	sp,a
      002F3E                       7667 00104$:
                                   7668 ;	..\src\COMMON\easyax5043.c:1341: if (axradio_framing_swcrclen && !AXRADIO_MODE_IS_STREAM_RECEIVE(axradio_mode)) {
      002F3E 90 7A E1         [24] 7669 	mov	dptr,#_axradio_framing_swcrclen
      002F41 E4               [12] 7670 	clr	a
      002F42 93               [24] 7671 	movc	a,@a+dptr
      002F43 60 66            [24] 7672 	jz	00109$
      002F45 74 F8            [12] 7673 	mov	a,#0xf8
      002F47 55 0B            [12] 7674 	anl	a,_axradio_mode
      002F49 FF               [12] 7675 	mov	r7,a
      002F4A BF 28 02         [24] 7676 	cjne	r7,#0x28,00284$
      002F4D 80 5C            [24] 7677 	sjmp	00109$
      002F4F                       7678 00284$:
                                   7679 ;	..\src\COMMON\easyax5043.c:1342: uint16_t __autodata len = axradio_cb_receive.st.rx.pktlen;
      002F4F 90 03 05         [24] 7680 	mov	dptr,#(_axradio_cb_receive + 0x0020)
      002F52 E0               [24] 7681 	movx	a,@dptr
      002F53 FE               [12] 7682 	mov	r6,a
      002F54 A3               [24] 7683 	inc	dptr
      002F55 E0               [24] 7684 	movx	a,@dptr
      002F56 FF               [12] 7685 	mov	r7,a
                                   7686 ;	..\src\COMMON\easyax5043.c:1343: len += axradio_framing_maclen;
      002F57 90 7A DA         [24] 7687 	mov	dptr,#_axradio_framing_maclen
      002F5A E4               [12] 7688 	clr	a
      002F5B 93               [24] 7689 	movc	a,@a+dptr
      002F5C 7C 00            [12] 7690 	mov	r4,#0x00
      002F5E 2E               [12] 7691 	add	a,r6
      002F5F FE               [12] 7692 	mov	r6,a
      002F60 EC               [12] 7693 	mov	a,r4
      002F61 3F               [12] 7694 	addc	a,r7
      002F62 FF               [12] 7695 	mov	r7,a
                                   7696 ;	..\src\COMMON\easyax5043.c:1344: len = axradio_framing_check_crc((uint8_t __xdata *)axradio_cb_receive.st.rx.mac.raw, len);
      002F63 90 03 01         [24] 7697 	mov	dptr,#(_axradio_cb_receive + 0x001c)
      002F66 E0               [24] 7698 	movx	a,@dptr
      002F67 FC               [12] 7699 	mov	r4,a
      002F68 A3               [24] 7700 	inc	dptr
      002F69 E0               [24] 7701 	movx	a,@dptr
      002F6A FD               [12] 7702 	mov	r5,a
      002F6B C0 06            [24] 7703 	push	ar6
      002F6D C0 07            [24] 7704 	push	ar7
      002F6F 8C 82            [24] 7705 	mov	dpl,r4
      002F71 8D 83            [24] 7706 	mov	dph,r5
      002F73 12 0A 4F         [24] 7707 	lcall	_axradio_framing_check_crc
      002F76 AE 82            [24] 7708 	mov	r6,dpl
      002F78 AF 83            [24] 7709 	mov	r7,dph
      002F7A 15 81            [12] 7710 	dec	sp
      002F7C 15 81            [12] 7711 	dec	sp
                                   7712 ;	..\src\COMMON\easyax5043.c:1345: if (!len)
      002F7E EE               [12] 7713 	mov	a,r6
      002F7F 4F               [12] 7714 	orl	a,r7
      002F80 70 03            [24] 7715 	jnz	00285$
      002F82 02 33 BB         [24] 7716 	ljmp	00159$
      002F85                       7717 00285$:
                                   7718 ;	..\src\COMMON\easyax5043.c:1348: len -= axradio_framing_maclen;
      002F85 90 7A DA         [24] 7719 	mov	dptr,#_axradio_framing_maclen
      002F88 E4               [12] 7720 	clr	a
      002F89 93               [24] 7721 	movc	a,@a+dptr
      002F8A FD               [12] 7722 	mov	r5,a
      002F8B 7C 00            [12] 7723 	mov	r4,#0x00
      002F8D EE               [12] 7724 	mov	a,r6
      002F8E C3               [12] 7725 	clr	c
      002F8F 9D               [12] 7726 	subb	a,r5
      002F90 FE               [12] 7727 	mov	r6,a
      002F91 EF               [12] 7728 	mov	a,r7
      002F92 9C               [12] 7729 	subb	a,r4
      002F93 FF               [12] 7730 	mov	r7,a
                                   7731 ;	..\src\COMMON\easyax5043.c:1349: len -= axradio_framing_swcrclen; // drop crc
      002F94 90 7A E1         [24] 7732 	mov	dptr,#_axradio_framing_swcrclen
      002F97 E4               [12] 7733 	clr	a
      002F98 93               [24] 7734 	movc	a,@a+dptr
      002F99 FD               [12] 7735 	mov	r5,a
      002F9A 7C 00            [12] 7736 	mov	r4,#0x00
      002F9C EE               [12] 7737 	mov	a,r6
      002F9D C3               [12] 7738 	clr	c
      002F9E 9D               [12] 7739 	subb	a,r5
      002F9F FE               [12] 7740 	mov	r6,a
      002FA0 EF               [12] 7741 	mov	a,r7
      002FA1 9C               [12] 7742 	subb	a,r4
      002FA2 FF               [12] 7743 	mov	r7,a
                                   7744 ;	..\src\COMMON\easyax5043.c:1350: axradio_cb_receive.st.rx.pktlen = len;
      002FA3 90 03 05         [24] 7745 	mov	dptr,#(_axradio_cb_receive + 0x0020)
      002FA6 EE               [12] 7746 	mov	a,r6
      002FA7 F0               [24] 7747 	movx	@dptr,a
      002FA8 EF               [12] 7748 	mov	a,r7
      002FA9 A3               [24] 7749 	inc	dptr
      002FAA F0               [24] 7750 	movx	@dptr,a
      002FAB                       7751 00109$:
                                   7752 ;	..\src\COMMON\easyax5043.c:1354: axradio_cb_receive.st.rx.phy.timeoffset = 0;
      002FAB 90 02 F5         [24] 7753 	mov	dptr,#(_axradio_cb_receive + 0x0010)
      002FAE E4               [12] 7754 	clr	a
      002FAF F0               [24] 7755 	movx	@dptr,a
      002FB0 A3               [24] 7756 	inc	dptr
      002FB1 F0               [24] 7757 	movx	@dptr,a
                                   7758 ;	..\src\COMMON\easyax5043.c:1355: axradio_cb_receive.st.rx.phy.period = 0;
      002FB2 90 02 F7         [24] 7759 	mov	dptr,#(_axradio_cb_receive + 0x0012)
      002FB5 F0               [24] 7760 	movx	@dptr,a
      002FB6 A3               [24] 7761 	inc	dptr
      002FB7 F0               [24] 7762 	movx	@dptr,a
                                   7763 ;	..\src\COMMON\easyax5043.c:1356: if (axradio_mode == AXRADIO_MODE_ACK_TRANSMIT ||
      002FB8 74 12            [12] 7764 	mov	a,#0x12
      002FBA B5 0B 02         [24] 7765 	cjne	a,_axradio_mode,00286$
      002FBD 80 0C            [24] 7766 	sjmp	00113$
      002FBF                       7767 00286$:
                                   7768 ;	..\src\COMMON\easyax5043.c:1357: axradio_mode == AXRADIO_MODE_WOR_ACK_TRANSMIT ||
      002FBF 74 13            [12] 7769 	mov	a,#0x13
      002FC1 B5 0B 02         [24] 7770 	cjne	a,_axradio_mode,00287$
      002FC4 80 05            [24] 7771 	sjmp	00113$
      002FC6                       7772 00287$:
                                   7773 ;	..\src\COMMON\easyax5043.c:1358: axradio_mode == AXRADIO_MODE_SYNC_ACK_MASTER) {
      002FC6 74 31            [12] 7774 	mov	a,#0x31
      002FC8 B5 0B 65         [24] 7775 	cjne	a,_axradio_mode,00114$
      002FCB                       7776 00113$:
                                   7777 ;	..\src\COMMON\easyax5043.c:1359: ax5043_off();
      002FCB 12 22 CE         [24] 7778 	lcall	_ax5043_off
                                   7779 ;	..\src\COMMON\easyax5043.c:1360: wtimer_remove(&axradio_timer);
      002FCE 90 03 3C         [24] 7780 	mov	dptr,#_axradio_timer
      002FD1 12 71 84         [24] 7781 	lcall	_wtimer_remove
                                   7782 ;	..\src\COMMON\easyax5043.c:1361: if (axradio_mode == AXRADIO_MODE_SYNC_ACK_MASTER) {
      002FD4 74 31            [12] 7783 	mov	a,#0x31
      002FD6 B5 0B 26         [24] 7784 	cjne	a,_axradio_mode,00112$
                                   7785 ;	..\src\COMMON\easyax5043.c:1362: axradio_syncstate = syncstate_master_normal;
      002FD9 90 00 B7         [24] 7786 	mov	dptr,#_axradio_syncstate
      002FDC 74 03            [12] 7787 	mov	a,#0x03
      002FDE F0               [24] 7788 	movx	@dptr,a
                                   7789 ;	..\src\COMMON\easyax5043.c:1363: axradio_sync_settimeradv(axradio_sync_xoscstartup);
      002FDF 90 7A FA         [24] 7790 	mov	dptr,#_axradio_sync_xoscstartup
      002FE2 E4               [12] 7791 	clr	a
      002FE3 93               [24] 7792 	movc	a,@a+dptr
      002FE4 FC               [12] 7793 	mov	r4,a
      002FE5 74 01            [12] 7794 	mov	a,#0x01
      002FE7 93               [24] 7795 	movc	a,@a+dptr
      002FE8 FD               [12] 7796 	mov	r5,a
      002FE9 74 02            [12] 7797 	mov	a,#0x02
      002FEB 93               [24] 7798 	movc	a,@a+dptr
      002FEC FE               [12] 7799 	mov	r6,a
      002FED 74 03            [12] 7800 	mov	a,#0x03
      002FEF 93               [24] 7801 	movc	a,@a+dptr
      002FF0 8C 82            [24] 7802 	mov	dpl,r4
      002FF2 8D 83            [24] 7803 	mov	dph,r5
      002FF4 8E F0            [24] 7804 	mov	b,r6
      002FF6 12 24 C1         [24] 7805 	lcall	_axradio_sync_settimeradv
                                   7806 ;	..\src\COMMON\easyax5043.c:1364: wtimer0_addabsolute(&axradio_timer);
      002FF9 90 03 3C         [24] 7807 	mov	dptr,#_axradio_timer
      002FFC 12 67 3B         [24] 7808 	lcall	_wtimer0_addabsolute
      002FFF                       7809 00112$:
                                   7810 ;	..\src\COMMON\easyax5043.c:1366: wtimer_remove_callback(&axradio_cb_transmitend.cb);
      002FFF 90 03 28         [24] 7811 	mov	dptr,#_axradio_cb_transmitend
      003002 12 74 C0         [24] 7812 	lcall	_wtimer_remove_callback
                                   7813 ;	..\src\COMMON\easyax5043.c:1367: axradio_cb_transmitend.st.error = AXRADIO_ERR_NOERROR;
      003005 90 03 2D         [24] 7814 	mov	dptr,#(_axradio_cb_transmitend + 0x0005)
      003008 E4               [12] 7815 	clr	a
      003009 F0               [24] 7816 	movx	@dptr,a
                                   7817 ;	..\src\COMMON\easyax5043.c:1368: axradio_cb_transmitend.st.time.t = radio_read24((uint16_t)&AX5043_TIMER2);
      00300A 7E 59            [12] 7818 	mov	r6,#_AX5043_TIMER2
      00300C 7F 40            [12] 7819 	mov	r7,#(_AX5043_TIMER2 >> 8)
      00300E 8E 82            [24] 7820 	mov	dpl,r6
      003010 8F 83            [24] 7821 	mov	dph,r7
      003012 12 67 67         [24] 7822 	lcall	_radio_read24
      003015 AC 82            [24] 7823 	mov	r4,dpl
      003017 AD 83            [24] 7824 	mov	r5,dph
      003019 AE F0            [24] 7825 	mov	r6,b
      00301B FF               [12] 7826 	mov	r7,a
      00301C 90 03 2E         [24] 7827 	mov	dptr,#(_axradio_cb_transmitend + 0x0006)
      00301F EC               [12] 7828 	mov	a,r4
      003020 F0               [24] 7829 	movx	@dptr,a
      003021 ED               [12] 7830 	mov	a,r5
      003022 A3               [24] 7831 	inc	dptr
      003023 F0               [24] 7832 	movx	@dptr,a
      003024 EE               [12] 7833 	mov	a,r6
      003025 A3               [24] 7834 	inc	dptr
      003026 F0               [24] 7835 	movx	@dptr,a
      003027 EF               [12] 7836 	mov	a,r7
      003028 A3               [24] 7837 	inc	dptr
      003029 F0               [24] 7838 	movx	@dptr,a
                                   7839 ;	..\src\COMMON\easyax5043.c:1369: wtimer_add_callback(&axradio_cb_transmitend.cb);
      00302A 90 03 28         [24] 7840 	mov	dptr,#_axradio_cb_transmitend
      00302D 12 66 0B         [24] 7841 	lcall	_wtimer_add_callback
      003030                       7842 00114$:
                                   7843 ;	..\src\COMMON\easyax5043.c:1371: if (axradio_framing_destaddrpos != 0xff)
      003030 90 7A DC         [24] 7844 	mov	dptr,#_axradio_framing_destaddrpos
      003033 E4               [12] 7845 	clr	a
      003034 93               [24] 7846 	movc	a,@a+dptr
      003035 FF               [12] 7847 	mov	r7,a
      003036 BF FF 02         [24] 7848 	cjne	r7,#0xff,00292$
      003039 80 34            [24] 7849 	sjmp	00118$
      00303B                       7850 00292$:
                                   7851 ;	..\src\COMMON\easyax5043.c:1372: memcpy_xdata(&axradio_cb_receive.st.rx.mac.localaddr, &axradio_cb_receive.st.rx.mac.raw[axradio_framing_destaddrpos], axradio_framing_addrlen);
      00303B 90 03 01         [24] 7852 	mov	dptr,#(_axradio_cb_receive + 0x001c)
      00303E E0               [24] 7853 	movx	a,@dptr
      00303F FD               [12] 7854 	mov	r5,a
      003040 A3               [24] 7855 	inc	dptr
      003041 E0               [24] 7856 	movx	a,@dptr
      003042 FE               [12] 7857 	mov	r6,a
      003043 EF               [12] 7858 	mov	a,r7
      003044 2D               [12] 7859 	add	a,r5
      003045 FF               [12] 7860 	mov	r7,a
      003046 E4               [12] 7861 	clr	a
      003047 3E               [12] 7862 	addc	a,r6
      003048 FC               [12] 7863 	mov	r4,a
      003049 7E 00            [12] 7864 	mov	r6,#0x00
      00304B 90 7A DB         [24] 7865 	mov	dptr,#_axradio_framing_addrlen
      00304E E4               [12] 7866 	clr	a
      00304F 93               [24] 7867 	movc	a,@a+dptr
      003050 FD               [12] 7868 	mov	r5,a
      003051 7B 00            [12] 7869 	mov	r3,#0x00
      003053 90 03 E2         [24] 7870 	mov	dptr,#_memcpy_PARM_2
      003056 EF               [12] 7871 	mov	a,r7
      003057 F0               [24] 7872 	movx	@dptr,a
      003058 EC               [12] 7873 	mov	a,r4
      003059 A3               [24] 7874 	inc	dptr
      00305A F0               [24] 7875 	movx	@dptr,a
      00305B EE               [12] 7876 	mov	a,r6
      00305C A3               [24] 7877 	inc	dptr
      00305D F0               [24] 7878 	movx	@dptr,a
      00305E 90 03 E5         [24] 7879 	mov	dptr,#_memcpy_PARM_3
      003061 ED               [12] 7880 	mov	a,r5
      003062 F0               [24] 7881 	movx	@dptr,a
      003063 EB               [12] 7882 	mov	a,r3
      003064 A3               [24] 7883 	inc	dptr
      003065 F0               [24] 7884 	movx	@dptr,a
      003066 90 02 FD         [24] 7885 	mov	dptr,#(_axradio_cb_receive + 0x0018)
      003069 75 F0 00         [24] 7886 	mov	b,#0x00
      00306C 12 65 97         [24] 7887 	lcall	_memcpy
      00306F                       7888 00118$:
                                   7889 ;	..\src\COMMON\easyax5043.c:1373: if (axradio_framing_sourceaddrpos != 0xff)
      00306F 90 7A DD         [24] 7890 	mov	dptr,#_axradio_framing_sourceaddrpos
      003072 E4               [12] 7891 	clr	a
      003073 93               [24] 7892 	movc	a,@a+dptr
      003074 FF               [12] 7893 	mov	r7,a
      003075 BF FF 02         [24] 7894 	cjne	r7,#0xff,00293$
      003078 80 34            [24] 7895 	sjmp	00120$
      00307A                       7896 00293$:
                                   7897 ;	..\src\COMMON\easyax5043.c:1374: memcpy_xdata(&axradio_cb_receive.st.rx.mac.remoteaddr, &axradio_cb_receive.st.rx.mac.raw[axradio_framing_sourceaddrpos], axradio_framing_addrlen);
      00307A 90 03 01         [24] 7898 	mov	dptr,#(_axradio_cb_receive + 0x001c)
      00307D E0               [24] 7899 	movx	a,@dptr
      00307E FD               [12] 7900 	mov	r5,a
      00307F A3               [24] 7901 	inc	dptr
      003080 E0               [24] 7902 	movx	a,@dptr
      003081 FE               [12] 7903 	mov	r6,a
      003082 EF               [12] 7904 	mov	a,r7
      003083 2D               [12] 7905 	add	a,r5
      003084 FF               [12] 7906 	mov	r7,a
      003085 E4               [12] 7907 	clr	a
      003086 3E               [12] 7908 	addc	a,r6
      003087 FC               [12] 7909 	mov	r4,a
      003088 7E 00            [12] 7910 	mov	r6,#0x00
      00308A 90 7A DB         [24] 7911 	mov	dptr,#_axradio_framing_addrlen
      00308D E4               [12] 7912 	clr	a
      00308E 93               [24] 7913 	movc	a,@a+dptr
      00308F FD               [12] 7914 	mov	r5,a
      003090 7B 00            [12] 7915 	mov	r3,#0x00
      003092 90 03 E2         [24] 7916 	mov	dptr,#_memcpy_PARM_2
      003095 EF               [12] 7917 	mov	a,r7
      003096 F0               [24] 7918 	movx	@dptr,a
      003097 EC               [12] 7919 	mov	a,r4
      003098 A3               [24] 7920 	inc	dptr
      003099 F0               [24] 7921 	movx	@dptr,a
      00309A EE               [12] 7922 	mov	a,r6
      00309B A3               [24] 7923 	inc	dptr
      00309C F0               [24] 7924 	movx	@dptr,a
      00309D 90 03 E5         [24] 7925 	mov	dptr,#_memcpy_PARM_3
      0030A0 ED               [12] 7926 	mov	a,r5
      0030A1 F0               [24] 7927 	movx	@dptr,a
      0030A2 EB               [12] 7928 	mov	a,r3
      0030A3 A3               [24] 7929 	inc	dptr
      0030A4 F0               [24] 7930 	movx	@dptr,a
      0030A5 90 02 F9         [24] 7931 	mov	dptr,#(_axradio_cb_receive + 0x0014)
      0030A8 75 F0 00         [24] 7932 	mov	b,#0x00
      0030AB 12 65 97         [24] 7933 	lcall	_memcpy
      0030AE                       7934 00120$:
                                   7935 ;	..\src\COMMON\easyax5043.c:1375: if (axradio_mode == AXRADIO_MODE_ACK_RECEIVE ||
      0030AE 74 22            [12] 7936 	mov	a,#0x22
      0030B0 B5 0B 02         [24] 7937 	cjne	a,_axradio_mode,00294$
      0030B3 80 11            [24] 7938 	sjmp	00142$
      0030B5                       7939 00294$:
                                   7940 ;	..\src\COMMON\easyax5043.c:1376: axradio_mode == AXRADIO_MODE_WOR_ACK_RECEIVE ||
      0030B5 74 23            [12] 7941 	mov	a,#0x23
      0030B7 B5 0B 02         [24] 7942 	cjne	a,_axradio_mode,00295$
      0030BA 80 0A            [24] 7943 	sjmp	00142$
      0030BC                       7944 00295$:
                                   7945 ;	..\src\COMMON\easyax5043.c:1377: axradio_mode == AXRADIO_MODE_SYNC_ACK_SLAVE) {
      0030BC 74 33            [12] 7946 	mov	a,#0x33
      0030BE B5 0B 02         [24] 7947 	cjne	a,_axradio_mode,00296$
      0030C1 80 03            [24] 7948 	sjmp	00297$
      0030C3                       7949 00296$:
      0030C3 02 32 D1         [24] 7950 	ljmp	00143$
      0030C6                       7951 00297$:
      0030C6                       7952 00142$:
                                   7953 ;	..\src\COMMON\easyax5043.c:1378: axradio_ack_count = 0;
      0030C6 90 00 C1         [24] 7954 	mov	dptr,#_axradio_ack_count
      0030C9 E4               [12] 7955 	clr	a
      0030CA F0               [24] 7956 	movx	@dptr,a
                                   7957 ;	..\src\COMMON\easyax5043.c:1379: axradio_txbuffer_len = axradio_framing_maclen + axradio_framing_minpayloadlen;
      0030CB 90 7A DA         [24] 7958 	mov	dptr,#_axradio_framing_maclen
                                   7959 ;	genFromRTrack removed	clr	a
      0030CE 93               [24] 7960 	movc	a,@a+dptr
      0030CF FF               [12] 7961 	mov	r7,a
      0030D0 FD               [12] 7962 	mov	r5,a
      0030D1 7E 00            [12] 7963 	mov	r6,#0x00
      0030D3 90 7A F3         [24] 7964 	mov	dptr,#_axradio_framing_minpayloadlen
      0030D6 E4               [12] 7965 	clr	a
      0030D7 93               [24] 7966 	movc	a,@a+dptr
      0030D8 FC               [12] 7967 	mov	r4,a
      0030D9 7B 00            [12] 7968 	mov	r3,#0x00
      0030DB 90 00 B8         [24] 7969 	mov	dptr,#_axradio_txbuffer_len
      0030DE EC               [12] 7970 	mov	a,r4
      0030DF 2D               [12] 7971 	add	a,r5
      0030E0 F0               [24] 7972 	movx	@dptr,a
      0030E1 EB               [12] 7973 	mov	a,r3
      0030E2 3E               [12] 7974 	addc	a,r6
      0030E3 A3               [24] 7975 	inc	dptr
      0030E4 F0               [24] 7976 	movx	@dptr,a
                                   7977 ;	..\src\COMMON\easyax5043.c:1380: memset_xdata(axradio_txbuffer, 0, axradio_framing_maclen);
      0030E5 7E 00            [12] 7978 	mov	r6,#0x00
      0030E7 90 03 CD         [24] 7979 	mov	dptr,#_memset_PARM_2
      0030EA E4               [12] 7980 	clr	a
      0030EB F0               [24] 7981 	movx	@dptr,a
      0030EC 90 03 CE         [24] 7982 	mov	dptr,#_memset_PARM_3
      0030EF EF               [12] 7983 	mov	a,r7
      0030F0 F0               [24] 7984 	movx	@dptr,a
      0030F1 EE               [12] 7985 	mov	a,r6
      0030F2 A3               [24] 7986 	inc	dptr
      0030F3 F0               [24] 7987 	movx	@dptr,a
      0030F4 90 00 DD         [24] 7988 	mov	dptr,#_axradio_txbuffer
      0030F7 75 F0 00         [24] 7989 	mov	b,#0x00
      0030FA 12 62 A1         [24] 7990 	lcall	_memset
                                   7991 ;	..\src\COMMON\easyax5043.c:1381: if (axradio_framing_ack_seqnrpos != 0xff) {
      0030FD 90 7A F2         [24] 7992 	mov	dptr,#_axradio_framing_ack_seqnrpos
      003100 E4               [12] 7993 	clr	a
      003101 93               [24] 7994 	movc	a,@a+dptr
      003102 FF               [12] 7995 	mov	r7,a
      003103 BF FF 02         [24] 7996 	cjne	r7,#0xff,00298$
      003106 80 35            [24] 7997 	sjmp	00125$
      003108                       7998 00298$:
                                   7999 ;	..\src\COMMON\easyax5043.c:1382: uint8_t seqnr = axradio_cb_receive.st.rx.mac.raw[axradio_framing_ack_seqnrpos];
      003108 90 03 01         [24] 8000 	mov	dptr,#(_axradio_cb_receive + 0x001c)
      00310B E0               [24] 8001 	movx	a,@dptr
      00310C FD               [12] 8002 	mov	r5,a
      00310D A3               [24] 8003 	inc	dptr
      00310E E0               [24] 8004 	movx	a,@dptr
      00310F FE               [12] 8005 	mov	r6,a
      003110 EF               [12] 8006 	mov	a,r7
      003111 2D               [12] 8007 	add	a,r5
      003112 F5 82            [12] 8008 	mov	dpl,a
      003114 E4               [12] 8009 	clr	a
      003115 3E               [12] 8010 	addc	a,r6
      003116 F5 83            [12] 8011 	mov	dph,a
      003118 E0               [24] 8012 	movx	a,@dptr
      003119 FE               [12] 8013 	mov	r6,a
                                   8014 ;	..\src\COMMON\easyax5043.c:1383: axradio_txbuffer[axradio_framing_ack_seqnrpos] = seqnr;
      00311A EF               [12] 8015 	mov	a,r7
      00311B 24 DD            [12] 8016 	add	a,#_axradio_txbuffer
      00311D F5 82            [12] 8017 	mov	dpl,a
      00311F E4               [12] 8018 	clr	a
      003120 34 00            [12] 8019 	addc	a,#(_axradio_txbuffer >> 8)
      003122 F5 83            [12] 8020 	mov	dph,a
      003124 EE               [12] 8021 	mov	a,r6
      003125 F0               [24] 8022 	movx	@dptr,a
                                   8023 ;	..\src\COMMON\easyax5043.c:1384: if (axradio_ack_seqnr != seqnr)
      003126 90 00 C2         [24] 8024 	mov	dptr,#_axradio_ack_seqnr
      003129 E0               [24] 8025 	movx	a,@dptr
      00312A FF               [12] 8026 	mov	r7,a
      00312B B5 06 02         [24] 8027 	cjne	a,ar6,00299$
      00312E 80 07            [24] 8028 	sjmp	00122$
      003130                       8029 00299$:
                                   8030 ;	..\src\COMMON\easyax5043.c:1385: axradio_ack_seqnr = seqnr;
      003130 90 00 C2         [24] 8031 	mov	dptr,#_axradio_ack_seqnr
      003133 EE               [12] 8032 	mov	a,r6
      003134 F0               [24] 8033 	movx	@dptr,a
      003135 80 06            [24] 8034 	sjmp	00125$
      003137                       8035 00122$:
                                   8036 ;	..\src\COMMON\easyax5043.c:1387: axradio_cb_receive.st.error = AXRADIO_ERR_RETRANSMISSION;
      003137 90 02 EA         [24] 8037 	mov	dptr,#(_axradio_cb_receive + 0x0005)
      00313A 74 08            [12] 8038 	mov	a,#0x08
      00313C F0               [24] 8039 	movx	@dptr,a
      00313D                       8040 00125$:
                                   8041 ;	..\src\COMMON\easyax5043.c:1389: if (axradio_framing_destaddrpos != 0xff) {
      00313D 90 7A DC         [24] 8042 	mov	dptr,#_axradio_framing_destaddrpos
      003140 E4               [12] 8043 	clr	a
      003141 93               [24] 8044 	movc	a,@a+dptr
      003142 FF               [12] 8045 	mov	r7,a
      003143 BF FF 02         [24] 8046 	cjne	r7,#0xff,00300$
      003146 80 6D            [24] 8047 	sjmp	00130$
      003148                       8048 00300$:
                                   8049 ;	..\src\COMMON\easyax5043.c:1390: if (axradio_framing_sourceaddrpos != 0xff)
      003148 90 7A DD         [24] 8050 	mov	dptr,#_axradio_framing_sourceaddrpos
      00314B E4               [12] 8051 	clr	a
      00314C 93               [24] 8052 	movc	a,@a+dptr
      00314D FE               [12] 8053 	mov	r6,a
      00314E BE FF 02         [24] 8054 	cjne	r6,#0xff,00301$
      003151 80 32            [24] 8055 	sjmp	00127$
      003153                       8056 00301$:
                                   8057 ;	..\src\COMMON\easyax5043.c:1391: memcpy_xdata(&axradio_txbuffer[axradio_framing_destaddrpos], &axradio_cb_receive.st.rx.mac.remoteaddr, axradio_framing_addrlen);
      003153 EF               [12] 8058 	mov	a,r7
      003154 24 DD            [12] 8059 	add	a,#_axradio_txbuffer
      003156 FD               [12] 8060 	mov	r5,a
      003157 E4               [12] 8061 	clr	a
      003158 34 00            [12] 8062 	addc	a,#(_axradio_txbuffer >> 8)
      00315A FE               [12] 8063 	mov	r6,a
      00315B 7C 00            [12] 8064 	mov	r4,#0x00
      00315D 90 7A DB         [24] 8065 	mov	dptr,#_axradio_framing_addrlen
      003160 E4               [12] 8066 	clr	a
      003161 93               [24] 8067 	movc	a,@a+dptr
      003162 FB               [12] 8068 	mov	r3,a
      003163 7A 00            [12] 8069 	mov	r2,#0x00
      003165 90 03 E2         [24] 8070 	mov	dptr,#_memcpy_PARM_2
      003168 74 F9            [12] 8071 	mov	a,#(_axradio_cb_receive + 0x0014)
      00316A F0               [24] 8072 	movx	@dptr,a
      00316B 74 02            [12] 8073 	mov	a,#((_axradio_cb_receive + 0x0014) >> 8)
      00316D A3               [24] 8074 	inc	dptr
      00316E F0               [24] 8075 	movx	@dptr,a
      00316F E4               [12] 8076 	clr	a
      003170 A3               [24] 8077 	inc	dptr
      003171 F0               [24] 8078 	movx	@dptr,a
      003172 90 03 E5         [24] 8079 	mov	dptr,#_memcpy_PARM_3
      003175 EB               [12] 8080 	mov	a,r3
      003176 F0               [24] 8081 	movx	@dptr,a
      003177 EA               [12] 8082 	mov	a,r2
      003178 A3               [24] 8083 	inc	dptr
      003179 F0               [24] 8084 	movx	@dptr,a
      00317A 8D 82            [24] 8085 	mov	dpl,r5
      00317C 8E 83            [24] 8086 	mov	dph,r6
      00317E 8C F0            [24] 8087 	mov	b,r4
      003180 12 65 97         [24] 8088 	lcall	_memcpy
      003183 80 30            [24] 8089 	sjmp	00130$
      003185                       8090 00127$:
                                   8091 ;	..\src\COMMON\easyax5043.c:1393: memcpy_xdata(&axradio_txbuffer[axradio_framing_destaddrpos], &axradio_default_remoteaddr, axradio_framing_addrlen);
      003185 EF               [12] 8092 	mov	a,r7
      003186 24 DD            [12] 8093 	add	a,#_axradio_txbuffer
      003188 FF               [12] 8094 	mov	r7,a
      003189 E4               [12] 8095 	clr	a
      00318A 34 00            [12] 8096 	addc	a,#(_axradio_txbuffer >> 8)
      00318C FE               [12] 8097 	mov	r6,a
      00318D 7D 00            [12] 8098 	mov	r5,#0x00
      00318F 90 7A DB         [24] 8099 	mov	dptr,#_axradio_framing_addrlen
      003192 E4               [12] 8100 	clr	a
      003193 93               [24] 8101 	movc	a,@a+dptr
      003194 FC               [12] 8102 	mov	r4,a
      003195 7B 00            [12] 8103 	mov	r3,#0x00
      003197 90 03 E2         [24] 8104 	mov	dptr,#_memcpy_PARM_2
      00319A 74 D9            [12] 8105 	mov	a,#_axradio_default_remoteaddr
      00319C F0               [24] 8106 	movx	@dptr,a
      00319D 74 00            [12] 8107 	mov	a,#(_axradio_default_remoteaddr >> 8)
      00319F A3               [24] 8108 	inc	dptr
      0031A0 F0               [24] 8109 	movx	@dptr,a
      0031A1 E4               [12] 8110 	clr	a
      0031A2 A3               [24] 8111 	inc	dptr
      0031A3 F0               [24] 8112 	movx	@dptr,a
      0031A4 90 03 E5         [24] 8113 	mov	dptr,#_memcpy_PARM_3
      0031A7 EC               [12] 8114 	mov	a,r4
      0031A8 F0               [24] 8115 	movx	@dptr,a
      0031A9 EB               [12] 8116 	mov	a,r3
      0031AA A3               [24] 8117 	inc	dptr
      0031AB F0               [24] 8118 	movx	@dptr,a
      0031AC 8F 82            [24] 8119 	mov	dpl,r7
      0031AE 8E 83            [24] 8120 	mov	dph,r6
      0031B0 8D F0            [24] 8121 	mov	b,r5
      0031B2 12 65 97         [24] 8122 	lcall	_memcpy
      0031B5                       8123 00130$:
                                   8124 ;	..\src\COMMON\easyax5043.c:1395: if (axradio_framing_sourceaddrpos != 0xff)
      0031B5 90 7A DD         [24] 8125 	mov	dptr,#_axradio_framing_sourceaddrpos
      0031B8 E4               [12] 8126 	clr	a
      0031B9 93               [24] 8127 	movc	a,@a+dptr
      0031BA FF               [12] 8128 	mov	r7,a
      0031BB BF FF 02         [24] 8129 	cjne	r7,#0xff,00302$
      0031BE 80 30            [24] 8130 	sjmp	00132$
      0031C0                       8131 00302$:
                                   8132 ;	..\src\COMMON\easyax5043.c:1396: memcpy_xdata(&axradio_txbuffer[axradio_framing_sourceaddrpos], &axradio_localaddr.addr, axradio_framing_addrlen);
      0031C0 EF               [12] 8133 	mov	a,r7
      0031C1 24 DD            [12] 8134 	add	a,#_axradio_txbuffer
      0031C3 FF               [12] 8135 	mov	r7,a
      0031C4 E4               [12] 8136 	clr	a
      0031C5 34 00            [12] 8137 	addc	a,#(_axradio_txbuffer >> 8)
      0031C7 FE               [12] 8138 	mov	r6,a
      0031C8 7D 00            [12] 8139 	mov	r5,#0x00
      0031CA 90 7A DB         [24] 8140 	mov	dptr,#_axradio_framing_addrlen
      0031CD E4               [12] 8141 	clr	a
      0031CE 93               [24] 8142 	movc	a,@a+dptr
      0031CF FC               [12] 8143 	mov	r4,a
      0031D0 7B 00            [12] 8144 	mov	r3,#0x00
      0031D2 90 03 E2         [24] 8145 	mov	dptr,#_memcpy_PARM_2
      0031D5 74 D1            [12] 8146 	mov	a,#_axradio_localaddr
      0031D7 F0               [24] 8147 	movx	@dptr,a
      0031D8 74 00            [12] 8148 	mov	a,#(_axradio_localaddr >> 8)
      0031DA A3               [24] 8149 	inc	dptr
      0031DB F0               [24] 8150 	movx	@dptr,a
      0031DC E4               [12] 8151 	clr	a
      0031DD A3               [24] 8152 	inc	dptr
      0031DE F0               [24] 8153 	movx	@dptr,a
      0031DF 90 03 E5         [24] 8154 	mov	dptr,#_memcpy_PARM_3
      0031E2 EC               [12] 8155 	mov	a,r4
      0031E3 F0               [24] 8156 	movx	@dptr,a
      0031E4 EB               [12] 8157 	mov	a,r3
      0031E5 A3               [24] 8158 	inc	dptr
      0031E6 F0               [24] 8159 	movx	@dptr,a
      0031E7 8F 82            [24] 8160 	mov	dpl,r7
      0031E9 8E 83            [24] 8161 	mov	dph,r6
      0031EB 8D F0            [24] 8162 	mov	b,r5
      0031ED 12 65 97         [24] 8163 	lcall	_memcpy
      0031F0                       8164 00132$:
                                   8165 ;	..\src\COMMON\easyax5043.c:1397: if (axradio_framing_lenmask) {
      0031F0 90 7A E0         [24] 8166 	mov	dptr,#_axradio_framing_lenmask
      0031F3 E4               [12] 8167 	clr	a
      0031F4 93               [24] 8168 	movc	a,@a+dptr
      0031F5 FF               [12] 8169 	mov	r7,a
      0031F6 60 30            [24] 8170 	jz	00134$
                                   8171 ;	..\src\COMMON\easyax5043.c:1398: uint8_t len_byte = (uint8_t)(axradio_txbuffer_len - axradio_framing_lenoffs) & axradio_framing_lenmask; // if you prefer not counting the len byte itself, set LENOFFS = 1
      0031F8 90 00 B8         [24] 8172 	mov	dptr,#_axradio_txbuffer_len
      0031FB E0               [24] 8173 	movx	a,@dptr
      0031FC FD               [12] 8174 	mov	r5,a
      0031FD A3               [24] 8175 	inc	dptr
      0031FE E0               [24] 8176 	movx	a,@dptr
      0031FF 90 7A DF         [24] 8177 	mov	dptr,#_axradio_framing_lenoffs
      003202 E4               [12] 8178 	clr	a
      003203 93               [24] 8179 	movc	a,@a+dptr
      003204 FE               [12] 8180 	mov	r6,a
      003205 ED               [12] 8181 	mov	a,r5
      003206 C3               [12] 8182 	clr	c
      003207 9E               [12] 8183 	subb	a,r6
      003208 5F               [12] 8184 	anl	a,r7
      003209 FE               [12] 8185 	mov	r6,a
                                   8186 ;	..\src\COMMON\easyax5043.c:1399: axradio_txbuffer[axradio_framing_lenpos] = (axradio_txbuffer[axradio_framing_lenpos] & (uint8_t)~axradio_framing_lenmask) | len_byte;
      00320A 90 7A DE         [24] 8187 	mov	dptr,#_axradio_framing_lenpos
      00320D E4               [12] 8188 	clr	a
      00320E 93               [24] 8189 	movc	a,@a+dptr
      00320F 24 DD            [12] 8190 	add	a,#_axradio_txbuffer
      003211 FD               [12] 8191 	mov	r5,a
      003212 E4               [12] 8192 	clr	a
      003213 34 00            [12] 8193 	addc	a,#(_axradio_txbuffer >> 8)
      003215 FC               [12] 8194 	mov	r4,a
      003216 8D 82            [24] 8195 	mov	dpl,r5
      003218 8C 83            [24] 8196 	mov	dph,r4
      00321A E0               [24] 8197 	movx	a,@dptr
      00321B FB               [12] 8198 	mov	r3,a
      00321C EF               [12] 8199 	mov	a,r7
      00321D F4               [12] 8200 	cpl	a
      00321E FF               [12] 8201 	mov	r7,a
      00321F 5B               [12] 8202 	anl	a,r3
      003220 42 06            [12] 8203 	orl	ar6,a
      003222 8D 82            [24] 8204 	mov	dpl,r5
      003224 8C 83            [24] 8205 	mov	dph,r4
      003226 EE               [12] 8206 	mov	a,r6
      003227 F0               [24] 8207 	movx	@dptr,a
      003228                       8208 00134$:
                                   8209 ;	..\src\COMMON\easyax5043.c:1401: if (axradio_framing_swcrclen)
      003228 90 7A E1         [24] 8210 	mov	dptr,#_axradio_framing_swcrclen
      00322B E4               [12] 8211 	clr	a
      00322C 93               [24] 8212 	movc	a,@a+dptr
      00322D 60 20            [24] 8213 	jz	00136$
                                   8214 ;	..\src\COMMON\easyax5043.c:1402: axradio_txbuffer_len = axradio_framing_append_crc(axradio_txbuffer, axradio_txbuffer_len);
      00322F 90 00 B8         [24] 8215 	mov	dptr,#_axradio_txbuffer_len
      003232 E0               [24] 8216 	movx	a,@dptr
      003233 C0 E0            [24] 8217 	push	acc
      003235 A3               [24] 8218 	inc	dptr
      003236 E0               [24] 8219 	movx	a,@dptr
      003237 C0 E0            [24] 8220 	push	acc
      003239 90 00 DD         [24] 8221 	mov	dptr,#_axradio_txbuffer
      00323C 12 0A A6         [24] 8222 	lcall	_axradio_framing_append_crc
      00323F AE 82            [24] 8223 	mov	r6,dpl
      003241 AF 83            [24] 8224 	mov	r7,dph
      003243 15 81            [12] 8225 	dec	sp
      003245 15 81            [12] 8226 	dec	sp
      003247 90 00 B8         [24] 8227 	mov	dptr,#_axradio_txbuffer_len
      00324A EE               [12] 8228 	mov	a,r6
      00324B F0               [24] 8229 	movx	@dptr,a
      00324C EF               [12] 8230 	mov	a,r7
      00324D A3               [24] 8231 	inc	dptr
      00324E F0               [24] 8232 	movx	@dptr,a
      00324F                       8233 00136$:
                                   8234 ;	..\src\COMMON\easyax5043.c:1403: if (axradio_phy_pn9) {
      00324F 90 7A B9         [24] 8235 	mov	dptr,#_axradio_phy_pn9
      003252 E4               [12] 8236 	clr	a
      003253 93               [24] 8237 	movc	a,@a+dptr
      003254 60 2F            [24] 8238 	jz	00138$
                                   8239 ;	..\src\COMMON\easyax5043.c:1404: pn9_buffer(axradio_txbuffer, axradio_txbuffer_len, 0x1ff, -(AX5043_ENCODING & 0x01));
      003256 90 40 11         [24] 8240 	mov	dptr,#_AX5043_ENCODING
      003259 E0               [24] 8241 	movx	a,@dptr
      00325A FF               [12] 8242 	mov	r7,a
      00325B 53 07 01         [24] 8243 	anl	ar7,#0x01
      00325E C3               [12] 8244 	clr	c
      00325F E4               [12] 8245 	clr	a
      003260 9F               [12] 8246 	subb	a,r7
      003261 FF               [12] 8247 	mov	r7,a
      003262 C0 07            [24] 8248 	push	ar7
      003264 74 FF            [12] 8249 	mov	a,#0xff
      003266 C0 E0            [24] 8250 	push	acc
      003268 74 01            [12] 8251 	mov	a,#0x01
      00326A C0 E0            [24] 8252 	push	acc
      00326C 90 00 B8         [24] 8253 	mov	dptr,#_axradio_txbuffer_len
      00326F E0               [24] 8254 	movx	a,@dptr
      003270 C0 E0            [24] 8255 	push	acc
      003272 A3               [24] 8256 	inc	dptr
      003273 E0               [24] 8257 	movx	a,@dptr
      003274 C0 E0            [24] 8258 	push	acc
      003276 90 00 DD         [24] 8259 	mov	dptr,#_axradio_txbuffer
      003279 75 F0 00         [24] 8260 	mov	b,#0x00
      00327C 12 67 8E         [24] 8261 	lcall	_pn9_buffer
      00327F E5 81            [12] 8262 	mov	a,sp
      003281 24 FB            [12] 8263 	add	a,#0xfb
      003283 F5 81            [12] 8264 	mov	sp,a
      003285                       8265 00138$:
                                   8266 ;	..\src\COMMON\easyax5043.c:1406: AX5043_IRQMASK1 = 0x00;
      003285 90 40 06         [24] 8267 	mov	dptr,#_AX5043_IRQMASK1
      003288 E4               [12] 8268 	clr	a
      003289 F0               [24] 8269 	movx	@dptr,a
                                   8270 ;	..\src\COMMON\easyax5043.c:1407: AX5043_IRQMASK0 = 0x00;
      00328A 90 40 07         [24] 8271 	mov	dptr,#_AX5043_IRQMASK0
      00328D F0               [24] 8272 	movx	@dptr,a
                                   8273 ;	..\src\COMMON\easyax5043.c:1408: AX5043_PWRMODE = AX5043_PWRSTATE_XTAL_ON;
      00328E 90 40 02         [24] 8274 	mov	dptr,#_AX5043_PWRMODE
      003291 74 05            [12] 8275 	mov	a,#0x05
      003293 F0               [24] 8276 	movx	@dptr,a
                                   8277 ;	..\src\COMMON\easyax5043.c:1409: AX5043_FIFOSTAT = 3;
      003294 90 40 28         [24] 8278 	mov	dptr,#_AX5043_FIFOSTAT
      003297 74 03            [12] 8279 	mov	a,#0x03
      003299 F0               [24] 8280 	movx	@dptr,a
                                   8281 ;	..\src\COMMON\easyax5043.c:1410: axradio_trxstate = trxstate_tx_longpreamble; // ensure that trxstate != off, otherwise we would prematurely enable the receiver, see below
      00329A 75 0C 0A         [24] 8282 	mov	_axradio_trxstate,#0x0a
                                   8283 ;	..\src\COMMON\easyax5043.c:1411: while (AX5043_POWSTAT & 0x08);
      00329D                       8284 00139$:
      00329D 90 40 03         [24] 8285 	mov	dptr,#_AX5043_POWSTAT
      0032A0 E0               [24] 8286 	movx	a,@dptr
      0032A1 FF               [12] 8287 	mov	r7,a
      0032A2 20 E3 F8         [24] 8288 	jb	acc.3,00139$
                                   8289 ;	..\src\COMMON\easyax5043.c:1412: wtimer_remove(&axradio_timer);
      0032A5 90 03 3C         [24] 8290 	mov	dptr,#_axradio_timer
      0032A8 12 71 84         [24] 8291 	lcall	_wtimer_remove
                                   8292 ;	..\src\COMMON\easyax5043.c:1413: axradio_timer.time = axradio_framing_ack_delay;
      0032AB 90 7A ED         [24] 8293 	mov	dptr,#_axradio_framing_ack_delay
      0032AE E4               [12] 8294 	clr	a
      0032AF 93               [24] 8295 	movc	a,@a+dptr
      0032B0 FC               [12] 8296 	mov	r4,a
      0032B1 74 01            [12] 8297 	mov	a,#0x01
      0032B3 93               [24] 8298 	movc	a,@a+dptr
      0032B4 FD               [12] 8299 	mov	r5,a
      0032B5 74 02            [12] 8300 	mov	a,#0x02
      0032B7 93               [24] 8301 	movc	a,@a+dptr
      0032B8 FE               [12] 8302 	mov	r6,a
      0032B9 74 03            [12] 8303 	mov	a,#0x03
      0032BB 93               [24] 8304 	movc	a,@a+dptr
      0032BC FF               [12] 8305 	mov	r7,a
      0032BD 90 03 40         [24] 8306 	mov	dptr,#(_axradio_timer + 0x0004)
      0032C0 EC               [12] 8307 	mov	a,r4
      0032C1 F0               [24] 8308 	movx	@dptr,a
      0032C2 ED               [12] 8309 	mov	a,r5
      0032C3 A3               [24] 8310 	inc	dptr
      0032C4 F0               [24] 8311 	movx	@dptr,a
      0032C5 EE               [12] 8312 	mov	a,r6
      0032C6 A3               [24] 8313 	inc	dptr
      0032C7 F0               [24] 8314 	movx	@dptr,a
      0032C8 EF               [12] 8315 	mov	a,r7
      0032C9 A3               [24] 8316 	inc	dptr
      0032CA F0               [24] 8317 	movx	@dptr,a
                                   8318 ;	..\src\COMMON\easyax5043.c:1414: wtimer1_addrelative(&axradio_timer);
      0032CB 90 03 3C         [24] 8319 	mov	dptr,#_axradio_timer
      0032CE 12 66 6C         [24] 8320 	lcall	_wtimer1_addrelative
      0032D1                       8321 00143$:
                                   8322 ;	..\src\COMMON\easyax5043.c:1416: if (axradio_mode == AXRADIO_MODE_SYNC_SLAVE ||
      0032D1 74 32            [12] 8323 	mov	a,#0x32
      0032D3 B5 0B 02         [24] 8324 	cjne	a,_axradio_mode,00307$
      0032D6 80 0A            [24] 8325 	sjmp	00156$
      0032D8                       8326 00307$:
                                   8327 ;	..\src\COMMON\easyax5043.c:1417: axradio_mode == AXRADIO_MODE_SYNC_ACK_SLAVE) {
      0032D8 74 33            [12] 8328 	mov	a,#0x33
      0032DA B5 0B 02         [24] 8329 	cjne	a,_axradio_mode,00308$
      0032DD 80 03            [24] 8330 	sjmp	00309$
      0032DF                       8331 00308$:
      0032DF 02 33 B5         [24] 8332 	ljmp	00157$
      0032E2                       8333 00309$:
      0032E2                       8334 00156$:
                                   8335 ;	..\src\COMMON\easyax5043.c:1418: if (axradio_mode != AXRADIO_MODE_SYNC_ACK_SLAVE)
      0032E2 74 33            [12] 8336 	mov	a,#0x33
      0032E4 B5 0B 02         [24] 8337 	cjne	a,_axradio_mode,00310$
      0032E7 80 03            [24] 8338 	sjmp	00147$
      0032E9                       8339 00310$:
                                   8340 ;	..\src\COMMON\easyax5043.c:1419: ax5043_off();
      0032E9 12 22 CE         [24] 8341 	lcall	_ax5043_off
      0032EC                       8342 00147$:
                                   8343 ;	..\src\COMMON\easyax5043.c:1420: switch (axradio_syncstate) {
      0032EC 90 00 B7         [24] 8344 	mov	dptr,#_axradio_syncstate
      0032EF E0               [24] 8345 	movx	a,@dptr
      0032F0 FF               [12] 8346 	mov	r7,a
      0032F1 BF 08 02         [24] 8347 	cjne	r7,#0x08,00311$
      0032F4 80 45            [24] 8348 	sjmp	00151$
      0032F6                       8349 00311$:
      0032F6 BF 0A 02         [24] 8350 	cjne	r7,#0x0a,00312$
      0032F9 80 40            [24] 8351 	sjmp	00151$
      0032FB                       8352 00312$:
      0032FB BF 0B 02         [24] 8353 	cjne	r7,#0x0b,00313$
      0032FE 80 3B            [24] 8354 	sjmp	00151$
      003300                       8355 00313$:
                                   8356 ;	..\src\COMMON\easyax5043.c:1424: axradio_sync_time = axradio_conv_time_totimer0(axradio_cb_receive.st.time.t);
      003300 90 02 EB         [24] 8357 	mov	dptr,#(_axradio_cb_receive + 0x0006)
      003303 E0               [24] 8358 	movx	a,@dptr
      003304 FC               [12] 8359 	mov	r4,a
      003305 A3               [24] 8360 	inc	dptr
      003306 E0               [24] 8361 	movx	a,@dptr
      003307 FD               [12] 8362 	mov	r5,a
      003308 A3               [24] 8363 	inc	dptr
      003309 E0               [24] 8364 	movx	a,@dptr
      00330A FE               [12] 8365 	mov	r6,a
      00330B A3               [24] 8366 	inc	dptr
      00330C E0               [24] 8367 	movx	a,@dptr
      00330D 8C 82            [24] 8368 	mov	dpl,r4
      00330F 8D 83            [24] 8369 	mov	dph,r5
      003311 8E F0            [24] 8370 	mov	b,r6
      003313 12 16 01         [24] 8371 	lcall	_axradio_conv_time_totimer0
      003316 AC 82            [24] 8372 	mov	r4,dpl
      003318 AD 83            [24] 8373 	mov	r5,dph
      00331A AE F0            [24] 8374 	mov	r6,b
      00331C FF               [12] 8375 	mov	r7,a
      00331D 90 00 C3         [24] 8376 	mov	dptr,#_axradio_sync_time
      003320 EC               [12] 8377 	mov	a,r4
      003321 F0               [24] 8378 	movx	@dptr,a
      003322 ED               [12] 8379 	mov	a,r5
      003323 A3               [24] 8380 	inc	dptr
      003324 F0               [24] 8381 	movx	@dptr,a
      003325 EE               [12] 8382 	mov	a,r6
      003326 A3               [24] 8383 	inc	dptr
      003327 F0               [24] 8384 	movx	@dptr,a
      003328 EF               [12] 8385 	mov	a,r7
      003329 A3               [24] 8386 	inc	dptr
      00332A F0               [24] 8387 	movx	@dptr,a
                                   8388 ;	..\src\COMMON\easyax5043.c:1425: axradio_sync_periodcorr = -32768;
      00332B 90 00 C7         [24] 8389 	mov	dptr,#_axradio_sync_periodcorr
      00332E E4               [12] 8390 	clr	a
      00332F F0               [24] 8391 	movx	@dptr,a
      003330 74 80            [12] 8392 	mov	a,#0x80
      003332 A3               [24] 8393 	inc	dptr
      003333 F0               [24] 8394 	movx	@dptr,a
                                   8395 ;	..\src\COMMON\easyax5043.c:1426: axradio_sync_seqnr = 0;
      003334 90 00 C2         [24] 8396 	mov	dptr,#_axradio_ack_seqnr
      003337 E4               [12] 8397 	clr	a
      003338 F0               [24] 8398 	movx	@dptr,a
                                   8399 ;	..\src\COMMON\easyax5043.c:1427: break;
                                   8400 ;	..\src\COMMON\easyax5043.c:1431: case syncstate_slave_rxpacket:
      003339 80 2D            [24] 8401 	sjmp	00152$
      00333B                       8402 00151$:
                                   8403 ;	..\src\COMMON\easyax5043.c:1432: axradio_sync_adjustperiodcorr();
      00333B 12 25 00         [24] 8404 	lcall	_axradio_sync_adjustperiodcorr
                                   8405 ;	..\src\COMMON\easyax5043.c:1433: axradio_cb_receive.st.rx.phy.period = axradio_sync_periodcorr >> SYNC_K1;
      00333E 90 00 C7         [24] 8406 	mov	dptr,#_axradio_sync_periodcorr
      003341 E0               [24] 8407 	movx	a,@dptr
      003342 FE               [12] 8408 	mov	r6,a
      003343 A3               [24] 8409 	inc	dptr
      003344 E0               [24] 8410 	movx	a,@dptr
      003345 FF               [12] 8411 	mov	r7,a
      003346 C4               [12] 8412 	swap	a
      003347 03               [12] 8413 	rr	a
      003348 CE               [12] 8414 	xch	a,r6
      003349 C4               [12] 8415 	swap	a
      00334A 03               [12] 8416 	rr	a
      00334B 54 07            [12] 8417 	anl	a,#0x07
      00334D 6E               [12] 8418 	xrl	a,r6
      00334E CE               [12] 8419 	xch	a,r6
      00334F 54 07            [12] 8420 	anl	a,#0x07
      003351 CE               [12] 8421 	xch	a,r6
      003352 6E               [12] 8422 	xrl	a,r6
      003353 CE               [12] 8423 	xch	a,r6
      003354 30 E2 02         [24] 8424 	jnb	acc.2,00314$
      003357 44 F8            [12] 8425 	orl	a,#0xf8
      003359                       8426 00314$:
      003359 FF               [12] 8427 	mov	r7,a
      00335A 90 02 F7         [24] 8428 	mov	dptr,#(_axradio_cb_receive + 0x0012)
      00335D EE               [12] 8429 	mov	a,r6
      00335E F0               [24] 8430 	movx	@dptr,a
      00335F EF               [12] 8431 	mov	a,r7
      003360 A3               [24] 8432 	inc	dptr
      003361 F0               [24] 8433 	movx	@dptr,a
                                   8434 ;	..\src\COMMON\easyax5043.c:1434: axradio_sync_seqnr = 1;
      003362 90 00 C2         [24] 8435 	mov	dptr,#_axradio_ack_seqnr
      003365 74 01            [12] 8436 	mov	a,#0x01
      003367 F0               [24] 8437 	movx	@dptr,a
                                   8438 ;	..\src\COMMON\easyax5043.c:1436: };
      003368                       8439 00152$:
                                   8440 ;	..\src\COMMON\easyax5043.c:1437: axradio_sync_slave_nextperiod();
      003368 12 26 27         [24] 8441 	lcall	_axradio_sync_slave_nextperiod
                                   8442 ;	..\src\COMMON\easyax5043.c:1438: if (axradio_mode != AXRADIO_MODE_SYNC_ACK_SLAVE) {
      00336B 74 33            [12] 8443 	mov	a,#0x33
      00336D B5 0B 02         [24] 8444 	cjne	a,_axradio_mode,00315$
      003370 80 3D            [24] 8445 	sjmp	00154$
      003372                       8446 00315$:
                                   8447 ;	..\src\COMMON\easyax5043.c:1439: axradio_syncstate = syncstate_slave_rxidle;
      003372 90 00 B7         [24] 8448 	mov	dptr,#_axradio_syncstate
      003375 74 08            [12] 8449 	mov	a,#0x08
      003377 F0               [24] 8450 	movx	@dptr,a
                                   8451 ;	..\src\COMMON\easyax5043.c:1440: wtimer_remove(&axradio_timer);
      003378 90 03 3C         [24] 8452 	mov	dptr,#_axradio_timer
      00337B 12 71 84         [24] 8453 	lcall	_wtimer_remove
                                   8454 ;	..\src\COMMON\easyax5043.c:1441: axradio_sync_settimeradv(axradio_sync_slave_rxadvance[axradio_sync_seqnr]);
      00337E 90 00 C2         [24] 8455 	mov	dptr,#_axradio_ack_seqnr
      003381 E0               [24] 8456 	movx	a,@dptr
      003382 75 F0 04         [24] 8457 	mov	b,#0x04
      003385 A4               [48] 8458 	mul	ab
      003386 24 0E            [12] 8459 	add	a,#_axradio_sync_slave_rxadvance
      003388 F5 82            [12] 8460 	mov	dpl,a
      00338A 74 7B            [12] 8461 	mov	a,#(_axradio_sync_slave_rxadvance >> 8)
      00338C 35 F0            [12] 8462 	addc	a,b
      00338E F5 83            [12] 8463 	mov	dph,a
      003390 E4               [12] 8464 	clr	a
      003391 93               [24] 8465 	movc	a,@a+dptr
      003392 FC               [12] 8466 	mov	r4,a
      003393 A3               [24] 8467 	inc	dptr
      003394 E4               [12] 8468 	clr	a
      003395 93               [24] 8469 	movc	a,@a+dptr
      003396 FD               [12] 8470 	mov	r5,a
      003397 A3               [24] 8471 	inc	dptr
      003398 E4               [12] 8472 	clr	a
      003399 93               [24] 8473 	movc	a,@a+dptr
      00339A FE               [12] 8474 	mov	r6,a
      00339B A3               [24] 8475 	inc	dptr
      00339C E4               [12] 8476 	clr	a
      00339D 93               [24] 8477 	movc	a,@a+dptr
      00339E 8C 82            [24] 8478 	mov	dpl,r4
      0033A0 8D 83            [24] 8479 	mov	dph,r5
      0033A2 8E F0            [24] 8480 	mov	b,r6
      0033A4 12 24 C1         [24] 8481 	lcall	_axradio_sync_settimeradv
                                   8482 ;	..\src\COMMON\easyax5043.c:1442: wtimer0_addabsolute(&axradio_timer);
      0033A7 90 03 3C         [24] 8483 	mov	dptr,#_axradio_timer
      0033AA 12 67 3B         [24] 8484 	lcall	_wtimer0_addabsolute
      0033AD 80 06            [24] 8485 	sjmp	00157$
      0033AF                       8486 00154$:
                                   8487 ;	..\src\COMMON\easyax5043.c:1444: axradio_syncstate = syncstate_slave_rxack;
      0033AF 90 00 B7         [24] 8488 	mov	dptr,#_axradio_syncstate
      0033B2 74 0C            [12] 8489 	mov	a,#0x0c
      0033B4 F0               [24] 8490 	movx	@dptr,a
      0033B5                       8491 00157$:
                                   8492 ;	..\src\COMMON\easyax5043.c:1447: axradio_statuschange((struct axradio_status __xdata *)&axradio_cb_receive.st);
      0033B5 90 02 E9         [24] 8493 	mov	dptr,#(_axradio_cb_receive + 0x0004)
      0033B8 12 0C 08         [24] 8494 	lcall	_axradio_statuschange
                                   8495 ;	..\src\COMMON\easyax5043.c:1448: endcb:
      0033BB                       8496 00159$:
                                   8497 ;	..\src\COMMON\easyax5043.c:1449: if (axradio_mode == AXRADIO_MODE_WOR_RECEIVE) {
      0033BB 74 21            [12] 8498 	mov	a,#0x21
      0033BD B5 0B 03         [24] 8499 	cjne	a,_axradio_mode,00174$
                                   8500 ;	..\src\COMMON\easyax5043.c:1450: ax5043_receiver_on_wor();
      0033C0 02 21 DF         [24] 8501 	ljmp	_ax5043_receiver_on_wor
      0033C3                       8502 00174$:
                                   8503 ;	..\src\COMMON\easyax5043.c:1451: } else if (axradio_mode == AXRADIO_MODE_ACK_RECEIVE ||
      0033C3 74 22            [12] 8504 	mov	a,#0x22
      0033C5 B5 0B 02         [24] 8505 	cjne	a,_axradio_mode,00318$
      0033C8 80 05            [24] 8506 	sjmp	00169$
      0033CA                       8507 00318$:
                                   8508 ;	..\src\COMMON\easyax5043.c:1452: axradio_mode == AXRADIO_MODE_WOR_ACK_RECEIVE) {
      0033CA 74 23            [12] 8509 	mov	a,#0x23
      0033CC B5 0B 20         [24] 8510 	cjne	a,_axradio_mode,00170$
      0033CF                       8511 00169$:
                                   8512 ;	..\src\COMMON\easyax5043.c:1455: uint8_t __autodata iesave = IE & 0x80;
      0033CF 74 80            [12] 8513 	mov	a,#0x80
      0033D1 55 A8            [12] 8514 	anl	a,_IE
      0033D3 FF               [12] 8515 	mov	r7,a
                                   8516 ;	..\src\COMMON\easyax5043.c:1456: EA = 0;
      0033D4 C2 AF            [12] 8517 	clr	_EA
                                   8518 ;	..\src\COMMON\easyax5043.c:1457: trxst = axradio_trxstate;
      0033D6 AE 0C            [24] 8519 	mov	r6,_axradio_trxstate
                                   8520 ;	..\src\COMMON\easyax5043.c:1458: axradio_cb_receive.st.error = AXRADIO_ERR_PACKETDONE;
      0033D8 90 02 EA         [24] 8521 	mov	dptr,#(_axradio_cb_receive + 0x0005)
      0033DB 74 F0            [12] 8522 	mov	a,#0xf0
      0033DD F0               [24] 8523 	movx	@dptr,a
                                   8524 ;	..\src\COMMON\easyax5043.c:1459: IE |= iesave;
      0033DE EF               [12] 8525 	mov	a,r7
      0033DF 42 A8            [12] 8526 	orl	_IE,a
                                   8527 ;	..\src\COMMON\easyax5043.c:1461: if (trxst == trxstate_off) {
      0033E1 EE               [12] 8528 	mov	a,r6
      0033E2 70 1E            [24] 8529 	jnz	00176$
                                   8530 ;	..\src\COMMON\easyax5043.c:1462: if (axradio_mode == AXRADIO_MODE_WOR_ACK_RECEIVE)
      0033E4 74 23            [12] 8531 	mov	a,#0x23
      0033E6 B5 0B 03         [24] 8532 	cjne	a,_axradio_mode,00161$
                                   8533 ;	..\src\COMMON\easyax5043.c:1463: ax5043_receiver_on_wor();
      0033E9 02 21 DF         [24] 8534 	ljmp	_ax5043_receiver_on_wor
      0033EC                       8535 00161$:
                                   8536 ;	..\src\COMMON\easyax5043.c:1465: ax5043_receiver_on_continuous();
      0033EC 02 21 77         [24] 8537 	ljmp	_ax5043_receiver_on_continuous
      0033EF                       8538 00170$:
                                   8539 ;	..\src\COMMON\easyax5043.c:1468: switch (axradio_trxstate) {
      0033EF AF 0C            [24] 8540 	mov	r7,_axradio_trxstate
      0033F1 BF 01 02         [24] 8541 	cjne	r7,#0x01,00324$
      0033F4 80 03            [24] 8542 	sjmp	00166$
      0033F6                       8543 00324$:
      0033F6 BF 02 09         [24] 8544 	cjne	r7,#0x02,00176$
                                   8545 ;	..\src\COMMON\easyax5043.c:1470: case trxstate_rxwor:
      0033F9                       8546 00166$:
                                   8547 ;	..\src\COMMON\easyax5043.c:1471: AX5043_IRQMASK0 |= 0x01; // re-enable FIFO not empty irq
      0033F9 90 40 07         [24] 8548 	mov	dptr,#_AX5043_IRQMASK0
      0033FC E0               [24] 8549 	movx	a,@dptr
      0033FD FF               [12] 8550 	mov	r7,a
      0033FE 74 01            [12] 8551 	mov	a,#0x01
      003400 4F               [12] 8552 	orl	a,r7
      003401 F0               [24] 8553 	movx	@dptr,a
                                   8554 ;	..\src\COMMON\easyax5043.c:1476: }
      003402                       8555 00176$:
      003402 22               [24] 8556 	ret
                                   8557 ;------------------------------------------------------------
                                   8558 ;Allocation info for local variables in function 'axradio_killallcb'
                                   8559 ;------------------------------------------------------------
                                   8560 ;	..\src\COMMON\easyax5043.c:1480: static void axradio_killallcb(void)
                                   8561 ;	-----------------------------------------
                                   8562 ;	 function axradio_killallcb
                                   8563 ;	-----------------------------------------
      003403                       8564 _axradio_killallcb:
                                   8565 ;	..\src\COMMON\easyax5043.c:1482: wtimer_remove_callback(&axradio_cb_receive.cb);
      003403 90 02 E5         [24] 8566 	mov	dptr,#_axradio_cb_receive
      003406 12 74 C0         [24] 8567 	lcall	_wtimer_remove_callback
                                   8568 ;	..\src\COMMON\easyax5043.c:1483: wtimer_remove_callback(&axradio_cb_receivesfd.cb);
      003409 90 03 07         [24] 8569 	mov	dptr,#_axradio_cb_receivesfd
      00340C 12 74 C0         [24] 8570 	lcall	_wtimer_remove_callback
                                   8571 ;	..\src\COMMON\easyax5043.c:1484: wtimer_remove_callback(&axradio_cb_channelstate.cb);
      00340F 90 03 11         [24] 8572 	mov	dptr,#_axradio_cb_channelstate
      003412 12 74 C0         [24] 8573 	lcall	_wtimer_remove_callback
                                   8574 ;	..\src\COMMON\easyax5043.c:1485: wtimer_remove_callback(&axradio_cb_transmitstart.cb);
      003415 90 03 1E         [24] 8575 	mov	dptr,#_axradio_cb_transmitstart
      003418 12 74 C0         [24] 8576 	lcall	_wtimer_remove_callback
                                   8577 ;	..\src\COMMON\easyax5043.c:1486: wtimer_remove_callback(&axradio_cb_transmitend.cb);
      00341B 90 03 28         [24] 8578 	mov	dptr,#_axradio_cb_transmitend
      00341E 12 74 C0         [24] 8579 	lcall	_wtimer_remove_callback
                                   8580 ;	..\src\COMMON\easyax5043.c:1487: wtimer_remove_callback(&axradio_cb_transmitdata.cb);
      003421 90 03 32         [24] 8581 	mov	dptr,#_axradio_cb_transmitdata
      003424 12 74 C0         [24] 8582 	lcall	_wtimer_remove_callback
                                   8583 ;	..\src\COMMON\easyax5043.c:1488: wtimer_remove(&axradio_timer);
      003427 90 03 3C         [24] 8584 	mov	dptr,#_axradio_timer
      00342A 02 71 84         [24] 8585 	ljmp	_wtimer_remove
                                   8586 ;------------------------------------------------------------
                                   8587 ;Allocation info for local variables in function 'axradio_tunevoltage'
                                   8588 ;------------------------------------------------------------
                                   8589 ;x                         Allocated with name '_axradio_tunevoltage_x_3_374'
                                   8590 ;r                         Allocated to registers r6 r7 
                                   8591 ;cnt                       Allocated to registers r5 
                                   8592 ;------------------------------------------------------------
                                   8593 ;	..\src\COMMON\easyax5043.c:1515: static int16_t axradio_tunevoltage(void)
                                   8594 ;	-----------------------------------------
                                   8595 ;	 function axradio_tunevoltage
                                   8596 ;	-----------------------------------------
      00342D                       8597 _axradio_tunevoltage:
                                   8598 ;	..\src\COMMON\easyax5043.c:1517: int16_t __autodata r = 0;
      00342D 7E 00            [12] 8599 	mov	r6,#0x00
      00342F 7F 00            [12] 8600 	mov	r7,#0x00
                                   8601 ;	..\src\COMMON\easyax5043.c:1519: do {
      003431 7D 40            [12] 8602 	mov	r5,#0x40
      003433                       8603 00103$:
                                   8604 ;	..\src\COMMON\easyax5043.c:1520: AX5043_GPADCCTRL = 0x84;
      003433 90 43 00         [24] 8605 	mov	dptr,#_AX5043_GPADCCTRL
      003436 74 84            [12] 8606 	mov	a,#0x84
      003438 F0               [24] 8607 	movx	@dptr,a
                                   8608 ;	..\src\COMMON\easyax5043.c:1521: do {} while (AX5043_GPADCCTRL & 0x80);
      003439                       8609 00101$:
      003439 90 43 00         [24] 8610 	mov	dptr,#_AX5043_GPADCCTRL
      00343C E0               [24] 8611 	movx	a,@dptr
      00343D FC               [12] 8612 	mov	r4,a
      00343E 20 E7 F8         [24] 8613 	jb	acc.7,00101$
                                   8614 ;	..\src\COMMON\easyax5043.c:1522: } while (--cnt);
      003441 DD F0            [24] 8615 	djnz	r5,00103$
                                   8616 ;	..\src\COMMON\easyax5043.c:1524: do {
      003443 7D 20            [12] 8617 	mov	r5,#0x20
      003445                       8618 00108$:
                                   8619 ;	..\src\COMMON\easyax5043.c:1525: AX5043_GPADCCTRL = 0x84;
      003445 90 43 00         [24] 8620 	mov	dptr,#_AX5043_GPADCCTRL
      003448 74 84            [12] 8621 	mov	a,#0x84
      00344A F0               [24] 8622 	movx	@dptr,a
                                   8623 ;	..\src\COMMON\easyax5043.c:1526: do {} while (AX5043_GPADCCTRL & 0x80);
      00344B                       8624 00106$:
      00344B 90 43 00         [24] 8625 	mov	dptr,#_AX5043_GPADCCTRL
      00344E E0               [24] 8626 	movx	a,@dptr
      00344F FC               [12] 8627 	mov	r4,a
      003450 20 E7 F8         [24] 8628 	jb	acc.7,00106$
                                   8629 ;	..\src\COMMON\easyax5043.c:1528: int16_t x = AX5043_GPADC13VALUE1 & 0x03;
      003453 90 43 08         [24] 8630 	mov	dptr,#_AX5043_GPADC13VALUE1
      003456 E0               [24] 8631 	movx	a,@dptr
      003457 FC               [12] 8632 	mov	r4,a
      003458 53 04 03         [24] 8633 	anl	ar4,#0x03
                                   8634 ;	..\src\COMMON\easyax5043.c:1529: x <<= 8;
      00345B 8C 03            [24] 8635 	mov	ar3,r4
      00345D 7C 00            [12] 8636 	mov	r4,#0x00
                                   8637 ;	..\src\COMMON\easyax5043.c:1530: x |= AX5043_GPADC13VALUE0;
      00345F 90 43 09         [24] 8638 	mov	dptr,#_AX5043_GPADC13VALUE0
      003462 E0               [24] 8639 	movx	a,@dptr
      003463 F9               [12] 8640 	mov	r1,a
      003464 7A 00            [12] 8641 	mov	r2,#0x00
      003466 E9               [12] 8642 	mov	a,r1
      003467 42 04            [12] 8643 	orl	ar4,a
      003469 EA               [12] 8644 	mov	a,r2
      00346A 42 03            [12] 8645 	orl	ar3,a
                                   8646 ;	..\src\COMMON\easyax5043.c:1531: r += x;
      00346C EC               [12] 8647 	mov	a,r4
      00346D 2E               [12] 8648 	add	a,r6
      00346E FE               [12] 8649 	mov	r6,a
      00346F EB               [12] 8650 	mov	a,r3
      003470 3F               [12] 8651 	addc	a,r7
      003471 FF               [12] 8652 	mov	r7,a
                                   8653 ;	..\src\COMMON\easyax5043.c:1533: } while (--cnt);
      003472 DD D1            [24] 8654 	djnz	r5,00108$
                                   8655 ;	..\src\COMMON\easyax5043.c:1534: return r;
      003474 8E 82            [24] 8656 	mov	dpl,r6
      003476 8F 83            [24] 8657 	mov	dph,r7
      003478 22               [24] 8658 	ret
                                   8659 ;------------------------------------------------------------
                                   8660 ;Allocation info for local variables in function 'axradio_adjustvcoi'
                                   8661 ;------------------------------------------------------------
                                   8662 ;rng                       Allocated to registers r7 
                                   8663 ;offs                      Allocated to registers r3 
                                   8664 ;bestrng                   Allocated to registers r4 
                                   8665 ;bestval                   Allocated to registers r5 r6 
                                   8666 ;val                       Allocated to stack - _bp +1
                                   8667 ;------------------------------------------------------------
                                   8668 ;	..\src\COMMON\easyax5043.c:1539: static __reentrantb uint8_t axradio_adjustvcoi(uint8_t rng) __reentrant
                                   8669 ;	-----------------------------------------
                                   8670 ;	 function axradio_adjustvcoi
                                   8671 ;	-----------------------------------------
      003479                       8672 _axradio_adjustvcoi:
      003479 C0 1F            [24] 8673 	push	_bp
      00347B 85 81 1F         [24] 8674 	mov	_bp,sp
      00347E 05 81            [12] 8675 	inc	sp
      003480 05 81            [12] 8676 	inc	sp
      003482 AF 82            [24] 8677 	mov	r7,dpl
                                   8678 ;	..\src\COMMON\easyax5043.c:1543: uint16_t bestval = ~0;
      003484 7D FF            [12] 8679 	mov	r5,#0xff
      003486 7E FF            [12] 8680 	mov	r6,#0xff
                                   8681 ;	..\src\COMMON\easyax5043.c:1544: rng &= 0x7F;
      003488 53 07 7F         [24] 8682 	anl	ar7,#0x7f
                                   8683 ;	..\src\COMMON\easyax5043.c:1545: bestrng = rng;
      00348B 8F 04            [24] 8684 	mov	ar4,r7
                                   8685 ;	..\src\COMMON\easyax5043.c:1546: for (offs = 0; offs != 16; ++offs) {
      00348D 7B 00            [12] 8686 	mov	r3,#0x00
      00348F                       8687 00115$:
                                   8688 ;	..\src\COMMON\easyax5043.c:1548: if (!((uint8_t)(rng + offs) & 0xC0)) {
      00348F EB               [12] 8689 	mov	a,r3
      003490 2F               [12] 8690 	add	a,r7
      003491 54 C0            [12] 8691 	anl	a,#0xc0
      003493 60 02            [24] 8692 	jz	00144$
      003495 80 42            [24] 8693 	sjmp	00104$
      003497                       8694 00144$:
                                   8695 ;	..\src\COMMON\easyax5043.c:1549: AX5043_PLLVCOI = 0x80 | (rng + offs);
      003497 C0 04            [24] 8696 	push	ar4
      003499 EB               [12] 8697 	mov	a,r3
      00349A 2F               [12] 8698 	add	a,r7
      00349B 90 41 80         [24] 8699 	mov	dptr,#_AX5043_PLLVCOI
      00349E 44 80            [12] 8700 	orl	a,#0x80
      0034A0 F0               [24] 8701 	movx	@dptr,a
                                   8702 ;	..\src\COMMON\easyax5043.c:1550: val = axradio_tunevoltage();
      0034A1 C0 07            [24] 8703 	push	ar7
      0034A3 C0 06            [24] 8704 	push	ar6
      0034A5 C0 05            [24] 8705 	push	ar5
      0034A7 C0 03            [24] 8706 	push	ar3
      0034A9 12 34 2D         [24] 8707 	lcall	_axradio_tunevoltage
      0034AC AA 82            [24] 8708 	mov	r2,dpl
      0034AE AC 83            [24] 8709 	mov	r4,dph
      0034B0 D0 03            [24] 8710 	pop	ar3
      0034B2 D0 05            [24] 8711 	pop	ar5
      0034B4 D0 06            [24] 8712 	pop	ar6
      0034B6 D0 07            [24] 8713 	pop	ar7
      0034B8 A8 1F            [24] 8714 	mov	r0,_bp
      0034BA 08               [12] 8715 	inc	r0
      0034BB A6 02            [24] 8716 	mov	@r0,ar2
      0034BD 08               [12] 8717 	inc	r0
      0034BE A6 04            [24] 8718 	mov	@r0,ar4
                                   8719 ;	..\src\COMMON\easyax5043.c:1551: if (val < bestval) {
      0034C0 A8 1F            [24] 8720 	mov	r0,_bp
      0034C2 08               [12] 8721 	inc	r0
      0034C3 C3               [12] 8722 	clr	c
      0034C4 E6               [12] 8723 	mov	a,@r0
      0034C5 9D               [12] 8724 	subb	a,r5
      0034C6 08               [12] 8725 	inc	r0
      0034C7 E6               [12] 8726 	mov	a,@r0
      0034C8 9E               [12] 8727 	subb	a,r6
      0034C9 D0 04            [24] 8728 	pop	ar4
      0034CB 50 0C            [24] 8729 	jnc	00104$
                                   8730 ;	..\src\COMMON\easyax5043.c:1552: bestval = val;
      0034CD A8 1F            [24] 8731 	mov	r0,_bp
      0034CF 08               [12] 8732 	inc	r0
      0034D0 86 05            [24] 8733 	mov	ar5,@r0
      0034D2 08               [12] 8734 	inc	r0
      0034D3 86 06            [24] 8735 	mov	ar6,@r0
                                   8736 ;	..\src\COMMON\easyax5043.c:1553: bestrng = rng + offs;
      0034D5 EB               [12] 8737 	mov	a,r3
      0034D6 2F               [12] 8738 	add	a,r7
      0034D7 FA               [12] 8739 	mov	r2,a
      0034D8 FC               [12] 8740 	mov	r4,a
      0034D9                       8741 00104$:
                                   8742 ;	..\src\COMMON\easyax5043.c:1556: if (!offs)
      0034D9 EB               [12] 8743 	mov	a,r3
      0034DA 60 4D            [24] 8744 	jz	00111$
                                   8745 ;	..\src\COMMON\easyax5043.c:1558: if (!((uint8_t)(rng - offs) & 0xC0)) {
      0034DC EF               [12] 8746 	mov	a,r7
      0034DD C3               [12] 8747 	clr	c
      0034DE 9B               [12] 8748 	subb	a,r3
      0034DF 54 C0            [12] 8749 	anl	a,#0xc0
      0034E1 60 02            [24] 8750 	jz	00148$
      0034E3 80 44            [24] 8751 	sjmp	00111$
      0034E5                       8752 00148$:
                                   8753 ;	..\src\COMMON\easyax5043.c:1559: AX5043_PLLVCOI = 0x80 | (rng - offs);
      0034E5 C0 04            [24] 8754 	push	ar4
      0034E7 EF               [12] 8755 	mov	a,r7
      0034E8 C3               [12] 8756 	clr	c
      0034E9 9B               [12] 8757 	subb	a,r3
      0034EA 90 41 80         [24] 8758 	mov	dptr,#_AX5043_PLLVCOI
      0034ED 44 80            [12] 8759 	orl	a,#0x80
      0034EF F0               [24] 8760 	movx	@dptr,a
                                   8761 ;	..\src\COMMON\easyax5043.c:1560: val = axradio_tunevoltage();
      0034F0 C0 07            [24] 8762 	push	ar7
      0034F2 C0 06            [24] 8763 	push	ar6
      0034F4 C0 05            [24] 8764 	push	ar5
      0034F6 C0 03            [24] 8765 	push	ar3
      0034F8 12 34 2D         [24] 8766 	lcall	_axradio_tunevoltage
      0034FB AA 82            [24] 8767 	mov	r2,dpl
      0034FD AC 83            [24] 8768 	mov	r4,dph
      0034FF D0 03            [24] 8769 	pop	ar3
      003501 D0 05            [24] 8770 	pop	ar5
      003503 D0 06            [24] 8771 	pop	ar6
      003505 D0 07            [24] 8772 	pop	ar7
      003507 A8 1F            [24] 8773 	mov	r0,_bp
      003509 08               [12] 8774 	inc	r0
      00350A A6 02            [24] 8775 	mov	@r0,ar2
      00350C 08               [12] 8776 	inc	r0
      00350D A6 04            [24] 8777 	mov	@r0,ar4
                                   8778 ;	..\src\COMMON\easyax5043.c:1561: if (val < bestval) {
      00350F A8 1F            [24] 8779 	mov	r0,_bp
      003511 08               [12] 8780 	inc	r0
      003512 C3               [12] 8781 	clr	c
      003513 E6               [12] 8782 	mov	a,@r0
      003514 9D               [12] 8783 	subb	a,r5
      003515 08               [12] 8784 	inc	r0
      003516 E6               [12] 8785 	mov	a,@r0
      003517 9E               [12] 8786 	subb	a,r6
      003518 D0 04            [24] 8787 	pop	ar4
      00351A 50 0D            [24] 8788 	jnc	00111$
                                   8789 ;	..\src\COMMON\easyax5043.c:1562: bestval = val;
      00351C A8 1F            [24] 8790 	mov	r0,_bp
      00351E 08               [12] 8791 	inc	r0
      00351F 86 05            [24] 8792 	mov	ar5,@r0
      003521 08               [12] 8793 	inc	r0
      003522 86 06            [24] 8794 	mov	ar6,@r0
                                   8795 ;	..\src\COMMON\easyax5043.c:1563: bestrng = rng - offs;
      003524 EF               [12] 8796 	mov	a,r7
      003525 C3               [12] 8797 	clr	c
      003526 9B               [12] 8798 	subb	a,r3
      003527 FA               [12] 8799 	mov	r2,a
      003528 FC               [12] 8800 	mov	r4,a
      003529                       8801 00111$:
                                   8802 ;	..\src\COMMON\easyax5043.c:1546: for (offs = 0; offs != 16; ++offs) {
      003529 0B               [12] 8803 	inc	r3
      00352A BB 10 02         [24] 8804 	cjne	r3,#0x10,00150$
      00352D 80 03            [24] 8805 	sjmp	00151$
      00352F                       8806 00150$:
      00352F 02 34 8F         [24] 8807 	ljmp	00115$
      003532                       8808 00151$:
                                   8809 ;	..\src\COMMON\easyax5043.c:1568: if (bestval <= 0x0010)
      003532 C3               [12] 8810 	clr	c
      003533 74 10            [12] 8811 	mov	a,#0x10
      003535 9D               [12] 8812 	subb	a,r5
      003536 E4               [12] 8813 	clr	a
      003537 9E               [12] 8814 	subb	a,r6
      003538 40 07            [24] 8815 	jc	00114$
                                   8816 ;	..\src\COMMON\easyax5043.c:1569: return rng | 0x80;
      00353A 43 07 80         [24] 8817 	orl	ar7,#0x80
      00353D 8F 82            [24] 8818 	mov	dpl,r7
      00353F 80 05            [24] 8819 	sjmp	00116$
      003541                       8820 00114$:
                                   8821 ;	..\src\COMMON\easyax5043.c:1570: return bestrng | 0x80;
      003541 43 04 80         [24] 8822 	orl	ar4,#0x80
      003544 8C 82            [24] 8823 	mov	dpl,r4
      003546                       8824 00116$:
      003546 85 1F 81         [24] 8825 	mov	sp,_bp
      003549 D0 1F            [24] 8826 	pop	_bp
      00354B 22               [24] 8827 	ret
                                   8828 ;------------------------------------------------------------
                                   8829 ;Allocation info for local variables in function 'axradio_calvcoi'
                                   8830 ;------------------------------------------------------------
                                   8831 ;i                         Allocated to registers r2 
                                   8832 ;r                         Allocated to registers r7 
                                   8833 ;vmin                      Allocated to registers r5 r6 
                                   8834 ;vmax                      Allocated to stack - _bp +1
                                   8835 ;curtune                   Allocated to registers r3 r4 
                                   8836 ;------------------------------------------------------------
                                   8837 ;	..\src\COMMON\easyax5043.c:1573: static __reentrantb uint8_t axradio_calvcoi(void) __reentrant
                                   8838 ;	-----------------------------------------
                                   8839 ;	 function axradio_calvcoi
                                   8840 ;	-----------------------------------------
      00354C                       8841 _axradio_calvcoi:
      00354C C0 1F            [24] 8842 	push	_bp
      00354E 85 81 1F         [24] 8843 	mov	_bp,sp
      003551 05 81            [12] 8844 	inc	sp
      003553 05 81            [12] 8845 	inc	sp
                                   8846 ;	..\src\COMMON\easyax5043.c:1576: uint8_t r = 0;
      003555 7F 00            [12] 8847 	mov	r7,#0x00
                                   8848 ;	..\src\COMMON\easyax5043.c:1577: uint16_t vmin = 0xffff;
      003557 7D FF            [12] 8849 	mov	r5,#0xff
      003559 7E FF            [12] 8850 	mov	r6,#0xff
                                   8851 ;	..\src\COMMON\easyax5043.c:1578: uint16_t vmax = 0x0000;
      00355B A8 1F            [24] 8852 	mov	r0,_bp
      00355D 08               [12] 8853 	inc	r0
      00355E E4               [12] 8854 	clr	a
      00355F F6               [12] 8855 	mov	@r0,a
      003560 08               [12] 8856 	inc	r0
      003561 F6               [12] 8857 	mov	@r0,a
                                   8858 ;	..\src\COMMON\easyax5043.c:1579: for (i = 0x40; i != 0;) {
      003562 7A 40            [12] 8859 	mov	r2,#0x40
      003564                       8860 00113$:
                                   8861 ;	..\src\COMMON\easyax5043.c:1581: --i;
      003564 C0 07            [24] 8862 	push	ar7
      003566 1A               [12] 8863 	dec	r2
                                   8864 ;	..\src\COMMON\easyax5043.c:1582: AX5043_PLLVCOI = 0x80 | i;
      003567 90 41 80         [24] 8865 	mov	dptr,#_AX5043_PLLVCOI
      00356A 74 80            [12] 8866 	mov	a,#0x80
      00356C 4A               [12] 8867 	orl	a,r2
      00356D F0               [24] 8868 	movx	@dptr,a
                                   8869 ;	..\src\COMMON\easyax5043.c:1583: AX5043_PLLRANGINGA; // clear PLL lock loss
      00356E 90 40 33         [24] 8870 	mov	dptr,#_AX5043_PLLRANGINGA
      003571 E0               [24] 8871 	movx	a,@dptr
                                   8872 ;	..\src\COMMON\easyax5043.c:1584: curtune = axradio_tunevoltage();
      003572 C0 06            [24] 8873 	push	ar6
      003574 C0 05            [24] 8874 	push	ar5
      003576 C0 02            [24] 8875 	push	ar2
      003578 12 34 2D         [24] 8876 	lcall	_axradio_tunevoltage
      00357B AF 82            [24] 8877 	mov	r7,dpl
      00357D AC 83            [24] 8878 	mov	r4,dph
      00357F D0 02            [24] 8879 	pop	ar2
      003581 D0 05            [24] 8880 	pop	ar5
      003583 D0 06            [24] 8881 	pop	ar6
      003585 8F 03            [24] 8882 	mov	ar3,r7
                                   8883 ;	..\src\COMMON\easyax5043.c:1585: AX5043_PLLRANGINGA; // clear PLL lock loss
      003587 90 40 33         [24] 8884 	mov	dptr,#_AX5043_PLLRANGINGA
      00358A E0               [24] 8885 	movx	a,@dptr
                                   8886 ;	..\src\COMMON\easyax5043.c:1586: ((uint16_t __xdata *)axradio_rxbuffer)[i] = curtune;
      00358B EA               [12] 8887 	mov	a,r2
      00358C 75 F0 02         [24] 8888 	mov	b,#0x02
      00358F A4               [48] 8889 	mul	ab
      003590 24 E1            [12] 8890 	add	a,#_axradio_rxbuffer
      003592 F5 82            [12] 8891 	mov	dpl,a
      003594 74 01            [12] 8892 	mov	a,#(_axradio_rxbuffer >> 8)
      003596 35 F0            [12] 8893 	addc	a,b
      003598 F5 83            [12] 8894 	mov	dph,a
      00359A EB               [12] 8895 	mov	a,r3
      00359B F0               [24] 8896 	movx	@dptr,a
      00359C EC               [12] 8897 	mov	a,r4
      00359D A3               [24] 8898 	inc	dptr
      00359E F0               [24] 8899 	movx	@dptr,a
                                   8900 ;	..\src\COMMON\easyax5043.c:1587: if (curtune > vmax)
      00359F A8 1F            [24] 8901 	mov	r0,_bp
      0035A1 08               [12] 8902 	inc	r0
      0035A2 C3               [12] 8903 	clr	c
      0035A3 E6               [12] 8904 	mov	a,@r0
      0035A4 9B               [12] 8905 	subb	a,r3
      0035A5 08               [12] 8906 	inc	r0
      0035A6 E6               [12] 8907 	mov	a,@r0
      0035A7 9C               [12] 8908 	subb	a,r4
      0035A8 D0 07            [24] 8909 	pop	ar7
      0035AA 50 08            [24] 8910 	jnc	00102$
                                   8911 ;	..\src\COMMON\easyax5043.c:1588: vmax = curtune;
      0035AC A8 1F            [24] 8912 	mov	r0,_bp
      0035AE 08               [12] 8913 	inc	r0
      0035AF A6 03            [24] 8914 	mov	@r0,ar3
      0035B1 08               [12] 8915 	inc	r0
      0035B2 A6 04            [24] 8916 	mov	@r0,ar4
      0035B4                       8917 00102$:
                                   8918 ;	..\src\COMMON\easyax5043.c:1589: if (curtune < vmin) {
      0035B4 C3               [12] 8919 	clr	c
      0035B5 EB               [12] 8920 	mov	a,r3
      0035B6 9D               [12] 8921 	subb	a,r5
      0035B7 EC               [12] 8922 	mov	a,r4
      0035B8 9E               [12] 8923 	subb	a,r6
      0035B9 50 14            [24] 8924 	jnc	00114$
                                   8925 ;	..\src\COMMON\easyax5043.c:1590: vmin = curtune;
      0035BB 8B 05            [24] 8926 	mov	ar5,r3
      0035BD 8C 06            [24] 8927 	mov	ar6,r4
                                   8928 ;	..\src\COMMON\easyax5043.c:1592: if (!(0xC0 & (uint8_t)~AX5043_PLLRANGINGA))
      0035BF 90 40 33         [24] 8929 	mov	dptr,#_AX5043_PLLRANGINGA
      0035C2 E0               [24] 8930 	movx	a,@dptr
      0035C3 F4               [12] 8931 	cpl	a
      0035C4 FC               [12] 8932 	mov	r4,a
      0035C5 54 C0            [12] 8933 	anl	a,#0xc0
      0035C7 60 02            [24] 8934 	jz	00147$
      0035C9 80 04            [24] 8935 	sjmp	00114$
      0035CB                       8936 00147$:
                                   8937 ;	..\src\COMMON\easyax5043.c:1593: r = i | 0x80;
      0035CB 74 80            [12] 8938 	mov	a,#0x80
      0035CD 4A               [12] 8939 	orl	a,r2
      0035CE FF               [12] 8940 	mov	r7,a
      0035CF                       8941 00114$:
                                   8942 ;	..\src\COMMON\easyax5043.c:1579: for (i = 0x40; i != 0;) {
      0035CF EA               [12] 8943 	mov	a,r2
      0035D0 70 92            [24] 8944 	jnz	00113$
                                   8945 ;	..\src\COMMON\easyax5043.c:1596: if (!(r & 0x80) || vmax >= 0xFF00 || vmin < 0x0100 || vmax - vmin < 0x6000)
      0035D2 EF               [12] 8946 	mov	a,r7
      0035D3 30 E7 1F         [24] 8947 	jnb	acc.7,00108$
      0035D6 A8 1F            [24] 8948 	mov	r0,_bp
      0035D8 08               [12] 8949 	inc	r0
      0035D9 C3               [12] 8950 	clr	c
      0035DA 08               [12] 8951 	inc	r0
      0035DB E6               [12] 8952 	mov	a,@r0
      0035DC 94 FF            [12] 8953 	subb	a,#0xff
      0035DE 50 15            [24] 8954 	jnc	00108$
      0035E0 74 FF            [12] 8955 	mov	a,#0x100 - 0x01
      0035E2 2E               [12] 8956 	add	a,r6
      0035E3 50 10            [24] 8957 	jnc	00108$
      0035E5 A8 1F            [24] 8958 	mov	r0,_bp
      0035E7 08               [12] 8959 	inc	r0
      0035E8 E6               [12] 8960 	mov	a,@r0
      0035E9 C3               [12] 8961 	clr	c
      0035EA 9D               [12] 8962 	subb	a,r5
      0035EB FD               [12] 8963 	mov	r5,a
      0035EC 08               [12] 8964 	inc	r0
      0035ED E6               [12] 8965 	mov	a,@r0
      0035EE 9E               [12] 8966 	subb	a,r6
      0035EF FE               [12] 8967 	mov	r6,a
      0035F0 C3               [12] 8968 	clr	c
      0035F1 94 60            [12] 8969 	subb	a,#0x60
      0035F3 50 05            [24] 8970 	jnc	00109$
      0035F5                       8971 00108$:
                                   8972 ;	..\src\COMMON\easyax5043.c:1597: return 0;
      0035F5 75 82 00         [24] 8973 	mov	dpl,#0x00
      0035F8 80 02            [24] 8974 	sjmp	00115$
      0035FA                       8975 00109$:
                                   8976 ;	..\src\COMMON\easyax5043.c:1598: return r;
      0035FA 8F 82            [24] 8977 	mov	dpl,r7
      0035FC                       8978 00115$:
      0035FC 85 1F 81         [24] 8979 	mov	sp,_bp
      0035FF D0 1F            [24] 8980 	pop	_bp
      003601 22               [24] 8981 	ret
                                   8982 ;------------------------------------------------------------
                                   8983 ;Allocation info for local variables in function 'axradio_init'
                                   8984 ;------------------------------------------------------------
                                   8985 ;i                         Allocated to registers r7 
                                   8986 ;iesave                    Allocated to registers r6 
                                   8987 ;f                         Allocated to registers r3 r4 r5 r6 
                                   8988 ;r                         Allocated to registers r5 
                                   8989 ;vcoisave                  Allocated to registers r7 
                                   8990 ;f                         Allocated to registers r0 r1 r2 r3 
                                   8991 ;f                         Allocated to registers r4 r5 r6 r7 
                                   8992 ;x                         Allocated with name '_axradio_init_x_3_396'
                                   8993 ;j                         Allocated with name '_axradio_init_j_3_397'
                                   8994 ;x                         Allocated with name '_axradio_init_x_6_401'
                                   8995 ;chg                       Allocated with name '_axradio_init_chg_4_404'
                                   8996 ;------------------------------------------------------------
                                   8997 ;	..\src\COMMON\easyax5043.c:1605: uint8_t axradio_init(void)
                                   8998 ;	-----------------------------------------
                                   8999 ;	 function axradio_init
                                   9000 ;	-----------------------------------------
      003602                       9001 _axradio_init:
                                   9002 ;	..\src\COMMON\easyax5043.c:1608: axradio_mode = AXRADIO_MODE_UNINIT;
      003602 75 0B 00         [24] 9003 	mov	_axradio_mode,#0x00
                                   9004 ;	..\src\COMMON\easyax5043.c:1609: axradio_killallcb();
      003605 12 34 03         [24] 9005 	lcall	_axradio_killallcb
                                   9006 ;	..\src\COMMON\easyax5043.c:1610: axradio_cb_receive.cb.handler = axradio_receive_callback_fwd;
      003608 90 02 E7         [24] 9007 	mov	dptr,#(_axradio_cb_receive + 0x0002)
      00360B 74 DA            [12] 9008 	mov	a,#_axradio_receive_callback_fwd
      00360D F0               [24] 9009 	movx	@dptr,a
      00360E 74 2E            [12] 9010 	mov	a,#(_axradio_receive_callback_fwd >> 8)
      003610 A3               [24] 9011 	inc	dptr
      003611 F0               [24] 9012 	movx	@dptr,a
                                   9013 ;	..\src\COMMON\easyax5043.c:1611: axradio_cb_receive.st.status = AXRADIO_STAT_RECEIVE;
      003612 90 02 E9         [24] 9014 	mov	dptr,#(_axradio_cb_receive + 0x0004)
      003615 E4               [12] 9015 	clr	a
      003616 F0               [24] 9016 	movx	@dptr,a
                                   9017 ;	..\src\COMMON\easyax5043.c:1612: memset_xdata(axradio_cb_receive.st.rx.mac.remoteaddr, 0, sizeof(axradio_cb_receive.st.rx.mac.remoteaddr));
      003617 90 03 CD         [24] 9018 	mov	dptr,#_memset_PARM_2
      00361A F0               [24] 9019 	movx	@dptr,a
      00361B 90 03 CE         [24] 9020 	mov	dptr,#_memset_PARM_3
      00361E 74 04            [12] 9021 	mov	a,#0x04
      003620 F0               [24] 9022 	movx	@dptr,a
      003621 E4               [12] 9023 	clr	a
      003622 A3               [24] 9024 	inc	dptr
      003623 F0               [24] 9025 	movx	@dptr,a
      003624 90 02 F9         [24] 9026 	mov	dptr,#(_axradio_cb_receive + 0x0014)
      003627 75 F0 00         [24] 9027 	mov	b,#0x00
      00362A 12 62 A1         [24] 9028 	lcall	_memset
                                   9029 ;	..\src\COMMON\easyax5043.c:1613: memset_xdata(axradio_cb_receive.st.rx.mac.localaddr, 0, sizeof(axradio_cb_receive.st.rx.mac.localaddr));
      00362D 90 03 CD         [24] 9030 	mov	dptr,#_memset_PARM_2
      003630 E4               [12] 9031 	clr	a
      003631 F0               [24] 9032 	movx	@dptr,a
      003632 90 03 CE         [24] 9033 	mov	dptr,#_memset_PARM_3
      003635 74 04            [12] 9034 	mov	a,#0x04
      003637 F0               [24] 9035 	movx	@dptr,a
      003638 E4               [12] 9036 	clr	a
      003639 A3               [24] 9037 	inc	dptr
      00363A F0               [24] 9038 	movx	@dptr,a
      00363B 90 02 FD         [24] 9039 	mov	dptr,#(_axradio_cb_receive + 0x0018)
      00363E 75 F0 00         [24] 9040 	mov	b,#0x00
      003641 12 62 A1         [24] 9041 	lcall	_memset
                                   9042 ;	..\src\COMMON\easyax5043.c:1614: axradio_cb_receivesfd.cb.handler = axradio_callback_fwd;
      003644 90 03 09         [24] 9043 	mov	dptr,#(_axradio_cb_receivesfd + 0x0002)
      003647 74 C8            [12] 9044 	mov	a,#_axradio_callback_fwd
      003649 F0               [24] 9045 	movx	@dptr,a
      00364A 74 2E            [12] 9046 	mov	a,#(_axradio_callback_fwd >> 8)
      00364C A3               [24] 9047 	inc	dptr
      00364D F0               [24] 9048 	movx	@dptr,a
                                   9049 ;	..\src\COMMON\easyax5043.c:1615: axradio_cb_receivesfd.st.status = AXRADIO_STAT_RECEIVESFD;
      00364E 90 03 0B         [24] 9050 	mov	dptr,#(_axradio_cb_receivesfd + 0x0004)
      003651 74 01            [12] 9051 	mov	a,#0x01
      003653 F0               [24] 9052 	movx	@dptr,a
                                   9053 ;	..\src\COMMON\easyax5043.c:1616: axradio_cb_channelstate.cb.handler = axradio_callback_fwd;
      003654 90 03 13         [24] 9054 	mov	dptr,#(_axradio_cb_channelstate + 0x0002)
      003657 74 C8            [12] 9055 	mov	a,#_axradio_callback_fwd
      003659 F0               [24] 9056 	movx	@dptr,a
      00365A 74 2E            [12] 9057 	mov	a,#(_axradio_callback_fwd >> 8)
      00365C A3               [24] 9058 	inc	dptr
      00365D F0               [24] 9059 	movx	@dptr,a
                                   9060 ;	..\src\COMMON\easyax5043.c:1617: axradio_cb_channelstate.st.status = AXRADIO_STAT_CHANNELSTATE;
      00365E 90 03 15         [24] 9061 	mov	dptr,#(_axradio_cb_channelstate + 0x0004)
      003661 74 02            [12] 9062 	mov	a,#0x02
      003663 F0               [24] 9063 	movx	@dptr,a
                                   9064 ;	..\src\COMMON\easyax5043.c:1618: axradio_cb_transmitstart.cb.handler = axradio_callback_fwd;
      003664 90 03 20         [24] 9065 	mov	dptr,#(_axradio_cb_transmitstart + 0x0002)
      003667 74 C8            [12] 9066 	mov	a,#_axradio_callback_fwd
      003669 F0               [24] 9067 	movx	@dptr,a
      00366A 74 2E            [12] 9068 	mov	a,#(_axradio_callback_fwd >> 8)
      00366C A3               [24] 9069 	inc	dptr
      00366D F0               [24] 9070 	movx	@dptr,a
                                   9071 ;	..\src\COMMON\easyax5043.c:1619: axradio_cb_transmitstart.st.status = AXRADIO_STAT_TRANSMITSTART;
      00366E 90 03 22         [24] 9072 	mov	dptr,#(_axradio_cb_transmitstart + 0x0004)
      003671 74 03            [12] 9073 	mov	a,#0x03
      003673 F0               [24] 9074 	movx	@dptr,a
                                   9075 ;	..\src\COMMON\easyax5043.c:1620: axradio_cb_transmitend.cb.handler = axradio_callback_fwd;
      003674 90 03 2A         [24] 9076 	mov	dptr,#(_axradio_cb_transmitend + 0x0002)
      003677 74 C8            [12] 9077 	mov	a,#_axradio_callback_fwd
      003679 F0               [24] 9078 	movx	@dptr,a
      00367A 74 2E            [12] 9079 	mov	a,#(_axradio_callback_fwd >> 8)
      00367C A3               [24] 9080 	inc	dptr
      00367D F0               [24] 9081 	movx	@dptr,a
                                   9082 ;	..\src\COMMON\easyax5043.c:1621: axradio_cb_transmitend.st.status = AXRADIO_STAT_TRANSMITEND;
      00367E 90 03 2C         [24] 9083 	mov	dptr,#(_axradio_cb_transmitend + 0x0004)
      003681 74 04            [12] 9084 	mov	a,#0x04
      003683 F0               [24] 9085 	movx	@dptr,a
                                   9086 ;	..\src\COMMON\easyax5043.c:1622: axradio_cb_transmitdata.cb.handler = axradio_callback_fwd;
      003684 90 03 34         [24] 9087 	mov	dptr,#(_axradio_cb_transmitdata + 0x0002)
      003687 74 C8            [12] 9088 	mov	a,#_axradio_callback_fwd
      003689 F0               [24] 9089 	movx	@dptr,a
      00368A 74 2E            [12] 9090 	mov	a,#(_axradio_callback_fwd >> 8)
      00368C A3               [24] 9091 	inc	dptr
      00368D F0               [24] 9092 	movx	@dptr,a
                                   9093 ;	..\src\COMMON\easyax5043.c:1623: axradio_cb_transmitdata.st.status = AXRADIO_STAT_TRANSMITDATA;
      00368E 90 03 36         [24] 9094 	mov	dptr,#(_axradio_cb_transmitdata + 0x0004)
      003691 74 05            [12] 9095 	mov	a,#0x05
      003693 F0               [24] 9096 	movx	@dptr,a
                                   9097 ;	..\src\COMMON\easyax5043.c:1624: axradio_timer.handler = axradio_timer_callback;
      003694 90 03 3E         [24] 9098 	mov	dptr,#(_axradio_timer + 0x0002)
      003697 74 8F            [12] 9099 	mov	a,#_axradio_timer_callback
      003699 F0               [24] 9100 	movx	@dptr,a
      00369A 74 26            [12] 9101 	mov	a,#(_axradio_timer_callback >> 8)
      00369C A3               [24] 9102 	inc	dptr
      00369D F0               [24] 9103 	movx	@dptr,a
                                   9104 ;	..\src\COMMON\easyax5043.c:1625: axradio_curchannel = 0;
      00369E 90 00 BC         [24] 9105 	mov	dptr,#_axradio_curchannel
      0036A1 E4               [12] 9106 	clr	a
      0036A2 F0               [24] 9107 	movx	@dptr,a
                                   9108 ;	..\src\COMMON\easyax5043.c:1626: axradio_curfreqoffset = 0;
      0036A3 90 00 BD         [24] 9109 	mov	dptr,#_axradio_curfreqoffset
      0036A6 F0               [24] 9110 	movx	@dptr,a
      0036A7 A3               [24] 9111 	inc	dptr
      0036A8 F0               [24] 9112 	movx	@dptr,a
      0036A9 A3               [24] 9113 	inc	dptr
      0036AA F0               [24] 9114 	movx	@dptr,a
      0036AB A3               [24] 9115 	inc	dptr
      0036AC F0               [24] 9116 	movx	@dptr,a
                                   9117 ;	..\src\COMMON\easyax5043.c:1627: IE_4 = 0;
      0036AD C2 AC            [12] 9118 	clr	_IE_4
                                   9119 ;	..\src\COMMON\easyax5043.c:1628: axradio_trxstate = trxstate_off;
      0036AF 75 0C 00         [24] 9120 	mov	_axradio_trxstate,#0x00
                                   9121 ;	..\src\COMMON\easyax5043.c:1629: if (ax5043_reset())
      0036B2 12 5E 43         [24] 9122 	lcall	_ax5043_reset
      0036B5 E5 82            [12] 9123 	mov	a,dpl
      0036B7 60 04            [24] 9124 	jz	00102$
                                   9125 ;	..\src\COMMON\easyax5043.c:1630: return AXRADIO_ERR_NOCHIP;
      0036B9 75 82 05         [24] 9126 	mov	dpl,#0x05
      0036BC 22               [24] 9127 	ret
      0036BD                       9128 00102$:
                                   9129 ;	..\src\COMMON\easyax5043.c:1631: ax5043_init_registers();
      0036BD 12 24 44         [24] 9130 	lcall	_ax5043_init_registers
                                   9131 ;	..\src\COMMON\easyax5043.c:1632: ax5043_set_registers_tx();
      0036C0 12 06 56         [24] 9132 	lcall	_ax5043_set_registers_tx
                                   9133 ;	..\src\COMMON\easyax5043.c:1633: AX5043_PLLLOOP = 0x09; // default 100kHz loop BW for ranging
      0036C3 90 40 30         [24] 9134 	mov	dptr,#_AX5043_PLLLOOP
      0036C6 74 09            [12] 9135 	mov	a,#0x09
      0036C8 F0               [24] 9136 	movx	@dptr,a
                                   9137 ;	..\src\COMMON\easyax5043.c:1634: AX5043_PLLCPI = 0x08;
      0036C9 90 40 31         [24] 9138 	mov	dptr,#_AX5043_PLLCPI
      0036CC 14               [12] 9139 	dec	a
      0036CD F0               [24] 9140 	movx	@dptr,a
                                   9141 ;	..\src\COMMON\easyax5043.c:1636: IE_4 = 1;
      0036CE D2 AC            [12] 9142 	setb	_IE_4
                                   9143 ;	..\src\COMMON\easyax5043.c:1638: AX5043_PWRMODE = AX5043_PWRSTATE_XTAL_ON;
      0036D0 90 40 02         [24] 9144 	mov	dptr,#_AX5043_PWRMODE
      0036D3 74 05            [12] 9145 	mov	a,#0x05
      0036D5 F0               [24] 9146 	movx	@dptr,a
                                   9147 ;	..\src\COMMON\easyax5043.c:1639: AX5043_MODULATION = 0x08;
      0036D6 90 40 10         [24] 9148 	mov	dptr,#_AX5043_MODULATION
      0036D9 74 08            [12] 9149 	mov	a,#0x08
      0036DB F0               [24] 9150 	movx	@dptr,a
                                   9151 ;	..\src\COMMON\easyax5043.c:1640: AX5043_FSKDEV2 = 0x00;
      0036DC 90 41 61         [24] 9152 	mov	dptr,#_AX5043_FSKDEV2
      0036DF E4               [12] 9153 	clr	a
      0036E0 F0               [24] 9154 	movx	@dptr,a
                                   9155 ;	..\src\COMMON\easyax5043.c:1641: AX5043_FSKDEV1 = 0x00;
      0036E1 90 41 62         [24] 9156 	mov	dptr,#_AX5043_FSKDEV1
      0036E4 F0               [24] 9157 	movx	@dptr,a
                                   9158 ;	..\src\COMMON\easyax5043.c:1642: AX5043_FSKDEV0 = 0x00;
      0036E5 90 41 63         [24] 9159 	mov	dptr,#_AX5043_FSKDEV0
      0036E8 F0               [24] 9160 	movx	@dptr,a
                                   9161 ;	..\src\COMMON\easyax5043.c:1643: axradio_wait_for_xtal();
      0036E9 12 22 EE         [24] 9162 	lcall	_axradio_wait_for_xtal
                                   9163 ;	..\src\COMMON\easyax5043.c:1644: for (i = 0; i < axradio_phy_nrchannels; ++i) {
      0036EC 7F 00            [12] 9164 	mov	r7,#0x00
      0036EE                       9165 00149$:
      0036EE 90 7A BA         [24] 9166 	mov	dptr,#_axradio_phy_nrchannels
      0036F1 E4               [12] 9167 	clr	a
      0036F2 93               [24] 9168 	movc	a,@a+dptr
      0036F3 FE               [12] 9169 	mov	r6,a
      0036F4 C3               [12] 9170 	clr	c
      0036F5 EF               [12] 9171 	mov	a,r7
      0036F6 9E               [12] 9172 	subb	a,r6
      0036F7 40 03            [24] 9173 	jc	00267$
      0036F9 02 37 C1         [24] 9174 	ljmp	00113$
      0036FC                       9175 00267$:
                                   9176 ;	..\src\COMMON\easyax5043.c:1647: uint32_t __autodata f = axradio_phy_chanfreq[i];
      0036FC EF               [12] 9177 	mov	a,r7
      0036FD 75 F0 04         [24] 9178 	mov	b,#0x04
      003700 A4               [48] 9179 	mul	ab
      003701 24 BB            [12] 9180 	add	a,#_axradio_phy_chanfreq
      003703 F5 82            [12] 9181 	mov	dpl,a
      003705 74 7A            [12] 9182 	mov	a,#(_axradio_phy_chanfreq >> 8)
      003707 35 F0            [12] 9183 	addc	a,b
      003709 F5 83            [12] 9184 	mov	dph,a
      00370B E4               [12] 9185 	clr	a
      00370C 93               [24] 9186 	movc	a,@a+dptr
      00370D FB               [12] 9187 	mov	r3,a
      00370E A3               [24] 9188 	inc	dptr
      00370F E4               [12] 9189 	clr	a
      003710 93               [24] 9190 	movc	a,@a+dptr
      003711 FC               [12] 9191 	mov	r4,a
      003712 A3               [24] 9192 	inc	dptr
      003713 E4               [12] 9193 	clr	a
      003714 93               [24] 9194 	movc	a,@a+dptr
      003715 FD               [12] 9195 	mov	r5,a
      003716 A3               [24] 9196 	inc	dptr
      003717 E4               [12] 9197 	clr	a
      003718 93               [24] 9198 	movc	a,@a+dptr
      003719 FE               [12] 9199 	mov	r6,a
                                   9200 ;	..\src\COMMON\easyax5043.c:1648: AX5043_FREQA0 = f;
      00371A 90 40 37         [24] 9201 	mov	dptr,#_AX5043_FREQA0
      00371D EB               [12] 9202 	mov	a,r3
      00371E F0               [24] 9203 	movx	@dptr,a
                                   9204 ;	..\src\COMMON\easyax5043.c:1649: AX5043_FREQA1 = f >> 8;
      00371F 90 40 36         [24] 9205 	mov	dptr,#_AX5043_FREQA1
      003722 EC               [12] 9206 	mov	a,r4
      003723 F0               [24] 9207 	movx	@dptr,a
                                   9208 ;	..\src\COMMON\easyax5043.c:1650: AX5043_FREQA2 = f >> 16;
      003724 90 40 35         [24] 9209 	mov	dptr,#_AX5043_FREQA2
      003727 ED               [12] 9210 	mov	a,r5
      003728 F0               [24] 9211 	movx	@dptr,a
                                   9212 ;	..\src\COMMON\easyax5043.c:1651: AX5043_FREQA3 = f >> 24;
      003729 90 40 34         [24] 9213 	mov	dptr,#_AX5043_FREQA3
      00372C EE               [12] 9214 	mov	a,r6
      00372D F0               [24] 9215 	movx	@dptr,a
                                   9216 ;	..\src\COMMON\easyax5043.c:1653: iesave = IE & 0x80;
      00372E 74 80            [12] 9217 	mov	a,#0x80
      003730 55 A8            [12] 9218 	anl	a,_IE
      003732 FE               [12] 9219 	mov	r6,a
                                   9220 ;	..\src\COMMON\easyax5043.c:1654: EA = 0;
      003733 C2 AF            [12] 9221 	clr	_EA
                                   9222 ;	..\src\COMMON\easyax5043.c:1655: axradio_trxstate = trxstate_pll_ranging;
      003735 75 0C 05         [24] 9223 	mov	_axradio_trxstate,#0x05
                                   9224 ;	..\src\COMMON\easyax5043.c:1656: AX5043_IRQMASK1 = 0x10; // enable pll autoranging done interrupt
      003738 90 40 06         [24] 9225 	mov	dptr,#_AX5043_IRQMASK1
      00373B 74 10            [12] 9226 	mov	a,#0x10
      00373D F0               [24] 9227 	movx	@dptr,a
                                   9228 ;	..\src\COMMON\easyax5043.c:1659: if( !(axradio_phy_chanpllrnginit[0] & 0xF0) ) { // start values for ranging available
      00373E 90 7A BF         [24] 9229 	mov	dptr,#_axradio_phy_chanpllrnginit
      003741 E4               [12] 9230 	clr	a
      003742 93               [24] 9231 	movc	a,@a+dptr
      003743 FD               [12] 9232 	mov	r5,a
      003744 54 F0            [12] 9233 	anl	a,#0xf0
      003746 70 0B            [24] 9234 	jnz	00108$
                                   9235 ;	..\src\COMMON\easyax5043.c:1660: r = axradio_phy_chanpllrnginit[i] | 0x10;
      003748 EF               [12] 9236 	mov	a,r7
      003749 90 7A BF         [24] 9237 	mov	dptr,#_axradio_phy_chanpllrnginit
      00374C 93               [24] 9238 	movc	a,@a+dptr
      00374D FD               [12] 9239 	mov	r5,a
      00374E 43 05 10         [24] 9240 	orl	ar5,#0x10
      003751 80 25            [24] 9241 	sjmp	00109$
      003753                       9242 00108$:
                                   9243 ;	..\src\COMMON\easyax5043.c:1663: r = 0x18;
      003753 7D 18            [12] 9244 	mov	r5,#0x18
                                   9245 ;	..\src\COMMON\easyax5043.c:1664: if (i) {
      003755 EF               [12] 9246 	mov	a,r7
      003756 60 20            [24] 9247 	jz	00109$
                                   9248 ;	..\src\COMMON\easyax5043.c:1665: r = axradio_phy_chanpllrng[i - 1];
      003758 8F 03            [24] 9249 	mov	ar3,r7
      00375A 7C 00            [12] 9250 	mov	r4,#0x00
      00375C 1B               [12] 9251 	dec	r3
      00375D BB FF 01         [24] 9252 	cjne	r3,#0xff,00271$
      003760 1C               [12] 9253 	dec	r4
      003761                       9254 00271$:
      003761 EB               [12] 9255 	mov	a,r3
      003762 24 09            [12] 9256 	add	a,#_axradio_phy_chanpllrng
      003764 F5 82            [12] 9257 	mov	dpl,a
      003766 EC               [12] 9258 	mov	a,r4
      003767 34 00            [12] 9259 	addc	a,#(_axradio_phy_chanpllrng >> 8)
      003769 F5 83            [12] 9260 	mov	dph,a
      00376B E0               [24] 9261 	movx	a,@dptr
                                   9262 ;	..\src\COMMON\easyax5043.c:1666: if (r & 0x20)
      00376C FD               [12] 9263 	mov	r5,a
      00376D 30 E5 02         [24] 9264 	jnb	acc.5,00104$
                                   9265 ;	..\src\COMMON\easyax5043.c:1667: r = 0x08;
      003770 7D 08            [12] 9266 	mov	r5,#0x08
      003772                       9267 00104$:
                                   9268 ;	..\src\COMMON\easyax5043.c:1668: r &= 0x0F;
      003772 53 05 0F         [24] 9269 	anl	ar5,#0x0f
                                   9270 ;	..\src\COMMON\easyax5043.c:1669: r |= 0x10;
      003775 43 05 10         [24] 9271 	orl	ar5,#0x10
      003778                       9272 00109$:
                                   9273 ;	..\src\COMMON\easyax5043.c:1672: AX5043_PLLRANGINGA = r; // init ranging process starting from "range"
      003778 90 40 33         [24] 9274 	mov	dptr,#_AX5043_PLLRANGINGA
      00377B ED               [12] 9275 	mov	a,r5
      00377C F0               [24] 9276 	movx	@dptr,a
      00377D                       9277 00146$:
                                   9278 ;	..\src\COMMON\easyax5043.c:1675: EA = 0;
      00377D C2 AF            [12] 9279 	clr	_EA
                                   9280 ;	..\src\COMMON\easyax5043.c:1676: if (axradio_trxstate == trxstate_pll_ranging_done)
      00377F 74 06            [12] 9281 	mov	a,#0x06
      003781 B5 0C 02         [24] 9282 	cjne	a,_axradio_trxstate,00273$
      003784 80 1A            [24] 9283 	sjmp	00112$
      003786                       9284 00273$:
                                   9285 ;	..\src\COMMON\easyax5043.c:1678: wtimer_idle(WTFLAG_CANSTANDBY);
      003786 75 82 02         [24] 9286 	mov	dpl,#0x02
      003789 C0 07            [24] 9287 	push	ar7
      00378B C0 06            [24] 9288 	push	ar6
      00378D 12 65 13         [24] 9289 	lcall	_wtimer_idle
      003790 D0 06            [24] 9290 	pop	ar6
                                   9291 ;	..\src\COMMON\easyax5043.c:1679: IE |= iesave;
      003792 EE               [12] 9292 	mov	a,r6
      003793 42 A8            [12] 9293 	orl	_IE,a
                                   9294 ;	..\src\COMMON\easyax5043.c:1680: wtimer_runcallbacks();
      003795 C0 06            [24] 9295 	push	ar6
      003797 12 64 92         [24] 9296 	lcall	_wtimer_runcallbacks
      00379A D0 06            [24] 9297 	pop	ar6
      00379C D0 07            [24] 9298 	pop	ar7
      00379E 80 DD            [24] 9299 	sjmp	00146$
      0037A0                       9300 00112$:
                                   9301 ;	..\src\COMMON\easyax5043.c:1682: axradio_trxstate = trxstate_off;
      0037A0 75 0C 00         [24] 9302 	mov	_axradio_trxstate,#0x00
                                   9303 ;	..\src\COMMON\easyax5043.c:1683: AX5043_IRQMASK1 = 0x00;
      0037A3 90 40 06         [24] 9304 	mov	dptr,#_AX5043_IRQMASK1
      0037A6 E4               [12] 9305 	clr	a
      0037A7 F0               [24] 9306 	movx	@dptr,a
                                   9307 ;	..\src\COMMON\easyax5043.c:1684: axradio_phy_chanpllrng[i] = AX5043_PLLRANGINGA;
      0037A8 EF               [12] 9308 	mov	a,r7
      0037A9 24 09            [12] 9309 	add	a,#_axradio_phy_chanpllrng
      0037AB FC               [12] 9310 	mov	r4,a
      0037AC E4               [12] 9311 	clr	a
      0037AD 34 00            [12] 9312 	addc	a,#(_axradio_phy_chanpllrng >> 8)
      0037AF FD               [12] 9313 	mov	r5,a
      0037B0 90 40 33         [24] 9314 	mov	dptr,#_AX5043_PLLRANGINGA
      0037B3 E0               [24] 9315 	movx	a,@dptr
      0037B4 FB               [12] 9316 	mov	r3,a
      0037B5 8C 82            [24] 9317 	mov	dpl,r4
      0037B7 8D 83            [24] 9318 	mov	dph,r5
      0037B9 F0               [24] 9319 	movx	@dptr,a
                                   9320 ;	..\src\COMMON\easyax5043.c:1685: IE |= iesave;
      0037BA EE               [12] 9321 	mov	a,r6
      0037BB 42 A8            [12] 9322 	orl	_IE,a
                                   9323 ;	..\src\COMMON\easyax5043.c:1644: for (i = 0; i < axradio_phy_nrchannels; ++i) {
      0037BD 0F               [12] 9324 	inc	r7
      0037BE 02 36 EE         [24] 9325 	ljmp	00149$
      0037C1                       9326 00113$:
                                   9327 ;	..\src\COMMON\easyax5043.c:1688: if (axradio_phy_vcocalib) {
      0037C1 90 7A C1         [24] 9328 	mov	dptr,#_axradio_phy_vcocalib
      0037C4 E4               [12] 9329 	clr	a
      0037C5 93               [24] 9330 	movc	a,@a+dptr
      0037C6 70 03            [24] 9331 	jnz	00274$
      0037C8 02 3A B2         [24] 9332 	ljmp	00142$
      0037CB                       9333 00274$:
                                   9334 ;	..\src\COMMON\easyax5043.c:1689: ax5043_set_registers_tx();
      0037CB 12 06 56         [24] 9335 	lcall	_ax5043_set_registers_tx
                                   9336 ;	..\src\COMMON\easyax5043.c:1690: AX5043_MODULATION = 0x08;
      0037CE 90 40 10         [24] 9337 	mov	dptr,#_AX5043_MODULATION
      0037D1 74 08            [12] 9338 	mov	a,#0x08
      0037D3 F0               [24] 9339 	movx	@dptr,a
                                   9340 ;	..\src\COMMON\easyax5043.c:1691: AX5043_FSKDEV2 = 0x00;
      0037D4 90 41 61         [24] 9341 	mov	dptr,#_AX5043_FSKDEV2
      0037D7 E4               [12] 9342 	clr	a
      0037D8 F0               [24] 9343 	movx	@dptr,a
                                   9344 ;	..\src\COMMON\easyax5043.c:1692: AX5043_FSKDEV1 = 0x00;
      0037D9 90 41 62         [24] 9345 	mov	dptr,#_AX5043_FSKDEV1
      0037DC F0               [24] 9346 	movx	@dptr,a
                                   9347 ;	..\src\COMMON\easyax5043.c:1693: AX5043_FSKDEV0 = 0x00;
      0037DD 90 41 63         [24] 9348 	mov	dptr,#_AX5043_FSKDEV0
      0037E0 F0               [24] 9349 	movx	@dptr,a
                                   9350 ;	..\src\COMMON\easyax5043.c:1694: AX5043_PLLLOOP |= 0x04;
      0037E1 90 40 30         [24] 9351 	mov	dptr,#_AX5043_PLLLOOP
      0037E4 E0               [24] 9352 	movx	a,@dptr
      0037E5 FF               [12] 9353 	mov	r7,a
      0037E6 74 04            [12] 9354 	mov	a,#0x04
      0037E8 4F               [12] 9355 	orl	a,r7
      0037E9 F0               [24] 9356 	movx	@dptr,a
                                   9357 ;	..\src\COMMON\easyax5043.c:1696: uint8_t x = AX5043_0xF35;
      0037EA 90 4F 35         [24] 9358 	mov	dptr,#_AX5043_0xF35
      0037ED E0               [24] 9359 	movx	a,@dptr
      0037EE FF               [12] 9360 	mov	r7,a
                                   9361 ;	..\src\COMMON\easyax5043.c:1697: x |= 0x80;
      0037EF 43 07 80         [24] 9362 	orl	ar7,#0x80
      0037F2 90 03 61         [24] 9363 	mov	dptr,#_axradio_init_x_3_396
      0037F5 EF               [12] 9364 	mov	a,r7
      0037F6 F0               [24] 9365 	movx	@dptr,a
                                   9366 ;	..\src\COMMON\easyax5043.c:1698: if (2 & (uint8_t)~x)
      0037F7 EF               [12] 9367 	mov	a,r7
      0037F8 F4               [12] 9368 	cpl	a
      0037F9 FE               [12] 9369 	mov	r6,a
      0037FA 30 E1 06         [24] 9370 	jnb	acc.1,00115$
                                   9371 ;	..\src\COMMON\easyax5043.c:1699: ++x;
      0037FD 90 03 61         [24] 9372 	mov	dptr,#_axradio_init_x_3_396
      003800 EF               [12] 9373 	mov	a,r7
      003801 04               [12] 9374 	inc	a
      003802 F0               [24] 9375 	movx	@dptr,a
      003803                       9376 00115$:
                                   9377 ;	..\src\COMMON\easyax5043.c:1700: AX5043_0xF35 = x;
      003803 90 03 61         [24] 9378 	mov	dptr,#_axradio_init_x_3_396
      003806 E0               [24] 9379 	movx	a,@dptr
      003807 90 4F 35         [24] 9380 	mov	dptr,#_AX5043_0xF35
      00380A F0               [24] 9381 	movx	@dptr,a
                                   9382 ;	..\src\COMMON\easyax5043.c:1702: AX5043_PWRMODE = AX5043_PWRSTATE_SYNTH_TX;
      00380B 90 40 02         [24] 9383 	mov	dptr,#_AX5043_PWRMODE
      00380E 74 0C            [12] 9384 	mov	a,#0x0c
      003810 F0               [24] 9385 	movx	@dptr,a
                                   9386 ;	..\src\COMMON\easyax5043.c:1704: uint8_t __autodata vcoisave = AX5043_PLLVCOI;
      003811 90 41 80         [24] 9387 	mov	dptr,#_AX5043_PLLVCOI
      003814 E0               [24] 9388 	movx	a,@dptr
      003815 FF               [12] 9389 	mov	r7,a
                                   9390 ;	..\src\COMMON\easyax5043.c:1705: uint8_t j = 2;
      003816 90 03 62         [24] 9391 	mov	dptr,#_axradio_init_j_3_397
      003819 74 02            [12] 9392 	mov	a,#0x02
      00381B F0               [24] 9393 	movx	@dptr,a
                                   9394 ;	..\src\COMMON\easyax5043.c:1706: for (i = 0; i < axradio_phy_nrchannels; ++i) {
      00381C 7E 00            [12] 9395 	mov	r6,#0x00
      00381E                       9396 00152$:
      00381E 90 7A BA         [24] 9397 	mov	dptr,#_axradio_phy_nrchannels
      003821 E4               [12] 9398 	clr	a
      003822 93               [24] 9399 	movc	a,@a+dptr
      003823 FD               [12] 9400 	mov	r5,a
      003824 C3               [12] 9401 	clr	c
      003825 EE               [12] 9402 	mov	a,r6
      003826 9D               [12] 9403 	subb	a,r5
      003827 40 03            [24] 9404 	jc	00276$
      003829 02 39 36         [24] 9405 	ljmp	00127$
      00382C                       9406 00276$:
                                   9407 ;	..\src\COMMON\easyax5043.c:1707: axradio_phy_chanvcoi[i] = 0;
      00382C EE               [12] 9408 	mov	a,r6
      00382D 24 0A            [12] 9409 	add	a,#_axradio_phy_chanvcoi
      00382F F5 82            [12] 9410 	mov	dpl,a
      003831 E4               [12] 9411 	clr	a
      003832 34 00            [12] 9412 	addc	a,#(_axradio_phy_chanvcoi >> 8)
      003834 F5 83            [12] 9413 	mov	dph,a
      003836 E4               [12] 9414 	clr	a
      003837 F0               [24] 9415 	movx	@dptr,a
                                   9416 ;	..\src\COMMON\easyax5043.c:1708: if (axradio_phy_chanpllrng[i] & 0x20)
      003838 EE               [12] 9417 	mov	a,r6
      003839 24 09            [12] 9418 	add	a,#_axradio_phy_chanpllrng
      00383B FC               [12] 9419 	mov	r4,a
      00383C E4               [12] 9420 	clr	a
      00383D 34 00            [12] 9421 	addc	a,#(_axradio_phy_chanpllrng >> 8)
      00383F FD               [12] 9422 	mov	r5,a
      003840 8C 82            [24] 9423 	mov	dpl,r4
      003842 8D 83            [24] 9424 	mov	dph,r5
      003844 E0               [24] 9425 	movx	a,@dptr
      003845 FB               [12] 9426 	mov	r3,a
      003846 30 E5 03         [24] 9427 	jnb	acc.5,00277$
      003849 02 39 32         [24] 9428 	ljmp	00126$
      00384C                       9429 00277$:
                                   9430 ;	..\src\COMMON\easyax5043.c:1710: AX5043_PLLRANGINGA = axradio_phy_chanpllrng[i] & 0x0F;
      00384C 90 40 33         [24] 9431 	mov	dptr,#_AX5043_PLLRANGINGA
      00384F 74 0F            [12] 9432 	mov	a,#0x0f
      003851 5B               [12] 9433 	anl	a,r3
      003852 F0               [24] 9434 	movx	@dptr,a
                                   9435 ;	..\src\COMMON\easyax5043.c:1712: uint32_t __autodata f = axradio_phy_chanfreq[i];
      003853 EE               [12] 9436 	mov	a,r6
      003854 75 F0 04         [24] 9437 	mov	b,#0x04
      003857 A4               [48] 9438 	mul	ab
      003858 24 BB            [12] 9439 	add	a,#_axradio_phy_chanfreq
      00385A F5 82            [12] 9440 	mov	dpl,a
      00385C 74 7A            [12] 9441 	mov	a,#(_axradio_phy_chanfreq >> 8)
      00385E 35 F0            [12] 9442 	addc	a,b
      003860 F5 83            [12] 9443 	mov	dph,a
      003862 E4               [12] 9444 	clr	a
      003863 93               [24] 9445 	movc	a,@a+dptr
      003864 F8               [12] 9446 	mov	r0,a
      003865 A3               [24] 9447 	inc	dptr
      003866 E4               [12] 9448 	clr	a
      003867 93               [24] 9449 	movc	a,@a+dptr
      003868 F9               [12] 9450 	mov	r1,a
      003869 A3               [24] 9451 	inc	dptr
      00386A E4               [12] 9452 	clr	a
      00386B 93               [24] 9453 	movc	a,@a+dptr
      00386C FA               [12] 9454 	mov	r2,a
      00386D A3               [24] 9455 	inc	dptr
      00386E E4               [12] 9456 	clr	a
      00386F 93               [24] 9457 	movc	a,@a+dptr
      003870 FB               [12] 9458 	mov	r3,a
                                   9459 ;	..\src\COMMON\easyax5043.c:1713: AX5043_FREQA0 = f;
      003871 90 40 37         [24] 9460 	mov	dptr,#_AX5043_FREQA0
      003874 E8               [12] 9461 	mov	a,r0
      003875 F0               [24] 9462 	movx	@dptr,a
                                   9463 ;	..\src\COMMON\easyax5043.c:1714: AX5043_FREQA1 = f >> 8;
      003876 90 40 36         [24] 9464 	mov	dptr,#_AX5043_FREQA1
      003879 E9               [12] 9465 	mov	a,r1
      00387A F0               [24] 9466 	movx	@dptr,a
                                   9467 ;	..\src\COMMON\easyax5043.c:1715: AX5043_FREQA2 = f >> 16;
      00387B 90 40 35         [24] 9468 	mov	dptr,#_AX5043_FREQA2
      00387E EA               [12] 9469 	mov	a,r2
      00387F F0               [24] 9470 	movx	@dptr,a
                                   9471 ;	..\src\COMMON\easyax5043.c:1716: AX5043_FREQA3 = f >> 24;
      003880 90 40 34         [24] 9472 	mov	dptr,#_AX5043_FREQA3
      003883 EB               [12] 9473 	mov	a,r3
      003884 F0               [24] 9474 	movx	@dptr,a
                                   9475 ;	..\src\COMMON\easyax5043.c:1718: do {
      003885 90 03 62         [24] 9476 	mov	dptr,#_axradio_init_j_3_397
      003888 E0               [24] 9477 	movx	a,@dptr
      003889 FB               [12] 9478 	mov	r3,a
      00388A                       9479 00123$:
                                   9480 ;	..\src\COMMON\easyax5043.c:1719: if (axradio_phy_chanvcoiinit[0]) {
      00388A 90 7A C0         [24] 9481 	mov	dptr,#_axradio_phy_chanvcoiinit
      00388D E4               [12] 9482 	clr	a
      00388E 93               [24] 9483 	movc	a,@a+dptr
      00388F 60 65            [24] 9484 	jz	00121$
                                   9485 ;	..\src\COMMON\easyax5043.c:1720: uint8_t x = axradio_phy_chanvcoiinit[i];
      003891 EE               [12] 9486 	mov	a,r6
      003892 90 7A C0         [24] 9487 	mov	dptr,#_axradio_phy_chanvcoiinit
      003895 93               [24] 9488 	movc	a,@a+dptr
      003896 FA               [12] 9489 	mov	r2,a
      003897 90 03 63         [24] 9490 	mov	dptr,#_axradio_init_x_6_401
      00389A F0               [24] 9491 	movx	@dptr,a
                                   9492 ;	..\src\COMMON\easyax5043.c:1721: if (!(axradio_phy_chanpllrnginit[0] & 0xF0))
      00389B 90 7A BF         [24] 9493 	mov	dptr,#_axradio_phy_chanpllrnginit
      00389E E4               [12] 9494 	clr	a
      00389F 93               [24] 9495 	movc	a,@a+dptr
      0038A0 F9               [12] 9496 	mov	r1,a
      0038A1 54 F0            [12] 9497 	anl	a,#0xf0
      0038A3 70 1A            [24] 9498 	jnz	00119$
                                   9499 ;	..\src\COMMON\easyax5043.c:1722: x += (axradio_phy_chanpllrng[i] & 0x0F) - (axradio_phy_chanpllrnginit[i] & 0x0F);
      0038A5 8C 82            [24] 9500 	mov	dpl,r4
      0038A7 8D 83            [24] 9501 	mov	dph,r5
      0038A9 E0               [24] 9502 	movx	a,@dptr
      0038AA F9               [12] 9503 	mov	r1,a
      0038AB 53 01 0F         [24] 9504 	anl	ar1,#0x0f
      0038AE EE               [12] 9505 	mov	a,r6
      0038AF 90 7A BF         [24] 9506 	mov	dptr,#_axradio_phy_chanpllrnginit
      0038B2 93               [24] 9507 	movc	a,@a+dptr
      0038B3 F8               [12] 9508 	mov	r0,a
      0038B4 74 0F            [12] 9509 	mov	a,#0x0f
      0038B6 58               [12] 9510 	anl	a,r0
      0038B7 D3               [12] 9511 	setb	c
      0038B8 99               [12] 9512 	subb	a,r1
      0038B9 F4               [12] 9513 	cpl	a
      0038BA 90 03 63         [24] 9514 	mov	dptr,#_axradio_init_x_6_401
      0038BD 2A               [12] 9515 	add	a,r2
      0038BE F0               [24] 9516 	movx	@dptr,a
      0038BF                       9517 00119$:
                                   9518 ;	..\src\COMMON\easyax5043.c:1723: axradio_phy_chanvcoi[i] = axradio_adjustvcoi(x);
      0038BF EE               [12] 9519 	mov	a,r6
      0038C0 24 0A            [12] 9520 	add	a,#_axradio_phy_chanvcoi
      0038C2 F9               [12] 9521 	mov	r1,a
      0038C3 E4               [12] 9522 	clr	a
      0038C4 34 00            [12] 9523 	addc	a,#(_axradio_phy_chanvcoi >> 8)
      0038C6 FA               [12] 9524 	mov	r2,a
      0038C7 90 03 63         [24] 9525 	mov	dptr,#_axradio_init_x_6_401
      0038CA E0               [24] 9526 	movx	a,@dptr
      0038CB F5 82            [12] 9527 	mov	dpl,a
      0038CD C0 07            [24] 9528 	push	ar7
      0038CF C0 06            [24] 9529 	push	ar6
      0038D1 C0 05            [24] 9530 	push	ar5
      0038D3 C0 04            [24] 9531 	push	ar4
      0038D5 C0 03            [24] 9532 	push	ar3
      0038D7 C0 02            [24] 9533 	push	ar2
      0038D9 C0 01            [24] 9534 	push	ar1
      0038DB 12 34 79         [24] 9535 	lcall	_axradio_adjustvcoi
      0038DE A8 82            [24] 9536 	mov	r0,dpl
      0038E0 D0 01            [24] 9537 	pop	ar1
      0038E2 D0 02            [24] 9538 	pop	ar2
      0038E4 D0 03            [24] 9539 	pop	ar3
      0038E6 D0 04            [24] 9540 	pop	ar4
      0038E8 D0 05            [24] 9541 	pop	ar5
      0038EA D0 06            [24] 9542 	pop	ar6
      0038EC D0 07            [24] 9543 	pop	ar7
      0038EE 89 82            [24] 9544 	mov	dpl,r1
      0038F0 8A 83            [24] 9545 	mov	dph,r2
      0038F2 E8               [12] 9546 	mov	a,r0
      0038F3 F0               [24] 9547 	movx	@dptr,a
      0038F4 80 2F            [24] 9548 	sjmp	00124$
      0038F6                       9549 00121$:
                                   9550 ;	..\src\COMMON\easyax5043.c:1725: axradio_phy_chanvcoi[i] = axradio_calvcoi();
      0038F6 EE               [12] 9551 	mov	a,r6
      0038F7 24 0A            [12] 9552 	add	a,#_axradio_phy_chanvcoi
      0038F9 F9               [12] 9553 	mov	r1,a
      0038FA E4               [12] 9554 	clr	a
      0038FB 34 00            [12] 9555 	addc	a,#(_axradio_phy_chanvcoi >> 8)
      0038FD FA               [12] 9556 	mov	r2,a
      0038FE C0 07            [24] 9557 	push	ar7
      003900 C0 06            [24] 9558 	push	ar6
      003902 C0 05            [24] 9559 	push	ar5
      003904 C0 04            [24] 9560 	push	ar4
      003906 C0 03            [24] 9561 	push	ar3
      003908 C0 02            [24] 9562 	push	ar2
      00390A C0 01            [24] 9563 	push	ar1
      00390C 12 35 4C         [24] 9564 	lcall	_axradio_calvcoi
      00390F A8 82            [24] 9565 	mov	r0,dpl
      003911 D0 01            [24] 9566 	pop	ar1
      003913 D0 02            [24] 9567 	pop	ar2
      003915 D0 03            [24] 9568 	pop	ar3
      003917 D0 04            [24] 9569 	pop	ar4
      003919 D0 05            [24] 9570 	pop	ar5
      00391B D0 06            [24] 9571 	pop	ar6
      00391D D0 07            [24] 9572 	pop	ar7
      00391F 89 82            [24] 9573 	mov	dpl,r1
      003921 8A 83            [24] 9574 	mov	dph,r2
      003923 E8               [12] 9575 	mov	a,r0
      003924 F0               [24] 9576 	movx	@dptr,a
      003925                       9577 00124$:
                                   9578 ;	..\src\COMMON\easyax5043.c:1727: } while (--j);
      003925 DB 02            [24] 9579 	djnz	r3,00281$
      003927 80 03            [24] 9580 	sjmp	00282$
      003929                       9581 00281$:
      003929 02 38 8A         [24] 9582 	ljmp	00123$
      00392C                       9583 00282$:
                                   9584 ;	..\src\COMMON\easyax5043.c:1728: j = 1;
      00392C 90 03 62         [24] 9585 	mov	dptr,#_axradio_init_j_3_397
      00392F 74 01            [12] 9586 	mov	a,#0x01
      003931 F0               [24] 9587 	movx	@dptr,a
      003932                       9588 00126$:
                                   9589 ;	..\src\COMMON\easyax5043.c:1706: for (i = 0; i < axradio_phy_nrchannels; ++i) {
      003932 0E               [12] 9590 	inc	r6
      003933 02 38 1E         [24] 9591 	ljmp	00152$
      003936                       9592 00127$:
                                   9593 ;	..\src\COMMON\easyax5043.c:1748: AX5043_PLLVCOI = vcoisave;
      003936 90 41 80         [24] 9594 	mov	dptr,#_AX5043_PLLVCOI
      003939 EF               [12] 9595 	mov	a,r7
      00393A F0               [24] 9596 	movx	@dptr,a
                                   9597 ;	..\src\COMMON\easyax5043.c:1751: if (DBGLNKSTAT & 0x10) {
      00393B E5 E2            [12] 9598 	mov	a,_DBGLNKSTAT
      00393D 20 E4 03         [24] 9599 	jb	acc.4,00283$
      003940 02 3A B2         [24] 9600 	ljmp	00142$
      003943                       9601 00283$:
                                   9602 ;	..\src\COMMON\easyax5043.c:1752: for (i = 0; i < axradio_phy_nrchannels; ++i) {
      003943 7F 00            [12] 9603 	mov	r7,#0x00
      003945                       9604 00154$:
      003945 90 7A BA         [24] 9605 	mov	dptr,#_axradio_phy_nrchannels
      003948 E4               [12] 9606 	clr	a
      003949 93               [24] 9607 	movc	a,@a+dptr
      00394A FE               [12] 9608 	mov	r6,a
      00394B C3               [12] 9609 	clr	c
      00394C EF               [12] 9610 	mov	a,r7
      00394D 9E               [12] 9611 	subb	a,r6
      00394E 40 03            [24] 9612 	jc	00284$
      003950 02 3A B2         [24] 9613 	ljmp	00142$
      003953                       9614 00284$:
                                   9615 ;	..\src\COMMON\easyax5043.c:1753: uint8_t chg = ((axradio_phy_chanpllrnginit[0] & 0xF0) || axradio_phy_chanpllrnginit[i] != axradio_phy_chanpllrng[i])
      003953 90 7A BF         [24] 9616 	mov	dptr,#_axradio_phy_chanpllrnginit
      003956 E4               [12] 9617 	clr	a
      003957 93               [24] 9618 	movc	a,@a+dptr
      003958 FE               [12] 9619 	mov	r6,a
      003959 54 F0            [12] 9620 	anl	a,#0xf0
      00395B 70 40            [24] 9621 	jnz	00161$
      00395D EF               [12] 9622 	mov	a,r7
      00395E 90 7A BF         [24] 9623 	mov	dptr,#_axradio_phy_chanpllrnginit
      003961 93               [24] 9624 	movc	a,@a+dptr
      003962 FE               [12] 9625 	mov	r6,a
      003963 EF               [12] 9626 	mov	a,r7
      003964 24 09            [12] 9627 	add	a,#_axradio_phy_chanpllrng
      003966 F5 82            [12] 9628 	mov	dpl,a
      003968 E4               [12] 9629 	clr	a
      003969 34 00            [12] 9630 	addc	a,#(_axradio_phy_chanpllrng >> 8)
      00396B F5 83            [12] 9631 	mov	dph,a
      00396D E0               [24] 9632 	movx	a,@dptr
      00396E FD               [12] 9633 	mov	r5,a
      00396F EE               [12] 9634 	mov	a,r6
      003970 B5 05 03         [24] 9635 	cjne	a,ar5,00287$
      003973 D3               [12] 9636 	setb	c
      003974 80 01            [24] 9637 	sjmp	00288$
      003976                       9638 00287$:
      003976 C3               [12] 9639 	clr	c
      003977                       9640 00288$:
      003977 92 02            [24] 9641 	mov	_axradio_init_sloc0_1_0,c
      003979 50 22            [24] 9642 	jnc	00161$
      00397B 90 7A C0         [24] 9643 	mov	dptr,#_axradio_phy_chanvcoiinit
      00397E E4               [12] 9644 	clr	a
      00397F 93               [24] 9645 	movc	a,@a+dptr
      003980 60 1B            [24] 9646 	jz	00161$
      003982 EF               [12] 9647 	mov	a,r7
      003983 90 7A C0         [24] 9648 	mov	dptr,#_axradio_phy_chanvcoiinit
      003986 93               [24] 9649 	movc	a,@a+dptr
      003987 FE               [12] 9650 	mov	r6,a
      003988 EF               [12] 9651 	mov	a,r7
      003989 24 0A            [12] 9652 	add	a,#_axradio_phy_chanvcoi
      00398B F5 82            [12] 9653 	mov	dpl,a
      00398D E4               [12] 9654 	clr	a
      00398E 34 00            [12] 9655 	addc	a,#(_axradio_phy_chanvcoi >> 8)
      003990 F5 83            [12] 9656 	mov	dph,a
      003992 E0               [24] 9657 	movx	a,@dptr
      003993 FD               [12] 9658 	mov	r5,a
      003994 6E               [12] 9659 	xrl	a,r6
      003995 54 7F            [12] 9660 	anl	a,#0x7f
      003997 70 04            [24] 9661 	jnz	00161$
      003999 C2 02            [12] 9662 	clr	_axradio_init_sloc0_1_0
      00399B 80 02            [24] 9663 	sjmp	00162$
      00399D                       9664 00161$:
      00399D D2 02            [12] 9665 	setb	_axradio_init_sloc0_1_0
      00399F                       9666 00162$:
      00399F A2 02            [12] 9667 	mov	c,_axradio_init_sloc0_1_0
      0039A1 E4               [12] 9668 	clr	a
      0039A2 33               [12] 9669 	rlc	a
      0039A3 FE               [12] 9670 	mov	r6,a
                                   9671 ;	..\src\COMMON\easyax5043.c:1755: if (1 && !chg)
      0039A4 20 02 03         [24] 9672 	jb	_axradio_init_sloc0_1_0,00293$
      0039A7 02 3A AE         [24] 9673 	ljmp	00137$
      0039AA                       9674 00293$:
                                   9675 ;	..\src\COMMON\easyax5043.c:1757: dbglink_writestr("CH ");
      0039AA 90 7C 5C         [24] 9676 	mov	dptr,#___str_3
      0039AD 75 F0 80         [24] 9677 	mov	b,#0x80
      0039B0 C0 07            [24] 9678 	push	ar7
      0039B2 C0 06            [24] 9679 	push	ar6
      0039B4 12 6E FA         [24] 9680 	lcall	_dbglink_writestr
      0039B7 D0 06            [24] 9681 	pop	ar6
      0039B9 D0 07            [24] 9682 	pop	ar7
                                   9683 ;	..\src\COMMON\easyax5043.c:1758: dbglink_writenum16(i, 0, 0);
      0039BB 8F 04            [24] 9684 	mov	ar4,r7
      0039BD 7D 00            [12] 9685 	mov	r5,#0x00
      0039BF C0 07            [24] 9686 	push	ar7
      0039C1 C0 06            [24] 9687 	push	ar6
      0039C3 E4               [12] 9688 	clr	a
      0039C4 C0 E0            [24] 9689 	push	acc
      0039C6 C0 E0            [24] 9690 	push	acc
      0039C8 8C 82            [24] 9691 	mov	dpl,r4
      0039CA 8D 83            [24] 9692 	mov	dph,r5
      0039CC 12 78 6F         [24] 9693 	lcall	_dbglink_writenum16
      0039CF 15 81            [12] 9694 	dec	sp
      0039D1 15 81            [12] 9695 	dec	sp
                                   9696 ;	..\src\COMMON\easyax5043.c:1759: dbglink_writestr(" RNG ");
      0039D3 90 7C 60         [24] 9697 	mov	dptr,#___str_4
      0039D6 75 F0 80         [24] 9698 	mov	b,#0x80
      0039D9 12 6E FA         [24] 9699 	lcall	_dbglink_writestr
      0039DC D0 06            [24] 9700 	pop	ar6
      0039DE D0 07            [24] 9701 	pop	ar7
                                   9702 ;	..\src\COMMON\easyax5043.c:1760: if (!(axradio_phy_chanpllrnginit[0] & 0xF0)) {
      0039E0 90 7A BF         [24] 9703 	mov	dptr,#_axradio_phy_chanpllrnginit
      0039E3 E4               [12] 9704 	clr	a
      0039E4 93               [24] 9705 	movc	a,@a+dptr
      0039E5 FD               [12] 9706 	mov	r5,a
      0039E6 54 F0            [12] 9707 	anl	a,#0xf0
      0039E8 70 26            [24] 9708 	jnz	00132$
                                   9709 ;	..\src\COMMON\easyax5043.c:1761: dbglink_writenum16(axradio_phy_chanpllrnginit[i], 0, 0);
      0039EA EF               [12] 9710 	mov	a,r7
      0039EB 90 7A BF         [24] 9711 	mov	dptr,#_axradio_phy_chanpllrnginit
      0039EE 93               [24] 9712 	movc	a,@a+dptr
      0039EF FD               [12] 9713 	mov	r5,a
      0039F0 7C 00            [12] 9714 	mov	r4,#0x00
      0039F2 C0 07            [24] 9715 	push	ar7
      0039F4 C0 06            [24] 9716 	push	ar6
      0039F6 E4               [12] 9717 	clr	a
      0039F7 C0 E0            [24] 9718 	push	acc
      0039F9 C0 E0            [24] 9719 	push	acc
      0039FB 8D 82            [24] 9720 	mov	dpl,r5
      0039FD 8C 83            [24] 9721 	mov	dph,r4
      0039FF 12 78 6F         [24] 9722 	lcall	_dbglink_writenum16
      003A02 15 81            [12] 9723 	dec	sp
      003A04 15 81            [12] 9724 	dec	sp
      003A06 D0 06            [24] 9725 	pop	ar6
      003A08 D0 07            [24] 9726 	pop	ar7
                                   9727 ;	..\src\COMMON\easyax5043.c:1762: dbglink_tx('/');
      003A0A 75 82 2F         [24] 9728 	mov	dpl,#0x2f
      003A0D 12 5D 95         [24] 9729 	lcall	_dbglink_tx
      003A10                       9730 00132$:
                                   9731 ;	..\src\COMMON\easyax5043.c:1764: dbglink_writenum16(axradio_phy_chanpllrng[i], 0, 0);
      003A10 EF               [12] 9732 	mov	a,r7
      003A11 24 09            [12] 9733 	add	a,#_axradio_phy_chanpllrng
      003A13 F5 82            [12] 9734 	mov	dpl,a
      003A15 E4               [12] 9735 	clr	a
      003A16 34 00            [12] 9736 	addc	a,#(_axradio_phy_chanpllrng >> 8)
      003A18 F5 83            [12] 9737 	mov	dph,a
      003A1A E0               [24] 9738 	movx	a,@dptr
      003A1B FD               [12] 9739 	mov	r5,a
      003A1C 7C 00            [12] 9740 	mov	r4,#0x00
      003A1E C0 07            [24] 9741 	push	ar7
      003A20 C0 06            [24] 9742 	push	ar6
      003A22 E4               [12] 9743 	clr	a
      003A23 C0 E0            [24] 9744 	push	acc
      003A25 C0 E0            [24] 9745 	push	acc
      003A27 8D 82            [24] 9746 	mov	dpl,r5
      003A29 8C 83            [24] 9747 	mov	dph,r4
      003A2B 12 78 6F         [24] 9748 	lcall	_dbglink_writenum16
      003A2E 15 81            [12] 9749 	dec	sp
      003A30 15 81            [12] 9750 	dec	sp
                                   9751 ;	..\src\COMMON\easyax5043.c:1765: dbglink_writestr(" VCOI ");
      003A32 90 7C 66         [24] 9752 	mov	dptr,#___str_5
      003A35 75 F0 80         [24] 9753 	mov	b,#0x80
      003A38 12 6E FA         [24] 9754 	lcall	_dbglink_writestr
      003A3B D0 06            [24] 9755 	pop	ar6
      003A3D D0 07            [24] 9756 	pop	ar7
                                   9757 ;	..\src\COMMON\easyax5043.c:1766: if (axradio_phy_chanvcoiinit[0]) {
      003A3F 90 7A C0         [24] 9758 	mov	dptr,#_axradio_phy_chanvcoiinit
      003A42 E4               [12] 9759 	clr	a
      003A43 93               [24] 9760 	movc	a,@a+dptr
      003A44 60 29            [24] 9761 	jz	00134$
                                   9762 ;	..\src\COMMON\easyax5043.c:1767: dbglink_writenum16(axradio_phy_chanvcoiinit[i] & 0x7F, 0, 0);
      003A46 EF               [12] 9763 	mov	a,r7
      003A47 90 7A C0         [24] 9764 	mov	dptr,#_axradio_phy_chanvcoiinit
      003A4A 93               [24] 9765 	movc	a,@a+dptr
      003A4B FD               [12] 9766 	mov	r5,a
      003A4C 53 05 7F         [24] 9767 	anl	ar5,#0x7f
      003A4F 7C 00            [12] 9768 	mov	r4,#0x00
      003A51 C0 07            [24] 9769 	push	ar7
      003A53 C0 06            [24] 9770 	push	ar6
      003A55 E4               [12] 9771 	clr	a
      003A56 C0 E0            [24] 9772 	push	acc
      003A58 C0 E0            [24] 9773 	push	acc
      003A5A 8D 82            [24] 9774 	mov	dpl,r5
      003A5C 8C 83            [24] 9775 	mov	dph,r4
      003A5E 12 78 6F         [24] 9776 	lcall	_dbglink_writenum16
      003A61 15 81            [12] 9777 	dec	sp
      003A63 15 81            [12] 9778 	dec	sp
      003A65 D0 06            [24] 9779 	pop	ar6
      003A67 D0 07            [24] 9780 	pop	ar7
                                   9781 ;	..\src\COMMON\easyax5043.c:1768: dbglink_tx('/');
      003A69 75 82 2F         [24] 9782 	mov	dpl,#0x2f
      003A6C 12 5D 95         [24] 9783 	lcall	_dbglink_tx
      003A6F                       9784 00134$:
                                   9785 ;	..\src\COMMON\easyax5043.c:1770: dbglink_writenum16(axradio_phy_chanvcoi[i] & 0x7F, 0, 0);
      003A6F EF               [12] 9786 	mov	a,r7
      003A70 24 0A            [12] 9787 	add	a,#_axradio_phy_chanvcoi
      003A72 F5 82            [12] 9788 	mov	dpl,a
      003A74 E4               [12] 9789 	clr	a
      003A75 34 00            [12] 9790 	addc	a,#(_axradio_phy_chanvcoi >> 8)
      003A77 F5 83            [12] 9791 	mov	dph,a
      003A79 E0               [24] 9792 	movx	a,@dptr
      003A7A FD               [12] 9793 	mov	r5,a
      003A7B 53 05 7F         [24] 9794 	anl	ar5,#0x7f
      003A7E 7C 00            [12] 9795 	mov	r4,#0x00
      003A80 C0 07            [24] 9796 	push	ar7
      003A82 C0 06            [24] 9797 	push	ar6
      003A84 E4               [12] 9798 	clr	a
      003A85 C0 E0            [24] 9799 	push	acc
      003A87 C0 E0            [24] 9800 	push	acc
      003A89 8D 82            [24] 9801 	mov	dpl,r5
      003A8B 8C 83            [24] 9802 	mov	dph,r4
      003A8D 12 78 6F         [24] 9803 	lcall	_dbglink_writenum16
      003A90 15 81            [12] 9804 	dec	sp
      003A92 15 81            [12] 9805 	dec	sp
      003A94 D0 06            [24] 9806 	pop	ar6
      003A96 D0 07            [24] 9807 	pop	ar7
                                   9808 ;	..\src\COMMON\easyax5043.c:1771: if (chg)
      003A98 EE               [12] 9809 	mov	a,r6
      003A99 60 0D            [24] 9810 	jz	00136$
                                   9811 ;	..\src\COMMON\easyax5043.c:1772: dbglink_writestr(" *");
      003A9B 90 7C 6D         [24] 9812 	mov	dptr,#___str_6
      003A9E 75 F0 80         [24] 9813 	mov	b,#0x80
      003AA1 C0 07            [24] 9814 	push	ar7
      003AA3 12 6E FA         [24] 9815 	lcall	_dbglink_writestr
      003AA6 D0 07            [24] 9816 	pop	ar7
      003AA8                       9817 00136$:
                                   9818 ;	..\src\COMMON\easyax5043.c:1773: dbglink_tx('\n');
      003AA8 75 82 0A         [24] 9819 	mov	dpl,#0x0a
      003AAB 12 5D 95         [24] 9820 	lcall	_dbglink_tx
      003AAE                       9821 00137$:
                                   9822 ;	..\src\COMMON\easyax5043.c:1752: for (i = 0; i < axradio_phy_nrchannels; ++i) {
      003AAE 0F               [12] 9823 	inc	r7
      003AAF 02 39 45         [24] 9824 	ljmp	00154$
      003AB2                       9825 00142$:
                                   9826 ;	..\src\COMMON\easyax5043.c:1778: AX5043_PWRMODE = AX5043_PWRSTATE_POWERDOWN;
      003AB2 90 40 02         [24] 9827 	mov	dptr,#_AX5043_PWRMODE
      003AB5 E4               [12] 9828 	clr	a
      003AB6 F0               [24] 9829 	movx	@dptr,a
                                   9830 ;	..\src\COMMON\easyax5043.c:1779: ax5043_init_registers();
      003AB7 12 24 44         [24] 9831 	lcall	_ax5043_init_registers
                                   9832 ;	..\src\COMMON\easyax5043.c:1780: ax5043_set_registers_rx();
      003ABA 12 06 7A         [24] 9833 	lcall	_ax5043_set_registers_rx
                                   9834 ;	..\src\COMMON\easyax5043.c:1781: AX5043_PLLRANGINGA = axradio_phy_chanpllrng[0] & 0x0F;
      003ABD 90 00 09         [24] 9835 	mov	dptr,#_axradio_phy_chanpllrng
      003AC0 E0               [24] 9836 	movx	a,@dptr
      003AC1 FF               [12] 9837 	mov	r7,a
      003AC2 90 40 33         [24] 9838 	mov	dptr,#_AX5043_PLLRANGINGA
      003AC5 74 0F            [12] 9839 	mov	a,#0x0f
      003AC7 5F               [12] 9840 	anl	a,r7
      003AC8 F0               [24] 9841 	movx	@dptr,a
                                   9842 ;	..\src\COMMON\easyax5043.c:1783: uint32_t __autodata f = axradio_phy_chanfreq[0];
      003AC9 90 7A BB         [24] 9843 	mov	dptr,#_axradio_phy_chanfreq
      003ACC E4               [12] 9844 	clr	a
      003ACD 93               [24] 9845 	movc	a,@a+dptr
      003ACE FC               [12] 9846 	mov	r4,a
      003ACF A3               [24] 9847 	inc	dptr
      003AD0 E4               [12] 9848 	clr	a
      003AD1 93               [24] 9849 	movc	a,@a+dptr
      003AD2 FD               [12] 9850 	mov	r5,a
      003AD3 A3               [24] 9851 	inc	dptr
      003AD4 E4               [12] 9852 	clr	a
      003AD5 93               [24] 9853 	movc	a,@a+dptr
      003AD6 FE               [12] 9854 	mov	r6,a
      003AD7 A3               [24] 9855 	inc	dptr
      003AD8 E4               [12] 9856 	clr	a
      003AD9 93               [24] 9857 	movc	a,@a+dptr
      003ADA FF               [12] 9858 	mov	r7,a
                                   9859 ;	..\src\COMMON\easyax5043.c:1784: AX5043_FREQA0 = f;
      003ADB 90 40 37         [24] 9860 	mov	dptr,#_AX5043_FREQA0
      003ADE EC               [12] 9861 	mov	a,r4
      003ADF F0               [24] 9862 	movx	@dptr,a
                                   9863 ;	..\src\COMMON\easyax5043.c:1785: AX5043_FREQA1 = f >> 8;
      003AE0 90 40 36         [24] 9864 	mov	dptr,#_AX5043_FREQA1
      003AE3 ED               [12] 9865 	mov	a,r5
      003AE4 F0               [24] 9866 	movx	@dptr,a
                                   9867 ;	..\src\COMMON\easyax5043.c:1786: AX5043_FREQA2 = f >> 16;
      003AE5 90 40 35         [24] 9868 	mov	dptr,#_AX5043_FREQA2
      003AE8 EE               [12] 9869 	mov	a,r6
      003AE9 F0               [24] 9870 	movx	@dptr,a
                                   9871 ;	..\src\COMMON\easyax5043.c:1787: AX5043_FREQA3 = f >> 24;
      003AEA 90 40 34         [24] 9872 	mov	dptr,#_AX5043_FREQA3
      003AED EF               [12] 9873 	mov	a,r7
      003AEE F0               [24] 9874 	movx	@dptr,a
                                   9875 ;	..\src\COMMON\easyax5043.c:1790: axradio_mode = AXRADIO_MODE_OFF;
      003AEF 75 0B 01         [24] 9876 	mov	_axradio_mode,#0x01
                                   9877 ;	..\src\COMMON\easyax5043.c:1791: for (i = 0; i < axradio_phy_nrchannels; ++i)
      003AF2 7F 00            [12] 9878 	mov	r7,#0x00
      003AF4                       9879 00156$:
      003AF4 90 7A BA         [24] 9880 	mov	dptr,#_axradio_phy_nrchannels
      003AF7 E4               [12] 9881 	clr	a
      003AF8 93               [24] 9882 	movc	a,@a+dptr
      003AF9 FE               [12] 9883 	mov	r6,a
      003AFA C3               [12] 9884 	clr	c
      003AFB EF               [12] 9885 	mov	a,r7
      003AFC 9E               [12] 9886 	subb	a,r6
      003AFD 50 16            [24] 9887 	jnc	00145$
                                   9888 ;	..\src\COMMON\easyax5043.c:1792: if (axradio_phy_chanpllrng[i] & 0x20)
      003AFF EF               [12] 9889 	mov	a,r7
      003B00 24 09            [12] 9890 	add	a,#_axradio_phy_chanpllrng
      003B02 F5 82            [12] 9891 	mov	dpl,a
      003B04 E4               [12] 9892 	clr	a
      003B05 34 00            [12] 9893 	addc	a,#(_axradio_phy_chanpllrng >> 8)
      003B07 F5 83            [12] 9894 	mov	dph,a
      003B09 E0               [24] 9895 	movx	a,@dptr
      003B0A FE               [12] 9896 	mov	r6,a
      003B0B 30 E5 04         [24] 9897 	jnb	acc.5,00157$
                                   9898 ;	..\src\COMMON\easyax5043.c:1793: return AXRADIO_ERR_RANGING;
      003B0E 75 82 06         [24] 9899 	mov	dpl,#0x06
      003B11 22               [24] 9900 	ret
      003B12                       9901 00157$:
                                   9902 ;	..\src\COMMON\easyax5043.c:1791: for (i = 0; i < axradio_phy_nrchannels; ++i)
      003B12 0F               [12] 9903 	inc	r7
      003B13 80 DF            [24] 9904 	sjmp	00156$
      003B15                       9905 00145$:
                                   9906 ;	..\src\COMMON\easyax5043.c:1794: return AXRADIO_ERR_NOERROR;
      003B15 75 82 00         [24] 9907 	mov	dpl,#0x00
      003B18 22               [24] 9908 	ret
                                   9909 ;------------------------------------------------------------
                                   9910 ;Allocation info for local variables in function 'axradio_cansleep'
                                   9911 ;------------------------------------------------------------
                                   9912 ;	..\src\COMMON\easyax5043.c:1797: __reentrantb uint8_t axradio_cansleep(void) __reentrant
                                   9913 ;	-----------------------------------------
                                   9914 ;	 function axradio_cansleep
                                   9915 ;	-----------------------------------------
      003B19                       9916 _axradio_cansleep:
                                   9917 ;	..\src\COMMON\easyax5043.c:1799: if (axradio_trxstate == trxstate_off || axradio_trxstate == trxstate_rxwor)
      003B19 E5 0C            [12] 9918 	mov	a,_axradio_trxstate
      003B1B 60 05            [24] 9919 	jz	00101$
      003B1D 74 02            [12] 9920 	mov	a,#0x02
      003B1F B5 0C 04         [24] 9921 	cjne	a,_axradio_trxstate,00102$
      003B22                       9922 00101$:
                                   9923 ;	..\src\COMMON\easyax5043.c:1800: return 1;
      003B22 75 82 01         [24] 9924 	mov	dpl,#0x01
      003B25 22               [24] 9925 	ret
      003B26                       9926 00102$:
                                   9927 ;	..\src\COMMON\easyax5043.c:1801: return 0;
      003B26 75 82 00         [24] 9928 	mov	dpl,#0x00
      003B29 22               [24] 9929 	ret
                                   9930 ;------------------------------------------------------------
                                   9931 ;Allocation info for local variables in function 'axradio_set_mode'
                                   9932 ;------------------------------------------------------------
                                   9933 ;r                         Allocated to registers r5 
                                   9934 ;r                         Allocated to registers r6 
                                   9935 ;iesave                    Allocated to registers r6 
                                   9936 ;mode                      Allocated with name '_axradio_set_mode_mode_1_410'
                                   9937 ;------------------------------------------------------------
                                   9938 ;	..\src\COMMON\easyax5043.c:1805: uint8_t axradio_set_mode(uint8_t mode)
                                   9939 ;	-----------------------------------------
                                   9940 ;	 function axradio_set_mode
                                   9941 ;	-----------------------------------------
      003B2A                       9942 _axradio_set_mode:
      003B2A E5 82            [12] 9943 	mov	a,dpl
      003B2C 90 03 64         [24] 9944 	mov	dptr,#_axradio_set_mode_mode_1_410
      003B2F F0               [24] 9945 	movx	@dptr,a
                                   9946 ;	..\src\COMMON\easyax5043.c:1807: if (mode == axradio_mode)
      003B30 E0               [24] 9947 	movx	a,@dptr
      003B31 FF               [12] 9948 	mov	r7,a
      003B32 B5 0B 04         [24] 9949 	cjne	a,_axradio_mode,00102$
                                   9950 ;	..\src\COMMON\easyax5043.c:1808: return AXRADIO_ERR_NOERROR;
      003B35 75 82 00         [24] 9951 	mov	dpl,#0x00
      003B38 22               [24] 9952 	ret
      003B39                       9953 00102$:
                                   9954 ;	..\src\COMMON\easyax5043.c:1809: switch (axradio_mode) {
      003B39 AE 0B            [24] 9955 	mov	r6,_axradio_mode
      003B3B BE 00 02         [24] 9956 	cjne	r6,#0x00,00283$
      003B3E 80 4C            [24] 9957 	sjmp	00103$
      003B40                       9958 00283$:
      003B40 BE 02 02         [24] 9959 	cjne	r6,#0x02,00284$
      003B43 80 5A            [24] 9960 	sjmp	00106$
      003B45                       9961 00284$:
      003B45 BE 03 03         [24] 9962 	cjne	r6,#0x03,00285$
      003B48 02 3B CF         [24] 9963 	ljmp	00116$
      003B4B                       9964 00285$:
      003B4B BE 18 03         [24] 9965 	cjne	r6,#0x18,00286$
      003B4E 02 3B CF         [24] 9966 	ljmp	00116$
      003B51                       9967 00286$:
      003B51 BE 19 02         [24] 9968 	cjne	r6,#0x19,00287$
      003B54 80 79            [24] 9969 	sjmp	00116$
      003B56                       9970 00287$:
      003B56 BE 1A 02         [24] 9971 	cjne	r6,#0x1a,00288$
      003B59 80 74            [24] 9972 	sjmp	00116$
      003B5B                       9973 00288$:
      003B5B BE 1B 02         [24] 9974 	cjne	r6,#0x1b,00289$
      003B5E 80 6F            [24] 9975 	sjmp	00116$
      003B60                       9976 00289$:
      003B60 BE 1C 02         [24] 9977 	cjne	r6,#0x1c,00290$
      003B63 80 6A            [24] 9978 	sjmp	00116$
      003B65                       9979 00290$:
      003B65 BE 28 03         [24] 9980 	cjne	r6,#0x28,00291$
      003B68 02 3C 28         [24] 9981 	ljmp	00124$
      003B6B                       9982 00291$:
      003B6B BE 29 03         [24] 9983 	cjne	r6,#0x29,00292$
      003B6E 02 3C 28         [24] 9984 	ljmp	00124$
      003B71                       9985 00292$:
      003B71 BE 2A 03         [24] 9986 	cjne	r6,#0x2a,00293$
      003B74 02 3C 28         [24] 9987 	ljmp	00124$
      003B77                       9988 00293$:
      003B77 BE 2B 03         [24] 9989 	cjne	r6,#0x2b,00294$
      003B7A 02 3C 28         [24] 9990 	ljmp	00124$
      003B7D                       9991 00294$:
      003B7D BE 2C 03         [24] 9992 	cjne	r6,#0x2c,00295$
      003B80 02 3C 28         [24] 9993 	ljmp	00124$
      003B83                       9994 00295$:
      003B83 BE 2D 03         [24] 9995 	cjne	r6,#0x2d,00296$
      003B86 02 3C 28         [24] 9996 	ljmp	00124$
      003B89                       9997 00296$:
      003B89 02 3C 35         [24] 9998 	ljmp	00125$
                                   9999 ;	..\src\COMMON\easyax5043.c:1810: case AXRADIO_MODE_UNINIT:
      003B8C                      10000 00103$:
                                  10001 ;	..\src\COMMON\easyax5043.c:1812: uint8_t __autodata r = axradio_init();
      003B8C C0 07            [24]10002 	push	ar7
      003B8E 12 36 02         [24]10003 	lcall	_axradio_init
      003B91 AE 82            [24]10004 	mov	r6,dpl
      003B93 D0 07            [24]10005 	pop	ar7
                                  10006 ;	..\src\COMMON\easyax5043.c:1813: if (r != AXRADIO_ERR_NOERROR)
      003B95 EE               [12]10007 	mov	a,r6
      003B96 FD               [12]10008 	mov	r5,a
      003B97 70 03            [24]10009 	jnz	00297$
      003B99 02 3C 3F         [24]10010 	ljmp	00126$
      003B9C                      10011 00297$:
                                  10012 ;	..\src\COMMON\easyax5043.c:1814: return r;
      003B9C 8D 82            [24]10013 	mov	dpl,r5
      003B9E 22               [24]10014 	ret
                                  10015 ;	..\src\COMMON\easyax5043.c:1818: case AXRADIO_MODE_DEEPSLEEP:
      003B9F                      10016 00106$:
                                  10017 ;	..\src\COMMON\easyax5043.c:1820: uint8_t __autodata r = ax5043_wakeup_deepsleep();
      003B9F C0 07            [24]10018 	push	ar7
      003BA1 12 5E 00         [24]10019 	lcall	_ax5043_wakeup_deepsleep
      003BA4 AE 82            [24]10020 	mov	r6,dpl
      003BA6 D0 07            [24]10021 	pop	ar7
                                  10022 ;	..\src\COMMON\easyax5043.c:1821: if (r)
      003BA8 EE               [12]10023 	mov	a,r6
      003BA9 60 04            [24]10024 	jz	00108$
                                  10025 ;	..\src\COMMON\easyax5043.c:1822: return AXRADIO_ERR_NOCHIP;
      003BAB 75 82 05         [24]10026 	mov	dpl,#0x05
      003BAE 22               [24]10027 	ret
      003BAF                      10028 00108$:
                                  10029 ;	..\src\COMMON\easyax5043.c:1823: ax5043_init_registers();
      003BAF C0 07            [24]10030 	push	ar7
      003BB1 12 24 44         [24]10031 	lcall	_ax5043_init_registers
                                  10032 ;	..\src\COMMON\easyax5043.c:1824: r = axradio_set_channel(axradio_curchannel);
      003BB4 90 00 BC         [24]10033 	mov	dptr,#_axradio_curchannel
      003BB7 E0               [24]10034 	movx	a,@dptr
      003BB8 F5 82            [12]10035 	mov	dpl,a
      003BBA 12 3F 84         [24]10036 	lcall	_axradio_set_channel
      003BBD AE 82            [24]10037 	mov	r6,dpl
      003BBF D0 07            [24]10038 	pop	ar7
                                  10039 ;	..\src\COMMON\easyax5043.c:1825: if (r != AXRADIO_ERR_NOERROR)
      003BC1 EE               [12]10040 	mov	a,r6
      003BC2 60 03            [24]10041 	jz	00110$
                                  10042 ;	..\src\COMMON\easyax5043.c:1826: return r;
      003BC4 8E 82            [24]10043 	mov	dpl,r6
      003BC6 22               [24]10044 	ret
      003BC7                      10045 00110$:
                                  10046 ;	..\src\COMMON\easyax5043.c:1827: axradio_trxstate = trxstate_off;
      003BC7 75 0C 00         [24]10047 	mov	_axradio_trxstate,#0x00
                                  10048 ;	..\src\COMMON\easyax5043.c:1828: axradio_mode = AXRADIO_MODE_OFF;
      003BCA 75 0B 01         [24]10049 	mov	_axradio_mode,#0x01
                                  10050 ;	..\src\COMMON\easyax5043.c:1829: break;
                                  10051 ;	..\src\COMMON\easyax5043.c:1837: case AXRADIO_MODE_CW_TRANSMIT:
      003BCD 80 70            [24]10052 	sjmp	00126$
      003BCF                      10053 00116$:
                                  10054 ;	..\src\COMMON\easyax5043.c:1839: uint8_t __autodata iesave = IE & 0x80;
      003BCF 74 80            [12]10055 	mov	a,#0x80
      003BD1 55 A8            [12]10056 	anl	a,_IE
      003BD3 FE               [12]10057 	mov	r6,a
                                  10058 ;	..\src\COMMON\easyax5043.c:1840: EA = 0;
      003BD4 C2 AF            [12]10059 	clr	_EA
                                  10060 ;	..\src\COMMON\easyax5043.c:1841: if (axradio_trxstate == trxstate_off) {
      003BD6 E5 0C            [12]10061 	mov	a,_axradio_trxstate
      003BD8 70 38            [24]10062 	jnz	00118$
                                  10063 ;	..\src\COMMON\easyax5043.c:1842: update_timeanchor();
      003BDA C0 07            [24]10064 	push	ar7
      003BDC C0 06            [24]10065 	push	ar6
      003BDE 12 15 BA         [24]10066 	lcall	_update_timeanchor
                                  10067 ;	..\src\COMMON\easyax5043.c:1843: wtimer_remove_callback(&axradio_cb_transmitend.cb);
      003BE1 90 03 28         [24]10068 	mov	dptr,#_axradio_cb_transmitend
      003BE4 12 74 C0         [24]10069 	lcall	_wtimer_remove_callback
                                  10070 ;	..\src\COMMON\easyax5043.c:1844: axradio_cb_transmitend.st.error = AXRADIO_ERR_NOERROR;
      003BE7 90 03 2D         [24]10071 	mov	dptr,#(_axradio_cb_transmitend + 0x0005)
      003BEA E4               [12]10072 	clr	a
      003BEB F0               [24]10073 	movx	@dptr,a
                                  10074 ;	..\src\COMMON\easyax5043.c:1845: axradio_cb_transmitend.st.time.t = axradio_timeanchor.radiotimer;
      003BEC 90 00 CD         [24]10075 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      003BEF E0               [24]10076 	movx	a,@dptr
      003BF0 FA               [12]10077 	mov	r2,a
      003BF1 A3               [24]10078 	inc	dptr
      003BF2 E0               [24]10079 	movx	a,@dptr
      003BF3 FB               [12]10080 	mov	r3,a
      003BF4 A3               [24]10081 	inc	dptr
      003BF5 E0               [24]10082 	movx	a,@dptr
      003BF6 FC               [12]10083 	mov	r4,a
      003BF7 A3               [24]10084 	inc	dptr
      003BF8 E0               [24]10085 	movx	a,@dptr
      003BF9 FD               [12]10086 	mov	r5,a
      003BFA 90 03 2E         [24]10087 	mov	dptr,#(_axradio_cb_transmitend + 0x0006)
      003BFD EA               [12]10088 	mov	a,r2
      003BFE F0               [24]10089 	movx	@dptr,a
      003BFF EB               [12]10090 	mov	a,r3
      003C00 A3               [24]10091 	inc	dptr
      003C01 F0               [24]10092 	movx	@dptr,a
      003C02 EC               [12]10093 	mov	a,r4
      003C03 A3               [24]10094 	inc	dptr
      003C04 F0               [24]10095 	movx	@dptr,a
      003C05 ED               [12]10096 	mov	a,r5
      003C06 A3               [24]10097 	inc	dptr
      003C07 F0               [24]10098 	movx	@dptr,a
                                  10099 ;	..\src\COMMON\easyax5043.c:1846: wtimer_add_callback(&axradio_cb_transmitend.cb);
      003C08 90 03 28         [24]10100 	mov	dptr,#_axradio_cb_transmitend
      003C0B 12 66 0B         [24]10101 	lcall	_wtimer_add_callback
      003C0E D0 06            [24]10102 	pop	ar6
      003C10 D0 07            [24]10103 	pop	ar7
      003C12                      10104 00118$:
                                  10105 ;	..\src\COMMON\easyax5043.c:1848: ax5043_off();
      003C12 C0 07            [24]10106 	push	ar7
      003C14 C0 06            [24]10107 	push	ar6
      003C16 12 22 CE         [24]10108 	lcall	_ax5043_off
      003C19 D0 06            [24]10109 	pop	ar6
                                  10110 ;	..\src\COMMON\easyax5043.c:1849: IE |= iesave;
      003C1B EE               [12]10111 	mov	a,r6
      003C1C 42 A8            [12]10112 	orl	_IE,a
                                  10113 ;	..\src\COMMON\easyax5043.c:1851: ax5043_init_registers();
      003C1E 12 24 44         [24]10114 	lcall	_ax5043_init_registers
      003C21 D0 07            [24]10115 	pop	ar7
                                  10116 ;	..\src\COMMON\easyax5043.c:1852: axradio_mode = AXRADIO_MODE_OFF;
      003C23 75 0B 01         [24]10117 	mov	_axradio_mode,#0x01
                                  10118 ;	..\src\COMMON\easyax5043.c:1853: break;
                                  10119 ;	..\src\COMMON\easyax5043.c:1861: case AXRADIO_MODE_STREAM_RECEIVE_DATAPIN:
      003C26 80 17            [24]10120 	sjmp	00126$
      003C28                      10121 00124$:
                                  10122 ;	..\src\COMMON\easyax5043.c:1862: ax5043_off();
      003C28 C0 07            [24]10123 	push	ar7
      003C2A 12 22 CE         [24]10124 	lcall	_ax5043_off
                                  10125 ;	..\src\COMMON\easyax5043.c:1863: ax5043_init_registers();
      003C2D 12 24 44         [24]10126 	lcall	_ax5043_init_registers
      003C30 D0 07            [24]10127 	pop	ar7
                                  10128 ;	..\src\COMMON\easyax5043.c:1864: axradio_mode = AXRADIO_MODE_OFF;
      003C32 75 0B 01         [24]10129 	mov	_axradio_mode,#0x01
                                  10130 ;	..\src\COMMON\easyax5043.c:1866: default:
      003C35                      10131 00125$:
                                  10132 ;	..\src\COMMON\easyax5043.c:1867: ax5043_off();
      003C35 C0 07            [24]10133 	push	ar7
      003C37 12 22 CE         [24]10134 	lcall	_ax5043_off
      003C3A D0 07            [24]10135 	pop	ar7
                                  10136 ;	..\src\COMMON\easyax5043.c:1868: axradio_mode = AXRADIO_MODE_OFF;
      003C3C 75 0B 01         [24]10137 	mov	_axradio_mode,#0x01
                                  10138 ;	..\src\COMMON\easyax5043.c:1870: }
      003C3F                      10139 00126$:
                                  10140 ;	..\src\COMMON\easyax5043.c:1871: axradio_killallcb();
      003C3F C0 07            [24]10141 	push	ar7
      003C41 12 34 03         [24]10142 	lcall	_axradio_killallcb
      003C44 D0 07            [24]10143 	pop	ar7
                                  10144 ;	..\src\COMMON\easyax5043.c:1872: if (mode == AXRADIO_MODE_UNINIT)
      003C46 EF               [12]10145 	mov	a,r7
      003C47 70 04            [24]10146 	jnz	00128$
                                  10147 ;	..\src\COMMON\easyax5043.c:1873: return AXRADIO_ERR_NOTSUPPORTED;
      003C49 75 82 01         [24]10148 	mov	dpl,#0x01
      003C4C 22               [24]10149 	ret
      003C4D                      10150 00128$:
                                  10151 ;	..\src\COMMON\easyax5043.c:1874: axradio_syncstate = syncstate_off;
      003C4D 90 00 B7         [24]10152 	mov	dptr,#_axradio_syncstate
      003C50 E4               [12]10153 	clr	a
      003C51 F0               [24]10154 	movx	@dptr,a
                                  10155 ;	..\src\COMMON\easyax5043.c:1875: switch (mode) {
      003C52 EF               [12]10156 	mov	a,r7
      003C53 FE               [12]10157 	mov	r6,a
      003C54 24 CC            [12]10158 	add	a,#0xff - 0x33
      003C56 50 03            [24]10159 	jnc	00302$
      003C58 02 3F 80         [24]10160 	ljmp	00181$
      003C5B                      10161 00302$:
      003C5B EE               [12]10162 	mov	a,r6
      003C5C 24 0A            [12]10163 	add	a,#(00303$-3-.)
      003C5E 83               [24]10164 	movc	a,@a+pc
      003C5F F5 82            [12]10165 	mov	dpl,a
      003C61 EE               [12]10166 	mov	a,r6
      003C62 24 38            [12]10167 	add	a,#(00304$-3-.)
      003C64 83               [24]10168 	movc	a,@a+pc
      003C65 F5 83            [12]10169 	mov	dph,a
      003C67 E4               [12]10170 	clr	a
      003C68 73               [24]10171 	jmp	@a+dptr
      003C69                      10172 00303$:
      003C69 80                   10173 	.db	00181$
      003C6A D1                   10174 	.db	00129$
      003C6B D5                   10175 	.db	00130$
      003C6C 43                   10176 	.db	00176$
      003C6D 80                   10177 	.db	00181$
      003C6E 80                   10178 	.db	00181$
      003C6F 80                   10179 	.db	00181$
      003C70 80                   10180 	.db	00181$
      003C71 80                   10181 	.db	00181$
      003C72 80                   10182 	.db	00181$
      003C73 80                   10183 	.db	00181$
      003C74 80                   10184 	.db	00181$
      003C75 80                   10185 	.db	00181$
      003C76 80                   10186 	.db	00181$
      003C77 80                   10187 	.db	00181$
      003C78 80                   10188 	.db	00181$
      003C79 DF                   10189 	.db	00131$
      003C7A EE                   10190 	.db	00133$
      003C7B DF                   10191 	.db	00132$
      003C7C EE                   10192 	.db	00134$
      003C7D 80                   10193 	.db	00181$
      003C7E 80                   10194 	.db	00181$
      003C7F 80                   10195 	.db	00181$
      003C80 80                   10196 	.db	00181$
      003C81 50                   10197 	.db	00143$
      003C82 50                   10198 	.db	00144$
      003C83 50                   10199 	.db	00145$
      003C84 50                   10200 	.db	00146$
      003C85 50                   10201 	.db	00142$
      003C86 80                   10202 	.db	00181$
      003C87 80                   10203 	.db	00181$
      003C88 80                   10204 	.db	00181$
      003C89 FD                   10205 	.db	00135$
      003C8A 3E                   10206 	.db	00140$
      003C8B FD                   10207 	.db	00136$
      003C8C 3E                   10208 	.db	00141$
      003C8D 80                   10209 	.db	00181$
      003C8E 80                   10210 	.db	00181$
      003C8F 80                   10211 	.db	00181$
      003C90 80                   10212 	.db	00181$
      003C91 DE                   10213 	.db	00160$
      003C92 DE                   10214 	.db	00161$
      003C93 DE                   10215 	.db	00162$
      003C94 DE                   10216 	.db	00163$
      003C95 DE                   10217 	.db	00159$
      003C96 DE                   10218 	.db	00164$
      003C97 80                   10219 	.db	00181$
      003C98 80                   10220 	.db	00181$
      003C99 86                   10221 	.db	00177$
      003C9A 86                   10222 	.db	00178$
      003C9B E1                   10223 	.db	00179$
      003C9C E1                   10224 	.db	00180$
      003C9D                      10225 00304$:
      003C9D 3F                   10226 	.db	00181$>>8
      003C9E 3C                   10227 	.db	00129$>>8
      003C9F 3C                   10228 	.db	00130$>>8
      003CA0 3E                   10229 	.db	00176$>>8
      003CA1 3F                   10230 	.db	00181$>>8
      003CA2 3F                   10231 	.db	00181$>>8
      003CA3 3F                   10232 	.db	00181$>>8
      003CA4 3F                   10233 	.db	00181$>>8
      003CA5 3F                   10234 	.db	00181$>>8
      003CA6 3F                   10235 	.db	00181$>>8
      003CA7 3F                   10236 	.db	00181$>>8
      003CA8 3F                   10237 	.db	00181$>>8
      003CA9 3F                   10238 	.db	00181$>>8
      003CAA 3F                   10239 	.db	00181$>>8
      003CAB 3F                   10240 	.db	00181$>>8
      003CAC 3F                   10241 	.db	00181$>>8
      003CAD 3C                   10242 	.db	00131$>>8
      003CAE 3C                   10243 	.db	00133$>>8
      003CAF 3C                   10244 	.db	00132$>>8
      003CB0 3C                   10245 	.db	00134$>>8
      003CB1 3F                   10246 	.db	00181$>>8
      003CB2 3F                   10247 	.db	00181$>>8
      003CB3 3F                   10248 	.db	00181$>>8
      003CB4 3F                   10249 	.db	00181$>>8
      003CB5 3D                   10250 	.db	00143$>>8
      003CB6 3D                   10251 	.db	00144$>>8
      003CB7 3D                   10252 	.db	00145$>>8
      003CB8 3D                   10253 	.db	00146$>>8
      003CB9 3D                   10254 	.db	00142$>>8
      003CBA 3F                   10255 	.db	00181$>>8
      003CBB 3F                   10256 	.db	00181$>>8
      003CBC 3F                   10257 	.db	00181$>>8
      003CBD 3C                   10258 	.db	00135$>>8
      003CBE 3D                   10259 	.db	00140$>>8
      003CBF 3C                   10260 	.db	00136$>>8
      003CC0 3D                   10261 	.db	00141$>>8
      003CC1 3F                   10262 	.db	00181$>>8
      003CC2 3F                   10263 	.db	00181$>>8
      003CC3 3F                   10264 	.db	00181$>>8
      003CC4 3F                   10265 	.db	00181$>>8
      003CC5 3D                   10266 	.db	00160$>>8
      003CC6 3D                   10267 	.db	00161$>>8
      003CC7 3D                   10268 	.db	00162$>>8
      003CC8 3D                   10269 	.db	00163$>>8
      003CC9 3D                   10270 	.db	00159$>>8
      003CCA 3D                   10271 	.db	00164$>>8
      003CCB 3F                   10272 	.db	00181$>>8
      003CCC 3F                   10273 	.db	00181$>>8
      003CCD 3E                   10274 	.db	00177$>>8
      003CCE 3E                   10275 	.db	00178$>>8
      003CCF 3E                   10276 	.db	00179$>>8
      003CD0 3E                   10277 	.db	00180$>>8
                                  10278 ;	..\src\COMMON\easyax5043.c:1876: case AXRADIO_MODE_OFF:
      003CD1                      10279 00129$:
                                  10280 ;	..\src\COMMON\easyax5043.c:1877: return AXRADIO_ERR_NOERROR;
      003CD1 75 82 00         [24]10281 	mov	dpl,#0x00
      003CD4 22               [24]10282 	ret
                                  10283 ;	..\src\COMMON\easyax5043.c:1879: case AXRADIO_MODE_DEEPSLEEP:
      003CD5                      10284 00130$:
                                  10285 ;	..\src\COMMON\easyax5043.c:1880: ax5043_enter_deepsleep();
      003CD5 12 5D E0         [24]10286 	lcall	_ax5043_enter_deepsleep
                                  10287 ;	..\src\COMMON\easyax5043.c:1881: axradio_mode = AXRADIO_MODE_DEEPSLEEP;
      003CD8 75 0B 02         [24]10288 	mov	_axradio_mode,#0x02
                                  10289 ;	..\src\COMMON\easyax5043.c:1882: return AXRADIO_ERR_NOERROR;
      003CDB 75 82 00         [24]10290 	mov	dpl,#0x00
      003CDE 22               [24]10291 	ret
                                  10292 ;	..\src\COMMON\easyax5043.c:1884: case AXRADIO_MODE_ASYNC_TRANSMIT:
      003CDF                      10293 00131$:
                                  10294 ;	..\src\COMMON\easyax5043.c:1885: case AXRADIO_MODE_ACK_TRANSMIT:
      003CDF                      10295 00132$:
                                  10296 ;	..\src\COMMON\easyax5043.c:1886: axradio_mode = mode;
      003CDF 8F 0B            [24]10297 	mov	_axradio_mode,r7
                                  10298 ;	..\src\COMMON\easyax5043.c:1887: axradio_ack_seqnr = 0xff;
      003CE1 90 00 C2         [24]10299 	mov	dptr,#_axradio_ack_seqnr
      003CE4 74 FF            [12]10300 	mov	a,#0xff
      003CE6 F0               [24]10301 	movx	@dptr,a
                                  10302 ;	..\src\COMMON\easyax5043.c:1888: ax5043_init_registers_tx();
      003CE7 12 16 97         [24]10303 	lcall	_ax5043_init_registers_tx
                                  10304 ;	..\src\COMMON\easyax5043.c:1889: return AXRADIO_ERR_NOERROR;
      003CEA 75 82 00         [24]10305 	mov	dpl,#0x00
      003CED 22               [24]10306 	ret
                                  10307 ;	..\src\COMMON\easyax5043.c:1891: case AXRADIO_MODE_WOR_TRANSMIT:
      003CEE                      10308 00133$:
                                  10309 ;	..\src\COMMON\easyax5043.c:1892: case AXRADIO_MODE_WOR_ACK_TRANSMIT:
      003CEE                      10310 00134$:
                                  10311 ;	..\src\COMMON\easyax5043.c:1893: axradio_mode = mode;
      003CEE 8F 0B            [24]10312 	mov	_axradio_mode,r7
                                  10313 ;	..\src\COMMON\easyax5043.c:1894: axradio_ack_seqnr = 0xff;
      003CF0 90 00 C2         [24]10314 	mov	dptr,#_axradio_ack_seqnr
      003CF3 74 FF            [12]10315 	mov	a,#0xff
      003CF5 F0               [24]10316 	movx	@dptr,a
                                  10317 ;	..\src\COMMON\easyax5043.c:1895: ax5043_init_registers_tx();
      003CF6 12 16 97         [24]10318 	lcall	_ax5043_init_registers_tx
                                  10319 ;	..\src\COMMON\easyax5043.c:1896: return AXRADIO_ERR_NOERROR;
      003CF9 75 82 00         [24]10320 	mov	dpl,#0x00
      003CFC 22               [24]10321 	ret
                                  10322 ;	..\src\COMMON\easyax5043.c:1898: case AXRADIO_MODE_ASYNC_RECEIVE:
      003CFD                      10323 00135$:
                                  10324 ;	..\src\COMMON\easyax5043.c:1899: case AXRADIO_MODE_ACK_RECEIVE:
      003CFD                      10325 00136$:
                                  10326 ;	..\src\COMMON\easyax5043.c:1900: axradio_mode = mode;
      003CFD 8F 0B            [24]10327 	mov	_axradio_mode,r7
                                  10328 ;	..\src\COMMON\easyax5043.c:1901: axradio_ack_seqnr = 0xff;
      003CFF 90 00 C2         [24]10329 	mov	dptr,#_axradio_ack_seqnr
      003D02 74 FF            [12]10330 	mov	a,#0xff
      003D04 F0               [24]10331 	movx	@dptr,a
                                  10332 ;	..\src\COMMON\easyax5043.c:1902: ax5043_init_registers_rx();
      003D05 12 16 9D         [24]10333 	lcall	_ax5043_init_registers_rx
                                  10334 ;	..\src\COMMON\easyax5043.c:1903: ax5043_receiver_on_continuous();
      003D08 12 21 77         [24]10335 	lcall	_ax5043_receiver_on_continuous
                                  10336 ;	..\src\COMMON\easyax5043.c:1904: enablecs:
      003D0B                      10337 00137$:
                                  10338 ;	..\src\COMMON\easyax5043.c:1905: if (axradio_phy_cs_enabled) {
      003D0B 90 7A CB         [24]10339 	mov	dptr,#_axradio_phy_cs_enabled
      003D0E E4               [12]10340 	clr	a
      003D0F 93               [24]10341 	movc	a,@a+dptr
      003D10 60 28            [24]10342 	jz	00139$
                                  10343 ;	..\src\COMMON\easyax5043.c:1906: wtimer_remove(&axradio_timer);
      003D12 90 03 3C         [24]10344 	mov	dptr,#_axradio_timer
      003D15 12 71 84         [24]10345 	lcall	_wtimer_remove
                                  10346 ;	..\src\COMMON\easyax5043.c:1907: axradio_timer.time = axradio_phy_cs_period;
      003D18 90 7A C9         [24]10347 	mov	dptr,#_axradio_phy_cs_period
      003D1B E4               [12]10348 	clr	a
      003D1C 93               [24]10349 	movc	a,@a+dptr
      003D1D FD               [12]10350 	mov	r5,a
      003D1E 74 01            [12]10351 	mov	a,#0x01
      003D20 93               [24]10352 	movc	a,@a+dptr
      003D21 FE               [12]10353 	mov	r6,a
      003D22 7C 00            [12]10354 	mov	r4,#0x00
      003D24 7B 00            [12]10355 	mov	r3,#0x00
      003D26 90 03 40         [24]10356 	mov	dptr,#(_axradio_timer + 0x0004)
      003D29 ED               [12]10357 	mov	a,r5
      003D2A F0               [24]10358 	movx	@dptr,a
      003D2B EE               [12]10359 	mov	a,r6
      003D2C A3               [24]10360 	inc	dptr
      003D2D F0               [24]10361 	movx	@dptr,a
      003D2E EC               [12]10362 	mov	a,r4
      003D2F A3               [24]10363 	inc	dptr
      003D30 F0               [24]10364 	movx	@dptr,a
      003D31 EB               [12]10365 	mov	a,r3
      003D32 A3               [24]10366 	inc	dptr
      003D33 F0               [24]10367 	movx	@dptr,a
                                  10368 ;	..\src\COMMON\easyax5043.c:1908: wtimer0_addrelative(&axradio_timer);
      003D34 90 03 3C         [24]10369 	mov	dptr,#_axradio_timer
      003D37 12 66 25         [24]10370 	lcall	_wtimer0_addrelative
      003D3A                      10371 00139$:
                                  10372 ;	..\src\COMMON\easyax5043.c:1910: return AXRADIO_ERR_NOERROR;
      003D3A 75 82 00         [24]10373 	mov	dpl,#0x00
      003D3D 22               [24]10374 	ret
                                  10375 ;	..\src\COMMON\easyax5043.c:1912: case AXRADIO_MODE_WOR_RECEIVE:
      003D3E                      10376 00140$:
                                  10377 ;	..\src\COMMON\easyax5043.c:1913: case AXRADIO_MODE_WOR_ACK_RECEIVE:
      003D3E                      10378 00141$:
                                  10379 ;	..\src\COMMON\easyax5043.c:1914: axradio_ack_seqnr = 0xff;
      003D3E 90 00 C2         [24]10380 	mov	dptr,#_axradio_ack_seqnr
      003D41 74 FF            [12]10381 	mov	a,#0xff
      003D43 F0               [24]10382 	movx	@dptr,a
                                  10383 ;	..\src\COMMON\easyax5043.c:1915: axradio_mode = mode;
      003D44 8F 0B            [24]10384 	mov	_axradio_mode,r7
                                  10385 ;	..\src\COMMON\easyax5043.c:1916: ax5043_init_registers_rx();
      003D46 12 16 9D         [24]10386 	lcall	_ax5043_init_registers_rx
                                  10387 ;	..\src\COMMON\easyax5043.c:1917: ax5043_receiver_on_wor();
      003D49 12 21 DF         [24]10388 	lcall	_ax5043_receiver_on_wor
                                  10389 ;	..\src\COMMON\easyax5043.c:1918: return AXRADIO_ERR_NOERROR;
      003D4C 75 82 00         [24]10390 	mov	dpl,#0x00
      003D4F 22               [24]10391 	ret
                                  10392 ;	..\src\COMMON\easyax5043.c:1920: case AXRADIO_MODE_STREAM_TRANSMIT:
      003D50                      10393 00142$:
                                  10394 ;	..\src\COMMON\easyax5043.c:1921: case AXRADIO_MODE_STREAM_TRANSMIT_UNENC:
      003D50                      10395 00143$:
                                  10396 ;	..\src\COMMON\easyax5043.c:1922: case AXRADIO_MODE_STREAM_TRANSMIT_SCRAM:
      003D50                      10397 00144$:
                                  10398 ;	..\src\COMMON\easyax5043.c:1923: case AXRADIO_MODE_STREAM_TRANSMIT_UNENC_LSB:
      003D50                      10399 00145$:
                                  10400 ;	..\src\COMMON\easyax5043.c:1924: case AXRADIO_MODE_STREAM_TRANSMIT_SCRAM_LSB:
      003D50                      10401 00146$:
                                  10402 ;	..\src\COMMON\easyax5043.c:1925: axradio_mode = mode;
      003D50 8F 0B            [24]10403 	mov	_axradio_mode,r7
                                  10404 ;	..\src\COMMON\easyax5043.c:1926: if (axradio_mode == AXRADIO_MODE_STREAM_TRANSMIT_UNENC ||
      003D52 74 18            [12]10405 	mov	a,#0x18
      003D54 B5 0B 02         [24]10406 	cjne	a,_axradio_mode,00306$
      003D57 80 05            [24]10407 	sjmp	00147$
      003D59                      10408 00306$:
                                  10409 ;	..\src\COMMON\easyax5043.c:1927: axradio_mode == AXRADIO_MODE_STREAM_TRANSMIT_UNENC_LSB)
      003D59 74 1A            [12]10410 	mov	a,#0x1a
      003D5B B5 0B 05         [24]10411 	cjne	a,_axradio_mode,00148$
      003D5E                      10412 00147$:
                                  10413 ;	..\src\COMMON\easyax5043.c:1928: AX5043_ENCODING = 0;
      003D5E 90 40 11         [24]10414 	mov	dptr,#_AX5043_ENCODING
      003D61 E4               [12]10415 	clr	a
      003D62 F0               [24]10416 	movx	@dptr,a
      003D63                      10417 00148$:
                                  10418 ;	..\src\COMMON\easyax5043.c:1929: if (axradio_mode == AXRADIO_MODE_STREAM_TRANSMIT_SCRAM ||
      003D63 74 19            [12]10419 	mov	a,#0x19
      003D65 B5 0B 02         [24]10420 	cjne	a,_axradio_mode,00309$
      003D68 80 05            [24]10421 	sjmp	00150$
      003D6A                      10422 00309$:
                                  10423 ;	..\src\COMMON\easyax5043.c:1930: axradio_mode == AXRADIO_MODE_STREAM_TRANSMIT_SCRAM_LSB)
      003D6A 74 1B            [12]10424 	mov	a,#0x1b
      003D6C B5 0B 06         [24]10425 	cjne	a,_axradio_mode,00151$
      003D6F                      10426 00150$:
                                  10427 ;	..\src\COMMON\easyax5043.c:1931: AX5043_ENCODING = 4;
      003D6F 90 40 11         [24]10428 	mov	dptr,#_AX5043_ENCODING
      003D72 74 04            [12]10429 	mov	a,#0x04
      003D74 F0               [24]10430 	movx	@dptr,a
      003D75                      10431 00151$:
                                  10432 ;	..\src\COMMON\easyax5043.c:1932: if (axradio_mode == AXRADIO_MODE_STREAM_TRANSMIT_UNENC_LSB ||
      003D75 74 1A            [12]10433 	mov	a,#0x1a
      003D77 B5 0B 02         [24]10434 	cjne	a,_axradio_mode,00312$
      003D7A 80 05            [24]10435 	sjmp	00153$
      003D7C                      10436 00312$:
                                  10437 ;	..\src\COMMON\easyax5043.c:1933: axradio_mode == AXRADIO_MODE_STREAM_TRANSMIT_SCRAM_LSB)
      003D7C 74 1B            [12]10438 	mov	a,#0x1b
      003D7E B5 0B 09         [24]10439 	cjne	a,_axradio_mode,00154$
      003D81                      10440 00153$:
                                  10441 ;	..\src\COMMON\easyax5043.c:1934: AX5043_PKTADDRCFG &= 0x7F;
      003D81 90 42 00         [24]10442 	mov	dptr,#_AX5043_PKTADDRCFG
      003D84 E0               [24]10443 	movx	a,@dptr
      003D85 FE               [12]10444 	mov	r6,a
      003D86 74 7F            [12]10445 	mov	a,#0x7f
      003D88 5E               [12]10446 	anl	a,r6
      003D89 F0               [24]10447 	movx	@dptr,a
      003D8A                      10448 00154$:
                                  10449 ;	..\src\COMMON\easyax5043.c:1935: ax5043_init_registers_tx();
      003D8A 12 16 97         [24]10450 	lcall	_ax5043_init_registers_tx
                                  10451 ;	..\src\COMMON\easyax5043.c:1936: AX5043_FRAMING = 0;
      003D8D 90 40 12         [24]10452 	mov	dptr,#_AX5043_FRAMING
      003D90 E4               [12]10453 	clr	a
      003D91 F0               [24]10454 	movx	@dptr,a
                                  10455 ;	..\src\COMMON\easyax5043.c:1937: ax5043_prepare_tx();
      003D92 12 22 A5         [24]10456 	lcall	_ax5043_prepare_tx
                                  10457 ;	..\src\COMMON\easyax5043.c:1938: axradio_trxstate = trxstate_txstream_xtalwait;
      003D95 75 0C 0F         [24]10458 	mov	_axradio_trxstate,#0x0f
                                  10459 ;	..\src\COMMON\easyax5043.c:1939: while (!(AX5043_POWSTAT & 0x08)) {}; // wait for modem vdd so writing the FIFO is safe
      003D98                      10460 00156$:
      003D98 90 40 03         [24]10461 	mov	dptr,#_AX5043_POWSTAT
      003D9B E0               [24]10462 	movx	a,@dptr
      003D9C FE               [12]10463 	mov	r6,a
      003D9D 30 E3 F8         [24]10464 	jnb	acc.3,00156$
                                  10465 ;	..\src\COMMON\easyax5043.c:1940: AX5043_FIFOSTAT = 3; // clear FIFO data & flags (prevent transmitting anything left over in the FIFO, this has no effect if the FIFO is not powerered, in this case it is reset any way)
      003DA0 90 40 28         [24]10466 	mov	dptr,#_AX5043_FIFOSTAT
      003DA3 74 03            [12]10467 	mov	a,#0x03
      003DA5 F0               [24]10468 	movx	@dptr,a
                                  10469 ;	..\src\COMMON\easyax5043.c:1941: AX5043_RADIOEVENTREQ0; // make sure REVRDONE bit is cleared, so it is a reliable indicator that the packet is out
      003DA6 90 40 0F         [24]10470 	mov	dptr,#_AX5043_RADIOEVENTREQ0
      003DA9 E0               [24]10471 	movx	a,@dptr
                                  10472 ;	..\src\COMMON\easyax5043.c:1942: update_timeanchor();
      003DAA 12 15 BA         [24]10473 	lcall	_update_timeanchor
                                  10474 ;	..\src\COMMON\easyax5043.c:1943: wtimer_remove_callback(&axradio_cb_transmitdata.cb);
      003DAD 90 03 32         [24]10475 	mov	dptr,#_axradio_cb_transmitdata
      003DB0 12 74 C0         [24]10476 	lcall	_wtimer_remove_callback
                                  10477 ;	..\src\COMMON\easyax5043.c:1944: axradio_cb_transmitdata.st.error = AXRADIO_ERR_NOERROR;
      003DB3 90 03 37         [24]10478 	mov	dptr,#(_axradio_cb_transmitdata + 0x0005)
      003DB6 E4               [12]10479 	clr	a
      003DB7 F0               [24]10480 	movx	@dptr,a
                                  10481 ;	..\src\COMMON\easyax5043.c:1945: axradio_cb_transmitdata.st.time.t = axradio_timeanchor.radiotimer;
      003DB8 90 00 CD         [24]10482 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      003DBB E0               [24]10483 	movx	a,@dptr
      003DBC FB               [12]10484 	mov	r3,a
      003DBD A3               [24]10485 	inc	dptr
      003DBE E0               [24]10486 	movx	a,@dptr
      003DBF FC               [12]10487 	mov	r4,a
      003DC0 A3               [24]10488 	inc	dptr
      003DC1 E0               [24]10489 	movx	a,@dptr
      003DC2 FD               [12]10490 	mov	r5,a
      003DC3 A3               [24]10491 	inc	dptr
      003DC4 E0               [24]10492 	movx	a,@dptr
      003DC5 FE               [12]10493 	mov	r6,a
      003DC6 90 03 38         [24]10494 	mov	dptr,#(_axradio_cb_transmitdata + 0x0006)
      003DC9 EB               [12]10495 	mov	a,r3
      003DCA F0               [24]10496 	movx	@dptr,a
      003DCB EC               [12]10497 	mov	a,r4
      003DCC A3               [24]10498 	inc	dptr
      003DCD F0               [24]10499 	movx	@dptr,a
      003DCE ED               [12]10500 	mov	a,r5
      003DCF A3               [24]10501 	inc	dptr
      003DD0 F0               [24]10502 	movx	@dptr,a
      003DD1 EE               [12]10503 	mov	a,r6
      003DD2 A3               [24]10504 	inc	dptr
      003DD3 F0               [24]10505 	movx	@dptr,a
                                  10506 ;	..\src\COMMON\easyax5043.c:1946: wtimer_add_callback(&axradio_cb_transmitdata.cb);
      003DD4 90 03 32         [24]10507 	mov	dptr,#_axradio_cb_transmitdata
      003DD7 12 66 0B         [24]10508 	lcall	_wtimer_add_callback
                                  10509 ;	..\src\COMMON\easyax5043.c:1947: return AXRADIO_ERR_NOERROR;
      003DDA 75 82 00         [24]10510 	mov	dpl,#0x00
      003DDD 22               [24]10511 	ret
                                  10512 ;	..\src\COMMON\easyax5043.c:1949: case AXRADIO_MODE_STREAM_RECEIVE:
      003DDE                      10513 00159$:
                                  10514 ;	..\src\COMMON\easyax5043.c:1950: case AXRADIO_MODE_STREAM_RECEIVE_UNENC:
      003DDE                      10515 00160$:
                                  10516 ;	..\src\COMMON\easyax5043.c:1951: case AXRADIO_MODE_STREAM_RECEIVE_SCRAM:
      003DDE                      10517 00161$:
                                  10518 ;	..\src\COMMON\easyax5043.c:1952: case AXRADIO_MODE_STREAM_RECEIVE_UNENC_LSB:
      003DDE                      10519 00162$:
                                  10520 ;	..\src\COMMON\easyax5043.c:1953: case AXRADIO_MODE_STREAM_RECEIVE_SCRAM_LSB:
      003DDE                      10521 00163$:
                                  10522 ;	..\src\COMMON\easyax5043.c:1954: case AXRADIO_MODE_STREAM_RECEIVE_DATAPIN:
      003DDE                      10523 00164$:
                                  10524 ;	..\src\COMMON\easyax5043.c:1955: axradio_mode = mode;
      003DDE 8F 0B            [24]10525 	mov	_axradio_mode,r7
                                  10526 ;	..\src\COMMON\easyax5043.c:1956: ax5043_init_registers_rx();
      003DE0 12 16 9D         [24]10527 	lcall	_ax5043_init_registers_rx
                                  10528 ;	..\src\COMMON\easyax5043.c:1957: if (axradio_mode == AXRADIO_MODE_STREAM_RECEIVE_UNENC ||
      003DE3 74 28            [12]10529 	mov	a,#0x28
      003DE5 B5 0B 02         [24]10530 	cjne	a,_axradio_mode,00316$
      003DE8 80 05            [24]10531 	sjmp	00165$
      003DEA                      10532 00316$:
                                  10533 ;	..\src\COMMON\easyax5043.c:1958: axradio_mode == AXRADIO_MODE_STREAM_RECEIVE_UNENC_LSB)
      003DEA 74 2A            [12]10534 	mov	a,#0x2a
      003DEC B5 0B 05         [24]10535 	cjne	a,_axradio_mode,00166$
      003DEF                      10536 00165$:
                                  10537 ;	..\src\COMMON\easyax5043.c:1959: AX5043_ENCODING = 0;
      003DEF 90 40 11         [24]10538 	mov	dptr,#_AX5043_ENCODING
      003DF2 E4               [12]10539 	clr	a
      003DF3 F0               [24]10540 	movx	@dptr,a
      003DF4                      10541 00166$:
                                  10542 ;	..\src\COMMON\easyax5043.c:1960: if (axradio_mode == AXRADIO_MODE_STREAM_RECEIVE_SCRAM ||
      003DF4 74 29            [12]10543 	mov	a,#0x29
      003DF6 B5 0B 02         [24]10544 	cjne	a,_axradio_mode,00319$
      003DF9 80 05            [24]10545 	sjmp	00168$
      003DFB                      10546 00319$:
                                  10547 ;	..\src\COMMON\easyax5043.c:1961: axradio_mode == AXRADIO_MODE_STREAM_RECEIVE_SCRAM_LSB)
      003DFB 74 2B            [12]10548 	mov	a,#0x2b
      003DFD B5 0B 06         [24]10549 	cjne	a,_axradio_mode,00169$
      003E00                      10550 00168$:
                                  10551 ;	..\src\COMMON\easyax5043.c:1962: AX5043_ENCODING = 4;
      003E00 90 40 11         [24]10552 	mov	dptr,#_AX5043_ENCODING
      003E03 74 04            [12]10553 	mov	a,#0x04
      003E05 F0               [24]10554 	movx	@dptr,a
      003E06                      10555 00169$:
                                  10556 ;	..\src\COMMON\easyax5043.c:1963: if (axradio_mode == AXRADIO_MODE_STREAM_RECEIVE_UNENC_LSB ||
      003E06 74 2A            [12]10557 	mov	a,#0x2a
      003E08 B5 0B 02         [24]10558 	cjne	a,_axradio_mode,00322$
      003E0B 80 05            [24]10559 	sjmp	00171$
      003E0D                      10560 00322$:
                                  10561 ;	..\src\COMMON\easyax5043.c:1964: axradio_mode == AXRADIO_MODE_STREAM_RECEIVE_SCRAM_LSB)
      003E0D 74 2B            [12]10562 	mov	a,#0x2b
      003E0F B5 0B 09         [24]10563 	cjne	a,_axradio_mode,00172$
      003E12                      10564 00171$:
                                  10565 ;	..\src\COMMON\easyax5043.c:1965: AX5043_PKTADDRCFG &= 0x7F;
      003E12 90 42 00         [24]10566 	mov	dptr,#_AX5043_PKTADDRCFG
      003E15 E0               [24]10567 	movx	a,@dptr
      003E16 FE               [12]10568 	mov	r6,a
      003E17 74 7F            [12]10569 	mov	a,#0x7f
      003E19 5E               [12]10570 	anl	a,r6
      003E1A F0               [24]10571 	movx	@dptr,a
      003E1B                      10572 00172$:
                                  10573 ;	..\src\COMMON\easyax5043.c:1966: AX5043_FRAMING = 0;
      003E1B 90 40 12         [24]10574 	mov	dptr,#_AX5043_FRAMING
      003E1E E4               [12]10575 	clr	a
      003E1F F0               [24]10576 	movx	@dptr,a
                                  10577 ;	..\src\COMMON\easyax5043.c:1967: AX5043_PKTCHUNKSIZE = 8; // 64 byte
      003E20 90 42 30         [24]10578 	mov	dptr,#_AX5043_PKTCHUNKSIZE
      003E23 74 08            [12]10579 	mov	a,#0x08
      003E25 F0               [24]10580 	movx	@dptr,a
                                  10581 ;	..\src\COMMON\easyax5043.c:1968: AX5043_RXPARAMSETS = 0x00;
      003E26 90 41 17         [24]10582 	mov	dptr,#_AX5043_RXPARAMSETS
      003E29 E4               [12]10583 	clr	a
      003E2A F0               [24]10584 	movx	@dptr,a
                                  10585 ;	..\src\COMMON\easyax5043.c:1969: if( axradio_mode == AXRADIO_MODE_STREAM_RECEIVE_DATAPIN )
      003E2B 74 2D            [12]10586 	mov	a,#0x2d
      003E2D B5 0B 0D         [24]10587 	cjne	a,_axradio_mode,00175$
                                  10588 ;	..\src\COMMON\easyax5043.c:1971: ax5043_set_registers_rxcont_singleparamset();
      003E30 12 06 BF         [24]10589 	lcall	_ax5043_set_registers_rxcont_singleparamset
                                  10590 ;	..\src\COMMON\easyax5043.c:1972: AX5043_PINFUNCDATA = 0x04;
      003E33 90 40 23         [24]10591 	mov	dptr,#_AX5043_PINFUNCDATA
      003E36 74 04            [12]10592 	mov	a,#0x04
      003E38 F0               [24]10593 	movx	@dptr,a
                                  10594 ;	..\src\COMMON\easyax5043.c:1973: AX5043_PINFUNCDCLK = 0x04;
      003E39 90 40 22         [24]10595 	mov	dptr,#_AX5043_PINFUNCDCLK
      003E3C F0               [24]10596 	movx	@dptr,a
      003E3D                      10597 00175$:
                                  10598 ;	..\src\COMMON\easyax5043.c:1975: ax5043_receiver_on_continuous();
      003E3D 12 21 77         [24]10599 	lcall	_ax5043_receiver_on_continuous
                                  10600 ;	..\src\COMMON\easyax5043.c:1976: goto enablecs;
      003E40 02 3D 0B         [24]10601 	ljmp	00137$
                                  10602 ;	..\src\COMMON\easyax5043.c:1978: case AXRADIO_MODE_CW_TRANSMIT:
      003E43                      10603 00176$:
                                  10604 ;	..\src\COMMON\easyax5043.c:1979: axradio_mode = AXRADIO_MODE_CW_TRANSMIT;
      003E43 75 0B 03         [24]10605 	mov	_axradio_mode,#0x03
                                  10606 ;	..\src\COMMON\easyax5043.c:1980: ax5043_init_registers_tx();
      003E46 12 16 97         [24]10607 	lcall	_ax5043_init_registers_tx
                                  10608 ;	..\src\COMMON\easyax5043.c:1981: AX5043_MODULATION = 8;   // Set an FSK mode
      003E49 90 40 10         [24]10609 	mov	dptr,#_AX5043_MODULATION
      003E4C 74 08            [12]10610 	mov	a,#0x08
      003E4E F0               [24]10611 	movx	@dptr,a
                                  10612 ;	..\src\COMMON\easyax5043.c:1982: AX5043_FSKDEV2 = 0x00;
      003E4F 90 41 61         [24]10613 	mov	dptr,#_AX5043_FSKDEV2
      003E52 E4               [12]10614 	clr	a
      003E53 F0               [24]10615 	movx	@dptr,a
                                  10616 ;	..\src\COMMON\easyax5043.c:1983: AX5043_FSKDEV1 = 0x00;
      003E54 90 41 62         [24]10617 	mov	dptr,#_AX5043_FSKDEV1
      003E57 F0               [24]10618 	movx	@dptr,a
                                  10619 ;	..\src\COMMON\easyax5043.c:1984: AX5043_FSKDEV0 = 0x00;
      003E58 90 41 63         [24]10620 	mov	dptr,#_AX5043_FSKDEV0
      003E5B F0               [24]10621 	movx	@dptr,a
                                  10622 ;	..\src\COMMON\easyax5043.c:1985: AX5043_TXRATE2 = 0x00;
      003E5C 90 41 65         [24]10623 	mov	dptr,#_AX5043_TXRATE2
      003E5F F0               [24]10624 	movx	@dptr,a
                                  10625 ;	..\src\COMMON\easyax5043.c:1986: AX5043_TXRATE1 = 0x00;
      003E60 90 41 66         [24]10626 	mov	dptr,#_AX5043_TXRATE1
      003E63 F0               [24]10627 	movx	@dptr,a
                                  10628 ;	..\src\COMMON\easyax5043.c:1987: AX5043_TXRATE0 = 0x01;
      003E64 90 41 67         [24]10629 	mov	dptr,#_AX5043_TXRATE0
      003E67 04               [12]10630 	inc	a
      003E68 F0               [24]10631 	movx	@dptr,a
                                  10632 ;	..\src\COMMON\easyax5043.c:1988: AX5043_PINFUNCDATA = 0x04;
      003E69 90 40 23         [24]10633 	mov	dptr,#_AX5043_PINFUNCDATA
      003E6C 74 04            [12]10634 	mov	a,#0x04
      003E6E F0               [24]10635 	movx	@dptr,a
                                  10636 ;	..\src\COMMON\easyax5043.c:1989: AX5043_PWRMODE = AX5043_PWRSTATE_FIFO_ON;
      003E6F 90 40 02         [24]10637 	mov	dptr,#_AX5043_PWRMODE
      003E72 74 07            [12]10638 	mov	a,#0x07
      003E74 F0               [24]10639 	movx	@dptr,a
                                  10640 ;	..\src\COMMON\easyax5043.c:1990: axradio_trxstate = trxstate_txcw_xtalwait;
      003E75 75 0C 0E         [24]10641 	mov	_axradio_trxstate,#0x0e
                                  10642 ;	..\src\COMMON\easyax5043.c:1991: AX5043_IRQMASK0 = 0x00;
      003E78 90 40 07         [24]10643 	mov	dptr,#_AX5043_IRQMASK0
      003E7B E4               [12]10644 	clr	a
      003E7C F0               [24]10645 	movx	@dptr,a
                                  10646 ;	..\src\COMMON\easyax5043.c:1992: AX5043_IRQMASK1 = 0x01; // enable xtal ready interrupt
      003E7D 90 40 06         [24]10647 	mov	dptr,#_AX5043_IRQMASK1
      003E80 04               [12]10648 	inc	a
      003E81 F0               [24]10649 	movx	@dptr,a
                                  10650 ;	..\src\COMMON\easyax5043.c:1993: return AXRADIO_ERR_NOERROR;
      003E82 75 82 00         [24]10651 	mov	dpl,#0x00
      003E85 22               [24]10652 	ret
                                  10653 ;	..\src\COMMON\easyax5043.c:1995: case AXRADIO_MODE_SYNC_MASTER:
      003E86                      10654 00177$:
                                  10655 ;	..\src\COMMON\easyax5043.c:1996: case AXRADIO_MODE_SYNC_ACK_MASTER:
      003E86                      10656 00178$:
                                  10657 ;	..\src\COMMON\easyax5043.c:1997: axradio_mode = mode;
      003E86 8F 0B            [24]10658 	mov	_axradio_mode,r7
                                  10659 ;	..\src\COMMON\easyax5043.c:1998: axradio_syncstate = syncstate_master_normal;
      003E88 90 00 B7         [24]10660 	mov	dptr,#_axradio_syncstate
      003E8B 74 03            [12]10661 	mov	a,#0x03
      003E8D F0               [24]10662 	movx	@dptr,a
                                  10663 ;	..\src\COMMON\easyax5043.c:2000: wtimer_remove(&axradio_timer);
      003E8E 90 03 3C         [24]10664 	mov	dptr,#_axradio_timer
      003E91 12 71 84         [24]10665 	lcall	_wtimer_remove
                                  10666 ;	..\src\COMMON\easyax5043.c:2001: axradio_timer.time = 2;
      003E94 90 03 40         [24]10667 	mov	dptr,#(_axradio_timer + 0x0004)
      003E97 74 02            [12]10668 	mov	a,#0x02
      003E99 F0               [24]10669 	movx	@dptr,a
      003E9A E4               [12]10670 	clr	a
      003E9B A3               [24]10671 	inc	dptr
      003E9C F0               [24]10672 	movx	@dptr,a
      003E9D A3               [24]10673 	inc	dptr
      003E9E F0               [24]10674 	movx	@dptr,a
      003E9F A3               [24]10675 	inc	dptr
      003EA0 F0               [24]10676 	movx	@dptr,a
                                  10677 ;	..\src\COMMON\easyax5043.c:2002: wtimer0_addrelative(&axradio_timer);
      003EA1 90 03 3C         [24]10678 	mov	dptr,#_axradio_timer
      003EA4 12 66 25         [24]10679 	lcall	_wtimer0_addrelative
                                  10680 ;	..\src\COMMON\easyax5043.c:2003: axradio_sync_time = axradio_timer.time;
      003EA7 90 03 40         [24]10681 	mov	dptr,#(_axradio_timer + 0x0004)
      003EAA E0               [24]10682 	movx	a,@dptr
      003EAB FB               [12]10683 	mov	r3,a
      003EAC A3               [24]10684 	inc	dptr
      003EAD E0               [24]10685 	movx	a,@dptr
      003EAE FC               [12]10686 	mov	r4,a
      003EAF A3               [24]10687 	inc	dptr
      003EB0 E0               [24]10688 	movx	a,@dptr
      003EB1 FD               [12]10689 	mov	r5,a
      003EB2 A3               [24]10690 	inc	dptr
      003EB3 E0               [24]10691 	movx	a,@dptr
      003EB4 FE               [12]10692 	mov	r6,a
      003EB5 90 00 C3         [24]10693 	mov	dptr,#_axradio_sync_time
      003EB8 EB               [12]10694 	mov	a,r3
      003EB9 F0               [24]10695 	movx	@dptr,a
      003EBA EC               [12]10696 	mov	a,r4
      003EBB A3               [24]10697 	inc	dptr
      003EBC F0               [24]10698 	movx	@dptr,a
      003EBD ED               [12]10699 	mov	a,r5
      003EBE A3               [24]10700 	inc	dptr
      003EBF F0               [24]10701 	movx	@dptr,a
      003EC0 EE               [12]10702 	mov	a,r6
      003EC1 A3               [24]10703 	inc	dptr
      003EC2 F0               [24]10704 	movx	@dptr,a
                                  10705 ;	..\src\COMMON\easyax5043.c:2004: axradio_sync_addtime(axradio_sync_xoscstartup);
      003EC3 90 7A FA         [24]10706 	mov	dptr,#_axradio_sync_xoscstartup
      003EC6 E4               [12]10707 	clr	a
      003EC7 93               [24]10708 	movc	a,@a+dptr
      003EC8 FB               [12]10709 	mov	r3,a
      003EC9 74 01            [12]10710 	mov	a,#0x01
      003ECB 93               [24]10711 	movc	a,@a+dptr
      003ECC FC               [12]10712 	mov	r4,a
      003ECD 74 02            [12]10713 	mov	a,#0x02
      003ECF 93               [24]10714 	movc	a,@a+dptr
      003ED0 FD               [12]10715 	mov	r5,a
      003ED1 74 03            [12]10716 	mov	a,#0x03
      003ED3 93               [24]10717 	movc	a,@a+dptr
      003ED4 8B 82            [24]10718 	mov	dpl,r3
      003ED6 8C 83            [24]10719 	mov	dph,r4
      003ED8 8D F0            [24]10720 	mov	b,r5
      003EDA 12 24 70         [24]10721 	lcall	_axradio_sync_addtime
                                  10722 ;	..\src\COMMON\easyax5043.c:2005: return AXRADIO_ERR_NOERROR;
      003EDD 75 82 00         [24]10723 	mov	dpl,#0x00
      003EE0 22               [24]10724 	ret
                                  10725 ;	..\src\COMMON\easyax5043.c:2007: case AXRADIO_MODE_SYNC_SLAVE:
      003EE1                      10726 00179$:
                                  10727 ;	..\src\COMMON\easyax5043.c:2008: case AXRADIO_MODE_SYNC_ACK_SLAVE:
      003EE1                      10728 00180$:
                                  10729 ;	..\src\COMMON\easyax5043.c:2009: axradio_mode = mode;
      003EE1 8F 0B            [24]10730 	mov	_axradio_mode,r7
                                  10731 ;	..\src\COMMON\easyax5043.c:2010: ax5043_init_registers_rx();
      003EE3 12 16 9D         [24]10732 	lcall	_ax5043_init_registers_rx
                                  10733 ;	..\src\COMMON\easyax5043.c:2011: ax5043_receiver_on_continuous();
      003EE6 12 21 77         [24]10734 	lcall	_ax5043_receiver_on_continuous
                                  10735 ;	..\src\COMMON\easyax5043.c:2012: axradio_syncstate = syncstate_slave_synchunt;
      003EE9 90 00 B7         [24]10736 	mov	dptr,#_axradio_syncstate
      003EEC 74 06            [12]10737 	mov	a,#0x06
      003EEE F0               [24]10738 	movx	@dptr,a
                                  10739 ;	..\src\COMMON\easyax5043.c:2013: wtimer_remove(&axradio_timer);
      003EEF 90 03 3C         [24]10740 	mov	dptr,#_axradio_timer
      003EF2 12 71 84         [24]10741 	lcall	_wtimer_remove
                                  10742 ;	..\src\COMMON\easyax5043.c:2014: axradio_timer.time = axradio_sync_slave_initialsyncwindow;
      003EF5 90 7B 02         [24]10743 	mov	dptr,#_axradio_sync_slave_initialsyncwindow
      003EF8 E4               [12]10744 	clr	a
      003EF9 93               [24]10745 	movc	a,@a+dptr
      003EFA FC               [12]10746 	mov	r4,a
      003EFB 74 01            [12]10747 	mov	a,#0x01
      003EFD 93               [24]10748 	movc	a,@a+dptr
      003EFE FD               [12]10749 	mov	r5,a
      003EFF 74 02            [12]10750 	mov	a,#0x02
      003F01 93               [24]10751 	movc	a,@a+dptr
      003F02 FE               [12]10752 	mov	r6,a
      003F03 74 03            [12]10753 	mov	a,#0x03
      003F05 93               [24]10754 	movc	a,@a+dptr
      003F06 FF               [12]10755 	mov	r7,a
      003F07 90 03 40         [24]10756 	mov	dptr,#(_axradio_timer + 0x0004)
      003F0A EC               [12]10757 	mov	a,r4
      003F0B F0               [24]10758 	movx	@dptr,a
      003F0C ED               [12]10759 	mov	a,r5
      003F0D A3               [24]10760 	inc	dptr
      003F0E F0               [24]10761 	movx	@dptr,a
      003F0F EE               [12]10762 	mov	a,r6
      003F10 A3               [24]10763 	inc	dptr
      003F11 F0               [24]10764 	movx	@dptr,a
      003F12 EF               [12]10765 	mov	a,r7
      003F13 A3               [24]10766 	inc	dptr
      003F14 F0               [24]10767 	movx	@dptr,a
                                  10768 ;	..\src\COMMON\easyax5043.c:2015: wtimer0_addrelative(&axradio_timer);
      003F15 90 03 3C         [24]10769 	mov	dptr,#_axradio_timer
      003F18 12 66 25         [24]10770 	lcall	_wtimer0_addrelative
                                  10771 ;	..\src\COMMON\easyax5043.c:2016: axradio_sync_time = axradio_timer.time;
      003F1B 90 03 40         [24]10772 	mov	dptr,#(_axradio_timer + 0x0004)
      003F1E E0               [24]10773 	movx	a,@dptr
      003F1F FC               [12]10774 	mov	r4,a
      003F20 A3               [24]10775 	inc	dptr
      003F21 E0               [24]10776 	movx	a,@dptr
      003F22 FD               [12]10777 	mov	r5,a
      003F23 A3               [24]10778 	inc	dptr
      003F24 E0               [24]10779 	movx	a,@dptr
      003F25 FE               [12]10780 	mov	r6,a
      003F26 A3               [24]10781 	inc	dptr
      003F27 E0               [24]10782 	movx	a,@dptr
      003F28 FF               [12]10783 	mov	r7,a
      003F29 90 00 C3         [24]10784 	mov	dptr,#_axradio_sync_time
      003F2C EC               [12]10785 	mov	a,r4
      003F2D F0               [24]10786 	movx	@dptr,a
      003F2E ED               [12]10787 	mov	a,r5
      003F2F A3               [24]10788 	inc	dptr
      003F30 F0               [24]10789 	movx	@dptr,a
      003F31 EE               [12]10790 	mov	a,r6
      003F32 A3               [24]10791 	inc	dptr
      003F33 F0               [24]10792 	movx	@dptr,a
      003F34 EF               [12]10793 	mov	a,r7
      003F35 A3               [24]10794 	inc	dptr
      003F36 F0               [24]10795 	movx	@dptr,a
                                  10796 ;	..\src\COMMON\easyax5043.c:2017: wtimer_remove_callback(&axradio_cb_receive.cb);
      003F37 90 02 E5         [24]10797 	mov	dptr,#_axradio_cb_receive
      003F3A 12 74 C0         [24]10798 	lcall	_wtimer_remove_callback
                                  10799 ;	..\src\COMMON\easyax5043.c:2018: memset_xdata(&axradio_cb_receive.st, 0, sizeof(axradio_cb_receive.st));
      003F3D 90 03 CD         [24]10800 	mov	dptr,#_memset_PARM_2
      003F40 E4               [12]10801 	clr	a
      003F41 F0               [24]10802 	movx	@dptr,a
      003F42 90 03 CE         [24]10803 	mov	dptr,#_memset_PARM_3
      003F45 74 1E            [12]10804 	mov	a,#0x1e
      003F47 F0               [24]10805 	movx	@dptr,a
      003F48 E4               [12]10806 	clr	a
      003F49 A3               [24]10807 	inc	dptr
      003F4A F0               [24]10808 	movx	@dptr,a
      003F4B 90 02 E9         [24]10809 	mov	dptr,#(_axradio_cb_receive + 0x0004)
      003F4E 75 F0 00         [24]10810 	mov	b,#0x00
      003F51 12 62 A1         [24]10811 	lcall	_memset
                                  10812 ;	..\src\COMMON\easyax5043.c:2019: axradio_cb_receive.st.time.t = axradio_timeanchor.radiotimer;
      003F54 90 00 CD         [24]10813 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      003F57 E0               [24]10814 	movx	a,@dptr
      003F58 FC               [12]10815 	mov	r4,a
      003F59 A3               [24]10816 	inc	dptr
      003F5A E0               [24]10817 	movx	a,@dptr
      003F5B FD               [12]10818 	mov	r5,a
      003F5C A3               [24]10819 	inc	dptr
      003F5D E0               [24]10820 	movx	a,@dptr
      003F5E FE               [12]10821 	mov	r6,a
      003F5F A3               [24]10822 	inc	dptr
      003F60 E0               [24]10823 	movx	a,@dptr
      003F61 FF               [12]10824 	mov	r7,a
      003F62 90 02 EB         [24]10825 	mov	dptr,#(_axradio_cb_receive + 0x0006)
      003F65 EC               [12]10826 	mov	a,r4
      003F66 F0               [24]10827 	movx	@dptr,a
      003F67 ED               [12]10828 	mov	a,r5
      003F68 A3               [24]10829 	inc	dptr
      003F69 F0               [24]10830 	movx	@dptr,a
      003F6A EE               [12]10831 	mov	a,r6
      003F6B A3               [24]10832 	inc	dptr
      003F6C F0               [24]10833 	movx	@dptr,a
      003F6D EF               [12]10834 	mov	a,r7
      003F6E A3               [24]10835 	inc	dptr
      003F6F F0               [24]10836 	movx	@dptr,a
                                  10837 ;	..\src\COMMON\easyax5043.c:2020: axradio_cb_receive.st.error = AXRADIO_ERR_RESYNC;
      003F70 90 02 EA         [24]10838 	mov	dptr,#(_axradio_cb_receive + 0x0005)
      003F73 74 09            [12]10839 	mov	a,#0x09
      003F75 F0               [24]10840 	movx	@dptr,a
                                  10841 ;	..\src\COMMON\easyax5043.c:2021: wtimer_add_callback(&axradio_cb_receive.cb);
      003F76 90 02 E5         [24]10842 	mov	dptr,#_axradio_cb_receive
      003F79 12 66 0B         [24]10843 	lcall	_wtimer_add_callback
                                  10844 ;	..\src\COMMON\easyax5043.c:2022: return AXRADIO_ERR_NOERROR;
      003F7C 75 82 00         [24]10845 	mov	dpl,#0x00
                                  10846 ;	..\src\COMMON\easyax5043.c:2024: default:
      003F7F 22               [24]10847 	ret
      003F80                      10848 00181$:
                                  10849 ;	..\src\COMMON\easyax5043.c:2025: return AXRADIO_ERR_NOTSUPPORTED;
      003F80 75 82 01         [24]10850 	mov	dpl,#0x01
                                  10851 ;	..\src\COMMON\easyax5043.c:2026: }
      003F83 22               [24]10852 	ret
                                  10853 ;------------------------------------------------------------
                                  10854 ;Allocation info for local variables in function 'axradio_set_channel'
                                  10855 ;------------------------------------------------------------
                                  10856 ;chnum                     Allocated with name '_axradio_set_channel_chnum_1_421'
                                  10857 ;rng                       Allocated with name '_axradio_set_channel_rng_1_422'
                                  10858 ;f                         Allocated to registers r3 r4 r5 r7 
                                  10859 ;------------------------------------------------------------
                                  10860 ;	..\src\COMMON\easyax5043.c:2029: uint8_t axradio_set_channel(uint8_t chnum)
                                  10861 ;	-----------------------------------------
                                  10862 ;	 function axradio_set_channel
                                  10863 ;	-----------------------------------------
      003F84                      10864 _axradio_set_channel:
      003F84 E5 82            [12]10865 	mov	a,dpl
      003F86 90 03 65         [24]10866 	mov	dptr,#_axradio_set_channel_chnum_1_421
      003F89 F0               [24]10867 	movx	@dptr,a
                                  10868 ;	..\src\COMMON\easyax5043.c:2032: if (chnum >= axradio_phy_nrchannels)
      003F8A E0               [24]10869 	movx	a,@dptr
      003F8B FF               [12]10870 	mov	r7,a
      003F8C 90 7A BA         [24]10871 	mov	dptr,#_axradio_phy_nrchannels
      003F8F E4               [12]10872 	clr	a
      003F90 93               [24]10873 	movc	a,@a+dptr
      003F91 FE               [12]10874 	mov	r6,a
      003F92 C3               [12]10875 	clr	c
      003F93 EF               [12]10876 	mov	a,r7
      003F94 9E               [12]10877 	subb	a,r6
      003F95 40 04            [24]10878 	jc	00102$
                                  10879 ;	..\src\COMMON\easyax5043.c:2033: return AXRADIO_ERR_INVALID;
      003F97 75 82 04         [24]10880 	mov	dpl,#0x04
      003F9A 22               [24]10881 	ret
      003F9B                      10882 00102$:
                                  10883 ;	..\src\COMMON\easyax5043.c:2034: axradio_curchannel = chnum;
      003F9B 90 00 BC         [24]10884 	mov	dptr,#_axradio_curchannel
      003F9E EF               [12]10885 	mov	a,r7
      003F9F F0               [24]10886 	movx	@dptr,a
                                  10887 ;	..\src\COMMON\easyax5043.c:2035: rng = axradio_phy_chanpllrng[chnum];
      003FA0 EF               [12]10888 	mov	a,r7
      003FA1 24 09            [12]10889 	add	a,#_axradio_phy_chanpllrng
      003FA3 F5 82            [12]10890 	mov	dpl,a
      003FA5 E4               [12]10891 	clr	a
      003FA6 34 00            [12]10892 	addc	a,#(_axradio_phy_chanpllrng >> 8)
      003FA8 F5 83            [12]10893 	mov	dph,a
      003FAA E0               [24]10894 	movx	a,@dptr
                                  10895 ;	..\src\COMMON\easyax5043.c:2036: if (rng & 0x20)
      003FAB F5 43            [12]10896 	mov	_axradio_set_channel_rng_1_422,a
      003FAD 30 E5 04         [24]10897 	jnb	acc.5,00104$
                                  10898 ;	..\src\COMMON\easyax5043.c:2037: return AXRADIO_ERR_RANGING;
      003FB0 75 82 06         [24]10899 	mov	dpl,#0x06
      003FB3 22               [24]10900 	ret
      003FB4                      10901 00104$:
                                  10902 ;	..\src\COMMON\easyax5043.c:2039: uint32_t __autodata f = axradio_phy_chanfreq[chnum];
      003FB4 EF               [12]10903 	mov	a,r7
      003FB5 75 F0 04         [24]10904 	mov	b,#0x04
      003FB8 A4               [48]10905 	mul	ab
      003FB9 24 BB            [12]10906 	add	a,#_axradio_phy_chanfreq
      003FBB F5 82            [12]10907 	mov	dpl,a
      003FBD 74 7A            [12]10908 	mov	a,#(_axradio_phy_chanfreq >> 8)
      003FBF 35 F0            [12]10909 	addc	a,b
      003FC1 F5 83            [12]10910 	mov	dph,a
      003FC3 E4               [12]10911 	clr	a
      003FC4 93               [24]10912 	movc	a,@a+dptr
      003FC5 FB               [12]10913 	mov	r3,a
      003FC6 A3               [24]10914 	inc	dptr
      003FC7 E4               [12]10915 	clr	a
      003FC8 93               [24]10916 	movc	a,@a+dptr
      003FC9 FC               [12]10917 	mov	r4,a
      003FCA A3               [24]10918 	inc	dptr
      003FCB E4               [12]10919 	clr	a
      003FCC 93               [24]10920 	movc	a,@a+dptr
      003FCD FD               [12]10921 	mov	r5,a
      003FCE A3               [24]10922 	inc	dptr
      003FCF E4               [12]10923 	clr	a
      003FD0 93               [24]10924 	movc	a,@a+dptr
      003FD1 FF               [12]10925 	mov	r7,a
                                  10926 ;	..\src\COMMON\easyax5043.c:2040: f += axradio_curfreqoffset;
      003FD2 90 00 BD         [24]10927 	mov	dptr,#_axradio_curfreqoffset
      003FD5 E0               [24]10928 	movx	a,@dptr
      003FD6 F8               [12]10929 	mov	r0,a
      003FD7 A3               [24]10930 	inc	dptr
      003FD8 E0               [24]10931 	movx	a,@dptr
      003FD9 F9               [12]10932 	mov	r1,a
      003FDA A3               [24]10933 	inc	dptr
      003FDB E0               [24]10934 	movx	a,@dptr
      003FDC FA               [12]10935 	mov	r2,a
      003FDD A3               [24]10936 	inc	dptr
      003FDE E0               [24]10937 	movx	a,@dptr
      003FDF FE               [12]10938 	mov	r6,a
      003FE0 E8               [12]10939 	mov	a,r0
      003FE1 2B               [12]10940 	add	a,r3
      003FE2 FB               [12]10941 	mov	r3,a
      003FE3 E9               [12]10942 	mov	a,r1
      003FE4 3C               [12]10943 	addc	a,r4
      003FE5 FC               [12]10944 	mov	r4,a
      003FE6 EA               [12]10945 	mov	a,r2
      003FE7 3D               [12]10946 	addc	a,r5
      003FE8 FD               [12]10947 	mov	r5,a
      003FE9 EE               [12]10948 	mov	a,r6
      003FEA 3F               [12]10949 	addc	a,r7
      003FEB FF               [12]10950 	mov	r7,a
                                  10951 ;	..\src\COMMON\easyax5043.c:2041: if (AX5043_PLLLOOP & 0x80) {
      003FEC 90 40 30         [24]10952 	mov	dptr,#_AX5043_PLLLOOP
      003FEF E0               [24]10953 	movx	a,@dptr
      003FF0 FE               [12]10954 	mov	r6,a
      003FF1 30 E7 1E         [24]10955 	jnb	acc.7,00106$
                                  10956 ;	..\src\COMMON\easyax5043.c:2042: AX5043_PLLRANGINGA = rng & 0x0F;
      003FF4 90 40 33         [24]10957 	mov	dptr,#_AX5043_PLLRANGINGA
      003FF7 74 0F            [12]10958 	mov	a,#0x0f
      003FF9 55 43            [12]10959 	anl	a,_axradio_set_channel_rng_1_422
      003FFB F0               [24]10960 	movx	@dptr,a
                                  10961 ;	..\src\COMMON\easyax5043.c:2043: AX5043_FREQA0 = f;
      003FFC 90 40 37         [24]10962 	mov	dptr,#_AX5043_FREQA0
      003FFF EB               [12]10963 	mov	a,r3
      004000 F0               [24]10964 	movx	@dptr,a
                                  10965 ;	..\src\COMMON\easyax5043.c:2044: AX5043_FREQA1 = f >> 8;
      004001 90 40 36         [24]10966 	mov	dptr,#_AX5043_FREQA1
      004004 EC               [12]10967 	mov	a,r4
      004005 F0               [24]10968 	movx	@dptr,a
                                  10969 ;	..\src\COMMON\easyax5043.c:2045: AX5043_FREQA2 = f >> 16;
      004006 90 40 35         [24]10970 	mov	dptr,#_AX5043_FREQA2
      004009 ED               [12]10971 	mov	a,r5
      00400A F0               [24]10972 	movx	@dptr,a
                                  10973 ;	..\src\COMMON\easyax5043.c:2046: AX5043_FREQA3 = f >> 24;
      00400B 90 40 34         [24]10974 	mov	dptr,#_AX5043_FREQA3
      00400E EF               [12]10975 	mov	a,r7
      00400F F0               [24]10976 	movx	@dptr,a
      004010 80 1C            [24]10977 	sjmp	00107$
      004012                      10978 00106$:
                                  10979 ;	..\src\COMMON\easyax5043.c:2048: AX5043_PLLRANGINGB = rng & 0x0F;
      004012 90 40 3B         [24]10980 	mov	dptr,#_AX5043_PLLRANGINGB
      004015 74 0F            [12]10981 	mov	a,#0x0f
      004017 55 43            [12]10982 	anl	a,_axradio_set_channel_rng_1_422
      004019 F0               [24]10983 	movx	@dptr,a
                                  10984 ;	..\src\COMMON\easyax5043.c:2049: AX5043_FREQB0 = f;
      00401A 90 40 3F         [24]10985 	mov	dptr,#_AX5043_FREQB0
      00401D EB               [12]10986 	mov	a,r3
      00401E F0               [24]10987 	movx	@dptr,a
                                  10988 ;	..\src\COMMON\easyax5043.c:2050: AX5043_FREQB1 = f >> 8;
      00401F 90 40 3E         [24]10989 	mov	dptr,#_AX5043_FREQB1
      004022 EC               [12]10990 	mov	a,r4
      004023 F0               [24]10991 	movx	@dptr,a
                                  10992 ;	..\src\COMMON\easyax5043.c:2051: AX5043_FREQB2 = f >> 16;
      004024 90 40 3D         [24]10993 	mov	dptr,#_AX5043_FREQB2
      004027 ED               [12]10994 	mov	a,r5
      004028 F0               [24]10995 	movx	@dptr,a
                                  10996 ;	..\src\COMMON\easyax5043.c:2052: AX5043_FREQB3 = f >> 24;
      004029 90 40 3C         [24]10997 	mov	dptr,#_AX5043_FREQB3
      00402C EF               [12]10998 	mov	a,r7
      00402D F0               [24]10999 	movx	@dptr,a
      00402E                      11000 00107$:
                                  11001 ;	..\src\COMMON\easyax5043.c:2055: AX5043_PLLLOOP ^= 0x80;
      00402E 90 40 30         [24]11002 	mov	dptr,#_AX5043_PLLLOOP
      004031 E0               [24]11003 	movx	a,@dptr
      004032 FF               [12]11004 	mov	r7,a
      004033 74 80            [12]11005 	mov	a,#0x80
      004035 6F               [12]11006 	xrl	a,r7
      004036 F0               [24]11007 	movx	@dptr,a
                                  11008 ;	..\src\COMMON\easyax5043.c:2056: return AXRADIO_ERR_NOERROR;
      004037 75 82 00         [24]11009 	mov	dpl,#0x00
      00403A 22               [24]11010 	ret
                                  11011 ;------------------------------------------------------------
                                  11012 ;Allocation info for local variables in function 'axradio_get_pllvcoi'
                                  11013 ;------------------------------------------------------------
                                  11014 ;x                         Allocated with name '_axradio_get_pllvcoi_x_2_428'
                                  11015 ;x                         Allocated with name '_axradio_get_pllvcoi_x_2_429'
                                  11016 ;------------------------------------------------------------
                                  11017 ;	..\src\COMMON\easyax5043.c:2059: uint8_t axradio_get_pllvcoi(void)
                                  11018 ;	-----------------------------------------
                                  11019 ;	 function axradio_get_pllvcoi
                                  11020 ;	-----------------------------------------
      00403B                      11021 _axradio_get_pllvcoi:
                                  11022 ;	..\src\COMMON\easyax5043.c:2061: if (axradio_phy_vcocalib) {
      00403B 90 7A C1         [24]11023 	mov	dptr,#_axradio_phy_vcocalib
      00403E E4               [12]11024 	clr	a
      00403F 93               [24]11025 	movc	a,@a+dptr
      004040 60 15            [24]11026 	jz	00104$
                                  11027 ;	..\src\COMMON\easyax5043.c:2062: uint8_t x = axradio_phy_chanvcoi[axradio_curchannel];
      004042 90 00 BC         [24]11028 	mov	dptr,#_axradio_curchannel
      004045 E0               [24]11029 	movx	a,@dptr
      004046 24 0A            [12]11030 	add	a,#_axradio_phy_chanvcoi
      004048 F5 82            [12]11031 	mov	dpl,a
      00404A E4               [12]11032 	clr	a
      00404B 34 00            [12]11033 	addc	a,#(_axradio_phy_chanvcoi >> 8)
      00404D F5 83            [12]11034 	mov	dph,a
      00404F E0               [24]11035 	movx	a,@dptr
                                  11036 ;	..\src\COMMON\easyax5043.c:2063: if (x & 0x80)
      004050 FF               [12]11037 	mov	r7,a
      004051 30 E7 03         [24]11038 	jnb	acc.7,00104$
                                  11039 ;	..\src\COMMON\easyax5043.c:2064: return x;
      004054 8F 82            [24]11040 	mov	dpl,r7
      004056 22               [24]11041 	ret
      004057                      11042 00104$:
                                  11043 ;	..\src\COMMON\easyax5043.c:2067: uint8_t x = axradio_phy_chanvcoiinit[axradio_curchannel];
      004057 90 00 BC         [24]11044 	mov	dptr,#_axradio_curchannel
      00405A E0               [24]11045 	movx	a,@dptr
      00405B FF               [12]11046 	mov	r7,a
      00405C 90 7A C0         [24]11047 	mov	dptr,#_axradio_phy_chanvcoiinit
      00405F 93               [24]11048 	movc	a,@a+dptr
      004060 FE               [12]11049 	mov	r6,a
      004061 90 03 66         [24]11050 	mov	dptr,#_axradio_get_pllvcoi_x_2_429
      004064 F0               [24]11051 	movx	@dptr,a
                                  11052 ;	..\src\COMMON\easyax5043.c:2068: if (x & 0x80) {
      004065 EE               [12]11053 	mov	a,r6
      004066 30 E7 37         [24]11054 	jnb	acc.7,00108$
                                  11055 ;	..\src\COMMON\easyax5043.c:2069: if (!(axradio_phy_chanpllrnginit[0] & 0xF0)) {
      004069 90 7A BF         [24]11056 	mov	dptr,#_axradio_phy_chanpllrnginit
      00406C E4               [12]11057 	clr	a
      00406D 93               [24]11058 	movc	a,@a+dptr
      00406E FD               [12]11059 	mov	r5,a
      00406F 54 F0            [12]11060 	anl	a,#0xf0
      004071 60 02            [24]11061 	jz	00127$
      004073 80 24            [24]11062 	sjmp	00106$
      004075                      11063 00127$:
                                  11064 ;	..\src\COMMON\easyax5043.c:2070: x += (axradio_phy_chanpllrng[axradio_curchannel] & 0x0F) - (axradio_phy_chanpllrnginit[axradio_curchannel] & 0x0F);
      004075 EF               [12]11065 	mov	a,r7
      004076 24 09            [12]11066 	add	a,#_axradio_phy_chanpllrng
      004078 F5 82            [12]11067 	mov	dpl,a
      00407A E4               [12]11068 	clr	a
      00407B 34 00            [12]11069 	addc	a,#(_axradio_phy_chanpllrng >> 8)
      00407D F5 83            [12]11070 	mov	dph,a
      00407F E0               [24]11071 	movx	a,@dptr
      004080 FD               [12]11072 	mov	r5,a
      004081 53 05 0F         [24]11073 	anl	ar5,#0x0f
      004084 EF               [12]11074 	mov	a,r7
      004085 90 7A BF         [24]11075 	mov	dptr,#_axradio_phy_chanpllrnginit
      004088 93               [24]11076 	movc	a,@a+dptr
      004089 FF               [12]11077 	mov	r7,a
      00408A 74 0F            [12]11078 	mov	a,#0x0f
      00408C 5F               [12]11079 	anl	a,r7
      00408D D3               [12]11080 	setb	c
      00408E 9D               [12]11081 	subb	a,r5
      00408F F4               [12]11082 	cpl	a
      004090 2E               [12]11083 	add	a,r6
                                  11084 ;	..\src\COMMON\easyax5043.c:2071: x &= 0x3f;
      004091 54 3F            [12]11085 	anl	a,#0x3f
                                  11086 ;	..\src\COMMON\easyax5043.c:2072: x |= 0x80;
      004093 90 03 66         [24]11087 	mov	dptr,#_axradio_get_pllvcoi_x_2_429
      004096 44 80            [12]11088 	orl	a,#0x80
      004098 F0               [24]11089 	movx	@dptr,a
      004099                      11090 00106$:
                                  11091 ;	..\src\COMMON\easyax5043.c:2074: return x;
      004099 90 03 66         [24]11092 	mov	dptr,#_axradio_get_pllvcoi_x_2_429
      00409C E0               [24]11093 	movx	a,@dptr
      00409D F5 82            [12]11094 	mov	dpl,a
      00409F 22               [24]11095 	ret
      0040A0                      11096 00108$:
                                  11097 ;	..\src\COMMON\easyax5043.c:2077: return AX5043_PLLVCOI;
      0040A0 90 41 80         [24]11098 	mov	dptr,#_AX5043_PLLVCOI
      0040A3 E0               [24]11099 	movx	a,@dptr
      0040A4 F5 82            [12]11100 	mov	dpl,a
      0040A6 22               [24]11101 	ret
                                  11102 ;------------------------------------------------------------
                                  11103 ;Allocation info for local variables in function 'axradio_set_curfreqoffset'
                                  11104 ;------------------------------------------------------------
                                  11105 ;offs                      Allocated with name '_axradio_set_curfreqoffset_offs_1_432'
                                  11106 ;------------------------------------------------------------
                                  11107 ;	..\src\COMMON\easyax5043.c:2080: static uint8_t axradio_set_curfreqoffset(int32_t offs)
                                  11108 ;	-----------------------------------------
                                  11109 ;	 function axradio_set_curfreqoffset
                                  11110 ;	-----------------------------------------
      0040A7                      11111 _axradio_set_curfreqoffset:
      0040A7 AF 82            [24]11112 	mov	r7,dpl
      0040A9 AE 83            [24]11113 	mov	r6,dph
      0040AB AD F0            [24]11114 	mov	r5,b
      0040AD FC               [12]11115 	mov	r4,a
      0040AE 90 03 67         [24]11116 	mov	dptr,#_axradio_set_curfreqoffset_offs_1_432
      0040B1 EF               [12]11117 	mov	a,r7
      0040B2 F0               [24]11118 	movx	@dptr,a
      0040B3 EE               [12]11119 	mov	a,r6
      0040B4 A3               [24]11120 	inc	dptr
      0040B5 F0               [24]11121 	movx	@dptr,a
      0040B6 ED               [12]11122 	mov	a,r5
      0040B7 A3               [24]11123 	inc	dptr
      0040B8 F0               [24]11124 	movx	@dptr,a
      0040B9 EC               [12]11125 	mov	a,r4
      0040BA A3               [24]11126 	inc	dptr
      0040BB F0               [24]11127 	movx	@dptr,a
                                  11128 ;	..\src\COMMON\easyax5043.c:2082: axradio_curfreqoffset = offs;
      0040BC 90 03 67         [24]11129 	mov	dptr,#_axradio_set_curfreqoffset_offs_1_432
      0040BF E0               [24]11130 	movx	a,@dptr
      0040C0 FC               [12]11131 	mov	r4,a
      0040C1 A3               [24]11132 	inc	dptr
      0040C2 E0               [24]11133 	movx	a,@dptr
      0040C3 FD               [12]11134 	mov	r5,a
      0040C4 A3               [24]11135 	inc	dptr
      0040C5 E0               [24]11136 	movx	a,@dptr
      0040C6 FE               [12]11137 	mov	r6,a
      0040C7 A3               [24]11138 	inc	dptr
      0040C8 E0               [24]11139 	movx	a,@dptr
      0040C9 FF               [12]11140 	mov	r7,a
      0040CA 90 00 BD         [24]11141 	mov	dptr,#_axradio_curfreqoffset
      0040CD EC               [12]11142 	mov	a,r4
      0040CE F0               [24]11143 	movx	@dptr,a
      0040CF ED               [12]11144 	mov	a,r5
      0040D0 A3               [24]11145 	inc	dptr
      0040D1 F0               [24]11146 	movx	@dptr,a
      0040D2 EE               [12]11147 	mov	a,r6
      0040D3 A3               [24]11148 	inc	dptr
      0040D4 F0               [24]11149 	movx	@dptr,a
      0040D5 EF               [12]11150 	mov	a,r7
      0040D6 A3               [24]11151 	inc	dptr
      0040D7 F0               [24]11152 	movx	@dptr,a
                                  11153 ;	..\src\COMMON\easyax5043.c:2083: if (checksignedlimit32(offs, axradio_phy_maxfreqoffset))
      0040D8 90 7A C2         [24]11154 	mov	dptr,#_axradio_phy_maxfreqoffset
      0040DB E4               [12]11155 	clr	a
      0040DC 93               [24]11156 	movc	a,@a+dptr
      0040DD C0 E0            [24]11157 	push	acc
      0040DF 74 01            [12]11158 	mov	a,#0x01
      0040E1 93               [24]11159 	movc	a,@a+dptr
      0040E2 C0 E0            [24]11160 	push	acc
      0040E4 74 02            [12]11161 	mov	a,#0x02
      0040E6 93               [24]11162 	movc	a,@a+dptr
      0040E7 C0 E0            [24]11163 	push	acc
      0040E9 74 03            [12]11164 	mov	a,#0x03
      0040EB 93               [24]11165 	movc	a,@a+dptr
      0040EC C0 E0            [24]11166 	push	acc
      0040EE 8C 82            [24]11167 	mov	dpl,r4
      0040F0 8D 83            [24]11168 	mov	dph,r5
      0040F2 8E F0            [24]11169 	mov	b,r6
      0040F4 EF               [12]11170 	mov	a,r7
      0040F5 12 6E 9A         [24]11171 	lcall	_checksignedlimit32
      0040F8 AF 82            [24]11172 	mov	r7,dpl
      0040FA E5 81            [12]11173 	mov	a,sp
      0040FC 24 FC            [12]11174 	add	a,#0xfc
      0040FE F5 81            [12]11175 	mov	sp,a
      004100 EF               [12]11176 	mov	a,r7
      004101 60 04            [24]11177 	jz	00102$
                                  11178 ;	..\src\COMMON\easyax5043.c:2084: return AXRADIO_ERR_NOERROR;
      004103 75 82 00         [24]11179 	mov	dpl,#0x00
      004106 22               [24]11180 	ret
      004107                      11181 00102$:
                                  11182 ;	..\src\COMMON\easyax5043.c:2085: if (axradio_curfreqoffset < 0)
      004107 90 00 BD         [24]11183 	mov	dptr,#_axradio_curfreqoffset
      00410A E0               [24]11184 	movx	a,@dptr
      00410B FC               [12]11185 	mov	r4,a
      00410C A3               [24]11186 	inc	dptr
      00410D E0               [24]11187 	movx	a,@dptr
      00410E FD               [12]11188 	mov	r5,a
      00410F A3               [24]11189 	inc	dptr
      004110 E0               [24]11190 	movx	a,@dptr
      004111 FE               [12]11191 	mov	r6,a
      004112 A3               [24]11192 	inc	dptr
      004113 E0               [24]11193 	movx	a,@dptr
      004114 FF               [12]11194 	mov	r7,a
      004115 30 E7 27         [24]11195 	jnb	acc.7,00104$
                                  11196 ;	..\src\COMMON\easyax5043.c:2086: axradio_curfreqoffset = -axradio_phy_maxfreqoffset;
      004118 90 7A C2         [24]11197 	mov	dptr,#_axradio_phy_maxfreqoffset
      00411B E4               [12]11198 	clr	a
      00411C 93               [24]11199 	movc	a,@a+dptr
      00411D FC               [12]11200 	mov	r4,a
      00411E 74 01            [12]11201 	mov	a,#0x01
      004120 93               [24]11202 	movc	a,@a+dptr
      004121 FD               [12]11203 	mov	r5,a
      004122 74 02            [12]11204 	mov	a,#0x02
      004124 93               [24]11205 	movc	a,@a+dptr
      004125 FE               [12]11206 	mov	r6,a
      004126 74 03            [12]11207 	mov	a,#0x03
      004128 93               [24]11208 	movc	a,@a+dptr
      004129 FF               [12]11209 	mov	r7,a
      00412A 90 00 BD         [24]11210 	mov	dptr,#_axradio_curfreqoffset
      00412D C3               [12]11211 	clr	c
      00412E E4               [12]11212 	clr	a
      00412F 9C               [12]11213 	subb	a,r4
      004130 F0               [24]11214 	movx	@dptr,a
      004131 E4               [12]11215 	clr	a
      004132 9D               [12]11216 	subb	a,r5
      004133 A3               [24]11217 	inc	dptr
      004134 F0               [24]11218 	movx	@dptr,a
      004135 E4               [12]11219 	clr	a
      004136 9E               [12]11220 	subb	a,r6
      004137 A3               [24]11221 	inc	dptr
      004138 F0               [24]11222 	movx	@dptr,a
      004139 E4               [12]11223 	clr	a
      00413A 9F               [12]11224 	subb	a,r7
      00413B A3               [24]11225 	inc	dptr
      00413C F0               [24]11226 	movx	@dptr,a
      00413D 80 20            [24]11227 	sjmp	00105$
      00413F                      11228 00104$:
                                  11229 ;	..\src\COMMON\easyax5043.c:2088: axradio_curfreqoffset = axradio_phy_maxfreqoffset;
      00413F 90 7A C2         [24]11230 	mov	dptr,#_axradio_phy_maxfreqoffset
      004142 E4               [12]11231 	clr	a
      004143 93               [24]11232 	movc	a,@a+dptr
      004144 FC               [12]11233 	mov	r4,a
      004145 74 01            [12]11234 	mov	a,#0x01
      004147 93               [24]11235 	movc	a,@a+dptr
      004148 FD               [12]11236 	mov	r5,a
      004149 74 02            [12]11237 	mov	a,#0x02
      00414B 93               [24]11238 	movc	a,@a+dptr
      00414C FE               [12]11239 	mov	r6,a
      00414D 74 03            [12]11240 	mov	a,#0x03
      00414F 93               [24]11241 	movc	a,@a+dptr
      004150 FF               [12]11242 	mov	r7,a
      004151 90 00 BD         [24]11243 	mov	dptr,#_axradio_curfreqoffset
      004154 EC               [12]11244 	mov	a,r4
      004155 F0               [24]11245 	movx	@dptr,a
      004156 ED               [12]11246 	mov	a,r5
      004157 A3               [24]11247 	inc	dptr
      004158 F0               [24]11248 	movx	@dptr,a
      004159 EE               [12]11249 	mov	a,r6
      00415A A3               [24]11250 	inc	dptr
      00415B F0               [24]11251 	movx	@dptr,a
      00415C EF               [12]11252 	mov	a,r7
      00415D A3               [24]11253 	inc	dptr
      00415E F0               [24]11254 	movx	@dptr,a
      00415F                      11255 00105$:
                                  11256 ;	..\src\COMMON\easyax5043.c:2089: return AXRADIO_ERR_INVALID;
      00415F 75 82 04         [24]11257 	mov	dpl,#0x04
      004162 22               [24]11258 	ret
                                  11259 ;------------------------------------------------------------
                                  11260 ;Allocation info for local variables in function 'axradio_set_local_address'
                                  11261 ;------------------------------------------------------------
                                  11262 ;addr                      Allocated with name '_axradio_set_local_address_addr_1_434'
                                  11263 ;------------------------------------------------------------
                                  11264 ;	..\src\COMMON\easyax5043.c:2095: void axradio_set_local_address(const struct axradio_address_mask __generic *addr)
                                  11265 ;	-----------------------------------------
                                  11266 ;	 function axradio_set_local_address
                                  11267 ;	-----------------------------------------
      004163                      11268 _axradio_set_local_address:
      004163 AF F0            [24]11269 	mov	r7,b
      004165 AE 83            [24]11270 	mov	r6,dph
      004167 E5 82            [12]11271 	mov	a,dpl
      004169 90 03 6B         [24]11272 	mov	dptr,#_axradio_set_local_address_addr_1_434
      00416C F0               [24]11273 	movx	@dptr,a
      00416D EE               [12]11274 	mov	a,r6
      00416E A3               [24]11275 	inc	dptr
      00416F F0               [24]11276 	movx	@dptr,a
      004170 EF               [12]11277 	mov	a,r7
      004171 A3               [24]11278 	inc	dptr
      004172 F0               [24]11279 	movx	@dptr,a
                                  11280 ;	..\src\COMMON\easyax5043.c:2097: memcpy_xdatageneric(&axradio_localaddr, addr, sizeof(axradio_localaddr));
      004173 90 03 6B         [24]11281 	mov	dptr,#_axradio_set_local_address_addr_1_434
      004176 E0               [24]11282 	movx	a,@dptr
      004177 FD               [12]11283 	mov	r5,a
      004178 A3               [24]11284 	inc	dptr
      004179 E0               [24]11285 	movx	a,@dptr
      00417A FE               [12]11286 	mov	r6,a
      00417B A3               [24]11287 	inc	dptr
      00417C E0               [24]11288 	movx	a,@dptr
      00417D FF               [12]11289 	mov	r7,a
      00417E 90 03 E2         [24]11290 	mov	dptr,#_memcpy_PARM_2
      004181 ED               [12]11291 	mov	a,r5
      004182 F0               [24]11292 	movx	@dptr,a
      004183 EE               [12]11293 	mov	a,r6
      004184 A3               [24]11294 	inc	dptr
      004185 F0               [24]11295 	movx	@dptr,a
      004186 EF               [12]11296 	mov	a,r7
      004187 A3               [24]11297 	inc	dptr
      004188 F0               [24]11298 	movx	@dptr,a
      004189 90 03 E5         [24]11299 	mov	dptr,#_memcpy_PARM_3
      00418C 74 08            [12]11300 	mov	a,#0x08
      00418E F0               [24]11301 	movx	@dptr,a
      00418F E4               [12]11302 	clr	a
      004190 A3               [24]11303 	inc	dptr
      004191 F0               [24]11304 	movx	@dptr,a
      004192 90 00 D1         [24]11305 	mov	dptr,#_axradio_localaddr
      004195 75 F0 00         [24]11306 	mov	b,#0x00
      004198 12 65 97         [24]11307 	lcall	_memcpy
                                  11308 ;	..\src\COMMON\easyax5043.c:2098: axradio_setaddrregs();
      00419B 02 23 1F         [24]11309 	ljmp	_axradio_setaddrregs
                                  11310 ;------------------------------------------------------------
                                  11311 ;Allocation info for local variables in function 'axradio_set_default_remote_address'
                                  11312 ;------------------------------------------------------------
                                  11313 ;addr                      Allocated with name '_axradio_set_default_remote_address_addr_1_436'
                                  11314 ;------------------------------------------------------------
                                  11315 ;	..\src\COMMON\easyax5043.c:2102: void axradio_set_default_remote_address(const struct axradio_address __generic *addr)
                                  11316 ;	-----------------------------------------
                                  11317 ;	 function axradio_set_default_remote_address
                                  11318 ;	-----------------------------------------
      00419E                      11319 _axradio_set_default_remote_address:
      00419E AF F0            [24]11320 	mov	r7,b
      0041A0 AE 83            [24]11321 	mov	r6,dph
      0041A2 E5 82            [12]11322 	mov	a,dpl
      0041A4 90 03 6E         [24]11323 	mov	dptr,#_axradio_set_default_remote_address_addr_1_436
      0041A7 F0               [24]11324 	movx	@dptr,a
      0041A8 EE               [12]11325 	mov	a,r6
      0041A9 A3               [24]11326 	inc	dptr
      0041AA F0               [24]11327 	movx	@dptr,a
      0041AB EF               [12]11328 	mov	a,r7
      0041AC A3               [24]11329 	inc	dptr
      0041AD F0               [24]11330 	movx	@dptr,a
                                  11331 ;	..\src\COMMON\easyax5043.c:2104: memcpy_xdatageneric(&axradio_default_remoteaddr, addr, sizeof(axradio_default_remoteaddr));
      0041AE 90 03 6E         [24]11332 	mov	dptr,#_axradio_set_default_remote_address_addr_1_436
      0041B1 E0               [24]11333 	movx	a,@dptr
      0041B2 FD               [12]11334 	mov	r5,a
      0041B3 A3               [24]11335 	inc	dptr
      0041B4 E0               [24]11336 	movx	a,@dptr
      0041B5 FE               [12]11337 	mov	r6,a
      0041B6 A3               [24]11338 	inc	dptr
      0041B7 E0               [24]11339 	movx	a,@dptr
      0041B8 FF               [12]11340 	mov	r7,a
      0041B9 90 03 E2         [24]11341 	mov	dptr,#_memcpy_PARM_2
      0041BC ED               [12]11342 	mov	a,r5
      0041BD F0               [24]11343 	movx	@dptr,a
      0041BE EE               [12]11344 	mov	a,r6
      0041BF A3               [24]11345 	inc	dptr
      0041C0 F0               [24]11346 	movx	@dptr,a
      0041C1 EF               [12]11347 	mov	a,r7
      0041C2 A3               [24]11348 	inc	dptr
      0041C3 F0               [24]11349 	movx	@dptr,a
      0041C4 90 03 E5         [24]11350 	mov	dptr,#_memcpy_PARM_3
      0041C7 74 04            [12]11351 	mov	a,#0x04
      0041C9 F0               [24]11352 	movx	@dptr,a
      0041CA E4               [12]11353 	clr	a
      0041CB A3               [24]11354 	inc	dptr
      0041CC F0               [24]11355 	movx	@dptr,a
      0041CD 90 00 D9         [24]11356 	mov	dptr,#_axradio_default_remoteaddr
      0041D0 75 F0 00         [24]11357 	mov	b,#0x00
      0041D3 02 65 97         [24]11358 	ljmp	_memcpy
                                  11359 ;------------------------------------------------------------
                                  11360 ;Allocation info for local variables in function 'axradio_transmit'
                                  11361 ;------------------------------------------------------------
                                  11362 ;fifofree                  Allocated to registers r6 r7 
                                  11363 ;i                         Allocated to registers r4 
                                  11364 ;iesave                    Allocated to registers r7 
                                  11365 ;len_byte                  Allocated to registers r6 
                                  11366 ;pkt                       Allocated with name '_axradio_transmit_PARM_2'
                                  11367 ;pktlen                    Allocated with name '_axradio_transmit_PARM_3'
                                  11368 ;addr                      Allocated with name '_axradio_transmit_addr_1_438'
                                  11369 ;------------------------------------------------------------
                                  11370 ;	..\src\COMMON\easyax5043.c:2108: uint8_t axradio_transmit(const struct axradio_address __generic *addr, const uint8_t __generic *pkt, uint16_t pktlen)
                                  11371 ;	-----------------------------------------
                                  11372 ;	 function axradio_transmit
                                  11373 ;	-----------------------------------------
      0041D6                      11374 _axradio_transmit:
      0041D6 AF F0            [24]11375 	mov	r7,b
      0041D8 AE 83            [24]11376 	mov	r6,dph
      0041DA E5 82            [12]11377 	mov	a,dpl
      0041DC 90 03 76         [24]11378 	mov	dptr,#_axradio_transmit_addr_1_438
      0041DF F0               [24]11379 	movx	@dptr,a
      0041E0 EE               [12]11380 	mov	a,r6
      0041E1 A3               [24]11381 	inc	dptr
      0041E2 F0               [24]11382 	movx	@dptr,a
      0041E3 EF               [12]11383 	mov	a,r7
      0041E4 A3               [24]11384 	inc	dptr
      0041E5 F0               [24]11385 	movx	@dptr,a
                                  11386 ;	..\src\COMMON\easyax5043.c:2110: switch (axradio_mode) {
      0041E6 AF 0B            [24]11387 	mov	r7,_axradio_mode
      0041E8 BF 10 03         [24]11388 	cjne	r7,#0x10,00278$
      0041EB 02 42 D8         [24]11389 	ljmp	00125$
      0041EE                      11390 00278$:
      0041EE BF 11 03         [24]11391 	cjne	r7,#0x11,00279$
      0041F1 02 42 D8         [24]11392 	ljmp	00125$
      0041F4                      11393 00279$:
      0041F4 BF 12 03         [24]11394 	cjne	r7,#0x12,00280$
      0041F7 02 42 D8         [24]11395 	ljmp	00125$
      0041FA                      11396 00280$:
      0041FA BF 13 03         [24]11397 	cjne	r7,#0x13,00281$
      0041FD 02 42 D8         [24]11398 	ljmp	00125$
      004200                      11399 00281$:
      004200 BF 18 02         [24]11400 	cjne	r7,#0x18,00282$
      004203 80 2F            [24]11401 	sjmp	00105$
      004205                      11402 00282$:
      004205 BF 19 02         [24]11403 	cjne	r7,#0x19,00283$
      004208 80 2A            [24]11404 	sjmp	00105$
      00420A                      11405 00283$:
      00420A BF 1A 02         [24]11406 	cjne	r7,#0x1a,00284$
      00420D 80 25            [24]11407 	sjmp	00105$
      00420F                      11408 00284$:
      00420F BF 1B 02         [24]11409 	cjne	r7,#0x1b,00285$
      004212 80 20            [24]11410 	sjmp	00105$
      004214                      11411 00285$:
      004214 BF 1C 02         [24]11412 	cjne	r7,#0x1c,00286$
      004217 80 1B            [24]11413 	sjmp	00105$
      004219                      11414 00286$:
      004219 BF 20 03         [24]11415 	cjne	r7,#0x20,00287$
      00421C 02 42 AB         [24]11416 	ljmp	00116$
      00421F                      11417 00287$:
      00421F BF 21 03         [24]11418 	cjne	r7,#0x21,00288$
      004222 02 42 AB         [24]11419 	ljmp	00116$
      004225                      11420 00288$:
      004225 BF 30 03         [24]11421 	cjne	r7,#0x30,00289$
      004228 02 42 E3         [24]11422 	ljmp	00128$
      00422B                      11423 00289$:
      00422B BF 31 03         [24]11424 	cjne	r7,#0x31,00290$
      00422E 02 42 E3         [24]11425 	ljmp	00128$
      004231                      11426 00290$:
      004231 02 45 76         [24]11427 	ljmp	00162$
                                  11428 ;	..\src\COMMON\easyax5043.c:2115: case AXRADIO_MODE_STREAM_TRANSMIT_SCRAM_LSB:
      004234                      11429 00105$:
                                  11430 ;	..\src\COMMON\easyax5043.c:2117: uint16_t __autodata fifofree = radio_read16((uint16_t)&AX5043_FIFOFREE1);
      004234 7E 2C            [12]11431 	mov	r6,#_AX5043_FIFOFREE1
      004236 7F 40            [12]11432 	mov	r7,#(_AX5043_FIFOFREE1 >> 8)
      004238 8E 82            [24]11433 	mov	dpl,r6
      00423A 8F 83            [24]11434 	mov	dph,r7
      00423C 12 68 A0         [24]11435 	lcall	_radio_read16
      00423F AE 82            [24]11436 	mov	r6,dpl
      004241 AF 83            [24]11437 	mov	r7,dph
                                  11438 ;	..\src\COMMON\easyax5043.c:2118: if (fifofree < pktlen + 3)
      004243 90 03 74         [24]11439 	mov	dptr,#_axradio_transmit_PARM_3
      004246 E0               [24]11440 	movx	a,@dptr
      004247 FC               [12]11441 	mov	r4,a
      004248 A3               [24]11442 	inc	dptr
      004249 E0               [24]11443 	movx	a,@dptr
      00424A FD               [12]11444 	mov	r5,a
      00424B 74 03            [12]11445 	mov	a,#0x03
      00424D 2C               [12]11446 	add	a,r4
      00424E FA               [12]11447 	mov	r2,a
      00424F E4               [12]11448 	clr	a
      004250 3D               [12]11449 	addc	a,r5
      004251 FB               [12]11450 	mov	r3,a
      004252 C3               [12]11451 	clr	c
      004253 EE               [12]11452 	mov	a,r6
      004254 9A               [12]11453 	subb	a,r2
      004255 EF               [12]11454 	mov	a,r7
      004256 9B               [12]11455 	subb	a,r3
      004257 50 04            [24]11456 	jnc	00107$
                                  11457 ;	..\src\COMMON\easyax5043.c:2119: return AXRADIO_ERR_INVALID;
      004259 75 82 04         [24]11458 	mov	dpl,#0x04
      00425C 22               [24]11459 	ret
      00425D                      11460 00107$:
                                  11461 ;	..\src\COMMON\easyax5043.c:2121: if (pktlen) {
      00425D EC               [12]11462 	mov	a,r4
      00425E 4D               [12]11463 	orl	a,r5
      00425F 60 2D            [24]11464 	jz	00112$
                                  11465 ;	..\src\COMMON\easyax5043.c:2122: uint8_t __autodata i = pktlen;
                                  11466 ;	..\src\COMMON\easyax5043.c:2123: AX5043_FIFODATA = AX5043_FIFOCMD_DATA | (7 << 5);
      004261 90 40 29         [24]11467 	mov	dptr,#_AX5043_FIFODATA
      004264 74 E1            [12]11468 	mov	a,#0xe1
      004266 F0               [24]11469 	movx	@dptr,a
                                  11470 ;	..\src\COMMON\easyax5043.c:2124: AX5043_FIFODATA = i + 1;
      004267 EC               [12]11471 	mov	a,r4
      004268 04               [12]11472 	inc	a
      004269 F0               [24]11473 	movx	@dptr,a
                                  11474 ;	..\src\COMMON\easyax5043.c:2125: AX5043_FIFODATA = 0x08;
      00426A 74 08            [12]11475 	mov	a,#0x08
      00426C F0               [24]11476 	movx	@dptr,a
                                  11477 ;	..\src\COMMON\easyax5043.c:2126: do {
      00426D 90 03 71         [24]11478 	mov	dptr,#_axradio_transmit_PARM_2
      004270 E0               [24]11479 	movx	a,@dptr
      004271 FD               [12]11480 	mov	r5,a
      004272 A3               [24]11481 	inc	dptr
      004273 E0               [24]11482 	movx	a,@dptr
      004274 FE               [12]11483 	mov	r6,a
      004275 A3               [24]11484 	inc	dptr
      004276 E0               [24]11485 	movx	a,@dptr
      004277 FF               [12]11486 	mov	r7,a
      004278                      11487 00108$:
                                  11488 ;	..\src\COMMON\easyax5043.c:2127: AX5043_FIFODATA = *pkt++;
      004278 8D 82            [24]11489 	mov	dpl,r5
      00427A 8E 83            [24]11490 	mov	dph,r6
      00427C 8F F0            [24]11491 	mov	b,r7
      00427E 12 79 65         [24]11492 	lcall	__gptrget
      004281 FB               [12]11493 	mov	r3,a
      004282 A3               [24]11494 	inc	dptr
      004283 AD 82            [24]11495 	mov	r5,dpl
      004285 AE 83            [24]11496 	mov	r6,dph
      004287 90 40 29         [24]11497 	mov	dptr,#_AX5043_FIFODATA
      00428A EB               [12]11498 	mov	a,r3
      00428B F0               [24]11499 	movx	@dptr,a
                                  11500 ;	..\src\COMMON\easyax5043.c:2128: } while (--i);
      00428C DC EA            [24]11501 	djnz	r4,00108$
      00428E                      11502 00112$:
                                  11503 ;	..\src\COMMON\easyax5043.c:2130: AX5043_FIFOSTAT =  4; // FIFO commit
      00428E 90 40 28         [24]11504 	mov	dptr,#_AX5043_FIFOSTAT
      004291 74 04            [12]11505 	mov	a,#0x04
      004293 F0               [24]11506 	movx	@dptr,a
                                  11507 ;	..\src\COMMON\easyax5043.c:2132: uint8_t __autodata iesave = IE & 0x80;
      004294 74 80            [12]11508 	mov	a,#0x80
      004296 55 A8            [12]11509 	anl	a,_IE
      004298 FF               [12]11510 	mov	r7,a
                                  11511 ;	..\src\COMMON\easyax5043.c:2133: EA = 0;
      004299 C2 AF            [12]11512 	clr	_EA
                                  11513 ;	..\src\COMMON\easyax5043.c:2134: AX5043_IRQMASK0 |= 0x08;
      00429B 90 40 07         [24]11514 	mov	dptr,#_AX5043_IRQMASK0
      00429E E0               [24]11515 	movx	a,@dptr
      00429F FE               [12]11516 	mov	r6,a
      0042A0 74 08            [12]11517 	mov	a,#0x08
      0042A2 4E               [12]11518 	orl	a,r6
      0042A3 F0               [24]11519 	movx	@dptr,a
                                  11520 ;	..\src\COMMON\easyax5043.c:2135: IE |= iesave;
      0042A4 EF               [12]11521 	mov	a,r7
      0042A5 42 A8            [12]11522 	orl	_IE,a
                                  11523 ;	..\src\COMMON\easyax5043.c:2137: return AXRADIO_ERR_NOERROR;
      0042A7 75 82 00         [24]11524 	mov	dpl,#0x00
      0042AA 22               [24]11525 	ret
                                  11526 ;	..\src\COMMON\easyax5043.c:2144: case AXRADIO_MODE_WOR_RECEIVE:
      0042AB                      11527 00116$:
                                  11528 ;	..\src\COMMON\easyax5043.c:2145: if (axradio_syncstate != syncstate_off)
      0042AB 90 00 B7         [24]11529 	mov	dptr,#_axradio_syncstate
      0042AE E0               [24]11530 	movx	a,@dptr
      0042AF E0               [24]11531 	movx	a,@dptr
      0042B0 60 04            [24]11532 	jz	00118$
                                  11533 ;	..\src\COMMON\easyax5043.c:2146: return AXRADIO_ERR_BUSY;
      0042B2 75 82 02         [24]11534 	mov	dpl,#0x02
      0042B5 22               [24]11535 	ret
      0042B6                      11536 00118$:
                                  11537 ;	..\src\COMMON\easyax5043.c:2147: AX5043_IRQMASK1 = 0x00;
      0042B6 90 40 06         [24]11538 	mov	dptr,#_AX5043_IRQMASK1
      0042B9 E4               [12]11539 	clr	a
      0042BA F0               [24]11540 	movx	@dptr,a
                                  11541 ;	..\src\COMMON\easyax5043.c:2148: AX5043_IRQMASK0 = 0x00;
      0042BB 90 40 07         [24]11542 	mov	dptr,#_AX5043_IRQMASK0
      0042BE F0               [24]11543 	movx	@dptr,a
                                  11544 ;	..\src\COMMON\easyax5043.c:2149: AX5043_PWRMODE = AX5043_PWRSTATE_XTAL_ON;
      0042BF 90 40 02         [24]11545 	mov	dptr,#_AX5043_PWRMODE
      0042C2 74 05            [12]11546 	mov	a,#0x05
      0042C4 F0               [24]11547 	movx	@dptr,a
                                  11548 ;	..\src\COMMON\easyax5043.c:2150: AX5043_FIFOSTAT = 3;
      0042C5 90 40 28         [24]11549 	mov	dptr,#_AX5043_FIFOSTAT
      0042C8 74 03            [12]11550 	mov	a,#0x03
      0042CA F0               [24]11551 	movx	@dptr,a
                                  11552 ;	..\src\COMMON\easyax5043.c:2151: while (AX5043_POWSTAT & 0x08);
      0042CB                      11553 00119$:
      0042CB 90 40 03         [24]11554 	mov	dptr,#_AX5043_POWSTAT
      0042CE E0               [24]11555 	movx	a,@dptr
      0042CF FF               [12]11556 	mov	r7,a
      0042D0 20 E3 F8         [24]11557 	jb	acc.3,00119$
                                  11558 ;	..\src\COMMON\easyax5043.c:2152: ax5043_init_registers_tx();
      0042D3 12 16 97         [24]11559 	lcall	_ax5043_init_registers_tx
                                  11560 ;	..\src\COMMON\easyax5043.c:2153: goto dotx;
                                  11561 ;	..\src\COMMON\easyax5043.c:2158: case AXRADIO_MODE_WOR_ACK_TRANSMIT:
      0042D6 80 0B            [24]11562 	sjmp	00128$
      0042D8                      11563 00125$:
                                  11564 ;	..\src\COMMON\easyax5043.c:2159: if (axradio_syncstate != syncstate_off)
      0042D8 90 00 B7         [24]11565 	mov	dptr,#_axradio_syncstate
      0042DB E0               [24]11566 	movx	a,@dptr
      0042DC E0               [24]11567 	movx	a,@dptr
      0042DD 60 04            [24]11568 	jz	00128$
                                  11569 ;	..\src\COMMON\easyax5043.c:2160: return AXRADIO_ERR_BUSY;
      0042DF 75 82 02         [24]11570 	mov	dpl,#0x02
      0042E2 22               [24]11571 	ret
                                  11572 ;	..\src\COMMON\easyax5043.c:2161: dotx:
      0042E3                      11573 00128$:
                                  11574 ;	..\src\COMMON\easyax5043.c:2162: axradio_ack_count = axradio_framing_ack_retransmissions;
      0042E3 90 7A F1         [24]11575 	mov	dptr,#_axradio_framing_ack_retransmissions
      0042E6 E4               [12]11576 	clr	a
      0042E7 93               [24]11577 	movc	a,@a+dptr
      0042E8 90 00 C1         [24]11578 	mov	dptr,#_axradio_ack_count
      0042EB F0               [24]11579 	movx	@dptr,a
                                  11580 ;	..\src\COMMON\easyax5043.c:2163: ++axradio_ack_seqnr;
      0042EC 90 00 C2         [24]11581 	mov	dptr,#_axradio_ack_seqnr
      0042EF E0               [24]11582 	movx	a,@dptr
      0042F0 24 01            [12]11583 	add	a,#0x01
      0042F2 F0               [24]11584 	movx	@dptr,a
                                  11585 ;	..\src\COMMON\easyax5043.c:2164: axradio_txbuffer_len = pktlen + axradio_framing_maclen;
      0042F3 90 7A DA         [24]11586 	mov	dptr,#_axradio_framing_maclen
      0042F6 E4               [12]11587 	clr	a
      0042F7 93               [24]11588 	movc	a,@a+dptr
      0042F8 FF               [12]11589 	mov	r7,a
      0042F9 FD               [12]11590 	mov	r5,a
      0042FA 7E 00            [12]11591 	mov	r6,#0x00
      0042FC 90 03 74         [24]11592 	mov	dptr,#_axradio_transmit_PARM_3
      0042FF E0               [24]11593 	movx	a,@dptr
      004300 FB               [12]11594 	mov	r3,a
      004301 A3               [24]11595 	inc	dptr
      004302 E0               [24]11596 	movx	a,@dptr
      004303 FC               [12]11597 	mov	r4,a
      004304 ED               [12]11598 	mov	a,r5
      004305 2B               [12]11599 	add	a,r3
      004306 FD               [12]11600 	mov	r5,a
      004307 EE               [12]11601 	mov	a,r6
      004308 3C               [12]11602 	addc	a,r4
      004309 FE               [12]11603 	mov	r6,a
      00430A 90 00 B8         [24]11604 	mov	dptr,#_axradio_txbuffer_len
      00430D ED               [12]11605 	mov	a,r5
      00430E F0               [24]11606 	movx	@dptr,a
      00430F EE               [12]11607 	mov	a,r6
      004310 A3               [24]11608 	inc	dptr
      004311 F0               [24]11609 	movx	@dptr,a
                                  11610 ;	..\src\COMMON\easyax5043.c:2165: if (axradio_txbuffer_len > sizeof(axradio_txbuffer))
      004312 C3               [12]11611 	clr	c
      004313 74 04            [12]11612 	mov	a,#0x04
      004315 9D               [12]11613 	subb	a,r5
      004316 74 01            [12]11614 	mov	a,#0x01
      004318 9E               [12]11615 	subb	a,r6
      004319 50 04            [24]11616 	jnc	00130$
                                  11617 ;	..\src\COMMON\easyax5043.c:2166: return AXRADIO_ERR_INVALID;
      00431B 75 82 04         [24]11618 	mov	dpl,#0x04
      00431E 22               [24]11619 	ret
      00431F                      11620 00130$:
                                  11621 ;	..\src\COMMON\easyax5043.c:2167: memset_xdata(axradio_txbuffer, 0, axradio_framing_maclen);
      00431F 7E 00            [12]11622 	mov	r6,#0x00
      004321 90 03 CD         [24]11623 	mov	dptr,#_memset_PARM_2
      004324 E4               [12]11624 	clr	a
      004325 F0               [24]11625 	movx	@dptr,a
      004326 90 03 CE         [24]11626 	mov	dptr,#_memset_PARM_3
      004329 EF               [12]11627 	mov	a,r7
      00432A F0               [24]11628 	movx	@dptr,a
      00432B EE               [12]11629 	mov	a,r6
      00432C A3               [24]11630 	inc	dptr
      00432D F0               [24]11631 	movx	@dptr,a
      00432E 90 00 DD         [24]11632 	mov	dptr,#_axradio_txbuffer
      004331 75 F0 00         [24]11633 	mov	b,#0x00
      004334 C0 04            [24]11634 	push	ar4
      004336 C0 03            [24]11635 	push	ar3
      004338 12 62 A1         [24]11636 	lcall	_memset
      00433B D0 03            [24]11637 	pop	ar3
      00433D D0 04            [24]11638 	pop	ar4
                                  11639 ;	..\src\COMMON\easyax5043.c:2168: memcpy_xdatageneric(&axradio_txbuffer[axradio_framing_maclen], pkt, pktlen);
      00433F 90 7A DA         [24]11640 	mov	dptr,#_axradio_framing_maclen
      004342 E4               [12]11641 	clr	a
      004343 93               [24]11642 	movc	a,@a+dptr
      004344 24 DD            [12]11643 	add	a,#_axradio_txbuffer
      004346 FF               [12]11644 	mov	r7,a
      004347 E4               [12]11645 	clr	a
      004348 34 00            [12]11646 	addc	a,#(_axradio_txbuffer >> 8)
      00434A FE               [12]11647 	mov	r6,a
      00434B 7D 00            [12]11648 	mov	r5,#0x00
      00434D 90 03 71         [24]11649 	mov	dptr,#_axradio_transmit_PARM_2
      004350 E0               [24]11650 	movx	a,@dptr
      004351 F8               [12]11651 	mov	r0,a
      004352 A3               [24]11652 	inc	dptr
      004353 E0               [24]11653 	movx	a,@dptr
      004354 F9               [12]11654 	mov	r1,a
      004355 A3               [24]11655 	inc	dptr
      004356 E0               [24]11656 	movx	a,@dptr
      004357 FA               [12]11657 	mov	r2,a
      004358 90 03 E2         [24]11658 	mov	dptr,#_memcpy_PARM_2
      00435B E8               [12]11659 	mov	a,r0
      00435C F0               [24]11660 	movx	@dptr,a
      00435D E9               [12]11661 	mov	a,r1
      00435E A3               [24]11662 	inc	dptr
      00435F F0               [24]11663 	movx	@dptr,a
      004360 EA               [12]11664 	mov	a,r2
      004361 A3               [24]11665 	inc	dptr
      004362 F0               [24]11666 	movx	@dptr,a
      004363 90 03 E5         [24]11667 	mov	dptr,#_memcpy_PARM_3
      004366 EB               [12]11668 	mov	a,r3
      004367 F0               [24]11669 	movx	@dptr,a
      004368 EC               [12]11670 	mov	a,r4
      004369 A3               [24]11671 	inc	dptr
      00436A F0               [24]11672 	movx	@dptr,a
      00436B 8F 82            [24]11673 	mov	dpl,r7
      00436D 8E 83            [24]11674 	mov	dph,r6
      00436F 8D F0            [24]11675 	mov	b,r5
      004371 12 65 97         [24]11676 	lcall	_memcpy
                                  11677 ;	..\src\COMMON\easyax5043.c:2169: if (axradio_framing_ack_seqnrpos != 0xff)
      004374 90 7A F2         [24]11678 	mov	dptr,#_axradio_framing_ack_seqnrpos
      004377 E4               [12]11679 	clr	a
      004378 93               [24]11680 	movc	a,@a+dptr
      004379 FF               [12]11681 	mov	r7,a
      00437A BF FF 02         [24]11682 	cjne	r7,#0xff,00299$
      00437D 80 12            [24]11683 	sjmp	00132$
      00437F                      11684 00299$:
                                  11685 ;	..\src\COMMON\easyax5043.c:2170: axradio_txbuffer[axradio_framing_ack_seqnrpos] = axradio_ack_seqnr;
      00437F EF               [12]11686 	mov	a,r7
      004380 24 DD            [12]11687 	add	a,#_axradio_txbuffer
      004382 FF               [12]11688 	mov	r7,a
      004383 E4               [12]11689 	clr	a
      004384 34 00            [12]11690 	addc	a,#(_axradio_txbuffer >> 8)
      004386 FE               [12]11691 	mov	r6,a
      004387 90 00 C2         [24]11692 	mov	dptr,#_axradio_ack_seqnr
      00438A E0               [24]11693 	movx	a,@dptr
      00438B FD               [12]11694 	mov	r5,a
      00438C 8F 82            [24]11695 	mov	dpl,r7
      00438E 8E 83            [24]11696 	mov	dph,r6
      004390 F0               [24]11697 	movx	@dptr,a
      004391                      11698 00132$:
                                  11699 ;	..\src\COMMON\easyax5043.c:2171: if (axradio_framing_destaddrpos != 0xff)
      004391 90 7A DC         [24]11700 	mov	dptr,#_axradio_framing_destaddrpos
      004394 E4               [12]11701 	clr	a
      004395 93               [24]11702 	movc	a,@a+dptr
      004396 FF               [12]11703 	mov	r7,a
      004397 BF FF 02         [24]11704 	cjne	r7,#0xff,00300$
      00439A 80 39            [24]11705 	sjmp	00134$
      00439C                      11706 00300$:
                                  11707 ;	..\src\COMMON\easyax5043.c:2172: memcpy_xdatageneric(&axradio_txbuffer[axradio_framing_destaddrpos], &addr->addr, axradio_framing_addrlen);
      00439C EF               [12]11708 	mov	a,r7
      00439D 24 DD            [12]11709 	add	a,#_axradio_txbuffer
      00439F FF               [12]11710 	mov	r7,a
      0043A0 E4               [12]11711 	clr	a
      0043A1 34 00            [12]11712 	addc	a,#(_axradio_txbuffer >> 8)
      0043A3 FE               [12]11713 	mov	r6,a
      0043A4 7D 00            [12]11714 	mov	r5,#0x00
      0043A6 90 03 76         [24]11715 	mov	dptr,#_axradio_transmit_addr_1_438
      0043A9 E0               [24]11716 	movx	a,@dptr
      0043AA FA               [12]11717 	mov	r2,a
      0043AB A3               [24]11718 	inc	dptr
      0043AC E0               [24]11719 	movx	a,@dptr
      0043AD FB               [12]11720 	mov	r3,a
      0043AE A3               [24]11721 	inc	dptr
      0043AF E0               [24]11722 	movx	a,@dptr
      0043B0 FC               [12]11723 	mov	r4,a
      0043B1 90 7A DB         [24]11724 	mov	dptr,#_axradio_framing_addrlen
      0043B4 E4               [12]11725 	clr	a
      0043B5 93               [24]11726 	movc	a,@a+dptr
      0043B6 F8               [12]11727 	mov	r0,a
      0043B7 79 00            [12]11728 	mov	r1,#0x00
      0043B9 90 03 E2         [24]11729 	mov	dptr,#_memcpy_PARM_2
      0043BC EA               [12]11730 	mov	a,r2
      0043BD F0               [24]11731 	movx	@dptr,a
      0043BE EB               [12]11732 	mov	a,r3
      0043BF A3               [24]11733 	inc	dptr
      0043C0 F0               [24]11734 	movx	@dptr,a
      0043C1 EC               [12]11735 	mov	a,r4
      0043C2 A3               [24]11736 	inc	dptr
      0043C3 F0               [24]11737 	movx	@dptr,a
      0043C4 90 03 E5         [24]11738 	mov	dptr,#_memcpy_PARM_3
      0043C7 E8               [12]11739 	mov	a,r0
      0043C8 F0               [24]11740 	movx	@dptr,a
      0043C9 E9               [12]11741 	mov	a,r1
      0043CA A3               [24]11742 	inc	dptr
      0043CB F0               [24]11743 	movx	@dptr,a
      0043CC 8F 82            [24]11744 	mov	dpl,r7
      0043CE 8E 83            [24]11745 	mov	dph,r6
      0043D0 8D F0            [24]11746 	mov	b,r5
      0043D2 12 65 97         [24]11747 	lcall	_memcpy
      0043D5                      11748 00134$:
                                  11749 ;	..\src\COMMON\easyax5043.c:2173: if (axradio_framing_sourceaddrpos != 0xff)
      0043D5 90 7A DD         [24]11750 	mov	dptr,#_axradio_framing_sourceaddrpos
      0043D8 E4               [12]11751 	clr	a
      0043D9 93               [24]11752 	movc	a,@a+dptr
      0043DA FF               [12]11753 	mov	r7,a
      0043DB BF FF 02         [24]11754 	cjne	r7,#0xff,00301$
      0043DE 80 30            [24]11755 	sjmp	00136$
      0043E0                      11756 00301$:
                                  11757 ;	..\src\COMMON\easyax5043.c:2174: memcpy_xdata(&axradio_txbuffer[axradio_framing_sourceaddrpos], &axradio_localaddr.addr, axradio_framing_addrlen);
      0043E0 EF               [12]11758 	mov	a,r7
      0043E1 24 DD            [12]11759 	add	a,#_axradio_txbuffer
      0043E3 FF               [12]11760 	mov	r7,a
      0043E4 E4               [12]11761 	clr	a
      0043E5 34 00            [12]11762 	addc	a,#(_axradio_txbuffer >> 8)
      0043E7 FE               [12]11763 	mov	r6,a
      0043E8 7D 00            [12]11764 	mov	r5,#0x00
      0043EA 90 7A DB         [24]11765 	mov	dptr,#_axradio_framing_addrlen
      0043ED E4               [12]11766 	clr	a
      0043EE 93               [24]11767 	movc	a,@a+dptr
      0043EF FC               [12]11768 	mov	r4,a
      0043F0 7B 00            [12]11769 	mov	r3,#0x00
      0043F2 90 03 E2         [24]11770 	mov	dptr,#_memcpy_PARM_2
      0043F5 74 D1            [12]11771 	mov	a,#_axradio_localaddr
      0043F7 F0               [24]11772 	movx	@dptr,a
      0043F8 74 00            [12]11773 	mov	a,#(_axradio_localaddr >> 8)
      0043FA A3               [24]11774 	inc	dptr
      0043FB F0               [24]11775 	movx	@dptr,a
      0043FC E4               [12]11776 	clr	a
      0043FD A3               [24]11777 	inc	dptr
      0043FE F0               [24]11778 	movx	@dptr,a
      0043FF 90 03 E5         [24]11779 	mov	dptr,#_memcpy_PARM_3
      004402 EC               [12]11780 	mov	a,r4
      004403 F0               [24]11781 	movx	@dptr,a
      004404 EB               [12]11782 	mov	a,r3
      004405 A3               [24]11783 	inc	dptr
      004406 F0               [24]11784 	movx	@dptr,a
      004407 8F 82            [24]11785 	mov	dpl,r7
      004409 8E 83            [24]11786 	mov	dph,r6
      00440B 8D F0            [24]11787 	mov	b,r5
      00440D 12 65 97         [24]11788 	lcall	_memcpy
      004410                      11789 00136$:
                                  11790 ;	..\src\COMMON\easyax5043.c:2175: if (axradio_framing_lenmask) {
      004410 90 7A E0         [24]11791 	mov	dptr,#_axradio_framing_lenmask
      004413 E4               [12]11792 	clr	a
      004414 93               [24]11793 	movc	a,@a+dptr
      004415 FF               [12]11794 	mov	r7,a
      004416 60 30            [24]11795 	jz	00138$
                                  11796 ;	..\src\COMMON\easyax5043.c:2176: uint8_t __autodata len_byte = (uint8_t)(axradio_txbuffer_len - axradio_framing_lenoffs) & axradio_framing_lenmask; // if you prefer not counting the len byte itself, set LENOFFS = 1
      004418 90 00 B8         [24]11797 	mov	dptr,#_axradio_txbuffer_len
      00441B E0               [24]11798 	movx	a,@dptr
      00441C FD               [12]11799 	mov	r5,a
      00441D A3               [24]11800 	inc	dptr
      00441E E0               [24]11801 	movx	a,@dptr
      00441F 90 7A DF         [24]11802 	mov	dptr,#_axradio_framing_lenoffs
      004422 E4               [12]11803 	clr	a
      004423 93               [24]11804 	movc	a,@a+dptr
      004424 FE               [12]11805 	mov	r6,a
      004425 ED               [12]11806 	mov	a,r5
      004426 C3               [12]11807 	clr	c
      004427 9E               [12]11808 	subb	a,r6
      004428 5F               [12]11809 	anl	a,r7
      004429 FE               [12]11810 	mov	r6,a
                                  11811 ;	..\src\COMMON\easyax5043.c:2177: axradio_txbuffer[axradio_framing_lenpos] = (axradio_txbuffer[axradio_framing_lenpos] & (uint8_t)~axradio_framing_lenmask) | len_byte;
      00442A 90 7A DE         [24]11812 	mov	dptr,#_axradio_framing_lenpos
      00442D E4               [12]11813 	clr	a
      00442E 93               [24]11814 	movc	a,@a+dptr
      00442F 24 DD            [12]11815 	add	a,#_axradio_txbuffer
      004431 FD               [12]11816 	mov	r5,a
      004432 E4               [12]11817 	clr	a
      004433 34 00            [12]11818 	addc	a,#(_axradio_txbuffer >> 8)
      004435 FC               [12]11819 	mov	r4,a
      004436 8D 82            [24]11820 	mov	dpl,r5
      004438 8C 83            [24]11821 	mov	dph,r4
      00443A E0               [24]11822 	movx	a,@dptr
      00443B FB               [12]11823 	mov	r3,a
      00443C EF               [12]11824 	mov	a,r7
      00443D F4               [12]11825 	cpl	a
      00443E FF               [12]11826 	mov	r7,a
      00443F 5B               [12]11827 	anl	a,r3
      004440 42 06            [12]11828 	orl	ar6,a
      004442 8D 82            [24]11829 	mov	dpl,r5
      004444 8C 83            [24]11830 	mov	dph,r4
      004446 EE               [12]11831 	mov	a,r6
      004447 F0               [24]11832 	movx	@dptr,a
      004448                      11833 00138$:
                                  11834 ;	..\src\COMMON\easyax5043.c:2179: if (axradio_framing_swcrclen)
      004448 90 7A E1         [24]11835 	mov	dptr,#_axradio_framing_swcrclen
      00444B E4               [12]11836 	clr	a
      00444C 93               [24]11837 	movc	a,@a+dptr
      00444D 60 20            [24]11838 	jz	00140$
                                  11839 ;	..\src\COMMON\easyax5043.c:2180: axradio_txbuffer_len = axradio_framing_append_crc(axradio_txbuffer, axradio_txbuffer_len);
      00444F 90 00 B8         [24]11840 	mov	dptr,#_axradio_txbuffer_len
      004452 E0               [24]11841 	movx	a,@dptr
      004453 C0 E0            [24]11842 	push	acc
      004455 A3               [24]11843 	inc	dptr
      004456 E0               [24]11844 	movx	a,@dptr
      004457 C0 E0            [24]11845 	push	acc
      004459 90 00 DD         [24]11846 	mov	dptr,#_axradio_txbuffer
      00445C 12 0A A6         [24]11847 	lcall	_axradio_framing_append_crc
      00445F AE 82            [24]11848 	mov	r6,dpl
      004461 AF 83            [24]11849 	mov	r7,dph
      004463 15 81            [12]11850 	dec	sp
      004465 15 81            [12]11851 	dec	sp
      004467 90 00 B8         [24]11852 	mov	dptr,#_axradio_txbuffer_len
      00446A EE               [12]11853 	mov	a,r6
      00446B F0               [24]11854 	movx	@dptr,a
      00446C EF               [12]11855 	mov	a,r7
      00446D A3               [24]11856 	inc	dptr
      00446E F0               [24]11857 	movx	@dptr,a
      00446F                      11858 00140$:
                                  11859 ;	..\src\COMMON\easyax5043.c:2181: if (axradio_phy_pn9)
      00446F 90 7A B9         [24]11860 	mov	dptr,#_axradio_phy_pn9
      004472 E4               [12]11861 	clr	a
      004473 93               [24]11862 	movc	a,@a+dptr
      004474 60 2F            [24]11863 	jz	00142$
                                  11864 ;	..\src\COMMON\easyax5043.c:2182: pn9_buffer(axradio_txbuffer, axradio_txbuffer_len, 0x1ff, -(AX5043_ENCODING & 0x01));
      004476 90 40 11         [24]11865 	mov	dptr,#_AX5043_ENCODING
      004479 E0               [24]11866 	movx	a,@dptr
      00447A FF               [12]11867 	mov	r7,a
      00447B 53 07 01         [24]11868 	anl	ar7,#0x01
      00447E C3               [12]11869 	clr	c
      00447F E4               [12]11870 	clr	a
      004480 9F               [12]11871 	subb	a,r7
      004481 FF               [12]11872 	mov	r7,a
      004482 C0 07            [24]11873 	push	ar7
      004484 74 FF            [12]11874 	mov	a,#0xff
      004486 C0 E0            [24]11875 	push	acc
      004488 74 01            [12]11876 	mov	a,#0x01
      00448A C0 E0            [24]11877 	push	acc
      00448C 90 00 B8         [24]11878 	mov	dptr,#_axradio_txbuffer_len
      00448F E0               [24]11879 	movx	a,@dptr
      004490 C0 E0            [24]11880 	push	acc
      004492 A3               [24]11881 	inc	dptr
      004493 E0               [24]11882 	movx	a,@dptr
      004494 C0 E0            [24]11883 	push	acc
      004496 90 00 DD         [24]11884 	mov	dptr,#_axradio_txbuffer
      004499 75 F0 00         [24]11885 	mov	b,#0x00
      00449C 12 67 8E         [24]11886 	lcall	_pn9_buffer
      00449F E5 81            [12]11887 	mov	a,sp
      0044A1 24 FB            [12]11888 	add	a,#0xfb
      0044A3 F5 81            [12]11889 	mov	sp,a
      0044A5                      11890 00142$:
                                  11891 ;	..\src\COMMON\easyax5043.c:2183: if (axradio_mode == AXRADIO_MODE_SYNC_MASTER ||
      0044A5 74 30            [12]11892 	mov	a,#0x30
      0044A7 B5 0B 02         [24]11893 	cjne	a,_axradio_mode,00305$
      0044AA 80 05            [24]11894 	sjmp	00143$
      0044AC                      11895 00305$:
                                  11896 ;	..\src\COMMON\easyax5043.c:2184: axradio_mode == AXRADIO_MODE_SYNC_ACK_MASTER)
      0044AC 74 31            [12]11897 	mov	a,#0x31
      0044AE B5 0B 04         [24]11898 	cjne	a,_axradio_mode,00144$
      0044B1                      11899 00143$:
                                  11900 ;	..\src\COMMON\easyax5043.c:2185: return AXRADIO_ERR_NOERROR;
      0044B1 75 82 00         [24]11901 	mov	dpl,#0x00
      0044B4 22               [24]11902 	ret
      0044B5                      11903 00144$:
                                  11904 ;	..\src\COMMON\easyax5043.c:2186: if (axradio_mode == AXRADIO_MODE_WOR_TRANSMIT ||
      0044B5 74 11            [12]11905 	mov	a,#0x11
      0044B7 B5 0B 02         [24]11906 	cjne	a,_axradio_mode,00308$
      0044BA 80 05            [24]11907 	sjmp	00146$
      0044BC                      11908 00308$:
                                  11909 ;	..\src\COMMON\easyax5043.c:2187: axradio_mode == AXRADIO_MODE_WOR_ACK_TRANSMIT)
      0044BC 74 13            [12]11910 	mov	a,#0x13
      0044BE B5 0B 14         [24]11911 	cjne	a,_axradio_mode,00147$
      0044C1                      11912 00146$:
                                  11913 ;	..\src\COMMON\easyax5043.c:2188: axradio_txbuffer_cnt = axradio_phy_preamble_wor_longlen;
      0044C1 90 7A CE         [24]11914 	mov	dptr,#_axradio_phy_preamble_wor_longlen
      0044C4 E4               [12]11915 	clr	a
      0044C5 93               [24]11916 	movc	a,@a+dptr
      0044C6 FE               [12]11917 	mov	r6,a
      0044C7 74 01            [12]11918 	mov	a,#0x01
      0044C9 93               [24]11919 	movc	a,@a+dptr
      0044CA FF               [12]11920 	mov	r7,a
      0044CB 90 00 BA         [24]11921 	mov	dptr,#_axradio_txbuffer_cnt
      0044CE EE               [12]11922 	mov	a,r6
      0044CF F0               [24]11923 	movx	@dptr,a
      0044D0 EF               [12]11924 	mov	a,r7
      0044D1 A3               [24]11925 	inc	dptr
      0044D2 F0               [24]11926 	movx	@dptr,a
      0044D3 80 12            [24]11927 	sjmp	00148$
      0044D5                      11928 00147$:
                                  11929 ;	..\src\COMMON\easyax5043.c:2190: axradio_txbuffer_cnt = axradio_phy_preamble_longlen;
      0044D5 90 7A D2         [24]11930 	mov	dptr,#_axradio_phy_preamble_longlen
      0044D8 E4               [12]11931 	clr	a
      0044D9 93               [24]11932 	movc	a,@a+dptr
      0044DA FE               [12]11933 	mov	r6,a
      0044DB 74 01            [12]11934 	mov	a,#0x01
      0044DD 93               [24]11935 	movc	a,@a+dptr
      0044DE FF               [12]11936 	mov	r7,a
      0044DF 90 00 BA         [24]11937 	mov	dptr,#_axradio_txbuffer_cnt
      0044E2 EE               [12]11938 	mov	a,r6
      0044E3 F0               [24]11939 	movx	@dptr,a
      0044E4 EF               [12]11940 	mov	a,r7
      0044E5 A3               [24]11941 	inc	dptr
      0044E6 F0               [24]11942 	movx	@dptr,a
      0044E7                      11943 00148$:
                                  11944 ;	..\src\COMMON\easyax5043.c:2191: if (axradio_phy_lbt_retries) {
      0044E7 90 7A CC         [24]11945 	mov	dptr,#_axradio_phy_lbt_retries
      0044EA E4               [12]11946 	clr	a
      0044EB 93               [24]11947 	movc	a,@a+dptr
      0044EC 70 03            [24]11948 	jnz	00311$
      0044EE 02 45 69         [24]11949 	ljmp	00161$
      0044F1                      11950 00311$:
                                  11951 ;	..\src\COMMON\easyax5043.c:2192: switch (axradio_mode) {
      0044F1 AF 0B            [24]11952 	mov	r7,_axradio_mode
      0044F3 BF 10 02         [24]11953 	cjne	r7,#0x10,00312$
      0044F6 80 21            [24]11954 	sjmp	00157$
      0044F8                      11955 00312$:
      0044F8 BF 11 02         [24]11956 	cjne	r7,#0x11,00313$
      0044FB 80 1C            [24]11957 	sjmp	00157$
      0044FD                      11958 00313$:
      0044FD BF 12 02         [24]11959 	cjne	r7,#0x12,00314$
      004500 80 17            [24]11960 	sjmp	00157$
      004502                      11961 00314$:
      004502 BF 13 02         [24]11962 	cjne	r7,#0x13,00315$
      004505 80 12            [24]11963 	sjmp	00157$
      004507                      11964 00315$:
      004507 BF 20 02         [24]11965 	cjne	r7,#0x20,00316$
      00450A 80 0D            [24]11966 	sjmp	00157$
      00450C                      11967 00316$:
      00450C BF 21 02         [24]11968 	cjne	r7,#0x21,00317$
      00450F 80 08            [24]11969 	sjmp	00157$
      004511                      11970 00317$:
      004511 BF 22 02         [24]11971 	cjne	r7,#0x22,00318$
      004514 80 03            [24]11972 	sjmp	00157$
      004516                      11973 00318$:
      004516 BF 23 50         [24]11974 	cjne	r7,#0x23,00161$
                                  11975 ;	..\src\COMMON\easyax5043.c:2200: case AXRADIO_MODE_ACK_RECEIVE:
      004519                      11976 00157$:
                                  11977 ;	..\src\COMMON\easyax5043.c:2201: ax5043_off_xtal();
      004519 12 22 D7         [24]11978 	lcall	_ax5043_off_xtal
                                  11979 ;	..\src\COMMON\easyax5043.c:2202: ax5043_init_registers_rx();
      00451C 12 16 9D         [24]11980 	lcall	_ax5043_init_registers_rx
                                  11981 ;	..\src\COMMON\easyax5043.c:2203: AX5043_RSSIREFERENCE = axradio_phy_rssireference;
      00451F 90 7A C7         [24]11982 	mov	dptr,#_axradio_phy_rssireference
      004522 E4               [12]11983 	clr	a
      004523 93               [24]11984 	movc	a,@a+dptr
      004524 90 42 2C         [24]11985 	mov	dptr,#_AX5043_RSSIREFERENCE
      004527 F0               [24]11986 	movx	@dptr,a
                                  11987 ;	..\src\COMMON\easyax5043.c:2204: AX5043_PWRMODE = AX5043_PWRSTATE_FULL_RX;
      004528 90 40 02         [24]11988 	mov	dptr,#_AX5043_PWRMODE
      00452B 74 09            [12]11989 	mov	a,#0x09
      00452D F0               [24]11990 	movx	@dptr,a
                                  11991 ;	..\src\COMMON\easyax5043.c:2205: axradio_ack_count = axradio_phy_lbt_retries;
      00452E 90 7A CC         [24]11992 	mov	dptr,#_axradio_phy_lbt_retries
      004531 E4               [12]11993 	clr	a
      004532 93               [24]11994 	movc	a,@a+dptr
      004533 90 00 C1         [24]11995 	mov	dptr,#_axradio_ack_count
      004536 F0               [24]11996 	movx	@dptr,a
                                  11997 ;	..\src\COMMON\easyax5043.c:2206: axradio_syncstate = syncstate_lbt;
      004537 90 00 B7         [24]11998 	mov	dptr,#_axradio_syncstate
      00453A 74 01            [12]11999 	mov	a,#0x01
      00453C F0               [24]12000 	movx	@dptr,a
                                  12001 ;	..\src\COMMON\easyax5043.c:2207: wtimer_remove(&axradio_timer);
      00453D 90 03 3C         [24]12002 	mov	dptr,#_axradio_timer
      004540 12 71 84         [24]12003 	lcall	_wtimer_remove
                                  12004 ;	..\src\COMMON\easyax5043.c:2208: axradio_timer.time = axradio_phy_cs_period;
      004543 90 7A C9         [24]12005 	mov	dptr,#_axradio_phy_cs_period
      004546 E4               [12]12006 	clr	a
      004547 93               [24]12007 	movc	a,@a+dptr
      004548 FE               [12]12008 	mov	r6,a
      004549 74 01            [12]12009 	mov	a,#0x01
      00454B 93               [24]12010 	movc	a,@a+dptr
      00454C FF               [12]12011 	mov	r7,a
      00454D 7D 00            [12]12012 	mov	r5,#0x00
      00454F 7C 00            [12]12013 	mov	r4,#0x00
      004551 90 03 40         [24]12014 	mov	dptr,#(_axradio_timer + 0x0004)
      004554 EE               [12]12015 	mov	a,r6
      004555 F0               [24]12016 	movx	@dptr,a
      004556 EF               [12]12017 	mov	a,r7
      004557 A3               [24]12018 	inc	dptr
      004558 F0               [24]12019 	movx	@dptr,a
      004559 ED               [12]12020 	mov	a,r5
      00455A A3               [24]12021 	inc	dptr
      00455B F0               [24]12022 	movx	@dptr,a
      00455C EC               [12]12023 	mov	a,r4
      00455D A3               [24]12024 	inc	dptr
      00455E F0               [24]12025 	movx	@dptr,a
                                  12026 ;	..\src\COMMON\easyax5043.c:2209: wtimer0_addrelative(&axradio_timer);
      00455F 90 03 3C         [24]12027 	mov	dptr,#_axradio_timer
      004562 12 66 25         [24]12028 	lcall	_wtimer0_addrelative
                                  12029 ;	..\src\COMMON\easyax5043.c:2210: return AXRADIO_ERR_NOERROR;
      004565 75 82 00         [24]12030 	mov	dpl,#0x00
                                  12031 ;	..\src\COMMON\easyax5043.c:2214: }
      004568 22               [24]12032 	ret
      004569                      12033 00161$:
                                  12034 ;	..\src\COMMON\easyax5043.c:2216: axradio_syncstate = syncstate_asynctx;
      004569 90 00 B7         [24]12035 	mov	dptr,#_axradio_syncstate
      00456C 74 02            [12]12036 	mov	a,#0x02
      00456E F0               [24]12037 	movx	@dptr,a
                                  12038 ;	..\src\COMMON\easyax5043.c:2217: ax5043_prepare_tx();
      00456F 12 22 A5         [24]12039 	lcall	_ax5043_prepare_tx
                                  12040 ;	..\src\COMMON\easyax5043.c:2218: return AXRADIO_ERR_NOERROR;
      004572 75 82 00         [24]12041 	mov	dpl,#0x00
                                  12042 ;	..\src\COMMON\easyax5043.c:2220: default:
      004575 22               [24]12043 	ret
      004576                      12044 00162$:
                                  12045 ;	..\src\COMMON\easyax5043.c:2221: return AXRADIO_ERR_NOTSUPPORTED;
      004576 75 82 01         [24]12046 	mov	dpl,#0x01
                                  12047 ;	..\src\COMMON\easyax5043.c:2222: }
      004579 22               [24]12048 	ret
                                  12049 ;------------------------------------------------------------
                                  12050 ;Allocation info for local variables in function 'axradio_set_paramsets'
                                  12051 ;------------------------------------------------------------
                                  12052 ;val                       Allocated to registers r7 
                                  12053 ;------------------------------------------------------------
                                  12054 ;	..\src\COMMON\easyax5043.c:2225: static __reentrantb uint8_t axradio_set_paramsets(uint8_t val) __reentrant
                                  12055 ;	-----------------------------------------
                                  12056 ;	 function axradio_set_paramsets
                                  12057 ;	-----------------------------------------
      00457A                      12058 _axradio_set_paramsets:
      00457A AF 82            [24]12059 	mov	r7,dpl
                                  12060 ;	..\src\COMMON\easyax5043.c:2227: if (!AXRADIO_MODE_IS_STREAM_RECEIVE(axradio_mode))
      00457C 74 F8            [12]12061 	mov	a,#0xf8
      00457E 55 0B            [12]12062 	anl	a,_axradio_mode
      004580 FE               [12]12063 	mov	r6,a
      004581 BE 28 02         [24]12064 	cjne	r6,#0x28,00108$
      004584 80 04            [24]12065 	sjmp	00102$
      004586                      12066 00108$:
                                  12067 ;	..\src\COMMON\easyax5043.c:2228: return AXRADIO_ERR_NOTSUPPORTED;
      004586 75 82 01         [24]12068 	mov	dpl,#0x01
      004589 22               [24]12069 	ret
      00458A                      12070 00102$:
                                  12071 ;	..\src\COMMON\easyax5043.c:2229: AX5043_RXPARAMSETS = val;
      00458A 90 41 17         [24]12072 	mov	dptr,#_AX5043_RXPARAMSETS
      00458D EF               [12]12073 	mov	a,r7
      00458E F0               [24]12074 	movx	@dptr,a
                                  12075 ;	..\src\COMMON\easyax5043.c:2230: return AXRADIO_ERR_NOERROR;
      00458F 75 82 00         [24]12076 	mov	dpl,#0x00
      004592 22               [24]12077 	ret
                                  12078 	.area CSEG    (CODE)
                                  12079 	.area CONST   (CODE)
      007C2B                      12080 ___str_0:
      007C2B 20 72 65 63 65 69 76 12081 	.ascii " receive_isr "
             65 5F 69 73 72 20
      007C38 00                   12082 	.db 0x00
      007C39                      12083 ___str_1:
      007C39 20 74 69 6D 65 20 61 12084 	.ascii " time anchor "
             6E 63 68 6F 72 20
      007C46 00                   12085 	.db 0x00
      007C47                      12086 ___str_2:
      007C47 20 65 6E 61 62 6C 65 12087 	.ascii " enable_sfdcallback "
             5F 73 66 64 63 61 6C
             6C 62 61 63 6B 20
      007C5B 00                   12088 	.db 0x00
      007C5C                      12089 ___str_3:
      007C5C 43 48 20             12090 	.ascii "CH "
      007C5F 00                   12091 	.db 0x00
      007C60                      12092 ___str_4:
      007C60 20 52 4E 47 20       12093 	.ascii " RNG "
      007C65 00                   12094 	.db 0x00
      007C66                      12095 ___str_5:
      007C66 20 56 43 4F 49 20    12096 	.ascii " VCOI "
      007C6C 00                   12097 	.db 0x00
      007C6D                      12098 ___str_6:
      007C6D 20 2A                12099 	.ascii " *"
      007C6F 00                   12100 	.db 0x00
                                  12101 	.area XINIT   (CODE)
      0081AD                      12102 __xinit__f30_saved:
      0081AD 3F                   12103 	.db #0x3f	; 63
      0081AE                      12104 __xinit__f31_saved:
      0081AE F0                   12105 	.db #0xf0	; 240
      0081AF                      12106 __xinit__f32_saved:
      0081AF 3F                   12107 	.db #0x3f	; 63
      0081B0                      12108 __xinit__f33_saved:
      0081B0 F0                   12109 	.db #0xf0	; 240
                                  12110 	.area CABS    (ABS,CODE)
