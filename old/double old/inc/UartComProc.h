#include <libmftypes.h>
#include <libmfuart0.h>
#include <libmfuart1.h>
#include <libmfuart.h>
#include <stdlib.h>
#include <string.h>
#include <libmfdbglink.h>
#include <ax8052f143.h>
#include "misc.h"


//KISS protocol defines
#define FRAME_START 0xC0
#define FRAME_END 0xC0
#define FRAME_SCAPE 0xDB
#define TRANSPOSE_FRAME_END 0xDC
#define TRANSPOSE_FRAME_ESCAPE 0xDD

//Function returns
#define ERR_NO_DATA 0
#define ERR_BAD_CRC 1
#define ERR_BAD_FORMAT 2
#define ERR_BAD_ENDING 3
#define RX_SUCCESFULL  4

//magic numbers
#define CRC_LENGTH 4
#define START_CHARACTER_LENGTH 1
#define END_CHARACTER_LENGTH 1
#define COMMAND_LENGTH 1

void UART_Proc_PortInit(void);

uint8_t UART_Proc_VerifyIncomingMsg(uint8_t *cmd, uint8_t *payload,uint8_t *PayloadLength);

uint8_t UART_Proc_SendMessage(uint8_t *payload, uint8_t length, uint8_t command);

void UART_Proc_ModifiyKissSpecialCharacters(uint8_t *UartRxBuffer, uint8_t *RxLength);

uint32_t UART_Calc_CRC32(uint8_t *message,uint8_t length,uint8_t littleEndian);

