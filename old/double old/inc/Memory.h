

#define MEM_SOFT_CONFIG_START       0x0
#define MEM_SOFT_COFIG_END          0x3FF
#define MEM_CRYPTOGRAPHY_KEYS_START 0x400
#define MEM_CRYPTOGRAPHY_KEYS_END   0x7FF
#define MEM_HMAC_KEYS_START         0x800
#define MEM_HMAC_KEYS_END           0xBFF
#define MEM_GOLD_SEQUENCES_START    0xC00
#define MEM_GOLD_SEQUENCES_END      0xFFF
#define MEM_GSP_MESSAGES_START      0x1000
#define MEM_GPS_MESSAGES_END        0x13FF
#define MEM_GPS_DATA_START          0x1400
#define MEM_GPS_DATA_END            0x17FF
#define MEM_PROCD_DATA_START        0x1800
#define MEM_PROCD_DATA_END          0x1BFF

typedef unsigned char bool;
enum Memory_pages
{
e_Software_Config,
e_Cryptograpy_keys,
e_HMAC_Keys,
e_Gold_Sequences,
e_GPS_Messages,
e_GPS_Data,
e_ProcD_Data
};

void ReadFromMemory(uint16_t *RxBuffer, uint16_t address);
void WriteMemory(uint16_t address, uint16_t data, bool erase_flag);
