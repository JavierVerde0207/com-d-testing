/** \file
*
*   \brief Main block for AMICS COM-D firmware.
*
*   AMICS COM-D firmware. This firmware will be in charge of receiving beacons from the satellite, process them transmit
*   a packet to the satellite if needing while implementing CRDSA protocol. It will have communications capacities with the
*   PROC-D teriminal, and GPS module.
*
*   \author Javier Verde
*   \author Aistech Space
*
*/


#include "../AX_Radio_Lab_output/configmaster.h"
#include <ax8052f143.h>
#include <libmftypes.h>
#include <libmfradio.h>
#include <libmfflash.h>
#include <libmfwtimer.h>
#include <libmfcrypto.h>
#include "../COMMON/UBLOX.h"
#include <string.h>
#include "../COMMON/easyax5043.h"
#include "../COMMON/UartComProc.h"
#include <stdlib.h>
#include "../COMMON/misc.h"
#include "../COMMON/GoldSequence.h"
#include  "../COMMON/Beacon.h"
#include <libmfdbglink.h>
#include "../COMMON/display_com0.h"
#include <libdvk2leds.h>
#include "../COMMON/misc.h"
#include "../COMMON/configcommon.h"




//Main software variables

uint8_t RfStateMachine = e_WaitForBeaconAndMessageSend; /**< \brief RF state machine actual state */
int RxIsrFlag; /**< \brief Rx interruption flag */
RF_beacon_t BeaconRx;/**< \brief Received beacon processed */
uint16_t __xdata *RxBuffer;/**<\brief  Verify this variable */
uint8_t i;/**< \brief counter */
uint8_t Tmr0Flg = 0;/**< \brief timer0 activation flag */
#if defined(SDCC)
extern uint8_t _start__stack[];
#endif
uint8_t counter = 0;/**< \brief counter */
uint16_t __data pkt_counter = 0;/**< \brief amount of packets received */
uint8_t __data coldstart = 1; /**< \brief Cold start indicator*/ // caution: initialization with 1 is necessary! Variables are initialized upon _sdcc_external_startup returning 0 -> the coldstart value returned from _sdcc_external startup does not survive in the coldstart case



static void pwrmgmt_irq(void) __interrupt(INT_POWERMGMT)
{
    uint8_t pc = PCON;
    if (!(pc & 0x80))
        return;
    GPIOENABLE = 0;
    IE = EIE = E2IE = 0;
    for (;;)
        PCON |= 0x01;
}

static void transmit_packet(void)
{
    static uint8_t i = 0;
    static uint8_t __xdata demo_packet_[sizeof(demo_packet)];
    dbglink_writestr("Tx");
    axradio_set_mode(AXRADIO_MODE_ASYNC_TRANSMIT);
    ++pkt_counter;
    memcpy(demo_packet_, demo_packet, sizeof(demo_packet));
    if (framing_insert_counter) {
        demo_packet_[framing_counter_pos] = pkt_counter & 0xFF ;
        demo_packet_[framing_counter_pos+1] = (pkt_counter>>8) & 0xFF;
    }
       axradio_transmit(&remoteaddr, demo_packet_, sizeof(demo_packet));

}


void axradio_statuschange(struct axradio_status __xdata *st)
{

    int i = 0;
    switch (st->status)
    {
    case AXRADIO_STAT_TRANSMITSTART:
        led0_on();
        if (st->error == AXRADIO_ERR_RETRANSMISSION)
            led2_on();
        break;

    case AXRADIO_STAT_TRANSMITEND:
        led0_off();
        if (st->error == AXRADIO_ERR_NOERROR) {
            led2_off();

        } else if (st->error == AXRADIO_ERR_TIMEOUT) {
            led2_on();

        }
        if (st->error == AXRADIO_ERR_BUSY)
            led3_on();
        else
            led3_off();
        break;

#if RADIO_MODE == AXRADIO_MODE_SYNC_MASTER || RADIO_MODE == AXRADIO_MODE_SYNC_ACK_MASTER
    case AXRADIO_STAT_TRANSMITDATA:
        // in SYNC_MASTER mode, transmit data may be prepared between the call to TRANSMITEND until the call to TRANSMITSTART
        // TRANSMITDATA is called when the crystal oscillator is enabled, approximately 1ms before transmission
        transmit_packet();
        break;
#endif

    case AXRADIO_STAT_CHANNELSTATE:
        if (st->u.cs.busy)
            led3_on();
        else
            led3_off();
        break;

    default:
        break;
    }
}




uint8_t _sdcc_external_startup(void)
{
    LPXOSCGM = 0x8A;
    wtimer0_setclksrc(WTIMER0_CLKSRC, WTIMER0_PRESCALER);
    wtimer1_setclksrc(CLKSRC_FRCOSC, 7);

    LPOSCCONFIG = 0x09; // Slow, PRESC /1, no cal. Does NOT enable LPOSC. LPOSC is enabled upon configuring WTCFGA (MODE_TX_PERIODIC and receive_ack() )

    coldstart = !(PCON & 0x40);

    ANALOGA = 0x18; // PA[3,4] LPXOSC, other PA are used as digital pins
    PORTA = 0xFF; //
    PORTB = 0xFD | (PINB & 0x02); // init LEDs to previous (frozen) state
    PORTC = 0xFF; //
    PORTR = 0x0B; //
    DIRA = 0x00; //
    DIRB = 0x0e; //  PB1 = LED; PB2 / PB3 are outputs (in case PWRAMP / ANSTSEL are used)
    DIRC = 0x00; //  PC4 = button
    DIRR = 0x15; //

//    RNGMODE = 0x0F;
 //   RNGCLKSRC0 = 0x08;
 //   RNGCLKSRC1 = 0x00;
   axradio_setup_pincfg1();

    DPS = 0;
    IE = 0x40;
    EIE = 0x10;//habilito interrupcion de UART
    E2IE = 0x00;// Keep random number generation interrupt disabled, if needed a random number just check the variable, and wait for it
    UBLOX_GPS_PortInit();
    UART_Proc_PortInit();
    flash_pageerase(0x02); // for memory init, if its done after in the code it does not work
    //display_portinit();
    GPIOENABLE = 1; // unfreeze GPIO
#if defined(__ICC8051__)
    return coldstart;
#else
    return !coldstart; // coldstart -> return 0 -> var initialization; start from sleep -> return 1 -> no var initialization
#endif
}
/** \brief Main function
*
*   Program flow will be controled from this function
**/
void main(void)
{
// Variables declaration


    volatile uint8_t    PROCD_PayloadLength= 0; /**< \var length of the payload received from PROC-D*/
    volatile uint8_t __xdata *PROCD_UartRxBuffer;/**< \brief PROC-D UART interface buffer */
    uint8_t PROCD_Command;/**< \brief Command word received from PROC-D*/
    uint8_t i;
    extern int RxIsrFlag;
    uint8_t c=0;/**< \brief counter */
    extern RF_beacon_t BeaconRx;
__xdata uint8_t  UBLOX_setNav1[]= {0xF0 ,0x04 ,0x00};/**< \brief Initial cofiguration for UBLOX GPS*/
__xdata uint8_t  UBLOX_setNav2[]= {0xF0, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01}; /**< \brief Initial cofiguration for UBLOX GPS*/
__xdata uint8_t  UBLOX_setNav3[] = {0xF0, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01};/**< \brief Initial cofiguration for UBLOX GPS*/
__xdata uint8_t  UBLOX_setNav5[] ={0xFF, 0xFF, 0x06, 0x03, 0x00, 0x00, 0x00, 0x00, 0x10, 0x27, 0x00, 0x00,
    0x05, 0x00, 0xFA, 0x00, 0xFA, 0x00, 0x64, 0x00, 0x2C, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00};/**< \brief Initial cofiguration for UBLOX GPS*/
__xdata uint16_t UBLOX_Rx_Length = 0;/**< \brief UBLOX GPS received data length */
__xdata uint8_t  *UBLOX_Rx_Payload;/**< \brief UBLOX GPS payload */

//End of variable declarations



#if !defined(SDCC) && !defined(__ICC8051__)
    _sdcc_external_startup();
#endif
#if defined(SDCC)
    __asm
    G$_start__stack$0$0 = __start__stack
                          .globl G$_start__stack$0$0
                          __endasm;
#endif
    RxBuffer =(uint16_t*) malloc(sizeof(uint16_t)*20);
    IE_4 = 1;
    EA = 1;
 //   flash_apply_calibration();
    CLKCON = 0x00;
    wtimer_init();
    dbglink_init();

    if (coldstart) {
        // coldstart
       led0_off();
       led1_off();
       led2_off();
       led3_off();

        i = axradio_init();

        axradio_set_local_address(&localaddr);
        axradio_set_default_remote_address(&remoteaddr);



        i = axradio_set_mode(RADIO_MODE);

    } else {
        // warmstart
        ax5043_commsleepexit();

    }
    axradio_setup_pincfg2();
  //  wakeup_desc.time = WTIMER0_PERIOD; // se cambia aqui para enviar apenas se enciende el modulo
  //  wtimer0_addrelative(&wakeup_desc);
#ifdef TRANSMIT
delay_ms(2000);
transmit_packet();
#endif // TRANSMIT
    GOLDSEQUENCE_Init();
    /*
    //GPS initialization  (eliminate NMAE messages, only UBX by pooling
    setNav1[1]=0x02;
    do{
    delay(5000);
    }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x03,setNav1,ACK,NULL));

    setNav1[1]=0x01;
    do{
    delay(5000);
    }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x03,setNav1,ACK,NULL));

    setNav1[1]=0x00;
    do{
    delay(5000);
    }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x03,setNav1,ACK,NULL));

    setNav1[1]=0x03;
    do{
     delay(5000);
    }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x03,setNav1,ACK,NULL));


    setNav1[1]=0x05;
    do{
     delay(5000);
    }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x03,setNav1,ACK,NULL));

    do{
    delay(5000);
    }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x08,setNav2,ACK,NULL));

    do{
    delay(5000);
    }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x08,setNav3,ACK,NULL));

    do{
    delay(5000);
    }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x24,sizeof(setNav5),setNav5,ACK,NULL));

*/


    for(;;)
    {

		//wtimer_runcallbacks();
        switch(RfStateMachine)
        {
        case e_WaitTxMessage:
            //wait for uart interrupt
      //      if(UartMessageReceived)/*Flag que me indica que hay un mensaje de UART*/
      //      {

     //       }
            break;

        case e_WaitForBeaconAndMessageSend:
            if(RxIsrFlag==0x01)
            {

                RxIsrFlag=0x00;
               // MessageReadFromMemory(RxBuffer);
            //En Rx buffer se obtiene el mensaje recibido
            // funcion para parsear y verificar que el mensaje sea correcto
            //esperar al timer y enviar mensaje

                RfStateMachine = e_WaitForACK;
            }

            break;
        case e_WaitForACK:
            if(RxIsrFlag == 0x01)
            {
                //MessageReadFromMemory(RxBuffer);
            }
            break;
        default:
            break;
        }
// pooling section

//UART ver cual se va a utilizar luego

    //pool communication uart
    if( UART_Proc_VerifyIncomingMsg(&PROCD_Command,PROCD_UartRxBuffer , &PROCD_PayloadLength) == RX_SUCCESFULL)
    {

        dbglink_writestr("comando Rx: ");
        dbglink_writehex16(PROCD_Command,1,WRNUM_PADZERO);
        dbglink_writestr("\n Longitud Rx: ");
        dbglink_writehex16(PROCD_PayloadLength,1,WRNUM_PADZERO);
        dbglink_writestr("\n payload: ");
        for(i=0;i<PROCD_PayloadLength;i++)
            {
                dbglink_writehex16(*(PROCD_UartRxBuffer+i),1,WRNUM_PADZERO);
            }
    }
    //pool GPS timer
    if(Tmr0Flg == 0x01) //timer has been activated
    {
    Tmr0Flg = 0x00;
    UBLOX_Rx_Payload = malloc(30*sizeof(uint8_t));
    UBLOX_GPS_SendCommand_WaitACK(eNAV,POSLLH,0x00,UBLOX_Rx_Payload,ANS,&UBLOX_Rx_Length);
    free(UBLOX_Rx_Payload);
    //update GPS data on memory
    }


     if(BeaconRx.FlagReceived == 0x01)
    {
        delay_tx(20);
        dbglink_writestr(" 0x");
        dbglink_writehex16(BeaconRx.SatId,1,WRNUM_PADZERO);
        BeaconRx.FlagReceived = 0x00;
    }
    GOLDSEQUENCE_Obtain();
      /* pseudo code para main

      check main (radio) state machine

      radio state machine :

      wait for Tx message:
      la radio se encuentra en recepcion
      se espera un flag el cual indique que hay un mensaje listo para enviar
      se trata y empaqueta el mensaje dejandolo en el buffer listo para enviar
      se avanza al estado wait for beacon en caso de recibir un mensaje

      wait for beacon and message send:
      Se espera a una interrupcion de radio con un beacon de uplink ( o la cantidad de beacons necesarios segun el contador de back off)
      se genera el numero randon en donde se va a transmitir ( o se utiliza el numbero fijo si premium)
      se activa el timer de acuerdo a este numero , aqui se debe bloquear el programa para mantener el sincronismo
      se vence el timer y
      se envia el mensaje  , se pasa al estado de wait for ACK

      Wait for ACK:
      /* estado bloqueante, apagar ISR
      Se espera a la interrupcion de radio un un downlink frame
      se trata el downlink frame para comprobar si esta el ACK del mensaje anteriormente enviado
      Se recibe ACK, se pasa a estado wait for Tx message
      No se recive el ACK se pasa al estado wait for beacon and message send, se incrementa el contador de retransmision, se genera un numero random de frame para Tx
      */


      // pooling

      //UART
      /*
       * verificar si hay un nuevo mensaje en la UART, mediante el flag de mensaje recibido
       * En el caso de que haya un mensaje, responder segun corresponda:
       *
       * add message to send : Verificar el mensaje recibido, tamaño maximo, crc y responder con el mensaje OK.
       *                       dar formato al mensaje y encriptar de ser necesario.
       *                       levantar el flag para la maquina de estado
       *
       * get status message: Verificar el mensaje recibido, tamaño y CRC.
       *                     Dar formato al mensaje de get status, acutalizando la informacion necesario y responder
       *
       * get received message: Verificar el mensaje recibido, tamaño y CRC
       *                       Responder con el mensaje recibido, en caso de no haber ningun mensaje responder con un error
       *
       * set config : Verificar el mensaje recibido, tamaño y CRC. Parsear el mensaje y modificar las configuraciones afectadas.
       *


      //GPS
      /*
       * Verificar si el timer de pooling de gps se agoto, pedir coordenadas al GPS, renovar el contador y actualziar las coordenadas del sistema
       * realizar pooling de geofencing en caso de ser necesario, verificar si esat dentro de la zona permitda, en caso contrario, dar una alarma
      */
    wtimer_runcallbacks();
    wtimer_idle(WTFLAG_CANSTANDBY);

    }
}


