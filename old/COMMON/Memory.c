/**
*   \file
*   \brief Flash memory driver
*
*    This module contains
*
*   \author Javier Verde
*   \author Aistech Space
*/
#include <stdlib.h>
#include <string.h>
#include "Memory.h"
#include  "libmfflash.h"
//memory read
/**
*   \brief Reads the amount of byte requested
*
*   @param[out] uint8_t *RxBuffer : pointer to buffer where the data is needed
*/
void ReadFromMemory(uint16_t *RxBuffer, uint16_t address)
{

    uint8_t i;
    flash_unlock();
    *RxBuffer = flash_read(address);
    flash_lock();
}


//memory write
/**
*   \brief Writes a memory address
*
*    Memory driver is only capable of flash bits from 1 to 0, therefore in order to write any memory byte it is necesarry to
*    Erase the whole page ( unless consecutive data is being written
*
*   @param[in] uint16_t address
*   @param[in] uint16_t data
*   @param[in] bool erase_flag
*/

void WriteMemory(uint16_t address, uint16_t data, bool erase_flag)
{
    flash_unlock();
    if(erase_flag)
    {
        flash_pageerase(address);
    }
    flash_wait(5);
    flash_write(address,data);
    flash_lock();

}
