//MemoryMap.h

/* In this file the organization of the eeprom memory is detalided */
#define OFFSET_GOLD_SEQUENCE_START 0x0000
#define OFFSET_GOLD_SEQUENCE_END   0x0084
//Each Gold sequence is defined by 4 bytes

#define OFFSET_RESERVED_START 0xFC00
#define OFFSET_RESERVED_END   0xFFFF
