/**
*   \file
*   \brief Driver for communication interface with processing board
*
*    This module contains functions for initializing, receiving and sending data through UART interface
*
*   \author Javier Verde
*   \author Aistech Space
*/


#include "UartComProc.h"
#include <libmfcrc.h>

/**
*
*  \brief UART intializer
*
*   This function inits the necesarry UART to communicate
*   with the processing board
*
*/
__reentrantb void UART_Proc_PortInit(void) __reentrant
{
    PALTB |= 0x11;
    DIRB |= (1<<0) | (1<<4);
    DIRB &= (uint8_t)~(1<<5);
    DIRB &= (uint8_t)~(1<<1);
    PINSEL &= (uint8_t)~((1<<0) | (1<<1));
    uart_timer1_baud(CLKSRC_SYSCLK, 9600, 20000000UL);
    uart0_init(1, 8, 1);
}

/**
*
* \brief Verifies if there is a UART incoming message and decodes it
*
*   This function verifies incoming data, its format, makes the substitution of KISS special characters, verifies CRC and
*
*   @param[out] cmd Command word received
*   @param[out] payload Pointer to payload reception buffer
*   @param[out] PayloadLength   payload length value
*
*
*   @return Output according to the result of the operation, RX_SUCCESFULL or several error codes are returned
*/

uint8_t  UART_Proc_VerifyIncomingMsg(uint8_t *cmd, uint8_t *payload,uint8_t *PayloadLength)
{
    uint8_t RetVal = ERR_NO_DATA;
    uint8_t i = 0;
    uint8_t scape = 0;
    uint8_t __xdata *UartRxBuffer;
    uint8_t RxLength;
    uint32_t __xdata CRC32;
    uint32_t __xdata CRC32Rx;
    UartRxBuffer  =(uint8_t*) malloc(sizeof(uint8_t)*40);
    if(uart0_rxcount())
    {
        *UartRxBuffer = uart0_rx();
        RetVal = ERR_BAD_FORMAT; // Indico que se recibio algo, pero de volver con este valor, no se recibio una trama KISS
        delay_ms(500);
        RxLength = 1;
        if(*UartRxBuffer == 0xC0)
        {
            do{
                if(uart0_rxcount())
                {
                    *(UartRxBuffer+RxLength)=uart0_rx();
                    RxLength=(RxLength)+1;
                }
                else
                {
                    //En caso de perder la trama 0xC0, o que por algun motivo llegue mal, para que la funcion no bloquee al programa se incluyen estas lineas
                    scape ++;
                    delay_ms(1);
                }
            }while(*(UartRxBuffer+(RxLength-1)) != 0xC0);
            if(scape == 250)
            {
                RetVal = ERR_BAD_ENDING;// indico que no se recibio el paquete correctamente
            }
            else
            {

                UART_Proc_ModifiyKissSpecialCharacters(UartRxBuffer,&RxLength);
              //if the format is correct validate CRC32 ( CRC is calculated using command + payload)
              if(*UartRxBuffer == 0xC0 && *(UartRxBuffer+RxLength-1) == 0xC0)
              {
                CRC32 = UART_Calc_CRC32(UartRxBuffer+1,RxLength-START_CHARACTER_LENGTH-END_CHARACTER_LENGTH-CRC_LENGTH,1);// lo pongo en little endian, ya que el memcpy lo pasa asi
                memcpy(&CRC32Rx,UartRxBuffer+RxLength-START_CHARACTER_LENGTH-CRC_LENGTH,CRC_LENGTH);

                if(CRC32Rx == CRC32)
                {
                    //si el CRC es correcto parseo el mensaje de acuerdo al formato KISS
                    RetVal = RX_SUCCESFULL;
                    *cmd = *(UartRxBuffer+1);
                    memcpy(payload,UartRxBuffer+START_CHARACTER_LENGTH+COMMAND_LENGTH,RxLength-START_CHARACTER_LENGTH-CRC_LENGTH-END_CHARACTER_LENGTH); // porq esta linea me adelanta !!
                }
                else
                {
                        RetVal = ERR_BAD_CRC;
                }
             *PayloadLength = RxLength -START_CHARACTER_LENGTH-COMMAND_LENGTH-CRC_LENGTH-END_CHARACTER_LENGTH;
             }
            }
        }
    free(UartRxBuffer);
    return RetVal;
    }
}

/**
* \brief  KISS special characters modification
*
* This function modifies the special characters defined on kiss protocol
*
*  @param[in] UartRxBuffer  Pointer to rx buffer where data needs to be modified
*  @param[in] RxLenght  Lenght of the incoming message
*
*/

void UART_Proc_ModifiyKissSpecialCharacters(uint8_t *UartRxBuffer, uint8_t *RxLength)
{
    uint8_t i = 0;
    uint8_t j = 0;
    for(i=0;i<*RxLength;i++)
    {
        if (*(UartRxBuffer+i) == FRAME_SCAPE)
        {
            if(*(UartRxBuffer+i+1)== TRANSPOSE_FRAME_END)
            {
                *(UartRxBuffer+i) = FRAME_END;
                for(j=i+1;j<*RxLength;j++)
                {
                    *(UartRxBuffer+j) = *(UartRxBuffer+j+1);
                }
                *RxLength= *RxLength -1; //suprimi un caracter por lo cual el mensaje se disminuye su tama�o en 1
            }
            else if(*(UartRxBuffer+i+1) == TRANSPOSE_FRAME_ESCAPE)
            {
                *(UartRxBuffer+i) = FRAME_SCAPE;
                for(j=i+1;j<*RxLength;j++)
                {
                    *(UartRxBuffer+j) = *(UartRxBuffer+j+1);
                }
                *RxLength = *RxLength -1; //suprimi un caracter por lo cual el mensaje se disminuye su tama�o en 1
            }
         }
     }
}


/**
* \brief Send message through PROC-D UART interface
*
* This function receives the payload and command to be
* sent, formats the message, calcultes the CRC32 and sends
* it to the processing terminal.
*
* @param[in] payload    payload of the message that must be sent.
* @param[in] length     payload length
* @param[in] command    command code to be sent
*
* @return Returns TRUE if no errors, else FALSE
***********************************************************************/

uint8_t UART_Proc_SendMessage(uint8_t *payload, uint8_t length, uint8_t command)
{
    uint8_t i;
    uint8_t RetVal=1;
    uint8_t __xdata *CrcBuffer;
    uint32_t __xdata CRC32;
    CrcBuffer = (uint8_t) malloc(sizeof(uint8_t)*(length+1));
    *CrcBuffer = command;

    memcpy(CrcBuffer+1,payload,length);
    CRC32 = UART_Calc_CRC32(CrcBuffer,length+1,0);
    uart0_tx(FRAME_START);
    uart0_tx(command);
    for(i=0;i<length;i++)
    {
        uart0_tx(*(payload+i));
    }
    uart0_tx((uint8_t)((CRC32 & 0xFF000000) >>24));
    uart0_tx((uint8_t)((CRC32 & 0x00FF0000) >>16));
    uart0_tx((uint8_t)((CRC32 & 0x0000FF00) >>8));
    uart0_tx((uint8_t)(CRC32 & 0x000000FF));

    uart0_tx(FRAME_END);
    /* validate command
     * validate payload lenght according to command
     * format message
     * calculate CRC
     * send message
    */

    return RetVal;
}


/**
* \brief CRC32 calc
*
* This function calculates CRC32 of the given buffer
*
* @param[in] *message   pointer to the data of which CRC32 need to be calculated
* @param[in] length data length
* @param[in] littleEndian   endiannes of the CRC32
*
* @return CRC32 of the given data
*
*/


uint32_t UART_Calc_CRC32(uint8_t *message,uint8_t length,uint8_t littleEndian) {
   int i, j;
   uint32_t __xdata byte, crc, mask;

   i = 0;
   crc = 0xFFFFFFFF;
   while (i < length) {
      byte = message[i];
            // Get next byte.
      crc = crc ^ byte;
      for (j = 7; j >= 0; j--) {    // Do eight times.
         mask = -(crc & 1);
         crc = (crc >> 1) ^ (0xEDB88320 & mask);
      }
      i = i + 1;
   }
   crc = ~crc;
 if(littleEndian)crc=(((crc & 0x000000FF) << 24)+((crc & 0x0000FF00) << 8)+ ((crc & 0x00FF0000) >> 8)+((crc & 0xFF000000) >> 24));//swap little endian big endian
     return crc;
}

