//beacon definition file
#include <libmftypes.h>
#define TRUE 1
#define FALSE 0

#define DOWNLINK_BEACON 0xA
#define UPLINK_BEACON   0xC

//masks for obtaining the desired information
#define MASK_BEACONID 0xF0
#define MASK_HMAC_FLAG 0x80
#define MASK_HMAC_TYPE 0x70
#define MASK_ENCRYPTION_FLAG 0x08
#define MASK_ENCRYPTION_TYPE 0x07

//Position in the beacon ( in bytes)

#define POS_BEACONID 0x02
#define POS_NUMPREM  0x03
#define POS_SECURITY 0x04
#define POS_NONCE    0x05
#define POS_RESERVED 0x07
#define POS_CRC      0x0C
#define POS_HMAC_MSB 0x10
#define POS_HMAC_LSB 0x14

//values

#define INVALID_BEACON 0
#define VALID_BEACON   1

typedef unsigned char bool;

enum HMAC{
e_HMAC1,
e_HMAC2,
e_HMAC3,
e_HMAC4,
e_HMAC5,
e_HMAC6,
e_HMAC7,
e_HMAC8
};

enum Encryption{
e_Encryption1,
e_Encryption2,
e_Encryption3,
e_Encryption4,
e_Encryption5,
e_Encryption6,
e_Encryption7,
e_Encryption8
};


//values not defined yet
//TODO: define values

//Beacon can be uplink or downlink, they share the structure
__xdata typedef struct {
    //flags
    uint8_t FlagReceived;
    //service header
    uint16_t SatId;
    uint8_t  BeaconID;
    uint8_t  NumPremSlots;
    uint8_t  NumACK;
    //security
    bool     HMAC_flag;
    uint8_t  HMAC_type;
    bool     Encryption_flag;
    uint8_t  Encryption_type;
    uint16_t Nonce;
    //reserved
    uint8_t reserved[5];
    //integrity and hash
    uint32_t CRC;
    uint32_t HMAC_Msb;
    uint32_t HMAC_Lsb;

}RF_beacon_t;

__xdata void BEACON_decoding(uint8_t* Rxbuffer, RF_beacon_t* beacon, uint8_t length);



