                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.9.3 #11354 (MINGW32)
                                      4 ;--------------------------------------------------------
                                      5 	.module misc
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _ACK_Callback
                                     12 	.globl _BackOff_Callback
                                     13 	.globl _GPS_callback
                                     14 	.globl _TwinMessage2_callback
                                     15 	.globl _TwinMessage1_callback
                                     16 	.globl _OriginalMessage_callback
                                     17 	.globl _crc_crc32_msb
                                     18 	.globl _dbglink_writenum32
                                     19 	.globl _dbglink_writestr
                                     20 	.globl _axradio_transmit
                                     21 	.globl _axradio_set_channel
                                     22 	.globl _memcpy
                                     23 	.globl _wtimer0_remove
                                     24 	.globl _wtimer_remove
                                     25 	.globl _wtimer1_addrelative
                                     26 	.globl _wtimer0_addrelative
                                     27 	.globl _wtimer0_addabsolute
                                     28 	.globl _wtimer_runcallbacks
                                     29 	.globl _wtimer_idle
                                     30 	.globl _PORTC_7
                                     31 	.globl _PORTC_6
                                     32 	.globl _PORTC_5
                                     33 	.globl _PORTC_4
                                     34 	.globl _PORTC_3
                                     35 	.globl _PORTC_2
                                     36 	.globl _PORTC_1
                                     37 	.globl _PORTC_0
                                     38 	.globl _PORTB_7
                                     39 	.globl _PORTB_6
                                     40 	.globl _PORTB_5
                                     41 	.globl _PORTB_4
                                     42 	.globl _PORTB_3
                                     43 	.globl _PORTB_2
                                     44 	.globl _PORTB_1
                                     45 	.globl _PORTB_0
                                     46 	.globl _PORTA_7
                                     47 	.globl _PORTA_6
                                     48 	.globl _PORTA_5
                                     49 	.globl _PORTA_4
                                     50 	.globl _PORTA_3
                                     51 	.globl _PORTA_2
                                     52 	.globl _PORTA_1
                                     53 	.globl _PORTA_0
                                     54 	.globl _PINC_7
                                     55 	.globl _PINC_6
                                     56 	.globl _PINC_5
                                     57 	.globl _PINC_4
                                     58 	.globl _PINC_3
                                     59 	.globl _PINC_2
                                     60 	.globl _PINC_1
                                     61 	.globl _PINC_0
                                     62 	.globl _PINB_7
                                     63 	.globl _PINB_6
                                     64 	.globl _PINB_5
                                     65 	.globl _PINB_4
                                     66 	.globl _PINB_3
                                     67 	.globl _PINB_2
                                     68 	.globl _PINB_1
                                     69 	.globl _PINB_0
                                     70 	.globl _PINA_7
                                     71 	.globl _PINA_6
                                     72 	.globl _PINA_5
                                     73 	.globl _PINA_4
                                     74 	.globl _PINA_3
                                     75 	.globl _PINA_2
                                     76 	.globl _PINA_1
                                     77 	.globl _PINA_0
                                     78 	.globl _CY
                                     79 	.globl _AC
                                     80 	.globl _F0
                                     81 	.globl _RS1
                                     82 	.globl _RS0
                                     83 	.globl _OV
                                     84 	.globl _F1
                                     85 	.globl _P
                                     86 	.globl _IP_7
                                     87 	.globl _IP_6
                                     88 	.globl _IP_5
                                     89 	.globl _IP_4
                                     90 	.globl _IP_3
                                     91 	.globl _IP_2
                                     92 	.globl _IP_1
                                     93 	.globl _IP_0
                                     94 	.globl _EA
                                     95 	.globl _IE_7
                                     96 	.globl _IE_6
                                     97 	.globl _IE_5
                                     98 	.globl _IE_4
                                     99 	.globl _IE_3
                                    100 	.globl _IE_2
                                    101 	.globl _IE_1
                                    102 	.globl _IE_0
                                    103 	.globl _EIP_7
                                    104 	.globl _EIP_6
                                    105 	.globl _EIP_5
                                    106 	.globl _EIP_4
                                    107 	.globl _EIP_3
                                    108 	.globl _EIP_2
                                    109 	.globl _EIP_1
                                    110 	.globl _EIP_0
                                    111 	.globl _EIE_7
                                    112 	.globl _EIE_6
                                    113 	.globl _EIE_5
                                    114 	.globl _EIE_4
                                    115 	.globl _EIE_3
                                    116 	.globl _EIE_2
                                    117 	.globl _EIE_1
                                    118 	.globl _EIE_0
                                    119 	.globl _E2IP_7
                                    120 	.globl _E2IP_6
                                    121 	.globl _E2IP_5
                                    122 	.globl _E2IP_4
                                    123 	.globl _E2IP_3
                                    124 	.globl _E2IP_2
                                    125 	.globl _E2IP_1
                                    126 	.globl _E2IP_0
                                    127 	.globl _E2IE_7
                                    128 	.globl _E2IE_6
                                    129 	.globl _E2IE_5
                                    130 	.globl _E2IE_4
                                    131 	.globl _E2IE_3
                                    132 	.globl _E2IE_2
                                    133 	.globl _E2IE_1
                                    134 	.globl _E2IE_0
                                    135 	.globl _B_7
                                    136 	.globl _B_6
                                    137 	.globl _B_5
                                    138 	.globl _B_4
                                    139 	.globl _B_3
                                    140 	.globl _B_2
                                    141 	.globl _B_1
                                    142 	.globl _B_0
                                    143 	.globl _ACC_7
                                    144 	.globl _ACC_6
                                    145 	.globl _ACC_5
                                    146 	.globl _ACC_4
                                    147 	.globl _ACC_3
                                    148 	.globl _ACC_2
                                    149 	.globl _ACC_1
                                    150 	.globl _ACC_0
                                    151 	.globl _WTSTAT
                                    152 	.globl _WTIRQEN
                                    153 	.globl _WTEVTD
                                    154 	.globl _WTEVTD1
                                    155 	.globl _WTEVTD0
                                    156 	.globl _WTEVTC
                                    157 	.globl _WTEVTC1
                                    158 	.globl _WTEVTC0
                                    159 	.globl _WTEVTB
                                    160 	.globl _WTEVTB1
                                    161 	.globl _WTEVTB0
                                    162 	.globl _WTEVTA
                                    163 	.globl _WTEVTA1
                                    164 	.globl _WTEVTA0
                                    165 	.globl _WTCNTR1
                                    166 	.globl _WTCNTB
                                    167 	.globl _WTCNTB1
                                    168 	.globl _WTCNTB0
                                    169 	.globl _WTCNTA
                                    170 	.globl _WTCNTA1
                                    171 	.globl _WTCNTA0
                                    172 	.globl _WTCFGB
                                    173 	.globl _WTCFGA
                                    174 	.globl _WDTRESET
                                    175 	.globl _WDTCFG
                                    176 	.globl _U1STATUS
                                    177 	.globl _U1SHREG
                                    178 	.globl _U1MODE
                                    179 	.globl _U1CTRL
                                    180 	.globl _U0STATUS
                                    181 	.globl _U0SHREG
                                    182 	.globl _U0MODE
                                    183 	.globl _U0CTRL
                                    184 	.globl _T2STATUS
                                    185 	.globl _T2PERIOD
                                    186 	.globl _T2PERIOD1
                                    187 	.globl _T2PERIOD0
                                    188 	.globl _T2MODE
                                    189 	.globl _T2CNT
                                    190 	.globl _T2CNT1
                                    191 	.globl _T2CNT0
                                    192 	.globl _T2CLKSRC
                                    193 	.globl _T1STATUS
                                    194 	.globl _T1PERIOD
                                    195 	.globl _T1PERIOD1
                                    196 	.globl _T1PERIOD0
                                    197 	.globl _T1MODE
                                    198 	.globl _T1CNT
                                    199 	.globl _T1CNT1
                                    200 	.globl _T1CNT0
                                    201 	.globl _T1CLKSRC
                                    202 	.globl _T0STATUS
                                    203 	.globl _T0PERIOD
                                    204 	.globl _T0PERIOD1
                                    205 	.globl _T0PERIOD0
                                    206 	.globl _T0MODE
                                    207 	.globl _T0CNT
                                    208 	.globl _T0CNT1
                                    209 	.globl _T0CNT0
                                    210 	.globl _T0CLKSRC
                                    211 	.globl _SPSTATUS
                                    212 	.globl _SPSHREG
                                    213 	.globl _SPMODE
                                    214 	.globl _SPCLKSRC
                                    215 	.globl _RADIOSTAT
                                    216 	.globl _RADIOSTAT1
                                    217 	.globl _RADIOSTAT0
                                    218 	.globl _RADIODATA
                                    219 	.globl _RADIODATA3
                                    220 	.globl _RADIODATA2
                                    221 	.globl _RADIODATA1
                                    222 	.globl _RADIODATA0
                                    223 	.globl _RADIOADDR
                                    224 	.globl _RADIOADDR1
                                    225 	.globl _RADIOADDR0
                                    226 	.globl _RADIOACC
                                    227 	.globl _OC1STATUS
                                    228 	.globl _OC1PIN
                                    229 	.globl _OC1MODE
                                    230 	.globl _OC1COMP
                                    231 	.globl _OC1COMP1
                                    232 	.globl _OC1COMP0
                                    233 	.globl _OC0STATUS
                                    234 	.globl _OC0PIN
                                    235 	.globl _OC0MODE
                                    236 	.globl _OC0COMP
                                    237 	.globl _OC0COMP1
                                    238 	.globl _OC0COMP0
                                    239 	.globl _NVSTATUS
                                    240 	.globl _NVKEY
                                    241 	.globl _NVDATA
                                    242 	.globl _NVDATA1
                                    243 	.globl _NVDATA0
                                    244 	.globl _NVADDR
                                    245 	.globl _NVADDR1
                                    246 	.globl _NVADDR0
                                    247 	.globl _IC1STATUS
                                    248 	.globl _IC1MODE
                                    249 	.globl _IC1CAPT
                                    250 	.globl _IC1CAPT1
                                    251 	.globl _IC1CAPT0
                                    252 	.globl _IC0STATUS
                                    253 	.globl _IC0MODE
                                    254 	.globl _IC0CAPT
                                    255 	.globl _IC0CAPT1
                                    256 	.globl _IC0CAPT0
                                    257 	.globl _PORTR
                                    258 	.globl _PORTC
                                    259 	.globl _PORTB
                                    260 	.globl _PORTA
                                    261 	.globl _PINR
                                    262 	.globl _PINC
                                    263 	.globl _PINB
                                    264 	.globl _PINA
                                    265 	.globl _DIRR
                                    266 	.globl _DIRC
                                    267 	.globl _DIRB
                                    268 	.globl _DIRA
                                    269 	.globl _DBGLNKSTAT
                                    270 	.globl _DBGLNKBUF
                                    271 	.globl _CODECONFIG
                                    272 	.globl _CLKSTAT
                                    273 	.globl _CLKCON
                                    274 	.globl _ANALOGCOMP
                                    275 	.globl _ADCCONV
                                    276 	.globl _ADCCLKSRC
                                    277 	.globl _ADCCH3CONFIG
                                    278 	.globl _ADCCH2CONFIG
                                    279 	.globl _ADCCH1CONFIG
                                    280 	.globl _ADCCH0CONFIG
                                    281 	.globl __XPAGE
                                    282 	.globl _XPAGE
                                    283 	.globl _SP
                                    284 	.globl _PSW
                                    285 	.globl _PCON
                                    286 	.globl _IP
                                    287 	.globl _IE
                                    288 	.globl _EIP
                                    289 	.globl _EIE
                                    290 	.globl _E2IP
                                    291 	.globl _E2IE
                                    292 	.globl _DPS
                                    293 	.globl _DPTR1
                                    294 	.globl _DPTR0
                                    295 	.globl _DPL1
                                    296 	.globl _DPL
                                    297 	.globl _DPH1
                                    298 	.globl _DPH
                                    299 	.globl _B
                                    300 	.globl _ACC
                                    301 	.globl _AssemblyPacket_PARM_8
                                    302 	.globl _AssemblyPacket_PARM_7
                                    303 	.globl _AssemblyPacket_PARM_6
                                    304 	.globl _AssemblyPacket_PARM_5
                                    305 	.globl _AssemblyPacket_PARM_4
                                    306 	.globl _AssemblyPacket_PARM_3
                                    307 	.globl _AssemblyPacket_PARM_2
                                    308 	.globl _GetRandomSlots_PARM_3
                                    309 	.globl _GetRandomSlots_PARM_2
                                    310 	.globl _swap_variables_PARM_2
                                    311 	.globl _delay_tx_PARM_3
                                    312 	.globl _delay_tx_PARM_2
                                    313 	.globl _GPS_PoolFlag
                                    314 	.globl _Channel_change
                                    315 	.globl _ACKTimer
                                    316 	.globl _BackOff
                                    317 	.globl _GPS_tmr
                                    318 	.globl _TwinMessage2_tmr
                                    319 	.globl _TwinMessage1_tmr
                                    320 	.globl _OriginalMessage_tmr
                                    321 	.globl _RNGCLKSRC1
                                    322 	.globl _RNGCLKSRC0
                                    323 	.globl _RNGMODE
                                    324 	.globl _AESOUTADDR
                                    325 	.globl _AESOUTADDR1
                                    326 	.globl _AESOUTADDR0
                                    327 	.globl _AESMODE
                                    328 	.globl _AESKEYADDR
                                    329 	.globl _AESKEYADDR1
                                    330 	.globl _AESKEYADDR0
                                    331 	.globl _AESINADDR
                                    332 	.globl _AESINADDR1
                                    333 	.globl _AESINADDR0
                                    334 	.globl _AESCURBLOCK
                                    335 	.globl _AESCONFIG
                                    336 	.globl _RNGBYTE
                                    337 	.globl _XTALREADY
                                    338 	.globl _XTALOSC
                                    339 	.globl _XTALAMPL
                                    340 	.globl _SILICONREV
                                    341 	.globl _SCRATCH3
                                    342 	.globl _SCRATCH2
                                    343 	.globl _SCRATCH1
                                    344 	.globl _SCRATCH0
                                    345 	.globl _RADIOMUX
                                    346 	.globl _RADIOFSTATADDR
                                    347 	.globl _RADIOFSTATADDR1
                                    348 	.globl _RADIOFSTATADDR0
                                    349 	.globl _RADIOFDATAADDR
                                    350 	.globl _RADIOFDATAADDR1
                                    351 	.globl _RADIOFDATAADDR0
                                    352 	.globl _OSCRUN
                                    353 	.globl _OSCREADY
                                    354 	.globl _OSCFORCERUN
                                    355 	.globl _OSCCALIB
                                    356 	.globl _MISCCTRL
                                    357 	.globl _LPXOSCGM
                                    358 	.globl _LPOSCREF
                                    359 	.globl _LPOSCREF1
                                    360 	.globl _LPOSCREF0
                                    361 	.globl _LPOSCPER
                                    362 	.globl _LPOSCPER1
                                    363 	.globl _LPOSCPER0
                                    364 	.globl _LPOSCKFILT
                                    365 	.globl _LPOSCKFILT1
                                    366 	.globl _LPOSCKFILT0
                                    367 	.globl _LPOSCFREQ
                                    368 	.globl _LPOSCFREQ1
                                    369 	.globl _LPOSCFREQ0
                                    370 	.globl _LPOSCCONFIG
                                    371 	.globl _PINSEL
                                    372 	.globl _PINCHGC
                                    373 	.globl _PINCHGB
                                    374 	.globl _PINCHGA
                                    375 	.globl _PALTRADIO
                                    376 	.globl _PALTC
                                    377 	.globl _PALTB
                                    378 	.globl _PALTA
                                    379 	.globl _INTCHGC
                                    380 	.globl _INTCHGB
                                    381 	.globl _INTCHGA
                                    382 	.globl _EXTIRQ
                                    383 	.globl _GPIOENABLE
                                    384 	.globl _ANALOGA
                                    385 	.globl _FRCOSCREF
                                    386 	.globl _FRCOSCREF1
                                    387 	.globl _FRCOSCREF0
                                    388 	.globl _FRCOSCPER
                                    389 	.globl _FRCOSCPER1
                                    390 	.globl _FRCOSCPER0
                                    391 	.globl _FRCOSCKFILT
                                    392 	.globl _FRCOSCKFILT1
                                    393 	.globl _FRCOSCKFILT0
                                    394 	.globl _FRCOSCFREQ
                                    395 	.globl _FRCOSCFREQ1
                                    396 	.globl _FRCOSCFREQ0
                                    397 	.globl _FRCOSCCTRL
                                    398 	.globl _FRCOSCCONFIG
                                    399 	.globl _DMA1CONFIG
                                    400 	.globl _DMA1ADDR
                                    401 	.globl _DMA1ADDR1
                                    402 	.globl _DMA1ADDR0
                                    403 	.globl _DMA0CONFIG
                                    404 	.globl _DMA0ADDR
                                    405 	.globl _DMA0ADDR1
                                    406 	.globl _DMA0ADDR0
                                    407 	.globl _ADCTUNE2
                                    408 	.globl _ADCTUNE1
                                    409 	.globl _ADCTUNE0
                                    410 	.globl _ADCCH3VAL
                                    411 	.globl _ADCCH3VAL1
                                    412 	.globl _ADCCH3VAL0
                                    413 	.globl _ADCCH2VAL
                                    414 	.globl _ADCCH2VAL1
                                    415 	.globl _ADCCH2VAL0
                                    416 	.globl _ADCCH1VAL
                                    417 	.globl _ADCCH1VAL1
                                    418 	.globl _ADCCH1VAL0
                                    419 	.globl _ADCCH0VAL
                                    420 	.globl _ADCCH0VAL1
                                    421 	.globl _ADCCH0VAL0
                                    422 	.globl _delay_ms
                                    423 	.globl _Channel_change_callback
                                    424 	.globl _delay_tx
                                    425 	.globl _InitGPSPolling
                                    426 	.globl _InitChannelChange
                                    427 	.globl _StartBackoffTimer
                                    428 	.globl _StartACKTimer
                                    429 	.globl _swap_variables
                                    430 	.globl _GetRandomSlots
                                    431 	.globl _AssemblyPacket
                                    432 ;--------------------------------------------------------
                                    433 ; special function registers
                                    434 ;--------------------------------------------------------
                                    435 	.area RSEG    (ABS,DATA)
      000000                        436 	.org 0x0000
                           0000E0   437 _ACC	=	0x00e0
                           0000F0   438 _B	=	0x00f0
                           000083   439 _DPH	=	0x0083
                           000085   440 _DPH1	=	0x0085
                           000082   441 _DPL	=	0x0082
                           000084   442 _DPL1	=	0x0084
                           008382   443 _DPTR0	=	0x8382
                           008584   444 _DPTR1	=	0x8584
                           000086   445 _DPS	=	0x0086
                           0000A0   446 _E2IE	=	0x00a0
                           0000C0   447 _E2IP	=	0x00c0
                           000098   448 _EIE	=	0x0098
                           0000B0   449 _EIP	=	0x00b0
                           0000A8   450 _IE	=	0x00a8
                           0000B8   451 _IP	=	0x00b8
                           000087   452 _PCON	=	0x0087
                           0000D0   453 _PSW	=	0x00d0
                           000081   454 _SP	=	0x0081
                           0000D9   455 _XPAGE	=	0x00d9
                           0000D9   456 __XPAGE	=	0x00d9
                           0000CA   457 _ADCCH0CONFIG	=	0x00ca
                           0000CB   458 _ADCCH1CONFIG	=	0x00cb
                           0000D2   459 _ADCCH2CONFIG	=	0x00d2
                           0000D3   460 _ADCCH3CONFIG	=	0x00d3
                           0000D1   461 _ADCCLKSRC	=	0x00d1
                           0000C9   462 _ADCCONV	=	0x00c9
                           0000E1   463 _ANALOGCOMP	=	0x00e1
                           0000C6   464 _CLKCON	=	0x00c6
                           0000C7   465 _CLKSTAT	=	0x00c7
                           000097   466 _CODECONFIG	=	0x0097
                           0000E3   467 _DBGLNKBUF	=	0x00e3
                           0000E2   468 _DBGLNKSTAT	=	0x00e2
                           000089   469 _DIRA	=	0x0089
                           00008A   470 _DIRB	=	0x008a
                           00008B   471 _DIRC	=	0x008b
                           00008E   472 _DIRR	=	0x008e
                           0000C8   473 _PINA	=	0x00c8
                           0000E8   474 _PINB	=	0x00e8
                           0000F8   475 _PINC	=	0x00f8
                           00008D   476 _PINR	=	0x008d
                           000080   477 _PORTA	=	0x0080
                           000088   478 _PORTB	=	0x0088
                           000090   479 _PORTC	=	0x0090
                           00008C   480 _PORTR	=	0x008c
                           0000CE   481 _IC0CAPT0	=	0x00ce
                           0000CF   482 _IC0CAPT1	=	0x00cf
                           00CFCE   483 _IC0CAPT	=	0xcfce
                           0000CC   484 _IC0MODE	=	0x00cc
                           0000CD   485 _IC0STATUS	=	0x00cd
                           0000D6   486 _IC1CAPT0	=	0x00d6
                           0000D7   487 _IC1CAPT1	=	0x00d7
                           00D7D6   488 _IC1CAPT	=	0xd7d6
                           0000D4   489 _IC1MODE	=	0x00d4
                           0000D5   490 _IC1STATUS	=	0x00d5
                           000092   491 _NVADDR0	=	0x0092
                           000093   492 _NVADDR1	=	0x0093
                           009392   493 _NVADDR	=	0x9392
                           000094   494 _NVDATA0	=	0x0094
                           000095   495 _NVDATA1	=	0x0095
                           009594   496 _NVDATA	=	0x9594
                           000096   497 _NVKEY	=	0x0096
                           000091   498 _NVSTATUS	=	0x0091
                           0000BC   499 _OC0COMP0	=	0x00bc
                           0000BD   500 _OC0COMP1	=	0x00bd
                           00BDBC   501 _OC0COMP	=	0xbdbc
                           0000B9   502 _OC0MODE	=	0x00b9
                           0000BA   503 _OC0PIN	=	0x00ba
                           0000BB   504 _OC0STATUS	=	0x00bb
                           0000C4   505 _OC1COMP0	=	0x00c4
                           0000C5   506 _OC1COMP1	=	0x00c5
                           00C5C4   507 _OC1COMP	=	0xc5c4
                           0000C1   508 _OC1MODE	=	0x00c1
                           0000C2   509 _OC1PIN	=	0x00c2
                           0000C3   510 _OC1STATUS	=	0x00c3
                           0000B1   511 _RADIOACC	=	0x00b1
                           0000B3   512 _RADIOADDR0	=	0x00b3
                           0000B2   513 _RADIOADDR1	=	0x00b2
                           00B2B3   514 _RADIOADDR	=	0xb2b3
                           0000B7   515 _RADIODATA0	=	0x00b7
                           0000B6   516 _RADIODATA1	=	0x00b6
                           0000B5   517 _RADIODATA2	=	0x00b5
                           0000B4   518 _RADIODATA3	=	0x00b4
                           B4B5B6B7   519 _RADIODATA	=	0xb4b5b6b7
                           0000BE   520 _RADIOSTAT0	=	0x00be
                           0000BF   521 _RADIOSTAT1	=	0x00bf
                           00BFBE   522 _RADIOSTAT	=	0xbfbe
                           0000DF   523 _SPCLKSRC	=	0x00df
                           0000DC   524 _SPMODE	=	0x00dc
                           0000DE   525 _SPSHREG	=	0x00de
                           0000DD   526 _SPSTATUS	=	0x00dd
                           00009A   527 _T0CLKSRC	=	0x009a
                           00009C   528 _T0CNT0	=	0x009c
                           00009D   529 _T0CNT1	=	0x009d
                           009D9C   530 _T0CNT	=	0x9d9c
                           000099   531 _T0MODE	=	0x0099
                           00009E   532 _T0PERIOD0	=	0x009e
                           00009F   533 _T0PERIOD1	=	0x009f
                           009F9E   534 _T0PERIOD	=	0x9f9e
                           00009B   535 _T0STATUS	=	0x009b
                           0000A2   536 _T1CLKSRC	=	0x00a2
                           0000A4   537 _T1CNT0	=	0x00a4
                           0000A5   538 _T1CNT1	=	0x00a5
                           00A5A4   539 _T1CNT	=	0xa5a4
                           0000A1   540 _T1MODE	=	0x00a1
                           0000A6   541 _T1PERIOD0	=	0x00a6
                           0000A7   542 _T1PERIOD1	=	0x00a7
                           00A7A6   543 _T1PERIOD	=	0xa7a6
                           0000A3   544 _T1STATUS	=	0x00a3
                           0000AA   545 _T2CLKSRC	=	0x00aa
                           0000AC   546 _T2CNT0	=	0x00ac
                           0000AD   547 _T2CNT1	=	0x00ad
                           00ADAC   548 _T2CNT	=	0xadac
                           0000A9   549 _T2MODE	=	0x00a9
                           0000AE   550 _T2PERIOD0	=	0x00ae
                           0000AF   551 _T2PERIOD1	=	0x00af
                           00AFAE   552 _T2PERIOD	=	0xafae
                           0000AB   553 _T2STATUS	=	0x00ab
                           0000E4   554 _U0CTRL	=	0x00e4
                           0000E7   555 _U0MODE	=	0x00e7
                           0000E6   556 _U0SHREG	=	0x00e6
                           0000E5   557 _U0STATUS	=	0x00e5
                           0000EC   558 _U1CTRL	=	0x00ec
                           0000EF   559 _U1MODE	=	0x00ef
                           0000EE   560 _U1SHREG	=	0x00ee
                           0000ED   561 _U1STATUS	=	0x00ed
                           0000DA   562 _WDTCFG	=	0x00da
                           0000DB   563 _WDTRESET	=	0x00db
                           0000F1   564 _WTCFGA	=	0x00f1
                           0000F9   565 _WTCFGB	=	0x00f9
                           0000F2   566 _WTCNTA0	=	0x00f2
                           0000F3   567 _WTCNTA1	=	0x00f3
                           00F3F2   568 _WTCNTA	=	0xf3f2
                           0000FA   569 _WTCNTB0	=	0x00fa
                           0000FB   570 _WTCNTB1	=	0x00fb
                           00FBFA   571 _WTCNTB	=	0xfbfa
                           0000EB   572 _WTCNTR1	=	0x00eb
                           0000F4   573 _WTEVTA0	=	0x00f4
                           0000F5   574 _WTEVTA1	=	0x00f5
                           00F5F4   575 _WTEVTA	=	0xf5f4
                           0000F6   576 _WTEVTB0	=	0x00f6
                           0000F7   577 _WTEVTB1	=	0x00f7
                           00F7F6   578 _WTEVTB	=	0xf7f6
                           0000FC   579 _WTEVTC0	=	0x00fc
                           0000FD   580 _WTEVTC1	=	0x00fd
                           00FDFC   581 _WTEVTC	=	0xfdfc
                           0000FE   582 _WTEVTD0	=	0x00fe
                           0000FF   583 _WTEVTD1	=	0x00ff
                           00FFFE   584 _WTEVTD	=	0xfffe
                           0000E9   585 _WTIRQEN	=	0x00e9
                           0000EA   586 _WTSTAT	=	0x00ea
                                    587 ;--------------------------------------------------------
                                    588 ; special function bits
                                    589 ;--------------------------------------------------------
                                    590 	.area RSEG    (ABS,DATA)
      000000                        591 	.org 0x0000
                           0000E0   592 _ACC_0	=	0x00e0
                           0000E1   593 _ACC_1	=	0x00e1
                           0000E2   594 _ACC_2	=	0x00e2
                           0000E3   595 _ACC_3	=	0x00e3
                           0000E4   596 _ACC_4	=	0x00e4
                           0000E5   597 _ACC_5	=	0x00e5
                           0000E6   598 _ACC_6	=	0x00e6
                           0000E7   599 _ACC_7	=	0x00e7
                           0000F0   600 _B_0	=	0x00f0
                           0000F1   601 _B_1	=	0x00f1
                           0000F2   602 _B_2	=	0x00f2
                           0000F3   603 _B_3	=	0x00f3
                           0000F4   604 _B_4	=	0x00f4
                           0000F5   605 _B_5	=	0x00f5
                           0000F6   606 _B_6	=	0x00f6
                           0000F7   607 _B_7	=	0x00f7
                           0000A0   608 _E2IE_0	=	0x00a0
                           0000A1   609 _E2IE_1	=	0x00a1
                           0000A2   610 _E2IE_2	=	0x00a2
                           0000A3   611 _E2IE_3	=	0x00a3
                           0000A4   612 _E2IE_4	=	0x00a4
                           0000A5   613 _E2IE_5	=	0x00a5
                           0000A6   614 _E2IE_6	=	0x00a6
                           0000A7   615 _E2IE_7	=	0x00a7
                           0000C0   616 _E2IP_0	=	0x00c0
                           0000C1   617 _E2IP_1	=	0x00c1
                           0000C2   618 _E2IP_2	=	0x00c2
                           0000C3   619 _E2IP_3	=	0x00c3
                           0000C4   620 _E2IP_4	=	0x00c4
                           0000C5   621 _E2IP_5	=	0x00c5
                           0000C6   622 _E2IP_6	=	0x00c6
                           0000C7   623 _E2IP_7	=	0x00c7
                           000098   624 _EIE_0	=	0x0098
                           000099   625 _EIE_1	=	0x0099
                           00009A   626 _EIE_2	=	0x009a
                           00009B   627 _EIE_3	=	0x009b
                           00009C   628 _EIE_4	=	0x009c
                           00009D   629 _EIE_5	=	0x009d
                           00009E   630 _EIE_6	=	0x009e
                           00009F   631 _EIE_7	=	0x009f
                           0000B0   632 _EIP_0	=	0x00b0
                           0000B1   633 _EIP_1	=	0x00b1
                           0000B2   634 _EIP_2	=	0x00b2
                           0000B3   635 _EIP_3	=	0x00b3
                           0000B4   636 _EIP_4	=	0x00b4
                           0000B5   637 _EIP_5	=	0x00b5
                           0000B6   638 _EIP_6	=	0x00b6
                           0000B7   639 _EIP_7	=	0x00b7
                           0000A8   640 _IE_0	=	0x00a8
                           0000A9   641 _IE_1	=	0x00a9
                           0000AA   642 _IE_2	=	0x00aa
                           0000AB   643 _IE_3	=	0x00ab
                           0000AC   644 _IE_4	=	0x00ac
                           0000AD   645 _IE_5	=	0x00ad
                           0000AE   646 _IE_6	=	0x00ae
                           0000AF   647 _IE_7	=	0x00af
                           0000AF   648 _EA	=	0x00af
                           0000B8   649 _IP_0	=	0x00b8
                           0000B9   650 _IP_1	=	0x00b9
                           0000BA   651 _IP_2	=	0x00ba
                           0000BB   652 _IP_3	=	0x00bb
                           0000BC   653 _IP_4	=	0x00bc
                           0000BD   654 _IP_5	=	0x00bd
                           0000BE   655 _IP_6	=	0x00be
                           0000BF   656 _IP_7	=	0x00bf
                           0000D0   657 _P	=	0x00d0
                           0000D1   658 _F1	=	0x00d1
                           0000D2   659 _OV	=	0x00d2
                           0000D3   660 _RS0	=	0x00d3
                           0000D4   661 _RS1	=	0x00d4
                           0000D5   662 _F0	=	0x00d5
                           0000D6   663 _AC	=	0x00d6
                           0000D7   664 _CY	=	0x00d7
                           0000C8   665 _PINA_0	=	0x00c8
                           0000C9   666 _PINA_1	=	0x00c9
                           0000CA   667 _PINA_2	=	0x00ca
                           0000CB   668 _PINA_3	=	0x00cb
                           0000CC   669 _PINA_4	=	0x00cc
                           0000CD   670 _PINA_5	=	0x00cd
                           0000CE   671 _PINA_6	=	0x00ce
                           0000CF   672 _PINA_7	=	0x00cf
                           0000E8   673 _PINB_0	=	0x00e8
                           0000E9   674 _PINB_1	=	0x00e9
                           0000EA   675 _PINB_2	=	0x00ea
                           0000EB   676 _PINB_3	=	0x00eb
                           0000EC   677 _PINB_4	=	0x00ec
                           0000ED   678 _PINB_5	=	0x00ed
                           0000EE   679 _PINB_6	=	0x00ee
                           0000EF   680 _PINB_7	=	0x00ef
                           0000F8   681 _PINC_0	=	0x00f8
                           0000F9   682 _PINC_1	=	0x00f9
                           0000FA   683 _PINC_2	=	0x00fa
                           0000FB   684 _PINC_3	=	0x00fb
                           0000FC   685 _PINC_4	=	0x00fc
                           0000FD   686 _PINC_5	=	0x00fd
                           0000FE   687 _PINC_6	=	0x00fe
                           0000FF   688 _PINC_7	=	0x00ff
                           000080   689 _PORTA_0	=	0x0080
                           000081   690 _PORTA_1	=	0x0081
                           000082   691 _PORTA_2	=	0x0082
                           000083   692 _PORTA_3	=	0x0083
                           000084   693 _PORTA_4	=	0x0084
                           000085   694 _PORTA_5	=	0x0085
                           000086   695 _PORTA_6	=	0x0086
                           000087   696 _PORTA_7	=	0x0087
                           000088   697 _PORTB_0	=	0x0088
                           000089   698 _PORTB_1	=	0x0089
                           00008A   699 _PORTB_2	=	0x008a
                           00008B   700 _PORTB_3	=	0x008b
                           00008C   701 _PORTB_4	=	0x008c
                           00008D   702 _PORTB_5	=	0x008d
                           00008E   703 _PORTB_6	=	0x008e
                           00008F   704 _PORTB_7	=	0x008f
                           000090   705 _PORTC_0	=	0x0090
                           000091   706 _PORTC_1	=	0x0091
                           000092   707 _PORTC_2	=	0x0092
                           000093   708 _PORTC_3	=	0x0093
                           000094   709 _PORTC_4	=	0x0094
                           000095   710 _PORTC_5	=	0x0095
                           000096   711 _PORTC_6	=	0x0096
                           000097   712 _PORTC_7	=	0x0097
                                    713 ;--------------------------------------------------------
                                    714 ; overlayable register banks
                                    715 ;--------------------------------------------------------
                                    716 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        717 	.ds 8
                                    718 ;--------------------------------------------------------
                                    719 ; internal ram data
                                    720 ;--------------------------------------------------------
                                    721 	.area DSEG    (DATA)
      00000B                        722 _GetRandomSlots_sloc0_1_0:
      00000B                        723 	.ds 1
      00000C                        724 _GetRandomSlots_sloc1_1_0:
      00000C                        725 	.ds 1
      00000D                        726 _GetRandomSlots_sloc2_1_0:
      00000D                        727 	.ds 3
      000010                        728 _AssemblyPacket_sloc0_1_0:
      000010                        729 	.ds 4
      000014                        730 _AssemblyPacket_sloc1_1_0:
      000014                        731 	.ds 3
                                    732 ;--------------------------------------------------------
                                    733 ; overlayable items in internal ram 
                                    734 ;--------------------------------------------------------
                                    735 ;--------------------------------------------------------
                                    736 ; indirectly addressable internal ram data
                                    737 ;--------------------------------------------------------
                                    738 	.area ISEG    (DATA)
                                    739 ;--------------------------------------------------------
                                    740 ; absolute internal ram data
                                    741 ;--------------------------------------------------------
                                    742 	.area IABS    (ABS,DATA)
                                    743 	.area IABS    (ABS,DATA)
                                    744 ;--------------------------------------------------------
                                    745 ; bit data
                                    746 ;--------------------------------------------------------
                                    747 	.area BSEG    (BIT)
                                    748 ;--------------------------------------------------------
                                    749 ; paged external ram data
                                    750 ;--------------------------------------------------------
                                    751 	.area PSEG    (PAG,XDATA)
                                    752 ;--------------------------------------------------------
                                    753 ; external ram data
                                    754 ;--------------------------------------------------------
                                    755 	.area XSEG    (XDATA)
                           007020   756 _ADCCH0VAL0	=	0x7020
                           007021   757 _ADCCH0VAL1	=	0x7021
                           007020   758 _ADCCH0VAL	=	0x7020
                           007022   759 _ADCCH1VAL0	=	0x7022
                           007023   760 _ADCCH1VAL1	=	0x7023
                           007022   761 _ADCCH1VAL	=	0x7022
                           007024   762 _ADCCH2VAL0	=	0x7024
                           007025   763 _ADCCH2VAL1	=	0x7025
                           007024   764 _ADCCH2VAL	=	0x7024
                           007026   765 _ADCCH3VAL0	=	0x7026
                           007027   766 _ADCCH3VAL1	=	0x7027
                           007026   767 _ADCCH3VAL	=	0x7026
                           007028   768 _ADCTUNE0	=	0x7028
                           007029   769 _ADCTUNE1	=	0x7029
                           00702A   770 _ADCTUNE2	=	0x702a
                           007010   771 _DMA0ADDR0	=	0x7010
                           007011   772 _DMA0ADDR1	=	0x7011
                           007010   773 _DMA0ADDR	=	0x7010
                           007014   774 _DMA0CONFIG	=	0x7014
                           007012   775 _DMA1ADDR0	=	0x7012
                           007013   776 _DMA1ADDR1	=	0x7013
                           007012   777 _DMA1ADDR	=	0x7012
                           007015   778 _DMA1CONFIG	=	0x7015
                           007070   779 _FRCOSCCONFIG	=	0x7070
                           007071   780 _FRCOSCCTRL	=	0x7071
                           007076   781 _FRCOSCFREQ0	=	0x7076
                           007077   782 _FRCOSCFREQ1	=	0x7077
                           007076   783 _FRCOSCFREQ	=	0x7076
                           007072   784 _FRCOSCKFILT0	=	0x7072
                           007073   785 _FRCOSCKFILT1	=	0x7073
                           007072   786 _FRCOSCKFILT	=	0x7072
                           007078   787 _FRCOSCPER0	=	0x7078
                           007079   788 _FRCOSCPER1	=	0x7079
                           007078   789 _FRCOSCPER	=	0x7078
                           007074   790 _FRCOSCREF0	=	0x7074
                           007075   791 _FRCOSCREF1	=	0x7075
                           007074   792 _FRCOSCREF	=	0x7074
                           007007   793 _ANALOGA	=	0x7007
                           00700C   794 _GPIOENABLE	=	0x700c
                           007003   795 _EXTIRQ	=	0x7003
                           007000   796 _INTCHGA	=	0x7000
                           007001   797 _INTCHGB	=	0x7001
                           007002   798 _INTCHGC	=	0x7002
                           007008   799 _PALTA	=	0x7008
                           007009   800 _PALTB	=	0x7009
                           00700A   801 _PALTC	=	0x700a
                           007046   802 _PALTRADIO	=	0x7046
                           007004   803 _PINCHGA	=	0x7004
                           007005   804 _PINCHGB	=	0x7005
                           007006   805 _PINCHGC	=	0x7006
                           00700B   806 _PINSEL	=	0x700b
                           007060   807 _LPOSCCONFIG	=	0x7060
                           007066   808 _LPOSCFREQ0	=	0x7066
                           007067   809 _LPOSCFREQ1	=	0x7067
                           007066   810 _LPOSCFREQ	=	0x7066
                           007062   811 _LPOSCKFILT0	=	0x7062
                           007063   812 _LPOSCKFILT1	=	0x7063
                           007062   813 _LPOSCKFILT	=	0x7062
                           007068   814 _LPOSCPER0	=	0x7068
                           007069   815 _LPOSCPER1	=	0x7069
                           007068   816 _LPOSCPER	=	0x7068
                           007064   817 _LPOSCREF0	=	0x7064
                           007065   818 _LPOSCREF1	=	0x7065
                           007064   819 _LPOSCREF	=	0x7064
                           007054   820 _LPXOSCGM	=	0x7054
                           007F01   821 _MISCCTRL	=	0x7f01
                           007053   822 _OSCCALIB	=	0x7053
                           007050   823 _OSCFORCERUN	=	0x7050
                           007052   824 _OSCREADY	=	0x7052
                           007051   825 _OSCRUN	=	0x7051
                           007040   826 _RADIOFDATAADDR0	=	0x7040
                           007041   827 _RADIOFDATAADDR1	=	0x7041
                           007040   828 _RADIOFDATAADDR	=	0x7040
                           007042   829 _RADIOFSTATADDR0	=	0x7042
                           007043   830 _RADIOFSTATADDR1	=	0x7043
                           007042   831 _RADIOFSTATADDR	=	0x7042
                           007044   832 _RADIOMUX	=	0x7044
                           007084   833 _SCRATCH0	=	0x7084
                           007085   834 _SCRATCH1	=	0x7085
                           007086   835 _SCRATCH2	=	0x7086
                           007087   836 _SCRATCH3	=	0x7087
                           007F00   837 _SILICONREV	=	0x7f00
                           007F19   838 _XTALAMPL	=	0x7f19
                           007F18   839 _XTALOSC	=	0x7f18
                           007F1A   840 _XTALREADY	=	0x7f1a
                           007081   841 _RNGBYTE	=	0x7081
                           007091   842 _AESCONFIG	=	0x7091
                           007098   843 _AESCURBLOCK	=	0x7098
                           007094   844 _AESINADDR0	=	0x7094
                           007095   845 _AESINADDR1	=	0x7095
                           007094   846 _AESINADDR	=	0x7094
                           007092   847 _AESKEYADDR0	=	0x7092
                           007093   848 _AESKEYADDR1	=	0x7093
                           007092   849 _AESKEYADDR	=	0x7092
                           007090   850 _AESMODE	=	0x7090
                           007096   851 _AESOUTADDR0	=	0x7096
                           007097   852 _AESOUTADDR1	=	0x7097
                           007096   853 _AESOUTADDR	=	0x7096
                           007080   854 _RNGMODE	=	0x7080
                           007082   855 _RNGCLKSRC0	=	0x7082
                           007083   856 _RNGCLKSRC1	=	0x7083
                           00FC06   857 _flash_deviceid	=	0xfc06
                           00FC00   858 _flash_calsector	=	0xfc00
      000065                        859 _delaymstimer:
      000065                        860 	.ds 8
      00006D                        861 _OriginalMessage_tmr::
      00006D                        862 	.ds 8
      000075                        863 _TwinMessage1_tmr::
      000075                        864 	.ds 8
      00007D                        865 _TwinMessage2_tmr::
      00007D                        866 	.ds 8
      000085                        867 _GPS_tmr::
      000085                        868 	.ds 8
      00008D                        869 _BackOff::
      00008D                        870 	.ds 8
      000095                        871 _ACKTimer::
      000095                        872 	.ds 8
      00009D                        873 _Channel_change::
      00009D                        874 	.ds 8
      0000A5                        875 _GPS_PoolFlag::
      0000A5                        876 	.ds 1
      0000A6                        877 _delay_tx_PARM_2:
      0000A6                        878 	.ds 4
      0000AA                        879 _delay_tx_PARM_3:
      0000AA                        880 	.ds 4
      0000AE                        881 _delay_tx_time1_ms_65536_283:
      0000AE                        882 	.ds 4
      0000B2                        883 _InitGPSPolling_GPS_Period_S_65536_285:
      0000B2                        884 	.ds 4
      0000B6                        885 _swap_variables_PARM_2:
      0000B6                        886 	.ds 3
      0000B9                        887 _swap_variables_a_65536_293:
      0000B9                        888 	.ds 3
      0000BC                        889 _GetRandomSlots_PARM_2:
      0000BC                        890 	.ds 3
      0000BF                        891 _GetRandomSlots_PARM_3:
      0000BF                        892 	.ds 3
      0000C2                        893 _GetRandomSlots_slot1_65536_295:
      0000C2                        894 	.ds 3
      0000C5                        895 _AssemblyPacket_PARM_2:
      0000C5                        896 	.ds 1
      0000C6                        897 _AssemblyPacket_PARM_3:
      0000C6                        898 	.ds 1
      0000C7                        899 _AssemblyPacket_PARM_4:
      0000C7                        900 	.ds 3
      0000CA                        901 _AssemblyPacket_PARM_5:
      0000CA                        902 	.ds 1
      0000CB                        903 _AssemblyPacket_PARM_6:
      0000CB                        904 	.ds 1
      0000CC                        905 _AssemblyPacket_PARM_7:
      0000CC                        906 	.ds 3
      0000CF                        907 _AssemblyPacket_PARM_8:
      0000CF                        908 	.ds 3
      0000D2                        909 _AssemblyPacket_GoldCode_65536_297:
      0000D2                        910 	.ds 4
      0000D6                        911 _AssemblyPacket_HMAC_65536_298:
      0000D6                        912 	.ds 4
      0000DA                        913 _AssemblyPacket_CRC32_65536_298:
      0000DA                        914 	.ds 4
                                    915 ;--------------------------------------------------------
                                    916 ; absolute external ram data
                                    917 ;--------------------------------------------------------
                                    918 	.area XABS    (ABS,XDATA)
                                    919 ;--------------------------------------------------------
                                    920 ; external initialized ram data
                                    921 ;--------------------------------------------------------
                                    922 	.area XISEG   (XDATA)
                                    923 	.area HOME    (CODE)
                                    924 	.area GSINIT0 (CODE)
                                    925 	.area GSINIT1 (CODE)
                                    926 	.area GSINIT2 (CODE)
                                    927 	.area GSINIT3 (CODE)
                                    928 	.area GSINIT4 (CODE)
                                    929 	.area GSINIT5 (CODE)
                                    930 	.area GSINIT  (CODE)
                                    931 	.area GSFINAL (CODE)
                                    932 	.area CSEG    (CODE)
                                    933 ;--------------------------------------------------------
                                    934 ; global & static initialisations
                                    935 ;--------------------------------------------------------
                                    936 	.area HOME    (CODE)
                                    937 	.area GSINIT  (CODE)
                                    938 	.area GSFINAL (CODE)
                                    939 	.area GSINIT  (CODE)
                                    940 ;--------------------------------------------------------
                                    941 ; Home
                                    942 ;--------------------------------------------------------
                                    943 	.area HOME    (CODE)
                                    944 	.area HOME    (CODE)
                                    945 ;--------------------------------------------------------
                                    946 ; code
                                    947 ;--------------------------------------------------------
                                    948 	.area CSEG    (CODE)
                                    949 ;------------------------------------------------------------
                                    950 ;Allocation info for local variables in function 'delayms_callback'
                                    951 ;------------------------------------------------------------
                                    952 ;desc                      Allocated with name '_delayms_callback_desc_65536_263'
                                    953 ;------------------------------------------------------------
                                    954 ;	..\src\atrs\misc.c:81: static void delayms_callback(struct wtimer_desc __xdata *desc)
                                    955 ;	-----------------------------------------
                                    956 ;	 function delayms_callback
                                    957 ;	-----------------------------------------
      000929                        958 _delayms_callback:
                           000007   959 	ar7 = 0x07
                           000006   960 	ar6 = 0x06
                           000005   961 	ar5 = 0x05
                           000004   962 	ar4 = 0x04
                           000003   963 	ar3 = 0x03
                           000002   964 	ar2 = 0x02
                           000001   965 	ar1 = 0x01
                           000000   966 	ar0 = 0x00
                                    967 ;	..\src\atrs\misc.c:84: delaymstimer.handler = 0;
      000929 90 00 67         [24]  968 	mov	dptr,#(_delaymstimer + 0x0002)
      00092C E4               [12]  969 	clr	a
      00092D F0               [24]  970 	movx	@dptr,a
      00092E A3               [24]  971 	inc	dptr
      00092F F0               [24]  972 	movx	@dptr,a
                                    973 ;	..\src\atrs\misc.c:85: }
      000930 22               [24]  974 	ret
                                    975 ;------------------------------------------------------------
                                    976 ;Allocation info for local variables in function 'delay_ms'
                                    977 ;------------------------------------------------------------
                                    978 ;ms                        Allocated to registers r6 r7 
                                    979 ;x                         Allocated to registers r4 r5 r6 r7 
                                    980 ;sloc0                     Allocated to stack - _bp +1
                                    981 ;sloc1                     Allocated to stack - _bp +5
                                    982 ;------------------------------------------------------------
                                    983 ;	..\src\atrs\misc.c:91: void delay_ms(uint16_t ms) __reentrant
                                    984 ;	-----------------------------------------
                                    985 ;	 function delay_ms
                                    986 ;	-----------------------------------------
      000931                        987 _delay_ms:
      000931 C0 1F            [24]  988 	push	_bp
      000933 E5 81            [12]  989 	mov	a,sp
      000935 F5 1F            [12]  990 	mov	_bp,a
      000937 24 08            [12]  991 	add	a,#0x08
      000939 F5 81            [12]  992 	mov	sp,a
      00093B AE 82            [24]  993 	mov	r6,dpl
      00093D AF 83            [24]  994 	mov	r7,dph
                                    995 ;	..\src\atrs\misc.c:95: wtimer_remove(&delaymstimer);
      00093F 90 00 65         [24]  996 	mov	dptr,#_delaymstimer
      000942 C0 07            [24]  997 	push	ar7
      000944 C0 06            [24]  998 	push	ar6
      000946 12 85 39         [24]  999 	lcall	_wtimer_remove
      000949 D0 06            [24] 1000 	pop	ar6
      00094B D0 07            [24] 1001 	pop	ar7
                                   1002 ;	..\src\atrs\misc.c:96: x = ms;
      00094D A8 1F            [24] 1003 	mov	r0,_bp
      00094F 08               [12] 1004 	inc	r0
      000950 A6 06            [24] 1005 	mov	@r0,ar6
      000952 08               [12] 1006 	inc	r0
      000953 A6 07            [24] 1007 	mov	@r0,ar7
      000955 08               [12] 1008 	inc	r0
      000956 76 00            [12] 1009 	mov	@r0,#0x00
      000958 08               [12] 1010 	inc	r0
      000959 76 00            [12] 1011 	mov	@r0,#0x00
                                   1012 ;	..\src\atrs\misc.c:97: delaymstimer.time = ms >> 1;
      00095B EF               [12] 1013 	mov	a,r7
      00095C C3               [12] 1014 	clr	c
      00095D 13               [12] 1015 	rrc	a
      00095E CE               [12] 1016 	xch	a,r6
      00095F 13               [12] 1017 	rrc	a
      000960 CE               [12] 1018 	xch	a,r6
      000961 FF               [12] 1019 	mov	r7,a
      000962 8E 04            [24] 1020 	mov	ar4,r6
      000964 8F 05            [24] 1021 	mov	ar5,r7
      000966 7E 00            [12] 1022 	mov	r6,#0x00
      000968 7F 00            [12] 1023 	mov	r7,#0x00
      00096A 90 00 69         [24] 1024 	mov	dptr,#(_delaymstimer + 0x0004)
      00096D EC               [12] 1025 	mov	a,r4
      00096E F0               [24] 1026 	movx	@dptr,a
      00096F ED               [12] 1027 	mov	a,r5
      000970 A3               [24] 1028 	inc	dptr
      000971 F0               [24] 1029 	movx	@dptr,a
      000972 EE               [12] 1030 	mov	a,r6
      000973 A3               [24] 1031 	inc	dptr
      000974 F0               [24] 1032 	movx	@dptr,a
      000975 EF               [12] 1033 	mov	a,r7
      000976 A3               [24] 1034 	inc	dptr
      000977 F0               [24] 1035 	movx	@dptr,a
                                   1036 ;	..\src\atrs\misc.c:98: x <<= 3;
      000978 A8 1F            [24] 1037 	mov	r0,_bp
      00097A 08               [12] 1038 	inc	r0
      00097B 08               [12] 1039 	inc	r0
      00097C 08               [12] 1040 	inc	r0
      00097D 08               [12] 1041 	inc	r0
      00097E E6               [12] 1042 	mov	a,@r0
      00097F 18               [12] 1043 	dec	r0
      000980 C4               [12] 1044 	swap	a
      000981 03               [12] 1045 	rr	a
      000982 54 F8            [12] 1046 	anl	a,#0xf8
      000984 C6               [12] 1047 	xch	a,@r0
      000985 C4               [12] 1048 	swap	a
      000986 03               [12] 1049 	rr	a
      000987 C6               [12] 1050 	xch	a,@r0
      000988 66               [12] 1051 	xrl	a,@r0
      000989 C6               [12] 1052 	xch	a,@r0
      00098A 54 F8            [12] 1053 	anl	a,#0xf8
      00098C C6               [12] 1054 	xch	a,@r0
      00098D 66               [12] 1055 	xrl	a,@r0
      00098E 08               [12] 1056 	inc	r0
      00098F F6               [12] 1057 	mov	@r0,a
      000990 18               [12] 1058 	dec	r0
      000991 18               [12] 1059 	dec	r0
      000992 E6               [12] 1060 	mov	a,@r0
      000993 C4               [12] 1061 	swap	a
      000994 03               [12] 1062 	rr	a
      000995 54 07            [12] 1063 	anl	a,#0x07
      000997 08               [12] 1064 	inc	r0
      000998 46               [12] 1065 	orl	a,@r0
      000999 F6               [12] 1066 	mov	@r0,a
      00099A 18               [12] 1067 	dec	r0
      00099B E6               [12] 1068 	mov	a,@r0
      00099C 18               [12] 1069 	dec	r0
      00099D C4               [12] 1070 	swap	a
      00099E 03               [12] 1071 	rr	a
      00099F 54 F8            [12] 1072 	anl	a,#0xf8
      0009A1 C6               [12] 1073 	xch	a,@r0
      0009A2 C4               [12] 1074 	swap	a
      0009A3 03               [12] 1075 	rr	a
      0009A4 C6               [12] 1076 	xch	a,@r0
      0009A5 66               [12] 1077 	xrl	a,@r0
      0009A6 C6               [12] 1078 	xch	a,@r0
      0009A7 54 F8            [12] 1079 	anl	a,#0xf8
      0009A9 C6               [12] 1080 	xch	a,@r0
      0009AA 66               [12] 1081 	xrl	a,@r0
      0009AB 08               [12] 1082 	inc	r0
      0009AC F6               [12] 1083 	mov	@r0,a
                                   1084 ;	..\src\atrs\misc.c:99: delaymstimer.time -= x;
      0009AD A8 1F            [24] 1085 	mov	r0,_bp
      0009AF 08               [12] 1086 	inc	r0
      0009B0 E5 1F            [12] 1087 	mov	a,_bp
      0009B2 24 05            [12] 1088 	add	a,#0x05
      0009B4 F9               [12] 1089 	mov	r1,a
      0009B5 EC               [12] 1090 	mov	a,r4
      0009B6 C3               [12] 1091 	clr	c
      0009B7 96               [12] 1092 	subb	a,@r0
      0009B8 F7               [12] 1093 	mov	@r1,a
      0009B9 ED               [12] 1094 	mov	a,r5
      0009BA 08               [12] 1095 	inc	r0
      0009BB 96               [12] 1096 	subb	a,@r0
      0009BC 09               [12] 1097 	inc	r1
      0009BD F7               [12] 1098 	mov	@r1,a
      0009BE EE               [12] 1099 	mov	a,r6
      0009BF 08               [12] 1100 	inc	r0
      0009C0 96               [12] 1101 	subb	a,@r0
      0009C1 09               [12] 1102 	inc	r1
      0009C2 F7               [12] 1103 	mov	@r1,a
      0009C3 EF               [12] 1104 	mov	a,r7
      0009C4 08               [12] 1105 	inc	r0
      0009C5 96               [12] 1106 	subb	a,@r0
      0009C6 09               [12] 1107 	inc	r1
      0009C7 F7               [12] 1108 	mov	@r1,a
      0009C8 90 00 69         [24] 1109 	mov	dptr,#(_delaymstimer + 0x0004)
      0009CB E5 1F            [12] 1110 	mov	a,_bp
      0009CD 24 05            [12] 1111 	add	a,#0x05
      0009CF F8               [12] 1112 	mov	r0,a
      0009D0 E6               [12] 1113 	mov	a,@r0
      0009D1 F0               [24] 1114 	movx	@dptr,a
      0009D2 08               [12] 1115 	inc	r0
      0009D3 E6               [12] 1116 	mov	a,@r0
      0009D4 A3               [24] 1117 	inc	dptr
      0009D5 F0               [24] 1118 	movx	@dptr,a
      0009D6 08               [12] 1119 	inc	r0
      0009D7 E6               [12] 1120 	mov	a,@r0
      0009D8 A3               [24] 1121 	inc	dptr
      0009D9 F0               [24] 1122 	movx	@dptr,a
      0009DA 08               [12] 1123 	inc	r0
      0009DB E6               [12] 1124 	mov	a,@r0
      0009DC A3               [24] 1125 	inc	dptr
      0009DD F0               [24] 1126 	movx	@dptr,a
                                   1127 ;	..\src\atrs\misc.c:100: x <<= 3;
      0009DE A8 1F            [24] 1128 	mov	r0,_bp
      0009E0 08               [12] 1129 	inc	r0
      0009E1 08               [12] 1130 	inc	r0
      0009E2 08               [12] 1131 	inc	r0
      0009E3 86 06            [24] 1132 	mov	ar6,@r0
      0009E5 08               [12] 1133 	inc	r0
      0009E6 E6               [12] 1134 	mov	a,@r0
      0009E7 C4               [12] 1135 	swap	a
      0009E8 03               [12] 1136 	rr	a
      0009E9 54 F8            [12] 1137 	anl	a,#0xf8
      0009EB CE               [12] 1138 	xch	a,r6
      0009EC C4               [12] 1139 	swap	a
      0009ED 03               [12] 1140 	rr	a
      0009EE CE               [12] 1141 	xch	a,r6
      0009EF 6E               [12] 1142 	xrl	a,r6
      0009F0 CE               [12] 1143 	xch	a,r6
      0009F1 54 F8            [12] 1144 	anl	a,#0xf8
      0009F3 CE               [12] 1145 	xch	a,r6
      0009F4 6E               [12] 1146 	xrl	a,r6
      0009F5 FF               [12] 1147 	mov	r7,a
      0009F6 18               [12] 1148 	dec	r0
      0009F7 18               [12] 1149 	dec	r0
      0009F8 E6               [12] 1150 	mov	a,@r0
      0009F9 C4               [12] 1151 	swap	a
      0009FA 03               [12] 1152 	rr	a
      0009FB 54 07            [12] 1153 	anl	a,#0x07
      0009FD 4E               [12] 1154 	orl	a,r6
      0009FE FE               [12] 1155 	mov	r6,a
      0009FF 18               [12] 1156 	dec	r0
      000A00 86 04            [24] 1157 	mov	ar4,@r0
      000A02 08               [12] 1158 	inc	r0
      000A03 E6               [12] 1159 	mov	a,@r0
      000A04 C4               [12] 1160 	swap	a
      000A05 03               [12] 1161 	rr	a
      000A06 54 F8            [12] 1162 	anl	a,#0xf8
      000A08 CC               [12] 1163 	xch	a,r4
      000A09 C4               [12] 1164 	swap	a
      000A0A 03               [12] 1165 	rr	a
      000A0B CC               [12] 1166 	xch	a,r4
      000A0C 6C               [12] 1167 	xrl	a,r4
      000A0D CC               [12] 1168 	xch	a,r4
      000A0E 54 F8            [12] 1169 	anl	a,#0xf8
      000A10 CC               [12] 1170 	xch	a,r4
      000A11 6C               [12] 1171 	xrl	a,r4
      000A12 FD               [12] 1172 	mov	r5,a
                                   1173 ;	..\src\atrs\misc.c:101: delaymstimer.time += x;
      000A13 E5 1F            [12] 1174 	mov	a,_bp
      000A15 24 05            [12] 1175 	add	a,#0x05
      000A17 F8               [12] 1176 	mov	r0,a
      000A18 EC               [12] 1177 	mov	a,r4
      000A19 26               [12] 1178 	add	a,@r0
      000A1A F6               [12] 1179 	mov	@r0,a
      000A1B ED               [12] 1180 	mov	a,r5
      000A1C 08               [12] 1181 	inc	r0
      000A1D 36               [12] 1182 	addc	a,@r0
      000A1E F6               [12] 1183 	mov	@r0,a
      000A1F EE               [12] 1184 	mov	a,r6
      000A20 08               [12] 1185 	inc	r0
      000A21 36               [12] 1186 	addc	a,@r0
      000A22 F6               [12] 1187 	mov	@r0,a
      000A23 EF               [12] 1188 	mov	a,r7
      000A24 08               [12] 1189 	inc	r0
      000A25 36               [12] 1190 	addc	a,@r0
      000A26 F6               [12] 1191 	mov	@r0,a
      000A27 90 00 69         [24] 1192 	mov	dptr,#(_delaymstimer + 0x0004)
      000A2A E5 1F            [12] 1193 	mov	a,_bp
      000A2C 24 05            [12] 1194 	add	a,#0x05
      000A2E F8               [12] 1195 	mov	r0,a
      000A2F E6               [12] 1196 	mov	a,@r0
      000A30 F0               [24] 1197 	movx	@dptr,a
      000A31 08               [12] 1198 	inc	r0
      000A32 E6               [12] 1199 	mov	a,@r0
      000A33 A3               [24] 1200 	inc	dptr
      000A34 F0               [24] 1201 	movx	@dptr,a
      000A35 08               [12] 1202 	inc	r0
      000A36 E6               [12] 1203 	mov	a,@r0
      000A37 A3               [24] 1204 	inc	dptr
      000A38 F0               [24] 1205 	movx	@dptr,a
      000A39 08               [12] 1206 	inc	r0
      000A3A E6               [12] 1207 	mov	a,@r0
      000A3B A3               [24] 1208 	inc	dptr
      000A3C F0               [24] 1209 	movx	@dptr,a
                                   1210 ;	..\src\atrs\misc.c:102: x <<= 2;
      000A3D EC               [12] 1211 	mov	a,r4
      000A3E 2C               [12] 1212 	add	a,r4
      000A3F FA               [12] 1213 	mov	r2,a
      000A40 ED               [12] 1214 	mov	a,r5
      000A41 33               [12] 1215 	rlc	a
      000A42 FB               [12] 1216 	mov	r3,a
      000A43 EE               [12] 1217 	mov	a,r6
      000A44 33               [12] 1218 	rlc	a
      000A45 FE               [12] 1219 	mov	r6,a
      000A46 EF               [12] 1220 	mov	a,r7
      000A47 33               [12] 1221 	rlc	a
      000A48 FF               [12] 1222 	mov	r7,a
      000A49 EA               [12] 1223 	mov	a,r2
      000A4A 2A               [12] 1224 	add	a,r2
      000A4B FA               [12] 1225 	mov	r2,a
      000A4C EB               [12] 1226 	mov	a,r3
      000A4D 33               [12] 1227 	rlc	a
      000A4E FB               [12] 1228 	mov	r3,a
      000A4F EE               [12] 1229 	mov	a,r6
      000A50 33               [12] 1230 	rlc	a
      000A51 FE               [12] 1231 	mov	r6,a
      000A52 EF               [12] 1232 	mov	a,r7
      000A53 33               [12] 1233 	rlc	a
      000A54 FF               [12] 1234 	mov	r7,a
                                   1235 ;	..\src\atrs\misc.c:103: delaymstimer.time += x;
      000A55 E5 1F            [12] 1236 	mov	a,_bp
      000A57 24 05            [12] 1237 	add	a,#0x05
      000A59 F8               [12] 1238 	mov	r0,a
      000A5A EA               [12] 1239 	mov	a,r2
      000A5B 26               [12] 1240 	add	a,@r0
      000A5C FA               [12] 1241 	mov	r2,a
      000A5D EB               [12] 1242 	mov	a,r3
      000A5E 08               [12] 1243 	inc	r0
      000A5F 36               [12] 1244 	addc	a,@r0
      000A60 FB               [12] 1245 	mov	r3,a
      000A61 EE               [12] 1246 	mov	a,r6
      000A62 08               [12] 1247 	inc	r0
      000A63 36               [12] 1248 	addc	a,@r0
      000A64 FE               [12] 1249 	mov	r6,a
      000A65 EF               [12] 1250 	mov	a,r7
      000A66 08               [12] 1251 	inc	r0
      000A67 36               [12] 1252 	addc	a,@r0
      000A68 FF               [12] 1253 	mov	r7,a
      000A69 90 00 69         [24] 1254 	mov	dptr,#(_delaymstimer + 0x0004)
      000A6C EA               [12] 1255 	mov	a,r2
      000A6D F0               [24] 1256 	movx	@dptr,a
      000A6E EB               [12] 1257 	mov	a,r3
      000A6F A3               [24] 1258 	inc	dptr
      000A70 F0               [24] 1259 	movx	@dptr,a
      000A71 EE               [12] 1260 	mov	a,r6
      000A72 A3               [24] 1261 	inc	dptr
      000A73 F0               [24] 1262 	movx	@dptr,a
      000A74 EF               [12] 1263 	mov	a,r7
      000A75 A3               [24] 1264 	inc	dptr
      000A76 F0               [24] 1265 	movx	@dptr,a
                                   1266 ;	..\src\atrs\misc.c:104: delaymstimer.handler = delayms_callback;
      000A77 90 00 67         [24] 1267 	mov	dptr,#(_delaymstimer + 0x0002)
      000A7A 74 29            [12] 1268 	mov	a,#_delayms_callback
      000A7C F0               [24] 1269 	movx	@dptr,a
      000A7D 74 09            [12] 1270 	mov	a,#(_delayms_callback >> 8)
      000A7F A3               [24] 1271 	inc	dptr
      000A80 F0               [24] 1272 	movx	@dptr,a
                                   1273 ;	..\src\atrs\misc.c:105: wtimer1_addrelative(&delaymstimer);
      000A81 90 00 65         [24] 1274 	mov	dptr,#_delaymstimer
      000A84 12 78 BF         [24] 1275 	lcall	_wtimer1_addrelative
                                   1276 ;	..\src\atrs\misc.c:106: wtimer_runcallbacks();
      000A87 12 77 0F         [24] 1277 	lcall	_wtimer_runcallbacks
                                   1278 ;	..\src\atrs\misc.c:107: do
      000A8A                       1279 00101$:
                                   1280 ;	..\src\atrs\misc.c:109: wtimer_idle(WTFLAG_CANSTANDBY);
      000A8A 75 82 02         [24] 1281 	mov	dpl,#0x02
      000A8D 12 77 90         [24] 1282 	lcall	_wtimer_idle
                                   1283 ;	..\src\atrs\misc.c:110: wtimer_runcallbacks();
      000A90 12 77 0F         [24] 1284 	lcall	_wtimer_runcallbacks
                                   1285 ;	..\src\atrs\misc.c:111: } while (delaymstimer.handler);
      000A93 90 00 67         [24] 1286 	mov	dptr,#(_delaymstimer + 0x0002)
      000A96 E0               [24] 1287 	movx	a,@dptr
      000A97 FE               [12] 1288 	mov	r6,a
      000A98 A3               [24] 1289 	inc	dptr
      000A99 E0               [24] 1290 	movx	a,@dptr
      000A9A 4E               [12] 1291 	orl	a,r6
      000A9B 70 ED            [24] 1292 	jnz	00101$
                                   1293 ;	..\src\atrs\misc.c:112: }
      000A9D 85 1F 81         [24] 1294 	mov	sp,_bp
      000AA0 D0 1F            [24] 1295 	pop	_bp
      000AA2 22               [24] 1296 	ret
                                   1297 ;------------------------------------------------------------
                                   1298 ;Allocation info for local variables in function 'OriginalMessage_callback'
                                   1299 ;------------------------------------------------------------
                                   1300 ;desc                      Allocated with name '_OriginalMessage_callback_desc_65536_268'
                                   1301 ;------------------------------------------------------------
                                   1302 ;	..\src\atrs\misc.c:122: void OriginalMessage_callback(struct wtimer_desc __xdata *desc)
                                   1303 ;	-----------------------------------------
                                   1304 ;	 function OriginalMessage_callback
                                   1305 ;	-----------------------------------------
      000AA3                       1306 _OriginalMessage_callback:
                                   1307 ;	..\src\atrs\misc.c:128: dbglink_writestr(" transmit ");
      000AA3 90 90 38         [24] 1308 	mov	dptr,#___str_0
      000AA6 75 F0 80         [24] 1309 	mov	b,#0x80
      000AA9 12 82 8A         [24] 1310 	lcall	_dbglink_writestr
                                   1311 ;	..\src\atrs\misc.c:131: axradio_transmit(&remoteaddr, TxPackageOrig, TxPacketLength);
      000AAC 90 00 2A         [24] 1312 	mov	dptr,#_TxPackageOrig
      000AAF E0               [24] 1313 	movx	a,@dptr
      000AB0 FE               [12] 1314 	mov	r6,a
      000AB1 A3               [24] 1315 	inc	dptr
      000AB2 E0               [24] 1316 	movx	a,@dptr
      000AB3 FF               [12] 1317 	mov	r7,a
      000AB4 7D 00            [12] 1318 	mov	r5,#0x00
      000AB6 90 00 34         [24] 1319 	mov	dptr,#_TxPacketLength
      000AB9 E0               [24] 1320 	movx	a,@dptr
      000ABA FC               [12] 1321 	mov	r4,a
      000ABB 7B 00            [12] 1322 	mov	r3,#0x00
      000ABD 90 03 A7         [24] 1323 	mov	dptr,#_axradio_transmit_PARM_2
      000AC0 EE               [12] 1324 	mov	a,r6
      000AC1 F0               [24] 1325 	movx	@dptr,a
      000AC2 EF               [12] 1326 	mov	a,r7
      000AC3 A3               [24] 1327 	inc	dptr
      000AC4 F0               [24] 1328 	movx	@dptr,a
      000AC5 ED               [12] 1329 	mov	a,r5
      000AC6 A3               [24] 1330 	inc	dptr
      000AC7 F0               [24] 1331 	movx	@dptr,a
      000AC8 90 03 AA         [24] 1332 	mov	dptr,#_axradio_transmit_PARM_3
      000ACB EC               [12] 1333 	mov	a,r4
      000ACC F0               [24] 1334 	movx	@dptr,a
      000ACD EB               [12] 1335 	mov	a,r3
      000ACE A3               [24] 1336 	inc	dptr
      000ACF F0               [24] 1337 	movx	@dptr,a
      000AD0 90 91 31         [24] 1338 	mov	dptr,#_remoteaddr
      000AD3 75 F0 80         [24] 1339 	mov	b,#0x80
                                   1340 ;	..\src\atrs\misc.c:133: }
      000AD6 02 4A 16         [24] 1341 	ljmp	_axradio_transmit
                                   1342 ;------------------------------------------------------------
                                   1343 ;Allocation info for local variables in function 'TwinMessage1_callback'
                                   1344 ;------------------------------------------------------------
                                   1345 ;desc                      Allocated with name '_TwinMessage1_callback_desc_65536_270'
                                   1346 ;------------------------------------------------------------
                                   1347 ;	..\src\atrs\misc.c:143: void TwinMessage1_callback(struct wtimer_desc __xdata *desc)
                                   1348 ;	-----------------------------------------
                                   1349 ;	 function TwinMessage1_callback
                                   1350 ;	-----------------------------------------
      000AD9                       1351 _TwinMessage1_callback:
                                   1352 ;	..\src\atrs\misc.c:149: axradio_transmit(&remoteaddr, TxPackageTwin1, TxPacketLength);
      000AD9 90 00 2C         [24] 1353 	mov	dptr,#_TxPackageTwin1
      000ADC E0               [24] 1354 	movx	a,@dptr
      000ADD FE               [12] 1355 	mov	r6,a
      000ADE A3               [24] 1356 	inc	dptr
      000ADF E0               [24] 1357 	movx	a,@dptr
      000AE0 FF               [12] 1358 	mov	r7,a
      000AE1 7D 00            [12] 1359 	mov	r5,#0x00
      000AE3 90 00 34         [24] 1360 	mov	dptr,#_TxPacketLength
      000AE6 E0               [24] 1361 	movx	a,@dptr
      000AE7 FC               [12] 1362 	mov	r4,a
      000AE8 7B 00            [12] 1363 	mov	r3,#0x00
      000AEA 90 03 A7         [24] 1364 	mov	dptr,#_axradio_transmit_PARM_2
      000AED EE               [12] 1365 	mov	a,r6
      000AEE F0               [24] 1366 	movx	@dptr,a
      000AEF EF               [12] 1367 	mov	a,r7
      000AF0 A3               [24] 1368 	inc	dptr
      000AF1 F0               [24] 1369 	movx	@dptr,a
      000AF2 ED               [12] 1370 	mov	a,r5
      000AF3 A3               [24] 1371 	inc	dptr
      000AF4 F0               [24] 1372 	movx	@dptr,a
      000AF5 90 03 AA         [24] 1373 	mov	dptr,#_axradio_transmit_PARM_3
      000AF8 EC               [12] 1374 	mov	a,r4
      000AF9 F0               [24] 1375 	movx	@dptr,a
      000AFA EB               [12] 1376 	mov	a,r3
      000AFB A3               [24] 1377 	inc	dptr
      000AFC F0               [24] 1378 	movx	@dptr,a
      000AFD 90 91 31         [24] 1379 	mov	dptr,#_remoteaddr
      000B00 75 F0 80         [24] 1380 	mov	b,#0x80
                                   1381 ;	..\src\atrs\misc.c:150: }
      000B03 02 4A 16         [24] 1382 	ljmp	_axradio_transmit
                                   1383 ;------------------------------------------------------------
                                   1384 ;Allocation info for local variables in function 'TwinMessage2_callback'
                                   1385 ;------------------------------------------------------------
                                   1386 ;desc                      Allocated with name '_TwinMessage2_callback_desc_65536_272'
                                   1387 ;------------------------------------------------------------
                                   1388 ;	..\src\atrs\misc.c:161: void TwinMessage2_callback(struct wtimer_desc __xdata *desc)
                                   1389 ;	-----------------------------------------
                                   1390 ;	 function TwinMessage2_callback
                                   1391 ;	-----------------------------------------
      000B06                       1392 _TwinMessage2_callback:
                                   1393 ;	..\src\atrs\misc.c:166: axradio_transmit(&remoteaddr, TxPackageTwin2, TxPacketLength);
      000B06 90 00 2E         [24] 1394 	mov	dptr,#_TxPackageTwin2
      000B09 E0               [24] 1395 	movx	a,@dptr
      000B0A FE               [12] 1396 	mov	r6,a
      000B0B A3               [24] 1397 	inc	dptr
      000B0C E0               [24] 1398 	movx	a,@dptr
      000B0D FF               [12] 1399 	mov	r7,a
      000B0E 7D 00            [12] 1400 	mov	r5,#0x00
      000B10 90 00 34         [24] 1401 	mov	dptr,#_TxPacketLength
      000B13 E0               [24] 1402 	movx	a,@dptr
      000B14 FC               [12] 1403 	mov	r4,a
      000B15 7B 00            [12] 1404 	mov	r3,#0x00
      000B17 90 03 A7         [24] 1405 	mov	dptr,#_axradio_transmit_PARM_2
      000B1A EE               [12] 1406 	mov	a,r6
      000B1B F0               [24] 1407 	movx	@dptr,a
      000B1C EF               [12] 1408 	mov	a,r7
      000B1D A3               [24] 1409 	inc	dptr
      000B1E F0               [24] 1410 	movx	@dptr,a
      000B1F ED               [12] 1411 	mov	a,r5
      000B20 A3               [24] 1412 	inc	dptr
      000B21 F0               [24] 1413 	movx	@dptr,a
      000B22 90 03 AA         [24] 1414 	mov	dptr,#_axradio_transmit_PARM_3
      000B25 EC               [12] 1415 	mov	a,r4
      000B26 F0               [24] 1416 	movx	@dptr,a
      000B27 EB               [12] 1417 	mov	a,r3
      000B28 A3               [24] 1418 	inc	dptr
      000B29 F0               [24] 1419 	movx	@dptr,a
      000B2A 90 91 31         [24] 1420 	mov	dptr,#_remoteaddr
      000B2D 75 F0 80         [24] 1421 	mov	b,#0x80
                                   1422 ;	..\src\atrs\misc.c:168: }
      000B30 02 4A 16         [24] 1423 	ljmp	_axradio_transmit
                                   1424 ;------------------------------------------------------------
                                   1425 ;Allocation info for local variables in function 'GPS_callback'
                                   1426 ;------------------------------------------------------------
                                   1427 ;desc                      Allocated with name '_GPS_callback_desc_65536_274'
                                   1428 ;------------------------------------------------------------
                                   1429 ;	..\src\atrs\misc.c:179: void GPS_callback(struct wtimer_desc __xdata *desc)
                                   1430 ;	-----------------------------------------
                                   1431 ;	 function GPS_callback
                                   1432 ;	-----------------------------------------
      000B33                       1433 _GPS_callback:
                                   1434 ;	..\src\atrs\misc.c:183: dbglink_writestr(" \r\n GPS callback \r\n");
      000B33 90 90 43         [24] 1435 	mov	dptr,#___str_1
      000B36 75 F0 80         [24] 1436 	mov	b,#0x80
      000B39 12 82 8A         [24] 1437 	lcall	_dbglink_writestr
                                   1438 ;	..\src\atrs\misc.c:185: GPS_PoolFlag = 1;
      000B3C 90 00 A5         [24] 1439 	mov	dptr,#_GPS_PoolFlag
      000B3F 74 01            [12] 1440 	mov	a,#0x01
      000B41 F0               [24] 1441 	movx	@dptr,a
                                   1442 ;	..\src\atrs\misc.c:187: wtimer0_remove(&GPS_tmr);
      000B42 90 00 85         [24] 1443 	mov	dptr,#_GPS_tmr
      000B45 12 7C 24         [24] 1444 	lcall	_wtimer0_remove
                                   1445 ;	..\src\atrs\misc.c:188: GPS_tmr.time +=  (uint32_t) TIME_FOR_TIMER_SEC(GPS_POLL_TIME_S);//(uint32_t) TIME_FOR_TIMER_SEC(10); //+= ? o solo = ? verificar esto
      000B48 90 00 89         [24] 1446 	mov	dptr,#(_GPS_tmr + 0x0004)
      000B4B E0               [24] 1447 	movx	a,@dptr
      000B4C FC               [12] 1448 	mov	r4,a
      000B4D A3               [24] 1449 	inc	dptr
      000B4E E0               [24] 1450 	movx	a,@dptr
      000B4F FD               [12] 1451 	mov	r5,a
      000B50 A3               [24] 1452 	inc	dptr
      000B51 E0               [24] 1453 	movx	a,@dptr
      000B52 FE               [12] 1454 	mov	r6,a
      000B53 A3               [24] 1455 	inc	dptr
      000B54 E0               [24] 1456 	movx	a,@dptr
      000B55 FF               [12] 1457 	mov	r7,a
      000B56 74 19            [12] 1458 	mov	a,#0x19
      000B58 2D               [12] 1459 	add	a,r5
      000B59 FD               [12] 1460 	mov	r5,a
      000B5A E4               [12] 1461 	clr	a
      000B5B 3E               [12] 1462 	addc	a,r6
      000B5C FE               [12] 1463 	mov	r6,a
      000B5D E4               [12] 1464 	clr	a
      000B5E 3F               [12] 1465 	addc	a,r7
      000B5F FF               [12] 1466 	mov	r7,a
      000B60 90 00 89         [24] 1467 	mov	dptr,#(_GPS_tmr + 0x0004)
      000B63 EC               [12] 1468 	mov	a,r4
      000B64 F0               [24] 1469 	movx	@dptr,a
      000B65 ED               [12] 1470 	mov	a,r5
      000B66 A3               [24] 1471 	inc	dptr
      000B67 F0               [24] 1472 	movx	@dptr,a
      000B68 EE               [12] 1473 	mov	a,r6
      000B69 A3               [24] 1474 	inc	dptr
      000B6A F0               [24] 1475 	movx	@dptr,a
      000B6B EF               [12] 1476 	mov	a,r7
      000B6C A3               [24] 1477 	inc	dptr
      000B6D F0               [24] 1478 	movx	@dptr,a
                                   1479 ;	..\src\atrs\misc.c:189: wtimer0_addrelative(&GPS_tmr);
      000B6E 90 00 85         [24] 1480 	mov	dptr,#_GPS_tmr
                                   1481 ;	..\src\atrs\misc.c:192: }
      000B71 02 78 78         [24] 1482 	ljmp	_wtimer0_addrelative
                                   1483 ;------------------------------------------------------------
                                   1484 ;Allocation info for local variables in function 'Channel_change_callback'
                                   1485 ;------------------------------------------------------------
                                   1486 ;desc                      Allocated with name '_Channel_change_callback_desc_65536_276'
                                   1487 ;------------------------------------------------------------
                                   1488 ;	..\src\atrs\misc.c:201: void Channel_change_callback(struct wtimer_desc __xdata *desc)
                                   1489 ;	-----------------------------------------
                                   1490 ;	 function Channel_change_callback
                                   1491 ;	-----------------------------------------
      000B74                       1492 _Channel_change_callback:
                                   1493 ;	..\src\atrs\misc.c:207: if(ChannelChangeActive)
      000B74 90 00 36         [24] 1494 	mov	dptr,#_ChannelChangeActive
      000B77 E0               [24] 1495 	movx	a,@dptr
      000B78 70 01            [24] 1496 	jnz	00117$
      000B7A 22               [24] 1497 	ret
      000B7B                       1498 00117$:
                                   1499 ;	..\src\atrs\misc.c:210: axradio_set_channel(FREQHZ_TO_AXFREQ(freq));
      000B7B 90 0A 30         [24] 1500 	mov	dptr,#_freq
      000B7E E0               [24] 1501 	movx	a,@dptr
      000B7F FC               [12] 1502 	mov	r4,a
      000B80 A3               [24] 1503 	inc	dptr
      000B81 E0               [24] 1504 	movx	a,@dptr
      000B82 FD               [12] 1505 	mov	r5,a
      000B83 A3               [24] 1506 	inc	dptr
      000B84 E0               [24] 1507 	movx	a,@dptr
      000B85 FE               [12] 1508 	mov	r6,a
      000B86 A3               [24] 1509 	inc	dptr
      000B87 E0               [24] 1510 	movx	a,@dptr
      000B88 8C 82            [24] 1511 	mov	dpl,r4
      000B8A 8D 83            [24] 1512 	mov	dph,r5
      000B8C 8E F0            [24] 1513 	mov	b,r6
      000B8E 12 79 06         [24] 1514 	lcall	___ulong2fs
      000B91 AC 82            [24] 1515 	mov	r4,dpl
      000B93 AD 83            [24] 1516 	mov	r5,dph
      000B95 AE F0            [24] 1517 	mov	r6,b
      000B97 FF               [12] 1518 	mov	r7,a
      000B98 E4               [12] 1519 	clr	a
      000B99 C0 E0            [24] 1520 	push	acc
      000B9B 74 1B            [12] 1521 	mov	a,#0x1b
      000B9D C0 E0            [24] 1522 	push	acc
      000B9F 74 37            [12] 1523 	mov	a,#0x37
      000BA1 C0 E0            [24] 1524 	push	acc
      000BA3 74 4C            [12] 1525 	mov	a,#0x4c
      000BA5 C0 E0            [24] 1526 	push	acc
      000BA7 8C 82            [24] 1527 	mov	dpl,r4
      000BA9 8D 83            [24] 1528 	mov	dph,r5
      000BAB 8E F0            [24] 1529 	mov	b,r6
      000BAD EF               [12] 1530 	mov	a,r7
      000BAE 12 8C 15         [24] 1531 	lcall	___fsdiv
      000BB1 AC 82            [24] 1532 	mov	r4,dpl
      000BB3 AD 83            [24] 1533 	mov	r5,dph
      000BB5 AE F0            [24] 1534 	mov	r6,b
      000BB7 FF               [12] 1535 	mov	r7,a
      000BB8 E5 81            [12] 1536 	mov	a,sp
      000BBA 24 FC            [12] 1537 	add	a,#0xfc
      000BBC F5 81            [12] 1538 	mov	sp,a
      000BBE C0 04            [24] 1539 	push	ar4
      000BC0 C0 05            [24] 1540 	push	ar5
      000BC2 C0 06            [24] 1541 	push	ar6
      000BC4 C0 07            [24] 1542 	push	ar7
      000BC6 90 00 00         [24] 1543 	mov	dptr,#0x0000
      000BC9 75 F0 80         [24] 1544 	mov	b,#0x80
      000BCC 74 4B            [12] 1545 	mov	a,#0x4b
      000BCE 12 6F 8C         [24] 1546 	lcall	___fsmul
      000BD1 AC 82            [24] 1547 	mov	r4,dpl
      000BD3 AD 83            [24] 1548 	mov	r5,dph
      000BD5 AE F0            [24] 1549 	mov	r6,b
      000BD7 FF               [12] 1550 	mov	r7,a
      000BD8 E5 81            [12] 1551 	mov	a,sp
      000BDA 24 FC            [12] 1552 	add	a,#0xfc
      000BDC F5 81            [12] 1553 	mov	sp,a
      000BDE 8C 82            [24] 1554 	mov	dpl,r4
      000BE0 8D 83            [24] 1555 	mov	dph,r5
      000BE2 8E F0            [24] 1556 	mov	b,r6
      000BE4 EF               [12] 1557 	mov	a,r7
      000BE5 12 79 19         [24] 1558 	lcall	___fs2ulong
      000BE8 12 47 F4         [24] 1559 	lcall	_axradio_set_channel
                                   1560 ;	..\src\atrs\misc.c:211: freq +=600;
      000BEB 90 0A 30         [24] 1561 	mov	dptr,#_freq
      000BEE E0               [24] 1562 	movx	a,@dptr
      000BEF FC               [12] 1563 	mov	r4,a
      000BF0 A3               [24] 1564 	inc	dptr
      000BF1 E0               [24] 1565 	movx	a,@dptr
      000BF2 FD               [12] 1566 	mov	r5,a
      000BF3 A3               [24] 1567 	inc	dptr
      000BF4 E0               [24] 1568 	movx	a,@dptr
      000BF5 FE               [12] 1569 	mov	r6,a
      000BF6 A3               [24] 1570 	inc	dptr
      000BF7 E0               [24] 1571 	movx	a,@dptr
      000BF8 FF               [12] 1572 	mov	r7,a
      000BF9 90 0A 30         [24] 1573 	mov	dptr,#_freq
      000BFC 74 58            [12] 1574 	mov	a,#0x58
      000BFE 2C               [12] 1575 	add	a,r4
      000BFF F0               [24] 1576 	movx	@dptr,a
      000C00 74 02            [12] 1577 	mov	a,#0x02
      000C02 3D               [12] 1578 	addc	a,r5
      000C03 A3               [24] 1579 	inc	dptr
      000C04 F0               [24] 1580 	movx	@dptr,a
      000C05 E4               [12] 1581 	clr	a
      000C06 3E               [12] 1582 	addc	a,r6
      000C07 A3               [24] 1583 	inc	dptr
      000C08 F0               [24] 1584 	movx	@dptr,a
      000C09 E4               [12] 1585 	clr	a
      000C0A 3F               [12] 1586 	addc	a,r7
      000C0B A3               [24] 1587 	inc	dptr
      000C0C F0               [24] 1588 	movx	@dptr,a
                                   1589 ;	..\src\atrs\misc.c:212: dbglink_writestr(" frecuencia: ");
      000C0D 90 90 57         [24] 1590 	mov	dptr,#___str_2
      000C10 75 F0 80         [24] 1591 	mov	b,#0x80
      000C13 12 82 8A         [24] 1592 	lcall	_dbglink_writestr
                                   1593 ;	..\src\atrs\misc.c:213: wtimer0_remove(&Channel_change);
      000C16 90 00 9D         [24] 1594 	mov	dptr,#_Channel_change
      000C19 12 7C 24         [24] 1595 	lcall	_wtimer0_remove
                                   1596 ;	..\src\atrs\misc.c:214: dbglink_writenum32(freq,1,WRNUM_PADZERO);
      000C1C 90 0A 30         [24] 1597 	mov	dptr,#_freq
      000C1F E0               [24] 1598 	movx	a,@dptr
      000C20 FC               [12] 1599 	mov	r4,a
      000C21 A3               [24] 1600 	inc	dptr
      000C22 E0               [24] 1601 	movx	a,@dptr
      000C23 FD               [12] 1602 	mov	r5,a
      000C24 A3               [24] 1603 	inc	dptr
      000C25 E0               [24] 1604 	movx	a,@dptr
      000C26 FE               [12] 1605 	mov	r6,a
      000C27 A3               [24] 1606 	inc	dptr
      000C28 E0               [24] 1607 	movx	a,@dptr
      000C29 FF               [12] 1608 	mov	r7,a
      000C2A 74 08            [12] 1609 	mov	a,#0x08
      000C2C C0 E0            [24] 1610 	push	acc
      000C2E 74 01            [12] 1611 	mov	a,#0x01
      000C30 C0 E0            [24] 1612 	push	acc
      000C32 8C 82            [24] 1613 	mov	dpl,r4
      000C34 8D 83            [24] 1614 	mov	dph,r5
      000C36 8E F0            [24] 1615 	mov	b,r6
      000C38 EF               [12] 1616 	mov	a,r7
      000C39 12 8A ED         [24] 1617 	lcall	_dbglink_writenum32
      000C3C 15 81            [12] 1618 	dec	sp
      000C3E 15 81            [12] 1619 	dec	sp
                                   1620 ;	..\src\atrs\misc.c:215: if( freq > FREQUENCY_HZ + 10000) freq = (FREQUENCY_HZ -10000);
      000C40 90 0A 30         [24] 1621 	mov	dptr,#_freq
      000C43 E0               [24] 1622 	movx	a,@dptr
      000C44 FC               [12] 1623 	mov	r4,a
      000C45 A3               [24] 1624 	inc	dptr
      000C46 E0               [24] 1625 	movx	a,@dptr
      000C47 FD               [12] 1626 	mov	r5,a
      000C48 A3               [24] 1627 	inc	dptr
      000C49 E0               [24] 1628 	movx	a,@dptr
      000C4A FE               [12] 1629 	mov	r6,a
      000C4B A3               [24] 1630 	inc	dptr
      000C4C E0               [24] 1631 	movx	a,@dptr
      000C4D FF               [12] 1632 	mov	r7,a
      000C4E C3               [12] 1633 	clr	c
      000C4F 74 E0            [12] 1634 	mov	a,#0xe0
      000C51 9C               [12] 1635 	subb	a,r4
      000C52 74 4A            [12] 1636 	mov	a,#0x4a
      000C54 9D               [12] 1637 	subb	a,r5
      000C55 74 06            [12] 1638 	mov	a,#0x06
      000C57 9E               [12] 1639 	subb	a,r6
      000C58 74 1A            [12] 1640 	mov	a,#0x1a
      000C5A 9F               [12] 1641 	subb	a,r7
      000C5B 50 12            [24] 1642 	jnc	00104$
      000C5D 90 0A 30         [24] 1643 	mov	dptr,#_freq
      000C60 74 C0            [12] 1644 	mov	a,#0xc0
      000C62 F0               [24] 1645 	movx	@dptr,a
      000C63 74 FC            [12] 1646 	mov	a,#0xfc
      000C65 A3               [24] 1647 	inc	dptr
      000C66 F0               [24] 1648 	movx	@dptr,a
      000C67 74 05            [12] 1649 	mov	a,#0x05
      000C69 A3               [24] 1650 	inc	dptr
      000C6A F0               [24] 1651 	movx	@dptr,a
      000C6B 74 1A            [12] 1652 	mov	a,#0x1a
      000C6D A3               [24] 1653 	inc	dptr
      000C6E F0               [24] 1654 	movx	@dptr,a
      000C6F                       1655 00104$:
                                   1656 ;	..\src\atrs\misc.c:216: Channel_change.time +=  (uint32_t) TIME_FOR_TIMER_MSEC(1000);//(uint32_t) TIME_FOR_TIMER_SEC(10); //+= ? o solo = ? verificar esto
      000C6F 90 00 A1         [24] 1657 	mov	dptr,#(_Channel_change + 0x0004)
      000C72 E0               [24] 1658 	movx	a,@dptr
      000C73 FC               [12] 1659 	mov	r4,a
      000C74 A3               [24] 1660 	inc	dptr
      000C75 E0               [24] 1661 	movx	a,@dptr
      000C76 FD               [12] 1662 	mov	r5,a
      000C77 A3               [24] 1663 	inc	dptr
      000C78 E0               [24] 1664 	movx	a,@dptr
      000C79 FE               [12] 1665 	mov	r6,a
      000C7A A3               [24] 1666 	inc	dptr
      000C7B E0               [24] 1667 	movx	a,@dptr
      000C7C FF               [12] 1668 	mov	r7,a
      000C7D 74 80            [12] 1669 	mov	a,#0x80
      000C7F 2C               [12] 1670 	add	a,r4
      000C80 FC               [12] 1671 	mov	r4,a
      000C81 74 02            [12] 1672 	mov	a,#0x02
      000C83 3D               [12] 1673 	addc	a,r5
      000C84 FD               [12] 1674 	mov	r5,a
      000C85 E4               [12] 1675 	clr	a
      000C86 3E               [12] 1676 	addc	a,r6
      000C87 FE               [12] 1677 	mov	r6,a
      000C88 E4               [12] 1678 	clr	a
      000C89 3F               [12] 1679 	addc	a,r7
      000C8A FF               [12] 1680 	mov	r7,a
      000C8B 90 00 A1         [24] 1681 	mov	dptr,#(_Channel_change + 0x0004)
      000C8E EC               [12] 1682 	mov	a,r4
      000C8F F0               [24] 1683 	movx	@dptr,a
      000C90 ED               [12] 1684 	mov	a,r5
      000C91 A3               [24] 1685 	inc	dptr
      000C92 F0               [24] 1686 	movx	@dptr,a
      000C93 EE               [12] 1687 	mov	a,r6
      000C94 A3               [24] 1688 	inc	dptr
      000C95 F0               [24] 1689 	movx	@dptr,a
      000C96 EF               [12] 1690 	mov	a,r7
      000C97 A3               [24] 1691 	inc	dptr
      000C98 F0               [24] 1692 	movx	@dptr,a
                                   1693 ;	..\src\atrs\misc.c:217: wtimer0_addabsolute(&Channel_change);
      000C99 90 00 9D         [24] 1694 	mov	dptr,#_Channel_change
                                   1695 ;	..\src\atrs\misc.c:220: }
      000C9C 02 79 8E         [24] 1696 	ljmp	_wtimer0_addabsolute
                                   1697 ;------------------------------------------------------------
                                   1698 ;Allocation info for local variables in function 'BackOff_Callback'
                                   1699 ;------------------------------------------------------------
                                   1700 ;desc                      Allocated with name '_BackOff_Callback_desc_65536_279'
                                   1701 ;------------------------------------------------------------
                                   1702 ;	..\src\atrs\misc.c:231: void BackOff_Callback(struct wtimer_desc __xdata *desc)
                                   1703 ;	-----------------------------------------
                                   1704 ;	 function BackOff_Callback
                                   1705 ;	-----------------------------------------
      000C9F                       1706 _BackOff_Callback:
                                   1707 ;	..\src\atrs\misc.c:235: BackOffFlag = 1;
      000C9F 90 00 35         [24] 1708 	mov	dptr,#_BackOffFlag
      000CA2 74 01            [12] 1709 	mov	a,#0x01
      000CA4 F0               [24] 1710 	movx	@dptr,a
                                   1711 ;	..\src\atrs\misc.c:237: dbglink_writestr(" \n backoff timer expired ");
      000CA5 90 90 65         [24] 1712 	mov	dptr,#___str_3
      000CA8 75 F0 80         [24] 1713 	mov	b,#0x80
                                   1714 ;	..\src\atrs\misc.c:240: }
      000CAB 02 82 8A         [24] 1715 	ljmp	_dbglink_writestr
                                   1716 ;------------------------------------------------------------
                                   1717 ;Allocation info for local variables in function 'ACK_Callback'
                                   1718 ;------------------------------------------------------------
                                   1719 ;desc                      Allocated with name '_ACK_Callback_desc_65536_281'
                                   1720 ;------------------------------------------------------------
                                   1721 ;	..\src\atrs\misc.c:250: __reentrantb void ACK_Callback(struct wtimer_desc __xdata *desc)
                                   1722 ;	-----------------------------------------
                                   1723 ;	 function ACK_Callback
                                   1724 ;	-----------------------------------------
      000CAE                       1725 _ACK_Callback:
                                   1726 ;	..\src\atrs\misc.c:255: dbglink_writestr("ACK timer expired");
      000CAE 90 90 7F         [24] 1727 	mov	dptr,#___str_4
      000CB1 75 F0 80         [24] 1728 	mov	b,#0x80
      000CB4 12 82 8A         [24] 1729 	lcall	_dbglink_writestr
                                   1730 ;	..\src\atrs\misc.c:257: ACKTimerFlag = 1;
      000CB7 90 00 08         [24] 1731 	mov	dptr,#_ACKTimerFlag
      000CBA 74 01            [12] 1732 	mov	a,#0x01
      000CBC F0               [24] 1733 	movx	@dptr,a
      000CBD E4               [12] 1734 	clr	a
      000CBE A3               [24] 1735 	inc	dptr
      000CBF F0               [24] 1736 	movx	@dptr,a
                                   1737 ;	..\src\atrs\misc.c:259: }
      000CC0 22               [24] 1738 	ret
                                   1739 ;------------------------------------------------------------
                                   1740 ;Allocation info for local variables in function 'delay_tx'
                                   1741 ;------------------------------------------------------------
                                   1742 ;time2_ms                  Allocated with name '_delay_tx_PARM_2'
                                   1743 ;time3_ms                  Allocated with name '_delay_tx_PARM_3'
                                   1744 ;time1_ms                  Allocated with name '_delay_tx_time1_ms_65536_283'
                                   1745 ;------------------------------------------------------------
                                   1746 ;	..\src\atrs\misc.c:273: void delay_tx(uint32_t time1_ms, uint32_t time2_ms, uint32_t time3_ms)
                                   1747 ;	-----------------------------------------
                                   1748 ;	 function delay_tx
                                   1749 ;	-----------------------------------------
      000CC1                       1750 _delay_tx:
      000CC1 AF 82            [24] 1751 	mov	r7,dpl
      000CC3 AE 83            [24] 1752 	mov	r6,dph
      000CC5 AD F0            [24] 1753 	mov	r5,b
      000CC7 FC               [12] 1754 	mov	r4,a
      000CC8 90 00 AE         [24] 1755 	mov	dptr,#_delay_tx_time1_ms_65536_283
      000CCB EF               [12] 1756 	mov	a,r7
      000CCC F0               [24] 1757 	movx	@dptr,a
      000CCD EE               [12] 1758 	mov	a,r6
      000CCE A3               [24] 1759 	inc	dptr
      000CCF F0               [24] 1760 	movx	@dptr,a
      000CD0 ED               [12] 1761 	mov	a,r5
      000CD1 A3               [24] 1762 	inc	dptr
      000CD2 F0               [24] 1763 	movx	@dptr,a
      000CD3 EC               [12] 1764 	mov	a,r4
      000CD4 A3               [24] 1765 	inc	dptr
      000CD5 F0               [24] 1766 	movx	@dptr,a
                                   1767 ;	..\src\atrs\misc.c:275: wtimer0_remove(&OriginalMessage_tmr);
      000CD6 90 00 6D         [24] 1768 	mov	dptr,#_OriginalMessage_tmr
      000CD9 12 7C 24         [24] 1769 	lcall	_wtimer0_remove
                                   1770 ;	..\src\atrs\misc.c:276: OriginalMessage_tmr.handler = OriginalMessage_callback;//Handler for timer 0
      000CDC 90 00 6F         [24] 1771 	mov	dptr,#(_OriginalMessage_tmr + 0x0002)
      000CDF 74 A3            [12] 1772 	mov	a,#_OriginalMessage_callback
      000CE1 F0               [24] 1773 	movx	@dptr,a
      000CE2 74 0A            [12] 1774 	mov	a,#(_OriginalMessage_callback >> 8)
      000CE4 A3               [24] 1775 	inc	dptr
      000CE5 F0               [24] 1776 	movx	@dptr,a
                                   1777 ;	..\src\atrs\misc.c:277: time1_ms = (uint32_t) TIME_FOR_TIMER_MSEC(time1_ms);
      000CE6 90 00 AE         [24] 1778 	mov	dptr,#_delay_tx_time1_ms_65536_283
      000CE9 E0               [24] 1779 	movx	a,@dptr
      000CEA FC               [12] 1780 	mov	r4,a
      000CEB A3               [24] 1781 	inc	dptr
      000CEC E0               [24] 1782 	movx	a,@dptr
      000CED FD               [12] 1783 	mov	r5,a
      000CEE A3               [24] 1784 	inc	dptr
      000CEF E0               [24] 1785 	movx	a,@dptr
      000CF0 FE               [12] 1786 	mov	r6,a
      000CF1 A3               [24] 1787 	inc	dptr
      000CF2 E0               [24] 1788 	movx	a,@dptr
      000CF3 8C 82            [24] 1789 	mov	dpl,r4
      000CF5 8D 83            [24] 1790 	mov	dph,r5
      000CF7 8E F0            [24] 1791 	mov	b,r6
      000CF9 12 79 06         [24] 1792 	lcall	___ulong2fs
      000CFC AC 82            [24] 1793 	mov	r4,dpl
      000CFE AD 83            [24] 1794 	mov	r5,dph
      000D00 AE F0            [24] 1795 	mov	r6,b
      000D02 FF               [12] 1796 	mov	r7,a
      000D03 C0 04            [24] 1797 	push	ar4
      000D05 C0 05            [24] 1798 	push	ar5
      000D07 C0 06            [24] 1799 	push	ar6
      000D09 C0 07            [24] 1800 	push	ar7
      000D0B 90 00 00         [24] 1801 	mov	dptr,#0x0000
      000D0E 75 F0 20         [24] 1802 	mov	b,#0x20
      000D11 74 44            [12] 1803 	mov	a,#0x44
      000D13 12 6F 8C         [24] 1804 	lcall	___fsmul
      000D16 AC 82            [24] 1805 	mov	r4,dpl
      000D18 AD 83            [24] 1806 	mov	r5,dph
      000D1A AE F0            [24] 1807 	mov	r6,b
      000D1C FF               [12] 1808 	mov	r7,a
      000D1D E5 81            [12] 1809 	mov	a,sp
      000D1F 24 FC            [12] 1810 	add	a,#0xfc
      000D21 F5 81            [12] 1811 	mov	sp,a
      000D23 E4               [12] 1812 	clr	a
      000D24 C0 E0            [24] 1813 	push	acc
      000D26 C0 E0            [24] 1814 	push	acc
      000D28 74 7A            [12] 1815 	mov	a,#0x7a
      000D2A C0 E0            [24] 1816 	push	acc
      000D2C 74 44            [12] 1817 	mov	a,#0x44
      000D2E C0 E0            [24] 1818 	push	acc
      000D30 8C 82            [24] 1819 	mov	dpl,r4
      000D32 8D 83            [24] 1820 	mov	dph,r5
      000D34 8E F0            [24] 1821 	mov	b,r6
      000D36 EF               [12] 1822 	mov	a,r7
      000D37 12 8C 15         [24] 1823 	lcall	___fsdiv
      000D3A AC 82            [24] 1824 	mov	r4,dpl
      000D3C AD 83            [24] 1825 	mov	r5,dph
      000D3E AE F0            [24] 1826 	mov	r6,b
      000D40 FF               [12] 1827 	mov	r7,a
      000D41 E5 81            [12] 1828 	mov	a,sp
      000D43 24 FC            [12] 1829 	add	a,#0xfc
      000D45 F5 81            [12] 1830 	mov	sp,a
      000D47 8C 82            [24] 1831 	mov	dpl,r4
      000D49 8D 83            [24] 1832 	mov	dph,r5
      000D4B 8E F0            [24] 1833 	mov	b,r6
      000D4D EF               [12] 1834 	mov	a,r7
      000D4E 12 79 19         [24] 1835 	lcall	___fs2ulong
      000D51 AC 82            [24] 1836 	mov	r4,dpl
      000D53 AD 83            [24] 1837 	mov	r5,dph
      000D55 AE F0            [24] 1838 	mov	r6,b
      000D57 FF               [12] 1839 	mov	r7,a
      000D58 90 00 AE         [24] 1840 	mov	dptr,#_delay_tx_time1_ms_65536_283
      000D5B EC               [12] 1841 	mov	a,r4
      000D5C F0               [24] 1842 	movx	@dptr,a
      000D5D ED               [12] 1843 	mov	a,r5
      000D5E A3               [24] 1844 	inc	dptr
      000D5F F0               [24] 1845 	movx	@dptr,a
      000D60 EE               [12] 1846 	mov	a,r6
      000D61 A3               [24] 1847 	inc	dptr
      000D62 F0               [24] 1848 	movx	@dptr,a
      000D63 EF               [12] 1849 	mov	a,r7
      000D64 A3               [24] 1850 	inc	dptr
      000D65 F0               [24] 1851 	movx	@dptr,a
                                   1852 ;	..\src\atrs\misc.c:278: OriginalMessage_tmr.time = time1_ms; //+= ? o solo = ? verificar esto
      000D66 90 00 AE         [24] 1853 	mov	dptr,#_delay_tx_time1_ms_65536_283
      000D69 E0               [24] 1854 	movx	a,@dptr
      000D6A FC               [12] 1855 	mov	r4,a
      000D6B A3               [24] 1856 	inc	dptr
      000D6C E0               [24] 1857 	movx	a,@dptr
      000D6D FD               [12] 1858 	mov	r5,a
      000D6E A3               [24] 1859 	inc	dptr
      000D6F E0               [24] 1860 	movx	a,@dptr
      000D70 FE               [12] 1861 	mov	r6,a
      000D71 A3               [24] 1862 	inc	dptr
      000D72 E0               [24] 1863 	movx	a,@dptr
      000D73 FF               [12] 1864 	mov	r7,a
      000D74 90 00 71         [24] 1865 	mov	dptr,#(_OriginalMessage_tmr + 0x0004)
      000D77 EC               [12] 1866 	mov	a,r4
      000D78 F0               [24] 1867 	movx	@dptr,a
      000D79 ED               [12] 1868 	mov	a,r5
      000D7A A3               [24] 1869 	inc	dptr
      000D7B F0               [24] 1870 	movx	@dptr,a
      000D7C EE               [12] 1871 	mov	a,r6
      000D7D A3               [24] 1872 	inc	dptr
      000D7E F0               [24] 1873 	movx	@dptr,a
      000D7F EF               [12] 1874 	mov	a,r7
      000D80 A3               [24] 1875 	inc	dptr
      000D81 F0               [24] 1876 	movx	@dptr,a
                                   1877 ;	..\src\atrs\misc.c:279: wtimer0_addrelative(&OriginalMessage_tmr);
      000D82 90 00 6D         [24] 1878 	mov	dptr,#_OriginalMessage_tmr
      000D85 12 78 78         [24] 1879 	lcall	_wtimer0_addrelative
                                   1880 ;	..\src\atrs\misc.c:280: wtimer0_remove(&TwinMessage1_tmr);
      000D88 90 00 75         [24] 1881 	mov	dptr,#_TwinMessage1_tmr
      000D8B 12 7C 24         [24] 1882 	lcall	_wtimer0_remove
                                   1883 ;	..\src\atrs\misc.c:281: TwinMessage1_tmr.handler = TwinMessage1_callback;//Handler for timer 0
      000D8E 90 00 77         [24] 1884 	mov	dptr,#(_TwinMessage1_tmr + 0x0002)
      000D91 74 D9            [12] 1885 	mov	a,#_TwinMessage1_callback
      000D93 F0               [24] 1886 	movx	@dptr,a
      000D94 74 0A            [12] 1887 	mov	a,#(_TwinMessage1_callback >> 8)
      000D96 A3               [24] 1888 	inc	dptr
      000D97 F0               [24] 1889 	movx	@dptr,a
                                   1890 ;	..\src\atrs\misc.c:282: time2_ms = (uint32_t) TIME_FOR_TIMER_MSEC(time2_ms);
      000D98 90 00 A6         [24] 1891 	mov	dptr,#_delay_tx_PARM_2
      000D9B E0               [24] 1892 	movx	a,@dptr
      000D9C FC               [12] 1893 	mov	r4,a
      000D9D A3               [24] 1894 	inc	dptr
      000D9E E0               [24] 1895 	movx	a,@dptr
      000D9F FD               [12] 1896 	mov	r5,a
      000DA0 A3               [24] 1897 	inc	dptr
      000DA1 E0               [24] 1898 	movx	a,@dptr
      000DA2 FE               [12] 1899 	mov	r6,a
      000DA3 A3               [24] 1900 	inc	dptr
      000DA4 E0               [24] 1901 	movx	a,@dptr
      000DA5 8C 82            [24] 1902 	mov	dpl,r4
      000DA7 8D 83            [24] 1903 	mov	dph,r5
      000DA9 8E F0            [24] 1904 	mov	b,r6
      000DAB 12 79 06         [24] 1905 	lcall	___ulong2fs
      000DAE AC 82            [24] 1906 	mov	r4,dpl
      000DB0 AD 83            [24] 1907 	mov	r5,dph
      000DB2 AE F0            [24] 1908 	mov	r6,b
      000DB4 FF               [12] 1909 	mov	r7,a
      000DB5 C0 04            [24] 1910 	push	ar4
      000DB7 C0 05            [24] 1911 	push	ar5
      000DB9 C0 06            [24] 1912 	push	ar6
      000DBB C0 07            [24] 1913 	push	ar7
      000DBD 90 00 00         [24] 1914 	mov	dptr,#0x0000
      000DC0 75 F0 20         [24] 1915 	mov	b,#0x20
      000DC3 74 44            [12] 1916 	mov	a,#0x44
      000DC5 12 6F 8C         [24] 1917 	lcall	___fsmul
      000DC8 AC 82            [24] 1918 	mov	r4,dpl
      000DCA AD 83            [24] 1919 	mov	r5,dph
      000DCC AE F0            [24] 1920 	mov	r6,b
      000DCE FF               [12] 1921 	mov	r7,a
      000DCF E5 81            [12] 1922 	mov	a,sp
      000DD1 24 FC            [12] 1923 	add	a,#0xfc
      000DD3 F5 81            [12] 1924 	mov	sp,a
      000DD5 E4               [12] 1925 	clr	a
      000DD6 C0 E0            [24] 1926 	push	acc
      000DD8 C0 E0            [24] 1927 	push	acc
      000DDA 74 7A            [12] 1928 	mov	a,#0x7a
      000DDC C0 E0            [24] 1929 	push	acc
      000DDE 74 44            [12] 1930 	mov	a,#0x44
      000DE0 C0 E0            [24] 1931 	push	acc
      000DE2 8C 82            [24] 1932 	mov	dpl,r4
      000DE4 8D 83            [24] 1933 	mov	dph,r5
      000DE6 8E F0            [24] 1934 	mov	b,r6
      000DE8 EF               [12] 1935 	mov	a,r7
      000DE9 12 8C 15         [24] 1936 	lcall	___fsdiv
      000DEC AC 82            [24] 1937 	mov	r4,dpl
      000DEE AD 83            [24] 1938 	mov	r5,dph
      000DF0 AE F0            [24] 1939 	mov	r6,b
      000DF2 FF               [12] 1940 	mov	r7,a
      000DF3 E5 81            [12] 1941 	mov	a,sp
      000DF5 24 FC            [12] 1942 	add	a,#0xfc
      000DF7 F5 81            [12] 1943 	mov	sp,a
      000DF9 8C 82            [24] 1944 	mov	dpl,r4
      000DFB 8D 83            [24] 1945 	mov	dph,r5
      000DFD 8E F0            [24] 1946 	mov	b,r6
      000DFF EF               [12] 1947 	mov	a,r7
      000E00 12 79 19         [24] 1948 	lcall	___fs2ulong
      000E03 AC 82            [24] 1949 	mov	r4,dpl
      000E05 AD 83            [24] 1950 	mov	r5,dph
      000E07 AE F0            [24] 1951 	mov	r6,b
      000E09 FF               [12] 1952 	mov	r7,a
      000E0A 90 00 A6         [24] 1953 	mov	dptr,#_delay_tx_PARM_2
      000E0D EC               [12] 1954 	mov	a,r4
      000E0E F0               [24] 1955 	movx	@dptr,a
      000E0F ED               [12] 1956 	mov	a,r5
      000E10 A3               [24] 1957 	inc	dptr
      000E11 F0               [24] 1958 	movx	@dptr,a
      000E12 EE               [12] 1959 	mov	a,r6
      000E13 A3               [24] 1960 	inc	dptr
      000E14 F0               [24] 1961 	movx	@dptr,a
      000E15 EF               [12] 1962 	mov	a,r7
      000E16 A3               [24] 1963 	inc	dptr
      000E17 F0               [24] 1964 	movx	@dptr,a
                                   1965 ;	..\src\atrs\misc.c:283: TwinMessage1_tmr.time = time2_ms; //+= ? o solo = ? verificar esto
      000E18 90 00 A6         [24] 1966 	mov	dptr,#_delay_tx_PARM_2
      000E1B E0               [24] 1967 	movx	a,@dptr
      000E1C FC               [12] 1968 	mov	r4,a
      000E1D A3               [24] 1969 	inc	dptr
      000E1E E0               [24] 1970 	movx	a,@dptr
      000E1F FD               [12] 1971 	mov	r5,a
      000E20 A3               [24] 1972 	inc	dptr
      000E21 E0               [24] 1973 	movx	a,@dptr
      000E22 FE               [12] 1974 	mov	r6,a
      000E23 A3               [24] 1975 	inc	dptr
      000E24 E0               [24] 1976 	movx	a,@dptr
      000E25 FF               [12] 1977 	mov	r7,a
      000E26 90 00 79         [24] 1978 	mov	dptr,#(_TwinMessage1_tmr + 0x0004)
      000E29 EC               [12] 1979 	mov	a,r4
      000E2A F0               [24] 1980 	movx	@dptr,a
      000E2B ED               [12] 1981 	mov	a,r5
      000E2C A3               [24] 1982 	inc	dptr
      000E2D F0               [24] 1983 	movx	@dptr,a
      000E2E EE               [12] 1984 	mov	a,r6
      000E2F A3               [24] 1985 	inc	dptr
      000E30 F0               [24] 1986 	movx	@dptr,a
      000E31 EF               [12] 1987 	mov	a,r7
      000E32 A3               [24] 1988 	inc	dptr
      000E33 F0               [24] 1989 	movx	@dptr,a
                                   1990 ;	..\src\atrs\misc.c:284: wtimer0_addrelative(&TwinMessage1_tmr);
      000E34 90 00 75         [24] 1991 	mov	dptr,#_TwinMessage1_tmr
      000E37 12 78 78         [24] 1992 	lcall	_wtimer0_addrelative
                                   1993 ;	..\src\atrs\misc.c:285: wtimer0_remove(&TwinMessage2_tmr);
      000E3A 90 00 7D         [24] 1994 	mov	dptr,#_TwinMessage2_tmr
      000E3D 12 7C 24         [24] 1995 	lcall	_wtimer0_remove
                                   1996 ;	..\src\atrs\misc.c:286: TwinMessage2_tmr.handler = TwinMessage2_callback;//Handler for timer 0
      000E40 90 00 7F         [24] 1997 	mov	dptr,#(_TwinMessage2_tmr + 0x0002)
      000E43 74 06            [12] 1998 	mov	a,#_TwinMessage2_callback
      000E45 F0               [24] 1999 	movx	@dptr,a
      000E46 74 0B            [12] 2000 	mov	a,#(_TwinMessage2_callback >> 8)
      000E48 A3               [24] 2001 	inc	dptr
      000E49 F0               [24] 2002 	movx	@dptr,a
                                   2003 ;	..\src\atrs\misc.c:287: time3_ms = (uint32_t) TIME_FOR_TIMER_MSEC(time3_ms);
      000E4A 90 00 AA         [24] 2004 	mov	dptr,#_delay_tx_PARM_3
      000E4D E0               [24] 2005 	movx	a,@dptr
      000E4E FC               [12] 2006 	mov	r4,a
      000E4F A3               [24] 2007 	inc	dptr
      000E50 E0               [24] 2008 	movx	a,@dptr
      000E51 FD               [12] 2009 	mov	r5,a
      000E52 A3               [24] 2010 	inc	dptr
      000E53 E0               [24] 2011 	movx	a,@dptr
      000E54 FE               [12] 2012 	mov	r6,a
      000E55 A3               [24] 2013 	inc	dptr
      000E56 E0               [24] 2014 	movx	a,@dptr
      000E57 8C 82            [24] 2015 	mov	dpl,r4
      000E59 8D 83            [24] 2016 	mov	dph,r5
      000E5B 8E F0            [24] 2017 	mov	b,r6
      000E5D 12 79 06         [24] 2018 	lcall	___ulong2fs
      000E60 AC 82            [24] 2019 	mov	r4,dpl
      000E62 AD 83            [24] 2020 	mov	r5,dph
      000E64 AE F0            [24] 2021 	mov	r6,b
      000E66 FF               [12] 2022 	mov	r7,a
      000E67 C0 04            [24] 2023 	push	ar4
      000E69 C0 05            [24] 2024 	push	ar5
      000E6B C0 06            [24] 2025 	push	ar6
      000E6D C0 07            [24] 2026 	push	ar7
      000E6F 90 00 00         [24] 2027 	mov	dptr,#0x0000
      000E72 75 F0 20         [24] 2028 	mov	b,#0x20
      000E75 74 44            [12] 2029 	mov	a,#0x44
      000E77 12 6F 8C         [24] 2030 	lcall	___fsmul
      000E7A AC 82            [24] 2031 	mov	r4,dpl
      000E7C AD 83            [24] 2032 	mov	r5,dph
      000E7E AE F0            [24] 2033 	mov	r6,b
      000E80 FF               [12] 2034 	mov	r7,a
      000E81 E5 81            [12] 2035 	mov	a,sp
      000E83 24 FC            [12] 2036 	add	a,#0xfc
      000E85 F5 81            [12] 2037 	mov	sp,a
      000E87 E4               [12] 2038 	clr	a
      000E88 C0 E0            [24] 2039 	push	acc
      000E8A C0 E0            [24] 2040 	push	acc
      000E8C 74 7A            [12] 2041 	mov	a,#0x7a
      000E8E C0 E0            [24] 2042 	push	acc
      000E90 74 44            [12] 2043 	mov	a,#0x44
      000E92 C0 E0            [24] 2044 	push	acc
      000E94 8C 82            [24] 2045 	mov	dpl,r4
      000E96 8D 83            [24] 2046 	mov	dph,r5
      000E98 8E F0            [24] 2047 	mov	b,r6
      000E9A EF               [12] 2048 	mov	a,r7
      000E9B 12 8C 15         [24] 2049 	lcall	___fsdiv
      000E9E AC 82            [24] 2050 	mov	r4,dpl
      000EA0 AD 83            [24] 2051 	mov	r5,dph
      000EA2 AE F0            [24] 2052 	mov	r6,b
      000EA4 FF               [12] 2053 	mov	r7,a
      000EA5 E5 81            [12] 2054 	mov	a,sp
      000EA7 24 FC            [12] 2055 	add	a,#0xfc
      000EA9 F5 81            [12] 2056 	mov	sp,a
      000EAB 8C 82            [24] 2057 	mov	dpl,r4
      000EAD 8D 83            [24] 2058 	mov	dph,r5
      000EAF 8E F0            [24] 2059 	mov	b,r6
      000EB1 EF               [12] 2060 	mov	a,r7
      000EB2 12 79 19         [24] 2061 	lcall	___fs2ulong
      000EB5 AC 82            [24] 2062 	mov	r4,dpl
      000EB7 AD 83            [24] 2063 	mov	r5,dph
      000EB9 AE F0            [24] 2064 	mov	r6,b
      000EBB FF               [12] 2065 	mov	r7,a
      000EBC 90 00 AA         [24] 2066 	mov	dptr,#_delay_tx_PARM_3
      000EBF EC               [12] 2067 	mov	a,r4
      000EC0 F0               [24] 2068 	movx	@dptr,a
      000EC1 ED               [12] 2069 	mov	a,r5
      000EC2 A3               [24] 2070 	inc	dptr
      000EC3 F0               [24] 2071 	movx	@dptr,a
      000EC4 EE               [12] 2072 	mov	a,r6
      000EC5 A3               [24] 2073 	inc	dptr
      000EC6 F0               [24] 2074 	movx	@dptr,a
      000EC7 EF               [12] 2075 	mov	a,r7
      000EC8 A3               [24] 2076 	inc	dptr
      000EC9 F0               [24] 2077 	movx	@dptr,a
                                   2078 ;	..\src\atrs\misc.c:288: TwinMessage2_tmr.time = time3_ms; //+= ? o solo = ? verificar esto
      000ECA 90 00 AA         [24] 2079 	mov	dptr,#_delay_tx_PARM_3
      000ECD E0               [24] 2080 	movx	a,@dptr
      000ECE FC               [12] 2081 	mov	r4,a
      000ECF A3               [24] 2082 	inc	dptr
      000ED0 E0               [24] 2083 	movx	a,@dptr
      000ED1 FD               [12] 2084 	mov	r5,a
      000ED2 A3               [24] 2085 	inc	dptr
      000ED3 E0               [24] 2086 	movx	a,@dptr
      000ED4 FE               [12] 2087 	mov	r6,a
      000ED5 A3               [24] 2088 	inc	dptr
      000ED6 E0               [24] 2089 	movx	a,@dptr
      000ED7 FF               [12] 2090 	mov	r7,a
      000ED8 90 00 81         [24] 2091 	mov	dptr,#(_TwinMessage2_tmr + 0x0004)
      000EDB EC               [12] 2092 	mov	a,r4
      000EDC F0               [24] 2093 	movx	@dptr,a
      000EDD ED               [12] 2094 	mov	a,r5
      000EDE A3               [24] 2095 	inc	dptr
      000EDF F0               [24] 2096 	movx	@dptr,a
      000EE0 EE               [12] 2097 	mov	a,r6
      000EE1 A3               [24] 2098 	inc	dptr
      000EE2 F0               [24] 2099 	movx	@dptr,a
      000EE3 EF               [12] 2100 	mov	a,r7
      000EE4 A3               [24] 2101 	inc	dptr
      000EE5 F0               [24] 2102 	movx	@dptr,a
                                   2103 ;	..\src\atrs\misc.c:289: wtimer0_addrelative(&TwinMessage2_tmr);
      000EE6 90 00 7D         [24] 2104 	mov	dptr,#_TwinMessage2_tmr
                                   2105 ;	..\src\atrs\misc.c:290: }
      000EE9 02 78 78         [24] 2106 	ljmp	_wtimer0_addrelative
                                   2107 ;------------------------------------------------------------
                                   2108 ;Allocation info for local variables in function 'InitGPSPolling'
                                   2109 ;------------------------------------------------------------
                                   2110 ;GPS_Period_S              Allocated with name '_InitGPSPolling_GPS_Period_S_65536_285'
                                   2111 ;------------------------------------------------------------
                                   2112 ;	..\src\atrs\misc.c:300: void InitGPSPolling (uint32_t GPS_Period_S)
                                   2113 ;	-----------------------------------------
                                   2114 ;	 function InitGPSPolling
                                   2115 ;	-----------------------------------------
      000EEC                       2116 _InitGPSPolling:
      000EEC AF 82            [24] 2117 	mov	r7,dpl
      000EEE AE 83            [24] 2118 	mov	r6,dph
      000EF0 AD F0            [24] 2119 	mov	r5,b
      000EF2 FC               [12] 2120 	mov	r4,a
      000EF3 90 00 B2         [24] 2121 	mov	dptr,#_InitGPSPolling_GPS_Period_S_65536_285
      000EF6 EF               [12] 2122 	mov	a,r7
      000EF7 F0               [24] 2123 	movx	@dptr,a
      000EF8 EE               [12] 2124 	mov	a,r6
      000EF9 A3               [24] 2125 	inc	dptr
      000EFA F0               [24] 2126 	movx	@dptr,a
      000EFB ED               [12] 2127 	mov	a,r5
      000EFC A3               [24] 2128 	inc	dptr
      000EFD F0               [24] 2129 	movx	@dptr,a
      000EFE EC               [12] 2130 	mov	a,r4
      000EFF A3               [24] 2131 	inc	dptr
      000F00 F0               [24] 2132 	movx	@dptr,a
                                   2133 ;	..\src\atrs\misc.c:302: GPS_tmr.handler = GPS_callback;//Handler for timer 0
      000F01 90 00 87         [24] 2134 	mov	dptr,#(_GPS_tmr + 0x0002)
      000F04 74 33            [12] 2135 	mov	a,#_GPS_callback
      000F06 F0               [24] 2136 	movx	@dptr,a
      000F07 74 0B            [12] 2137 	mov	a,#(_GPS_callback >> 8)
      000F09 A3               [24] 2138 	inc	dptr
      000F0A F0               [24] 2139 	movx	@dptr,a
                                   2140 ;	..\src\atrs\misc.c:303: GPS_Period_S = (uint32_t) TIME_FOR_TIMER_SEC(GPS_Period_S);
      000F0B 90 00 B2         [24] 2141 	mov	dptr,#_InitGPSPolling_GPS_Period_S_65536_285
      000F0E E0               [24] 2142 	movx	a,@dptr
      000F0F FC               [12] 2143 	mov	r4,a
      000F10 A3               [24] 2144 	inc	dptr
      000F11 E0               [24] 2145 	movx	a,@dptr
      000F12 FD               [12] 2146 	mov	r5,a
      000F13 A3               [24] 2147 	inc	dptr
      000F14 E0               [24] 2148 	movx	a,@dptr
      000F15 FE               [12] 2149 	mov	r6,a
      000F16 A3               [24] 2150 	inc	dptr
      000F17 E0               [24] 2151 	movx	a,@dptr
      000F18 FF               [12] 2152 	mov	r7,a
      000F19 90 05 04         [24] 2153 	mov	dptr,#__mullong_PARM_2
      000F1C EC               [12] 2154 	mov	a,r4
      000F1D F0               [24] 2155 	movx	@dptr,a
      000F1E ED               [12] 2156 	mov	a,r5
      000F1F A3               [24] 2157 	inc	dptr
      000F20 F0               [24] 2158 	movx	@dptr,a
      000F21 EE               [12] 2159 	mov	a,r6
      000F22 A3               [24] 2160 	inc	dptr
      000F23 F0               [24] 2161 	movx	@dptr,a
      000F24 EF               [12] 2162 	mov	a,r7
      000F25 A3               [24] 2163 	inc	dptr
      000F26 F0               [24] 2164 	movx	@dptr,a
      000F27 90 02 80         [24] 2165 	mov	dptr,#0x0280
      000F2A E4               [12] 2166 	clr	a
      000F2B F5 F0            [12] 2167 	mov	b,a
      000F2D 12 81 8F         [24] 2168 	lcall	__mullong
      000F30 AC 82            [24] 2169 	mov	r4,dpl
      000F32 AD 83            [24] 2170 	mov	r5,dph
      000F34 AE F0            [24] 2171 	mov	r6,b
      000F36 FF               [12] 2172 	mov	r7,a
      000F37 90 00 B2         [24] 2173 	mov	dptr,#_InitGPSPolling_GPS_Period_S_65536_285
      000F3A EC               [12] 2174 	mov	a,r4
      000F3B F0               [24] 2175 	movx	@dptr,a
      000F3C ED               [12] 2176 	mov	a,r5
      000F3D A3               [24] 2177 	inc	dptr
      000F3E F0               [24] 2178 	movx	@dptr,a
      000F3F EE               [12] 2179 	mov	a,r6
      000F40 A3               [24] 2180 	inc	dptr
      000F41 F0               [24] 2181 	movx	@dptr,a
      000F42 EF               [12] 2182 	mov	a,r7
      000F43 A3               [24] 2183 	inc	dptr
      000F44 F0               [24] 2184 	movx	@dptr,a
                                   2185 ;	..\src\atrs\misc.c:304: GPS_tmr.time = GPS_Period_S; //+= ? o solo = ? verificar esto
      000F45 90 00 B2         [24] 2186 	mov	dptr,#_InitGPSPolling_GPS_Period_S_65536_285
      000F48 E0               [24] 2187 	movx	a,@dptr
      000F49 FC               [12] 2188 	mov	r4,a
      000F4A A3               [24] 2189 	inc	dptr
      000F4B E0               [24] 2190 	movx	a,@dptr
      000F4C FD               [12] 2191 	mov	r5,a
      000F4D A3               [24] 2192 	inc	dptr
      000F4E E0               [24] 2193 	movx	a,@dptr
      000F4F FE               [12] 2194 	mov	r6,a
      000F50 A3               [24] 2195 	inc	dptr
      000F51 E0               [24] 2196 	movx	a,@dptr
      000F52 FF               [12] 2197 	mov	r7,a
      000F53 90 00 89         [24] 2198 	mov	dptr,#(_GPS_tmr + 0x0004)
      000F56 EC               [12] 2199 	mov	a,r4
      000F57 F0               [24] 2200 	movx	@dptr,a
      000F58 ED               [12] 2201 	mov	a,r5
      000F59 A3               [24] 2202 	inc	dptr
      000F5A F0               [24] 2203 	movx	@dptr,a
      000F5B EE               [12] 2204 	mov	a,r6
      000F5C A3               [24] 2205 	inc	dptr
      000F5D F0               [24] 2206 	movx	@dptr,a
      000F5E EF               [12] 2207 	mov	a,r7
      000F5F A3               [24] 2208 	inc	dptr
      000F60 F0               [24] 2209 	movx	@dptr,a
                                   2210 ;	..\src\atrs\misc.c:305: wtimer0_remove(&GPS_tmr);
      000F61 90 00 85         [24] 2211 	mov	dptr,#_GPS_tmr
      000F64 12 7C 24         [24] 2212 	lcall	_wtimer0_remove
                                   2213 ;	..\src\atrs\misc.c:306: wtimer0_addrelative(&GPS_tmr);
      000F67 90 00 85         [24] 2214 	mov	dptr,#_GPS_tmr
                                   2215 ;	..\src\atrs\misc.c:309: }
      000F6A 02 78 78         [24] 2216 	ljmp	_wtimer0_addrelative
                                   2217 ;------------------------------------------------------------
                                   2218 ;Allocation info for local variables in function 'InitChannelChange'
                                   2219 ;------------------------------------------------------------
                                   2220 ;	..\src\atrs\misc.c:315: void InitChannelChange(void)
                                   2221 ;	-----------------------------------------
                                   2222 ;	 function InitChannelChange
                                   2223 ;	-----------------------------------------
      000F6D                       2224 _InitChannelChange:
                                   2225 ;	..\src\atrs\misc.c:317: Channel_change.handler = Channel_change_callback;
      000F6D 90 00 9F         [24] 2226 	mov	dptr,#(_Channel_change + 0x0002)
      000F70 74 74            [12] 2227 	mov	a,#_Channel_change_callback
      000F72 F0               [24] 2228 	movx	@dptr,a
      000F73 74 0B            [12] 2229 	mov	a,#(_Channel_change_callback >> 8)
      000F75 A3               [24] 2230 	inc	dptr
      000F76 F0               [24] 2231 	movx	@dptr,a
                                   2232 ;	..\src\atrs\misc.c:318: Channel_change.time = (uint32_t) TIME_FOR_TIMER_MSEC(250);
      000F77 90 00 A1         [24] 2233 	mov	dptr,#(_Channel_change + 0x0004)
      000F7A 74 A0            [12] 2234 	mov	a,#0xa0
      000F7C F0               [24] 2235 	movx	@dptr,a
      000F7D E4               [12] 2236 	clr	a
      000F7E A3               [24] 2237 	inc	dptr
      000F7F F0               [24] 2238 	movx	@dptr,a
      000F80 A3               [24] 2239 	inc	dptr
      000F81 F0               [24] 2240 	movx	@dptr,a
      000F82 A3               [24] 2241 	inc	dptr
      000F83 F0               [24] 2242 	movx	@dptr,a
                                   2243 ;	..\src\atrs\misc.c:319: wtimer0_remove(&Channel_change);
      000F84 90 00 9D         [24] 2244 	mov	dptr,#_Channel_change
      000F87 12 7C 24         [24] 2245 	lcall	_wtimer0_remove
                                   2246 ;	..\src\atrs\misc.c:320: wtimer0_addrelative(&Channel_change);
      000F8A 90 00 9D         [24] 2247 	mov	dptr,#_Channel_change
                                   2248 ;	..\src\atrs\misc.c:321: }
      000F8D 02 78 78         [24] 2249 	ljmp	_wtimer0_addrelative
                                   2250 ;------------------------------------------------------------
                                   2251 ;Allocation info for local variables in function 'StartBackoffTimer'
                                   2252 ;------------------------------------------------------------
                                   2253 ;	..\src\atrs\misc.c:328: void StartBackoffTimer(void)
                                   2254 ;	-----------------------------------------
                                   2255 ;	 function StartBackoffTimer
                                   2256 ;	-----------------------------------------
      000F90                       2257 _StartBackoffTimer:
                                   2258 ;	..\src\atrs\misc.c:330: BackOff.handler = BackOff_Callback;
      000F90 90 00 8F         [24] 2259 	mov	dptr,#(_BackOff + 0x0002)
      000F93 74 9F            [12] 2260 	mov	a,#_BackOff_Callback
      000F95 F0               [24] 2261 	movx	@dptr,a
      000F96 74 0C            [12] 2262 	mov	a,#(_BackOff_Callback >> 8)
      000F98 A3               [24] 2263 	inc	dptr
      000F99 F0               [24] 2264 	movx	@dptr,a
                                   2265 ;	..\src\atrs\misc.c:331: BackOff.time = (uint32_t) TIME_FOR_TIMER_SEC(30+(RNGBYTE%60));
      000F9A 90 70 81         [24] 2266 	mov	dptr,#_RNGBYTE
      000F9D E0               [24] 2267 	movx	a,@dptr
      000F9E FF               [12] 2268 	mov	r7,a
      000F9F 7E 00            [12] 2269 	mov	r6,#0x00
      000FA1 90 05 08         [24] 2270 	mov	dptr,#__modsint_PARM_2
      000FA4 74 3C            [12] 2271 	mov	a,#0x3c
      000FA6 F0               [24] 2272 	movx	@dptr,a
      000FA7 E4               [12] 2273 	clr	a
      000FA8 A3               [24] 2274 	inc	dptr
      000FA9 F0               [24] 2275 	movx	@dptr,a
      000FAA 8F 82            [24] 2276 	mov	dpl,r7
      000FAC 8E 83            [24] 2277 	mov	dph,r6
      000FAE 12 81 FB         [24] 2278 	lcall	__modsint
      000FB1 AE 82            [24] 2279 	mov	r6,dpl
      000FB3 AF 83            [24] 2280 	mov	r7,dph
      000FB5 90 05 02         [24] 2281 	mov	dptr,#__mulint_PARM_2
      000FB8 EE               [12] 2282 	mov	a,r6
      000FB9 F0               [24] 2283 	movx	@dptr,a
      000FBA EF               [12] 2284 	mov	a,r7
      000FBB A3               [24] 2285 	inc	dptr
      000FBC F0               [24] 2286 	movx	@dptr,a
      000FBD 90 02 80         [24] 2287 	mov	dptr,#0x0280
      000FC0 12 7F C9         [24] 2288 	lcall	__mulint
      000FC3 AE 82            [24] 2289 	mov	r6,dpl
      000FC5 E5 83            [12] 2290 	mov	a,dph
      000FC7 FF               [12] 2291 	mov	r7,a
      000FC8 33               [12] 2292 	rlc	a
      000FC9 95 E0            [12] 2293 	subb	a,acc
      000FCB FD               [12] 2294 	mov	r5,a
      000FCC FC               [12] 2295 	mov	r4,a
      000FCD 74 1E            [12] 2296 	mov	a,#0x1e
      000FCF 2E               [12] 2297 	add	a,r6
      000FD0 FE               [12] 2298 	mov	r6,a
      000FD1 E4               [12] 2299 	clr	a
      000FD2 3F               [12] 2300 	addc	a,r7
      000FD3 FF               [12] 2301 	mov	r7,a
      000FD4 E4               [12] 2302 	clr	a
      000FD5 3D               [12] 2303 	addc	a,r5
      000FD6 FD               [12] 2304 	mov	r5,a
      000FD7 E4               [12] 2305 	clr	a
      000FD8 3C               [12] 2306 	addc	a,r4
      000FD9 FC               [12] 2307 	mov	r4,a
      000FDA 90 00 91         [24] 2308 	mov	dptr,#(_BackOff + 0x0004)
      000FDD EE               [12] 2309 	mov	a,r6
      000FDE F0               [24] 2310 	movx	@dptr,a
      000FDF EF               [12] 2311 	mov	a,r7
      000FE0 A3               [24] 2312 	inc	dptr
      000FE1 F0               [24] 2313 	movx	@dptr,a
      000FE2 ED               [12] 2314 	mov	a,r5
      000FE3 A3               [24] 2315 	inc	dptr
      000FE4 F0               [24] 2316 	movx	@dptr,a
      000FE5 EC               [12] 2317 	mov	a,r4
      000FE6 A3               [24] 2318 	inc	dptr
      000FE7 F0               [24] 2319 	movx	@dptr,a
                                   2320 ;	..\src\atrs\misc.c:333: dbglink_writestr(" \n Backoff timer started \n");
      000FE8 90 90 91         [24] 2321 	mov	dptr,#___str_5
      000FEB 75 F0 80         [24] 2322 	mov	b,#0x80
      000FEE 12 82 8A         [24] 2323 	lcall	_dbglink_writestr
                                   2324 ;	..\src\atrs\misc.c:335: wtimer0_remove(&BackOff);
      000FF1 90 00 8D         [24] 2325 	mov	dptr,#_BackOff
      000FF4 12 7C 24         [24] 2326 	lcall	_wtimer0_remove
                                   2327 ;	..\src\atrs\misc.c:336: wtimer0_addrelative(&BackOff);
      000FF7 90 00 8D         [24] 2328 	mov	dptr,#_BackOff
                                   2329 ;	..\src\atrs\misc.c:337: }
      000FFA 02 78 78         [24] 2330 	ljmp	_wtimer0_addrelative
                                   2331 ;------------------------------------------------------------
                                   2332 ;Allocation info for local variables in function 'StartACKTimer'
                                   2333 ;------------------------------------------------------------
                                   2334 ;	..\src\atrs\misc.c:344: void StartACKTimer(void)
                                   2335 ;	-----------------------------------------
                                   2336 ;	 function StartACKTimer
                                   2337 ;	-----------------------------------------
      000FFD                       2338 _StartACKTimer:
                                   2339 ;	..\src\atrs\misc.c:346: ACKTimer.handler = ACK_Callback;
      000FFD 90 00 97         [24] 2340 	mov	dptr,#(_ACKTimer + 0x0002)
      001000 74 AE            [12] 2341 	mov	a,#_ACK_Callback
      001002 F0               [24] 2342 	movx	@dptr,a
      001003 74 0C            [12] 2343 	mov	a,#(_ACK_Callback >> 8)
      001005 A3               [24] 2344 	inc	dptr
      001006 F0               [24] 2345 	movx	@dptr,a
                                   2346 ;	..\src\atrs\misc.c:347: ACKTimer.time = (uint32_t) TIME_FOR_TIMER_SEC(/*(RNGBYTE%15)+*/5);
      001007 90 00 99         [24] 2347 	mov	dptr,#(_ACKTimer + 0x0004)
      00100A 74 80            [12] 2348 	mov	a,#0x80
      00100C F0               [24] 2349 	movx	@dptr,a
      00100D 74 0C            [12] 2350 	mov	a,#0x0c
      00100F A3               [24] 2351 	inc	dptr
      001010 F0               [24] 2352 	movx	@dptr,a
      001011 E4               [12] 2353 	clr	a
      001012 A3               [24] 2354 	inc	dptr
      001013 F0               [24] 2355 	movx	@dptr,a
      001014 A3               [24] 2356 	inc	dptr
      001015 F0               [24] 2357 	movx	@dptr,a
                                   2358 ;	..\src\atrs\misc.c:348: wtimer0_remove(&ACKTimer);
      001016 90 00 95         [24] 2359 	mov	dptr,#_ACKTimer
      001019 12 7C 24         [24] 2360 	lcall	_wtimer0_remove
                                   2361 ;	..\src\atrs\misc.c:349: wtimer0_addrelative(&ACKTimer);
      00101C 90 00 95         [24] 2362 	mov	dptr,#_ACKTimer
      00101F 12 78 78         [24] 2363 	lcall	_wtimer0_addrelative
                                   2364 ;	..\src\atrs\misc.c:351: dbglink_writestr(" ACK timer started");
      001022 90 90 AC         [24] 2365 	mov	dptr,#___str_6
      001025 75 F0 80         [24] 2366 	mov	b,#0x80
                                   2367 ;	..\src\atrs\misc.c:353: }
      001028 02 82 8A         [24] 2368 	ljmp	_dbglink_writestr
                                   2369 ;------------------------------------------------------------
                                   2370 ;Allocation info for local variables in function 'swap_variables'
                                   2371 ;------------------------------------------------------------
                                   2372 ;b                         Allocated with name '_swap_variables_PARM_2'
                                   2373 ;a                         Allocated with name '_swap_variables_a_65536_293'
                                   2374 ;------------------------------------------------------------
                                   2375 ;	..\src\atrs\misc.c:364: void swap_variables(uint8_t *a,uint8_t *b)
                                   2376 ;	-----------------------------------------
                                   2377 ;	 function swap_variables
                                   2378 ;	-----------------------------------------
      00102B                       2379 _swap_variables:
      00102B AF F0            [24] 2380 	mov	r7,b
      00102D AE 83            [24] 2381 	mov	r6,dph
      00102F E5 82            [12] 2382 	mov	a,dpl
      001031 90 00 B9         [24] 2383 	mov	dptr,#_swap_variables_a_65536_293
      001034 F0               [24] 2384 	movx	@dptr,a
      001035 EE               [12] 2385 	mov	a,r6
      001036 A3               [24] 2386 	inc	dptr
      001037 F0               [24] 2387 	movx	@dptr,a
      001038 EF               [12] 2388 	mov	a,r7
      001039 A3               [24] 2389 	inc	dptr
      00103A F0               [24] 2390 	movx	@dptr,a
                                   2391 ;	..\src\atrs\misc.c:366: *a = *a+*b;
      00103B 90 00 B9         [24] 2392 	mov	dptr,#_swap_variables_a_65536_293
      00103E E0               [24] 2393 	movx	a,@dptr
      00103F FD               [12] 2394 	mov	r5,a
      001040 A3               [24] 2395 	inc	dptr
      001041 E0               [24] 2396 	movx	a,@dptr
      001042 FE               [12] 2397 	mov	r6,a
      001043 A3               [24] 2398 	inc	dptr
      001044 E0               [24] 2399 	movx	a,@dptr
      001045 FF               [12] 2400 	mov	r7,a
      001046 8D 82            [24] 2401 	mov	dpl,r5
      001048 8E 83            [24] 2402 	mov	dph,r6
      00104A 8F F0            [24] 2403 	mov	b,r7
      00104C 12 8D 8E         [24] 2404 	lcall	__gptrget
      00104F FC               [12] 2405 	mov	r4,a
      001050 90 00 B6         [24] 2406 	mov	dptr,#_swap_variables_PARM_2
      001053 E0               [24] 2407 	movx	a,@dptr
      001054 F9               [12] 2408 	mov	r1,a
      001055 A3               [24] 2409 	inc	dptr
      001056 E0               [24] 2410 	movx	a,@dptr
      001057 FA               [12] 2411 	mov	r2,a
      001058 A3               [24] 2412 	inc	dptr
      001059 E0               [24] 2413 	movx	a,@dptr
      00105A FB               [12] 2414 	mov	r3,a
      00105B 89 82            [24] 2415 	mov	dpl,r1
      00105D 8A 83            [24] 2416 	mov	dph,r2
      00105F 8B F0            [24] 2417 	mov	b,r3
      001061 12 8D 8E         [24] 2418 	lcall	__gptrget
      001064 2C               [12] 2419 	add	a,r4
      001065 FC               [12] 2420 	mov	r4,a
      001066 8D 82            [24] 2421 	mov	dpl,r5
      001068 8E 83            [24] 2422 	mov	dph,r6
      00106A 8F F0            [24] 2423 	mov	b,r7
      00106C 12 7C 62         [24] 2424 	lcall	__gptrput
                                   2425 ;	..\src\atrs\misc.c:367: *b= *a-*b;
      00106F 89 82            [24] 2426 	mov	dpl,r1
      001071 8A 83            [24] 2427 	mov	dph,r2
      001073 8B F0            [24] 2428 	mov	b,r3
      001075 12 8D 8E         [24] 2429 	lcall	__gptrget
      001078 F8               [12] 2430 	mov	r0,a
      001079 EC               [12] 2431 	mov	a,r4
      00107A C3               [12] 2432 	clr	c
      00107B 98               [12] 2433 	subb	a,r0
      00107C F8               [12] 2434 	mov	r0,a
      00107D 89 82            [24] 2435 	mov	dpl,r1
      00107F 8A 83            [24] 2436 	mov	dph,r2
      001081 8B F0            [24] 2437 	mov	b,r3
      001083 12 7C 62         [24] 2438 	lcall	__gptrput
                                   2439 ;	..\src\atrs\misc.c:368: *a = *a-*b;
      001086 EC               [12] 2440 	mov	a,r4
      001087 C3               [12] 2441 	clr	c
      001088 98               [12] 2442 	subb	a,r0
      001089 8D 82            [24] 2443 	mov	dpl,r5
      00108B 8E 83            [24] 2444 	mov	dph,r6
      00108D 8F F0            [24] 2445 	mov	b,r7
                                   2446 ;	..\src\atrs\misc.c:369: return;
                                   2447 ;	..\src\atrs\misc.c:370: }
      00108F 02 7C 62         [24] 2448 	ljmp	__gptrput
                                   2449 ;------------------------------------------------------------
                                   2450 ;Allocation info for local variables in function 'GetRandomSlots'
                                   2451 ;------------------------------------------------------------
                                   2452 ;sloc0                     Allocated with name '_GetRandomSlots_sloc0_1_0'
                                   2453 ;sloc1                     Allocated with name '_GetRandomSlots_sloc1_1_0'
                                   2454 ;sloc2                     Allocated with name '_GetRandomSlots_sloc2_1_0'
                                   2455 ;slot2                     Allocated with name '_GetRandomSlots_PARM_2'
                                   2456 ;slot3                     Allocated with name '_GetRandomSlots_PARM_3'
                                   2457 ;slot1                     Allocated with name '_GetRandomSlots_slot1_65536_295'
                                   2458 ;------------------------------------------------------------
                                   2459 ;	..\src\atrs\misc.c:380: void GetRandomSlots(uint8_t* slot1, uint8_t* slot2, uint8_t* slot3)
                                   2460 ;	-----------------------------------------
                                   2461 ;	 function GetRandomSlots
                                   2462 ;	-----------------------------------------
      001092                       2463 _GetRandomSlots:
      001092 AF F0            [24] 2464 	mov	r7,b
      001094 AE 83            [24] 2465 	mov	r6,dph
      001096 E5 82            [12] 2466 	mov	a,dpl
      001098 90 00 C2         [24] 2467 	mov	dptr,#_GetRandomSlots_slot1_65536_295
      00109B F0               [24] 2468 	movx	@dptr,a
      00109C EE               [12] 2469 	mov	a,r6
      00109D A3               [24] 2470 	inc	dptr
      00109E F0               [24] 2471 	movx	@dptr,a
      00109F EF               [12] 2472 	mov	a,r7
      0010A0 A3               [24] 2473 	inc	dptr
      0010A1 F0               [24] 2474 	movx	@dptr,a
                                   2475 ;	..\src\atrs\misc.c:382: *slot1 = RND_NUMBER_TO_SLOTS(RNGBYTE);
      0010A2 90 00 C2         [24] 2476 	mov	dptr,#_GetRandomSlots_slot1_65536_295
      0010A5 E0               [24] 2477 	movx	a,@dptr
      0010A6 FD               [12] 2478 	mov	r5,a
      0010A7 A3               [24] 2479 	inc	dptr
      0010A8 E0               [24] 2480 	movx	a,@dptr
      0010A9 FE               [12] 2481 	mov	r6,a
      0010AA A3               [24] 2482 	inc	dptr
      0010AB E0               [24] 2483 	movx	a,@dptr
      0010AC FF               [12] 2484 	mov	r7,a
      0010AD 90 70 81         [24] 2485 	mov	dptr,#_RNGBYTE
      0010B0 E0               [24] 2486 	movx	a,@dptr
      0010B1 FC               [12] 2487 	mov	r4,a
      0010B2 7B 00            [12] 2488 	mov	r3,#0x00
      0010B4 90 05 08         [24] 2489 	mov	dptr,#__modsint_PARM_2
      0010B7 74 32            [12] 2490 	mov	a,#0x32
      0010B9 F0               [24] 2491 	movx	@dptr,a
      0010BA E4               [12] 2492 	clr	a
      0010BB A3               [24] 2493 	inc	dptr
      0010BC F0               [24] 2494 	movx	@dptr,a
      0010BD 8C 82            [24] 2495 	mov	dpl,r4
      0010BF 8B 83            [24] 2496 	mov	dph,r3
      0010C1 C0 07            [24] 2497 	push	ar7
      0010C3 C0 06            [24] 2498 	push	ar6
      0010C5 C0 05            [24] 2499 	push	ar5
      0010C7 12 81 FB         [24] 2500 	lcall	__modsint
      0010CA AB 82            [24] 2501 	mov	r3,dpl
      0010CC D0 05            [24] 2502 	pop	ar5
      0010CE D0 06            [24] 2503 	pop	ar6
      0010D0 D0 07            [24] 2504 	pop	ar7
      0010D2 8B 0C            [24] 2505 	mov	_GetRandomSlots_sloc1_1_0,r3
      0010D4 8D 82            [24] 2506 	mov	dpl,r5
      0010D6 8E 83            [24] 2507 	mov	dph,r6
      0010D8 8F F0            [24] 2508 	mov	b,r7
      0010DA E5 0C            [12] 2509 	mov	a,_GetRandomSlots_sloc1_1_0
      0010DC 12 7C 62         [24] 2510 	lcall	__gptrput
                                   2511 ;	..\src\atrs\misc.c:383: *slot2 = RND_NUMBER_TO_SLOTS(RNGBYTE);
      0010DF 90 00 BC         [24] 2512 	mov	dptr,#_GetRandomSlots_PARM_2
      0010E2 E0               [24] 2513 	movx	a,@dptr
      0010E3 FD               [12] 2514 	mov	r5,a
      0010E4 A3               [24] 2515 	inc	dptr
      0010E5 E0               [24] 2516 	movx	a,@dptr
      0010E6 FE               [12] 2517 	mov	r6,a
      0010E7 A3               [24] 2518 	inc	dptr
      0010E8 E0               [24] 2519 	movx	a,@dptr
      0010E9 FF               [12] 2520 	mov	r7,a
      0010EA 90 70 81         [24] 2521 	mov	dptr,#_RNGBYTE
      0010ED E0               [24] 2522 	movx	a,@dptr
      0010EE FC               [12] 2523 	mov	r4,a
      0010EF 7A 00            [12] 2524 	mov	r2,#0x00
      0010F1 90 05 08         [24] 2525 	mov	dptr,#__modsint_PARM_2
      0010F4 74 32            [12] 2526 	mov	a,#0x32
      0010F6 F0               [24] 2527 	movx	@dptr,a
      0010F7 E4               [12] 2528 	clr	a
      0010F8 A3               [24] 2529 	inc	dptr
      0010F9 F0               [24] 2530 	movx	@dptr,a
      0010FA 8C 82            [24] 2531 	mov	dpl,r4
      0010FC 8A 83            [24] 2532 	mov	dph,r2
      0010FE C0 07            [24] 2533 	push	ar7
      001100 C0 06            [24] 2534 	push	ar6
      001102 C0 05            [24] 2535 	push	ar5
      001104 12 81 FB         [24] 2536 	lcall	__modsint
      001107 AA 82            [24] 2537 	mov	r2,dpl
      001109 D0 05            [24] 2538 	pop	ar5
      00110B D0 06            [24] 2539 	pop	ar6
      00110D D0 07            [24] 2540 	pop	ar7
      00110F 8A 0B            [24] 2541 	mov	_GetRandomSlots_sloc0_1_0,r2
      001111 8D 82            [24] 2542 	mov	dpl,r5
      001113 8E 83            [24] 2543 	mov	dph,r6
      001115 8F F0            [24] 2544 	mov	b,r7
      001117 E5 0B            [12] 2545 	mov	a,_GetRandomSlots_sloc0_1_0
      001119 12 7C 62         [24] 2546 	lcall	__gptrput
                                   2547 ;	..\src\atrs\misc.c:384: *slot3 = RND_NUMBER_TO_SLOTS(RNGBYTE);
      00111C 90 00 BF         [24] 2548 	mov	dptr,#_GetRandomSlots_PARM_3
      00111F E0               [24] 2549 	movx	a,@dptr
      001120 F8               [12] 2550 	mov	r0,a
      001121 A3               [24] 2551 	inc	dptr
      001122 E0               [24] 2552 	movx	a,@dptr
      001123 F9               [12] 2553 	mov	r1,a
      001124 A3               [24] 2554 	inc	dptr
      001125 E0               [24] 2555 	movx	a,@dptr
      001126 FC               [12] 2556 	mov	r4,a
      001127 90 70 81         [24] 2557 	mov	dptr,#_RNGBYTE
      00112A E0               [24] 2558 	movx	a,@dptr
      00112B FA               [12] 2559 	mov	r2,a
      00112C 7B 00            [12] 2560 	mov	r3,#0x00
      00112E 90 05 08         [24] 2561 	mov	dptr,#__modsint_PARM_2
      001131 74 32            [12] 2562 	mov	a,#0x32
      001133 F0               [24] 2563 	movx	@dptr,a
      001134 E4               [12] 2564 	clr	a
      001135 A3               [24] 2565 	inc	dptr
      001136 F0               [24] 2566 	movx	@dptr,a
      001137 8A 82            [24] 2567 	mov	dpl,r2
      001139 8B 83            [24] 2568 	mov	dph,r3
      00113B C0 07            [24] 2569 	push	ar7
      00113D C0 06            [24] 2570 	push	ar6
      00113F C0 05            [24] 2571 	push	ar5
      001141 C0 04            [24] 2572 	push	ar4
      001143 C0 01            [24] 2573 	push	ar1
      001145 C0 00            [24] 2574 	push	ar0
      001147 12 81 FB         [24] 2575 	lcall	__modsint
      00114A AA 82            [24] 2576 	mov	r2,dpl
      00114C D0 00            [24] 2577 	pop	ar0
      00114E D0 01            [24] 2578 	pop	ar1
      001150 D0 04            [24] 2579 	pop	ar4
      001152 D0 05            [24] 2580 	pop	ar5
      001154 D0 06            [24] 2581 	pop	ar6
      001156 D0 07            [24] 2582 	pop	ar7
      001158 88 82            [24] 2583 	mov	dpl,r0
      00115A 89 83            [24] 2584 	mov	dph,r1
      00115C 8C F0            [24] 2585 	mov	b,r4
      00115E EA               [12] 2586 	mov	a,r2
      00115F 12 7C 62         [24] 2587 	lcall	__gptrput
                                   2588 ;	..\src\atrs\misc.c:385: if(*slot1 == *slot2) *slot2 = RND_NUMBER_TO_SLOTS(RNGBYTE);
      001162 E5 0B            [12] 2589 	mov	a,_GetRandomSlots_sloc0_1_0
      001164 B5 0C 3D         [24] 2590 	cjne	a,_GetRandomSlots_sloc1_1_0,00102$
      001167 90 70 81         [24] 2591 	mov	dptr,#_RNGBYTE
      00116A E0               [24] 2592 	movx	a,@dptr
      00116B FB               [12] 2593 	mov	r3,a
      00116C 7A 00            [12] 2594 	mov	r2,#0x00
      00116E 90 05 08         [24] 2595 	mov	dptr,#__modsint_PARM_2
      001171 74 32            [12] 2596 	mov	a,#0x32
      001173 F0               [24] 2597 	movx	@dptr,a
      001174 E4               [12] 2598 	clr	a
      001175 A3               [24] 2599 	inc	dptr
      001176 F0               [24] 2600 	movx	@dptr,a
      001177 8B 82            [24] 2601 	mov	dpl,r3
      001179 8A 83            [24] 2602 	mov	dph,r2
      00117B C0 07            [24] 2603 	push	ar7
      00117D C0 06            [24] 2604 	push	ar6
      00117F C0 05            [24] 2605 	push	ar5
      001181 C0 04            [24] 2606 	push	ar4
      001183 C0 01            [24] 2607 	push	ar1
      001185 C0 00            [24] 2608 	push	ar0
      001187 12 81 FB         [24] 2609 	lcall	__modsint
      00118A AA 82            [24] 2610 	mov	r2,dpl
      00118C AB 83            [24] 2611 	mov	r3,dph
      00118E D0 00            [24] 2612 	pop	ar0
      001190 D0 01            [24] 2613 	pop	ar1
      001192 D0 04            [24] 2614 	pop	ar4
      001194 D0 05            [24] 2615 	pop	ar5
      001196 D0 06            [24] 2616 	pop	ar6
      001198 D0 07            [24] 2617 	pop	ar7
      00119A 8D 82            [24] 2618 	mov	dpl,r5
      00119C 8E 83            [24] 2619 	mov	dph,r6
      00119E 8F F0            [24] 2620 	mov	b,r7
      0011A0 EA               [12] 2621 	mov	a,r2
      0011A1 12 7C 62         [24] 2622 	lcall	__gptrput
      0011A4                       2623 00102$:
                                   2624 ;	..\src\atrs\misc.c:386: if(*slot2 == *slot3) *slot3 = RND_NUMBER_TO_SLOTS(RNGBYTE);
      0011A4 8D 82            [24] 2625 	mov	dpl,r5
      0011A6 8E 83            [24] 2626 	mov	dph,r6
      0011A8 8F F0            [24] 2627 	mov	b,r7
      0011AA 12 8D 8E         [24] 2628 	lcall	__gptrget
      0011AD FB               [12] 2629 	mov	r3,a
      0011AE 88 82            [24] 2630 	mov	dpl,r0
      0011B0 89 83            [24] 2631 	mov	dph,r1
      0011B2 8C F0            [24] 2632 	mov	b,r4
      0011B4 12 8D 8E         [24] 2633 	lcall	__gptrget
      0011B7 FA               [12] 2634 	mov	r2,a
      0011B8 EB               [12] 2635 	mov	a,r3
      0011B9 B5 02 3D         [24] 2636 	cjne	a,ar2,00104$
      0011BC 90 70 81         [24] 2637 	mov	dptr,#_RNGBYTE
      0011BF E0               [24] 2638 	movx	a,@dptr
      0011C0 FB               [12] 2639 	mov	r3,a
      0011C1 7A 00            [12] 2640 	mov	r2,#0x00
      0011C3 90 05 08         [24] 2641 	mov	dptr,#__modsint_PARM_2
      0011C6 74 32            [12] 2642 	mov	a,#0x32
      0011C8 F0               [24] 2643 	movx	@dptr,a
      0011C9 E4               [12] 2644 	clr	a
      0011CA A3               [24] 2645 	inc	dptr
      0011CB F0               [24] 2646 	movx	@dptr,a
      0011CC 8B 82            [24] 2647 	mov	dpl,r3
      0011CE 8A 83            [24] 2648 	mov	dph,r2
      0011D0 C0 07            [24] 2649 	push	ar7
      0011D2 C0 06            [24] 2650 	push	ar6
      0011D4 C0 05            [24] 2651 	push	ar5
      0011D6 C0 04            [24] 2652 	push	ar4
      0011D8 C0 01            [24] 2653 	push	ar1
      0011DA C0 00            [24] 2654 	push	ar0
      0011DC 12 81 FB         [24] 2655 	lcall	__modsint
      0011DF AA 82            [24] 2656 	mov	r2,dpl
      0011E1 AB 83            [24] 2657 	mov	r3,dph
      0011E3 D0 00            [24] 2658 	pop	ar0
      0011E5 D0 01            [24] 2659 	pop	ar1
      0011E7 D0 04            [24] 2660 	pop	ar4
      0011E9 D0 05            [24] 2661 	pop	ar5
      0011EB D0 06            [24] 2662 	pop	ar6
      0011ED D0 07            [24] 2663 	pop	ar7
      0011EF 88 82            [24] 2664 	mov	dpl,r0
      0011F1 89 83            [24] 2665 	mov	dph,r1
      0011F3 8C F0            [24] 2666 	mov	b,r4
      0011F5 EA               [12] 2667 	mov	a,r2
      0011F6 12 7C 62         [24] 2668 	lcall	__gptrput
      0011F9                       2669 00104$:
                                   2670 ;	..\src\atrs\misc.c:387: if(*slot1 == *slot3) *slot3 = RND_NUMBER_TO_SLOTS(RNGBYTE);
      0011F9 C0 05            [24] 2671 	push	ar5
      0011FB C0 06            [24] 2672 	push	ar6
      0011FD C0 07            [24] 2673 	push	ar7
      0011FF 90 00 C2         [24] 2674 	mov	dptr,#_GetRandomSlots_slot1_65536_295
      001202 E0               [24] 2675 	movx	a,@dptr
      001203 FA               [12] 2676 	mov	r2,a
      001204 A3               [24] 2677 	inc	dptr
      001205 E0               [24] 2678 	movx	a,@dptr
      001206 FB               [12] 2679 	mov	r3,a
      001207 A3               [24] 2680 	inc	dptr
      001208 E0               [24] 2681 	movx	a,@dptr
      001209 FF               [12] 2682 	mov	r7,a
      00120A 8A 82            [24] 2683 	mov	dpl,r2
      00120C 8B 83            [24] 2684 	mov	dph,r3
      00120E 8F F0            [24] 2685 	mov	b,r7
      001210 12 8D 8E         [24] 2686 	lcall	__gptrget
      001213 FA               [12] 2687 	mov	r2,a
      001214 88 82            [24] 2688 	mov	dpl,r0
      001216 89 83            [24] 2689 	mov	dph,r1
      001218 8C F0            [24] 2690 	mov	b,r4
      00121A 12 8D 8E         [24] 2691 	lcall	__gptrget
      00121D FF               [12] 2692 	mov	r7,a
      00121E EA               [12] 2693 	mov	a,r2
      00121F B5 07 02         [24] 2694 	cjne	a,ar7,00143$
      001222 80 08            [24] 2695 	sjmp	00144$
      001224                       2696 00143$:
      001224 D0 07            [24] 2697 	pop	ar7
      001226 D0 06            [24] 2698 	pop	ar6
      001228 D0 05            [24] 2699 	pop	ar5
      00122A 80 43            [24] 2700 	sjmp	00106$
      00122C                       2701 00144$:
      00122C D0 07            [24] 2702 	pop	ar7
      00122E D0 06            [24] 2703 	pop	ar6
      001230 D0 05            [24] 2704 	pop	ar5
      001232 90 70 81         [24] 2705 	mov	dptr,#_RNGBYTE
      001235 E0               [24] 2706 	movx	a,@dptr
      001236 FB               [12] 2707 	mov	r3,a
      001237 7A 00            [12] 2708 	mov	r2,#0x00
      001239 90 05 08         [24] 2709 	mov	dptr,#__modsint_PARM_2
      00123C 74 32            [12] 2710 	mov	a,#0x32
      00123E F0               [24] 2711 	movx	@dptr,a
      00123F E4               [12] 2712 	clr	a
      001240 A3               [24] 2713 	inc	dptr
      001241 F0               [24] 2714 	movx	@dptr,a
      001242 8B 82            [24] 2715 	mov	dpl,r3
      001244 8A 83            [24] 2716 	mov	dph,r2
      001246 C0 07            [24] 2717 	push	ar7
      001248 C0 06            [24] 2718 	push	ar6
      00124A C0 05            [24] 2719 	push	ar5
      00124C C0 04            [24] 2720 	push	ar4
      00124E C0 01            [24] 2721 	push	ar1
      001250 C0 00            [24] 2722 	push	ar0
      001252 12 81 FB         [24] 2723 	lcall	__modsint
      001255 AA 82            [24] 2724 	mov	r2,dpl
      001257 AB 83            [24] 2725 	mov	r3,dph
      001259 D0 00            [24] 2726 	pop	ar0
      00125B D0 01            [24] 2727 	pop	ar1
      00125D D0 04            [24] 2728 	pop	ar4
      00125F D0 05            [24] 2729 	pop	ar5
      001261 D0 06            [24] 2730 	pop	ar6
      001263 D0 07            [24] 2731 	pop	ar7
      001265 88 82            [24] 2732 	mov	dpl,r0
      001267 89 83            [24] 2733 	mov	dph,r1
      001269 8C F0            [24] 2734 	mov	b,r4
      00126B EA               [12] 2735 	mov	a,r2
      00126C 12 7C 62         [24] 2736 	lcall	__gptrput
      00126F                       2737 00106$:
                                   2738 ;	..\src\atrs\misc.c:389: if(*slot1 > *slot2) swap_variables(slot1,slot2);
      00126F 90 00 C2         [24] 2739 	mov	dptr,#_GetRandomSlots_slot1_65536_295
      001272 E0               [24] 2740 	movx	a,@dptr
      001273 F5 0D            [12] 2741 	mov	_GetRandomSlots_sloc2_1_0,a
      001275 A3               [24] 2742 	inc	dptr
      001276 E0               [24] 2743 	movx	a,@dptr
      001277 F5 0E            [12] 2744 	mov	(_GetRandomSlots_sloc2_1_0 + 1),a
      001279 A3               [24] 2745 	inc	dptr
      00127A E0               [24] 2746 	movx	a,@dptr
      00127B F5 0F            [12] 2747 	mov	(_GetRandomSlots_sloc2_1_0 + 2),a
      00127D 85 0D 82         [24] 2748 	mov	dpl,_GetRandomSlots_sloc2_1_0
      001280 85 0E 83         [24] 2749 	mov	dph,(_GetRandomSlots_sloc2_1_0 + 1)
      001283 85 0F F0         [24] 2750 	mov	b,(_GetRandomSlots_sloc2_1_0 + 2)
      001286 12 8D 8E         [24] 2751 	lcall	__gptrget
      001289 FB               [12] 2752 	mov	r3,a
      00128A 8D 82            [24] 2753 	mov	dpl,r5
      00128C 8E 83            [24] 2754 	mov	dph,r6
      00128E 8F F0            [24] 2755 	mov	b,r7
      001290 12 8D 8E         [24] 2756 	lcall	__gptrget
      001293 C3               [12] 2757 	clr	c
      001294 9B               [12] 2758 	subb	a,r3
      001295 50 23            [24] 2759 	jnc	00108$
      001297 90 00 B6         [24] 2760 	mov	dptr,#_swap_variables_PARM_2
      00129A ED               [12] 2761 	mov	a,r5
      00129B F0               [24] 2762 	movx	@dptr,a
      00129C EE               [12] 2763 	mov	a,r6
      00129D A3               [24] 2764 	inc	dptr
      00129E F0               [24] 2765 	movx	@dptr,a
      00129F EF               [12] 2766 	mov	a,r7
      0012A0 A3               [24] 2767 	inc	dptr
      0012A1 F0               [24] 2768 	movx	@dptr,a
      0012A2 85 0D 82         [24] 2769 	mov	dpl,_GetRandomSlots_sloc2_1_0
      0012A5 85 0E 83         [24] 2770 	mov	dph,(_GetRandomSlots_sloc2_1_0 + 1)
      0012A8 85 0F F0         [24] 2771 	mov	b,(_GetRandomSlots_sloc2_1_0 + 2)
      0012AB C0 04            [24] 2772 	push	ar4
      0012AD C0 01            [24] 2773 	push	ar1
      0012AF C0 00            [24] 2774 	push	ar0
      0012B1 12 10 2B         [24] 2775 	lcall	_swap_variables
      0012B4 D0 00            [24] 2776 	pop	ar0
      0012B6 D0 01            [24] 2777 	pop	ar1
      0012B8 D0 04            [24] 2778 	pop	ar4
      0012BA                       2779 00108$:
                                   2780 ;	..\src\atrs\misc.c:390: if(*slot2 > *slot3) swap_variables(slot2,slot3);
      0012BA 90 00 BC         [24] 2781 	mov	dptr,#_GetRandomSlots_PARM_2
      0012BD E0               [24] 2782 	movx	a,@dptr
      0012BE FD               [12] 2783 	mov	r5,a
      0012BF A3               [24] 2784 	inc	dptr
      0012C0 E0               [24] 2785 	movx	a,@dptr
      0012C1 FE               [12] 2786 	mov	r6,a
      0012C2 A3               [24] 2787 	inc	dptr
      0012C3 E0               [24] 2788 	movx	a,@dptr
      0012C4 FF               [12] 2789 	mov	r7,a
      0012C5 8D 82            [24] 2790 	mov	dpl,r5
      0012C7 8E 83            [24] 2791 	mov	dph,r6
      0012C9 8F F0            [24] 2792 	mov	b,r7
      0012CB 12 8D 8E         [24] 2793 	lcall	__gptrget
      0012CE FB               [12] 2794 	mov	r3,a
      0012CF 88 82            [24] 2795 	mov	dpl,r0
      0012D1 89 83            [24] 2796 	mov	dph,r1
      0012D3 8C F0            [24] 2797 	mov	b,r4
      0012D5 12 8D 8E         [24] 2798 	lcall	__gptrget
      0012D8 C3               [12] 2799 	clr	c
      0012D9 9B               [12] 2800 	subb	a,r3
      0012DA 50 14            [24] 2801 	jnc	00110$
      0012DC 90 00 B6         [24] 2802 	mov	dptr,#_swap_variables_PARM_2
      0012DF E8               [12] 2803 	mov	a,r0
      0012E0 F0               [24] 2804 	movx	@dptr,a
      0012E1 E9               [12] 2805 	mov	a,r1
      0012E2 A3               [24] 2806 	inc	dptr
      0012E3 F0               [24] 2807 	movx	@dptr,a
      0012E4 EC               [12] 2808 	mov	a,r4
      0012E5 A3               [24] 2809 	inc	dptr
      0012E6 F0               [24] 2810 	movx	@dptr,a
      0012E7 8D 82            [24] 2811 	mov	dpl,r5
      0012E9 8E 83            [24] 2812 	mov	dph,r6
      0012EB 8F F0            [24] 2813 	mov	b,r7
      0012ED 12 10 2B         [24] 2814 	lcall	_swap_variables
      0012F0                       2815 00110$:
                                   2816 ;	..\src\atrs\misc.c:391: if(*slot1 > *slot2) swap_variables(slot1,slot2);
      0012F0 90 00 C2         [24] 2817 	mov	dptr,#_GetRandomSlots_slot1_65536_295
      0012F3 E0               [24] 2818 	movx	a,@dptr
      0012F4 FD               [12] 2819 	mov	r5,a
      0012F5 A3               [24] 2820 	inc	dptr
      0012F6 E0               [24] 2821 	movx	a,@dptr
      0012F7 FE               [12] 2822 	mov	r6,a
      0012F8 A3               [24] 2823 	inc	dptr
      0012F9 E0               [24] 2824 	movx	a,@dptr
      0012FA FF               [12] 2825 	mov	r7,a
      0012FB 8D 82            [24] 2826 	mov	dpl,r5
      0012FD 8E 83            [24] 2827 	mov	dph,r6
      0012FF 8F F0            [24] 2828 	mov	b,r7
      001301 12 8D 8E         [24] 2829 	lcall	__gptrget
      001304 FC               [12] 2830 	mov	r4,a
      001305 90 00 BC         [24] 2831 	mov	dptr,#_GetRandomSlots_PARM_2
      001308 E0               [24] 2832 	movx	a,@dptr
      001309 F9               [12] 2833 	mov	r1,a
      00130A A3               [24] 2834 	inc	dptr
      00130B E0               [24] 2835 	movx	a,@dptr
      00130C FA               [12] 2836 	mov	r2,a
      00130D A3               [24] 2837 	inc	dptr
      00130E E0               [24] 2838 	movx	a,@dptr
      00130F FB               [12] 2839 	mov	r3,a
      001310 89 82            [24] 2840 	mov	dpl,r1
      001312 8A 83            [24] 2841 	mov	dph,r2
      001314 8B F0            [24] 2842 	mov	b,r3
      001316 12 8D 8E         [24] 2843 	lcall	__gptrget
      001319 C3               [12] 2844 	clr	c
      00131A 9C               [12] 2845 	subb	a,r4
      00131B 50 14            [24] 2846 	jnc	00113$
      00131D 90 00 B6         [24] 2847 	mov	dptr,#_swap_variables_PARM_2
      001320 E9               [12] 2848 	mov	a,r1
      001321 F0               [24] 2849 	movx	@dptr,a
      001322 EA               [12] 2850 	mov	a,r2
      001323 A3               [24] 2851 	inc	dptr
      001324 F0               [24] 2852 	movx	@dptr,a
      001325 EB               [12] 2853 	mov	a,r3
      001326 A3               [24] 2854 	inc	dptr
      001327 F0               [24] 2855 	movx	@dptr,a
      001328 8D 82            [24] 2856 	mov	dpl,r5
      00132A 8E 83            [24] 2857 	mov	dph,r6
      00132C 8F F0            [24] 2858 	mov	b,r7
                                   2859 ;	..\src\atrs\misc.c:392: }
      00132E 02 10 2B         [24] 2860 	ljmp	_swap_variables
      001331                       2861 00113$:
      001331 22               [24] 2862 	ret
                                   2863 ;------------------------------------------------------------
                                   2864 ;Allocation info for local variables in function 'AssemblyPacket'
                                   2865 ;------------------------------------------------------------
                                   2866 ;sloc0                     Allocated with name '_AssemblyPacket_sloc0_1_0'
                                   2867 ;sloc1                     Allocated with name '_AssemblyPacket_sloc1_1_0'
                                   2868 ;Copy1                     Allocated with name '_AssemblyPacket_PARM_2'
                                   2869 ;Copy2                     Allocated with name '_AssemblyPacket_PARM_3'
                                   2870 ;Payload                   Allocated with name '_AssemblyPacket_PARM_4'
                                   2871 ;PayloadLength             Allocated with name '_AssemblyPacket_PARM_5'
                                   2872 ;ack                       Allocated with name '_AssemblyPacket_PARM_6'
                                   2873 ;TxPackage                 Allocated with name '_AssemblyPacket_PARM_7'
                                   2874 ;TotalLength               Allocated with name '_AssemblyPacket_PARM_8'
                                   2875 ;GoldCode                  Allocated with name '_AssemblyPacket_GoldCode_65536_297'
                                   2876 ;boardID                   Allocated with name '_AssemblyPacket_boardID_65536_298'
                                   2877 ;HMAC                      Allocated with name '_AssemblyPacket_HMAC_65536_298'
                                   2878 ;StdPrem                   Allocated with name '_AssemblyPacket_StdPrem_65536_298'
                                   2879 ;CRC32                     Allocated with name '_AssemblyPacket_CRC32_65536_298'
                                   2880 ;------------------------------------------------------------
                                   2881 ;	..\src\atrs\misc.c:407: void AssemblyPacket(uint32_t GoldCode, uint8_t Copy1, uint8_t Copy2, uint8_t *Payload, uint8_t PayloadLength, uint8_t ack, uint8_t *TxPackage, uint8_t *TotalLength)
                                   2882 ;	-----------------------------------------
                                   2883 ;	 function AssemblyPacket
                                   2884 ;	-----------------------------------------
      001332                       2885 _AssemblyPacket:
      001332 AF 82            [24] 2886 	mov	r7,dpl
      001334 AE 83            [24] 2887 	mov	r6,dph
      001336 AD F0            [24] 2888 	mov	r5,b
      001338 FC               [12] 2889 	mov	r4,a
      001339 90 00 D2         [24] 2890 	mov	dptr,#_AssemblyPacket_GoldCode_65536_297
      00133C EF               [12] 2891 	mov	a,r7
      00133D F0               [24] 2892 	movx	@dptr,a
      00133E EE               [12] 2893 	mov	a,r6
      00133F A3               [24] 2894 	inc	dptr
      001340 F0               [24] 2895 	movx	@dptr,a
      001341 ED               [12] 2896 	mov	a,r5
      001342 A3               [24] 2897 	inc	dptr
      001343 F0               [24] 2898 	movx	@dptr,a
      001344 EC               [12] 2899 	mov	a,r4
      001345 A3               [24] 2900 	inc	dptr
      001346 F0               [24] 2901 	movx	@dptr,a
                                   2902 ;	..\src\atrs\misc.c:411: uint32_t HMAC= 0x12345678;
      001347 90 00 D6         [24] 2903 	mov	dptr,#_AssemblyPacket_HMAC_65536_298
      00134A 74 78            [12] 2904 	mov	a,#0x78
      00134C F0               [24] 2905 	movx	@dptr,a
      00134D 74 56            [12] 2906 	mov	a,#0x56
      00134F A3               [24] 2907 	inc	dptr
      001350 F0               [24] 2908 	movx	@dptr,a
      001351 74 34            [12] 2909 	mov	a,#0x34
      001353 A3               [24] 2910 	inc	dptr
      001354 F0               [24] 2911 	movx	@dptr,a
      001355 74 12            [12] 2912 	mov	a,#0x12
      001357 A3               [24] 2913 	inc	dptr
      001358 F0               [24] 2914 	movx	@dptr,a
                                   2915 ;	..\src\atrs\misc.c:415: *TxPackage =(uint8_t) (GoldCode & 0x000000FF);
      001359 90 00 CC         [24] 2916 	mov	dptr,#_AssemblyPacket_PARM_7
      00135C E0               [24] 2917 	movx	a,@dptr
      00135D FD               [12] 2918 	mov	r5,a
      00135E A3               [24] 2919 	inc	dptr
      00135F E0               [24] 2920 	movx	a,@dptr
      001360 FE               [12] 2921 	mov	r6,a
      001361 A3               [24] 2922 	inc	dptr
      001362 E0               [24] 2923 	movx	a,@dptr
      001363 FF               [12] 2924 	mov	r7,a
      001364 90 00 D2         [24] 2925 	mov	dptr,#_AssemblyPacket_GoldCode_65536_297
      001367 E0               [24] 2926 	movx	a,@dptr
      001368 F5 10            [12] 2927 	mov	_AssemblyPacket_sloc0_1_0,a
      00136A A3               [24] 2928 	inc	dptr
      00136B E0               [24] 2929 	movx	a,@dptr
      00136C F5 11            [12] 2930 	mov	(_AssemblyPacket_sloc0_1_0 + 1),a
      00136E A3               [24] 2931 	inc	dptr
      00136F E0               [24] 2932 	movx	a,@dptr
      001370 F5 12            [12] 2933 	mov	(_AssemblyPacket_sloc0_1_0 + 2),a
      001372 A3               [24] 2934 	inc	dptr
      001373 E0               [24] 2935 	movx	a,@dptr
      001374 F5 13            [12] 2936 	mov	(_AssemblyPacket_sloc0_1_0 + 3),a
      001376 A8 10            [24] 2937 	mov	r0,_AssemblyPacket_sloc0_1_0
      001378 8D 82            [24] 2938 	mov	dpl,r5
      00137A 8E 83            [24] 2939 	mov	dph,r6
      00137C 8F F0            [24] 2940 	mov	b,r7
      00137E E8               [12] 2941 	mov	a,r0
      00137F 12 7C 62         [24] 2942 	lcall	__gptrput
                                   2943 ;	..\src\atrs\misc.c:416: *(TxPackage+1) = (uint8_t) ((GoldCode & 0x0000FF00)>>8);
      001382 74 01            [12] 2944 	mov	a,#0x01
      001384 2D               [12] 2945 	add	a,r5
      001385 F5 14            [12] 2946 	mov	_AssemblyPacket_sloc1_1_0,a
      001387 E4               [12] 2947 	clr	a
      001388 3E               [12] 2948 	addc	a,r6
      001389 F5 15            [12] 2949 	mov	(_AssemblyPacket_sloc1_1_0 + 1),a
      00138B 8F 16            [24] 2950 	mov	(_AssemblyPacket_sloc1_1_0 + 2),r7
      00138D AA 11            [24] 2951 	mov	r2,(_AssemblyPacket_sloc0_1_0 + 1)
      00138F 8A 01            [24] 2952 	mov	ar1,r2
      001391 85 14 82         [24] 2953 	mov	dpl,_AssemblyPacket_sloc1_1_0
      001394 85 15 83         [24] 2954 	mov	dph,(_AssemblyPacket_sloc1_1_0 + 1)
      001397 85 16 F0         [24] 2955 	mov	b,(_AssemblyPacket_sloc1_1_0 + 2)
      00139A E9               [12] 2956 	mov	a,r1
      00139B 12 7C 62         [24] 2957 	lcall	__gptrput
                                   2958 ;	..\src\atrs\misc.c:417: *(TxPackage+2) = (uint8_t) ((GoldCode & 0x00FF0000)>>16);
      00139E 74 02            [12] 2959 	mov	a,#0x02
      0013A0 2D               [12] 2960 	add	a,r5
      0013A1 F5 14            [12] 2961 	mov	_AssemblyPacket_sloc1_1_0,a
      0013A3 E4               [12] 2962 	clr	a
      0013A4 3E               [12] 2963 	addc	a,r6
      0013A5 F5 15            [12] 2964 	mov	(_AssemblyPacket_sloc1_1_0 + 1),a
      0013A7 8F 16            [24] 2965 	mov	(_AssemblyPacket_sloc1_1_0 + 2),r7
      0013A9 AB 12            [24] 2966 	mov	r3,(_AssemblyPacket_sloc0_1_0 + 2)
      0013AB 8B 00            [24] 2967 	mov	ar0,r3
      0013AD 85 14 82         [24] 2968 	mov	dpl,_AssemblyPacket_sloc1_1_0
      0013B0 85 15 83         [24] 2969 	mov	dph,(_AssemblyPacket_sloc1_1_0 + 1)
      0013B3 85 16 F0         [24] 2970 	mov	b,(_AssemblyPacket_sloc1_1_0 + 2)
      0013B6 E8               [12] 2971 	mov	a,r0
      0013B7 12 7C 62         [24] 2972 	lcall	__gptrput
                                   2973 ;	..\src\atrs\misc.c:418: *(TxPackage+3) = (uint8_t) (((GoldCode & 0x7F000000)>>24)| (Copy1 & 0x01)<<7);
      0013BA 74 03            [12] 2974 	mov	a,#0x03
      0013BC 2D               [12] 2975 	add	a,r5
      0013BD F5 14            [12] 2976 	mov	_AssemblyPacket_sloc1_1_0,a
      0013BF E4               [12] 2977 	clr	a
      0013C0 3E               [12] 2978 	addc	a,r6
      0013C1 F5 15            [12] 2979 	mov	(_AssemblyPacket_sloc1_1_0 + 1),a
      0013C3 8F 16            [24] 2980 	mov	(_AssemblyPacket_sloc1_1_0 + 2),r7
      0013C5 E4               [12] 2981 	clr	a
      0013C6 74 7F            [12] 2982 	mov	a,#0x7f
      0013C8 55 13            [12] 2983 	anl	a,(_AssemblyPacket_sloc0_1_0 + 3)
      0013CA F8               [12] 2984 	mov	r0,a
      0013CB 90 00 C5         [24] 2985 	mov	dptr,#_AssemblyPacket_PARM_2
      0013CE E0               [24] 2986 	movx	a,@dptr
      0013CF FC               [12] 2987 	mov	r4,a
      0013D0 54 01            [12] 2988 	anl	a,#0x01
      0013D2 03               [12] 2989 	rr	a
      0013D3 54 80            [12] 2990 	anl	a,#0x80
      0013D5 FB               [12] 2991 	mov	r3,a
      0013D6 E8               [12] 2992 	mov	a,r0
      0013D7 42 03            [12] 2993 	orl	ar3,a
      0013D9 85 14 82         [24] 2994 	mov	dpl,_AssemblyPacket_sloc1_1_0
      0013DC 85 15 83         [24] 2995 	mov	dph,(_AssemblyPacket_sloc1_1_0 + 1)
      0013DF 85 16 F0         [24] 2996 	mov	b,(_AssemblyPacket_sloc1_1_0 + 2)
      0013E2 EB               [12] 2997 	mov	a,r3
      0013E3 12 7C 62         [24] 2998 	lcall	__gptrput
                                   2999 ;	..\src\atrs\misc.c:419: *(TxPackage+4) = (uint8_t)  (Copy1 & 0xFE)>>1 | ((Copy2 & 0x07)<<5);
      0013E6 74 04            [12] 3000 	mov	a,#0x04
      0013E8 2D               [12] 3001 	add	a,r5
      0013E9 F9               [12] 3002 	mov	r1,a
      0013EA E4               [12] 3003 	clr	a
      0013EB 3E               [12] 3004 	addc	a,r6
      0013EC FA               [12] 3005 	mov	r2,a
      0013ED 8F 03            [24] 3006 	mov	ar3,r7
      0013EF 53 04 FE         [24] 3007 	anl	ar4,#0xfe
      0013F2 EC               [12] 3008 	mov	a,r4
      0013F3 C3               [12] 3009 	clr	c
      0013F4 13               [12] 3010 	rrc	a
      0013F5 F5 14            [12] 3011 	mov	_AssemblyPacket_sloc1_1_0,a
      0013F7 90 00 C6         [24] 3012 	mov	dptr,#_AssemblyPacket_PARM_3
      0013FA E0               [24] 3013 	movx	a,@dptr
      0013FB F8               [12] 3014 	mov	r0,a
      0013FC 54 07            [12] 3015 	anl	a,#0x07
      0013FE C4               [12] 3016 	swap	a
      0013FF 23               [12] 3017 	rl	a
      001400 54 E0            [12] 3018 	anl	a,#0xe0
      001402 F5 10            [12] 3019 	mov	_AssemblyPacket_sloc0_1_0,a
      001404 AC 14            [24] 3020 	mov	r4,_AssemblyPacket_sloc1_1_0
      001406 42 04            [12] 3021 	orl	ar4,a
      001408 89 82            [24] 3022 	mov	dpl,r1
      00140A 8A 83            [24] 3023 	mov	dph,r2
      00140C 8B F0            [24] 3024 	mov	b,r3
      00140E EC               [12] 3025 	mov	a,r4
      00140F 12 7C 62         [24] 3026 	lcall	__gptrput
                                   3027 ;	..\src\atrs\misc.c:420: *(TxPackage+5) = (uint8_t) ((Copy2 & 0x38)>>3) | (boardID & 0x00001F)<<3;
      001412 74 05            [12] 3028 	mov	a,#0x05
      001414 2D               [12] 3029 	add	a,r5
      001415 FA               [12] 3030 	mov	r2,a
      001416 E4               [12] 3031 	clr	a
      001417 3E               [12] 3032 	addc	a,r6
      001418 FB               [12] 3033 	mov	r3,a
      001419 8F 04            [24] 3034 	mov	ar4,r7
      00141B 53 00 38         [24] 3035 	anl	ar0,#0x38
      00141E E4               [12] 3036 	clr	a
      00141F 23               [12] 3037 	rl	a
      001420 C8               [12] 3038 	xch	a,r0
      001421 C4               [12] 3039 	swap	a
      001422 23               [12] 3040 	rl	a
      001423 54 1F            [12] 3041 	anl	a,#0x1f
      001425 68               [12] 3042 	xrl	a,r0
      001426 C8               [12] 3043 	xch	a,r0
      001427 54 1F            [12] 3044 	anl	a,#0x1f
      001429 C8               [12] 3045 	xch	a,r0
      00142A 68               [12] 3046 	xrl	a,r0
      00142B C8               [12] 3047 	xch	a,r0
      00142C 30 E4 02         [24] 3048 	jnb	acc.4,00103$
      00142F 44 E0            [12] 3049 	orl	a,#0xe0
      001431                       3050 00103$:
      001431 43 00 B0         [24] 3051 	orl	ar0,#0xb0
      001434 8A 82            [24] 3052 	mov	dpl,r2
      001436 8B 83            [24] 3053 	mov	dph,r3
      001438 8C F0            [24] 3054 	mov	b,r4
      00143A E8               [12] 3055 	mov	a,r0
      00143B 12 7C 62         [24] 3056 	lcall	__gptrput
                                   3057 ;	..\src\atrs\misc.c:421: *(TxPackage+6) = (uint8_t) ((boardID & 0x001FE0)>>5);
      00143E 74 06            [12] 3058 	mov	a,#0x06
      001440 2D               [12] 3059 	add	a,r5
      001441 FA               [12] 3060 	mov	r2,a
      001442 E4               [12] 3061 	clr	a
      001443 3E               [12] 3062 	addc	a,r6
      001444 FB               [12] 3063 	mov	r3,a
      001445 8F 04            [24] 3064 	mov	ar4,r7
      001447 8A 82            [24] 3065 	mov	dpl,r2
      001449 8B 83            [24] 3066 	mov	dph,r3
      00144B 8C F0            [24] 3067 	mov	b,r4
      00144D 74 A2            [12] 3068 	mov	a,#0xa2
      00144F 12 7C 62         [24] 3069 	lcall	__gptrput
                                   3070 ;	..\src\atrs\misc.c:422: *(TxPackage+7) = (uint8_t) ((boardID & 0x1FE000)>>13);
      001452 74 07            [12] 3071 	mov	a,#0x07
      001454 2D               [12] 3072 	add	a,r5
      001455 FA               [12] 3073 	mov	r2,a
      001456 E4               [12] 3074 	clr	a
      001457 3E               [12] 3075 	addc	a,r6
      001458 FB               [12] 3076 	mov	r3,a
      001459 8F 04            [24] 3077 	mov	ar4,r7
      00145B 8A 82            [24] 3078 	mov	dpl,r2
      00145D 8B 83            [24] 3079 	mov	dph,r3
      00145F 8C F0            [24] 3080 	mov	b,r4
      001461 74 91            [12] 3081 	mov	a,#0x91
      001463 12 7C 62         [24] 3082 	lcall	__gptrput
                                   3083 ;	..\src\atrs\misc.c:423: *(TxPackage+8) = (uint8_t) ((boardID & 0xE0000000)>>21)| (StdPrem & 0x0001) <<4|(ack << 5 & 0x0001);//quedan los tres bits reservados
      001466 74 08            [12] 3084 	mov	a,#0x08
      001468 2D               [12] 3085 	add	a,r5
      001469 FA               [12] 3086 	mov	r2,a
      00146A E4               [12] 3087 	clr	a
      00146B 3E               [12] 3088 	addc	a,r6
      00146C FB               [12] 3089 	mov	r3,a
      00146D 8F 04            [24] 3090 	mov	ar4,r7
      00146F 74 01            [12] 3091 	mov	a,#0x01
      001471 54 01            [12] 3092 	anl	a,#0x01
      001473 C4               [12] 3093 	swap	a
      001474 54 F0            [12] 3094 	anl	a,#0xf0
      001476 F9               [12] 3095 	mov	r1,a
      001477 90 00 CB         [24] 3096 	mov	dptr,#_AssemblyPacket_PARM_6
      00147A E0               [24] 3097 	movx	a,@dptr
      00147B C4               [12] 3098 	swap	a
      00147C 23               [12] 3099 	rl	a
      00147D 54 E0            [12] 3100 	anl	a,#0xe0
      00147F F8               [12] 3101 	mov	r0,a
      001480 53 00 01         [24] 3102 	anl	ar0,#0x01
      001483 E9               [12] 3103 	mov	a,r1
      001484 42 00            [12] 3104 	orl	ar0,a
      001486 8A 82            [24] 3105 	mov	dpl,r2
      001488 8B 83            [24] 3106 	mov	dph,r3
      00148A 8C F0            [24] 3107 	mov	b,r4
      00148C E8               [12] 3108 	mov	a,r0
      00148D 12 7C 62         [24] 3109 	lcall	__gptrput
                                   3110 ;	..\src\atrs\misc.c:424: memcpy(TxPackage+9,&HMAC,sizeof(uint32_t));
      001490 74 09            [12] 3111 	mov	a,#0x09
      001492 2D               [12] 3112 	add	a,r5
      001493 FA               [12] 3113 	mov	r2,a
      001494 E4               [12] 3114 	clr	a
      001495 3E               [12] 3115 	addc	a,r6
      001496 FB               [12] 3116 	mov	r3,a
      001497 8F 04            [24] 3117 	mov	ar4,r7
      001499 90 04 6D         [24] 3118 	mov	dptr,#_memcpy_PARM_2
      00149C 74 D6            [12] 3119 	mov	a,#_AssemblyPacket_HMAC_65536_298
      00149E F0               [24] 3120 	movx	@dptr,a
      00149F 74 00            [12] 3121 	mov	a,#(_AssemblyPacket_HMAC_65536_298 >> 8)
      0014A1 A3               [24] 3122 	inc	dptr
      0014A2 F0               [24] 3123 	movx	@dptr,a
      0014A3 E4               [12] 3124 	clr	a
      0014A4 A3               [24] 3125 	inc	dptr
      0014A5 F0               [24] 3126 	movx	@dptr,a
      0014A6 90 04 70         [24] 3127 	mov	dptr,#_memcpy_PARM_3
      0014A9 74 04            [12] 3128 	mov	a,#0x04
      0014AB F0               [24] 3129 	movx	@dptr,a
      0014AC E4               [12] 3130 	clr	a
      0014AD A3               [24] 3131 	inc	dptr
      0014AE F0               [24] 3132 	movx	@dptr,a
      0014AF 8A 82            [24] 3133 	mov	dpl,r2
      0014B1 8B 83            [24] 3134 	mov	dph,r3
      0014B3 8C F0            [24] 3135 	mov	b,r4
      0014B5 C0 07            [24] 3136 	push	ar7
      0014B7 C0 06            [24] 3137 	push	ar6
      0014B9 C0 05            [24] 3138 	push	ar5
      0014BB 12 78 14         [24] 3139 	lcall	_memcpy
      0014BE D0 05            [24] 3140 	pop	ar5
      0014C0 D0 06            [24] 3141 	pop	ar6
      0014C2 D0 07            [24] 3142 	pop	ar7
                                   3143 ;	..\src\atrs\misc.c:425: memcpy(TxPackage+13,Payload,sizeof(uint8_t)*PayloadLength);
      0014C4 74 0D            [12] 3144 	mov	a,#0x0d
      0014C6 2D               [12] 3145 	add	a,r5
      0014C7 FA               [12] 3146 	mov	r2,a
      0014C8 E4               [12] 3147 	clr	a
      0014C9 3E               [12] 3148 	addc	a,r6
      0014CA FB               [12] 3149 	mov	r3,a
      0014CB 8F 04            [24] 3150 	mov	ar4,r7
      0014CD 8A 14            [24] 3151 	mov	_AssemblyPacket_sloc1_1_0,r2
      0014CF 8B 15            [24] 3152 	mov	(_AssemblyPacket_sloc1_1_0 + 1),r3
      0014D1 8C 16            [24] 3153 	mov	(_AssemblyPacket_sloc1_1_0 + 2),r4
      0014D3 90 00 C7         [24] 3154 	mov	dptr,#_AssemblyPacket_PARM_4
      0014D6 E0               [24] 3155 	movx	a,@dptr
      0014D7 F8               [12] 3156 	mov	r0,a
      0014D8 A3               [24] 3157 	inc	dptr
      0014D9 E0               [24] 3158 	movx	a,@dptr
      0014DA F9               [12] 3159 	mov	r1,a
      0014DB A3               [24] 3160 	inc	dptr
      0014DC E0               [24] 3161 	movx	a,@dptr
      0014DD FC               [12] 3162 	mov	r4,a
      0014DE 88 10            [24] 3163 	mov	_AssemblyPacket_sloc0_1_0,r0
      0014E0 89 11            [24] 3164 	mov	(_AssemblyPacket_sloc0_1_0 + 1),r1
      0014E2 8C 12            [24] 3165 	mov	(_AssemblyPacket_sloc0_1_0 + 2),r4
      0014E4 90 00 CA         [24] 3166 	mov	dptr,#_AssemblyPacket_PARM_5
      0014E7 E0               [24] 3167 	movx	a,@dptr
      0014E8 FB               [12] 3168 	mov	r3,a
      0014E9 FA               [12] 3169 	mov	r2,a
      0014EA 7C 00            [12] 3170 	mov	r4,#0x00
      0014EC 90 04 6D         [24] 3171 	mov	dptr,#_memcpy_PARM_2
      0014EF E5 10            [12] 3172 	mov	a,_AssemblyPacket_sloc0_1_0
      0014F1 F0               [24] 3173 	movx	@dptr,a
      0014F2 E5 11            [12] 3174 	mov	a,(_AssemblyPacket_sloc0_1_0 + 1)
      0014F4 A3               [24] 3175 	inc	dptr
      0014F5 F0               [24] 3176 	movx	@dptr,a
      0014F6 E5 12            [12] 3177 	mov	a,(_AssemblyPacket_sloc0_1_0 + 2)
      0014F8 A3               [24] 3178 	inc	dptr
      0014F9 F0               [24] 3179 	movx	@dptr,a
      0014FA 90 04 70         [24] 3180 	mov	dptr,#_memcpy_PARM_3
      0014FD EA               [12] 3181 	mov	a,r2
      0014FE F0               [24] 3182 	movx	@dptr,a
      0014FF EC               [12] 3183 	mov	a,r4
      001500 A3               [24] 3184 	inc	dptr
      001501 F0               [24] 3185 	movx	@dptr,a
      001502 85 14 82         [24] 3186 	mov	dpl,_AssemblyPacket_sloc1_1_0
      001505 85 15 83         [24] 3187 	mov	dph,(_AssemblyPacket_sloc1_1_0 + 1)
      001508 85 16 F0         [24] 3188 	mov	b,(_AssemblyPacket_sloc1_1_0 + 2)
      00150B C0 07            [24] 3189 	push	ar7
      00150D C0 06            [24] 3190 	push	ar6
      00150F C0 05            [24] 3191 	push	ar5
      001511 C0 04            [24] 3192 	push	ar4
      001513 C0 03            [24] 3193 	push	ar3
      001515 C0 02            [24] 3194 	push	ar2
      001517 12 78 14         [24] 3195 	lcall	_memcpy
      00151A D0 02            [24] 3196 	pop	ar2
      00151C D0 03            [24] 3197 	pop	ar3
      00151E D0 04            [24] 3198 	pop	ar4
      001520 D0 05            [24] 3199 	pop	ar5
      001522 D0 06            [24] 3200 	pop	ar6
      001524 D0 07            [24] 3201 	pop	ar7
                                   3202 ;	..\src\atrs\misc.c:426: CRC32 = crc_crc32_msb(TxPackage,13+PayloadLength,0xFFFFFFFF);
      001526 74 0D            [12] 3203 	mov	a,#0x0d
      001528 2A               [12] 3204 	add	a,r2
      001529 FA               [12] 3205 	mov	r2,a
      00152A E4               [12] 3206 	clr	a
      00152B 3C               [12] 3207 	addc	a,r4
      00152C FC               [12] 3208 	mov	r4,a
      00152D C0 03            [24] 3209 	push	ar3
      00152F 74 FF            [12] 3210 	mov	a,#0xff
      001531 C0 E0            [24] 3211 	push	acc
      001533 C0 E0            [24] 3212 	push	acc
      001535 C0 E0            [24] 3213 	push	acc
      001537 C0 E0            [24] 3214 	push	acc
      001539 C0 02            [24] 3215 	push	ar2
      00153B C0 04            [24] 3216 	push	ar4
      00153D 8D 82            [24] 3217 	mov	dpl,r5
      00153F 8E 83            [24] 3218 	mov	dph,r6
      001541 8F F0            [24] 3219 	mov	b,r7
      001543 12 89 7F         [24] 3220 	lcall	_crc_crc32_msb
      001546 AC 82            [24] 3221 	mov	r4,dpl
      001548 AD 83            [24] 3222 	mov	r5,dph
      00154A AE F0            [24] 3223 	mov	r6,b
      00154C FF               [12] 3224 	mov	r7,a
      00154D E5 81            [12] 3225 	mov	a,sp
      00154F 24 FA            [12] 3226 	add	a,#0xfa
      001551 F5 81            [12] 3227 	mov	sp,a
      001553 D0 03            [24] 3228 	pop	ar3
      001555 90 00 DA         [24] 3229 	mov	dptr,#_AssemblyPacket_CRC32_65536_298
      001558 EC               [12] 3230 	mov	a,r4
      001559 F0               [24] 3231 	movx	@dptr,a
      00155A ED               [12] 3232 	mov	a,r5
      00155B A3               [24] 3233 	inc	dptr
      00155C F0               [24] 3234 	movx	@dptr,a
      00155D EE               [12] 3235 	mov	a,r6
      00155E A3               [24] 3236 	inc	dptr
      00155F F0               [24] 3237 	movx	@dptr,a
      001560 EF               [12] 3238 	mov	a,r7
      001561 A3               [24] 3239 	inc	dptr
      001562 F0               [24] 3240 	movx	@dptr,a
                                   3241 ;	..\src\atrs\misc.c:427: memcpy(TxPackage+13+PayloadLength,&CRC32,sizeof(uint32_t));
      001563 90 00 CC         [24] 3242 	mov	dptr,#_AssemblyPacket_PARM_7
      001566 E0               [24] 3243 	movx	a,@dptr
      001567 FD               [12] 3244 	mov	r5,a
      001568 A3               [24] 3245 	inc	dptr
      001569 E0               [24] 3246 	movx	a,@dptr
      00156A FE               [12] 3247 	mov	r6,a
      00156B A3               [24] 3248 	inc	dptr
      00156C E0               [24] 3249 	movx	a,@dptr
      00156D FF               [12] 3250 	mov	r7,a
      00156E 74 0D            [12] 3251 	mov	a,#0x0d
      001570 2D               [12] 3252 	add	a,r5
      001571 FD               [12] 3253 	mov	r5,a
      001572 E4               [12] 3254 	clr	a
      001573 3E               [12] 3255 	addc	a,r6
      001574 FE               [12] 3256 	mov	r6,a
      001575 EB               [12] 3257 	mov	a,r3
      001576 2D               [12] 3258 	add	a,r5
      001577 FD               [12] 3259 	mov	r5,a
      001578 E4               [12] 3260 	clr	a
      001579 3E               [12] 3261 	addc	a,r6
      00157A FE               [12] 3262 	mov	r6,a
      00157B 90 04 6D         [24] 3263 	mov	dptr,#_memcpy_PARM_2
      00157E 74 DA            [12] 3264 	mov	a,#_AssemblyPacket_CRC32_65536_298
      001580 F0               [24] 3265 	movx	@dptr,a
      001581 74 00            [12] 3266 	mov	a,#(_AssemblyPacket_CRC32_65536_298 >> 8)
      001583 A3               [24] 3267 	inc	dptr
      001584 F0               [24] 3268 	movx	@dptr,a
      001585 E4               [12] 3269 	clr	a
      001586 A3               [24] 3270 	inc	dptr
      001587 F0               [24] 3271 	movx	@dptr,a
      001588 90 04 70         [24] 3272 	mov	dptr,#_memcpy_PARM_3
      00158B 74 04            [12] 3273 	mov	a,#0x04
      00158D F0               [24] 3274 	movx	@dptr,a
      00158E E4               [12] 3275 	clr	a
      00158F A3               [24] 3276 	inc	dptr
      001590 F0               [24] 3277 	movx	@dptr,a
      001591 8D 82            [24] 3278 	mov	dpl,r5
      001593 8E 83            [24] 3279 	mov	dph,r6
      001595 8F F0            [24] 3280 	mov	b,r7
      001597 C0 03            [24] 3281 	push	ar3
      001599 12 78 14         [24] 3282 	lcall	_memcpy
      00159C D0 03            [24] 3283 	pop	ar3
                                   3284 ;	..\src\atrs\misc.c:428: *TotalLength = 13+PayloadLength+4;
      00159E 90 00 CF         [24] 3285 	mov	dptr,#_AssemblyPacket_PARM_8
      0015A1 E0               [24] 3286 	movx	a,@dptr
      0015A2 FD               [12] 3287 	mov	r5,a
      0015A3 A3               [24] 3288 	inc	dptr
      0015A4 E0               [24] 3289 	movx	a,@dptr
      0015A5 FE               [12] 3290 	mov	r6,a
      0015A6 A3               [24] 3291 	inc	dptr
      0015A7 E0               [24] 3292 	movx	a,@dptr
      0015A8 FF               [12] 3293 	mov	r7,a
      0015A9 74 11            [12] 3294 	mov	a,#0x11
      0015AB 2B               [12] 3295 	add	a,r3
      0015AC 8D 82            [24] 3296 	mov	dpl,r5
      0015AE 8E 83            [24] 3297 	mov	dph,r6
      0015B0 8F F0            [24] 3298 	mov	b,r7
                                   3299 ;	..\src\atrs\misc.c:429: return;
                                   3300 ;	..\src\atrs\misc.c:431: }
      0015B2 02 7C 62         [24] 3301 	ljmp	__gptrput
                                   3302 	.area CSEG    (CODE)
                                   3303 	.area CONST   (CODE)
                                   3304 	.area CONST   (CODE)
      009038                       3305 ___str_0:
      009038 20 74 72 61 6E 73 6D  3306 	.ascii " transmit "
             69 74 20
      009042 00                    3307 	.db 0x00
                                   3308 	.area CSEG    (CODE)
                                   3309 	.area CONST   (CODE)
      009043                       3310 ___str_1:
      009043 20                    3311 	.ascii " "
      009044 0D                    3312 	.db 0x0d
      009045 0A                    3313 	.db 0x0a
      009046 20 47 50 53 20 63 61  3314 	.ascii " GPS callback "
             6C 6C 62 61 63 6B 20
      009054 0D                    3315 	.db 0x0d
      009055 0A                    3316 	.db 0x0a
      009056 00                    3317 	.db 0x00
                                   3318 	.area CSEG    (CODE)
                                   3319 	.area CONST   (CODE)
      009057                       3320 ___str_2:
      009057 20 66 72 65 63 75 65  3321 	.ascii " frecuencia: "
             6E 63 69 61 3A 20
      009064 00                    3322 	.db 0x00
                                   3323 	.area CSEG    (CODE)
                                   3324 	.area CONST   (CODE)
      009065                       3325 ___str_3:
      009065 20                    3326 	.ascii " "
      009066 0A                    3327 	.db 0x0a
      009067 20 62 61 63 6B 6F 66  3328 	.ascii " backoff timer expired "
             66 20 74 69 6D 65 72
             20 65 78 70 69 72 65
             64 20
      00907E 00                    3329 	.db 0x00
                                   3330 	.area CSEG    (CODE)
                                   3331 	.area CONST   (CODE)
      00907F                       3332 ___str_4:
      00907F 41 43 4B 20 74 69 6D  3333 	.ascii "ACK timer expired"
             65 72 20 65 78 70 69
             72 65 64
      009090 00                    3334 	.db 0x00
                                   3335 	.area CSEG    (CODE)
                                   3336 	.area CONST   (CODE)
      009091                       3337 ___str_5:
      009091 20                    3338 	.ascii " "
      009092 0A                    3339 	.db 0x0a
      009093 20 42 61 63 6B 6F 66  3340 	.ascii " Backoff timer started "
             66 20 74 69 6D 65 72
             20 73 74 61 72 74 65
             64 20
      0090AA 0A                    3341 	.db 0x0a
      0090AB 00                    3342 	.db 0x00
                                   3343 	.area CSEG    (CODE)
                                   3344 	.area CONST   (CODE)
      0090AC                       3345 ___str_6:
      0090AC 20 41 43 4B 20 74 69  3346 	.ascii " ACK timer started"
             6D 65 72 20 73 74 61
             72 74 65 64
      0090BE 00                    3347 	.db 0x00
                                   3348 	.area CSEG    (CODE)
                                   3349 	.area XINIT   (CODE)
                                   3350 	.area CABS    (ABS,CODE)
