                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.9.3 #11354 (MINGW32)
                                      4 ;--------------------------------------------------------
                                      5 	.module main
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _main
                                     12 	.globl __sdcc_external_startup
                                     13 	.globl _GOLDSEQUENCE_Init
                                     14 	.globl _UART_Proc_PortInit
                                     15 	.globl _uart0_tx
                                     16 	.globl _uart0_rx
                                     17 	.globl _UBLOX_GPS_SendCommand_WaitACK
                                     18 	.globl _UBLOX_GPS_PortInit
                                     19 	.globl _delay_ms
                                     20 	.globl _axradio_setup_pincfg2
                                     21 	.globl _axradio_setup_pincfg1
                                     22 	.globl _axradio_transmit
                                     23 	.globl _axradio_set_default_remote_address
                                     24 	.globl _axradio_set_local_address
                                     25 	.globl _axradio_set_mode
                                     26 	.globl _axradio_init
                                     27 	.globl _dbglink_writehex16
                                     28 	.globl _dbglink_writestr
                                     29 	.globl _dbglink_init
                                     30 	.globl _memcpy
                                     31 	.globl _wtimer_runcallbacks
                                     32 	.globl _wtimer_idle
                                     33 	.globl _wtimer_init
                                     34 	.globl _wtimer1_setconfig
                                     35 	.globl _wtimer0_setconfig
                                     36 	.globl _flash_apply_calibration
                                     37 	.globl _ax5043_commsleepexit
                                     38 	.globl _PORTC_7
                                     39 	.globl _PORTC_6
                                     40 	.globl _PORTC_5
                                     41 	.globl _PORTC_4
                                     42 	.globl _PORTC_3
                                     43 	.globl _PORTC_2
                                     44 	.globl _PORTC_1
                                     45 	.globl _PORTC_0
                                     46 	.globl _PORTB_7
                                     47 	.globl _PORTB_6
                                     48 	.globl _PORTB_5
                                     49 	.globl _PORTB_4
                                     50 	.globl _PORTB_3
                                     51 	.globl _PORTB_2
                                     52 	.globl _PORTB_1
                                     53 	.globl _PORTB_0
                                     54 	.globl _PORTA_7
                                     55 	.globl _PORTA_6
                                     56 	.globl _PORTA_5
                                     57 	.globl _PORTA_4
                                     58 	.globl _PORTA_3
                                     59 	.globl _PORTA_2
                                     60 	.globl _PORTA_1
                                     61 	.globl _PORTA_0
                                     62 	.globl _PINC_7
                                     63 	.globl _PINC_6
                                     64 	.globl _PINC_5
                                     65 	.globl _PINC_4
                                     66 	.globl _PINC_3
                                     67 	.globl _PINC_2
                                     68 	.globl _PINC_1
                                     69 	.globl _PINC_0
                                     70 	.globl _PINB_7
                                     71 	.globl _PINB_6
                                     72 	.globl _PINB_5
                                     73 	.globl _PINB_4
                                     74 	.globl _PINB_3
                                     75 	.globl _PINB_2
                                     76 	.globl _PINB_1
                                     77 	.globl _PINB_0
                                     78 	.globl _PINA_7
                                     79 	.globl _PINA_6
                                     80 	.globl _PINA_5
                                     81 	.globl _PINA_4
                                     82 	.globl _PINA_3
                                     83 	.globl _PINA_2
                                     84 	.globl _PINA_1
                                     85 	.globl _PINA_0
                                     86 	.globl _CY
                                     87 	.globl _AC
                                     88 	.globl _F0
                                     89 	.globl _RS1
                                     90 	.globl _RS0
                                     91 	.globl _OV
                                     92 	.globl _F1
                                     93 	.globl _P
                                     94 	.globl _IP_7
                                     95 	.globl _IP_6
                                     96 	.globl _IP_5
                                     97 	.globl _IP_4
                                     98 	.globl _IP_3
                                     99 	.globl _IP_2
                                    100 	.globl _IP_1
                                    101 	.globl _IP_0
                                    102 	.globl _EA
                                    103 	.globl _IE_7
                                    104 	.globl _IE_6
                                    105 	.globl _IE_5
                                    106 	.globl _IE_4
                                    107 	.globl _IE_3
                                    108 	.globl _IE_2
                                    109 	.globl _IE_1
                                    110 	.globl _IE_0
                                    111 	.globl _EIP_7
                                    112 	.globl _EIP_6
                                    113 	.globl _EIP_5
                                    114 	.globl _EIP_4
                                    115 	.globl _EIP_3
                                    116 	.globl _EIP_2
                                    117 	.globl _EIP_1
                                    118 	.globl _EIP_0
                                    119 	.globl _EIE_7
                                    120 	.globl _EIE_6
                                    121 	.globl _EIE_5
                                    122 	.globl _EIE_4
                                    123 	.globl _EIE_3
                                    124 	.globl _EIE_2
                                    125 	.globl _EIE_1
                                    126 	.globl _EIE_0
                                    127 	.globl _E2IP_7
                                    128 	.globl _E2IP_6
                                    129 	.globl _E2IP_5
                                    130 	.globl _E2IP_4
                                    131 	.globl _E2IP_3
                                    132 	.globl _E2IP_2
                                    133 	.globl _E2IP_1
                                    134 	.globl _E2IP_0
                                    135 	.globl _E2IE_7
                                    136 	.globl _E2IE_6
                                    137 	.globl _E2IE_5
                                    138 	.globl _E2IE_4
                                    139 	.globl _E2IE_3
                                    140 	.globl _E2IE_2
                                    141 	.globl _E2IE_1
                                    142 	.globl _E2IE_0
                                    143 	.globl _B_7
                                    144 	.globl _B_6
                                    145 	.globl _B_5
                                    146 	.globl _B_4
                                    147 	.globl _B_3
                                    148 	.globl _B_2
                                    149 	.globl _B_1
                                    150 	.globl _B_0
                                    151 	.globl _ACC_7
                                    152 	.globl _ACC_6
                                    153 	.globl _ACC_5
                                    154 	.globl _ACC_4
                                    155 	.globl _ACC_3
                                    156 	.globl _ACC_2
                                    157 	.globl _ACC_1
                                    158 	.globl _ACC_0
                                    159 	.globl _WTSTAT
                                    160 	.globl _WTIRQEN
                                    161 	.globl _WTEVTD
                                    162 	.globl _WTEVTD1
                                    163 	.globl _WTEVTD0
                                    164 	.globl _WTEVTC
                                    165 	.globl _WTEVTC1
                                    166 	.globl _WTEVTC0
                                    167 	.globl _WTEVTB
                                    168 	.globl _WTEVTB1
                                    169 	.globl _WTEVTB0
                                    170 	.globl _WTEVTA
                                    171 	.globl _WTEVTA1
                                    172 	.globl _WTEVTA0
                                    173 	.globl _WTCNTR1
                                    174 	.globl _WTCNTB
                                    175 	.globl _WTCNTB1
                                    176 	.globl _WTCNTB0
                                    177 	.globl _WTCNTA
                                    178 	.globl _WTCNTA1
                                    179 	.globl _WTCNTA0
                                    180 	.globl _WTCFGB
                                    181 	.globl _WTCFGA
                                    182 	.globl _WDTRESET
                                    183 	.globl _WDTCFG
                                    184 	.globl _U1STATUS
                                    185 	.globl _U1SHREG
                                    186 	.globl _U1MODE
                                    187 	.globl _U1CTRL
                                    188 	.globl _U0STATUS
                                    189 	.globl _U0SHREG
                                    190 	.globl _U0MODE
                                    191 	.globl _U0CTRL
                                    192 	.globl _T2STATUS
                                    193 	.globl _T2PERIOD
                                    194 	.globl _T2PERIOD1
                                    195 	.globl _T2PERIOD0
                                    196 	.globl _T2MODE
                                    197 	.globl _T2CNT
                                    198 	.globl _T2CNT1
                                    199 	.globl _T2CNT0
                                    200 	.globl _T2CLKSRC
                                    201 	.globl _T1STATUS
                                    202 	.globl _T1PERIOD
                                    203 	.globl _T1PERIOD1
                                    204 	.globl _T1PERIOD0
                                    205 	.globl _T1MODE
                                    206 	.globl _T1CNT
                                    207 	.globl _T1CNT1
                                    208 	.globl _T1CNT0
                                    209 	.globl _T1CLKSRC
                                    210 	.globl _T0STATUS
                                    211 	.globl _T0PERIOD
                                    212 	.globl _T0PERIOD1
                                    213 	.globl _T0PERIOD0
                                    214 	.globl _T0MODE
                                    215 	.globl _T0CNT
                                    216 	.globl _T0CNT1
                                    217 	.globl _T0CNT0
                                    218 	.globl _T0CLKSRC
                                    219 	.globl _SPSTATUS
                                    220 	.globl _SPSHREG
                                    221 	.globl _SPMODE
                                    222 	.globl _SPCLKSRC
                                    223 	.globl _RADIOSTAT
                                    224 	.globl _RADIOSTAT1
                                    225 	.globl _RADIOSTAT0
                                    226 	.globl _RADIODATA
                                    227 	.globl _RADIODATA3
                                    228 	.globl _RADIODATA2
                                    229 	.globl _RADIODATA1
                                    230 	.globl _RADIODATA0
                                    231 	.globl _RADIOADDR
                                    232 	.globl _RADIOADDR1
                                    233 	.globl _RADIOADDR0
                                    234 	.globl _RADIOACC
                                    235 	.globl _OC1STATUS
                                    236 	.globl _OC1PIN
                                    237 	.globl _OC1MODE
                                    238 	.globl _OC1COMP
                                    239 	.globl _OC1COMP1
                                    240 	.globl _OC1COMP0
                                    241 	.globl _OC0STATUS
                                    242 	.globl _OC0PIN
                                    243 	.globl _OC0MODE
                                    244 	.globl _OC0COMP
                                    245 	.globl _OC0COMP1
                                    246 	.globl _OC0COMP0
                                    247 	.globl _NVSTATUS
                                    248 	.globl _NVKEY
                                    249 	.globl _NVDATA
                                    250 	.globl _NVDATA1
                                    251 	.globl _NVDATA0
                                    252 	.globl _NVADDR
                                    253 	.globl _NVADDR1
                                    254 	.globl _NVADDR0
                                    255 	.globl _IC1STATUS
                                    256 	.globl _IC1MODE
                                    257 	.globl _IC1CAPT
                                    258 	.globl _IC1CAPT1
                                    259 	.globl _IC1CAPT0
                                    260 	.globl _IC0STATUS
                                    261 	.globl _IC0MODE
                                    262 	.globl _IC0CAPT
                                    263 	.globl _IC0CAPT1
                                    264 	.globl _IC0CAPT0
                                    265 	.globl _PORTR
                                    266 	.globl _PORTC
                                    267 	.globl _PORTB
                                    268 	.globl _PORTA
                                    269 	.globl _PINR
                                    270 	.globl _PINC
                                    271 	.globl _PINB
                                    272 	.globl _PINA
                                    273 	.globl _DIRR
                                    274 	.globl _DIRC
                                    275 	.globl _DIRB
                                    276 	.globl _DIRA
                                    277 	.globl _DBGLNKSTAT
                                    278 	.globl _DBGLNKBUF
                                    279 	.globl _CODECONFIG
                                    280 	.globl _CLKSTAT
                                    281 	.globl _CLKCON
                                    282 	.globl _ANALOGCOMP
                                    283 	.globl _ADCCONV
                                    284 	.globl _ADCCLKSRC
                                    285 	.globl _ADCCH3CONFIG
                                    286 	.globl _ADCCH2CONFIG
                                    287 	.globl _ADCCH1CONFIG
                                    288 	.globl _ADCCH0CONFIG
                                    289 	.globl __XPAGE
                                    290 	.globl _XPAGE
                                    291 	.globl _SP
                                    292 	.globl _PSW
                                    293 	.globl _PCON
                                    294 	.globl _IP
                                    295 	.globl _IE
                                    296 	.globl _EIP
                                    297 	.globl _EIE
                                    298 	.globl _E2IP
                                    299 	.globl _E2IE
                                    300 	.globl _DPS
                                    301 	.globl _DPTR1
                                    302 	.globl _DPTR0
                                    303 	.globl _DPL1
                                    304 	.globl _DPL
                                    305 	.globl _DPH1
                                    306 	.globl _DPH
                                    307 	.globl _B
                                    308 	.globl _ACC
                                    309 	.globl _counter
                                    310 	.globl _Tmr0Flg
                                    311 	.globl _freq
                                    312 	.globl _GPSMessageReadyFlag
                                    313 	.globl _RfStateMachine
                                    314 	.globl _TimerDiscarded
                                    315 	.globl _ChannelChangeActive
                                    316 	.globl _BackOffFlag
                                    317 	.globl _TxPacketLength
                                    318 	.globl _EncryptedData
                                    319 	.globl _AES_KEY
                                    320 	.globl _TxPackageTwin2
                                    321 	.globl _TxPackageTwin1
                                    322 	.globl _TxPackageOrig
                                    323 	.globl _i
                                    324 	.globl _RxBuffer
                                    325 	.globl _BeaconRx
                                    326 	.globl _ACKTimerFlag
                                    327 	.globl _BackOffCounter
                                    328 	.globl _ACKReceivedFlag
                                    329 	.globl _DownLinkIsrFlag
                                    330 	.globl _UpLinkIsrFlag
                                    331 	.globl _AX5043_XTALAMPL
                                    332 	.globl _AX5043_XTALOSC
                                    333 	.globl _AX5043_MODCFGP
                                    334 	.globl _AX5043_POWCTRL1
                                    335 	.globl _AX5043_REF
                                    336 	.globl _AX5043_0xF44
                                    337 	.globl _AX5043_0xF35
                                    338 	.globl _AX5043_0xF34
                                    339 	.globl _AX5043_0xF33
                                    340 	.globl _AX5043_0xF32
                                    341 	.globl _AX5043_0xF31
                                    342 	.globl _AX5043_0xF30
                                    343 	.globl _AX5043_0xF26
                                    344 	.globl _AX5043_0xF23
                                    345 	.globl _AX5043_0xF22
                                    346 	.globl _AX5043_0xF21
                                    347 	.globl _AX5043_0xF1C
                                    348 	.globl _AX5043_0xF18
                                    349 	.globl _AX5043_0xF11
                                    350 	.globl _AX5043_0xF10
                                    351 	.globl _AX5043_0xF0C
                                    352 	.globl _AX5043_0xF00
                                    353 	.globl _AX5043_TIMEGAIN3NB
                                    354 	.globl _AX5043_TIMEGAIN2NB
                                    355 	.globl _AX5043_TIMEGAIN1NB
                                    356 	.globl _AX5043_TIMEGAIN0NB
                                    357 	.globl _AX5043_RXPARAMSETSNB
                                    358 	.globl _AX5043_RXPARAMCURSETNB
                                    359 	.globl _AX5043_PKTMAXLENNB
                                    360 	.globl _AX5043_PKTLENOFFSETNB
                                    361 	.globl _AX5043_PKTLENCFGNB
                                    362 	.globl _AX5043_PKTADDRMASK3NB
                                    363 	.globl _AX5043_PKTADDRMASK2NB
                                    364 	.globl _AX5043_PKTADDRMASK1NB
                                    365 	.globl _AX5043_PKTADDRMASK0NB
                                    366 	.globl _AX5043_PKTADDRCFGNB
                                    367 	.globl _AX5043_PKTADDR3NB
                                    368 	.globl _AX5043_PKTADDR2NB
                                    369 	.globl _AX5043_PKTADDR1NB
                                    370 	.globl _AX5043_PKTADDR0NB
                                    371 	.globl _AX5043_PHASEGAIN3NB
                                    372 	.globl _AX5043_PHASEGAIN2NB
                                    373 	.globl _AX5043_PHASEGAIN1NB
                                    374 	.globl _AX5043_PHASEGAIN0NB
                                    375 	.globl _AX5043_FREQUENCYLEAKNB
                                    376 	.globl _AX5043_FREQUENCYGAIND3NB
                                    377 	.globl _AX5043_FREQUENCYGAIND2NB
                                    378 	.globl _AX5043_FREQUENCYGAIND1NB
                                    379 	.globl _AX5043_FREQUENCYGAIND0NB
                                    380 	.globl _AX5043_FREQUENCYGAINC3NB
                                    381 	.globl _AX5043_FREQUENCYGAINC2NB
                                    382 	.globl _AX5043_FREQUENCYGAINC1NB
                                    383 	.globl _AX5043_FREQUENCYGAINC0NB
                                    384 	.globl _AX5043_FREQUENCYGAINB3NB
                                    385 	.globl _AX5043_FREQUENCYGAINB2NB
                                    386 	.globl _AX5043_FREQUENCYGAINB1NB
                                    387 	.globl _AX5043_FREQUENCYGAINB0NB
                                    388 	.globl _AX5043_FREQUENCYGAINA3NB
                                    389 	.globl _AX5043_FREQUENCYGAINA2NB
                                    390 	.globl _AX5043_FREQUENCYGAINA1NB
                                    391 	.globl _AX5043_FREQUENCYGAINA0NB
                                    392 	.globl _AX5043_FREQDEV13NB
                                    393 	.globl _AX5043_FREQDEV12NB
                                    394 	.globl _AX5043_FREQDEV11NB
                                    395 	.globl _AX5043_FREQDEV10NB
                                    396 	.globl _AX5043_FREQDEV03NB
                                    397 	.globl _AX5043_FREQDEV02NB
                                    398 	.globl _AX5043_FREQDEV01NB
                                    399 	.globl _AX5043_FREQDEV00NB
                                    400 	.globl _AX5043_FOURFSK3NB
                                    401 	.globl _AX5043_FOURFSK2NB
                                    402 	.globl _AX5043_FOURFSK1NB
                                    403 	.globl _AX5043_FOURFSK0NB
                                    404 	.globl _AX5043_DRGAIN3NB
                                    405 	.globl _AX5043_DRGAIN2NB
                                    406 	.globl _AX5043_DRGAIN1NB
                                    407 	.globl _AX5043_DRGAIN0NB
                                    408 	.globl _AX5043_BBOFFSRES3NB
                                    409 	.globl _AX5043_BBOFFSRES2NB
                                    410 	.globl _AX5043_BBOFFSRES1NB
                                    411 	.globl _AX5043_BBOFFSRES0NB
                                    412 	.globl _AX5043_AMPLITUDEGAIN3NB
                                    413 	.globl _AX5043_AMPLITUDEGAIN2NB
                                    414 	.globl _AX5043_AMPLITUDEGAIN1NB
                                    415 	.globl _AX5043_AMPLITUDEGAIN0NB
                                    416 	.globl _AX5043_AGCTARGET3NB
                                    417 	.globl _AX5043_AGCTARGET2NB
                                    418 	.globl _AX5043_AGCTARGET1NB
                                    419 	.globl _AX5043_AGCTARGET0NB
                                    420 	.globl _AX5043_AGCMINMAX3NB
                                    421 	.globl _AX5043_AGCMINMAX2NB
                                    422 	.globl _AX5043_AGCMINMAX1NB
                                    423 	.globl _AX5043_AGCMINMAX0NB
                                    424 	.globl _AX5043_AGCGAIN3NB
                                    425 	.globl _AX5043_AGCGAIN2NB
                                    426 	.globl _AX5043_AGCGAIN1NB
                                    427 	.globl _AX5043_AGCGAIN0NB
                                    428 	.globl _AX5043_AGCAHYST3NB
                                    429 	.globl _AX5043_AGCAHYST2NB
                                    430 	.globl _AX5043_AGCAHYST1NB
                                    431 	.globl _AX5043_AGCAHYST0NB
                                    432 	.globl _AX5043_0xF44NB
                                    433 	.globl _AX5043_0xF35NB
                                    434 	.globl _AX5043_0xF34NB
                                    435 	.globl _AX5043_0xF33NB
                                    436 	.globl _AX5043_0xF32NB
                                    437 	.globl _AX5043_0xF31NB
                                    438 	.globl _AX5043_0xF30NB
                                    439 	.globl _AX5043_0xF26NB
                                    440 	.globl _AX5043_0xF23NB
                                    441 	.globl _AX5043_0xF22NB
                                    442 	.globl _AX5043_0xF21NB
                                    443 	.globl _AX5043_0xF1CNB
                                    444 	.globl _AX5043_0xF18NB
                                    445 	.globl _AX5043_0xF0CNB
                                    446 	.globl _AX5043_0xF00NB
                                    447 	.globl _AX5043_XTALSTATUSNB
                                    448 	.globl _AX5043_XTALOSCNB
                                    449 	.globl _AX5043_XTALCAPNB
                                    450 	.globl _AX5043_XTALAMPLNB
                                    451 	.globl _AX5043_WAKEUPXOEARLYNB
                                    452 	.globl _AX5043_WAKEUPTIMER1NB
                                    453 	.globl _AX5043_WAKEUPTIMER0NB
                                    454 	.globl _AX5043_WAKEUPFREQ1NB
                                    455 	.globl _AX5043_WAKEUPFREQ0NB
                                    456 	.globl _AX5043_WAKEUP1NB
                                    457 	.globl _AX5043_WAKEUP0NB
                                    458 	.globl _AX5043_TXRATE2NB
                                    459 	.globl _AX5043_TXRATE1NB
                                    460 	.globl _AX5043_TXRATE0NB
                                    461 	.globl _AX5043_TXPWRCOEFFE1NB
                                    462 	.globl _AX5043_TXPWRCOEFFE0NB
                                    463 	.globl _AX5043_TXPWRCOEFFD1NB
                                    464 	.globl _AX5043_TXPWRCOEFFD0NB
                                    465 	.globl _AX5043_TXPWRCOEFFC1NB
                                    466 	.globl _AX5043_TXPWRCOEFFC0NB
                                    467 	.globl _AX5043_TXPWRCOEFFB1NB
                                    468 	.globl _AX5043_TXPWRCOEFFB0NB
                                    469 	.globl _AX5043_TXPWRCOEFFA1NB
                                    470 	.globl _AX5043_TXPWRCOEFFA0NB
                                    471 	.globl _AX5043_TRKRFFREQ2NB
                                    472 	.globl _AX5043_TRKRFFREQ1NB
                                    473 	.globl _AX5043_TRKRFFREQ0NB
                                    474 	.globl _AX5043_TRKPHASE1NB
                                    475 	.globl _AX5043_TRKPHASE0NB
                                    476 	.globl _AX5043_TRKFSKDEMOD1NB
                                    477 	.globl _AX5043_TRKFSKDEMOD0NB
                                    478 	.globl _AX5043_TRKFREQ1NB
                                    479 	.globl _AX5043_TRKFREQ0NB
                                    480 	.globl _AX5043_TRKDATARATE2NB
                                    481 	.globl _AX5043_TRKDATARATE1NB
                                    482 	.globl _AX5043_TRKDATARATE0NB
                                    483 	.globl _AX5043_TRKAMPLITUDE1NB
                                    484 	.globl _AX5043_TRKAMPLITUDE0NB
                                    485 	.globl _AX5043_TRKAFSKDEMOD1NB
                                    486 	.globl _AX5043_TRKAFSKDEMOD0NB
                                    487 	.globl _AX5043_TMGTXSETTLENB
                                    488 	.globl _AX5043_TMGTXBOOSTNB
                                    489 	.globl _AX5043_TMGRXSETTLENB
                                    490 	.globl _AX5043_TMGRXRSSINB
                                    491 	.globl _AX5043_TMGRXPREAMBLE3NB
                                    492 	.globl _AX5043_TMGRXPREAMBLE2NB
                                    493 	.globl _AX5043_TMGRXPREAMBLE1NB
                                    494 	.globl _AX5043_TMGRXOFFSACQNB
                                    495 	.globl _AX5043_TMGRXCOARSEAGCNB
                                    496 	.globl _AX5043_TMGRXBOOSTNB
                                    497 	.globl _AX5043_TMGRXAGCNB
                                    498 	.globl _AX5043_TIMER2NB
                                    499 	.globl _AX5043_TIMER1NB
                                    500 	.globl _AX5043_TIMER0NB
                                    501 	.globl _AX5043_SILICONREVISIONNB
                                    502 	.globl _AX5043_SCRATCHNB
                                    503 	.globl _AX5043_RXDATARATE2NB
                                    504 	.globl _AX5043_RXDATARATE1NB
                                    505 	.globl _AX5043_RXDATARATE0NB
                                    506 	.globl _AX5043_RSSIREFERENCENB
                                    507 	.globl _AX5043_RSSIABSTHRNB
                                    508 	.globl _AX5043_RSSINB
                                    509 	.globl _AX5043_REFNB
                                    510 	.globl _AX5043_RADIOSTATENB
                                    511 	.globl _AX5043_RADIOEVENTREQ1NB
                                    512 	.globl _AX5043_RADIOEVENTREQ0NB
                                    513 	.globl _AX5043_RADIOEVENTMASK1NB
                                    514 	.globl _AX5043_RADIOEVENTMASK0NB
                                    515 	.globl _AX5043_PWRMODENB
                                    516 	.globl _AX5043_PWRAMPNB
                                    517 	.globl _AX5043_POWSTICKYSTATNB
                                    518 	.globl _AX5043_POWSTATNB
                                    519 	.globl _AX5043_POWIRQMASKNB
                                    520 	.globl _AX5043_POWCTRL1NB
                                    521 	.globl _AX5043_PLLVCOIRNB
                                    522 	.globl _AX5043_PLLVCOINB
                                    523 	.globl _AX5043_PLLVCODIVNB
                                    524 	.globl _AX5043_PLLRNGCLKNB
                                    525 	.globl _AX5043_PLLRANGINGBNB
                                    526 	.globl _AX5043_PLLRANGINGANB
                                    527 	.globl _AX5043_PLLLOOPBOOSTNB
                                    528 	.globl _AX5043_PLLLOOPNB
                                    529 	.globl _AX5043_PLLLOCKDETNB
                                    530 	.globl _AX5043_PLLCPIBOOSTNB
                                    531 	.globl _AX5043_PLLCPINB
                                    532 	.globl _AX5043_PKTSTOREFLAGSNB
                                    533 	.globl _AX5043_PKTMISCFLAGSNB
                                    534 	.globl _AX5043_PKTCHUNKSIZENB
                                    535 	.globl _AX5043_PKTACCEPTFLAGSNB
                                    536 	.globl _AX5043_PINSTATENB
                                    537 	.globl _AX5043_PINFUNCSYSCLKNB
                                    538 	.globl _AX5043_PINFUNCPWRAMPNB
                                    539 	.globl _AX5043_PINFUNCIRQNB
                                    540 	.globl _AX5043_PINFUNCDCLKNB
                                    541 	.globl _AX5043_PINFUNCDATANB
                                    542 	.globl _AX5043_PINFUNCANTSELNB
                                    543 	.globl _AX5043_MODULATIONNB
                                    544 	.globl _AX5043_MODCFGPNB
                                    545 	.globl _AX5043_MODCFGFNB
                                    546 	.globl _AX5043_MODCFGANB
                                    547 	.globl _AX5043_MAXRFOFFSET2NB
                                    548 	.globl _AX5043_MAXRFOFFSET1NB
                                    549 	.globl _AX5043_MAXRFOFFSET0NB
                                    550 	.globl _AX5043_MAXDROFFSET2NB
                                    551 	.globl _AX5043_MAXDROFFSET1NB
                                    552 	.globl _AX5043_MAXDROFFSET0NB
                                    553 	.globl _AX5043_MATCH1PAT1NB
                                    554 	.globl _AX5043_MATCH1PAT0NB
                                    555 	.globl _AX5043_MATCH1MINNB
                                    556 	.globl _AX5043_MATCH1MAXNB
                                    557 	.globl _AX5043_MATCH1LENNB
                                    558 	.globl _AX5043_MATCH0PAT3NB
                                    559 	.globl _AX5043_MATCH0PAT2NB
                                    560 	.globl _AX5043_MATCH0PAT1NB
                                    561 	.globl _AX5043_MATCH0PAT0NB
                                    562 	.globl _AX5043_MATCH0MINNB
                                    563 	.globl _AX5043_MATCH0MAXNB
                                    564 	.globl _AX5043_MATCH0LENNB
                                    565 	.globl _AX5043_LPOSCSTATUSNB
                                    566 	.globl _AX5043_LPOSCREF1NB
                                    567 	.globl _AX5043_LPOSCREF0NB
                                    568 	.globl _AX5043_LPOSCPER1NB
                                    569 	.globl _AX5043_LPOSCPER0NB
                                    570 	.globl _AX5043_LPOSCKFILT1NB
                                    571 	.globl _AX5043_LPOSCKFILT0NB
                                    572 	.globl _AX5043_LPOSCFREQ1NB
                                    573 	.globl _AX5043_LPOSCFREQ0NB
                                    574 	.globl _AX5043_LPOSCCONFIGNB
                                    575 	.globl _AX5043_IRQREQUEST1NB
                                    576 	.globl _AX5043_IRQREQUEST0NB
                                    577 	.globl _AX5043_IRQMASK1NB
                                    578 	.globl _AX5043_IRQMASK0NB
                                    579 	.globl _AX5043_IRQINVERSION1NB
                                    580 	.globl _AX5043_IRQINVERSION0NB
                                    581 	.globl _AX5043_IFFREQ1NB
                                    582 	.globl _AX5043_IFFREQ0NB
                                    583 	.globl _AX5043_GPADCPERIODNB
                                    584 	.globl _AX5043_GPADCCTRLNB
                                    585 	.globl _AX5043_GPADC13VALUE1NB
                                    586 	.globl _AX5043_GPADC13VALUE0NB
                                    587 	.globl _AX5043_FSKDMIN1NB
                                    588 	.globl _AX5043_FSKDMIN0NB
                                    589 	.globl _AX5043_FSKDMAX1NB
                                    590 	.globl _AX5043_FSKDMAX0NB
                                    591 	.globl _AX5043_FSKDEV2NB
                                    592 	.globl _AX5043_FSKDEV1NB
                                    593 	.globl _AX5043_FSKDEV0NB
                                    594 	.globl _AX5043_FREQB3NB
                                    595 	.globl _AX5043_FREQB2NB
                                    596 	.globl _AX5043_FREQB1NB
                                    597 	.globl _AX5043_FREQB0NB
                                    598 	.globl _AX5043_FREQA3NB
                                    599 	.globl _AX5043_FREQA2NB
                                    600 	.globl _AX5043_FREQA1NB
                                    601 	.globl _AX5043_FREQA0NB
                                    602 	.globl _AX5043_FRAMINGNB
                                    603 	.globl _AX5043_FIFOTHRESH1NB
                                    604 	.globl _AX5043_FIFOTHRESH0NB
                                    605 	.globl _AX5043_FIFOSTATNB
                                    606 	.globl _AX5043_FIFOFREE1NB
                                    607 	.globl _AX5043_FIFOFREE0NB
                                    608 	.globl _AX5043_FIFODATANB
                                    609 	.globl _AX5043_FIFOCOUNT1NB
                                    610 	.globl _AX5043_FIFOCOUNT0NB
                                    611 	.globl _AX5043_FECSYNCNB
                                    612 	.globl _AX5043_FECSTATUSNB
                                    613 	.globl _AX5043_FECNB
                                    614 	.globl _AX5043_ENCODINGNB
                                    615 	.globl _AX5043_DIVERSITYNB
                                    616 	.globl _AX5043_DECIMATIONNB
                                    617 	.globl _AX5043_DACVALUE1NB
                                    618 	.globl _AX5043_DACVALUE0NB
                                    619 	.globl _AX5043_DACCONFIGNB
                                    620 	.globl _AX5043_CRCINIT3NB
                                    621 	.globl _AX5043_CRCINIT2NB
                                    622 	.globl _AX5043_CRCINIT1NB
                                    623 	.globl _AX5043_CRCINIT0NB
                                    624 	.globl _AX5043_BGNDRSSITHRNB
                                    625 	.globl _AX5043_BGNDRSSIGAINNB
                                    626 	.globl _AX5043_BGNDRSSINB
                                    627 	.globl _AX5043_BBTUNENB
                                    628 	.globl _AX5043_BBOFFSCAPNB
                                    629 	.globl _AX5043_AMPLFILTERNB
                                    630 	.globl _AX5043_AGCCOUNTERNB
                                    631 	.globl _AX5043_AFSKSPACE1NB
                                    632 	.globl _AX5043_AFSKSPACE0NB
                                    633 	.globl _AX5043_AFSKMARK1NB
                                    634 	.globl _AX5043_AFSKMARK0NB
                                    635 	.globl _AX5043_AFSKCTRLNB
                                    636 	.globl _AX5043_TIMEGAIN3
                                    637 	.globl _AX5043_TIMEGAIN2
                                    638 	.globl _AX5043_TIMEGAIN1
                                    639 	.globl _AX5043_TIMEGAIN0
                                    640 	.globl _AX5043_RXPARAMSETS
                                    641 	.globl _AX5043_RXPARAMCURSET
                                    642 	.globl _AX5043_PKTMAXLEN
                                    643 	.globl _AX5043_PKTLENOFFSET
                                    644 	.globl _AX5043_PKTLENCFG
                                    645 	.globl _AX5043_PKTADDRMASK3
                                    646 	.globl _AX5043_PKTADDRMASK2
                                    647 	.globl _AX5043_PKTADDRMASK1
                                    648 	.globl _AX5043_PKTADDRMASK0
                                    649 	.globl _AX5043_PKTADDRCFG
                                    650 	.globl _AX5043_PKTADDR3
                                    651 	.globl _AX5043_PKTADDR2
                                    652 	.globl _AX5043_PKTADDR1
                                    653 	.globl _AX5043_PKTADDR0
                                    654 	.globl _AX5043_PHASEGAIN3
                                    655 	.globl _AX5043_PHASEGAIN2
                                    656 	.globl _AX5043_PHASEGAIN1
                                    657 	.globl _AX5043_PHASEGAIN0
                                    658 	.globl _AX5043_FREQUENCYLEAK
                                    659 	.globl _AX5043_FREQUENCYGAIND3
                                    660 	.globl _AX5043_FREQUENCYGAIND2
                                    661 	.globl _AX5043_FREQUENCYGAIND1
                                    662 	.globl _AX5043_FREQUENCYGAIND0
                                    663 	.globl _AX5043_FREQUENCYGAINC3
                                    664 	.globl _AX5043_FREQUENCYGAINC2
                                    665 	.globl _AX5043_FREQUENCYGAINC1
                                    666 	.globl _AX5043_FREQUENCYGAINC0
                                    667 	.globl _AX5043_FREQUENCYGAINB3
                                    668 	.globl _AX5043_FREQUENCYGAINB2
                                    669 	.globl _AX5043_FREQUENCYGAINB1
                                    670 	.globl _AX5043_FREQUENCYGAINB0
                                    671 	.globl _AX5043_FREQUENCYGAINA3
                                    672 	.globl _AX5043_FREQUENCYGAINA2
                                    673 	.globl _AX5043_FREQUENCYGAINA1
                                    674 	.globl _AX5043_FREQUENCYGAINA0
                                    675 	.globl _AX5043_FREQDEV13
                                    676 	.globl _AX5043_FREQDEV12
                                    677 	.globl _AX5043_FREQDEV11
                                    678 	.globl _AX5043_FREQDEV10
                                    679 	.globl _AX5043_FREQDEV03
                                    680 	.globl _AX5043_FREQDEV02
                                    681 	.globl _AX5043_FREQDEV01
                                    682 	.globl _AX5043_FREQDEV00
                                    683 	.globl _AX5043_FOURFSK3
                                    684 	.globl _AX5043_FOURFSK2
                                    685 	.globl _AX5043_FOURFSK1
                                    686 	.globl _AX5043_FOURFSK0
                                    687 	.globl _AX5043_DRGAIN3
                                    688 	.globl _AX5043_DRGAIN2
                                    689 	.globl _AX5043_DRGAIN1
                                    690 	.globl _AX5043_DRGAIN0
                                    691 	.globl _AX5043_BBOFFSRES3
                                    692 	.globl _AX5043_BBOFFSRES2
                                    693 	.globl _AX5043_BBOFFSRES1
                                    694 	.globl _AX5043_BBOFFSRES0
                                    695 	.globl _AX5043_AMPLITUDEGAIN3
                                    696 	.globl _AX5043_AMPLITUDEGAIN2
                                    697 	.globl _AX5043_AMPLITUDEGAIN1
                                    698 	.globl _AX5043_AMPLITUDEGAIN0
                                    699 	.globl _AX5043_AGCTARGET3
                                    700 	.globl _AX5043_AGCTARGET2
                                    701 	.globl _AX5043_AGCTARGET1
                                    702 	.globl _AX5043_AGCTARGET0
                                    703 	.globl _AX5043_AGCMINMAX3
                                    704 	.globl _AX5043_AGCMINMAX2
                                    705 	.globl _AX5043_AGCMINMAX1
                                    706 	.globl _AX5043_AGCMINMAX0
                                    707 	.globl _AX5043_AGCGAIN3
                                    708 	.globl _AX5043_AGCGAIN2
                                    709 	.globl _AX5043_AGCGAIN1
                                    710 	.globl _AX5043_AGCGAIN0
                                    711 	.globl _AX5043_AGCAHYST3
                                    712 	.globl _AX5043_AGCAHYST2
                                    713 	.globl _AX5043_AGCAHYST1
                                    714 	.globl _AX5043_AGCAHYST0
                                    715 	.globl _AX5043_XTALSTATUS
                                    716 	.globl _AX5043_XTALCAP
                                    717 	.globl _AX5043_WAKEUPXOEARLY
                                    718 	.globl _AX5043_WAKEUPTIMER1
                                    719 	.globl _AX5043_WAKEUPTIMER0
                                    720 	.globl _AX5043_WAKEUPFREQ1
                                    721 	.globl _AX5043_WAKEUPFREQ0
                                    722 	.globl _AX5043_WAKEUP1
                                    723 	.globl _AX5043_WAKEUP0
                                    724 	.globl _AX5043_TXRATE2
                                    725 	.globl _AX5043_TXRATE1
                                    726 	.globl _AX5043_TXRATE0
                                    727 	.globl _AX5043_TXPWRCOEFFE1
                                    728 	.globl _AX5043_TXPWRCOEFFE0
                                    729 	.globl _AX5043_TXPWRCOEFFD1
                                    730 	.globl _AX5043_TXPWRCOEFFD0
                                    731 	.globl _AX5043_TXPWRCOEFFC1
                                    732 	.globl _AX5043_TXPWRCOEFFC0
                                    733 	.globl _AX5043_TXPWRCOEFFB1
                                    734 	.globl _AX5043_TXPWRCOEFFB0
                                    735 	.globl _AX5043_TXPWRCOEFFA1
                                    736 	.globl _AX5043_TXPWRCOEFFA0
                                    737 	.globl _AX5043_TRKRFFREQ2
                                    738 	.globl _AX5043_TRKRFFREQ1
                                    739 	.globl _AX5043_TRKRFFREQ0
                                    740 	.globl _AX5043_TRKPHASE1
                                    741 	.globl _AX5043_TRKPHASE0
                                    742 	.globl _AX5043_TRKFSKDEMOD1
                                    743 	.globl _AX5043_TRKFSKDEMOD0
                                    744 	.globl _AX5043_TRKFREQ1
                                    745 	.globl _AX5043_TRKFREQ0
                                    746 	.globl _AX5043_TRKDATARATE2
                                    747 	.globl _AX5043_TRKDATARATE1
                                    748 	.globl _AX5043_TRKDATARATE0
                                    749 	.globl _AX5043_TRKAMPLITUDE1
                                    750 	.globl _AX5043_TRKAMPLITUDE0
                                    751 	.globl _AX5043_TRKAFSKDEMOD1
                                    752 	.globl _AX5043_TRKAFSKDEMOD0
                                    753 	.globl _AX5043_TMGTXSETTLE
                                    754 	.globl _AX5043_TMGTXBOOST
                                    755 	.globl _AX5043_TMGRXSETTLE
                                    756 	.globl _AX5043_TMGRXRSSI
                                    757 	.globl _AX5043_TMGRXPREAMBLE3
                                    758 	.globl _AX5043_TMGRXPREAMBLE2
                                    759 	.globl _AX5043_TMGRXPREAMBLE1
                                    760 	.globl _AX5043_TMGRXOFFSACQ
                                    761 	.globl _AX5043_TMGRXCOARSEAGC
                                    762 	.globl _AX5043_TMGRXBOOST
                                    763 	.globl _AX5043_TMGRXAGC
                                    764 	.globl _AX5043_TIMER2
                                    765 	.globl _AX5043_TIMER1
                                    766 	.globl _AX5043_TIMER0
                                    767 	.globl _AX5043_SILICONREVISION
                                    768 	.globl _AX5043_SCRATCH
                                    769 	.globl _AX5043_RXDATARATE2
                                    770 	.globl _AX5043_RXDATARATE1
                                    771 	.globl _AX5043_RXDATARATE0
                                    772 	.globl _AX5043_RSSIREFERENCE
                                    773 	.globl _AX5043_RSSIABSTHR
                                    774 	.globl _AX5043_RSSI
                                    775 	.globl _AX5043_RADIOSTATE
                                    776 	.globl _AX5043_RADIOEVENTREQ1
                                    777 	.globl _AX5043_RADIOEVENTREQ0
                                    778 	.globl _AX5043_RADIOEVENTMASK1
                                    779 	.globl _AX5043_RADIOEVENTMASK0
                                    780 	.globl _AX5043_PWRMODE
                                    781 	.globl _AX5043_PWRAMP
                                    782 	.globl _AX5043_POWSTICKYSTAT
                                    783 	.globl _AX5043_POWSTAT
                                    784 	.globl _AX5043_POWIRQMASK
                                    785 	.globl _AX5043_PLLVCOIR
                                    786 	.globl _AX5043_PLLVCOI
                                    787 	.globl _AX5043_PLLVCODIV
                                    788 	.globl _AX5043_PLLRNGCLK
                                    789 	.globl _AX5043_PLLRANGINGB
                                    790 	.globl _AX5043_PLLRANGINGA
                                    791 	.globl _AX5043_PLLLOOPBOOST
                                    792 	.globl _AX5043_PLLLOOP
                                    793 	.globl _AX5043_PLLLOCKDET
                                    794 	.globl _AX5043_PLLCPIBOOST
                                    795 	.globl _AX5043_PLLCPI
                                    796 	.globl _AX5043_PKTSTOREFLAGS
                                    797 	.globl _AX5043_PKTMISCFLAGS
                                    798 	.globl _AX5043_PKTCHUNKSIZE
                                    799 	.globl _AX5043_PKTACCEPTFLAGS
                                    800 	.globl _AX5043_PINSTATE
                                    801 	.globl _AX5043_PINFUNCSYSCLK
                                    802 	.globl _AX5043_PINFUNCPWRAMP
                                    803 	.globl _AX5043_PINFUNCIRQ
                                    804 	.globl _AX5043_PINFUNCDCLK
                                    805 	.globl _AX5043_PINFUNCDATA
                                    806 	.globl _AX5043_PINFUNCANTSEL
                                    807 	.globl _AX5043_MODULATION
                                    808 	.globl _AX5043_MODCFGF
                                    809 	.globl _AX5043_MODCFGA
                                    810 	.globl _AX5043_MAXRFOFFSET2
                                    811 	.globl _AX5043_MAXRFOFFSET1
                                    812 	.globl _AX5043_MAXRFOFFSET0
                                    813 	.globl _AX5043_MAXDROFFSET2
                                    814 	.globl _AX5043_MAXDROFFSET1
                                    815 	.globl _AX5043_MAXDROFFSET0
                                    816 	.globl _AX5043_MATCH1PAT1
                                    817 	.globl _AX5043_MATCH1PAT0
                                    818 	.globl _AX5043_MATCH1MIN
                                    819 	.globl _AX5043_MATCH1MAX
                                    820 	.globl _AX5043_MATCH1LEN
                                    821 	.globl _AX5043_MATCH0PAT3
                                    822 	.globl _AX5043_MATCH0PAT2
                                    823 	.globl _AX5043_MATCH0PAT1
                                    824 	.globl _AX5043_MATCH0PAT0
                                    825 	.globl _AX5043_MATCH0MIN
                                    826 	.globl _AX5043_MATCH0MAX
                                    827 	.globl _AX5043_MATCH0LEN
                                    828 	.globl _AX5043_LPOSCSTATUS
                                    829 	.globl _AX5043_LPOSCREF1
                                    830 	.globl _AX5043_LPOSCREF0
                                    831 	.globl _AX5043_LPOSCPER1
                                    832 	.globl _AX5043_LPOSCPER0
                                    833 	.globl _AX5043_LPOSCKFILT1
                                    834 	.globl _AX5043_LPOSCKFILT0
                                    835 	.globl _AX5043_LPOSCFREQ1
                                    836 	.globl _AX5043_LPOSCFREQ0
                                    837 	.globl _AX5043_LPOSCCONFIG
                                    838 	.globl _AX5043_IRQREQUEST1
                                    839 	.globl _AX5043_IRQREQUEST0
                                    840 	.globl _AX5043_IRQMASK1
                                    841 	.globl _AX5043_IRQMASK0
                                    842 	.globl _AX5043_IRQINVERSION1
                                    843 	.globl _AX5043_IRQINVERSION0
                                    844 	.globl _AX5043_IFFREQ1
                                    845 	.globl _AX5043_IFFREQ0
                                    846 	.globl _AX5043_GPADCPERIOD
                                    847 	.globl _AX5043_GPADCCTRL
                                    848 	.globl _AX5043_GPADC13VALUE1
                                    849 	.globl _AX5043_GPADC13VALUE0
                                    850 	.globl _AX5043_FSKDMIN1
                                    851 	.globl _AX5043_FSKDMIN0
                                    852 	.globl _AX5043_FSKDMAX1
                                    853 	.globl _AX5043_FSKDMAX0
                                    854 	.globl _AX5043_FSKDEV2
                                    855 	.globl _AX5043_FSKDEV1
                                    856 	.globl _AX5043_FSKDEV0
                                    857 	.globl _AX5043_FREQB3
                                    858 	.globl _AX5043_FREQB2
                                    859 	.globl _AX5043_FREQB1
                                    860 	.globl _AX5043_FREQB0
                                    861 	.globl _AX5043_FREQA3
                                    862 	.globl _AX5043_FREQA2
                                    863 	.globl _AX5043_FREQA1
                                    864 	.globl _AX5043_FREQA0
                                    865 	.globl _AX5043_FRAMING
                                    866 	.globl _AX5043_FIFOTHRESH1
                                    867 	.globl _AX5043_FIFOTHRESH0
                                    868 	.globl _AX5043_FIFOSTAT
                                    869 	.globl _AX5043_FIFOFREE1
                                    870 	.globl _AX5043_FIFOFREE0
                                    871 	.globl _AX5043_FIFODATA
                                    872 	.globl _AX5043_FIFOCOUNT1
                                    873 	.globl _AX5043_FIFOCOUNT0
                                    874 	.globl _AX5043_FECSYNC
                                    875 	.globl _AX5043_FECSTATUS
                                    876 	.globl _AX5043_FEC
                                    877 	.globl _AX5043_ENCODING
                                    878 	.globl _AX5043_DIVERSITY
                                    879 	.globl _AX5043_DECIMATION
                                    880 	.globl _AX5043_DACVALUE1
                                    881 	.globl _AX5043_DACVALUE0
                                    882 	.globl _AX5043_DACCONFIG
                                    883 	.globl _AX5043_CRCINIT3
                                    884 	.globl _AX5043_CRCINIT2
                                    885 	.globl _AX5043_CRCINIT1
                                    886 	.globl _AX5043_CRCINIT0
                                    887 	.globl _AX5043_BGNDRSSITHR
                                    888 	.globl _AX5043_BGNDRSSIGAIN
                                    889 	.globl _AX5043_BGNDRSSI
                                    890 	.globl _AX5043_BBTUNE
                                    891 	.globl _AX5043_BBOFFSCAP
                                    892 	.globl _AX5043_AMPLFILTER
                                    893 	.globl _AX5043_AGCCOUNTER
                                    894 	.globl _AX5043_AFSKSPACE1
                                    895 	.globl _AX5043_AFSKSPACE0
                                    896 	.globl _AX5043_AFSKMARK1
                                    897 	.globl _AX5043_AFSKMARK0
                                    898 	.globl _AX5043_AFSKCTRL
                                    899 	.globl _XWTSTAT
                                    900 	.globl _XWTIRQEN
                                    901 	.globl _XWTEVTD
                                    902 	.globl _XWTEVTD1
                                    903 	.globl _XWTEVTD0
                                    904 	.globl _XWTEVTC
                                    905 	.globl _XWTEVTC1
                                    906 	.globl _XWTEVTC0
                                    907 	.globl _XWTEVTB
                                    908 	.globl _XWTEVTB1
                                    909 	.globl _XWTEVTB0
                                    910 	.globl _XWTEVTA
                                    911 	.globl _XWTEVTA1
                                    912 	.globl _XWTEVTA0
                                    913 	.globl _XWTCNTR1
                                    914 	.globl _XWTCNTB
                                    915 	.globl _XWTCNTB1
                                    916 	.globl _XWTCNTB0
                                    917 	.globl _XWTCNTA
                                    918 	.globl _XWTCNTA1
                                    919 	.globl _XWTCNTA0
                                    920 	.globl _XWTCFGB
                                    921 	.globl _XWTCFGA
                                    922 	.globl _XWDTRESET
                                    923 	.globl _XWDTCFG
                                    924 	.globl _XU1STATUS
                                    925 	.globl _XU1SHREG
                                    926 	.globl _XU1MODE
                                    927 	.globl _XU1CTRL
                                    928 	.globl _XU0STATUS
                                    929 	.globl _XU0SHREG
                                    930 	.globl _XU0MODE
                                    931 	.globl _XU0CTRL
                                    932 	.globl _XT2STATUS
                                    933 	.globl _XT2PERIOD
                                    934 	.globl _XT2PERIOD1
                                    935 	.globl _XT2PERIOD0
                                    936 	.globl _XT2MODE
                                    937 	.globl _XT2CNT
                                    938 	.globl _XT2CNT1
                                    939 	.globl _XT2CNT0
                                    940 	.globl _XT2CLKSRC
                                    941 	.globl _XT1STATUS
                                    942 	.globl _XT1PERIOD
                                    943 	.globl _XT1PERIOD1
                                    944 	.globl _XT1PERIOD0
                                    945 	.globl _XT1MODE
                                    946 	.globl _XT1CNT
                                    947 	.globl _XT1CNT1
                                    948 	.globl _XT1CNT0
                                    949 	.globl _XT1CLKSRC
                                    950 	.globl _XT0STATUS
                                    951 	.globl _XT0PERIOD
                                    952 	.globl _XT0PERIOD1
                                    953 	.globl _XT0PERIOD0
                                    954 	.globl _XT0MODE
                                    955 	.globl _XT0CNT
                                    956 	.globl _XT0CNT1
                                    957 	.globl _XT0CNT0
                                    958 	.globl _XT0CLKSRC
                                    959 	.globl _XSPSTATUS
                                    960 	.globl _XSPSHREG
                                    961 	.globl _XSPMODE
                                    962 	.globl _XSPCLKSRC
                                    963 	.globl _XRADIOSTAT
                                    964 	.globl _XRADIOSTAT1
                                    965 	.globl _XRADIOSTAT0
                                    966 	.globl _XRADIODATA3
                                    967 	.globl _XRADIODATA2
                                    968 	.globl _XRADIODATA1
                                    969 	.globl _XRADIODATA0
                                    970 	.globl _XRADIOADDR1
                                    971 	.globl _XRADIOADDR0
                                    972 	.globl _XRADIOACC
                                    973 	.globl _XOC1STATUS
                                    974 	.globl _XOC1PIN
                                    975 	.globl _XOC1MODE
                                    976 	.globl _XOC1COMP
                                    977 	.globl _XOC1COMP1
                                    978 	.globl _XOC1COMP0
                                    979 	.globl _XOC0STATUS
                                    980 	.globl _XOC0PIN
                                    981 	.globl _XOC0MODE
                                    982 	.globl _XOC0COMP
                                    983 	.globl _XOC0COMP1
                                    984 	.globl _XOC0COMP0
                                    985 	.globl _XNVSTATUS
                                    986 	.globl _XNVKEY
                                    987 	.globl _XNVDATA
                                    988 	.globl _XNVDATA1
                                    989 	.globl _XNVDATA0
                                    990 	.globl _XNVADDR
                                    991 	.globl _XNVADDR1
                                    992 	.globl _XNVADDR0
                                    993 	.globl _XIC1STATUS
                                    994 	.globl _XIC1MODE
                                    995 	.globl _XIC1CAPT
                                    996 	.globl _XIC1CAPT1
                                    997 	.globl _XIC1CAPT0
                                    998 	.globl _XIC0STATUS
                                    999 	.globl _XIC0MODE
                                   1000 	.globl _XIC0CAPT
                                   1001 	.globl _XIC0CAPT1
                                   1002 	.globl _XIC0CAPT0
                                   1003 	.globl _XPORTR
                                   1004 	.globl _XPORTC
                                   1005 	.globl _XPORTB
                                   1006 	.globl _XPORTA
                                   1007 	.globl _XPINR
                                   1008 	.globl _XPINC
                                   1009 	.globl _XPINB
                                   1010 	.globl _XPINA
                                   1011 	.globl _XDIRR
                                   1012 	.globl _XDIRC
                                   1013 	.globl _XDIRB
                                   1014 	.globl _XDIRA
                                   1015 	.globl _XDBGLNKSTAT
                                   1016 	.globl _XDBGLNKBUF
                                   1017 	.globl _XCODECONFIG
                                   1018 	.globl _XCLKSTAT
                                   1019 	.globl _XCLKCON
                                   1020 	.globl _XANALOGCOMP
                                   1021 	.globl _XADCCONV
                                   1022 	.globl _XADCCLKSRC
                                   1023 	.globl _XADCCH3CONFIG
                                   1024 	.globl _XADCCH2CONFIG
                                   1025 	.globl _XADCCH1CONFIG
                                   1026 	.globl _XADCCH0CONFIG
                                   1027 	.globl _XPCON
                                   1028 	.globl _XIP
                                   1029 	.globl _XIE
                                   1030 	.globl _XDPTR1
                                   1031 	.globl _XDPTR0
                                   1032 	.globl _RNGCLKSRC1
                                   1033 	.globl _RNGCLKSRC0
                                   1034 	.globl _RNGMODE
                                   1035 	.globl _AESOUTADDR
                                   1036 	.globl _AESOUTADDR1
                                   1037 	.globl _AESOUTADDR0
                                   1038 	.globl _AESMODE
                                   1039 	.globl _AESKEYADDR
                                   1040 	.globl _AESKEYADDR1
                                   1041 	.globl _AESKEYADDR0
                                   1042 	.globl _AESINADDR
                                   1043 	.globl _AESINADDR1
                                   1044 	.globl _AESINADDR0
                                   1045 	.globl _AESCURBLOCK
                                   1046 	.globl _AESCONFIG
                                   1047 	.globl _RNGBYTE
                                   1048 	.globl _XTALREADY
                                   1049 	.globl _XTALOSC
                                   1050 	.globl _XTALAMPL
                                   1051 	.globl _SILICONREV
                                   1052 	.globl _SCRATCH3
                                   1053 	.globl _SCRATCH2
                                   1054 	.globl _SCRATCH1
                                   1055 	.globl _SCRATCH0
                                   1056 	.globl _RADIOMUX
                                   1057 	.globl _RADIOFSTATADDR
                                   1058 	.globl _RADIOFSTATADDR1
                                   1059 	.globl _RADIOFSTATADDR0
                                   1060 	.globl _RADIOFDATAADDR
                                   1061 	.globl _RADIOFDATAADDR1
                                   1062 	.globl _RADIOFDATAADDR0
                                   1063 	.globl _OSCRUN
                                   1064 	.globl _OSCREADY
                                   1065 	.globl _OSCFORCERUN
                                   1066 	.globl _OSCCALIB
                                   1067 	.globl _MISCCTRL
                                   1068 	.globl _LPXOSCGM
                                   1069 	.globl _LPOSCREF
                                   1070 	.globl _LPOSCREF1
                                   1071 	.globl _LPOSCREF0
                                   1072 	.globl _LPOSCPER
                                   1073 	.globl _LPOSCPER1
                                   1074 	.globl _LPOSCPER0
                                   1075 	.globl _LPOSCKFILT
                                   1076 	.globl _LPOSCKFILT1
                                   1077 	.globl _LPOSCKFILT0
                                   1078 	.globl _LPOSCFREQ
                                   1079 	.globl _LPOSCFREQ1
                                   1080 	.globl _LPOSCFREQ0
                                   1081 	.globl _LPOSCCONFIG
                                   1082 	.globl _PINSEL
                                   1083 	.globl _PINCHGC
                                   1084 	.globl _PINCHGB
                                   1085 	.globl _PINCHGA
                                   1086 	.globl _PALTRADIO
                                   1087 	.globl _PALTC
                                   1088 	.globl _PALTB
                                   1089 	.globl _PALTA
                                   1090 	.globl _INTCHGC
                                   1091 	.globl _INTCHGB
                                   1092 	.globl _INTCHGA
                                   1093 	.globl _EXTIRQ
                                   1094 	.globl _GPIOENABLE
                                   1095 	.globl _ANALOGA
                                   1096 	.globl _FRCOSCREF
                                   1097 	.globl _FRCOSCREF1
                                   1098 	.globl _FRCOSCREF0
                                   1099 	.globl _FRCOSCPER
                                   1100 	.globl _FRCOSCPER1
                                   1101 	.globl _FRCOSCPER0
                                   1102 	.globl _FRCOSCKFILT
                                   1103 	.globl _FRCOSCKFILT1
                                   1104 	.globl _FRCOSCKFILT0
                                   1105 	.globl _FRCOSCFREQ
                                   1106 	.globl _FRCOSCFREQ1
                                   1107 	.globl _FRCOSCFREQ0
                                   1108 	.globl _FRCOSCCTRL
                                   1109 	.globl _FRCOSCCONFIG
                                   1110 	.globl _DMA1CONFIG
                                   1111 	.globl _DMA1ADDR
                                   1112 	.globl _DMA1ADDR1
                                   1113 	.globl _DMA1ADDR0
                                   1114 	.globl _DMA0CONFIG
                                   1115 	.globl _DMA0ADDR
                                   1116 	.globl _DMA0ADDR1
                                   1117 	.globl _DMA0ADDR0
                                   1118 	.globl _ADCTUNE2
                                   1119 	.globl _ADCTUNE1
                                   1120 	.globl _ADCTUNE0
                                   1121 	.globl _ADCCH3VAL
                                   1122 	.globl _ADCCH3VAL1
                                   1123 	.globl _ADCCH3VAL0
                                   1124 	.globl _ADCCH2VAL
                                   1125 	.globl _ADCCH2VAL1
                                   1126 	.globl _ADCCH2VAL0
                                   1127 	.globl _ADCCH1VAL
                                   1128 	.globl _ADCCH1VAL1
                                   1129 	.globl _ADCCH1VAL0
                                   1130 	.globl _ADCCH0VAL
                                   1131 	.globl _ADCCH0VAL1
                                   1132 	.globl _ADCCH0VAL0
                                   1133 	.globl _coldstart
                                   1134 	.globl _pkt_counter
                                   1135 	.globl _transmit_packet
                                   1136 	.globl _axradio_statuschange
                                   1137 ;--------------------------------------------------------
                                   1138 ; special function registers
                                   1139 ;--------------------------------------------------------
                                   1140 	.area RSEG    (ABS,DATA)
      000000                       1141 	.org 0x0000
                           0000E0  1142 _ACC	=	0x00e0
                           0000F0  1143 _B	=	0x00f0
                           000083  1144 _DPH	=	0x0083
                           000085  1145 _DPH1	=	0x0085
                           000082  1146 _DPL	=	0x0082
                           000084  1147 _DPL1	=	0x0084
                           008382  1148 _DPTR0	=	0x8382
                           008584  1149 _DPTR1	=	0x8584
                           000086  1150 _DPS	=	0x0086
                           0000A0  1151 _E2IE	=	0x00a0
                           0000C0  1152 _E2IP	=	0x00c0
                           000098  1153 _EIE	=	0x0098
                           0000B0  1154 _EIP	=	0x00b0
                           0000A8  1155 _IE	=	0x00a8
                           0000B8  1156 _IP	=	0x00b8
                           000087  1157 _PCON	=	0x0087
                           0000D0  1158 _PSW	=	0x00d0
                           000081  1159 _SP	=	0x0081
                           0000D9  1160 _XPAGE	=	0x00d9
                           0000D9  1161 __XPAGE	=	0x00d9
                           0000CA  1162 _ADCCH0CONFIG	=	0x00ca
                           0000CB  1163 _ADCCH1CONFIG	=	0x00cb
                           0000D2  1164 _ADCCH2CONFIG	=	0x00d2
                           0000D3  1165 _ADCCH3CONFIG	=	0x00d3
                           0000D1  1166 _ADCCLKSRC	=	0x00d1
                           0000C9  1167 _ADCCONV	=	0x00c9
                           0000E1  1168 _ANALOGCOMP	=	0x00e1
                           0000C6  1169 _CLKCON	=	0x00c6
                           0000C7  1170 _CLKSTAT	=	0x00c7
                           000097  1171 _CODECONFIG	=	0x0097
                           0000E3  1172 _DBGLNKBUF	=	0x00e3
                           0000E2  1173 _DBGLNKSTAT	=	0x00e2
                           000089  1174 _DIRA	=	0x0089
                           00008A  1175 _DIRB	=	0x008a
                           00008B  1176 _DIRC	=	0x008b
                           00008E  1177 _DIRR	=	0x008e
                           0000C8  1178 _PINA	=	0x00c8
                           0000E8  1179 _PINB	=	0x00e8
                           0000F8  1180 _PINC	=	0x00f8
                           00008D  1181 _PINR	=	0x008d
                           000080  1182 _PORTA	=	0x0080
                           000088  1183 _PORTB	=	0x0088
                           000090  1184 _PORTC	=	0x0090
                           00008C  1185 _PORTR	=	0x008c
                           0000CE  1186 _IC0CAPT0	=	0x00ce
                           0000CF  1187 _IC0CAPT1	=	0x00cf
                           00CFCE  1188 _IC0CAPT	=	0xcfce
                           0000CC  1189 _IC0MODE	=	0x00cc
                           0000CD  1190 _IC0STATUS	=	0x00cd
                           0000D6  1191 _IC1CAPT0	=	0x00d6
                           0000D7  1192 _IC1CAPT1	=	0x00d7
                           00D7D6  1193 _IC1CAPT	=	0xd7d6
                           0000D4  1194 _IC1MODE	=	0x00d4
                           0000D5  1195 _IC1STATUS	=	0x00d5
                           000092  1196 _NVADDR0	=	0x0092
                           000093  1197 _NVADDR1	=	0x0093
                           009392  1198 _NVADDR	=	0x9392
                           000094  1199 _NVDATA0	=	0x0094
                           000095  1200 _NVDATA1	=	0x0095
                           009594  1201 _NVDATA	=	0x9594
                           000096  1202 _NVKEY	=	0x0096
                           000091  1203 _NVSTATUS	=	0x0091
                           0000BC  1204 _OC0COMP0	=	0x00bc
                           0000BD  1205 _OC0COMP1	=	0x00bd
                           00BDBC  1206 _OC0COMP	=	0xbdbc
                           0000B9  1207 _OC0MODE	=	0x00b9
                           0000BA  1208 _OC0PIN	=	0x00ba
                           0000BB  1209 _OC0STATUS	=	0x00bb
                           0000C4  1210 _OC1COMP0	=	0x00c4
                           0000C5  1211 _OC1COMP1	=	0x00c5
                           00C5C4  1212 _OC1COMP	=	0xc5c4
                           0000C1  1213 _OC1MODE	=	0x00c1
                           0000C2  1214 _OC1PIN	=	0x00c2
                           0000C3  1215 _OC1STATUS	=	0x00c3
                           0000B1  1216 _RADIOACC	=	0x00b1
                           0000B3  1217 _RADIOADDR0	=	0x00b3
                           0000B2  1218 _RADIOADDR1	=	0x00b2
                           00B2B3  1219 _RADIOADDR	=	0xb2b3
                           0000B7  1220 _RADIODATA0	=	0x00b7
                           0000B6  1221 _RADIODATA1	=	0x00b6
                           0000B5  1222 _RADIODATA2	=	0x00b5
                           0000B4  1223 _RADIODATA3	=	0x00b4
                           B4B5B6B7  1224 _RADIODATA	=	0xb4b5b6b7
                           0000BE  1225 _RADIOSTAT0	=	0x00be
                           0000BF  1226 _RADIOSTAT1	=	0x00bf
                           00BFBE  1227 _RADIOSTAT	=	0xbfbe
                           0000DF  1228 _SPCLKSRC	=	0x00df
                           0000DC  1229 _SPMODE	=	0x00dc
                           0000DE  1230 _SPSHREG	=	0x00de
                           0000DD  1231 _SPSTATUS	=	0x00dd
                           00009A  1232 _T0CLKSRC	=	0x009a
                           00009C  1233 _T0CNT0	=	0x009c
                           00009D  1234 _T0CNT1	=	0x009d
                           009D9C  1235 _T0CNT	=	0x9d9c
                           000099  1236 _T0MODE	=	0x0099
                           00009E  1237 _T0PERIOD0	=	0x009e
                           00009F  1238 _T0PERIOD1	=	0x009f
                           009F9E  1239 _T0PERIOD	=	0x9f9e
                           00009B  1240 _T0STATUS	=	0x009b
                           0000A2  1241 _T1CLKSRC	=	0x00a2
                           0000A4  1242 _T1CNT0	=	0x00a4
                           0000A5  1243 _T1CNT1	=	0x00a5
                           00A5A4  1244 _T1CNT	=	0xa5a4
                           0000A1  1245 _T1MODE	=	0x00a1
                           0000A6  1246 _T1PERIOD0	=	0x00a6
                           0000A7  1247 _T1PERIOD1	=	0x00a7
                           00A7A6  1248 _T1PERIOD	=	0xa7a6
                           0000A3  1249 _T1STATUS	=	0x00a3
                           0000AA  1250 _T2CLKSRC	=	0x00aa
                           0000AC  1251 _T2CNT0	=	0x00ac
                           0000AD  1252 _T2CNT1	=	0x00ad
                           00ADAC  1253 _T2CNT	=	0xadac
                           0000A9  1254 _T2MODE	=	0x00a9
                           0000AE  1255 _T2PERIOD0	=	0x00ae
                           0000AF  1256 _T2PERIOD1	=	0x00af
                           00AFAE  1257 _T2PERIOD	=	0xafae
                           0000AB  1258 _T2STATUS	=	0x00ab
                           0000E4  1259 _U0CTRL	=	0x00e4
                           0000E7  1260 _U0MODE	=	0x00e7
                           0000E6  1261 _U0SHREG	=	0x00e6
                           0000E5  1262 _U0STATUS	=	0x00e5
                           0000EC  1263 _U1CTRL	=	0x00ec
                           0000EF  1264 _U1MODE	=	0x00ef
                           0000EE  1265 _U1SHREG	=	0x00ee
                           0000ED  1266 _U1STATUS	=	0x00ed
                           0000DA  1267 _WDTCFG	=	0x00da
                           0000DB  1268 _WDTRESET	=	0x00db
                           0000F1  1269 _WTCFGA	=	0x00f1
                           0000F9  1270 _WTCFGB	=	0x00f9
                           0000F2  1271 _WTCNTA0	=	0x00f2
                           0000F3  1272 _WTCNTA1	=	0x00f3
                           00F3F2  1273 _WTCNTA	=	0xf3f2
                           0000FA  1274 _WTCNTB0	=	0x00fa
                           0000FB  1275 _WTCNTB1	=	0x00fb
                           00FBFA  1276 _WTCNTB	=	0xfbfa
                           0000EB  1277 _WTCNTR1	=	0x00eb
                           0000F4  1278 _WTEVTA0	=	0x00f4
                           0000F5  1279 _WTEVTA1	=	0x00f5
                           00F5F4  1280 _WTEVTA	=	0xf5f4
                           0000F6  1281 _WTEVTB0	=	0x00f6
                           0000F7  1282 _WTEVTB1	=	0x00f7
                           00F7F6  1283 _WTEVTB	=	0xf7f6
                           0000FC  1284 _WTEVTC0	=	0x00fc
                           0000FD  1285 _WTEVTC1	=	0x00fd
                           00FDFC  1286 _WTEVTC	=	0xfdfc
                           0000FE  1287 _WTEVTD0	=	0x00fe
                           0000FF  1288 _WTEVTD1	=	0x00ff
                           00FFFE  1289 _WTEVTD	=	0xfffe
                           0000E9  1290 _WTIRQEN	=	0x00e9
                           0000EA  1291 _WTSTAT	=	0x00ea
                                   1292 ;--------------------------------------------------------
                                   1293 ; special function bits
                                   1294 ;--------------------------------------------------------
                                   1295 	.area RSEG    (ABS,DATA)
      000000                       1296 	.org 0x0000
                           0000E0  1297 _ACC_0	=	0x00e0
                           0000E1  1298 _ACC_1	=	0x00e1
                           0000E2  1299 _ACC_2	=	0x00e2
                           0000E3  1300 _ACC_3	=	0x00e3
                           0000E4  1301 _ACC_4	=	0x00e4
                           0000E5  1302 _ACC_5	=	0x00e5
                           0000E6  1303 _ACC_6	=	0x00e6
                           0000E7  1304 _ACC_7	=	0x00e7
                           0000F0  1305 _B_0	=	0x00f0
                           0000F1  1306 _B_1	=	0x00f1
                           0000F2  1307 _B_2	=	0x00f2
                           0000F3  1308 _B_3	=	0x00f3
                           0000F4  1309 _B_4	=	0x00f4
                           0000F5  1310 _B_5	=	0x00f5
                           0000F6  1311 _B_6	=	0x00f6
                           0000F7  1312 _B_7	=	0x00f7
                           0000A0  1313 _E2IE_0	=	0x00a0
                           0000A1  1314 _E2IE_1	=	0x00a1
                           0000A2  1315 _E2IE_2	=	0x00a2
                           0000A3  1316 _E2IE_3	=	0x00a3
                           0000A4  1317 _E2IE_4	=	0x00a4
                           0000A5  1318 _E2IE_5	=	0x00a5
                           0000A6  1319 _E2IE_6	=	0x00a6
                           0000A7  1320 _E2IE_7	=	0x00a7
                           0000C0  1321 _E2IP_0	=	0x00c0
                           0000C1  1322 _E2IP_1	=	0x00c1
                           0000C2  1323 _E2IP_2	=	0x00c2
                           0000C3  1324 _E2IP_3	=	0x00c3
                           0000C4  1325 _E2IP_4	=	0x00c4
                           0000C5  1326 _E2IP_5	=	0x00c5
                           0000C6  1327 _E2IP_6	=	0x00c6
                           0000C7  1328 _E2IP_7	=	0x00c7
                           000098  1329 _EIE_0	=	0x0098
                           000099  1330 _EIE_1	=	0x0099
                           00009A  1331 _EIE_2	=	0x009a
                           00009B  1332 _EIE_3	=	0x009b
                           00009C  1333 _EIE_4	=	0x009c
                           00009D  1334 _EIE_5	=	0x009d
                           00009E  1335 _EIE_6	=	0x009e
                           00009F  1336 _EIE_7	=	0x009f
                           0000B0  1337 _EIP_0	=	0x00b0
                           0000B1  1338 _EIP_1	=	0x00b1
                           0000B2  1339 _EIP_2	=	0x00b2
                           0000B3  1340 _EIP_3	=	0x00b3
                           0000B4  1341 _EIP_4	=	0x00b4
                           0000B5  1342 _EIP_5	=	0x00b5
                           0000B6  1343 _EIP_6	=	0x00b6
                           0000B7  1344 _EIP_7	=	0x00b7
                           0000A8  1345 _IE_0	=	0x00a8
                           0000A9  1346 _IE_1	=	0x00a9
                           0000AA  1347 _IE_2	=	0x00aa
                           0000AB  1348 _IE_3	=	0x00ab
                           0000AC  1349 _IE_4	=	0x00ac
                           0000AD  1350 _IE_5	=	0x00ad
                           0000AE  1351 _IE_6	=	0x00ae
                           0000AF  1352 _IE_7	=	0x00af
                           0000AF  1353 _EA	=	0x00af
                           0000B8  1354 _IP_0	=	0x00b8
                           0000B9  1355 _IP_1	=	0x00b9
                           0000BA  1356 _IP_2	=	0x00ba
                           0000BB  1357 _IP_3	=	0x00bb
                           0000BC  1358 _IP_4	=	0x00bc
                           0000BD  1359 _IP_5	=	0x00bd
                           0000BE  1360 _IP_6	=	0x00be
                           0000BF  1361 _IP_7	=	0x00bf
                           0000D0  1362 _P	=	0x00d0
                           0000D1  1363 _F1	=	0x00d1
                           0000D2  1364 _OV	=	0x00d2
                           0000D3  1365 _RS0	=	0x00d3
                           0000D4  1366 _RS1	=	0x00d4
                           0000D5  1367 _F0	=	0x00d5
                           0000D6  1368 _AC	=	0x00d6
                           0000D7  1369 _CY	=	0x00d7
                           0000C8  1370 _PINA_0	=	0x00c8
                           0000C9  1371 _PINA_1	=	0x00c9
                           0000CA  1372 _PINA_2	=	0x00ca
                           0000CB  1373 _PINA_3	=	0x00cb
                           0000CC  1374 _PINA_4	=	0x00cc
                           0000CD  1375 _PINA_5	=	0x00cd
                           0000CE  1376 _PINA_6	=	0x00ce
                           0000CF  1377 _PINA_7	=	0x00cf
                           0000E8  1378 _PINB_0	=	0x00e8
                           0000E9  1379 _PINB_1	=	0x00e9
                           0000EA  1380 _PINB_2	=	0x00ea
                           0000EB  1381 _PINB_3	=	0x00eb
                           0000EC  1382 _PINB_4	=	0x00ec
                           0000ED  1383 _PINB_5	=	0x00ed
                           0000EE  1384 _PINB_6	=	0x00ee
                           0000EF  1385 _PINB_7	=	0x00ef
                           0000F8  1386 _PINC_0	=	0x00f8
                           0000F9  1387 _PINC_1	=	0x00f9
                           0000FA  1388 _PINC_2	=	0x00fa
                           0000FB  1389 _PINC_3	=	0x00fb
                           0000FC  1390 _PINC_4	=	0x00fc
                           0000FD  1391 _PINC_5	=	0x00fd
                           0000FE  1392 _PINC_6	=	0x00fe
                           0000FF  1393 _PINC_7	=	0x00ff
                           000080  1394 _PORTA_0	=	0x0080
                           000081  1395 _PORTA_1	=	0x0081
                           000082  1396 _PORTA_2	=	0x0082
                           000083  1397 _PORTA_3	=	0x0083
                           000084  1398 _PORTA_4	=	0x0084
                           000085  1399 _PORTA_5	=	0x0085
                           000086  1400 _PORTA_6	=	0x0086
                           000087  1401 _PORTA_7	=	0x0087
                           000088  1402 _PORTB_0	=	0x0088
                           000089  1403 _PORTB_1	=	0x0089
                           00008A  1404 _PORTB_2	=	0x008a
                           00008B  1405 _PORTB_3	=	0x008b
                           00008C  1406 _PORTB_4	=	0x008c
                           00008D  1407 _PORTB_5	=	0x008d
                           00008E  1408 _PORTB_6	=	0x008e
                           00008F  1409 _PORTB_7	=	0x008f
                           000090  1410 _PORTC_0	=	0x0090
                           000091  1411 _PORTC_1	=	0x0091
                           000092  1412 _PORTC_2	=	0x0092
                           000093  1413 _PORTC_3	=	0x0093
                           000094  1414 _PORTC_4	=	0x0094
                           000095  1415 _PORTC_5	=	0x0095
                           000096  1416 _PORTC_6	=	0x0096
                           000097  1417 _PORTC_7	=	0x0097
                                   1418 ;--------------------------------------------------------
                                   1419 ; overlayable register banks
                                   1420 ;--------------------------------------------------------
                                   1421 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                       1422 	.ds 8
                                   1423 ;--------------------------------------------------------
                                   1424 ; internal ram data
                                   1425 ;--------------------------------------------------------
                                   1426 	.area DSEG    (DATA)
      000008                       1427 _pkt_counter::
      000008                       1428 	.ds 2
      00000A                       1429 _coldstart::
      00000A                       1430 	.ds 1
                                   1431 ;--------------------------------------------------------
                                   1432 ; overlayable items in internal ram 
                                   1433 ;--------------------------------------------------------
                                   1434 ;--------------------------------------------------------
                                   1435 ; Stack segment in internal ram 
                                   1436 ;--------------------------------------------------------
                                   1437 	.area	SSEG
      00006F                       1438 __start__stack:
      00006F                       1439 	.ds	1
                                   1440 
                                   1441 ;--------------------------------------------------------
                                   1442 ; indirectly addressable internal ram data
                                   1443 ;--------------------------------------------------------
                                   1444 	.area ISEG    (DATA)
                                   1445 ;--------------------------------------------------------
                                   1446 ; absolute internal ram data
                                   1447 ;--------------------------------------------------------
                                   1448 	.area IABS    (ABS,DATA)
                                   1449 	.area IABS    (ABS,DATA)
                                   1450 ;--------------------------------------------------------
                                   1451 ; bit data
                                   1452 ;--------------------------------------------------------
                                   1453 	.area BSEG    (BIT)
                                   1454 ;--------------------------------------------------------
                                   1455 ; paged external ram data
                                   1456 ;--------------------------------------------------------
                                   1457 	.area PSEG    (PAG,XDATA)
                                   1458 ;--------------------------------------------------------
                                   1459 ; external ram data
                                   1460 ;--------------------------------------------------------
                                   1461 	.area XSEG    (XDATA)
                           007020  1462 _ADCCH0VAL0	=	0x7020
                           007021  1463 _ADCCH0VAL1	=	0x7021
                           007020  1464 _ADCCH0VAL	=	0x7020
                           007022  1465 _ADCCH1VAL0	=	0x7022
                           007023  1466 _ADCCH1VAL1	=	0x7023
                           007022  1467 _ADCCH1VAL	=	0x7022
                           007024  1468 _ADCCH2VAL0	=	0x7024
                           007025  1469 _ADCCH2VAL1	=	0x7025
                           007024  1470 _ADCCH2VAL	=	0x7024
                           007026  1471 _ADCCH3VAL0	=	0x7026
                           007027  1472 _ADCCH3VAL1	=	0x7027
                           007026  1473 _ADCCH3VAL	=	0x7026
                           007028  1474 _ADCTUNE0	=	0x7028
                           007029  1475 _ADCTUNE1	=	0x7029
                           00702A  1476 _ADCTUNE2	=	0x702a
                           007010  1477 _DMA0ADDR0	=	0x7010
                           007011  1478 _DMA0ADDR1	=	0x7011
                           007010  1479 _DMA0ADDR	=	0x7010
                           007014  1480 _DMA0CONFIG	=	0x7014
                           007012  1481 _DMA1ADDR0	=	0x7012
                           007013  1482 _DMA1ADDR1	=	0x7013
                           007012  1483 _DMA1ADDR	=	0x7012
                           007015  1484 _DMA1CONFIG	=	0x7015
                           007070  1485 _FRCOSCCONFIG	=	0x7070
                           007071  1486 _FRCOSCCTRL	=	0x7071
                           007076  1487 _FRCOSCFREQ0	=	0x7076
                           007077  1488 _FRCOSCFREQ1	=	0x7077
                           007076  1489 _FRCOSCFREQ	=	0x7076
                           007072  1490 _FRCOSCKFILT0	=	0x7072
                           007073  1491 _FRCOSCKFILT1	=	0x7073
                           007072  1492 _FRCOSCKFILT	=	0x7072
                           007078  1493 _FRCOSCPER0	=	0x7078
                           007079  1494 _FRCOSCPER1	=	0x7079
                           007078  1495 _FRCOSCPER	=	0x7078
                           007074  1496 _FRCOSCREF0	=	0x7074
                           007075  1497 _FRCOSCREF1	=	0x7075
                           007074  1498 _FRCOSCREF	=	0x7074
                           007007  1499 _ANALOGA	=	0x7007
                           00700C  1500 _GPIOENABLE	=	0x700c
                           007003  1501 _EXTIRQ	=	0x7003
                           007000  1502 _INTCHGA	=	0x7000
                           007001  1503 _INTCHGB	=	0x7001
                           007002  1504 _INTCHGC	=	0x7002
                           007008  1505 _PALTA	=	0x7008
                           007009  1506 _PALTB	=	0x7009
                           00700A  1507 _PALTC	=	0x700a
                           007046  1508 _PALTRADIO	=	0x7046
                           007004  1509 _PINCHGA	=	0x7004
                           007005  1510 _PINCHGB	=	0x7005
                           007006  1511 _PINCHGC	=	0x7006
                           00700B  1512 _PINSEL	=	0x700b
                           007060  1513 _LPOSCCONFIG	=	0x7060
                           007066  1514 _LPOSCFREQ0	=	0x7066
                           007067  1515 _LPOSCFREQ1	=	0x7067
                           007066  1516 _LPOSCFREQ	=	0x7066
                           007062  1517 _LPOSCKFILT0	=	0x7062
                           007063  1518 _LPOSCKFILT1	=	0x7063
                           007062  1519 _LPOSCKFILT	=	0x7062
                           007068  1520 _LPOSCPER0	=	0x7068
                           007069  1521 _LPOSCPER1	=	0x7069
                           007068  1522 _LPOSCPER	=	0x7068
                           007064  1523 _LPOSCREF0	=	0x7064
                           007065  1524 _LPOSCREF1	=	0x7065
                           007064  1525 _LPOSCREF	=	0x7064
                           007054  1526 _LPXOSCGM	=	0x7054
                           007F01  1527 _MISCCTRL	=	0x7f01
                           007053  1528 _OSCCALIB	=	0x7053
                           007050  1529 _OSCFORCERUN	=	0x7050
                           007052  1530 _OSCREADY	=	0x7052
                           007051  1531 _OSCRUN	=	0x7051
                           007040  1532 _RADIOFDATAADDR0	=	0x7040
                           007041  1533 _RADIOFDATAADDR1	=	0x7041
                           007040  1534 _RADIOFDATAADDR	=	0x7040
                           007042  1535 _RADIOFSTATADDR0	=	0x7042
                           007043  1536 _RADIOFSTATADDR1	=	0x7043
                           007042  1537 _RADIOFSTATADDR	=	0x7042
                           007044  1538 _RADIOMUX	=	0x7044
                           007084  1539 _SCRATCH0	=	0x7084
                           007085  1540 _SCRATCH1	=	0x7085
                           007086  1541 _SCRATCH2	=	0x7086
                           007087  1542 _SCRATCH3	=	0x7087
                           007F00  1543 _SILICONREV	=	0x7f00
                           007F19  1544 _XTALAMPL	=	0x7f19
                           007F18  1545 _XTALOSC	=	0x7f18
                           007F1A  1546 _XTALREADY	=	0x7f1a
                           007081  1547 _RNGBYTE	=	0x7081
                           007091  1548 _AESCONFIG	=	0x7091
                           007098  1549 _AESCURBLOCK	=	0x7098
                           007094  1550 _AESINADDR0	=	0x7094
                           007095  1551 _AESINADDR1	=	0x7095
                           007094  1552 _AESINADDR	=	0x7094
                           007092  1553 _AESKEYADDR0	=	0x7092
                           007093  1554 _AESKEYADDR1	=	0x7093
                           007092  1555 _AESKEYADDR	=	0x7092
                           007090  1556 _AESMODE	=	0x7090
                           007096  1557 _AESOUTADDR0	=	0x7096
                           007097  1558 _AESOUTADDR1	=	0x7097
                           007096  1559 _AESOUTADDR	=	0x7096
                           007080  1560 _RNGMODE	=	0x7080
                           007082  1561 _RNGCLKSRC0	=	0x7082
                           007083  1562 _RNGCLKSRC1	=	0x7083
                           003F82  1563 _XDPTR0	=	0x3f82
                           003F84  1564 _XDPTR1	=	0x3f84
                           003FA8  1565 _XIE	=	0x3fa8
                           003FB8  1566 _XIP	=	0x3fb8
                           003F87  1567 _XPCON	=	0x3f87
                           003FCA  1568 _XADCCH0CONFIG	=	0x3fca
                           003FCB  1569 _XADCCH1CONFIG	=	0x3fcb
                           003FD2  1570 _XADCCH2CONFIG	=	0x3fd2
                           003FD3  1571 _XADCCH3CONFIG	=	0x3fd3
                           003FD1  1572 _XADCCLKSRC	=	0x3fd1
                           003FC9  1573 _XADCCONV	=	0x3fc9
                           003FE1  1574 _XANALOGCOMP	=	0x3fe1
                           003FC6  1575 _XCLKCON	=	0x3fc6
                           003FC7  1576 _XCLKSTAT	=	0x3fc7
                           003F97  1577 _XCODECONFIG	=	0x3f97
                           003FE3  1578 _XDBGLNKBUF	=	0x3fe3
                           003FE2  1579 _XDBGLNKSTAT	=	0x3fe2
                           003F89  1580 _XDIRA	=	0x3f89
                           003F8A  1581 _XDIRB	=	0x3f8a
                           003F8B  1582 _XDIRC	=	0x3f8b
                           003F8E  1583 _XDIRR	=	0x3f8e
                           003FC8  1584 _XPINA	=	0x3fc8
                           003FE8  1585 _XPINB	=	0x3fe8
                           003FF8  1586 _XPINC	=	0x3ff8
                           003F8D  1587 _XPINR	=	0x3f8d
                           003F80  1588 _XPORTA	=	0x3f80
                           003F88  1589 _XPORTB	=	0x3f88
                           003F90  1590 _XPORTC	=	0x3f90
                           003F8C  1591 _XPORTR	=	0x3f8c
                           003FCE  1592 _XIC0CAPT0	=	0x3fce
                           003FCF  1593 _XIC0CAPT1	=	0x3fcf
                           003FCE  1594 _XIC0CAPT	=	0x3fce
                           003FCC  1595 _XIC0MODE	=	0x3fcc
                           003FCD  1596 _XIC0STATUS	=	0x3fcd
                           003FD6  1597 _XIC1CAPT0	=	0x3fd6
                           003FD7  1598 _XIC1CAPT1	=	0x3fd7
                           003FD6  1599 _XIC1CAPT	=	0x3fd6
                           003FD4  1600 _XIC1MODE	=	0x3fd4
                           003FD5  1601 _XIC1STATUS	=	0x3fd5
                           003F92  1602 _XNVADDR0	=	0x3f92
                           003F93  1603 _XNVADDR1	=	0x3f93
                           003F92  1604 _XNVADDR	=	0x3f92
                           003F94  1605 _XNVDATA0	=	0x3f94
                           003F95  1606 _XNVDATA1	=	0x3f95
                           003F94  1607 _XNVDATA	=	0x3f94
                           003F96  1608 _XNVKEY	=	0x3f96
                           003F91  1609 _XNVSTATUS	=	0x3f91
                           003FBC  1610 _XOC0COMP0	=	0x3fbc
                           003FBD  1611 _XOC0COMP1	=	0x3fbd
                           003FBC  1612 _XOC0COMP	=	0x3fbc
                           003FB9  1613 _XOC0MODE	=	0x3fb9
                           003FBA  1614 _XOC0PIN	=	0x3fba
                           003FBB  1615 _XOC0STATUS	=	0x3fbb
                           003FC4  1616 _XOC1COMP0	=	0x3fc4
                           003FC5  1617 _XOC1COMP1	=	0x3fc5
                           003FC4  1618 _XOC1COMP	=	0x3fc4
                           003FC1  1619 _XOC1MODE	=	0x3fc1
                           003FC2  1620 _XOC1PIN	=	0x3fc2
                           003FC3  1621 _XOC1STATUS	=	0x3fc3
                           003FB1  1622 _XRADIOACC	=	0x3fb1
                           003FB3  1623 _XRADIOADDR0	=	0x3fb3
                           003FB2  1624 _XRADIOADDR1	=	0x3fb2
                           003FB7  1625 _XRADIODATA0	=	0x3fb7
                           003FB6  1626 _XRADIODATA1	=	0x3fb6
                           003FB5  1627 _XRADIODATA2	=	0x3fb5
                           003FB4  1628 _XRADIODATA3	=	0x3fb4
                           003FBE  1629 _XRADIOSTAT0	=	0x3fbe
                           003FBF  1630 _XRADIOSTAT1	=	0x3fbf
                           003FBE  1631 _XRADIOSTAT	=	0x3fbe
                           003FDF  1632 _XSPCLKSRC	=	0x3fdf
                           003FDC  1633 _XSPMODE	=	0x3fdc
                           003FDE  1634 _XSPSHREG	=	0x3fde
                           003FDD  1635 _XSPSTATUS	=	0x3fdd
                           003F9A  1636 _XT0CLKSRC	=	0x3f9a
                           003F9C  1637 _XT0CNT0	=	0x3f9c
                           003F9D  1638 _XT0CNT1	=	0x3f9d
                           003F9C  1639 _XT0CNT	=	0x3f9c
                           003F99  1640 _XT0MODE	=	0x3f99
                           003F9E  1641 _XT0PERIOD0	=	0x3f9e
                           003F9F  1642 _XT0PERIOD1	=	0x3f9f
                           003F9E  1643 _XT0PERIOD	=	0x3f9e
                           003F9B  1644 _XT0STATUS	=	0x3f9b
                           003FA2  1645 _XT1CLKSRC	=	0x3fa2
                           003FA4  1646 _XT1CNT0	=	0x3fa4
                           003FA5  1647 _XT1CNT1	=	0x3fa5
                           003FA4  1648 _XT1CNT	=	0x3fa4
                           003FA1  1649 _XT1MODE	=	0x3fa1
                           003FA6  1650 _XT1PERIOD0	=	0x3fa6
                           003FA7  1651 _XT1PERIOD1	=	0x3fa7
                           003FA6  1652 _XT1PERIOD	=	0x3fa6
                           003FA3  1653 _XT1STATUS	=	0x3fa3
                           003FAA  1654 _XT2CLKSRC	=	0x3faa
                           003FAC  1655 _XT2CNT0	=	0x3fac
                           003FAD  1656 _XT2CNT1	=	0x3fad
                           003FAC  1657 _XT2CNT	=	0x3fac
                           003FA9  1658 _XT2MODE	=	0x3fa9
                           003FAE  1659 _XT2PERIOD0	=	0x3fae
                           003FAF  1660 _XT2PERIOD1	=	0x3faf
                           003FAE  1661 _XT2PERIOD	=	0x3fae
                           003FAB  1662 _XT2STATUS	=	0x3fab
                           003FE4  1663 _XU0CTRL	=	0x3fe4
                           003FE7  1664 _XU0MODE	=	0x3fe7
                           003FE6  1665 _XU0SHREG	=	0x3fe6
                           003FE5  1666 _XU0STATUS	=	0x3fe5
                           003FEC  1667 _XU1CTRL	=	0x3fec
                           003FEF  1668 _XU1MODE	=	0x3fef
                           003FEE  1669 _XU1SHREG	=	0x3fee
                           003FED  1670 _XU1STATUS	=	0x3fed
                           003FDA  1671 _XWDTCFG	=	0x3fda
                           003FDB  1672 _XWDTRESET	=	0x3fdb
                           003FF1  1673 _XWTCFGA	=	0x3ff1
                           003FF9  1674 _XWTCFGB	=	0x3ff9
                           003FF2  1675 _XWTCNTA0	=	0x3ff2
                           003FF3  1676 _XWTCNTA1	=	0x3ff3
                           003FF2  1677 _XWTCNTA	=	0x3ff2
                           003FFA  1678 _XWTCNTB0	=	0x3ffa
                           003FFB  1679 _XWTCNTB1	=	0x3ffb
                           003FFA  1680 _XWTCNTB	=	0x3ffa
                           003FEB  1681 _XWTCNTR1	=	0x3feb
                           003FF4  1682 _XWTEVTA0	=	0x3ff4
                           003FF5  1683 _XWTEVTA1	=	0x3ff5
                           003FF4  1684 _XWTEVTA	=	0x3ff4
                           003FF6  1685 _XWTEVTB0	=	0x3ff6
                           003FF7  1686 _XWTEVTB1	=	0x3ff7
                           003FF6  1687 _XWTEVTB	=	0x3ff6
                           003FFC  1688 _XWTEVTC0	=	0x3ffc
                           003FFD  1689 _XWTEVTC1	=	0x3ffd
                           003FFC  1690 _XWTEVTC	=	0x3ffc
                           003FFE  1691 _XWTEVTD0	=	0x3ffe
                           003FFF  1692 _XWTEVTD1	=	0x3fff
                           003FFE  1693 _XWTEVTD	=	0x3ffe
                           003FE9  1694 _XWTIRQEN	=	0x3fe9
                           003FEA  1695 _XWTSTAT	=	0x3fea
                           004114  1696 _AX5043_AFSKCTRL	=	0x4114
                           004113  1697 _AX5043_AFSKMARK0	=	0x4113
                           004112  1698 _AX5043_AFSKMARK1	=	0x4112
                           004111  1699 _AX5043_AFSKSPACE0	=	0x4111
                           004110  1700 _AX5043_AFSKSPACE1	=	0x4110
                           004043  1701 _AX5043_AGCCOUNTER	=	0x4043
                           004115  1702 _AX5043_AMPLFILTER	=	0x4115
                           004189  1703 _AX5043_BBOFFSCAP	=	0x4189
                           004188  1704 _AX5043_BBTUNE	=	0x4188
                           004041  1705 _AX5043_BGNDRSSI	=	0x4041
                           00422E  1706 _AX5043_BGNDRSSIGAIN	=	0x422e
                           00422F  1707 _AX5043_BGNDRSSITHR	=	0x422f
                           004017  1708 _AX5043_CRCINIT0	=	0x4017
                           004016  1709 _AX5043_CRCINIT1	=	0x4016
                           004015  1710 _AX5043_CRCINIT2	=	0x4015
                           004014  1711 _AX5043_CRCINIT3	=	0x4014
                           004332  1712 _AX5043_DACCONFIG	=	0x4332
                           004331  1713 _AX5043_DACVALUE0	=	0x4331
                           004330  1714 _AX5043_DACVALUE1	=	0x4330
                           004102  1715 _AX5043_DECIMATION	=	0x4102
                           004042  1716 _AX5043_DIVERSITY	=	0x4042
                           004011  1717 _AX5043_ENCODING	=	0x4011
                           004018  1718 _AX5043_FEC	=	0x4018
                           00401A  1719 _AX5043_FECSTATUS	=	0x401a
                           004019  1720 _AX5043_FECSYNC	=	0x4019
                           00402B  1721 _AX5043_FIFOCOUNT0	=	0x402b
                           00402A  1722 _AX5043_FIFOCOUNT1	=	0x402a
                           004029  1723 _AX5043_FIFODATA	=	0x4029
                           00402D  1724 _AX5043_FIFOFREE0	=	0x402d
                           00402C  1725 _AX5043_FIFOFREE1	=	0x402c
                           004028  1726 _AX5043_FIFOSTAT	=	0x4028
                           00402F  1727 _AX5043_FIFOTHRESH0	=	0x402f
                           00402E  1728 _AX5043_FIFOTHRESH1	=	0x402e
                           004012  1729 _AX5043_FRAMING	=	0x4012
                           004037  1730 _AX5043_FREQA0	=	0x4037
                           004036  1731 _AX5043_FREQA1	=	0x4036
                           004035  1732 _AX5043_FREQA2	=	0x4035
                           004034  1733 _AX5043_FREQA3	=	0x4034
                           00403F  1734 _AX5043_FREQB0	=	0x403f
                           00403E  1735 _AX5043_FREQB1	=	0x403e
                           00403D  1736 _AX5043_FREQB2	=	0x403d
                           00403C  1737 _AX5043_FREQB3	=	0x403c
                           004163  1738 _AX5043_FSKDEV0	=	0x4163
                           004162  1739 _AX5043_FSKDEV1	=	0x4162
                           004161  1740 _AX5043_FSKDEV2	=	0x4161
                           00410D  1741 _AX5043_FSKDMAX0	=	0x410d
                           00410C  1742 _AX5043_FSKDMAX1	=	0x410c
                           00410F  1743 _AX5043_FSKDMIN0	=	0x410f
                           00410E  1744 _AX5043_FSKDMIN1	=	0x410e
                           004309  1745 _AX5043_GPADC13VALUE0	=	0x4309
                           004308  1746 _AX5043_GPADC13VALUE1	=	0x4308
                           004300  1747 _AX5043_GPADCCTRL	=	0x4300
                           004301  1748 _AX5043_GPADCPERIOD	=	0x4301
                           004101  1749 _AX5043_IFFREQ0	=	0x4101
                           004100  1750 _AX5043_IFFREQ1	=	0x4100
                           00400B  1751 _AX5043_IRQINVERSION0	=	0x400b
                           00400A  1752 _AX5043_IRQINVERSION1	=	0x400a
                           004007  1753 _AX5043_IRQMASK0	=	0x4007
                           004006  1754 _AX5043_IRQMASK1	=	0x4006
                           00400D  1755 _AX5043_IRQREQUEST0	=	0x400d
                           00400C  1756 _AX5043_IRQREQUEST1	=	0x400c
                           004310  1757 _AX5043_LPOSCCONFIG	=	0x4310
                           004317  1758 _AX5043_LPOSCFREQ0	=	0x4317
                           004316  1759 _AX5043_LPOSCFREQ1	=	0x4316
                           004313  1760 _AX5043_LPOSCKFILT0	=	0x4313
                           004312  1761 _AX5043_LPOSCKFILT1	=	0x4312
                           004319  1762 _AX5043_LPOSCPER0	=	0x4319
                           004318  1763 _AX5043_LPOSCPER1	=	0x4318
                           004315  1764 _AX5043_LPOSCREF0	=	0x4315
                           004314  1765 _AX5043_LPOSCREF1	=	0x4314
                           004311  1766 _AX5043_LPOSCSTATUS	=	0x4311
                           004214  1767 _AX5043_MATCH0LEN	=	0x4214
                           004216  1768 _AX5043_MATCH0MAX	=	0x4216
                           004215  1769 _AX5043_MATCH0MIN	=	0x4215
                           004213  1770 _AX5043_MATCH0PAT0	=	0x4213
                           004212  1771 _AX5043_MATCH0PAT1	=	0x4212
                           004211  1772 _AX5043_MATCH0PAT2	=	0x4211
                           004210  1773 _AX5043_MATCH0PAT3	=	0x4210
                           00421C  1774 _AX5043_MATCH1LEN	=	0x421c
                           00421E  1775 _AX5043_MATCH1MAX	=	0x421e
                           00421D  1776 _AX5043_MATCH1MIN	=	0x421d
                           004219  1777 _AX5043_MATCH1PAT0	=	0x4219
                           004218  1778 _AX5043_MATCH1PAT1	=	0x4218
                           004108  1779 _AX5043_MAXDROFFSET0	=	0x4108
                           004107  1780 _AX5043_MAXDROFFSET1	=	0x4107
                           004106  1781 _AX5043_MAXDROFFSET2	=	0x4106
                           00410B  1782 _AX5043_MAXRFOFFSET0	=	0x410b
                           00410A  1783 _AX5043_MAXRFOFFSET1	=	0x410a
                           004109  1784 _AX5043_MAXRFOFFSET2	=	0x4109
                           004164  1785 _AX5043_MODCFGA	=	0x4164
                           004160  1786 _AX5043_MODCFGF	=	0x4160
                           004010  1787 _AX5043_MODULATION	=	0x4010
                           004025  1788 _AX5043_PINFUNCANTSEL	=	0x4025
                           004023  1789 _AX5043_PINFUNCDATA	=	0x4023
                           004022  1790 _AX5043_PINFUNCDCLK	=	0x4022
                           004024  1791 _AX5043_PINFUNCIRQ	=	0x4024
                           004026  1792 _AX5043_PINFUNCPWRAMP	=	0x4026
                           004021  1793 _AX5043_PINFUNCSYSCLK	=	0x4021
                           004020  1794 _AX5043_PINSTATE	=	0x4020
                           004233  1795 _AX5043_PKTACCEPTFLAGS	=	0x4233
                           004230  1796 _AX5043_PKTCHUNKSIZE	=	0x4230
                           004231  1797 _AX5043_PKTMISCFLAGS	=	0x4231
                           004232  1798 _AX5043_PKTSTOREFLAGS	=	0x4232
                           004031  1799 _AX5043_PLLCPI	=	0x4031
                           004039  1800 _AX5043_PLLCPIBOOST	=	0x4039
                           004182  1801 _AX5043_PLLLOCKDET	=	0x4182
                           004030  1802 _AX5043_PLLLOOP	=	0x4030
                           004038  1803 _AX5043_PLLLOOPBOOST	=	0x4038
                           004033  1804 _AX5043_PLLRANGINGA	=	0x4033
                           00403B  1805 _AX5043_PLLRANGINGB	=	0x403b
                           004183  1806 _AX5043_PLLRNGCLK	=	0x4183
                           004032  1807 _AX5043_PLLVCODIV	=	0x4032
                           004180  1808 _AX5043_PLLVCOI	=	0x4180
                           004181  1809 _AX5043_PLLVCOIR	=	0x4181
                           004005  1810 _AX5043_POWIRQMASK	=	0x4005
                           004003  1811 _AX5043_POWSTAT	=	0x4003
                           004004  1812 _AX5043_POWSTICKYSTAT	=	0x4004
                           004027  1813 _AX5043_PWRAMP	=	0x4027
                           004002  1814 _AX5043_PWRMODE	=	0x4002
                           004009  1815 _AX5043_RADIOEVENTMASK0	=	0x4009
                           004008  1816 _AX5043_RADIOEVENTMASK1	=	0x4008
                           00400F  1817 _AX5043_RADIOEVENTREQ0	=	0x400f
                           00400E  1818 _AX5043_RADIOEVENTREQ1	=	0x400e
                           00401C  1819 _AX5043_RADIOSTATE	=	0x401c
                           004040  1820 _AX5043_RSSI	=	0x4040
                           00422D  1821 _AX5043_RSSIABSTHR	=	0x422d
                           00422C  1822 _AX5043_RSSIREFERENCE	=	0x422c
                           004105  1823 _AX5043_RXDATARATE0	=	0x4105
                           004104  1824 _AX5043_RXDATARATE1	=	0x4104
                           004103  1825 _AX5043_RXDATARATE2	=	0x4103
                           004001  1826 _AX5043_SCRATCH	=	0x4001
                           004000  1827 _AX5043_SILICONREVISION	=	0x4000
                           00405B  1828 _AX5043_TIMER0	=	0x405b
                           00405A  1829 _AX5043_TIMER1	=	0x405a
                           004059  1830 _AX5043_TIMER2	=	0x4059
                           004227  1831 _AX5043_TMGRXAGC	=	0x4227
                           004223  1832 _AX5043_TMGRXBOOST	=	0x4223
                           004226  1833 _AX5043_TMGRXCOARSEAGC	=	0x4226
                           004225  1834 _AX5043_TMGRXOFFSACQ	=	0x4225
                           004229  1835 _AX5043_TMGRXPREAMBLE1	=	0x4229
                           00422A  1836 _AX5043_TMGRXPREAMBLE2	=	0x422a
                           00422B  1837 _AX5043_TMGRXPREAMBLE3	=	0x422b
                           004228  1838 _AX5043_TMGRXRSSI	=	0x4228
                           004224  1839 _AX5043_TMGRXSETTLE	=	0x4224
                           004220  1840 _AX5043_TMGTXBOOST	=	0x4220
                           004221  1841 _AX5043_TMGTXSETTLE	=	0x4221
                           004055  1842 _AX5043_TRKAFSKDEMOD0	=	0x4055
                           004054  1843 _AX5043_TRKAFSKDEMOD1	=	0x4054
                           004049  1844 _AX5043_TRKAMPLITUDE0	=	0x4049
                           004048  1845 _AX5043_TRKAMPLITUDE1	=	0x4048
                           004047  1846 _AX5043_TRKDATARATE0	=	0x4047
                           004046  1847 _AX5043_TRKDATARATE1	=	0x4046
                           004045  1848 _AX5043_TRKDATARATE2	=	0x4045
                           004051  1849 _AX5043_TRKFREQ0	=	0x4051
                           004050  1850 _AX5043_TRKFREQ1	=	0x4050
                           004053  1851 _AX5043_TRKFSKDEMOD0	=	0x4053
                           004052  1852 _AX5043_TRKFSKDEMOD1	=	0x4052
                           00404B  1853 _AX5043_TRKPHASE0	=	0x404b
                           00404A  1854 _AX5043_TRKPHASE1	=	0x404a
                           00404F  1855 _AX5043_TRKRFFREQ0	=	0x404f
                           00404E  1856 _AX5043_TRKRFFREQ1	=	0x404e
                           00404D  1857 _AX5043_TRKRFFREQ2	=	0x404d
                           004169  1858 _AX5043_TXPWRCOEFFA0	=	0x4169
                           004168  1859 _AX5043_TXPWRCOEFFA1	=	0x4168
                           00416B  1860 _AX5043_TXPWRCOEFFB0	=	0x416b
                           00416A  1861 _AX5043_TXPWRCOEFFB1	=	0x416a
                           00416D  1862 _AX5043_TXPWRCOEFFC0	=	0x416d
                           00416C  1863 _AX5043_TXPWRCOEFFC1	=	0x416c
                           00416F  1864 _AX5043_TXPWRCOEFFD0	=	0x416f
                           00416E  1865 _AX5043_TXPWRCOEFFD1	=	0x416e
                           004171  1866 _AX5043_TXPWRCOEFFE0	=	0x4171
                           004170  1867 _AX5043_TXPWRCOEFFE1	=	0x4170
                           004167  1868 _AX5043_TXRATE0	=	0x4167
                           004166  1869 _AX5043_TXRATE1	=	0x4166
                           004165  1870 _AX5043_TXRATE2	=	0x4165
                           00406B  1871 _AX5043_WAKEUP0	=	0x406b
                           00406A  1872 _AX5043_WAKEUP1	=	0x406a
                           00406D  1873 _AX5043_WAKEUPFREQ0	=	0x406d
                           00406C  1874 _AX5043_WAKEUPFREQ1	=	0x406c
                           004069  1875 _AX5043_WAKEUPTIMER0	=	0x4069
                           004068  1876 _AX5043_WAKEUPTIMER1	=	0x4068
                           00406E  1877 _AX5043_WAKEUPXOEARLY	=	0x406e
                           004184  1878 _AX5043_XTALCAP	=	0x4184
                           00401D  1879 _AX5043_XTALSTATUS	=	0x401d
                           004122  1880 _AX5043_AGCAHYST0	=	0x4122
                           004132  1881 _AX5043_AGCAHYST1	=	0x4132
                           004142  1882 _AX5043_AGCAHYST2	=	0x4142
                           004152  1883 _AX5043_AGCAHYST3	=	0x4152
                           004120  1884 _AX5043_AGCGAIN0	=	0x4120
                           004130  1885 _AX5043_AGCGAIN1	=	0x4130
                           004140  1886 _AX5043_AGCGAIN2	=	0x4140
                           004150  1887 _AX5043_AGCGAIN3	=	0x4150
                           004123  1888 _AX5043_AGCMINMAX0	=	0x4123
                           004133  1889 _AX5043_AGCMINMAX1	=	0x4133
                           004143  1890 _AX5043_AGCMINMAX2	=	0x4143
                           004153  1891 _AX5043_AGCMINMAX3	=	0x4153
                           004121  1892 _AX5043_AGCTARGET0	=	0x4121
                           004131  1893 _AX5043_AGCTARGET1	=	0x4131
                           004141  1894 _AX5043_AGCTARGET2	=	0x4141
                           004151  1895 _AX5043_AGCTARGET3	=	0x4151
                           00412B  1896 _AX5043_AMPLITUDEGAIN0	=	0x412b
                           00413B  1897 _AX5043_AMPLITUDEGAIN1	=	0x413b
                           00414B  1898 _AX5043_AMPLITUDEGAIN2	=	0x414b
                           00415B  1899 _AX5043_AMPLITUDEGAIN3	=	0x415b
                           00412F  1900 _AX5043_BBOFFSRES0	=	0x412f
                           00413F  1901 _AX5043_BBOFFSRES1	=	0x413f
                           00414F  1902 _AX5043_BBOFFSRES2	=	0x414f
                           00415F  1903 _AX5043_BBOFFSRES3	=	0x415f
                           004125  1904 _AX5043_DRGAIN0	=	0x4125
                           004135  1905 _AX5043_DRGAIN1	=	0x4135
                           004145  1906 _AX5043_DRGAIN2	=	0x4145
                           004155  1907 _AX5043_DRGAIN3	=	0x4155
                           00412E  1908 _AX5043_FOURFSK0	=	0x412e
                           00413E  1909 _AX5043_FOURFSK1	=	0x413e
                           00414E  1910 _AX5043_FOURFSK2	=	0x414e
                           00415E  1911 _AX5043_FOURFSK3	=	0x415e
                           00412D  1912 _AX5043_FREQDEV00	=	0x412d
                           00413D  1913 _AX5043_FREQDEV01	=	0x413d
                           00414D  1914 _AX5043_FREQDEV02	=	0x414d
                           00415D  1915 _AX5043_FREQDEV03	=	0x415d
                           00412C  1916 _AX5043_FREQDEV10	=	0x412c
                           00413C  1917 _AX5043_FREQDEV11	=	0x413c
                           00414C  1918 _AX5043_FREQDEV12	=	0x414c
                           00415C  1919 _AX5043_FREQDEV13	=	0x415c
                           004127  1920 _AX5043_FREQUENCYGAINA0	=	0x4127
                           004137  1921 _AX5043_FREQUENCYGAINA1	=	0x4137
                           004147  1922 _AX5043_FREQUENCYGAINA2	=	0x4147
                           004157  1923 _AX5043_FREQUENCYGAINA3	=	0x4157
                           004128  1924 _AX5043_FREQUENCYGAINB0	=	0x4128
                           004138  1925 _AX5043_FREQUENCYGAINB1	=	0x4138
                           004148  1926 _AX5043_FREQUENCYGAINB2	=	0x4148
                           004158  1927 _AX5043_FREQUENCYGAINB3	=	0x4158
                           004129  1928 _AX5043_FREQUENCYGAINC0	=	0x4129
                           004139  1929 _AX5043_FREQUENCYGAINC1	=	0x4139
                           004149  1930 _AX5043_FREQUENCYGAINC2	=	0x4149
                           004159  1931 _AX5043_FREQUENCYGAINC3	=	0x4159
                           00412A  1932 _AX5043_FREQUENCYGAIND0	=	0x412a
                           00413A  1933 _AX5043_FREQUENCYGAIND1	=	0x413a
                           00414A  1934 _AX5043_FREQUENCYGAIND2	=	0x414a
                           00415A  1935 _AX5043_FREQUENCYGAIND3	=	0x415a
                           004116  1936 _AX5043_FREQUENCYLEAK	=	0x4116
                           004126  1937 _AX5043_PHASEGAIN0	=	0x4126
                           004136  1938 _AX5043_PHASEGAIN1	=	0x4136
                           004146  1939 _AX5043_PHASEGAIN2	=	0x4146
                           004156  1940 _AX5043_PHASEGAIN3	=	0x4156
                           004207  1941 _AX5043_PKTADDR0	=	0x4207
                           004206  1942 _AX5043_PKTADDR1	=	0x4206
                           004205  1943 _AX5043_PKTADDR2	=	0x4205
                           004204  1944 _AX5043_PKTADDR3	=	0x4204
                           004200  1945 _AX5043_PKTADDRCFG	=	0x4200
                           00420B  1946 _AX5043_PKTADDRMASK0	=	0x420b
                           00420A  1947 _AX5043_PKTADDRMASK1	=	0x420a
                           004209  1948 _AX5043_PKTADDRMASK2	=	0x4209
                           004208  1949 _AX5043_PKTADDRMASK3	=	0x4208
                           004201  1950 _AX5043_PKTLENCFG	=	0x4201
                           004202  1951 _AX5043_PKTLENOFFSET	=	0x4202
                           004203  1952 _AX5043_PKTMAXLEN	=	0x4203
                           004118  1953 _AX5043_RXPARAMCURSET	=	0x4118
                           004117  1954 _AX5043_RXPARAMSETS	=	0x4117
                           004124  1955 _AX5043_TIMEGAIN0	=	0x4124
                           004134  1956 _AX5043_TIMEGAIN1	=	0x4134
                           004144  1957 _AX5043_TIMEGAIN2	=	0x4144
                           004154  1958 _AX5043_TIMEGAIN3	=	0x4154
                           005114  1959 _AX5043_AFSKCTRLNB	=	0x5114
                           005113  1960 _AX5043_AFSKMARK0NB	=	0x5113
                           005112  1961 _AX5043_AFSKMARK1NB	=	0x5112
                           005111  1962 _AX5043_AFSKSPACE0NB	=	0x5111
                           005110  1963 _AX5043_AFSKSPACE1NB	=	0x5110
                           005043  1964 _AX5043_AGCCOUNTERNB	=	0x5043
                           005115  1965 _AX5043_AMPLFILTERNB	=	0x5115
                           005189  1966 _AX5043_BBOFFSCAPNB	=	0x5189
                           005188  1967 _AX5043_BBTUNENB	=	0x5188
                           005041  1968 _AX5043_BGNDRSSINB	=	0x5041
                           00522E  1969 _AX5043_BGNDRSSIGAINNB	=	0x522e
                           00522F  1970 _AX5043_BGNDRSSITHRNB	=	0x522f
                           005017  1971 _AX5043_CRCINIT0NB	=	0x5017
                           005016  1972 _AX5043_CRCINIT1NB	=	0x5016
                           005015  1973 _AX5043_CRCINIT2NB	=	0x5015
                           005014  1974 _AX5043_CRCINIT3NB	=	0x5014
                           005332  1975 _AX5043_DACCONFIGNB	=	0x5332
                           005331  1976 _AX5043_DACVALUE0NB	=	0x5331
                           005330  1977 _AX5043_DACVALUE1NB	=	0x5330
                           005102  1978 _AX5043_DECIMATIONNB	=	0x5102
                           005042  1979 _AX5043_DIVERSITYNB	=	0x5042
                           005011  1980 _AX5043_ENCODINGNB	=	0x5011
                           005018  1981 _AX5043_FECNB	=	0x5018
                           00501A  1982 _AX5043_FECSTATUSNB	=	0x501a
                           005019  1983 _AX5043_FECSYNCNB	=	0x5019
                           00502B  1984 _AX5043_FIFOCOUNT0NB	=	0x502b
                           00502A  1985 _AX5043_FIFOCOUNT1NB	=	0x502a
                           005029  1986 _AX5043_FIFODATANB	=	0x5029
                           00502D  1987 _AX5043_FIFOFREE0NB	=	0x502d
                           00502C  1988 _AX5043_FIFOFREE1NB	=	0x502c
                           005028  1989 _AX5043_FIFOSTATNB	=	0x5028
                           00502F  1990 _AX5043_FIFOTHRESH0NB	=	0x502f
                           00502E  1991 _AX5043_FIFOTHRESH1NB	=	0x502e
                           005012  1992 _AX5043_FRAMINGNB	=	0x5012
                           005037  1993 _AX5043_FREQA0NB	=	0x5037
                           005036  1994 _AX5043_FREQA1NB	=	0x5036
                           005035  1995 _AX5043_FREQA2NB	=	0x5035
                           005034  1996 _AX5043_FREQA3NB	=	0x5034
                           00503F  1997 _AX5043_FREQB0NB	=	0x503f
                           00503E  1998 _AX5043_FREQB1NB	=	0x503e
                           00503D  1999 _AX5043_FREQB2NB	=	0x503d
                           00503C  2000 _AX5043_FREQB3NB	=	0x503c
                           005163  2001 _AX5043_FSKDEV0NB	=	0x5163
                           005162  2002 _AX5043_FSKDEV1NB	=	0x5162
                           005161  2003 _AX5043_FSKDEV2NB	=	0x5161
                           00510D  2004 _AX5043_FSKDMAX0NB	=	0x510d
                           00510C  2005 _AX5043_FSKDMAX1NB	=	0x510c
                           00510F  2006 _AX5043_FSKDMIN0NB	=	0x510f
                           00510E  2007 _AX5043_FSKDMIN1NB	=	0x510e
                           005309  2008 _AX5043_GPADC13VALUE0NB	=	0x5309
                           005308  2009 _AX5043_GPADC13VALUE1NB	=	0x5308
                           005300  2010 _AX5043_GPADCCTRLNB	=	0x5300
                           005301  2011 _AX5043_GPADCPERIODNB	=	0x5301
                           005101  2012 _AX5043_IFFREQ0NB	=	0x5101
                           005100  2013 _AX5043_IFFREQ1NB	=	0x5100
                           00500B  2014 _AX5043_IRQINVERSION0NB	=	0x500b
                           00500A  2015 _AX5043_IRQINVERSION1NB	=	0x500a
                           005007  2016 _AX5043_IRQMASK0NB	=	0x5007
                           005006  2017 _AX5043_IRQMASK1NB	=	0x5006
                           00500D  2018 _AX5043_IRQREQUEST0NB	=	0x500d
                           00500C  2019 _AX5043_IRQREQUEST1NB	=	0x500c
                           005310  2020 _AX5043_LPOSCCONFIGNB	=	0x5310
                           005317  2021 _AX5043_LPOSCFREQ0NB	=	0x5317
                           005316  2022 _AX5043_LPOSCFREQ1NB	=	0x5316
                           005313  2023 _AX5043_LPOSCKFILT0NB	=	0x5313
                           005312  2024 _AX5043_LPOSCKFILT1NB	=	0x5312
                           005319  2025 _AX5043_LPOSCPER0NB	=	0x5319
                           005318  2026 _AX5043_LPOSCPER1NB	=	0x5318
                           005315  2027 _AX5043_LPOSCREF0NB	=	0x5315
                           005314  2028 _AX5043_LPOSCREF1NB	=	0x5314
                           005311  2029 _AX5043_LPOSCSTATUSNB	=	0x5311
                           005214  2030 _AX5043_MATCH0LENNB	=	0x5214
                           005216  2031 _AX5043_MATCH0MAXNB	=	0x5216
                           005215  2032 _AX5043_MATCH0MINNB	=	0x5215
                           005213  2033 _AX5043_MATCH0PAT0NB	=	0x5213
                           005212  2034 _AX5043_MATCH0PAT1NB	=	0x5212
                           005211  2035 _AX5043_MATCH0PAT2NB	=	0x5211
                           005210  2036 _AX5043_MATCH0PAT3NB	=	0x5210
                           00521C  2037 _AX5043_MATCH1LENNB	=	0x521c
                           00521E  2038 _AX5043_MATCH1MAXNB	=	0x521e
                           00521D  2039 _AX5043_MATCH1MINNB	=	0x521d
                           005219  2040 _AX5043_MATCH1PAT0NB	=	0x5219
                           005218  2041 _AX5043_MATCH1PAT1NB	=	0x5218
                           005108  2042 _AX5043_MAXDROFFSET0NB	=	0x5108
                           005107  2043 _AX5043_MAXDROFFSET1NB	=	0x5107
                           005106  2044 _AX5043_MAXDROFFSET2NB	=	0x5106
                           00510B  2045 _AX5043_MAXRFOFFSET0NB	=	0x510b
                           00510A  2046 _AX5043_MAXRFOFFSET1NB	=	0x510a
                           005109  2047 _AX5043_MAXRFOFFSET2NB	=	0x5109
                           005164  2048 _AX5043_MODCFGANB	=	0x5164
                           005160  2049 _AX5043_MODCFGFNB	=	0x5160
                           005F5F  2050 _AX5043_MODCFGPNB	=	0x5f5f
                           005010  2051 _AX5043_MODULATIONNB	=	0x5010
                           005025  2052 _AX5043_PINFUNCANTSELNB	=	0x5025
                           005023  2053 _AX5043_PINFUNCDATANB	=	0x5023
                           005022  2054 _AX5043_PINFUNCDCLKNB	=	0x5022
                           005024  2055 _AX5043_PINFUNCIRQNB	=	0x5024
                           005026  2056 _AX5043_PINFUNCPWRAMPNB	=	0x5026
                           005021  2057 _AX5043_PINFUNCSYSCLKNB	=	0x5021
                           005020  2058 _AX5043_PINSTATENB	=	0x5020
                           005233  2059 _AX5043_PKTACCEPTFLAGSNB	=	0x5233
                           005230  2060 _AX5043_PKTCHUNKSIZENB	=	0x5230
                           005231  2061 _AX5043_PKTMISCFLAGSNB	=	0x5231
                           005232  2062 _AX5043_PKTSTOREFLAGSNB	=	0x5232
                           005031  2063 _AX5043_PLLCPINB	=	0x5031
                           005039  2064 _AX5043_PLLCPIBOOSTNB	=	0x5039
                           005182  2065 _AX5043_PLLLOCKDETNB	=	0x5182
                           005030  2066 _AX5043_PLLLOOPNB	=	0x5030
                           005038  2067 _AX5043_PLLLOOPBOOSTNB	=	0x5038
                           005033  2068 _AX5043_PLLRANGINGANB	=	0x5033
                           00503B  2069 _AX5043_PLLRANGINGBNB	=	0x503b
                           005183  2070 _AX5043_PLLRNGCLKNB	=	0x5183
                           005032  2071 _AX5043_PLLVCODIVNB	=	0x5032
                           005180  2072 _AX5043_PLLVCOINB	=	0x5180
                           005181  2073 _AX5043_PLLVCOIRNB	=	0x5181
                           005F08  2074 _AX5043_POWCTRL1NB	=	0x5f08
                           005005  2075 _AX5043_POWIRQMASKNB	=	0x5005
                           005003  2076 _AX5043_POWSTATNB	=	0x5003
                           005004  2077 _AX5043_POWSTICKYSTATNB	=	0x5004
                           005027  2078 _AX5043_PWRAMPNB	=	0x5027
                           005002  2079 _AX5043_PWRMODENB	=	0x5002
                           005009  2080 _AX5043_RADIOEVENTMASK0NB	=	0x5009
                           005008  2081 _AX5043_RADIOEVENTMASK1NB	=	0x5008
                           00500F  2082 _AX5043_RADIOEVENTREQ0NB	=	0x500f
                           00500E  2083 _AX5043_RADIOEVENTREQ1NB	=	0x500e
                           00501C  2084 _AX5043_RADIOSTATENB	=	0x501c
                           005F0D  2085 _AX5043_REFNB	=	0x5f0d
                           005040  2086 _AX5043_RSSINB	=	0x5040
                           00522D  2087 _AX5043_RSSIABSTHRNB	=	0x522d
                           00522C  2088 _AX5043_RSSIREFERENCENB	=	0x522c
                           005105  2089 _AX5043_RXDATARATE0NB	=	0x5105
                           005104  2090 _AX5043_RXDATARATE1NB	=	0x5104
                           005103  2091 _AX5043_RXDATARATE2NB	=	0x5103
                           005001  2092 _AX5043_SCRATCHNB	=	0x5001
                           005000  2093 _AX5043_SILICONREVISIONNB	=	0x5000
                           00505B  2094 _AX5043_TIMER0NB	=	0x505b
                           00505A  2095 _AX5043_TIMER1NB	=	0x505a
                           005059  2096 _AX5043_TIMER2NB	=	0x5059
                           005227  2097 _AX5043_TMGRXAGCNB	=	0x5227
                           005223  2098 _AX5043_TMGRXBOOSTNB	=	0x5223
                           005226  2099 _AX5043_TMGRXCOARSEAGCNB	=	0x5226
                           005225  2100 _AX5043_TMGRXOFFSACQNB	=	0x5225
                           005229  2101 _AX5043_TMGRXPREAMBLE1NB	=	0x5229
                           00522A  2102 _AX5043_TMGRXPREAMBLE2NB	=	0x522a
                           00522B  2103 _AX5043_TMGRXPREAMBLE3NB	=	0x522b
                           005228  2104 _AX5043_TMGRXRSSINB	=	0x5228
                           005224  2105 _AX5043_TMGRXSETTLENB	=	0x5224
                           005220  2106 _AX5043_TMGTXBOOSTNB	=	0x5220
                           005221  2107 _AX5043_TMGTXSETTLENB	=	0x5221
                           005055  2108 _AX5043_TRKAFSKDEMOD0NB	=	0x5055
                           005054  2109 _AX5043_TRKAFSKDEMOD1NB	=	0x5054
                           005049  2110 _AX5043_TRKAMPLITUDE0NB	=	0x5049
                           005048  2111 _AX5043_TRKAMPLITUDE1NB	=	0x5048
                           005047  2112 _AX5043_TRKDATARATE0NB	=	0x5047
                           005046  2113 _AX5043_TRKDATARATE1NB	=	0x5046
                           005045  2114 _AX5043_TRKDATARATE2NB	=	0x5045
                           005051  2115 _AX5043_TRKFREQ0NB	=	0x5051
                           005050  2116 _AX5043_TRKFREQ1NB	=	0x5050
                           005053  2117 _AX5043_TRKFSKDEMOD0NB	=	0x5053
                           005052  2118 _AX5043_TRKFSKDEMOD1NB	=	0x5052
                           00504B  2119 _AX5043_TRKPHASE0NB	=	0x504b
                           00504A  2120 _AX5043_TRKPHASE1NB	=	0x504a
                           00504F  2121 _AX5043_TRKRFFREQ0NB	=	0x504f
                           00504E  2122 _AX5043_TRKRFFREQ1NB	=	0x504e
                           00504D  2123 _AX5043_TRKRFFREQ2NB	=	0x504d
                           005169  2124 _AX5043_TXPWRCOEFFA0NB	=	0x5169
                           005168  2125 _AX5043_TXPWRCOEFFA1NB	=	0x5168
                           00516B  2126 _AX5043_TXPWRCOEFFB0NB	=	0x516b
                           00516A  2127 _AX5043_TXPWRCOEFFB1NB	=	0x516a
                           00516D  2128 _AX5043_TXPWRCOEFFC0NB	=	0x516d
                           00516C  2129 _AX5043_TXPWRCOEFFC1NB	=	0x516c
                           00516F  2130 _AX5043_TXPWRCOEFFD0NB	=	0x516f
                           00516E  2131 _AX5043_TXPWRCOEFFD1NB	=	0x516e
                           005171  2132 _AX5043_TXPWRCOEFFE0NB	=	0x5171
                           005170  2133 _AX5043_TXPWRCOEFFE1NB	=	0x5170
                           005167  2134 _AX5043_TXRATE0NB	=	0x5167
                           005166  2135 _AX5043_TXRATE1NB	=	0x5166
                           005165  2136 _AX5043_TXRATE2NB	=	0x5165
                           00506B  2137 _AX5043_WAKEUP0NB	=	0x506b
                           00506A  2138 _AX5043_WAKEUP1NB	=	0x506a
                           00506D  2139 _AX5043_WAKEUPFREQ0NB	=	0x506d
                           00506C  2140 _AX5043_WAKEUPFREQ1NB	=	0x506c
                           005069  2141 _AX5043_WAKEUPTIMER0NB	=	0x5069
                           005068  2142 _AX5043_WAKEUPTIMER1NB	=	0x5068
                           00506E  2143 _AX5043_WAKEUPXOEARLYNB	=	0x506e
                           005F11  2144 _AX5043_XTALAMPLNB	=	0x5f11
                           005184  2145 _AX5043_XTALCAPNB	=	0x5184
                           005F10  2146 _AX5043_XTALOSCNB	=	0x5f10
                           00501D  2147 _AX5043_XTALSTATUSNB	=	0x501d
                           005F00  2148 _AX5043_0xF00NB	=	0x5f00
                           005F0C  2149 _AX5043_0xF0CNB	=	0x5f0c
                           005F18  2150 _AX5043_0xF18NB	=	0x5f18
                           005F1C  2151 _AX5043_0xF1CNB	=	0x5f1c
                           005F21  2152 _AX5043_0xF21NB	=	0x5f21
                           005F22  2153 _AX5043_0xF22NB	=	0x5f22
                           005F23  2154 _AX5043_0xF23NB	=	0x5f23
                           005F26  2155 _AX5043_0xF26NB	=	0x5f26
                           005F30  2156 _AX5043_0xF30NB	=	0x5f30
                           005F31  2157 _AX5043_0xF31NB	=	0x5f31
                           005F32  2158 _AX5043_0xF32NB	=	0x5f32
                           005F33  2159 _AX5043_0xF33NB	=	0x5f33
                           005F34  2160 _AX5043_0xF34NB	=	0x5f34
                           005F35  2161 _AX5043_0xF35NB	=	0x5f35
                           005F44  2162 _AX5043_0xF44NB	=	0x5f44
                           005122  2163 _AX5043_AGCAHYST0NB	=	0x5122
                           005132  2164 _AX5043_AGCAHYST1NB	=	0x5132
                           005142  2165 _AX5043_AGCAHYST2NB	=	0x5142
                           005152  2166 _AX5043_AGCAHYST3NB	=	0x5152
                           005120  2167 _AX5043_AGCGAIN0NB	=	0x5120
                           005130  2168 _AX5043_AGCGAIN1NB	=	0x5130
                           005140  2169 _AX5043_AGCGAIN2NB	=	0x5140
                           005150  2170 _AX5043_AGCGAIN3NB	=	0x5150
                           005123  2171 _AX5043_AGCMINMAX0NB	=	0x5123
                           005133  2172 _AX5043_AGCMINMAX1NB	=	0x5133
                           005143  2173 _AX5043_AGCMINMAX2NB	=	0x5143
                           005153  2174 _AX5043_AGCMINMAX3NB	=	0x5153
                           005121  2175 _AX5043_AGCTARGET0NB	=	0x5121
                           005131  2176 _AX5043_AGCTARGET1NB	=	0x5131
                           005141  2177 _AX5043_AGCTARGET2NB	=	0x5141
                           005151  2178 _AX5043_AGCTARGET3NB	=	0x5151
                           00512B  2179 _AX5043_AMPLITUDEGAIN0NB	=	0x512b
                           00513B  2180 _AX5043_AMPLITUDEGAIN1NB	=	0x513b
                           00514B  2181 _AX5043_AMPLITUDEGAIN2NB	=	0x514b
                           00515B  2182 _AX5043_AMPLITUDEGAIN3NB	=	0x515b
                           00512F  2183 _AX5043_BBOFFSRES0NB	=	0x512f
                           00513F  2184 _AX5043_BBOFFSRES1NB	=	0x513f
                           00514F  2185 _AX5043_BBOFFSRES2NB	=	0x514f
                           00515F  2186 _AX5043_BBOFFSRES3NB	=	0x515f
                           005125  2187 _AX5043_DRGAIN0NB	=	0x5125
                           005135  2188 _AX5043_DRGAIN1NB	=	0x5135
                           005145  2189 _AX5043_DRGAIN2NB	=	0x5145
                           005155  2190 _AX5043_DRGAIN3NB	=	0x5155
                           00512E  2191 _AX5043_FOURFSK0NB	=	0x512e
                           00513E  2192 _AX5043_FOURFSK1NB	=	0x513e
                           00514E  2193 _AX5043_FOURFSK2NB	=	0x514e
                           00515E  2194 _AX5043_FOURFSK3NB	=	0x515e
                           00512D  2195 _AX5043_FREQDEV00NB	=	0x512d
                           00513D  2196 _AX5043_FREQDEV01NB	=	0x513d
                           00514D  2197 _AX5043_FREQDEV02NB	=	0x514d
                           00515D  2198 _AX5043_FREQDEV03NB	=	0x515d
                           00512C  2199 _AX5043_FREQDEV10NB	=	0x512c
                           00513C  2200 _AX5043_FREQDEV11NB	=	0x513c
                           00514C  2201 _AX5043_FREQDEV12NB	=	0x514c
                           00515C  2202 _AX5043_FREQDEV13NB	=	0x515c
                           005127  2203 _AX5043_FREQUENCYGAINA0NB	=	0x5127
                           005137  2204 _AX5043_FREQUENCYGAINA1NB	=	0x5137
                           005147  2205 _AX5043_FREQUENCYGAINA2NB	=	0x5147
                           005157  2206 _AX5043_FREQUENCYGAINA3NB	=	0x5157
                           005128  2207 _AX5043_FREQUENCYGAINB0NB	=	0x5128
                           005138  2208 _AX5043_FREQUENCYGAINB1NB	=	0x5138
                           005148  2209 _AX5043_FREQUENCYGAINB2NB	=	0x5148
                           005158  2210 _AX5043_FREQUENCYGAINB3NB	=	0x5158
                           005129  2211 _AX5043_FREQUENCYGAINC0NB	=	0x5129
                           005139  2212 _AX5043_FREQUENCYGAINC1NB	=	0x5139
                           005149  2213 _AX5043_FREQUENCYGAINC2NB	=	0x5149
                           005159  2214 _AX5043_FREQUENCYGAINC3NB	=	0x5159
                           00512A  2215 _AX5043_FREQUENCYGAIND0NB	=	0x512a
                           00513A  2216 _AX5043_FREQUENCYGAIND1NB	=	0x513a
                           00514A  2217 _AX5043_FREQUENCYGAIND2NB	=	0x514a
                           00515A  2218 _AX5043_FREQUENCYGAIND3NB	=	0x515a
                           005116  2219 _AX5043_FREQUENCYLEAKNB	=	0x5116
                           005126  2220 _AX5043_PHASEGAIN0NB	=	0x5126
                           005136  2221 _AX5043_PHASEGAIN1NB	=	0x5136
                           005146  2222 _AX5043_PHASEGAIN2NB	=	0x5146
                           005156  2223 _AX5043_PHASEGAIN3NB	=	0x5156
                           005207  2224 _AX5043_PKTADDR0NB	=	0x5207
                           005206  2225 _AX5043_PKTADDR1NB	=	0x5206
                           005205  2226 _AX5043_PKTADDR2NB	=	0x5205
                           005204  2227 _AX5043_PKTADDR3NB	=	0x5204
                           005200  2228 _AX5043_PKTADDRCFGNB	=	0x5200
                           00520B  2229 _AX5043_PKTADDRMASK0NB	=	0x520b
                           00520A  2230 _AX5043_PKTADDRMASK1NB	=	0x520a
                           005209  2231 _AX5043_PKTADDRMASK2NB	=	0x5209
                           005208  2232 _AX5043_PKTADDRMASK3NB	=	0x5208
                           005201  2233 _AX5043_PKTLENCFGNB	=	0x5201
                           005202  2234 _AX5043_PKTLENOFFSETNB	=	0x5202
                           005203  2235 _AX5043_PKTMAXLENNB	=	0x5203
                           005118  2236 _AX5043_RXPARAMCURSETNB	=	0x5118
                           005117  2237 _AX5043_RXPARAMSETSNB	=	0x5117
                           005124  2238 _AX5043_TIMEGAIN0NB	=	0x5124
                           005134  2239 _AX5043_TIMEGAIN1NB	=	0x5134
                           005144  2240 _AX5043_TIMEGAIN2NB	=	0x5144
                           005154  2241 _AX5043_TIMEGAIN3NB	=	0x5154
                           00FC06  2242 _flash_deviceid	=	0xfc06
                           004F00  2243 _AX5043_0xF00	=	0x4f00
                           004F0C  2244 _AX5043_0xF0C	=	0x4f0c
                           004F10  2245 _AX5043_0xF10	=	0x4f10
                           004F11  2246 _AX5043_0xF11	=	0x4f11
                           004F18  2247 _AX5043_0xF18	=	0x4f18
                           004F1C  2248 _AX5043_0xF1C	=	0x4f1c
                           004F21  2249 _AX5043_0xF21	=	0x4f21
                           004F22  2250 _AX5043_0xF22	=	0x4f22
                           004F23  2251 _AX5043_0xF23	=	0x4f23
                           004F26  2252 _AX5043_0xF26	=	0x4f26
                           004F30  2253 _AX5043_0xF30	=	0x4f30
                           004F31  2254 _AX5043_0xF31	=	0x4f31
                           004F32  2255 _AX5043_0xF32	=	0x4f32
                           004F33  2256 _AX5043_0xF33	=	0x4f33
                           004F34  2257 _AX5043_0xF34	=	0x4f34
                           004F35  2258 _AX5043_0xF35	=	0x4f35
                           004F44  2259 _AX5043_0xF44	=	0x4f44
                           004F0D  2260 _AX5043_REF	=	0x4f0d
                           004F08  2261 _AX5043_POWCTRL1	=	0x4f08
                           004F5F  2262 _AX5043_MODCFGP	=	0x4f5f
                           004F10  2263 _AX5043_XTALOSC	=	0x4f10
                           004F11  2264 _AX5043_XTALAMPL	=	0x4f11
                           00FC00  2265 _flash_calsector	=	0xfc00
      000001                       2266 _UpLinkIsrFlag::
      000001                       2267 	.ds 2
      000003                       2268 _DownLinkIsrFlag::
      000003                       2269 	.ds 2
      000005                       2270 _ACKReceivedFlag::
      000005                       2271 	.ds 2
      000007                       2272 _BackOffCounter::
      000007                       2273 	.ds 1
      000008                       2274 _ACKTimerFlag::
      000008                       2275 	.ds 2
      00000A                       2276 _BeaconRx::
      00000A                       2277 	.ds 29
      000027                       2278 _RxBuffer::
      000027                       2279 	.ds 2
      000029                       2280 _i::
      000029                       2281 	.ds 1
      00002A                       2282 _TxPackageOrig::
      00002A                       2283 	.ds 2
      00002C                       2284 _TxPackageTwin1::
      00002C                       2285 	.ds 2
      00002E                       2286 _TxPackageTwin2::
      00002E                       2287 	.ds 2
      000030                       2288 _AES_KEY::
      000030                       2289 	.ds 2
      000032                       2290 _EncryptedData::
      000032                       2291 	.ds 2
      000034                       2292 _TxPacketLength::
      000034                       2293 	.ds 1
      000035                       2294 _BackOffFlag::
      000035                       2295 	.ds 1
      000036                       2296 _ChannelChangeActive::
      000036                       2297 	.ds 1
      000037                       2298 _TimerDiscarded::
      000037                       2299 	.ds 2
      000039                       2300 _pwrmgmt_irq_pc_65536_373:
      000039                       2301 	.ds 1
      00003A                       2302 _transmit_packet_i_65536_376:
      00003A                       2303 	.ds 1
      00003B                       2304 _transmit_packet_demo_packet__65536_376:
      00003B                       2305 	.ds 23
      000052                       2306 _axradio_statuschange_st_65536_378:
      000052                       2307 	.ds 2
      000054                       2308 _main_PROCD_PayloadLength_65536_406:
      000054                       2309 	.ds 1
      000055                       2310 _main_PROCD_UartRxBuffer_65536_406:
      000055                       2311 	.ds 2
      000057                       2312 _main_UBLOX_Rx_Length_65536_406:
      000057                       2313 	.ds 2
      000059                       2314 _main_UBLOX_Rx_Payload_65536_406:
      000059                       2315 	.ds 2
      00005B                       2316 _main_RxBuff_65536_406:
      00005B                       2317 	.ds 10
                                   2318 ;--------------------------------------------------------
                                   2319 ; absolute external ram data
                                   2320 ;--------------------------------------------------------
                                   2321 	.area XABS    (ABS,XDATA)
                                   2322 ;--------------------------------------------------------
                                   2323 ; external initialized ram data
                                   2324 ;--------------------------------------------------------
                                   2325 	.area XISEG   (XDATA)
      000A2D                       2326 _RfStateMachine::
      000A2D                       2327 	.ds 1
      000A2E                       2328 _GPSMessageReadyFlag::
      000A2E                       2329 	.ds 2
      000A30                       2330 _freq::
      000A30                       2331 	.ds 4
      000A34                       2332 _Tmr0Flg::
      000A34                       2333 	.ds 1
      000A35                       2334 _counter::
      000A35                       2335 	.ds 1
                                   2336 	.area HOME    (CODE)
                                   2337 	.area GSINIT0 (CODE)
                                   2338 	.area GSINIT1 (CODE)
                                   2339 	.area GSINIT2 (CODE)
                                   2340 	.area GSINIT3 (CODE)
                                   2341 	.area GSINIT4 (CODE)
                                   2342 	.area GSINIT5 (CODE)
                                   2343 	.area GSINIT  (CODE)
                                   2344 	.area GSFINAL (CODE)
                                   2345 	.area CSEG    (CODE)
                                   2346 ;--------------------------------------------------------
                                   2347 ; interrupt vector 
                                   2348 ;--------------------------------------------------------
                                   2349 	.area HOME    (CODE)
      000000                       2350 __interrupt_vect:
      000000 02 03 11         [24] 2351 	ljmp	__sdcc_gsinit_startup
      000003 32               [24] 2352 	reti
      000004                       2353 	.ds	7
      00000B 02 00 E8         [24] 2354 	ljmp	_wtimer_irq
      00000E                       2355 	.ds	5
      000013 32               [24] 2356 	reti
      000014                       2357 	.ds	7
      00001B 32               [24] 2358 	reti
      00001C                       2359 	.ds	7
      000023 02 27 A5         [24] 2360 	ljmp	_axradio_isr
      000026                       2361 	.ds	5
      00002B 32               [24] 2362 	reti
      00002C                       2363 	.ds	7
      000033 02 03 9A         [24] 2364 	ljmp	_pwrmgmt_irq
      000036                       2365 	.ds	5
      00003B 32               [24] 2366 	reti
      00003C                       2367 	.ds	7
      000043 32               [24] 2368 	reti
      000044                       2369 	.ds	7
      00004B 32               [24] 2370 	reti
      00004C                       2371 	.ds	7
      000053 32               [24] 2372 	reti
      000054                       2373 	.ds	7
      00005B 02 02 DA         [24] 2374 	ljmp	_uart0_irq
      00005E                       2375 	.ds	5
      000063 02 00 B1         [24] 2376 	ljmp	_uart1_irq
      000066                       2377 	.ds	5
      00006B 32               [24] 2378 	reti
      00006C                       2379 	.ds	7
      000073 32               [24] 2380 	reti
      000074                       2381 	.ds	7
      00007B 32               [24] 2382 	reti
      00007C                       2383 	.ds	7
      000083 32               [24] 2384 	reti
      000084                       2385 	.ds	7
      00008B 32               [24] 2386 	reti
      00008C                       2387 	.ds	7
      000093 32               [24] 2388 	reti
      000094                       2389 	.ds	7
      00009B 32               [24] 2390 	reti
      00009C                       2391 	.ds	7
      0000A3 32               [24] 2392 	reti
      0000A4                       2393 	.ds	7
      0000AB 02 02 A3         [24] 2394 	ljmp	_dbglink_irq
                                   2395 ;--------------------------------------------------------
                                   2396 ; global & static initialisations
                                   2397 ;--------------------------------------------------------
                                   2398 	.area HOME    (CODE)
                                   2399 	.area GSINIT  (CODE)
                                   2400 	.area GSFINAL (CODE)
                                   2401 	.area GSINIT  (CODE)
                                   2402 	.globl __sdcc_gsinit_startup
                                   2403 	.globl __sdcc_program_startup
                                   2404 	.globl __start__stack
                                   2405 	.globl __mcs51_genXINIT
                                   2406 	.globl __mcs51_genXRAMCLEAR
                                   2407 	.globl __mcs51_genRAMCLEAR
                                   2408 ;------------------------------------------------------------
                                   2409 ;Allocation info for local variables in function 'transmit_packet'
                                   2410 ;------------------------------------------------------------
                                   2411 ;i                         Allocated with name '_transmit_packet_i_65536_376'
                                   2412 ;demo_packet_              Allocated with name '_transmit_packet_demo_packet__65536_376'
                                   2413 ;------------------------------------------------------------
                                   2414 ;	..\src\atrs\main.c:81: static uint8_t i = 0;
      000384 90 00 3A         [24] 2415 	mov	dptr,#_transmit_packet_i_65536_376
      000387 E4               [12] 2416 	clr	a
      000388 F0               [24] 2417 	movx	@dptr,a
                                   2418 ;	..\src\atrs\main.c:54: uint16_t __data pkt_counter = 0;/**< \brief amount of packets received */
      000389 E4               [12] 2419 	clr	a
      00038A F5 08            [12] 2420 	mov	_pkt_counter,a
      00038C F5 09            [12] 2421 	mov	(_pkt_counter + 1),a
                                   2422 ;	..\src\atrs\main.c:55: uint8_t __data coldstart = 1; /**< \brief Cold start indicator*/ // caution: initialization with 1 is necessary! Variables are initialized upon _sdcc_external_startup returning 0 -> the coldstart value returned from _sdcc_external startup does not survive in the coldstart case
      00038E 75 0A 01         [24] 2423 	mov	_coldstart,#0x01
                                   2424 	.area GSFINAL (CODE)
      000397 02 00 AE         [24] 2425 	ljmp	__sdcc_program_startup
                                   2426 ;--------------------------------------------------------
                                   2427 ; Home
                                   2428 ;--------------------------------------------------------
                                   2429 	.area HOME    (CODE)
                                   2430 	.area HOME    (CODE)
      0000AE                       2431 __sdcc_program_startup:
      0000AE 02 05 5C         [24] 2432 	ljmp	_main
                                   2433 ;	return from main will return to caller
                                   2434 ;--------------------------------------------------------
                                   2435 ; code
                                   2436 ;--------------------------------------------------------
                                   2437 	.area CSEG    (CODE)
                                   2438 ;------------------------------------------------------------
                                   2439 ;Allocation info for local variables in function 'pwrmgmt_irq'
                                   2440 ;------------------------------------------------------------
                                   2441 ;pc                        Allocated with name '_pwrmgmt_irq_pc_65536_373'
                                   2442 ;------------------------------------------------------------
                                   2443 ;	..\src\atrs\main.c:68: static void pwrmgmt_irq(void) __interrupt(INT_POWERMGMT)
                                   2444 ;	-----------------------------------------
                                   2445 ;	 function pwrmgmt_irq
                                   2446 ;	-----------------------------------------
      00039A                       2447 _pwrmgmt_irq:
                           000007  2448 	ar7 = 0x07
                           000006  2449 	ar6 = 0x06
                           000005  2450 	ar5 = 0x05
                           000004  2451 	ar4 = 0x04
                           000003  2452 	ar3 = 0x03
                           000002  2453 	ar2 = 0x02
                           000001  2454 	ar1 = 0x01
                           000000  2455 	ar0 = 0x00
      00039A C0 E0            [24] 2456 	push	acc
      00039C C0 82            [24] 2457 	push	dpl
      00039E C0 83            [24] 2458 	push	dph
                                   2459 ;	..\src\atrs\main.c:70: uint8_t pc = PCON;
      0003A0 90 00 39         [24] 2460 	mov	dptr,#_pwrmgmt_irq_pc_65536_373
      0003A3 E5 87            [12] 2461 	mov	a,_PCON
      0003A5 F0               [24] 2462 	movx	@dptr,a
                                   2463 ;	..\src\atrs\main.c:71: if (!(pc & 0x80))
      0003A6 E0               [24] 2464 	movx	a,@dptr
      0003A7 20 E7 02         [24] 2465 	jb	acc.7,00102$
                                   2466 ;	..\src\atrs\main.c:72: return;
      0003AA 80 10            [24] 2467 	sjmp	00106$
      0003AC                       2468 00102$:
                                   2469 ;	..\src\atrs\main.c:73: GPIOENABLE = 0;
      0003AC 90 70 0C         [24] 2470 	mov	dptr,#_GPIOENABLE
      0003AF E4               [12] 2471 	clr	a
      0003B0 F0               [24] 2472 	movx	@dptr,a
                                   2473 ;	..\src\atrs\main.c:74: IE = EIE = E2IE = 0;
                                   2474 ;	1-genFromRTrack replaced	mov	_E2IE,#0x00
      0003B1 F5 A0            [12] 2475 	mov	_E2IE,a
                                   2476 ;	1-genFromRTrack replaced	mov	_EIE,#0x00
      0003B3 F5 98            [12] 2477 	mov	_EIE,a
                                   2478 ;	1-genFromRTrack replaced	mov	_IE,#0x00
      0003B5 F5 A8            [12] 2479 	mov	_IE,a
      0003B7                       2480 00104$:
                                   2481 ;	..\src\atrs\main.c:76: PCON |= 0x01;
      0003B7 43 87 01         [24] 2482 	orl	_PCON,#0x01
      0003BA 80 FB            [24] 2483 	sjmp	00104$
      0003BC                       2484 00106$:
                                   2485 ;	..\src\atrs\main.c:77: }
      0003BC D0 83            [24] 2486 	pop	dph
      0003BE D0 82            [24] 2487 	pop	dpl
      0003C0 D0 E0            [24] 2488 	pop	acc
      0003C2 32               [24] 2489 	reti
                                   2490 ;	eliminated unneeded mov psw,# (no regs used in bank)
                                   2491 ;	eliminated unneeded push/pop psw
                                   2492 ;	eliminated unneeded push/pop b
                                   2493 ;------------------------------------------------------------
                                   2494 ;Allocation info for local variables in function 'transmit_packet'
                                   2495 ;------------------------------------------------------------
                                   2496 ;i                         Allocated with name '_transmit_packet_i_65536_376'
                                   2497 ;demo_packet_              Allocated with name '_transmit_packet_demo_packet__65536_376'
                                   2498 ;------------------------------------------------------------
                                   2499 ;	..\src\atrs\main.c:79: void transmit_packet(void)
                                   2500 ;	-----------------------------------------
                                   2501 ;	 function transmit_packet
                                   2502 ;	-----------------------------------------
      0003C3                       2503 _transmit_packet:
                                   2504 ;	..\src\atrs\main.c:83: axradio_set_mode(AXRADIO_MODE_ASYNC_TRANSMIT);
      0003C3 75 82 10         [24] 2505 	mov	dpl,#0x10
      0003C6 12 43 D4         [24] 2506 	lcall	_axradio_set_mode
                                   2507 ;	..\src\atrs\main.c:84: ++pkt_counter;
      0003C9 05 08            [12] 2508 	inc	_pkt_counter
      0003CB E4               [12] 2509 	clr	a
      0003CC B5 08 02         [24] 2510 	cjne	a,_pkt_counter,00109$
      0003CF 05 09            [12] 2511 	inc	(_pkt_counter + 1)
      0003D1                       2512 00109$:
                                   2513 ;	..\src\atrs\main.c:85: memcpy(demo_packet_, demo_packet, sizeof(demo_packet));
      0003D1 90 04 6D         [24] 2514 	mov	dptr,#_memcpy_PARM_2
      0003D4 74 36            [12] 2515 	mov	a,#_demo_packet
      0003D6 F0               [24] 2516 	movx	@dptr,a
      0003D7 74 0A            [12] 2517 	mov	a,#(_demo_packet >> 8)
      0003D9 A3               [24] 2518 	inc	dptr
      0003DA F0               [24] 2519 	movx	@dptr,a
      0003DB E4               [12] 2520 	clr	a
      0003DC A3               [24] 2521 	inc	dptr
      0003DD F0               [24] 2522 	movx	@dptr,a
      0003DE 90 04 70         [24] 2523 	mov	dptr,#_memcpy_PARM_3
      0003E1 74 17            [12] 2524 	mov	a,#0x17
      0003E3 F0               [24] 2525 	movx	@dptr,a
      0003E4 E4               [12] 2526 	clr	a
      0003E5 A3               [24] 2527 	inc	dptr
      0003E6 F0               [24] 2528 	movx	@dptr,a
      0003E7 90 00 3B         [24] 2529 	mov	dptr,#_transmit_packet_demo_packet__65536_376
      0003EA 75 F0 00         [24] 2530 	mov	b,#0x00
      0003ED 12 78 14         [24] 2531 	lcall	_memcpy
                                   2532 ;	..\src\atrs\main.c:86: if (framing_insert_counter) {
      0003F0 90 91 3D         [24] 2533 	mov	dptr,#_framing_insert_counter
      0003F3 E4               [12] 2534 	clr	a
      0003F4 93               [24] 2535 	movc	a,@a+dptr
      0003F5 60 26            [24] 2536 	jz	00102$
                                   2537 ;	..\src\atrs\main.c:87: demo_packet_[framing_counter_pos] = pkt_counter & 0xFF ;
      0003F7 90 91 3E         [24] 2538 	mov	dptr,#_framing_counter_pos
      0003FA E4               [12] 2539 	clr	a
      0003FB 93               [24] 2540 	movc	a,@a+dptr
      0003FC FF               [12] 2541 	mov	r7,a
      0003FD 24 3B            [12] 2542 	add	a,#_transmit_packet_demo_packet__65536_376
      0003FF F5 82            [12] 2543 	mov	dpl,a
      000401 E4               [12] 2544 	clr	a
      000402 34 00            [12] 2545 	addc	a,#(_transmit_packet_demo_packet__65536_376 >> 8)
      000404 F5 83            [12] 2546 	mov	dph,a
      000406 E5 08            [12] 2547 	mov	a,_pkt_counter
      000408 F0               [24] 2548 	movx	@dptr,a
                                   2549 ;	..\src\atrs\main.c:88: demo_packet_[framing_counter_pos+1] = (pkt_counter>>8) & 0xFF;
      000409 0F               [12] 2550 	inc	r7
      00040A EF               [12] 2551 	mov	a,r7
      00040B 33               [12] 2552 	rlc	a
      00040C 95 E0            [12] 2553 	subb	a,acc
      00040E FE               [12] 2554 	mov	r6,a
      00040F EF               [12] 2555 	mov	a,r7
      000410 24 3B            [12] 2556 	add	a,#_transmit_packet_demo_packet__65536_376
      000412 F5 82            [12] 2557 	mov	dpl,a
      000414 EE               [12] 2558 	mov	a,r6
      000415 34 00            [12] 2559 	addc	a,#(_transmit_packet_demo_packet__65536_376 >> 8)
      000417 F5 83            [12] 2560 	mov	dph,a
      000419 E5 09            [12] 2561 	mov	a,(_pkt_counter + 1)
      00041B FF               [12] 2562 	mov	r7,a
      00041C F0               [24] 2563 	movx	@dptr,a
      00041D                       2564 00102$:
                                   2565 ;	..\src\atrs\main.c:90: axradio_transmit(&remoteaddr, demo_packet_, sizeof(demo_packet));
      00041D 90 03 A7         [24] 2566 	mov	dptr,#_axradio_transmit_PARM_2
      000420 74 3B            [12] 2567 	mov	a,#_transmit_packet_demo_packet__65536_376
      000422 F0               [24] 2568 	movx	@dptr,a
      000423 74 00            [12] 2569 	mov	a,#(_transmit_packet_demo_packet__65536_376 >> 8)
      000425 A3               [24] 2570 	inc	dptr
      000426 F0               [24] 2571 	movx	@dptr,a
      000427 E4               [12] 2572 	clr	a
      000428 A3               [24] 2573 	inc	dptr
      000429 F0               [24] 2574 	movx	@dptr,a
      00042A 90 03 AA         [24] 2575 	mov	dptr,#_axradio_transmit_PARM_3
      00042D 74 17            [12] 2576 	mov	a,#0x17
      00042F F0               [24] 2577 	movx	@dptr,a
      000430 E4               [12] 2578 	clr	a
      000431 A3               [24] 2579 	inc	dptr
      000432 F0               [24] 2580 	movx	@dptr,a
      000433 90 91 31         [24] 2581 	mov	dptr,#_remoteaddr
      000436 75 F0 80         [24] 2582 	mov	b,#0x80
                                   2583 ;	..\src\atrs\main.c:92: }
      000439 02 4A 16         [24] 2584 	ljmp	_axradio_transmit
                                   2585 ;------------------------------------------------------------
                                   2586 ;Allocation info for local variables in function 'axradio_statuschange'
                                   2587 ;------------------------------------------------------------
                                   2588 ;st                        Allocated with name '_axradio_statuschange_st_65536_378'
                                   2589 ;i                         Allocated with name '_axradio_statuschange_i_65536_379'
                                   2590 ;------------------------------------------------------------
                                   2591 ;	..\src\atrs\main.c:95: void axradio_statuschange(struct axradio_status __xdata *st)
                                   2592 ;	-----------------------------------------
                                   2593 ;	 function axradio_statuschange
                                   2594 ;	-----------------------------------------
      00043C                       2595 _axradio_statuschange:
      00043C AF 83            [24] 2596 	mov	r7,dph
      00043E E5 82            [12] 2597 	mov	a,dpl
      000440 90 00 52         [24] 2598 	mov	dptr,#_axradio_statuschange_st_65536_378
      000443 F0               [24] 2599 	movx	@dptr,a
      000444 EF               [12] 2600 	mov	a,r7
      000445 A3               [24] 2601 	inc	dptr
      000446 F0               [24] 2602 	movx	@dptr,a
                                   2603 ;	..\src\atrs\main.c:99: switch (st->status)
      000447 90 00 52         [24] 2604 	mov	dptr,#_axradio_statuschange_st_65536_378
      00044A E0               [24] 2605 	movx	a,@dptr
      00044B FE               [12] 2606 	mov	r6,a
      00044C A3               [24] 2607 	inc	dptr
      00044D E0               [24] 2608 	movx	a,@dptr
      00044E FF               [12] 2609 	mov	r7,a
      00044F 8E 82            [24] 2610 	mov	dpl,r6
      000451 8F 83            [24] 2611 	mov	dph,r7
      000453 E0               [24] 2612 	movx	a,@dptr
      000454 FD               [12] 2613 	mov	r5,a
      000455 BD 02 02         [24] 2614 	cjne	r5,#0x02,00207$
      000458 80 45            [24] 2615 	sjmp	00155$
      00045A                       2616 00207$:
      00045A BD 03 02         [24] 2617 	cjne	r5,#0x03,00208$
      00045D 80 05            [24] 2618 	sjmp	00105$
      00045F                       2619 00208$:
                                   2620 ;	..\src\atrs\main.c:102: led0_on();
      00045F BD 04 50         [24] 2621 	cjne	r5,#0x04,00173$
      000462 80 0F            [24] 2622 	sjmp	00120$
      000464                       2623 00105$:
                                   2624 ;	assignBit
      000464 D2 93            [12] 2625 	setb	_PORTC_3
                                   2626 ;	..\src\atrs\main.c:103: if (st->error == AXRADIO_ERR_RETRANSMISSION)
      000466 8E 82            [24] 2627 	mov	dpl,r6
      000468 8F 83            [24] 2628 	mov	dph,r7
      00046A A3               [24] 2629 	inc	dptr
      00046B E0               [24] 2630 	movx	a,@dptr
      00046C FD               [12] 2631 	mov	r5,a
      00046D BD 08 42         [24] 2632 	cjne	r5,#0x08,00173$
                                   2633 ;	..\src\atrs\main.c:104: led2_on();
                                   2634 ;	assignBit
      000470 D2 82            [12] 2635 	setb	_PORTA_2
                                   2636 ;	..\src\atrs\main.c:105: break;
                                   2637 ;	..\src\atrs\main.c:108: led0_off();
      000472 22               [24] 2638 	ret
      000473                       2639 00120$:
                                   2640 ;	assignBit
      000473 C2 93            [12] 2641 	clr	_PORTC_3
                                   2642 ;	..\src\atrs\main.c:109: if (st->error == AXRADIO_ERR_NOERROR) {
      000475 8E 82            [24] 2643 	mov	dpl,r6
      000477 8F 83            [24] 2644 	mov	dph,r7
      000479 A3               [24] 2645 	inc	dptr
      00047A E0               [24] 2646 	movx	a,@dptr
      00047B FD               [12] 2647 	mov	r5,a
      00047C 70 04            [24] 2648 	jnz	00138$
                                   2649 ;	..\src\atrs\main.c:110: led2_off();
                                   2650 ;	assignBit
      00047E C2 82            [12] 2651 	clr	_PORTA_2
      000480 80 05            [24] 2652 	sjmp	00139$
      000482                       2653 00138$:
                                   2654 ;	..\src\atrs\main.c:112: } else if (st->error == AXRADIO_ERR_TIMEOUT) {
      000482 BD 03 02         [24] 2655 	cjne	r5,#0x03,00139$
                                   2656 ;	..\src\atrs\main.c:113: led2_on();
                                   2657 ;	assignBit
      000485 D2 82            [12] 2658 	setb	_PORTA_2
      000487                       2659 00139$:
                                   2660 ;	..\src\atrs\main.c:116: if (st->error == AXRADIO_ERR_BUSY)
      000487 90 00 52         [24] 2661 	mov	dptr,#_axradio_statuschange_st_65536_378
      00048A E0               [24] 2662 	movx	a,@dptr
      00048B FC               [12] 2663 	mov	r4,a
      00048C A3               [24] 2664 	inc	dptr
      00048D E0               [24] 2665 	movx	a,@dptr
      00048E FD               [12] 2666 	mov	r5,a
      00048F 8C 82            [24] 2667 	mov	dpl,r4
      000491 8D 83            [24] 2668 	mov	dph,r5
      000493 A3               [24] 2669 	inc	dptr
      000494 E0               [24] 2670 	movx	a,@dptr
      000495 FD               [12] 2671 	mov	r5,a
      000496 BD 02 03         [24] 2672 	cjne	r5,#0x02,00149$
                                   2673 ;	..\src\atrs\main.c:117: led3_on();
                                   2674 ;	assignBit
      000499 D2 85            [12] 2675 	setb	_PORTA_5
                                   2676 ;	..\src\atrs\main.c:119: led3_off();
      00049B 22               [24] 2677 	ret
      00049C                       2678 00149$:
                                   2679 ;	assignBit
      00049C C2 85            [12] 2680 	clr	_PORTA_5
                                   2681 ;	..\src\atrs\main.c:120: break;
                                   2682 ;	..\src\atrs\main.c:130: case AXRADIO_STAT_CHANNELSTATE:
      00049E 22               [24] 2683 	ret
      00049F                       2684 00155$:
                                   2685 ;	..\src\atrs\main.c:131: if (st->u.cs.busy)
      00049F 74 08            [12] 2686 	mov	a,#0x08
      0004A1 2E               [12] 2687 	add	a,r6
      0004A2 FE               [12] 2688 	mov	r6,a
      0004A3 E4               [12] 2689 	clr	a
      0004A4 3F               [12] 2690 	addc	a,r7
      0004A5 FF               [12] 2691 	mov	r7,a
      0004A6 8E 82            [24] 2692 	mov	dpl,r6
      0004A8 8F 83            [24] 2693 	mov	dph,r7
      0004AA E0               [24] 2694 	movx	a,@dptr
      0004AB 60 03            [24] 2695 	jz	00165$
                                   2696 ;	..\src\atrs\main.c:132: led3_on();
                                   2697 ;	assignBit
      0004AD D2 85            [12] 2698 	setb	_PORTA_5
                                   2699 ;	..\src\atrs\main.c:134: led3_off();
      0004AF 22               [24] 2700 	ret
      0004B0                       2701 00165$:
                                   2702 ;	assignBit
      0004B0 C2 85            [12] 2703 	clr	_PORTA_5
                                   2704 ;	..\src\atrs\main.c:139: }
      0004B2                       2705 00173$:
                                   2706 ;	..\src\atrs\main.c:140: }
      0004B2 22               [24] 2707 	ret
                                   2708 ;------------------------------------------------------------
                                   2709 ;Allocation info for local variables in function '_sdcc_external_startup'
                                   2710 ;------------------------------------------------------------
                                   2711 ;c                         Allocated with name '__sdcc_external_startup_c_131072_403'
                                   2712 ;p                         Allocated with name '__sdcc_external_startup_p_131072_403'
                                   2713 ;c                         Allocated with name '__sdcc_external_startup_c_131072_404'
                                   2714 ;p                         Allocated with name '__sdcc_external_startup_p_131072_404'
                                   2715 ;------------------------------------------------------------
                                   2716 ;	..\src\atrs\main.c:143: uint8_t _sdcc_external_startup(void)
                                   2717 ;	-----------------------------------------
                                   2718 ;	 function _sdcc_external_startup
                                   2719 ;	-----------------------------------------
      0004B3                       2720 __sdcc_external_startup:
                                   2721 ;	..\src\atrs\main.c:146: LPXOSCGM = 0x8A;
      0004B3 90 70 54         [24] 2722 	mov	dptr,#_LPXOSCGM
      0004B6 74 8A            [12] 2723 	mov	a,#0x8a
      0004B8 F0               [24] 2724 	movx	@dptr,a
                                   2725 ;	..\src\atrs\main.c:147: wtimer0_setclksrc(WTIMER0_CLKSRC, WTIMER0_PRESCALER);
      0004B9 74 01            [12] 2726 	mov	a,#0x01
      0004BB 54 07            [12] 2727 	anl	a,#0x07
      0004BD C4               [12] 2728 	swap	a
      0004BE 03               [12] 2729 	rr	a
      0004BF 54 F8            [12] 2730 	anl	a,#0xf8
      0004C1 FF               [12] 2731 	mov	r7,a
      0004C2 43 07 01         [24] 2732 	orl	ar7,#0x01
      0004C5 8F 82            [24] 2733 	mov	dpl,r7
      0004C7 12 74 AF         [24] 2734 	lcall	_wtimer0_setconfig
                                   2735 ;	..\src\atrs\main.c:148: wtimer1_setclksrc(CLKSRC_FRCOSC, 7);
      0004CA 74 07            [12] 2736 	mov	a,#0x07
      0004CC 54 07            [12] 2737 	anl	a,#0x07
      0004CE C4               [12] 2738 	swap	a
      0004CF 03               [12] 2739 	rr	a
      0004D0 54 F8            [12] 2740 	anl	a,#0xf8
      0004D2 F5 82            [12] 2741 	mov	dpl,a
      0004D4 12 75 00         [24] 2742 	lcall	_wtimer1_setconfig
                                   2743 ;	..\src\atrs\main.c:149: LPOSCCONFIG = 0x09; // Slow, PRESC /1, no cal. Does NOT enable LPOSC. LPOSC is enabled upon configuring WTCFGA (MODE_TX_PERIODIC and receive_ack() )
      0004D7 90 70 60         [24] 2744 	mov	dptr,#_LPOSCCONFIG
      0004DA 74 09            [12] 2745 	mov	a,#0x09
      0004DC F0               [24] 2746 	movx	@dptr,a
                                   2747 ;	..\src\atrs\main.c:150: coldstart = !(PCON & 0x40);
      0004DD E5 87            [12] 2748 	mov	a,_PCON
      0004DF 23               [12] 2749 	rl	a
      0004E0 23               [12] 2750 	rl	a
      0004E1 54 01            [12] 2751 	anl	a,#0x01
      0004E3 B4 01 00         [24] 2752 	cjne	a,#0x01,00109$
      0004E6                       2753 00109$:
      0004E6 E4               [12] 2754 	clr	a
      0004E7 33               [12] 2755 	rlc	a
      0004E8 FF               [12] 2756 	mov	r7,a
      0004E9 8F 0A            [24] 2757 	mov	_coldstart,r7
                                   2758 ;	..\src\atrs\main.c:151: ANALOGA = 0x18; // PA[3,4] LPXOSC, other PA are used as digital pins
      0004EB 90 70 07         [24] 2759 	mov	dptr,#_ANALOGA
      0004EE 74 18            [12] 2760 	mov	a,#0x18
      0004F0 F0               [24] 2761 	movx	@dptr,a
                                   2762 ;	..\src\atrs\main.c:152: PORTA = 0x21;
      0004F1 75 80 21         [24] 2763 	mov	_PORTA,#0x21
                                   2764 ;	..\src\atrs\main.c:153: PORTB = 0xFD | (PINB & 0x02); // init LEDs to previous (frozen) state
      0004F4 E5 E8            [12] 2765 	mov	a,_PINB
      0004F6 54 02            [12] 2766 	anl	a,#0x02
      0004F8 44 FD            [12] 2767 	orl	a,#0xfd
      0004FA F5 88            [12] 2768 	mov	_PORTB,a
                                   2769 ;	..\src\atrs\main.c:154: PORTC = 0x00; //
      0004FC 75 90 00         [24] 2770 	mov	_PORTC,#0x00
                                   2771 ;	..\src\atrs\main.c:155: PORTR = 0x0B; //
      0004FF 75 8C 0B         [24] 2772 	mov	_PORTR,#0x0b
                                   2773 ;	..\src\atrs\main.c:156: DIRA = 0x3F; //
      000502 75 89 3F         [24] 2774 	mov	_DIRA,#0x3f
                                   2775 ;	..\src\atrs\main.c:157: DIRB = 0x0C; //  PB1 = LED; PB2 / PB3 are outputs (in case PWRAMP / ANSTSEL are used)
      000505 75 8A 0C         [24] 2776 	mov	_DIRB,#0x0c
                                   2777 ;	..\src\atrs\main.c:158: DIRC = 0x10; //  PC4 = button
      000508 75 8B 10         [24] 2778 	mov	_DIRC,#0x10
                                   2779 ;	..\src\atrs\main.c:159: DIRR = 0x15; //
      00050B 75 8E 15         [24] 2780 	mov	_DIRR,#0x15
                                   2781 ;	..\src\atrs\main.c:160: PALTA  = 0x00;//for comanding cirucuit outputs
      00050E 90 70 08         [24] 2782 	mov	dptr,#_PALTA
      000511 E4               [12] 2783 	clr	a
      000512 F0               [24] 2784 	movx	@dptr,a
                                   2785 ;	..\src\atrs\main.c:161: PALTB  = 0x11; // PB3 as aoutput
      000513 90 70 09         [24] 2786 	mov	dptr,#_PALTB
      000516 74 11            [12] 2787 	mov	a,#0x11
      000518 F0               [24] 2788 	movx	@dptr,a
                                   2789 ;	..\src\atrs\main.c:162: PALTC  = 0x00;
      000519 90 70 0A         [24] 2790 	mov	dptr,#_PALTC
      00051C E4               [12] 2791 	clr	a
      00051D F0               [24] 2792 	movx	@dptr,a
                                   2793 ;	..\src\atrs\main.c:163: PINSEL |= 0xC0;//con esta linea no funciona el gpio, se usara al reves ?
      00051E 90 70 0B         [24] 2794 	mov	dptr,#_PINSEL
      000521 E0               [24] 2795 	movx	a,@dptr
      000522 43 E0 C0         [24] 2796 	orl	acc,#0xc0
      000525 F0               [24] 2797 	movx	@dptr,a
                                   2798 ;	..\src\atrs\main.c:165: RNGMODE = 0x0F;
      000526 90 70 80         [24] 2799 	mov	dptr,#_RNGMODE
      000529 74 0F            [12] 2800 	mov	a,#0x0f
      00052B F0               [24] 2801 	movx	@dptr,a
                                   2802 ;	..\src\atrs\main.c:166: RNGCLKSRC0 = 0x09;
      00052C 90 70 82         [24] 2803 	mov	dptr,#_RNGCLKSRC0
      00052F 74 09            [12] 2804 	mov	a,#0x09
      000531 F0               [24] 2805 	movx	@dptr,a
                                   2806 ;	..\src\atrs\main.c:167: RNGCLKSRC1 = 0x00;
      000532 90 70 83         [24] 2807 	mov	dptr,#_RNGCLKSRC1
      000535 E4               [12] 2808 	clr	a
      000536 F0               [24] 2809 	movx	@dptr,a
                                   2810 ;	..\src\atrs\main.c:168: axradio_setup_pincfg1();
      000537 12 18 F0         [24] 2811 	lcall	_axradio_setup_pincfg1
                                   2812 ;	..\src\atrs\main.c:169: DPS = 0;
      00053A 75 86 00         [24] 2813 	mov	_DPS,#0x00
                                   2814 ;	..\src\atrs\main.c:170: IE = 0x40;
      00053D 75 A8 40         [24] 2815 	mov	_IE,#0x40
                                   2816 ;	..\src\atrs\main.c:171: EIE = 0x13;//habilito interrupcion de UART + timer0 + timer 1
      000540 75 98 13         [24] 2817 	mov	_EIE,#0x13
                                   2818 ;	..\src\atrs\main.c:172: IP_4 = 1; // radio interrupt high priority
                                   2819 ;	assignBit
      000543 D2 BC            [12] 2820 	setb	_IP_4
                                   2821 ;	..\src\atrs\main.c:173: EIP_0 = 0; //timer0 interrupt high priority
                                   2822 ;	assignBit
      000545 C2 B0            [12] 2823 	clr	_EIP_0
                                   2824 ;	..\src\atrs\main.c:174: EIP_1 = 0;
                                   2825 ;	assignBit
      000547 C2 B1            [12] 2826 	clr	_EIP_1
                                   2827 ;	..\src\atrs\main.c:175: E2IE = 0x00;// Keep random number generation interrupt disabled, if needed a random number just check the variable, and wait for it
      000549 75 A0 00         [24] 2828 	mov	_E2IE,#0x00
                                   2829 ;	..\src\atrs\main.c:180: GPIOENABLE = 1; // unfreeze GPIO
      00054C 90 70 0C         [24] 2830 	mov	dptr,#_GPIOENABLE
      00054F 74 01            [12] 2831 	mov	a,#0x01
      000551 F0               [24] 2832 	movx	@dptr,a
                                   2833 ;	..\src\atrs\main.c:184: return !coldstart; // coldstart -> return 0 -> var initialization; start from sleep -> return 1 -> no var initialization
      000552 E5 0A            [12] 2834 	mov	a,_coldstart
      000554 B4 01 00         [24] 2835 	cjne	a,#0x01,00110$
      000557                       2836 00110$:
      000557 E4               [12] 2837 	clr	a
      000558 33               [12] 2838 	rlc	a
                                   2839 ;	..\src\atrs\main.c:186: }
      000559 F5 82            [12] 2840 	mov	dpl,a
      00055B 22               [24] 2841 	ret
                                   2842 ;------------------------------------------------------------
                                   2843 ;Allocation info for local variables in function 'main'
                                   2844 ;------------------------------------------------------------
                                   2845 ;PROCD_PayloadLength       Allocated with name '_main_PROCD_PayloadLength_65536_406'
                                   2846 ;PROCD_UartRxBuffer        Allocated with name '_main_PROCD_UartRxBuffer_65536_406'
                                   2847 ;PROCD_Command             Allocated with name '_main_PROCD_Command_65536_406'
                                   2848 ;c                         Allocated with name '_main_c_65536_406'
                                   2849 ;UBLOX_Rx_Length           Allocated with name '_main_UBLOX_Rx_Length_65536_406'
                                   2850 ;UBLOX_Rx_Payload          Allocated with name '_main_UBLOX_Rx_Payload_65536_406'
                                   2851 ;gold_sequence_tx          Allocated with name '_main_gold_sequence_tx_65536_406'
                                   2852 ;rnd1                      Allocated with name '_main_rnd1_65536_406'
                                   2853 ;rnd2                      Allocated with name '_main_rnd2_65536_406'
                                   2854 ;rnd3                      Allocated with name '_main_rnd3_65536_406'
                                   2855 ;RxBuff                    Allocated with name '_main_RxBuff_65536_406'
                                   2856 ;------------------------------------------------------------
                                   2857 ;	..\src\atrs\main.c:191: void main(void)
                                   2858 ;	-----------------------------------------
                                   2859 ;	 function main
                                   2860 ;	-----------------------------------------
      00055C                       2861 _main:
                                   2862 ;	..\src\atrs\main.c:194: volatile uint8_t    PROCD_PayloadLength= 0;     /**< \var length of the payload received from PROC-D*/
      00055C 90 00 54         [24] 2863 	mov	dptr,#_main_PROCD_PayloadLength_65536_406
      00055F E4               [12] 2864 	clr	a
      000560 F0               [24] 2865 	movx	@dptr,a
                                   2866 ;	..\src\atrs\main.c:204: __xdata uint16_t UBLOX_Rx_Length = 0;               /**< \brief UBLOX GPS received data length */
      000561 90 00 57         [24] 2867 	mov	dptr,#_main_UBLOX_Rx_Length_65536_406
      000564 F0               [24] 2868 	movx	@dptr,a
      000565 A3               [24] 2869 	inc	dptr
      000566 F0               [24] 2870 	movx	@dptr,a
                                   2871 ;	..\src\atrs\main.c:222: __endasm;
                           000000  2872 	G$_start__stack$0$0	= __start__stack
                                   2873 	.globl	G$_start__stack$0$0
                                   2874 ;	..\src\atrs\main.c:226: EA = 1;
                                   2875 ;	assignBit
      000567 D2 AF            [12] 2876 	setb	_EA
                                   2877 ;	..\src\atrs\main.c:227: wtimer_init();
      000569 12 75 DB         [24] 2878 	lcall	_wtimer_init
                                   2879 ;	..\src\atrs\main.c:228: flash_apply_calibration();
      00056C 12 7F FA         [24] 2880 	lcall	_flash_apply_calibration
                                   2881 ;	..\src\atrs\main.c:229: GOLDSEQUENCE_Init(); // ver que pasa con esta funcion*/
      00056F 12 58 08         [24] 2882 	lcall	_GOLDSEQUENCE_Init
                                   2883 ;	..\src\atrs\main.c:230: CLKCON = 0x00;
      000572 75 C6 00         [24] 2884 	mov	_CLKCON,#0x00
                                   2885 ;	..\src\atrs\main.c:235: dbglink_init(); // inicio la UART de debug
      000575 12 79 7B         [24] 2886 	lcall	_dbglink_init
                                   2887 ;	..\src\atrs\main.c:236: dbglink_writestr(" DIRB: ");
      000578 90 8F 06         [24] 2888 	mov	dptr,#___str_0
      00057B 75 F0 80         [24] 2889 	mov	b,#0x80
      00057E 12 82 8A         [24] 2890 	lcall	_dbglink_writestr
                                   2891 ;	..\src\atrs\main.c:237: dbglink_writehex16(DIRB,1,WRNUM_PADZERO);
      000581 AE 8A            [24] 2892 	mov	r6,_DIRB
      000583 7F 00            [12] 2893 	mov	r7,#0x00
      000585 74 08            [12] 2894 	mov	a,#0x08
      000587 C0 E0            [24] 2895 	push	acc
      000589 74 01            [12] 2896 	mov	a,#0x01
      00058B C0 E0            [24] 2897 	push	acc
      00058D 8E 82            [24] 2898 	mov	dpl,r6
      00058F 8F 83            [24] 2899 	mov	dph,r7
      000591 12 87 C9         [24] 2900 	lcall	_dbglink_writehex16
      000594 15 81            [12] 2901 	dec	sp
      000596 15 81            [12] 2902 	dec	sp
                                   2903 ;	..\src\atrs\main.c:238: dbglink_writestr("\r\n PALTB: ");
      000598 90 8F 0E         [24] 2904 	mov	dptr,#___str_1
      00059B 75 F0 80         [24] 2905 	mov	b,#0x80
      00059E 12 82 8A         [24] 2906 	lcall	_dbglink_writestr
                                   2907 ;	..\src\atrs\main.c:239: dbglink_writehex16(PALTB,1,WRNUM_PADZERO);
      0005A1 90 70 09         [24] 2908 	mov	dptr,#_PALTB
      0005A4 E0               [24] 2909 	movx	a,@dptr
      0005A5 FF               [12] 2910 	mov	r7,a
      0005A6 7E 00            [12] 2911 	mov	r6,#0x00
      0005A8 74 08            [12] 2912 	mov	a,#0x08
      0005AA C0 E0            [24] 2913 	push	acc
      0005AC 74 01            [12] 2914 	mov	a,#0x01
      0005AE C0 E0            [24] 2915 	push	acc
      0005B0 8F 82            [24] 2916 	mov	dpl,r7
      0005B2 8E 83            [24] 2917 	mov	dph,r6
      0005B4 12 87 C9         [24] 2918 	lcall	_dbglink_writehex16
      0005B7 15 81            [12] 2919 	dec	sp
      0005B9 15 81            [12] 2920 	dec	sp
                                   2921 ;	..\src\atrs\main.c:240: dbglink_writestr("\r\n PINSEL: ");
      0005BB 90 8F 19         [24] 2922 	mov	dptr,#___str_2
      0005BE 75 F0 80         [24] 2923 	mov	b,#0x80
      0005C1 12 82 8A         [24] 2924 	lcall	_dbglink_writestr
                                   2925 ;	..\src\atrs\main.c:241: dbglink_writehex16(PINSEL,1,WRNUM_PADZERO);
      0005C4 90 70 0B         [24] 2926 	mov	dptr,#_PINSEL
      0005C7 E0               [24] 2927 	movx	a,@dptr
      0005C8 FF               [12] 2928 	mov	r7,a
      0005C9 7E 00            [12] 2929 	mov	r6,#0x00
      0005CB 74 08            [12] 2930 	mov	a,#0x08
      0005CD C0 E0            [24] 2931 	push	acc
      0005CF 74 01            [12] 2932 	mov	a,#0x01
      0005D1 C0 E0            [24] 2933 	push	acc
      0005D3 8F 82            [24] 2934 	mov	dpl,r7
      0005D5 8E 83            [24] 2935 	mov	dph,r6
      0005D7 12 87 C9         [24] 2936 	lcall	_dbglink_writehex16
      0005DA 15 81            [12] 2937 	dec	sp
      0005DC 15 81            [12] 2938 	dec	sp
                                   2939 ;	..\src\atrs\main.c:242: dbglink_writestr("\r\n PORTB_3: ");
      0005DE 90 8F 25         [24] 2940 	mov	dptr,#___str_3
      0005E1 75 F0 80         [24] 2941 	mov	b,#0x80
      0005E4 12 82 8A         [24] 2942 	lcall	_dbglink_writestr
                                   2943 ;	..\src\atrs\main.c:243: dbglink_writehex16(PORTB_3,1,WRNUM_PADZERO);
      0005E7 A2 8B            [12] 2944 	mov	c,_PORTB_3
      0005E9 E4               [12] 2945 	clr	a
      0005EA 33               [12] 2946 	rlc	a
      0005EB FE               [12] 2947 	mov	r6,a
      0005EC 7F 00            [12] 2948 	mov	r7,#0x00
      0005EE 74 08            [12] 2949 	mov	a,#0x08
      0005F0 C0 E0            [24] 2950 	push	acc
      0005F2 74 01            [12] 2951 	mov	a,#0x01
      0005F4 C0 E0            [24] 2952 	push	acc
      0005F6 8E 82            [24] 2953 	mov	dpl,r6
      0005F8 8F 83            [24] 2954 	mov	dph,r7
      0005FA 12 87 C9         [24] 2955 	lcall	_dbglink_writehex16
      0005FD 15 81            [12] 2956 	dec	sp
      0005FF 15 81            [12] 2957 	dec	sp
                                   2958 ;	..\src\atrs\main.c:247: if (coldstart) {
      000601 E5 0A            [12] 2959 	mov	a,_coldstart
      000603 60 36            [24] 2960 	jz	00102$
                                   2961 ;	..\src\atrs\main.c:249: axradio_init();
      000605 12 40 28         [24] 2962 	lcall	_axradio_init
                                   2963 ;	..\src\atrs\main.c:250: axradio_set_local_address(&localaddr);
      000608 90 91 35         [24] 2964 	mov	dptr,#_localaddr
      00060B 75 F0 80         [24] 2965 	mov	b,#0x80
      00060E 12 49 A3         [24] 2966 	lcall	_axradio_set_local_address
                                   2967 ;	..\src\atrs\main.c:251: axradio_set_default_remote_address(&remoteaddr);
      000611 90 91 31         [24] 2968 	mov	dptr,#_remoteaddr
      000614 75 F0 80         [24] 2969 	mov	b,#0x80
      000617 12 49 DE         [24] 2970 	lcall	_axradio_set_default_remote_address
                                   2971 ;	..\src\atrs\main.c:252: delay_ms(lpxosc_settlingtime+1500);//this delay is important to let the radio init
      00061A 90 91 3F         [24] 2972 	mov	dptr,#_lpxosc_settlingtime
      00061D E4               [12] 2973 	clr	a
      00061E 93               [24] 2974 	movc	a,@a+dptr
      00061F FE               [12] 2975 	mov	r6,a
      000620 74 01            [12] 2976 	mov	a,#0x01
      000622 93               [24] 2977 	movc	a,@a+dptr
      000623 FF               [12] 2978 	mov	r7,a
      000624 74 DC            [12] 2979 	mov	a,#0xdc
      000626 2E               [12] 2980 	add	a,r6
      000627 FE               [12] 2981 	mov	r6,a
      000628 74 05            [12] 2982 	mov	a,#0x05
      00062A 3F               [12] 2983 	addc	a,r7
      00062B FF               [12] 2984 	mov	r7,a
      00062C 8E 82            [24] 2985 	mov	dpl,r6
      00062E 8F 83            [24] 2986 	mov	dph,r7
      000630 12 09 31         [24] 2987 	lcall	_delay_ms
                                   2988 ;	..\src\atrs\main.c:253: axradio_set_mode(RADIO_MODE);
      000633 75 82 10         [24] 2989 	mov	dpl,#0x10
      000636 12 43 D4         [24] 2990 	lcall	_axradio_set_mode
      000639 80 05            [24] 2991 	sjmp	00103$
      00063B                       2992 00102$:
                                   2993 ;	..\src\atrs\main.c:258: ax5043_commsleepexit();
      00063B 12 84 BD         [24] 2994 	lcall	_ax5043_commsleepexit
                                   2995 ;	..\src\atrs\main.c:259: IE_4 = TRUE ;
                                   2996 ;	assignBit
      00063E D2 AC            [12] 2997 	setb	_IE_4
      000640                       2998 00103$:
                                   2999 ;	..\src\atrs\main.c:261: axradio_setup_pincfg2();
      000640 12 18 F7         [24] 3000 	lcall	_axradio_setup_pincfg2
                                   3001 ;	..\src\atrs\main.c:265: UART_Proc_PortInit(); // inicio la UART con PC o PROC-D
      000643 12 5E A9         [24] 3002 	lcall	_UART_Proc_PortInit
                                   3003 ;	..\src\atrs\main.c:267: dbglink_writestr(" Comienza el software de testing ");
      000646 90 8F 32         [24] 3004 	mov	dptr,#___str_4
      000649 75 F0 80         [24] 3005 	mov	b,#0x80
      00064C 12 82 8A         [24] 3006 	lcall	_dbglink_writestr
                                   3007 ;	..\src\atrs\main.c:268: dbglink_writestr("\r\n DIRB: ");
      00064F 90 8F 54         [24] 3008 	mov	dptr,#___str_5
      000652 75 F0 80         [24] 3009 	mov	b,#0x80
      000655 12 82 8A         [24] 3010 	lcall	_dbglink_writestr
                                   3011 ;	..\src\atrs\main.c:269: dbglink_writehex16(DIRB,1,WRNUM_PADZERO);
      000658 AE 8A            [24] 3012 	mov	r6,_DIRB
      00065A 7F 00            [12] 3013 	mov	r7,#0x00
      00065C 74 08            [12] 3014 	mov	a,#0x08
      00065E C0 E0            [24] 3015 	push	acc
      000660 74 01            [12] 3016 	mov	a,#0x01
      000662 C0 E0            [24] 3017 	push	acc
      000664 8E 82            [24] 3018 	mov	dpl,r6
      000666 8F 83            [24] 3019 	mov	dph,r7
      000668 12 87 C9         [24] 3020 	lcall	_dbglink_writehex16
      00066B 15 81            [12] 3021 	dec	sp
      00066D 15 81            [12] 3022 	dec	sp
                                   3023 ;	..\src\atrs\main.c:270: dbglink_writestr("\r\n PALTB: ");
      00066F 90 8F 0E         [24] 3024 	mov	dptr,#___str_1
      000672 75 F0 80         [24] 3025 	mov	b,#0x80
      000675 12 82 8A         [24] 3026 	lcall	_dbglink_writestr
                                   3027 ;	..\src\atrs\main.c:271: dbglink_writehex16(PALTB,1,WRNUM_PADZERO);
      000678 90 70 09         [24] 3028 	mov	dptr,#_PALTB
      00067B E0               [24] 3029 	movx	a,@dptr
      00067C FF               [12] 3030 	mov	r7,a
      00067D 7E 00            [12] 3031 	mov	r6,#0x00
      00067F 74 08            [12] 3032 	mov	a,#0x08
      000681 C0 E0            [24] 3033 	push	acc
      000683 74 01            [12] 3034 	mov	a,#0x01
      000685 C0 E0            [24] 3035 	push	acc
      000687 8F 82            [24] 3036 	mov	dpl,r7
      000689 8E 83            [24] 3037 	mov	dph,r6
      00068B 12 87 C9         [24] 3038 	lcall	_dbglink_writehex16
      00068E 15 81            [12] 3039 	dec	sp
      000690 15 81            [12] 3040 	dec	sp
                                   3041 ;	..\src\atrs\main.c:272: dbglink_writestr("\r\n PINSEL: ");
      000692 90 8F 19         [24] 3042 	mov	dptr,#___str_2
      000695 75 F0 80         [24] 3043 	mov	b,#0x80
      000698 12 82 8A         [24] 3044 	lcall	_dbglink_writestr
                                   3045 ;	..\src\atrs\main.c:273: dbglink_writehex16(PINSEL,1,WRNUM_PADZERO);
      00069B 90 70 0B         [24] 3046 	mov	dptr,#_PINSEL
      00069E E0               [24] 3047 	movx	a,@dptr
      00069F FF               [12] 3048 	mov	r7,a
      0006A0 7E 00            [12] 3049 	mov	r6,#0x00
      0006A2 74 08            [12] 3050 	mov	a,#0x08
      0006A4 C0 E0            [24] 3051 	push	acc
      0006A6 74 01            [12] 3052 	mov	a,#0x01
      0006A8 C0 E0            [24] 3053 	push	acc
      0006AA 8F 82            [24] 3054 	mov	dpl,r7
      0006AC 8E 83            [24] 3055 	mov	dph,r6
      0006AE 12 87 C9         [24] 3056 	lcall	_dbglink_writehex16
      0006B1 15 81            [12] 3057 	dec	sp
      0006B3 15 81            [12] 3058 	dec	sp
                                   3059 ;	..\src\atrs\main.c:274: dbglink_writestr("\r\n PORTB_3: ");
      0006B5 90 8F 25         [24] 3060 	mov	dptr,#___str_3
      0006B8 75 F0 80         [24] 3061 	mov	b,#0x80
      0006BB 12 82 8A         [24] 3062 	lcall	_dbglink_writestr
                                   3063 ;	..\src\atrs\main.c:275: dbglink_writehex16(PORTB_3,1,WRNUM_PADZERO);
      0006BE A2 8B            [12] 3064 	mov	c,_PORTB_3
      0006C0 E4               [12] 3065 	clr	a
      0006C1 33               [12] 3066 	rlc	a
      0006C2 FE               [12] 3067 	mov	r6,a
      0006C3 7F 00            [12] 3068 	mov	r7,#0x00
      0006C5 74 08            [12] 3069 	mov	a,#0x08
      0006C7 C0 E0            [24] 3070 	push	acc
      0006C9 74 01            [12] 3071 	mov	a,#0x01
      0006CB C0 E0            [24] 3072 	push	acc
      0006CD 8E 82            [24] 3073 	mov	dpl,r6
      0006CF 8F 83            [24] 3074 	mov	dph,r7
      0006D1 12 87 C9         [24] 3075 	lcall	_dbglink_writehex16
      0006D4 15 81            [12] 3076 	dec	sp
      0006D6 15 81            [12] 3077 	dec	sp
      0006D8                       3078 00126$:
                                   3079 ;	..\src\atrs\main.c:280: i = 0;
      0006D8 90 00 29         [24] 3080 	mov	dptr,#_i
      0006DB E4               [12] 3081 	clr	a
      0006DC F0               [24] 3082 	movx	@dptr,a
                                   3083 ;	..\src\atrs\main.c:281: RxBuff[0] = uart0_rx();
      0006DD 12 7C AF         [24] 3084 	lcall	_uart0_rx
      0006E0 AF 82            [24] 3085 	mov	r7,dpl
                                   3086 ;	..\src\atrs\main.c:284: switch(RxBuff[0])
      0006E2 90 00 5B         [24] 3087 	mov	dptr,#_main_RxBuff_65536_406
      0006E5 EF               [12] 3088 	mov	a,r7
      0006E6 F0               [24] 3089 	movx	@dptr,a
      0006E7 E0               [24] 3090 	movx	a,@dptr
      0006E8 FE               [12] 3091 	mov	r6,a
      0006E9 BF 61 00         [24] 3092 	cjne	r7,#0x61,00155$
      0006EC                       3093 00155$:
      0006EC 50 03            [24] 3094 	jnc	00156$
      0006EE 02 08 EA         [24] 3095 	ljmp	00120$
      0006F1                       3096 00156$:
      0006F1 EE               [12] 3097 	mov	a,r6
      0006F2 24 90            [12] 3098 	add	a,#0xff - 0x6f
      0006F4 50 03            [24] 3099 	jnc	00157$
      0006F6 02 08 EA         [24] 3100 	ljmp	00120$
      0006F9                       3101 00157$:
      0006F9 EE               [12] 3102 	mov	a,r6
      0006FA 24 9F            [12] 3103 	add	a,#0x9f
      0006FC FE               [12] 3104 	mov	r6,a
      0006FD 24 0A            [12] 3105 	add	a,#(00158$-3-.)
      0006FF 83               [24] 3106 	movc	a,@a+pc
      000700 F5 82            [12] 3107 	mov	dpl,a
      000702 EE               [12] 3108 	mov	a,r6
      000703 24 13            [12] 3109 	add	a,#(00159$-3-.)
      000705 83               [24] 3110 	movc	a,@a+pc
      000706 F5 83            [12] 3111 	mov	dph,a
      000708 E4               [12] 3112 	clr	a
      000709 73               [24] 3113 	jmp	@a+dptr
      00070A                       3114 00158$:
      00070A 28                    3115 	.db	00104$
      00070B 3C                    3116 	.db	00105$
      00070C 50                    3117 	.db	00106$
      00070D 64                    3118 	.db	00107$
      00070E 78                    3119 	.db	00108$
      00070F 8C                    3120 	.db	00109$
      000710 A0                    3121 	.db	00110$
      000711 B4                    3122 	.db	00111$
      000712 C8                    3123 	.db	00112$
      000713 DC                    3124 	.db	00113$
      000714 F0                    3125 	.db	00114$
      000715 04                    3126 	.db	00115$
      000716 18                    3127 	.db	00116$
      000717 24                    3128 	.db	00117$
      000718 DF                    3129 	.db	00119$
      000719                       3130 00159$:
      000719 07                    3131 	.db	00104$>>8
      00071A 07                    3132 	.db	00105$>>8
      00071B 07                    3133 	.db	00106$>>8
      00071C 07                    3134 	.db	00107$>>8
      00071D 07                    3135 	.db	00108$>>8
      00071E 07                    3136 	.db	00109$>>8
      00071F 07                    3137 	.db	00110$>>8
      000720 07                    3138 	.db	00111$>>8
      000721 07                    3139 	.db	00112$>>8
      000722 07                    3140 	.db	00113$>>8
      000723 07                    3141 	.db	00114$>>8
      000724 08                    3142 	.db	00115$>>8
      000725 08                    3143 	.db	00116$>>8
      000726 08                    3144 	.db	00117$>>8
      000727 08                    3145 	.db	00119$>>8
                                   3146 ;	..\src\atrs\main.c:286: case('a'):
      000728                       3147 00104$:
                                   3148 ;	..\src\atrs\main.c:287: ENABLE_SW_CTRL1
                                   3149 ;	assignBit
      000728 D2 80            [12] 3150 	setb	_PORTA_0
                                   3151 ;	..\src\atrs\main.c:288: uart0_tx('a');
      00072A 75 82 61         [24] 3152 	mov	dpl,#0x61
      00072D 12 7D 6A         [24] 3153 	lcall	_uart0_tx
                                   3154 ;	..\src\atrs\main.c:290: dbglink_writestr("ENABLE_SW_CTRL1 ");
      000730 90 8F 5E         [24] 3155 	mov	dptr,#___str_6
      000733 75 F0 80         [24] 3156 	mov	b,#0x80
      000736 12 82 8A         [24] 3157 	lcall	_dbglink_writestr
                                   3158 ;	..\src\atrs\main.c:292: break;
      000739 02 09 19         [24] 3159 	ljmp	00121$
                                   3160 ;	..\src\atrs\main.c:293: case('b'):
      00073C                       3161 00105$:
                                   3162 ;	..\src\atrs\main.c:294: DISABLE_SW_CTRL1
                                   3163 ;	assignBit
      00073C C2 80            [12] 3164 	clr	_PORTA_0
                                   3165 ;	..\src\atrs\main.c:295: uart0_tx('b');
      00073E 75 82 62         [24] 3166 	mov	dpl,#0x62
      000741 12 7D 6A         [24] 3167 	lcall	_uart0_tx
                                   3168 ;	..\src\atrs\main.c:297: dbglink_writestr("DISABLE_SW_CTRL1  ");
      000744 90 8F 6F         [24] 3169 	mov	dptr,#___str_7
      000747 75 F0 80         [24] 3170 	mov	b,#0x80
      00074A 12 82 8A         [24] 3171 	lcall	_dbglink_writestr
                                   3172 ;	..\src\atrs\main.c:299: break;
      00074D 02 09 19         [24] 3173 	ljmp	00121$
                                   3174 ;	..\src\atrs\main.c:300: case('c'):
      000750                       3175 00106$:
                                   3176 ;	..\src\atrs\main.c:301: ENABLE_GPS
                                   3177 ;	assignBit
      000750 D2 81            [12] 3178 	setb	_PORTA_1
                                   3179 ;	..\src\atrs\main.c:302: uart0_tx('c');
      000752 75 82 63         [24] 3180 	mov	dpl,#0x63
      000755 12 7D 6A         [24] 3181 	lcall	_uart0_tx
                                   3182 ;	..\src\atrs\main.c:304: dbglink_writestr("ENABLE_GPS ");
      000758 90 8F 82         [24] 3183 	mov	dptr,#___str_8
      00075B 75 F0 80         [24] 3184 	mov	b,#0x80
      00075E 12 82 8A         [24] 3185 	lcall	_dbglink_writestr
                                   3186 ;	..\src\atrs\main.c:306: break;
      000761 02 09 19         [24] 3187 	ljmp	00121$
                                   3188 ;	..\src\atrs\main.c:307: case('d'):
      000764                       3189 00107$:
                                   3190 ;	..\src\atrs\main.c:308: DISABLE_GPS
                                   3191 ;	assignBit
      000764 C2 81            [12] 3192 	clr	_PORTA_1
                                   3193 ;	..\src\atrs\main.c:309: uart0_tx('d');
      000766 75 82 64         [24] 3194 	mov	dpl,#0x64
      000769 12 7D 6A         [24] 3195 	lcall	_uart0_tx
                                   3196 ;	..\src\atrs\main.c:311: dbglink_writestr("DISABLE_GPS ");
      00076C 90 8F 8E         [24] 3197 	mov	dptr,#___str_9
      00076F 75 F0 80         [24] 3198 	mov	b,#0x80
      000772 12 82 8A         [24] 3199 	lcall	_dbglink_writestr
                                   3200 ;	..\src\atrs\main.c:313: break;
      000775 02 09 19         [24] 3201 	ljmp	00121$
                                   3202 ;	..\src\atrs\main.c:314: case('e'):
      000778                       3203 00108$:
                                   3204 ;	..\src\atrs\main.c:315: ENABLE_LNA
                                   3205 ;	assignBit
      000778 D2 82            [12] 3206 	setb	_PORTA_2
                                   3207 ;	..\src\atrs\main.c:316: uart0_tx('e');
      00077A 75 82 65         [24] 3208 	mov	dpl,#0x65
      00077D 12 7D 6A         [24] 3209 	lcall	_uart0_tx
                                   3210 ;	..\src\atrs\main.c:318: dbglink_writestr("ENABLE_LNA ");
      000780 90 8F 9B         [24] 3211 	mov	dptr,#___str_10
      000783 75 F0 80         [24] 3212 	mov	b,#0x80
      000786 12 82 8A         [24] 3213 	lcall	_dbglink_writestr
                                   3214 ;	..\src\atrs\main.c:320: break;
      000789 02 09 19         [24] 3215 	ljmp	00121$
                                   3216 ;	..\src\atrs\main.c:321: case('f'):
      00078C                       3217 00109$:
                                   3218 ;	..\src\atrs\main.c:322: DISABLE_LNA
                                   3219 ;	assignBit
      00078C C2 82            [12] 3220 	clr	_PORTA_2
                                   3221 ;	..\src\atrs\main.c:323: uart0_tx('f');
      00078E 75 82 66         [24] 3222 	mov	dpl,#0x66
      000791 12 7D 6A         [24] 3223 	lcall	_uart0_tx
                                   3224 ;	..\src\atrs\main.c:325: dbglink_writestr("DISABLE_LNA ");
      000794 90 8F A7         [24] 3225 	mov	dptr,#___str_11
      000797 75 F0 80         [24] 3226 	mov	b,#0x80
      00079A 12 82 8A         [24] 3227 	lcall	_dbglink_writestr
                                   3228 ;	..\src\atrs\main.c:327: break;
      00079D 02 09 19         [24] 3229 	ljmp	00121$
                                   3230 ;	..\src\atrs\main.c:328: case('g'):
      0007A0                       3231 00110$:
                                   3232 ;	..\src\atrs\main.c:329: GPS_WAKEUP
                                   3233 ;	assignBit
      0007A0 D2 84            [12] 3234 	setb	_PORTA_4
                                   3235 ;	..\src\atrs\main.c:330: uart0_tx('g');
      0007A2 75 82 67         [24] 3236 	mov	dpl,#0x67
      0007A5 12 7D 6A         [24] 3237 	lcall	_uart0_tx
                                   3238 ;	..\src\atrs\main.c:332: dbglink_writestr("GPS_WAKEUP ");
      0007A8 90 8F B4         [24] 3239 	mov	dptr,#___str_12
      0007AB 75 F0 80         [24] 3240 	mov	b,#0x80
      0007AE 12 82 8A         [24] 3241 	lcall	_dbglink_writestr
                                   3242 ;	..\src\atrs\main.c:334: break;
      0007B1 02 09 19         [24] 3243 	ljmp	00121$
                                   3244 ;	..\src\atrs\main.c:335: case('h'):
      0007B4                       3245 00111$:
                                   3246 ;	..\src\atrs\main.c:336: GPS_TURNOFF
                                   3247 ;	assignBit
      0007B4 C2 84            [12] 3248 	clr	_PORTA_4
                                   3249 ;	..\src\atrs\main.c:337: uart0_tx('h');
      0007B6 75 82 68         [24] 3250 	mov	dpl,#0x68
      0007B9 12 7D 6A         [24] 3251 	lcall	_uart0_tx
                                   3252 ;	..\src\atrs\main.c:339: dbglink_writestr("GPS_TURNOFF ");
      0007BC 90 8F C0         [24] 3253 	mov	dptr,#___str_13
      0007BF 75 F0 80         [24] 3254 	mov	b,#0x80
      0007C2 12 82 8A         [24] 3255 	lcall	_dbglink_writestr
                                   3256 ;	..\src\atrs\main.c:341: break;
      0007C5 02 09 19         [24] 3257 	ljmp	00121$
                                   3258 ;	..\src\atrs\main.c:342: case('i'):
      0007C8                       3259 00112$:
                                   3260 ;	..\src\atrs\main.c:343: GPS_RESET_ON
                                   3261 ;	assignBit
      0007C8 D2 85            [12] 3262 	setb	_PORTA_5
                                   3263 ;	..\src\atrs\main.c:344: uart0_tx('i');
      0007CA 75 82 69         [24] 3264 	mov	dpl,#0x69
      0007CD 12 7D 6A         [24] 3265 	lcall	_uart0_tx
                                   3266 ;	..\src\atrs\main.c:346: dbglink_writestr("GPS_RESET_ON ");
      0007D0 90 8F CD         [24] 3267 	mov	dptr,#___str_14
      0007D3 75 F0 80         [24] 3268 	mov	b,#0x80
      0007D6 12 82 8A         [24] 3269 	lcall	_dbglink_writestr
                                   3270 ;	..\src\atrs\main.c:348: break;
      0007D9 02 09 19         [24] 3271 	ljmp	00121$
                                   3272 ;	..\src\atrs\main.c:349: case('j'):
      0007DC                       3273 00113$:
                                   3274 ;	..\src\atrs\main.c:350: GPS_RESET_OFF
                                   3275 ;	assignBit
      0007DC C2 85            [12] 3276 	clr	_PORTA_5
                                   3277 ;	..\src\atrs\main.c:351: uart0_tx('j');
      0007DE 75 82 6A         [24] 3278 	mov	dpl,#0x6a
      0007E1 12 7D 6A         [24] 3279 	lcall	_uart0_tx
                                   3280 ;	..\src\atrs\main.c:353: dbglink_writestr("GPS_RESET_OFF ");
      0007E4 90 8F DB         [24] 3281 	mov	dptr,#___str_15
      0007E7 75 F0 80         [24] 3282 	mov	b,#0x80
      0007EA 12 82 8A         [24] 3283 	lcall	_dbglink_writestr
                                   3284 ;	..\src\atrs\main.c:355: break;
      0007ED 02 09 19         [24] 3285 	ljmp	00121$
                                   3286 ;	..\src\atrs\main.c:356: case('k'):
      0007F0                       3287 00114$:
                                   3288 ;	..\src\atrs\main.c:357: ENABLE_PA_3_6V
                                   3289 ;	assignBit
      0007F0 D2 94            [12] 3290 	setb	_PORTC_4
                                   3291 ;	..\src\atrs\main.c:359: uart0_tx('k');
      0007F2 75 82 6B         [24] 3292 	mov	dpl,#0x6b
      0007F5 12 7D 6A         [24] 3293 	lcall	_uart0_tx
                                   3294 ;	..\src\atrs\main.c:360: dbglink_writestr("ENABLE_PA_3.6V ");
      0007F8 90 8F EA         [24] 3295 	mov	dptr,#___str_16
      0007FB 75 F0 80         [24] 3296 	mov	b,#0x80
      0007FE 12 82 8A         [24] 3297 	lcall	_dbglink_writestr
                                   3298 ;	..\src\atrs\main.c:362: break;
      000801 02 09 19         [24] 3299 	ljmp	00121$
                                   3300 ;	..\src\atrs\main.c:363: case('l'):
      000804                       3301 00115$:
                                   3302 ;	..\src\atrs\main.c:364: DISABLE_PA_3_6V
                                   3303 ;	assignBit
      000804 C2 94            [12] 3304 	clr	_PORTC_4
                                   3305 ;	..\src\atrs\main.c:365: uart0_tx('l');
      000806 75 82 6C         [24] 3306 	mov	dpl,#0x6c
      000809 12 7D 6A         [24] 3307 	lcall	_uart0_tx
                                   3308 ;	..\src\atrs\main.c:367: dbglink_writestr("DISABLE_PA_3.6V ");
      00080C 90 8F FA         [24] 3309 	mov	dptr,#___str_17
      00080F 75 F0 80         [24] 3310 	mov	b,#0x80
      000812 12 82 8A         [24] 3311 	lcall	_dbglink_writestr
                                   3312 ;	..\src\atrs\main.c:369: break;
      000815 02 09 19         [24] 3313 	ljmp	00121$
                                   3314 ;	..\src\atrs\main.c:370: case('m'):
      000818                       3315 00116$:
                                   3316 ;	..\src\atrs\main.c:371: UBLOX_GPS_PortInit();
      000818 12 66 2B         [24] 3317 	lcall	_UBLOX_GPS_PortInit
                                   3318 ;	..\src\atrs\main.c:372: uart0_tx('m');
      00081B 75 82 6D         [24] 3319 	mov	dpl,#0x6d
      00081E 12 7D 6A         [24] 3320 	lcall	_uart0_tx
                                   3321 ;	..\src\atrs\main.c:373: break;
      000821 02 09 19         [24] 3322 	ljmp	00121$
                                   3323 ;	..\src\atrs\main.c:374: case('n'):
      000824                       3324 00117$:
                                   3325 ;	..\src\atrs\main.c:375: UBLOX_GPS_SendCommand_WaitACK(eNAV,POSLLH,0x00,UBLOX_Rx_Payload,ANS,&UBLOX_Rx_Length);
      000824 90 00 59         [24] 3326 	mov	dptr,#_main_UBLOX_Rx_Payload_65536_406
      000827 E0               [24] 3327 	movx	a,@dptr
      000828 FE               [12] 3328 	mov	r6,a
      000829 A3               [24] 3329 	inc	dptr
      00082A E0               [24] 3330 	movx	a,@dptr
      00082B FF               [12] 3331 	mov	r7,a
      00082C 8E 03            [24] 3332 	mov	ar3,r6
      00082E 8F 04            [24] 3333 	mov	ar4,r7
      000830 7D 00            [12] 3334 	mov	r5,#0x00
      000832 90 04 3E         [24] 3335 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_2
      000835 74 02            [12] 3336 	mov	a,#0x02
      000837 F0               [24] 3337 	movx	@dptr,a
      000838 90 04 3F         [24] 3338 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_3
      00083B E4               [12] 3339 	clr	a
      00083C F0               [24] 3340 	movx	@dptr,a
      00083D A3               [24] 3341 	inc	dptr
      00083E F0               [24] 3342 	movx	@dptr,a
      00083F 90 04 41         [24] 3343 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_4
      000842 EB               [12] 3344 	mov	a,r3
      000843 F0               [24] 3345 	movx	@dptr,a
      000844 EC               [12] 3346 	mov	a,r4
      000845 A3               [24] 3347 	inc	dptr
      000846 F0               [24] 3348 	movx	@dptr,a
      000847 ED               [12] 3349 	mov	a,r5
      000848 A3               [24] 3350 	inc	dptr
      000849 F0               [24] 3351 	movx	@dptr,a
      00084A 90 04 44         [24] 3352 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_5
      00084D F0               [24] 3353 	movx	@dptr,a
      00084E 90 04 45         [24] 3354 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_6
      000851 74 57            [12] 3355 	mov	a,#_main_UBLOX_Rx_Length_65536_406
      000853 F0               [24] 3356 	movx	@dptr,a
      000854 74 00            [12] 3357 	mov	a,#(_main_UBLOX_Rx_Length_65536_406 >> 8)
      000856 A3               [24] 3358 	inc	dptr
      000857 F0               [24] 3359 	movx	@dptr,a
      000858 E4               [12] 3360 	clr	a
      000859 A3               [24] 3361 	inc	dptr
      00085A F0               [24] 3362 	movx	@dptr,a
      00085B 75 82 01         [24] 3363 	mov	dpl,#0x01
      00085E C0 07            [24] 3364 	push	ar7
      000860 C0 06            [24] 3365 	push	ar6
      000862 12 6A 1F         [24] 3366 	lcall	_UBLOX_GPS_SendCommand_WaitACK
                                   3367 ;	..\src\atrs\main.c:377: dbglink_writestr(" GPS payload: ");
      000865 90 90 0B         [24] 3368 	mov	dptr,#___str_18
      000868 75 F0 80         [24] 3369 	mov	b,#0x80
      00086B 12 82 8A         [24] 3370 	lcall	_dbglink_writestr
      00086E D0 06            [24] 3371 	pop	ar6
      000870 D0 07            [24] 3372 	pop	ar7
                                   3373 ;	..\src\atrs\main.c:378: for(i=0;i<UBLOX_Rx_Length;i++)
      000872 90 00 29         [24] 3374 	mov	dptr,#_i
      000875 E4               [12] 3375 	clr	a
      000876 F0               [24] 3376 	movx	@dptr,a
      000877                       3377 00124$:
      000877 90 00 29         [24] 3378 	mov	dptr,#_i
      00087A E0               [24] 3379 	movx	a,@dptr
      00087B FD               [12] 3380 	mov	r5,a
      00087C 90 00 57         [24] 3381 	mov	dptr,#_main_UBLOX_Rx_Length_65536_406
      00087F E0               [24] 3382 	movx	a,@dptr
      000880 FB               [12] 3383 	mov	r3,a
      000881 A3               [24] 3384 	inc	dptr
      000882 E0               [24] 3385 	movx	a,@dptr
      000883 FC               [12] 3386 	mov	r4,a
      000884 7A 00            [12] 3387 	mov	r2,#0x00
      000886 C3               [12] 3388 	clr	c
      000887 ED               [12] 3389 	mov	a,r5
      000888 9B               [12] 3390 	subb	a,r3
      000889 EA               [12] 3391 	mov	a,r2
      00088A 9C               [12] 3392 	subb	a,r4
      00088B 50 44            [24] 3393 	jnc	00118$
                                   3394 ;	..\src\atrs\main.c:380: dbglink_writestr(" ");
      00088D 90 90 1A         [24] 3395 	mov	dptr,#___str_19
      000890 75 F0 80         [24] 3396 	mov	b,#0x80
      000893 C0 07            [24] 3397 	push	ar7
      000895 C0 06            [24] 3398 	push	ar6
      000897 12 82 8A         [24] 3399 	lcall	_dbglink_writestr
      00089A D0 06            [24] 3400 	pop	ar6
      00089C D0 07            [24] 3401 	pop	ar7
                                   3402 ;	..\src\atrs\main.c:381: dbglink_writehex16(UBLOX_Rx_Payload[i],1,WRNUM_PADZERO);
      00089E 90 00 29         [24] 3403 	mov	dptr,#_i
      0008A1 E0               [24] 3404 	movx	a,@dptr
      0008A2 2E               [12] 3405 	add	a,r6
      0008A3 F5 82            [12] 3406 	mov	dpl,a
      0008A5 E4               [12] 3407 	clr	a
      0008A6 3F               [12] 3408 	addc	a,r7
      0008A7 F5 83            [12] 3409 	mov	dph,a
      0008A9 E0               [24] 3410 	movx	a,@dptr
      0008AA FD               [12] 3411 	mov	r5,a
      0008AB 7C 00            [12] 3412 	mov	r4,#0x00
      0008AD C0 07            [24] 3413 	push	ar7
      0008AF C0 06            [24] 3414 	push	ar6
      0008B1 74 08            [12] 3415 	mov	a,#0x08
      0008B3 C0 E0            [24] 3416 	push	acc
      0008B5 74 01            [12] 3417 	mov	a,#0x01
      0008B7 C0 E0            [24] 3418 	push	acc
      0008B9 8D 82            [24] 3419 	mov	dpl,r5
      0008BB 8C 83            [24] 3420 	mov	dph,r4
      0008BD 12 87 C9         [24] 3421 	lcall	_dbglink_writehex16
      0008C0 15 81            [12] 3422 	dec	sp
      0008C2 15 81            [12] 3423 	dec	sp
      0008C4 D0 06            [24] 3424 	pop	ar6
      0008C6 D0 07            [24] 3425 	pop	ar7
                                   3426 ;	..\src\atrs\main.c:378: for(i=0;i<UBLOX_Rx_Length;i++)
      0008C8 90 00 29         [24] 3427 	mov	dptr,#_i
      0008CB E0               [24] 3428 	movx	a,@dptr
      0008CC 24 01            [12] 3429 	add	a,#0x01
      0008CE F0               [24] 3430 	movx	@dptr,a
      0008CF 80 A6            [24] 3431 	sjmp	00124$
      0008D1                       3432 00118$:
                                   3433 ;	..\src\atrs\main.c:383: uart0_tx('n');
      0008D1 75 82 6E         [24] 3434 	mov	dpl,#0x6e
      0008D4 12 7D 6A         [24] 3435 	lcall	_uart0_tx
                                   3436 ;	..\src\atrs\main.c:385: uart0_tx('n');
      0008D7 75 82 6E         [24] 3437 	mov	dpl,#0x6e
      0008DA 12 7D 6A         [24] 3438 	lcall	_uart0_tx
                                   3439 ;	..\src\atrs\main.c:386: break;
                                   3440 ;	..\src\atrs\main.c:387: case('o'):
      0008DD 80 3A            [24] 3441 	sjmp	00121$
      0008DF                       3442 00119$:
                                   3443 ;	..\src\atrs\main.c:388: transmit_packet();
      0008DF 12 03 C3         [24] 3444 	lcall	_transmit_packet
                                   3445 ;	..\src\atrs\main.c:389: uart0_tx('o');
      0008E2 75 82 6F         [24] 3446 	mov	dpl,#0x6f
      0008E5 12 7D 6A         [24] 3447 	lcall	_uart0_tx
                                   3448 ;	..\src\atrs\main.c:390: break;
                                   3449 ;	..\src\atrs\main.c:391: default:
      0008E8 80 2F            [24] 3450 	sjmp	00121$
      0008EA                       3451 00120$:
                                   3452 ;	..\src\atrs\main.c:393: dbglink_writestr(" caracter erroneo recibido ");
      0008EA 90 90 1C         [24] 3453 	mov	dptr,#___str_20
      0008ED 75 F0 80         [24] 3454 	mov	b,#0x80
      0008F0 12 82 8A         [24] 3455 	lcall	_dbglink_writestr
                                   3456 ;	..\src\atrs\main.c:395: dbglink_writehex16(RxBuff[0],1,WRNUM_PADZERO);
      0008F3 90 00 5B         [24] 3457 	mov	dptr,#_main_RxBuff_65536_406
      0008F6 E0               [24] 3458 	movx	a,@dptr
      0008F7 FF               [12] 3459 	mov	r7,a
      0008F8 7E 00            [12] 3460 	mov	r6,#0x00
      0008FA 74 08            [12] 3461 	mov	a,#0x08
      0008FC C0 E0            [24] 3462 	push	acc
      0008FE 74 01            [12] 3463 	mov	a,#0x01
      000900 C0 E0            [24] 3464 	push	acc
      000902 8F 82            [24] 3465 	mov	dpl,r7
      000904 8E 83            [24] 3466 	mov	dph,r6
      000906 12 87 C9         [24] 3467 	lcall	_dbglink_writehex16
      000909 15 81            [12] 3468 	dec	sp
      00090B 15 81            [12] 3469 	dec	sp
                                   3470 ;	..\src\atrs\main.c:396: uart0_tx('F');
      00090D 75 82 46         [24] 3471 	mov	dpl,#0x46
      000910 12 7D 6A         [24] 3472 	lcall	_uart0_tx
                                   3473 ;	..\src\atrs\main.c:397: uart0_tx('F');
      000913 75 82 46         [24] 3474 	mov	dpl,#0x46
      000916 12 7D 6A         [24] 3475 	lcall	_uart0_tx
                                   3476 ;	..\src\atrs\main.c:399: }
      000919                       3477 00121$:
                                   3478 ;	..\src\atrs\main.c:401: wtimer_runcallbacks();
      000919 12 77 0F         [24] 3479 	lcall	_wtimer_runcallbacks
                                   3480 ;	..\src\atrs\main.c:402: wtimer_idle(WTFLAG_CANSTANDBY);
      00091C 75 82 02         [24] 3481 	mov	dpl,#0x02
      00091F 12 77 90         [24] 3482 	lcall	_wtimer_idle
                                   3483 ;	..\src\atrs\main.c:403: IE_4 = 1;
                                   3484 ;	assignBit
      000922 D2 AC            [12] 3485 	setb	_IE_4
                                   3486 ;	..\src\atrs\main.c:404: EA = 1;
                                   3487 ;	assignBit
      000924 D2 AF            [12] 3488 	setb	_EA
                                   3489 ;	..\src\atrs\main.c:406: }
      000926 02 06 D8         [24] 3490 	ljmp	00126$
                                   3491 	.area CSEG    (CODE)
                                   3492 	.area CONST   (CODE)
                                   3493 	.area CONST   (CODE)
      008F06                       3494 ___str_0:
      008F06 20 44 49 52 42 3A 20  3495 	.ascii " DIRB: "
      008F0D 00                    3496 	.db 0x00
                                   3497 	.area CSEG    (CODE)
                                   3498 	.area CONST   (CODE)
      008F0E                       3499 ___str_1:
      008F0E 0D                    3500 	.db 0x0d
      008F0F 0A                    3501 	.db 0x0a
      008F10 20 50 41 4C 54 42 3A  3502 	.ascii " PALTB: "
             20
      008F18 00                    3503 	.db 0x00
                                   3504 	.area CSEG    (CODE)
                                   3505 	.area CONST   (CODE)
      008F19                       3506 ___str_2:
      008F19 0D                    3507 	.db 0x0d
      008F1A 0A                    3508 	.db 0x0a
      008F1B 20 50 49 4E 53 45 4C  3509 	.ascii " PINSEL: "
             3A 20
      008F24 00                    3510 	.db 0x00
                                   3511 	.area CSEG    (CODE)
                                   3512 	.area CONST   (CODE)
      008F25                       3513 ___str_3:
      008F25 0D                    3514 	.db 0x0d
      008F26 0A                    3515 	.db 0x0a
      008F27 20 50 4F 52 54 42 5F  3516 	.ascii " PORTB_3: "
             33 3A 20
      008F31 00                    3517 	.db 0x00
                                   3518 	.area CSEG    (CODE)
                                   3519 	.area CONST   (CODE)
      008F32                       3520 ___str_4:
      008F32 20 43 6F 6D 69 65 6E  3521 	.ascii " Comienza el software de testing "
             7A 61 20 65 6C 20 73
             6F 66 74 77 61 72 65
             20 64 65 20 74 65 73
             74 69 6E 67 20
      008F53 00                    3522 	.db 0x00
                                   3523 	.area CSEG    (CODE)
                                   3524 	.area CONST   (CODE)
      008F54                       3525 ___str_5:
      008F54 0D                    3526 	.db 0x0d
      008F55 0A                    3527 	.db 0x0a
      008F56 20 44 49 52 42 3A 20  3528 	.ascii " DIRB: "
      008F5D 00                    3529 	.db 0x00
                                   3530 	.area CSEG    (CODE)
                                   3531 	.area CONST   (CODE)
      008F5E                       3532 ___str_6:
      008F5E 45 4E 41 42 4C 45 5F  3533 	.ascii "ENABLE_SW_CTRL1 "
             53 57 5F 43 54 52 4C
             31 20
      008F6E 00                    3534 	.db 0x00
                                   3535 	.area CSEG    (CODE)
                                   3536 	.area CONST   (CODE)
      008F6F                       3537 ___str_7:
      008F6F 44 49 53 41 42 4C 45  3538 	.ascii "DISABLE_SW_CTRL1  "
             5F 53 57 5F 43 54 52
             4C 31 20 20
      008F81 00                    3539 	.db 0x00
                                   3540 	.area CSEG    (CODE)
                                   3541 	.area CONST   (CODE)
      008F82                       3542 ___str_8:
      008F82 45 4E 41 42 4C 45 5F  3543 	.ascii "ENABLE_GPS "
             47 50 53 20
      008F8D 00                    3544 	.db 0x00
                                   3545 	.area CSEG    (CODE)
                                   3546 	.area CONST   (CODE)
      008F8E                       3547 ___str_9:
      008F8E 44 49 53 41 42 4C 45  3548 	.ascii "DISABLE_GPS "
             5F 47 50 53 20
      008F9A 00                    3549 	.db 0x00
                                   3550 	.area CSEG    (CODE)
                                   3551 	.area CONST   (CODE)
      008F9B                       3552 ___str_10:
      008F9B 45 4E 41 42 4C 45 5F  3553 	.ascii "ENABLE_LNA "
             4C 4E 41 20
      008FA6 00                    3554 	.db 0x00
                                   3555 	.area CSEG    (CODE)
                                   3556 	.area CONST   (CODE)
      008FA7                       3557 ___str_11:
      008FA7 44 49 53 41 42 4C 45  3558 	.ascii "DISABLE_LNA "
             5F 4C 4E 41 20
      008FB3 00                    3559 	.db 0x00
                                   3560 	.area CSEG    (CODE)
                                   3561 	.area CONST   (CODE)
      008FB4                       3562 ___str_12:
      008FB4 47 50 53 5F 57 41 4B  3563 	.ascii "GPS_WAKEUP "
             45 55 50 20
      008FBF 00                    3564 	.db 0x00
                                   3565 	.area CSEG    (CODE)
                                   3566 	.area CONST   (CODE)
      008FC0                       3567 ___str_13:
      008FC0 47 50 53 5F 54 55 52  3568 	.ascii "GPS_TURNOFF "
             4E 4F 46 46 20
      008FCC 00                    3569 	.db 0x00
                                   3570 	.area CSEG    (CODE)
                                   3571 	.area CONST   (CODE)
      008FCD                       3572 ___str_14:
      008FCD 47 50 53 5F 52 45 53  3573 	.ascii "GPS_RESET_ON "
             45 54 5F 4F 4E 20
      008FDA 00                    3574 	.db 0x00
                                   3575 	.area CSEG    (CODE)
                                   3576 	.area CONST   (CODE)
      008FDB                       3577 ___str_15:
      008FDB 47 50 53 5F 52 45 53  3578 	.ascii "GPS_RESET_OFF "
             45 54 5F 4F 46 46 20
      008FE9 00                    3579 	.db 0x00
                                   3580 	.area CSEG    (CODE)
                                   3581 	.area CONST   (CODE)
      008FEA                       3582 ___str_16:
      008FEA 45 4E 41 42 4C 45 5F  3583 	.ascii "ENABLE_PA_3.6V "
             50 41 5F 33 2E 36 56
             20
      008FF9 00                    3584 	.db 0x00
                                   3585 	.area CSEG    (CODE)
                                   3586 	.area CONST   (CODE)
      008FFA                       3587 ___str_17:
      008FFA 44 49 53 41 42 4C 45  3588 	.ascii "DISABLE_PA_3.6V "
             5F 50 41 5F 33 2E 36
             56 20
      00900A 00                    3589 	.db 0x00
                                   3590 	.area CSEG    (CODE)
                                   3591 	.area CONST   (CODE)
      00900B                       3592 ___str_18:
      00900B 20 47 50 53 20 70 61  3593 	.ascii " GPS payload: "
             79 6C 6F 61 64 3A 20
      009019 00                    3594 	.db 0x00
                                   3595 	.area CSEG    (CODE)
                                   3596 	.area CONST   (CODE)
      00901A                       3597 ___str_19:
      00901A 20                    3598 	.ascii " "
      00901B 00                    3599 	.db 0x00
                                   3600 	.area CSEG    (CODE)
                                   3601 	.area CONST   (CODE)
      00901C                       3602 ___str_20:
      00901C 20 63 61 72 61 63 74  3603 	.ascii " caracter erroneo recibido "
             65 72 20 65 72 72 6F
             6E 65 6F 20 72 65 63
             69 62 69 64 6F 20
      009037 00                    3604 	.db 0x00
                                   3605 	.area CSEG    (CODE)
                                   3606 	.area XINIT   (CODE)
      009CBE                       3607 __xinit__RfStateMachine:
      009CBE 00                    3608 	.db #0x00	; 0
      009CBF                       3609 __xinit__GPSMessageReadyFlag:
      009CBF 00 00                 3610 	.byte #0x00, #0x00	;  0
      009CC1                       3611 __xinit__freq:
      009CC1 C0 FC 05 1A           3612 	.byte #0xc0, #0xfc, #0x05, #0x1a	; 436600000
      009CC5                       3613 __xinit__Tmr0Flg:
      009CC5 00                    3614 	.db #0x00	; 0
      009CC6                       3615 __xinit__counter:
      009CC6 00                    3616 	.db #0x00	; 0
                                   3617 	.area CABS    (ABS,CODE)
