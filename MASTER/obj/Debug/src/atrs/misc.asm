;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.9.3 #11354 (MINGW32)
;--------------------------------------------------------
	.module misc
	.optsdcc -mmcs51 --model-large
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _ACK_Callback
	.globl _BackOff_Callback
	.globl _GPS_callback
	.globl _TwinMessage2_callback
	.globl _TwinMessage1_callback
	.globl _OriginalMessage_callback
	.globl _crc_crc32_msb
	.globl _dbglink_writenum32
	.globl _dbglink_writestr
	.globl _axradio_transmit
	.globl _axradio_set_channel
	.globl _memcpy
	.globl _wtimer0_remove
	.globl _wtimer_remove
	.globl _wtimer1_addrelative
	.globl _wtimer0_addrelative
	.globl _wtimer0_addabsolute
	.globl _wtimer_runcallbacks
	.globl _wtimer_idle
	.globl _PORTC_7
	.globl _PORTC_6
	.globl _PORTC_5
	.globl _PORTC_4
	.globl _PORTC_3
	.globl _PORTC_2
	.globl _PORTC_1
	.globl _PORTC_0
	.globl _PORTB_7
	.globl _PORTB_6
	.globl _PORTB_5
	.globl _PORTB_4
	.globl _PORTB_3
	.globl _PORTB_2
	.globl _PORTB_1
	.globl _PORTB_0
	.globl _PORTA_7
	.globl _PORTA_6
	.globl _PORTA_5
	.globl _PORTA_4
	.globl _PORTA_3
	.globl _PORTA_2
	.globl _PORTA_1
	.globl _PORTA_0
	.globl _PINC_7
	.globl _PINC_6
	.globl _PINC_5
	.globl _PINC_4
	.globl _PINC_3
	.globl _PINC_2
	.globl _PINC_1
	.globl _PINC_0
	.globl _PINB_7
	.globl _PINB_6
	.globl _PINB_5
	.globl _PINB_4
	.globl _PINB_3
	.globl _PINB_2
	.globl _PINB_1
	.globl _PINB_0
	.globl _PINA_7
	.globl _PINA_6
	.globl _PINA_5
	.globl _PINA_4
	.globl _PINA_3
	.globl _PINA_2
	.globl _PINA_1
	.globl _PINA_0
	.globl _CY
	.globl _AC
	.globl _F0
	.globl _RS1
	.globl _RS0
	.globl _OV
	.globl _F1
	.globl _P
	.globl _IP_7
	.globl _IP_6
	.globl _IP_5
	.globl _IP_4
	.globl _IP_3
	.globl _IP_2
	.globl _IP_1
	.globl _IP_0
	.globl _EA
	.globl _IE_7
	.globl _IE_6
	.globl _IE_5
	.globl _IE_4
	.globl _IE_3
	.globl _IE_2
	.globl _IE_1
	.globl _IE_0
	.globl _EIP_7
	.globl _EIP_6
	.globl _EIP_5
	.globl _EIP_4
	.globl _EIP_3
	.globl _EIP_2
	.globl _EIP_1
	.globl _EIP_0
	.globl _EIE_7
	.globl _EIE_6
	.globl _EIE_5
	.globl _EIE_4
	.globl _EIE_3
	.globl _EIE_2
	.globl _EIE_1
	.globl _EIE_0
	.globl _E2IP_7
	.globl _E2IP_6
	.globl _E2IP_5
	.globl _E2IP_4
	.globl _E2IP_3
	.globl _E2IP_2
	.globl _E2IP_1
	.globl _E2IP_0
	.globl _E2IE_7
	.globl _E2IE_6
	.globl _E2IE_5
	.globl _E2IE_4
	.globl _E2IE_3
	.globl _E2IE_2
	.globl _E2IE_1
	.globl _E2IE_0
	.globl _B_7
	.globl _B_6
	.globl _B_5
	.globl _B_4
	.globl _B_3
	.globl _B_2
	.globl _B_1
	.globl _B_0
	.globl _ACC_7
	.globl _ACC_6
	.globl _ACC_5
	.globl _ACC_4
	.globl _ACC_3
	.globl _ACC_2
	.globl _ACC_1
	.globl _ACC_0
	.globl _WTSTAT
	.globl _WTIRQEN
	.globl _WTEVTD
	.globl _WTEVTD1
	.globl _WTEVTD0
	.globl _WTEVTC
	.globl _WTEVTC1
	.globl _WTEVTC0
	.globl _WTEVTB
	.globl _WTEVTB1
	.globl _WTEVTB0
	.globl _WTEVTA
	.globl _WTEVTA1
	.globl _WTEVTA0
	.globl _WTCNTR1
	.globl _WTCNTB
	.globl _WTCNTB1
	.globl _WTCNTB0
	.globl _WTCNTA
	.globl _WTCNTA1
	.globl _WTCNTA0
	.globl _WTCFGB
	.globl _WTCFGA
	.globl _WDTRESET
	.globl _WDTCFG
	.globl _U1STATUS
	.globl _U1SHREG
	.globl _U1MODE
	.globl _U1CTRL
	.globl _U0STATUS
	.globl _U0SHREG
	.globl _U0MODE
	.globl _U0CTRL
	.globl _T2STATUS
	.globl _T2PERIOD
	.globl _T2PERIOD1
	.globl _T2PERIOD0
	.globl _T2MODE
	.globl _T2CNT
	.globl _T2CNT1
	.globl _T2CNT0
	.globl _T2CLKSRC
	.globl _T1STATUS
	.globl _T1PERIOD
	.globl _T1PERIOD1
	.globl _T1PERIOD0
	.globl _T1MODE
	.globl _T1CNT
	.globl _T1CNT1
	.globl _T1CNT0
	.globl _T1CLKSRC
	.globl _T0STATUS
	.globl _T0PERIOD
	.globl _T0PERIOD1
	.globl _T0PERIOD0
	.globl _T0MODE
	.globl _T0CNT
	.globl _T0CNT1
	.globl _T0CNT0
	.globl _T0CLKSRC
	.globl _SPSTATUS
	.globl _SPSHREG
	.globl _SPMODE
	.globl _SPCLKSRC
	.globl _RADIOSTAT
	.globl _RADIOSTAT1
	.globl _RADIOSTAT0
	.globl _RADIODATA
	.globl _RADIODATA3
	.globl _RADIODATA2
	.globl _RADIODATA1
	.globl _RADIODATA0
	.globl _RADIOADDR
	.globl _RADIOADDR1
	.globl _RADIOADDR0
	.globl _RADIOACC
	.globl _OC1STATUS
	.globl _OC1PIN
	.globl _OC1MODE
	.globl _OC1COMP
	.globl _OC1COMP1
	.globl _OC1COMP0
	.globl _OC0STATUS
	.globl _OC0PIN
	.globl _OC0MODE
	.globl _OC0COMP
	.globl _OC0COMP1
	.globl _OC0COMP0
	.globl _NVSTATUS
	.globl _NVKEY
	.globl _NVDATA
	.globl _NVDATA1
	.globl _NVDATA0
	.globl _NVADDR
	.globl _NVADDR1
	.globl _NVADDR0
	.globl _IC1STATUS
	.globl _IC1MODE
	.globl _IC1CAPT
	.globl _IC1CAPT1
	.globl _IC1CAPT0
	.globl _IC0STATUS
	.globl _IC0MODE
	.globl _IC0CAPT
	.globl _IC0CAPT1
	.globl _IC0CAPT0
	.globl _PORTR
	.globl _PORTC
	.globl _PORTB
	.globl _PORTA
	.globl _PINR
	.globl _PINC
	.globl _PINB
	.globl _PINA
	.globl _DIRR
	.globl _DIRC
	.globl _DIRB
	.globl _DIRA
	.globl _DBGLNKSTAT
	.globl _DBGLNKBUF
	.globl _CODECONFIG
	.globl _CLKSTAT
	.globl _CLKCON
	.globl _ANALOGCOMP
	.globl _ADCCONV
	.globl _ADCCLKSRC
	.globl _ADCCH3CONFIG
	.globl _ADCCH2CONFIG
	.globl _ADCCH1CONFIG
	.globl _ADCCH0CONFIG
	.globl __XPAGE
	.globl _XPAGE
	.globl _SP
	.globl _PSW
	.globl _PCON
	.globl _IP
	.globl _IE
	.globl _EIP
	.globl _EIE
	.globl _E2IP
	.globl _E2IE
	.globl _DPS
	.globl _DPTR1
	.globl _DPTR0
	.globl _DPL1
	.globl _DPL
	.globl _DPH1
	.globl _DPH
	.globl _B
	.globl _ACC
	.globl _AssemblyPacket_PARM_8
	.globl _AssemblyPacket_PARM_7
	.globl _AssemblyPacket_PARM_6
	.globl _AssemblyPacket_PARM_5
	.globl _AssemblyPacket_PARM_4
	.globl _AssemblyPacket_PARM_3
	.globl _AssemblyPacket_PARM_2
	.globl _GetRandomSlots_PARM_3
	.globl _GetRandomSlots_PARM_2
	.globl _swap_variables_PARM_2
	.globl _delay_tx_PARM_3
	.globl _delay_tx_PARM_2
	.globl _GPS_PoolFlag
	.globl _Channel_change
	.globl _ACKTimer
	.globl _BackOff
	.globl _GPS_tmr
	.globl _TwinMessage2_tmr
	.globl _TwinMessage1_tmr
	.globl _OriginalMessage_tmr
	.globl _RNGCLKSRC1
	.globl _RNGCLKSRC0
	.globl _RNGMODE
	.globl _AESOUTADDR
	.globl _AESOUTADDR1
	.globl _AESOUTADDR0
	.globl _AESMODE
	.globl _AESKEYADDR
	.globl _AESKEYADDR1
	.globl _AESKEYADDR0
	.globl _AESINADDR
	.globl _AESINADDR1
	.globl _AESINADDR0
	.globl _AESCURBLOCK
	.globl _AESCONFIG
	.globl _RNGBYTE
	.globl _XTALREADY
	.globl _XTALOSC
	.globl _XTALAMPL
	.globl _SILICONREV
	.globl _SCRATCH3
	.globl _SCRATCH2
	.globl _SCRATCH1
	.globl _SCRATCH0
	.globl _RADIOMUX
	.globl _RADIOFSTATADDR
	.globl _RADIOFSTATADDR1
	.globl _RADIOFSTATADDR0
	.globl _RADIOFDATAADDR
	.globl _RADIOFDATAADDR1
	.globl _RADIOFDATAADDR0
	.globl _OSCRUN
	.globl _OSCREADY
	.globl _OSCFORCERUN
	.globl _OSCCALIB
	.globl _MISCCTRL
	.globl _LPXOSCGM
	.globl _LPOSCREF
	.globl _LPOSCREF1
	.globl _LPOSCREF0
	.globl _LPOSCPER
	.globl _LPOSCPER1
	.globl _LPOSCPER0
	.globl _LPOSCKFILT
	.globl _LPOSCKFILT1
	.globl _LPOSCKFILT0
	.globl _LPOSCFREQ
	.globl _LPOSCFREQ1
	.globl _LPOSCFREQ0
	.globl _LPOSCCONFIG
	.globl _PINSEL
	.globl _PINCHGC
	.globl _PINCHGB
	.globl _PINCHGA
	.globl _PALTRADIO
	.globl _PALTC
	.globl _PALTB
	.globl _PALTA
	.globl _INTCHGC
	.globl _INTCHGB
	.globl _INTCHGA
	.globl _EXTIRQ
	.globl _GPIOENABLE
	.globl _ANALOGA
	.globl _FRCOSCREF
	.globl _FRCOSCREF1
	.globl _FRCOSCREF0
	.globl _FRCOSCPER
	.globl _FRCOSCPER1
	.globl _FRCOSCPER0
	.globl _FRCOSCKFILT
	.globl _FRCOSCKFILT1
	.globl _FRCOSCKFILT0
	.globl _FRCOSCFREQ
	.globl _FRCOSCFREQ1
	.globl _FRCOSCFREQ0
	.globl _FRCOSCCTRL
	.globl _FRCOSCCONFIG
	.globl _DMA1CONFIG
	.globl _DMA1ADDR
	.globl _DMA1ADDR1
	.globl _DMA1ADDR0
	.globl _DMA0CONFIG
	.globl _DMA0ADDR
	.globl _DMA0ADDR1
	.globl _DMA0ADDR0
	.globl _ADCTUNE2
	.globl _ADCTUNE1
	.globl _ADCTUNE0
	.globl _ADCCH3VAL
	.globl _ADCCH3VAL1
	.globl _ADCCH3VAL0
	.globl _ADCCH2VAL
	.globl _ADCCH2VAL1
	.globl _ADCCH2VAL0
	.globl _ADCCH1VAL
	.globl _ADCCH1VAL1
	.globl _ADCCH1VAL0
	.globl _ADCCH0VAL
	.globl _ADCCH0VAL1
	.globl _ADCCH0VAL0
	.globl _delay_ms
	.globl _Channel_change_callback
	.globl _delay_tx
	.globl _InitGPSPolling
	.globl _InitChannelChange
	.globl _StartBackoffTimer
	.globl _StartACKTimer
	.globl _swap_variables
	.globl _GetRandomSlots
	.globl _AssemblyPacket
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
_ACC	=	0x00e0
_B	=	0x00f0
_DPH	=	0x0083
_DPH1	=	0x0085
_DPL	=	0x0082
_DPL1	=	0x0084
_DPTR0	=	0x8382
_DPTR1	=	0x8584
_DPS	=	0x0086
_E2IE	=	0x00a0
_E2IP	=	0x00c0
_EIE	=	0x0098
_EIP	=	0x00b0
_IE	=	0x00a8
_IP	=	0x00b8
_PCON	=	0x0087
_PSW	=	0x00d0
_SP	=	0x0081
_XPAGE	=	0x00d9
__XPAGE	=	0x00d9
_ADCCH0CONFIG	=	0x00ca
_ADCCH1CONFIG	=	0x00cb
_ADCCH2CONFIG	=	0x00d2
_ADCCH3CONFIG	=	0x00d3
_ADCCLKSRC	=	0x00d1
_ADCCONV	=	0x00c9
_ANALOGCOMP	=	0x00e1
_CLKCON	=	0x00c6
_CLKSTAT	=	0x00c7
_CODECONFIG	=	0x0097
_DBGLNKBUF	=	0x00e3
_DBGLNKSTAT	=	0x00e2
_DIRA	=	0x0089
_DIRB	=	0x008a
_DIRC	=	0x008b
_DIRR	=	0x008e
_PINA	=	0x00c8
_PINB	=	0x00e8
_PINC	=	0x00f8
_PINR	=	0x008d
_PORTA	=	0x0080
_PORTB	=	0x0088
_PORTC	=	0x0090
_PORTR	=	0x008c
_IC0CAPT0	=	0x00ce
_IC0CAPT1	=	0x00cf
_IC0CAPT	=	0xcfce
_IC0MODE	=	0x00cc
_IC0STATUS	=	0x00cd
_IC1CAPT0	=	0x00d6
_IC1CAPT1	=	0x00d7
_IC1CAPT	=	0xd7d6
_IC1MODE	=	0x00d4
_IC1STATUS	=	0x00d5
_NVADDR0	=	0x0092
_NVADDR1	=	0x0093
_NVADDR	=	0x9392
_NVDATA0	=	0x0094
_NVDATA1	=	0x0095
_NVDATA	=	0x9594
_NVKEY	=	0x0096
_NVSTATUS	=	0x0091
_OC0COMP0	=	0x00bc
_OC0COMP1	=	0x00bd
_OC0COMP	=	0xbdbc
_OC0MODE	=	0x00b9
_OC0PIN	=	0x00ba
_OC0STATUS	=	0x00bb
_OC1COMP0	=	0x00c4
_OC1COMP1	=	0x00c5
_OC1COMP	=	0xc5c4
_OC1MODE	=	0x00c1
_OC1PIN	=	0x00c2
_OC1STATUS	=	0x00c3
_RADIOACC	=	0x00b1
_RADIOADDR0	=	0x00b3
_RADIOADDR1	=	0x00b2
_RADIOADDR	=	0xb2b3
_RADIODATA0	=	0x00b7
_RADIODATA1	=	0x00b6
_RADIODATA2	=	0x00b5
_RADIODATA3	=	0x00b4
_RADIODATA	=	0xb4b5b6b7
_RADIOSTAT0	=	0x00be
_RADIOSTAT1	=	0x00bf
_RADIOSTAT	=	0xbfbe
_SPCLKSRC	=	0x00df
_SPMODE	=	0x00dc
_SPSHREG	=	0x00de
_SPSTATUS	=	0x00dd
_T0CLKSRC	=	0x009a
_T0CNT0	=	0x009c
_T0CNT1	=	0x009d
_T0CNT	=	0x9d9c
_T0MODE	=	0x0099
_T0PERIOD0	=	0x009e
_T0PERIOD1	=	0x009f
_T0PERIOD	=	0x9f9e
_T0STATUS	=	0x009b
_T1CLKSRC	=	0x00a2
_T1CNT0	=	0x00a4
_T1CNT1	=	0x00a5
_T1CNT	=	0xa5a4
_T1MODE	=	0x00a1
_T1PERIOD0	=	0x00a6
_T1PERIOD1	=	0x00a7
_T1PERIOD	=	0xa7a6
_T1STATUS	=	0x00a3
_T2CLKSRC	=	0x00aa
_T2CNT0	=	0x00ac
_T2CNT1	=	0x00ad
_T2CNT	=	0xadac
_T2MODE	=	0x00a9
_T2PERIOD0	=	0x00ae
_T2PERIOD1	=	0x00af
_T2PERIOD	=	0xafae
_T2STATUS	=	0x00ab
_U0CTRL	=	0x00e4
_U0MODE	=	0x00e7
_U0SHREG	=	0x00e6
_U0STATUS	=	0x00e5
_U1CTRL	=	0x00ec
_U1MODE	=	0x00ef
_U1SHREG	=	0x00ee
_U1STATUS	=	0x00ed
_WDTCFG	=	0x00da
_WDTRESET	=	0x00db
_WTCFGA	=	0x00f1
_WTCFGB	=	0x00f9
_WTCNTA0	=	0x00f2
_WTCNTA1	=	0x00f3
_WTCNTA	=	0xf3f2
_WTCNTB0	=	0x00fa
_WTCNTB1	=	0x00fb
_WTCNTB	=	0xfbfa
_WTCNTR1	=	0x00eb
_WTEVTA0	=	0x00f4
_WTEVTA1	=	0x00f5
_WTEVTA	=	0xf5f4
_WTEVTB0	=	0x00f6
_WTEVTB1	=	0x00f7
_WTEVTB	=	0xf7f6
_WTEVTC0	=	0x00fc
_WTEVTC1	=	0x00fd
_WTEVTC	=	0xfdfc
_WTEVTD0	=	0x00fe
_WTEVTD1	=	0x00ff
_WTEVTD	=	0xfffe
_WTIRQEN	=	0x00e9
_WTSTAT	=	0x00ea
;--------------------------------------------------------
; special function bits
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
_ACC_0	=	0x00e0
_ACC_1	=	0x00e1
_ACC_2	=	0x00e2
_ACC_3	=	0x00e3
_ACC_4	=	0x00e4
_ACC_5	=	0x00e5
_ACC_6	=	0x00e6
_ACC_7	=	0x00e7
_B_0	=	0x00f0
_B_1	=	0x00f1
_B_2	=	0x00f2
_B_3	=	0x00f3
_B_4	=	0x00f4
_B_5	=	0x00f5
_B_6	=	0x00f6
_B_7	=	0x00f7
_E2IE_0	=	0x00a0
_E2IE_1	=	0x00a1
_E2IE_2	=	0x00a2
_E2IE_3	=	0x00a3
_E2IE_4	=	0x00a4
_E2IE_5	=	0x00a5
_E2IE_6	=	0x00a6
_E2IE_7	=	0x00a7
_E2IP_0	=	0x00c0
_E2IP_1	=	0x00c1
_E2IP_2	=	0x00c2
_E2IP_3	=	0x00c3
_E2IP_4	=	0x00c4
_E2IP_5	=	0x00c5
_E2IP_6	=	0x00c6
_E2IP_7	=	0x00c7
_EIE_0	=	0x0098
_EIE_1	=	0x0099
_EIE_2	=	0x009a
_EIE_3	=	0x009b
_EIE_4	=	0x009c
_EIE_5	=	0x009d
_EIE_6	=	0x009e
_EIE_7	=	0x009f
_EIP_0	=	0x00b0
_EIP_1	=	0x00b1
_EIP_2	=	0x00b2
_EIP_3	=	0x00b3
_EIP_4	=	0x00b4
_EIP_5	=	0x00b5
_EIP_6	=	0x00b6
_EIP_7	=	0x00b7
_IE_0	=	0x00a8
_IE_1	=	0x00a9
_IE_2	=	0x00aa
_IE_3	=	0x00ab
_IE_4	=	0x00ac
_IE_5	=	0x00ad
_IE_6	=	0x00ae
_IE_7	=	0x00af
_EA	=	0x00af
_IP_0	=	0x00b8
_IP_1	=	0x00b9
_IP_2	=	0x00ba
_IP_3	=	0x00bb
_IP_4	=	0x00bc
_IP_5	=	0x00bd
_IP_6	=	0x00be
_IP_7	=	0x00bf
_P	=	0x00d0
_F1	=	0x00d1
_OV	=	0x00d2
_RS0	=	0x00d3
_RS1	=	0x00d4
_F0	=	0x00d5
_AC	=	0x00d6
_CY	=	0x00d7
_PINA_0	=	0x00c8
_PINA_1	=	0x00c9
_PINA_2	=	0x00ca
_PINA_3	=	0x00cb
_PINA_4	=	0x00cc
_PINA_5	=	0x00cd
_PINA_6	=	0x00ce
_PINA_7	=	0x00cf
_PINB_0	=	0x00e8
_PINB_1	=	0x00e9
_PINB_2	=	0x00ea
_PINB_3	=	0x00eb
_PINB_4	=	0x00ec
_PINB_5	=	0x00ed
_PINB_6	=	0x00ee
_PINB_7	=	0x00ef
_PINC_0	=	0x00f8
_PINC_1	=	0x00f9
_PINC_2	=	0x00fa
_PINC_3	=	0x00fb
_PINC_4	=	0x00fc
_PINC_5	=	0x00fd
_PINC_6	=	0x00fe
_PINC_7	=	0x00ff
_PORTA_0	=	0x0080
_PORTA_1	=	0x0081
_PORTA_2	=	0x0082
_PORTA_3	=	0x0083
_PORTA_4	=	0x0084
_PORTA_5	=	0x0085
_PORTA_6	=	0x0086
_PORTA_7	=	0x0087
_PORTB_0	=	0x0088
_PORTB_1	=	0x0089
_PORTB_2	=	0x008a
_PORTB_3	=	0x008b
_PORTB_4	=	0x008c
_PORTB_5	=	0x008d
_PORTB_6	=	0x008e
_PORTB_7	=	0x008f
_PORTC_0	=	0x0090
_PORTC_1	=	0x0091
_PORTC_2	=	0x0092
_PORTC_3	=	0x0093
_PORTC_4	=	0x0094
_PORTC_5	=	0x0095
_PORTC_6	=	0x0096
_PORTC_7	=	0x0097
;--------------------------------------------------------
; overlayable register banks
;--------------------------------------------------------
	.area REG_BANK_0	(REL,OVR,DATA)
	.ds 8
;--------------------------------------------------------
; internal ram data
;--------------------------------------------------------
	.area DSEG    (DATA)
_GetRandomSlots_sloc0_1_0:
	.ds 1
_GetRandomSlots_sloc1_1_0:
	.ds 1
_GetRandomSlots_sloc2_1_0:
	.ds 3
_AssemblyPacket_sloc0_1_0:
	.ds 4
_AssemblyPacket_sloc1_1_0:
	.ds 3
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;--------------------------------------------------------
; indirectly addressable internal ram data
;--------------------------------------------------------
	.area ISEG    (DATA)
;--------------------------------------------------------
; absolute internal ram data
;--------------------------------------------------------
	.area IABS    (ABS,DATA)
	.area IABS    (ABS,DATA)
;--------------------------------------------------------
; bit data
;--------------------------------------------------------
	.area BSEG    (BIT)
;--------------------------------------------------------
; paged external ram data
;--------------------------------------------------------
	.area PSEG    (PAG,XDATA)
;--------------------------------------------------------
; external ram data
;--------------------------------------------------------
	.area XSEG    (XDATA)
_ADCCH0VAL0	=	0x7020
_ADCCH0VAL1	=	0x7021
_ADCCH0VAL	=	0x7020
_ADCCH1VAL0	=	0x7022
_ADCCH1VAL1	=	0x7023
_ADCCH1VAL	=	0x7022
_ADCCH2VAL0	=	0x7024
_ADCCH2VAL1	=	0x7025
_ADCCH2VAL	=	0x7024
_ADCCH3VAL0	=	0x7026
_ADCCH3VAL1	=	0x7027
_ADCCH3VAL	=	0x7026
_ADCTUNE0	=	0x7028
_ADCTUNE1	=	0x7029
_ADCTUNE2	=	0x702a
_DMA0ADDR0	=	0x7010
_DMA0ADDR1	=	0x7011
_DMA0ADDR	=	0x7010
_DMA0CONFIG	=	0x7014
_DMA1ADDR0	=	0x7012
_DMA1ADDR1	=	0x7013
_DMA1ADDR	=	0x7012
_DMA1CONFIG	=	0x7015
_FRCOSCCONFIG	=	0x7070
_FRCOSCCTRL	=	0x7071
_FRCOSCFREQ0	=	0x7076
_FRCOSCFREQ1	=	0x7077
_FRCOSCFREQ	=	0x7076
_FRCOSCKFILT0	=	0x7072
_FRCOSCKFILT1	=	0x7073
_FRCOSCKFILT	=	0x7072
_FRCOSCPER0	=	0x7078
_FRCOSCPER1	=	0x7079
_FRCOSCPER	=	0x7078
_FRCOSCREF0	=	0x7074
_FRCOSCREF1	=	0x7075
_FRCOSCREF	=	0x7074
_ANALOGA	=	0x7007
_GPIOENABLE	=	0x700c
_EXTIRQ	=	0x7003
_INTCHGA	=	0x7000
_INTCHGB	=	0x7001
_INTCHGC	=	0x7002
_PALTA	=	0x7008
_PALTB	=	0x7009
_PALTC	=	0x700a
_PALTRADIO	=	0x7046
_PINCHGA	=	0x7004
_PINCHGB	=	0x7005
_PINCHGC	=	0x7006
_PINSEL	=	0x700b
_LPOSCCONFIG	=	0x7060
_LPOSCFREQ0	=	0x7066
_LPOSCFREQ1	=	0x7067
_LPOSCFREQ	=	0x7066
_LPOSCKFILT0	=	0x7062
_LPOSCKFILT1	=	0x7063
_LPOSCKFILT	=	0x7062
_LPOSCPER0	=	0x7068
_LPOSCPER1	=	0x7069
_LPOSCPER	=	0x7068
_LPOSCREF0	=	0x7064
_LPOSCREF1	=	0x7065
_LPOSCREF	=	0x7064
_LPXOSCGM	=	0x7054
_MISCCTRL	=	0x7f01
_OSCCALIB	=	0x7053
_OSCFORCERUN	=	0x7050
_OSCREADY	=	0x7052
_OSCRUN	=	0x7051
_RADIOFDATAADDR0	=	0x7040
_RADIOFDATAADDR1	=	0x7041
_RADIOFDATAADDR	=	0x7040
_RADIOFSTATADDR0	=	0x7042
_RADIOFSTATADDR1	=	0x7043
_RADIOFSTATADDR	=	0x7042
_RADIOMUX	=	0x7044
_SCRATCH0	=	0x7084
_SCRATCH1	=	0x7085
_SCRATCH2	=	0x7086
_SCRATCH3	=	0x7087
_SILICONREV	=	0x7f00
_XTALAMPL	=	0x7f19
_XTALOSC	=	0x7f18
_XTALREADY	=	0x7f1a
_RNGBYTE	=	0x7081
_AESCONFIG	=	0x7091
_AESCURBLOCK	=	0x7098
_AESINADDR0	=	0x7094
_AESINADDR1	=	0x7095
_AESINADDR	=	0x7094
_AESKEYADDR0	=	0x7092
_AESKEYADDR1	=	0x7093
_AESKEYADDR	=	0x7092
_AESMODE	=	0x7090
_AESOUTADDR0	=	0x7096
_AESOUTADDR1	=	0x7097
_AESOUTADDR	=	0x7096
_RNGMODE	=	0x7080
_RNGCLKSRC0	=	0x7082
_RNGCLKSRC1	=	0x7083
_flash_deviceid	=	0xfc06
_flash_calsector	=	0xfc00
_delaymstimer:
	.ds 8
_OriginalMessage_tmr::
	.ds 8
_TwinMessage1_tmr::
	.ds 8
_TwinMessage2_tmr::
	.ds 8
_GPS_tmr::
	.ds 8
_BackOff::
	.ds 8
_ACKTimer::
	.ds 8
_Channel_change::
	.ds 8
_GPS_PoolFlag::
	.ds 1
_delay_tx_PARM_2:
	.ds 4
_delay_tx_PARM_3:
	.ds 4
_delay_tx_time1_ms_65536_283:
	.ds 4
_InitGPSPolling_GPS_Period_S_65536_285:
	.ds 4
_swap_variables_PARM_2:
	.ds 3
_swap_variables_a_65536_293:
	.ds 3
_GetRandomSlots_PARM_2:
	.ds 3
_GetRandomSlots_PARM_3:
	.ds 3
_GetRandomSlots_slot1_65536_295:
	.ds 3
_AssemblyPacket_PARM_2:
	.ds 1
_AssemblyPacket_PARM_3:
	.ds 1
_AssemblyPacket_PARM_4:
	.ds 3
_AssemblyPacket_PARM_5:
	.ds 1
_AssemblyPacket_PARM_6:
	.ds 1
_AssemblyPacket_PARM_7:
	.ds 3
_AssemblyPacket_PARM_8:
	.ds 3
_AssemblyPacket_GoldCode_65536_297:
	.ds 4
_AssemblyPacket_HMAC_65536_298:
	.ds 4
_AssemblyPacket_CRC32_65536_298:
	.ds 4
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area XABS    (ABS,XDATA)
;--------------------------------------------------------
; external initialized ram data
;--------------------------------------------------------
	.area XISEG   (XDATA)
	.area HOME    (CODE)
	.area GSINIT0 (CODE)
	.area GSINIT1 (CODE)
	.area GSINIT2 (CODE)
	.area GSINIT3 (CODE)
	.area GSINIT4 (CODE)
	.area GSINIT5 (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area CSEG    (CODE)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME    (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area GSINIT  (CODE)
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME    (CODE)
	.area HOME    (CODE)
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CSEG    (CODE)
;------------------------------------------------------------
;Allocation info for local variables in function 'delayms_callback'
;------------------------------------------------------------
;desc                      Allocated with name '_delayms_callback_desc_65536_263'
;------------------------------------------------------------
;	..\src\atrs\misc.c:81: static void delayms_callback(struct wtimer_desc __xdata *desc)
;	-----------------------------------------
;	 function delayms_callback
;	-----------------------------------------
_delayms_callback:
	ar7 = 0x07
	ar6 = 0x06
	ar5 = 0x05
	ar4 = 0x04
	ar3 = 0x03
	ar2 = 0x02
	ar1 = 0x01
	ar0 = 0x00
;	..\src\atrs\misc.c:84: delaymstimer.handler = 0;
	mov	dptr,#(_delaymstimer + 0x0002)
	clr	a
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
;	..\src\atrs\misc.c:85: }
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'delay_ms'
;------------------------------------------------------------
;ms                        Allocated to registers r6 r7 
;x                         Allocated to registers r4 r5 r6 r7 
;sloc0                     Allocated to stack - _bp +1
;sloc1                     Allocated to stack - _bp +5
;------------------------------------------------------------
;	..\src\atrs\misc.c:91: void delay_ms(uint16_t ms) __reentrant
;	-----------------------------------------
;	 function delay_ms
;	-----------------------------------------
_delay_ms:
	push	_bp
	mov	a,sp
	mov	_bp,a
	add	a,#0x08
	mov	sp,a
	mov	r6,dpl
	mov	r7,dph
;	..\src\atrs\misc.c:95: wtimer_remove(&delaymstimer);
	mov	dptr,#_delaymstimer
	push	ar7
	push	ar6
	lcall	_wtimer_remove
	pop	ar6
	pop	ar7
;	..\src\atrs\misc.c:96: x = ms;
	mov	r0,_bp
	inc	r0
	mov	@r0,ar6
	inc	r0
	mov	@r0,ar7
	inc	r0
	mov	@r0,#0x00
	inc	r0
	mov	@r0,#0x00
;	..\src\atrs\misc.c:97: delaymstimer.time = ms >> 1;
	mov	a,r7
	clr	c
	rrc	a
	xch	a,r6
	rrc	a
	xch	a,r6
	mov	r7,a
	mov	ar4,r6
	mov	ar5,r7
	mov	r6,#0x00
	mov	r7,#0x00
	mov	dptr,#(_delaymstimer + 0x0004)
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\atrs\misc.c:98: x <<= 3;
	mov	r0,_bp
	inc	r0
	inc	r0
	inc	r0
	inc	r0
	mov	a,@r0
	dec	r0
	swap	a
	rr	a
	anl	a,#0xf8
	xch	a,@r0
	swap	a
	rr	a
	xch	a,@r0
	xrl	a,@r0
	xch	a,@r0
	anl	a,#0xf8
	xch	a,@r0
	xrl	a,@r0
	inc	r0
	mov	@r0,a
	dec	r0
	dec	r0
	mov	a,@r0
	swap	a
	rr	a
	anl	a,#0x07
	inc	r0
	orl	a,@r0
	mov	@r0,a
	dec	r0
	mov	a,@r0
	dec	r0
	swap	a
	rr	a
	anl	a,#0xf8
	xch	a,@r0
	swap	a
	rr	a
	xch	a,@r0
	xrl	a,@r0
	xch	a,@r0
	anl	a,#0xf8
	xch	a,@r0
	xrl	a,@r0
	inc	r0
	mov	@r0,a
;	..\src\atrs\misc.c:99: delaymstimer.time -= x;
	mov	r0,_bp
	inc	r0
	mov	a,_bp
	add	a,#0x05
	mov	r1,a
	mov	a,r4
	clr	c
	subb	a,@r0
	mov	@r1,a
	mov	a,r5
	inc	r0
	subb	a,@r0
	inc	r1
	mov	@r1,a
	mov	a,r6
	inc	r0
	subb	a,@r0
	inc	r1
	mov	@r1,a
	mov	a,r7
	inc	r0
	subb	a,@r0
	inc	r1
	mov	@r1,a
	mov	dptr,#(_delaymstimer + 0x0004)
	mov	a,_bp
	add	a,#0x05
	mov	r0,a
	mov	a,@r0
	movx	@dptr,a
	inc	r0
	mov	a,@r0
	inc	dptr
	movx	@dptr,a
	inc	r0
	mov	a,@r0
	inc	dptr
	movx	@dptr,a
	inc	r0
	mov	a,@r0
	inc	dptr
	movx	@dptr,a
;	..\src\atrs\misc.c:100: x <<= 3;
	mov	r0,_bp
	inc	r0
	inc	r0
	inc	r0
	mov	ar6,@r0
	inc	r0
	mov	a,@r0
	swap	a
	rr	a
	anl	a,#0xf8
	xch	a,r6
	swap	a
	rr	a
	xch	a,r6
	xrl	a,r6
	xch	a,r6
	anl	a,#0xf8
	xch	a,r6
	xrl	a,r6
	mov	r7,a
	dec	r0
	dec	r0
	mov	a,@r0
	swap	a
	rr	a
	anl	a,#0x07
	orl	a,r6
	mov	r6,a
	dec	r0
	mov	ar4,@r0
	inc	r0
	mov	a,@r0
	swap	a
	rr	a
	anl	a,#0xf8
	xch	a,r4
	swap	a
	rr	a
	xch	a,r4
	xrl	a,r4
	xch	a,r4
	anl	a,#0xf8
	xch	a,r4
	xrl	a,r4
	mov	r5,a
;	..\src\atrs\misc.c:101: delaymstimer.time += x;
	mov	a,_bp
	add	a,#0x05
	mov	r0,a
	mov	a,r4
	add	a,@r0
	mov	@r0,a
	mov	a,r5
	inc	r0
	addc	a,@r0
	mov	@r0,a
	mov	a,r6
	inc	r0
	addc	a,@r0
	mov	@r0,a
	mov	a,r7
	inc	r0
	addc	a,@r0
	mov	@r0,a
	mov	dptr,#(_delaymstimer + 0x0004)
	mov	a,_bp
	add	a,#0x05
	mov	r0,a
	mov	a,@r0
	movx	@dptr,a
	inc	r0
	mov	a,@r0
	inc	dptr
	movx	@dptr,a
	inc	r0
	mov	a,@r0
	inc	dptr
	movx	@dptr,a
	inc	r0
	mov	a,@r0
	inc	dptr
	movx	@dptr,a
;	..\src\atrs\misc.c:102: x <<= 2;
	mov	a,r4
	add	a,r4
	mov	r2,a
	mov	a,r5
	rlc	a
	mov	r3,a
	mov	a,r6
	rlc	a
	mov	r6,a
	mov	a,r7
	rlc	a
	mov	r7,a
	mov	a,r2
	add	a,r2
	mov	r2,a
	mov	a,r3
	rlc	a
	mov	r3,a
	mov	a,r6
	rlc	a
	mov	r6,a
	mov	a,r7
	rlc	a
	mov	r7,a
;	..\src\atrs\misc.c:103: delaymstimer.time += x;
	mov	a,_bp
	add	a,#0x05
	mov	r0,a
	mov	a,r2
	add	a,@r0
	mov	r2,a
	mov	a,r3
	inc	r0
	addc	a,@r0
	mov	r3,a
	mov	a,r6
	inc	r0
	addc	a,@r0
	mov	r6,a
	mov	a,r7
	inc	r0
	addc	a,@r0
	mov	r7,a
	mov	dptr,#(_delaymstimer + 0x0004)
	mov	a,r2
	movx	@dptr,a
	mov	a,r3
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\atrs\misc.c:104: delaymstimer.handler = delayms_callback;
	mov	dptr,#(_delaymstimer + 0x0002)
	mov	a,#_delayms_callback
	movx	@dptr,a
	mov	a,#(_delayms_callback >> 8)
	inc	dptr
	movx	@dptr,a
;	..\src\atrs\misc.c:105: wtimer1_addrelative(&delaymstimer);
	mov	dptr,#_delaymstimer
	lcall	_wtimer1_addrelative
;	..\src\atrs\misc.c:106: wtimer_runcallbacks();
	lcall	_wtimer_runcallbacks
;	..\src\atrs\misc.c:107: do
00101$:
;	..\src\atrs\misc.c:109: wtimer_idle(WTFLAG_CANSTANDBY);
	mov	dpl,#0x02
	lcall	_wtimer_idle
;	..\src\atrs\misc.c:110: wtimer_runcallbacks();
	lcall	_wtimer_runcallbacks
;	..\src\atrs\misc.c:111: } while (delaymstimer.handler);
	mov	dptr,#(_delaymstimer + 0x0002)
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	orl	a,r6
	jnz	00101$
;	..\src\atrs\misc.c:112: }
	mov	sp,_bp
	pop	_bp
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'OriginalMessage_callback'
;------------------------------------------------------------
;desc                      Allocated with name '_OriginalMessage_callback_desc_65536_268'
;------------------------------------------------------------
;	..\src\atrs\misc.c:122: void OriginalMessage_callback(struct wtimer_desc __xdata *desc)
;	-----------------------------------------
;	 function OriginalMessage_callback
;	-----------------------------------------
_OriginalMessage_callback:
;	..\src\atrs\misc.c:128: dbglink_writestr(" transmit ");
	mov	dptr,#___str_0
	mov	b,#0x80
	lcall	_dbglink_writestr
;	..\src\atrs\misc.c:131: axradio_transmit(&remoteaddr, TxPackageOrig, TxPacketLength);
	mov	dptr,#_TxPackageOrig
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	r5,#0x00
	mov	dptr,#_TxPacketLength
	movx	a,@dptr
	mov	r4,a
	mov	r3,#0x00
	mov	dptr,#_axradio_transmit_PARM_2
	mov	a,r6
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_axradio_transmit_PARM_3
	mov	a,r4
	movx	@dptr,a
	mov	a,r3
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_remoteaddr
	mov	b,#0x80
;	..\src\atrs\misc.c:133: }
	ljmp	_axradio_transmit
;------------------------------------------------------------
;Allocation info for local variables in function 'TwinMessage1_callback'
;------------------------------------------------------------
;desc                      Allocated with name '_TwinMessage1_callback_desc_65536_270'
;------------------------------------------------------------
;	..\src\atrs\misc.c:143: void TwinMessage1_callback(struct wtimer_desc __xdata *desc)
;	-----------------------------------------
;	 function TwinMessage1_callback
;	-----------------------------------------
_TwinMessage1_callback:
;	..\src\atrs\misc.c:149: axradio_transmit(&remoteaddr, TxPackageTwin1, TxPacketLength);
	mov	dptr,#_TxPackageTwin1
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	r5,#0x00
	mov	dptr,#_TxPacketLength
	movx	a,@dptr
	mov	r4,a
	mov	r3,#0x00
	mov	dptr,#_axradio_transmit_PARM_2
	mov	a,r6
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_axradio_transmit_PARM_3
	mov	a,r4
	movx	@dptr,a
	mov	a,r3
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_remoteaddr
	mov	b,#0x80
;	..\src\atrs\misc.c:150: }
	ljmp	_axradio_transmit
;------------------------------------------------------------
;Allocation info for local variables in function 'TwinMessage2_callback'
;------------------------------------------------------------
;desc                      Allocated with name '_TwinMessage2_callback_desc_65536_272'
;------------------------------------------------------------
;	..\src\atrs\misc.c:161: void TwinMessage2_callback(struct wtimer_desc __xdata *desc)
;	-----------------------------------------
;	 function TwinMessage2_callback
;	-----------------------------------------
_TwinMessage2_callback:
;	..\src\atrs\misc.c:166: axradio_transmit(&remoteaddr, TxPackageTwin2, TxPacketLength);
	mov	dptr,#_TxPackageTwin2
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	r5,#0x00
	mov	dptr,#_TxPacketLength
	movx	a,@dptr
	mov	r4,a
	mov	r3,#0x00
	mov	dptr,#_axradio_transmit_PARM_2
	mov	a,r6
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_axradio_transmit_PARM_3
	mov	a,r4
	movx	@dptr,a
	mov	a,r3
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_remoteaddr
	mov	b,#0x80
;	..\src\atrs\misc.c:168: }
	ljmp	_axradio_transmit
;------------------------------------------------------------
;Allocation info for local variables in function 'GPS_callback'
;------------------------------------------------------------
;desc                      Allocated with name '_GPS_callback_desc_65536_274'
;------------------------------------------------------------
;	..\src\atrs\misc.c:179: void GPS_callback(struct wtimer_desc __xdata *desc)
;	-----------------------------------------
;	 function GPS_callback
;	-----------------------------------------
_GPS_callback:
;	..\src\atrs\misc.c:183: dbglink_writestr(" \r\n GPS callback \r\n");
	mov	dptr,#___str_1
	mov	b,#0x80
	lcall	_dbglink_writestr
;	..\src\atrs\misc.c:185: GPS_PoolFlag = 1;
	mov	dptr,#_GPS_PoolFlag
	mov	a,#0x01
	movx	@dptr,a
;	..\src\atrs\misc.c:187: wtimer0_remove(&GPS_tmr);
	mov	dptr,#_GPS_tmr
	lcall	_wtimer0_remove
;	..\src\atrs\misc.c:188: GPS_tmr.time +=  (uint32_t) TIME_FOR_TIMER_SEC(GPS_POLL_TIME_S);//(uint32_t) TIME_FOR_TIMER_SEC(10); //+= ? o solo = ? verificar esto
	mov	dptr,#(_GPS_tmr + 0x0004)
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	a,#0x19
	add	a,r5
	mov	r5,a
	clr	a
	addc	a,r6
	mov	r6,a
	clr	a
	addc	a,r7
	mov	r7,a
	mov	dptr,#(_GPS_tmr + 0x0004)
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\atrs\misc.c:189: wtimer0_addrelative(&GPS_tmr);
	mov	dptr,#_GPS_tmr
;	..\src\atrs\misc.c:192: }
	ljmp	_wtimer0_addrelative
;------------------------------------------------------------
;Allocation info for local variables in function 'Channel_change_callback'
;------------------------------------------------------------
;desc                      Allocated with name '_Channel_change_callback_desc_65536_276'
;------------------------------------------------------------
;	..\src\atrs\misc.c:201: void Channel_change_callback(struct wtimer_desc __xdata *desc)
;	-----------------------------------------
;	 function Channel_change_callback
;	-----------------------------------------
_Channel_change_callback:
;	..\src\atrs\misc.c:207: if(ChannelChangeActive)
	mov	dptr,#_ChannelChangeActive
	movx	a,@dptr
	jnz	00117$
	ret
00117$:
;	..\src\atrs\misc.c:210: axradio_set_channel(FREQHZ_TO_AXFREQ(freq));
	mov	dptr,#_freq
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	lcall	___ulong2fs
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	clr	a
	push	acc
	mov	a,#0x1b
	push	acc
	mov	a,#0x37
	push	acc
	mov	a,#0x4c
	push	acc
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	mov	a,r7
	lcall	___fsdiv
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	mov	dptr,#0x0000
	mov	b,#0x80
	mov	a,#0x4b
	lcall	___fsmul
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	mov	a,r7
	lcall	___fs2ulong
	lcall	_axradio_set_channel
;	..\src\atrs\misc.c:211: freq +=600;
	mov	dptr,#_freq
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#_freq
	mov	a,#0x58
	add	a,r4
	movx	@dptr,a
	mov	a,#0x02
	addc	a,r5
	inc	dptr
	movx	@dptr,a
	clr	a
	addc	a,r6
	inc	dptr
	movx	@dptr,a
	clr	a
	addc	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\atrs\misc.c:212: dbglink_writestr(" frecuencia: ");
	mov	dptr,#___str_2
	mov	b,#0x80
	lcall	_dbglink_writestr
;	..\src\atrs\misc.c:213: wtimer0_remove(&Channel_change);
	mov	dptr,#_Channel_change
	lcall	_wtimer0_remove
;	..\src\atrs\misc.c:214: dbglink_writenum32(freq,1,WRNUM_PADZERO);
	mov	dptr,#_freq
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	a,#0x08
	push	acc
	mov	a,#0x01
	push	acc
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	mov	a,r7
	lcall	_dbglink_writenum32
	dec	sp
	dec	sp
;	..\src\atrs\misc.c:215: if( freq > FREQUENCY_HZ + 10000) freq = (FREQUENCY_HZ -10000);
	mov	dptr,#_freq
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	clr	c
	mov	a,#0xe0
	subb	a,r4
	mov	a,#0x4a
	subb	a,r5
	mov	a,#0x06
	subb	a,r6
	mov	a,#0x1a
	subb	a,r7
	jnc	00104$
	mov	dptr,#_freq
	mov	a,#0xc0
	movx	@dptr,a
	mov	a,#0xfc
	inc	dptr
	movx	@dptr,a
	mov	a,#0x05
	inc	dptr
	movx	@dptr,a
	mov	a,#0x1a
	inc	dptr
	movx	@dptr,a
00104$:
;	..\src\atrs\misc.c:216: Channel_change.time +=  (uint32_t) TIME_FOR_TIMER_MSEC(1000);//(uint32_t) TIME_FOR_TIMER_SEC(10); //+= ? o solo = ? verificar esto
	mov	dptr,#(_Channel_change + 0x0004)
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	a,#0x80
	add	a,r4
	mov	r4,a
	mov	a,#0x02
	addc	a,r5
	mov	r5,a
	clr	a
	addc	a,r6
	mov	r6,a
	clr	a
	addc	a,r7
	mov	r7,a
	mov	dptr,#(_Channel_change + 0x0004)
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\atrs\misc.c:217: wtimer0_addabsolute(&Channel_change);
	mov	dptr,#_Channel_change
;	..\src\atrs\misc.c:220: }
	ljmp	_wtimer0_addabsolute
;------------------------------------------------------------
;Allocation info for local variables in function 'BackOff_Callback'
;------------------------------------------------------------
;desc                      Allocated with name '_BackOff_Callback_desc_65536_279'
;------------------------------------------------------------
;	..\src\atrs\misc.c:231: void BackOff_Callback(struct wtimer_desc __xdata *desc)
;	-----------------------------------------
;	 function BackOff_Callback
;	-----------------------------------------
_BackOff_Callback:
;	..\src\atrs\misc.c:235: BackOffFlag = 1;
	mov	dptr,#_BackOffFlag
	mov	a,#0x01
	movx	@dptr,a
;	..\src\atrs\misc.c:237: dbglink_writestr(" \n backoff timer expired ");
	mov	dptr,#___str_3
	mov	b,#0x80
;	..\src\atrs\misc.c:240: }
	ljmp	_dbglink_writestr
;------------------------------------------------------------
;Allocation info for local variables in function 'ACK_Callback'
;------------------------------------------------------------
;desc                      Allocated with name '_ACK_Callback_desc_65536_281'
;------------------------------------------------------------
;	..\src\atrs\misc.c:250: __reentrantb void ACK_Callback(struct wtimer_desc __xdata *desc)
;	-----------------------------------------
;	 function ACK_Callback
;	-----------------------------------------
_ACK_Callback:
;	..\src\atrs\misc.c:255: dbglink_writestr("ACK timer expired");
	mov	dptr,#___str_4
	mov	b,#0x80
	lcall	_dbglink_writestr
;	..\src\atrs\misc.c:257: ACKTimerFlag = 1;
	mov	dptr,#_ACKTimerFlag
	mov	a,#0x01
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
;	..\src\atrs\misc.c:259: }
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'delay_tx'
;------------------------------------------------------------
;time2_ms                  Allocated with name '_delay_tx_PARM_2'
;time3_ms                  Allocated with name '_delay_tx_PARM_3'
;time1_ms                  Allocated with name '_delay_tx_time1_ms_65536_283'
;------------------------------------------------------------
;	..\src\atrs\misc.c:273: void delay_tx(uint32_t time1_ms, uint32_t time2_ms, uint32_t time3_ms)
;	-----------------------------------------
;	 function delay_tx
;	-----------------------------------------
_delay_tx:
	mov	r7,dpl
	mov	r6,dph
	mov	r5,b
	mov	r4,a
	mov	dptr,#_delay_tx_time1_ms_65536_283
	mov	a,r7
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
;	..\src\atrs\misc.c:275: wtimer0_remove(&OriginalMessage_tmr);
	mov	dptr,#_OriginalMessage_tmr
	lcall	_wtimer0_remove
;	..\src\atrs\misc.c:276: OriginalMessage_tmr.handler = OriginalMessage_callback;//Handler for timer 0
	mov	dptr,#(_OriginalMessage_tmr + 0x0002)
	mov	a,#_OriginalMessage_callback
	movx	@dptr,a
	mov	a,#(_OriginalMessage_callback >> 8)
	inc	dptr
	movx	@dptr,a
;	..\src\atrs\misc.c:277: time1_ms = (uint32_t) TIME_FOR_TIMER_MSEC(time1_ms);
	mov	dptr,#_delay_tx_time1_ms_65536_283
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	lcall	___ulong2fs
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	mov	dptr,#0x0000
	mov	b,#0x20
	mov	a,#0x44
	lcall	___fsmul
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	clr	a
	push	acc
	push	acc
	mov	a,#0x7a
	push	acc
	mov	a,#0x44
	push	acc
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	mov	a,r7
	lcall	___fsdiv
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	mov	a,r7
	lcall	___fs2ulong
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	dptr,#_delay_tx_time1_ms_65536_283
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\atrs\misc.c:278: OriginalMessage_tmr.time = time1_ms; //+= ? o solo = ? verificar esto
	mov	dptr,#_delay_tx_time1_ms_65536_283
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#(_OriginalMessage_tmr + 0x0004)
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\atrs\misc.c:279: wtimer0_addrelative(&OriginalMessage_tmr);
	mov	dptr,#_OriginalMessage_tmr
	lcall	_wtimer0_addrelative
;	..\src\atrs\misc.c:280: wtimer0_remove(&TwinMessage1_tmr);
	mov	dptr,#_TwinMessage1_tmr
	lcall	_wtimer0_remove
;	..\src\atrs\misc.c:281: TwinMessage1_tmr.handler = TwinMessage1_callback;//Handler for timer 0
	mov	dptr,#(_TwinMessage1_tmr + 0x0002)
	mov	a,#_TwinMessage1_callback
	movx	@dptr,a
	mov	a,#(_TwinMessage1_callback >> 8)
	inc	dptr
	movx	@dptr,a
;	..\src\atrs\misc.c:282: time2_ms = (uint32_t) TIME_FOR_TIMER_MSEC(time2_ms);
	mov	dptr,#_delay_tx_PARM_2
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	lcall	___ulong2fs
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	mov	dptr,#0x0000
	mov	b,#0x20
	mov	a,#0x44
	lcall	___fsmul
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	clr	a
	push	acc
	push	acc
	mov	a,#0x7a
	push	acc
	mov	a,#0x44
	push	acc
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	mov	a,r7
	lcall	___fsdiv
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	mov	a,r7
	lcall	___fs2ulong
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	dptr,#_delay_tx_PARM_2
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\atrs\misc.c:283: TwinMessage1_tmr.time = time2_ms; //+= ? o solo = ? verificar esto
	mov	dptr,#_delay_tx_PARM_2
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#(_TwinMessage1_tmr + 0x0004)
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\atrs\misc.c:284: wtimer0_addrelative(&TwinMessage1_tmr);
	mov	dptr,#_TwinMessage1_tmr
	lcall	_wtimer0_addrelative
;	..\src\atrs\misc.c:285: wtimer0_remove(&TwinMessage2_tmr);
	mov	dptr,#_TwinMessage2_tmr
	lcall	_wtimer0_remove
;	..\src\atrs\misc.c:286: TwinMessage2_tmr.handler = TwinMessage2_callback;//Handler for timer 0
	mov	dptr,#(_TwinMessage2_tmr + 0x0002)
	mov	a,#_TwinMessage2_callback
	movx	@dptr,a
	mov	a,#(_TwinMessage2_callback >> 8)
	inc	dptr
	movx	@dptr,a
;	..\src\atrs\misc.c:287: time3_ms = (uint32_t) TIME_FOR_TIMER_MSEC(time3_ms);
	mov	dptr,#_delay_tx_PARM_3
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	lcall	___ulong2fs
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	mov	dptr,#0x0000
	mov	b,#0x20
	mov	a,#0x44
	lcall	___fsmul
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	clr	a
	push	acc
	push	acc
	mov	a,#0x7a
	push	acc
	mov	a,#0x44
	push	acc
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	mov	a,r7
	lcall	___fsdiv
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	mov	a,r7
	lcall	___fs2ulong
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	dptr,#_delay_tx_PARM_3
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\atrs\misc.c:288: TwinMessage2_tmr.time = time3_ms; //+= ? o solo = ? verificar esto
	mov	dptr,#_delay_tx_PARM_3
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#(_TwinMessage2_tmr + 0x0004)
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\atrs\misc.c:289: wtimer0_addrelative(&TwinMessage2_tmr);
	mov	dptr,#_TwinMessage2_tmr
;	..\src\atrs\misc.c:290: }
	ljmp	_wtimer0_addrelative
;------------------------------------------------------------
;Allocation info for local variables in function 'InitGPSPolling'
;------------------------------------------------------------
;GPS_Period_S              Allocated with name '_InitGPSPolling_GPS_Period_S_65536_285'
;------------------------------------------------------------
;	..\src\atrs\misc.c:300: void InitGPSPolling (uint32_t GPS_Period_S)
;	-----------------------------------------
;	 function InitGPSPolling
;	-----------------------------------------
_InitGPSPolling:
	mov	r7,dpl
	mov	r6,dph
	mov	r5,b
	mov	r4,a
	mov	dptr,#_InitGPSPolling_GPS_Period_S_65536_285
	mov	a,r7
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
;	..\src\atrs\misc.c:302: GPS_tmr.handler = GPS_callback;//Handler for timer 0
	mov	dptr,#(_GPS_tmr + 0x0002)
	mov	a,#_GPS_callback
	movx	@dptr,a
	mov	a,#(_GPS_callback >> 8)
	inc	dptr
	movx	@dptr,a
;	..\src\atrs\misc.c:303: GPS_Period_S = (uint32_t) TIME_FOR_TIMER_SEC(GPS_Period_S);
	mov	dptr,#_InitGPSPolling_GPS_Period_S_65536_285
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#__mullong_PARM_2
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0280
	clr	a
	mov	b,a
	lcall	__mullong
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	dptr,#_InitGPSPolling_GPS_Period_S_65536_285
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\atrs\misc.c:304: GPS_tmr.time = GPS_Period_S; //+= ? o solo = ? verificar esto
	mov	dptr,#_InitGPSPolling_GPS_Period_S_65536_285
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#(_GPS_tmr + 0x0004)
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\atrs\misc.c:305: wtimer0_remove(&GPS_tmr);
	mov	dptr,#_GPS_tmr
	lcall	_wtimer0_remove
;	..\src\atrs\misc.c:306: wtimer0_addrelative(&GPS_tmr);
	mov	dptr,#_GPS_tmr
;	..\src\atrs\misc.c:309: }
	ljmp	_wtimer0_addrelative
;------------------------------------------------------------
;Allocation info for local variables in function 'InitChannelChange'
;------------------------------------------------------------
;	..\src\atrs\misc.c:315: void InitChannelChange(void)
;	-----------------------------------------
;	 function InitChannelChange
;	-----------------------------------------
_InitChannelChange:
;	..\src\atrs\misc.c:317: Channel_change.handler = Channel_change_callback;
	mov	dptr,#(_Channel_change + 0x0002)
	mov	a,#_Channel_change_callback
	movx	@dptr,a
	mov	a,#(_Channel_change_callback >> 8)
	inc	dptr
	movx	@dptr,a
;	..\src\atrs\misc.c:318: Channel_change.time = (uint32_t) TIME_FOR_TIMER_MSEC(250);
	mov	dptr,#(_Channel_change + 0x0004)
	mov	a,#0xa0
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
;	..\src\atrs\misc.c:319: wtimer0_remove(&Channel_change);
	mov	dptr,#_Channel_change
	lcall	_wtimer0_remove
;	..\src\atrs\misc.c:320: wtimer0_addrelative(&Channel_change);
	mov	dptr,#_Channel_change
;	..\src\atrs\misc.c:321: }
	ljmp	_wtimer0_addrelative
;------------------------------------------------------------
;Allocation info for local variables in function 'StartBackoffTimer'
;------------------------------------------------------------
;	..\src\atrs\misc.c:328: void StartBackoffTimer(void)
;	-----------------------------------------
;	 function StartBackoffTimer
;	-----------------------------------------
_StartBackoffTimer:
;	..\src\atrs\misc.c:330: BackOff.handler = BackOff_Callback;
	mov	dptr,#(_BackOff + 0x0002)
	mov	a,#_BackOff_Callback
	movx	@dptr,a
	mov	a,#(_BackOff_Callback >> 8)
	inc	dptr
	movx	@dptr,a
;	..\src\atrs\misc.c:331: BackOff.time = (uint32_t) TIME_FOR_TIMER_SEC(30+(RNGBYTE%60));
	mov	dptr,#_RNGBYTE
	movx	a,@dptr
	mov	r7,a
	mov	r6,#0x00
	mov	dptr,#__modsint_PARM_2
	mov	a,#0x3c
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
	mov	dpl,r7
	mov	dph,r6
	lcall	__modsint
	mov	r6,dpl
	mov	r7,dph
	mov	dptr,#__mulint_PARM_2
	mov	a,r6
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
	mov	dptr,#0x0280
	lcall	__mulint
	mov	r6,dpl
	mov	a,dph
	mov	r7,a
	rlc	a
	subb	a,acc
	mov	r5,a
	mov	r4,a
	mov	a,#0x1e
	add	a,r6
	mov	r6,a
	clr	a
	addc	a,r7
	mov	r7,a
	clr	a
	addc	a,r5
	mov	r5,a
	clr	a
	addc	a,r4
	mov	r4,a
	mov	dptr,#(_BackOff + 0x0004)
	mov	a,r6
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
;	..\src\atrs\misc.c:333: dbglink_writestr(" \n Backoff timer started \n");
	mov	dptr,#___str_5
	mov	b,#0x80
	lcall	_dbglink_writestr
;	..\src\atrs\misc.c:335: wtimer0_remove(&BackOff);
	mov	dptr,#_BackOff
	lcall	_wtimer0_remove
;	..\src\atrs\misc.c:336: wtimer0_addrelative(&BackOff);
	mov	dptr,#_BackOff
;	..\src\atrs\misc.c:337: }
	ljmp	_wtimer0_addrelative
;------------------------------------------------------------
;Allocation info for local variables in function 'StartACKTimer'
;------------------------------------------------------------
;	..\src\atrs\misc.c:344: void StartACKTimer(void)
;	-----------------------------------------
;	 function StartACKTimer
;	-----------------------------------------
_StartACKTimer:
;	..\src\atrs\misc.c:346: ACKTimer.handler = ACK_Callback;
	mov	dptr,#(_ACKTimer + 0x0002)
	mov	a,#_ACK_Callback
	movx	@dptr,a
	mov	a,#(_ACK_Callback >> 8)
	inc	dptr
	movx	@dptr,a
;	..\src\atrs\misc.c:347: ACKTimer.time = (uint32_t) TIME_FOR_TIMER_SEC(/*(RNGBYTE%15)+*/5);
	mov	dptr,#(_ACKTimer + 0x0004)
	mov	a,#0x80
	movx	@dptr,a
	mov	a,#0x0c
	inc	dptr
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
;	..\src\atrs\misc.c:348: wtimer0_remove(&ACKTimer);
	mov	dptr,#_ACKTimer
	lcall	_wtimer0_remove
;	..\src\atrs\misc.c:349: wtimer0_addrelative(&ACKTimer);
	mov	dptr,#_ACKTimer
	lcall	_wtimer0_addrelative
;	..\src\atrs\misc.c:351: dbglink_writestr(" ACK timer started");
	mov	dptr,#___str_6
	mov	b,#0x80
;	..\src\atrs\misc.c:353: }
	ljmp	_dbglink_writestr
;------------------------------------------------------------
;Allocation info for local variables in function 'swap_variables'
;------------------------------------------------------------
;b                         Allocated with name '_swap_variables_PARM_2'
;a                         Allocated with name '_swap_variables_a_65536_293'
;------------------------------------------------------------
;	..\src\atrs\misc.c:364: void swap_variables(uint8_t *a,uint8_t *b)
;	-----------------------------------------
;	 function swap_variables
;	-----------------------------------------
_swap_variables:
	mov	r7,b
	mov	r6,dph
	mov	a,dpl
	mov	dptr,#_swap_variables_a_65536_293
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\atrs\misc.c:366: *a = *a+*b;
	mov	dptr,#_swap_variables_a_65536_293
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	lcall	__gptrget
	mov	r4,a
	mov	dptr,#_swap_variables_PARM_2
	movx	a,@dptr
	mov	r1,a
	inc	dptr
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	mov	dpl,r1
	mov	dph,r2
	mov	b,r3
	lcall	__gptrget
	add	a,r4
	mov	r4,a
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	lcall	__gptrput
;	..\src\atrs\misc.c:367: *b= *a-*b;
	mov	dpl,r1
	mov	dph,r2
	mov	b,r3
	lcall	__gptrget
	mov	r0,a
	mov	a,r4
	clr	c
	subb	a,r0
	mov	r0,a
	mov	dpl,r1
	mov	dph,r2
	mov	b,r3
	lcall	__gptrput
;	..\src\atrs\misc.c:368: *a = *a-*b;
	mov	a,r4
	clr	c
	subb	a,r0
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
;	..\src\atrs\misc.c:369: return;
;	..\src\atrs\misc.c:370: }
	ljmp	__gptrput
;------------------------------------------------------------
;Allocation info for local variables in function 'GetRandomSlots'
;------------------------------------------------------------
;sloc0                     Allocated with name '_GetRandomSlots_sloc0_1_0'
;sloc1                     Allocated with name '_GetRandomSlots_sloc1_1_0'
;sloc2                     Allocated with name '_GetRandomSlots_sloc2_1_0'
;slot2                     Allocated with name '_GetRandomSlots_PARM_2'
;slot3                     Allocated with name '_GetRandomSlots_PARM_3'
;slot1                     Allocated with name '_GetRandomSlots_slot1_65536_295'
;------------------------------------------------------------
;	..\src\atrs\misc.c:380: void GetRandomSlots(uint8_t* slot1, uint8_t* slot2, uint8_t* slot3)
;	-----------------------------------------
;	 function GetRandomSlots
;	-----------------------------------------
_GetRandomSlots:
	mov	r7,b
	mov	r6,dph
	mov	a,dpl
	mov	dptr,#_GetRandomSlots_slot1_65536_295
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\atrs\misc.c:382: *slot1 = RND_NUMBER_TO_SLOTS(RNGBYTE);
	mov	dptr,#_GetRandomSlots_slot1_65536_295
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#_RNGBYTE
	movx	a,@dptr
	mov	r4,a
	mov	r3,#0x00
	mov	dptr,#__modsint_PARM_2
	mov	a,#0x32
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
	mov	dpl,r4
	mov	dph,r3
	push	ar7
	push	ar6
	push	ar5
	lcall	__modsint
	mov	r3,dpl
	pop	ar5
	pop	ar6
	pop	ar7
	mov	_GetRandomSlots_sloc1_1_0,r3
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	mov	a,_GetRandomSlots_sloc1_1_0
	lcall	__gptrput
;	..\src\atrs\misc.c:383: *slot2 = RND_NUMBER_TO_SLOTS(RNGBYTE);
	mov	dptr,#_GetRandomSlots_PARM_2
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#_RNGBYTE
	movx	a,@dptr
	mov	r4,a
	mov	r2,#0x00
	mov	dptr,#__modsint_PARM_2
	mov	a,#0x32
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
	mov	dpl,r4
	mov	dph,r2
	push	ar7
	push	ar6
	push	ar5
	lcall	__modsint
	mov	r2,dpl
	pop	ar5
	pop	ar6
	pop	ar7
	mov	_GetRandomSlots_sloc0_1_0,r2
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	mov	a,_GetRandomSlots_sloc0_1_0
	lcall	__gptrput
;	..\src\atrs\misc.c:384: *slot3 = RND_NUMBER_TO_SLOTS(RNGBYTE);
	mov	dptr,#_GetRandomSlots_PARM_3
	movx	a,@dptr
	mov	r0,a
	inc	dptr
	movx	a,@dptr
	mov	r1,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
	mov	dptr,#_RNGBYTE
	movx	a,@dptr
	mov	r2,a
	mov	r3,#0x00
	mov	dptr,#__modsint_PARM_2
	mov	a,#0x32
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
	mov	dpl,r2
	mov	dph,r3
	push	ar7
	push	ar6
	push	ar5
	push	ar4
	push	ar1
	push	ar0
	lcall	__modsint
	mov	r2,dpl
	pop	ar0
	pop	ar1
	pop	ar4
	pop	ar5
	pop	ar6
	pop	ar7
	mov	dpl,r0
	mov	dph,r1
	mov	b,r4
	mov	a,r2
	lcall	__gptrput
;	..\src\atrs\misc.c:385: if(*slot1 == *slot2) *slot2 = RND_NUMBER_TO_SLOTS(RNGBYTE);
	mov	a,_GetRandomSlots_sloc0_1_0
	cjne	a,_GetRandomSlots_sloc1_1_0,00102$
	mov	dptr,#_RNGBYTE
	movx	a,@dptr
	mov	r3,a
	mov	r2,#0x00
	mov	dptr,#__modsint_PARM_2
	mov	a,#0x32
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
	mov	dpl,r3
	mov	dph,r2
	push	ar7
	push	ar6
	push	ar5
	push	ar4
	push	ar1
	push	ar0
	lcall	__modsint
	mov	r2,dpl
	mov	r3,dph
	pop	ar0
	pop	ar1
	pop	ar4
	pop	ar5
	pop	ar6
	pop	ar7
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	mov	a,r2
	lcall	__gptrput
00102$:
;	..\src\atrs\misc.c:386: if(*slot2 == *slot3) *slot3 = RND_NUMBER_TO_SLOTS(RNGBYTE);
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	lcall	__gptrget
	mov	r3,a
	mov	dpl,r0
	mov	dph,r1
	mov	b,r4
	lcall	__gptrget
	mov	r2,a
	mov	a,r3
	cjne	a,ar2,00104$
	mov	dptr,#_RNGBYTE
	movx	a,@dptr
	mov	r3,a
	mov	r2,#0x00
	mov	dptr,#__modsint_PARM_2
	mov	a,#0x32
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
	mov	dpl,r3
	mov	dph,r2
	push	ar7
	push	ar6
	push	ar5
	push	ar4
	push	ar1
	push	ar0
	lcall	__modsint
	mov	r2,dpl
	mov	r3,dph
	pop	ar0
	pop	ar1
	pop	ar4
	pop	ar5
	pop	ar6
	pop	ar7
	mov	dpl,r0
	mov	dph,r1
	mov	b,r4
	mov	a,r2
	lcall	__gptrput
00104$:
;	..\src\atrs\misc.c:387: if(*slot1 == *slot3) *slot3 = RND_NUMBER_TO_SLOTS(RNGBYTE);
	push	ar5
	push	ar6
	push	ar7
	mov	dptr,#_GetRandomSlots_slot1_65536_295
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dpl,r2
	mov	dph,r3
	mov	b,r7
	lcall	__gptrget
	mov	r2,a
	mov	dpl,r0
	mov	dph,r1
	mov	b,r4
	lcall	__gptrget
	mov	r7,a
	mov	a,r2
	cjne	a,ar7,00143$
	sjmp	00144$
00143$:
	pop	ar7
	pop	ar6
	pop	ar5
	sjmp	00106$
00144$:
	pop	ar7
	pop	ar6
	pop	ar5
	mov	dptr,#_RNGBYTE
	movx	a,@dptr
	mov	r3,a
	mov	r2,#0x00
	mov	dptr,#__modsint_PARM_2
	mov	a,#0x32
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
	mov	dpl,r3
	mov	dph,r2
	push	ar7
	push	ar6
	push	ar5
	push	ar4
	push	ar1
	push	ar0
	lcall	__modsint
	mov	r2,dpl
	mov	r3,dph
	pop	ar0
	pop	ar1
	pop	ar4
	pop	ar5
	pop	ar6
	pop	ar7
	mov	dpl,r0
	mov	dph,r1
	mov	b,r4
	mov	a,r2
	lcall	__gptrput
00106$:
;	..\src\atrs\misc.c:389: if(*slot1 > *slot2) swap_variables(slot1,slot2);
	mov	dptr,#_GetRandomSlots_slot1_65536_295
	movx	a,@dptr
	mov	_GetRandomSlots_sloc2_1_0,a
	inc	dptr
	movx	a,@dptr
	mov	(_GetRandomSlots_sloc2_1_0 + 1),a
	inc	dptr
	movx	a,@dptr
	mov	(_GetRandomSlots_sloc2_1_0 + 2),a
	mov	dpl,_GetRandomSlots_sloc2_1_0
	mov	dph,(_GetRandomSlots_sloc2_1_0 + 1)
	mov	b,(_GetRandomSlots_sloc2_1_0 + 2)
	lcall	__gptrget
	mov	r3,a
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	lcall	__gptrget
	clr	c
	subb	a,r3
	jnc	00108$
	mov	dptr,#_swap_variables_PARM_2
	mov	a,r5
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
	mov	dpl,_GetRandomSlots_sloc2_1_0
	mov	dph,(_GetRandomSlots_sloc2_1_0 + 1)
	mov	b,(_GetRandomSlots_sloc2_1_0 + 2)
	push	ar4
	push	ar1
	push	ar0
	lcall	_swap_variables
	pop	ar0
	pop	ar1
	pop	ar4
00108$:
;	..\src\atrs\misc.c:390: if(*slot2 > *slot3) swap_variables(slot2,slot3);
	mov	dptr,#_GetRandomSlots_PARM_2
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	lcall	__gptrget
	mov	r3,a
	mov	dpl,r0
	mov	dph,r1
	mov	b,r4
	lcall	__gptrget
	clr	c
	subb	a,r3
	jnc	00110$
	mov	dptr,#_swap_variables_PARM_2
	mov	a,r0
	movx	@dptr,a
	mov	a,r1
	inc	dptr
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	lcall	_swap_variables
00110$:
;	..\src\atrs\misc.c:391: if(*slot1 > *slot2) swap_variables(slot1,slot2);
	mov	dptr,#_GetRandomSlots_slot1_65536_295
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	lcall	__gptrget
	mov	r4,a
	mov	dptr,#_GetRandomSlots_PARM_2
	movx	a,@dptr
	mov	r1,a
	inc	dptr
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	mov	dpl,r1
	mov	dph,r2
	mov	b,r3
	lcall	__gptrget
	clr	c
	subb	a,r4
	jnc	00113$
	mov	dptr,#_swap_variables_PARM_2
	mov	a,r1
	movx	@dptr,a
	mov	a,r2
	inc	dptr
	movx	@dptr,a
	mov	a,r3
	inc	dptr
	movx	@dptr,a
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
;	..\src\atrs\misc.c:392: }
	ljmp	_swap_variables
00113$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'AssemblyPacket'
;------------------------------------------------------------
;sloc0                     Allocated with name '_AssemblyPacket_sloc0_1_0'
;sloc1                     Allocated with name '_AssemblyPacket_sloc1_1_0'
;Copy1                     Allocated with name '_AssemblyPacket_PARM_2'
;Copy2                     Allocated with name '_AssemblyPacket_PARM_3'
;Payload                   Allocated with name '_AssemblyPacket_PARM_4'
;PayloadLength             Allocated with name '_AssemblyPacket_PARM_5'
;ack                       Allocated with name '_AssemblyPacket_PARM_6'
;TxPackage                 Allocated with name '_AssemblyPacket_PARM_7'
;TotalLength               Allocated with name '_AssemblyPacket_PARM_8'
;GoldCode                  Allocated with name '_AssemblyPacket_GoldCode_65536_297'
;boardID                   Allocated with name '_AssemblyPacket_boardID_65536_298'
;HMAC                      Allocated with name '_AssemblyPacket_HMAC_65536_298'
;StdPrem                   Allocated with name '_AssemblyPacket_StdPrem_65536_298'
;CRC32                     Allocated with name '_AssemblyPacket_CRC32_65536_298'
;------------------------------------------------------------
;	..\src\atrs\misc.c:407: void AssemblyPacket(uint32_t GoldCode, uint8_t Copy1, uint8_t Copy2, uint8_t *Payload, uint8_t PayloadLength, uint8_t ack, uint8_t *TxPackage, uint8_t *TotalLength)
;	-----------------------------------------
;	 function AssemblyPacket
;	-----------------------------------------
_AssemblyPacket:
	mov	r7,dpl
	mov	r6,dph
	mov	r5,b
	mov	r4,a
	mov	dptr,#_AssemblyPacket_GoldCode_65536_297
	mov	a,r7
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
;	..\src\atrs\misc.c:411: uint32_t HMAC= 0x12345678;
	mov	dptr,#_AssemblyPacket_HMAC_65536_298
	mov	a,#0x78
	movx	@dptr,a
	mov	a,#0x56
	inc	dptr
	movx	@dptr,a
	mov	a,#0x34
	inc	dptr
	movx	@dptr,a
	mov	a,#0x12
	inc	dptr
	movx	@dptr,a
;	..\src\atrs\misc.c:415: *TxPackage =(uint8_t) (GoldCode & 0x000000FF);
	mov	dptr,#_AssemblyPacket_PARM_7
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#_AssemblyPacket_GoldCode_65536_297
	movx	a,@dptr
	mov	_AssemblyPacket_sloc0_1_0,a
	inc	dptr
	movx	a,@dptr
	mov	(_AssemblyPacket_sloc0_1_0 + 1),a
	inc	dptr
	movx	a,@dptr
	mov	(_AssemblyPacket_sloc0_1_0 + 2),a
	inc	dptr
	movx	a,@dptr
	mov	(_AssemblyPacket_sloc0_1_0 + 3),a
	mov	r0,_AssemblyPacket_sloc0_1_0
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	mov	a,r0
	lcall	__gptrput
;	..\src\atrs\misc.c:416: *(TxPackage+1) = (uint8_t) ((GoldCode & 0x0000FF00)>>8);
	mov	a,#0x01
	add	a,r5
	mov	_AssemblyPacket_sloc1_1_0,a
	clr	a
	addc	a,r6
	mov	(_AssemblyPacket_sloc1_1_0 + 1),a
	mov	(_AssemblyPacket_sloc1_1_0 + 2),r7
	mov	r2,(_AssemblyPacket_sloc0_1_0 + 1)
	mov	ar1,r2
	mov	dpl,_AssemblyPacket_sloc1_1_0
	mov	dph,(_AssemblyPacket_sloc1_1_0 + 1)
	mov	b,(_AssemblyPacket_sloc1_1_0 + 2)
	mov	a,r1
	lcall	__gptrput
;	..\src\atrs\misc.c:417: *(TxPackage+2) = (uint8_t) ((GoldCode & 0x00FF0000)>>16);
	mov	a,#0x02
	add	a,r5
	mov	_AssemblyPacket_sloc1_1_0,a
	clr	a
	addc	a,r6
	mov	(_AssemblyPacket_sloc1_1_0 + 1),a
	mov	(_AssemblyPacket_sloc1_1_0 + 2),r7
	mov	r3,(_AssemblyPacket_sloc0_1_0 + 2)
	mov	ar0,r3
	mov	dpl,_AssemblyPacket_sloc1_1_0
	mov	dph,(_AssemblyPacket_sloc1_1_0 + 1)
	mov	b,(_AssemblyPacket_sloc1_1_0 + 2)
	mov	a,r0
	lcall	__gptrput
;	..\src\atrs\misc.c:418: *(TxPackage+3) = (uint8_t) (((GoldCode & 0x7F000000)>>24)| (Copy1 & 0x01)<<7);
	mov	a,#0x03
	add	a,r5
	mov	_AssemblyPacket_sloc1_1_0,a
	clr	a
	addc	a,r6
	mov	(_AssemblyPacket_sloc1_1_0 + 1),a
	mov	(_AssemblyPacket_sloc1_1_0 + 2),r7
	clr	a
	mov	a,#0x7f
	anl	a,(_AssemblyPacket_sloc0_1_0 + 3)
	mov	r0,a
	mov	dptr,#_AssemblyPacket_PARM_2
	movx	a,@dptr
	mov	r4,a
	anl	a,#0x01
	rr	a
	anl	a,#0x80
	mov	r3,a
	mov	a,r0
	orl	ar3,a
	mov	dpl,_AssemblyPacket_sloc1_1_0
	mov	dph,(_AssemblyPacket_sloc1_1_0 + 1)
	mov	b,(_AssemblyPacket_sloc1_1_0 + 2)
	mov	a,r3
	lcall	__gptrput
;	..\src\atrs\misc.c:419: *(TxPackage+4) = (uint8_t)  (Copy1 & 0xFE)>>1 | ((Copy2 & 0x07)<<5);
	mov	a,#0x04
	add	a,r5
	mov	r1,a
	clr	a
	addc	a,r6
	mov	r2,a
	mov	ar3,r7
	anl	ar4,#0xfe
	mov	a,r4
	clr	c
	rrc	a
	mov	_AssemblyPacket_sloc1_1_0,a
	mov	dptr,#_AssemblyPacket_PARM_3
	movx	a,@dptr
	mov	r0,a
	anl	a,#0x07
	swap	a
	rl	a
	anl	a,#0xe0
	mov	_AssemblyPacket_sloc0_1_0,a
	mov	r4,_AssemblyPacket_sloc1_1_0
	orl	ar4,a
	mov	dpl,r1
	mov	dph,r2
	mov	b,r3
	mov	a,r4
	lcall	__gptrput
;	..\src\atrs\misc.c:420: *(TxPackage+5) = (uint8_t) ((Copy2 & 0x38)>>3) | (boardID & 0x00001F)<<3;
	mov	a,#0x05
	add	a,r5
	mov	r2,a
	clr	a
	addc	a,r6
	mov	r3,a
	mov	ar4,r7
	anl	ar0,#0x38
	clr	a
	rl	a
	xch	a,r0
	swap	a
	rl	a
	anl	a,#0x1f
	xrl	a,r0
	xch	a,r0
	anl	a,#0x1f
	xch	a,r0
	xrl	a,r0
	xch	a,r0
	jnb	acc.4,00103$
	orl	a,#0xe0
00103$:
	orl	ar0,#0xb0
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	mov	a,r0
	lcall	__gptrput
;	..\src\atrs\misc.c:421: *(TxPackage+6) = (uint8_t) ((boardID & 0x001FE0)>>5);
	mov	a,#0x06
	add	a,r5
	mov	r2,a
	clr	a
	addc	a,r6
	mov	r3,a
	mov	ar4,r7
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	mov	a,#0xa2
	lcall	__gptrput
;	..\src\atrs\misc.c:422: *(TxPackage+7) = (uint8_t) ((boardID & 0x1FE000)>>13);
	mov	a,#0x07
	add	a,r5
	mov	r2,a
	clr	a
	addc	a,r6
	mov	r3,a
	mov	ar4,r7
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	mov	a,#0x91
	lcall	__gptrput
;	..\src\atrs\misc.c:423: *(TxPackage+8) = (uint8_t) ((boardID & 0xE0000000)>>21)| (StdPrem & 0x0001) <<4|(ack << 5 & 0x0001);//quedan los tres bits reservados
	mov	a,#0x08
	add	a,r5
	mov	r2,a
	clr	a
	addc	a,r6
	mov	r3,a
	mov	ar4,r7
	mov	a,#0x01
	anl	a,#0x01
	swap	a
	anl	a,#0xf0
	mov	r1,a
	mov	dptr,#_AssemblyPacket_PARM_6
	movx	a,@dptr
	swap	a
	rl	a
	anl	a,#0xe0
	mov	r0,a
	anl	ar0,#0x01
	mov	a,r1
	orl	ar0,a
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	mov	a,r0
	lcall	__gptrput
;	..\src\atrs\misc.c:424: memcpy(TxPackage+9,&HMAC,sizeof(uint32_t));
	mov	a,#0x09
	add	a,r5
	mov	r2,a
	clr	a
	addc	a,r6
	mov	r3,a
	mov	ar4,r7
	mov	dptr,#_memcpy_PARM_2
	mov	a,#_AssemblyPacket_HMAC_65536_298
	movx	@dptr,a
	mov	a,#(_AssemblyPacket_HMAC_65536_298 >> 8)
	inc	dptr
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_memcpy_PARM_3
	mov	a,#0x04
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	push	ar7
	push	ar6
	push	ar5
	lcall	_memcpy
	pop	ar5
	pop	ar6
	pop	ar7
;	..\src\atrs\misc.c:425: memcpy(TxPackage+13,Payload,sizeof(uint8_t)*PayloadLength);
	mov	a,#0x0d
	add	a,r5
	mov	r2,a
	clr	a
	addc	a,r6
	mov	r3,a
	mov	ar4,r7
	mov	_AssemblyPacket_sloc1_1_0,r2
	mov	(_AssemblyPacket_sloc1_1_0 + 1),r3
	mov	(_AssemblyPacket_sloc1_1_0 + 2),r4
	mov	dptr,#_AssemblyPacket_PARM_4
	movx	a,@dptr
	mov	r0,a
	inc	dptr
	movx	a,@dptr
	mov	r1,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
	mov	_AssemblyPacket_sloc0_1_0,r0
	mov	(_AssemblyPacket_sloc0_1_0 + 1),r1
	mov	(_AssemblyPacket_sloc0_1_0 + 2),r4
	mov	dptr,#_AssemblyPacket_PARM_5
	movx	a,@dptr
	mov	r3,a
	mov	r2,a
	mov	r4,#0x00
	mov	dptr,#_memcpy_PARM_2
	mov	a,_AssemblyPacket_sloc0_1_0
	movx	@dptr,a
	mov	a,(_AssemblyPacket_sloc0_1_0 + 1)
	inc	dptr
	movx	@dptr,a
	mov	a,(_AssemblyPacket_sloc0_1_0 + 2)
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_memcpy_PARM_3
	mov	a,r2
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
	mov	dpl,_AssemblyPacket_sloc1_1_0
	mov	dph,(_AssemblyPacket_sloc1_1_0 + 1)
	mov	b,(_AssemblyPacket_sloc1_1_0 + 2)
	push	ar7
	push	ar6
	push	ar5
	push	ar4
	push	ar3
	push	ar2
	lcall	_memcpy
	pop	ar2
	pop	ar3
	pop	ar4
	pop	ar5
	pop	ar6
	pop	ar7
;	..\src\atrs\misc.c:426: CRC32 = crc_crc32_msb(TxPackage,13+PayloadLength,0xFFFFFFFF);
	mov	a,#0x0d
	add	a,r2
	mov	r2,a
	clr	a
	addc	a,r4
	mov	r4,a
	push	ar3
	mov	a,#0xff
	push	acc
	push	acc
	push	acc
	push	acc
	push	ar2
	push	ar4
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	lcall	_crc_crc32_msb
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfa
	mov	sp,a
	pop	ar3
	mov	dptr,#_AssemblyPacket_CRC32_65536_298
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\atrs\misc.c:427: memcpy(TxPackage+13+PayloadLength,&CRC32,sizeof(uint32_t));
	mov	dptr,#_AssemblyPacket_PARM_7
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	a,#0x0d
	add	a,r5
	mov	r5,a
	clr	a
	addc	a,r6
	mov	r6,a
	mov	a,r3
	add	a,r5
	mov	r5,a
	clr	a
	addc	a,r6
	mov	r6,a
	mov	dptr,#_memcpy_PARM_2
	mov	a,#_AssemblyPacket_CRC32_65536_298
	movx	@dptr,a
	mov	a,#(_AssemblyPacket_CRC32_65536_298 >> 8)
	inc	dptr
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_memcpy_PARM_3
	mov	a,#0x04
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	push	ar3
	lcall	_memcpy
	pop	ar3
;	..\src\atrs\misc.c:428: *TotalLength = 13+PayloadLength+4;
	mov	dptr,#_AssemblyPacket_PARM_8
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	a,#0x11
	add	a,r3
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
;	..\src\atrs\misc.c:429: return;
;	..\src\atrs\misc.c:431: }
	ljmp	__gptrput
	.area CSEG    (CODE)
	.area CONST   (CODE)
	.area CONST   (CODE)
___str_0:
	.ascii " transmit "
	.db 0x00
	.area CSEG    (CODE)
	.area CONST   (CODE)
___str_1:
	.ascii " "
	.db 0x0d
	.db 0x0a
	.ascii " GPS callback "
	.db 0x0d
	.db 0x0a
	.db 0x00
	.area CSEG    (CODE)
	.area CONST   (CODE)
___str_2:
	.ascii " frecuencia: "
	.db 0x00
	.area CSEG    (CODE)
	.area CONST   (CODE)
___str_3:
	.ascii " "
	.db 0x0a
	.ascii " backoff timer expired "
	.db 0x00
	.area CSEG    (CODE)
	.area CONST   (CODE)
___str_4:
	.ascii "ACK timer expired"
	.db 0x00
	.area CSEG    (CODE)
	.area CONST   (CODE)
___str_5:
	.ascii " "
	.db 0x0a
	.ascii " Backoff timer started "
	.db 0x0a
	.db 0x00
	.area CSEG    (CODE)
	.area CONST   (CODE)
___str_6:
	.ascii " ACK timer started"
	.db 0x00
	.area CSEG    (CODE)
	.area XINIT   (CODE)
	.area CABS    (ABS,CODE)
