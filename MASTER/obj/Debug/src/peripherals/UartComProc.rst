                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.9.3 #11354 (MINGW32)
                                      4 ;--------------------------------------------------------
                                      5 	.module UartComProc
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _UART_ParseMessage
                                     12 	.globl _delay_ms
                                     13 	.globl _memcpy
                                     14 	.globl _free
                                     15 	.globl _malloc
                                     16 	.globl _uart_timer1_baud
                                     17 	.globl _uart0_tx
                                     18 	.globl _uart0_rx
                                     19 	.globl _uart0_init
                                     20 	.globl _uart0_rxcount
                                     21 	.globl _PORTC_7
                                     22 	.globl _PORTC_6
                                     23 	.globl _PORTC_5
                                     24 	.globl _PORTC_4
                                     25 	.globl _PORTC_3
                                     26 	.globl _PORTC_2
                                     27 	.globl _PORTC_1
                                     28 	.globl _PORTC_0
                                     29 	.globl _PORTB_7
                                     30 	.globl _PORTB_6
                                     31 	.globl _PORTB_5
                                     32 	.globl _PORTB_4
                                     33 	.globl _PORTB_3
                                     34 	.globl _PORTB_2
                                     35 	.globl _PORTB_1
                                     36 	.globl _PORTB_0
                                     37 	.globl _PORTA_7
                                     38 	.globl _PORTA_6
                                     39 	.globl _PORTA_5
                                     40 	.globl _PORTA_4
                                     41 	.globl _PORTA_3
                                     42 	.globl _PORTA_2
                                     43 	.globl _PORTA_1
                                     44 	.globl _PORTA_0
                                     45 	.globl _PINC_7
                                     46 	.globl _PINC_6
                                     47 	.globl _PINC_5
                                     48 	.globl _PINC_4
                                     49 	.globl _PINC_3
                                     50 	.globl _PINC_2
                                     51 	.globl _PINC_1
                                     52 	.globl _PINC_0
                                     53 	.globl _PINB_7
                                     54 	.globl _PINB_6
                                     55 	.globl _PINB_5
                                     56 	.globl _PINB_4
                                     57 	.globl _PINB_3
                                     58 	.globl _PINB_2
                                     59 	.globl _PINB_1
                                     60 	.globl _PINB_0
                                     61 	.globl _PINA_7
                                     62 	.globl _PINA_6
                                     63 	.globl _PINA_5
                                     64 	.globl _PINA_4
                                     65 	.globl _PINA_3
                                     66 	.globl _PINA_2
                                     67 	.globl _PINA_1
                                     68 	.globl _PINA_0
                                     69 	.globl _CY
                                     70 	.globl _AC
                                     71 	.globl _F0
                                     72 	.globl _RS1
                                     73 	.globl _RS0
                                     74 	.globl _OV
                                     75 	.globl _F1
                                     76 	.globl _P
                                     77 	.globl _IP_7
                                     78 	.globl _IP_6
                                     79 	.globl _IP_5
                                     80 	.globl _IP_4
                                     81 	.globl _IP_3
                                     82 	.globl _IP_2
                                     83 	.globl _IP_1
                                     84 	.globl _IP_0
                                     85 	.globl _EA
                                     86 	.globl _IE_7
                                     87 	.globl _IE_6
                                     88 	.globl _IE_5
                                     89 	.globl _IE_4
                                     90 	.globl _IE_3
                                     91 	.globl _IE_2
                                     92 	.globl _IE_1
                                     93 	.globl _IE_0
                                     94 	.globl _EIP_7
                                     95 	.globl _EIP_6
                                     96 	.globl _EIP_5
                                     97 	.globl _EIP_4
                                     98 	.globl _EIP_3
                                     99 	.globl _EIP_2
                                    100 	.globl _EIP_1
                                    101 	.globl _EIP_0
                                    102 	.globl _EIE_7
                                    103 	.globl _EIE_6
                                    104 	.globl _EIE_5
                                    105 	.globl _EIE_4
                                    106 	.globl _EIE_3
                                    107 	.globl _EIE_2
                                    108 	.globl _EIE_1
                                    109 	.globl _EIE_0
                                    110 	.globl _E2IP_7
                                    111 	.globl _E2IP_6
                                    112 	.globl _E2IP_5
                                    113 	.globl _E2IP_4
                                    114 	.globl _E2IP_3
                                    115 	.globl _E2IP_2
                                    116 	.globl _E2IP_1
                                    117 	.globl _E2IP_0
                                    118 	.globl _E2IE_7
                                    119 	.globl _E2IE_6
                                    120 	.globl _E2IE_5
                                    121 	.globl _E2IE_4
                                    122 	.globl _E2IE_3
                                    123 	.globl _E2IE_2
                                    124 	.globl _E2IE_1
                                    125 	.globl _E2IE_0
                                    126 	.globl _B_7
                                    127 	.globl _B_6
                                    128 	.globl _B_5
                                    129 	.globl _B_4
                                    130 	.globl _B_3
                                    131 	.globl _B_2
                                    132 	.globl _B_1
                                    133 	.globl _B_0
                                    134 	.globl _ACC_7
                                    135 	.globl _ACC_6
                                    136 	.globl _ACC_5
                                    137 	.globl _ACC_4
                                    138 	.globl _ACC_3
                                    139 	.globl _ACC_2
                                    140 	.globl _ACC_1
                                    141 	.globl _ACC_0
                                    142 	.globl _WTSTAT
                                    143 	.globl _WTIRQEN
                                    144 	.globl _WTEVTD
                                    145 	.globl _WTEVTD1
                                    146 	.globl _WTEVTD0
                                    147 	.globl _WTEVTC
                                    148 	.globl _WTEVTC1
                                    149 	.globl _WTEVTC0
                                    150 	.globl _WTEVTB
                                    151 	.globl _WTEVTB1
                                    152 	.globl _WTEVTB0
                                    153 	.globl _WTEVTA
                                    154 	.globl _WTEVTA1
                                    155 	.globl _WTEVTA0
                                    156 	.globl _WTCNTR1
                                    157 	.globl _WTCNTB
                                    158 	.globl _WTCNTB1
                                    159 	.globl _WTCNTB0
                                    160 	.globl _WTCNTA
                                    161 	.globl _WTCNTA1
                                    162 	.globl _WTCNTA0
                                    163 	.globl _WTCFGB
                                    164 	.globl _WTCFGA
                                    165 	.globl _WDTRESET
                                    166 	.globl _WDTCFG
                                    167 	.globl _U1STATUS
                                    168 	.globl _U1SHREG
                                    169 	.globl _U1MODE
                                    170 	.globl _U1CTRL
                                    171 	.globl _U0STATUS
                                    172 	.globl _U0SHREG
                                    173 	.globl _U0MODE
                                    174 	.globl _U0CTRL
                                    175 	.globl _T2STATUS
                                    176 	.globl _T2PERIOD
                                    177 	.globl _T2PERIOD1
                                    178 	.globl _T2PERIOD0
                                    179 	.globl _T2MODE
                                    180 	.globl _T2CNT
                                    181 	.globl _T2CNT1
                                    182 	.globl _T2CNT0
                                    183 	.globl _T2CLKSRC
                                    184 	.globl _T1STATUS
                                    185 	.globl _T1PERIOD
                                    186 	.globl _T1PERIOD1
                                    187 	.globl _T1PERIOD0
                                    188 	.globl _T1MODE
                                    189 	.globl _T1CNT
                                    190 	.globl _T1CNT1
                                    191 	.globl _T1CNT0
                                    192 	.globl _T1CLKSRC
                                    193 	.globl _T0STATUS
                                    194 	.globl _T0PERIOD
                                    195 	.globl _T0PERIOD1
                                    196 	.globl _T0PERIOD0
                                    197 	.globl _T0MODE
                                    198 	.globl _T0CNT
                                    199 	.globl _T0CNT1
                                    200 	.globl _T0CNT0
                                    201 	.globl _T0CLKSRC
                                    202 	.globl _SPSTATUS
                                    203 	.globl _SPSHREG
                                    204 	.globl _SPMODE
                                    205 	.globl _SPCLKSRC
                                    206 	.globl _RADIOSTAT
                                    207 	.globl _RADIOSTAT1
                                    208 	.globl _RADIOSTAT0
                                    209 	.globl _RADIODATA
                                    210 	.globl _RADIODATA3
                                    211 	.globl _RADIODATA2
                                    212 	.globl _RADIODATA1
                                    213 	.globl _RADIODATA0
                                    214 	.globl _RADIOADDR
                                    215 	.globl _RADIOADDR1
                                    216 	.globl _RADIOADDR0
                                    217 	.globl _RADIOACC
                                    218 	.globl _OC1STATUS
                                    219 	.globl _OC1PIN
                                    220 	.globl _OC1MODE
                                    221 	.globl _OC1COMP
                                    222 	.globl _OC1COMP1
                                    223 	.globl _OC1COMP0
                                    224 	.globl _OC0STATUS
                                    225 	.globl _OC0PIN
                                    226 	.globl _OC0MODE
                                    227 	.globl _OC0COMP
                                    228 	.globl _OC0COMP1
                                    229 	.globl _OC0COMP0
                                    230 	.globl _NVSTATUS
                                    231 	.globl _NVKEY
                                    232 	.globl _NVDATA
                                    233 	.globl _NVDATA1
                                    234 	.globl _NVDATA0
                                    235 	.globl _NVADDR
                                    236 	.globl _NVADDR1
                                    237 	.globl _NVADDR0
                                    238 	.globl _IC1STATUS
                                    239 	.globl _IC1MODE
                                    240 	.globl _IC1CAPT
                                    241 	.globl _IC1CAPT1
                                    242 	.globl _IC1CAPT0
                                    243 	.globl _IC0STATUS
                                    244 	.globl _IC0MODE
                                    245 	.globl _IC0CAPT
                                    246 	.globl _IC0CAPT1
                                    247 	.globl _IC0CAPT0
                                    248 	.globl _PORTR
                                    249 	.globl _PORTC
                                    250 	.globl _PORTB
                                    251 	.globl _PORTA
                                    252 	.globl _PINR
                                    253 	.globl _PINC
                                    254 	.globl _PINB
                                    255 	.globl _PINA
                                    256 	.globl _DIRR
                                    257 	.globl _DIRC
                                    258 	.globl _DIRB
                                    259 	.globl _DIRA
                                    260 	.globl _DBGLNKSTAT
                                    261 	.globl _DBGLNKBUF
                                    262 	.globl _CODECONFIG
                                    263 	.globl _CLKSTAT
                                    264 	.globl _CLKCON
                                    265 	.globl _ANALOGCOMP
                                    266 	.globl _ADCCONV
                                    267 	.globl _ADCCLKSRC
                                    268 	.globl _ADCCH3CONFIG
                                    269 	.globl _ADCCH2CONFIG
                                    270 	.globl _ADCCH1CONFIG
                                    271 	.globl _ADCCH0CONFIG
                                    272 	.globl __XPAGE
                                    273 	.globl _XPAGE
                                    274 	.globl _SP
                                    275 	.globl _PSW
                                    276 	.globl _PCON
                                    277 	.globl _IP
                                    278 	.globl _IE
                                    279 	.globl _EIP
                                    280 	.globl _EIE
                                    281 	.globl _E2IP
                                    282 	.globl _E2IE
                                    283 	.globl _DPS
                                    284 	.globl _DPTR1
                                    285 	.globl _DPTR0
                                    286 	.globl _DPL1
                                    287 	.globl _DPL
                                    288 	.globl _DPH1
                                    289 	.globl _DPH
                                    290 	.globl _B
                                    291 	.globl _ACC
                                    292 	.globl _UART_ParseAnsMessage_PARM_2
                                    293 	.globl _UART_ParseGpsDataReq_PARM_2
                                    294 	.globl _UART_ParsePwrDwn_PARM_2
                                    295 	.globl _UART_ParseTxMessage_PARM_2
                                    296 	.globl _UART_ParseSetDbgModeMessage_PARM_2
                                    297 	.globl _UART_ParseConfigMessage_PARM_2
                                    298 	.globl _UART_ParseMessage_PARM_3
                                    299 	.globl _UART_ParseMessage_PARM_2
                                    300 	.globl _UART_Calc_CRC32_PARM_3
                                    301 	.globl _UART_Calc_CRC32_PARM_2
                                    302 	.globl _UART_Proc_SendMessage_PARM_3
                                    303 	.globl _UART_Proc_SendMessage_PARM_2
                                    304 	.globl _UART_Proc_ModifiyKissSpecialCharacters_PARM_2
                                    305 	.globl _UART_Proc_VerifyIncomingMsg_PARM_3
                                    306 	.globl _UART_Proc_VerifyIncomingMsg_PARM_2
                                    307 	.globl _AX5043_TIMEGAIN3NB
                                    308 	.globl _AX5043_TIMEGAIN2NB
                                    309 	.globl _AX5043_TIMEGAIN1NB
                                    310 	.globl _AX5043_TIMEGAIN0NB
                                    311 	.globl _AX5043_RXPARAMSETSNB
                                    312 	.globl _AX5043_RXPARAMCURSETNB
                                    313 	.globl _AX5043_PKTMAXLENNB
                                    314 	.globl _AX5043_PKTLENOFFSETNB
                                    315 	.globl _AX5043_PKTLENCFGNB
                                    316 	.globl _AX5043_PKTADDRMASK3NB
                                    317 	.globl _AX5043_PKTADDRMASK2NB
                                    318 	.globl _AX5043_PKTADDRMASK1NB
                                    319 	.globl _AX5043_PKTADDRMASK0NB
                                    320 	.globl _AX5043_PKTADDRCFGNB
                                    321 	.globl _AX5043_PKTADDR3NB
                                    322 	.globl _AX5043_PKTADDR2NB
                                    323 	.globl _AX5043_PKTADDR1NB
                                    324 	.globl _AX5043_PKTADDR0NB
                                    325 	.globl _AX5043_PHASEGAIN3NB
                                    326 	.globl _AX5043_PHASEGAIN2NB
                                    327 	.globl _AX5043_PHASEGAIN1NB
                                    328 	.globl _AX5043_PHASEGAIN0NB
                                    329 	.globl _AX5043_FREQUENCYLEAKNB
                                    330 	.globl _AX5043_FREQUENCYGAIND3NB
                                    331 	.globl _AX5043_FREQUENCYGAIND2NB
                                    332 	.globl _AX5043_FREQUENCYGAIND1NB
                                    333 	.globl _AX5043_FREQUENCYGAIND0NB
                                    334 	.globl _AX5043_FREQUENCYGAINC3NB
                                    335 	.globl _AX5043_FREQUENCYGAINC2NB
                                    336 	.globl _AX5043_FREQUENCYGAINC1NB
                                    337 	.globl _AX5043_FREQUENCYGAINC0NB
                                    338 	.globl _AX5043_FREQUENCYGAINB3NB
                                    339 	.globl _AX5043_FREQUENCYGAINB2NB
                                    340 	.globl _AX5043_FREQUENCYGAINB1NB
                                    341 	.globl _AX5043_FREQUENCYGAINB0NB
                                    342 	.globl _AX5043_FREQUENCYGAINA3NB
                                    343 	.globl _AX5043_FREQUENCYGAINA2NB
                                    344 	.globl _AX5043_FREQUENCYGAINA1NB
                                    345 	.globl _AX5043_FREQUENCYGAINA0NB
                                    346 	.globl _AX5043_FREQDEV13NB
                                    347 	.globl _AX5043_FREQDEV12NB
                                    348 	.globl _AX5043_FREQDEV11NB
                                    349 	.globl _AX5043_FREQDEV10NB
                                    350 	.globl _AX5043_FREQDEV03NB
                                    351 	.globl _AX5043_FREQDEV02NB
                                    352 	.globl _AX5043_FREQDEV01NB
                                    353 	.globl _AX5043_FREQDEV00NB
                                    354 	.globl _AX5043_FOURFSK3NB
                                    355 	.globl _AX5043_FOURFSK2NB
                                    356 	.globl _AX5043_FOURFSK1NB
                                    357 	.globl _AX5043_FOURFSK0NB
                                    358 	.globl _AX5043_DRGAIN3NB
                                    359 	.globl _AX5043_DRGAIN2NB
                                    360 	.globl _AX5043_DRGAIN1NB
                                    361 	.globl _AX5043_DRGAIN0NB
                                    362 	.globl _AX5043_BBOFFSRES3NB
                                    363 	.globl _AX5043_BBOFFSRES2NB
                                    364 	.globl _AX5043_BBOFFSRES1NB
                                    365 	.globl _AX5043_BBOFFSRES0NB
                                    366 	.globl _AX5043_AMPLITUDEGAIN3NB
                                    367 	.globl _AX5043_AMPLITUDEGAIN2NB
                                    368 	.globl _AX5043_AMPLITUDEGAIN1NB
                                    369 	.globl _AX5043_AMPLITUDEGAIN0NB
                                    370 	.globl _AX5043_AGCTARGET3NB
                                    371 	.globl _AX5043_AGCTARGET2NB
                                    372 	.globl _AX5043_AGCTARGET1NB
                                    373 	.globl _AX5043_AGCTARGET0NB
                                    374 	.globl _AX5043_AGCMINMAX3NB
                                    375 	.globl _AX5043_AGCMINMAX2NB
                                    376 	.globl _AX5043_AGCMINMAX1NB
                                    377 	.globl _AX5043_AGCMINMAX0NB
                                    378 	.globl _AX5043_AGCGAIN3NB
                                    379 	.globl _AX5043_AGCGAIN2NB
                                    380 	.globl _AX5043_AGCGAIN1NB
                                    381 	.globl _AX5043_AGCGAIN0NB
                                    382 	.globl _AX5043_AGCAHYST3NB
                                    383 	.globl _AX5043_AGCAHYST2NB
                                    384 	.globl _AX5043_AGCAHYST1NB
                                    385 	.globl _AX5043_AGCAHYST0NB
                                    386 	.globl _AX5043_0xF44NB
                                    387 	.globl _AX5043_0xF35NB
                                    388 	.globl _AX5043_0xF34NB
                                    389 	.globl _AX5043_0xF33NB
                                    390 	.globl _AX5043_0xF32NB
                                    391 	.globl _AX5043_0xF31NB
                                    392 	.globl _AX5043_0xF30NB
                                    393 	.globl _AX5043_0xF26NB
                                    394 	.globl _AX5043_0xF23NB
                                    395 	.globl _AX5043_0xF22NB
                                    396 	.globl _AX5043_0xF21NB
                                    397 	.globl _AX5043_0xF1CNB
                                    398 	.globl _AX5043_0xF18NB
                                    399 	.globl _AX5043_0xF0CNB
                                    400 	.globl _AX5043_0xF00NB
                                    401 	.globl _AX5043_XTALSTATUSNB
                                    402 	.globl _AX5043_XTALOSCNB
                                    403 	.globl _AX5043_XTALCAPNB
                                    404 	.globl _AX5043_XTALAMPLNB
                                    405 	.globl _AX5043_WAKEUPXOEARLYNB
                                    406 	.globl _AX5043_WAKEUPTIMER1NB
                                    407 	.globl _AX5043_WAKEUPTIMER0NB
                                    408 	.globl _AX5043_WAKEUPFREQ1NB
                                    409 	.globl _AX5043_WAKEUPFREQ0NB
                                    410 	.globl _AX5043_WAKEUP1NB
                                    411 	.globl _AX5043_WAKEUP0NB
                                    412 	.globl _AX5043_TXRATE2NB
                                    413 	.globl _AX5043_TXRATE1NB
                                    414 	.globl _AX5043_TXRATE0NB
                                    415 	.globl _AX5043_TXPWRCOEFFE1NB
                                    416 	.globl _AX5043_TXPWRCOEFFE0NB
                                    417 	.globl _AX5043_TXPWRCOEFFD1NB
                                    418 	.globl _AX5043_TXPWRCOEFFD0NB
                                    419 	.globl _AX5043_TXPWRCOEFFC1NB
                                    420 	.globl _AX5043_TXPWRCOEFFC0NB
                                    421 	.globl _AX5043_TXPWRCOEFFB1NB
                                    422 	.globl _AX5043_TXPWRCOEFFB0NB
                                    423 	.globl _AX5043_TXPWRCOEFFA1NB
                                    424 	.globl _AX5043_TXPWRCOEFFA0NB
                                    425 	.globl _AX5043_TRKRFFREQ2NB
                                    426 	.globl _AX5043_TRKRFFREQ1NB
                                    427 	.globl _AX5043_TRKRFFREQ0NB
                                    428 	.globl _AX5043_TRKPHASE1NB
                                    429 	.globl _AX5043_TRKPHASE0NB
                                    430 	.globl _AX5043_TRKFSKDEMOD1NB
                                    431 	.globl _AX5043_TRKFSKDEMOD0NB
                                    432 	.globl _AX5043_TRKFREQ1NB
                                    433 	.globl _AX5043_TRKFREQ0NB
                                    434 	.globl _AX5043_TRKDATARATE2NB
                                    435 	.globl _AX5043_TRKDATARATE1NB
                                    436 	.globl _AX5043_TRKDATARATE0NB
                                    437 	.globl _AX5043_TRKAMPLITUDE1NB
                                    438 	.globl _AX5043_TRKAMPLITUDE0NB
                                    439 	.globl _AX5043_TRKAFSKDEMOD1NB
                                    440 	.globl _AX5043_TRKAFSKDEMOD0NB
                                    441 	.globl _AX5043_TMGTXSETTLENB
                                    442 	.globl _AX5043_TMGTXBOOSTNB
                                    443 	.globl _AX5043_TMGRXSETTLENB
                                    444 	.globl _AX5043_TMGRXRSSINB
                                    445 	.globl _AX5043_TMGRXPREAMBLE3NB
                                    446 	.globl _AX5043_TMGRXPREAMBLE2NB
                                    447 	.globl _AX5043_TMGRXPREAMBLE1NB
                                    448 	.globl _AX5043_TMGRXOFFSACQNB
                                    449 	.globl _AX5043_TMGRXCOARSEAGCNB
                                    450 	.globl _AX5043_TMGRXBOOSTNB
                                    451 	.globl _AX5043_TMGRXAGCNB
                                    452 	.globl _AX5043_TIMER2NB
                                    453 	.globl _AX5043_TIMER1NB
                                    454 	.globl _AX5043_TIMER0NB
                                    455 	.globl _AX5043_SILICONREVISIONNB
                                    456 	.globl _AX5043_SCRATCHNB
                                    457 	.globl _AX5043_RXDATARATE2NB
                                    458 	.globl _AX5043_RXDATARATE1NB
                                    459 	.globl _AX5043_RXDATARATE0NB
                                    460 	.globl _AX5043_RSSIREFERENCENB
                                    461 	.globl _AX5043_RSSIABSTHRNB
                                    462 	.globl _AX5043_RSSINB
                                    463 	.globl _AX5043_REFNB
                                    464 	.globl _AX5043_RADIOSTATENB
                                    465 	.globl _AX5043_RADIOEVENTREQ1NB
                                    466 	.globl _AX5043_RADIOEVENTREQ0NB
                                    467 	.globl _AX5043_RADIOEVENTMASK1NB
                                    468 	.globl _AX5043_RADIOEVENTMASK0NB
                                    469 	.globl _AX5043_PWRMODENB
                                    470 	.globl _AX5043_PWRAMPNB
                                    471 	.globl _AX5043_POWSTICKYSTATNB
                                    472 	.globl _AX5043_POWSTATNB
                                    473 	.globl _AX5043_POWIRQMASKNB
                                    474 	.globl _AX5043_POWCTRL1NB
                                    475 	.globl _AX5043_PLLVCOIRNB
                                    476 	.globl _AX5043_PLLVCOINB
                                    477 	.globl _AX5043_PLLVCODIVNB
                                    478 	.globl _AX5043_PLLRNGCLKNB
                                    479 	.globl _AX5043_PLLRANGINGBNB
                                    480 	.globl _AX5043_PLLRANGINGANB
                                    481 	.globl _AX5043_PLLLOOPBOOSTNB
                                    482 	.globl _AX5043_PLLLOOPNB
                                    483 	.globl _AX5043_PLLLOCKDETNB
                                    484 	.globl _AX5043_PLLCPIBOOSTNB
                                    485 	.globl _AX5043_PLLCPINB
                                    486 	.globl _AX5043_PKTSTOREFLAGSNB
                                    487 	.globl _AX5043_PKTMISCFLAGSNB
                                    488 	.globl _AX5043_PKTCHUNKSIZENB
                                    489 	.globl _AX5043_PKTACCEPTFLAGSNB
                                    490 	.globl _AX5043_PINSTATENB
                                    491 	.globl _AX5043_PINFUNCSYSCLKNB
                                    492 	.globl _AX5043_PINFUNCPWRAMPNB
                                    493 	.globl _AX5043_PINFUNCIRQNB
                                    494 	.globl _AX5043_PINFUNCDCLKNB
                                    495 	.globl _AX5043_PINFUNCDATANB
                                    496 	.globl _AX5043_PINFUNCANTSELNB
                                    497 	.globl _AX5043_MODULATIONNB
                                    498 	.globl _AX5043_MODCFGPNB
                                    499 	.globl _AX5043_MODCFGFNB
                                    500 	.globl _AX5043_MODCFGANB
                                    501 	.globl _AX5043_MAXRFOFFSET2NB
                                    502 	.globl _AX5043_MAXRFOFFSET1NB
                                    503 	.globl _AX5043_MAXRFOFFSET0NB
                                    504 	.globl _AX5043_MAXDROFFSET2NB
                                    505 	.globl _AX5043_MAXDROFFSET1NB
                                    506 	.globl _AX5043_MAXDROFFSET0NB
                                    507 	.globl _AX5043_MATCH1PAT1NB
                                    508 	.globl _AX5043_MATCH1PAT0NB
                                    509 	.globl _AX5043_MATCH1MINNB
                                    510 	.globl _AX5043_MATCH1MAXNB
                                    511 	.globl _AX5043_MATCH1LENNB
                                    512 	.globl _AX5043_MATCH0PAT3NB
                                    513 	.globl _AX5043_MATCH0PAT2NB
                                    514 	.globl _AX5043_MATCH0PAT1NB
                                    515 	.globl _AX5043_MATCH0PAT0NB
                                    516 	.globl _AX5043_MATCH0MINNB
                                    517 	.globl _AX5043_MATCH0MAXNB
                                    518 	.globl _AX5043_MATCH0LENNB
                                    519 	.globl _AX5043_LPOSCSTATUSNB
                                    520 	.globl _AX5043_LPOSCREF1NB
                                    521 	.globl _AX5043_LPOSCREF0NB
                                    522 	.globl _AX5043_LPOSCPER1NB
                                    523 	.globl _AX5043_LPOSCPER0NB
                                    524 	.globl _AX5043_LPOSCKFILT1NB
                                    525 	.globl _AX5043_LPOSCKFILT0NB
                                    526 	.globl _AX5043_LPOSCFREQ1NB
                                    527 	.globl _AX5043_LPOSCFREQ0NB
                                    528 	.globl _AX5043_LPOSCCONFIGNB
                                    529 	.globl _AX5043_IRQREQUEST1NB
                                    530 	.globl _AX5043_IRQREQUEST0NB
                                    531 	.globl _AX5043_IRQMASK1NB
                                    532 	.globl _AX5043_IRQMASK0NB
                                    533 	.globl _AX5043_IRQINVERSION1NB
                                    534 	.globl _AX5043_IRQINVERSION0NB
                                    535 	.globl _AX5043_IFFREQ1NB
                                    536 	.globl _AX5043_IFFREQ0NB
                                    537 	.globl _AX5043_GPADCPERIODNB
                                    538 	.globl _AX5043_GPADCCTRLNB
                                    539 	.globl _AX5043_GPADC13VALUE1NB
                                    540 	.globl _AX5043_GPADC13VALUE0NB
                                    541 	.globl _AX5043_FSKDMIN1NB
                                    542 	.globl _AX5043_FSKDMIN0NB
                                    543 	.globl _AX5043_FSKDMAX1NB
                                    544 	.globl _AX5043_FSKDMAX0NB
                                    545 	.globl _AX5043_FSKDEV2NB
                                    546 	.globl _AX5043_FSKDEV1NB
                                    547 	.globl _AX5043_FSKDEV0NB
                                    548 	.globl _AX5043_FREQB3NB
                                    549 	.globl _AX5043_FREQB2NB
                                    550 	.globl _AX5043_FREQB1NB
                                    551 	.globl _AX5043_FREQB0NB
                                    552 	.globl _AX5043_FREQA3NB
                                    553 	.globl _AX5043_FREQA2NB
                                    554 	.globl _AX5043_FREQA1NB
                                    555 	.globl _AX5043_FREQA0NB
                                    556 	.globl _AX5043_FRAMINGNB
                                    557 	.globl _AX5043_FIFOTHRESH1NB
                                    558 	.globl _AX5043_FIFOTHRESH0NB
                                    559 	.globl _AX5043_FIFOSTATNB
                                    560 	.globl _AX5043_FIFOFREE1NB
                                    561 	.globl _AX5043_FIFOFREE0NB
                                    562 	.globl _AX5043_FIFODATANB
                                    563 	.globl _AX5043_FIFOCOUNT1NB
                                    564 	.globl _AX5043_FIFOCOUNT0NB
                                    565 	.globl _AX5043_FECSYNCNB
                                    566 	.globl _AX5043_FECSTATUSNB
                                    567 	.globl _AX5043_FECNB
                                    568 	.globl _AX5043_ENCODINGNB
                                    569 	.globl _AX5043_DIVERSITYNB
                                    570 	.globl _AX5043_DECIMATIONNB
                                    571 	.globl _AX5043_DACVALUE1NB
                                    572 	.globl _AX5043_DACVALUE0NB
                                    573 	.globl _AX5043_DACCONFIGNB
                                    574 	.globl _AX5043_CRCINIT3NB
                                    575 	.globl _AX5043_CRCINIT2NB
                                    576 	.globl _AX5043_CRCINIT1NB
                                    577 	.globl _AX5043_CRCINIT0NB
                                    578 	.globl _AX5043_BGNDRSSITHRNB
                                    579 	.globl _AX5043_BGNDRSSIGAINNB
                                    580 	.globl _AX5043_BGNDRSSINB
                                    581 	.globl _AX5043_BBTUNENB
                                    582 	.globl _AX5043_BBOFFSCAPNB
                                    583 	.globl _AX5043_AMPLFILTERNB
                                    584 	.globl _AX5043_AGCCOUNTERNB
                                    585 	.globl _AX5043_AFSKSPACE1NB
                                    586 	.globl _AX5043_AFSKSPACE0NB
                                    587 	.globl _AX5043_AFSKMARK1NB
                                    588 	.globl _AX5043_AFSKMARK0NB
                                    589 	.globl _AX5043_AFSKCTRLNB
                                    590 	.globl _AX5043_TIMEGAIN3
                                    591 	.globl _AX5043_TIMEGAIN2
                                    592 	.globl _AX5043_TIMEGAIN1
                                    593 	.globl _AX5043_TIMEGAIN0
                                    594 	.globl _AX5043_RXPARAMSETS
                                    595 	.globl _AX5043_RXPARAMCURSET
                                    596 	.globl _AX5043_PKTMAXLEN
                                    597 	.globl _AX5043_PKTLENOFFSET
                                    598 	.globl _AX5043_PKTLENCFG
                                    599 	.globl _AX5043_PKTADDRMASK3
                                    600 	.globl _AX5043_PKTADDRMASK2
                                    601 	.globl _AX5043_PKTADDRMASK1
                                    602 	.globl _AX5043_PKTADDRMASK0
                                    603 	.globl _AX5043_PKTADDRCFG
                                    604 	.globl _AX5043_PKTADDR3
                                    605 	.globl _AX5043_PKTADDR2
                                    606 	.globl _AX5043_PKTADDR1
                                    607 	.globl _AX5043_PKTADDR0
                                    608 	.globl _AX5043_PHASEGAIN3
                                    609 	.globl _AX5043_PHASEGAIN2
                                    610 	.globl _AX5043_PHASEGAIN1
                                    611 	.globl _AX5043_PHASEGAIN0
                                    612 	.globl _AX5043_FREQUENCYLEAK
                                    613 	.globl _AX5043_FREQUENCYGAIND3
                                    614 	.globl _AX5043_FREQUENCYGAIND2
                                    615 	.globl _AX5043_FREQUENCYGAIND1
                                    616 	.globl _AX5043_FREQUENCYGAIND0
                                    617 	.globl _AX5043_FREQUENCYGAINC3
                                    618 	.globl _AX5043_FREQUENCYGAINC2
                                    619 	.globl _AX5043_FREQUENCYGAINC1
                                    620 	.globl _AX5043_FREQUENCYGAINC0
                                    621 	.globl _AX5043_FREQUENCYGAINB3
                                    622 	.globl _AX5043_FREQUENCYGAINB2
                                    623 	.globl _AX5043_FREQUENCYGAINB1
                                    624 	.globl _AX5043_FREQUENCYGAINB0
                                    625 	.globl _AX5043_FREQUENCYGAINA3
                                    626 	.globl _AX5043_FREQUENCYGAINA2
                                    627 	.globl _AX5043_FREQUENCYGAINA1
                                    628 	.globl _AX5043_FREQUENCYGAINA0
                                    629 	.globl _AX5043_FREQDEV13
                                    630 	.globl _AX5043_FREQDEV12
                                    631 	.globl _AX5043_FREQDEV11
                                    632 	.globl _AX5043_FREQDEV10
                                    633 	.globl _AX5043_FREQDEV03
                                    634 	.globl _AX5043_FREQDEV02
                                    635 	.globl _AX5043_FREQDEV01
                                    636 	.globl _AX5043_FREQDEV00
                                    637 	.globl _AX5043_FOURFSK3
                                    638 	.globl _AX5043_FOURFSK2
                                    639 	.globl _AX5043_FOURFSK1
                                    640 	.globl _AX5043_FOURFSK0
                                    641 	.globl _AX5043_DRGAIN3
                                    642 	.globl _AX5043_DRGAIN2
                                    643 	.globl _AX5043_DRGAIN1
                                    644 	.globl _AX5043_DRGAIN0
                                    645 	.globl _AX5043_BBOFFSRES3
                                    646 	.globl _AX5043_BBOFFSRES2
                                    647 	.globl _AX5043_BBOFFSRES1
                                    648 	.globl _AX5043_BBOFFSRES0
                                    649 	.globl _AX5043_AMPLITUDEGAIN3
                                    650 	.globl _AX5043_AMPLITUDEGAIN2
                                    651 	.globl _AX5043_AMPLITUDEGAIN1
                                    652 	.globl _AX5043_AMPLITUDEGAIN0
                                    653 	.globl _AX5043_AGCTARGET3
                                    654 	.globl _AX5043_AGCTARGET2
                                    655 	.globl _AX5043_AGCTARGET1
                                    656 	.globl _AX5043_AGCTARGET0
                                    657 	.globl _AX5043_AGCMINMAX3
                                    658 	.globl _AX5043_AGCMINMAX2
                                    659 	.globl _AX5043_AGCMINMAX1
                                    660 	.globl _AX5043_AGCMINMAX0
                                    661 	.globl _AX5043_AGCGAIN3
                                    662 	.globl _AX5043_AGCGAIN2
                                    663 	.globl _AX5043_AGCGAIN1
                                    664 	.globl _AX5043_AGCGAIN0
                                    665 	.globl _AX5043_AGCAHYST3
                                    666 	.globl _AX5043_AGCAHYST2
                                    667 	.globl _AX5043_AGCAHYST1
                                    668 	.globl _AX5043_AGCAHYST0
                                    669 	.globl _AX5043_0xF44
                                    670 	.globl _AX5043_0xF35
                                    671 	.globl _AX5043_0xF34
                                    672 	.globl _AX5043_0xF33
                                    673 	.globl _AX5043_0xF32
                                    674 	.globl _AX5043_0xF31
                                    675 	.globl _AX5043_0xF30
                                    676 	.globl _AX5043_0xF26
                                    677 	.globl _AX5043_0xF23
                                    678 	.globl _AX5043_0xF22
                                    679 	.globl _AX5043_0xF21
                                    680 	.globl _AX5043_0xF1C
                                    681 	.globl _AX5043_0xF18
                                    682 	.globl _AX5043_0xF0C
                                    683 	.globl _AX5043_0xF00
                                    684 	.globl _AX5043_XTALSTATUS
                                    685 	.globl _AX5043_XTALOSC
                                    686 	.globl _AX5043_XTALCAP
                                    687 	.globl _AX5043_XTALAMPL
                                    688 	.globl _AX5043_WAKEUPXOEARLY
                                    689 	.globl _AX5043_WAKEUPTIMER1
                                    690 	.globl _AX5043_WAKEUPTIMER0
                                    691 	.globl _AX5043_WAKEUPFREQ1
                                    692 	.globl _AX5043_WAKEUPFREQ0
                                    693 	.globl _AX5043_WAKEUP1
                                    694 	.globl _AX5043_WAKEUP0
                                    695 	.globl _AX5043_TXRATE2
                                    696 	.globl _AX5043_TXRATE1
                                    697 	.globl _AX5043_TXRATE0
                                    698 	.globl _AX5043_TXPWRCOEFFE1
                                    699 	.globl _AX5043_TXPWRCOEFFE0
                                    700 	.globl _AX5043_TXPWRCOEFFD1
                                    701 	.globl _AX5043_TXPWRCOEFFD0
                                    702 	.globl _AX5043_TXPWRCOEFFC1
                                    703 	.globl _AX5043_TXPWRCOEFFC0
                                    704 	.globl _AX5043_TXPWRCOEFFB1
                                    705 	.globl _AX5043_TXPWRCOEFFB0
                                    706 	.globl _AX5043_TXPWRCOEFFA1
                                    707 	.globl _AX5043_TXPWRCOEFFA0
                                    708 	.globl _AX5043_TRKRFFREQ2
                                    709 	.globl _AX5043_TRKRFFREQ1
                                    710 	.globl _AX5043_TRKRFFREQ0
                                    711 	.globl _AX5043_TRKPHASE1
                                    712 	.globl _AX5043_TRKPHASE0
                                    713 	.globl _AX5043_TRKFSKDEMOD1
                                    714 	.globl _AX5043_TRKFSKDEMOD0
                                    715 	.globl _AX5043_TRKFREQ1
                                    716 	.globl _AX5043_TRKFREQ0
                                    717 	.globl _AX5043_TRKDATARATE2
                                    718 	.globl _AX5043_TRKDATARATE1
                                    719 	.globl _AX5043_TRKDATARATE0
                                    720 	.globl _AX5043_TRKAMPLITUDE1
                                    721 	.globl _AX5043_TRKAMPLITUDE0
                                    722 	.globl _AX5043_TRKAFSKDEMOD1
                                    723 	.globl _AX5043_TRKAFSKDEMOD0
                                    724 	.globl _AX5043_TMGTXSETTLE
                                    725 	.globl _AX5043_TMGTXBOOST
                                    726 	.globl _AX5043_TMGRXSETTLE
                                    727 	.globl _AX5043_TMGRXRSSI
                                    728 	.globl _AX5043_TMGRXPREAMBLE3
                                    729 	.globl _AX5043_TMGRXPREAMBLE2
                                    730 	.globl _AX5043_TMGRXPREAMBLE1
                                    731 	.globl _AX5043_TMGRXOFFSACQ
                                    732 	.globl _AX5043_TMGRXCOARSEAGC
                                    733 	.globl _AX5043_TMGRXBOOST
                                    734 	.globl _AX5043_TMGRXAGC
                                    735 	.globl _AX5043_TIMER2
                                    736 	.globl _AX5043_TIMER1
                                    737 	.globl _AX5043_TIMER0
                                    738 	.globl _AX5043_SILICONREVISION
                                    739 	.globl _AX5043_SCRATCH
                                    740 	.globl _AX5043_RXDATARATE2
                                    741 	.globl _AX5043_RXDATARATE1
                                    742 	.globl _AX5043_RXDATARATE0
                                    743 	.globl _AX5043_RSSIREFERENCE
                                    744 	.globl _AX5043_RSSIABSTHR
                                    745 	.globl _AX5043_RSSI
                                    746 	.globl _AX5043_REF
                                    747 	.globl _AX5043_RADIOSTATE
                                    748 	.globl _AX5043_RADIOEVENTREQ1
                                    749 	.globl _AX5043_RADIOEVENTREQ0
                                    750 	.globl _AX5043_RADIOEVENTMASK1
                                    751 	.globl _AX5043_RADIOEVENTMASK0
                                    752 	.globl _AX5043_PWRMODE
                                    753 	.globl _AX5043_PWRAMP
                                    754 	.globl _AX5043_POWSTICKYSTAT
                                    755 	.globl _AX5043_POWSTAT
                                    756 	.globl _AX5043_POWIRQMASK
                                    757 	.globl _AX5043_POWCTRL1
                                    758 	.globl _AX5043_PLLVCOIR
                                    759 	.globl _AX5043_PLLVCOI
                                    760 	.globl _AX5043_PLLVCODIV
                                    761 	.globl _AX5043_PLLRNGCLK
                                    762 	.globl _AX5043_PLLRANGINGB
                                    763 	.globl _AX5043_PLLRANGINGA
                                    764 	.globl _AX5043_PLLLOOPBOOST
                                    765 	.globl _AX5043_PLLLOOP
                                    766 	.globl _AX5043_PLLLOCKDET
                                    767 	.globl _AX5043_PLLCPIBOOST
                                    768 	.globl _AX5043_PLLCPI
                                    769 	.globl _AX5043_PKTSTOREFLAGS
                                    770 	.globl _AX5043_PKTMISCFLAGS
                                    771 	.globl _AX5043_PKTCHUNKSIZE
                                    772 	.globl _AX5043_PKTACCEPTFLAGS
                                    773 	.globl _AX5043_PINSTATE
                                    774 	.globl _AX5043_PINFUNCSYSCLK
                                    775 	.globl _AX5043_PINFUNCPWRAMP
                                    776 	.globl _AX5043_PINFUNCIRQ
                                    777 	.globl _AX5043_PINFUNCDCLK
                                    778 	.globl _AX5043_PINFUNCDATA
                                    779 	.globl _AX5043_PINFUNCANTSEL
                                    780 	.globl _AX5043_MODULATION
                                    781 	.globl _AX5043_MODCFGP
                                    782 	.globl _AX5043_MODCFGF
                                    783 	.globl _AX5043_MODCFGA
                                    784 	.globl _AX5043_MAXRFOFFSET2
                                    785 	.globl _AX5043_MAXRFOFFSET1
                                    786 	.globl _AX5043_MAXRFOFFSET0
                                    787 	.globl _AX5043_MAXDROFFSET2
                                    788 	.globl _AX5043_MAXDROFFSET1
                                    789 	.globl _AX5043_MAXDROFFSET0
                                    790 	.globl _AX5043_MATCH1PAT1
                                    791 	.globl _AX5043_MATCH1PAT0
                                    792 	.globl _AX5043_MATCH1MIN
                                    793 	.globl _AX5043_MATCH1MAX
                                    794 	.globl _AX5043_MATCH1LEN
                                    795 	.globl _AX5043_MATCH0PAT3
                                    796 	.globl _AX5043_MATCH0PAT2
                                    797 	.globl _AX5043_MATCH0PAT1
                                    798 	.globl _AX5043_MATCH0PAT0
                                    799 	.globl _AX5043_MATCH0MIN
                                    800 	.globl _AX5043_MATCH0MAX
                                    801 	.globl _AX5043_MATCH0LEN
                                    802 	.globl _AX5043_LPOSCSTATUS
                                    803 	.globl _AX5043_LPOSCREF1
                                    804 	.globl _AX5043_LPOSCREF0
                                    805 	.globl _AX5043_LPOSCPER1
                                    806 	.globl _AX5043_LPOSCPER0
                                    807 	.globl _AX5043_LPOSCKFILT1
                                    808 	.globl _AX5043_LPOSCKFILT0
                                    809 	.globl _AX5043_LPOSCFREQ1
                                    810 	.globl _AX5043_LPOSCFREQ0
                                    811 	.globl _AX5043_LPOSCCONFIG
                                    812 	.globl _AX5043_IRQREQUEST1
                                    813 	.globl _AX5043_IRQREQUEST0
                                    814 	.globl _AX5043_IRQMASK1
                                    815 	.globl _AX5043_IRQMASK0
                                    816 	.globl _AX5043_IRQINVERSION1
                                    817 	.globl _AX5043_IRQINVERSION0
                                    818 	.globl _AX5043_IFFREQ1
                                    819 	.globl _AX5043_IFFREQ0
                                    820 	.globl _AX5043_GPADCPERIOD
                                    821 	.globl _AX5043_GPADCCTRL
                                    822 	.globl _AX5043_GPADC13VALUE1
                                    823 	.globl _AX5043_GPADC13VALUE0
                                    824 	.globl _AX5043_FSKDMIN1
                                    825 	.globl _AX5043_FSKDMIN0
                                    826 	.globl _AX5043_FSKDMAX1
                                    827 	.globl _AX5043_FSKDMAX0
                                    828 	.globl _AX5043_FSKDEV2
                                    829 	.globl _AX5043_FSKDEV1
                                    830 	.globl _AX5043_FSKDEV0
                                    831 	.globl _AX5043_FREQB3
                                    832 	.globl _AX5043_FREQB2
                                    833 	.globl _AX5043_FREQB1
                                    834 	.globl _AX5043_FREQB0
                                    835 	.globl _AX5043_FREQA3
                                    836 	.globl _AX5043_FREQA2
                                    837 	.globl _AX5043_FREQA1
                                    838 	.globl _AX5043_FREQA0
                                    839 	.globl _AX5043_FRAMING
                                    840 	.globl _AX5043_FIFOTHRESH1
                                    841 	.globl _AX5043_FIFOTHRESH0
                                    842 	.globl _AX5043_FIFOSTAT
                                    843 	.globl _AX5043_FIFOFREE1
                                    844 	.globl _AX5043_FIFOFREE0
                                    845 	.globl _AX5043_FIFODATA
                                    846 	.globl _AX5043_FIFOCOUNT1
                                    847 	.globl _AX5043_FIFOCOUNT0
                                    848 	.globl _AX5043_FECSYNC
                                    849 	.globl _AX5043_FECSTATUS
                                    850 	.globl _AX5043_FEC
                                    851 	.globl _AX5043_ENCODING
                                    852 	.globl _AX5043_DIVERSITY
                                    853 	.globl _AX5043_DECIMATION
                                    854 	.globl _AX5043_DACVALUE1
                                    855 	.globl _AX5043_DACVALUE0
                                    856 	.globl _AX5043_DACCONFIG
                                    857 	.globl _AX5043_CRCINIT3
                                    858 	.globl _AX5043_CRCINIT2
                                    859 	.globl _AX5043_CRCINIT1
                                    860 	.globl _AX5043_CRCINIT0
                                    861 	.globl _AX5043_BGNDRSSITHR
                                    862 	.globl _AX5043_BGNDRSSIGAIN
                                    863 	.globl _AX5043_BGNDRSSI
                                    864 	.globl _AX5043_BBTUNE
                                    865 	.globl _AX5043_BBOFFSCAP
                                    866 	.globl _AX5043_AMPLFILTER
                                    867 	.globl _AX5043_AGCCOUNTER
                                    868 	.globl _AX5043_AFSKSPACE1
                                    869 	.globl _AX5043_AFSKSPACE0
                                    870 	.globl _AX5043_AFSKMARK1
                                    871 	.globl _AX5043_AFSKMARK0
                                    872 	.globl _AX5043_AFSKCTRL
                                    873 	.globl _XWTSTAT
                                    874 	.globl _XWTIRQEN
                                    875 	.globl _XWTEVTD
                                    876 	.globl _XWTEVTD1
                                    877 	.globl _XWTEVTD0
                                    878 	.globl _XWTEVTC
                                    879 	.globl _XWTEVTC1
                                    880 	.globl _XWTEVTC0
                                    881 	.globl _XWTEVTB
                                    882 	.globl _XWTEVTB1
                                    883 	.globl _XWTEVTB0
                                    884 	.globl _XWTEVTA
                                    885 	.globl _XWTEVTA1
                                    886 	.globl _XWTEVTA0
                                    887 	.globl _XWTCNTR1
                                    888 	.globl _XWTCNTB
                                    889 	.globl _XWTCNTB1
                                    890 	.globl _XWTCNTB0
                                    891 	.globl _XWTCNTA
                                    892 	.globl _XWTCNTA1
                                    893 	.globl _XWTCNTA0
                                    894 	.globl _XWTCFGB
                                    895 	.globl _XWTCFGA
                                    896 	.globl _XWDTRESET
                                    897 	.globl _XWDTCFG
                                    898 	.globl _XU1STATUS
                                    899 	.globl _XU1SHREG
                                    900 	.globl _XU1MODE
                                    901 	.globl _XU1CTRL
                                    902 	.globl _XU0STATUS
                                    903 	.globl _XU0SHREG
                                    904 	.globl _XU0MODE
                                    905 	.globl _XU0CTRL
                                    906 	.globl _XT2STATUS
                                    907 	.globl _XT2PERIOD
                                    908 	.globl _XT2PERIOD1
                                    909 	.globl _XT2PERIOD0
                                    910 	.globl _XT2MODE
                                    911 	.globl _XT2CNT
                                    912 	.globl _XT2CNT1
                                    913 	.globl _XT2CNT0
                                    914 	.globl _XT2CLKSRC
                                    915 	.globl _XT1STATUS
                                    916 	.globl _XT1PERIOD
                                    917 	.globl _XT1PERIOD1
                                    918 	.globl _XT1PERIOD0
                                    919 	.globl _XT1MODE
                                    920 	.globl _XT1CNT
                                    921 	.globl _XT1CNT1
                                    922 	.globl _XT1CNT0
                                    923 	.globl _XT1CLKSRC
                                    924 	.globl _XT0STATUS
                                    925 	.globl _XT0PERIOD
                                    926 	.globl _XT0PERIOD1
                                    927 	.globl _XT0PERIOD0
                                    928 	.globl _XT0MODE
                                    929 	.globl _XT0CNT
                                    930 	.globl _XT0CNT1
                                    931 	.globl _XT0CNT0
                                    932 	.globl _XT0CLKSRC
                                    933 	.globl _XSPSTATUS
                                    934 	.globl _XSPSHREG
                                    935 	.globl _XSPMODE
                                    936 	.globl _XSPCLKSRC
                                    937 	.globl _XRADIOSTAT
                                    938 	.globl _XRADIOSTAT1
                                    939 	.globl _XRADIOSTAT0
                                    940 	.globl _XRADIODATA3
                                    941 	.globl _XRADIODATA2
                                    942 	.globl _XRADIODATA1
                                    943 	.globl _XRADIODATA0
                                    944 	.globl _XRADIOADDR1
                                    945 	.globl _XRADIOADDR0
                                    946 	.globl _XRADIOACC
                                    947 	.globl _XOC1STATUS
                                    948 	.globl _XOC1PIN
                                    949 	.globl _XOC1MODE
                                    950 	.globl _XOC1COMP
                                    951 	.globl _XOC1COMP1
                                    952 	.globl _XOC1COMP0
                                    953 	.globl _XOC0STATUS
                                    954 	.globl _XOC0PIN
                                    955 	.globl _XOC0MODE
                                    956 	.globl _XOC0COMP
                                    957 	.globl _XOC0COMP1
                                    958 	.globl _XOC0COMP0
                                    959 	.globl _XNVSTATUS
                                    960 	.globl _XNVKEY
                                    961 	.globl _XNVDATA
                                    962 	.globl _XNVDATA1
                                    963 	.globl _XNVDATA0
                                    964 	.globl _XNVADDR
                                    965 	.globl _XNVADDR1
                                    966 	.globl _XNVADDR0
                                    967 	.globl _XIC1STATUS
                                    968 	.globl _XIC1MODE
                                    969 	.globl _XIC1CAPT
                                    970 	.globl _XIC1CAPT1
                                    971 	.globl _XIC1CAPT0
                                    972 	.globl _XIC0STATUS
                                    973 	.globl _XIC0MODE
                                    974 	.globl _XIC0CAPT
                                    975 	.globl _XIC0CAPT1
                                    976 	.globl _XIC0CAPT0
                                    977 	.globl _XPORTR
                                    978 	.globl _XPORTC
                                    979 	.globl _XPORTB
                                    980 	.globl _XPORTA
                                    981 	.globl _XPINR
                                    982 	.globl _XPINC
                                    983 	.globl _XPINB
                                    984 	.globl _XPINA
                                    985 	.globl _XDIRR
                                    986 	.globl _XDIRC
                                    987 	.globl _XDIRB
                                    988 	.globl _XDIRA
                                    989 	.globl _XDBGLNKSTAT
                                    990 	.globl _XDBGLNKBUF
                                    991 	.globl _XCODECONFIG
                                    992 	.globl _XCLKSTAT
                                    993 	.globl _XCLKCON
                                    994 	.globl _XANALOGCOMP
                                    995 	.globl _XADCCONV
                                    996 	.globl _XADCCLKSRC
                                    997 	.globl _XADCCH3CONFIG
                                    998 	.globl _XADCCH2CONFIG
                                    999 	.globl _XADCCH1CONFIG
                                   1000 	.globl _XADCCH0CONFIG
                                   1001 	.globl _XPCON
                                   1002 	.globl _XIP
                                   1003 	.globl _XIE
                                   1004 	.globl _XDPTR1
                                   1005 	.globl _XDPTR0
                                   1006 	.globl _RNGCLKSRC1
                                   1007 	.globl _RNGCLKSRC0
                                   1008 	.globl _RNGMODE
                                   1009 	.globl _AESOUTADDR
                                   1010 	.globl _AESOUTADDR1
                                   1011 	.globl _AESOUTADDR0
                                   1012 	.globl _AESMODE
                                   1013 	.globl _AESKEYADDR
                                   1014 	.globl _AESKEYADDR1
                                   1015 	.globl _AESKEYADDR0
                                   1016 	.globl _AESINADDR
                                   1017 	.globl _AESINADDR1
                                   1018 	.globl _AESINADDR0
                                   1019 	.globl _AESCURBLOCK
                                   1020 	.globl _AESCONFIG
                                   1021 	.globl _RNGBYTE
                                   1022 	.globl _XTALREADY
                                   1023 	.globl _XTALOSC
                                   1024 	.globl _XTALAMPL
                                   1025 	.globl _SILICONREV
                                   1026 	.globl _SCRATCH3
                                   1027 	.globl _SCRATCH2
                                   1028 	.globl _SCRATCH1
                                   1029 	.globl _SCRATCH0
                                   1030 	.globl _RADIOMUX
                                   1031 	.globl _RADIOFSTATADDR
                                   1032 	.globl _RADIOFSTATADDR1
                                   1033 	.globl _RADIOFSTATADDR0
                                   1034 	.globl _RADIOFDATAADDR
                                   1035 	.globl _RADIOFDATAADDR1
                                   1036 	.globl _RADIOFDATAADDR0
                                   1037 	.globl _OSCRUN
                                   1038 	.globl _OSCREADY
                                   1039 	.globl _OSCFORCERUN
                                   1040 	.globl _OSCCALIB
                                   1041 	.globl _MISCCTRL
                                   1042 	.globl _LPXOSCGM
                                   1043 	.globl _LPOSCREF
                                   1044 	.globl _LPOSCREF1
                                   1045 	.globl _LPOSCREF0
                                   1046 	.globl _LPOSCPER
                                   1047 	.globl _LPOSCPER1
                                   1048 	.globl _LPOSCPER0
                                   1049 	.globl _LPOSCKFILT
                                   1050 	.globl _LPOSCKFILT1
                                   1051 	.globl _LPOSCKFILT0
                                   1052 	.globl _LPOSCFREQ
                                   1053 	.globl _LPOSCFREQ1
                                   1054 	.globl _LPOSCFREQ0
                                   1055 	.globl _LPOSCCONFIG
                                   1056 	.globl _PINSEL
                                   1057 	.globl _PINCHGC
                                   1058 	.globl _PINCHGB
                                   1059 	.globl _PINCHGA
                                   1060 	.globl _PALTRADIO
                                   1061 	.globl _PALTC
                                   1062 	.globl _PALTB
                                   1063 	.globl _PALTA
                                   1064 	.globl _INTCHGC
                                   1065 	.globl _INTCHGB
                                   1066 	.globl _INTCHGA
                                   1067 	.globl _EXTIRQ
                                   1068 	.globl _GPIOENABLE
                                   1069 	.globl _ANALOGA
                                   1070 	.globl _FRCOSCREF
                                   1071 	.globl _FRCOSCREF1
                                   1072 	.globl _FRCOSCREF0
                                   1073 	.globl _FRCOSCPER
                                   1074 	.globl _FRCOSCPER1
                                   1075 	.globl _FRCOSCPER0
                                   1076 	.globl _FRCOSCKFILT
                                   1077 	.globl _FRCOSCKFILT1
                                   1078 	.globl _FRCOSCKFILT0
                                   1079 	.globl _FRCOSCFREQ
                                   1080 	.globl _FRCOSCFREQ1
                                   1081 	.globl _FRCOSCFREQ0
                                   1082 	.globl _FRCOSCCTRL
                                   1083 	.globl _FRCOSCCONFIG
                                   1084 	.globl _DMA1CONFIG
                                   1085 	.globl _DMA1ADDR
                                   1086 	.globl _DMA1ADDR1
                                   1087 	.globl _DMA1ADDR0
                                   1088 	.globl _DMA0CONFIG
                                   1089 	.globl _DMA0ADDR
                                   1090 	.globl _DMA0ADDR1
                                   1091 	.globl _DMA0ADDR0
                                   1092 	.globl _ADCTUNE2
                                   1093 	.globl _ADCTUNE1
                                   1094 	.globl _ADCTUNE0
                                   1095 	.globl _ADCCH3VAL
                                   1096 	.globl _ADCCH3VAL1
                                   1097 	.globl _ADCCH3VAL0
                                   1098 	.globl _ADCCH2VAL
                                   1099 	.globl _ADCCH2VAL1
                                   1100 	.globl _ADCCH2VAL0
                                   1101 	.globl _ADCCH1VAL
                                   1102 	.globl _ADCCH1VAL1
                                   1103 	.globl _ADCCH1VAL0
                                   1104 	.globl _ADCCH0VAL
                                   1105 	.globl _ADCCH0VAL1
                                   1106 	.globl _ADCCH0VAL0
                                   1107 	.globl _UART_Proc_PortInit
                                   1108 	.globl _UART_Proc_VerifyIncomingMsg
                                   1109 	.globl _UART_Proc_ModifiyKissSpecialCharacters
                                   1110 	.globl _UART_Proc_SendMessage
                                   1111 	.globl _UART_Calc_CRC32
                                   1112 	.globl _UART_ParseConfigMessage
                                   1113 	.globl _UART_ParseSetDbgModeMessage
                                   1114 	.globl _UART_ParseTxMessage
                                   1115 	.globl _UART_ParsePwrDwn
                                   1116 	.globl _UART_ParseGpsDataReq
                                   1117 	.globl _UART_ParseAnsMessage
                                   1118 ;--------------------------------------------------------
                                   1119 ; special function registers
                                   1120 ;--------------------------------------------------------
                                   1121 	.area RSEG    (ABS,DATA)
      000000                       1122 	.org 0x0000
                           0000E0  1123 _ACC	=	0x00e0
                           0000F0  1124 _B	=	0x00f0
                           000083  1125 _DPH	=	0x0083
                           000085  1126 _DPH1	=	0x0085
                           000082  1127 _DPL	=	0x0082
                           000084  1128 _DPL1	=	0x0084
                           008382  1129 _DPTR0	=	0x8382
                           008584  1130 _DPTR1	=	0x8584
                           000086  1131 _DPS	=	0x0086
                           0000A0  1132 _E2IE	=	0x00a0
                           0000C0  1133 _E2IP	=	0x00c0
                           000098  1134 _EIE	=	0x0098
                           0000B0  1135 _EIP	=	0x00b0
                           0000A8  1136 _IE	=	0x00a8
                           0000B8  1137 _IP	=	0x00b8
                           000087  1138 _PCON	=	0x0087
                           0000D0  1139 _PSW	=	0x00d0
                           000081  1140 _SP	=	0x0081
                           0000D9  1141 _XPAGE	=	0x00d9
                           0000D9  1142 __XPAGE	=	0x00d9
                           0000CA  1143 _ADCCH0CONFIG	=	0x00ca
                           0000CB  1144 _ADCCH1CONFIG	=	0x00cb
                           0000D2  1145 _ADCCH2CONFIG	=	0x00d2
                           0000D3  1146 _ADCCH3CONFIG	=	0x00d3
                           0000D1  1147 _ADCCLKSRC	=	0x00d1
                           0000C9  1148 _ADCCONV	=	0x00c9
                           0000E1  1149 _ANALOGCOMP	=	0x00e1
                           0000C6  1150 _CLKCON	=	0x00c6
                           0000C7  1151 _CLKSTAT	=	0x00c7
                           000097  1152 _CODECONFIG	=	0x0097
                           0000E3  1153 _DBGLNKBUF	=	0x00e3
                           0000E2  1154 _DBGLNKSTAT	=	0x00e2
                           000089  1155 _DIRA	=	0x0089
                           00008A  1156 _DIRB	=	0x008a
                           00008B  1157 _DIRC	=	0x008b
                           00008E  1158 _DIRR	=	0x008e
                           0000C8  1159 _PINA	=	0x00c8
                           0000E8  1160 _PINB	=	0x00e8
                           0000F8  1161 _PINC	=	0x00f8
                           00008D  1162 _PINR	=	0x008d
                           000080  1163 _PORTA	=	0x0080
                           000088  1164 _PORTB	=	0x0088
                           000090  1165 _PORTC	=	0x0090
                           00008C  1166 _PORTR	=	0x008c
                           0000CE  1167 _IC0CAPT0	=	0x00ce
                           0000CF  1168 _IC0CAPT1	=	0x00cf
                           00CFCE  1169 _IC0CAPT	=	0xcfce
                           0000CC  1170 _IC0MODE	=	0x00cc
                           0000CD  1171 _IC0STATUS	=	0x00cd
                           0000D6  1172 _IC1CAPT0	=	0x00d6
                           0000D7  1173 _IC1CAPT1	=	0x00d7
                           00D7D6  1174 _IC1CAPT	=	0xd7d6
                           0000D4  1175 _IC1MODE	=	0x00d4
                           0000D5  1176 _IC1STATUS	=	0x00d5
                           000092  1177 _NVADDR0	=	0x0092
                           000093  1178 _NVADDR1	=	0x0093
                           009392  1179 _NVADDR	=	0x9392
                           000094  1180 _NVDATA0	=	0x0094
                           000095  1181 _NVDATA1	=	0x0095
                           009594  1182 _NVDATA	=	0x9594
                           000096  1183 _NVKEY	=	0x0096
                           000091  1184 _NVSTATUS	=	0x0091
                           0000BC  1185 _OC0COMP0	=	0x00bc
                           0000BD  1186 _OC0COMP1	=	0x00bd
                           00BDBC  1187 _OC0COMP	=	0xbdbc
                           0000B9  1188 _OC0MODE	=	0x00b9
                           0000BA  1189 _OC0PIN	=	0x00ba
                           0000BB  1190 _OC0STATUS	=	0x00bb
                           0000C4  1191 _OC1COMP0	=	0x00c4
                           0000C5  1192 _OC1COMP1	=	0x00c5
                           00C5C4  1193 _OC1COMP	=	0xc5c4
                           0000C1  1194 _OC1MODE	=	0x00c1
                           0000C2  1195 _OC1PIN	=	0x00c2
                           0000C3  1196 _OC1STATUS	=	0x00c3
                           0000B1  1197 _RADIOACC	=	0x00b1
                           0000B3  1198 _RADIOADDR0	=	0x00b3
                           0000B2  1199 _RADIOADDR1	=	0x00b2
                           00B2B3  1200 _RADIOADDR	=	0xb2b3
                           0000B7  1201 _RADIODATA0	=	0x00b7
                           0000B6  1202 _RADIODATA1	=	0x00b6
                           0000B5  1203 _RADIODATA2	=	0x00b5
                           0000B4  1204 _RADIODATA3	=	0x00b4
                           B4B5B6B7  1205 _RADIODATA	=	0xb4b5b6b7
                           0000BE  1206 _RADIOSTAT0	=	0x00be
                           0000BF  1207 _RADIOSTAT1	=	0x00bf
                           00BFBE  1208 _RADIOSTAT	=	0xbfbe
                           0000DF  1209 _SPCLKSRC	=	0x00df
                           0000DC  1210 _SPMODE	=	0x00dc
                           0000DE  1211 _SPSHREG	=	0x00de
                           0000DD  1212 _SPSTATUS	=	0x00dd
                           00009A  1213 _T0CLKSRC	=	0x009a
                           00009C  1214 _T0CNT0	=	0x009c
                           00009D  1215 _T0CNT1	=	0x009d
                           009D9C  1216 _T0CNT	=	0x9d9c
                           000099  1217 _T0MODE	=	0x0099
                           00009E  1218 _T0PERIOD0	=	0x009e
                           00009F  1219 _T0PERIOD1	=	0x009f
                           009F9E  1220 _T0PERIOD	=	0x9f9e
                           00009B  1221 _T0STATUS	=	0x009b
                           0000A2  1222 _T1CLKSRC	=	0x00a2
                           0000A4  1223 _T1CNT0	=	0x00a4
                           0000A5  1224 _T1CNT1	=	0x00a5
                           00A5A4  1225 _T1CNT	=	0xa5a4
                           0000A1  1226 _T1MODE	=	0x00a1
                           0000A6  1227 _T1PERIOD0	=	0x00a6
                           0000A7  1228 _T1PERIOD1	=	0x00a7
                           00A7A6  1229 _T1PERIOD	=	0xa7a6
                           0000A3  1230 _T1STATUS	=	0x00a3
                           0000AA  1231 _T2CLKSRC	=	0x00aa
                           0000AC  1232 _T2CNT0	=	0x00ac
                           0000AD  1233 _T2CNT1	=	0x00ad
                           00ADAC  1234 _T2CNT	=	0xadac
                           0000A9  1235 _T2MODE	=	0x00a9
                           0000AE  1236 _T2PERIOD0	=	0x00ae
                           0000AF  1237 _T2PERIOD1	=	0x00af
                           00AFAE  1238 _T2PERIOD	=	0xafae
                           0000AB  1239 _T2STATUS	=	0x00ab
                           0000E4  1240 _U0CTRL	=	0x00e4
                           0000E7  1241 _U0MODE	=	0x00e7
                           0000E6  1242 _U0SHREG	=	0x00e6
                           0000E5  1243 _U0STATUS	=	0x00e5
                           0000EC  1244 _U1CTRL	=	0x00ec
                           0000EF  1245 _U1MODE	=	0x00ef
                           0000EE  1246 _U1SHREG	=	0x00ee
                           0000ED  1247 _U1STATUS	=	0x00ed
                           0000DA  1248 _WDTCFG	=	0x00da
                           0000DB  1249 _WDTRESET	=	0x00db
                           0000F1  1250 _WTCFGA	=	0x00f1
                           0000F9  1251 _WTCFGB	=	0x00f9
                           0000F2  1252 _WTCNTA0	=	0x00f2
                           0000F3  1253 _WTCNTA1	=	0x00f3
                           00F3F2  1254 _WTCNTA	=	0xf3f2
                           0000FA  1255 _WTCNTB0	=	0x00fa
                           0000FB  1256 _WTCNTB1	=	0x00fb
                           00FBFA  1257 _WTCNTB	=	0xfbfa
                           0000EB  1258 _WTCNTR1	=	0x00eb
                           0000F4  1259 _WTEVTA0	=	0x00f4
                           0000F5  1260 _WTEVTA1	=	0x00f5
                           00F5F4  1261 _WTEVTA	=	0xf5f4
                           0000F6  1262 _WTEVTB0	=	0x00f6
                           0000F7  1263 _WTEVTB1	=	0x00f7
                           00F7F6  1264 _WTEVTB	=	0xf7f6
                           0000FC  1265 _WTEVTC0	=	0x00fc
                           0000FD  1266 _WTEVTC1	=	0x00fd
                           00FDFC  1267 _WTEVTC	=	0xfdfc
                           0000FE  1268 _WTEVTD0	=	0x00fe
                           0000FF  1269 _WTEVTD1	=	0x00ff
                           00FFFE  1270 _WTEVTD	=	0xfffe
                           0000E9  1271 _WTIRQEN	=	0x00e9
                           0000EA  1272 _WTSTAT	=	0x00ea
                                   1273 ;--------------------------------------------------------
                                   1274 ; special function bits
                                   1275 ;--------------------------------------------------------
                                   1276 	.area RSEG    (ABS,DATA)
      000000                       1277 	.org 0x0000
                           0000E0  1278 _ACC_0	=	0x00e0
                           0000E1  1279 _ACC_1	=	0x00e1
                           0000E2  1280 _ACC_2	=	0x00e2
                           0000E3  1281 _ACC_3	=	0x00e3
                           0000E4  1282 _ACC_4	=	0x00e4
                           0000E5  1283 _ACC_5	=	0x00e5
                           0000E6  1284 _ACC_6	=	0x00e6
                           0000E7  1285 _ACC_7	=	0x00e7
                           0000F0  1286 _B_0	=	0x00f0
                           0000F1  1287 _B_1	=	0x00f1
                           0000F2  1288 _B_2	=	0x00f2
                           0000F3  1289 _B_3	=	0x00f3
                           0000F4  1290 _B_4	=	0x00f4
                           0000F5  1291 _B_5	=	0x00f5
                           0000F6  1292 _B_6	=	0x00f6
                           0000F7  1293 _B_7	=	0x00f7
                           0000A0  1294 _E2IE_0	=	0x00a0
                           0000A1  1295 _E2IE_1	=	0x00a1
                           0000A2  1296 _E2IE_2	=	0x00a2
                           0000A3  1297 _E2IE_3	=	0x00a3
                           0000A4  1298 _E2IE_4	=	0x00a4
                           0000A5  1299 _E2IE_5	=	0x00a5
                           0000A6  1300 _E2IE_6	=	0x00a6
                           0000A7  1301 _E2IE_7	=	0x00a7
                           0000C0  1302 _E2IP_0	=	0x00c0
                           0000C1  1303 _E2IP_1	=	0x00c1
                           0000C2  1304 _E2IP_2	=	0x00c2
                           0000C3  1305 _E2IP_3	=	0x00c3
                           0000C4  1306 _E2IP_4	=	0x00c4
                           0000C5  1307 _E2IP_5	=	0x00c5
                           0000C6  1308 _E2IP_6	=	0x00c6
                           0000C7  1309 _E2IP_7	=	0x00c7
                           000098  1310 _EIE_0	=	0x0098
                           000099  1311 _EIE_1	=	0x0099
                           00009A  1312 _EIE_2	=	0x009a
                           00009B  1313 _EIE_3	=	0x009b
                           00009C  1314 _EIE_4	=	0x009c
                           00009D  1315 _EIE_5	=	0x009d
                           00009E  1316 _EIE_6	=	0x009e
                           00009F  1317 _EIE_7	=	0x009f
                           0000B0  1318 _EIP_0	=	0x00b0
                           0000B1  1319 _EIP_1	=	0x00b1
                           0000B2  1320 _EIP_2	=	0x00b2
                           0000B3  1321 _EIP_3	=	0x00b3
                           0000B4  1322 _EIP_4	=	0x00b4
                           0000B5  1323 _EIP_5	=	0x00b5
                           0000B6  1324 _EIP_6	=	0x00b6
                           0000B7  1325 _EIP_7	=	0x00b7
                           0000A8  1326 _IE_0	=	0x00a8
                           0000A9  1327 _IE_1	=	0x00a9
                           0000AA  1328 _IE_2	=	0x00aa
                           0000AB  1329 _IE_3	=	0x00ab
                           0000AC  1330 _IE_4	=	0x00ac
                           0000AD  1331 _IE_5	=	0x00ad
                           0000AE  1332 _IE_6	=	0x00ae
                           0000AF  1333 _IE_7	=	0x00af
                           0000AF  1334 _EA	=	0x00af
                           0000B8  1335 _IP_0	=	0x00b8
                           0000B9  1336 _IP_1	=	0x00b9
                           0000BA  1337 _IP_2	=	0x00ba
                           0000BB  1338 _IP_3	=	0x00bb
                           0000BC  1339 _IP_4	=	0x00bc
                           0000BD  1340 _IP_5	=	0x00bd
                           0000BE  1341 _IP_6	=	0x00be
                           0000BF  1342 _IP_7	=	0x00bf
                           0000D0  1343 _P	=	0x00d0
                           0000D1  1344 _F1	=	0x00d1
                           0000D2  1345 _OV	=	0x00d2
                           0000D3  1346 _RS0	=	0x00d3
                           0000D4  1347 _RS1	=	0x00d4
                           0000D5  1348 _F0	=	0x00d5
                           0000D6  1349 _AC	=	0x00d6
                           0000D7  1350 _CY	=	0x00d7
                           0000C8  1351 _PINA_0	=	0x00c8
                           0000C9  1352 _PINA_1	=	0x00c9
                           0000CA  1353 _PINA_2	=	0x00ca
                           0000CB  1354 _PINA_3	=	0x00cb
                           0000CC  1355 _PINA_4	=	0x00cc
                           0000CD  1356 _PINA_5	=	0x00cd
                           0000CE  1357 _PINA_6	=	0x00ce
                           0000CF  1358 _PINA_7	=	0x00cf
                           0000E8  1359 _PINB_0	=	0x00e8
                           0000E9  1360 _PINB_1	=	0x00e9
                           0000EA  1361 _PINB_2	=	0x00ea
                           0000EB  1362 _PINB_3	=	0x00eb
                           0000EC  1363 _PINB_4	=	0x00ec
                           0000ED  1364 _PINB_5	=	0x00ed
                           0000EE  1365 _PINB_6	=	0x00ee
                           0000EF  1366 _PINB_7	=	0x00ef
                           0000F8  1367 _PINC_0	=	0x00f8
                           0000F9  1368 _PINC_1	=	0x00f9
                           0000FA  1369 _PINC_2	=	0x00fa
                           0000FB  1370 _PINC_3	=	0x00fb
                           0000FC  1371 _PINC_4	=	0x00fc
                           0000FD  1372 _PINC_5	=	0x00fd
                           0000FE  1373 _PINC_6	=	0x00fe
                           0000FF  1374 _PINC_7	=	0x00ff
                           000080  1375 _PORTA_0	=	0x0080
                           000081  1376 _PORTA_1	=	0x0081
                           000082  1377 _PORTA_2	=	0x0082
                           000083  1378 _PORTA_3	=	0x0083
                           000084  1379 _PORTA_4	=	0x0084
                           000085  1380 _PORTA_5	=	0x0085
                           000086  1381 _PORTA_6	=	0x0086
                           000087  1382 _PORTA_7	=	0x0087
                           000088  1383 _PORTB_0	=	0x0088
                           000089  1384 _PORTB_1	=	0x0089
                           00008A  1385 _PORTB_2	=	0x008a
                           00008B  1386 _PORTB_3	=	0x008b
                           00008C  1387 _PORTB_4	=	0x008c
                           00008D  1388 _PORTB_5	=	0x008d
                           00008E  1389 _PORTB_6	=	0x008e
                           00008F  1390 _PORTB_7	=	0x008f
                           000090  1391 _PORTC_0	=	0x0090
                           000091  1392 _PORTC_1	=	0x0091
                           000092  1393 _PORTC_2	=	0x0092
                           000093  1394 _PORTC_3	=	0x0093
                           000094  1395 _PORTC_4	=	0x0094
                           000095  1396 _PORTC_5	=	0x0095
                           000096  1397 _PORTC_6	=	0x0096
                           000097  1398 _PORTC_7	=	0x0097
                                   1399 ;--------------------------------------------------------
                                   1400 ; overlayable register banks
                                   1401 ;--------------------------------------------------------
                                   1402 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                       1403 	.ds 8
                                   1404 ;--------------------------------------------------------
                                   1405 ; internal ram data
                                   1406 ;--------------------------------------------------------
                                   1407 	.area DSEG    (DATA)
      00003B                       1408 _UART_Proc_VerifyIncomingMsg_sloc0_1_0:
      00003B                       1409 	.ds 4
      00003F                       1410 _UART_Proc_SendMessage_sloc0_1_0:
      00003F                       1411 	.ds 3
      000042                       1412 _UART_Proc_SendMessage_sloc1_1_0:
      000042                       1413 	.ds 1
      000043                       1414 _UART_Proc_SendMessage_sloc2_1_0:
      000043                       1415 	.ds 3
      000046                       1416 _UART_Proc_SendMessage_sloc3_1_0:
      000046                       1417 	.ds 4
                                   1418 ;--------------------------------------------------------
                                   1419 ; overlayable items in internal ram 
                                   1420 ;--------------------------------------------------------
                                   1421 	.area	OSEG    (OVR,DATA)
      00005E                       1422 _UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0:
      00005E                       1423 	.ds 3
      000061                       1424 _UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0:
      000061                       1425 	.ds 3
      000064                       1426 _UART_Proc_ModifiyKissSpecialCharacters_sloc2_1_0:
      000064                       1427 	.ds 3
      000067                       1428 _UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0:
      000067                       1429 	.ds 1
      000068                       1430 _UART_Proc_ModifiyKissSpecialCharacters_sloc4_1_0:
      000068                       1431 	.ds 1
      000069                       1432 _UART_Proc_ModifiyKissSpecialCharacters_sloc5_1_0:
      000069                       1433 	.ds 3
      00006C                       1434 _UART_Proc_ModifiyKissSpecialCharacters_sloc6_1_0:
      00006C                       1435 	.ds 3
                                   1436 	.area	OSEG    (OVR,DATA)
      00005E                       1437 _UART_Calc_CRC32_sloc0_1_0:
      00005E                       1438 	.ds 3
      000061                       1439 _UART_Calc_CRC32_sloc1_1_0:
      000061                       1440 	.ds 2
      000063                       1441 _UART_Calc_CRC32_sloc2_1_0:
      000063                       1442 	.ds 2
      000065                       1443 _UART_Calc_CRC32_sloc3_1_0:
      000065                       1444 	.ds 4
                                   1445 ;--------------------------------------------------------
                                   1446 ; indirectly addressable internal ram data
                                   1447 ;--------------------------------------------------------
                                   1448 	.area ISEG    (DATA)
                                   1449 ;--------------------------------------------------------
                                   1450 ; absolute internal ram data
                                   1451 ;--------------------------------------------------------
                                   1452 	.area IABS    (ABS,DATA)
                                   1453 	.area IABS    (ABS,DATA)
                                   1454 ;--------------------------------------------------------
                                   1455 ; bit data
                                   1456 ;--------------------------------------------------------
                                   1457 	.area BSEG    (BIT)
                                   1458 ;--------------------------------------------------------
                                   1459 ; paged external ram data
                                   1460 ;--------------------------------------------------------
                                   1461 	.area PSEG    (PAG,XDATA)
                                   1462 ;--------------------------------------------------------
                                   1463 ; external ram data
                                   1464 ;--------------------------------------------------------
                                   1465 	.area XSEG    (XDATA)
                           007020  1466 _ADCCH0VAL0	=	0x7020
                           007021  1467 _ADCCH0VAL1	=	0x7021
                           007020  1468 _ADCCH0VAL	=	0x7020
                           007022  1469 _ADCCH1VAL0	=	0x7022
                           007023  1470 _ADCCH1VAL1	=	0x7023
                           007022  1471 _ADCCH1VAL	=	0x7022
                           007024  1472 _ADCCH2VAL0	=	0x7024
                           007025  1473 _ADCCH2VAL1	=	0x7025
                           007024  1474 _ADCCH2VAL	=	0x7024
                           007026  1475 _ADCCH3VAL0	=	0x7026
                           007027  1476 _ADCCH3VAL1	=	0x7027
                           007026  1477 _ADCCH3VAL	=	0x7026
                           007028  1478 _ADCTUNE0	=	0x7028
                           007029  1479 _ADCTUNE1	=	0x7029
                           00702A  1480 _ADCTUNE2	=	0x702a
                           007010  1481 _DMA0ADDR0	=	0x7010
                           007011  1482 _DMA0ADDR1	=	0x7011
                           007010  1483 _DMA0ADDR	=	0x7010
                           007014  1484 _DMA0CONFIG	=	0x7014
                           007012  1485 _DMA1ADDR0	=	0x7012
                           007013  1486 _DMA1ADDR1	=	0x7013
                           007012  1487 _DMA1ADDR	=	0x7012
                           007015  1488 _DMA1CONFIG	=	0x7015
                           007070  1489 _FRCOSCCONFIG	=	0x7070
                           007071  1490 _FRCOSCCTRL	=	0x7071
                           007076  1491 _FRCOSCFREQ0	=	0x7076
                           007077  1492 _FRCOSCFREQ1	=	0x7077
                           007076  1493 _FRCOSCFREQ	=	0x7076
                           007072  1494 _FRCOSCKFILT0	=	0x7072
                           007073  1495 _FRCOSCKFILT1	=	0x7073
                           007072  1496 _FRCOSCKFILT	=	0x7072
                           007078  1497 _FRCOSCPER0	=	0x7078
                           007079  1498 _FRCOSCPER1	=	0x7079
                           007078  1499 _FRCOSCPER	=	0x7078
                           007074  1500 _FRCOSCREF0	=	0x7074
                           007075  1501 _FRCOSCREF1	=	0x7075
                           007074  1502 _FRCOSCREF	=	0x7074
                           007007  1503 _ANALOGA	=	0x7007
                           00700C  1504 _GPIOENABLE	=	0x700c
                           007003  1505 _EXTIRQ	=	0x7003
                           007000  1506 _INTCHGA	=	0x7000
                           007001  1507 _INTCHGB	=	0x7001
                           007002  1508 _INTCHGC	=	0x7002
                           007008  1509 _PALTA	=	0x7008
                           007009  1510 _PALTB	=	0x7009
                           00700A  1511 _PALTC	=	0x700a
                           007046  1512 _PALTRADIO	=	0x7046
                           007004  1513 _PINCHGA	=	0x7004
                           007005  1514 _PINCHGB	=	0x7005
                           007006  1515 _PINCHGC	=	0x7006
                           00700B  1516 _PINSEL	=	0x700b
                           007060  1517 _LPOSCCONFIG	=	0x7060
                           007066  1518 _LPOSCFREQ0	=	0x7066
                           007067  1519 _LPOSCFREQ1	=	0x7067
                           007066  1520 _LPOSCFREQ	=	0x7066
                           007062  1521 _LPOSCKFILT0	=	0x7062
                           007063  1522 _LPOSCKFILT1	=	0x7063
                           007062  1523 _LPOSCKFILT	=	0x7062
                           007068  1524 _LPOSCPER0	=	0x7068
                           007069  1525 _LPOSCPER1	=	0x7069
                           007068  1526 _LPOSCPER	=	0x7068
                           007064  1527 _LPOSCREF0	=	0x7064
                           007065  1528 _LPOSCREF1	=	0x7065
                           007064  1529 _LPOSCREF	=	0x7064
                           007054  1530 _LPXOSCGM	=	0x7054
                           007F01  1531 _MISCCTRL	=	0x7f01
                           007053  1532 _OSCCALIB	=	0x7053
                           007050  1533 _OSCFORCERUN	=	0x7050
                           007052  1534 _OSCREADY	=	0x7052
                           007051  1535 _OSCRUN	=	0x7051
                           007040  1536 _RADIOFDATAADDR0	=	0x7040
                           007041  1537 _RADIOFDATAADDR1	=	0x7041
                           007040  1538 _RADIOFDATAADDR	=	0x7040
                           007042  1539 _RADIOFSTATADDR0	=	0x7042
                           007043  1540 _RADIOFSTATADDR1	=	0x7043
                           007042  1541 _RADIOFSTATADDR	=	0x7042
                           007044  1542 _RADIOMUX	=	0x7044
                           007084  1543 _SCRATCH0	=	0x7084
                           007085  1544 _SCRATCH1	=	0x7085
                           007086  1545 _SCRATCH2	=	0x7086
                           007087  1546 _SCRATCH3	=	0x7087
                           007F00  1547 _SILICONREV	=	0x7f00
                           007F19  1548 _XTALAMPL	=	0x7f19
                           007F18  1549 _XTALOSC	=	0x7f18
                           007F1A  1550 _XTALREADY	=	0x7f1a
                           007081  1551 _RNGBYTE	=	0x7081
                           007091  1552 _AESCONFIG	=	0x7091
                           007098  1553 _AESCURBLOCK	=	0x7098
                           007094  1554 _AESINADDR0	=	0x7094
                           007095  1555 _AESINADDR1	=	0x7095
                           007094  1556 _AESINADDR	=	0x7094
                           007092  1557 _AESKEYADDR0	=	0x7092
                           007093  1558 _AESKEYADDR1	=	0x7093
                           007092  1559 _AESKEYADDR	=	0x7092
                           007090  1560 _AESMODE	=	0x7090
                           007096  1561 _AESOUTADDR0	=	0x7096
                           007097  1562 _AESOUTADDR1	=	0x7097
                           007096  1563 _AESOUTADDR	=	0x7096
                           007080  1564 _RNGMODE	=	0x7080
                           007082  1565 _RNGCLKSRC0	=	0x7082
                           007083  1566 _RNGCLKSRC1	=	0x7083
                           003F82  1567 _XDPTR0	=	0x3f82
                           003F84  1568 _XDPTR1	=	0x3f84
                           003FA8  1569 _XIE	=	0x3fa8
                           003FB8  1570 _XIP	=	0x3fb8
                           003F87  1571 _XPCON	=	0x3f87
                           003FCA  1572 _XADCCH0CONFIG	=	0x3fca
                           003FCB  1573 _XADCCH1CONFIG	=	0x3fcb
                           003FD2  1574 _XADCCH2CONFIG	=	0x3fd2
                           003FD3  1575 _XADCCH3CONFIG	=	0x3fd3
                           003FD1  1576 _XADCCLKSRC	=	0x3fd1
                           003FC9  1577 _XADCCONV	=	0x3fc9
                           003FE1  1578 _XANALOGCOMP	=	0x3fe1
                           003FC6  1579 _XCLKCON	=	0x3fc6
                           003FC7  1580 _XCLKSTAT	=	0x3fc7
                           003F97  1581 _XCODECONFIG	=	0x3f97
                           003FE3  1582 _XDBGLNKBUF	=	0x3fe3
                           003FE2  1583 _XDBGLNKSTAT	=	0x3fe2
                           003F89  1584 _XDIRA	=	0x3f89
                           003F8A  1585 _XDIRB	=	0x3f8a
                           003F8B  1586 _XDIRC	=	0x3f8b
                           003F8E  1587 _XDIRR	=	0x3f8e
                           003FC8  1588 _XPINA	=	0x3fc8
                           003FE8  1589 _XPINB	=	0x3fe8
                           003FF8  1590 _XPINC	=	0x3ff8
                           003F8D  1591 _XPINR	=	0x3f8d
                           003F80  1592 _XPORTA	=	0x3f80
                           003F88  1593 _XPORTB	=	0x3f88
                           003F90  1594 _XPORTC	=	0x3f90
                           003F8C  1595 _XPORTR	=	0x3f8c
                           003FCE  1596 _XIC0CAPT0	=	0x3fce
                           003FCF  1597 _XIC0CAPT1	=	0x3fcf
                           003FCE  1598 _XIC0CAPT	=	0x3fce
                           003FCC  1599 _XIC0MODE	=	0x3fcc
                           003FCD  1600 _XIC0STATUS	=	0x3fcd
                           003FD6  1601 _XIC1CAPT0	=	0x3fd6
                           003FD7  1602 _XIC1CAPT1	=	0x3fd7
                           003FD6  1603 _XIC1CAPT	=	0x3fd6
                           003FD4  1604 _XIC1MODE	=	0x3fd4
                           003FD5  1605 _XIC1STATUS	=	0x3fd5
                           003F92  1606 _XNVADDR0	=	0x3f92
                           003F93  1607 _XNVADDR1	=	0x3f93
                           003F92  1608 _XNVADDR	=	0x3f92
                           003F94  1609 _XNVDATA0	=	0x3f94
                           003F95  1610 _XNVDATA1	=	0x3f95
                           003F94  1611 _XNVDATA	=	0x3f94
                           003F96  1612 _XNVKEY	=	0x3f96
                           003F91  1613 _XNVSTATUS	=	0x3f91
                           003FBC  1614 _XOC0COMP0	=	0x3fbc
                           003FBD  1615 _XOC0COMP1	=	0x3fbd
                           003FBC  1616 _XOC0COMP	=	0x3fbc
                           003FB9  1617 _XOC0MODE	=	0x3fb9
                           003FBA  1618 _XOC0PIN	=	0x3fba
                           003FBB  1619 _XOC0STATUS	=	0x3fbb
                           003FC4  1620 _XOC1COMP0	=	0x3fc4
                           003FC5  1621 _XOC1COMP1	=	0x3fc5
                           003FC4  1622 _XOC1COMP	=	0x3fc4
                           003FC1  1623 _XOC1MODE	=	0x3fc1
                           003FC2  1624 _XOC1PIN	=	0x3fc2
                           003FC3  1625 _XOC1STATUS	=	0x3fc3
                           003FB1  1626 _XRADIOACC	=	0x3fb1
                           003FB3  1627 _XRADIOADDR0	=	0x3fb3
                           003FB2  1628 _XRADIOADDR1	=	0x3fb2
                           003FB7  1629 _XRADIODATA0	=	0x3fb7
                           003FB6  1630 _XRADIODATA1	=	0x3fb6
                           003FB5  1631 _XRADIODATA2	=	0x3fb5
                           003FB4  1632 _XRADIODATA3	=	0x3fb4
                           003FBE  1633 _XRADIOSTAT0	=	0x3fbe
                           003FBF  1634 _XRADIOSTAT1	=	0x3fbf
                           003FBE  1635 _XRADIOSTAT	=	0x3fbe
                           003FDF  1636 _XSPCLKSRC	=	0x3fdf
                           003FDC  1637 _XSPMODE	=	0x3fdc
                           003FDE  1638 _XSPSHREG	=	0x3fde
                           003FDD  1639 _XSPSTATUS	=	0x3fdd
                           003F9A  1640 _XT0CLKSRC	=	0x3f9a
                           003F9C  1641 _XT0CNT0	=	0x3f9c
                           003F9D  1642 _XT0CNT1	=	0x3f9d
                           003F9C  1643 _XT0CNT	=	0x3f9c
                           003F99  1644 _XT0MODE	=	0x3f99
                           003F9E  1645 _XT0PERIOD0	=	0x3f9e
                           003F9F  1646 _XT0PERIOD1	=	0x3f9f
                           003F9E  1647 _XT0PERIOD	=	0x3f9e
                           003F9B  1648 _XT0STATUS	=	0x3f9b
                           003FA2  1649 _XT1CLKSRC	=	0x3fa2
                           003FA4  1650 _XT1CNT0	=	0x3fa4
                           003FA5  1651 _XT1CNT1	=	0x3fa5
                           003FA4  1652 _XT1CNT	=	0x3fa4
                           003FA1  1653 _XT1MODE	=	0x3fa1
                           003FA6  1654 _XT1PERIOD0	=	0x3fa6
                           003FA7  1655 _XT1PERIOD1	=	0x3fa7
                           003FA6  1656 _XT1PERIOD	=	0x3fa6
                           003FA3  1657 _XT1STATUS	=	0x3fa3
                           003FAA  1658 _XT2CLKSRC	=	0x3faa
                           003FAC  1659 _XT2CNT0	=	0x3fac
                           003FAD  1660 _XT2CNT1	=	0x3fad
                           003FAC  1661 _XT2CNT	=	0x3fac
                           003FA9  1662 _XT2MODE	=	0x3fa9
                           003FAE  1663 _XT2PERIOD0	=	0x3fae
                           003FAF  1664 _XT2PERIOD1	=	0x3faf
                           003FAE  1665 _XT2PERIOD	=	0x3fae
                           003FAB  1666 _XT2STATUS	=	0x3fab
                           003FE4  1667 _XU0CTRL	=	0x3fe4
                           003FE7  1668 _XU0MODE	=	0x3fe7
                           003FE6  1669 _XU0SHREG	=	0x3fe6
                           003FE5  1670 _XU0STATUS	=	0x3fe5
                           003FEC  1671 _XU1CTRL	=	0x3fec
                           003FEF  1672 _XU1MODE	=	0x3fef
                           003FEE  1673 _XU1SHREG	=	0x3fee
                           003FED  1674 _XU1STATUS	=	0x3fed
                           003FDA  1675 _XWDTCFG	=	0x3fda
                           003FDB  1676 _XWDTRESET	=	0x3fdb
                           003FF1  1677 _XWTCFGA	=	0x3ff1
                           003FF9  1678 _XWTCFGB	=	0x3ff9
                           003FF2  1679 _XWTCNTA0	=	0x3ff2
                           003FF3  1680 _XWTCNTA1	=	0x3ff3
                           003FF2  1681 _XWTCNTA	=	0x3ff2
                           003FFA  1682 _XWTCNTB0	=	0x3ffa
                           003FFB  1683 _XWTCNTB1	=	0x3ffb
                           003FFA  1684 _XWTCNTB	=	0x3ffa
                           003FEB  1685 _XWTCNTR1	=	0x3feb
                           003FF4  1686 _XWTEVTA0	=	0x3ff4
                           003FF5  1687 _XWTEVTA1	=	0x3ff5
                           003FF4  1688 _XWTEVTA	=	0x3ff4
                           003FF6  1689 _XWTEVTB0	=	0x3ff6
                           003FF7  1690 _XWTEVTB1	=	0x3ff7
                           003FF6  1691 _XWTEVTB	=	0x3ff6
                           003FFC  1692 _XWTEVTC0	=	0x3ffc
                           003FFD  1693 _XWTEVTC1	=	0x3ffd
                           003FFC  1694 _XWTEVTC	=	0x3ffc
                           003FFE  1695 _XWTEVTD0	=	0x3ffe
                           003FFF  1696 _XWTEVTD1	=	0x3fff
                           003FFE  1697 _XWTEVTD	=	0x3ffe
                           003FE9  1698 _XWTIRQEN	=	0x3fe9
                           003FEA  1699 _XWTSTAT	=	0x3fea
                           004114  1700 _AX5043_AFSKCTRL	=	0x4114
                           004113  1701 _AX5043_AFSKMARK0	=	0x4113
                           004112  1702 _AX5043_AFSKMARK1	=	0x4112
                           004111  1703 _AX5043_AFSKSPACE0	=	0x4111
                           004110  1704 _AX5043_AFSKSPACE1	=	0x4110
                           004043  1705 _AX5043_AGCCOUNTER	=	0x4043
                           004115  1706 _AX5043_AMPLFILTER	=	0x4115
                           004189  1707 _AX5043_BBOFFSCAP	=	0x4189
                           004188  1708 _AX5043_BBTUNE	=	0x4188
                           004041  1709 _AX5043_BGNDRSSI	=	0x4041
                           00422E  1710 _AX5043_BGNDRSSIGAIN	=	0x422e
                           00422F  1711 _AX5043_BGNDRSSITHR	=	0x422f
                           004017  1712 _AX5043_CRCINIT0	=	0x4017
                           004016  1713 _AX5043_CRCINIT1	=	0x4016
                           004015  1714 _AX5043_CRCINIT2	=	0x4015
                           004014  1715 _AX5043_CRCINIT3	=	0x4014
                           004332  1716 _AX5043_DACCONFIG	=	0x4332
                           004331  1717 _AX5043_DACVALUE0	=	0x4331
                           004330  1718 _AX5043_DACVALUE1	=	0x4330
                           004102  1719 _AX5043_DECIMATION	=	0x4102
                           004042  1720 _AX5043_DIVERSITY	=	0x4042
                           004011  1721 _AX5043_ENCODING	=	0x4011
                           004018  1722 _AX5043_FEC	=	0x4018
                           00401A  1723 _AX5043_FECSTATUS	=	0x401a
                           004019  1724 _AX5043_FECSYNC	=	0x4019
                           00402B  1725 _AX5043_FIFOCOUNT0	=	0x402b
                           00402A  1726 _AX5043_FIFOCOUNT1	=	0x402a
                           004029  1727 _AX5043_FIFODATA	=	0x4029
                           00402D  1728 _AX5043_FIFOFREE0	=	0x402d
                           00402C  1729 _AX5043_FIFOFREE1	=	0x402c
                           004028  1730 _AX5043_FIFOSTAT	=	0x4028
                           00402F  1731 _AX5043_FIFOTHRESH0	=	0x402f
                           00402E  1732 _AX5043_FIFOTHRESH1	=	0x402e
                           004012  1733 _AX5043_FRAMING	=	0x4012
                           004037  1734 _AX5043_FREQA0	=	0x4037
                           004036  1735 _AX5043_FREQA1	=	0x4036
                           004035  1736 _AX5043_FREQA2	=	0x4035
                           004034  1737 _AX5043_FREQA3	=	0x4034
                           00403F  1738 _AX5043_FREQB0	=	0x403f
                           00403E  1739 _AX5043_FREQB1	=	0x403e
                           00403D  1740 _AX5043_FREQB2	=	0x403d
                           00403C  1741 _AX5043_FREQB3	=	0x403c
                           004163  1742 _AX5043_FSKDEV0	=	0x4163
                           004162  1743 _AX5043_FSKDEV1	=	0x4162
                           004161  1744 _AX5043_FSKDEV2	=	0x4161
                           00410D  1745 _AX5043_FSKDMAX0	=	0x410d
                           00410C  1746 _AX5043_FSKDMAX1	=	0x410c
                           00410F  1747 _AX5043_FSKDMIN0	=	0x410f
                           00410E  1748 _AX5043_FSKDMIN1	=	0x410e
                           004309  1749 _AX5043_GPADC13VALUE0	=	0x4309
                           004308  1750 _AX5043_GPADC13VALUE1	=	0x4308
                           004300  1751 _AX5043_GPADCCTRL	=	0x4300
                           004301  1752 _AX5043_GPADCPERIOD	=	0x4301
                           004101  1753 _AX5043_IFFREQ0	=	0x4101
                           004100  1754 _AX5043_IFFREQ1	=	0x4100
                           00400B  1755 _AX5043_IRQINVERSION0	=	0x400b
                           00400A  1756 _AX5043_IRQINVERSION1	=	0x400a
                           004007  1757 _AX5043_IRQMASK0	=	0x4007
                           004006  1758 _AX5043_IRQMASK1	=	0x4006
                           00400D  1759 _AX5043_IRQREQUEST0	=	0x400d
                           00400C  1760 _AX5043_IRQREQUEST1	=	0x400c
                           004310  1761 _AX5043_LPOSCCONFIG	=	0x4310
                           004317  1762 _AX5043_LPOSCFREQ0	=	0x4317
                           004316  1763 _AX5043_LPOSCFREQ1	=	0x4316
                           004313  1764 _AX5043_LPOSCKFILT0	=	0x4313
                           004312  1765 _AX5043_LPOSCKFILT1	=	0x4312
                           004319  1766 _AX5043_LPOSCPER0	=	0x4319
                           004318  1767 _AX5043_LPOSCPER1	=	0x4318
                           004315  1768 _AX5043_LPOSCREF0	=	0x4315
                           004314  1769 _AX5043_LPOSCREF1	=	0x4314
                           004311  1770 _AX5043_LPOSCSTATUS	=	0x4311
                           004214  1771 _AX5043_MATCH0LEN	=	0x4214
                           004216  1772 _AX5043_MATCH0MAX	=	0x4216
                           004215  1773 _AX5043_MATCH0MIN	=	0x4215
                           004213  1774 _AX5043_MATCH0PAT0	=	0x4213
                           004212  1775 _AX5043_MATCH0PAT1	=	0x4212
                           004211  1776 _AX5043_MATCH0PAT2	=	0x4211
                           004210  1777 _AX5043_MATCH0PAT3	=	0x4210
                           00421C  1778 _AX5043_MATCH1LEN	=	0x421c
                           00421E  1779 _AX5043_MATCH1MAX	=	0x421e
                           00421D  1780 _AX5043_MATCH1MIN	=	0x421d
                           004219  1781 _AX5043_MATCH1PAT0	=	0x4219
                           004218  1782 _AX5043_MATCH1PAT1	=	0x4218
                           004108  1783 _AX5043_MAXDROFFSET0	=	0x4108
                           004107  1784 _AX5043_MAXDROFFSET1	=	0x4107
                           004106  1785 _AX5043_MAXDROFFSET2	=	0x4106
                           00410B  1786 _AX5043_MAXRFOFFSET0	=	0x410b
                           00410A  1787 _AX5043_MAXRFOFFSET1	=	0x410a
                           004109  1788 _AX5043_MAXRFOFFSET2	=	0x4109
                           004164  1789 _AX5043_MODCFGA	=	0x4164
                           004160  1790 _AX5043_MODCFGF	=	0x4160
                           004F5F  1791 _AX5043_MODCFGP	=	0x4f5f
                           004010  1792 _AX5043_MODULATION	=	0x4010
                           004025  1793 _AX5043_PINFUNCANTSEL	=	0x4025
                           004023  1794 _AX5043_PINFUNCDATA	=	0x4023
                           004022  1795 _AX5043_PINFUNCDCLK	=	0x4022
                           004024  1796 _AX5043_PINFUNCIRQ	=	0x4024
                           004026  1797 _AX5043_PINFUNCPWRAMP	=	0x4026
                           004021  1798 _AX5043_PINFUNCSYSCLK	=	0x4021
                           004020  1799 _AX5043_PINSTATE	=	0x4020
                           004233  1800 _AX5043_PKTACCEPTFLAGS	=	0x4233
                           004230  1801 _AX5043_PKTCHUNKSIZE	=	0x4230
                           004231  1802 _AX5043_PKTMISCFLAGS	=	0x4231
                           004232  1803 _AX5043_PKTSTOREFLAGS	=	0x4232
                           004031  1804 _AX5043_PLLCPI	=	0x4031
                           004039  1805 _AX5043_PLLCPIBOOST	=	0x4039
                           004182  1806 _AX5043_PLLLOCKDET	=	0x4182
                           004030  1807 _AX5043_PLLLOOP	=	0x4030
                           004038  1808 _AX5043_PLLLOOPBOOST	=	0x4038
                           004033  1809 _AX5043_PLLRANGINGA	=	0x4033
                           00403B  1810 _AX5043_PLLRANGINGB	=	0x403b
                           004183  1811 _AX5043_PLLRNGCLK	=	0x4183
                           004032  1812 _AX5043_PLLVCODIV	=	0x4032
                           004180  1813 _AX5043_PLLVCOI	=	0x4180
                           004181  1814 _AX5043_PLLVCOIR	=	0x4181
                           004F08  1815 _AX5043_POWCTRL1	=	0x4f08
                           004005  1816 _AX5043_POWIRQMASK	=	0x4005
                           004003  1817 _AX5043_POWSTAT	=	0x4003
                           004004  1818 _AX5043_POWSTICKYSTAT	=	0x4004
                           004027  1819 _AX5043_PWRAMP	=	0x4027
                           004002  1820 _AX5043_PWRMODE	=	0x4002
                           004009  1821 _AX5043_RADIOEVENTMASK0	=	0x4009
                           004008  1822 _AX5043_RADIOEVENTMASK1	=	0x4008
                           00400F  1823 _AX5043_RADIOEVENTREQ0	=	0x400f
                           00400E  1824 _AX5043_RADIOEVENTREQ1	=	0x400e
                           00401C  1825 _AX5043_RADIOSTATE	=	0x401c
                           004F0D  1826 _AX5043_REF	=	0x4f0d
                           004040  1827 _AX5043_RSSI	=	0x4040
                           00422D  1828 _AX5043_RSSIABSTHR	=	0x422d
                           00422C  1829 _AX5043_RSSIREFERENCE	=	0x422c
                           004105  1830 _AX5043_RXDATARATE0	=	0x4105
                           004104  1831 _AX5043_RXDATARATE1	=	0x4104
                           004103  1832 _AX5043_RXDATARATE2	=	0x4103
                           004001  1833 _AX5043_SCRATCH	=	0x4001
                           004000  1834 _AX5043_SILICONREVISION	=	0x4000
                           00405B  1835 _AX5043_TIMER0	=	0x405b
                           00405A  1836 _AX5043_TIMER1	=	0x405a
                           004059  1837 _AX5043_TIMER2	=	0x4059
                           004227  1838 _AX5043_TMGRXAGC	=	0x4227
                           004223  1839 _AX5043_TMGRXBOOST	=	0x4223
                           004226  1840 _AX5043_TMGRXCOARSEAGC	=	0x4226
                           004225  1841 _AX5043_TMGRXOFFSACQ	=	0x4225
                           004229  1842 _AX5043_TMGRXPREAMBLE1	=	0x4229
                           00422A  1843 _AX5043_TMGRXPREAMBLE2	=	0x422a
                           00422B  1844 _AX5043_TMGRXPREAMBLE3	=	0x422b
                           004228  1845 _AX5043_TMGRXRSSI	=	0x4228
                           004224  1846 _AX5043_TMGRXSETTLE	=	0x4224
                           004220  1847 _AX5043_TMGTXBOOST	=	0x4220
                           004221  1848 _AX5043_TMGTXSETTLE	=	0x4221
                           004055  1849 _AX5043_TRKAFSKDEMOD0	=	0x4055
                           004054  1850 _AX5043_TRKAFSKDEMOD1	=	0x4054
                           004049  1851 _AX5043_TRKAMPLITUDE0	=	0x4049
                           004048  1852 _AX5043_TRKAMPLITUDE1	=	0x4048
                           004047  1853 _AX5043_TRKDATARATE0	=	0x4047
                           004046  1854 _AX5043_TRKDATARATE1	=	0x4046
                           004045  1855 _AX5043_TRKDATARATE2	=	0x4045
                           004051  1856 _AX5043_TRKFREQ0	=	0x4051
                           004050  1857 _AX5043_TRKFREQ1	=	0x4050
                           004053  1858 _AX5043_TRKFSKDEMOD0	=	0x4053
                           004052  1859 _AX5043_TRKFSKDEMOD1	=	0x4052
                           00404B  1860 _AX5043_TRKPHASE0	=	0x404b
                           00404A  1861 _AX5043_TRKPHASE1	=	0x404a
                           00404F  1862 _AX5043_TRKRFFREQ0	=	0x404f
                           00404E  1863 _AX5043_TRKRFFREQ1	=	0x404e
                           00404D  1864 _AX5043_TRKRFFREQ2	=	0x404d
                           004169  1865 _AX5043_TXPWRCOEFFA0	=	0x4169
                           004168  1866 _AX5043_TXPWRCOEFFA1	=	0x4168
                           00416B  1867 _AX5043_TXPWRCOEFFB0	=	0x416b
                           00416A  1868 _AX5043_TXPWRCOEFFB1	=	0x416a
                           00416D  1869 _AX5043_TXPWRCOEFFC0	=	0x416d
                           00416C  1870 _AX5043_TXPWRCOEFFC1	=	0x416c
                           00416F  1871 _AX5043_TXPWRCOEFFD0	=	0x416f
                           00416E  1872 _AX5043_TXPWRCOEFFD1	=	0x416e
                           004171  1873 _AX5043_TXPWRCOEFFE0	=	0x4171
                           004170  1874 _AX5043_TXPWRCOEFFE1	=	0x4170
                           004167  1875 _AX5043_TXRATE0	=	0x4167
                           004166  1876 _AX5043_TXRATE1	=	0x4166
                           004165  1877 _AX5043_TXRATE2	=	0x4165
                           00406B  1878 _AX5043_WAKEUP0	=	0x406b
                           00406A  1879 _AX5043_WAKEUP1	=	0x406a
                           00406D  1880 _AX5043_WAKEUPFREQ0	=	0x406d
                           00406C  1881 _AX5043_WAKEUPFREQ1	=	0x406c
                           004069  1882 _AX5043_WAKEUPTIMER0	=	0x4069
                           004068  1883 _AX5043_WAKEUPTIMER1	=	0x4068
                           00406E  1884 _AX5043_WAKEUPXOEARLY	=	0x406e
                           004F11  1885 _AX5043_XTALAMPL	=	0x4f11
                           004184  1886 _AX5043_XTALCAP	=	0x4184
                           004F10  1887 _AX5043_XTALOSC	=	0x4f10
                           00401D  1888 _AX5043_XTALSTATUS	=	0x401d
                           004F00  1889 _AX5043_0xF00	=	0x4f00
                           004F0C  1890 _AX5043_0xF0C	=	0x4f0c
                           004F18  1891 _AX5043_0xF18	=	0x4f18
                           004F1C  1892 _AX5043_0xF1C	=	0x4f1c
                           004F21  1893 _AX5043_0xF21	=	0x4f21
                           004F22  1894 _AX5043_0xF22	=	0x4f22
                           004F23  1895 _AX5043_0xF23	=	0x4f23
                           004F26  1896 _AX5043_0xF26	=	0x4f26
                           004F30  1897 _AX5043_0xF30	=	0x4f30
                           004F31  1898 _AX5043_0xF31	=	0x4f31
                           004F32  1899 _AX5043_0xF32	=	0x4f32
                           004F33  1900 _AX5043_0xF33	=	0x4f33
                           004F34  1901 _AX5043_0xF34	=	0x4f34
                           004F35  1902 _AX5043_0xF35	=	0x4f35
                           004F44  1903 _AX5043_0xF44	=	0x4f44
                           004122  1904 _AX5043_AGCAHYST0	=	0x4122
                           004132  1905 _AX5043_AGCAHYST1	=	0x4132
                           004142  1906 _AX5043_AGCAHYST2	=	0x4142
                           004152  1907 _AX5043_AGCAHYST3	=	0x4152
                           004120  1908 _AX5043_AGCGAIN0	=	0x4120
                           004130  1909 _AX5043_AGCGAIN1	=	0x4130
                           004140  1910 _AX5043_AGCGAIN2	=	0x4140
                           004150  1911 _AX5043_AGCGAIN3	=	0x4150
                           004123  1912 _AX5043_AGCMINMAX0	=	0x4123
                           004133  1913 _AX5043_AGCMINMAX1	=	0x4133
                           004143  1914 _AX5043_AGCMINMAX2	=	0x4143
                           004153  1915 _AX5043_AGCMINMAX3	=	0x4153
                           004121  1916 _AX5043_AGCTARGET0	=	0x4121
                           004131  1917 _AX5043_AGCTARGET1	=	0x4131
                           004141  1918 _AX5043_AGCTARGET2	=	0x4141
                           004151  1919 _AX5043_AGCTARGET3	=	0x4151
                           00412B  1920 _AX5043_AMPLITUDEGAIN0	=	0x412b
                           00413B  1921 _AX5043_AMPLITUDEGAIN1	=	0x413b
                           00414B  1922 _AX5043_AMPLITUDEGAIN2	=	0x414b
                           00415B  1923 _AX5043_AMPLITUDEGAIN3	=	0x415b
                           00412F  1924 _AX5043_BBOFFSRES0	=	0x412f
                           00413F  1925 _AX5043_BBOFFSRES1	=	0x413f
                           00414F  1926 _AX5043_BBOFFSRES2	=	0x414f
                           00415F  1927 _AX5043_BBOFFSRES3	=	0x415f
                           004125  1928 _AX5043_DRGAIN0	=	0x4125
                           004135  1929 _AX5043_DRGAIN1	=	0x4135
                           004145  1930 _AX5043_DRGAIN2	=	0x4145
                           004155  1931 _AX5043_DRGAIN3	=	0x4155
                           00412E  1932 _AX5043_FOURFSK0	=	0x412e
                           00413E  1933 _AX5043_FOURFSK1	=	0x413e
                           00414E  1934 _AX5043_FOURFSK2	=	0x414e
                           00415E  1935 _AX5043_FOURFSK3	=	0x415e
                           00412D  1936 _AX5043_FREQDEV00	=	0x412d
                           00413D  1937 _AX5043_FREQDEV01	=	0x413d
                           00414D  1938 _AX5043_FREQDEV02	=	0x414d
                           00415D  1939 _AX5043_FREQDEV03	=	0x415d
                           00412C  1940 _AX5043_FREQDEV10	=	0x412c
                           00413C  1941 _AX5043_FREQDEV11	=	0x413c
                           00414C  1942 _AX5043_FREQDEV12	=	0x414c
                           00415C  1943 _AX5043_FREQDEV13	=	0x415c
                           004127  1944 _AX5043_FREQUENCYGAINA0	=	0x4127
                           004137  1945 _AX5043_FREQUENCYGAINA1	=	0x4137
                           004147  1946 _AX5043_FREQUENCYGAINA2	=	0x4147
                           004157  1947 _AX5043_FREQUENCYGAINA3	=	0x4157
                           004128  1948 _AX5043_FREQUENCYGAINB0	=	0x4128
                           004138  1949 _AX5043_FREQUENCYGAINB1	=	0x4138
                           004148  1950 _AX5043_FREQUENCYGAINB2	=	0x4148
                           004158  1951 _AX5043_FREQUENCYGAINB3	=	0x4158
                           004129  1952 _AX5043_FREQUENCYGAINC0	=	0x4129
                           004139  1953 _AX5043_FREQUENCYGAINC1	=	0x4139
                           004149  1954 _AX5043_FREQUENCYGAINC2	=	0x4149
                           004159  1955 _AX5043_FREQUENCYGAINC3	=	0x4159
                           00412A  1956 _AX5043_FREQUENCYGAIND0	=	0x412a
                           00413A  1957 _AX5043_FREQUENCYGAIND1	=	0x413a
                           00414A  1958 _AX5043_FREQUENCYGAIND2	=	0x414a
                           00415A  1959 _AX5043_FREQUENCYGAIND3	=	0x415a
                           004116  1960 _AX5043_FREQUENCYLEAK	=	0x4116
                           004126  1961 _AX5043_PHASEGAIN0	=	0x4126
                           004136  1962 _AX5043_PHASEGAIN1	=	0x4136
                           004146  1963 _AX5043_PHASEGAIN2	=	0x4146
                           004156  1964 _AX5043_PHASEGAIN3	=	0x4156
                           004207  1965 _AX5043_PKTADDR0	=	0x4207
                           004206  1966 _AX5043_PKTADDR1	=	0x4206
                           004205  1967 _AX5043_PKTADDR2	=	0x4205
                           004204  1968 _AX5043_PKTADDR3	=	0x4204
                           004200  1969 _AX5043_PKTADDRCFG	=	0x4200
                           00420B  1970 _AX5043_PKTADDRMASK0	=	0x420b
                           00420A  1971 _AX5043_PKTADDRMASK1	=	0x420a
                           004209  1972 _AX5043_PKTADDRMASK2	=	0x4209
                           004208  1973 _AX5043_PKTADDRMASK3	=	0x4208
                           004201  1974 _AX5043_PKTLENCFG	=	0x4201
                           004202  1975 _AX5043_PKTLENOFFSET	=	0x4202
                           004203  1976 _AX5043_PKTMAXLEN	=	0x4203
                           004118  1977 _AX5043_RXPARAMCURSET	=	0x4118
                           004117  1978 _AX5043_RXPARAMSETS	=	0x4117
                           004124  1979 _AX5043_TIMEGAIN0	=	0x4124
                           004134  1980 _AX5043_TIMEGAIN1	=	0x4134
                           004144  1981 _AX5043_TIMEGAIN2	=	0x4144
                           004154  1982 _AX5043_TIMEGAIN3	=	0x4154
                           005114  1983 _AX5043_AFSKCTRLNB	=	0x5114
                           005113  1984 _AX5043_AFSKMARK0NB	=	0x5113
                           005112  1985 _AX5043_AFSKMARK1NB	=	0x5112
                           005111  1986 _AX5043_AFSKSPACE0NB	=	0x5111
                           005110  1987 _AX5043_AFSKSPACE1NB	=	0x5110
                           005043  1988 _AX5043_AGCCOUNTERNB	=	0x5043
                           005115  1989 _AX5043_AMPLFILTERNB	=	0x5115
                           005189  1990 _AX5043_BBOFFSCAPNB	=	0x5189
                           005188  1991 _AX5043_BBTUNENB	=	0x5188
                           005041  1992 _AX5043_BGNDRSSINB	=	0x5041
                           00522E  1993 _AX5043_BGNDRSSIGAINNB	=	0x522e
                           00522F  1994 _AX5043_BGNDRSSITHRNB	=	0x522f
                           005017  1995 _AX5043_CRCINIT0NB	=	0x5017
                           005016  1996 _AX5043_CRCINIT1NB	=	0x5016
                           005015  1997 _AX5043_CRCINIT2NB	=	0x5015
                           005014  1998 _AX5043_CRCINIT3NB	=	0x5014
                           005332  1999 _AX5043_DACCONFIGNB	=	0x5332
                           005331  2000 _AX5043_DACVALUE0NB	=	0x5331
                           005330  2001 _AX5043_DACVALUE1NB	=	0x5330
                           005102  2002 _AX5043_DECIMATIONNB	=	0x5102
                           005042  2003 _AX5043_DIVERSITYNB	=	0x5042
                           005011  2004 _AX5043_ENCODINGNB	=	0x5011
                           005018  2005 _AX5043_FECNB	=	0x5018
                           00501A  2006 _AX5043_FECSTATUSNB	=	0x501a
                           005019  2007 _AX5043_FECSYNCNB	=	0x5019
                           00502B  2008 _AX5043_FIFOCOUNT0NB	=	0x502b
                           00502A  2009 _AX5043_FIFOCOUNT1NB	=	0x502a
                           005029  2010 _AX5043_FIFODATANB	=	0x5029
                           00502D  2011 _AX5043_FIFOFREE0NB	=	0x502d
                           00502C  2012 _AX5043_FIFOFREE1NB	=	0x502c
                           005028  2013 _AX5043_FIFOSTATNB	=	0x5028
                           00502F  2014 _AX5043_FIFOTHRESH0NB	=	0x502f
                           00502E  2015 _AX5043_FIFOTHRESH1NB	=	0x502e
                           005012  2016 _AX5043_FRAMINGNB	=	0x5012
                           005037  2017 _AX5043_FREQA0NB	=	0x5037
                           005036  2018 _AX5043_FREQA1NB	=	0x5036
                           005035  2019 _AX5043_FREQA2NB	=	0x5035
                           005034  2020 _AX5043_FREQA3NB	=	0x5034
                           00503F  2021 _AX5043_FREQB0NB	=	0x503f
                           00503E  2022 _AX5043_FREQB1NB	=	0x503e
                           00503D  2023 _AX5043_FREQB2NB	=	0x503d
                           00503C  2024 _AX5043_FREQB3NB	=	0x503c
                           005163  2025 _AX5043_FSKDEV0NB	=	0x5163
                           005162  2026 _AX5043_FSKDEV1NB	=	0x5162
                           005161  2027 _AX5043_FSKDEV2NB	=	0x5161
                           00510D  2028 _AX5043_FSKDMAX0NB	=	0x510d
                           00510C  2029 _AX5043_FSKDMAX1NB	=	0x510c
                           00510F  2030 _AX5043_FSKDMIN0NB	=	0x510f
                           00510E  2031 _AX5043_FSKDMIN1NB	=	0x510e
                           005309  2032 _AX5043_GPADC13VALUE0NB	=	0x5309
                           005308  2033 _AX5043_GPADC13VALUE1NB	=	0x5308
                           005300  2034 _AX5043_GPADCCTRLNB	=	0x5300
                           005301  2035 _AX5043_GPADCPERIODNB	=	0x5301
                           005101  2036 _AX5043_IFFREQ0NB	=	0x5101
                           005100  2037 _AX5043_IFFREQ1NB	=	0x5100
                           00500B  2038 _AX5043_IRQINVERSION0NB	=	0x500b
                           00500A  2039 _AX5043_IRQINVERSION1NB	=	0x500a
                           005007  2040 _AX5043_IRQMASK0NB	=	0x5007
                           005006  2041 _AX5043_IRQMASK1NB	=	0x5006
                           00500D  2042 _AX5043_IRQREQUEST0NB	=	0x500d
                           00500C  2043 _AX5043_IRQREQUEST1NB	=	0x500c
                           005310  2044 _AX5043_LPOSCCONFIGNB	=	0x5310
                           005317  2045 _AX5043_LPOSCFREQ0NB	=	0x5317
                           005316  2046 _AX5043_LPOSCFREQ1NB	=	0x5316
                           005313  2047 _AX5043_LPOSCKFILT0NB	=	0x5313
                           005312  2048 _AX5043_LPOSCKFILT1NB	=	0x5312
                           005319  2049 _AX5043_LPOSCPER0NB	=	0x5319
                           005318  2050 _AX5043_LPOSCPER1NB	=	0x5318
                           005315  2051 _AX5043_LPOSCREF0NB	=	0x5315
                           005314  2052 _AX5043_LPOSCREF1NB	=	0x5314
                           005311  2053 _AX5043_LPOSCSTATUSNB	=	0x5311
                           005214  2054 _AX5043_MATCH0LENNB	=	0x5214
                           005216  2055 _AX5043_MATCH0MAXNB	=	0x5216
                           005215  2056 _AX5043_MATCH0MINNB	=	0x5215
                           005213  2057 _AX5043_MATCH0PAT0NB	=	0x5213
                           005212  2058 _AX5043_MATCH0PAT1NB	=	0x5212
                           005211  2059 _AX5043_MATCH0PAT2NB	=	0x5211
                           005210  2060 _AX5043_MATCH0PAT3NB	=	0x5210
                           00521C  2061 _AX5043_MATCH1LENNB	=	0x521c
                           00521E  2062 _AX5043_MATCH1MAXNB	=	0x521e
                           00521D  2063 _AX5043_MATCH1MINNB	=	0x521d
                           005219  2064 _AX5043_MATCH1PAT0NB	=	0x5219
                           005218  2065 _AX5043_MATCH1PAT1NB	=	0x5218
                           005108  2066 _AX5043_MAXDROFFSET0NB	=	0x5108
                           005107  2067 _AX5043_MAXDROFFSET1NB	=	0x5107
                           005106  2068 _AX5043_MAXDROFFSET2NB	=	0x5106
                           00510B  2069 _AX5043_MAXRFOFFSET0NB	=	0x510b
                           00510A  2070 _AX5043_MAXRFOFFSET1NB	=	0x510a
                           005109  2071 _AX5043_MAXRFOFFSET2NB	=	0x5109
                           005164  2072 _AX5043_MODCFGANB	=	0x5164
                           005160  2073 _AX5043_MODCFGFNB	=	0x5160
                           005F5F  2074 _AX5043_MODCFGPNB	=	0x5f5f
                           005010  2075 _AX5043_MODULATIONNB	=	0x5010
                           005025  2076 _AX5043_PINFUNCANTSELNB	=	0x5025
                           005023  2077 _AX5043_PINFUNCDATANB	=	0x5023
                           005022  2078 _AX5043_PINFUNCDCLKNB	=	0x5022
                           005024  2079 _AX5043_PINFUNCIRQNB	=	0x5024
                           005026  2080 _AX5043_PINFUNCPWRAMPNB	=	0x5026
                           005021  2081 _AX5043_PINFUNCSYSCLKNB	=	0x5021
                           005020  2082 _AX5043_PINSTATENB	=	0x5020
                           005233  2083 _AX5043_PKTACCEPTFLAGSNB	=	0x5233
                           005230  2084 _AX5043_PKTCHUNKSIZENB	=	0x5230
                           005231  2085 _AX5043_PKTMISCFLAGSNB	=	0x5231
                           005232  2086 _AX5043_PKTSTOREFLAGSNB	=	0x5232
                           005031  2087 _AX5043_PLLCPINB	=	0x5031
                           005039  2088 _AX5043_PLLCPIBOOSTNB	=	0x5039
                           005182  2089 _AX5043_PLLLOCKDETNB	=	0x5182
                           005030  2090 _AX5043_PLLLOOPNB	=	0x5030
                           005038  2091 _AX5043_PLLLOOPBOOSTNB	=	0x5038
                           005033  2092 _AX5043_PLLRANGINGANB	=	0x5033
                           00503B  2093 _AX5043_PLLRANGINGBNB	=	0x503b
                           005183  2094 _AX5043_PLLRNGCLKNB	=	0x5183
                           005032  2095 _AX5043_PLLVCODIVNB	=	0x5032
                           005180  2096 _AX5043_PLLVCOINB	=	0x5180
                           005181  2097 _AX5043_PLLVCOIRNB	=	0x5181
                           005F08  2098 _AX5043_POWCTRL1NB	=	0x5f08
                           005005  2099 _AX5043_POWIRQMASKNB	=	0x5005
                           005003  2100 _AX5043_POWSTATNB	=	0x5003
                           005004  2101 _AX5043_POWSTICKYSTATNB	=	0x5004
                           005027  2102 _AX5043_PWRAMPNB	=	0x5027
                           005002  2103 _AX5043_PWRMODENB	=	0x5002
                           005009  2104 _AX5043_RADIOEVENTMASK0NB	=	0x5009
                           005008  2105 _AX5043_RADIOEVENTMASK1NB	=	0x5008
                           00500F  2106 _AX5043_RADIOEVENTREQ0NB	=	0x500f
                           00500E  2107 _AX5043_RADIOEVENTREQ1NB	=	0x500e
                           00501C  2108 _AX5043_RADIOSTATENB	=	0x501c
                           005F0D  2109 _AX5043_REFNB	=	0x5f0d
                           005040  2110 _AX5043_RSSINB	=	0x5040
                           00522D  2111 _AX5043_RSSIABSTHRNB	=	0x522d
                           00522C  2112 _AX5043_RSSIREFERENCENB	=	0x522c
                           005105  2113 _AX5043_RXDATARATE0NB	=	0x5105
                           005104  2114 _AX5043_RXDATARATE1NB	=	0x5104
                           005103  2115 _AX5043_RXDATARATE2NB	=	0x5103
                           005001  2116 _AX5043_SCRATCHNB	=	0x5001
                           005000  2117 _AX5043_SILICONREVISIONNB	=	0x5000
                           00505B  2118 _AX5043_TIMER0NB	=	0x505b
                           00505A  2119 _AX5043_TIMER1NB	=	0x505a
                           005059  2120 _AX5043_TIMER2NB	=	0x5059
                           005227  2121 _AX5043_TMGRXAGCNB	=	0x5227
                           005223  2122 _AX5043_TMGRXBOOSTNB	=	0x5223
                           005226  2123 _AX5043_TMGRXCOARSEAGCNB	=	0x5226
                           005225  2124 _AX5043_TMGRXOFFSACQNB	=	0x5225
                           005229  2125 _AX5043_TMGRXPREAMBLE1NB	=	0x5229
                           00522A  2126 _AX5043_TMGRXPREAMBLE2NB	=	0x522a
                           00522B  2127 _AX5043_TMGRXPREAMBLE3NB	=	0x522b
                           005228  2128 _AX5043_TMGRXRSSINB	=	0x5228
                           005224  2129 _AX5043_TMGRXSETTLENB	=	0x5224
                           005220  2130 _AX5043_TMGTXBOOSTNB	=	0x5220
                           005221  2131 _AX5043_TMGTXSETTLENB	=	0x5221
                           005055  2132 _AX5043_TRKAFSKDEMOD0NB	=	0x5055
                           005054  2133 _AX5043_TRKAFSKDEMOD1NB	=	0x5054
                           005049  2134 _AX5043_TRKAMPLITUDE0NB	=	0x5049
                           005048  2135 _AX5043_TRKAMPLITUDE1NB	=	0x5048
                           005047  2136 _AX5043_TRKDATARATE0NB	=	0x5047
                           005046  2137 _AX5043_TRKDATARATE1NB	=	0x5046
                           005045  2138 _AX5043_TRKDATARATE2NB	=	0x5045
                           005051  2139 _AX5043_TRKFREQ0NB	=	0x5051
                           005050  2140 _AX5043_TRKFREQ1NB	=	0x5050
                           005053  2141 _AX5043_TRKFSKDEMOD0NB	=	0x5053
                           005052  2142 _AX5043_TRKFSKDEMOD1NB	=	0x5052
                           00504B  2143 _AX5043_TRKPHASE0NB	=	0x504b
                           00504A  2144 _AX5043_TRKPHASE1NB	=	0x504a
                           00504F  2145 _AX5043_TRKRFFREQ0NB	=	0x504f
                           00504E  2146 _AX5043_TRKRFFREQ1NB	=	0x504e
                           00504D  2147 _AX5043_TRKRFFREQ2NB	=	0x504d
                           005169  2148 _AX5043_TXPWRCOEFFA0NB	=	0x5169
                           005168  2149 _AX5043_TXPWRCOEFFA1NB	=	0x5168
                           00516B  2150 _AX5043_TXPWRCOEFFB0NB	=	0x516b
                           00516A  2151 _AX5043_TXPWRCOEFFB1NB	=	0x516a
                           00516D  2152 _AX5043_TXPWRCOEFFC0NB	=	0x516d
                           00516C  2153 _AX5043_TXPWRCOEFFC1NB	=	0x516c
                           00516F  2154 _AX5043_TXPWRCOEFFD0NB	=	0x516f
                           00516E  2155 _AX5043_TXPWRCOEFFD1NB	=	0x516e
                           005171  2156 _AX5043_TXPWRCOEFFE0NB	=	0x5171
                           005170  2157 _AX5043_TXPWRCOEFFE1NB	=	0x5170
                           005167  2158 _AX5043_TXRATE0NB	=	0x5167
                           005166  2159 _AX5043_TXRATE1NB	=	0x5166
                           005165  2160 _AX5043_TXRATE2NB	=	0x5165
                           00506B  2161 _AX5043_WAKEUP0NB	=	0x506b
                           00506A  2162 _AX5043_WAKEUP1NB	=	0x506a
                           00506D  2163 _AX5043_WAKEUPFREQ0NB	=	0x506d
                           00506C  2164 _AX5043_WAKEUPFREQ1NB	=	0x506c
                           005069  2165 _AX5043_WAKEUPTIMER0NB	=	0x5069
                           005068  2166 _AX5043_WAKEUPTIMER1NB	=	0x5068
                           00506E  2167 _AX5043_WAKEUPXOEARLYNB	=	0x506e
                           005F11  2168 _AX5043_XTALAMPLNB	=	0x5f11
                           005184  2169 _AX5043_XTALCAPNB	=	0x5184
                           005F10  2170 _AX5043_XTALOSCNB	=	0x5f10
                           00501D  2171 _AX5043_XTALSTATUSNB	=	0x501d
                           005F00  2172 _AX5043_0xF00NB	=	0x5f00
                           005F0C  2173 _AX5043_0xF0CNB	=	0x5f0c
                           005F18  2174 _AX5043_0xF18NB	=	0x5f18
                           005F1C  2175 _AX5043_0xF1CNB	=	0x5f1c
                           005F21  2176 _AX5043_0xF21NB	=	0x5f21
                           005F22  2177 _AX5043_0xF22NB	=	0x5f22
                           005F23  2178 _AX5043_0xF23NB	=	0x5f23
                           005F26  2179 _AX5043_0xF26NB	=	0x5f26
                           005F30  2180 _AX5043_0xF30NB	=	0x5f30
                           005F31  2181 _AX5043_0xF31NB	=	0x5f31
                           005F32  2182 _AX5043_0xF32NB	=	0x5f32
                           005F33  2183 _AX5043_0xF33NB	=	0x5f33
                           005F34  2184 _AX5043_0xF34NB	=	0x5f34
                           005F35  2185 _AX5043_0xF35NB	=	0x5f35
                           005F44  2186 _AX5043_0xF44NB	=	0x5f44
                           005122  2187 _AX5043_AGCAHYST0NB	=	0x5122
                           005132  2188 _AX5043_AGCAHYST1NB	=	0x5132
                           005142  2189 _AX5043_AGCAHYST2NB	=	0x5142
                           005152  2190 _AX5043_AGCAHYST3NB	=	0x5152
                           005120  2191 _AX5043_AGCGAIN0NB	=	0x5120
                           005130  2192 _AX5043_AGCGAIN1NB	=	0x5130
                           005140  2193 _AX5043_AGCGAIN2NB	=	0x5140
                           005150  2194 _AX5043_AGCGAIN3NB	=	0x5150
                           005123  2195 _AX5043_AGCMINMAX0NB	=	0x5123
                           005133  2196 _AX5043_AGCMINMAX1NB	=	0x5133
                           005143  2197 _AX5043_AGCMINMAX2NB	=	0x5143
                           005153  2198 _AX5043_AGCMINMAX3NB	=	0x5153
                           005121  2199 _AX5043_AGCTARGET0NB	=	0x5121
                           005131  2200 _AX5043_AGCTARGET1NB	=	0x5131
                           005141  2201 _AX5043_AGCTARGET2NB	=	0x5141
                           005151  2202 _AX5043_AGCTARGET3NB	=	0x5151
                           00512B  2203 _AX5043_AMPLITUDEGAIN0NB	=	0x512b
                           00513B  2204 _AX5043_AMPLITUDEGAIN1NB	=	0x513b
                           00514B  2205 _AX5043_AMPLITUDEGAIN2NB	=	0x514b
                           00515B  2206 _AX5043_AMPLITUDEGAIN3NB	=	0x515b
                           00512F  2207 _AX5043_BBOFFSRES0NB	=	0x512f
                           00513F  2208 _AX5043_BBOFFSRES1NB	=	0x513f
                           00514F  2209 _AX5043_BBOFFSRES2NB	=	0x514f
                           00515F  2210 _AX5043_BBOFFSRES3NB	=	0x515f
                           005125  2211 _AX5043_DRGAIN0NB	=	0x5125
                           005135  2212 _AX5043_DRGAIN1NB	=	0x5135
                           005145  2213 _AX5043_DRGAIN2NB	=	0x5145
                           005155  2214 _AX5043_DRGAIN3NB	=	0x5155
                           00512E  2215 _AX5043_FOURFSK0NB	=	0x512e
                           00513E  2216 _AX5043_FOURFSK1NB	=	0x513e
                           00514E  2217 _AX5043_FOURFSK2NB	=	0x514e
                           00515E  2218 _AX5043_FOURFSK3NB	=	0x515e
                           00512D  2219 _AX5043_FREQDEV00NB	=	0x512d
                           00513D  2220 _AX5043_FREQDEV01NB	=	0x513d
                           00514D  2221 _AX5043_FREQDEV02NB	=	0x514d
                           00515D  2222 _AX5043_FREQDEV03NB	=	0x515d
                           00512C  2223 _AX5043_FREQDEV10NB	=	0x512c
                           00513C  2224 _AX5043_FREQDEV11NB	=	0x513c
                           00514C  2225 _AX5043_FREQDEV12NB	=	0x514c
                           00515C  2226 _AX5043_FREQDEV13NB	=	0x515c
                           005127  2227 _AX5043_FREQUENCYGAINA0NB	=	0x5127
                           005137  2228 _AX5043_FREQUENCYGAINA1NB	=	0x5137
                           005147  2229 _AX5043_FREQUENCYGAINA2NB	=	0x5147
                           005157  2230 _AX5043_FREQUENCYGAINA3NB	=	0x5157
                           005128  2231 _AX5043_FREQUENCYGAINB0NB	=	0x5128
                           005138  2232 _AX5043_FREQUENCYGAINB1NB	=	0x5138
                           005148  2233 _AX5043_FREQUENCYGAINB2NB	=	0x5148
                           005158  2234 _AX5043_FREQUENCYGAINB3NB	=	0x5158
                           005129  2235 _AX5043_FREQUENCYGAINC0NB	=	0x5129
                           005139  2236 _AX5043_FREQUENCYGAINC1NB	=	0x5139
                           005149  2237 _AX5043_FREQUENCYGAINC2NB	=	0x5149
                           005159  2238 _AX5043_FREQUENCYGAINC3NB	=	0x5159
                           00512A  2239 _AX5043_FREQUENCYGAIND0NB	=	0x512a
                           00513A  2240 _AX5043_FREQUENCYGAIND1NB	=	0x513a
                           00514A  2241 _AX5043_FREQUENCYGAIND2NB	=	0x514a
                           00515A  2242 _AX5043_FREQUENCYGAIND3NB	=	0x515a
                           005116  2243 _AX5043_FREQUENCYLEAKNB	=	0x5116
                           005126  2244 _AX5043_PHASEGAIN0NB	=	0x5126
                           005136  2245 _AX5043_PHASEGAIN1NB	=	0x5136
                           005146  2246 _AX5043_PHASEGAIN2NB	=	0x5146
                           005156  2247 _AX5043_PHASEGAIN3NB	=	0x5156
                           005207  2248 _AX5043_PKTADDR0NB	=	0x5207
                           005206  2249 _AX5043_PKTADDR1NB	=	0x5206
                           005205  2250 _AX5043_PKTADDR2NB	=	0x5205
                           005204  2251 _AX5043_PKTADDR3NB	=	0x5204
                           005200  2252 _AX5043_PKTADDRCFGNB	=	0x5200
                           00520B  2253 _AX5043_PKTADDRMASK0NB	=	0x520b
                           00520A  2254 _AX5043_PKTADDRMASK1NB	=	0x520a
                           005209  2255 _AX5043_PKTADDRMASK2NB	=	0x5209
                           005208  2256 _AX5043_PKTADDRMASK3NB	=	0x5208
                           005201  2257 _AX5043_PKTLENCFGNB	=	0x5201
                           005202  2258 _AX5043_PKTLENOFFSETNB	=	0x5202
                           005203  2259 _AX5043_PKTMAXLENNB	=	0x5203
                           005118  2260 _AX5043_RXPARAMCURSETNB	=	0x5118
                           005117  2261 _AX5043_RXPARAMSETSNB	=	0x5117
                           005124  2262 _AX5043_TIMEGAIN0NB	=	0x5124
                           005134  2263 _AX5043_TIMEGAIN1NB	=	0x5134
                           005144  2264 _AX5043_TIMEGAIN2NB	=	0x5144
                           005154  2265 _AX5043_TIMEGAIN3NB	=	0x5154
      0003CE                       2266 _UART_Proc_VerifyIncomingMsg_PARM_2:
      0003CE                       2267 	.ds 3
      0003D1                       2268 _UART_Proc_VerifyIncomingMsg_PARM_3:
      0003D1                       2269 	.ds 3
      0003D4                       2270 _UART_Proc_VerifyIncomingMsg_cmd_65536_254:
      0003D4                       2271 	.ds 3
      0003D7                       2272 _UART_Proc_VerifyIncomingMsg_RetVal_65536_255:
      0003D7                       2273 	.ds 1
      0003D8                       2274 _UART_Proc_VerifyIncomingMsg_RxLength_65536_255:
      0003D8                       2275 	.ds 1
      0003D9                       2276 _UART_Proc_VerifyIncomingMsg_CRC32Rx_65536_255:
      0003D9                       2277 	.ds 4
      0003DD                       2278 _UART_Proc_ModifiyKissSpecialCharacters_PARM_2:
      0003DD                       2279 	.ds 3
      0003E0                       2280 _UART_Proc_ModifiyKissSpecialCharacters_UartRxBuffer_65536_266:
      0003E0                       2281 	.ds 3
      0003E3                       2282 _UART_Proc_SendMessage_PARM_2:
      0003E3                       2283 	.ds 1
      0003E4                       2284 _UART_Proc_SendMessage_PARM_3:
      0003E4                       2285 	.ds 1
      0003E5                       2286 _UART_Proc_SendMessage_payload_65536_277:
      0003E5                       2287 	.ds 3
      0003E8                       2288 _UART_Calc_CRC32_PARM_2:
      0003E8                       2289 	.ds 1
      0003E9                       2290 _UART_Calc_CRC32_PARM_3:
      0003E9                       2291 	.ds 1
      0003EA                       2292 _UART_Calc_CRC32_message_65536_281:
      0003EA                       2293 	.ds 3
      0003ED                       2294 _UART_Calc_CRC32_crc_65536_282:
      0003ED                       2295 	.ds 4
      0003F1                       2296 _UART_ParseMessage_PARM_2:
      0003F1                       2297 	.ds 3
      0003F4                       2298 _UART_ParseMessage_PARM_3:
      0003F4                       2299 	.ds 1
      0003F5                       2300 _UART_ParseMessage_cmd_65536_286:
      0003F5                       2301 	.ds 1
      0003F6                       2302 _UART_ParseConfigMessage_PARM_2:
      0003F6                       2303 	.ds 1
      0003F7                       2304 _UART_ParseSetDbgModeMessage_PARM_2:
      0003F7                       2305 	.ds 1
      0003F8                       2306 _UART_ParseTxMessage_PARM_2:
      0003F8                       2307 	.ds 1
      0003F9                       2308 _UART_ParsePwrDwn_PARM_2:
      0003F9                       2309 	.ds 1
      0003FA                       2310 _UART_ParseGpsDataReq_PARM_2:
      0003FA                       2311 	.ds 1
      0003FB                       2312 _UART_ParseAnsMessage_PARM_2:
      0003FB                       2313 	.ds 1
                                   2314 ;--------------------------------------------------------
                                   2315 ; absolute external ram data
                                   2316 ;--------------------------------------------------------
                                   2317 	.area XABS    (ABS,XDATA)
                                   2318 ;--------------------------------------------------------
                                   2319 ; external initialized ram data
                                   2320 ;--------------------------------------------------------
                                   2321 	.area XISEG   (XDATA)
                                   2322 	.area HOME    (CODE)
                                   2323 	.area GSINIT0 (CODE)
                                   2324 	.area GSINIT1 (CODE)
                                   2325 	.area GSINIT2 (CODE)
                                   2326 	.area GSINIT3 (CODE)
                                   2327 	.area GSINIT4 (CODE)
                                   2328 	.area GSINIT5 (CODE)
                                   2329 	.area GSINIT  (CODE)
                                   2330 	.area GSFINAL (CODE)
                                   2331 	.area CSEG    (CODE)
                                   2332 ;--------------------------------------------------------
                                   2333 ; global & static initialisations
                                   2334 ;--------------------------------------------------------
                                   2335 	.area HOME    (CODE)
                                   2336 	.area GSINIT  (CODE)
                                   2337 	.area GSFINAL (CODE)
                                   2338 	.area GSINIT  (CODE)
                                   2339 ;--------------------------------------------------------
                                   2340 ; Home
                                   2341 ;--------------------------------------------------------
                                   2342 	.area HOME    (CODE)
                                   2343 	.area HOME    (CODE)
                                   2344 ;--------------------------------------------------------
                                   2345 ; code
                                   2346 ;--------------------------------------------------------
                                   2347 	.area CSEG    (CODE)
                                   2348 ;------------------------------------------------------------
                                   2349 ;Allocation info for local variables in function 'UART_Proc_PortInit'
                                   2350 ;------------------------------------------------------------
                                   2351 ;	..\src\peripherals\UartComProc.c:24: __reentrantb void UART_Proc_PortInit(void) __reentrant
                                   2352 ;	-----------------------------------------
                                   2353 ;	 function UART_Proc_PortInit
                                   2354 ;	-----------------------------------------
      005EA9                       2355 _UART_Proc_PortInit:
                           000007  2356 	ar7 = 0x07
                           000006  2357 	ar6 = 0x06
                           000005  2358 	ar5 = 0x05
                           000004  2359 	ar4 = 0x04
                           000003  2360 	ar3 = 0x03
                           000002  2361 	ar2 = 0x02
                           000001  2362 	ar1 = 0x01
                           000000  2363 	ar0 = 0x00
                                   2364 ;	..\src\peripherals\UartComProc.c:26: PALTB |= 0x11;
      005EA9 90 70 09         [24] 2365 	mov	dptr,#_PALTB
      005EAC E0               [24] 2366 	movx	a,@dptr
      005EAD 43 E0 11         [24] 2367 	orl	acc,#0x11
      005EB0 F0               [24] 2368 	movx	@dptr,a
                                   2369 ;	..\src\peripherals\UartComProc.c:27: DIRB |= (1<<0) | (1<<4);
      005EB1 43 8A 11         [24] 2370 	orl	_DIRB,#0x11
                                   2371 ;	..\src\peripherals\UartComProc.c:28: DIRB &= (uint8_t)~(1<<5);
      005EB4 53 8A DF         [24] 2372 	anl	_DIRB,#0xdf
                                   2373 ;	..\src\peripherals\UartComProc.c:29: DIRB &= (uint8_t)~(1<<1);
      005EB7 53 8A FD         [24] 2374 	anl	_DIRB,#0xfd
                                   2375 ;	..\src\peripherals\UartComProc.c:30: PINSEL &= (uint8_t)~((1<<0) | (1<<1));
      005EBA 90 70 0B         [24] 2376 	mov	dptr,#_PINSEL
      005EBD E0               [24] 2377 	movx	a,@dptr
      005EBE 53 E0 FC         [24] 2378 	anl	acc,#0xfc
      005EC1 F0               [24] 2379 	movx	@dptr,a
                                   2380 ;	..\src\peripherals\UartComProc.c:31: uart_timer1_baud(CLKSRC_FRCOSC, 9600, 20000000UL);
      005EC2 E4               [12] 2381 	clr	a
      005EC3 C0 E0            [24] 2382 	push	acc
      005EC5 74 2D            [12] 2383 	mov	a,#0x2d
      005EC7 C0 E0            [24] 2384 	push	acc
      005EC9 74 31            [12] 2385 	mov	a,#0x31
      005ECB C0 E0            [24] 2386 	push	acc
      005ECD 74 01            [12] 2387 	mov	a,#0x01
      005ECF C0 E0            [24] 2388 	push	acc
      005ED1 03               [12] 2389 	rr	a
      005ED2 C0 E0            [24] 2390 	push	acc
      005ED4 74 25            [12] 2391 	mov	a,#0x25
      005ED6 C0 E0            [24] 2392 	push	acc
      005ED8 E4               [12] 2393 	clr	a
      005ED9 C0 E0            [24] 2394 	push	acc
      005EDB C0 E0            [24] 2395 	push	acc
      005EDD 75 82 00         [24] 2396 	mov	dpl,#0x00
      005EE0 12 73 23         [24] 2397 	lcall	_uart_timer1_baud
      005EE3 E5 81            [12] 2398 	mov	a,sp
      005EE5 24 F8            [12] 2399 	add	a,#0xf8
      005EE7 F5 81            [12] 2400 	mov	sp,a
                                   2401 ;	..\src\peripherals\UartComProc.c:32: uart0_init(1, 8, 1);
      005EE9 90 05 11         [24] 2402 	mov	dptr,#_uart0_init_PARM_2
      005EEC 74 08            [12] 2403 	mov	a,#0x08
      005EEE F0               [24] 2404 	movx	@dptr,a
      005EEF 90 05 12         [24] 2405 	mov	dptr,#_uart0_init_PARM_3
      005EF2 74 01            [12] 2406 	mov	a,#0x01
      005EF4 F0               [24] 2407 	movx	@dptr,a
      005EF5 75 82 01         [24] 2408 	mov	dpl,#0x01
                                   2409 ;	..\src\peripherals\UartComProc.c:33: }
      005EF8 02 83 F0         [24] 2410 	ljmp	_uart0_init
                                   2411 ;------------------------------------------------------------
                                   2412 ;Allocation info for local variables in function 'UART_Proc_VerifyIncomingMsg'
                                   2413 ;------------------------------------------------------------
                                   2414 ;sloc0                     Allocated with name '_UART_Proc_VerifyIncomingMsg_sloc0_1_0'
                                   2415 ;payload                   Allocated with name '_UART_Proc_VerifyIncomingMsg_PARM_2'
                                   2416 ;PayloadLength             Allocated with name '_UART_Proc_VerifyIncomingMsg_PARM_3'
                                   2417 ;cmd                       Allocated with name '_UART_Proc_VerifyIncomingMsg_cmd_65536_254'
                                   2418 ;RetVal                    Allocated with name '_UART_Proc_VerifyIncomingMsg_RetVal_65536_255'
                                   2419 ;scape                     Allocated with name '_UART_Proc_VerifyIncomingMsg_scape_65536_255'
                                   2420 ;UartRxBuffer              Allocated with name '_UART_Proc_VerifyIncomingMsg_UartRxBuffer_65536_255'
                                   2421 ;RxLength                  Allocated with name '_UART_Proc_VerifyIncomingMsg_RxLength_65536_255'
                                   2422 ;CRC32                     Allocated with name '_UART_Proc_VerifyIncomingMsg_CRC32_65536_255'
                                   2423 ;CRC32Rx                   Allocated with name '_UART_Proc_VerifyIncomingMsg_CRC32Rx_65536_255'
                                   2424 ;------------------------------------------------------------
                                   2425 ;	..\src\peripherals\UartComProc.c:47: uint8_t  UART_Proc_VerifyIncomingMsg(uint8_t *cmd, uint8_t *payload,uint8_t *PayloadLength)
                                   2426 ;	-----------------------------------------
                                   2427 ;	 function UART_Proc_VerifyIncomingMsg
                                   2428 ;	-----------------------------------------
      005EFB                       2429 _UART_Proc_VerifyIncomingMsg:
      005EFB AF F0            [24] 2430 	mov	r7,b
      005EFD AE 83            [24] 2431 	mov	r6,dph
      005EFF E5 82            [12] 2432 	mov	a,dpl
      005F01 90 03 D4         [24] 2433 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_cmd_65536_254
      005F04 F0               [24] 2434 	movx	@dptr,a
      005F05 EE               [12] 2435 	mov	a,r6
      005F06 A3               [24] 2436 	inc	dptr
      005F07 F0               [24] 2437 	movx	@dptr,a
      005F08 EF               [12] 2438 	mov	a,r7
      005F09 A3               [24] 2439 	inc	dptr
      005F0A F0               [24] 2440 	movx	@dptr,a
                                   2441 ;	..\src\peripherals\UartComProc.c:49: uint8_t RetVal = ERR_NO_DATA;
      005F0B 90 03 D7         [24] 2442 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RetVal_65536_255
      005F0E E4               [12] 2443 	clr	a
      005F0F F0               [24] 2444 	movx	@dptr,a
                                   2445 ;	..\src\peripherals\UartComProc.c:55: UartRxBuffer  =(uint8_t*) malloc(sizeof(uint8_t)*40);
      005F10 90 00 28         [24] 2446 	mov	dptr,#0x0028
      005F13 12 7E 60         [24] 2447 	lcall	_malloc
      005F16 AE 82            [24] 2448 	mov	r6,dpl
      005F18 AF 83            [24] 2449 	mov	r7,dph
                                   2450 ;	..\src\peripherals\UartComProc.c:57: if(uart0_rxcount())
      005F1A 12 A0 C7         [24] 2451 	lcall	_uart0_rxcount
      005F1D E5 82            [12] 2452 	mov	a,dpl
      005F1F 70 03            [24] 2453 	jnz	00158$
      005F21 02 61 3D         [24] 2454 	ljmp	00119$
      005F24                       2455 00158$:
                                   2456 ;	..\src\peripherals\UartComProc.c:59: *UartRxBuffer = uart0_rx();
      005F24 12 7C AF         [24] 2457 	lcall	_uart0_rx
      005F27 AD 82            [24] 2458 	mov	r5,dpl
      005F29 8E 82            [24] 2459 	mov	dpl,r6
      005F2B 8F 83            [24] 2460 	mov	dph,r7
      005F2D ED               [12] 2461 	mov	a,r5
      005F2E F0               [24] 2462 	movx	@dptr,a
                                   2463 ;	..\src\peripherals\UartComProc.c:60: RetVal = ERR_BAD_FORMAT; // Indico que se recibio algo, pero de volver con este valor, no se recibio una trama KISS
      005F2F 90 03 D7         [24] 2464 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RetVal_65536_255
      005F32 74 02            [12] 2465 	mov	a,#0x02
      005F34 F0               [24] 2466 	movx	@dptr,a
                                   2467 ;	..\src\peripherals\UartComProc.c:61: delay_ms(500);
      005F35 90 01 F4         [24] 2468 	mov	dptr,#0x01f4
      005F38 C0 07            [24] 2469 	push	ar7
      005F3A C0 06            [24] 2470 	push	ar6
      005F3C 12 09 31         [24] 2471 	lcall	_delay_ms
      005F3F D0 06            [24] 2472 	pop	ar6
      005F41 D0 07            [24] 2473 	pop	ar7
                                   2474 ;	..\src\peripherals\UartComProc.c:62: RxLength = 1;
      005F43 90 03 D8         [24] 2475 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RxLength_65536_255
      005F46 74 01            [12] 2476 	mov	a,#0x01
      005F48 F0               [24] 2477 	movx	@dptr,a
                                   2478 ;	..\src\peripherals\UartComProc.c:63: if(*UartRxBuffer == 0xC0)
      005F49 8E 82            [24] 2479 	mov	dpl,r6
      005F4B 8F 83            [24] 2480 	mov	dph,r7
      005F4D E0               [24] 2481 	movx	a,@dptr
      005F4E FD               [12] 2482 	mov	r5,a
      005F4F BD C0 02         [24] 2483 	cjne	r5,#0xc0,00159$
      005F52 80 03            [24] 2484 	sjmp	00160$
      005F54                       2485 00159$:
      005F54 02 61 32         [24] 2486 	ljmp	00117$
      005F57                       2487 00160$:
                                   2488 ;	..\src\peripherals\UartComProc.c:65: do
      005F57 7D 00            [12] 2489 	mov	r5,#0x00
      005F59                       2490 00104$:
                                   2491 ;	..\src\peripherals\UartComProc.c:67: if(uart0_rxcount())
      005F59 12 A0 C7         [24] 2492 	lcall	_uart0_rxcount
      005F5C E5 82            [12] 2493 	mov	a,dpl
      005F5E 60 1D            [24] 2494 	jz	00102$
                                   2495 ;	..\src\peripherals\UartComProc.c:69: *(UartRxBuffer+RxLength)=uart0_rx();
      005F60 90 03 D8         [24] 2496 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RxLength_65536_255
      005F63 E0               [24] 2497 	movx	a,@dptr
      005F64 2E               [12] 2498 	add	a,r6
      005F65 FC               [12] 2499 	mov	r4,a
      005F66 E4               [12] 2500 	clr	a
      005F67 3F               [12] 2501 	addc	a,r7
      005F68 FB               [12] 2502 	mov	r3,a
      005F69 12 7C AF         [24] 2503 	lcall	_uart0_rx
      005F6C AA 82            [24] 2504 	mov	r2,dpl
      005F6E 8C 82            [24] 2505 	mov	dpl,r4
      005F70 8B 83            [24] 2506 	mov	dph,r3
      005F72 EA               [12] 2507 	mov	a,r2
      005F73 F0               [24] 2508 	movx	@dptr,a
                                   2509 ;	..\src\peripherals\UartComProc.c:70: RxLength=(RxLength)+1;
      005F74 90 03 D8         [24] 2510 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RxLength_65536_255
      005F77 E0               [24] 2511 	movx	a,@dptr
      005F78 FC               [12] 2512 	mov	r4,a
      005F79 04               [12] 2513 	inc	a
      005F7A F0               [24] 2514 	movx	@dptr,a
      005F7B 80 13            [24] 2515 	sjmp	00105$
      005F7D                       2516 00102$:
                                   2517 ;	..\src\peripherals\UartComProc.c:75: scape ++;
      005F7D 0D               [12] 2518 	inc	r5
                                   2519 ;	..\src\peripherals\UartComProc.c:76: delay_ms(1);
      005F7E 90 00 01         [24] 2520 	mov	dptr,#0x0001
      005F81 C0 07            [24] 2521 	push	ar7
      005F83 C0 06            [24] 2522 	push	ar6
      005F85 C0 05            [24] 2523 	push	ar5
      005F87 12 09 31         [24] 2524 	lcall	_delay_ms
      005F8A D0 05            [24] 2525 	pop	ar5
      005F8C D0 06            [24] 2526 	pop	ar6
      005F8E D0 07            [24] 2527 	pop	ar7
      005F90                       2528 00105$:
                                   2529 ;	..\src\peripherals\UartComProc.c:78: }while(*(UartRxBuffer+(RxLength-1)) != 0xC0);
      005F90 90 03 D8         [24] 2530 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RxLength_65536_255
      005F93 E0               [24] 2531 	movx	a,@dptr
      005F94 FC               [12] 2532 	mov	r4,a
      005F95 7B 00            [12] 2533 	mov	r3,#0x00
      005F97 1C               [12] 2534 	dec	r4
      005F98 BC FF 01         [24] 2535 	cjne	r4,#0xff,00162$
      005F9B 1B               [12] 2536 	dec	r3
      005F9C                       2537 00162$:
      005F9C EC               [12] 2538 	mov	a,r4
      005F9D 2E               [12] 2539 	add	a,r6
      005F9E F5 82            [12] 2540 	mov	dpl,a
      005FA0 EB               [12] 2541 	mov	a,r3
      005FA1 3F               [12] 2542 	addc	a,r7
      005FA2 F5 83            [12] 2543 	mov	dph,a
      005FA4 E0               [24] 2544 	movx	a,@dptr
      005FA5 FC               [12] 2545 	mov	r4,a
      005FA6 BC C0 B0         [24] 2546 	cjne	r4,#0xc0,00104$
                                   2547 ;	..\src\peripherals\UartComProc.c:79: if(scape == 250)
      005FA9 BD FA 09         [24] 2548 	cjne	r5,#0xfa,00114$
                                   2549 ;	..\src\peripherals\UartComProc.c:81: RetVal = ERR_BAD_ENDING;// indico que no se recibio el paquete correctamente
      005FAC 90 03 D7         [24] 2550 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RetVal_65536_255
      005FAF 74 03            [12] 2551 	mov	a,#0x03
      005FB1 F0               [24] 2552 	movx	@dptr,a
      005FB2 02 61 32         [24] 2553 	ljmp	00117$
      005FB5                       2554 00114$:
                                   2555 ;	..\src\peripherals\UartComProc.c:85: UART_Proc_ModifiyKissSpecialCharacters(UartRxBuffer,&RxLength);
      005FB5 8E 04            [24] 2556 	mov	ar4,r6
      005FB7 8F 05            [24] 2557 	mov	ar5,r7
      005FB9 7B 00            [12] 2558 	mov	r3,#0x00
      005FBB 90 03 DD         [24] 2559 	mov	dptr,#_UART_Proc_ModifiyKissSpecialCharacters_PARM_2
      005FBE 74 D8            [12] 2560 	mov	a,#_UART_Proc_VerifyIncomingMsg_RxLength_65536_255
      005FC0 F0               [24] 2561 	movx	@dptr,a
      005FC1 74 03            [12] 2562 	mov	a,#(_UART_Proc_VerifyIncomingMsg_RxLength_65536_255 >> 8)
      005FC3 A3               [24] 2563 	inc	dptr
      005FC4 F0               [24] 2564 	movx	@dptr,a
      005FC5 E4               [12] 2565 	clr	a
      005FC6 A3               [24] 2566 	inc	dptr
      005FC7 F0               [24] 2567 	movx	@dptr,a
      005FC8 8C 82            [24] 2568 	mov	dpl,r4
      005FCA 8D 83            [24] 2569 	mov	dph,r5
      005FCC 8B F0            [24] 2570 	mov	b,r3
      005FCE C0 07            [24] 2571 	push	ar7
      005FD0 C0 06            [24] 2572 	push	ar6
      005FD2 12 61 44         [24] 2573 	lcall	_UART_Proc_ModifiyKissSpecialCharacters
      005FD5 D0 06            [24] 2574 	pop	ar6
      005FD7 D0 07            [24] 2575 	pop	ar7
                                   2576 ;	..\src\peripherals\UartComProc.c:87: if(*UartRxBuffer == 0xC0 && *(UartRxBuffer+RxLength-1) == 0xC0)
      005FD9 8E 82            [24] 2577 	mov	dpl,r6
      005FDB 8F 83            [24] 2578 	mov	dph,r7
      005FDD E0               [24] 2579 	movx	a,@dptr
      005FDE FD               [12] 2580 	mov	r5,a
      005FDF BD C0 02         [24] 2581 	cjne	r5,#0xc0,00167$
      005FE2 80 03            [24] 2582 	sjmp	00168$
      005FE4                       2583 00167$:
      005FE4 02 61 32         [24] 2584 	ljmp	00117$
      005FE7                       2585 00168$:
      005FE7 90 03 D8         [24] 2586 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RxLength_65536_255
      005FEA E0               [24] 2587 	movx	a,@dptr
      005FEB FD               [12] 2588 	mov	r5,a
      005FEC 2E               [12] 2589 	add	a,r6
      005FED FB               [12] 2590 	mov	r3,a
      005FEE E4               [12] 2591 	clr	a
      005FEF 3F               [12] 2592 	addc	a,r7
      005FF0 FC               [12] 2593 	mov	r4,a
      005FF1 EB               [12] 2594 	mov	a,r3
      005FF2 24 FF            [12] 2595 	add	a,#0xff
      005FF4 F5 82            [12] 2596 	mov	dpl,a
      005FF6 EC               [12] 2597 	mov	a,r4
      005FF7 34 FF            [12] 2598 	addc	a,#0xff
      005FF9 F5 83            [12] 2599 	mov	dph,a
      005FFB E0               [24] 2600 	movx	a,@dptr
      005FFC FC               [12] 2601 	mov	r4,a
      005FFD BC C0 02         [24] 2602 	cjne	r4,#0xc0,00169$
      006000 80 03            [24] 2603 	sjmp	00170$
      006002                       2604 00169$:
      006002 02 61 32         [24] 2605 	ljmp	00117$
      006005                       2606 00170$:
                                   2607 ;	..\src\peripherals\UartComProc.c:89: CRC32 = UART_Calc_CRC32(UartRxBuffer+1,RxLength-START_CHARACTER_LENGTH-END_CHARACTER_LENGTH-CRC_LENGTH,1);// lo pongo en little endian, ya que el memcpy lo pasa asi
      006005 74 01            [12] 2608 	mov	a,#0x01
      006007 2E               [12] 2609 	add	a,r6
      006008 FB               [12] 2610 	mov	r3,a
      006009 E4               [12] 2611 	clr	a
      00600A 3F               [12] 2612 	addc	a,r7
      00600B FC               [12] 2613 	mov	r4,a
      00600C 8B 00            [24] 2614 	mov	ar0,r3
      00600E 8C 01            [24] 2615 	mov	ar1,r4
      006010 7A 00            [12] 2616 	mov	r2,#0x00
      006012 ED               [12] 2617 	mov	a,r5
      006013 24 FA            [12] 2618 	add	a,#0xfa
      006015 90 03 E8         [24] 2619 	mov	dptr,#_UART_Calc_CRC32_PARM_2
      006018 F0               [24] 2620 	movx	@dptr,a
      006019 90 03 E9         [24] 2621 	mov	dptr,#_UART_Calc_CRC32_PARM_3
      00601C 74 01            [12] 2622 	mov	a,#0x01
      00601E F0               [24] 2623 	movx	@dptr,a
      00601F 88 82            [24] 2624 	mov	dpl,r0
      006021 89 83            [24] 2625 	mov	dph,r1
      006023 8A F0            [24] 2626 	mov	b,r2
      006025 C0 07            [24] 2627 	push	ar7
      006027 C0 06            [24] 2628 	push	ar6
      006029 C0 04            [24] 2629 	push	ar4
      00602B C0 03            [24] 2630 	push	ar3
      00602D 12 63 BF         [24] 2631 	lcall	_UART_Calc_CRC32
      006030 85 82 3B         [24] 2632 	mov	_UART_Proc_VerifyIncomingMsg_sloc0_1_0,dpl
      006033 85 83 3C         [24] 2633 	mov	(_UART_Proc_VerifyIncomingMsg_sloc0_1_0 + 1),dph
      006036 85 F0 3D         [24] 2634 	mov	(_UART_Proc_VerifyIncomingMsg_sloc0_1_0 + 2),b
      006039 F5 3E            [12] 2635 	mov	(_UART_Proc_VerifyIncomingMsg_sloc0_1_0 + 3),a
      00603B D0 03            [24] 2636 	pop	ar3
      00603D D0 04            [24] 2637 	pop	ar4
      00603F D0 06            [24] 2638 	pop	ar6
      006041 D0 07            [24] 2639 	pop	ar7
                                   2640 ;	..\src\peripherals\UartComProc.c:90: memcpy(&CRC32Rx,UartRxBuffer+RxLength-START_CHARACTER_LENGTH-CRC_LENGTH,CRC_LENGTH);
      006043 90 03 D8         [24] 2641 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RxLength_65536_255
      006046 E0               [24] 2642 	movx	a,@dptr
      006047 2E               [12] 2643 	add	a,r6
      006048 FD               [12] 2644 	mov	r5,a
      006049 E4               [12] 2645 	clr	a
      00604A 3F               [12] 2646 	addc	a,r7
      00604B FA               [12] 2647 	mov	r2,a
      00604C ED               [12] 2648 	mov	a,r5
      00604D 24 FB            [12] 2649 	add	a,#0xfb
      00604F FD               [12] 2650 	mov	r5,a
      006050 EA               [12] 2651 	mov	a,r2
      006051 34 FF            [12] 2652 	addc	a,#0xff
      006053 FA               [12] 2653 	mov	r2,a
      006054 90 04 6D         [24] 2654 	mov	dptr,#_memcpy_PARM_2
      006057 ED               [12] 2655 	mov	a,r5
      006058 F0               [24] 2656 	movx	@dptr,a
      006059 EA               [12] 2657 	mov	a,r2
      00605A A3               [24] 2658 	inc	dptr
      00605B F0               [24] 2659 	movx	@dptr,a
      00605C E4               [12] 2660 	clr	a
      00605D A3               [24] 2661 	inc	dptr
      00605E F0               [24] 2662 	movx	@dptr,a
      00605F 90 04 70         [24] 2663 	mov	dptr,#_memcpy_PARM_3
      006062 74 04            [12] 2664 	mov	a,#0x04
      006064 F0               [24] 2665 	movx	@dptr,a
      006065 E4               [12] 2666 	clr	a
      006066 A3               [24] 2667 	inc	dptr
      006067 F0               [24] 2668 	movx	@dptr,a
      006068 90 03 D9         [24] 2669 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_CRC32Rx_65536_255
      00606B 75 F0 00         [24] 2670 	mov	b,#0x00
      00606E C0 07            [24] 2671 	push	ar7
      006070 C0 06            [24] 2672 	push	ar6
      006072 C0 04            [24] 2673 	push	ar4
      006074 C0 03            [24] 2674 	push	ar3
      006076 12 78 14         [24] 2675 	lcall	_memcpy
      006079 D0 03            [24] 2676 	pop	ar3
      00607B D0 04            [24] 2677 	pop	ar4
      00607D D0 06            [24] 2678 	pop	ar6
      00607F D0 07            [24] 2679 	pop	ar7
                                   2680 ;	..\src\peripherals\UartComProc.c:91: if(CRC32Rx == CRC32)
      006081 90 03 D9         [24] 2681 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_CRC32Rx_65536_255
      006084 E0               [24] 2682 	movx	a,@dptr
      006085 F8               [12] 2683 	mov	r0,a
      006086 A3               [24] 2684 	inc	dptr
      006087 E0               [24] 2685 	movx	a,@dptr
      006088 F9               [12] 2686 	mov	r1,a
      006089 A3               [24] 2687 	inc	dptr
      00608A E0               [24] 2688 	movx	a,@dptr
      00608B FA               [12] 2689 	mov	r2,a
      00608C A3               [24] 2690 	inc	dptr
      00608D E0               [24] 2691 	movx	a,@dptr
      00608E FD               [12] 2692 	mov	r5,a
      00608F E8               [12] 2693 	mov	a,r0
      006090 B5 3B 0E         [24] 2694 	cjne	a,_UART_Proc_VerifyIncomingMsg_sloc0_1_0,00171$
      006093 E9               [12] 2695 	mov	a,r1
      006094 B5 3C 0A         [24] 2696 	cjne	a,(_UART_Proc_VerifyIncomingMsg_sloc0_1_0 + 1),00171$
      006097 EA               [12] 2697 	mov	a,r2
      006098 B5 3D 06         [24] 2698 	cjne	a,(_UART_Proc_VerifyIncomingMsg_sloc0_1_0 + 2),00171$
      00609B ED               [12] 2699 	mov	a,r5
      00609C B5 3E 02         [24] 2700 	cjne	a,(_UART_Proc_VerifyIncomingMsg_sloc0_1_0 + 3),00171$
      00609F 80 02            [24] 2701 	sjmp	00172$
      0060A1                       2702 00171$:
      0060A1 80 6E            [24] 2703 	sjmp	00108$
      0060A3                       2704 00172$:
                                   2705 ;	..\src\peripherals\UartComProc.c:94: RetVal = RX_SUCCESFULL;
      0060A3 90 03 D7         [24] 2706 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RetVal_65536_255
      0060A6 74 DD            [12] 2707 	mov	a,#0xdd
      0060A8 F0               [24] 2708 	movx	@dptr,a
                                   2709 ;	..\src\peripherals\UartComProc.c:95: *cmd = *(UartRxBuffer+1);
      0060A9 90 03 D4         [24] 2710 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_cmd_65536_254
      0060AC E0               [24] 2711 	movx	a,@dptr
      0060AD F9               [12] 2712 	mov	r1,a
      0060AE A3               [24] 2713 	inc	dptr
      0060AF E0               [24] 2714 	movx	a,@dptr
      0060B0 FA               [12] 2715 	mov	r2,a
      0060B1 A3               [24] 2716 	inc	dptr
      0060B2 E0               [24] 2717 	movx	a,@dptr
      0060B3 FD               [12] 2718 	mov	r5,a
      0060B4 8B 82            [24] 2719 	mov	dpl,r3
      0060B6 8C 83            [24] 2720 	mov	dph,r4
      0060B8 E0               [24] 2721 	movx	a,@dptr
      0060B9 89 82            [24] 2722 	mov	dpl,r1
      0060BB 8A 83            [24] 2723 	mov	dph,r2
      0060BD 8D F0            [24] 2724 	mov	b,r5
      0060BF 12 7C 62         [24] 2725 	lcall	__gptrput
                                   2726 ;	..\src\peripherals\UartComProc.c:96: memcpy(payload,UartRxBuffer+START_CHARACTER_LENGTH+COMMAND_LENGTH,RxLength-START_CHARACTER_LENGTH-CRC_LENGTH-END_CHARACTER_LENGTH); // porq esta linea me adelanta !!
      0060C2 90 03 CE         [24] 2727 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_PARM_2
      0060C5 E0               [24] 2728 	movx	a,@dptr
      0060C6 FB               [12] 2729 	mov	r3,a
      0060C7 A3               [24] 2730 	inc	dptr
      0060C8 E0               [24] 2731 	movx	a,@dptr
      0060C9 FC               [12] 2732 	mov	r4,a
      0060CA A3               [24] 2733 	inc	dptr
      0060CB E0               [24] 2734 	movx	a,@dptr
      0060CC FD               [12] 2735 	mov	r5,a
      0060CD 74 02            [12] 2736 	mov	a,#0x02
      0060CF 2E               [12] 2737 	add	a,r6
      0060D0 F9               [12] 2738 	mov	r1,a
      0060D1 E4               [12] 2739 	clr	a
      0060D2 3F               [12] 2740 	addc	a,r7
      0060D3 F8               [12] 2741 	mov	r0,a
      0060D4 7A 00            [12] 2742 	mov	r2,#0x00
      0060D6 C0 06            [24] 2743 	push	ar6
      0060D8 C0 07            [24] 2744 	push	ar7
      0060DA 90 03 D8         [24] 2745 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RxLength_65536_255
      0060DD E0               [24] 2746 	movx	a,@dptr
      0060DE 7E 00            [12] 2747 	mov	r6,#0x00
      0060E0 24 FA            [12] 2748 	add	a,#0xfa
      0060E2 FF               [12] 2749 	mov	r7,a
      0060E3 EE               [12] 2750 	mov	a,r6
      0060E4 34 FF            [12] 2751 	addc	a,#0xff
      0060E6 FE               [12] 2752 	mov	r6,a
      0060E7 90 04 6D         [24] 2753 	mov	dptr,#_memcpy_PARM_2
      0060EA E9               [12] 2754 	mov	a,r1
      0060EB F0               [24] 2755 	movx	@dptr,a
      0060EC E8               [12] 2756 	mov	a,r0
      0060ED A3               [24] 2757 	inc	dptr
      0060EE F0               [24] 2758 	movx	@dptr,a
      0060EF EA               [12] 2759 	mov	a,r2
      0060F0 A3               [24] 2760 	inc	dptr
      0060F1 F0               [24] 2761 	movx	@dptr,a
      0060F2 90 04 70         [24] 2762 	mov	dptr,#_memcpy_PARM_3
      0060F5 EF               [12] 2763 	mov	a,r7
      0060F6 F0               [24] 2764 	movx	@dptr,a
      0060F7 EE               [12] 2765 	mov	a,r6
      0060F8 A3               [24] 2766 	inc	dptr
      0060F9 F0               [24] 2767 	movx	@dptr,a
      0060FA 8B 82            [24] 2768 	mov	dpl,r3
      0060FC 8C 83            [24] 2769 	mov	dph,r4
      0060FE 8D F0            [24] 2770 	mov	b,r5
      006100 C0 07            [24] 2771 	push	ar7
      006102 C0 06            [24] 2772 	push	ar6
      006104 12 78 14         [24] 2773 	lcall	_memcpy
      006107 D0 06            [24] 2774 	pop	ar6
      006109 D0 07            [24] 2775 	pop	ar7
      00610B D0 07            [24] 2776 	pop	ar7
      00610D D0 06            [24] 2777 	pop	ar6
      00610F 80 06            [24] 2778 	sjmp	00109$
      006111                       2779 00108$:
                                   2780 ;	..\src\peripherals\UartComProc.c:100: RetVal = ERR_BAD_CRC;
      006111 90 03 D7         [24] 2781 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RetVal_65536_255
      006114 74 01            [12] 2782 	mov	a,#0x01
      006116 F0               [24] 2783 	movx	@dptr,a
      006117                       2784 00109$:
                                   2785 ;	..\src\peripherals\UartComProc.c:102: *PayloadLength = RxLength -START_CHARACTER_LENGTH-COMMAND_LENGTH-CRC_LENGTH-END_CHARACTER_LENGTH;
      006117 90 03 D1         [24] 2786 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_PARM_3
      00611A E0               [24] 2787 	movx	a,@dptr
      00611B FB               [12] 2788 	mov	r3,a
      00611C A3               [24] 2789 	inc	dptr
      00611D E0               [24] 2790 	movx	a,@dptr
      00611E FC               [12] 2791 	mov	r4,a
      00611F A3               [24] 2792 	inc	dptr
      006120 E0               [24] 2793 	movx	a,@dptr
      006121 FD               [12] 2794 	mov	r5,a
      006122 90 03 D8         [24] 2795 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RxLength_65536_255
      006125 E0               [24] 2796 	movx	a,@dptr
      006126 24 F9            [12] 2797 	add	a,#0xf9
      006128 FA               [12] 2798 	mov	r2,a
      006129 8B 82            [24] 2799 	mov	dpl,r3
      00612B 8C 83            [24] 2800 	mov	dph,r4
      00612D 8D F0            [24] 2801 	mov	b,r5
      00612F 12 7C 62         [24] 2802 	lcall	__gptrput
      006132                       2803 00117$:
                                   2804 ;	..\src\peripherals\UartComProc.c:106: free(UartRxBuffer);
      006132 7D 00            [12] 2805 	mov	r5,#0x00
      006134 8E 82            [24] 2806 	mov	dpl,r6
      006136 8F 83            [24] 2807 	mov	dph,r7
      006138 8D F0            [24] 2808 	mov	b,r5
      00613A 12 6E 35         [24] 2809 	lcall	_free
      00613D                       2810 00119$:
                                   2811 ;	..\src\peripherals\UartComProc.c:108: return RetVal;
      00613D 90 03 D7         [24] 2812 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RetVal_65536_255
      006140 E0               [24] 2813 	movx	a,@dptr
                                   2814 ;	..\src\peripherals\UartComProc.c:109: }
      006141 F5 82            [12] 2815 	mov	dpl,a
      006143 22               [24] 2816 	ret
                                   2817 ;------------------------------------------------------------
                                   2818 ;Allocation info for local variables in function 'UART_Proc_ModifiyKissSpecialCharacters'
                                   2819 ;------------------------------------------------------------
                                   2820 ;RxLength                  Allocated with name '_UART_Proc_ModifiyKissSpecialCharacters_PARM_2'
                                   2821 ;UartRxBuffer              Allocated with name '_UART_Proc_ModifiyKissSpecialCharacters_UartRxBuffer_65536_266'
                                   2822 ;i                         Allocated with name '_UART_Proc_ModifiyKissSpecialCharacters_i_65536_267'
                                   2823 ;j                         Allocated with name '_UART_Proc_ModifiyKissSpecialCharacters_j_65536_267'
                                   2824 ;sloc0                     Allocated with name '_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0'
                                   2825 ;sloc1                     Allocated with name '_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0'
                                   2826 ;sloc2                     Allocated with name '_UART_Proc_ModifiyKissSpecialCharacters_sloc2_1_0'
                                   2827 ;sloc3                     Allocated with name '_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0'
                                   2828 ;sloc4                     Allocated with name '_UART_Proc_ModifiyKissSpecialCharacters_sloc4_1_0'
                                   2829 ;sloc5                     Allocated with name '_UART_Proc_ModifiyKissSpecialCharacters_sloc5_1_0'
                                   2830 ;sloc6                     Allocated with name '_UART_Proc_ModifiyKissSpecialCharacters_sloc6_1_0'
                                   2831 ;------------------------------------------------------------
                                   2832 ;	..\src\peripherals\UartComProc.c:121: void UART_Proc_ModifiyKissSpecialCharacters(uint8_t *UartRxBuffer, uint8_t *RxLength)
                                   2833 ;	-----------------------------------------
                                   2834 ;	 function UART_Proc_ModifiyKissSpecialCharacters
                                   2835 ;	-----------------------------------------
      006144                       2836 _UART_Proc_ModifiyKissSpecialCharacters:
      006144 AF F0            [24] 2837 	mov	r7,b
      006146 AE 83            [24] 2838 	mov	r6,dph
      006148 E5 82            [12] 2839 	mov	a,dpl
      00614A 90 03 E0         [24] 2840 	mov	dptr,#_UART_Proc_ModifiyKissSpecialCharacters_UartRxBuffer_65536_266
      00614D F0               [24] 2841 	movx	@dptr,a
      00614E EE               [12] 2842 	mov	a,r6
      00614F A3               [24] 2843 	inc	dptr
      006150 F0               [24] 2844 	movx	@dptr,a
      006151 EF               [12] 2845 	mov	a,r7
      006152 A3               [24] 2846 	inc	dptr
      006153 F0               [24] 2847 	movx	@dptr,a
                                   2848 ;	..\src\peripherals\UartComProc.c:125: for(i=0;i<*RxLength;i++)
      006154 90 03 E0         [24] 2849 	mov	dptr,#_UART_Proc_ModifiyKissSpecialCharacters_UartRxBuffer_65536_266
      006157 E0               [24] 2850 	movx	a,@dptr
      006158 F5 69            [12] 2851 	mov	_UART_Proc_ModifiyKissSpecialCharacters_sloc5_1_0,a
      00615A A3               [24] 2852 	inc	dptr
      00615B E0               [24] 2853 	movx	a,@dptr
      00615C F5 6A            [12] 2854 	mov	(_UART_Proc_ModifiyKissSpecialCharacters_sloc5_1_0 + 1),a
      00615E A3               [24] 2855 	inc	dptr
      00615F E0               [24] 2856 	movx	a,@dptr
      006160 F5 6B            [12] 2857 	mov	(_UART_Proc_ModifiyKissSpecialCharacters_sloc5_1_0 + 2),a
      006162 85 69 6C         [24] 2858 	mov	_UART_Proc_ModifiyKissSpecialCharacters_sloc6_1_0,_UART_Proc_ModifiyKissSpecialCharacters_sloc5_1_0
      006165 85 6A 6D         [24] 2859 	mov	(_UART_Proc_ModifiyKissSpecialCharacters_sloc6_1_0 + 1),(_UART_Proc_ModifiyKissSpecialCharacters_sloc5_1_0 + 1)
      006168 85 6B 6E         [24] 2860 	mov	(_UART_Proc_ModifiyKissSpecialCharacters_sloc6_1_0 + 2),(_UART_Proc_ModifiyKissSpecialCharacters_sloc5_1_0 + 2)
      00616B 85 69 5E         [24] 2861 	mov	_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0,_UART_Proc_ModifiyKissSpecialCharacters_sloc5_1_0
      00616E 85 6A 5F         [24] 2862 	mov	(_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0 + 1),(_UART_Proc_ModifiyKissSpecialCharacters_sloc5_1_0 + 1)
      006171 85 6B 60         [24] 2863 	mov	(_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0 + 2),(_UART_Proc_ModifiyKissSpecialCharacters_sloc5_1_0 + 2)
      006174 79 00            [12] 2864 	mov	r1,#0x00
      006176                       2865 00118$:
      006176 90 03 DD         [24] 2866 	mov	dptr,#_UART_Proc_ModifiyKissSpecialCharacters_PARM_2
      006179 E0               [24] 2867 	movx	a,@dptr
      00617A F5 61            [12] 2868 	mov	_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0,a
      00617C A3               [24] 2869 	inc	dptr
      00617D E0               [24] 2870 	movx	a,@dptr
      00617E F5 62            [12] 2871 	mov	(_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0 + 1),a
      006180 A3               [24] 2872 	inc	dptr
      006181 E0               [24] 2873 	movx	a,@dptr
      006182 F5 63            [12] 2874 	mov	(_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0 + 2),a
      006184 85 61 82         [24] 2875 	mov	dpl,_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0
      006187 85 62 83         [24] 2876 	mov	dph,(_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0 + 1)
      00618A 85 63 F0         [24] 2877 	mov	b,(_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0 + 2)
      00618D 12 8D 8E         [24] 2878 	lcall	__gptrget
      006190 F8               [12] 2879 	mov	r0,a
      006191 C3               [12] 2880 	clr	c
      006192 E9               [12] 2881 	mov	a,r1
      006193 98               [12] 2882 	subb	a,r0
      006194 40 01            [24] 2883 	jc	00161$
      006196 22               [24] 2884 	ret
      006197                       2885 00161$:
                                   2886 ;	..\src\peripherals\UartComProc.c:127: if (*(UartRxBuffer+i) == FRAME_SCAPE)
      006197 E9               [12] 2887 	mov	a,r1
      006198 25 5E            [12] 2888 	add	a,_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0
      00619A F5 64            [12] 2889 	mov	_UART_Proc_ModifiyKissSpecialCharacters_sloc2_1_0,a
      00619C E4               [12] 2890 	clr	a
      00619D 35 5F            [12] 2891 	addc	a,(_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0 + 1)
      00619F F5 65            [12] 2892 	mov	(_UART_Proc_ModifiyKissSpecialCharacters_sloc2_1_0 + 1),a
      0061A1 85 60 66         [24] 2893 	mov	(_UART_Proc_ModifiyKissSpecialCharacters_sloc2_1_0 + 2),(_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0 + 2)
      0061A4 85 64 82         [24] 2894 	mov	dpl,_UART_Proc_ModifiyKissSpecialCharacters_sloc2_1_0
      0061A7 85 65 83         [24] 2895 	mov	dph,(_UART_Proc_ModifiyKissSpecialCharacters_sloc2_1_0 + 1)
      0061AA 85 66 F0         [24] 2896 	mov	b,(_UART_Proc_ModifiyKissSpecialCharacters_sloc2_1_0 + 2)
      0061AD 12 8D 8E         [24] 2897 	lcall	__gptrget
      0061B0 F8               [12] 2898 	mov	r0,a
      0061B1 B8 DB 02         [24] 2899 	cjne	r0,#0xdb,00162$
      0061B4 80 03            [24] 2900 	sjmp	00163$
      0061B6                       2901 00162$:
      0061B6 02 62 97         [24] 2902 	ljmp	00119$
      0061B9                       2903 00163$:
                                   2904 ;	..\src\peripherals\UartComProc.c:129: if(*(UartRxBuffer+i+1)== TRANSPOSE_FRAME_END)
      0061B9 74 01            [12] 2905 	mov	a,#0x01
      0061BB 25 64            [12] 2906 	add	a,_UART_Proc_ModifiyKissSpecialCharacters_sloc2_1_0
      0061BD F8               [12] 2907 	mov	r0,a
      0061BE E4               [12] 2908 	clr	a
      0061BF 35 65            [12] 2909 	addc	a,(_UART_Proc_ModifiyKissSpecialCharacters_sloc2_1_0 + 1)
      0061C1 FB               [12] 2910 	mov	r3,a
      0061C2 AC 66            [24] 2911 	mov	r4,(_UART_Proc_ModifiyKissSpecialCharacters_sloc2_1_0 + 2)
      0061C4 88 82            [24] 2912 	mov	dpl,r0
      0061C6 8B 83            [24] 2913 	mov	dph,r3
      0061C8 8C F0            [24] 2914 	mov	b,r4
      0061CA 12 8D 8E         [24] 2915 	lcall	__gptrget
      0061CD F5 67            [12] 2916 	mov	_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0,a
      0061CF 74 DC            [12] 2917 	mov	a,#0xdc
      0061D1 B5 67 60         [24] 2918 	cjne	a,_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0,00106$
                                   2919 ;	..\src\peripherals\UartComProc.c:131: *(UartRxBuffer+i) = FRAME_END;
      0061D4 85 64 82         [24] 2920 	mov	dpl,_UART_Proc_ModifiyKissSpecialCharacters_sloc2_1_0
      0061D7 85 65 83         [24] 2921 	mov	dph,(_UART_Proc_ModifiyKissSpecialCharacters_sloc2_1_0 + 1)
      0061DA 85 66 F0         [24] 2922 	mov	b,(_UART_Proc_ModifiyKissSpecialCharacters_sloc2_1_0 + 2)
      0061DD 74 C0            [12] 2923 	mov	a,#0xc0
      0061DF 12 7C 62         [24] 2924 	lcall	__gptrput
                                   2925 ;	..\src\peripherals\UartComProc.c:132: for(j=i+1;j<*RxLength;j++)
      0061E2 89 00            [24] 2926 	mov	ar0,r1
      0061E4 08               [12] 2927 	inc	r0
      0061E5                       2928 00112$:
      0061E5 85 61 82         [24] 2929 	mov	dpl,_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0
      0061E8 85 62 83         [24] 2930 	mov	dph,(_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0 + 1)
      0061EB 85 63 F0         [24] 2931 	mov	b,(_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0 + 2)
      0061EE 12 8D 8E         [24] 2932 	lcall	__gptrget
      0061F1 F5 68            [12] 2933 	mov	_UART_Proc_ModifiyKissSpecialCharacters_sloc4_1_0,a
      0061F3 C3               [12] 2934 	clr	c
      0061F4 E8               [12] 2935 	mov	a,r0
      0061F5 95 68            [12] 2936 	subb	a,_UART_Proc_ModifiyKissSpecialCharacters_sloc4_1_0
      0061F7 50 29            [24] 2937 	jnc	00101$
                                   2938 ;	..\src\peripherals\UartComProc.c:134: *(UartRxBuffer+j) = *(UartRxBuffer+j+1);
      0061F9 E8               [12] 2939 	mov	a,r0
      0061FA 25 69            [12] 2940 	add	a,_UART_Proc_ModifiyKissSpecialCharacters_sloc5_1_0
      0061FC FA               [12] 2941 	mov	r2,a
      0061FD E4               [12] 2942 	clr	a
      0061FE 35 6A            [12] 2943 	addc	a,(_UART_Proc_ModifiyKissSpecialCharacters_sloc5_1_0 + 1)
      006200 FB               [12] 2944 	mov	r3,a
      006201 AC 6B            [24] 2945 	mov	r4,(_UART_Proc_ModifiyKissSpecialCharacters_sloc5_1_0 + 2)
      006203 74 01            [12] 2946 	mov	a,#0x01
      006205 2A               [12] 2947 	add	a,r2
      006206 FD               [12] 2948 	mov	r5,a
      006207 E4               [12] 2949 	clr	a
      006208 3B               [12] 2950 	addc	a,r3
      006209 FE               [12] 2951 	mov	r6,a
      00620A 8C 07            [24] 2952 	mov	ar7,r4
      00620C 8D 82            [24] 2953 	mov	dpl,r5
      00620E 8E 83            [24] 2954 	mov	dph,r6
      006210 8F F0            [24] 2955 	mov	b,r7
      006212 12 8D 8E         [24] 2956 	lcall	__gptrget
      006215 FD               [12] 2957 	mov	r5,a
      006216 8A 82            [24] 2958 	mov	dpl,r2
      006218 8B 83            [24] 2959 	mov	dph,r3
      00621A 8C F0            [24] 2960 	mov	b,r4
      00621C 12 7C 62         [24] 2961 	lcall	__gptrput
                                   2962 ;	..\src\peripherals\UartComProc.c:132: for(j=i+1;j<*RxLength;j++)
      00621F 08               [12] 2963 	inc	r0
      006220 80 C3            [24] 2964 	sjmp	00112$
      006222                       2965 00101$:
                                   2966 ;	..\src\peripherals\UartComProc.c:136: *RxLength= *RxLength -1; //suprimi un caracter por lo cual el mensaje se disminuye su tama�o en 1
      006222 AF 68            [24] 2967 	mov	r7,_UART_Proc_ModifiyKissSpecialCharacters_sloc4_1_0
      006224 1F               [12] 2968 	dec	r7
      006225 85 61 82         [24] 2969 	mov	dpl,_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0
      006228 85 62 83         [24] 2970 	mov	dph,(_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0 + 1)
      00622B 85 63 F0         [24] 2971 	mov	b,(_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0 + 2)
      00622E EF               [12] 2972 	mov	a,r7
      00622F 12 7C 62         [24] 2973 	lcall	__gptrput
      006232 80 63            [24] 2974 	sjmp	00119$
      006234                       2975 00106$:
                                   2976 ;	..\src\peripherals\UartComProc.c:138: else if(*(UartRxBuffer+i+1) == TRANSPOSE_FRAME_ESCAPE)
      006234 74 DD            [12] 2977 	mov	a,#0xdd
      006236 B5 67 5E         [24] 2978 	cjne	a,_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0,00119$
                                   2979 ;	..\src\peripherals\UartComProc.c:140: *(UartRxBuffer+i) = FRAME_SCAPE;
      006239 85 64 82         [24] 2980 	mov	dpl,_UART_Proc_ModifiyKissSpecialCharacters_sloc2_1_0
      00623C 85 65 83         [24] 2981 	mov	dph,(_UART_Proc_ModifiyKissSpecialCharacters_sloc2_1_0 + 1)
      00623F 85 66 F0         [24] 2982 	mov	b,(_UART_Proc_ModifiyKissSpecialCharacters_sloc2_1_0 + 2)
      006242 74 DB            [12] 2983 	mov	a,#0xdb
      006244 12 7C 62         [24] 2984 	lcall	__gptrput
                                   2985 ;	..\src\peripherals\UartComProc.c:141: for(j=i+1;j<*RxLength;j++)
      006247 89 07            [24] 2986 	mov	ar7,r1
      006249 0F               [12] 2987 	inc	r7
      00624A                       2988 00115$:
      00624A 85 61 82         [24] 2989 	mov	dpl,_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0
      00624D 85 62 83         [24] 2990 	mov	dph,(_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0 + 1)
      006250 85 63 F0         [24] 2991 	mov	b,(_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0 + 2)
      006253 12 8D 8E         [24] 2992 	lcall	__gptrget
      006256 FE               [12] 2993 	mov	r6,a
      006257 C3               [12] 2994 	clr	c
      006258 EF               [12] 2995 	mov	a,r7
      006259 9E               [12] 2996 	subb	a,r6
      00625A 50 2D            [24] 2997 	jnc	00102$
                                   2998 ;	..\src\peripherals\UartComProc.c:143: *(UartRxBuffer+j) = *(UartRxBuffer+j+1);
      00625C C0 01            [24] 2999 	push	ar1
      00625E EF               [12] 3000 	mov	a,r7
      00625F 25 6C            [12] 3001 	add	a,_UART_Proc_ModifiyKissSpecialCharacters_sloc6_1_0
      006261 F8               [12] 3002 	mov	r0,a
      006262 E4               [12] 3003 	clr	a
      006263 35 6D            [12] 3004 	addc	a,(_UART_Proc_ModifiyKissSpecialCharacters_sloc6_1_0 + 1)
      006265 F9               [12] 3005 	mov	r1,a
      006266 AD 6E            [24] 3006 	mov	r5,(_UART_Proc_ModifiyKissSpecialCharacters_sloc6_1_0 + 2)
      006268 74 01            [12] 3007 	mov	a,#0x01
      00626A 28               [12] 3008 	add	a,r0
      00626B FA               [12] 3009 	mov	r2,a
      00626C E4               [12] 3010 	clr	a
      00626D 39               [12] 3011 	addc	a,r1
      00626E FB               [12] 3012 	mov	r3,a
      00626F 8D 04            [24] 3013 	mov	ar4,r5
      006271 8A 82            [24] 3014 	mov	dpl,r2
      006273 8B 83            [24] 3015 	mov	dph,r3
      006275 8C F0            [24] 3016 	mov	b,r4
      006277 12 8D 8E         [24] 3017 	lcall	__gptrget
      00627A FA               [12] 3018 	mov	r2,a
      00627B 88 82            [24] 3019 	mov	dpl,r0
      00627D 89 83            [24] 3020 	mov	dph,r1
      00627F 8D F0            [24] 3021 	mov	b,r5
      006281 12 7C 62         [24] 3022 	lcall	__gptrput
                                   3023 ;	..\src\peripherals\UartComProc.c:141: for(j=i+1;j<*RxLength;j++)
      006284 0F               [12] 3024 	inc	r7
      006285 D0 01            [24] 3025 	pop	ar1
      006287 80 C1            [24] 3026 	sjmp	00115$
      006289                       3027 00102$:
                                   3028 ;	..\src\peripherals\UartComProc.c:145: *RxLength = *RxLength -1; //suprimi un caracter por lo cual el mensaje se disminuye su tama�o en 1
      006289 1E               [12] 3029 	dec	r6
      00628A 85 61 82         [24] 3030 	mov	dpl,_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0
      00628D 85 62 83         [24] 3031 	mov	dph,(_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0 + 1)
      006290 85 63 F0         [24] 3032 	mov	b,(_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0 + 2)
      006293 EE               [12] 3033 	mov	a,r6
      006294 12 7C 62         [24] 3034 	lcall	__gptrput
      006297                       3035 00119$:
                                   3036 ;	..\src\peripherals\UartComProc.c:125: for(i=0;i<*RxLength;i++)
      006297 09               [12] 3037 	inc	r1
                                   3038 ;	..\src\peripherals\UartComProc.c:149: }
      006298 02 61 76         [24] 3039 	ljmp	00118$
                                   3040 ;------------------------------------------------------------
                                   3041 ;Allocation info for local variables in function 'UART_Proc_SendMessage'
                                   3042 ;------------------------------------------------------------
                                   3043 ;sloc0                     Allocated with name '_UART_Proc_SendMessage_sloc0_1_0'
                                   3044 ;sloc1                     Allocated with name '_UART_Proc_SendMessage_sloc1_1_0'
                                   3045 ;sloc2                     Allocated with name '_UART_Proc_SendMessage_sloc2_1_0'
                                   3046 ;sloc3                     Allocated with name '_UART_Proc_SendMessage_sloc3_1_0'
                                   3047 ;length                    Allocated with name '_UART_Proc_SendMessage_PARM_2'
                                   3048 ;command                   Allocated with name '_UART_Proc_SendMessage_PARM_3'
                                   3049 ;payload                   Allocated with name '_UART_Proc_SendMessage_payload_65536_277'
                                   3050 ;i                         Allocated with name '_UART_Proc_SendMessage_i_65536_278'
                                   3051 ;RetVal                    Allocated with name '_UART_Proc_SendMessage_RetVal_65536_278'
                                   3052 ;CrcBuffer                 Allocated with name '_UART_Proc_SendMessage_CrcBuffer_65536_278'
                                   3053 ;CRC32                     Allocated with name '_UART_Proc_SendMessage_CRC32_65536_278'
                                   3054 ;------------------------------------------------------------
                                   3055 ;	..\src\peripherals\UartComProc.c:163: uint8_t UART_Proc_SendMessage(uint8_t *payload, uint8_t length, uint8_t command)
                                   3056 ;	-----------------------------------------
                                   3057 ;	 function UART_Proc_SendMessage
                                   3058 ;	-----------------------------------------
      00629B                       3059 _UART_Proc_SendMessage:
      00629B AF F0            [24] 3060 	mov	r7,b
      00629D AE 83            [24] 3061 	mov	r6,dph
      00629F E5 82            [12] 3062 	mov	a,dpl
      0062A1 90 03 E5         [24] 3063 	mov	dptr,#_UART_Proc_SendMessage_payload_65536_277
      0062A4 F0               [24] 3064 	movx	@dptr,a
      0062A5 EE               [12] 3065 	mov	a,r6
      0062A6 A3               [24] 3066 	inc	dptr
      0062A7 F0               [24] 3067 	movx	@dptr,a
      0062A8 EF               [12] 3068 	mov	a,r7
      0062A9 A3               [24] 3069 	inc	dptr
      0062AA F0               [24] 3070 	movx	@dptr,a
                                   3071 ;	..\src\peripherals\UartComProc.c:169: CrcBuffer = (uint8_t*) malloc(sizeof(uint8_t)*(length+1));
      0062AB 90 03 E3         [24] 3072 	mov	dptr,#_UART_Proc_SendMessage_PARM_2
      0062AE E0               [24] 3073 	movx	a,@dptr
      0062AF FF               [12] 3074 	mov	r7,a
      0062B0 FD               [12] 3075 	mov	r5,a
      0062B1 7E 00            [12] 3076 	mov	r6,#0x00
      0062B3 0D               [12] 3077 	inc	r5
      0062B4 BD 00 01         [24] 3078 	cjne	r5,#0x00,00116$
      0062B7 0E               [12] 3079 	inc	r6
      0062B8                       3080 00116$:
      0062B8 8D 82            [24] 3081 	mov	dpl,r5
      0062BA 8E 83            [24] 3082 	mov	dph,r6
      0062BC C0 07            [24] 3083 	push	ar7
      0062BE 12 7E 60         [24] 3084 	lcall	_malloc
      0062C1 AD 82            [24] 3085 	mov	r5,dpl
      0062C3 AE 83            [24] 3086 	mov	r6,dph
      0062C5 D0 07            [24] 3087 	pop	ar7
                                   3088 ;	..\src\peripherals\UartComProc.c:170: *CrcBuffer = command;
      0062C7 90 03 E4         [24] 3089 	mov	dptr,#_UART_Proc_SendMessage_PARM_3
      0062CA E0               [24] 3090 	movx	a,@dptr
      0062CB F5 42            [12] 3091 	mov	_UART_Proc_SendMessage_sloc1_1_0,a
      0062CD 8D 82            [24] 3092 	mov	dpl,r5
      0062CF 8E 83            [24] 3093 	mov	dph,r6
      0062D1 F0               [24] 3094 	movx	@dptr,a
                                   3095 ;	..\src\peripherals\UartComProc.c:171: memcpy(CrcBuffer+1,payload,length);
      0062D2 74 01            [12] 3096 	mov	a,#0x01
      0062D4 2D               [12] 3097 	add	a,r5
      0062D5 FA               [12] 3098 	mov	r2,a
      0062D6 E4               [12] 3099 	clr	a
      0062D7 3E               [12] 3100 	addc	a,r6
      0062D8 FB               [12] 3101 	mov	r3,a
      0062D9 8A 3F            [24] 3102 	mov	_UART_Proc_SendMessage_sloc0_1_0,r2
      0062DB 8B 40            [24] 3103 	mov	(_UART_Proc_SendMessage_sloc0_1_0 + 1),r3
      0062DD 75 41 00         [24] 3104 	mov	(_UART_Proc_SendMessage_sloc0_1_0 + 2),#0x00
      0062E0 90 03 E5         [24] 3105 	mov	dptr,#_UART_Proc_SendMessage_payload_65536_277
      0062E3 E0               [24] 3106 	movx	a,@dptr
      0062E4 F5 43            [12] 3107 	mov	_UART_Proc_SendMessage_sloc2_1_0,a
      0062E6 A3               [24] 3108 	inc	dptr
      0062E7 E0               [24] 3109 	movx	a,@dptr
      0062E8 F5 44            [12] 3110 	mov	(_UART_Proc_SendMessage_sloc2_1_0 + 1),a
      0062EA A3               [24] 3111 	inc	dptr
      0062EB E0               [24] 3112 	movx	a,@dptr
      0062EC F5 45            [12] 3113 	mov	(_UART_Proc_SendMessage_sloc2_1_0 + 2),a
      0062EE 90 04 6D         [24] 3114 	mov	dptr,#_memcpy_PARM_2
      0062F1 E5 43            [12] 3115 	mov	a,_UART_Proc_SendMessage_sloc2_1_0
      0062F3 F0               [24] 3116 	movx	@dptr,a
      0062F4 E5 44            [12] 3117 	mov	a,(_UART_Proc_SendMessage_sloc2_1_0 + 1)
      0062F6 A3               [24] 3118 	inc	dptr
      0062F7 F0               [24] 3119 	movx	@dptr,a
      0062F8 E5 45            [12] 3120 	mov	a,(_UART_Proc_SendMessage_sloc2_1_0 + 2)
      0062FA A3               [24] 3121 	inc	dptr
      0062FB F0               [24] 3122 	movx	@dptr,a
      0062FC 90 04 70         [24] 3123 	mov	dptr,#_memcpy_PARM_3
      0062FF EF               [12] 3124 	mov	a,r7
      006300 F0               [24] 3125 	movx	@dptr,a
      006301 E4               [12] 3126 	clr	a
      006302 A3               [24] 3127 	inc	dptr
      006303 F0               [24] 3128 	movx	@dptr,a
      006304 85 3F 82         [24] 3129 	mov	dpl,_UART_Proc_SendMessage_sloc0_1_0
      006307 85 40 83         [24] 3130 	mov	dph,(_UART_Proc_SendMessage_sloc0_1_0 + 1)
      00630A 85 41 F0         [24] 3131 	mov	b,(_UART_Proc_SendMessage_sloc0_1_0 + 2)
      00630D C0 07            [24] 3132 	push	ar7
      00630F C0 06            [24] 3133 	push	ar6
      006311 C0 05            [24] 3134 	push	ar5
      006313 12 78 14         [24] 3135 	lcall	_memcpy
      006316 D0 05            [24] 3136 	pop	ar5
      006318 D0 06            [24] 3137 	pop	ar6
      00631A D0 07            [24] 3138 	pop	ar7
                                   3139 ;	..\src\peripherals\UartComProc.c:172: CRC32 = UART_Calc_CRC32(CrcBuffer,length+1,FALSE);
      00631C 8D 01            [24] 3140 	mov	ar1,r5
      00631E 8E 04            [24] 3141 	mov	ar4,r6
      006320 8C 03            [24] 3142 	mov	ar3,r4
      006322 7C 00            [12] 3143 	mov	r4,#0x00
      006324 8F 02            [24] 3144 	mov	ar2,r7
      006326 90 03 E8         [24] 3145 	mov	dptr,#_UART_Calc_CRC32_PARM_2
      006329 EA               [12] 3146 	mov	a,r2
      00632A 04               [12] 3147 	inc	a
      00632B F0               [24] 3148 	movx	@dptr,a
      00632C 90 03 E9         [24] 3149 	mov	dptr,#_UART_Calc_CRC32_PARM_3
      00632F E4               [12] 3150 	clr	a
      006330 F0               [24] 3151 	movx	@dptr,a
      006331 89 82            [24] 3152 	mov	dpl,r1
      006333 8B 83            [24] 3153 	mov	dph,r3
      006335 8C F0            [24] 3154 	mov	b,r4
      006337 C0 07            [24] 3155 	push	ar7
      006339 C0 06            [24] 3156 	push	ar6
      00633B C0 05            [24] 3157 	push	ar5
      00633D 12 63 BF         [24] 3158 	lcall	_UART_Calc_CRC32
      006340 85 82 46         [24] 3159 	mov	_UART_Proc_SendMessage_sloc3_1_0,dpl
      006343 85 83 47         [24] 3160 	mov	(_UART_Proc_SendMessage_sloc3_1_0 + 1),dph
      006346 85 F0 48         [24] 3161 	mov	(_UART_Proc_SendMessage_sloc3_1_0 + 2),b
      006349 F5 49            [12] 3162 	mov	(_UART_Proc_SendMessage_sloc3_1_0 + 3),a
      00634B D0 05            [24] 3163 	pop	ar5
      00634D D0 06            [24] 3164 	pop	ar6
      00634F D0 07            [24] 3165 	pop	ar7
                                   3166 ;	..\src\peripherals\UartComProc.c:173: uart0_tx(FRAME_START);
      006351 75 82 C0         [24] 3167 	mov	dpl,#0xc0
      006354 12 7D 6A         [24] 3168 	lcall	_uart0_tx
                                   3169 ;	..\src\peripherals\UartComProc.c:174: uart0_tx(command);
      006357 85 42 82         [24] 3170 	mov	dpl,_UART_Proc_SendMessage_sloc1_1_0
      00635A 12 7D 6A         [24] 3171 	lcall	_uart0_tx
                                   3172 ;	..\src\peripherals\UartComProc.c:175: for(i=0;i<length;i++)
      00635D 78 00            [12] 3173 	mov	r0,#0x00
      00635F                       3174 00103$:
      00635F C3               [12] 3175 	clr	c
      006360 E8               [12] 3176 	mov	a,r0
      006361 9F               [12] 3177 	subb	a,r7
      006362 50 24            [24] 3178 	jnc	00101$
                                   3179 ;	..\src\peripherals\UartComProc.c:177: uart0_tx(*(payload+i));
      006364 C0 05            [24] 3180 	push	ar5
      006366 C0 06            [24] 3181 	push	ar6
      006368 E8               [12] 3182 	mov	a,r0
      006369 25 43            [12] 3183 	add	a,_UART_Proc_SendMessage_sloc2_1_0
      00636B FC               [12] 3184 	mov	r4,a
      00636C E4               [12] 3185 	clr	a
      00636D 35 44            [12] 3186 	addc	a,(_UART_Proc_SendMessage_sloc2_1_0 + 1)
      00636F FD               [12] 3187 	mov	r5,a
      006370 AE 45            [24] 3188 	mov	r6,(_UART_Proc_SendMessage_sloc2_1_0 + 2)
      006372 8C 82            [24] 3189 	mov	dpl,r4
      006374 8D 83            [24] 3190 	mov	dph,r5
      006376 8E F0            [24] 3191 	mov	b,r6
      006378 12 8D 8E         [24] 3192 	lcall	__gptrget
      00637B FC               [12] 3193 	mov	r4,a
      00637C F5 82            [12] 3194 	mov	dpl,a
      00637E 12 7D 6A         [24] 3195 	lcall	_uart0_tx
                                   3196 ;	..\src\peripherals\UartComProc.c:175: for(i=0;i<length;i++)
      006381 08               [12] 3197 	inc	r0
      006382 D0 06            [24] 3198 	pop	ar6
      006384 D0 05            [24] 3199 	pop	ar5
      006386 80 D7            [24] 3200 	sjmp	00103$
      006388                       3201 00101$:
                                   3202 ;	..\src\peripherals\UartComProc.c:179: uart0_tx((uint8_t)((CRC32 & 0xFF000000) >>24));
      006388 AF 49            [24] 3203 	mov	r7,(_UART_Proc_SendMessage_sloc3_1_0 + 3)
      00638A 8F 02            [24] 3204 	mov	ar2,r7
      00638C 8A 82            [24] 3205 	mov	dpl,r2
      00638E 12 7D 6A         [24] 3206 	lcall	_uart0_tx
                                   3207 ;	..\src\peripherals\UartComProc.c:180: uart0_tx((uint8_t)((CRC32 & 0x00FF0000) >>16));
      006391 AC 48            [24] 3208 	mov	r4,(_UART_Proc_SendMessage_sloc3_1_0 + 2)
      006393 8C 02            [24] 3209 	mov	ar2,r4
      006395 8A 82            [24] 3210 	mov	dpl,r2
      006397 12 7D 6A         [24] 3211 	lcall	_uart0_tx
                                   3212 ;	..\src\peripherals\UartComProc.c:181: uart0_tx((uint8_t)((CRC32 & 0x0000FF00) >>8));
      00639A AB 47            [24] 3213 	mov	r3,(_UART_Proc_SendMessage_sloc3_1_0 + 1)
      00639C 8B 02            [24] 3214 	mov	ar2,r3
      00639E 8A 82            [24] 3215 	mov	dpl,r2
      0063A0 12 7D 6A         [24] 3216 	lcall	_uart0_tx
                                   3217 ;	..\src\peripherals\UartComProc.c:182: uart0_tx((uint8_t)(CRC32 & 0x000000FF));
      0063A3 A9 46            [24] 3218 	mov	r1,_UART_Proc_SendMessage_sloc3_1_0
      0063A5 89 82            [24] 3219 	mov	dpl,r1
      0063A7 12 7D 6A         [24] 3220 	lcall	_uart0_tx
                                   3221 ;	..\src\peripherals\UartComProc.c:183: uart0_tx(FRAME_END);
      0063AA 75 82 C0         [24] 3222 	mov	dpl,#0xc0
      0063AD 12 7D 6A         [24] 3223 	lcall	_uart0_tx
                                   3224 ;	..\src\peripherals\UartComProc.c:184: free(CrcBuffer);
      0063B0 7F 00            [12] 3225 	mov	r7,#0x00
      0063B2 8D 82            [24] 3226 	mov	dpl,r5
      0063B4 8E 83            [24] 3227 	mov	dph,r6
      0063B6 8F F0            [24] 3228 	mov	b,r7
      0063B8 12 6E 35         [24] 3229 	lcall	_free
                                   3230 ;	..\src\peripherals\UartComProc.c:185: return RetVal;
      0063BB 75 82 00         [24] 3231 	mov	dpl,#0x00
                                   3232 ;	..\src\peripherals\UartComProc.c:186: }
      0063BE 22               [24] 3233 	ret
                                   3234 ;------------------------------------------------------------
                                   3235 ;Allocation info for local variables in function 'UART_Calc_CRC32'
                                   3236 ;------------------------------------------------------------
                                   3237 ;length                    Allocated with name '_UART_Calc_CRC32_PARM_2'
                                   3238 ;littleEndian              Allocated with name '_UART_Calc_CRC32_PARM_3'
                                   3239 ;message                   Allocated with name '_UART_Calc_CRC32_message_65536_281'
                                   3240 ;i                         Allocated with name '_UART_Calc_CRC32_i_65536_282'
                                   3241 ;j                         Allocated with name '_UART_Calc_CRC32_j_65536_282'
                                   3242 ;byte                      Allocated with name '_UART_Calc_CRC32_byte_65536_282'
                                   3243 ;crc                       Allocated with name '_UART_Calc_CRC32_crc_65536_282'
                                   3244 ;mask                      Allocated with name '_UART_Calc_CRC32_mask_65536_282'
                                   3245 ;sloc0                     Allocated with name '_UART_Calc_CRC32_sloc0_1_0'
                                   3246 ;sloc1                     Allocated with name '_UART_Calc_CRC32_sloc1_1_0'
                                   3247 ;sloc2                     Allocated with name '_UART_Calc_CRC32_sloc2_1_0'
                                   3248 ;sloc3                     Allocated with name '_UART_Calc_CRC32_sloc3_1_0'
                                   3249 ;------------------------------------------------------------
                                   3250 ;	..\src\peripherals\UartComProc.c:199: uint32_t UART_Calc_CRC32(uint8_t *message,uint8_t length,uint8_t littleEndian) {
                                   3251 ;	-----------------------------------------
                                   3252 ;	 function UART_Calc_CRC32
                                   3253 ;	-----------------------------------------
      0063BF                       3254 _UART_Calc_CRC32:
      0063BF AF F0            [24] 3255 	mov	r7,b
      0063C1 AE 83            [24] 3256 	mov	r6,dph
      0063C3 E5 82            [12] 3257 	mov	a,dpl
      0063C5 90 03 EA         [24] 3258 	mov	dptr,#_UART_Calc_CRC32_message_65536_281
      0063C8 F0               [24] 3259 	movx	@dptr,a
      0063C9 EE               [12] 3260 	mov	a,r6
      0063CA A3               [24] 3261 	inc	dptr
      0063CB F0               [24] 3262 	movx	@dptr,a
      0063CC EF               [12] 3263 	mov	a,r7
      0063CD A3               [24] 3264 	inc	dptr
      0063CE F0               [24] 3265 	movx	@dptr,a
                                   3266 ;	..\src\peripherals\UartComProc.c:203: crc = 0xFFFFFFFF;
      0063CF 90 03 ED         [24] 3267 	mov	dptr,#_UART_Calc_CRC32_crc_65536_282
      0063D2 74 FF            [12] 3268 	mov	a,#0xff
      0063D4 F0               [24] 3269 	movx	@dptr,a
      0063D5 A3               [24] 3270 	inc	dptr
      0063D6 F0               [24] 3271 	movx	@dptr,a
      0063D7 A3               [24] 3272 	inc	dptr
      0063D8 F0               [24] 3273 	movx	@dptr,a
      0063D9 A3               [24] 3274 	inc	dptr
      0063DA F0               [24] 3275 	movx	@dptr,a
                                   3276 ;	..\src\peripherals\UartComProc.c:204: while (i < length)
      0063DB 90 03 EA         [24] 3277 	mov	dptr,#_UART_Calc_CRC32_message_65536_281
      0063DE E0               [24] 3278 	movx	a,@dptr
      0063DF F5 5E            [12] 3279 	mov	_UART_Calc_CRC32_sloc0_1_0,a
      0063E1 A3               [24] 3280 	inc	dptr
      0063E2 E0               [24] 3281 	movx	a,@dptr
      0063E3 F5 5F            [12] 3282 	mov	(_UART_Calc_CRC32_sloc0_1_0 + 1),a
      0063E5 A3               [24] 3283 	inc	dptr
      0063E6 E0               [24] 3284 	movx	a,@dptr
      0063E7 F5 60            [12] 3285 	mov	(_UART_Calc_CRC32_sloc0_1_0 + 2),a
      0063E9 90 03 E8         [24] 3286 	mov	dptr,#_UART_Calc_CRC32_PARM_2
      0063EC E0               [24] 3287 	movx	a,@dptr
      0063ED FC               [12] 3288 	mov	r4,a
      0063EE E4               [12] 3289 	clr	a
      0063EF F5 63            [12] 3290 	mov	_UART_Calc_CRC32_sloc2_1_0,a
      0063F1 F5 64            [12] 3291 	mov	(_UART_Calc_CRC32_sloc2_1_0 + 1),a
      0063F3                       3292 00102$:
      0063F3 8C 00            [24] 3293 	mov	ar0,r4
      0063F5 79 00            [12] 3294 	mov	r1,#0x00
      0063F7 C3               [12] 3295 	clr	c
      0063F8 E5 63            [12] 3296 	mov	a,_UART_Calc_CRC32_sloc2_1_0
      0063FA 98               [12] 3297 	subb	a,r0
      0063FB E5 64            [12] 3298 	mov	a,(_UART_Calc_CRC32_sloc2_1_0 + 1)
      0063FD 64 80            [12] 3299 	xrl	a,#0x80
      0063FF 89 F0            [24] 3300 	mov	b,r1
      006401 63 F0 80         [24] 3301 	xrl	b,#0x80
      006404 95 F0            [12] 3302 	subb	a,b
      006406 40 03            [24] 3303 	jc	00133$
      006408 02 64 AF         [24] 3304 	ljmp	00104$
      00640B                       3305 00133$:
                                   3306 ;	..\src\peripherals\UartComProc.c:206: byte = message[i];
      00640B C0 04            [24] 3307 	push	ar4
      00640D E5 63            [12] 3308 	mov	a,_UART_Calc_CRC32_sloc2_1_0
      00640F 25 5E            [12] 3309 	add	a,_UART_Calc_CRC32_sloc0_1_0
      006411 F8               [12] 3310 	mov	r0,a
      006412 E5 64            [12] 3311 	mov	a,(_UART_Calc_CRC32_sloc2_1_0 + 1)
      006414 35 5F            [12] 3312 	addc	a,(_UART_Calc_CRC32_sloc0_1_0 + 1)
      006416 F9               [12] 3313 	mov	r1,a
      006417 AC 60            [24] 3314 	mov	r4,(_UART_Calc_CRC32_sloc0_1_0 + 2)
      006419 88 82            [24] 3315 	mov	dpl,r0
      00641B 89 83            [24] 3316 	mov	dph,r1
      00641D 8C F0            [24] 3317 	mov	b,r4
      00641F 12 8D 8E         [24] 3318 	lcall	__gptrget
      006422 F8               [12] 3319 	mov	r0,a
      006423 79 00            [12] 3320 	mov	r1,#0x00
      006425 7C 00            [12] 3321 	mov	r4,#0x00
      006427 7F 00            [12] 3322 	mov	r7,#0x00
                                   3323 ;	..\src\peripherals\UartComProc.c:208: crc = crc ^ byte;
      006429 90 03 ED         [24] 3324 	mov	dptr,#_UART_Calc_CRC32_crc_65536_282
      00642C E0               [24] 3325 	movx	a,@dptr
      00642D 68               [12] 3326 	xrl	a,r0
      00642E F0               [24] 3327 	movx	@dptr,a
      00642F A3               [24] 3328 	inc	dptr
      006430 E0               [24] 3329 	movx	a,@dptr
      006431 69               [12] 3330 	xrl	a,r1
      006432 F0               [24] 3331 	movx	@dptr,a
      006433 A3               [24] 3332 	inc	dptr
      006434 E0               [24] 3333 	movx	a,@dptr
      006435 6C               [12] 3334 	xrl	a,r4
      006436 F0               [24] 3335 	movx	@dptr,a
      006437 A3               [24] 3336 	inc	dptr
      006438 E0               [24] 3337 	movx	a,@dptr
      006439 6F               [12] 3338 	xrl	a,r7
      00643A F0               [24] 3339 	movx	@dptr,a
                                   3340 ;	..\src\peripherals\UartComProc.c:209: for (j = 7; j >= 0; j--)
      00643B 75 61 07         [24] 3341 	mov	_UART_Calc_CRC32_sloc1_1_0,#0x07
                                   3342 ;	1-genFromRTrack replaced	mov	(_UART_Calc_CRC32_sloc1_1_0 + 1),#0x00
      00643E 8F 62            [24] 3343 	mov	(_UART_Calc_CRC32_sloc1_1_0 + 1),r7
                                   3344 ;	..\src\peripherals\UartComProc.c:218: return crc;
      006440 D0 04            [24] 3345 	pop	ar4
                                   3346 ;	..\src\peripherals\UartComProc.c:209: for (j = 7; j >= 0; j--)
      006442                       3347 00107$:
                                   3348 ;	..\src\peripherals\UartComProc.c:211: mask = -(crc & 1);
      006442 C0 04            [24] 3349 	push	ar4
      006444 90 03 ED         [24] 3350 	mov	dptr,#_UART_Calc_CRC32_crc_65536_282
      006447 E0               [24] 3351 	movx	a,@dptr
      006448 F9               [12] 3352 	mov	r1,a
      006449 A3               [24] 3353 	inc	dptr
      00644A E0               [24] 3354 	movx	a,@dptr
      00644B FD               [12] 3355 	mov	r5,a
      00644C A3               [24] 3356 	inc	dptr
      00644D E0               [24] 3357 	movx	a,@dptr
      00644E FE               [12] 3358 	mov	r6,a
      00644F A3               [24] 3359 	inc	dptr
      006450 E0               [24] 3360 	movx	a,@dptr
      006451 FF               [12] 3361 	mov	r7,a
      006452 74 01            [12] 3362 	mov	a,#0x01
      006454 59               [12] 3363 	anl	a,r1
      006455 F8               [12] 3364 	mov	r0,a
      006456 7A 00            [12] 3365 	mov	r2,#0x00
      006458 7B 00            [12] 3366 	mov	r3,#0x00
      00645A 7C 00            [12] 3367 	mov	r4,#0x00
      00645C C3               [12] 3368 	clr	c
      00645D E4               [12] 3369 	clr	a
      00645E 98               [12] 3370 	subb	a,r0
      00645F F8               [12] 3371 	mov	r0,a
      006460 E4               [12] 3372 	clr	a
      006461 9A               [12] 3373 	subb	a,r2
      006462 FA               [12] 3374 	mov	r2,a
      006463 E4               [12] 3375 	clr	a
      006464 9B               [12] 3376 	subb	a,r3
      006465 FB               [12] 3377 	mov	r3,a
      006466 E4               [12] 3378 	clr	a
      006467 9C               [12] 3379 	subb	a,r4
      006468 FC               [12] 3380 	mov	r4,a
                                   3381 ;	..\src\peripherals\UartComProc.c:212: crc = (crc >> 1) ^ (0xEDB88320 & mask);
      006469 EF               [12] 3382 	mov	a,r7
      00646A C3               [12] 3383 	clr	c
      00646B 13               [12] 3384 	rrc	a
      00646C FF               [12] 3385 	mov	r7,a
      00646D EE               [12] 3386 	mov	a,r6
      00646E 13               [12] 3387 	rrc	a
      00646F FE               [12] 3388 	mov	r6,a
      006470 ED               [12] 3389 	mov	a,r5
      006471 13               [12] 3390 	rrc	a
      006472 FD               [12] 3391 	mov	r5,a
      006473 E9               [12] 3392 	mov	a,r1
      006474 13               [12] 3393 	rrc	a
      006475 F9               [12] 3394 	mov	r1,a
      006476 53 00 20         [24] 3395 	anl	ar0,#0x20
      006479 53 02 83         [24] 3396 	anl	ar2,#0x83
      00647C 53 03 B8         [24] 3397 	anl	ar3,#0xb8
      00647F 53 04 ED         [24] 3398 	anl	ar4,#0xed
      006482 90 03 ED         [24] 3399 	mov	dptr,#_UART_Calc_CRC32_crc_65536_282
      006485 E8               [12] 3400 	mov	a,r0
      006486 69               [12] 3401 	xrl	a,r1
      006487 F0               [24] 3402 	movx	@dptr,a
      006488 EA               [12] 3403 	mov	a,r2
      006489 6D               [12] 3404 	xrl	a,r5
      00648A A3               [24] 3405 	inc	dptr
      00648B F0               [24] 3406 	movx	@dptr,a
      00648C EB               [12] 3407 	mov	a,r3
      00648D 6E               [12] 3408 	xrl	a,r6
      00648E A3               [24] 3409 	inc	dptr
      00648F F0               [24] 3410 	movx	@dptr,a
      006490 EC               [12] 3411 	mov	a,r4
      006491 6F               [12] 3412 	xrl	a,r7
      006492 A3               [24] 3413 	inc	dptr
      006493 F0               [24] 3414 	movx	@dptr,a
                                   3415 ;	..\src\peripherals\UartComProc.c:209: for (j = 7; j >= 0; j--)
      006494 15 61            [12] 3416 	dec	_UART_Calc_CRC32_sloc1_1_0
      006496 74 FF            [12] 3417 	mov	a,#0xff
      006498 B5 61 02         [24] 3418 	cjne	a,_UART_Calc_CRC32_sloc1_1_0,00134$
      00649B 15 62            [12] 3419 	dec	(_UART_Calc_CRC32_sloc1_1_0 + 1)
      00649D                       3420 00134$:
      00649D E5 62            [12] 3421 	mov	a,(_UART_Calc_CRC32_sloc1_1_0 + 1)
      00649F D0 04            [24] 3422 	pop	ar4
      0064A1 30 E7 9E         [24] 3423 	jnb	acc.7,00107$
                                   3424 ;	..\src\peripherals\UartComProc.c:214: i = i + 1;
      0064A4 05 63            [12] 3425 	inc	_UART_Calc_CRC32_sloc2_1_0
      0064A6 E4               [12] 3426 	clr	a
      0064A7 B5 63 02         [24] 3427 	cjne	a,_UART_Calc_CRC32_sloc2_1_0,00136$
      0064AA 05 64            [12] 3428 	inc	(_UART_Calc_CRC32_sloc2_1_0 + 1)
      0064AC                       3429 00136$:
      0064AC 02 63 F3         [24] 3430 	ljmp	00102$
      0064AF                       3431 00104$:
                                   3432 ;	..\src\peripherals\UartComProc.c:216: crc = ~crc;
      0064AF 90 03 ED         [24] 3433 	mov	dptr,#_UART_Calc_CRC32_crc_65536_282
      0064B2 E0               [24] 3434 	movx	a,@dptr
      0064B3 FC               [12] 3435 	mov	r4,a
      0064B4 A3               [24] 3436 	inc	dptr
      0064B5 E0               [24] 3437 	movx	a,@dptr
      0064B6 FD               [12] 3438 	mov	r5,a
      0064B7 A3               [24] 3439 	inc	dptr
      0064B8 E0               [24] 3440 	movx	a,@dptr
      0064B9 FE               [12] 3441 	mov	r6,a
      0064BA A3               [24] 3442 	inc	dptr
      0064BB E0               [24] 3443 	movx	a,@dptr
      0064BC FF               [12] 3444 	mov	r7,a
      0064BD 90 03 ED         [24] 3445 	mov	dptr,#_UART_Calc_CRC32_crc_65536_282
      0064C0 EC               [12] 3446 	mov	a,r4
      0064C1 F4               [12] 3447 	cpl	a
      0064C2 F0               [24] 3448 	movx	@dptr,a
      0064C3 ED               [12] 3449 	mov	a,r5
      0064C4 F4               [12] 3450 	cpl	a
      0064C5 A3               [24] 3451 	inc	dptr
      0064C6 F0               [24] 3452 	movx	@dptr,a
      0064C7 EE               [12] 3453 	mov	a,r6
      0064C8 F4               [12] 3454 	cpl	a
      0064C9 A3               [24] 3455 	inc	dptr
      0064CA F0               [24] 3456 	movx	@dptr,a
      0064CB EF               [12] 3457 	mov	a,r7
      0064CC F4               [12] 3458 	cpl	a
      0064CD A3               [24] 3459 	inc	dptr
      0064CE F0               [24] 3460 	movx	@dptr,a
                                   3461 ;	..\src\peripherals\UartComProc.c:217: if(littleEndian)crc=(((crc & 0x000000FF) << 24)+((crc & 0x0000FF00) << 8)+ ((crc & 0x00FF0000) >> 8)+((crc & 0xFF000000) >> 24));//swap little endian big endian
      0064CF 90 03 E9         [24] 3462 	mov	dptr,#_UART_Calc_CRC32_PARM_3
      0064D2 E0               [24] 3463 	movx	a,@dptr
      0064D3 70 03            [24] 3464 	jnz	00137$
      0064D5 02 65 4A         [24] 3465 	ljmp	00106$
      0064D8                       3466 00137$:
      0064D8 90 03 ED         [24] 3467 	mov	dptr,#_UART_Calc_CRC32_crc_65536_282
      0064DB E0               [24] 3468 	movx	a,@dptr
      0064DC FC               [12] 3469 	mov	r4,a
      0064DD A3               [24] 3470 	inc	dptr
      0064DE E0               [24] 3471 	movx	a,@dptr
      0064DF FD               [12] 3472 	mov	r5,a
      0064E0 A3               [24] 3473 	inc	dptr
      0064E1 E0               [24] 3474 	movx	a,@dptr
      0064E2 FE               [12] 3475 	mov	r6,a
      0064E3 A3               [24] 3476 	inc	dptr
      0064E4 E0               [24] 3477 	movx	a,@dptr
      0064E5 FF               [12] 3478 	mov	r7,a
      0064E6 8C 00            [24] 3479 	mov	ar0,r4
      0064E8 7B 00            [12] 3480 	mov	r3,#0x00
      0064EA 88 68            [24] 3481 	mov	(_UART_Calc_CRC32_sloc3_1_0 + 3),r0
                                   3482 ;	1-genFromRTrack replaced	mov	_UART_Calc_CRC32_sloc3_1_0,#0x00
      0064EC 8B 65            [24] 3483 	mov	_UART_Calc_CRC32_sloc3_1_0,r3
                                   3484 ;	1-genFromRTrack replaced	mov	(_UART_Calc_CRC32_sloc3_1_0 + 1),#0x00
      0064EE 8B 66            [24] 3485 	mov	(_UART_Calc_CRC32_sloc3_1_0 + 1),r3
                                   3486 ;	1-genFromRTrack replaced	mov	(_UART_Calc_CRC32_sloc3_1_0 + 2),#0x00
      0064F0 8B 67            [24] 3487 	mov	(_UART_Calc_CRC32_sloc3_1_0 + 2),r3
      0064F2 78 00            [12] 3488 	mov	r0,#0x00
      0064F4 8D 01            [24] 3489 	mov	ar1,r5
      0064F6 7A 00            [12] 3490 	mov	r2,#0x00
      0064F8 8A 03            [24] 3491 	mov	ar3,r2
      0064FA 89 02            [24] 3492 	mov	ar2,r1
      0064FC 88 01            [24] 3493 	mov	ar1,r0
      0064FE E4               [12] 3494 	clr	a
      0064FF 25 65            [12] 3495 	add	a,_UART_Calc_CRC32_sloc3_1_0
      006501 F5 65            [12] 3496 	mov	_UART_Calc_CRC32_sloc3_1_0,a
      006503 E9               [12] 3497 	mov	a,r1
      006504 35 66            [12] 3498 	addc	a,(_UART_Calc_CRC32_sloc3_1_0 + 1)
      006506 F5 66            [12] 3499 	mov	(_UART_Calc_CRC32_sloc3_1_0 + 1),a
      006508 EA               [12] 3500 	mov	a,r2
      006509 35 67            [12] 3501 	addc	a,(_UART_Calc_CRC32_sloc3_1_0 + 2)
      00650B F5 67            [12] 3502 	mov	(_UART_Calc_CRC32_sloc3_1_0 + 2),a
      00650D EB               [12] 3503 	mov	a,r3
      00650E 35 68            [12] 3504 	addc	a,(_UART_Calc_CRC32_sloc3_1_0 + 3)
      006510 F5 68            [12] 3505 	mov	(_UART_Calc_CRC32_sloc3_1_0 + 3),a
      006512 79 00            [12] 3506 	mov	r1,#0x00
      006514 8E 02            [24] 3507 	mov	ar2,r6
      006516 7B 00            [12] 3508 	mov	r3,#0x00
      006518 89 00            [24] 3509 	mov	ar0,r1
      00651A 8A 01            [24] 3510 	mov	ar1,r2
      00651C 8B 02            [24] 3511 	mov	ar2,r3
      00651E 7B 00            [12] 3512 	mov	r3,#0x00
      006520 E8               [12] 3513 	mov	a,r0
      006521 25 65            [12] 3514 	add	a,_UART_Calc_CRC32_sloc3_1_0
      006523 F8               [12] 3515 	mov	r0,a
      006524 E9               [12] 3516 	mov	a,r1
      006525 35 66            [12] 3517 	addc	a,(_UART_Calc_CRC32_sloc3_1_0 + 1)
      006527 F9               [12] 3518 	mov	r1,a
      006528 EA               [12] 3519 	mov	a,r2
      006529 35 67            [12] 3520 	addc	a,(_UART_Calc_CRC32_sloc3_1_0 + 2)
      00652B FA               [12] 3521 	mov	r2,a
      00652C EB               [12] 3522 	mov	a,r3
      00652D 35 68            [12] 3523 	addc	a,(_UART_Calc_CRC32_sloc3_1_0 + 3)
      00652F FB               [12] 3524 	mov	r3,a
      006530 8F 04            [24] 3525 	mov	ar4,r7
      006532 7D 00            [12] 3526 	mov	r5,#0x00
      006534 7E 00            [12] 3527 	mov	r6,#0x00
      006536 7F 00            [12] 3528 	mov	r7,#0x00
      006538 90 03 ED         [24] 3529 	mov	dptr,#_UART_Calc_CRC32_crc_65536_282
      00653B EC               [12] 3530 	mov	a,r4
      00653C 28               [12] 3531 	add	a,r0
      00653D F0               [24] 3532 	movx	@dptr,a
      00653E ED               [12] 3533 	mov	a,r5
      00653F 39               [12] 3534 	addc	a,r1
      006540 A3               [24] 3535 	inc	dptr
      006541 F0               [24] 3536 	movx	@dptr,a
      006542 EE               [12] 3537 	mov	a,r6
      006543 3A               [12] 3538 	addc	a,r2
      006544 A3               [24] 3539 	inc	dptr
      006545 F0               [24] 3540 	movx	@dptr,a
      006546 EF               [12] 3541 	mov	a,r7
      006547 3B               [12] 3542 	addc	a,r3
      006548 A3               [24] 3543 	inc	dptr
      006549 F0               [24] 3544 	movx	@dptr,a
      00654A                       3545 00106$:
                                   3546 ;	..\src\peripherals\UartComProc.c:218: return crc;
      00654A 90 03 ED         [24] 3547 	mov	dptr,#_UART_Calc_CRC32_crc_65536_282
      00654D E0               [24] 3548 	movx	a,@dptr
      00654E FC               [12] 3549 	mov	r4,a
      00654F A3               [24] 3550 	inc	dptr
      006550 E0               [24] 3551 	movx	a,@dptr
      006551 FD               [12] 3552 	mov	r5,a
      006552 A3               [24] 3553 	inc	dptr
      006553 E0               [24] 3554 	movx	a,@dptr
      006554 FE               [12] 3555 	mov	r6,a
      006555 A3               [24] 3556 	inc	dptr
      006556 E0               [24] 3557 	movx	a,@dptr
      006557 8C 82            [24] 3558 	mov	dpl,r4
      006559 8D 83            [24] 3559 	mov	dph,r5
      00655B 8E F0            [24] 3560 	mov	b,r6
                                   3561 ;	..\src\peripherals\UartComProc.c:219: }
      00655D 22               [24] 3562 	ret
                                   3563 ;------------------------------------------------------------
                                   3564 ;Allocation info for local variables in function 'UART_ParseMessage'
                                   3565 ;------------------------------------------------------------
                                   3566 ;payload                   Allocated with name '_UART_ParseMessage_PARM_2'
                                   3567 ;length                    Allocated with name '_UART_ParseMessage_PARM_3'
                                   3568 ;cmd                       Allocated with name '_UART_ParseMessage_cmd_65536_286'
                                   3569 ;RetVal                    Allocated with name '_UART_ParseMessage_RetVal_65536_287'
                                   3570 ;PayloadLength             Allocated with name '_UART_ParseMessage_PayloadLength_65536_287'
                                   3571 ;------------------------------------------------------------
                                   3572 ;	..\src\peripherals\UartComProc.c:232: uint8_t UART_ParseMessage(uint8_t cmd, uint8_t *payload, uint8_t length)
                                   3573 ;	-----------------------------------------
                                   3574 ;	 function UART_ParseMessage
                                   3575 ;	-----------------------------------------
      00655E                       3576 _UART_ParseMessage:
      00655E E5 82            [12] 3577 	mov	a,dpl
      006560 90 03 F5         [24] 3578 	mov	dptr,#_UART_ParseMessage_cmd_65536_286
      006563 F0               [24] 3579 	movx	@dptr,a
                                   3580 ;	..\src\peripherals\UartComProc.c:235: uint8_t PayloadLength = length -1;
      006564 90 03 F4         [24] 3581 	mov	dptr,#_UART_ParseMessage_PARM_3
      006567 E0               [24] 3582 	movx	a,@dptr
      006568 FF               [12] 3583 	mov	r7,a
      006569 1F               [12] 3584 	dec	r7
                                   3585 ;	..\src\peripherals\UartComProc.c:236: switch(cmd)
      00656A 90 03 F5         [24] 3586 	mov	dptr,#_UART_ParseMessage_cmd_65536_286
      00656D E0               [24] 3587 	movx	a,@dptr
      00656E FE               [12] 3588 	mov	r6,a
      00656F BE 01 02         [24] 3589 	cjne	r6,#0x01,00135$
      006572 80 1B            [24] 3590 	sjmp	00101$
      006574                       3591 00135$:
      006574 BE 02 02         [24] 3592 	cjne	r6,#0x02,00136$
      006577 80 2F            [24] 3593 	sjmp	00102$
      006579                       3594 00136$:
      006579 BE 03 02         [24] 3595 	cjne	r6,#0x03,00137$
      00657C 80 43            [24] 3596 	sjmp	00103$
      00657E                       3597 00137$:
      00657E BE 04 02         [24] 3598 	cjne	r6,#0x04,00138$
      006581 80 57            [24] 3599 	sjmp	00104$
      006583                       3600 00138$:
      006583 BE 07 02         [24] 3601 	cjne	r6,#0x07,00139$
      006586 80 6B            [24] 3602 	sjmp	00105$
      006588                       3603 00139$:
      006588 BE 0A 03         [24] 3604 	cjne	r6,#0x0a,00140$
      00658B 02 66 0C         [24] 3605 	ljmp	00106$
      00658E                       3606 00140$:
      00658E 22               [24] 3607 	ret
                                   3608 ;	..\src\peripherals\UartComProc.c:238: case CONFIG:
      00658F                       3609 00101$:
                                   3610 ;	..\src\peripherals\UartComProc.c:239: UART_ParseConfigMessage(payload, PayloadLength);
      00658F 90 03 F1         [24] 3611 	mov	dptr,#_UART_ParseMessage_PARM_2
      006592 E0               [24] 3612 	movx	a,@dptr
      006593 FC               [12] 3613 	mov	r4,a
      006594 A3               [24] 3614 	inc	dptr
      006595 E0               [24] 3615 	movx	a,@dptr
      006596 FD               [12] 3616 	mov	r5,a
      006597 A3               [24] 3617 	inc	dptr
      006598 E0               [24] 3618 	movx	a,@dptr
      006599 FE               [12] 3619 	mov	r6,a
      00659A 90 03 F6         [24] 3620 	mov	dptr,#_UART_ParseConfigMessage_PARM_2
      00659D EF               [12] 3621 	mov	a,r7
      00659E F0               [24] 3622 	movx	@dptr,a
      00659F 8C 82            [24] 3623 	mov	dpl,r4
      0065A1 8D 83            [24] 3624 	mov	dph,r5
      0065A3 8E F0            [24] 3625 	mov	b,r6
                                   3626 ;	..\src\peripherals\UartComProc.c:240: break;
      0065A5 02 66 25         [24] 3627 	ljmp	_UART_ParseConfigMessage
                                   3628 ;	..\src\peripherals\UartComProc.c:242: case SET_DBG_MODE:
      0065A8                       3629 00102$:
                                   3630 ;	..\src\peripherals\UartComProc.c:243: UART_ParseSetDbgModeMessage(payload, PayloadLength);
      0065A8 90 03 F1         [24] 3631 	mov	dptr,#_UART_ParseMessage_PARM_2
      0065AB E0               [24] 3632 	movx	a,@dptr
      0065AC FC               [12] 3633 	mov	r4,a
      0065AD A3               [24] 3634 	inc	dptr
      0065AE E0               [24] 3635 	movx	a,@dptr
      0065AF FD               [12] 3636 	mov	r5,a
      0065B0 A3               [24] 3637 	inc	dptr
      0065B1 E0               [24] 3638 	movx	a,@dptr
      0065B2 FE               [12] 3639 	mov	r6,a
      0065B3 90 03 F7         [24] 3640 	mov	dptr,#_UART_ParseSetDbgModeMessage_PARM_2
      0065B6 EF               [12] 3641 	mov	a,r7
      0065B7 F0               [24] 3642 	movx	@dptr,a
      0065B8 8C 82            [24] 3643 	mov	dpl,r4
      0065BA 8D 83            [24] 3644 	mov	dph,r5
      0065BC 8E F0            [24] 3645 	mov	b,r6
                                   3646 ;	..\src\peripherals\UartComProc.c:244: break;
                                   3647 ;	..\src\peripherals\UartComProc.c:246: case TX_MESSAGE:
      0065BE 02 66 26         [24] 3648 	ljmp	_UART_ParseSetDbgModeMessage
      0065C1                       3649 00103$:
                                   3650 ;	..\src\peripherals\UartComProc.c:247: UART_ParseTxMessage(payload, PayloadLength);
      0065C1 90 03 F1         [24] 3651 	mov	dptr,#_UART_ParseMessage_PARM_2
      0065C4 E0               [24] 3652 	movx	a,@dptr
      0065C5 FC               [12] 3653 	mov	r4,a
      0065C6 A3               [24] 3654 	inc	dptr
      0065C7 E0               [24] 3655 	movx	a,@dptr
      0065C8 FD               [12] 3656 	mov	r5,a
      0065C9 A3               [24] 3657 	inc	dptr
      0065CA E0               [24] 3658 	movx	a,@dptr
      0065CB FE               [12] 3659 	mov	r6,a
      0065CC 90 03 F8         [24] 3660 	mov	dptr,#_UART_ParseTxMessage_PARM_2
      0065CF EF               [12] 3661 	mov	a,r7
      0065D0 F0               [24] 3662 	movx	@dptr,a
      0065D1 8C 82            [24] 3663 	mov	dpl,r4
      0065D3 8D 83            [24] 3664 	mov	dph,r5
      0065D5 8E F0            [24] 3665 	mov	b,r6
                                   3666 ;	..\src\peripherals\UartComProc.c:248: break;
                                   3667 ;	..\src\peripherals\UartComProc.c:250: case POWER_DOWN:
      0065D7 02 66 27         [24] 3668 	ljmp	_UART_ParseTxMessage
      0065DA                       3669 00104$:
                                   3670 ;	..\src\peripherals\UartComProc.c:251: UART_ParsePwrDwn(payload, PayloadLength);
      0065DA 90 03 F1         [24] 3671 	mov	dptr,#_UART_ParseMessage_PARM_2
      0065DD E0               [24] 3672 	movx	a,@dptr
      0065DE FC               [12] 3673 	mov	r4,a
      0065DF A3               [24] 3674 	inc	dptr
      0065E0 E0               [24] 3675 	movx	a,@dptr
      0065E1 FD               [12] 3676 	mov	r5,a
      0065E2 A3               [24] 3677 	inc	dptr
      0065E3 E0               [24] 3678 	movx	a,@dptr
      0065E4 FE               [12] 3679 	mov	r6,a
      0065E5 90 03 F9         [24] 3680 	mov	dptr,#_UART_ParsePwrDwn_PARM_2
      0065E8 EF               [12] 3681 	mov	a,r7
      0065E9 F0               [24] 3682 	movx	@dptr,a
      0065EA 8C 82            [24] 3683 	mov	dpl,r4
      0065EC 8D 83            [24] 3684 	mov	dph,r5
      0065EE 8E F0            [24] 3685 	mov	b,r6
                                   3686 ;	..\src\peripherals\UartComProc.c:252: break;
                                   3687 ;	..\src\peripherals\UartComProc.c:254: case GPS_DATA_REQ:
      0065F0 02 66 28         [24] 3688 	ljmp	_UART_ParsePwrDwn
      0065F3                       3689 00105$:
                                   3690 ;	..\src\peripherals\UartComProc.c:255: UART_ParseGpsDataReq(payload, PayloadLength);
      0065F3 90 03 F1         [24] 3691 	mov	dptr,#_UART_ParseMessage_PARM_2
      0065F6 E0               [24] 3692 	movx	a,@dptr
      0065F7 FC               [12] 3693 	mov	r4,a
      0065F8 A3               [24] 3694 	inc	dptr
      0065F9 E0               [24] 3695 	movx	a,@dptr
      0065FA FD               [12] 3696 	mov	r5,a
      0065FB A3               [24] 3697 	inc	dptr
      0065FC E0               [24] 3698 	movx	a,@dptr
      0065FD FE               [12] 3699 	mov	r6,a
      0065FE 90 03 FA         [24] 3700 	mov	dptr,#_UART_ParseGpsDataReq_PARM_2
      006601 EF               [12] 3701 	mov	a,r7
      006602 F0               [24] 3702 	movx	@dptr,a
      006603 8C 82            [24] 3703 	mov	dpl,r4
      006605 8D 83            [24] 3704 	mov	dph,r5
      006607 8E F0            [24] 3705 	mov	b,r6
                                   3706 ;	..\src\peripherals\UartComProc.c:256: break;
                                   3707 ;	..\src\peripherals\UartComProc.c:258: case ANSWER_MSG:
      006609 02 66 29         [24] 3708 	ljmp	_UART_ParseGpsDataReq
      00660C                       3709 00106$:
                                   3710 ;	..\src\peripherals\UartComProc.c:259: UART_ParseAnsMessage(payload, PayloadLength);
      00660C 90 03 F1         [24] 3711 	mov	dptr,#_UART_ParseMessage_PARM_2
      00660F E0               [24] 3712 	movx	a,@dptr
      006610 FC               [12] 3713 	mov	r4,a
      006611 A3               [24] 3714 	inc	dptr
      006612 E0               [24] 3715 	movx	a,@dptr
      006613 FD               [12] 3716 	mov	r5,a
      006614 A3               [24] 3717 	inc	dptr
      006615 E0               [24] 3718 	movx	a,@dptr
      006616 FE               [12] 3719 	mov	r6,a
      006617 90 03 FB         [24] 3720 	mov	dptr,#_UART_ParseAnsMessage_PARM_2
      00661A EF               [12] 3721 	mov	a,r7
      00661B F0               [24] 3722 	movx	@dptr,a
      00661C 8C 82            [24] 3723 	mov	dpl,r4
      00661E 8D 83            [24] 3724 	mov	dph,r5
      006620 8E F0            [24] 3725 	mov	b,r6
                                   3726 ;	..\src\peripherals\UartComProc.c:265: }
                                   3727 ;	..\src\peripherals\UartComProc.c:266: }
      006622 02 66 2A         [24] 3728 	ljmp	_UART_ParseAnsMessage
                                   3729 ;------------------------------------------------------------
                                   3730 ;Allocation info for local variables in function 'UART_ParseConfigMessage'
                                   3731 ;------------------------------------------------------------
                                   3732 ;PayloadLength             Allocated with name '_UART_ParseConfigMessage_PARM_2'
                                   3733 ;payload                   Allocated with name '_UART_ParseConfigMessage_payload_65536_289'
                                   3734 ;RetVal                    Allocated with name '_UART_ParseConfigMessage_RetVal_65536_290'
                                   3735 ;------------------------------------------------------------
                                   3736 ;	..\src\peripherals\UartComProc.c:277: uint8_t UART_ParseConfigMessage(uint8_t *payload, uint8_t PayloadLength)
                                   3737 ;	-----------------------------------------
                                   3738 ;	 function UART_ParseConfigMessage
                                   3739 ;	-----------------------------------------
      006625                       3740 _UART_ParseConfigMessage:
                                   3741 ;	..\src\peripherals\UartComProc.c:280: uint8_t RetVal = SUCCESFULL ;
                                   3742 ;	..\src\peripherals\UartComProc.c:282: }
      006625 22               [24] 3743 	ret
                                   3744 ;------------------------------------------------------------
                                   3745 ;Allocation info for local variables in function 'UART_ParseSetDbgModeMessage'
                                   3746 ;------------------------------------------------------------
                                   3747 ;PayloadLength             Allocated with name '_UART_ParseSetDbgModeMessage_PARM_2'
                                   3748 ;payload                   Allocated with name '_UART_ParseSetDbgModeMessage_payload_65536_291'
                                   3749 ;RetVal                    Allocated with name '_UART_ParseSetDbgModeMessage_RetVal_65536_292'
                                   3750 ;------------------------------------------------------------
                                   3751 ;	..\src\peripherals\UartComProc.c:293: uint8_t UART_ParseSetDbgModeMessage(uint8_t *payload, uint8_t PayloadLength)
                                   3752 ;	-----------------------------------------
                                   3753 ;	 function UART_ParseSetDbgModeMessage
                                   3754 ;	-----------------------------------------
      006626                       3755 _UART_ParseSetDbgModeMessage:
                                   3756 ;	..\src\peripherals\UartComProc.c:295: uint8_t RetVal = SUCCESFULL;
                                   3757 ;	..\src\peripherals\UartComProc.c:297: }
      006626 22               [24] 3758 	ret
                                   3759 ;------------------------------------------------------------
                                   3760 ;Allocation info for local variables in function 'UART_ParseTxMessage'
                                   3761 ;------------------------------------------------------------
                                   3762 ;PayloadLength             Allocated with name '_UART_ParseTxMessage_PARM_2'
                                   3763 ;payload                   Allocated with name '_UART_ParseTxMessage_payload_65536_293'
                                   3764 ;RetVal                    Allocated with name '_UART_ParseTxMessage_RetVal_65536_294'
                                   3765 ;------------------------------------------------------------
                                   3766 ;	..\src\peripherals\UartComProc.c:308: uint8_t UART_ParseTxMessage(uint8_t *payload, uint8_t PayloadLength)
                                   3767 ;	-----------------------------------------
                                   3768 ;	 function UART_ParseTxMessage
                                   3769 ;	-----------------------------------------
      006627                       3770 _UART_ParseTxMessage:
                                   3771 ;	..\src\peripherals\UartComProc.c:310: uint8_t RetVal = SUCCESFULL;
                                   3772 ;	..\src\peripherals\UartComProc.c:312: }
      006627 22               [24] 3773 	ret
                                   3774 ;------------------------------------------------------------
                                   3775 ;Allocation info for local variables in function 'UART_ParsePwrDwn'
                                   3776 ;------------------------------------------------------------
                                   3777 ;PayloadLength             Allocated with name '_UART_ParsePwrDwn_PARM_2'
                                   3778 ;payload                   Allocated with name '_UART_ParsePwrDwn_payload_65536_295'
                                   3779 ;RetVal                    Allocated with name '_UART_ParsePwrDwn_RetVal_65536_296'
                                   3780 ;------------------------------------------------------------
                                   3781 ;	..\src\peripherals\UartComProc.c:324: uint8_t UART_ParsePwrDwn(uint8_t *payload, uint8_t PayloadLength)
                                   3782 ;	-----------------------------------------
                                   3783 ;	 function UART_ParsePwrDwn
                                   3784 ;	-----------------------------------------
      006628                       3785 _UART_ParsePwrDwn:
                                   3786 ;	..\src\peripherals\UartComProc.c:326: uint8_t RetVal = SUCCESFULL;
                                   3787 ;	..\src\peripherals\UartComProc.c:328: }
      006628 22               [24] 3788 	ret
                                   3789 ;------------------------------------------------------------
                                   3790 ;Allocation info for local variables in function 'UART_ParseGpsDataReq'
                                   3791 ;------------------------------------------------------------
                                   3792 ;PayloadLength             Allocated with name '_UART_ParseGpsDataReq_PARM_2'
                                   3793 ;payload                   Allocated with name '_UART_ParseGpsDataReq_payload_65536_297'
                                   3794 ;RetVal                    Allocated with name '_UART_ParseGpsDataReq_RetVal_65536_298'
                                   3795 ;------------------------------------------------------------
                                   3796 ;	..\src\peripherals\UartComProc.c:338: uint8_t UART_ParseGpsDataReq(uint8_t *payload, uint8_t PayloadLength)
                                   3797 ;	-----------------------------------------
                                   3798 ;	 function UART_ParseGpsDataReq
                                   3799 ;	-----------------------------------------
      006629                       3800 _UART_ParseGpsDataReq:
                                   3801 ;	..\src\peripherals\UartComProc.c:340: uint8_t RetVal = SUCCESFULL;
                                   3802 ;	..\src\peripherals\UartComProc.c:342: }
      006629 22               [24] 3803 	ret
                                   3804 ;------------------------------------------------------------
                                   3805 ;Allocation info for local variables in function 'UART_ParseAnsMessage'
                                   3806 ;------------------------------------------------------------
                                   3807 ;PayloadLength             Allocated with name '_UART_ParseAnsMessage_PARM_2'
                                   3808 ;payload                   Allocated with name '_UART_ParseAnsMessage_payload_65536_299'
                                   3809 ;RetVal                    Allocated with name '_UART_ParseAnsMessage_RetVal_65536_300'
                                   3810 ;------------------------------------------------------------
                                   3811 ;	..\src\peripherals\UartComProc.c:353: uint8_t UART_ParseAnsMessage(uint8_t *payload, uint8_t PayloadLength)
                                   3812 ;	-----------------------------------------
                                   3813 ;	 function UART_ParseAnsMessage
                                   3814 ;	-----------------------------------------
      00662A                       3815 _UART_ParseAnsMessage:
                                   3816 ;	..\src\peripherals\UartComProc.c:355: uint8_t RetVal = SUCCESFULL;
                                   3817 ;	..\src\peripherals\UartComProc.c:357: }
      00662A 22               [24] 3818 	ret
                                   3819 	.area CSEG    (CODE)
                                   3820 	.area CONST   (CODE)
                                   3821 	.area XINIT   (CODE)
                                   3822 	.area CABS    (ABS,CODE)
