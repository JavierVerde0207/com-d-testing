                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.9.3 #11354 (MINGW32)
                                      4 ;--------------------------------------------------------
                                      5 	.module GoldSequence
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _EraseMemoryPage
                                     12 	.globl _WriteMemory
                                     13 	.globl _flash_read
                                     14 	.globl _flash_lock
                                     15 	.globl _flash_unlock
                                     16 	.globl _PORTC_7
                                     17 	.globl _PORTC_6
                                     18 	.globl _PORTC_5
                                     19 	.globl _PORTC_4
                                     20 	.globl _PORTC_3
                                     21 	.globl _PORTC_2
                                     22 	.globl _PORTC_1
                                     23 	.globl _PORTC_0
                                     24 	.globl _PORTB_7
                                     25 	.globl _PORTB_6
                                     26 	.globl _PORTB_5
                                     27 	.globl _PORTB_4
                                     28 	.globl _PORTB_3
                                     29 	.globl _PORTB_2
                                     30 	.globl _PORTB_1
                                     31 	.globl _PORTB_0
                                     32 	.globl _PORTA_7
                                     33 	.globl _PORTA_6
                                     34 	.globl _PORTA_5
                                     35 	.globl _PORTA_4
                                     36 	.globl _PORTA_3
                                     37 	.globl _PORTA_2
                                     38 	.globl _PORTA_1
                                     39 	.globl _PORTA_0
                                     40 	.globl _PINC_7
                                     41 	.globl _PINC_6
                                     42 	.globl _PINC_5
                                     43 	.globl _PINC_4
                                     44 	.globl _PINC_3
                                     45 	.globl _PINC_2
                                     46 	.globl _PINC_1
                                     47 	.globl _PINC_0
                                     48 	.globl _PINB_7
                                     49 	.globl _PINB_6
                                     50 	.globl _PINB_5
                                     51 	.globl _PINB_4
                                     52 	.globl _PINB_3
                                     53 	.globl _PINB_2
                                     54 	.globl _PINB_1
                                     55 	.globl _PINB_0
                                     56 	.globl _PINA_7
                                     57 	.globl _PINA_6
                                     58 	.globl _PINA_5
                                     59 	.globl _PINA_4
                                     60 	.globl _PINA_3
                                     61 	.globl _PINA_2
                                     62 	.globl _PINA_1
                                     63 	.globl _PINA_0
                                     64 	.globl _CY
                                     65 	.globl _AC
                                     66 	.globl _F0
                                     67 	.globl _RS1
                                     68 	.globl _RS0
                                     69 	.globl _OV
                                     70 	.globl _F1
                                     71 	.globl _P
                                     72 	.globl _IP_7
                                     73 	.globl _IP_6
                                     74 	.globl _IP_5
                                     75 	.globl _IP_4
                                     76 	.globl _IP_3
                                     77 	.globl _IP_2
                                     78 	.globl _IP_1
                                     79 	.globl _IP_0
                                     80 	.globl _EA
                                     81 	.globl _IE_7
                                     82 	.globl _IE_6
                                     83 	.globl _IE_5
                                     84 	.globl _IE_4
                                     85 	.globl _IE_3
                                     86 	.globl _IE_2
                                     87 	.globl _IE_1
                                     88 	.globl _IE_0
                                     89 	.globl _EIP_7
                                     90 	.globl _EIP_6
                                     91 	.globl _EIP_5
                                     92 	.globl _EIP_4
                                     93 	.globl _EIP_3
                                     94 	.globl _EIP_2
                                     95 	.globl _EIP_1
                                     96 	.globl _EIP_0
                                     97 	.globl _EIE_7
                                     98 	.globl _EIE_6
                                     99 	.globl _EIE_5
                                    100 	.globl _EIE_4
                                    101 	.globl _EIE_3
                                    102 	.globl _EIE_2
                                    103 	.globl _EIE_1
                                    104 	.globl _EIE_0
                                    105 	.globl _E2IP_7
                                    106 	.globl _E2IP_6
                                    107 	.globl _E2IP_5
                                    108 	.globl _E2IP_4
                                    109 	.globl _E2IP_3
                                    110 	.globl _E2IP_2
                                    111 	.globl _E2IP_1
                                    112 	.globl _E2IP_0
                                    113 	.globl _E2IE_7
                                    114 	.globl _E2IE_6
                                    115 	.globl _E2IE_5
                                    116 	.globl _E2IE_4
                                    117 	.globl _E2IE_3
                                    118 	.globl _E2IE_2
                                    119 	.globl _E2IE_1
                                    120 	.globl _E2IE_0
                                    121 	.globl _B_7
                                    122 	.globl _B_6
                                    123 	.globl _B_5
                                    124 	.globl _B_4
                                    125 	.globl _B_3
                                    126 	.globl _B_2
                                    127 	.globl _B_1
                                    128 	.globl _B_0
                                    129 	.globl _ACC_7
                                    130 	.globl _ACC_6
                                    131 	.globl _ACC_5
                                    132 	.globl _ACC_4
                                    133 	.globl _ACC_3
                                    134 	.globl _ACC_2
                                    135 	.globl _ACC_1
                                    136 	.globl _ACC_0
                                    137 	.globl _WTSTAT
                                    138 	.globl _WTIRQEN
                                    139 	.globl _WTEVTD
                                    140 	.globl _WTEVTD1
                                    141 	.globl _WTEVTD0
                                    142 	.globl _WTEVTC
                                    143 	.globl _WTEVTC1
                                    144 	.globl _WTEVTC0
                                    145 	.globl _WTEVTB
                                    146 	.globl _WTEVTB1
                                    147 	.globl _WTEVTB0
                                    148 	.globl _WTEVTA
                                    149 	.globl _WTEVTA1
                                    150 	.globl _WTEVTA0
                                    151 	.globl _WTCNTR1
                                    152 	.globl _WTCNTB
                                    153 	.globl _WTCNTB1
                                    154 	.globl _WTCNTB0
                                    155 	.globl _WTCNTA
                                    156 	.globl _WTCNTA1
                                    157 	.globl _WTCNTA0
                                    158 	.globl _WTCFGB
                                    159 	.globl _WTCFGA
                                    160 	.globl _WDTRESET
                                    161 	.globl _WDTCFG
                                    162 	.globl _U1STATUS
                                    163 	.globl _U1SHREG
                                    164 	.globl _U1MODE
                                    165 	.globl _U1CTRL
                                    166 	.globl _U0STATUS
                                    167 	.globl _U0SHREG
                                    168 	.globl _U0MODE
                                    169 	.globl _U0CTRL
                                    170 	.globl _T2STATUS
                                    171 	.globl _T2PERIOD
                                    172 	.globl _T2PERIOD1
                                    173 	.globl _T2PERIOD0
                                    174 	.globl _T2MODE
                                    175 	.globl _T2CNT
                                    176 	.globl _T2CNT1
                                    177 	.globl _T2CNT0
                                    178 	.globl _T2CLKSRC
                                    179 	.globl _T1STATUS
                                    180 	.globl _T1PERIOD
                                    181 	.globl _T1PERIOD1
                                    182 	.globl _T1PERIOD0
                                    183 	.globl _T1MODE
                                    184 	.globl _T1CNT
                                    185 	.globl _T1CNT1
                                    186 	.globl _T1CNT0
                                    187 	.globl _T1CLKSRC
                                    188 	.globl _T0STATUS
                                    189 	.globl _T0PERIOD
                                    190 	.globl _T0PERIOD1
                                    191 	.globl _T0PERIOD0
                                    192 	.globl _T0MODE
                                    193 	.globl _T0CNT
                                    194 	.globl _T0CNT1
                                    195 	.globl _T0CNT0
                                    196 	.globl _T0CLKSRC
                                    197 	.globl _SPSTATUS
                                    198 	.globl _SPSHREG
                                    199 	.globl _SPMODE
                                    200 	.globl _SPCLKSRC
                                    201 	.globl _RADIOSTAT
                                    202 	.globl _RADIOSTAT1
                                    203 	.globl _RADIOSTAT0
                                    204 	.globl _RADIODATA
                                    205 	.globl _RADIODATA3
                                    206 	.globl _RADIODATA2
                                    207 	.globl _RADIODATA1
                                    208 	.globl _RADIODATA0
                                    209 	.globl _RADIOADDR
                                    210 	.globl _RADIOADDR1
                                    211 	.globl _RADIOADDR0
                                    212 	.globl _RADIOACC
                                    213 	.globl _OC1STATUS
                                    214 	.globl _OC1PIN
                                    215 	.globl _OC1MODE
                                    216 	.globl _OC1COMP
                                    217 	.globl _OC1COMP1
                                    218 	.globl _OC1COMP0
                                    219 	.globl _OC0STATUS
                                    220 	.globl _OC0PIN
                                    221 	.globl _OC0MODE
                                    222 	.globl _OC0COMP
                                    223 	.globl _OC0COMP1
                                    224 	.globl _OC0COMP0
                                    225 	.globl _NVSTATUS
                                    226 	.globl _NVKEY
                                    227 	.globl _NVDATA
                                    228 	.globl _NVDATA1
                                    229 	.globl _NVDATA0
                                    230 	.globl _NVADDR
                                    231 	.globl _NVADDR1
                                    232 	.globl _NVADDR0
                                    233 	.globl _IC1STATUS
                                    234 	.globl _IC1MODE
                                    235 	.globl _IC1CAPT
                                    236 	.globl _IC1CAPT1
                                    237 	.globl _IC1CAPT0
                                    238 	.globl _IC0STATUS
                                    239 	.globl _IC0MODE
                                    240 	.globl _IC0CAPT
                                    241 	.globl _IC0CAPT1
                                    242 	.globl _IC0CAPT0
                                    243 	.globl _PORTR
                                    244 	.globl _PORTC
                                    245 	.globl _PORTB
                                    246 	.globl _PORTA
                                    247 	.globl _PINR
                                    248 	.globl _PINC
                                    249 	.globl _PINB
                                    250 	.globl _PINA
                                    251 	.globl _DIRR
                                    252 	.globl _DIRC
                                    253 	.globl _DIRB
                                    254 	.globl _DIRA
                                    255 	.globl _DBGLNKSTAT
                                    256 	.globl _DBGLNKBUF
                                    257 	.globl _CODECONFIG
                                    258 	.globl _CLKSTAT
                                    259 	.globl _CLKCON
                                    260 	.globl _ANALOGCOMP
                                    261 	.globl _ADCCONV
                                    262 	.globl _ADCCLKSRC
                                    263 	.globl _ADCCH3CONFIG
                                    264 	.globl _ADCCH2CONFIG
                                    265 	.globl _ADCCH1CONFIG
                                    266 	.globl _ADCCH0CONFIG
                                    267 	.globl __XPAGE
                                    268 	.globl _XPAGE
                                    269 	.globl _SP
                                    270 	.globl _PSW
                                    271 	.globl _PCON
                                    272 	.globl _IP
                                    273 	.globl _IE
                                    274 	.globl _EIP
                                    275 	.globl _EIE
                                    276 	.globl _E2IP
                                    277 	.globl _E2IE
                                    278 	.globl _DPS
                                    279 	.globl _DPTR1
                                    280 	.globl _DPTR0
                                    281 	.globl _DPL1
                                    282 	.globl _DPL
                                    283 	.globl _DPH1
                                    284 	.globl _DPH
                                    285 	.globl _B
                                    286 	.globl _ACC
                                    287 	.globl _RNGCLKSRC1
                                    288 	.globl _RNGCLKSRC0
                                    289 	.globl _RNGMODE
                                    290 	.globl _AESOUTADDR
                                    291 	.globl _AESOUTADDR1
                                    292 	.globl _AESOUTADDR0
                                    293 	.globl _AESMODE
                                    294 	.globl _AESKEYADDR
                                    295 	.globl _AESKEYADDR1
                                    296 	.globl _AESKEYADDR0
                                    297 	.globl _AESINADDR
                                    298 	.globl _AESINADDR1
                                    299 	.globl _AESINADDR0
                                    300 	.globl _AESCURBLOCK
                                    301 	.globl _AESCONFIG
                                    302 	.globl _RNGBYTE
                                    303 	.globl _XTALREADY
                                    304 	.globl _XTALOSC
                                    305 	.globl _XTALAMPL
                                    306 	.globl _SILICONREV
                                    307 	.globl _SCRATCH3
                                    308 	.globl _SCRATCH2
                                    309 	.globl _SCRATCH1
                                    310 	.globl _SCRATCH0
                                    311 	.globl _RADIOMUX
                                    312 	.globl _RADIOFSTATADDR
                                    313 	.globl _RADIOFSTATADDR1
                                    314 	.globl _RADIOFSTATADDR0
                                    315 	.globl _RADIOFDATAADDR
                                    316 	.globl _RADIOFDATAADDR1
                                    317 	.globl _RADIOFDATAADDR0
                                    318 	.globl _OSCRUN
                                    319 	.globl _OSCREADY
                                    320 	.globl _OSCFORCERUN
                                    321 	.globl _OSCCALIB
                                    322 	.globl _MISCCTRL
                                    323 	.globl _LPXOSCGM
                                    324 	.globl _LPOSCREF
                                    325 	.globl _LPOSCREF1
                                    326 	.globl _LPOSCREF0
                                    327 	.globl _LPOSCPER
                                    328 	.globl _LPOSCPER1
                                    329 	.globl _LPOSCPER0
                                    330 	.globl _LPOSCKFILT
                                    331 	.globl _LPOSCKFILT1
                                    332 	.globl _LPOSCKFILT0
                                    333 	.globl _LPOSCFREQ
                                    334 	.globl _LPOSCFREQ1
                                    335 	.globl _LPOSCFREQ0
                                    336 	.globl _LPOSCCONFIG
                                    337 	.globl _PINSEL
                                    338 	.globl _PINCHGC
                                    339 	.globl _PINCHGB
                                    340 	.globl _PINCHGA
                                    341 	.globl _PALTRADIO
                                    342 	.globl _PALTC
                                    343 	.globl _PALTB
                                    344 	.globl _PALTA
                                    345 	.globl _INTCHGC
                                    346 	.globl _INTCHGB
                                    347 	.globl _INTCHGA
                                    348 	.globl _EXTIRQ
                                    349 	.globl _GPIOENABLE
                                    350 	.globl _ANALOGA
                                    351 	.globl _FRCOSCREF
                                    352 	.globl _FRCOSCREF1
                                    353 	.globl _FRCOSCREF0
                                    354 	.globl _FRCOSCPER
                                    355 	.globl _FRCOSCPER1
                                    356 	.globl _FRCOSCPER0
                                    357 	.globl _FRCOSCKFILT
                                    358 	.globl _FRCOSCKFILT1
                                    359 	.globl _FRCOSCKFILT0
                                    360 	.globl _FRCOSCFREQ
                                    361 	.globl _FRCOSCFREQ1
                                    362 	.globl _FRCOSCFREQ0
                                    363 	.globl _FRCOSCCTRL
                                    364 	.globl _FRCOSCCONFIG
                                    365 	.globl _DMA1CONFIG
                                    366 	.globl _DMA1ADDR
                                    367 	.globl _DMA1ADDR1
                                    368 	.globl _DMA1ADDR0
                                    369 	.globl _DMA0CONFIG
                                    370 	.globl _DMA0ADDR
                                    371 	.globl _DMA0ADDR1
                                    372 	.globl _DMA0ADDR0
                                    373 	.globl _ADCTUNE2
                                    374 	.globl _ADCTUNE1
                                    375 	.globl _ADCTUNE0
                                    376 	.globl _ADCCH3VAL
                                    377 	.globl _ADCCH3VAL1
                                    378 	.globl _ADCCH3VAL0
                                    379 	.globl _ADCCH2VAL
                                    380 	.globl _ADCCH2VAL1
                                    381 	.globl _ADCCH2VAL0
                                    382 	.globl _ADCCH1VAL
                                    383 	.globl _ADCCH1VAL1
                                    384 	.globl _ADCCH1VAL0
                                    385 	.globl _ADCCH0VAL
                                    386 	.globl _ADCCH0VAL1
                                    387 	.globl _ADCCH0VAL0
                                    388 	.globl _GOLDSEQUENCE_Init
                                    389 	.globl _GOLDSEQUENCE_Obtain
                                    390 ;--------------------------------------------------------
                                    391 ; special function registers
                                    392 ;--------------------------------------------------------
                                    393 	.area RSEG    (ABS,DATA)
      000000                        394 	.org 0x0000
                           0000E0   395 _ACC	=	0x00e0
                           0000F0   396 _B	=	0x00f0
                           000083   397 _DPH	=	0x0083
                           000085   398 _DPH1	=	0x0085
                           000082   399 _DPL	=	0x0082
                           000084   400 _DPL1	=	0x0084
                           008382   401 _DPTR0	=	0x8382
                           008584   402 _DPTR1	=	0x8584
                           000086   403 _DPS	=	0x0086
                           0000A0   404 _E2IE	=	0x00a0
                           0000C0   405 _E2IP	=	0x00c0
                           000098   406 _EIE	=	0x0098
                           0000B0   407 _EIP	=	0x00b0
                           0000A8   408 _IE	=	0x00a8
                           0000B8   409 _IP	=	0x00b8
                           000087   410 _PCON	=	0x0087
                           0000D0   411 _PSW	=	0x00d0
                           000081   412 _SP	=	0x0081
                           0000D9   413 _XPAGE	=	0x00d9
                           0000D9   414 __XPAGE	=	0x00d9
                           0000CA   415 _ADCCH0CONFIG	=	0x00ca
                           0000CB   416 _ADCCH1CONFIG	=	0x00cb
                           0000D2   417 _ADCCH2CONFIG	=	0x00d2
                           0000D3   418 _ADCCH3CONFIG	=	0x00d3
                           0000D1   419 _ADCCLKSRC	=	0x00d1
                           0000C9   420 _ADCCONV	=	0x00c9
                           0000E1   421 _ANALOGCOMP	=	0x00e1
                           0000C6   422 _CLKCON	=	0x00c6
                           0000C7   423 _CLKSTAT	=	0x00c7
                           000097   424 _CODECONFIG	=	0x0097
                           0000E3   425 _DBGLNKBUF	=	0x00e3
                           0000E2   426 _DBGLNKSTAT	=	0x00e2
                           000089   427 _DIRA	=	0x0089
                           00008A   428 _DIRB	=	0x008a
                           00008B   429 _DIRC	=	0x008b
                           00008E   430 _DIRR	=	0x008e
                           0000C8   431 _PINA	=	0x00c8
                           0000E8   432 _PINB	=	0x00e8
                           0000F8   433 _PINC	=	0x00f8
                           00008D   434 _PINR	=	0x008d
                           000080   435 _PORTA	=	0x0080
                           000088   436 _PORTB	=	0x0088
                           000090   437 _PORTC	=	0x0090
                           00008C   438 _PORTR	=	0x008c
                           0000CE   439 _IC0CAPT0	=	0x00ce
                           0000CF   440 _IC0CAPT1	=	0x00cf
                           00CFCE   441 _IC0CAPT	=	0xcfce
                           0000CC   442 _IC0MODE	=	0x00cc
                           0000CD   443 _IC0STATUS	=	0x00cd
                           0000D6   444 _IC1CAPT0	=	0x00d6
                           0000D7   445 _IC1CAPT1	=	0x00d7
                           00D7D6   446 _IC1CAPT	=	0xd7d6
                           0000D4   447 _IC1MODE	=	0x00d4
                           0000D5   448 _IC1STATUS	=	0x00d5
                           000092   449 _NVADDR0	=	0x0092
                           000093   450 _NVADDR1	=	0x0093
                           009392   451 _NVADDR	=	0x9392
                           000094   452 _NVDATA0	=	0x0094
                           000095   453 _NVDATA1	=	0x0095
                           009594   454 _NVDATA	=	0x9594
                           000096   455 _NVKEY	=	0x0096
                           000091   456 _NVSTATUS	=	0x0091
                           0000BC   457 _OC0COMP0	=	0x00bc
                           0000BD   458 _OC0COMP1	=	0x00bd
                           00BDBC   459 _OC0COMP	=	0xbdbc
                           0000B9   460 _OC0MODE	=	0x00b9
                           0000BA   461 _OC0PIN	=	0x00ba
                           0000BB   462 _OC0STATUS	=	0x00bb
                           0000C4   463 _OC1COMP0	=	0x00c4
                           0000C5   464 _OC1COMP1	=	0x00c5
                           00C5C4   465 _OC1COMP	=	0xc5c4
                           0000C1   466 _OC1MODE	=	0x00c1
                           0000C2   467 _OC1PIN	=	0x00c2
                           0000C3   468 _OC1STATUS	=	0x00c3
                           0000B1   469 _RADIOACC	=	0x00b1
                           0000B3   470 _RADIOADDR0	=	0x00b3
                           0000B2   471 _RADIOADDR1	=	0x00b2
                           00B2B3   472 _RADIOADDR	=	0xb2b3
                           0000B7   473 _RADIODATA0	=	0x00b7
                           0000B6   474 _RADIODATA1	=	0x00b6
                           0000B5   475 _RADIODATA2	=	0x00b5
                           0000B4   476 _RADIODATA3	=	0x00b4
                           B4B5B6B7   477 _RADIODATA	=	0xb4b5b6b7
                           0000BE   478 _RADIOSTAT0	=	0x00be
                           0000BF   479 _RADIOSTAT1	=	0x00bf
                           00BFBE   480 _RADIOSTAT	=	0xbfbe
                           0000DF   481 _SPCLKSRC	=	0x00df
                           0000DC   482 _SPMODE	=	0x00dc
                           0000DE   483 _SPSHREG	=	0x00de
                           0000DD   484 _SPSTATUS	=	0x00dd
                           00009A   485 _T0CLKSRC	=	0x009a
                           00009C   486 _T0CNT0	=	0x009c
                           00009D   487 _T0CNT1	=	0x009d
                           009D9C   488 _T0CNT	=	0x9d9c
                           000099   489 _T0MODE	=	0x0099
                           00009E   490 _T0PERIOD0	=	0x009e
                           00009F   491 _T0PERIOD1	=	0x009f
                           009F9E   492 _T0PERIOD	=	0x9f9e
                           00009B   493 _T0STATUS	=	0x009b
                           0000A2   494 _T1CLKSRC	=	0x00a2
                           0000A4   495 _T1CNT0	=	0x00a4
                           0000A5   496 _T1CNT1	=	0x00a5
                           00A5A4   497 _T1CNT	=	0xa5a4
                           0000A1   498 _T1MODE	=	0x00a1
                           0000A6   499 _T1PERIOD0	=	0x00a6
                           0000A7   500 _T1PERIOD1	=	0x00a7
                           00A7A6   501 _T1PERIOD	=	0xa7a6
                           0000A3   502 _T1STATUS	=	0x00a3
                           0000AA   503 _T2CLKSRC	=	0x00aa
                           0000AC   504 _T2CNT0	=	0x00ac
                           0000AD   505 _T2CNT1	=	0x00ad
                           00ADAC   506 _T2CNT	=	0xadac
                           0000A9   507 _T2MODE	=	0x00a9
                           0000AE   508 _T2PERIOD0	=	0x00ae
                           0000AF   509 _T2PERIOD1	=	0x00af
                           00AFAE   510 _T2PERIOD	=	0xafae
                           0000AB   511 _T2STATUS	=	0x00ab
                           0000E4   512 _U0CTRL	=	0x00e4
                           0000E7   513 _U0MODE	=	0x00e7
                           0000E6   514 _U0SHREG	=	0x00e6
                           0000E5   515 _U0STATUS	=	0x00e5
                           0000EC   516 _U1CTRL	=	0x00ec
                           0000EF   517 _U1MODE	=	0x00ef
                           0000EE   518 _U1SHREG	=	0x00ee
                           0000ED   519 _U1STATUS	=	0x00ed
                           0000DA   520 _WDTCFG	=	0x00da
                           0000DB   521 _WDTRESET	=	0x00db
                           0000F1   522 _WTCFGA	=	0x00f1
                           0000F9   523 _WTCFGB	=	0x00f9
                           0000F2   524 _WTCNTA0	=	0x00f2
                           0000F3   525 _WTCNTA1	=	0x00f3
                           00F3F2   526 _WTCNTA	=	0xf3f2
                           0000FA   527 _WTCNTB0	=	0x00fa
                           0000FB   528 _WTCNTB1	=	0x00fb
                           00FBFA   529 _WTCNTB	=	0xfbfa
                           0000EB   530 _WTCNTR1	=	0x00eb
                           0000F4   531 _WTEVTA0	=	0x00f4
                           0000F5   532 _WTEVTA1	=	0x00f5
                           00F5F4   533 _WTEVTA	=	0xf5f4
                           0000F6   534 _WTEVTB0	=	0x00f6
                           0000F7   535 _WTEVTB1	=	0x00f7
                           00F7F6   536 _WTEVTB	=	0xf7f6
                           0000FC   537 _WTEVTC0	=	0x00fc
                           0000FD   538 _WTEVTC1	=	0x00fd
                           00FDFC   539 _WTEVTC	=	0xfdfc
                           0000FE   540 _WTEVTD0	=	0x00fe
                           0000FF   541 _WTEVTD1	=	0x00ff
                           00FFFE   542 _WTEVTD	=	0xfffe
                           0000E9   543 _WTIRQEN	=	0x00e9
                           0000EA   544 _WTSTAT	=	0x00ea
                                    545 ;--------------------------------------------------------
                                    546 ; special function bits
                                    547 ;--------------------------------------------------------
                                    548 	.area RSEG    (ABS,DATA)
      000000                        549 	.org 0x0000
                           0000E0   550 _ACC_0	=	0x00e0
                           0000E1   551 _ACC_1	=	0x00e1
                           0000E2   552 _ACC_2	=	0x00e2
                           0000E3   553 _ACC_3	=	0x00e3
                           0000E4   554 _ACC_4	=	0x00e4
                           0000E5   555 _ACC_5	=	0x00e5
                           0000E6   556 _ACC_6	=	0x00e6
                           0000E7   557 _ACC_7	=	0x00e7
                           0000F0   558 _B_0	=	0x00f0
                           0000F1   559 _B_1	=	0x00f1
                           0000F2   560 _B_2	=	0x00f2
                           0000F3   561 _B_3	=	0x00f3
                           0000F4   562 _B_4	=	0x00f4
                           0000F5   563 _B_5	=	0x00f5
                           0000F6   564 _B_6	=	0x00f6
                           0000F7   565 _B_7	=	0x00f7
                           0000A0   566 _E2IE_0	=	0x00a0
                           0000A1   567 _E2IE_1	=	0x00a1
                           0000A2   568 _E2IE_2	=	0x00a2
                           0000A3   569 _E2IE_3	=	0x00a3
                           0000A4   570 _E2IE_4	=	0x00a4
                           0000A5   571 _E2IE_5	=	0x00a5
                           0000A6   572 _E2IE_6	=	0x00a6
                           0000A7   573 _E2IE_7	=	0x00a7
                           0000C0   574 _E2IP_0	=	0x00c0
                           0000C1   575 _E2IP_1	=	0x00c1
                           0000C2   576 _E2IP_2	=	0x00c2
                           0000C3   577 _E2IP_3	=	0x00c3
                           0000C4   578 _E2IP_4	=	0x00c4
                           0000C5   579 _E2IP_5	=	0x00c5
                           0000C6   580 _E2IP_6	=	0x00c6
                           0000C7   581 _E2IP_7	=	0x00c7
                           000098   582 _EIE_0	=	0x0098
                           000099   583 _EIE_1	=	0x0099
                           00009A   584 _EIE_2	=	0x009a
                           00009B   585 _EIE_3	=	0x009b
                           00009C   586 _EIE_4	=	0x009c
                           00009D   587 _EIE_5	=	0x009d
                           00009E   588 _EIE_6	=	0x009e
                           00009F   589 _EIE_7	=	0x009f
                           0000B0   590 _EIP_0	=	0x00b0
                           0000B1   591 _EIP_1	=	0x00b1
                           0000B2   592 _EIP_2	=	0x00b2
                           0000B3   593 _EIP_3	=	0x00b3
                           0000B4   594 _EIP_4	=	0x00b4
                           0000B5   595 _EIP_5	=	0x00b5
                           0000B6   596 _EIP_6	=	0x00b6
                           0000B7   597 _EIP_7	=	0x00b7
                           0000A8   598 _IE_0	=	0x00a8
                           0000A9   599 _IE_1	=	0x00a9
                           0000AA   600 _IE_2	=	0x00aa
                           0000AB   601 _IE_3	=	0x00ab
                           0000AC   602 _IE_4	=	0x00ac
                           0000AD   603 _IE_5	=	0x00ad
                           0000AE   604 _IE_6	=	0x00ae
                           0000AF   605 _IE_7	=	0x00af
                           0000AF   606 _EA	=	0x00af
                           0000B8   607 _IP_0	=	0x00b8
                           0000B9   608 _IP_1	=	0x00b9
                           0000BA   609 _IP_2	=	0x00ba
                           0000BB   610 _IP_3	=	0x00bb
                           0000BC   611 _IP_4	=	0x00bc
                           0000BD   612 _IP_5	=	0x00bd
                           0000BE   613 _IP_6	=	0x00be
                           0000BF   614 _IP_7	=	0x00bf
                           0000D0   615 _P	=	0x00d0
                           0000D1   616 _F1	=	0x00d1
                           0000D2   617 _OV	=	0x00d2
                           0000D3   618 _RS0	=	0x00d3
                           0000D4   619 _RS1	=	0x00d4
                           0000D5   620 _F0	=	0x00d5
                           0000D6   621 _AC	=	0x00d6
                           0000D7   622 _CY	=	0x00d7
                           0000C8   623 _PINA_0	=	0x00c8
                           0000C9   624 _PINA_1	=	0x00c9
                           0000CA   625 _PINA_2	=	0x00ca
                           0000CB   626 _PINA_3	=	0x00cb
                           0000CC   627 _PINA_4	=	0x00cc
                           0000CD   628 _PINA_5	=	0x00cd
                           0000CE   629 _PINA_6	=	0x00ce
                           0000CF   630 _PINA_7	=	0x00cf
                           0000E8   631 _PINB_0	=	0x00e8
                           0000E9   632 _PINB_1	=	0x00e9
                           0000EA   633 _PINB_2	=	0x00ea
                           0000EB   634 _PINB_3	=	0x00eb
                           0000EC   635 _PINB_4	=	0x00ec
                           0000ED   636 _PINB_5	=	0x00ed
                           0000EE   637 _PINB_6	=	0x00ee
                           0000EF   638 _PINB_7	=	0x00ef
                           0000F8   639 _PINC_0	=	0x00f8
                           0000F9   640 _PINC_1	=	0x00f9
                           0000FA   641 _PINC_2	=	0x00fa
                           0000FB   642 _PINC_3	=	0x00fb
                           0000FC   643 _PINC_4	=	0x00fc
                           0000FD   644 _PINC_5	=	0x00fd
                           0000FE   645 _PINC_6	=	0x00fe
                           0000FF   646 _PINC_7	=	0x00ff
                           000080   647 _PORTA_0	=	0x0080
                           000081   648 _PORTA_1	=	0x0081
                           000082   649 _PORTA_2	=	0x0082
                           000083   650 _PORTA_3	=	0x0083
                           000084   651 _PORTA_4	=	0x0084
                           000085   652 _PORTA_5	=	0x0085
                           000086   653 _PORTA_6	=	0x0086
                           000087   654 _PORTA_7	=	0x0087
                           000088   655 _PORTB_0	=	0x0088
                           000089   656 _PORTB_1	=	0x0089
                           00008A   657 _PORTB_2	=	0x008a
                           00008B   658 _PORTB_3	=	0x008b
                           00008C   659 _PORTB_4	=	0x008c
                           00008D   660 _PORTB_5	=	0x008d
                           00008E   661 _PORTB_6	=	0x008e
                           00008F   662 _PORTB_7	=	0x008f
                           000090   663 _PORTC_0	=	0x0090
                           000091   664 _PORTC_1	=	0x0091
                           000092   665 _PORTC_2	=	0x0092
                           000093   666 _PORTC_3	=	0x0093
                           000094   667 _PORTC_4	=	0x0094
                           000095   668 _PORTC_5	=	0x0095
                           000096   669 _PORTC_6	=	0x0096
                           000097   670 _PORTC_7	=	0x0097
                                    671 ;--------------------------------------------------------
                                    672 ; overlayable register banks
                                    673 ;--------------------------------------------------------
                                    674 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        675 	.ds 8
                                    676 ;--------------------------------------------------------
                                    677 ; internal ram data
                                    678 ;--------------------------------------------------------
                                    679 	.area DSEG    (DATA)
                                    680 ;--------------------------------------------------------
                                    681 ; overlayable items in internal ram 
                                    682 ;--------------------------------------------------------
                                    683 ;--------------------------------------------------------
                                    684 ; indirectly addressable internal ram data
                                    685 ;--------------------------------------------------------
                                    686 	.area ISEG    (DATA)
                                    687 ;--------------------------------------------------------
                                    688 ; absolute internal ram data
                                    689 ;--------------------------------------------------------
                                    690 	.area IABS    (ABS,DATA)
                                    691 	.area IABS    (ABS,DATA)
                                    692 ;--------------------------------------------------------
                                    693 ; bit data
                                    694 ;--------------------------------------------------------
                                    695 	.area BSEG    (BIT)
                                    696 ;--------------------------------------------------------
                                    697 ; paged external ram data
                                    698 ;--------------------------------------------------------
                                    699 	.area PSEG    (PAG,XDATA)
                                    700 ;--------------------------------------------------------
                                    701 ; external ram data
                                    702 ;--------------------------------------------------------
                                    703 	.area XSEG    (XDATA)
                           00FC06   704 _flash_deviceid	=	0xfc06
                           007020   705 _ADCCH0VAL0	=	0x7020
                           007021   706 _ADCCH0VAL1	=	0x7021
                           007020   707 _ADCCH0VAL	=	0x7020
                           007022   708 _ADCCH1VAL0	=	0x7022
                           007023   709 _ADCCH1VAL1	=	0x7023
                           007022   710 _ADCCH1VAL	=	0x7022
                           007024   711 _ADCCH2VAL0	=	0x7024
                           007025   712 _ADCCH2VAL1	=	0x7025
                           007024   713 _ADCCH2VAL	=	0x7024
                           007026   714 _ADCCH3VAL0	=	0x7026
                           007027   715 _ADCCH3VAL1	=	0x7027
                           007026   716 _ADCCH3VAL	=	0x7026
                           007028   717 _ADCTUNE0	=	0x7028
                           007029   718 _ADCTUNE1	=	0x7029
                           00702A   719 _ADCTUNE2	=	0x702a
                           007010   720 _DMA0ADDR0	=	0x7010
                           007011   721 _DMA0ADDR1	=	0x7011
                           007010   722 _DMA0ADDR	=	0x7010
                           007014   723 _DMA0CONFIG	=	0x7014
                           007012   724 _DMA1ADDR0	=	0x7012
                           007013   725 _DMA1ADDR1	=	0x7013
                           007012   726 _DMA1ADDR	=	0x7012
                           007015   727 _DMA1CONFIG	=	0x7015
                           007070   728 _FRCOSCCONFIG	=	0x7070
                           007071   729 _FRCOSCCTRL	=	0x7071
                           007076   730 _FRCOSCFREQ0	=	0x7076
                           007077   731 _FRCOSCFREQ1	=	0x7077
                           007076   732 _FRCOSCFREQ	=	0x7076
                           007072   733 _FRCOSCKFILT0	=	0x7072
                           007073   734 _FRCOSCKFILT1	=	0x7073
                           007072   735 _FRCOSCKFILT	=	0x7072
                           007078   736 _FRCOSCPER0	=	0x7078
                           007079   737 _FRCOSCPER1	=	0x7079
                           007078   738 _FRCOSCPER	=	0x7078
                           007074   739 _FRCOSCREF0	=	0x7074
                           007075   740 _FRCOSCREF1	=	0x7075
                           007074   741 _FRCOSCREF	=	0x7074
                           007007   742 _ANALOGA	=	0x7007
                           00700C   743 _GPIOENABLE	=	0x700c
                           007003   744 _EXTIRQ	=	0x7003
                           007000   745 _INTCHGA	=	0x7000
                           007001   746 _INTCHGB	=	0x7001
                           007002   747 _INTCHGC	=	0x7002
                           007008   748 _PALTA	=	0x7008
                           007009   749 _PALTB	=	0x7009
                           00700A   750 _PALTC	=	0x700a
                           007046   751 _PALTRADIO	=	0x7046
                           007004   752 _PINCHGA	=	0x7004
                           007005   753 _PINCHGB	=	0x7005
                           007006   754 _PINCHGC	=	0x7006
                           00700B   755 _PINSEL	=	0x700b
                           007060   756 _LPOSCCONFIG	=	0x7060
                           007066   757 _LPOSCFREQ0	=	0x7066
                           007067   758 _LPOSCFREQ1	=	0x7067
                           007066   759 _LPOSCFREQ	=	0x7066
                           007062   760 _LPOSCKFILT0	=	0x7062
                           007063   761 _LPOSCKFILT1	=	0x7063
                           007062   762 _LPOSCKFILT	=	0x7062
                           007068   763 _LPOSCPER0	=	0x7068
                           007069   764 _LPOSCPER1	=	0x7069
                           007068   765 _LPOSCPER	=	0x7068
                           007064   766 _LPOSCREF0	=	0x7064
                           007065   767 _LPOSCREF1	=	0x7065
                           007064   768 _LPOSCREF	=	0x7064
                           007054   769 _LPXOSCGM	=	0x7054
                           007F01   770 _MISCCTRL	=	0x7f01
                           007053   771 _OSCCALIB	=	0x7053
                           007050   772 _OSCFORCERUN	=	0x7050
                           007052   773 _OSCREADY	=	0x7052
                           007051   774 _OSCRUN	=	0x7051
                           007040   775 _RADIOFDATAADDR0	=	0x7040
                           007041   776 _RADIOFDATAADDR1	=	0x7041
                           007040   777 _RADIOFDATAADDR	=	0x7040
                           007042   778 _RADIOFSTATADDR0	=	0x7042
                           007043   779 _RADIOFSTATADDR1	=	0x7043
                           007042   780 _RADIOFSTATADDR	=	0x7042
                           007044   781 _RADIOMUX	=	0x7044
                           007084   782 _SCRATCH0	=	0x7084
                           007085   783 _SCRATCH1	=	0x7085
                           007086   784 _SCRATCH2	=	0x7086
                           007087   785 _SCRATCH3	=	0x7087
                           007F00   786 _SILICONREV	=	0x7f00
                           007F19   787 _XTALAMPL	=	0x7f19
                           007F18   788 _XTALOSC	=	0x7f18
                           007F1A   789 _XTALREADY	=	0x7f1a
                           007081   790 _RNGBYTE	=	0x7081
                           007091   791 _AESCONFIG	=	0x7091
                           007098   792 _AESCURBLOCK	=	0x7098
                           007094   793 _AESINADDR0	=	0x7094
                           007095   794 _AESINADDR1	=	0x7095
                           007094   795 _AESINADDR	=	0x7094
                           007092   796 _AESKEYADDR0	=	0x7092
                           007093   797 _AESKEYADDR1	=	0x7093
                           007092   798 _AESKEYADDR	=	0x7092
                           007090   799 _AESMODE	=	0x7090
                           007096   800 _AESOUTADDR0	=	0x7096
                           007097   801 _AESOUTADDR1	=	0x7097
                           007096   802 _AESOUTADDR	=	0x7096
                           007080   803 _RNGMODE	=	0x7080
                           007082   804 _RNGCLKSRC0	=	0x7082
                           007083   805 _RNGCLKSRC1	=	0x7083
                                    806 ;--------------------------------------------------------
                                    807 ; absolute external ram data
                                    808 ;--------------------------------------------------------
                                    809 	.area XABS    (ABS,XDATA)
                                    810 ;--------------------------------------------------------
                                    811 ; external initialized ram data
                                    812 ;--------------------------------------------------------
                                    813 	.area XISEG   (XDATA)
                                    814 	.area HOME    (CODE)
                                    815 	.area GSINIT0 (CODE)
                                    816 	.area GSINIT1 (CODE)
                                    817 	.area GSINIT2 (CODE)
                                    818 	.area GSINIT3 (CODE)
                                    819 	.area GSINIT4 (CODE)
                                    820 	.area GSINIT5 (CODE)
                                    821 	.area GSINIT  (CODE)
                                    822 	.area GSFINAL (CODE)
                                    823 	.area CSEG    (CODE)
                                    824 ;--------------------------------------------------------
                                    825 ; global & static initialisations
                                    826 ;--------------------------------------------------------
                                    827 	.area HOME    (CODE)
                                    828 	.area GSINIT  (CODE)
                                    829 	.area GSFINAL (CODE)
                                    830 	.area GSINIT  (CODE)
                                    831 ;--------------------------------------------------------
                                    832 ; Home
                                    833 ;--------------------------------------------------------
                                    834 	.area HOME    (CODE)
                                    835 	.area HOME    (CODE)
                                    836 ;--------------------------------------------------------
                                    837 ; code
                                    838 ;--------------------------------------------------------
                                    839 	.area CSEG    (CODE)
                                    840 ;------------------------------------------------------------
                                    841 ;Allocation info for local variables in function 'GOLDSEQUENCE_Init'
                                    842 ;------------------------------------------------------------
                                    843 ;	..\src\peripherals\GoldSequence.c:26: __xdata void GOLDSEQUENCE_Init(void)
                                    844 ;	-----------------------------------------
                                    845 ;	 function GOLDSEQUENCE_Init
                                    846 ;	-----------------------------------------
      005808                        847 _GOLDSEQUENCE_Init:
                           000007   848 	ar7 = 0x07
                           000006   849 	ar6 = 0x06
                           000005   850 	ar5 = 0x05
                           000004   851 	ar4 = 0x04
                           000003   852 	ar3 = 0x03
                           000002   853 	ar2 = 0x02
                           000001   854 	ar1 = 0x01
                           000000   855 	ar0 = 0x00
                                    856 ;	..\src\peripherals\GoldSequence.c:28: flash_unlock();
      005808 12 7F E9         [24]  857 	lcall	_flash_unlock
                                    858 ;	..\src\peripherals\GoldSequence.c:29: EraseMemoryPage(MEM_GOLD_SEQUENCES_START+1);
      00580B 90 1C 01         [24]  859 	mov	dptr,#0x1c01
      00580E 12 5E 84         [24]  860 	lcall	_EraseMemoryPage
                                    861 ;	..\src\peripherals\GoldSequence.c:30: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0000,0b1111100110100100,FALSE);
      005811 90 03 C7         [24]  862 	mov	dptr,#_WriteMemory_PARM_2
      005814 74 A4            [12]  863 	mov	a,#0xa4
      005816 F0               [24]  864 	movx	@dptr,a
      005817 74 F9            [12]  865 	mov	a,#0xf9
      005819 A3               [24]  866 	inc	dptr
      00581A F0               [24]  867 	movx	@dptr,a
      00581B 90 03 C9         [24]  868 	mov	dptr,#_WriteMemory_PARM_3
      00581E E4               [12]  869 	clr	a
      00581F F0               [24]  870 	movx	@dptr,a
      005820 90 1C 00         [24]  871 	mov	dptr,#0x1c00
      005823 12 5E 5A         [24]  872 	lcall	_WriteMemory
                                    873 ;	..\src\peripherals\GoldSequence.c:31: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0002,0b001010111011000,FALSE);
      005826 90 03 C7         [24]  874 	mov	dptr,#_WriteMemory_PARM_2
      005829 74 D8            [12]  875 	mov	a,#0xd8
      00582B F0               [24]  876 	movx	@dptr,a
      00582C 74 15            [12]  877 	mov	a,#0x15
      00582E A3               [24]  878 	inc	dptr
      00582F F0               [24]  879 	movx	@dptr,a
      005830 90 03 C9         [24]  880 	mov	dptr,#_WriteMemory_PARM_3
      005833 E4               [12]  881 	clr	a
      005834 F0               [24]  882 	movx	@dptr,a
      005835 90 1C 02         [24]  883 	mov	dptr,#0x1c02
      005838 12 5E 5A         [24]  884 	lcall	_WriteMemory
                                    885 ;	..\src\peripherals\GoldSequence.c:32: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0004,0b1111100100110000,FALSE);
      00583B 90 03 C7         [24]  886 	mov	dptr,#_WriteMemory_PARM_2
      00583E 74 30            [12]  887 	mov	a,#0x30
      005840 F0               [24]  888 	movx	@dptr,a
      005841 74 F9            [12]  889 	mov	a,#0xf9
      005843 A3               [24]  890 	inc	dptr
      005844 F0               [24]  891 	movx	@dptr,a
      005845 90 03 C9         [24]  892 	mov	dptr,#_WriteMemory_PARM_3
      005848 E4               [12]  893 	clr	a
      005849 F0               [24]  894 	movx	@dptr,a
      00584A 90 1C 04         [24]  895 	mov	dptr,#0x1c04
      00584D 12 5E 5A         [24]  896 	lcall	_WriteMemory
                                    897 ;	..\src\peripherals\GoldSequence.c:33: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0006,0b101101010001110,FALSE);
      005850 90 03 C7         [24]  898 	mov	dptr,#_WriteMemory_PARM_2
      005853 74 8E            [12]  899 	mov	a,#0x8e
      005855 F0               [24]  900 	movx	@dptr,a
      005856 74 5A            [12]  901 	mov	a,#0x5a
      005858 A3               [24]  902 	inc	dptr
      005859 F0               [24]  903 	movx	@dptr,a
      00585A 90 03 C9         [24]  904 	mov	dptr,#_WriteMemory_PARM_3
      00585D E4               [12]  905 	clr	a
      00585E F0               [24]  906 	movx	@dptr,a
      00585F 90 1C 06         [24]  907 	mov	dptr,#0x1c06
      005862 12 5E 5A         [24]  908 	lcall	_WriteMemory
                                    909 ;	..\src\peripherals\GoldSequence.c:34: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0008,0b0000000010010100,FALSE);
      005865 90 03 C7         [24]  910 	mov	dptr,#_WriteMemory_PARM_2
      005868 74 94            [12]  911 	mov	a,#0x94
      00586A F0               [24]  912 	movx	@dptr,a
      00586B E4               [12]  913 	clr	a
      00586C A3               [24]  914 	inc	dptr
      00586D F0               [24]  915 	movx	@dptr,a
      00586E 90 03 C9         [24]  916 	mov	dptr,#_WriteMemory_PARM_3
      005871 F0               [24]  917 	movx	@dptr,a
      005872 90 1C 08         [24]  918 	mov	dptr,#0x1c08
      005875 12 5E 5A         [24]  919 	lcall	_WriteMemory
                                    920 ;	..\src\peripherals\GoldSequence.c:35: WriteMemory(MEM_GOLD_SEQUENCES_START+0x000A,0b100111101010110,FALSE);
      005878 90 03 C7         [24]  921 	mov	dptr,#_WriteMemory_PARM_2
      00587B 74 56            [12]  922 	mov	a,#0x56
      00587D F0               [24]  923 	movx	@dptr,a
      00587E 74 4F            [12]  924 	mov	a,#0x4f
      005880 A3               [24]  925 	inc	dptr
      005881 F0               [24]  926 	movx	@dptr,a
      005882 90 03 C9         [24]  927 	mov	dptr,#_WriteMemory_PARM_3
      005885 E4               [12]  928 	clr	a
      005886 F0               [24]  929 	movx	@dptr,a
      005887 90 1C 0A         [24]  930 	mov	dptr,#0x1c0a
      00588A 12 5E 5A         [24]  931 	lcall	_WriteMemory
                                    932 ;	..\src\peripherals\GoldSequence.c:36: WriteMemory(MEM_GOLD_SEQUENCES_START+0x000C,0b1000010100111100,FALSE);
      00588D 90 03 C7         [24]  933 	mov	dptr,#_WriteMemory_PARM_2
      005890 74 3C            [12]  934 	mov	a,#0x3c
      005892 F0               [24]  935 	movx	@dptr,a
      005893 74 85            [12]  936 	mov	a,#0x85
      005895 A3               [24]  937 	inc	dptr
      005896 F0               [24]  938 	movx	@dptr,a
      005897 90 03 C9         [24]  939 	mov	dptr,#_WriteMemory_PARM_3
      00589A E4               [12]  940 	clr	a
      00589B F0               [24]  941 	movx	@dptr,a
      00589C 90 1C 0C         [24]  942 	mov	dptr,#0x1c0c
      00589F 12 5E 5A         [24]  943 	lcall	_WriteMemory
                                    944 ;	..\src\peripherals\GoldSequence.c:37: WriteMemory(MEM_GOLD_SEQUENCES_START+0x000E,0b011100010011111,FALSE);
      0058A2 90 03 C7         [24]  945 	mov	dptr,#_WriteMemory_PARM_2
      0058A5 74 9F            [12]  946 	mov	a,#0x9f
      0058A7 F0               [24]  947 	movx	@dptr,a
      0058A8 74 38            [12]  948 	mov	a,#0x38
      0058AA A3               [24]  949 	inc	dptr
      0058AB F0               [24]  950 	movx	@dptr,a
      0058AC 90 03 C9         [24]  951 	mov	dptr,#_WriteMemory_PARM_3
      0058AF E4               [12]  952 	clr	a
      0058B0 F0               [24]  953 	movx	@dptr,a
      0058B1 90 1C 0E         [24]  954 	mov	dptr,#0x1c0e
      0058B4 12 5E 5A         [24]  955 	lcall	_WriteMemory
                                    956 ;	..\src\peripherals\GoldSequence.c:38: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0010,0b0100011111101000,FALSE);
      0058B7 90 03 C7         [24]  957 	mov	dptr,#_WriteMemory_PARM_2
      0058BA 74 E8            [12]  958 	mov	a,#0xe8
      0058BC F0               [24]  959 	movx	@dptr,a
      0058BD 74 47            [12]  960 	mov	a,#0x47
      0058BF A3               [24]  961 	inc	dptr
      0058C0 F0               [24]  962 	movx	@dptr,a
      0058C1 90 03 C9         [24]  963 	mov	dptr,#_WriteMemory_PARM_3
      0058C4 E4               [12]  964 	clr	a
      0058C5 F0               [24]  965 	movx	@dptr,a
      0058C6 90 1C 10         [24]  966 	mov	dptr,#0x1c10
      0058C9 12 5E 5A         [24]  967 	lcall	_WriteMemory
                                    968 ;	..\src\peripherals\GoldSequence.c:39: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0012,0b000001101111011,FALSE);
      0058CC 90 03 C7         [24]  969 	mov	dptr,#_WriteMemory_PARM_2
      0058CF 74 7B            [12]  970 	mov	a,#0x7b
      0058D1 F0               [24]  971 	movx	@dptr,a
      0058D2 74 03            [12]  972 	mov	a,#0x03
      0058D4 A3               [24]  973 	inc	dptr
      0058D5 F0               [24]  974 	movx	@dptr,a
      0058D6 90 03 C9         [24]  975 	mov	dptr,#_WriteMemory_PARM_3
      0058D9 E4               [12]  976 	clr	a
      0058DA F0               [24]  977 	movx	@dptr,a
      0058DB 90 1C 12         [24]  978 	mov	dptr,#0x1c12
      0058DE 12 5E 5A         [24]  979 	lcall	_WriteMemory
                                    980 ;	..\src\peripherals\GoldSequence.c:40: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0014,0b0010011010000010,FALSE);
      0058E1 90 03 C7         [24]  981 	mov	dptr,#_WriteMemory_PARM_2
      0058E4 74 82            [12]  982 	mov	a,#0x82
      0058E6 F0               [24]  983 	movx	@dptr,a
      0058E7 74 26            [12]  984 	mov	a,#0x26
      0058E9 A3               [24]  985 	inc	dptr
      0058EA F0               [24]  986 	movx	@dptr,a
      0058EB 90 03 C9         [24]  987 	mov	dptr,#_WriteMemory_PARM_3
      0058EE E4               [12]  988 	clr	a
      0058EF F0               [24]  989 	movx	@dptr,a
      0058F0 90 1C 14         [24]  990 	mov	dptr,#0x1c14
      0058F3 12 5E 5A         [24]  991 	lcall	_WriteMemory
                                    992 ;	..\src\peripherals\GoldSequence.c:41: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0016,0b001111010001001,FALSE);
      0058F6 90 03 C7         [24]  993 	mov	dptr,#_WriteMemory_PARM_2
      0058F9 74 89            [12]  994 	mov	a,#0x89
      0058FB F0               [24]  995 	movx	@dptr,a
      0058FC 74 1E            [12]  996 	mov	a,#0x1e
      0058FE A3               [24]  997 	inc	dptr
      0058FF F0               [24]  998 	movx	@dptr,a
      005900 90 03 C9         [24]  999 	mov	dptr,#_WriteMemory_PARM_3
      005903 E4               [12] 1000 	clr	a
      005904 F0               [24] 1001 	movx	@dptr,a
      005905 90 1C 16         [24] 1002 	mov	dptr,#0x1c16
      005908 12 5E 5A         [24] 1003 	lcall	_WriteMemory
                                   1004 ;	..\src\peripherals\GoldSequence.c:42: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0018,0b0001011000110111,FALSE);
      00590B 90 03 C7         [24] 1005 	mov	dptr,#_WriteMemory_PARM_2
      00590E 74 37            [12] 1006 	mov	a,#0x37
      005910 F0               [24] 1007 	movx	@dptr,a
      005911 74 16            [12] 1008 	mov	a,#0x16
      005913 A3               [24] 1009 	inc	dptr
      005914 F0               [24] 1010 	movx	@dptr,a
      005915 90 03 C9         [24] 1011 	mov	dptr,#_WriteMemory_PARM_3
      005918 E4               [12] 1012 	clr	a
      005919 F0               [24] 1013 	movx	@dptr,a
      00591A 90 1C 18         [24] 1014 	mov	dptr,#0x1c18
      00591D 12 5E 5A         [24] 1015 	lcall	_WriteMemory
                                   1016 ;	..\src\peripherals\GoldSequence.c:43: WriteMemory(MEM_GOLD_SEQUENCES_START+0x001A,0b001000001110000,FALSE);
      005920 90 03 C7         [24] 1017 	mov	dptr,#_WriteMemory_PARM_2
      005923 74 70            [12] 1018 	mov	a,#0x70
      005925 F0               [24] 1019 	movx	@dptr,a
      005926 74 10            [12] 1020 	mov	a,#0x10
      005928 A3               [24] 1021 	inc	dptr
      005929 F0               [24] 1022 	movx	@dptr,a
      00592A 90 03 C9         [24] 1023 	mov	dptr,#_WriteMemory_PARM_3
      00592D E4               [12] 1024 	clr	a
      00592E F0               [24] 1025 	movx	@dptr,a
      00592F 90 1C 1A         [24] 1026 	mov	dptr,#0x1c1a
      005932 12 5E 5A         [24] 1027 	lcall	_WriteMemory
                                   1028 ;	..\src\peripherals\GoldSequence.c:44: WriteMemory(MEM_GOLD_SEQUENCES_START+0x001C,0b1000111001101101,FALSE);
      005935 90 03 C7         [24] 1029 	mov	dptr,#_WriteMemory_PARM_2
      005938 74 6D            [12] 1030 	mov	a,#0x6d
      00593A F0               [24] 1031 	movx	@dptr,a
      00593B 74 8E            [12] 1032 	mov	a,#0x8e
      00593D A3               [24] 1033 	inc	dptr
      00593E F0               [24] 1034 	movx	@dptr,a
      00593F 90 03 C9         [24] 1035 	mov	dptr,#_WriteMemory_PARM_3
      005942 E4               [12] 1036 	clr	a
      005943 F0               [24] 1037 	movx	@dptr,a
      005944 90 1C 1C         [24] 1038 	mov	dptr,#0x1c1c
      005947 12 5E 5A         [24] 1039 	lcall	_WriteMemory
                                   1040 ;	..\src\peripherals\GoldSequence.c:45: WriteMemory(MEM_GOLD_SEQUENCES_START+0x001E,0b101011100001100,FALSE);
      00594A 90 03 C7         [24] 1041 	mov	dptr,#_WriteMemory_PARM_2
      00594D 74 0C            [12] 1042 	mov	a,#0x0c
      00594F F0               [24] 1043 	movx	@dptr,a
      005950 74 57            [12] 1044 	mov	a,#0x57
      005952 A3               [24] 1045 	inc	dptr
      005953 F0               [24] 1046 	movx	@dptr,a
      005954 90 03 C9         [24] 1047 	mov	dptr,#_WriteMemory_PARM_3
      005957 E4               [12] 1048 	clr	a
      005958 F0               [24] 1049 	movx	@dptr,a
      005959 90 1C 1E         [24] 1050 	mov	dptr,#0x1c1e
      00595C 12 5E 5A         [24] 1051 	lcall	_WriteMemory
                                   1052 ;	..\src\peripherals\GoldSequence.c:46: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0020,0b1100001001000000,FALSE);
      00595F 90 03 C7         [24] 1053 	mov	dptr,#_WriteMemory_PARM_2
      005962 74 40            [12] 1054 	mov	a,#0x40
      005964 F0               [24] 1055 	movx	@dptr,a
      005965 74 C2            [12] 1056 	mov	a,#0xc2
      005967 A3               [24] 1057 	inc	dptr
      005968 F0               [24] 1058 	movx	@dptr,a
      005969 90 03 C9         [24] 1059 	mov	dptr,#_WriteMemory_PARM_3
      00596C E4               [12] 1060 	clr	a
      00596D F0               [24] 1061 	movx	@dptr,a
      00596E 90 1C 20         [24] 1062 	mov	dptr,#0x1c20
      005971 12 5E 5A         [24] 1063 	lcall	_WriteMemory
                                   1064 ;	..\src\peripherals\GoldSequence.c:47: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0022,0b111010010110010,FALSE);
      005974 90 03 C7         [24] 1065 	mov	dptr,#_WriteMemory_PARM_2
      005977 74 B2            [12] 1066 	mov	a,#0xb2
      005979 F0               [24] 1067 	movx	@dptr,a
      00597A 74 74            [12] 1068 	mov	a,#0x74
      00597C A3               [24] 1069 	inc	dptr
      00597D F0               [24] 1070 	movx	@dptr,a
      00597E 90 03 C9         [24] 1071 	mov	dptr,#_WriteMemory_PARM_3
      005981 E4               [12] 1072 	clr	a
      005982 F0               [24] 1073 	movx	@dptr,a
      005983 90 1C 22         [24] 1074 	mov	dptr,#0x1c22
      005986 12 5E 5A         [24] 1075 	lcall	_WriteMemory
                                   1076 ;	..\src\peripherals\GoldSequence.c:48: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0024,0b1110010001010110,FALSE);
      005989 90 03 C7         [24] 1077 	mov	dptr,#_WriteMemory_PARM_2
      00598C 74 56            [12] 1078 	mov	a,#0x56
      00598E F0               [24] 1079 	movx	@dptr,a
      00598F 74 E4            [12] 1080 	mov	a,#0xe4
      005991 A3               [24] 1081 	inc	dptr
      005992 F0               [24] 1082 	movx	@dptr,a
      005993 90 03 C9         [24] 1083 	mov	dptr,#_WriteMemory_PARM_3
      005996 E4               [12] 1084 	clr	a
      005997 F0               [24] 1085 	movx	@dptr,a
      005998 90 1C 24         [24] 1086 	mov	dptr,#0x1c24
      00599B 12 5E 5A         [24] 1087 	lcall	_WriteMemory
                                   1088 ;	..\src\peripherals\GoldSequence.c:49: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0026,0b010010101101101,FALSE);
      00599E 90 03 C7         [24] 1089 	mov	dptr,#_WriteMemory_PARM_2
      0059A1 74 6D            [12] 1090 	mov	a,#0x6d
      0059A3 F0               [24] 1091 	movx	@dptr,a
      0059A4 74 25            [12] 1092 	mov	a,#0x25
      0059A6 A3               [24] 1093 	inc	dptr
      0059A7 F0               [24] 1094 	movx	@dptr,a
      0059A8 90 03 C9         [24] 1095 	mov	dptr,#_WriteMemory_PARM_3
      0059AB E4               [12] 1096 	clr	a
      0059AC F0               [24] 1097 	movx	@dptr,a
      0059AD 90 1C 26         [24] 1098 	mov	dptr,#0x1c26
      0059B0 12 5E 5A         [24] 1099 	lcall	_WriteMemory
                                   1100 ;	..\src\peripherals\GoldSequence.c:50: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0028,0b0111011101011101,FALSE);
      0059B3 90 03 C7         [24] 1101 	mov	dptr,#_WriteMemory_PARM_2
      0059B6 74 5D            [12] 1102 	mov	a,#0x5d
      0059B8 F0               [24] 1103 	movx	@dptr,a
      0059B9 74 77            [12] 1104 	mov	a,#0x77
      0059BB A3               [24] 1105 	inc	dptr
      0059BC F0               [24] 1106 	movx	@dptr,a
      0059BD 90 03 C9         [24] 1107 	mov	dptr,#_WriteMemory_PARM_3
      0059C0 E4               [12] 1108 	clr	a
      0059C1 F0               [24] 1109 	movx	@dptr,a
      0059C2 90 1C 28         [24] 1110 	mov	dptr,#0x1c28
      0059C5 12 5E 5A         [24] 1111 	lcall	_WriteMemory
                                   1112 ;	..\src\peripherals\GoldSequence.c:51: WriteMemory(MEM_GOLD_SEQUENCES_START+0x002A,0b000110110000010,FALSE);
      0059C8 90 03 C7         [24] 1113 	mov	dptr,#_WriteMemory_PARM_2
      0059CB 74 82            [12] 1114 	mov	a,#0x82
      0059CD F0               [24] 1115 	movx	@dptr,a
      0059CE 74 0D            [12] 1116 	mov	a,#0x0d
      0059D0 A3               [24] 1117 	inc	dptr
      0059D1 F0               [24] 1118 	movx	@dptr,a
      0059D2 90 03 C9         [24] 1119 	mov	dptr,#_WriteMemory_PARM_3
      0059D5 E4               [12] 1120 	clr	a
      0059D6 F0               [24] 1121 	movx	@dptr,a
      0059D7 90 1C 2A         [24] 1122 	mov	dptr,#0x1c2a
      0059DA 12 5E 5A         [24] 1123 	lcall	_WriteMemory
                                   1124 ;	..\src\peripherals\GoldSequence.c:52: WriteMemory(MEM_GOLD_SEQUENCES_START+0x002C,0b1011111011011000,FALSE);
      0059DD 90 03 C7         [24] 1125 	mov	dptr,#_WriteMemory_PARM_2
      0059E0 74 D8            [12] 1126 	mov	a,#0xd8
      0059E2 F0               [24] 1127 	movx	@dptr,a
      0059E3 74 BE            [12] 1128 	mov	a,#0xbe
      0059E5 A3               [24] 1129 	inc	dptr
      0059E6 F0               [24] 1130 	movx	@dptr,a
      0059E7 90 03 C9         [24] 1131 	mov	dptr,#_WriteMemory_PARM_3
      0059EA E4               [12] 1132 	clr	a
      0059EB F0               [24] 1133 	movx	@dptr,a
      0059EC 90 1C 2C         [24] 1134 	mov	dptr,#0x1c2c
      0059EF 12 5E 5A         [24] 1135 	lcall	_WriteMemory
                                   1136 ;	..\src\peripherals\GoldSequence.c:53: WriteMemory(MEM_GOLD_SEQUENCES_START+0x002E,0b101100111110101,FALSE);
      0059F2 90 03 C7         [24] 1137 	mov	dptr,#_WriteMemory_PARM_2
      0059F5 74 F5            [12] 1138 	mov	a,#0xf5
      0059F7 F0               [24] 1139 	movx	@dptr,a
      0059F8 74 59            [12] 1140 	mov	a,#0x59
      0059FA A3               [24] 1141 	inc	dptr
      0059FB F0               [24] 1142 	movx	@dptr,a
      0059FC 90 03 C9         [24] 1143 	mov	dptr,#_WriteMemory_PARM_3
      0059FF E4               [12] 1144 	clr	a
      005A00 F0               [24] 1145 	movx	@dptr,a
      005A01 90 1C 2E         [24] 1146 	mov	dptr,#0x1c2e
      005A04 12 5E 5A         [24] 1147 	lcall	_WriteMemory
                                   1148 ;	..\src\peripherals\GoldSequence.c:54: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0030,0b0101101000011010,FALSE);
      005A07 90 03 C7         [24] 1149 	mov	dptr,#_WriteMemory_PARM_2
      005A0A 74 1A            [12] 1150 	mov	a,#0x1a
      005A0C F0               [24] 1151 	movx	@dptr,a
      005A0D 74 5A            [12] 1152 	mov	a,#0x5a
      005A0F A3               [24] 1153 	inc	dptr
      005A10 F0               [24] 1154 	movx	@dptr,a
      005A11 90 03 C9         [24] 1155 	mov	dptr,#_WriteMemory_PARM_3
      005A14 E4               [12] 1156 	clr	a
      005A15 F0               [24] 1157 	movx	@dptr,a
      005A16 90 1C 30         [24] 1158 	mov	dptr,#0x1c30
      005A19 12 5E 5A         [24] 1159 	lcall	_WriteMemory
                                   1160 ;	..\src\peripherals\GoldSequence.c:55: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0032,0b011001111001110,FALSE);
      005A1C 90 03 C7         [24] 1161 	mov	dptr,#_WriteMemory_PARM_2
      005A1F 74 CE            [12] 1162 	mov	a,#0xce
      005A21 F0               [24] 1163 	movx	@dptr,a
      005A22 74 33            [12] 1164 	mov	a,#0x33
      005A24 A3               [24] 1165 	inc	dptr
      005A25 F0               [24] 1166 	movx	@dptr,a
      005A26 90 03 C9         [24] 1167 	mov	dptr,#_WriteMemory_PARM_3
      005A29 E4               [12] 1168 	clr	a
      005A2A F0               [24] 1169 	movx	@dptr,a
      005A2B 90 1C 32         [24] 1170 	mov	dptr,#0x1c32
      005A2E 12 5E 5A         [24] 1171 	lcall	_WriteMemory
                                   1172 ;	..\src\peripherals\GoldSequence.c:56: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0034,0b1010100001111011,FALSE);
      005A31 90 03 C7         [24] 1173 	mov	dptr,#_WriteMemory_PARM_2
      005A34 74 7B            [12] 1174 	mov	a,#0x7b
      005A36 F0               [24] 1175 	movx	@dptr,a
      005A37 74 A8            [12] 1176 	mov	a,#0xa8
      005A39 A3               [24] 1177 	inc	dptr
      005A3A F0               [24] 1178 	movx	@dptr,a
      005A3B 90 03 C9         [24] 1179 	mov	dptr,#_WriteMemory_PARM_3
      005A3E E4               [12] 1180 	clr	a
      005A3F F0               [24] 1181 	movx	@dptr,a
      005A40 90 1C 34         [24] 1182 	mov	dptr,#0x1c34
      005A43 12 5E 5A         [24] 1183 	lcall	_WriteMemory
                                   1184 ;	..\src\peripherals\GoldSequence.c:57: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0036,0b000011011010011,FALSE);
      005A46 90 03 C7         [24] 1185 	mov	dptr,#_WriteMemory_PARM_2
      005A49 74 D3            [12] 1186 	mov	a,#0xd3
      005A4B F0               [24] 1187 	movx	@dptr,a
      005A4C 74 06            [12] 1188 	mov	a,#0x06
      005A4E A3               [24] 1189 	inc	dptr
      005A4F F0               [24] 1190 	movx	@dptr,a
      005A50 90 03 C9         [24] 1191 	mov	dptr,#_WriteMemory_PARM_3
      005A53 E4               [12] 1192 	clr	a
      005A54 F0               [24] 1193 	movx	@dptr,a
      005A55 90 1C 36         [24] 1194 	mov	dptr,#0x1c36
      005A58 12 5E 5A         [24] 1195 	lcall	_WriteMemory
                                   1196 ;	..\src\peripherals\GoldSequence.c:58: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0038,0b0101000101001011,FALSE);
      005A5B 90 03 C7         [24] 1197 	mov	dptr,#_WriteMemory_PARM_2
      005A5E 74 4B            [12] 1198 	mov	a,#0x4b
      005A60 F0               [24] 1199 	movx	@dptr,a
      005A61 74 51            [12] 1200 	mov	a,#0x51
      005A63 A3               [24] 1201 	inc	dptr
      005A64 F0               [24] 1202 	movx	@dptr,a
      005A65 90 03 C9         [24] 1203 	mov	dptr,#_WriteMemory_PARM_3
      005A68 E4               [12] 1204 	clr	a
      005A69 F0               [24] 1205 	movx	@dptr,a
      005A6A 90 1C 38         [24] 1206 	mov	dptr,#0x1c38
      005A6D 12 5E 5A         [24] 1207 	lcall	_WriteMemory
                                   1208 ;	..\src\peripherals\GoldSequence.c:59: WriteMemory(MEM_GOLD_SEQUENCES_START+0x003A,0b101110001011101,FALSE);
      005A70 90 03 C7         [24] 1209 	mov	dptr,#_WriteMemory_PARM_2
      005A73 74 5D            [12] 1210 	mov	a,#0x5d
      005A75 F0               [24] 1211 	movx	@dptr,a
      005A76 14               [12] 1212 	dec	a
      005A77 A3               [24] 1213 	inc	dptr
      005A78 F0               [24] 1214 	movx	@dptr,a
      005A79 90 03 C9         [24] 1215 	mov	dptr,#_WriteMemory_PARM_3
      005A7C E4               [12] 1216 	clr	a
      005A7D F0               [24] 1217 	movx	@dptr,a
      005A7E 90 1C 3A         [24] 1218 	mov	dptr,#0x1c3a
      005A81 12 5E 5A         [24] 1219 	lcall	_WriteMemory
                                   1220 ;	..\src\peripherals\GoldSequence.c:60: WriteMemory(MEM_GOLD_SEQUENCES_START+0x003C,0b0010110111010011,FALSE);
      005A84 90 03 C7         [24] 1221 	mov	dptr,#_WriteMemory_PARM_2
      005A87 74 D3            [12] 1222 	mov	a,#0xd3
      005A89 F0               [24] 1223 	movx	@dptr,a
      005A8A 74 2D            [12] 1224 	mov	a,#0x2d
      005A8C A3               [24] 1225 	inc	dptr
      005A8D F0               [24] 1226 	movx	@dptr,a
      005A8E 90 03 C9         [24] 1227 	mov	dptr,#_WriteMemory_PARM_3
      005A91 E4               [12] 1228 	clr	a
      005A92 F0               [24] 1229 	movx	@dptr,a
      005A93 90 1C 3C         [24] 1230 	mov	dptr,#0x1c3c
      005A96 12 5E 5A         [24] 1231 	lcall	_WriteMemory
                                   1232 ;	..\src\peripherals\GoldSequence.c:61: WriteMemory(MEM_GOLD_SEQUENCES_START+0x003E,0b111000100011010,FALSE);
      005A99 90 03 C7         [24] 1233 	mov	dptr,#_WriteMemory_PARM_2
      005A9C 74 1A            [12] 1234 	mov	a,#0x1a
      005A9E F0               [24] 1235 	movx	@dptr,a
      005A9F 74 71            [12] 1236 	mov	a,#0x71
      005AA1 A3               [24] 1237 	inc	dptr
      005AA2 F0               [24] 1238 	movx	@dptr,a
      005AA3 90 03 C9         [24] 1239 	mov	dptr,#_WriteMemory_PARM_3
      005AA6 E4               [12] 1240 	clr	a
      005AA7 F0               [24] 1241 	movx	@dptr,a
      005AA8 90 1C 3E         [24] 1242 	mov	dptr,#0x1c3e
      005AAB 12 5E 5A         [24] 1243 	lcall	_WriteMemory
                                   1244 ;	..\src\peripherals\GoldSequence.c:62: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0040,0b1001001110011111,FALSE);
      005AAE 90 03 C7         [24] 1245 	mov	dptr,#_WriteMemory_PARM_2
      005AB1 74 9F            [12] 1246 	mov	a,#0x9f
      005AB3 F0               [24] 1247 	movx	@dptr,a
      005AB4 74 93            [12] 1248 	mov	a,#0x93
      005AB6 A3               [24] 1249 	inc	dptr
      005AB7 F0               [24] 1250 	movx	@dptr,a
      005AB8 90 03 C9         [24] 1251 	mov	dptr,#_WriteMemory_PARM_3
      005ABB E4               [12] 1252 	clr	a
      005ABC F0               [24] 1253 	movx	@dptr,a
      005ABD 90 1C 40         [24] 1254 	mov	dptr,#0x1c40
      005AC0 12 5E 5A         [24] 1255 	lcall	_WriteMemory
                                   1256 ;	..\src\peripherals\GoldSequence.c:63: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0042,0b110011110111001,FALSE);
      005AC3 90 03 C7         [24] 1257 	mov	dptr,#_WriteMemory_PARM_2
      005AC6 74 B9            [12] 1258 	mov	a,#0xb9
      005AC8 F0               [24] 1259 	movx	@dptr,a
      005AC9 74 67            [12] 1260 	mov	a,#0x67
      005ACB A3               [24] 1261 	inc	dptr
      005ACC F0               [24] 1262 	movx	@dptr,a
      005ACD 90 03 C9         [24] 1263 	mov	dptr,#_WriteMemory_PARM_3
      005AD0 E4               [12] 1264 	clr	a
      005AD1 F0               [24] 1265 	movx	@dptr,a
      005AD2 90 1C 42         [24] 1266 	mov	dptr,#0x1c42
      005AD5 12 5E 5A         [24] 1267 	lcall	_WriteMemory
                                   1268 ;	..\src\peripherals\GoldSequence.c:64: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0044,0b0100110010111001,FALSE);
      005AD8 90 03 C7         [24] 1269 	mov	dptr,#_WriteMemory_PARM_2
      005ADB 74 B9            [12] 1270 	mov	a,#0xb9
      005ADD F0               [24] 1271 	movx	@dptr,a
      005ADE 74 4C            [12] 1272 	mov	a,#0x4c
      005AE0 A3               [24] 1273 	inc	dptr
      005AE1 F0               [24] 1274 	movx	@dptr,a
      005AE2 90 03 C9         [24] 1275 	mov	dptr,#_WriteMemory_PARM_3
      005AE5 E4               [12] 1276 	clr	a
      005AE6 F0               [24] 1277 	movx	@dptr,a
      005AE7 90 1C 44         [24] 1278 	mov	dptr,#0x1c44
      005AEA 12 5E 5A         [24] 1279 	lcall	_WriteMemory
                                   1280 ;	..\src\peripherals\GoldSequence.c:65: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0046,0b110110011101000,FALSE);
      005AED 90 03 C7         [24] 1281 	mov	dptr,#_WriteMemory_PARM_2
      005AF0 74 E8            [12] 1282 	mov	a,#0xe8
      005AF2 F0               [24] 1283 	movx	@dptr,a
      005AF3 74 6C            [12] 1284 	mov	a,#0x6c
      005AF5 A3               [24] 1285 	inc	dptr
      005AF6 F0               [24] 1286 	movx	@dptr,a
      005AF7 90 03 C9         [24] 1287 	mov	dptr,#_WriteMemory_PARM_3
      005AFA E4               [12] 1288 	clr	a
      005AFB F0               [24] 1289 	movx	@dptr,a
      005AFC 90 1C 46         [24] 1290 	mov	dptr,#0x1c46
      005AFF 12 5E 5A         [24] 1291 	lcall	_WriteMemory
                                   1292 ;	..\src\peripherals\GoldSequence.c:66: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0048,0b1010001100101010,FALSE);
      005B02 90 03 C7         [24] 1293 	mov	dptr,#_WriteMemory_PARM_2
      005B05 74 2A            [12] 1294 	mov	a,#0x2a
      005B07 F0               [24] 1295 	movx	@dptr,a
      005B08 74 A3            [12] 1296 	mov	a,#0xa3
      005B0A A3               [24] 1297 	inc	dptr
      005B0B F0               [24] 1298 	movx	@dptr,a
      005B0C 90 03 C9         [24] 1299 	mov	dptr,#_WriteMemory_PARM_3
      005B0F E4               [12] 1300 	clr	a
      005B10 F0               [24] 1301 	movx	@dptr,a
      005B11 90 1C 48         [24] 1302 	mov	dptr,#0x1c48
      005B14 12 5E 5A         [24] 1303 	lcall	_WriteMemory
                                   1304 ;	..\src\peripherals\GoldSequence.c:67: WriteMemory(MEM_GOLD_SEQUENCES_START+0x004A,0b110100101000000,FALSE);
      005B17 90 03 C7         [24] 1305 	mov	dptr,#_WriteMemory_PARM_2
      005B1A 74 40            [12] 1306 	mov	a,#0x40
      005B1C F0               [24] 1307 	movx	@dptr,a
      005B1D 74 69            [12] 1308 	mov	a,#0x69
      005B1F A3               [24] 1309 	inc	dptr
      005B20 F0               [24] 1310 	movx	@dptr,a
      005B21 90 03 C9         [24] 1311 	mov	dptr,#_WriteMemory_PARM_3
      005B24 E4               [12] 1312 	clr	a
      005B25 F0               [24] 1313 	movx	@dptr,a
      005B26 90 1C 4A         [24] 1314 	mov	dptr,#0x1c4a
      005B29 12 5E 5A         [24] 1315 	lcall	_WriteMemory
                                   1316 ;	..\src\peripherals\GoldSequence.c:68: WriteMemory(MEM_GOLD_SEQUENCES_START+0x004C,0b1101010011100011,FALSE);
      005B2C 90 03 C7         [24] 1317 	mov	dptr,#_WriteMemory_PARM_2
      005B2F 74 E3            [12] 1318 	mov	a,#0xe3
      005B31 F0               [24] 1319 	movx	@dptr,a
      005B32 74 D4            [12] 1320 	mov	a,#0xd4
      005B34 A3               [24] 1321 	inc	dptr
      005B35 F0               [24] 1322 	movx	@dptr,a
      005B36 90 03 C9         [24] 1323 	mov	dptr,#_WriteMemory_PARM_3
      005B39 E4               [12] 1324 	clr	a
      005B3A F0               [24] 1325 	movx	@dptr,a
      005B3B 90 1C 4C         [24] 1326 	mov	dptr,#0x1c4c
      005B3E 12 5E 5A         [24] 1327 	lcall	_WriteMemory
                                   1328 ;	..\src\peripherals\GoldSequence.c:69: WriteMemory(MEM_GOLD_SEQUENCES_START+0x004E,0b010101110010100,FALSE);
      005B41 90 03 C7         [24] 1329 	mov	dptr,#_WriteMemory_PARM_2
      005B44 74 94            [12] 1330 	mov	a,#0x94
      005B46 F0               [24] 1331 	movx	@dptr,a
      005B47 74 2B            [12] 1332 	mov	a,#0x2b
      005B49 A3               [24] 1333 	inc	dptr
      005B4A F0               [24] 1334 	movx	@dptr,a
      005B4B 90 03 C9         [24] 1335 	mov	dptr,#_WriteMemory_PARM_3
      005B4E E4               [12] 1336 	clr	a
      005B4F F0               [24] 1337 	movx	@dptr,a
      005B50 90 1C 4E         [24] 1338 	mov	dptr,#0x1c4e
      005B53 12 5E 5A         [24] 1339 	lcall	_WriteMemory
                                   1340 ;	..\src\peripherals\GoldSequence.c:70: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0050,0b1110111100000111,FALSE);
      005B56 90 03 C7         [24] 1341 	mov	dptr,#_WriteMemory_PARM_2
      005B59 74 07            [12] 1342 	mov	a,#0x07
      005B5B F0               [24] 1343 	movx	@dptr,a
      005B5C 74 EF            [12] 1344 	mov	a,#0xef
      005B5E A3               [24] 1345 	inc	dptr
      005B5F F0               [24] 1346 	movx	@dptr,a
      005B60 90 03 C9         [24] 1347 	mov	dptr,#_WriteMemory_PARM_3
      005B63 E4               [12] 1348 	clr	a
      005B64 F0               [24] 1349 	movx	@dptr,a
      005B65 90 1C 50         [24] 1350 	mov	dptr,#0x1c50
      005B68 12 5E 5A         [24] 1351 	lcall	_WriteMemory
                                   1352 ;	..\src\peripherals\GoldSequence.c:71: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0052,0b100101011111110,FALSE);
      005B6B 90 03 C7         [24] 1353 	mov	dptr,#_WriteMemory_PARM_2
      005B6E 74 FE            [12] 1354 	mov	a,#0xfe
      005B70 F0               [24] 1355 	movx	@dptr,a
      005B71 74 4A            [12] 1356 	mov	a,#0x4a
      005B73 A3               [24] 1357 	inc	dptr
      005B74 F0               [24] 1358 	movx	@dptr,a
      005B75 90 03 C9         [24] 1359 	mov	dptr,#_WriteMemory_PARM_3
      005B78 E4               [12] 1360 	clr	a
      005B79 F0               [24] 1361 	movx	@dptr,a
      005B7A 90 1C 52         [24] 1362 	mov	dptr,#0x1c52
      005B7D 12 5E 5A         [24] 1363 	lcall	_WriteMemory
                                   1364 ;	..\src\peripherals\GoldSequence.c:72: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0054,0b1111001011110101,FALSE);
      005B80 90 03 C7         [24] 1365 	mov	dptr,#_WriteMemory_PARM_2
      005B83 74 F5            [12] 1366 	mov	a,#0xf5
      005B85 F0               [24] 1367 	movx	@dptr,a
      005B86 74 F2            [12] 1368 	mov	a,#0xf2
      005B88 A3               [24] 1369 	inc	dptr
      005B89 F0               [24] 1370 	movx	@dptr,a
      005B8A 90 03 C9         [24] 1371 	mov	dptr,#_WriteMemory_PARM_3
      005B8D E4               [12] 1372 	clr	a
      005B8E F0               [24] 1373 	movx	@dptr,a
      005B8F 90 1C 54         [24] 1374 	mov	dptr,#0x1c54
      005B92 12 5E 5A         [24] 1375 	lcall	_WriteMemory
                                   1376 ;	..\src\peripherals\GoldSequence.c:73: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0056,0b111101001001011,FALSE);
      005B95 90 03 C7         [24] 1377 	mov	dptr,#_WriteMemory_PARM_2
      005B98 74 4B            [12] 1378 	mov	a,#0x4b
      005B9A F0               [24] 1379 	movx	@dptr,a
      005B9B 74 7A            [12] 1380 	mov	a,#0x7a
      005B9D A3               [24] 1381 	inc	dptr
      005B9E F0               [24] 1382 	movx	@dptr,a
      005B9F 90 03 C9         [24] 1383 	mov	dptr,#_WriteMemory_PARM_3
      005BA2 E4               [12] 1384 	clr	a
      005BA3 F0               [24] 1385 	movx	@dptr,a
      005BA4 90 1C 56         [24] 1386 	mov	dptr,#0x1c56
      005BA7 12 5E 5A         [24] 1387 	lcall	_WriteMemory
                                   1388 ;	..\src\peripherals\GoldSequence.c:74: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0058,0b0111110000001100,FALSE);
      005BAA 90 03 C7         [24] 1389 	mov	dptr,#_WriteMemory_PARM_2
      005BAD 74 0C            [12] 1390 	mov	a,#0x0c
      005BAF F0               [24] 1391 	movx	@dptr,a
      005BB0 74 7C            [12] 1392 	mov	a,#0x7c
      005BB2 A3               [24] 1393 	inc	dptr
      005BB3 F0               [24] 1394 	movx	@dptr,a
      005BB4 90 03 C9         [24] 1395 	mov	dptr,#_WriteMemory_PARM_3
      005BB7 E4               [12] 1396 	clr	a
      005BB8 F0               [24] 1397 	movx	@dptr,a
      005BB9 90 1C 58         [24] 1398 	mov	dptr,#0x1c58
      005BBC 12 5E 5A         [24] 1399 	lcall	_WriteMemory
                                   1400 ;	..\src\peripherals\GoldSequence.c:75: WriteMemory(MEM_GOLD_SEQUENCES_START+0x005A,0b110001000010001,FALSE);
      005BBF 90 03 C7         [24] 1401 	mov	dptr,#_WriteMemory_PARM_2
      005BC2 74 11            [12] 1402 	mov	a,#0x11
      005BC4 F0               [24] 1403 	movx	@dptr,a
      005BC5 74 62            [12] 1404 	mov	a,#0x62
      005BC7 A3               [24] 1405 	inc	dptr
      005BC8 F0               [24] 1406 	movx	@dptr,a
      005BC9 90 03 C9         [24] 1407 	mov	dptr,#_WriteMemory_PARM_3
      005BCC E4               [12] 1408 	clr	a
      005BCD F0               [24] 1409 	movx	@dptr,a
      005BCE 90 1C 5A         [24] 1410 	mov	dptr,#0x1c5a
      005BD1 12 5E 5A         [24] 1411 	lcall	_WriteMemory
                                   1412 ;	..\src\peripherals\GoldSequence.c:76: WriteMemory(MEM_GOLD_SEQUENCES_START+0x005C,0b0011101101110000,FALSE);
      005BD4 90 03 C7         [24] 1413 	mov	dptr,#_WriteMemory_PARM_2
      005BD7 74 70            [12] 1414 	mov	a,#0x70
      005BD9 F0               [24] 1415 	movx	@dptr,a
      005BDA 74 3B            [12] 1416 	mov	a,#0x3b
      005BDC A3               [24] 1417 	inc	dptr
      005BDD F0               [24] 1418 	movx	@dptr,a
      005BDE 90 03 C9         [24] 1419 	mov	dptr,#_WriteMemory_PARM_3
      005BE1 E4               [12] 1420 	clr	a
      005BE2 F0               [24] 1421 	movx	@dptr,a
      005BE3 90 1C 5C         [24] 1422 	mov	dptr,#0x1c5c
      005BE6 12 5E 5A         [24] 1423 	lcall	_WriteMemory
                                   1424 ;	..\src\peripherals\GoldSequence.c:77: WriteMemory(MEM_GOLD_SEQUENCES_START+0x005E,0b010111000111100,FALSE);
      005BE9 90 03 C7         [24] 1425 	mov	dptr,#_WriteMemory_PARM_2
      005BEC 74 3C            [12] 1426 	mov	a,#0x3c
      005BEE F0               [24] 1427 	movx	@dptr,a
      005BEF 74 2E            [12] 1428 	mov	a,#0x2e
      005BF1 A3               [24] 1429 	inc	dptr
      005BF2 F0               [24] 1430 	movx	@dptr,a
      005BF3 90 03 C9         [24] 1431 	mov	dptr,#_WriteMemory_PARM_3
      005BF6 E4               [12] 1432 	clr	a
      005BF7 F0               [24] 1433 	movx	@dptr,a
      005BF8 90 1C 5E         [24] 1434 	mov	dptr,#0x1c5e
      005BFB 12 5E 5A         [24] 1435 	lcall	_WriteMemory
                                   1436 ;	..\src\peripherals\GoldSequence.c:78: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0060,0b1001100011001110,FALSE);
      005BFE 90 03 C7         [24] 1437 	mov	dptr,#_WriteMemory_PARM_2
      005C01 74 CE            [12] 1438 	mov	a,#0xce
      005C03 F0               [24] 1439 	movx	@dptr,a
      005C04 74 98            [12] 1440 	mov	a,#0x98
      005C06 A3               [24] 1441 	inc	dptr
      005C07 F0               [24] 1442 	movx	@dptr,a
      005C08 90 03 C9         [24] 1443 	mov	dptr,#_WriteMemory_PARM_3
      005C0B E4               [12] 1444 	clr	a
      005C0C F0               [24] 1445 	movx	@dptr,a
      005C0D 90 1C 60         [24] 1446 	mov	dptr,#0x1c60
      005C10 12 5E 5A         [24] 1447 	lcall	_WriteMemory
                                   1448 ;	..\src\peripherals\GoldSequence.c:79: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0062,0b000100000101010,FALSE);
      005C13 90 03 C7         [24] 1449 	mov	dptr,#_WriteMemory_PARM_2
      005C16 74 2A            [12] 1450 	mov	a,#0x2a
      005C18 F0               [24] 1451 	movx	@dptr,a
      005C19 74 08            [12] 1452 	mov	a,#0x08
      005C1B A3               [24] 1453 	inc	dptr
      005C1C F0               [24] 1454 	movx	@dptr,a
      005C1D 90 03 C9         [24] 1455 	mov	dptr,#_WriteMemory_PARM_3
      005C20 E4               [12] 1456 	clr	a
      005C21 F0               [24] 1457 	movx	@dptr,a
      005C22 90 1C 62         [24] 1458 	mov	dptr,#0x1c62
      005C25 12 5E 5A         [24] 1459 	lcall	_WriteMemory
                                   1460 ;	..\src\peripherals\GoldSequence.c:80: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0064,0b1100100100010001,FALSE);
      005C28 90 03 C7         [24] 1461 	mov	dptr,#_WriteMemory_PARM_2
      005C2B 74 11            [12] 1462 	mov	a,#0x11
      005C2D F0               [24] 1463 	movx	@dptr,a
      005C2E 74 C9            [12] 1464 	mov	a,#0xc9
      005C30 A3               [24] 1465 	inc	dptr
      005C31 F0               [24] 1466 	movx	@dptr,a
      005C32 90 03 C9         [24] 1467 	mov	dptr,#_WriteMemory_PARM_3
      005C35 E4               [12] 1468 	clr	a
      005C36 F0               [24] 1469 	movx	@dptr,a
      005C37 90 1C 64         [24] 1470 	mov	dptr,#0x1c64
      005C3A 12 5E 5A         [24] 1471 	lcall	_WriteMemory
                                   1472 ;	..\src\peripherals\GoldSequence.c:81: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0066,0b001101100100001,FALSE);
      005C3D 90 03 C7         [24] 1473 	mov	dptr,#_WriteMemory_PARM_2
      005C40 74 21            [12] 1474 	mov	a,#0x21
      005C42 F0               [24] 1475 	movx	@dptr,a
      005C43 74 1B            [12] 1476 	mov	a,#0x1b
      005C45 A3               [24] 1477 	inc	dptr
      005C46 F0               [24] 1478 	movx	@dptr,a
      005C47 90 03 C9         [24] 1479 	mov	dptr,#_WriteMemory_PARM_3
      005C4A E4               [12] 1480 	clr	a
      005C4B F0               [24] 1481 	movx	@dptr,a
      005C4C 90 1C 66         [24] 1482 	mov	dptr,#0x1c66
      005C4F 12 5E 5A         [24] 1483 	lcall	_WriteMemory
                                   1484 ;	..\src\peripherals\GoldSequence.c:82: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0068,0b0110000111111110,FALSE);
      005C52 90 03 C7         [24] 1485 	mov	dptr,#_WriteMemory_PARM_2
      005C55 74 FE            [12] 1486 	mov	a,#0xfe
      005C57 F0               [24] 1487 	movx	@dptr,a
      005C58 74 61            [12] 1488 	mov	a,#0x61
      005C5A A3               [24] 1489 	inc	dptr
      005C5B F0               [24] 1490 	movx	@dptr,a
      005C5C 90 03 C9         [24] 1491 	mov	dptr,#_WriteMemory_PARM_3
      005C5F E4               [12] 1492 	clr	a
      005C60 F0               [24] 1493 	movx	@dptr,a
      005C61 90 1C 68         [24] 1494 	mov	dptr,#0x1c68
      005C64 12 5E 5A         [24] 1495 	lcall	_WriteMemory
                                   1496 ;	..\src\peripherals\GoldSequence.c:83: WriteMemory(MEM_GOLD_SEQUENCES_START+0x006A,0b101001010100100,FALSE);
      005C67 90 03 C7         [24] 1497 	mov	dptr,#_WriteMemory_PARM_2
      005C6A 74 A4            [12] 1498 	mov	a,#0xa4
      005C6C F0               [24] 1499 	movx	@dptr,a
      005C6D 03               [12] 1500 	rr	a
      005C6E A3               [24] 1501 	inc	dptr
      005C6F F0               [24] 1502 	movx	@dptr,a
      005C70 90 03 C9         [24] 1503 	mov	dptr,#_WriteMemory_PARM_3
      005C73 E4               [12] 1504 	clr	a
      005C74 F0               [24] 1505 	movx	@dptr,a
      005C75 90 1C 6A         [24] 1506 	mov	dptr,#0x1c6a
      005C78 12 5E 5A         [24] 1507 	lcall	_WriteMemory
                                   1508 ;	..\src\peripherals\GoldSequence.c:84: WriteMemory(MEM_GOLD_SEQUENCES_START+0x006C,0b1011010110001001,FALSE);
      005C7B 90 03 C7         [24] 1509 	mov	dptr,#_WriteMemory_PARM_2
      005C7E 74 89            [12] 1510 	mov	a,#0x89
      005C80 F0               [24] 1511 	movx	@dptr,a
      005C81 74 B5            [12] 1512 	mov	a,#0xb5
      005C83 A3               [24] 1513 	inc	dptr
      005C84 F0               [24] 1514 	movx	@dptr,a
      005C85 90 03 C9         [24] 1515 	mov	dptr,#_WriteMemory_PARM_3
      005C88 E4               [12] 1516 	clr	a
      005C89 F0               [24] 1517 	movx	@dptr,a
      005C8A 90 1C 6C         [24] 1518 	mov	dptr,#0x1c6c
      005C8D 12 5E 5A         [24] 1519 	lcall	_WriteMemory
                                   1520 ;	..\src\peripherals\GoldSequence.c:85: WriteMemory(MEM_GOLD_SEQUENCES_START+0x006E,0b011011001100110,FALSE);
      005C90 90 03 C7         [24] 1521 	mov	dptr,#_WriteMemory_PARM_2
      005C93 74 66            [12] 1522 	mov	a,#0x66
      005C95 F0               [24] 1523 	movx	@dptr,a
      005C96 74 36            [12] 1524 	mov	a,#0x36
      005C98 A3               [24] 1525 	inc	dptr
      005C99 F0               [24] 1526 	movx	@dptr,a
      005C9A 90 03 C9         [24] 1527 	mov	dptr,#_WriteMemory_PARM_3
      005C9D E4               [12] 1528 	clr	a
      005C9E F0               [24] 1529 	movx	@dptr,a
      005C9F 90 1C 6E         [24] 1530 	mov	dptr,#0x1c6e
      005CA2 12 5E 5A         [24] 1531 	lcall	_WriteMemory
                                   1532 ;	..\src\peripherals\GoldSequence.c:86: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0070,0b1101111110110010,FALSE);
      005CA5 90 03 C7         [24] 1533 	mov	dptr,#_WriteMemory_PARM_2
      005CA8 74 B2            [12] 1534 	mov	a,#0xb2
      005CAA F0               [24] 1535 	movx	@dptr,a
      005CAB 74 DF            [12] 1536 	mov	a,#0xdf
      005CAD A3               [24] 1537 	inc	dptr
      005CAE F0               [24] 1538 	movx	@dptr,a
      005CAF 90 03 C9         [24] 1539 	mov	dptr,#_WriteMemory_PARM_3
      005CB2 E4               [12] 1540 	clr	a
      005CB3 F0               [24] 1541 	movx	@dptr,a
      005CB4 90 1C 70         [24] 1542 	mov	dptr,#0x1c70
      005CB7 12 5E 5A         [24] 1543 	lcall	_WriteMemory
                                   1544 ;	..\src\peripherals\GoldSequence.c:87: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0072,0b100010000000111,FALSE);
      005CBA 90 03 C7         [24] 1545 	mov	dptr,#_WriteMemory_PARM_2
      005CBD 74 07            [12] 1546 	mov	a,#0x07
      005CBF F0               [24] 1547 	movx	@dptr,a
      005CC0 74 44            [12] 1548 	mov	a,#0x44
      005CC2 A3               [24] 1549 	inc	dptr
      005CC3 F0               [24] 1550 	movx	@dptr,a
      005CC4 90 03 C9         [24] 1551 	mov	dptr,#_WriteMemory_PARM_3
      005CC7 E4               [12] 1552 	clr	a
      005CC8 F0               [24] 1553 	movx	@dptr,a
      005CC9 90 1C 72         [24] 1554 	mov	dptr,#0x1c72
      005CCC 12 5E 5A         [24] 1555 	lcall	_WriteMemory
                                   1556 ;	..\src\peripherals\GoldSequence.c:88: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0074,0b0110101010101111,FALSE);
      005CCF 90 03 C7         [24] 1557 	mov	dptr,#_WriteMemory_PARM_2
      005CD2 74 AF            [12] 1558 	mov	a,#0xaf
      005CD4 F0               [24] 1559 	movx	@dptr,a
      005CD5 74 6A            [12] 1560 	mov	a,#0x6a
      005CD7 A3               [24] 1561 	inc	dptr
      005CD8 F0               [24] 1562 	movx	@dptr,a
      005CD9 90 03 C9         [24] 1563 	mov	dptr,#_WriteMemory_PARM_3
      005CDC E4               [12] 1564 	clr	a
      005CDD F0               [24] 1565 	movx	@dptr,a
      005CDE 90 1C 74         [24] 1566 	mov	dptr,#0x1c74
      005CE1 12 5E 5A         [24] 1567 	lcall	_WriteMemory
                                   1568 ;	..\src\peripherals\GoldSequence.c:89: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0076,0b011110100110111,FALSE);
      005CE4 90 03 C7         [24] 1569 	mov	dptr,#_WriteMemory_PARM_2
      005CE7 74 37            [12] 1570 	mov	a,#0x37
      005CE9 F0               [24] 1571 	movx	@dptr,a
      005CEA 74 3D            [12] 1572 	mov	a,#0x3d
      005CEC A3               [24] 1573 	inc	dptr
      005CED F0               [24] 1574 	movx	@dptr,a
      005CEE 90 03 C9         [24] 1575 	mov	dptr,#_WriteMemory_PARM_3
      005CF1 E4               [12] 1576 	clr	a
      005CF2 F0               [24] 1577 	movx	@dptr,a
      005CF3 90 1C 76         [24] 1578 	mov	dptr,#0x1c76
      005CF6 12 5E 5A         [24] 1579 	lcall	_WriteMemory
                                   1580 ;	..\src\peripherals\GoldSequence.c:90: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0078,0b0011000000100001,FALSE);
      005CF9 90 03 C7         [24] 1581 	mov	dptr,#_WriteMemory_PARM_2
      005CFC 74 21            [12] 1582 	mov	a,#0x21
      005CFE F0               [24] 1583 	movx	@dptr,a
      005CFF 74 30            [12] 1584 	mov	a,#0x30
      005D01 A3               [24] 1585 	inc	dptr
      005D02 F0               [24] 1586 	movx	@dptr,a
      005D03 90 03 C9         [24] 1587 	mov	dptr,#_WriteMemory_PARM_3
      005D06 E4               [12] 1588 	clr	a
      005D07 F0               [24] 1589 	movx	@dptr,a
      005D08 90 1C 78         [24] 1590 	mov	dptr,#0x1c78
      005D0B 12 5E 5A         [24] 1591 	lcall	_WriteMemory
                                   1592 ;	..\src\peripherals\GoldSequence.c:91: WriteMemory(MEM_GOLD_SEQUENCES_START+0x007A,0b100000110101111,FALSE);
      005D0E 90 03 C7         [24] 1593 	mov	dptr,#_WriteMemory_PARM_2
      005D11 74 AF            [12] 1594 	mov	a,#0xaf
      005D13 F0               [24] 1595 	movx	@dptr,a
      005D14 74 41            [12] 1596 	mov	a,#0x41
      005D16 A3               [24] 1597 	inc	dptr
      005D17 F0               [24] 1598 	movx	@dptr,a
      005D18 90 03 C9         [24] 1599 	mov	dptr,#_WriteMemory_PARM_3
      005D1B E4               [12] 1600 	clr	a
      005D1C F0               [24] 1601 	movx	@dptr,a
      005D1D 90 1C 7A         [24] 1602 	mov	dptr,#0x1c7a
      005D20 12 5E 5A         [24] 1603 	lcall	_WriteMemory
                                   1604 ;	..\src\peripherals\GoldSequence.c:92: WriteMemory(MEM_GOLD_SEQUENCES_START+0x007C,0b0001110101100110,FALSE);
      005D23 90 03 C7         [24] 1605 	mov	dptr,#_WriteMemory_PARM_2
      005D26 74 66            [12] 1606 	mov	a,#0x66
      005D28 F0               [24] 1607 	movx	@dptr,a
      005D29 74 1D            [12] 1608 	mov	a,#0x1d
      005D2B A3               [24] 1609 	inc	dptr
      005D2C F0               [24] 1610 	movx	@dptr,a
      005D2D 90 03 C9         [24] 1611 	mov	dptr,#_WriteMemory_PARM_3
      005D30 E4               [12] 1612 	clr	a
      005D31 F0               [24] 1613 	movx	@dptr,a
      005D32 90 1C 7C         [24] 1614 	mov	dptr,#0x1c7c
      005D35 12 5E 5A         [24] 1615 	lcall	_WriteMemory
                                   1616 ;	..\src\peripherals\GoldSequence.c:93: WriteMemory(MEM_GOLD_SEQUENCES_START+0x007E,0b111111111100011,FALSE);
      005D38 90 03 C7         [24] 1617 	mov	dptr,#_WriteMemory_PARM_2
      005D3B 74 E3            [12] 1618 	mov	a,#0xe3
      005D3D F0               [24] 1619 	movx	@dptr,a
      005D3E 74 7F            [12] 1620 	mov	a,#0x7f
      005D40 A3               [24] 1621 	inc	dptr
      005D41 F0               [24] 1622 	movx	@dptr,a
      005D42 90 03 C9         [24] 1623 	mov	dptr,#_WriteMemory_PARM_3
      005D45 E4               [12] 1624 	clr	a
      005D46 F0               [24] 1625 	movx	@dptr,a
      005D47 90 1C 7E         [24] 1626 	mov	dptr,#0x1c7e
      005D4A 12 5E 5A         [24] 1627 	lcall	_WriteMemory
                                   1628 ;	..\src\peripherals\GoldSequence.c:94: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0080,0b0000101111000101,FALSE);
      005D4D 90 03 C7         [24] 1629 	mov	dptr,#_WriteMemory_PARM_2
      005D50 74 C5            [12] 1630 	mov	a,#0xc5
      005D52 F0               [24] 1631 	movx	@dptr,a
      005D53 74 0B            [12] 1632 	mov	a,#0x0b
      005D55 A3               [24] 1633 	inc	dptr
      005D56 F0               [24] 1634 	movx	@dptr,a
      005D57 90 03 C9         [24] 1635 	mov	dptr,#_WriteMemory_PARM_3
      005D5A E4               [12] 1636 	clr	a
      005D5B F0               [24] 1637 	movx	@dptr,a
      005D5C 90 1C 80         [24] 1638 	mov	dptr,#0x1c80
      005D5F 12 5E 5A         [24] 1639 	lcall	_WriteMemory
                                   1640 ;	..\src\peripherals\GoldSequence.c:95: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0082,0b010000011000101,FALSE);
      005D62 90 03 C7         [24] 1641 	mov	dptr,#_WriteMemory_PARM_2
      005D65 74 C5            [12] 1642 	mov	a,#0xc5
      005D67 F0               [24] 1643 	movx	@dptr,a
      005D68 74 20            [12] 1644 	mov	a,#0x20
      005D6A A3               [24] 1645 	inc	dptr
      005D6B F0               [24] 1646 	movx	@dptr,a
      005D6C 90 03 C9         [24] 1647 	mov	dptr,#_WriteMemory_PARM_3
      005D6F E4               [12] 1648 	clr	a
      005D70 F0               [24] 1649 	movx	@dptr,a
      005D71 90 1C 82         [24] 1650 	mov	dptr,#0x1c82
      005D74 12 5E 5A         [24] 1651 	lcall	_WriteMemory
                                   1652 ;	..\src\peripherals\GoldSequence.c:96: flash_lock();
                                   1653 ;	..\src\peripherals\GoldSequence.c:99: }
      005D77 02 8A 91         [24] 1654 	ljmp	_flash_lock
                                   1655 ;------------------------------------------------------------
                                   1656 ;Allocation info for local variables in function 'GOLDSEQUENCE_Obtain'
                                   1657 ;------------------------------------------------------------
                                   1658 ;Rnd                       Allocated with name '_GOLDSEQUENCE_Obtain_Rnd_65536_132'
                                   1659 ;RetVal                    Allocated with name '_GOLDSEQUENCE_Obtain_RetVal_65536_132'
                                   1660 ;------------------------------------------------------------
                                   1661 ;	..\src\peripherals\GoldSequence.c:108: __xdata uint32_t GOLDSEQUENCE_Obtain(void)
                                   1662 ;	-----------------------------------------
                                   1663 ;	 function GOLDSEQUENCE_Obtain
                                   1664 ;	-----------------------------------------
      005D7A                       1665 _GOLDSEQUENCE_Obtain:
                                   1666 ;	..\src\peripherals\GoldSequence.c:113: Rnd = RNGBYTE;
      005D7A 90 70 81         [24] 1667 	mov	dptr,#_RNGBYTE
      005D7D E0               [24] 1668 	movx	a,@dptr
      005D7E FF               [12] 1669 	mov	r7,a
      005D7F 7E 00            [12] 1670 	mov	r6,#0x00
                                   1671 ;	..\src\peripherals\GoldSequence.c:114: flash_unlock();
      005D81 C0 07            [24] 1672 	push	ar7
      005D83 C0 06            [24] 1673 	push	ar6
      005D85 12 7F E9         [24] 1674 	lcall	_flash_unlock
      005D88 D0 06            [24] 1675 	pop	ar6
      005D8A D0 07            [24] 1676 	pop	ar7
                                   1677 ;	..\src\peripherals\GoldSequence.c:115: Rnd = ((Rnd % 33)*4);  // MOD operation is done so that the number is between 0 and 33, once a number between 0 and 32 has been obtained, its multiplied by 4 to match the memory address
      005D8C 90 05 0C         [24] 1678 	mov	dptr,#__moduint_PARM_2
      005D8F 74 21            [12] 1679 	mov	a,#0x21
      005D91 F0               [24] 1680 	movx	@dptr,a
      005D92 E4               [12] 1681 	clr	a
      005D93 A3               [24] 1682 	inc	dptr
      005D94 F0               [24] 1683 	movx	@dptr,a
      005D95 8F 82            [24] 1684 	mov	dpl,r7
      005D97 8E 83            [24] 1685 	mov	dph,r6
      005D99 12 83 01         [24] 1686 	lcall	__moduint
      005D9C AE 82            [24] 1687 	mov	r6,dpl
      005D9E AF 83            [24] 1688 	mov	r7,dph
      005DA0 EE               [12] 1689 	mov	a,r6
      005DA1 2E               [12] 1690 	add	a,r6
      005DA2 FE               [12] 1691 	mov	r6,a
      005DA3 EF               [12] 1692 	mov	a,r7
      005DA4 33               [12] 1693 	rlc	a
      005DA5 FF               [12] 1694 	mov	r7,a
      005DA6 EE               [12] 1695 	mov	a,r6
      005DA7 2E               [12] 1696 	add	a,r6
      005DA8 FE               [12] 1697 	mov	r6,a
      005DA9 EF               [12] 1698 	mov	a,r7
      005DAA 33               [12] 1699 	rlc	a
      005DAB FF               [12] 1700 	mov	r7,a
                                   1701 ;	..\src\peripherals\GoldSequence.c:116: RetVal = ((uint32_t)flash_read(MEM_GOLD_SEQUENCES_START+Rnd))<<16;
      005DAC 8E 04            [24] 1702 	mov	ar4,r6
      005DAE 74 1C            [12] 1703 	mov	a,#0x1c
      005DB0 2F               [12] 1704 	add	a,r7
      005DB1 FD               [12] 1705 	mov	r5,a
      005DB2 8C 82            [24] 1706 	mov	dpl,r4
      005DB4 8D 83            [24] 1707 	mov	dph,r5
      005DB6 C0 07            [24] 1708 	push	ar7
      005DB8 C0 06            [24] 1709 	push	ar6
      005DBA 12 86 FB         [24] 1710 	lcall	_flash_read
      005DBD AC 82            [24] 1711 	mov	r4,dpl
      005DBF AD 83            [24] 1712 	mov	r5,dph
      005DC1 D0 06            [24] 1713 	pop	ar6
      005DC3 D0 07            [24] 1714 	pop	ar7
      005DC5 8D 02            [24] 1715 	mov	ar2,r5
      005DC7 8C 03            [24] 1716 	mov	ar3,r4
                                   1717 ;	..\src\peripherals\GoldSequence.c:117: RetVal |= (((uint32_t)flash_read(MEM_GOLD_SEQUENCES_START+Rnd+2)) & 0x0000FFFF)<<1;
      005DC9 E4               [12] 1718 	clr	a
      005DCA FC               [12] 1719 	mov	r4,a
      005DCB FD               [12] 1720 	mov	r5,a
      005DCC 74 02            [12] 1721 	mov	a,#0x02
      005DCE 2E               [12] 1722 	add	a,r6
      005DCF FE               [12] 1723 	mov	r6,a
      005DD0 74 1C            [12] 1724 	mov	a,#0x1c
      005DD2 3F               [12] 1725 	addc	a,r7
      005DD3 FF               [12] 1726 	mov	r7,a
      005DD4 8E 82            [24] 1727 	mov	dpl,r6
      005DD6 8F 83            [24] 1728 	mov	dph,r7
      005DD8 C0 05            [24] 1729 	push	ar5
      005DDA C0 04            [24] 1730 	push	ar4
      005DDC C0 03            [24] 1731 	push	ar3
      005DDE C0 02            [24] 1732 	push	ar2
      005DE0 12 86 FB         [24] 1733 	lcall	_flash_read
      005DE3 AE 82            [24] 1734 	mov	r6,dpl
      005DE5 AF 83            [24] 1735 	mov	r7,dph
      005DE7 D0 02            [24] 1736 	pop	ar2
      005DE9 D0 03            [24] 1737 	pop	ar3
      005DEB D0 04            [24] 1738 	pop	ar4
      005DED D0 05            [24] 1739 	pop	ar5
      005DEF 8E 00            [24] 1740 	mov	ar0,r6
      005DF1 8F 01            [24] 1741 	mov	ar1,r7
      005DF3 E4               [12] 1742 	clr	a
      005DF4 FE               [12] 1743 	mov	r6,a
      005DF5 FF               [12] 1744 	mov	r7,a
      005DF6 E8               [12] 1745 	mov	a,r0
      005DF7 28               [12] 1746 	add	a,r0
      005DF8 F8               [12] 1747 	mov	r0,a
      005DF9 E9               [12] 1748 	mov	a,r1
      005DFA 33               [12] 1749 	rlc	a
      005DFB F9               [12] 1750 	mov	r1,a
      005DFC EE               [12] 1751 	mov	a,r6
      005DFD 33               [12] 1752 	rlc	a
      005DFE FE               [12] 1753 	mov	r6,a
      005DFF EF               [12] 1754 	mov	a,r7
      005E00 33               [12] 1755 	rlc	a
      005E01 FF               [12] 1756 	mov	r7,a
      005E02 E8               [12] 1757 	mov	a,r0
      005E03 42 04            [12] 1758 	orl	ar4,a
      005E05 E9               [12] 1759 	mov	a,r1
      005E06 42 05            [12] 1760 	orl	ar5,a
      005E08 EE               [12] 1761 	mov	a,r6
      005E09 42 03            [12] 1762 	orl	ar3,a
      005E0B EF               [12] 1763 	mov	a,r7
      005E0C 42 02            [12] 1764 	orl	ar2,a
                                   1765 ;	..\src\peripherals\GoldSequence.c:118: flash_lock();
      005E0E C0 05            [24] 1766 	push	ar5
      005E10 C0 04            [24] 1767 	push	ar4
      005E12 C0 03            [24] 1768 	push	ar3
      005E14 C0 02            [24] 1769 	push	ar2
      005E16 12 8A 91         [24] 1770 	lcall	_flash_lock
      005E19 D0 02            [24] 1771 	pop	ar2
      005E1B D0 03            [24] 1772 	pop	ar3
      005E1D D0 04            [24] 1773 	pop	ar4
      005E1F D0 05            [24] 1774 	pop	ar5
                                   1775 ;	..\src\peripherals\GoldSequence.c:119: return(RetVal);
      005E21 8C 82            [24] 1776 	mov	dpl,r4
      005E23 8D 83            [24] 1777 	mov	dph,r5
      005E25 8B F0            [24] 1778 	mov	b,r3
      005E27 EA               [12] 1779 	mov	a,r2
                                   1780 ;	..\src\peripherals\GoldSequence.c:120: }
      005E28 22               [24] 1781 	ret
                                   1782 	.area CSEG    (CODE)
                                   1783 	.area CONST   (CODE)
                                   1784 	.area XINIT   (CODE)
                                   1785 	.area CABS    (ABS,CODE)
