                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.9.3 #11354 (MINGW32)
                                      4 ;--------------------------------------------------------
                                      5 	.module Beacon
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _dbglink_writehex16
                                     12 	.globl _dbglink_writestr
                                     13 	.globl _memcpy
                                     14 	.globl _PORTC_7
                                     15 	.globl _PORTC_6
                                     16 	.globl _PORTC_5
                                     17 	.globl _PORTC_4
                                     18 	.globl _PORTC_3
                                     19 	.globl _PORTC_2
                                     20 	.globl _PORTC_1
                                     21 	.globl _PORTC_0
                                     22 	.globl _PORTB_7
                                     23 	.globl _PORTB_6
                                     24 	.globl _PORTB_5
                                     25 	.globl _PORTB_4
                                     26 	.globl _PORTB_3
                                     27 	.globl _PORTB_2
                                     28 	.globl _PORTB_1
                                     29 	.globl _PORTB_0
                                     30 	.globl _PORTA_7
                                     31 	.globl _PORTA_6
                                     32 	.globl _PORTA_5
                                     33 	.globl _PORTA_4
                                     34 	.globl _PORTA_3
                                     35 	.globl _PORTA_2
                                     36 	.globl _PORTA_1
                                     37 	.globl _PORTA_0
                                     38 	.globl _PINC_7
                                     39 	.globl _PINC_6
                                     40 	.globl _PINC_5
                                     41 	.globl _PINC_4
                                     42 	.globl _PINC_3
                                     43 	.globl _PINC_2
                                     44 	.globl _PINC_1
                                     45 	.globl _PINC_0
                                     46 	.globl _PINB_7
                                     47 	.globl _PINB_6
                                     48 	.globl _PINB_5
                                     49 	.globl _PINB_4
                                     50 	.globl _PINB_3
                                     51 	.globl _PINB_2
                                     52 	.globl _PINB_1
                                     53 	.globl _PINB_0
                                     54 	.globl _PINA_7
                                     55 	.globl _PINA_6
                                     56 	.globl _PINA_5
                                     57 	.globl _PINA_4
                                     58 	.globl _PINA_3
                                     59 	.globl _PINA_2
                                     60 	.globl _PINA_1
                                     61 	.globl _PINA_0
                                     62 	.globl _CY
                                     63 	.globl _AC
                                     64 	.globl _F0
                                     65 	.globl _RS1
                                     66 	.globl _RS0
                                     67 	.globl _OV
                                     68 	.globl _F1
                                     69 	.globl _P
                                     70 	.globl _IP_7
                                     71 	.globl _IP_6
                                     72 	.globl _IP_5
                                     73 	.globl _IP_4
                                     74 	.globl _IP_3
                                     75 	.globl _IP_2
                                     76 	.globl _IP_1
                                     77 	.globl _IP_0
                                     78 	.globl _EA
                                     79 	.globl _IE_7
                                     80 	.globl _IE_6
                                     81 	.globl _IE_5
                                     82 	.globl _IE_4
                                     83 	.globl _IE_3
                                     84 	.globl _IE_2
                                     85 	.globl _IE_1
                                     86 	.globl _IE_0
                                     87 	.globl _EIP_7
                                     88 	.globl _EIP_6
                                     89 	.globl _EIP_5
                                     90 	.globl _EIP_4
                                     91 	.globl _EIP_3
                                     92 	.globl _EIP_2
                                     93 	.globl _EIP_1
                                     94 	.globl _EIP_0
                                     95 	.globl _EIE_7
                                     96 	.globl _EIE_6
                                     97 	.globl _EIE_5
                                     98 	.globl _EIE_4
                                     99 	.globl _EIE_3
                                    100 	.globl _EIE_2
                                    101 	.globl _EIE_1
                                    102 	.globl _EIE_0
                                    103 	.globl _E2IP_7
                                    104 	.globl _E2IP_6
                                    105 	.globl _E2IP_5
                                    106 	.globl _E2IP_4
                                    107 	.globl _E2IP_3
                                    108 	.globl _E2IP_2
                                    109 	.globl _E2IP_1
                                    110 	.globl _E2IP_0
                                    111 	.globl _E2IE_7
                                    112 	.globl _E2IE_6
                                    113 	.globl _E2IE_5
                                    114 	.globl _E2IE_4
                                    115 	.globl _E2IE_3
                                    116 	.globl _E2IE_2
                                    117 	.globl _E2IE_1
                                    118 	.globl _E2IE_0
                                    119 	.globl _B_7
                                    120 	.globl _B_6
                                    121 	.globl _B_5
                                    122 	.globl _B_4
                                    123 	.globl _B_3
                                    124 	.globl _B_2
                                    125 	.globl _B_1
                                    126 	.globl _B_0
                                    127 	.globl _ACC_7
                                    128 	.globl _ACC_6
                                    129 	.globl _ACC_5
                                    130 	.globl _ACC_4
                                    131 	.globl _ACC_3
                                    132 	.globl _ACC_2
                                    133 	.globl _ACC_1
                                    134 	.globl _ACC_0
                                    135 	.globl _WTSTAT
                                    136 	.globl _WTIRQEN
                                    137 	.globl _WTEVTD
                                    138 	.globl _WTEVTD1
                                    139 	.globl _WTEVTD0
                                    140 	.globl _WTEVTC
                                    141 	.globl _WTEVTC1
                                    142 	.globl _WTEVTC0
                                    143 	.globl _WTEVTB
                                    144 	.globl _WTEVTB1
                                    145 	.globl _WTEVTB0
                                    146 	.globl _WTEVTA
                                    147 	.globl _WTEVTA1
                                    148 	.globl _WTEVTA0
                                    149 	.globl _WTCNTR1
                                    150 	.globl _WTCNTB
                                    151 	.globl _WTCNTB1
                                    152 	.globl _WTCNTB0
                                    153 	.globl _WTCNTA
                                    154 	.globl _WTCNTA1
                                    155 	.globl _WTCNTA0
                                    156 	.globl _WTCFGB
                                    157 	.globl _WTCFGA
                                    158 	.globl _WDTRESET
                                    159 	.globl _WDTCFG
                                    160 	.globl _U1STATUS
                                    161 	.globl _U1SHREG
                                    162 	.globl _U1MODE
                                    163 	.globl _U1CTRL
                                    164 	.globl _U0STATUS
                                    165 	.globl _U0SHREG
                                    166 	.globl _U0MODE
                                    167 	.globl _U0CTRL
                                    168 	.globl _T2STATUS
                                    169 	.globl _T2PERIOD
                                    170 	.globl _T2PERIOD1
                                    171 	.globl _T2PERIOD0
                                    172 	.globl _T2MODE
                                    173 	.globl _T2CNT
                                    174 	.globl _T2CNT1
                                    175 	.globl _T2CNT0
                                    176 	.globl _T2CLKSRC
                                    177 	.globl _T1STATUS
                                    178 	.globl _T1PERIOD
                                    179 	.globl _T1PERIOD1
                                    180 	.globl _T1PERIOD0
                                    181 	.globl _T1MODE
                                    182 	.globl _T1CNT
                                    183 	.globl _T1CNT1
                                    184 	.globl _T1CNT0
                                    185 	.globl _T1CLKSRC
                                    186 	.globl _T0STATUS
                                    187 	.globl _T0PERIOD
                                    188 	.globl _T0PERIOD1
                                    189 	.globl _T0PERIOD0
                                    190 	.globl _T0MODE
                                    191 	.globl _T0CNT
                                    192 	.globl _T0CNT1
                                    193 	.globl _T0CNT0
                                    194 	.globl _T0CLKSRC
                                    195 	.globl _SPSTATUS
                                    196 	.globl _SPSHREG
                                    197 	.globl _SPMODE
                                    198 	.globl _SPCLKSRC
                                    199 	.globl _RADIOSTAT
                                    200 	.globl _RADIOSTAT1
                                    201 	.globl _RADIOSTAT0
                                    202 	.globl _RADIODATA
                                    203 	.globl _RADIODATA3
                                    204 	.globl _RADIODATA2
                                    205 	.globl _RADIODATA1
                                    206 	.globl _RADIODATA0
                                    207 	.globl _RADIOADDR
                                    208 	.globl _RADIOADDR1
                                    209 	.globl _RADIOADDR0
                                    210 	.globl _RADIOACC
                                    211 	.globl _OC1STATUS
                                    212 	.globl _OC1PIN
                                    213 	.globl _OC1MODE
                                    214 	.globl _OC1COMP
                                    215 	.globl _OC1COMP1
                                    216 	.globl _OC1COMP0
                                    217 	.globl _OC0STATUS
                                    218 	.globl _OC0PIN
                                    219 	.globl _OC0MODE
                                    220 	.globl _OC0COMP
                                    221 	.globl _OC0COMP1
                                    222 	.globl _OC0COMP0
                                    223 	.globl _NVSTATUS
                                    224 	.globl _NVKEY
                                    225 	.globl _NVDATA
                                    226 	.globl _NVDATA1
                                    227 	.globl _NVDATA0
                                    228 	.globl _NVADDR
                                    229 	.globl _NVADDR1
                                    230 	.globl _NVADDR0
                                    231 	.globl _IC1STATUS
                                    232 	.globl _IC1MODE
                                    233 	.globl _IC1CAPT
                                    234 	.globl _IC1CAPT1
                                    235 	.globl _IC1CAPT0
                                    236 	.globl _IC0STATUS
                                    237 	.globl _IC0MODE
                                    238 	.globl _IC0CAPT
                                    239 	.globl _IC0CAPT1
                                    240 	.globl _IC0CAPT0
                                    241 	.globl _PORTR
                                    242 	.globl _PORTC
                                    243 	.globl _PORTB
                                    244 	.globl _PORTA
                                    245 	.globl _PINR
                                    246 	.globl _PINC
                                    247 	.globl _PINB
                                    248 	.globl _PINA
                                    249 	.globl _DIRR
                                    250 	.globl _DIRC
                                    251 	.globl _DIRB
                                    252 	.globl _DIRA
                                    253 	.globl _DBGLNKSTAT
                                    254 	.globl _DBGLNKBUF
                                    255 	.globl _CODECONFIG
                                    256 	.globl _CLKSTAT
                                    257 	.globl _CLKCON
                                    258 	.globl _ANALOGCOMP
                                    259 	.globl _ADCCONV
                                    260 	.globl _ADCCLKSRC
                                    261 	.globl _ADCCH3CONFIG
                                    262 	.globl _ADCCH2CONFIG
                                    263 	.globl _ADCCH1CONFIG
                                    264 	.globl _ADCCH0CONFIG
                                    265 	.globl __XPAGE
                                    266 	.globl _XPAGE
                                    267 	.globl _SP
                                    268 	.globl _PSW
                                    269 	.globl _PCON
                                    270 	.globl _IP
                                    271 	.globl _IE
                                    272 	.globl _EIP
                                    273 	.globl _EIE
                                    274 	.globl _E2IP
                                    275 	.globl _E2IE
                                    276 	.globl _DPS
                                    277 	.globl _DPTR1
                                    278 	.globl _DPTR0
                                    279 	.globl _DPL1
                                    280 	.globl _DPL
                                    281 	.globl _DPH1
                                    282 	.globl _DPH
                                    283 	.globl _B
                                    284 	.globl _ACC
                                    285 	.globl _VerifyACK_PARM_3
                                    286 	.globl _VerifyACK_PARM_2
                                    287 	.globl _BEACON_decoding_PARM_3
                                    288 	.globl _BEACON_decoding_PARM_2
                                    289 	.globl _RNGCLKSRC1
                                    290 	.globl _RNGCLKSRC0
                                    291 	.globl _RNGMODE
                                    292 	.globl _AESOUTADDR
                                    293 	.globl _AESOUTADDR1
                                    294 	.globl _AESOUTADDR0
                                    295 	.globl _AESMODE
                                    296 	.globl _AESKEYADDR
                                    297 	.globl _AESKEYADDR1
                                    298 	.globl _AESKEYADDR0
                                    299 	.globl _AESINADDR
                                    300 	.globl _AESINADDR1
                                    301 	.globl _AESINADDR0
                                    302 	.globl _AESCURBLOCK
                                    303 	.globl _AESCONFIG
                                    304 	.globl _RNGBYTE
                                    305 	.globl _XTALREADY
                                    306 	.globl _XTALOSC
                                    307 	.globl _XTALAMPL
                                    308 	.globl _SILICONREV
                                    309 	.globl _SCRATCH3
                                    310 	.globl _SCRATCH2
                                    311 	.globl _SCRATCH1
                                    312 	.globl _SCRATCH0
                                    313 	.globl _RADIOMUX
                                    314 	.globl _RADIOFSTATADDR
                                    315 	.globl _RADIOFSTATADDR1
                                    316 	.globl _RADIOFSTATADDR0
                                    317 	.globl _RADIOFDATAADDR
                                    318 	.globl _RADIOFDATAADDR1
                                    319 	.globl _RADIOFDATAADDR0
                                    320 	.globl _OSCRUN
                                    321 	.globl _OSCREADY
                                    322 	.globl _OSCFORCERUN
                                    323 	.globl _OSCCALIB
                                    324 	.globl _MISCCTRL
                                    325 	.globl _LPXOSCGM
                                    326 	.globl _LPOSCREF
                                    327 	.globl _LPOSCREF1
                                    328 	.globl _LPOSCREF0
                                    329 	.globl _LPOSCPER
                                    330 	.globl _LPOSCPER1
                                    331 	.globl _LPOSCPER0
                                    332 	.globl _LPOSCKFILT
                                    333 	.globl _LPOSCKFILT1
                                    334 	.globl _LPOSCKFILT0
                                    335 	.globl _LPOSCFREQ
                                    336 	.globl _LPOSCFREQ1
                                    337 	.globl _LPOSCFREQ0
                                    338 	.globl _LPOSCCONFIG
                                    339 	.globl _PINSEL
                                    340 	.globl _PINCHGC
                                    341 	.globl _PINCHGB
                                    342 	.globl _PINCHGA
                                    343 	.globl _PALTRADIO
                                    344 	.globl _PALTC
                                    345 	.globl _PALTB
                                    346 	.globl _PALTA
                                    347 	.globl _INTCHGC
                                    348 	.globl _INTCHGB
                                    349 	.globl _INTCHGA
                                    350 	.globl _EXTIRQ
                                    351 	.globl _GPIOENABLE
                                    352 	.globl _ANALOGA
                                    353 	.globl _FRCOSCREF
                                    354 	.globl _FRCOSCREF1
                                    355 	.globl _FRCOSCREF0
                                    356 	.globl _FRCOSCPER
                                    357 	.globl _FRCOSCPER1
                                    358 	.globl _FRCOSCPER0
                                    359 	.globl _FRCOSCKFILT
                                    360 	.globl _FRCOSCKFILT1
                                    361 	.globl _FRCOSCKFILT0
                                    362 	.globl _FRCOSCFREQ
                                    363 	.globl _FRCOSCFREQ1
                                    364 	.globl _FRCOSCFREQ0
                                    365 	.globl _FRCOSCCTRL
                                    366 	.globl _FRCOSCCONFIG
                                    367 	.globl _DMA1CONFIG
                                    368 	.globl _DMA1ADDR
                                    369 	.globl _DMA1ADDR1
                                    370 	.globl _DMA1ADDR0
                                    371 	.globl _DMA0CONFIG
                                    372 	.globl _DMA0ADDR
                                    373 	.globl _DMA0ADDR1
                                    374 	.globl _DMA0ADDR0
                                    375 	.globl _ADCTUNE2
                                    376 	.globl _ADCTUNE1
                                    377 	.globl _ADCTUNE0
                                    378 	.globl _ADCCH3VAL
                                    379 	.globl _ADCCH3VAL1
                                    380 	.globl _ADCCH3VAL0
                                    381 	.globl _ADCCH2VAL
                                    382 	.globl _ADCCH2VAL1
                                    383 	.globl _ADCCH2VAL0
                                    384 	.globl _ADCCH1VAL
                                    385 	.globl _ADCCH1VAL1
                                    386 	.globl _ADCCH1VAL0
                                    387 	.globl _ADCCH0VAL
                                    388 	.globl _ADCCH0VAL1
                                    389 	.globl _ADCCH0VAL0
                                    390 	.globl _BEACON_decoding
                                    391 	.globl _VerifyACK
                                    392 ;--------------------------------------------------------
                                    393 ; special function registers
                                    394 ;--------------------------------------------------------
                                    395 	.area RSEG    (ABS,DATA)
      000000                        396 	.org 0x0000
                           0000E0   397 _ACC	=	0x00e0
                           0000F0   398 _B	=	0x00f0
                           000083   399 _DPH	=	0x0083
                           000085   400 _DPH1	=	0x0085
                           000082   401 _DPL	=	0x0082
                           000084   402 _DPL1	=	0x0084
                           008382   403 _DPTR0	=	0x8382
                           008584   404 _DPTR1	=	0x8584
                           000086   405 _DPS	=	0x0086
                           0000A0   406 _E2IE	=	0x00a0
                           0000C0   407 _E2IP	=	0x00c0
                           000098   408 _EIE	=	0x0098
                           0000B0   409 _EIP	=	0x00b0
                           0000A8   410 _IE	=	0x00a8
                           0000B8   411 _IP	=	0x00b8
                           000087   412 _PCON	=	0x0087
                           0000D0   413 _PSW	=	0x00d0
                           000081   414 _SP	=	0x0081
                           0000D9   415 _XPAGE	=	0x00d9
                           0000D9   416 __XPAGE	=	0x00d9
                           0000CA   417 _ADCCH0CONFIG	=	0x00ca
                           0000CB   418 _ADCCH1CONFIG	=	0x00cb
                           0000D2   419 _ADCCH2CONFIG	=	0x00d2
                           0000D3   420 _ADCCH3CONFIG	=	0x00d3
                           0000D1   421 _ADCCLKSRC	=	0x00d1
                           0000C9   422 _ADCCONV	=	0x00c9
                           0000E1   423 _ANALOGCOMP	=	0x00e1
                           0000C6   424 _CLKCON	=	0x00c6
                           0000C7   425 _CLKSTAT	=	0x00c7
                           000097   426 _CODECONFIG	=	0x0097
                           0000E3   427 _DBGLNKBUF	=	0x00e3
                           0000E2   428 _DBGLNKSTAT	=	0x00e2
                           000089   429 _DIRA	=	0x0089
                           00008A   430 _DIRB	=	0x008a
                           00008B   431 _DIRC	=	0x008b
                           00008E   432 _DIRR	=	0x008e
                           0000C8   433 _PINA	=	0x00c8
                           0000E8   434 _PINB	=	0x00e8
                           0000F8   435 _PINC	=	0x00f8
                           00008D   436 _PINR	=	0x008d
                           000080   437 _PORTA	=	0x0080
                           000088   438 _PORTB	=	0x0088
                           000090   439 _PORTC	=	0x0090
                           00008C   440 _PORTR	=	0x008c
                           0000CE   441 _IC0CAPT0	=	0x00ce
                           0000CF   442 _IC0CAPT1	=	0x00cf
                           00CFCE   443 _IC0CAPT	=	0xcfce
                           0000CC   444 _IC0MODE	=	0x00cc
                           0000CD   445 _IC0STATUS	=	0x00cd
                           0000D6   446 _IC1CAPT0	=	0x00d6
                           0000D7   447 _IC1CAPT1	=	0x00d7
                           00D7D6   448 _IC1CAPT	=	0xd7d6
                           0000D4   449 _IC1MODE	=	0x00d4
                           0000D5   450 _IC1STATUS	=	0x00d5
                           000092   451 _NVADDR0	=	0x0092
                           000093   452 _NVADDR1	=	0x0093
                           009392   453 _NVADDR	=	0x9392
                           000094   454 _NVDATA0	=	0x0094
                           000095   455 _NVDATA1	=	0x0095
                           009594   456 _NVDATA	=	0x9594
                           000096   457 _NVKEY	=	0x0096
                           000091   458 _NVSTATUS	=	0x0091
                           0000BC   459 _OC0COMP0	=	0x00bc
                           0000BD   460 _OC0COMP1	=	0x00bd
                           00BDBC   461 _OC0COMP	=	0xbdbc
                           0000B9   462 _OC0MODE	=	0x00b9
                           0000BA   463 _OC0PIN	=	0x00ba
                           0000BB   464 _OC0STATUS	=	0x00bb
                           0000C4   465 _OC1COMP0	=	0x00c4
                           0000C5   466 _OC1COMP1	=	0x00c5
                           00C5C4   467 _OC1COMP	=	0xc5c4
                           0000C1   468 _OC1MODE	=	0x00c1
                           0000C2   469 _OC1PIN	=	0x00c2
                           0000C3   470 _OC1STATUS	=	0x00c3
                           0000B1   471 _RADIOACC	=	0x00b1
                           0000B3   472 _RADIOADDR0	=	0x00b3
                           0000B2   473 _RADIOADDR1	=	0x00b2
                           00B2B3   474 _RADIOADDR	=	0xb2b3
                           0000B7   475 _RADIODATA0	=	0x00b7
                           0000B6   476 _RADIODATA1	=	0x00b6
                           0000B5   477 _RADIODATA2	=	0x00b5
                           0000B4   478 _RADIODATA3	=	0x00b4
                           B4B5B6B7   479 _RADIODATA	=	0xb4b5b6b7
                           0000BE   480 _RADIOSTAT0	=	0x00be
                           0000BF   481 _RADIOSTAT1	=	0x00bf
                           00BFBE   482 _RADIOSTAT	=	0xbfbe
                           0000DF   483 _SPCLKSRC	=	0x00df
                           0000DC   484 _SPMODE	=	0x00dc
                           0000DE   485 _SPSHREG	=	0x00de
                           0000DD   486 _SPSTATUS	=	0x00dd
                           00009A   487 _T0CLKSRC	=	0x009a
                           00009C   488 _T0CNT0	=	0x009c
                           00009D   489 _T0CNT1	=	0x009d
                           009D9C   490 _T0CNT	=	0x9d9c
                           000099   491 _T0MODE	=	0x0099
                           00009E   492 _T0PERIOD0	=	0x009e
                           00009F   493 _T0PERIOD1	=	0x009f
                           009F9E   494 _T0PERIOD	=	0x9f9e
                           00009B   495 _T0STATUS	=	0x009b
                           0000A2   496 _T1CLKSRC	=	0x00a2
                           0000A4   497 _T1CNT0	=	0x00a4
                           0000A5   498 _T1CNT1	=	0x00a5
                           00A5A4   499 _T1CNT	=	0xa5a4
                           0000A1   500 _T1MODE	=	0x00a1
                           0000A6   501 _T1PERIOD0	=	0x00a6
                           0000A7   502 _T1PERIOD1	=	0x00a7
                           00A7A6   503 _T1PERIOD	=	0xa7a6
                           0000A3   504 _T1STATUS	=	0x00a3
                           0000AA   505 _T2CLKSRC	=	0x00aa
                           0000AC   506 _T2CNT0	=	0x00ac
                           0000AD   507 _T2CNT1	=	0x00ad
                           00ADAC   508 _T2CNT	=	0xadac
                           0000A9   509 _T2MODE	=	0x00a9
                           0000AE   510 _T2PERIOD0	=	0x00ae
                           0000AF   511 _T2PERIOD1	=	0x00af
                           00AFAE   512 _T2PERIOD	=	0xafae
                           0000AB   513 _T2STATUS	=	0x00ab
                           0000E4   514 _U0CTRL	=	0x00e4
                           0000E7   515 _U0MODE	=	0x00e7
                           0000E6   516 _U0SHREG	=	0x00e6
                           0000E5   517 _U0STATUS	=	0x00e5
                           0000EC   518 _U1CTRL	=	0x00ec
                           0000EF   519 _U1MODE	=	0x00ef
                           0000EE   520 _U1SHREG	=	0x00ee
                           0000ED   521 _U1STATUS	=	0x00ed
                           0000DA   522 _WDTCFG	=	0x00da
                           0000DB   523 _WDTRESET	=	0x00db
                           0000F1   524 _WTCFGA	=	0x00f1
                           0000F9   525 _WTCFGB	=	0x00f9
                           0000F2   526 _WTCNTA0	=	0x00f2
                           0000F3   527 _WTCNTA1	=	0x00f3
                           00F3F2   528 _WTCNTA	=	0xf3f2
                           0000FA   529 _WTCNTB0	=	0x00fa
                           0000FB   530 _WTCNTB1	=	0x00fb
                           00FBFA   531 _WTCNTB	=	0xfbfa
                           0000EB   532 _WTCNTR1	=	0x00eb
                           0000F4   533 _WTEVTA0	=	0x00f4
                           0000F5   534 _WTEVTA1	=	0x00f5
                           00F5F4   535 _WTEVTA	=	0xf5f4
                           0000F6   536 _WTEVTB0	=	0x00f6
                           0000F7   537 _WTEVTB1	=	0x00f7
                           00F7F6   538 _WTEVTB	=	0xf7f6
                           0000FC   539 _WTEVTC0	=	0x00fc
                           0000FD   540 _WTEVTC1	=	0x00fd
                           00FDFC   541 _WTEVTC	=	0xfdfc
                           0000FE   542 _WTEVTD0	=	0x00fe
                           0000FF   543 _WTEVTD1	=	0x00ff
                           00FFFE   544 _WTEVTD	=	0xfffe
                           0000E9   545 _WTIRQEN	=	0x00e9
                           0000EA   546 _WTSTAT	=	0x00ea
                                    547 ;--------------------------------------------------------
                                    548 ; special function bits
                                    549 ;--------------------------------------------------------
                                    550 	.area RSEG    (ABS,DATA)
      000000                        551 	.org 0x0000
                           0000E0   552 _ACC_0	=	0x00e0
                           0000E1   553 _ACC_1	=	0x00e1
                           0000E2   554 _ACC_2	=	0x00e2
                           0000E3   555 _ACC_3	=	0x00e3
                           0000E4   556 _ACC_4	=	0x00e4
                           0000E5   557 _ACC_5	=	0x00e5
                           0000E6   558 _ACC_6	=	0x00e6
                           0000E7   559 _ACC_7	=	0x00e7
                           0000F0   560 _B_0	=	0x00f0
                           0000F1   561 _B_1	=	0x00f1
                           0000F2   562 _B_2	=	0x00f2
                           0000F3   563 _B_3	=	0x00f3
                           0000F4   564 _B_4	=	0x00f4
                           0000F5   565 _B_5	=	0x00f5
                           0000F6   566 _B_6	=	0x00f6
                           0000F7   567 _B_7	=	0x00f7
                           0000A0   568 _E2IE_0	=	0x00a0
                           0000A1   569 _E2IE_1	=	0x00a1
                           0000A2   570 _E2IE_2	=	0x00a2
                           0000A3   571 _E2IE_3	=	0x00a3
                           0000A4   572 _E2IE_4	=	0x00a4
                           0000A5   573 _E2IE_5	=	0x00a5
                           0000A6   574 _E2IE_6	=	0x00a6
                           0000A7   575 _E2IE_7	=	0x00a7
                           0000C0   576 _E2IP_0	=	0x00c0
                           0000C1   577 _E2IP_1	=	0x00c1
                           0000C2   578 _E2IP_2	=	0x00c2
                           0000C3   579 _E2IP_3	=	0x00c3
                           0000C4   580 _E2IP_4	=	0x00c4
                           0000C5   581 _E2IP_5	=	0x00c5
                           0000C6   582 _E2IP_6	=	0x00c6
                           0000C7   583 _E2IP_7	=	0x00c7
                           000098   584 _EIE_0	=	0x0098
                           000099   585 _EIE_1	=	0x0099
                           00009A   586 _EIE_2	=	0x009a
                           00009B   587 _EIE_3	=	0x009b
                           00009C   588 _EIE_4	=	0x009c
                           00009D   589 _EIE_5	=	0x009d
                           00009E   590 _EIE_6	=	0x009e
                           00009F   591 _EIE_7	=	0x009f
                           0000B0   592 _EIP_0	=	0x00b0
                           0000B1   593 _EIP_1	=	0x00b1
                           0000B2   594 _EIP_2	=	0x00b2
                           0000B3   595 _EIP_3	=	0x00b3
                           0000B4   596 _EIP_4	=	0x00b4
                           0000B5   597 _EIP_5	=	0x00b5
                           0000B6   598 _EIP_6	=	0x00b6
                           0000B7   599 _EIP_7	=	0x00b7
                           0000A8   600 _IE_0	=	0x00a8
                           0000A9   601 _IE_1	=	0x00a9
                           0000AA   602 _IE_2	=	0x00aa
                           0000AB   603 _IE_3	=	0x00ab
                           0000AC   604 _IE_4	=	0x00ac
                           0000AD   605 _IE_5	=	0x00ad
                           0000AE   606 _IE_6	=	0x00ae
                           0000AF   607 _IE_7	=	0x00af
                           0000AF   608 _EA	=	0x00af
                           0000B8   609 _IP_0	=	0x00b8
                           0000B9   610 _IP_1	=	0x00b9
                           0000BA   611 _IP_2	=	0x00ba
                           0000BB   612 _IP_3	=	0x00bb
                           0000BC   613 _IP_4	=	0x00bc
                           0000BD   614 _IP_5	=	0x00bd
                           0000BE   615 _IP_6	=	0x00be
                           0000BF   616 _IP_7	=	0x00bf
                           0000D0   617 _P	=	0x00d0
                           0000D1   618 _F1	=	0x00d1
                           0000D2   619 _OV	=	0x00d2
                           0000D3   620 _RS0	=	0x00d3
                           0000D4   621 _RS1	=	0x00d4
                           0000D5   622 _F0	=	0x00d5
                           0000D6   623 _AC	=	0x00d6
                           0000D7   624 _CY	=	0x00d7
                           0000C8   625 _PINA_0	=	0x00c8
                           0000C9   626 _PINA_1	=	0x00c9
                           0000CA   627 _PINA_2	=	0x00ca
                           0000CB   628 _PINA_3	=	0x00cb
                           0000CC   629 _PINA_4	=	0x00cc
                           0000CD   630 _PINA_5	=	0x00cd
                           0000CE   631 _PINA_6	=	0x00ce
                           0000CF   632 _PINA_7	=	0x00cf
                           0000E8   633 _PINB_0	=	0x00e8
                           0000E9   634 _PINB_1	=	0x00e9
                           0000EA   635 _PINB_2	=	0x00ea
                           0000EB   636 _PINB_3	=	0x00eb
                           0000EC   637 _PINB_4	=	0x00ec
                           0000ED   638 _PINB_5	=	0x00ed
                           0000EE   639 _PINB_6	=	0x00ee
                           0000EF   640 _PINB_7	=	0x00ef
                           0000F8   641 _PINC_0	=	0x00f8
                           0000F9   642 _PINC_1	=	0x00f9
                           0000FA   643 _PINC_2	=	0x00fa
                           0000FB   644 _PINC_3	=	0x00fb
                           0000FC   645 _PINC_4	=	0x00fc
                           0000FD   646 _PINC_5	=	0x00fd
                           0000FE   647 _PINC_6	=	0x00fe
                           0000FF   648 _PINC_7	=	0x00ff
                           000080   649 _PORTA_0	=	0x0080
                           000081   650 _PORTA_1	=	0x0081
                           000082   651 _PORTA_2	=	0x0082
                           000083   652 _PORTA_3	=	0x0083
                           000084   653 _PORTA_4	=	0x0084
                           000085   654 _PORTA_5	=	0x0085
                           000086   655 _PORTA_6	=	0x0086
                           000087   656 _PORTA_7	=	0x0087
                           000088   657 _PORTB_0	=	0x0088
                           000089   658 _PORTB_1	=	0x0089
                           00008A   659 _PORTB_2	=	0x008a
                           00008B   660 _PORTB_3	=	0x008b
                           00008C   661 _PORTB_4	=	0x008c
                           00008D   662 _PORTB_5	=	0x008d
                           00008E   663 _PORTB_6	=	0x008e
                           00008F   664 _PORTB_7	=	0x008f
                           000090   665 _PORTC_0	=	0x0090
                           000091   666 _PORTC_1	=	0x0091
                           000092   667 _PORTC_2	=	0x0092
                           000093   668 _PORTC_3	=	0x0093
                           000094   669 _PORTC_4	=	0x0094
                           000095   670 _PORTC_5	=	0x0095
                           000096   671 _PORTC_6	=	0x0096
                           000097   672 _PORTC_7	=	0x0097
                                    673 ;--------------------------------------------------------
                                    674 ; overlayable register banks
                                    675 ;--------------------------------------------------------
                                    676 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        677 	.ds 8
                                    678 ;--------------------------------------------------------
                                    679 ; internal ram data
                                    680 ;--------------------------------------------------------
                                    681 	.area DSEG    (DATA)
      000021                        682 _BEACON_decoding_sloc0_1_0:
      000021                        683 	.ds 2
      000023                        684 _BEACON_decoding_sloc1_1_0:
      000023                        685 	.ds 2
      000025                        686 _BEACON_decoding_sloc2_1_0:
      000025                        687 	.ds 3
      000028                        688 _BEACON_decoding_sloc3_1_0:
      000028                        689 	.ds 3
      00002B                        690 _BEACON_decoding_sloc4_1_0:
      00002B                        691 	.ds 3
      00002E                        692 _BEACON_decoding_sloc5_1_0:
      00002E                        693 	.ds 4
                                    694 ;--------------------------------------------------------
                                    695 ; overlayable items in internal ram 
                                    696 ;--------------------------------------------------------
                                    697 	.area	OSEG    (OVR,DATA)
      00005E                        698 _VerifyACK_sloc0_1_0:
      00005E                        699 	.ds 3
      000061                        700 _VerifyACK_sloc1_1_0:
      000061                        701 	.ds 1
                                    702 ;--------------------------------------------------------
                                    703 ; indirectly addressable internal ram data
                                    704 ;--------------------------------------------------------
                                    705 	.area ISEG    (DATA)
                                    706 ;--------------------------------------------------------
                                    707 ; absolute internal ram data
                                    708 ;--------------------------------------------------------
                                    709 	.area IABS    (ABS,DATA)
                                    710 	.area IABS    (ABS,DATA)
                                    711 ;--------------------------------------------------------
                                    712 ; bit data
                                    713 ;--------------------------------------------------------
                                    714 	.area BSEG    (BIT)
                                    715 ;--------------------------------------------------------
                                    716 ; paged external ram data
                                    717 ;--------------------------------------------------------
                                    718 	.area PSEG    (PAG,XDATA)
                                    719 ;--------------------------------------------------------
                                    720 ; external ram data
                                    721 ;--------------------------------------------------------
                                    722 	.area XSEG    (XDATA)
                           007020   723 _ADCCH0VAL0	=	0x7020
                           007021   724 _ADCCH0VAL1	=	0x7021
                           007020   725 _ADCCH0VAL	=	0x7020
                           007022   726 _ADCCH1VAL0	=	0x7022
                           007023   727 _ADCCH1VAL1	=	0x7023
                           007022   728 _ADCCH1VAL	=	0x7022
                           007024   729 _ADCCH2VAL0	=	0x7024
                           007025   730 _ADCCH2VAL1	=	0x7025
                           007024   731 _ADCCH2VAL	=	0x7024
                           007026   732 _ADCCH3VAL0	=	0x7026
                           007027   733 _ADCCH3VAL1	=	0x7027
                           007026   734 _ADCCH3VAL	=	0x7026
                           007028   735 _ADCTUNE0	=	0x7028
                           007029   736 _ADCTUNE1	=	0x7029
                           00702A   737 _ADCTUNE2	=	0x702a
                           007010   738 _DMA0ADDR0	=	0x7010
                           007011   739 _DMA0ADDR1	=	0x7011
                           007010   740 _DMA0ADDR	=	0x7010
                           007014   741 _DMA0CONFIG	=	0x7014
                           007012   742 _DMA1ADDR0	=	0x7012
                           007013   743 _DMA1ADDR1	=	0x7013
                           007012   744 _DMA1ADDR	=	0x7012
                           007015   745 _DMA1CONFIG	=	0x7015
                           007070   746 _FRCOSCCONFIG	=	0x7070
                           007071   747 _FRCOSCCTRL	=	0x7071
                           007076   748 _FRCOSCFREQ0	=	0x7076
                           007077   749 _FRCOSCFREQ1	=	0x7077
                           007076   750 _FRCOSCFREQ	=	0x7076
                           007072   751 _FRCOSCKFILT0	=	0x7072
                           007073   752 _FRCOSCKFILT1	=	0x7073
                           007072   753 _FRCOSCKFILT	=	0x7072
                           007078   754 _FRCOSCPER0	=	0x7078
                           007079   755 _FRCOSCPER1	=	0x7079
                           007078   756 _FRCOSCPER	=	0x7078
                           007074   757 _FRCOSCREF0	=	0x7074
                           007075   758 _FRCOSCREF1	=	0x7075
                           007074   759 _FRCOSCREF	=	0x7074
                           007007   760 _ANALOGA	=	0x7007
                           00700C   761 _GPIOENABLE	=	0x700c
                           007003   762 _EXTIRQ	=	0x7003
                           007000   763 _INTCHGA	=	0x7000
                           007001   764 _INTCHGB	=	0x7001
                           007002   765 _INTCHGC	=	0x7002
                           007008   766 _PALTA	=	0x7008
                           007009   767 _PALTB	=	0x7009
                           00700A   768 _PALTC	=	0x700a
                           007046   769 _PALTRADIO	=	0x7046
                           007004   770 _PINCHGA	=	0x7004
                           007005   771 _PINCHGB	=	0x7005
                           007006   772 _PINCHGC	=	0x7006
                           00700B   773 _PINSEL	=	0x700b
                           007060   774 _LPOSCCONFIG	=	0x7060
                           007066   775 _LPOSCFREQ0	=	0x7066
                           007067   776 _LPOSCFREQ1	=	0x7067
                           007066   777 _LPOSCFREQ	=	0x7066
                           007062   778 _LPOSCKFILT0	=	0x7062
                           007063   779 _LPOSCKFILT1	=	0x7063
                           007062   780 _LPOSCKFILT	=	0x7062
                           007068   781 _LPOSCPER0	=	0x7068
                           007069   782 _LPOSCPER1	=	0x7069
                           007068   783 _LPOSCPER	=	0x7068
                           007064   784 _LPOSCREF0	=	0x7064
                           007065   785 _LPOSCREF1	=	0x7065
                           007064   786 _LPOSCREF	=	0x7064
                           007054   787 _LPXOSCGM	=	0x7054
                           007F01   788 _MISCCTRL	=	0x7f01
                           007053   789 _OSCCALIB	=	0x7053
                           007050   790 _OSCFORCERUN	=	0x7050
                           007052   791 _OSCREADY	=	0x7052
                           007051   792 _OSCRUN	=	0x7051
                           007040   793 _RADIOFDATAADDR0	=	0x7040
                           007041   794 _RADIOFDATAADDR1	=	0x7041
                           007040   795 _RADIOFDATAADDR	=	0x7040
                           007042   796 _RADIOFSTATADDR0	=	0x7042
                           007043   797 _RADIOFSTATADDR1	=	0x7043
                           007042   798 _RADIOFSTATADDR	=	0x7042
                           007044   799 _RADIOMUX	=	0x7044
                           007084   800 _SCRATCH0	=	0x7084
                           007085   801 _SCRATCH1	=	0x7085
                           007086   802 _SCRATCH2	=	0x7086
                           007087   803 _SCRATCH3	=	0x7087
                           007F00   804 _SILICONREV	=	0x7f00
                           007F19   805 _XTALAMPL	=	0x7f19
                           007F18   806 _XTALOSC	=	0x7f18
                           007F1A   807 _XTALREADY	=	0x7f1a
                           007081   808 _RNGBYTE	=	0x7081
                           007091   809 _AESCONFIG	=	0x7091
                           007098   810 _AESCURBLOCK	=	0x7098
                           007094   811 _AESINADDR0	=	0x7094
                           007095   812 _AESINADDR1	=	0x7095
                           007094   813 _AESINADDR	=	0x7094
                           007092   814 _AESKEYADDR0	=	0x7092
                           007093   815 _AESKEYADDR1	=	0x7093
                           007092   816 _AESKEYADDR	=	0x7092
                           007090   817 _AESMODE	=	0x7090
                           007096   818 _AESOUTADDR0	=	0x7096
                           007097   819 _AESOUTADDR1	=	0x7097
                           007096   820 _AESOUTADDR	=	0x7096
                           007080   821 _RNGMODE	=	0x7080
                           007082   822 _RNGCLKSRC0	=	0x7082
                           007083   823 _RNGCLKSRC1	=	0x7083
      0003AF                        824 _BEACON_decoding_PARM_2:
      0003AF                        825 	.ds 2
      0003B1                        826 _BEACON_decoding_PARM_3:
      0003B1                        827 	.ds 1
      0003B2                        828 _BEACON_decoding_Rxbuffer_65536_119:
      0003B2                        829 	.ds 3
      0003B5                        830 _VerifyACK_PARM_2:
      0003B5                        831 	.ds 1
      0003B6                        832 _VerifyACK_PARM_3:
      0003B6                        833 	.ds 3
      0003B9                        834 _VerifyACK_Rxbuffer_65536_124:
      0003B9                        835 	.ds 3
      0003BC                        836 _VerifyACK_RetVal_65536_125:
      0003BC                        837 	.ds 1
                                    838 ;--------------------------------------------------------
                                    839 ; absolute external ram data
                                    840 ;--------------------------------------------------------
                                    841 	.area XABS    (ABS,XDATA)
                                    842 ;--------------------------------------------------------
                                    843 ; external initialized ram data
                                    844 ;--------------------------------------------------------
                                    845 	.area XISEG   (XDATA)
                                    846 	.area HOME    (CODE)
                                    847 	.area GSINIT0 (CODE)
                                    848 	.area GSINIT1 (CODE)
                                    849 	.area GSINIT2 (CODE)
                                    850 	.area GSINIT3 (CODE)
                                    851 	.area GSINIT4 (CODE)
                                    852 	.area GSINIT5 (CODE)
                                    853 	.area GSINIT  (CODE)
                                    854 	.area GSFINAL (CODE)
                                    855 	.area CSEG    (CODE)
                                    856 ;--------------------------------------------------------
                                    857 ; global & static initialisations
                                    858 ;--------------------------------------------------------
                                    859 	.area HOME    (CODE)
                                    860 	.area GSINIT  (CODE)
                                    861 	.area GSFINAL (CODE)
                                    862 	.area GSINIT  (CODE)
                                    863 ;--------------------------------------------------------
                                    864 ; Home
                                    865 ;--------------------------------------------------------
                                    866 	.area HOME    (CODE)
                                    867 	.area HOME    (CODE)
                                    868 ;--------------------------------------------------------
                                    869 ; code
                                    870 ;--------------------------------------------------------
                                    871 	.area CSEG    (CODE)
                                    872 ;------------------------------------------------------------
                                    873 ;Allocation info for local variables in function 'BEACON_decoding'
                                    874 ;------------------------------------------------------------
                                    875 ;sloc0                     Allocated with name '_BEACON_decoding_sloc0_1_0'
                                    876 ;sloc1                     Allocated with name '_BEACON_decoding_sloc1_1_0'
                                    877 ;sloc2                     Allocated with name '_BEACON_decoding_sloc2_1_0'
                                    878 ;sloc3                     Allocated with name '_BEACON_decoding_sloc3_1_0'
                                    879 ;sloc4                     Allocated with name '_BEACON_decoding_sloc4_1_0'
                                    880 ;sloc5                     Allocated with name '_BEACON_decoding_sloc5_1_0'
                                    881 ;beacon                    Allocated with name '_BEACON_decoding_PARM_2'
                                    882 ;length                    Allocated with name '_BEACON_decoding_PARM_3'
                                    883 ;Rxbuffer                  Allocated with name '_BEACON_decoding_Rxbuffer_65536_119'
                                    884 ;i                         Allocated with name '_BEACON_decoding_i_65536_120'
                                    885 ;------------------------------------------------------------
                                    886 ;	..\src\peripherals\Beacon.c:29: __xdata void BEACON_decoding(uint8_t* Rxbuffer, RF_beacon_t* beacon, uint8_t length)
                                    887 ;	-----------------------------------------
                                    888 ;	 function BEACON_decoding
                                    889 ;	-----------------------------------------
      004DD6                        890 _BEACON_decoding:
                           000007   891 	ar7 = 0x07
                           000006   892 	ar6 = 0x06
                           000005   893 	ar5 = 0x05
                           000004   894 	ar4 = 0x04
                           000003   895 	ar3 = 0x03
                           000002   896 	ar2 = 0x02
                           000001   897 	ar1 = 0x01
                           000000   898 	ar0 = 0x00
      004DD6 AF F0            [24]  899 	mov	r7,b
      004DD8 AE 83            [24]  900 	mov	r6,dph
      004DDA E5 82            [12]  901 	mov	a,dpl
      004DDC 90 03 B2         [24]  902 	mov	dptr,#_BEACON_decoding_Rxbuffer_65536_119
      004DDF F0               [24]  903 	movx	@dptr,a
      004DE0 EE               [12]  904 	mov	a,r6
      004DE1 A3               [24]  905 	inc	dptr
      004DE2 F0               [24]  906 	movx	@dptr,a
      004DE3 EF               [12]  907 	mov	a,r7
      004DE4 A3               [24]  908 	inc	dptr
      004DE5 F0               [24]  909 	movx	@dptr,a
                                    910 ;	..\src\peripherals\Beacon.c:32: beacon->FlagReceived = INVALID_BEACON;
      004DE6 90 03 AF         [24]  911 	mov	dptr,#_BEACON_decoding_PARM_2
      004DE9 E0               [24]  912 	movx	a,@dptr
      004DEA FE               [12]  913 	mov	r6,a
      004DEB A3               [24]  914 	inc	dptr
      004DEC E0               [24]  915 	movx	a,@dptr
      004DED FF               [12]  916 	mov	r7,a
      004DEE 8E 82            [24]  917 	mov	dpl,r6
      004DF0 8F 83            [24]  918 	mov	dph,r7
      004DF2 E4               [12]  919 	clr	a
      004DF3 F0               [24]  920 	movx	@dptr,a
                                    921 ;	..\src\peripherals\Beacon.c:34: dbglink_writestr(" largo = ");
      004DF4 90 91 DB         [24]  922 	mov	dptr,#___str_0
      004DF7 75 F0 80         [24]  923 	mov	b,#0x80
      004DFA C0 07            [24]  924 	push	ar7
      004DFC C0 06            [24]  925 	push	ar6
      004DFE 12 82 8A         [24]  926 	lcall	_dbglink_writestr
                                    927 ;	..\src\peripherals\Beacon.c:35: dbglink_writehex16(length,1,WRNUM_PADZERO);
      004E01 90 03 B1         [24]  928 	mov	dptr,#_BEACON_decoding_PARM_3
      004E04 E0               [24]  929 	movx	a,@dptr
      004E05 FD               [12]  930 	mov	r5,a
      004E06 FB               [12]  931 	mov	r3,a
      004E07 7C 00            [12]  932 	mov	r4,#0x00
      004E09 C0 05            [24]  933 	push	ar5
      004E0B 74 08            [12]  934 	mov	a,#0x08
      004E0D C0 E0            [24]  935 	push	acc
      004E0F 74 01            [12]  936 	mov	a,#0x01
      004E11 C0 E0            [24]  937 	push	acc
      004E13 8B 82            [24]  938 	mov	dpl,r3
      004E15 8C 83            [24]  939 	mov	dph,r4
      004E17 12 87 C9         [24]  940 	lcall	_dbglink_writehex16
      004E1A 15 81            [12]  941 	dec	sp
      004E1C 15 81            [12]  942 	dec	sp
      004E1E D0 05            [24]  943 	pop	ar5
      004E20 D0 06            [24]  944 	pop	ar6
      004E22 D0 07            [24]  945 	pop	ar7
                                    946 ;	..\src\peripherals\Beacon.c:38: if(length == 24)
      004E24 BD 18 02         [24]  947 	cjne	r5,#0x18,00123$
      004E27 80 01            [24]  948 	sjmp	00124$
      004E29                        949 00123$:
      004E29 22               [24]  950 	ret
      004E2A                        951 00124$:
                                    952 ;	..\src\peripherals\Beacon.c:41: beacon->SatId = *Rxbuffer <<8 | *(Rxbuffer+1);
      004E2A 74 01            [12]  953 	mov	a,#0x01
      004E2C 2E               [12]  954 	add	a,r6
      004E2D F5 21            [12]  955 	mov	_BEACON_decoding_sloc0_1_0,a
      004E2F E4               [12]  956 	clr	a
      004E30 3F               [12]  957 	addc	a,r7
      004E31 F5 22            [12]  958 	mov	(_BEACON_decoding_sloc0_1_0 + 1),a
      004E33 90 03 B2         [24]  959 	mov	dptr,#_BEACON_decoding_Rxbuffer_65536_119
      004E36 E0               [24]  960 	movx	a,@dptr
      004E37 F9               [12]  961 	mov	r1,a
      004E38 A3               [24]  962 	inc	dptr
      004E39 E0               [24]  963 	movx	a,@dptr
      004E3A FA               [12]  964 	mov	r2,a
      004E3B A3               [24]  965 	inc	dptr
      004E3C E0               [24]  966 	movx	a,@dptr
      004E3D FB               [12]  967 	mov	r3,a
      004E3E 89 82            [24]  968 	mov	dpl,r1
      004E40 8A 83            [24]  969 	mov	dph,r2
      004E42 8B F0            [24]  970 	mov	b,r3
      004E44 12 8D 8E         [24]  971 	lcall	__gptrget
      004E47 F8               [12]  972 	mov	r0,a
      004E48 7D 00            [12]  973 	mov	r5,#0x00
      004E4A 88 24            [24]  974 	mov	(_BEACON_decoding_sloc1_1_0 + 1),r0
                                    975 ;	1-genFromRTrack replaced	mov	_BEACON_decoding_sloc1_1_0,#0x00
      004E4C 8D 23            [24]  976 	mov	_BEACON_decoding_sloc1_1_0,r5
      004E4E 74 01            [12]  977 	mov	a,#0x01
      004E50 29               [12]  978 	add	a,r1
      004E51 F8               [12]  979 	mov	r0,a
      004E52 E4               [12]  980 	clr	a
      004E53 3A               [12]  981 	addc	a,r2
      004E54 FC               [12]  982 	mov	r4,a
      004E55 8B 05            [24]  983 	mov	ar5,r3
      004E57 88 82            [24]  984 	mov	dpl,r0
      004E59 8C 83            [24]  985 	mov	dph,r4
      004E5B 8D F0            [24]  986 	mov	b,r5
      004E5D 12 8D 8E         [24]  987 	lcall	__gptrget
      004E60 F8               [12]  988 	mov	r0,a
      004E61 7D 00            [12]  989 	mov	r5,#0x00
      004E63 E5 23            [12]  990 	mov	a,_BEACON_decoding_sloc1_1_0
      004E65 42 00            [12]  991 	orl	ar0,a
      004E67 E5 24            [12]  992 	mov	a,(_BEACON_decoding_sloc1_1_0 + 1)
      004E69 42 05            [12]  993 	orl	ar5,a
      004E6B 85 21 82         [24]  994 	mov	dpl,_BEACON_decoding_sloc0_1_0
      004E6E 85 22 83         [24]  995 	mov	dph,(_BEACON_decoding_sloc0_1_0 + 1)
      004E71 E8               [12]  996 	mov	a,r0
      004E72 F0               [24]  997 	movx	@dptr,a
      004E73 ED               [12]  998 	mov	a,r5
      004E74 A3               [24]  999 	inc	dptr
      004E75 F0               [24] 1000 	movx	@dptr,a
                                   1001 ;	..\src\peripherals\Beacon.c:42: beacon->BeaconID = (*(Rxbuffer+POS_BEACONID) & MASK_BEACONID)>> 4;
      004E76 74 03            [12] 1002 	mov	a,#0x03
      004E78 2E               [12] 1003 	add	a,r6
      004E79 FC               [12] 1004 	mov	r4,a
      004E7A E4               [12] 1005 	clr	a
      004E7B 3F               [12] 1006 	addc	a,r7
      004E7C FD               [12] 1007 	mov	r5,a
      004E7D C0 06            [24] 1008 	push	ar6
      004E7F C0 07            [24] 1009 	push	ar7
      004E81 74 02            [12] 1010 	mov	a,#0x02
      004E83 29               [12] 1011 	add	a,r1
      004E84 F8               [12] 1012 	mov	r0,a
      004E85 E4               [12] 1013 	clr	a
      004E86 3A               [12] 1014 	addc	a,r2
      004E87 FE               [12] 1015 	mov	r6,a
      004E88 8B 07            [24] 1016 	mov	ar7,r3
      004E8A 88 82            [24] 1017 	mov	dpl,r0
      004E8C 8E 83            [24] 1018 	mov	dph,r6
      004E8E 8F F0            [24] 1019 	mov	b,r7
      004E90 12 8D 8E         [24] 1020 	lcall	__gptrget
      004E93 F8               [12] 1021 	mov	r0,a
      004E94 53 00 F0         [24] 1022 	anl	ar0,#0xf0
      004E97 E4               [12] 1023 	clr	a
      004E98 C8               [12] 1024 	xch	a,r0
      004E99 C4               [12] 1025 	swap	a
      004E9A 54 0F            [12] 1026 	anl	a,#0x0f
      004E9C 68               [12] 1027 	xrl	a,r0
      004E9D C8               [12] 1028 	xch	a,r0
      004E9E 54 0F            [12] 1029 	anl	a,#0x0f
      004EA0 C8               [12] 1030 	xch	a,r0
      004EA1 68               [12] 1031 	xrl	a,r0
      004EA2 C8               [12] 1032 	xch	a,r0
      004EA3 30 E3 02         [24] 1033 	jnb	acc.3,00125$
      004EA6 44 F0            [12] 1034 	orl	a,#0xf0
      004EA8                       1035 00125$:
      004EA8 FF               [12] 1036 	mov	r7,a
      004EA9 8C 82            [24] 1037 	mov	dpl,r4
      004EAB 8D 83            [24] 1038 	mov	dph,r5
      004EAD E8               [12] 1039 	mov	a,r0
      004EAE F0               [24] 1040 	movx	@dptr,a
                                   1041 ;	..\src\peripherals\Beacon.c:45: if(beacon->BeaconID == UPLINK_BEACON) // se usa 0x3 como valor provisorio porque es lo que tiene el transmisor
      004EAF 8C 82            [24] 1042 	mov	dpl,r4
      004EB1 8D 83            [24] 1043 	mov	dph,r5
      004EB3 E0               [24] 1044 	movx	a,@dptr
      004EB4 F5 23            [12] 1045 	mov	_BEACON_decoding_sloc1_1_0,a
      004EB6 B8 0C 02         [24] 1046 	cjne	r0,#0x0c,00126$
      004EB9 80 06            [24] 1047 	sjmp	00127$
      004EBB                       1048 00126$:
      004EBB D0 07            [24] 1049 	pop	ar7
      004EBD D0 06            [24] 1050 	pop	ar6
      004EBF 80 59            [24] 1051 	sjmp	00105$
      004EC1                       1052 00127$:
      004EC1 D0 07            [24] 1053 	pop	ar7
      004EC3 D0 06            [24] 1054 	pop	ar6
                                   1055 ;	..\src\peripherals\Beacon.c:47: beacon->FlagReceived = UPLINK_BEACON;
      004EC5 8E 82            [24] 1056 	mov	dpl,r6
      004EC7 8F 83            [24] 1057 	mov	dph,r7
      004EC9 74 0C            [12] 1058 	mov	a,#0x0c
      004ECB F0               [24] 1059 	movx	@dptr,a
                                   1060 ;	..\src\peripherals\Beacon.c:49: dbglink_writestr(" Uplink");
      004ECC 90 91 E5         [24] 1061 	mov	dptr,#___str_1
      004ECF 75 F0 80         [24] 1062 	mov	b,#0x80
      004ED2 C0 07            [24] 1063 	push	ar7
      004ED4 C0 06            [24] 1064 	push	ar6
      004ED6 C0 03            [24] 1065 	push	ar3
      004ED8 C0 02            [24] 1066 	push	ar2
      004EDA C0 01            [24] 1067 	push	ar1
      004EDC 12 82 8A         [24] 1068 	lcall	_dbglink_writestr
      004EDF D0 01            [24] 1069 	pop	ar1
      004EE1 D0 02            [24] 1070 	pop	ar2
      004EE3 D0 03            [24] 1071 	pop	ar3
      004EE5 D0 06            [24] 1072 	pop	ar6
      004EE7 D0 07            [24] 1073 	pop	ar7
                                   1074 ;	..\src\peripherals\Beacon.c:51: beacon->NumPremSlots = *(Rxbuffer+POS_NUMPREM);
      004EE9 74 04            [12] 1075 	mov	a,#0x04
      004EEB 2E               [12] 1076 	add	a,r6
      004EEC F5 21            [12] 1077 	mov	_BEACON_decoding_sloc0_1_0,a
      004EEE E4               [12] 1078 	clr	a
      004EEF 3F               [12] 1079 	addc	a,r7
      004EF0 F5 22            [12] 1080 	mov	(_BEACON_decoding_sloc0_1_0 + 1),a
      004EF2 74 03            [12] 1081 	mov	a,#0x03
      004EF4 29               [12] 1082 	add	a,r1
      004EF5 F8               [12] 1083 	mov	r0,a
      004EF6 E4               [12] 1084 	clr	a
      004EF7 3A               [12] 1085 	addc	a,r2
      004EF8 FC               [12] 1086 	mov	r4,a
      004EF9 8B 05            [24] 1087 	mov	ar5,r3
      004EFB 88 82            [24] 1088 	mov	dpl,r0
      004EFD 8C 83            [24] 1089 	mov	dph,r4
      004EFF 8D F0            [24] 1090 	mov	b,r5
      004F01 12 8D 8E         [24] 1091 	lcall	__gptrget
      004F04 F8               [12] 1092 	mov	r0,a
      004F05 85 21 82         [24] 1093 	mov	dpl,_BEACON_decoding_sloc0_1_0
      004F08 85 22 83         [24] 1094 	mov	dph,(_BEACON_decoding_sloc0_1_0 + 1)
      004F0B F0               [24] 1095 	movx	@dptr,a
                                   1096 ;	..\src\peripherals\Beacon.c:52: beacon->NumACK = 0xFF; //invalid data
      004F0C 8E 82            [24] 1097 	mov	dpl,r6
      004F0E 8F 83            [24] 1098 	mov	dph,r7
      004F10 A3               [24] 1099 	inc	dptr
      004F11 A3               [24] 1100 	inc	dptr
      004F12 A3               [24] 1101 	inc	dptr
      004F13 A3               [24] 1102 	inc	dptr
      004F14 A3               [24] 1103 	inc	dptr
      004F15 74 FF            [12] 1104 	mov	a,#0xff
      004F17 F0               [24] 1105 	movx	@dptr,a
      004F18 80 59            [24] 1106 	sjmp	00106$
      004F1A                       1107 00105$:
                                   1108 ;	..\src\peripherals\Beacon.c:54: else if(beacon->BeaconID == DOWNLINK_BEACON)
      004F1A 74 0A            [12] 1109 	mov	a,#0x0a
      004F1C B5 23 4E         [24] 1110 	cjne	a,_BEACON_decoding_sloc1_1_0,00102$
                                   1111 ;	..\src\peripherals\Beacon.c:56: beacon->FlagReceived = DOWNLINK_BEACON;
      004F1F 8E 82            [24] 1112 	mov	dpl,r6
      004F21 8F 83            [24] 1113 	mov	dph,r7
      004F23 74 0A            [12] 1114 	mov	a,#0x0a
      004F25 F0               [24] 1115 	movx	@dptr,a
                                   1116 ;	..\src\peripherals\Beacon.c:58: dbglink_writestr(" Downlink ");
      004F26 90 91 ED         [24] 1117 	mov	dptr,#___str_2
      004F29 75 F0 80         [24] 1118 	mov	b,#0x80
      004F2C C0 07            [24] 1119 	push	ar7
      004F2E C0 06            [24] 1120 	push	ar6
      004F30 C0 03            [24] 1121 	push	ar3
      004F32 C0 02            [24] 1122 	push	ar2
      004F34 C0 01            [24] 1123 	push	ar1
      004F36 12 82 8A         [24] 1124 	lcall	_dbglink_writestr
      004F39 D0 01            [24] 1125 	pop	ar1
      004F3B D0 02            [24] 1126 	pop	ar2
      004F3D D0 03            [24] 1127 	pop	ar3
      004F3F D0 06            [24] 1128 	pop	ar6
      004F41 D0 07            [24] 1129 	pop	ar7
                                   1130 ;	..\src\peripherals\Beacon.c:60: beacon->NumACK = *(Rxbuffer+POS_NUMPREM);
      004F43 74 05            [12] 1131 	mov	a,#0x05
      004F45 2E               [12] 1132 	add	a,r6
      004F46 FC               [12] 1133 	mov	r4,a
      004F47 E4               [12] 1134 	clr	a
      004F48 3F               [12] 1135 	addc	a,r7
      004F49 FD               [12] 1136 	mov	r5,a
      004F4A 74 03            [12] 1137 	mov	a,#0x03
      004F4C 29               [12] 1138 	add	a,r1
      004F4D F9               [12] 1139 	mov	r1,a
      004F4E E4               [12] 1140 	clr	a
      004F4F 3A               [12] 1141 	addc	a,r2
      004F50 FA               [12] 1142 	mov	r2,a
      004F51 89 82            [24] 1143 	mov	dpl,r1
      004F53 8A 83            [24] 1144 	mov	dph,r2
      004F55 8B F0            [24] 1145 	mov	b,r3
      004F57 12 8D 8E         [24] 1146 	lcall	__gptrget
      004F5A F9               [12] 1147 	mov	r1,a
      004F5B 8C 82            [24] 1148 	mov	dpl,r4
      004F5D 8D 83            [24] 1149 	mov	dph,r5
      004F5F F0               [24] 1150 	movx	@dptr,a
                                   1151 ;	..\src\peripherals\Beacon.c:61: beacon->NumPremSlots = 0xFF; //invalid data
      004F60 8E 82            [24] 1152 	mov	dpl,r6
      004F62 8F 83            [24] 1153 	mov	dph,r7
      004F64 A3               [24] 1154 	inc	dptr
      004F65 A3               [24] 1155 	inc	dptr
      004F66 A3               [24] 1156 	inc	dptr
      004F67 A3               [24] 1157 	inc	dptr
      004F68 74 FF            [12] 1158 	mov	a,#0xff
      004F6A F0               [24] 1159 	movx	@dptr,a
      004F6B 80 06            [24] 1160 	sjmp	00106$
      004F6D                       1161 00102$:
                                   1162 ;	..\src\peripherals\Beacon.c:63: else beacon->FlagReceived = INVALID_BEACON;
      004F6D 8E 82            [24] 1163 	mov	dpl,r6
      004F6F 8F 83            [24] 1164 	mov	dph,r7
      004F71 E4               [12] 1165 	clr	a
      004F72 F0               [24] 1166 	movx	@dptr,a
      004F73                       1167 00106$:
                                   1168 ;	..\src\peripherals\Beacon.c:64: beacon->HMAC_flag =(*(Rxbuffer+POS_SECURITY) & MASK_HMAC_FLAG)>>7;
      004F73 74 06            [12] 1169 	mov	a,#0x06
      004F75 2E               [12] 1170 	add	a,r6
      004F76 F5 23            [12] 1171 	mov	_BEACON_decoding_sloc1_1_0,a
      004F78 E4               [12] 1172 	clr	a
      004F79 3F               [12] 1173 	addc	a,r7
      004F7A F5 24            [12] 1174 	mov	(_BEACON_decoding_sloc1_1_0 + 1),a
      004F7C 90 03 B2         [24] 1175 	mov	dptr,#_BEACON_decoding_Rxbuffer_65536_119
      004F7F E0               [24] 1176 	movx	a,@dptr
      004F80 F5 2B            [12] 1177 	mov	_BEACON_decoding_sloc4_1_0,a
      004F82 A3               [24] 1178 	inc	dptr
      004F83 E0               [24] 1179 	movx	a,@dptr
      004F84 F5 2C            [12] 1180 	mov	(_BEACON_decoding_sloc4_1_0 + 1),a
      004F86 A3               [24] 1181 	inc	dptr
      004F87 E0               [24] 1182 	movx	a,@dptr
      004F88 F5 2D            [12] 1183 	mov	(_BEACON_decoding_sloc4_1_0 + 2),a
      004F8A 74 04            [12] 1184 	mov	a,#0x04
      004F8C 25 2B            [12] 1185 	add	a,_BEACON_decoding_sloc4_1_0
      004F8E F5 25            [12] 1186 	mov	_BEACON_decoding_sloc2_1_0,a
      004F90 E4               [12] 1187 	clr	a
      004F91 35 2C            [12] 1188 	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
      004F93 F5 26            [12] 1189 	mov	(_BEACON_decoding_sloc2_1_0 + 1),a
      004F95 85 2D 27         [24] 1190 	mov	(_BEACON_decoding_sloc2_1_0 + 2),(_BEACON_decoding_sloc4_1_0 + 2)
      004F98 85 25 82         [24] 1191 	mov	dpl,_BEACON_decoding_sloc2_1_0
      004F9B 85 26 83         [24] 1192 	mov	dph,(_BEACON_decoding_sloc2_1_0 + 1)
      004F9E 85 27 F0         [24] 1193 	mov	b,(_BEACON_decoding_sloc2_1_0 + 2)
      004FA1 12 8D 8E         [24] 1194 	lcall	__gptrget
      004FA4 FD               [12] 1195 	mov	r5,a
      004FA5 53 05 80         [24] 1196 	anl	ar5,#0x80
      004FA8 E4               [12] 1197 	clr	a
      004FA9 A2 E7            [12] 1198 	mov	c,acc.7
      004FAB CD               [12] 1199 	xch	a,r5
      004FAC 33               [12] 1200 	rlc	a
      004FAD CD               [12] 1201 	xch	a,r5
      004FAE 33               [12] 1202 	rlc	a
      004FAF CD               [12] 1203 	xch	a,r5
      004FB0 54 01            [12] 1204 	anl	a,#0x01
      004FB2 30 E0 02         [24] 1205 	jnb	acc.0,00130$
      004FB5 44 FE            [12] 1206 	orl	a,#0xfe
      004FB7                       1207 00130$:
      004FB7 85 23 82         [24] 1208 	mov	dpl,_BEACON_decoding_sloc1_1_0
      004FBA 85 24 83         [24] 1209 	mov	dph,(_BEACON_decoding_sloc1_1_0 + 1)
      004FBD ED               [12] 1210 	mov	a,r5
      004FBE F0               [24] 1211 	movx	@dptr,a
                                   1212 ;	..\src\peripherals\Beacon.c:65: beacon->HMAC_type = (*(Rxbuffer+POS_SECURITY) & MASK_HMAC_TYPE)>>4;
      004FBF 74 07            [12] 1213 	mov	a,#0x07
      004FC1 2E               [12] 1214 	add	a,r6
      004FC2 F5 23            [12] 1215 	mov	_BEACON_decoding_sloc1_1_0,a
      004FC4 E4               [12] 1216 	clr	a
      004FC5 3F               [12] 1217 	addc	a,r7
      004FC6 F5 24            [12] 1218 	mov	(_BEACON_decoding_sloc1_1_0 + 1),a
      004FC8 85 25 82         [24] 1219 	mov	dpl,_BEACON_decoding_sloc2_1_0
      004FCB 85 26 83         [24] 1220 	mov	dph,(_BEACON_decoding_sloc2_1_0 + 1)
      004FCE 85 27 F0         [24] 1221 	mov	b,(_BEACON_decoding_sloc2_1_0 + 2)
      004FD1 12 8D 8E         [24] 1222 	lcall	__gptrget
      004FD4 F8               [12] 1223 	mov	r0,a
      004FD5 53 00 70         [24] 1224 	anl	ar0,#0x70
      004FD8 E4               [12] 1225 	clr	a
      004FD9 C8               [12] 1226 	xch	a,r0
      004FDA C4               [12] 1227 	swap	a
      004FDB 54 0F            [12] 1228 	anl	a,#0x0f
      004FDD 68               [12] 1229 	xrl	a,r0
      004FDE C8               [12] 1230 	xch	a,r0
      004FDF 54 0F            [12] 1231 	anl	a,#0x0f
      004FE1 C8               [12] 1232 	xch	a,r0
      004FE2 68               [12] 1233 	xrl	a,r0
      004FE3 C8               [12] 1234 	xch	a,r0
      004FE4 30 E3 02         [24] 1235 	jnb	acc.3,00131$
      004FE7 44 F0            [12] 1236 	orl	a,#0xf0
      004FE9                       1237 00131$:
      004FE9 85 23 82         [24] 1238 	mov	dpl,_BEACON_decoding_sloc1_1_0
      004FEC 85 24 83         [24] 1239 	mov	dph,(_BEACON_decoding_sloc1_1_0 + 1)
      004FEF E8               [12] 1240 	mov	a,r0
      004FF0 F0               [24] 1241 	movx	@dptr,a
                                   1242 ;	..\src\peripherals\Beacon.c:66: beacon->Encryption_flag = (*(Rxbuffer+POS_SECURITY) & MASK_ENCRYPTION_FLAG)>>3;
      004FF1 74 08            [12] 1243 	mov	a,#0x08
      004FF3 2E               [12] 1244 	add	a,r6
      004FF4 F5 23            [12] 1245 	mov	_BEACON_decoding_sloc1_1_0,a
      004FF6 E4               [12] 1246 	clr	a
      004FF7 3F               [12] 1247 	addc	a,r7
      004FF8 F5 24            [12] 1248 	mov	(_BEACON_decoding_sloc1_1_0 + 1),a
      004FFA 85 25 82         [24] 1249 	mov	dpl,_BEACON_decoding_sloc2_1_0
      004FFD 85 26 83         [24] 1250 	mov	dph,(_BEACON_decoding_sloc2_1_0 + 1)
      005000 85 27 F0         [24] 1251 	mov	b,(_BEACON_decoding_sloc2_1_0 + 2)
      005003 12 8D 8E         [24] 1252 	lcall	__gptrget
      005006 F8               [12] 1253 	mov	r0,a
      005007 53 00 08         [24] 1254 	anl	ar0,#0x08
      00500A E4               [12] 1255 	clr	a
      00500B 23               [12] 1256 	rl	a
      00500C C8               [12] 1257 	xch	a,r0
      00500D C4               [12] 1258 	swap	a
      00500E 23               [12] 1259 	rl	a
      00500F 54 1F            [12] 1260 	anl	a,#0x1f
      005011 68               [12] 1261 	xrl	a,r0
      005012 C8               [12] 1262 	xch	a,r0
      005013 54 1F            [12] 1263 	anl	a,#0x1f
      005015 C8               [12] 1264 	xch	a,r0
      005016 68               [12] 1265 	xrl	a,r0
      005017 C8               [12] 1266 	xch	a,r0
      005018 30 E4 02         [24] 1267 	jnb	acc.4,00132$
      00501B 44 E0            [12] 1268 	orl	a,#0xe0
      00501D                       1269 00132$:
      00501D 85 23 82         [24] 1270 	mov	dpl,_BEACON_decoding_sloc1_1_0
      005020 85 24 83         [24] 1271 	mov	dph,(_BEACON_decoding_sloc1_1_0 + 1)
      005023 E8               [12] 1272 	mov	a,r0
      005024 F0               [24] 1273 	movx	@dptr,a
                                   1274 ;	..\src\peripherals\Beacon.c:67: beacon->Encryption_type = *(Rxbuffer+POS_SECURITY) & MASK_ENCRYPTION_TYPE;
      005025 74 09            [12] 1275 	mov	a,#0x09
      005027 2E               [12] 1276 	add	a,r6
      005028 FC               [12] 1277 	mov	r4,a
      005029 E4               [12] 1278 	clr	a
      00502A 3F               [12] 1279 	addc	a,r7
      00502B FD               [12] 1280 	mov	r5,a
      00502C 85 25 82         [24] 1281 	mov	dpl,_BEACON_decoding_sloc2_1_0
      00502F 85 26 83         [24] 1282 	mov	dph,(_BEACON_decoding_sloc2_1_0 + 1)
      005032 85 27 F0         [24] 1283 	mov	b,(_BEACON_decoding_sloc2_1_0 + 2)
      005035 12 8D 8E         [24] 1284 	lcall	__gptrget
      005038 F8               [12] 1285 	mov	r0,a
      005039 53 00 07         [24] 1286 	anl	ar0,#0x07
      00503C 8C 82            [24] 1287 	mov	dpl,r4
      00503E 8D 83            [24] 1288 	mov	dph,r5
      005040 E8               [12] 1289 	mov	a,r0
      005041 F0               [24] 1290 	movx	@dptr,a
                                   1291 ;	..\src\peripherals\Beacon.c:69: *(Rxbuffer+POS_NONCE)   = *(Rxbuffer+POS_NONCE) + *(Rxbuffer+POS_NONCE+1);
      005042 74 05            [12] 1292 	mov	a,#0x05
      005044 25 2B            [12] 1293 	add	a,_BEACON_decoding_sloc4_1_0
      005046 F5 25            [12] 1294 	mov	_BEACON_decoding_sloc2_1_0,a
      005048 E4               [12] 1295 	clr	a
      005049 35 2C            [12] 1296 	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
      00504B F5 26            [12] 1297 	mov	(_BEACON_decoding_sloc2_1_0 + 1),a
      00504D 85 2D 27         [24] 1298 	mov	(_BEACON_decoding_sloc2_1_0 + 2),(_BEACON_decoding_sloc4_1_0 + 2)
      005050 85 25 82         [24] 1299 	mov	dpl,_BEACON_decoding_sloc2_1_0
      005053 85 26 83         [24] 1300 	mov	dph,(_BEACON_decoding_sloc2_1_0 + 1)
      005056 85 27 F0         [24] 1301 	mov	b,(_BEACON_decoding_sloc2_1_0 + 2)
      005059 12 8D 8E         [24] 1302 	lcall	__gptrget
      00505C F5 23            [12] 1303 	mov	_BEACON_decoding_sloc1_1_0,a
      00505E 74 06            [12] 1304 	mov	a,#0x06
      005060 25 2B            [12] 1305 	add	a,_BEACON_decoding_sloc4_1_0
      005062 F5 28            [12] 1306 	mov	_BEACON_decoding_sloc3_1_0,a
      005064 E4               [12] 1307 	clr	a
      005065 35 2C            [12] 1308 	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
      005067 F5 29            [12] 1309 	mov	(_BEACON_decoding_sloc3_1_0 + 1),a
      005069 85 2D 2A         [24] 1310 	mov	(_BEACON_decoding_sloc3_1_0 + 2),(_BEACON_decoding_sloc4_1_0 + 2)
      00506C 85 28 82         [24] 1311 	mov	dpl,_BEACON_decoding_sloc3_1_0
      00506F 85 29 83         [24] 1312 	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
      005072 85 2A F0         [24] 1313 	mov	b,(_BEACON_decoding_sloc3_1_0 + 2)
      005075 12 8D 8E         [24] 1314 	lcall	__gptrget
      005078 25 23            [12] 1315 	add	a,_BEACON_decoding_sloc1_1_0
      00507A FD               [12] 1316 	mov	r5,a
      00507B 85 25 82         [24] 1317 	mov	dpl,_BEACON_decoding_sloc2_1_0
      00507E 85 26 83         [24] 1318 	mov	dph,(_BEACON_decoding_sloc2_1_0 + 1)
      005081 85 27 F0         [24] 1319 	mov	b,(_BEACON_decoding_sloc2_1_0 + 2)
      005084 12 7C 62         [24] 1320 	lcall	__gptrput
                                   1321 ;	..\src\peripherals\Beacon.c:70: *(Rxbuffer+POS_NONCE+1) = *(Rxbuffer+POS_NONCE) - *(Rxbuffer+POS_NONCE+1);
      005087 85 28 82         [24] 1322 	mov	dpl,_BEACON_decoding_sloc3_1_0
      00508A 85 29 83         [24] 1323 	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
      00508D 85 2A F0         [24] 1324 	mov	b,(_BEACON_decoding_sloc3_1_0 + 2)
      005090 12 8D 8E         [24] 1325 	lcall	__gptrget
      005093 FC               [12] 1326 	mov	r4,a
      005094 ED               [12] 1327 	mov	a,r5
      005095 C3               [12] 1328 	clr	c
      005096 9C               [12] 1329 	subb	a,r4
      005097 FC               [12] 1330 	mov	r4,a
      005098 85 28 82         [24] 1331 	mov	dpl,_BEACON_decoding_sloc3_1_0
      00509B 85 29 83         [24] 1332 	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
      00509E 85 2A F0         [24] 1333 	mov	b,(_BEACON_decoding_sloc3_1_0 + 2)
      0050A1 12 7C 62         [24] 1334 	lcall	__gptrput
                                   1335 ;	..\src\peripherals\Beacon.c:71: *(Rxbuffer+POS_NONCE)   = *(Rxbuffer+POS_NONCE) - *(Rxbuffer+POS_NONCE+1);
      0050A4 ED               [12] 1336 	mov	a,r5
      0050A5 C3               [12] 1337 	clr	c
      0050A6 9C               [12] 1338 	subb	a,r4
      0050A7 85 25 82         [24] 1339 	mov	dpl,_BEACON_decoding_sloc2_1_0
      0050AA 85 26 83         [24] 1340 	mov	dph,(_BEACON_decoding_sloc2_1_0 + 1)
      0050AD 85 27 F0         [24] 1341 	mov	b,(_BEACON_decoding_sloc2_1_0 + 2)
      0050B0 12 7C 62         [24] 1342 	lcall	__gptrput
                                   1343 ;	..\src\peripherals\Beacon.c:73: memcpy(&beacon->Nonce,Rxbuffer+POS_NONCE,sizeof(uint16_t));
      0050B3 74 0A            [12] 1344 	mov	a,#0x0a
      0050B5 2E               [12] 1345 	add	a,r6
      0050B6 FC               [12] 1346 	mov	r4,a
      0050B7 E4               [12] 1347 	clr	a
      0050B8 3F               [12] 1348 	addc	a,r7
      0050B9 FD               [12] 1349 	mov	r5,a
      0050BA 8C 28            [24] 1350 	mov	_BEACON_decoding_sloc3_1_0,r4
      0050BC 8D 29            [24] 1351 	mov	(_BEACON_decoding_sloc3_1_0 + 1),r5
      0050BE 75 2A 00         [24] 1352 	mov	(_BEACON_decoding_sloc3_1_0 + 2),#0x00
      0050C1 A8 25            [24] 1353 	mov	r0,_BEACON_decoding_sloc2_1_0
      0050C3 AC 26            [24] 1354 	mov	r4,(_BEACON_decoding_sloc2_1_0 + 1)
      0050C5 AD 27            [24] 1355 	mov	r5,(_BEACON_decoding_sloc2_1_0 + 2)
      0050C7 90 04 6D         [24] 1356 	mov	dptr,#_memcpy_PARM_2
      0050CA E8               [12] 1357 	mov	a,r0
      0050CB F0               [24] 1358 	movx	@dptr,a
      0050CC EC               [12] 1359 	mov	a,r4
      0050CD A3               [24] 1360 	inc	dptr
      0050CE F0               [24] 1361 	movx	@dptr,a
      0050CF ED               [12] 1362 	mov	a,r5
      0050D0 A3               [24] 1363 	inc	dptr
      0050D1 F0               [24] 1364 	movx	@dptr,a
      0050D2 90 04 70         [24] 1365 	mov	dptr,#_memcpy_PARM_3
      0050D5 74 02            [12] 1366 	mov	a,#0x02
      0050D7 F0               [24] 1367 	movx	@dptr,a
      0050D8 E4               [12] 1368 	clr	a
      0050D9 A3               [24] 1369 	inc	dptr
      0050DA F0               [24] 1370 	movx	@dptr,a
      0050DB 85 28 82         [24] 1371 	mov	dpl,_BEACON_decoding_sloc3_1_0
      0050DE 85 29 83         [24] 1372 	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
      0050E1 85 2A F0         [24] 1373 	mov	b,(_BEACON_decoding_sloc3_1_0 + 2)
      0050E4 C0 07            [24] 1374 	push	ar7
      0050E6 C0 06            [24] 1375 	push	ar6
      0050E8 12 78 14         [24] 1376 	lcall	_memcpy
      0050EB D0 06            [24] 1377 	pop	ar6
      0050ED D0 07            [24] 1378 	pop	ar7
                                   1379 ;	..\src\peripherals\Beacon.c:75: memcpy(&beacon->reserved,Rxbuffer+POS_RESERVED,sizeof(uint8_t)*5);
      0050EF 74 0C            [12] 1380 	mov	a,#0x0c
      0050F1 2E               [12] 1381 	add	a,r6
      0050F2 FC               [12] 1382 	mov	r4,a
      0050F3 E4               [12] 1383 	clr	a
      0050F4 3F               [12] 1384 	addc	a,r7
      0050F5 FD               [12] 1385 	mov	r5,a
      0050F6 8C 28            [24] 1386 	mov	_BEACON_decoding_sloc3_1_0,r4
      0050F8 8D 29            [24] 1387 	mov	(_BEACON_decoding_sloc3_1_0 + 1),r5
      0050FA 75 2A 00         [24] 1388 	mov	(_BEACON_decoding_sloc3_1_0 + 2),#0x00
      0050FD 74 07            [12] 1389 	mov	a,#0x07
      0050FF 25 2B            [12] 1390 	add	a,_BEACON_decoding_sloc4_1_0
      005101 F8               [12] 1391 	mov	r0,a
      005102 E4               [12] 1392 	clr	a
      005103 35 2C            [12] 1393 	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
      005105 FC               [12] 1394 	mov	r4,a
      005106 AD 2D            [24] 1395 	mov	r5,(_BEACON_decoding_sloc4_1_0 + 2)
      005108 90 04 6D         [24] 1396 	mov	dptr,#_memcpy_PARM_2
      00510B E8               [12] 1397 	mov	a,r0
      00510C F0               [24] 1398 	movx	@dptr,a
      00510D EC               [12] 1399 	mov	a,r4
      00510E A3               [24] 1400 	inc	dptr
      00510F F0               [24] 1401 	movx	@dptr,a
      005110 ED               [12] 1402 	mov	a,r5
      005111 A3               [24] 1403 	inc	dptr
      005112 F0               [24] 1404 	movx	@dptr,a
      005113 90 04 70         [24] 1405 	mov	dptr,#_memcpy_PARM_3
      005116 74 05            [12] 1406 	mov	a,#0x05
      005118 F0               [24] 1407 	movx	@dptr,a
      005119 E4               [12] 1408 	clr	a
      00511A A3               [24] 1409 	inc	dptr
      00511B F0               [24] 1410 	movx	@dptr,a
      00511C 85 28 82         [24] 1411 	mov	dpl,_BEACON_decoding_sloc3_1_0
      00511F 85 29 83         [24] 1412 	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
      005122 85 2A F0         [24] 1413 	mov	b,(_BEACON_decoding_sloc3_1_0 + 2)
      005125 C0 07            [24] 1414 	push	ar7
      005127 C0 06            [24] 1415 	push	ar6
      005129 12 78 14         [24] 1416 	lcall	_memcpy
      00512C D0 06            [24] 1417 	pop	ar6
      00512E D0 07            [24] 1418 	pop	ar7
                                   1419 ;	..\src\peripherals\Beacon.c:77: beacon->CRC =  *(Rxbuffer+POS_CRC) <<8 | *(Rxbuffer+POS_CRC+1);
      005130 74 11            [12] 1420 	mov	a,#0x11
      005132 2E               [12] 1421 	add	a,r6
      005133 F5 28            [12] 1422 	mov	_BEACON_decoding_sloc3_1_0,a
      005135 E4               [12] 1423 	clr	a
      005136 3F               [12] 1424 	addc	a,r7
      005137 F5 29            [12] 1425 	mov	(_BEACON_decoding_sloc3_1_0 + 1),a
      005139 74 0C            [12] 1426 	mov	a,#0x0c
      00513B 25 2B            [12] 1427 	add	a,_BEACON_decoding_sloc4_1_0
      00513D F8               [12] 1428 	mov	r0,a
      00513E E4               [12] 1429 	clr	a
      00513F 35 2C            [12] 1430 	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
      005141 FC               [12] 1431 	mov	r4,a
      005142 AD 2D            [24] 1432 	mov	r5,(_BEACON_decoding_sloc4_1_0 + 2)
      005144 88 82            [24] 1433 	mov	dpl,r0
      005146 8C 83            [24] 1434 	mov	dph,r4
      005148 8D F0            [24] 1435 	mov	b,r5
      00514A 12 8D 8E         [24] 1436 	lcall	__gptrget
      00514D F8               [12] 1437 	mov	r0,a
      00514E 7D 00            [12] 1438 	mov	r5,#0x00
      005150 88 26            [24] 1439 	mov	(_BEACON_decoding_sloc2_1_0 + 1),r0
                                   1440 ;	1-genFromRTrack replaced	mov	_BEACON_decoding_sloc2_1_0,#0x00
      005152 8D 25            [24] 1441 	mov	_BEACON_decoding_sloc2_1_0,r5
      005154 74 0D            [12] 1442 	mov	a,#0x0d
      005156 25 2B            [12] 1443 	add	a,_BEACON_decoding_sloc4_1_0
      005158 F8               [12] 1444 	mov	r0,a
      005159 E4               [12] 1445 	clr	a
      00515A 35 2C            [12] 1446 	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
      00515C FC               [12] 1447 	mov	r4,a
      00515D AD 2D            [24] 1448 	mov	r5,(_BEACON_decoding_sloc4_1_0 + 2)
      00515F 88 82            [24] 1449 	mov	dpl,r0
      005161 8C 83            [24] 1450 	mov	dph,r4
      005163 8D F0            [24] 1451 	mov	b,r5
      005165 12 8D 8E         [24] 1452 	lcall	__gptrget
      005168 F8               [12] 1453 	mov	r0,a
      005169 7D 00            [12] 1454 	mov	r5,#0x00
      00516B E5 25            [12] 1455 	mov	a,_BEACON_decoding_sloc2_1_0
      00516D 42 00            [12] 1456 	orl	ar0,a
      00516F E5 26            [12] 1457 	mov	a,(_BEACON_decoding_sloc2_1_0 + 1)
      005171 42 05            [12] 1458 	orl	ar5,a
      005173 ED               [12] 1459 	mov	a,r5
      005174 FB               [12] 1460 	mov	r3,a
      005175 33               [12] 1461 	rlc	a
      005176 95 E0            [12] 1462 	subb	a,acc
      005178 FC               [12] 1463 	mov	r4,a
      005179 FD               [12] 1464 	mov	r5,a
      00517A 85 28 82         [24] 1465 	mov	dpl,_BEACON_decoding_sloc3_1_0
      00517D 85 29 83         [24] 1466 	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
      005180 E8               [12] 1467 	mov	a,r0
      005181 F0               [24] 1468 	movx	@dptr,a
      005182 EB               [12] 1469 	mov	a,r3
      005183 A3               [24] 1470 	inc	dptr
      005184 F0               [24] 1471 	movx	@dptr,a
      005185 EC               [12] 1472 	mov	a,r4
      005186 A3               [24] 1473 	inc	dptr
      005187 F0               [24] 1474 	movx	@dptr,a
      005188 ED               [12] 1475 	mov	a,r5
      005189 A3               [24] 1476 	inc	dptr
      00518A F0               [24] 1477 	movx	@dptr,a
                                   1478 ;	..\src\peripherals\Beacon.c:78: beacon->CRC = beacon->CRC << 16;
      00518B 8B 31            [24] 1479 	mov	(_BEACON_decoding_sloc5_1_0 + 3),r3
      00518D 88 30            [24] 1480 	mov	(_BEACON_decoding_sloc5_1_0 + 2),r0
      00518F 75 2E 00         [24] 1481 	mov	_BEACON_decoding_sloc5_1_0,#0x00
      005192 75 2F 00         [24] 1482 	mov	(_BEACON_decoding_sloc5_1_0 + 1),#0x00
      005195 85 28 82         [24] 1483 	mov	dpl,_BEACON_decoding_sloc3_1_0
      005198 85 29 83         [24] 1484 	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
      00519B E5 2E            [12] 1485 	mov	a,_BEACON_decoding_sloc5_1_0
      00519D F0               [24] 1486 	movx	@dptr,a
      00519E E5 2F            [12] 1487 	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
      0051A0 A3               [24] 1488 	inc	dptr
      0051A1 F0               [24] 1489 	movx	@dptr,a
      0051A2 E5 30            [12] 1490 	mov	a,(_BEACON_decoding_sloc5_1_0 + 2)
      0051A4 A3               [24] 1491 	inc	dptr
      0051A5 F0               [24] 1492 	movx	@dptr,a
      0051A6 E5 31            [12] 1493 	mov	a,(_BEACON_decoding_sloc5_1_0 + 3)
      0051A8 A3               [24] 1494 	inc	dptr
      0051A9 F0               [24] 1495 	movx	@dptr,a
                                   1496 ;	..\src\peripherals\Beacon.c:79: beacon->CRC = beacon->CRC | *(Rxbuffer+POS_CRC+2)<<8 | *(Rxbuffer+POS_CRC+3);
      0051AA 74 0E            [12] 1497 	mov	a,#0x0e
      0051AC 25 2B            [12] 1498 	add	a,_BEACON_decoding_sloc4_1_0
      0051AE F9               [12] 1499 	mov	r1,a
      0051AF E4               [12] 1500 	clr	a
      0051B0 35 2C            [12] 1501 	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
      0051B2 FA               [12] 1502 	mov	r2,a
      0051B3 AD 2D            [24] 1503 	mov	r5,(_BEACON_decoding_sloc4_1_0 + 2)
      0051B5 89 82            [24] 1504 	mov	dpl,r1
      0051B7 8A 83            [24] 1505 	mov	dph,r2
      0051B9 8D F0            [24] 1506 	mov	b,r5
      0051BB 12 8D 8E         [24] 1507 	lcall	__gptrget
      0051BE FD               [12] 1508 	mov	r5,a
      0051BF 79 00            [12] 1509 	mov	r1,#0x00
      0051C1 33               [12] 1510 	rlc	a
      0051C2 95 E0            [12] 1511 	subb	a,acc
      0051C4 FC               [12] 1512 	mov	r4,a
      0051C5 FB               [12] 1513 	mov	r3,a
      0051C6 E9               [12] 1514 	mov	a,r1
      0051C7 42 2E            [12] 1515 	orl	_BEACON_decoding_sloc5_1_0,a
      0051C9 ED               [12] 1516 	mov	a,r5
      0051CA 42 2F            [12] 1517 	orl	(_BEACON_decoding_sloc5_1_0 + 1),a
      0051CC EC               [12] 1518 	mov	a,r4
      0051CD 42 30            [12] 1519 	orl	(_BEACON_decoding_sloc5_1_0 + 2),a
      0051CF EB               [12] 1520 	mov	a,r3
      0051D0 42 31            [12] 1521 	orl	(_BEACON_decoding_sloc5_1_0 + 3),a
      0051D2 74 0F            [12] 1522 	mov	a,#0x0f
      0051D4 25 2B            [12] 1523 	add	a,_BEACON_decoding_sloc4_1_0
      0051D6 F8               [12] 1524 	mov	r0,a
      0051D7 E4               [12] 1525 	clr	a
      0051D8 35 2C            [12] 1526 	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
      0051DA FA               [12] 1527 	mov	r2,a
      0051DB AD 2D            [24] 1528 	mov	r5,(_BEACON_decoding_sloc4_1_0 + 2)
      0051DD 88 82            [24] 1529 	mov	dpl,r0
      0051DF 8A 83            [24] 1530 	mov	dph,r2
      0051E1 8D F0            [24] 1531 	mov	b,r5
      0051E3 12 8D 8E         [24] 1532 	lcall	__gptrget
      0051E6 F8               [12] 1533 	mov	r0,a
      0051E7 E4               [12] 1534 	clr	a
      0051E8 FD               [12] 1535 	mov	r5,a
      0051E9 FC               [12] 1536 	mov	r4,a
      0051EA FB               [12] 1537 	mov	r3,a
      0051EB E5 2E            [12] 1538 	mov	a,_BEACON_decoding_sloc5_1_0
      0051ED 42 00            [12] 1539 	orl	ar0,a
      0051EF E5 2F            [12] 1540 	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
      0051F1 42 05            [12] 1541 	orl	ar5,a
      0051F3 E5 30            [12] 1542 	mov	a,(_BEACON_decoding_sloc5_1_0 + 2)
      0051F5 42 04            [12] 1543 	orl	ar4,a
      0051F7 E5 31            [12] 1544 	mov	a,(_BEACON_decoding_sloc5_1_0 + 3)
      0051F9 42 03            [12] 1545 	orl	ar3,a
      0051FB 85 28 82         [24] 1546 	mov	dpl,_BEACON_decoding_sloc3_1_0
      0051FE 85 29 83         [24] 1547 	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
      005201 E8               [12] 1548 	mov	a,r0
      005202 F0               [24] 1549 	movx	@dptr,a
      005203 ED               [12] 1550 	mov	a,r5
      005204 A3               [24] 1551 	inc	dptr
      005205 F0               [24] 1552 	movx	@dptr,a
      005206 EC               [12] 1553 	mov	a,r4
      005207 A3               [24] 1554 	inc	dptr
      005208 F0               [24] 1555 	movx	@dptr,a
      005209 EB               [12] 1556 	mov	a,r3
      00520A A3               [24] 1557 	inc	dptr
      00520B F0               [24] 1558 	movx	@dptr,a
                                   1559 ;	..\src\peripherals\Beacon.c:80: beacon->HMAC_Msb = *(Rxbuffer+POS_HMAC_MSB)<<8 | *(Rxbuffer+POS_HMAC_MSB+1);
      00520C 74 15            [12] 1560 	mov	a,#0x15
      00520E 2E               [12] 1561 	add	a,r6
      00520F FC               [12] 1562 	mov	r4,a
      005210 E4               [12] 1563 	clr	a
      005211 3F               [12] 1564 	addc	a,r7
      005212 FD               [12] 1565 	mov	r5,a
      005213 74 10            [12] 1566 	mov	a,#0x10
      005215 25 2B            [12] 1567 	add	a,_BEACON_decoding_sloc4_1_0
      005217 F9               [12] 1568 	mov	r1,a
      005218 E4               [12] 1569 	clr	a
      005219 35 2C            [12] 1570 	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
      00521B FA               [12] 1571 	mov	r2,a
      00521C AB 2D            [24] 1572 	mov	r3,(_BEACON_decoding_sloc4_1_0 + 2)
      00521E 89 82            [24] 1573 	mov	dpl,r1
      005220 8A 83            [24] 1574 	mov	dph,r2
      005222 8B F0            [24] 1575 	mov	b,r3
      005224 12 8D 8E         [24] 1576 	lcall	__gptrget
      005227 F9               [12] 1577 	mov	r1,a
      005228 7B 00            [12] 1578 	mov	r3,#0x00
      00522A 89 2F            [24] 1579 	mov	(_BEACON_decoding_sloc5_1_0 + 1),r1
                                   1580 ;	1-genFromRTrack replaced	mov	_BEACON_decoding_sloc5_1_0,#0x00
      00522C 8B 2E            [24] 1581 	mov	_BEACON_decoding_sloc5_1_0,r3
      00522E 74 11            [12] 1582 	mov	a,#0x11
      005230 25 2B            [12] 1583 	add	a,_BEACON_decoding_sloc4_1_0
      005232 F8               [12] 1584 	mov	r0,a
      005233 E4               [12] 1585 	clr	a
      005234 35 2C            [12] 1586 	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
      005236 FA               [12] 1587 	mov	r2,a
      005237 AB 2D            [24] 1588 	mov	r3,(_BEACON_decoding_sloc4_1_0 + 2)
      005239 88 82            [24] 1589 	mov	dpl,r0
      00523B 8A 83            [24] 1590 	mov	dph,r2
      00523D 8B F0            [24] 1591 	mov	b,r3
      00523F 12 8D 8E         [24] 1592 	lcall	__gptrget
      005242 F8               [12] 1593 	mov	r0,a
      005243 7B 00            [12] 1594 	mov	r3,#0x00
      005245 E5 2E            [12] 1595 	mov	a,_BEACON_decoding_sloc5_1_0
      005247 42 00            [12] 1596 	orl	ar0,a
      005249 E5 2F            [12] 1597 	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
      00524B 42 03            [12] 1598 	orl	ar3,a
      00524D EB               [12] 1599 	mov	a,r3
      00524E F9               [12] 1600 	mov	r1,a
      00524F 33               [12] 1601 	rlc	a
      005250 95 E0            [12] 1602 	subb	a,acc
      005252 FA               [12] 1603 	mov	r2,a
      005253 FB               [12] 1604 	mov	r3,a
      005254 8C 82            [24] 1605 	mov	dpl,r4
      005256 8D 83            [24] 1606 	mov	dph,r5
      005258 E8               [12] 1607 	mov	a,r0
      005259 F0               [24] 1608 	movx	@dptr,a
      00525A E9               [12] 1609 	mov	a,r1
      00525B A3               [24] 1610 	inc	dptr
      00525C F0               [24] 1611 	movx	@dptr,a
      00525D EA               [12] 1612 	mov	a,r2
      00525E A3               [24] 1613 	inc	dptr
      00525F F0               [24] 1614 	movx	@dptr,a
      005260 EB               [12] 1615 	mov	a,r3
      005261 A3               [24] 1616 	inc	dptr
      005262 F0               [24] 1617 	movx	@dptr,a
                                   1618 ;	..\src\peripherals\Beacon.c:81: beacon->HMAC_Msb = beacon->HMAC_Msb << 16;
      005263 89 31            [24] 1619 	mov	(_BEACON_decoding_sloc5_1_0 + 3),r1
      005265 88 30            [24] 1620 	mov	(_BEACON_decoding_sloc5_1_0 + 2),r0
      005267 75 2E 00         [24] 1621 	mov	_BEACON_decoding_sloc5_1_0,#0x00
      00526A 75 2F 00         [24] 1622 	mov	(_BEACON_decoding_sloc5_1_0 + 1),#0x00
      00526D 8C 82            [24] 1623 	mov	dpl,r4
      00526F 8D 83            [24] 1624 	mov	dph,r5
      005271 E5 2E            [12] 1625 	mov	a,_BEACON_decoding_sloc5_1_0
      005273 F0               [24] 1626 	movx	@dptr,a
      005274 E5 2F            [12] 1627 	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
      005276 A3               [24] 1628 	inc	dptr
      005277 F0               [24] 1629 	movx	@dptr,a
      005278 E5 30            [12] 1630 	mov	a,(_BEACON_decoding_sloc5_1_0 + 2)
      00527A A3               [24] 1631 	inc	dptr
      00527B F0               [24] 1632 	movx	@dptr,a
      00527C E5 31            [12] 1633 	mov	a,(_BEACON_decoding_sloc5_1_0 + 3)
      00527E A3               [24] 1634 	inc	dptr
      00527F F0               [24] 1635 	movx	@dptr,a
                                   1636 ;	..\src\peripherals\Beacon.c:82: beacon->HMAC_Msb = beacon->HMAC_Msb | (*(Rxbuffer+POS_HMAC_MSB+2)<<8 | *(Rxbuffer+POS_HMAC_MSB+3) & 0x0000FFFF);
      005280 74 12            [12] 1637 	mov	a,#0x12
      005282 25 2B            [12] 1638 	add	a,_BEACON_decoding_sloc4_1_0
      005284 F9               [12] 1639 	mov	r1,a
      005285 E4               [12] 1640 	clr	a
      005286 35 2C            [12] 1641 	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
      005288 FA               [12] 1642 	mov	r2,a
      005289 AB 2D            [24] 1643 	mov	r3,(_BEACON_decoding_sloc4_1_0 + 2)
      00528B 89 82            [24] 1644 	mov	dpl,r1
      00528D 8A 83            [24] 1645 	mov	dph,r2
      00528F 8B F0            [24] 1646 	mov	b,r3
      005291 12 8D 8E         [24] 1647 	lcall	__gptrget
      005294 F9               [12] 1648 	mov	r1,a
      005295 7B 00            [12] 1649 	mov	r3,#0x00
      005297 89 29            [24] 1650 	mov	(_BEACON_decoding_sloc3_1_0 + 1),r1
                                   1651 ;	1-genFromRTrack replaced	mov	_BEACON_decoding_sloc3_1_0,#0x00
      005299 8B 28            [24] 1652 	mov	_BEACON_decoding_sloc3_1_0,r3
      00529B 74 13            [12] 1653 	mov	a,#0x13
      00529D 25 2B            [12] 1654 	add	a,_BEACON_decoding_sloc4_1_0
      00529F F8               [12] 1655 	mov	r0,a
      0052A0 E4               [12] 1656 	clr	a
      0052A1 35 2C            [12] 1657 	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
      0052A3 FA               [12] 1658 	mov	r2,a
      0052A4 AB 2D            [24] 1659 	mov	r3,(_BEACON_decoding_sloc4_1_0 + 2)
      0052A6 88 82            [24] 1660 	mov	dpl,r0
      0052A8 8A 83            [24] 1661 	mov	dph,r2
      0052AA 8B F0            [24] 1662 	mov	b,r3
      0052AC 12 8D 8E         [24] 1663 	lcall	__gptrget
      0052AF F8               [12] 1664 	mov	r0,a
      0052B0 7B 00            [12] 1665 	mov	r3,#0x00
      0052B2 A9 28            [24] 1666 	mov	r1,_BEACON_decoding_sloc3_1_0
      0052B4 AA 29            [24] 1667 	mov	r2,(_BEACON_decoding_sloc3_1_0 + 1)
      0052B6 E9               [12] 1668 	mov	a,r1
      0052B7 42 00            [12] 1669 	orl	ar0,a
      0052B9 EA               [12] 1670 	mov	a,r2
      0052BA 42 03            [12] 1671 	orl	ar3,a
      0052BC 8B 01            [24] 1672 	mov	ar1,r3
      0052BE E4               [12] 1673 	clr	a
      0052BF FA               [12] 1674 	mov	r2,a
      0052C0 FB               [12] 1675 	mov	r3,a
      0052C1 E5 2E            [12] 1676 	mov	a,_BEACON_decoding_sloc5_1_0
      0052C3 42 00            [12] 1677 	orl	ar0,a
      0052C5 E5 2F            [12] 1678 	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
      0052C7 42 01            [12] 1679 	orl	ar1,a
      0052C9 E5 30            [12] 1680 	mov	a,(_BEACON_decoding_sloc5_1_0 + 2)
      0052CB 42 02            [12] 1681 	orl	ar2,a
      0052CD E5 31            [12] 1682 	mov	a,(_BEACON_decoding_sloc5_1_0 + 3)
      0052CF 42 03            [12] 1683 	orl	ar3,a
      0052D1 8C 82            [24] 1684 	mov	dpl,r4
      0052D3 8D 83            [24] 1685 	mov	dph,r5
      0052D5 E8               [12] 1686 	mov	a,r0
      0052D6 F0               [24] 1687 	movx	@dptr,a
      0052D7 E9               [12] 1688 	mov	a,r1
      0052D8 A3               [24] 1689 	inc	dptr
      0052D9 F0               [24] 1690 	movx	@dptr,a
      0052DA EA               [12] 1691 	mov	a,r2
      0052DB A3               [24] 1692 	inc	dptr
      0052DC F0               [24] 1693 	movx	@dptr,a
      0052DD EB               [12] 1694 	mov	a,r3
      0052DE A3               [24] 1695 	inc	dptr
      0052DF F0               [24] 1696 	movx	@dptr,a
                                   1697 ;	..\src\peripherals\Beacon.c:83: beacon->HMAC_Lsb = *(Rxbuffer+POS_HMAC_LSB)<<8 | *(Rxbuffer+POS_HMAC_LSB+1);
      0052E0 74 19            [12] 1698 	mov	a,#0x19
      0052E2 2E               [12] 1699 	add	a,r6
      0052E3 FE               [12] 1700 	mov	r6,a
      0052E4 E4               [12] 1701 	clr	a
      0052E5 3F               [12] 1702 	addc	a,r7
      0052E6 FF               [12] 1703 	mov	r7,a
      0052E7 74 14            [12] 1704 	mov	a,#0x14
      0052E9 25 2B            [12] 1705 	add	a,_BEACON_decoding_sloc4_1_0
      0052EB FB               [12] 1706 	mov	r3,a
      0052EC E4               [12] 1707 	clr	a
      0052ED 35 2C            [12] 1708 	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
      0052EF FC               [12] 1709 	mov	r4,a
      0052F0 AD 2D            [24] 1710 	mov	r5,(_BEACON_decoding_sloc4_1_0 + 2)
      0052F2 8B 82            [24] 1711 	mov	dpl,r3
      0052F4 8C 83            [24] 1712 	mov	dph,r4
      0052F6 8D F0            [24] 1713 	mov	b,r5
      0052F8 12 8D 8E         [24] 1714 	lcall	__gptrget
      0052FB FD               [12] 1715 	mov	r5,a
      0052FC 7B 00            [12] 1716 	mov	r3,#0x00
      0052FE 74 15            [12] 1717 	mov	a,#0x15
      005300 25 2B            [12] 1718 	add	a,_BEACON_decoding_sloc4_1_0
      005302 F9               [12] 1719 	mov	r1,a
      005303 E4               [12] 1720 	clr	a
      005304 35 2C            [12] 1721 	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
      005306 FA               [12] 1722 	mov	r2,a
      005307 AC 2D            [24] 1723 	mov	r4,(_BEACON_decoding_sloc4_1_0 + 2)
      005309 89 82            [24] 1724 	mov	dpl,r1
      00530B 8A 83            [24] 1725 	mov	dph,r2
      00530D 8C F0            [24] 1726 	mov	b,r4
      00530F 12 8D 8E         [24] 1727 	lcall	__gptrget
      005312 7C 00            [12] 1728 	mov	r4,#0x00
      005314 42 03            [12] 1729 	orl	ar3,a
      005316 EC               [12] 1730 	mov	a,r4
      005317 42 05            [12] 1731 	orl	ar5,a
      005319 ED               [12] 1732 	mov	a,r5
      00531A 33               [12] 1733 	rlc	a
      00531B 95 E0            [12] 1734 	subb	a,acc
      00531D FC               [12] 1735 	mov	r4,a
      00531E FA               [12] 1736 	mov	r2,a
      00531F 8E 82            [24] 1737 	mov	dpl,r6
      005321 8F 83            [24] 1738 	mov	dph,r7
      005323 EB               [12] 1739 	mov	a,r3
      005324 F0               [24] 1740 	movx	@dptr,a
      005325 ED               [12] 1741 	mov	a,r5
      005326 A3               [24] 1742 	inc	dptr
      005327 F0               [24] 1743 	movx	@dptr,a
      005328 EC               [12] 1744 	mov	a,r4
      005329 A3               [24] 1745 	inc	dptr
      00532A F0               [24] 1746 	movx	@dptr,a
      00532B EA               [12] 1747 	mov	a,r2
      00532C A3               [24] 1748 	inc	dptr
      00532D F0               [24] 1749 	movx	@dptr,a
                                   1750 ;	..\src\peripherals\Beacon.c:84: beacon->HMAC_Lsb = beacon->HMAC_Lsb << 16;
      00532E 8D 31            [24] 1751 	mov	(_BEACON_decoding_sloc5_1_0 + 3),r5
      005330 8B 30            [24] 1752 	mov	(_BEACON_decoding_sloc5_1_0 + 2),r3
      005332 75 2E 00         [24] 1753 	mov	_BEACON_decoding_sloc5_1_0,#0x00
      005335 75 2F 00         [24] 1754 	mov	(_BEACON_decoding_sloc5_1_0 + 1),#0x00
      005338 8E 82            [24] 1755 	mov	dpl,r6
      00533A 8F 83            [24] 1756 	mov	dph,r7
      00533C E5 2E            [12] 1757 	mov	a,_BEACON_decoding_sloc5_1_0
      00533E F0               [24] 1758 	movx	@dptr,a
      00533F E5 2F            [12] 1759 	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
      005341 A3               [24] 1760 	inc	dptr
      005342 F0               [24] 1761 	movx	@dptr,a
      005343 E5 30            [12] 1762 	mov	a,(_BEACON_decoding_sloc5_1_0 + 2)
      005345 A3               [24] 1763 	inc	dptr
      005346 F0               [24] 1764 	movx	@dptr,a
      005347 E5 31            [12] 1765 	mov	a,(_BEACON_decoding_sloc5_1_0 + 3)
      005349 A3               [24] 1766 	inc	dptr
      00534A F0               [24] 1767 	movx	@dptr,a
                                   1768 ;	..\src\peripherals\Beacon.c:85: beacon->HMAC_Lsb = beacon->HMAC_Lsb | (*(Rxbuffer+POS_HMAC_LSB+2)<<8 | *(Rxbuffer+POS_HMAC_LSB+3) & 0x0000FFFF);
      00534B 74 16            [12] 1769 	mov	a,#0x16
      00534D 25 2B            [12] 1770 	add	a,_BEACON_decoding_sloc4_1_0
      00534F F8               [12] 1771 	mov	r0,a
      005350 E4               [12] 1772 	clr	a
      005351 35 2C            [12] 1773 	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
      005353 F9               [12] 1774 	mov	r1,a
      005354 AD 2D            [24] 1775 	mov	r5,(_BEACON_decoding_sloc4_1_0 + 2)
      005356 88 82            [24] 1776 	mov	dpl,r0
      005358 89 83            [24] 1777 	mov	dph,r1
      00535A 8D F0            [24] 1778 	mov	b,r5
      00535C 12 8D 8E         [24] 1779 	lcall	__gptrget
      00535F FD               [12] 1780 	mov	r5,a
      005360 78 00            [12] 1781 	mov	r0,#0x00
      005362 74 17            [12] 1782 	mov	a,#0x17
      005364 25 2B            [12] 1783 	add	a,_BEACON_decoding_sloc4_1_0
      005366 FA               [12] 1784 	mov	r2,a
      005367 E4               [12] 1785 	clr	a
      005368 35 2C            [12] 1786 	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
      00536A FB               [12] 1787 	mov	r3,a
      00536B AC 2D            [24] 1788 	mov	r4,(_BEACON_decoding_sloc4_1_0 + 2)
      00536D 8A 82            [24] 1789 	mov	dpl,r2
      00536F 8B 83            [24] 1790 	mov	dph,r3
      005371 8C F0            [24] 1791 	mov	b,r4
      005373 12 8D 8E         [24] 1792 	lcall	__gptrget
      005376 FA               [12] 1793 	mov	r2,a
      005377 7C 00            [12] 1794 	mov	r4,#0x00
      005379 E8               [12] 1795 	mov	a,r0
      00537A 42 02            [12] 1796 	orl	ar2,a
      00537C ED               [12] 1797 	mov	a,r5
      00537D 42 04            [12] 1798 	orl	ar4,a
      00537F E4               [12] 1799 	clr	a
      005380 FD               [12] 1800 	mov	r5,a
      005381 FB               [12] 1801 	mov	r3,a
      005382 E5 2E            [12] 1802 	mov	a,_BEACON_decoding_sloc5_1_0
      005384 42 02            [12] 1803 	orl	ar2,a
      005386 E5 2F            [12] 1804 	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
      005388 42 04            [12] 1805 	orl	ar4,a
      00538A E5 30            [12] 1806 	mov	a,(_BEACON_decoding_sloc5_1_0 + 2)
      00538C 42 05            [12] 1807 	orl	ar5,a
      00538E E5 31            [12] 1808 	mov	a,(_BEACON_decoding_sloc5_1_0 + 3)
      005390 42 03            [12] 1809 	orl	ar3,a
      005392 8E 82            [24] 1810 	mov	dpl,r6
      005394 8F 83            [24] 1811 	mov	dph,r7
      005396 EA               [12] 1812 	mov	a,r2
      005397 F0               [24] 1813 	movx	@dptr,a
      005398 EC               [12] 1814 	mov	a,r4
      005399 A3               [24] 1815 	inc	dptr
      00539A F0               [24] 1816 	movx	@dptr,a
      00539B ED               [12] 1817 	mov	a,r5
      00539C A3               [24] 1818 	inc	dptr
      00539D F0               [24] 1819 	movx	@dptr,a
      00539E EB               [12] 1820 	mov	a,r3
      00539F A3               [24] 1821 	inc	dptr
      0053A0 F0               [24] 1822 	movx	@dptr,a
                                   1823 ;	..\src\peripherals\Beacon.c:89: }
      0053A1 22               [24] 1824 	ret
                                   1825 ;------------------------------------------------------------
                                   1826 ;Allocation info for local variables in function 'VerifyACK'
                                   1827 ;------------------------------------------------------------
                                   1828 ;length                    Allocated with name '_VerifyACK_PARM_2'
                                   1829 ;ReceivedData              Allocated with name '_VerifyACK_PARM_3'
                                   1830 ;Rxbuffer                  Allocated with name '_VerifyACK_Rxbuffer_65536_124'
                                   1831 ;i                         Allocated with name '_VerifyACK_i_65536_125'
                                   1832 ;RetVal                    Allocated with name '_VerifyACK_RetVal_65536_125'
                                   1833 ;sloc0                     Allocated with name '_VerifyACK_sloc0_1_0'
                                   1834 ;sloc1                     Allocated with name '_VerifyACK_sloc1_1_0'
                                   1835 ;------------------------------------------------------------
                                   1836 ;	..\src\peripherals\Beacon.c:101: uint8_t VerifyACK(uint8_t* Rxbuffer,uint8_t length, uint8_t* ReceivedData)
                                   1837 ;	-----------------------------------------
                                   1838 ;	 function VerifyACK
                                   1839 ;	-----------------------------------------
      0053A2                       1840 _VerifyACK:
      0053A2 AF F0            [24] 1841 	mov	r7,b
      0053A4 AE 83            [24] 1842 	mov	r6,dph
      0053A6 E5 82            [12] 1843 	mov	a,dpl
      0053A8 90 03 B9         [24] 1844 	mov	dptr,#_VerifyACK_Rxbuffer_65536_124
      0053AB F0               [24] 1845 	movx	@dptr,a
      0053AC EE               [12] 1846 	mov	a,r6
      0053AD A3               [24] 1847 	inc	dptr
      0053AE F0               [24] 1848 	movx	@dptr,a
      0053AF EF               [12] 1849 	mov	a,r7
      0053B0 A3               [24] 1850 	inc	dptr
      0053B1 F0               [24] 1851 	movx	@dptr,a
                                   1852 ;	..\src\peripherals\Beacon.c:104: uint8_t RetVal = NoACK;
      0053B2 90 03 BC         [24] 1853 	mov	dptr,#_VerifyACK_RetVal_65536_125
      0053B5 E4               [12] 1854 	clr	a
      0053B6 F0               [24] 1855 	movx	@dptr,a
                                   1856 ;	..\src\peripherals\Beacon.c:105: for(i = 0;i < length; i = i+3) //revisar bien desde donde empiezo a tomar el dato
      0053B7 90 03 B9         [24] 1857 	mov	dptr,#_VerifyACK_Rxbuffer_65536_124
      0053BA E0               [24] 1858 	movx	a,@dptr
      0053BB F5 5E            [12] 1859 	mov	_VerifyACK_sloc0_1_0,a
      0053BD A3               [24] 1860 	inc	dptr
      0053BE E0               [24] 1861 	movx	a,@dptr
      0053BF F5 5F            [12] 1862 	mov	(_VerifyACK_sloc0_1_0 + 1),a
      0053C1 A3               [24] 1863 	inc	dptr
      0053C2 E0               [24] 1864 	movx	a,@dptr
      0053C3 F5 60            [12] 1865 	mov	(_VerifyACK_sloc0_1_0 + 2),a
      0053C5 90 03 B5         [24] 1866 	mov	dptr,#_VerifyACK_PARM_2
      0053C8 E0               [24] 1867 	movx	a,@dptr
      0053C9 FC               [12] 1868 	mov	r4,a
      0053CA 7A 00            [12] 1869 	mov	r2,#0x00
      0053CC 7B 00            [12] 1870 	mov	r3,#0x00
      0053CE                       1871 00105$:
      0053CE 8C 00            [24] 1872 	mov	ar0,r4
      0053D0 79 00            [12] 1873 	mov	r1,#0x00
      0053D2 C3               [12] 1874 	clr	c
      0053D3 EA               [12] 1875 	mov	a,r2
      0053D4 98               [12] 1876 	subb	a,r0
      0053D5 EB               [12] 1877 	mov	a,r3
      0053D6 64 80            [12] 1878 	xrl	a,#0x80
      0053D8 89 F0            [24] 1879 	mov	b,r1
      0053DA 63 F0 80         [24] 1880 	xrl	b,#0x80
      0053DD 95 F0            [12] 1881 	subb	a,b
      0053DF 50 65            [24] 1882 	jnc	00103$
                                   1883 ;	..\src\peripherals\Beacon.c:107: if(*(Rxbuffer+i) == 0x12/*localaddr.addr[0]*/ & *(Rxbuffer+i+1) == 0x34 /*localaddr.addr[1]*/ & *(Rxbuffer+i+2) == 0x56 /*localaddr.addr[2]*/)
      0053E1 C0 04            [24] 1884 	push	ar4
      0053E3 EA               [12] 1885 	mov	a,r2
      0053E4 25 5E            [12] 1886 	add	a,_VerifyACK_sloc0_1_0
      0053E6 F8               [12] 1887 	mov	r0,a
      0053E7 EB               [12] 1888 	mov	a,r3
      0053E8 35 5F            [12] 1889 	addc	a,(_VerifyACK_sloc0_1_0 + 1)
      0053EA F9               [12] 1890 	mov	r1,a
      0053EB AC 60            [24] 1891 	mov	r4,(_VerifyACK_sloc0_1_0 + 2)
      0053ED 88 82            [24] 1892 	mov	dpl,r0
      0053EF 89 83            [24] 1893 	mov	dph,r1
      0053F1 8C F0            [24] 1894 	mov	b,r4
      0053F3 12 8D 8E         [24] 1895 	lcall	__gptrget
      0053F6 FF               [12] 1896 	mov	r7,a
      0053F7 E4               [12] 1897 	clr	a
      0053F8 BF 12 01         [24] 1898 	cjne	r7,#0x12,00122$
      0053FB 04               [12] 1899 	inc	a
      0053FC                       1900 00122$:
      0053FC F5 61            [12] 1901 	mov	_VerifyACK_sloc1_1_0,a
      0053FE 74 01            [12] 1902 	mov	a,#0x01
      005400 28               [12] 1903 	add	a,r0
      005401 FD               [12] 1904 	mov	r5,a
      005402 E4               [12] 1905 	clr	a
      005403 39               [12] 1906 	addc	a,r1
      005404 FE               [12] 1907 	mov	r6,a
      005405 8C 07            [24] 1908 	mov	ar7,r4
      005407 8D 82            [24] 1909 	mov	dpl,r5
      005409 8E 83            [24] 1910 	mov	dph,r6
      00540B 8F F0            [24] 1911 	mov	b,r7
      00540D 12 8D 8E         [24] 1912 	lcall	__gptrget
      005410 FD               [12] 1913 	mov	r5,a
      005411 E4               [12] 1914 	clr	a
      005412 BD 34 01         [24] 1915 	cjne	r5,#0x34,00124$
      005415 04               [12] 1916 	inc	a
      005416                       1917 00124$:
      005416 55 61            [12] 1918 	anl	a,_VerifyACK_sloc1_1_0
      005418 FF               [12] 1919 	mov	r7,a
      005419 74 02            [12] 1920 	mov	a,#0x02
      00541B 28               [12] 1921 	add	a,r0
      00541C F8               [12] 1922 	mov	r0,a
      00541D E4               [12] 1923 	clr	a
      00541E 39               [12] 1924 	addc	a,r1
      00541F F9               [12] 1925 	mov	r1,a
      005420 88 82            [24] 1926 	mov	dpl,r0
      005422 89 83            [24] 1927 	mov	dph,r1
      005424 8C F0            [24] 1928 	mov	b,r4
      005426 12 8D 8E         [24] 1929 	lcall	__gptrget
      005429 F8               [12] 1930 	mov	r0,a
      00542A E4               [12] 1931 	clr	a
      00542B B8 56 01         [24] 1932 	cjne	r0,#0x56,00126$
      00542E 04               [12] 1933 	inc	a
      00542F                       1934 00126$:
      00542F 5F               [12] 1935 	anl	a,r7
      005430 D0 04            [24] 1936 	pop	ar4
      005432 60 08            [24] 1937 	jz	00106$
                                   1938 ;	..\src\peripherals\Beacon.c:109: RetVal = ReceivedACK;
      005434 90 03 BC         [24] 1939 	mov	dptr,#_VerifyACK_RetVal_65536_125
      005437 74 01            [12] 1940 	mov	a,#0x01
      005439 F0               [24] 1941 	movx	@dptr,a
                                   1942 ;	..\src\peripherals\Beacon.c:110: break;
      00543A 80 0A            [24] 1943 	sjmp	00103$
      00543C                       1944 00106$:
                                   1945 ;	..\src\peripherals\Beacon.c:105: for(i = 0;i < length; i = i+3) //revisar bien desde donde empiezo a tomar el dato
      00543C 74 03            [12] 1946 	mov	a,#0x03
      00543E 2A               [12] 1947 	add	a,r2
      00543F FA               [12] 1948 	mov	r2,a
      005440 E4               [12] 1949 	clr	a
      005441 3B               [12] 1950 	addc	a,r3
      005442 FB               [12] 1951 	mov	r3,a
      005443 02 53 CE         [24] 1952 	ljmp	00105$
      005446                       1953 00103$:
                                   1954 ;	..\src\peripherals\Beacon.c:113: return RetVal;
      005446 90 03 BC         [24] 1955 	mov	dptr,#_VerifyACK_RetVal_65536_125
      005449 E0               [24] 1956 	movx	a,@dptr
                                   1957 ;	..\src\peripherals\Beacon.c:114: }
      00544A F5 82            [12] 1958 	mov	dpl,a
      00544C 22               [24] 1959 	ret
                                   1960 	.area CSEG    (CODE)
                                   1961 	.area CONST   (CODE)
                                   1962 	.area CONST   (CODE)
      0091DB                       1963 ___str_0:
      0091DB 20 6C 61 72 67 6F 20  1964 	.ascii " largo = "
             3D 20
      0091E4 00                    1965 	.db 0x00
                                   1966 	.area CSEG    (CODE)
                                   1967 	.area CONST   (CODE)
      0091E5                       1968 ___str_1:
      0091E5 20 55 70 6C 69 6E 6B  1969 	.ascii " Uplink"
      0091EC 00                    1970 	.db 0x00
                                   1971 	.area CSEG    (CODE)
                                   1972 	.area CONST   (CODE)
      0091ED                       1973 ___str_2:
      0091ED 20 44 6F 77 6E 6C 69  1974 	.ascii " Downlink "
             6E 6B 20
      0091F7 00                    1975 	.db 0x00
                                   1976 	.area CSEG    (CODE)
                                   1977 	.area XINIT   (CODE)
                                   1978 	.area CABS    (ABS,CODE)
