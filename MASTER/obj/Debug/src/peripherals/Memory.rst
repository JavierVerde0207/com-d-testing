                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.9.3 #11354 (MINGW32)
                                      4 ;--------------------------------------------------------
                                      5 	.module Memory
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _flash_read
                                     12 	.globl _flash_write
                                     13 	.globl _flash_lock
                                     14 	.globl _flash_unlock
                                     15 	.globl _PORTC_7
                                     16 	.globl _PORTC_6
                                     17 	.globl _PORTC_5
                                     18 	.globl _PORTC_4
                                     19 	.globl _PORTC_3
                                     20 	.globl _PORTC_2
                                     21 	.globl _PORTC_1
                                     22 	.globl _PORTC_0
                                     23 	.globl _PORTB_7
                                     24 	.globl _PORTB_6
                                     25 	.globl _PORTB_5
                                     26 	.globl _PORTB_4
                                     27 	.globl _PORTB_3
                                     28 	.globl _PORTB_2
                                     29 	.globl _PORTB_1
                                     30 	.globl _PORTB_0
                                     31 	.globl _PORTA_7
                                     32 	.globl _PORTA_6
                                     33 	.globl _PORTA_5
                                     34 	.globl _PORTA_4
                                     35 	.globl _PORTA_3
                                     36 	.globl _PORTA_2
                                     37 	.globl _PORTA_1
                                     38 	.globl _PORTA_0
                                     39 	.globl _PINC_7
                                     40 	.globl _PINC_6
                                     41 	.globl _PINC_5
                                     42 	.globl _PINC_4
                                     43 	.globl _PINC_3
                                     44 	.globl _PINC_2
                                     45 	.globl _PINC_1
                                     46 	.globl _PINC_0
                                     47 	.globl _PINB_7
                                     48 	.globl _PINB_6
                                     49 	.globl _PINB_5
                                     50 	.globl _PINB_4
                                     51 	.globl _PINB_3
                                     52 	.globl _PINB_2
                                     53 	.globl _PINB_1
                                     54 	.globl _PINB_0
                                     55 	.globl _PINA_7
                                     56 	.globl _PINA_6
                                     57 	.globl _PINA_5
                                     58 	.globl _PINA_4
                                     59 	.globl _PINA_3
                                     60 	.globl _PINA_2
                                     61 	.globl _PINA_1
                                     62 	.globl _PINA_0
                                     63 	.globl _CY
                                     64 	.globl _AC
                                     65 	.globl _F0
                                     66 	.globl _RS1
                                     67 	.globl _RS0
                                     68 	.globl _OV
                                     69 	.globl _F1
                                     70 	.globl _P
                                     71 	.globl _IP_7
                                     72 	.globl _IP_6
                                     73 	.globl _IP_5
                                     74 	.globl _IP_4
                                     75 	.globl _IP_3
                                     76 	.globl _IP_2
                                     77 	.globl _IP_1
                                     78 	.globl _IP_0
                                     79 	.globl _EA
                                     80 	.globl _IE_7
                                     81 	.globl _IE_6
                                     82 	.globl _IE_5
                                     83 	.globl _IE_4
                                     84 	.globl _IE_3
                                     85 	.globl _IE_2
                                     86 	.globl _IE_1
                                     87 	.globl _IE_0
                                     88 	.globl _EIP_7
                                     89 	.globl _EIP_6
                                     90 	.globl _EIP_5
                                     91 	.globl _EIP_4
                                     92 	.globl _EIP_3
                                     93 	.globl _EIP_2
                                     94 	.globl _EIP_1
                                     95 	.globl _EIP_0
                                     96 	.globl _EIE_7
                                     97 	.globl _EIE_6
                                     98 	.globl _EIE_5
                                     99 	.globl _EIE_4
                                    100 	.globl _EIE_3
                                    101 	.globl _EIE_2
                                    102 	.globl _EIE_1
                                    103 	.globl _EIE_0
                                    104 	.globl _E2IP_7
                                    105 	.globl _E2IP_6
                                    106 	.globl _E2IP_5
                                    107 	.globl _E2IP_4
                                    108 	.globl _E2IP_3
                                    109 	.globl _E2IP_2
                                    110 	.globl _E2IP_1
                                    111 	.globl _E2IP_0
                                    112 	.globl _E2IE_7
                                    113 	.globl _E2IE_6
                                    114 	.globl _E2IE_5
                                    115 	.globl _E2IE_4
                                    116 	.globl _E2IE_3
                                    117 	.globl _E2IE_2
                                    118 	.globl _E2IE_1
                                    119 	.globl _E2IE_0
                                    120 	.globl _B_7
                                    121 	.globl _B_6
                                    122 	.globl _B_5
                                    123 	.globl _B_4
                                    124 	.globl _B_3
                                    125 	.globl _B_2
                                    126 	.globl _B_1
                                    127 	.globl _B_0
                                    128 	.globl _ACC_7
                                    129 	.globl _ACC_6
                                    130 	.globl _ACC_5
                                    131 	.globl _ACC_4
                                    132 	.globl _ACC_3
                                    133 	.globl _ACC_2
                                    134 	.globl _ACC_1
                                    135 	.globl _ACC_0
                                    136 	.globl _WTSTAT
                                    137 	.globl _WTIRQEN
                                    138 	.globl _WTEVTD
                                    139 	.globl _WTEVTD1
                                    140 	.globl _WTEVTD0
                                    141 	.globl _WTEVTC
                                    142 	.globl _WTEVTC1
                                    143 	.globl _WTEVTC0
                                    144 	.globl _WTEVTB
                                    145 	.globl _WTEVTB1
                                    146 	.globl _WTEVTB0
                                    147 	.globl _WTEVTA
                                    148 	.globl _WTEVTA1
                                    149 	.globl _WTEVTA0
                                    150 	.globl _WTCNTR1
                                    151 	.globl _WTCNTB
                                    152 	.globl _WTCNTB1
                                    153 	.globl _WTCNTB0
                                    154 	.globl _WTCNTA
                                    155 	.globl _WTCNTA1
                                    156 	.globl _WTCNTA0
                                    157 	.globl _WTCFGB
                                    158 	.globl _WTCFGA
                                    159 	.globl _WDTRESET
                                    160 	.globl _WDTCFG
                                    161 	.globl _U1STATUS
                                    162 	.globl _U1SHREG
                                    163 	.globl _U1MODE
                                    164 	.globl _U1CTRL
                                    165 	.globl _U0STATUS
                                    166 	.globl _U0SHREG
                                    167 	.globl _U0MODE
                                    168 	.globl _U0CTRL
                                    169 	.globl _T2STATUS
                                    170 	.globl _T2PERIOD
                                    171 	.globl _T2PERIOD1
                                    172 	.globl _T2PERIOD0
                                    173 	.globl _T2MODE
                                    174 	.globl _T2CNT
                                    175 	.globl _T2CNT1
                                    176 	.globl _T2CNT0
                                    177 	.globl _T2CLKSRC
                                    178 	.globl _T1STATUS
                                    179 	.globl _T1PERIOD
                                    180 	.globl _T1PERIOD1
                                    181 	.globl _T1PERIOD0
                                    182 	.globl _T1MODE
                                    183 	.globl _T1CNT
                                    184 	.globl _T1CNT1
                                    185 	.globl _T1CNT0
                                    186 	.globl _T1CLKSRC
                                    187 	.globl _T0STATUS
                                    188 	.globl _T0PERIOD
                                    189 	.globl _T0PERIOD1
                                    190 	.globl _T0PERIOD0
                                    191 	.globl _T0MODE
                                    192 	.globl _T0CNT
                                    193 	.globl _T0CNT1
                                    194 	.globl _T0CNT0
                                    195 	.globl _T0CLKSRC
                                    196 	.globl _SPSTATUS
                                    197 	.globl _SPSHREG
                                    198 	.globl _SPMODE
                                    199 	.globl _SPCLKSRC
                                    200 	.globl _RADIOSTAT
                                    201 	.globl _RADIOSTAT1
                                    202 	.globl _RADIOSTAT0
                                    203 	.globl _RADIODATA
                                    204 	.globl _RADIODATA3
                                    205 	.globl _RADIODATA2
                                    206 	.globl _RADIODATA1
                                    207 	.globl _RADIODATA0
                                    208 	.globl _RADIOADDR
                                    209 	.globl _RADIOADDR1
                                    210 	.globl _RADIOADDR0
                                    211 	.globl _RADIOACC
                                    212 	.globl _OC1STATUS
                                    213 	.globl _OC1PIN
                                    214 	.globl _OC1MODE
                                    215 	.globl _OC1COMP
                                    216 	.globl _OC1COMP1
                                    217 	.globl _OC1COMP0
                                    218 	.globl _OC0STATUS
                                    219 	.globl _OC0PIN
                                    220 	.globl _OC0MODE
                                    221 	.globl _OC0COMP
                                    222 	.globl _OC0COMP1
                                    223 	.globl _OC0COMP0
                                    224 	.globl _NVSTATUS
                                    225 	.globl _NVKEY
                                    226 	.globl _NVDATA
                                    227 	.globl _NVDATA1
                                    228 	.globl _NVDATA0
                                    229 	.globl _NVADDR
                                    230 	.globl _NVADDR1
                                    231 	.globl _NVADDR0
                                    232 	.globl _IC1STATUS
                                    233 	.globl _IC1MODE
                                    234 	.globl _IC1CAPT
                                    235 	.globl _IC1CAPT1
                                    236 	.globl _IC1CAPT0
                                    237 	.globl _IC0STATUS
                                    238 	.globl _IC0MODE
                                    239 	.globl _IC0CAPT
                                    240 	.globl _IC0CAPT1
                                    241 	.globl _IC0CAPT0
                                    242 	.globl _PORTR
                                    243 	.globl _PORTC
                                    244 	.globl _PORTB
                                    245 	.globl _PORTA
                                    246 	.globl _PINR
                                    247 	.globl _PINC
                                    248 	.globl _PINB
                                    249 	.globl _PINA
                                    250 	.globl _DIRR
                                    251 	.globl _DIRC
                                    252 	.globl _DIRB
                                    253 	.globl _DIRA
                                    254 	.globl _DBGLNKSTAT
                                    255 	.globl _DBGLNKBUF
                                    256 	.globl _CODECONFIG
                                    257 	.globl _CLKSTAT
                                    258 	.globl _CLKCON
                                    259 	.globl _ANALOGCOMP
                                    260 	.globl _ADCCONV
                                    261 	.globl _ADCCLKSRC
                                    262 	.globl _ADCCH3CONFIG
                                    263 	.globl _ADCCH2CONFIG
                                    264 	.globl _ADCCH1CONFIG
                                    265 	.globl _ADCCH0CONFIG
                                    266 	.globl __XPAGE
                                    267 	.globl _XPAGE
                                    268 	.globl _SP
                                    269 	.globl _PSW
                                    270 	.globl _PCON
                                    271 	.globl _IP
                                    272 	.globl _IE
                                    273 	.globl _EIP
                                    274 	.globl _EIE
                                    275 	.globl _E2IP
                                    276 	.globl _E2IE
                                    277 	.globl _DPS
                                    278 	.globl _DPTR1
                                    279 	.globl _DPTR0
                                    280 	.globl _DPL1
                                    281 	.globl _DPL
                                    282 	.globl _DPH1
                                    283 	.globl _DPH
                                    284 	.globl _B
                                    285 	.globl _ACC
                                    286 	.globl _WriteMemory_PARM_3
                                    287 	.globl _WriteMemory_PARM_2
                                    288 	.globl _RNGCLKSRC1
                                    289 	.globl _RNGCLKSRC0
                                    290 	.globl _RNGMODE
                                    291 	.globl _AESOUTADDR
                                    292 	.globl _AESOUTADDR1
                                    293 	.globl _AESOUTADDR0
                                    294 	.globl _AESMODE
                                    295 	.globl _AESKEYADDR
                                    296 	.globl _AESKEYADDR1
                                    297 	.globl _AESKEYADDR0
                                    298 	.globl _AESINADDR
                                    299 	.globl _AESINADDR1
                                    300 	.globl _AESINADDR0
                                    301 	.globl _AESCURBLOCK
                                    302 	.globl _AESCONFIG
                                    303 	.globl _RNGBYTE
                                    304 	.globl _XTALREADY
                                    305 	.globl _XTALOSC
                                    306 	.globl _XTALAMPL
                                    307 	.globl _SILICONREV
                                    308 	.globl _SCRATCH3
                                    309 	.globl _SCRATCH2
                                    310 	.globl _SCRATCH1
                                    311 	.globl _SCRATCH0
                                    312 	.globl _RADIOMUX
                                    313 	.globl _RADIOFSTATADDR
                                    314 	.globl _RADIOFSTATADDR1
                                    315 	.globl _RADIOFSTATADDR0
                                    316 	.globl _RADIOFDATAADDR
                                    317 	.globl _RADIOFDATAADDR1
                                    318 	.globl _RADIOFDATAADDR0
                                    319 	.globl _OSCRUN
                                    320 	.globl _OSCREADY
                                    321 	.globl _OSCFORCERUN
                                    322 	.globl _OSCCALIB
                                    323 	.globl _MISCCTRL
                                    324 	.globl _LPXOSCGM
                                    325 	.globl _LPOSCREF
                                    326 	.globl _LPOSCREF1
                                    327 	.globl _LPOSCREF0
                                    328 	.globl _LPOSCPER
                                    329 	.globl _LPOSCPER1
                                    330 	.globl _LPOSCPER0
                                    331 	.globl _LPOSCKFILT
                                    332 	.globl _LPOSCKFILT1
                                    333 	.globl _LPOSCKFILT0
                                    334 	.globl _LPOSCFREQ
                                    335 	.globl _LPOSCFREQ1
                                    336 	.globl _LPOSCFREQ0
                                    337 	.globl _LPOSCCONFIG
                                    338 	.globl _PINSEL
                                    339 	.globl _PINCHGC
                                    340 	.globl _PINCHGB
                                    341 	.globl _PINCHGA
                                    342 	.globl _PALTRADIO
                                    343 	.globl _PALTC
                                    344 	.globl _PALTB
                                    345 	.globl _PALTA
                                    346 	.globl _INTCHGC
                                    347 	.globl _INTCHGB
                                    348 	.globl _INTCHGA
                                    349 	.globl _EXTIRQ
                                    350 	.globl _GPIOENABLE
                                    351 	.globl _ANALOGA
                                    352 	.globl _FRCOSCREF
                                    353 	.globl _FRCOSCREF1
                                    354 	.globl _FRCOSCREF0
                                    355 	.globl _FRCOSCPER
                                    356 	.globl _FRCOSCPER1
                                    357 	.globl _FRCOSCPER0
                                    358 	.globl _FRCOSCKFILT
                                    359 	.globl _FRCOSCKFILT1
                                    360 	.globl _FRCOSCKFILT0
                                    361 	.globl _FRCOSCFREQ
                                    362 	.globl _FRCOSCFREQ1
                                    363 	.globl _FRCOSCFREQ0
                                    364 	.globl _FRCOSCCTRL
                                    365 	.globl _FRCOSCCONFIG
                                    366 	.globl _DMA1CONFIG
                                    367 	.globl _DMA1ADDR
                                    368 	.globl _DMA1ADDR1
                                    369 	.globl _DMA1ADDR0
                                    370 	.globl _DMA0CONFIG
                                    371 	.globl _DMA0ADDR
                                    372 	.globl _DMA0ADDR1
                                    373 	.globl _DMA0ADDR0
                                    374 	.globl _ADCTUNE2
                                    375 	.globl _ADCTUNE1
                                    376 	.globl _ADCTUNE0
                                    377 	.globl _ADCCH3VAL
                                    378 	.globl _ADCCH3VAL1
                                    379 	.globl _ADCCH3VAL0
                                    380 	.globl _ADCCH2VAL
                                    381 	.globl _ADCCH2VAL1
                                    382 	.globl _ADCCH2VAL0
                                    383 	.globl _ADCCH1VAL
                                    384 	.globl _ADCCH1VAL1
                                    385 	.globl _ADCCH1VAL0
                                    386 	.globl _ADCCH0VAL
                                    387 	.globl _ADCCH0VAL1
                                    388 	.globl _ADCCH0VAL0
                                    389 	.globl _ReadFromMemory
                                    390 	.globl _WriteMemory
                                    391 	.globl _EraseMemoryPage
                                    392 ;--------------------------------------------------------
                                    393 ; special function registers
                                    394 ;--------------------------------------------------------
                                    395 	.area RSEG    (ABS,DATA)
      000000                        396 	.org 0x0000
                           0000E0   397 _ACC	=	0x00e0
                           0000F0   398 _B	=	0x00f0
                           000083   399 _DPH	=	0x0083
                           000085   400 _DPH1	=	0x0085
                           000082   401 _DPL	=	0x0082
                           000084   402 _DPL1	=	0x0084
                           008382   403 _DPTR0	=	0x8382
                           008584   404 _DPTR1	=	0x8584
                           000086   405 _DPS	=	0x0086
                           0000A0   406 _E2IE	=	0x00a0
                           0000C0   407 _E2IP	=	0x00c0
                           000098   408 _EIE	=	0x0098
                           0000B0   409 _EIP	=	0x00b0
                           0000A8   410 _IE	=	0x00a8
                           0000B8   411 _IP	=	0x00b8
                           000087   412 _PCON	=	0x0087
                           0000D0   413 _PSW	=	0x00d0
                           000081   414 _SP	=	0x0081
                           0000D9   415 _XPAGE	=	0x00d9
                           0000D9   416 __XPAGE	=	0x00d9
                           0000CA   417 _ADCCH0CONFIG	=	0x00ca
                           0000CB   418 _ADCCH1CONFIG	=	0x00cb
                           0000D2   419 _ADCCH2CONFIG	=	0x00d2
                           0000D3   420 _ADCCH3CONFIG	=	0x00d3
                           0000D1   421 _ADCCLKSRC	=	0x00d1
                           0000C9   422 _ADCCONV	=	0x00c9
                           0000E1   423 _ANALOGCOMP	=	0x00e1
                           0000C6   424 _CLKCON	=	0x00c6
                           0000C7   425 _CLKSTAT	=	0x00c7
                           000097   426 _CODECONFIG	=	0x0097
                           0000E3   427 _DBGLNKBUF	=	0x00e3
                           0000E2   428 _DBGLNKSTAT	=	0x00e2
                           000089   429 _DIRA	=	0x0089
                           00008A   430 _DIRB	=	0x008a
                           00008B   431 _DIRC	=	0x008b
                           00008E   432 _DIRR	=	0x008e
                           0000C8   433 _PINA	=	0x00c8
                           0000E8   434 _PINB	=	0x00e8
                           0000F8   435 _PINC	=	0x00f8
                           00008D   436 _PINR	=	0x008d
                           000080   437 _PORTA	=	0x0080
                           000088   438 _PORTB	=	0x0088
                           000090   439 _PORTC	=	0x0090
                           00008C   440 _PORTR	=	0x008c
                           0000CE   441 _IC0CAPT0	=	0x00ce
                           0000CF   442 _IC0CAPT1	=	0x00cf
                           00CFCE   443 _IC0CAPT	=	0xcfce
                           0000CC   444 _IC0MODE	=	0x00cc
                           0000CD   445 _IC0STATUS	=	0x00cd
                           0000D6   446 _IC1CAPT0	=	0x00d6
                           0000D7   447 _IC1CAPT1	=	0x00d7
                           00D7D6   448 _IC1CAPT	=	0xd7d6
                           0000D4   449 _IC1MODE	=	0x00d4
                           0000D5   450 _IC1STATUS	=	0x00d5
                           000092   451 _NVADDR0	=	0x0092
                           000093   452 _NVADDR1	=	0x0093
                           009392   453 _NVADDR	=	0x9392
                           000094   454 _NVDATA0	=	0x0094
                           000095   455 _NVDATA1	=	0x0095
                           009594   456 _NVDATA	=	0x9594
                           000096   457 _NVKEY	=	0x0096
                           000091   458 _NVSTATUS	=	0x0091
                           0000BC   459 _OC0COMP0	=	0x00bc
                           0000BD   460 _OC0COMP1	=	0x00bd
                           00BDBC   461 _OC0COMP	=	0xbdbc
                           0000B9   462 _OC0MODE	=	0x00b9
                           0000BA   463 _OC0PIN	=	0x00ba
                           0000BB   464 _OC0STATUS	=	0x00bb
                           0000C4   465 _OC1COMP0	=	0x00c4
                           0000C5   466 _OC1COMP1	=	0x00c5
                           00C5C4   467 _OC1COMP	=	0xc5c4
                           0000C1   468 _OC1MODE	=	0x00c1
                           0000C2   469 _OC1PIN	=	0x00c2
                           0000C3   470 _OC1STATUS	=	0x00c3
                           0000B1   471 _RADIOACC	=	0x00b1
                           0000B3   472 _RADIOADDR0	=	0x00b3
                           0000B2   473 _RADIOADDR1	=	0x00b2
                           00B2B3   474 _RADIOADDR	=	0xb2b3
                           0000B7   475 _RADIODATA0	=	0x00b7
                           0000B6   476 _RADIODATA1	=	0x00b6
                           0000B5   477 _RADIODATA2	=	0x00b5
                           0000B4   478 _RADIODATA3	=	0x00b4
                           B4B5B6B7   479 _RADIODATA	=	0xb4b5b6b7
                           0000BE   480 _RADIOSTAT0	=	0x00be
                           0000BF   481 _RADIOSTAT1	=	0x00bf
                           00BFBE   482 _RADIOSTAT	=	0xbfbe
                           0000DF   483 _SPCLKSRC	=	0x00df
                           0000DC   484 _SPMODE	=	0x00dc
                           0000DE   485 _SPSHREG	=	0x00de
                           0000DD   486 _SPSTATUS	=	0x00dd
                           00009A   487 _T0CLKSRC	=	0x009a
                           00009C   488 _T0CNT0	=	0x009c
                           00009D   489 _T0CNT1	=	0x009d
                           009D9C   490 _T0CNT	=	0x9d9c
                           000099   491 _T0MODE	=	0x0099
                           00009E   492 _T0PERIOD0	=	0x009e
                           00009F   493 _T0PERIOD1	=	0x009f
                           009F9E   494 _T0PERIOD	=	0x9f9e
                           00009B   495 _T0STATUS	=	0x009b
                           0000A2   496 _T1CLKSRC	=	0x00a2
                           0000A4   497 _T1CNT0	=	0x00a4
                           0000A5   498 _T1CNT1	=	0x00a5
                           00A5A4   499 _T1CNT	=	0xa5a4
                           0000A1   500 _T1MODE	=	0x00a1
                           0000A6   501 _T1PERIOD0	=	0x00a6
                           0000A7   502 _T1PERIOD1	=	0x00a7
                           00A7A6   503 _T1PERIOD	=	0xa7a6
                           0000A3   504 _T1STATUS	=	0x00a3
                           0000AA   505 _T2CLKSRC	=	0x00aa
                           0000AC   506 _T2CNT0	=	0x00ac
                           0000AD   507 _T2CNT1	=	0x00ad
                           00ADAC   508 _T2CNT	=	0xadac
                           0000A9   509 _T2MODE	=	0x00a9
                           0000AE   510 _T2PERIOD0	=	0x00ae
                           0000AF   511 _T2PERIOD1	=	0x00af
                           00AFAE   512 _T2PERIOD	=	0xafae
                           0000AB   513 _T2STATUS	=	0x00ab
                           0000E4   514 _U0CTRL	=	0x00e4
                           0000E7   515 _U0MODE	=	0x00e7
                           0000E6   516 _U0SHREG	=	0x00e6
                           0000E5   517 _U0STATUS	=	0x00e5
                           0000EC   518 _U1CTRL	=	0x00ec
                           0000EF   519 _U1MODE	=	0x00ef
                           0000EE   520 _U1SHREG	=	0x00ee
                           0000ED   521 _U1STATUS	=	0x00ed
                           0000DA   522 _WDTCFG	=	0x00da
                           0000DB   523 _WDTRESET	=	0x00db
                           0000F1   524 _WTCFGA	=	0x00f1
                           0000F9   525 _WTCFGB	=	0x00f9
                           0000F2   526 _WTCNTA0	=	0x00f2
                           0000F3   527 _WTCNTA1	=	0x00f3
                           00F3F2   528 _WTCNTA	=	0xf3f2
                           0000FA   529 _WTCNTB0	=	0x00fa
                           0000FB   530 _WTCNTB1	=	0x00fb
                           00FBFA   531 _WTCNTB	=	0xfbfa
                           0000EB   532 _WTCNTR1	=	0x00eb
                           0000F4   533 _WTEVTA0	=	0x00f4
                           0000F5   534 _WTEVTA1	=	0x00f5
                           00F5F4   535 _WTEVTA	=	0xf5f4
                           0000F6   536 _WTEVTB0	=	0x00f6
                           0000F7   537 _WTEVTB1	=	0x00f7
                           00F7F6   538 _WTEVTB	=	0xf7f6
                           0000FC   539 _WTEVTC0	=	0x00fc
                           0000FD   540 _WTEVTC1	=	0x00fd
                           00FDFC   541 _WTEVTC	=	0xfdfc
                           0000FE   542 _WTEVTD0	=	0x00fe
                           0000FF   543 _WTEVTD1	=	0x00ff
                           00FFFE   544 _WTEVTD	=	0xfffe
                           0000E9   545 _WTIRQEN	=	0x00e9
                           0000EA   546 _WTSTAT	=	0x00ea
                                    547 ;--------------------------------------------------------
                                    548 ; special function bits
                                    549 ;--------------------------------------------------------
                                    550 	.area RSEG    (ABS,DATA)
      000000                        551 	.org 0x0000
                           0000E0   552 _ACC_0	=	0x00e0
                           0000E1   553 _ACC_1	=	0x00e1
                           0000E2   554 _ACC_2	=	0x00e2
                           0000E3   555 _ACC_3	=	0x00e3
                           0000E4   556 _ACC_4	=	0x00e4
                           0000E5   557 _ACC_5	=	0x00e5
                           0000E6   558 _ACC_6	=	0x00e6
                           0000E7   559 _ACC_7	=	0x00e7
                           0000F0   560 _B_0	=	0x00f0
                           0000F1   561 _B_1	=	0x00f1
                           0000F2   562 _B_2	=	0x00f2
                           0000F3   563 _B_3	=	0x00f3
                           0000F4   564 _B_4	=	0x00f4
                           0000F5   565 _B_5	=	0x00f5
                           0000F6   566 _B_6	=	0x00f6
                           0000F7   567 _B_7	=	0x00f7
                           0000A0   568 _E2IE_0	=	0x00a0
                           0000A1   569 _E2IE_1	=	0x00a1
                           0000A2   570 _E2IE_2	=	0x00a2
                           0000A3   571 _E2IE_3	=	0x00a3
                           0000A4   572 _E2IE_4	=	0x00a4
                           0000A5   573 _E2IE_5	=	0x00a5
                           0000A6   574 _E2IE_6	=	0x00a6
                           0000A7   575 _E2IE_7	=	0x00a7
                           0000C0   576 _E2IP_0	=	0x00c0
                           0000C1   577 _E2IP_1	=	0x00c1
                           0000C2   578 _E2IP_2	=	0x00c2
                           0000C3   579 _E2IP_3	=	0x00c3
                           0000C4   580 _E2IP_4	=	0x00c4
                           0000C5   581 _E2IP_5	=	0x00c5
                           0000C6   582 _E2IP_6	=	0x00c6
                           0000C7   583 _E2IP_7	=	0x00c7
                           000098   584 _EIE_0	=	0x0098
                           000099   585 _EIE_1	=	0x0099
                           00009A   586 _EIE_2	=	0x009a
                           00009B   587 _EIE_3	=	0x009b
                           00009C   588 _EIE_4	=	0x009c
                           00009D   589 _EIE_5	=	0x009d
                           00009E   590 _EIE_6	=	0x009e
                           00009F   591 _EIE_7	=	0x009f
                           0000B0   592 _EIP_0	=	0x00b0
                           0000B1   593 _EIP_1	=	0x00b1
                           0000B2   594 _EIP_2	=	0x00b2
                           0000B3   595 _EIP_3	=	0x00b3
                           0000B4   596 _EIP_4	=	0x00b4
                           0000B5   597 _EIP_5	=	0x00b5
                           0000B6   598 _EIP_6	=	0x00b6
                           0000B7   599 _EIP_7	=	0x00b7
                           0000A8   600 _IE_0	=	0x00a8
                           0000A9   601 _IE_1	=	0x00a9
                           0000AA   602 _IE_2	=	0x00aa
                           0000AB   603 _IE_3	=	0x00ab
                           0000AC   604 _IE_4	=	0x00ac
                           0000AD   605 _IE_5	=	0x00ad
                           0000AE   606 _IE_6	=	0x00ae
                           0000AF   607 _IE_7	=	0x00af
                           0000AF   608 _EA	=	0x00af
                           0000B8   609 _IP_0	=	0x00b8
                           0000B9   610 _IP_1	=	0x00b9
                           0000BA   611 _IP_2	=	0x00ba
                           0000BB   612 _IP_3	=	0x00bb
                           0000BC   613 _IP_4	=	0x00bc
                           0000BD   614 _IP_5	=	0x00bd
                           0000BE   615 _IP_6	=	0x00be
                           0000BF   616 _IP_7	=	0x00bf
                           0000D0   617 _P	=	0x00d0
                           0000D1   618 _F1	=	0x00d1
                           0000D2   619 _OV	=	0x00d2
                           0000D3   620 _RS0	=	0x00d3
                           0000D4   621 _RS1	=	0x00d4
                           0000D5   622 _F0	=	0x00d5
                           0000D6   623 _AC	=	0x00d6
                           0000D7   624 _CY	=	0x00d7
                           0000C8   625 _PINA_0	=	0x00c8
                           0000C9   626 _PINA_1	=	0x00c9
                           0000CA   627 _PINA_2	=	0x00ca
                           0000CB   628 _PINA_3	=	0x00cb
                           0000CC   629 _PINA_4	=	0x00cc
                           0000CD   630 _PINA_5	=	0x00cd
                           0000CE   631 _PINA_6	=	0x00ce
                           0000CF   632 _PINA_7	=	0x00cf
                           0000E8   633 _PINB_0	=	0x00e8
                           0000E9   634 _PINB_1	=	0x00e9
                           0000EA   635 _PINB_2	=	0x00ea
                           0000EB   636 _PINB_3	=	0x00eb
                           0000EC   637 _PINB_4	=	0x00ec
                           0000ED   638 _PINB_5	=	0x00ed
                           0000EE   639 _PINB_6	=	0x00ee
                           0000EF   640 _PINB_7	=	0x00ef
                           0000F8   641 _PINC_0	=	0x00f8
                           0000F9   642 _PINC_1	=	0x00f9
                           0000FA   643 _PINC_2	=	0x00fa
                           0000FB   644 _PINC_3	=	0x00fb
                           0000FC   645 _PINC_4	=	0x00fc
                           0000FD   646 _PINC_5	=	0x00fd
                           0000FE   647 _PINC_6	=	0x00fe
                           0000FF   648 _PINC_7	=	0x00ff
                           000080   649 _PORTA_0	=	0x0080
                           000081   650 _PORTA_1	=	0x0081
                           000082   651 _PORTA_2	=	0x0082
                           000083   652 _PORTA_3	=	0x0083
                           000084   653 _PORTA_4	=	0x0084
                           000085   654 _PORTA_5	=	0x0085
                           000086   655 _PORTA_6	=	0x0086
                           000087   656 _PORTA_7	=	0x0087
                           000088   657 _PORTB_0	=	0x0088
                           000089   658 _PORTB_1	=	0x0089
                           00008A   659 _PORTB_2	=	0x008a
                           00008B   660 _PORTB_3	=	0x008b
                           00008C   661 _PORTB_4	=	0x008c
                           00008D   662 _PORTB_5	=	0x008d
                           00008E   663 _PORTB_6	=	0x008e
                           00008F   664 _PORTB_7	=	0x008f
                           000090   665 _PORTC_0	=	0x0090
                           000091   666 _PORTC_1	=	0x0091
                           000092   667 _PORTC_2	=	0x0092
                           000093   668 _PORTC_3	=	0x0093
                           000094   669 _PORTC_4	=	0x0094
                           000095   670 _PORTC_5	=	0x0095
                           000096   671 _PORTC_6	=	0x0096
                           000097   672 _PORTC_7	=	0x0097
                                    673 ;--------------------------------------------------------
                                    674 ; overlayable register banks
                                    675 ;--------------------------------------------------------
                                    676 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        677 	.ds 8
                                    678 ;--------------------------------------------------------
                                    679 ; internal ram data
                                    680 ;--------------------------------------------------------
                                    681 	.area DSEG    (DATA)
                                    682 ;--------------------------------------------------------
                                    683 ; overlayable items in internal ram 
                                    684 ;--------------------------------------------------------
                                    685 ;--------------------------------------------------------
                                    686 ; indirectly addressable internal ram data
                                    687 ;--------------------------------------------------------
                                    688 	.area ISEG    (DATA)
                                    689 ;--------------------------------------------------------
                                    690 ; absolute internal ram data
                                    691 ;--------------------------------------------------------
                                    692 	.area IABS    (ABS,DATA)
                                    693 	.area IABS    (ABS,DATA)
                                    694 ;--------------------------------------------------------
                                    695 ; bit data
                                    696 ;--------------------------------------------------------
                                    697 	.area BSEG    (BIT)
                                    698 ;--------------------------------------------------------
                                    699 ; paged external ram data
                                    700 ;--------------------------------------------------------
                                    701 	.area PSEG    (PAG,XDATA)
                                    702 ;--------------------------------------------------------
                                    703 ; external ram data
                                    704 ;--------------------------------------------------------
                                    705 	.area XSEG    (XDATA)
                           00FC06   706 _flash_deviceid	=	0xfc06
                           007020   707 _ADCCH0VAL0	=	0x7020
                           007021   708 _ADCCH0VAL1	=	0x7021
                           007020   709 _ADCCH0VAL	=	0x7020
                           007022   710 _ADCCH1VAL0	=	0x7022
                           007023   711 _ADCCH1VAL1	=	0x7023
                           007022   712 _ADCCH1VAL	=	0x7022
                           007024   713 _ADCCH2VAL0	=	0x7024
                           007025   714 _ADCCH2VAL1	=	0x7025
                           007024   715 _ADCCH2VAL	=	0x7024
                           007026   716 _ADCCH3VAL0	=	0x7026
                           007027   717 _ADCCH3VAL1	=	0x7027
                           007026   718 _ADCCH3VAL	=	0x7026
                           007028   719 _ADCTUNE0	=	0x7028
                           007029   720 _ADCTUNE1	=	0x7029
                           00702A   721 _ADCTUNE2	=	0x702a
                           007010   722 _DMA0ADDR0	=	0x7010
                           007011   723 _DMA0ADDR1	=	0x7011
                           007010   724 _DMA0ADDR	=	0x7010
                           007014   725 _DMA0CONFIG	=	0x7014
                           007012   726 _DMA1ADDR0	=	0x7012
                           007013   727 _DMA1ADDR1	=	0x7013
                           007012   728 _DMA1ADDR	=	0x7012
                           007015   729 _DMA1CONFIG	=	0x7015
                           007070   730 _FRCOSCCONFIG	=	0x7070
                           007071   731 _FRCOSCCTRL	=	0x7071
                           007076   732 _FRCOSCFREQ0	=	0x7076
                           007077   733 _FRCOSCFREQ1	=	0x7077
                           007076   734 _FRCOSCFREQ	=	0x7076
                           007072   735 _FRCOSCKFILT0	=	0x7072
                           007073   736 _FRCOSCKFILT1	=	0x7073
                           007072   737 _FRCOSCKFILT	=	0x7072
                           007078   738 _FRCOSCPER0	=	0x7078
                           007079   739 _FRCOSCPER1	=	0x7079
                           007078   740 _FRCOSCPER	=	0x7078
                           007074   741 _FRCOSCREF0	=	0x7074
                           007075   742 _FRCOSCREF1	=	0x7075
                           007074   743 _FRCOSCREF	=	0x7074
                           007007   744 _ANALOGA	=	0x7007
                           00700C   745 _GPIOENABLE	=	0x700c
                           007003   746 _EXTIRQ	=	0x7003
                           007000   747 _INTCHGA	=	0x7000
                           007001   748 _INTCHGB	=	0x7001
                           007002   749 _INTCHGC	=	0x7002
                           007008   750 _PALTA	=	0x7008
                           007009   751 _PALTB	=	0x7009
                           00700A   752 _PALTC	=	0x700a
                           007046   753 _PALTRADIO	=	0x7046
                           007004   754 _PINCHGA	=	0x7004
                           007005   755 _PINCHGB	=	0x7005
                           007006   756 _PINCHGC	=	0x7006
                           00700B   757 _PINSEL	=	0x700b
                           007060   758 _LPOSCCONFIG	=	0x7060
                           007066   759 _LPOSCFREQ0	=	0x7066
                           007067   760 _LPOSCFREQ1	=	0x7067
                           007066   761 _LPOSCFREQ	=	0x7066
                           007062   762 _LPOSCKFILT0	=	0x7062
                           007063   763 _LPOSCKFILT1	=	0x7063
                           007062   764 _LPOSCKFILT	=	0x7062
                           007068   765 _LPOSCPER0	=	0x7068
                           007069   766 _LPOSCPER1	=	0x7069
                           007068   767 _LPOSCPER	=	0x7068
                           007064   768 _LPOSCREF0	=	0x7064
                           007065   769 _LPOSCREF1	=	0x7065
                           007064   770 _LPOSCREF	=	0x7064
                           007054   771 _LPXOSCGM	=	0x7054
                           007F01   772 _MISCCTRL	=	0x7f01
                           007053   773 _OSCCALIB	=	0x7053
                           007050   774 _OSCFORCERUN	=	0x7050
                           007052   775 _OSCREADY	=	0x7052
                           007051   776 _OSCRUN	=	0x7051
                           007040   777 _RADIOFDATAADDR0	=	0x7040
                           007041   778 _RADIOFDATAADDR1	=	0x7041
                           007040   779 _RADIOFDATAADDR	=	0x7040
                           007042   780 _RADIOFSTATADDR0	=	0x7042
                           007043   781 _RADIOFSTATADDR1	=	0x7043
                           007042   782 _RADIOFSTATADDR	=	0x7042
                           007044   783 _RADIOMUX	=	0x7044
                           007084   784 _SCRATCH0	=	0x7084
                           007085   785 _SCRATCH1	=	0x7085
                           007086   786 _SCRATCH2	=	0x7086
                           007087   787 _SCRATCH3	=	0x7087
                           007F00   788 _SILICONREV	=	0x7f00
                           007F19   789 _XTALAMPL	=	0x7f19
                           007F18   790 _XTALOSC	=	0x7f18
                           007F1A   791 _XTALREADY	=	0x7f1a
                           007081   792 _RNGBYTE	=	0x7081
                           007091   793 _AESCONFIG	=	0x7091
                           007098   794 _AESCURBLOCK	=	0x7098
                           007094   795 _AESINADDR0	=	0x7094
                           007095   796 _AESINADDR1	=	0x7095
                           007094   797 _AESINADDR	=	0x7094
                           007092   798 _AESKEYADDR0	=	0x7092
                           007093   799 _AESKEYADDR1	=	0x7093
                           007092   800 _AESKEYADDR	=	0x7092
                           007090   801 _AESMODE	=	0x7090
                           007096   802 _AESOUTADDR0	=	0x7096
                           007097   803 _AESOUTADDR1	=	0x7097
                           007096   804 _AESOUTADDR	=	0x7096
                           007080   805 _RNGMODE	=	0x7080
                           007082   806 _RNGCLKSRC0	=	0x7082
                           007083   807 _RNGCLKSRC1	=	0x7083
      0003C5                        808 _ReadFromMemory_address_65536_147:
      0003C5                        809 	.ds 2
      0003C7                        810 _WriteMemory_PARM_2:
      0003C7                        811 	.ds 2
      0003C9                        812 _WriteMemory_PARM_3:
      0003C9                        813 	.ds 1
      0003CA                        814 _WriteMemory_address_65536_149:
      0003CA                        815 	.ds 2
      0003CC                        816 _EraseMemoryPage_address_65536_151:
      0003CC                        817 	.ds 2
                                    818 ;--------------------------------------------------------
                                    819 ; absolute external ram data
                                    820 ;--------------------------------------------------------
                                    821 	.area XABS    (ABS,XDATA)
                                    822 ;--------------------------------------------------------
                                    823 ; external initialized ram data
                                    824 ;--------------------------------------------------------
                                    825 	.area XISEG   (XDATA)
                                    826 	.area HOME    (CODE)
                                    827 	.area GSINIT0 (CODE)
                                    828 	.area GSINIT1 (CODE)
                                    829 	.area GSINIT2 (CODE)
                                    830 	.area GSINIT3 (CODE)
                                    831 	.area GSINIT4 (CODE)
                                    832 	.area GSINIT5 (CODE)
                                    833 	.area GSINIT  (CODE)
                                    834 	.area GSFINAL (CODE)
                                    835 	.area CSEG    (CODE)
                                    836 ;--------------------------------------------------------
                                    837 ; global & static initialisations
                                    838 ;--------------------------------------------------------
                                    839 	.area HOME    (CODE)
                                    840 	.area GSINIT  (CODE)
                                    841 	.area GSFINAL (CODE)
                                    842 	.area GSINIT  (CODE)
                                    843 ;--------------------------------------------------------
                                    844 ; Home
                                    845 ;--------------------------------------------------------
                                    846 	.area HOME    (CODE)
                                    847 	.area HOME    (CODE)
                                    848 ;--------------------------------------------------------
                                    849 ; code
                                    850 ;--------------------------------------------------------
                                    851 	.area CSEG    (CODE)
                                    852 ;------------------------------------------------------------
                                    853 ;Allocation info for local variables in function 'ReadFromMemory'
                                    854 ;------------------------------------------------------------
                                    855 ;address                   Allocated with name '_ReadFromMemory_address_65536_147'
                                    856 ;RetVal                    Allocated with name '_ReadFromMemory_RetVal_65536_148'
                                    857 ;------------------------------------------------------------
                                    858 ;	..\src\peripherals\Memory.c:23: uint16_t ReadFromMemory( uint16_t address)
                                    859 ;	-----------------------------------------
                                    860 ;	 function ReadFromMemory
                                    861 ;	-----------------------------------------
      005E29                        862 _ReadFromMemory:
                           000007   863 	ar7 = 0x07
                           000006   864 	ar6 = 0x06
                           000005   865 	ar5 = 0x05
                           000004   866 	ar4 = 0x04
                           000003   867 	ar3 = 0x03
                           000002   868 	ar2 = 0x02
                           000001   869 	ar1 = 0x01
                           000000   870 	ar0 = 0x00
      005E29 AF 83            [24]  871 	mov	r7,dph
      005E2B E5 82            [12]  872 	mov	a,dpl
      005E2D 90 03 C5         [24]  873 	mov	dptr,#_ReadFromMemory_address_65536_147
      005E30 F0               [24]  874 	movx	@dptr,a
      005E31 EF               [12]  875 	mov	a,r7
      005E32 A3               [24]  876 	inc	dptr
      005E33 F0               [24]  877 	movx	@dptr,a
                                    878 ;	..\src\peripherals\Memory.c:26: flash_unlock();
      005E34 12 7F E9         [24]  879 	lcall	_flash_unlock
                                    880 ;	..\src\peripherals\Memory.c:27: RetVal = flash_read(address);
      005E37 90 03 C5         [24]  881 	mov	dptr,#_ReadFromMemory_address_65536_147
      005E3A E0               [24]  882 	movx	a,@dptr
      005E3B FE               [12]  883 	mov	r6,a
      005E3C A3               [24]  884 	inc	dptr
      005E3D E0               [24]  885 	movx	a,@dptr
      005E3E FF               [12]  886 	mov	r7,a
      005E3F 8E 82            [24]  887 	mov	dpl,r6
      005E41 8F 83            [24]  888 	mov	dph,r7
      005E43 12 86 FB         [24]  889 	lcall	_flash_read
      005E46 AE 82            [24]  890 	mov	r6,dpl
      005E48 AF 83            [24]  891 	mov	r7,dph
                                    892 ;	..\src\peripherals\Memory.c:28: flash_lock();
      005E4A C0 07            [24]  893 	push	ar7
      005E4C C0 06            [24]  894 	push	ar6
      005E4E 12 8A 91         [24]  895 	lcall	_flash_lock
      005E51 D0 06            [24]  896 	pop	ar6
      005E53 D0 07            [24]  897 	pop	ar7
                                    898 ;	..\src\peripherals\Memory.c:29: return RetVal;
      005E55 8E 82            [24]  899 	mov	dpl,r6
      005E57 8F 83            [24]  900 	mov	dph,r7
                                    901 ;	..\src\peripherals\Memory.c:30: }
      005E59 22               [24]  902 	ret
                                    903 ;------------------------------------------------------------
                                    904 ;Allocation info for local variables in function 'WriteMemory'
                                    905 ;------------------------------------------------------------
                                    906 ;data                      Allocated with name '_WriteMemory_PARM_2'
                                    907 ;erase_flag                Allocated with name '_WriteMemory_PARM_3'
                                    908 ;address                   Allocated with name '_WriteMemory_address_65536_149'
                                    909 ;------------------------------------------------------------
                                    910 ;	..\src\peripherals\Memory.c:44: void WriteMemory(uint16_t address, uint16_t data, char erase_flag)
                                    911 ;	-----------------------------------------
                                    912 ;	 function WriteMemory
                                    913 ;	-----------------------------------------
      005E5A                        914 _WriteMemory:
      005E5A AF 83            [24]  915 	mov	r7,dph
      005E5C E5 82            [12]  916 	mov	a,dpl
      005E5E 90 03 CA         [24]  917 	mov	dptr,#_WriteMemory_address_65536_149
      005E61 F0               [24]  918 	movx	@dptr,a
      005E62 EF               [12]  919 	mov	a,r7
      005E63 A3               [24]  920 	inc	dptr
      005E64 F0               [24]  921 	movx	@dptr,a
                                    922 ;	..\src\peripherals\Memory.c:51: flash_write(address,data);
      005E65 90 03 CA         [24]  923 	mov	dptr,#_WriteMemory_address_65536_149
      005E68 E0               [24]  924 	movx	a,@dptr
      005E69 FE               [12]  925 	mov	r6,a
      005E6A A3               [24]  926 	inc	dptr
      005E6B E0               [24]  927 	movx	a,@dptr
      005E6C FF               [12]  928 	mov	r7,a
      005E6D 90 03 C7         [24]  929 	mov	dptr,#_WriteMemory_PARM_2
      005E70 E0               [24]  930 	movx	a,@dptr
      005E71 FC               [12]  931 	mov	r4,a
      005E72 A3               [24]  932 	inc	dptr
      005E73 E0               [24]  933 	movx	a,@dptr
      005E74 FD               [12]  934 	mov	r5,a
      005E75 90 04 F6         [24]  935 	mov	dptr,#_flash_write_PARM_2
      005E78 EC               [12]  936 	mov	a,r4
      005E79 F0               [24]  937 	movx	@dptr,a
      005E7A ED               [12]  938 	mov	a,r5
      005E7B A3               [24]  939 	inc	dptr
      005E7C F0               [24]  940 	movx	@dptr,a
      005E7D 8E 82            [24]  941 	mov	dpl,r6
      005E7F 8F 83            [24]  942 	mov	dph,r7
                                    943 ;	..\src\peripherals\Memory.c:55: }
      005E81 02 7D F7         [24]  944 	ljmp	_flash_write
                                    945 ;------------------------------------------------------------
                                    946 ;Allocation info for local variables in function 'EraseMemoryPage'
                                    947 ;------------------------------------------------------------
                                    948 ;address                   Allocated with name '_EraseMemoryPage_address_65536_151'
                                    949 ;------------------------------------------------------------
                                    950 ;	..\src\peripherals\Memory.c:66: void EraseMemoryPage(uint16_t address)
                                    951 ;	-----------------------------------------
                                    952 ;	 function EraseMemoryPage
                                    953 ;	-----------------------------------------
      005E84                        954 _EraseMemoryPage:
      005E84 AF 83            [24]  955 	mov	r7,dph
      005E86 E5 82            [12]  956 	mov	a,dpl
      005E88 90 03 CC         [24]  957 	mov	dptr,#_EraseMemoryPage_address_65536_151
      005E8B F0               [24]  958 	movx	@dptr,a
      005E8C EF               [12]  959 	mov	a,r7
      005E8D A3               [24]  960 	inc	dptr
      005E8E F0               [24]  961 	movx	@dptr,a
                                    962 ;	..\src\peripherals\Memory.c:68: EA = 0;
                                    963 ;	assignBit
      005E8F C2 AF            [12]  964 	clr	_EA
                                    965 ;	..\src\peripherals\Memory.c:69: flash_unlock();
      005E91 12 7F E9         [24]  966 	lcall	_flash_unlock
                                    967 ;	..\src\peripherals\Memory.c:70: NVADDR0 = (char) address & 0x00FF;
      005E94 90 03 CC         [24]  968 	mov	dptr,#_EraseMemoryPage_address_65536_151
      005E97 E0               [24]  969 	movx	a,@dptr
      005E98 FE               [12]  970 	mov	r6,a
      005E99 A3               [24]  971 	inc	dptr
      005E9A E0               [24]  972 	movx	a,@dptr
      005E9B FF               [12]  973 	mov	r7,a
      005E9C 8E 92            [24]  974 	mov	_NVADDR0,r6
                                    975 ;	..\src\peripherals\Memory.c:71: NVADDR1 = (char) (address >> 8 & 0x00FF);
      005E9E 8F 93            [24]  976 	mov	_NVADDR1,r7
                                    977 ;	..\src\peripherals\Memory.c:72: NVSTATUS = 0x20;
      005EA0 75 91 20         [24]  978 	mov	_NVSTATUS,#0x20
                                    979 ;	..\src\peripherals\Memory.c:75: flash_lock();
      005EA3 12 8A 91         [24]  980 	lcall	_flash_lock
                                    981 ;	..\src\peripherals\Memory.c:76: EA = 1;
                                    982 ;	assignBit
      005EA6 D2 AF            [12]  983 	setb	_EA
                                    984 ;	..\src\peripherals\Memory.c:77: }
      005EA8 22               [24]  985 	ret
                                    986 	.area CSEG    (CODE)
                                    987 	.area CONST   (CODE)
                                    988 	.area XINIT   (CODE)
                                    989 	.area CABS    (ABS,CODE)
