;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.9.3 #11354 (MINGW32)
;--------------------------------------------------------
	.module Beacon
	.optsdcc -mmcs51 --model-large
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _dbglink_writehex16
	.globl _dbglink_writestr
	.globl _memcpy
	.globl _PORTC_7
	.globl _PORTC_6
	.globl _PORTC_5
	.globl _PORTC_4
	.globl _PORTC_3
	.globl _PORTC_2
	.globl _PORTC_1
	.globl _PORTC_0
	.globl _PORTB_7
	.globl _PORTB_6
	.globl _PORTB_5
	.globl _PORTB_4
	.globl _PORTB_3
	.globl _PORTB_2
	.globl _PORTB_1
	.globl _PORTB_0
	.globl _PORTA_7
	.globl _PORTA_6
	.globl _PORTA_5
	.globl _PORTA_4
	.globl _PORTA_3
	.globl _PORTA_2
	.globl _PORTA_1
	.globl _PORTA_0
	.globl _PINC_7
	.globl _PINC_6
	.globl _PINC_5
	.globl _PINC_4
	.globl _PINC_3
	.globl _PINC_2
	.globl _PINC_1
	.globl _PINC_0
	.globl _PINB_7
	.globl _PINB_6
	.globl _PINB_5
	.globl _PINB_4
	.globl _PINB_3
	.globl _PINB_2
	.globl _PINB_1
	.globl _PINB_0
	.globl _PINA_7
	.globl _PINA_6
	.globl _PINA_5
	.globl _PINA_4
	.globl _PINA_3
	.globl _PINA_2
	.globl _PINA_1
	.globl _PINA_0
	.globl _CY
	.globl _AC
	.globl _F0
	.globl _RS1
	.globl _RS0
	.globl _OV
	.globl _F1
	.globl _P
	.globl _IP_7
	.globl _IP_6
	.globl _IP_5
	.globl _IP_4
	.globl _IP_3
	.globl _IP_2
	.globl _IP_1
	.globl _IP_0
	.globl _EA
	.globl _IE_7
	.globl _IE_6
	.globl _IE_5
	.globl _IE_4
	.globl _IE_3
	.globl _IE_2
	.globl _IE_1
	.globl _IE_0
	.globl _EIP_7
	.globl _EIP_6
	.globl _EIP_5
	.globl _EIP_4
	.globl _EIP_3
	.globl _EIP_2
	.globl _EIP_1
	.globl _EIP_0
	.globl _EIE_7
	.globl _EIE_6
	.globl _EIE_5
	.globl _EIE_4
	.globl _EIE_3
	.globl _EIE_2
	.globl _EIE_1
	.globl _EIE_0
	.globl _E2IP_7
	.globl _E2IP_6
	.globl _E2IP_5
	.globl _E2IP_4
	.globl _E2IP_3
	.globl _E2IP_2
	.globl _E2IP_1
	.globl _E2IP_0
	.globl _E2IE_7
	.globl _E2IE_6
	.globl _E2IE_5
	.globl _E2IE_4
	.globl _E2IE_3
	.globl _E2IE_2
	.globl _E2IE_1
	.globl _E2IE_0
	.globl _B_7
	.globl _B_6
	.globl _B_5
	.globl _B_4
	.globl _B_3
	.globl _B_2
	.globl _B_1
	.globl _B_0
	.globl _ACC_7
	.globl _ACC_6
	.globl _ACC_5
	.globl _ACC_4
	.globl _ACC_3
	.globl _ACC_2
	.globl _ACC_1
	.globl _ACC_0
	.globl _WTSTAT
	.globl _WTIRQEN
	.globl _WTEVTD
	.globl _WTEVTD1
	.globl _WTEVTD0
	.globl _WTEVTC
	.globl _WTEVTC1
	.globl _WTEVTC0
	.globl _WTEVTB
	.globl _WTEVTB1
	.globl _WTEVTB0
	.globl _WTEVTA
	.globl _WTEVTA1
	.globl _WTEVTA0
	.globl _WTCNTR1
	.globl _WTCNTB
	.globl _WTCNTB1
	.globl _WTCNTB0
	.globl _WTCNTA
	.globl _WTCNTA1
	.globl _WTCNTA0
	.globl _WTCFGB
	.globl _WTCFGA
	.globl _WDTRESET
	.globl _WDTCFG
	.globl _U1STATUS
	.globl _U1SHREG
	.globl _U1MODE
	.globl _U1CTRL
	.globl _U0STATUS
	.globl _U0SHREG
	.globl _U0MODE
	.globl _U0CTRL
	.globl _T2STATUS
	.globl _T2PERIOD
	.globl _T2PERIOD1
	.globl _T2PERIOD0
	.globl _T2MODE
	.globl _T2CNT
	.globl _T2CNT1
	.globl _T2CNT0
	.globl _T2CLKSRC
	.globl _T1STATUS
	.globl _T1PERIOD
	.globl _T1PERIOD1
	.globl _T1PERIOD0
	.globl _T1MODE
	.globl _T1CNT
	.globl _T1CNT1
	.globl _T1CNT0
	.globl _T1CLKSRC
	.globl _T0STATUS
	.globl _T0PERIOD
	.globl _T0PERIOD1
	.globl _T0PERIOD0
	.globl _T0MODE
	.globl _T0CNT
	.globl _T0CNT1
	.globl _T0CNT0
	.globl _T0CLKSRC
	.globl _SPSTATUS
	.globl _SPSHREG
	.globl _SPMODE
	.globl _SPCLKSRC
	.globl _RADIOSTAT
	.globl _RADIOSTAT1
	.globl _RADIOSTAT0
	.globl _RADIODATA
	.globl _RADIODATA3
	.globl _RADIODATA2
	.globl _RADIODATA1
	.globl _RADIODATA0
	.globl _RADIOADDR
	.globl _RADIOADDR1
	.globl _RADIOADDR0
	.globl _RADIOACC
	.globl _OC1STATUS
	.globl _OC1PIN
	.globl _OC1MODE
	.globl _OC1COMP
	.globl _OC1COMP1
	.globl _OC1COMP0
	.globl _OC0STATUS
	.globl _OC0PIN
	.globl _OC0MODE
	.globl _OC0COMP
	.globl _OC0COMP1
	.globl _OC0COMP0
	.globl _NVSTATUS
	.globl _NVKEY
	.globl _NVDATA
	.globl _NVDATA1
	.globl _NVDATA0
	.globl _NVADDR
	.globl _NVADDR1
	.globl _NVADDR0
	.globl _IC1STATUS
	.globl _IC1MODE
	.globl _IC1CAPT
	.globl _IC1CAPT1
	.globl _IC1CAPT0
	.globl _IC0STATUS
	.globl _IC0MODE
	.globl _IC0CAPT
	.globl _IC0CAPT1
	.globl _IC0CAPT0
	.globl _PORTR
	.globl _PORTC
	.globl _PORTB
	.globl _PORTA
	.globl _PINR
	.globl _PINC
	.globl _PINB
	.globl _PINA
	.globl _DIRR
	.globl _DIRC
	.globl _DIRB
	.globl _DIRA
	.globl _DBGLNKSTAT
	.globl _DBGLNKBUF
	.globl _CODECONFIG
	.globl _CLKSTAT
	.globl _CLKCON
	.globl _ANALOGCOMP
	.globl _ADCCONV
	.globl _ADCCLKSRC
	.globl _ADCCH3CONFIG
	.globl _ADCCH2CONFIG
	.globl _ADCCH1CONFIG
	.globl _ADCCH0CONFIG
	.globl __XPAGE
	.globl _XPAGE
	.globl _SP
	.globl _PSW
	.globl _PCON
	.globl _IP
	.globl _IE
	.globl _EIP
	.globl _EIE
	.globl _E2IP
	.globl _E2IE
	.globl _DPS
	.globl _DPTR1
	.globl _DPTR0
	.globl _DPL1
	.globl _DPL
	.globl _DPH1
	.globl _DPH
	.globl _B
	.globl _ACC
	.globl _VerifyACK_PARM_3
	.globl _VerifyACK_PARM_2
	.globl _BEACON_decoding_PARM_3
	.globl _BEACON_decoding_PARM_2
	.globl _RNGCLKSRC1
	.globl _RNGCLKSRC0
	.globl _RNGMODE
	.globl _AESOUTADDR
	.globl _AESOUTADDR1
	.globl _AESOUTADDR0
	.globl _AESMODE
	.globl _AESKEYADDR
	.globl _AESKEYADDR1
	.globl _AESKEYADDR0
	.globl _AESINADDR
	.globl _AESINADDR1
	.globl _AESINADDR0
	.globl _AESCURBLOCK
	.globl _AESCONFIG
	.globl _RNGBYTE
	.globl _XTALREADY
	.globl _XTALOSC
	.globl _XTALAMPL
	.globl _SILICONREV
	.globl _SCRATCH3
	.globl _SCRATCH2
	.globl _SCRATCH1
	.globl _SCRATCH0
	.globl _RADIOMUX
	.globl _RADIOFSTATADDR
	.globl _RADIOFSTATADDR1
	.globl _RADIOFSTATADDR0
	.globl _RADIOFDATAADDR
	.globl _RADIOFDATAADDR1
	.globl _RADIOFDATAADDR0
	.globl _OSCRUN
	.globl _OSCREADY
	.globl _OSCFORCERUN
	.globl _OSCCALIB
	.globl _MISCCTRL
	.globl _LPXOSCGM
	.globl _LPOSCREF
	.globl _LPOSCREF1
	.globl _LPOSCREF0
	.globl _LPOSCPER
	.globl _LPOSCPER1
	.globl _LPOSCPER0
	.globl _LPOSCKFILT
	.globl _LPOSCKFILT1
	.globl _LPOSCKFILT0
	.globl _LPOSCFREQ
	.globl _LPOSCFREQ1
	.globl _LPOSCFREQ0
	.globl _LPOSCCONFIG
	.globl _PINSEL
	.globl _PINCHGC
	.globl _PINCHGB
	.globl _PINCHGA
	.globl _PALTRADIO
	.globl _PALTC
	.globl _PALTB
	.globl _PALTA
	.globl _INTCHGC
	.globl _INTCHGB
	.globl _INTCHGA
	.globl _EXTIRQ
	.globl _GPIOENABLE
	.globl _ANALOGA
	.globl _FRCOSCREF
	.globl _FRCOSCREF1
	.globl _FRCOSCREF0
	.globl _FRCOSCPER
	.globl _FRCOSCPER1
	.globl _FRCOSCPER0
	.globl _FRCOSCKFILT
	.globl _FRCOSCKFILT1
	.globl _FRCOSCKFILT0
	.globl _FRCOSCFREQ
	.globl _FRCOSCFREQ1
	.globl _FRCOSCFREQ0
	.globl _FRCOSCCTRL
	.globl _FRCOSCCONFIG
	.globl _DMA1CONFIG
	.globl _DMA1ADDR
	.globl _DMA1ADDR1
	.globl _DMA1ADDR0
	.globl _DMA0CONFIG
	.globl _DMA0ADDR
	.globl _DMA0ADDR1
	.globl _DMA0ADDR0
	.globl _ADCTUNE2
	.globl _ADCTUNE1
	.globl _ADCTUNE0
	.globl _ADCCH3VAL
	.globl _ADCCH3VAL1
	.globl _ADCCH3VAL0
	.globl _ADCCH2VAL
	.globl _ADCCH2VAL1
	.globl _ADCCH2VAL0
	.globl _ADCCH1VAL
	.globl _ADCCH1VAL1
	.globl _ADCCH1VAL0
	.globl _ADCCH0VAL
	.globl _ADCCH0VAL1
	.globl _ADCCH0VAL0
	.globl _BEACON_decoding
	.globl _VerifyACK
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
_ACC	=	0x00e0
_B	=	0x00f0
_DPH	=	0x0083
_DPH1	=	0x0085
_DPL	=	0x0082
_DPL1	=	0x0084
_DPTR0	=	0x8382
_DPTR1	=	0x8584
_DPS	=	0x0086
_E2IE	=	0x00a0
_E2IP	=	0x00c0
_EIE	=	0x0098
_EIP	=	0x00b0
_IE	=	0x00a8
_IP	=	0x00b8
_PCON	=	0x0087
_PSW	=	0x00d0
_SP	=	0x0081
_XPAGE	=	0x00d9
__XPAGE	=	0x00d9
_ADCCH0CONFIG	=	0x00ca
_ADCCH1CONFIG	=	0x00cb
_ADCCH2CONFIG	=	0x00d2
_ADCCH3CONFIG	=	0x00d3
_ADCCLKSRC	=	0x00d1
_ADCCONV	=	0x00c9
_ANALOGCOMP	=	0x00e1
_CLKCON	=	0x00c6
_CLKSTAT	=	0x00c7
_CODECONFIG	=	0x0097
_DBGLNKBUF	=	0x00e3
_DBGLNKSTAT	=	0x00e2
_DIRA	=	0x0089
_DIRB	=	0x008a
_DIRC	=	0x008b
_DIRR	=	0x008e
_PINA	=	0x00c8
_PINB	=	0x00e8
_PINC	=	0x00f8
_PINR	=	0x008d
_PORTA	=	0x0080
_PORTB	=	0x0088
_PORTC	=	0x0090
_PORTR	=	0x008c
_IC0CAPT0	=	0x00ce
_IC0CAPT1	=	0x00cf
_IC0CAPT	=	0xcfce
_IC0MODE	=	0x00cc
_IC0STATUS	=	0x00cd
_IC1CAPT0	=	0x00d6
_IC1CAPT1	=	0x00d7
_IC1CAPT	=	0xd7d6
_IC1MODE	=	0x00d4
_IC1STATUS	=	0x00d5
_NVADDR0	=	0x0092
_NVADDR1	=	0x0093
_NVADDR	=	0x9392
_NVDATA0	=	0x0094
_NVDATA1	=	0x0095
_NVDATA	=	0x9594
_NVKEY	=	0x0096
_NVSTATUS	=	0x0091
_OC0COMP0	=	0x00bc
_OC0COMP1	=	0x00bd
_OC0COMP	=	0xbdbc
_OC0MODE	=	0x00b9
_OC0PIN	=	0x00ba
_OC0STATUS	=	0x00bb
_OC1COMP0	=	0x00c4
_OC1COMP1	=	0x00c5
_OC1COMP	=	0xc5c4
_OC1MODE	=	0x00c1
_OC1PIN	=	0x00c2
_OC1STATUS	=	0x00c3
_RADIOACC	=	0x00b1
_RADIOADDR0	=	0x00b3
_RADIOADDR1	=	0x00b2
_RADIOADDR	=	0xb2b3
_RADIODATA0	=	0x00b7
_RADIODATA1	=	0x00b6
_RADIODATA2	=	0x00b5
_RADIODATA3	=	0x00b4
_RADIODATA	=	0xb4b5b6b7
_RADIOSTAT0	=	0x00be
_RADIOSTAT1	=	0x00bf
_RADIOSTAT	=	0xbfbe
_SPCLKSRC	=	0x00df
_SPMODE	=	0x00dc
_SPSHREG	=	0x00de
_SPSTATUS	=	0x00dd
_T0CLKSRC	=	0x009a
_T0CNT0	=	0x009c
_T0CNT1	=	0x009d
_T0CNT	=	0x9d9c
_T0MODE	=	0x0099
_T0PERIOD0	=	0x009e
_T0PERIOD1	=	0x009f
_T0PERIOD	=	0x9f9e
_T0STATUS	=	0x009b
_T1CLKSRC	=	0x00a2
_T1CNT0	=	0x00a4
_T1CNT1	=	0x00a5
_T1CNT	=	0xa5a4
_T1MODE	=	0x00a1
_T1PERIOD0	=	0x00a6
_T1PERIOD1	=	0x00a7
_T1PERIOD	=	0xa7a6
_T1STATUS	=	0x00a3
_T2CLKSRC	=	0x00aa
_T2CNT0	=	0x00ac
_T2CNT1	=	0x00ad
_T2CNT	=	0xadac
_T2MODE	=	0x00a9
_T2PERIOD0	=	0x00ae
_T2PERIOD1	=	0x00af
_T2PERIOD	=	0xafae
_T2STATUS	=	0x00ab
_U0CTRL	=	0x00e4
_U0MODE	=	0x00e7
_U0SHREG	=	0x00e6
_U0STATUS	=	0x00e5
_U1CTRL	=	0x00ec
_U1MODE	=	0x00ef
_U1SHREG	=	0x00ee
_U1STATUS	=	0x00ed
_WDTCFG	=	0x00da
_WDTRESET	=	0x00db
_WTCFGA	=	0x00f1
_WTCFGB	=	0x00f9
_WTCNTA0	=	0x00f2
_WTCNTA1	=	0x00f3
_WTCNTA	=	0xf3f2
_WTCNTB0	=	0x00fa
_WTCNTB1	=	0x00fb
_WTCNTB	=	0xfbfa
_WTCNTR1	=	0x00eb
_WTEVTA0	=	0x00f4
_WTEVTA1	=	0x00f5
_WTEVTA	=	0xf5f4
_WTEVTB0	=	0x00f6
_WTEVTB1	=	0x00f7
_WTEVTB	=	0xf7f6
_WTEVTC0	=	0x00fc
_WTEVTC1	=	0x00fd
_WTEVTC	=	0xfdfc
_WTEVTD0	=	0x00fe
_WTEVTD1	=	0x00ff
_WTEVTD	=	0xfffe
_WTIRQEN	=	0x00e9
_WTSTAT	=	0x00ea
;--------------------------------------------------------
; special function bits
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
_ACC_0	=	0x00e0
_ACC_1	=	0x00e1
_ACC_2	=	0x00e2
_ACC_3	=	0x00e3
_ACC_4	=	0x00e4
_ACC_5	=	0x00e5
_ACC_6	=	0x00e6
_ACC_7	=	0x00e7
_B_0	=	0x00f0
_B_1	=	0x00f1
_B_2	=	0x00f2
_B_3	=	0x00f3
_B_4	=	0x00f4
_B_5	=	0x00f5
_B_6	=	0x00f6
_B_7	=	0x00f7
_E2IE_0	=	0x00a0
_E2IE_1	=	0x00a1
_E2IE_2	=	0x00a2
_E2IE_3	=	0x00a3
_E2IE_4	=	0x00a4
_E2IE_5	=	0x00a5
_E2IE_6	=	0x00a6
_E2IE_7	=	0x00a7
_E2IP_0	=	0x00c0
_E2IP_1	=	0x00c1
_E2IP_2	=	0x00c2
_E2IP_3	=	0x00c3
_E2IP_4	=	0x00c4
_E2IP_5	=	0x00c5
_E2IP_6	=	0x00c6
_E2IP_7	=	0x00c7
_EIE_0	=	0x0098
_EIE_1	=	0x0099
_EIE_2	=	0x009a
_EIE_3	=	0x009b
_EIE_4	=	0x009c
_EIE_5	=	0x009d
_EIE_6	=	0x009e
_EIE_7	=	0x009f
_EIP_0	=	0x00b0
_EIP_1	=	0x00b1
_EIP_2	=	0x00b2
_EIP_3	=	0x00b3
_EIP_4	=	0x00b4
_EIP_5	=	0x00b5
_EIP_6	=	0x00b6
_EIP_7	=	0x00b7
_IE_0	=	0x00a8
_IE_1	=	0x00a9
_IE_2	=	0x00aa
_IE_3	=	0x00ab
_IE_4	=	0x00ac
_IE_5	=	0x00ad
_IE_6	=	0x00ae
_IE_7	=	0x00af
_EA	=	0x00af
_IP_0	=	0x00b8
_IP_1	=	0x00b9
_IP_2	=	0x00ba
_IP_3	=	0x00bb
_IP_4	=	0x00bc
_IP_5	=	0x00bd
_IP_6	=	0x00be
_IP_7	=	0x00bf
_P	=	0x00d0
_F1	=	0x00d1
_OV	=	0x00d2
_RS0	=	0x00d3
_RS1	=	0x00d4
_F0	=	0x00d5
_AC	=	0x00d6
_CY	=	0x00d7
_PINA_0	=	0x00c8
_PINA_1	=	0x00c9
_PINA_2	=	0x00ca
_PINA_3	=	0x00cb
_PINA_4	=	0x00cc
_PINA_5	=	0x00cd
_PINA_6	=	0x00ce
_PINA_7	=	0x00cf
_PINB_0	=	0x00e8
_PINB_1	=	0x00e9
_PINB_2	=	0x00ea
_PINB_3	=	0x00eb
_PINB_4	=	0x00ec
_PINB_5	=	0x00ed
_PINB_6	=	0x00ee
_PINB_7	=	0x00ef
_PINC_0	=	0x00f8
_PINC_1	=	0x00f9
_PINC_2	=	0x00fa
_PINC_3	=	0x00fb
_PINC_4	=	0x00fc
_PINC_5	=	0x00fd
_PINC_6	=	0x00fe
_PINC_7	=	0x00ff
_PORTA_0	=	0x0080
_PORTA_1	=	0x0081
_PORTA_2	=	0x0082
_PORTA_3	=	0x0083
_PORTA_4	=	0x0084
_PORTA_5	=	0x0085
_PORTA_6	=	0x0086
_PORTA_7	=	0x0087
_PORTB_0	=	0x0088
_PORTB_1	=	0x0089
_PORTB_2	=	0x008a
_PORTB_3	=	0x008b
_PORTB_4	=	0x008c
_PORTB_5	=	0x008d
_PORTB_6	=	0x008e
_PORTB_7	=	0x008f
_PORTC_0	=	0x0090
_PORTC_1	=	0x0091
_PORTC_2	=	0x0092
_PORTC_3	=	0x0093
_PORTC_4	=	0x0094
_PORTC_5	=	0x0095
_PORTC_6	=	0x0096
_PORTC_7	=	0x0097
;--------------------------------------------------------
; overlayable register banks
;--------------------------------------------------------
	.area REG_BANK_0	(REL,OVR,DATA)
	.ds 8
;--------------------------------------------------------
; internal ram data
;--------------------------------------------------------
	.area DSEG    (DATA)
_BEACON_decoding_sloc0_1_0:
	.ds 2
_BEACON_decoding_sloc1_1_0:
	.ds 2
_BEACON_decoding_sloc2_1_0:
	.ds 3
_BEACON_decoding_sloc3_1_0:
	.ds 3
_BEACON_decoding_sloc4_1_0:
	.ds 3
_BEACON_decoding_sloc5_1_0:
	.ds 4
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
	.area	OSEG    (OVR,DATA)
_VerifyACK_sloc0_1_0:
	.ds 3
_VerifyACK_sloc1_1_0:
	.ds 1
;--------------------------------------------------------
; indirectly addressable internal ram data
;--------------------------------------------------------
	.area ISEG    (DATA)
;--------------------------------------------------------
; absolute internal ram data
;--------------------------------------------------------
	.area IABS    (ABS,DATA)
	.area IABS    (ABS,DATA)
;--------------------------------------------------------
; bit data
;--------------------------------------------------------
	.area BSEG    (BIT)
;--------------------------------------------------------
; paged external ram data
;--------------------------------------------------------
	.area PSEG    (PAG,XDATA)
;--------------------------------------------------------
; external ram data
;--------------------------------------------------------
	.area XSEG    (XDATA)
_ADCCH0VAL0	=	0x7020
_ADCCH0VAL1	=	0x7021
_ADCCH0VAL	=	0x7020
_ADCCH1VAL0	=	0x7022
_ADCCH1VAL1	=	0x7023
_ADCCH1VAL	=	0x7022
_ADCCH2VAL0	=	0x7024
_ADCCH2VAL1	=	0x7025
_ADCCH2VAL	=	0x7024
_ADCCH3VAL0	=	0x7026
_ADCCH3VAL1	=	0x7027
_ADCCH3VAL	=	0x7026
_ADCTUNE0	=	0x7028
_ADCTUNE1	=	0x7029
_ADCTUNE2	=	0x702a
_DMA0ADDR0	=	0x7010
_DMA0ADDR1	=	0x7011
_DMA0ADDR	=	0x7010
_DMA0CONFIG	=	0x7014
_DMA1ADDR0	=	0x7012
_DMA1ADDR1	=	0x7013
_DMA1ADDR	=	0x7012
_DMA1CONFIG	=	0x7015
_FRCOSCCONFIG	=	0x7070
_FRCOSCCTRL	=	0x7071
_FRCOSCFREQ0	=	0x7076
_FRCOSCFREQ1	=	0x7077
_FRCOSCFREQ	=	0x7076
_FRCOSCKFILT0	=	0x7072
_FRCOSCKFILT1	=	0x7073
_FRCOSCKFILT	=	0x7072
_FRCOSCPER0	=	0x7078
_FRCOSCPER1	=	0x7079
_FRCOSCPER	=	0x7078
_FRCOSCREF0	=	0x7074
_FRCOSCREF1	=	0x7075
_FRCOSCREF	=	0x7074
_ANALOGA	=	0x7007
_GPIOENABLE	=	0x700c
_EXTIRQ	=	0x7003
_INTCHGA	=	0x7000
_INTCHGB	=	0x7001
_INTCHGC	=	0x7002
_PALTA	=	0x7008
_PALTB	=	0x7009
_PALTC	=	0x700a
_PALTRADIO	=	0x7046
_PINCHGA	=	0x7004
_PINCHGB	=	0x7005
_PINCHGC	=	0x7006
_PINSEL	=	0x700b
_LPOSCCONFIG	=	0x7060
_LPOSCFREQ0	=	0x7066
_LPOSCFREQ1	=	0x7067
_LPOSCFREQ	=	0x7066
_LPOSCKFILT0	=	0x7062
_LPOSCKFILT1	=	0x7063
_LPOSCKFILT	=	0x7062
_LPOSCPER0	=	0x7068
_LPOSCPER1	=	0x7069
_LPOSCPER	=	0x7068
_LPOSCREF0	=	0x7064
_LPOSCREF1	=	0x7065
_LPOSCREF	=	0x7064
_LPXOSCGM	=	0x7054
_MISCCTRL	=	0x7f01
_OSCCALIB	=	0x7053
_OSCFORCERUN	=	0x7050
_OSCREADY	=	0x7052
_OSCRUN	=	0x7051
_RADIOFDATAADDR0	=	0x7040
_RADIOFDATAADDR1	=	0x7041
_RADIOFDATAADDR	=	0x7040
_RADIOFSTATADDR0	=	0x7042
_RADIOFSTATADDR1	=	0x7043
_RADIOFSTATADDR	=	0x7042
_RADIOMUX	=	0x7044
_SCRATCH0	=	0x7084
_SCRATCH1	=	0x7085
_SCRATCH2	=	0x7086
_SCRATCH3	=	0x7087
_SILICONREV	=	0x7f00
_XTALAMPL	=	0x7f19
_XTALOSC	=	0x7f18
_XTALREADY	=	0x7f1a
_RNGBYTE	=	0x7081
_AESCONFIG	=	0x7091
_AESCURBLOCK	=	0x7098
_AESINADDR0	=	0x7094
_AESINADDR1	=	0x7095
_AESINADDR	=	0x7094
_AESKEYADDR0	=	0x7092
_AESKEYADDR1	=	0x7093
_AESKEYADDR	=	0x7092
_AESMODE	=	0x7090
_AESOUTADDR0	=	0x7096
_AESOUTADDR1	=	0x7097
_AESOUTADDR	=	0x7096
_RNGMODE	=	0x7080
_RNGCLKSRC0	=	0x7082
_RNGCLKSRC1	=	0x7083
_BEACON_decoding_PARM_2:
	.ds 2
_BEACON_decoding_PARM_3:
	.ds 1
_BEACON_decoding_Rxbuffer_65536_119:
	.ds 3
_VerifyACK_PARM_2:
	.ds 1
_VerifyACK_PARM_3:
	.ds 3
_VerifyACK_Rxbuffer_65536_124:
	.ds 3
_VerifyACK_RetVal_65536_125:
	.ds 1
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area XABS    (ABS,XDATA)
;--------------------------------------------------------
; external initialized ram data
;--------------------------------------------------------
	.area XISEG   (XDATA)
	.area HOME    (CODE)
	.area GSINIT0 (CODE)
	.area GSINIT1 (CODE)
	.area GSINIT2 (CODE)
	.area GSINIT3 (CODE)
	.area GSINIT4 (CODE)
	.area GSINIT5 (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area CSEG    (CODE)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME    (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area GSINIT  (CODE)
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME    (CODE)
	.area HOME    (CODE)
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CSEG    (CODE)
;------------------------------------------------------------
;Allocation info for local variables in function 'BEACON_decoding'
;------------------------------------------------------------
;sloc0                     Allocated with name '_BEACON_decoding_sloc0_1_0'
;sloc1                     Allocated with name '_BEACON_decoding_sloc1_1_0'
;sloc2                     Allocated with name '_BEACON_decoding_sloc2_1_0'
;sloc3                     Allocated with name '_BEACON_decoding_sloc3_1_0'
;sloc4                     Allocated with name '_BEACON_decoding_sloc4_1_0'
;sloc5                     Allocated with name '_BEACON_decoding_sloc5_1_0'
;beacon                    Allocated with name '_BEACON_decoding_PARM_2'
;length                    Allocated with name '_BEACON_decoding_PARM_3'
;Rxbuffer                  Allocated with name '_BEACON_decoding_Rxbuffer_65536_119'
;i                         Allocated with name '_BEACON_decoding_i_65536_120'
;------------------------------------------------------------
;	..\src\peripherals\Beacon.c:29: __xdata void BEACON_decoding(uint8_t* Rxbuffer, RF_beacon_t* beacon, uint8_t length)
;	-----------------------------------------
;	 function BEACON_decoding
;	-----------------------------------------
_BEACON_decoding:
	ar7 = 0x07
	ar6 = 0x06
	ar5 = 0x05
	ar4 = 0x04
	ar3 = 0x03
	ar2 = 0x02
	ar1 = 0x01
	ar0 = 0x00
	mov	r7,b
	mov	r6,dph
	mov	a,dpl
	mov	dptr,#_BEACON_decoding_Rxbuffer_65536_119
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:32: beacon->FlagReceived = INVALID_BEACON;
	mov	dptr,#_BEACON_decoding_PARM_2
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dpl,r6
	mov	dph,r7
	clr	a
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:34: dbglink_writestr(" largo = ");
	mov	dptr,#___str_0
	mov	b,#0x80
	push	ar7
	push	ar6
	lcall	_dbglink_writestr
;	..\src\peripherals\Beacon.c:35: dbglink_writehex16(length,1,WRNUM_PADZERO);
	mov	dptr,#_BEACON_decoding_PARM_3
	movx	a,@dptr
	mov	r5,a
	mov	r3,a
	mov	r4,#0x00
	push	ar5
	mov	a,#0x08
	push	acc
	mov	a,#0x01
	push	acc
	mov	dpl,r3
	mov	dph,r4
	lcall	_dbglink_writehex16
	dec	sp
	dec	sp
	pop	ar5
	pop	ar6
	pop	ar7
;	..\src\peripherals\Beacon.c:38: if(length == 24)
	cjne	r5,#0x18,00123$
	sjmp	00124$
00123$:
	ret
00124$:
;	..\src\peripherals\Beacon.c:41: beacon->SatId = *Rxbuffer <<8 | *(Rxbuffer+1);
	mov	a,#0x01
	add	a,r6
	mov	_BEACON_decoding_sloc0_1_0,a
	clr	a
	addc	a,r7
	mov	(_BEACON_decoding_sloc0_1_0 + 1),a
	mov	dptr,#_BEACON_decoding_Rxbuffer_65536_119
	movx	a,@dptr
	mov	r1,a
	inc	dptr
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	mov	dpl,r1
	mov	dph,r2
	mov	b,r3
	lcall	__gptrget
	mov	r0,a
	mov	r5,#0x00
	mov	(_BEACON_decoding_sloc1_1_0 + 1),r0
;	1-genFromRTrack replaced	mov	_BEACON_decoding_sloc1_1_0,#0x00
	mov	_BEACON_decoding_sloc1_1_0,r5
	mov	a,#0x01
	add	a,r1
	mov	r0,a
	clr	a
	addc	a,r2
	mov	r4,a
	mov	ar5,r3
	mov	dpl,r0
	mov	dph,r4
	mov	b,r5
	lcall	__gptrget
	mov	r0,a
	mov	r5,#0x00
	mov	a,_BEACON_decoding_sloc1_1_0
	orl	ar0,a
	mov	a,(_BEACON_decoding_sloc1_1_0 + 1)
	orl	ar5,a
	mov	dpl,_BEACON_decoding_sloc0_1_0
	mov	dph,(_BEACON_decoding_sloc0_1_0 + 1)
	mov	a,r0
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:42: beacon->BeaconID = (*(Rxbuffer+POS_BEACONID) & MASK_BEACONID)>> 4;
	mov	a,#0x03
	add	a,r6
	mov	r4,a
	clr	a
	addc	a,r7
	mov	r5,a
	push	ar6
	push	ar7
	mov	a,#0x02
	add	a,r1
	mov	r0,a
	clr	a
	addc	a,r2
	mov	r6,a
	mov	ar7,r3
	mov	dpl,r0
	mov	dph,r6
	mov	b,r7
	lcall	__gptrget
	mov	r0,a
	anl	ar0,#0xf0
	clr	a
	xch	a,r0
	swap	a
	anl	a,#0x0f
	xrl	a,r0
	xch	a,r0
	anl	a,#0x0f
	xch	a,r0
	xrl	a,r0
	xch	a,r0
	jnb	acc.3,00125$
	orl	a,#0xf0
00125$:
	mov	r7,a
	mov	dpl,r4
	mov	dph,r5
	mov	a,r0
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:45: if(beacon->BeaconID == UPLINK_BEACON) // se usa 0x3 como valor provisorio porque es lo que tiene el transmisor
	mov	dpl,r4
	mov	dph,r5
	movx	a,@dptr
	mov	_BEACON_decoding_sloc1_1_0,a
	cjne	r0,#0x0c,00126$
	sjmp	00127$
00126$:
	pop	ar7
	pop	ar6
	sjmp	00105$
00127$:
	pop	ar7
	pop	ar6
;	..\src\peripherals\Beacon.c:47: beacon->FlagReceived = UPLINK_BEACON;
	mov	dpl,r6
	mov	dph,r7
	mov	a,#0x0c
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:49: dbglink_writestr(" Uplink");
	mov	dptr,#___str_1
	mov	b,#0x80
	push	ar7
	push	ar6
	push	ar3
	push	ar2
	push	ar1
	lcall	_dbglink_writestr
	pop	ar1
	pop	ar2
	pop	ar3
	pop	ar6
	pop	ar7
;	..\src\peripherals\Beacon.c:51: beacon->NumPremSlots = *(Rxbuffer+POS_NUMPREM);
	mov	a,#0x04
	add	a,r6
	mov	_BEACON_decoding_sloc0_1_0,a
	clr	a
	addc	a,r7
	mov	(_BEACON_decoding_sloc0_1_0 + 1),a
	mov	a,#0x03
	add	a,r1
	mov	r0,a
	clr	a
	addc	a,r2
	mov	r4,a
	mov	ar5,r3
	mov	dpl,r0
	mov	dph,r4
	mov	b,r5
	lcall	__gptrget
	mov	r0,a
	mov	dpl,_BEACON_decoding_sloc0_1_0
	mov	dph,(_BEACON_decoding_sloc0_1_0 + 1)
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:52: beacon->NumACK = 0xFF; //invalid data
	mov	dpl,r6
	mov	dph,r7
	inc	dptr
	inc	dptr
	inc	dptr
	inc	dptr
	inc	dptr
	mov	a,#0xff
	movx	@dptr,a
	sjmp	00106$
00105$:
;	..\src\peripherals\Beacon.c:54: else if(beacon->BeaconID == DOWNLINK_BEACON)
	mov	a,#0x0a
	cjne	a,_BEACON_decoding_sloc1_1_0,00102$
;	..\src\peripherals\Beacon.c:56: beacon->FlagReceived = DOWNLINK_BEACON;
	mov	dpl,r6
	mov	dph,r7
	mov	a,#0x0a
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:58: dbglink_writestr(" Downlink ");
	mov	dptr,#___str_2
	mov	b,#0x80
	push	ar7
	push	ar6
	push	ar3
	push	ar2
	push	ar1
	lcall	_dbglink_writestr
	pop	ar1
	pop	ar2
	pop	ar3
	pop	ar6
	pop	ar7
;	..\src\peripherals\Beacon.c:60: beacon->NumACK = *(Rxbuffer+POS_NUMPREM);
	mov	a,#0x05
	add	a,r6
	mov	r4,a
	clr	a
	addc	a,r7
	mov	r5,a
	mov	a,#0x03
	add	a,r1
	mov	r1,a
	clr	a
	addc	a,r2
	mov	r2,a
	mov	dpl,r1
	mov	dph,r2
	mov	b,r3
	lcall	__gptrget
	mov	r1,a
	mov	dpl,r4
	mov	dph,r5
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:61: beacon->NumPremSlots = 0xFF; //invalid data
	mov	dpl,r6
	mov	dph,r7
	inc	dptr
	inc	dptr
	inc	dptr
	inc	dptr
	mov	a,#0xff
	movx	@dptr,a
	sjmp	00106$
00102$:
;	..\src\peripherals\Beacon.c:63: else beacon->FlagReceived = INVALID_BEACON;
	mov	dpl,r6
	mov	dph,r7
	clr	a
	movx	@dptr,a
00106$:
;	..\src\peripherals\Beacon.c:64: beacon->HMAC_flag =(*(Rxbuffer+POS_SECURITY) & MASK_HMAC_FLAG)>>7;
	mov	a,#0x06
	add	a,r6
	mov	_BEACON_decoding_sloc1_1_0,a
	clr	a
	addc	a,r7
	mov	(_BEACON_decoding_sloc1_1_0 + 1),a
	mov	dptr,#_BEACON_decoding_Rxbuffer_65536_119
	movx	a,@dptr
	mov	_BEACON_decoding_sloc4_1_0,a
	inc	dptr
	movx	a,@dptr
	mov	(_BEACON_decoding_sloc4_1_0 + 1),a
	inc	dptr
	movx	a,@dptr
	mov	(_BEACON_decoding_sloc4_1_0 + 2),a
	mov	a,#0x04
	add	a,_BEACON_decoding_sloc4_1_0
	mov	_BEACON_decoding_sloc2_1_0,a
	clr	a
	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
	mov	(_BEACON_decoding_sloc2_1_0 + 1),a
	mov	(_BEACON_decoding_sloc2_1_0 + 2),(_BEACON_decoding_sloc4_1_0 + 2)
	mov	dpl,_BEACON_decoding_sloc2_1_0
	mov	dph,(_BEACON_decoding_sloc2_1_0 + 1)
	mov	b,(_BEACON_decoding_sloc2_1_0 + 2)
	lcall	__gptrget
	mov	r5,a
	anl	ar5,#0x80
	clr	a
	mov	c,acc.7
	xch	a,r5
	rlc	a
	xch	a,r5
	rlc	a
	xch	a,r5
	anl	a,#0x01
	jnb	acc.0,00130$
	orl	a,#0xfe
00130$:
	mov	dpl,_BEACON_decoding_sloc1_1_0
	mov	dph,(_BEACON_decoding_sloc1_1_0 + 1)
	mov	a,r5
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:65: beacon->HMAC_type = (*(Rxbuffer+POS_SECURITY) & MASK_HMAC_TYPE)>>4;
	mov	a,#0x07
	add	a,r6
	mov	_BEACON_decoding_sloc1_1_0,a
	clr	a
	addc	a,r7
	mov	(_BEACON_decoding_sloc1_1_0 + 1),a
	mov	dpl,_BEACON_decoding_sloc2_1_0
	mov	dph,(_BEACON_decoding_sloc2_1_0 + 1)
	mov	b,(_BEACON_decoding_sloc2_1_0 + 2)
	lcall	__gptrget
	mov	r0,a
	anl	ar0,#0x70
	clr	a
	xch	a,r0
	swap	a
	anl	a,#0x0f
	xrl	a,r0
	xch	a,r0
	anl	a,#0x0f
	xch	a,r0
	xrl	a,r0
	xch	a,r0
	jnb	acc.3,00131$
	orl	a,#0xf0
00131$:
	mov	dpl,_BEACON_decoding_sloc1_1_0
	mov	dph,(_BEACON_decoding_sloc1_1_0 + 1)
	mov	a,r0
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:66: beacon->Encryption_flag = (*(Rxbuffer+POS_SECURITY) & MASK_ENCRYPTION_FLAG)>>3;
	mov	a,#0x08
	add	a,r6
	mov	_BEACON_decoding_sloc1_1_0,a
	clr	a
	addc	a,r7
	mov	(_BEACON_decoding_sloc1_1_0 + 1),a
	mov	dpl,_BEACON_decoding_sloc2_1_0
	mov	dph,(_BEACON_decoding_sloc2_1_0 + 1)
	mov	b,(_BEACON_decoding_sloc2_1_0 + 2)
	lcall	__gptrget
	mov	r0,a
	anl	ar0,#0x08
	clr	a
	rl	a
	xch	a,r0
	swap	a
	rl	a
	anl	a,#0x1f
	xrl	a,r0
	xch	a,r0
	anl	a,#0x1f
	xch	a,r0
	xrl	a,r0
	xch	a,r0
	jnb	acc.4,00132$
	orl	a,#0xe0
00132$:
	mov	dpl,_BEACON_decoding_sloc1_1_0
	mov	dph,(_BEACON_decoding_sloc1_1_0 + 1)
	mov	a,r0
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:67: beacon->Encryption_type = *(Rxbuffer+POS_SECURITY) & MASK_ENCRYPTION_TYPE;
	mov	a,#0x09
	add	a,r6
	mov	r4,a
	clr	a
	addc	a,r7
	mov	r5,a
	mov	dpl,_BEACON_decoding_sloc2_1_0
	mov	dph,(_BEACON_decoding_sloc2_1_0 + 1)
	mov	b,(_BEACON_decoding_sloc2_1_0 + 2)
	lcall	__gptrget
	mov	r0,a
	anl	ar0,#0x07
	mov	dpl,r4
	mov	dph,r5
	mov	a,r0
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:69: *(Rxbuffer+POS_NONCE)   = *(Rxbuffer+POS_NONCE) + *(Rxbuffer+POS_NONCE+1);
	mov	a,#0x05
	add	a,_BEACON_decoding_sloc4_1_0
	mov	_BEACON_decoding_sloc2_1_0,a
	clr	a
	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
	mov	(_BEACON_decoding_sloc2_1_0 + 1),a
	mov	(_BEACON_decoding_sloc2_1_0 + 2),(_BEACON_decoding_sloc4_1_0 + 2)
	mov	dpl,_BEACON_decoding_sloc2_1_0
	mov	dph,(_BEACON_decoding_sloc2_1_0 + 1)
	mov	b,(_BEACON_decoding_sloc2_1_0 + 2)
	lcall	__gptrget
	mov	_BEACON_decoding_sloc1_1_0,a
	mov	a,#0x06
	add	a,_BEACON_decoding_sloc4_1_0
	mov	_BEACON_decoding_sloc3_1_0,a
	clr	a
	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
	mov	(_BEACON_decoding_sloc3_1_0 + 1),a
	mov	(_BEACON_decoding_sloc3_1_0 + 2),(_BEACON_decoding_sloc4_1_0 + 2)
	mov	dpl,_BEACON_decoding_sloc3_1_0
	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
	mov	b,(_BEACON_decoding_sloc3_1_0 + 2)
	lcall	__gptrget
	add	a,_BEACON_decoding_sloc1_1_0
	mov	r5,a
	mov	dpl,_BEACON_decoding_sloc2_1_0
	mov	dph,(_BEACON_decoding_sloc2_1_0 + 1)
	mov	b,(_BEACON_decoding_sloc2_1_0 + 2)
	lcall	__gptrput
;	..\src\peripherals\Beacon.c:70: *(Rxbuffer+POS_NONCE+1) = *(Rxbuffer+POS_NONCE) - *(Rxbuffer+POS_NONCE+1);
	mov	dpl,_BEACON_decoding_sloc3_1_0
	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
	mov	b,(_BEACON_decoding_sloc3_1_0 + 2)
	lcall	__gptrget
	mov	r4,a
	mov	a,r5
	clr	c
	subb	a,r4
	mov	r4,a
	mov	dpl,_BEACON_decoding_sloc3_1_0
	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
	mov	b,(_BEACON_decoding_sloc3_1_0 + 2)
	lcall	__gptrput
;	..\src\peripherals\Beacon.c:71: *(Rxbuffer+POS_NONCE)   = *(Rxbuffer+POS_NONCE) - *(Rxbuffer+POS_NONCE+1);
	mov	a,r5
	clr	c
	subb	a,r4
	mov	dpl,_BEACON_decoding_sloc2_1_0
	mov	dph,(_BEACON_decoding_sloc2_1_0 + 1)
	mov	b,(_BEACON_decoding_sloc2_1_0 + 2)
	lcall	__gptrput
;	..\src\peripherals\Beacon.c:73: memcpy(&beacon->Nonce,Rxbuffer+POS_NONCE,sizeof(uint16_t));
	mov	a,#0x0a
	add	a,r6
	mov	r4,a
	clr	a
	addc	a,r7
	mov	r5,a
	mov	_BEACON_decoding_sloc3_1_0,r4
	mov	(_BEACON_decoding_sloc3_1_0 + 1),r5
	mov	(_BEACON_decoding_sloc3_1_0 + 2),#0x00
	mov	r0,_BEACON_decoding_sloc2_1_0
	mov	r4,(_BEACON_decoding_sloc2_1_0 + 1)
	mov	r5,(_BEACON_decoding_sloc2_1_0 + 2)
	mov	dptr,#_memcpy_PARM_2
	mov	a,r0
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_memcpy_PARM_3
	mov	a,#0x02
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
	mov	dpl,_BEACON_decoding_sloc3_1_0
	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
	mov	b,(_BEACON_decoding_sloc3_1_0 + 2)
	push	ar7
	push	ar6
	lcall	_memcpy
	pop	ar6
	pop	ar7
;	..\src\peripherals\Beacon.c:75: memcpy(&beacon->reserved,Rxbuffer+POS_RESERVED,sizeof(uint8_t)*5);
	mov	a,#0x0c
	add	a,r6
	mov	r4,a
	clr	a
	addc	a,r7
	mov	r5,a
	mov	_BEACON_decoding_sloc3_1_0,r4
	mov	(_BEACON_decoding_sloc3_1_0 + 1),r5
	mov	(_BEACON_decoding_sloc3_1_0 + 2),#0x00
	mov	a,#0x07
	add	a,_BEACON_decoding_sloc4_1_0
	mov	r0,a
	clr	a
	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
	mov	r4,a
	mov	r5,(_BEACON_decoding_sloc4_1_0 + 2)
	mov	dptr,#_memcpy_PARM_2
	mov	a,r0
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_memcpy_PARM_3
	mov	a,#0x05
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
	mov	dpl,_BEACON_decoding_sloc3_1_0
	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
	mov	b,(_BEACON_decoding_sloc3_1_0 + 2)
	push	ar7
	push	ar6
	lcall	_memcpy
	pop	ar6
	pop	ar7
;	..\src\peripherals\Beacon.c:77: beacon->CRC =  *(Rxbuffer+POS_CRC) <<8 | *(Rxbuffer+POS_CRC+1);
	mov	a,#0x11
	add	a,r6
	mov	_BEACON_decoding_sloc3_1_0,a
	clr	a
	addc	a,r7
	mov	(_BEACON_decoding_sloc3_1_0 + 1),a
	mov	a,#0x0c
	add	a,_BEACON_decoding_sloc4_1_0
	mov	r0,a
	clr	a
	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
	mov	r4,a
	mov	r5,(_BEACON_decoding_sloc4_1_0 + 2)
	mov	dpl,r0
	mov	dph,r4
	mov	b,r5
	lcall	__gptrget
	mov	r0,a
	mov	r5,#0x00
	mov	(_BEACON_decoding_sloc2_1_0 + 1),r0
;	1-genFromRTrack replaced	mov	_BEACON_decoding_sloc2_1_0,#0x00
	mov	_BEACON_decoding_sloc2_1_0,r5
	mov	a,#0x0d
	add	a,_BEACON_decoding_sloc4_1_0
	mov	r0,a
	clr	a
	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
	mov	r4,a
	mov	r5,(_BEACON_decoding_sloc4_1_0 + 2)
	mov	dpl,r0
	mov	dph,r4
	mov	b,r5
	lcall	__gptrget
	mov	r0,a
	mov	r5,#0x00
	mov	a,_BEACON_decoding_sloc2_1_0
	orl	ar0,a
	mov	a,(_BEACON_decoding_sloc2_1_0 + 1)
	orl	ar5,a
	mov	a,r5
	mov	r3,a
	rlc	a
	subb	a,acc
	mov	r4,a
	mov	r5,a
	mov	dpl,_BEACON_decoding_sloc3_1_0
	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
	mov	a,r0
	movx	@dptr,a
	mov	a,r3
	inc	dptr
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:78: beacon->CRC = beacon->CRC << 16;
	mov	(_BEACON_decoding_sloc5_1_0 + 3),r3
	mov	(_BEACON_decoding_sloc5_1_0 + 2),r0
	mov	_BEACON_decoding_sloc5_1_0,#0x00
	mov	(_BEACON_decoding_sloc5_1_0 + 1),#0x00
	mov	dpl,_BEACON_decoding_sloc3_1_0
	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
	mov	a,_BEACON_decoding_sloc5_1_0
	movx	@dptr,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
	inc	dptr
	movx	@dptr,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 2)
	inc	dptr
	movx	@dptr,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 3)
	inc	dptr
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:79: beacon->CRC = beacon->CRC | *(Rxbuffer+POS_CRC+2)<<8 | *(Rxbuffer+POS_CRC+3);
	mov	a,#0x0e
	add	a,_BEACON_decoding_sloc4_1_0
	mov	r1,a
	clr	a
	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
	mov	r2,a
	mov	r5,(_BEACON_decoding_sloc4_1_0 + 2)
	mov	dpl,r1
	mov	dph,r2
	mov	b,r5
	lcall	__gptrget
	mov	r5,a
	mov	r1,#0x00
	rlc	a
	subb	a,acc
	mov	r4,a
	mov	r3,a
	mov	a,r1
	orl	_BEACON_decoding_sloc5_1_0,a
	mov	a,r5
	orl	(_BEACON_decoding_sloc5_1_0 + 1),a
	mov	a,r4
	orl	(_BEACON_decoding_sloc5_1_0 + 2),a
	mov	a,r3
	orl	(_BEACON_decoding_sloc5_1_0 + 3),a
	mov	a,#0x0f
	add	a,_BEACON_decoding_sloc4_1_0
	mov	r0,a
	clr	a
	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
	mov	r2,a
	mov	r5,(_BEACON_decoding_sloc4_1_0 + 2)
	mov	dpl,r0
	mov	dph,r2
	mov	b,r5
	lcall	__gptrget
	mov	r0,a
	clr	a
	mov	r5,a
	mov	r4,a
	mov	r3,a
	mov	a,_BEACON_decoding_sloc5_1_0
	orl	ar0,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
	orl	ar5,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 2)
	orl	ar4,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 3)
	orl	ar3,a
	mov	dpl,_BEACON_decoding_sloc3_1_0
	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
	mov	a,r0
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
	mov	a,r3
	inc	dptr
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:80: beacon->HMAC_Msb = *(Rxbuffer+POS_HMAC_MSB)<<8 | *(Rxbuffer+POS_HMAC_MSB+1);
	mov	a,#0x15
	add	a,r6
	mov	r4,a
	clr	a
	addc	a,r7
	mov	r5,a
	mov	a,#0x10
	add	a,_BEACON_decoding_sloc4_1_0
	mov	r1,a
	clr	a
	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
	mov	r2,a
	mov	r3,(_BEACON_decoding_sloc4_1_0 + 2)
	mov	dpl,r1
	mov	dph,r2
	mov	b,r3
	lcall	__gptrget
	mov	r1,a
	mov	r3,#0x00
	mov	(_BEACON_decoding_sloc5_1_0 + 1),r1
;	1-genFromRTrack replaced	mov	_BEACON_decoding_sloc5_1_0,#0x00
	mov	_BEACON_decoding_sloc5_1_0,r3
	mov	a,#0x11
	add	a,_BEACON_decoding_sloc4_1_0
	mov	r0,a
	clr	a
	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
	mov	r2,a
	mov	r3,(_BEACON_decoding_sloc4_1_0 + 2)
	mov	dpl,r0
	mov	dph,r2
	mov	b,r3
	lcall	__gptrget
	mov	r0,a
	mov	r3,#0x00
	mov	a,_BEACON_decoding_sloc5_1_0
	orl	ar0,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
	orl	ar3,a
	mov	a,r3
	mov	r1,a
	rlc	a
	subb	a,acc
	mov	r2,a
	mov	r3,a
	mov	dpl,r4
	mov	dph,r5
	mov	a,r0
	movx	@dptr,a
	mov	a,r1
	inc	dptr
	movx	@dptr,a
	mov	a,r2
	inc	dptr
	movx	@dptr,a
	mov	a,r3
	inc	dptr
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:81: beacon->HMAC_Msb = beacon->HMAC_Msb << 16;
	mov	(_BEACON_decoding_sloc5_1_0 + 3),r1
	mov	(_BEACON_decoding_sloc5_1_0 + 2),r0
	mov	_BEACON_decoding_sloc5_1_0,#0x00
	mov	(_BEACON_decoding_sloc5_1_0 + 1),#0x00
	mov	dpl,r4
	mov	dph,r5
	mov	a,_BEACON_decoding_sloc5_1_0
	movx	@dptr,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
	inc	dptr
	movx	@dptr,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 2)
	inc	dptr
	movx	@dptr,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 3)
	inc	dptr
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:82: beacon->HMAC_Msb = beacon->HMAC_Msb | (*(Rxbuffer+POS_HMAC_MSB+2)<<8 | *(Rxbuffer+POS_HMAC_MSB+3) & 0x0000FFFF);
	mov	a,#0x12
	add	a,_BEACON_decoding_sloc4_1_0
	mov	r1,a
	clr	a
	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
	mov	r2,a
	mov	r3,(_BEACON_decoding_sloc4_1_0 + 2)
	mov	dpl,r1
	mov	dph,r2
	mov	b,r3
	lcall	__gptrget
	mov	r1,a
	mov	r3,#0x00
	mov	(_BEACON_decoding_sloc3_1_0 + 1),r1
;	1-genFromRTrack replaced	mov	_BEACON_decoding_sloc3_1_0,#0x00
	mov	_BEACON_decoding_sloc3_1_0,r3
	mov	a,#0x13
	add	a,_BEACON_decoding_sloc4_1_0
	mov	r0,a
	clr	a
	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
	mov	r2,a
	mov	r3,(_BEACON_decoding_sloc4_1_0 + 2)
	mov	dpl,r0
	mov	dph,r2
	mov	b,r3
	lcall	__gptrget
	mov	r0,a
	mov	r3,#0x00
	mov	r1,_BEACON_decoding_sloc3_1_0
	mov	r2,(_BEACON_decoding_sloc3_1_0 + 1)
	mov	a,r1
	orl	ar0,a
	mov	a,r2
	orl	ar3,a
	mov	ar1,r3
	clr	a
	mov	r2,a
	mov	r3,a
	mov	a,_BEACON_decoding_sloc5_1_0
	orl	ar0,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
	orl	ar1,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 2)
	orl	ar2,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 3)
	orl	ar3,a
	mov	dpl,r4
	mov	dph,r5
	mov	a,r0
	movx	@dptr,a
	mov	a,r1
	inc	dptr
	movx	@dptr,a
	mov	a,r2
	inc	dptr
	movx	@dptr,a
	mov	a,r3
	inc	dptr
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:83: beacon->HMAC_Lsb = *(Rxbuffer+POS_HMAC_LSB)<<8 | *(Rxbuffer+POS_HMAC_LSB+1);
	mov	a,#0x19
	add	a,r6
	mov	r6,a
	clr	a
	addc	a,r7
	mov	r7,a
	mov	a,#0x14
	add	a,_BEACON_decoding_sloc4_1_0
	mov	r3,a
	clr	a
	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
	mov	r4,a
	mov	r5,(_BEACON_decoding_sloc4_1_0 + 2)
	mov	dpl,r3
	mov	dph,r4
	mov	b,r5
	lcall	__gptrget
	mov	r5,a
	mov	r3,#0x00
	mov	a,#0x15
	add	a,_BEACON_decoding_sloc4_1_0
	mov	r1,a
	clr	a
	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
	mov	r2,a
	mov	r4,(_BEACON_decoding_sloc4_1_0 + 2)
	mov	dpl,r1
	mov	dph,r2
	mov	b,r4
	lcall	__gptrget
	mov	r4,#0x00
	orl	ar3,a
	mov	a,r4
	orl	ar5,a
	mov	a,r5
	rlc	a
	subb	a,acc
	mov	r4,a
	mov	r2,a
	mov	dpl,r6
	mov	dph,r7
	mov	a,r3
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
	mov	a,r2
	inc	dptr
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:84: beacon->HMAC_Lsb = beacon->HMAC_Lsb << 16;
	mov	(_BEACON_decoding_sloc5_1_0 + 3),r5
	mov	(_BEACON_decoding_sloc5_1_0 + 2),r3
	mov	_BEACON_decoding_sloc5_1_0,#0x00
	mov	(_BEACON_decoding_sloc5_1_0 + 1),#0x00
	mov	dpl,r6
	mov	dph,r7
	mov	a,_BEACON_decoding_sloc5_1_0
	movx	@dptr,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
	inc	dptr
	movx	@dptr,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 2)
	inc	dptr
	movx	@dptr,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 3)
	inc	dptr
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:85: beacon->HMAC_Lsb = beacon->HMAC_Lsb | (*(Rxbuffer+POS_HMAC_LSB+2)<<8 | *(Rxbuffer+POS_HMAC_LSB+3) & 0x0000FFFF);
	mov	a,#0x16
	add	a,_BEACON_decoding_sloc4_1_0
	mov	r0,a
	clr	a
	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
	mov	r1,a
	mov	r5,(_BEACON_decoding_sloc4_1_0 + 2)
	mov	dpl,r0
	mov	dph,r1
	mov	b,r5
	lcall	__gptrget
	mov	r5,a
	mov	r0,#0x00
	mov	a,#0x17
	add	a,_BEACON_decoding_sloc4_1_0
	mov	r2,a
	clr	a
	addc	a,(_BEACON_decoding_sloc4_1_0 + 1)
	mov	r3,a
	mov	r4,(_BEACON_decoding_sloc4_1_0 + 2)
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
	mov	r2,a
	mov	r4,#0x00
	mov	a,r0
	orl	ar2,a
	mov	a,r5
	orl	ar4,a
	clr	a
	mov	r5,a
	mov	r3,a
	mov	a,_BEACON_decoding_sloc5_1_0
	orl	ar2,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
	orl	ar4,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 2)
	orl	ar5,a
	mov	a,(_BEACON_decoding_sloc5_1_0 + 3)
	orl	ar3,a
	mov	dpl,r6
	mov	dph,r7
	mov	a,r2
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r3
	inc	dptr
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:89: }
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'VerifyACK'
;------------------------------------------------------------
;length                    Allocated with name '_VerifyACK_PARM_2'
;ReceivedData              Allocated with name '_VerifyACK_PARM_3'
;Rxbuffer                  Allocated with name '_VerifyACK_Rxbuffer_65536_124'
;i                         Allocated with name '_VerifyACK_i_65536_125'
;RetVal                    Allocated with name '_VerifyACK_RetVal_65536_125'
;sloc0                     Allocated with name '_VerifyACK_sloc0_1_0'
;sloc1                     Allocated with name '_VerifyACK_sloc1_1_0'
;------------------------------------------------------------
;	..\src\peripherals\Beacon.c:101: uint8_t VerifyACK(uint8_t* Rxbuffer,uint8_t length, uint8_t* ReceivedData)
;	-----------------------------------------
;	 function VerifyACK
;	-----------------------------------------
_VerifyACK:
	mov	r7,b
	mov	r6,dph
	mov	a,dpl
	mov	dptr,#_VerifyACK_Rxbuffer_65536_124
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:104: uint8_t RetVal = NoACK;
	mov	dptr,#_VerifyACK_RetVal_65536_125
	clr	a
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:105: for(i = 0;i < length; i = i+3) //revisar bien desde donde empiezo a tomar el dato
	mov	dptr,#_VerifyACK_Rxbuffer_65536_124
	movx	a,@dptr
	mov	_VerifyACK_sloc0_1_0,a
	inc	dptr
	movx	a,@dptr
	mov	(_VerifyACK_sloc0_1_0 + 1),a
	inc	dptr
	movx	a,@dptr
	mov	(_VerifyACK_sloc0_1_0 + 2),a
	mov	dptr,#_VerifyACK_PARM_2
	movx	a,@dptr
	mov	r4,a
	mov	r2,#0x00
	mov	r3,#0x00
00105$:
	mov	ar0,r4
	mov	r1,#0x00
	clr	c
	mov	a,r2
	subb	a,r0
	mov	a,r3
	xrl	a,#0x80
	mov	b,r1
	xrl	b,#0x80
	subb	a,b
	jnc	00103$
;	..\src\peripherals\Beacon.c:107: if(*(Rxbuffer+i) == 0x12/*localaddr.addr[0]*/ & *(Rxbuffer+i+1) == 0x34 /*localaddr.addr[1]*/ & *(Rxbuffer+i+2) == 0x56 /*localaddr.addr[2]*/)
	push	ar4
	mov	a,r2
	add	a,_VerifyACK_sloc0_1_0
	mov	r0,a
	mov	a,r3
	addc	a,(_VerifyACK_sloc0_1_0 + 1)
	mov	r1,a
	mov	r4,(_VerifyACK_sloc0_1_0 + 2)
	mov	dpl,r0
	mov	dph,r1
	mov	b,r4
	lcall	__gptrget
	mov	r7,a
	clr	a
	cjne	r7,#0x12,00122$
	inc	a
00122$:
	mov	_VerifyACK_sloc1_1_0,a
	mov	a,#0x01
	add	a,r0
	mov	r5,a
	clr	a
	addc	a,r1
	mov	r6,a
	mov	ar7,r4
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	lcall	__gptrget
	mov	r5,a
	clr	a
	cjne	r5,#0x34,00124$
	inc	a
00124$:
	anl	a,_VerifyACK_sloc1_1_0
	mov	r7,a
	mov	a,#0x02
	add	a,r0
	mov	r0,a
	clr	a
	addc	a,r1
	mov	r1,a
	mov	dpl,r0
	mov	dph,r1
	mov	b,r4
	lcall	__gptrget
	mov	r0,a
	clr	a
	cjne	r0,#0x56,00126$
	inc	a
00126$:
	anl	a,r7
	pop	ar4
	jz	00106$
;	..\src\peripherals\Beacon.c:109: RetVal = ReceivedACK;
	mov	dptr,#_VerifyACK_RetVal_65536_125
	mov	a,#0x01
	movx	@dptr,a
;	..\src\peripherals\Beacon.c:110: break;
	sjmp	00103$
00106$:
;	..\src\peripherals\Beacon.c:105: for(i = 0;i < length; i = i+3) //revisar bien desde donde empiezo a tomar el dato
	mov	a,#0x03
	add	a,r2
	mov	r2,a
	clr	a
	addc	a,r3
	mov	r3,a
	ljmp	00105$
00103$:
;	..\src\peripherals\Beacon.c:113: return RetVal;
	mov	dptr,#_VerifyACK_RetVal_65536_125
	movx	a,@dptr
;	..\src\peripherals\Beacon.c:114: }
	mov	dpl,a
	ret
	.area CSEG    (CODE)
	.area CONST   (CODE)
	.area CONST   (CODE)
___str_0:
	.ascii " largo = "
	.db 0x00
	.area CSEG    (CODE)
	.area CONST   (CODE)
___str_1:
	.ascii " Uplink"
	.db 0x00
	.area CSEG    (CODE)
	.area CONST   (CODE)
___str_2:
	.ascii " Downlink "
	.db 0x00
	.area CSEG    (CODE)
	.area XINIT   (CODE)
	.area CABS    (ABS,CODE)
