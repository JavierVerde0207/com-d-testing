;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.9.3 #11354 (MINGW32)
;--------------------------------------------------------
	.module GoldSequence
	.optsdcc -mmcs51 --model-large
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _EraseMemoryPage
	.globl _WriteMemory
	.globl _flash_read
	.globl _flash_lock
	.globl _flash_unlock
	.globl _PORTC_7
	.globl _PORTC_6
	.globl _PORTC_5
	.globl _PORTC_4
	.globl _PORTC_3
	.globl _PORTC_2
	.globl _PORTC_1
	.globl _PORTC_0
	.globl _PORTB_7
	.globl _PORTB_6
	.globl _PORTB_5
	.globl _PORTB_4
	.globl _PORTB_3
	.globl _PORTB_2
	.globl _PORTB_1
	.globl _PORTB_0
	.globl _PORTA_7
	.globl _PORTA_6
	.globl _PORTA_5
	.globl _PORTA_4
	.globl _PORTA_3
	.globl _PORTA_2
	.globl _PORTA_1
	.globl _PORTA_0
	.globl _PINC_7
	.globl _PINC_6
	.globl _PINC_5
	.globl _PINC_4
	.globl _PINC_3
	.globl _PINC_2
	.globl _PINC_1
	.globl _PINC_0
	.globl _PINB_7
	.globl _PINB_6
	.globl _PINB_5
	.globl _PINB_4
	.globl _PINB_3
	.globl _PINB_2
	.globl _PINB_1
	.globl _PINB_0
	.globl _PINA_7
	.globl _PINA_6
	.globl _PINA_5
	.globl _PINA_4
	.globl _PINA_3
	.globl _PINA_2
	.globl _PINA_1
	.globl _PINA_0
	.globl _CY
	.globl _AC
	.globl _F0
	.globl _RS1
	.globl _RS0
	.globl _OV
	.globl _F1
	.globl _P
	.globl _IP_7
	.globl _IP_6
	.globl _IP_5
	.globl _IP_4
	.globl _IP_3
	.globl _IP_2
	.globl _IP_1
	.globl _IP_0
	.globl _EA
	.globl _IE_7
	.globl _IE_6
	.globl _IE_5
	.globl _IE_4
	.globl _IE_3
	.globl _IE_2
	.globl _IE_1
	.globl _IE_0
	.globl _EIP_7
	.globl _EIP_6
	.globl _EIP_5
	.globl _EIP_4
	.globl _EIP_3
	.globl _EIP_2
	.globl _EIP_1
	.globl _EIP_0
	.globl _EIE_7
	.globl _EIE_6
	.globl _EIE_5
	.globl _EIE_4
	.globl _EIE_3
	.globl _EIE_2
	.globl _EIE_1
	.globl _EIE_0
	.globl _E2IP_7
	.globl _E2IP_6
	.globl _E2IP_5
	.globl _E2IP_4
	.globl _E2IP_3
	.globl _E2IP_2
	.globl _E2IP_1
	.globl _E2IP_0
	.globl _E2IE_7
	.globl _E2IE_6
	.globl _E2IE_5
	.globl _E2IE_4
	.globl _E2IE_3
	.globl _E2IE_2
	.globl _E2IE_1
	.globl _E2IE_0
	.globl _B_7
	.globl _B_6
	.globl _B_5
	.globl _B_4
	.globl _B_3
	.globl _B_2
	.globl _B_1
	.globl _B_0
	.globl _ACC_7
	.globl _ACC_6
	.globl _ACC_5
	.globl _ACC_4
	.globl _ACC_3
	.globl _ACC_2
	.globl _ACC_1
	.globl _ACC_0
	.globl _WTSTAT
	.globl _WTIRQEN
	.globl _WTEVTD
	.globl _WTEVTD1
	.globl _WTEVTD0
	.globl _WTEVTC
	.globl _WTEVTC1
	.globl _WTEVTC0
	.globl _WTEVTB
	.globl _WTEVTB1
	.globl _WTEVTB0
	.globl _WTEVTA
	.globl _WTEVTA1
	.globl _WTEVTA0
	.globl _WTCNTR1
	.globl _WTCNTB
	.globl _WTCNTB1
	.globl _WTCNTB0
	.globl _WTCNTA
	.globl _WTCNTA1
	.globl _WTCNTA0
	.globl _WTCFGB
	.globl _WTCFGA
	.globl _WDTRESET
	.globl _WDTCFG
	.globl _U1STATUS
	.globl _U1SHREG
	.globl _U1MODE
	.globl _U1CTRL
	.globl _U0STATUS
	.globl _U0SHREG
	.globl _U0MODE
	.globl _U0CTRL
	.globl _T2STATUS
	.globl _T2PERIOD
	.globl _T2PERIOD1
	.globl _T2PERIOD0
	.globl _T2MODE
	.globl _T2CNT
	.globl _T2CNT1
	.globl _T2CNT0
	.globl _T2CLKSRC
	.globl _T1STATUS
	.globl _T1PERIOD
	.globl _T1PERIOD1
	.globl _T1PERIOD0
	.globl _T1MODE
	.globl _T1CNT
	.globl _T1CNT1
	.globl _T1CNT0
	.globl _T1CLKSRC
	.globl _T0STATUS
	.globl _T0PERIOD
	.globl _T0PERIOD1
	.globl _T0PERIOD0
	.globl _T0MODE
	.globl _T0CNT
	.globl _T0CNT1
	.globl _T0CNT0
	.globl _T0CLKSRC
	.globl _SPSTATUS
	.globl _SPSHREG
	.globl _SPMODE
	.globl _SPCLKSRC
	.globl _RADIOSTAT
	.globl _RADIOSTAT1
	.globl _RADIOSTAT0
	.globl _RADIODATA
	.globl _RADIODATA3
	.globl _RADIODATA2
	.globl _RADIODATA1
	.globl _RADIODATA0
	.globl _RADIOADDR
	.globl _RADIOADDR1
	.globl _RADIOADDR0
	.globl _RADIOACC
	.globl _OC1STATUS
	.globl _OC1PIN
	.globl _OC1MODE
	.globl _OC1COMP
	.globl _OC1COMP1
	.globl _OC1COMP0
	.globl _OC0STATUS
	.globl _OC0PIN
	.globl _OC0MODE
	.globl _OC0COMP
	.globl _OC0COMP1
	.globl _OC0COMP0
	.globl _NVSTATUS
	.globl _NVKEY
	.globl _NVDATA
	.globl _NVDATA1
	.globl _NVDATA0
	.globl _NVADDR
	.globl _NVADDR1
	.globl _NVADDR0
	.globl _IC1STATUS
	.globl _IC1MODE
	.globl _IC1CAPT
	.globl _IC1CAPT1
	.globl _IC1CAPT0
	.globl _IC0STATUS
	.globl _IC0MODE
	.globl _IC0CAPT
	.globl _IC0CAPT1
	.globl _IC0CAPT0
	.globl _PORTR
	.globl _PORTC
	.globl _PORTB
	.globl _PORTA
	.globl _PINR
	.globl _PINC
	.globl _PINB
	.globl _PINA
	.globl _DIRR
	.globl _DIRC
	.globl _DIRB
	.globl _DIRA
	.globl _DBGLNKSTAT
	.globl _DBGLNKBUF
	.globl _CODECONFIG
	.globl _CLKSTAT
	.globl _CLKCON
	.globl _ANALOGCOMP
	.globl _ADCCONV
	.globl _ADCCLKSRC
	.globl _ADCCH3CONFIG
	.globl _ADCCH2CONFIG
	.globl _ADCCH1CONFIG
	.globl _ADCCH0CONFIG
	.globl __XPAGE
	.globl _XPAGE
	.globl _SP
	.globl _PSW
	.globl _PCON
	.globl _IP
	.globl _IE
	.globl _EIP
	.globl _EIE
	.globl _E2IP
	.globl _E2IE
	.globl _DPS
	.globl _DPTR1
	.globl _DPTR0
	.globl _DPL1
	.globl _DPL
	.globl _DPH1
	.globl _DPH
	.globl _B
	.globl _ACC
	.globl _RNGCLKSRC1
	.globl _RNGCLKSRC0
	.globl _RNGMODE
	.globl _AESOUTADDR
	.globl _AESOUTADDR1
	.globl _AESOUTADDR0
	.globl _AESMODE
	.globl _AESKEYADDR
	.globl _AESKEYADDR1
	.globl _AESKEYADDR0
	.globl _AESINADDR
	.globl _AESINADDR1
	.globl _AESINADDR0
	.globl _AESCURBLOCK
	.globl _AESCONFIG
	.globl _RNGBYTE
	.globl _XTALREADY
	.globl _XTALOSC
	.globl _XTALAMPL
	.globl _SILICONREV
	.globl _SCRATCH3
	.globl _SCRATCH2
	.globl _SCRATCH1
	.globl _SCRATCH0
	.globl _RADIOMUX
	.globl _RADIOFSTATADDR
	.globl _RADIOFSTATADDR1
	.globl _RADIOFSTATADDR0
	.globl _RADIOFDATAADDR
	.globl _RADIOFDATAADDR1
	.globl _RADIOFDATAADDR0
	.globl _OSCRUN
	.globl _OSCREADY
	.globl _OSCFORCERUN
	.globl _OSCCALIB
	.globl _MISCCTRL
	.globl _LPXOSCGM
	.globl _LPOSCREF
	.globl _LPOSCREF1
	.globl _LPOSCREF0
	.globl _LPOSCPER
	.globl _LPOSCPER1
	.globl _LPOSCPER0
	.globl _LPOSCKFILT
	.globl _LPOSCKFILT1
	.globl _LPOSCKFILT0
	.globl _LPOSCFREQ
	.globl _LPOSCFREQ1
	.globl _LPOSCFREQ0
	.globl _LPOSCCONFIG
	.globl _PINSEL
	.globl _PINCHGC
	.globl _PINCHGB
	.globl _PINCHGA
	.globl _PALTRADIO
	.globl _PALTC
	.globl _PALTB
	.globl _PALTA
	.globl _INTCHGC
	.globl _INTCHGB
	.globl _INTCHGA
	.globl _EXTIRQ
	.globl _GPIOENABLE
	.globl _ANALOGA
	.globl _FRCOSCREF
	.globl _FRCOSCREF1
	.globl _FRCOSCREF0
	.globl _FRCOSCPER
	.globl _FRCOSCPER1
	.globl _FRCOSCPER0
	.globl _FRCOSCKFILT
	.globl _FRCOSCKFILT1
	.globl _FRCOSCKFILT0
	.globl _FRCOSCFREQ
	.globl _FRCOSCFREQ1
	.globl _FRCOSCFREQ0
	.globl _FRCOSCCTRL
	.globl _FRCOSCCONFIG
	.globl _DMA1CONFIG
	.globl _DMA1ADDR
	.globl _DMA1ADDR1
	.globl _DMA1ADDR0
	.globl _DMA0CONFIG
	.globl _DMA0ADDR
	.globl _DMA0ADDR1
	.globl _DMA0ADDR0
	.globl _ADCTUNE2
	.globl _ADCTUNE1
	.globl _ADCTUNE0
	.globl _ADCCH3VAL
	.globl _ADCCH3VAL1
	.globl _ADCCH3VAL0
	.globl _ADCCH2VAL
	.globl _ADCCH2VAL1
	.globl _ADCCH2VAL0
	.globl _ADCCH1VAL
	.globl _ADCCH1VAL1
	.globl _ADCCH1VAL0
	.globl _ADCCH0VAL
	.globl _ADCCH0VAL1
	.globl _ADCCH0VAL0
	.globl _GOLDSEQUENCE_Init
	.globl _GOLDSEQUENCE_Obtain
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
_ACC	=	0x00e0
_B	=	0x00f0
_DPH	=	0x0083
_DPH1	=	0x0085
_DPL	=	0x0082
_DPL1	=	0x0084
_DPTR0	=	0x8382
_DPTR1	=	0x8584
_DPS	=	0x0086
_E2IE	=	0x00a0
_E2IP	=	0x00c0
_EIE	=	0x0098
_EIP	=	0x00b0
_IE	=	0x00a8
_IP	=	0x00b8
_PCON	=	0x0087
_PSW	=	0x00d0
_SP	=	0x0081
_XPAGE	=	0x00d9
__XPAGE	=	0x00d9
_ADCCH0CONFIG	=	0x00ca
_ADCCH1CONFIG	=	0x00cb
_ADCCH2CONFIG	=	0x00d2
_ADCCH3CONFIG	=	0x00d3
_ADCCLKSRC	=	0x00d1
_ADCCONV	=	0x00c9
_ANALOGCOMP	=	0x00e1
_CLKCON	=	0x00c6
_CLKSTAT	=	0x00c7
_CODECONFIG	=	0x0097
_DBGLNKBUF	=	0x00e3
_DBGLNKSTAT	=	0x00e2
_DIRA	=	0x0089
_DIRB	=	0x008a
_DIRC	=	0x008b
_DIRR	=	0x008e
_PINA	=	0x00c8
_PINB	=	0x00e8
_PINC	=	0x00f8
_PINR	=	0x008d
_PORTA	=	0x0080
_PORTB	=	0x0088
_PORTC	=	0x0090
_PORTR	=	0x008c
_IC0CAPT0	=	0x00ce
_IC0CAPT1	=	0x00cf
_IC0CAPT	=	0xcfce
_IC0MODE	=	0x00cc
_IC0STATUS	=	0x00cd
_IC1CAPT0	=	0x00d6
_IC1CAPT1	=	0x00d7
_IC1CAPT	=	0xd7d6
_IC1MODE	=	0x00d4
_IC1STATUS	=	0x00d5
_NVADDR0	=	0x0092
_NVADDR1	=	0x0093
_NVADDR	=	0x9392
_NVDATA0	=	0x0094
_NVDATA1	=	0x0095
_NVDATA	=	0x9594
_NVKEY	=	0x0096
_NVSTATUS	=	0x0091
_OC0COMP0	=	0x00bc
_OC0COMP1	=	0x00bd
_OC0COMP	=	0xbdbc
_OC0MODE	=	0x00b9
_OC0PIN	=	0x00ba
_OC0STATUS	=	0x00bb
_OC1COMP0	=	0x00c4
_OC1COMP1	=	0x00c5
_OC1COMP	=	0xc5c4
_OC1MODE	=	0x00c1
_OC1PIN	=	0x00c2
_OC1STATUS	=	0x00c3
_RADIOACC	=	0x00b1
_RADIOADDR0	=	0x00b3
_RADIOADDR1	=	0x00b2
_RADIOADDR	=	0xb2b3
_RADIODATA0	=	0x00b7
_RADIODATA1	=	0x00b6
_RADIODATA2	=	0x00b5
_RADIODATA3	=	0x00b4
_RADIODATA	=	0xb4b5b6b7
_RADIOSTAT0	=	0x00be
_RADIOSTAT1	=	0x00bf
_RADIOSTAT	=	0xbfbe
_SPCLKSRC	=	0x00df
_SPMODE	=	0x00dc
_SPSHREG	=	0x00de
_SPSTATUS	=	0x00dd
_T0CLKSRC	=	0x009a
_T0CNT0	=	0x009c
_T0CNT1	=	0x009d
_T0CNT	=	0x9d9c
_T0MODE	=	0x0099
_T0PERIOD0	=	0x009e
_T0PERIOD1	=	0x009f
_T0PERIOD	=	0x9f9e
_T0STATUS	=	0x009b
_T1CLKSRC	=	0x00a2
_T1CNT0	=	0x00a4
_T1CNT1	=	0x00a5
_T1CNT	=	0xa5a4
_T1MODE	=	0x00a1
_T1PERIOD0	=	0x00a6
_T1PERIOD1	=	0x00a7
_T1PERIOD	=	0xa7a6
_T1STATUS	=	0x00a3
_T2CLKSRC	=	0x00aa
_T2CNT0	=	0x00ac
_T2CNT1	=	0x00ad
_T2CNT	=	0xadac
_T2MODE	=	0x00a9
_T2PERIOD0	=	0x00ae
_T2PERIOD1	=	0x00af
_T2PERIOD	=	0xafae
_T2STATUS	=	0x00ab
_U0CTRL	=	0x00e4
_U0MODE	=	0x00e7
_U0SHREG	=	0x00e6
_U0STATUS	=	0x00e5
_U1CTRL	=	0x00ec
_U1MODE	=	0x00ef
_U1SHREG	=	0x00ee
_U1STATUS	=	0x00ed
_WDTCFG	=	0x00da
_WDTRESET	=	0x00db
_WTCFGA	=	0x00f1
_WTCFGB	=	0x00f9
_WTCNTA0	=	0x00f2
_WTCNTA1	=	0x00f3
_WTCNTA	=	0xf3f2
_WTCNTB0	=	0x00fa
_WTCNTB1	=	0x00fb
_WTCNTB	=	0xfbfa
_WTCNTR1	=	0x00eb
_WTEVTA0	=	0x00f4
_WTEVTA1	=	0x00f5
_WTEVTA	=	0xf5f4
_WTEVTB0	=	0x00f6
_WTEVTB1	=	0x00f7
_WTEVTB	=	0xf7f6
_WTEVTC0	=	0x00fc
_WTEVTC1	=	0x00fd
_WTEVTC	=	0xfdfc
_WTEVTD0	=	0x00fe
_WTEVTD1	=	0x00ff
_WTEVTD	=	0xfffe
_WTIRQEN	=	0x00e9
_WTSTAT	=	0x00ea
;--------------------------------------------------------
; special function bits
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
_ACC_0	=	0x00e0
_ACC_1	=	0x00e1
_ACC_2	=	0x00e2
_ACC_3	=	0x00e3
_ACC_4	=	0x00e4
_ACC_5	=	0x00e5
_ACC_6	=	0x00e6
_ACC_7	=	0x00e7
_B_0	=	0x00f0
_B_1	=	0x00f1
_B_2	=	0x00f2
_B_3	=	0x00f3
_B_4	=	0x00f4
_B_5	=	0x00f5
_B_6	=	0x00f6
_B_7	=	0x00f7
_E2IE_0	=	0x00a0
_E2IE_1	=	0x00a1
_E2IE_2	=	0x00a2
_E2IE_3	=	0x00a3
_E2IE_4	=	0x00a4
_E2IE_5	=	0x00a5
_E2IE_6	=	0x00a6
_E2IE_7	=	0x00a7
_E2IP_0	=	0x00c0
_E2IP_1	=	0x00c1
_E2IP_2	=	0x00c2
_E2IP_3	=	0x00c3
_E2IP_4	=	0x00c4
_E2IP_5	=	0x00c5
_E2IP_6	=	0x00c6
_E2IP_7	=	0x00c7
_EIE_0	=	0x0098
_EIE_1	=	0x0099
_EIE_2	=	0x009a
_EIE_3	=	0x009b
_EIE_4	=	0x009c
_EIE_5	=	0x009d
_EIE_6	=	0x009e
_EIE_7	=	0x009f
_EIP_0	=	0x00b0
_EIP_1	=	0x00b1
_EIP_2	=	0x00b2
_EIP_3	=	0x00b3
_EIP_4	=	0x00b4
_EIP_5	=	0x00b5
_EIP_6	=	0x00b6
_EIP_7	=	0x00b7
_IE_0	=	0x00a8
_IE_1	=	0x00a9
_IE_2	=	0x00aa
_IE_3	=	0x00ab
_IE_4	=	0x00ac
_IE_5	=	0x00ad
_IE_6	=	0x00ae
_IE_7	=	0x00af
_EA	=	0x00af
_IP_0	=	0x00b8
_IP_1	=	0x00b9
_IP_2	=	0x00ba
_IP_3	=	0x00bb
_IP_4	=	0x00bc
_IP_5	=	0x00bd
_IP_6	=	0x00be
_IP_7	=	0x00bf
_P	=	0x00d0
_F1	=	0x00d1
_OV	=	0x00d2
_RS0	=	0x00d3
_RS1	=	0x00d4
_F0	=	0x00d5
_AC	=	0x00d6
_CY	=	0x00d7
_PINA_0	=	0x00c8
_PINA_1	=	0x00c9
_PINA_2	=	0x00ca
_PINA_3	=	0x00cb
_PINA_4	=	0x00cc
_PINA_5	=	0x00cd
_PINA_6	=	0x00ce
_PINA_7	=	0x00cf
_PINB_0	=	0x00e8
_PINB_1	=	0x00e9
_PINB_2	=	0x00ea
_PINB_3	=	0x00eb
_PINB_4	=	0x00ec
_PINB_5	=	0x00ed
_PINB_6	=	0x00ee
_PINB_7	=	0x00ef
_PINC_0	=	0x00f8
_PINC_1	=	0x00f9
_PINC_2	=	0x00fa
_PINC_3	=	0x00fb
_PINC_4	=	0x00fc
_PINC_5	=	0x00fd
_PINC_6	=	0x00fe
_PINC_7	=	0x00ff
_PORTA_0	=	0x0080
_PORTA_1	=	0x0081
_PORTA_2	=	0x0082
_PORTA_3	=	0x0083
_PORTA_4	=	0x0084
_PORTA_5	=	0x0085
_PORTA_6	=	0x0086
_PORTA_7	=	0x0087
_PORTB_0	=	0x0088
_PORTB_1	=	0x0089
_PORTB_2	=	0x008a
_PORTB_3	=	0x008b
_PORTB_4	=	0x008c
_PORTB_5	=	0x008d
_PORTB_6	=	0x008e
_PORTB_7	=	0x008f
_PORTC_0	=	0x0090
_PORTC_1	=	0x0091
_PORTC_2	=	0x0092
_PORTC_3	=	0x0093
_PORTC_4	=	0x0094
_PORTC_5	=	0x0095
_PORTC_6	=	0x0096
_PORTC_7	=	0x0097
;--------------------------------------------------------
; overlayable register banks
;--------------------------------------------------------
	.area REG_BANK_0	(REL,OVR,DATA)
	.ds 8
;--------------------------------------------------------
; internal ram data
;--------------------------------------------------------
	.area DSEG    (DATA)
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;--------------------------------------------------------
; indirectly addressable internal ram data
;--------------------------------------------------------
	.area ISEG    (DATA)
;--------------------------------------------------------
; absolute internal ram data
;--------------------------------------------------------
	.area IABS    (ABS,DATA)
	.area IABS    (ABS,DATA)
;--------------------------------------------------------
; bit data
;--------------------------------------------------------
	.area BSEG    (BIT)
;--------------------------------------------------------
; paged external ram data
;--------------------------------------------------------
	.area PSEG    (PAG,XDATA)
;--------------------------------------------------------
; external ram data
;--------------------------------------------------------
	.area XSEG    (XDATA)
_flash_deviceid	=	0xfc06
_ADCCH0VAL0	=	0x7020
_ADCCH0VAL1	=	0x7021
_ADCCH0VAL	=	0x7020
_ADCCH1VAL0	=	0x7022
_ADCCH1VAL1	=	0x7023
_ADCCH1VAL	=	0x7022
_ADCCH2VAL0	=	0x7024
_ADCCH2VAL1	=	0x7025
_ADCCH2VAL	=	0x7024
_ADCCH3VAL0	=	0x7026
_ADCCH3VAL1	=	0x7027
_ADCCH3VAL	=	0x7026
_ADCTUNE0	=	0x7028
_ADCTUNE1	=	0x7029
_ADCTUNE2	=	0x702a
_DMA0ADDR0	=	0x7010
_DMA0ADDR1	=	0x7011
_DMA0ADDR	=	0x7010
_DMA0CONFIG	=	0x7014
_DMA1ADDR0	=	0x7012
_DMA1ADDR1	=	0x7013
_DMA1ADDR	=	0x7012
_DMA1CONFIG	=	0x7015
_FRCOSCCONFIG	=	0x7070
_FRCOSCCTRL	=	0x7071
_FRCOSCFREQ0	=	0x7076
_FRCOSCFREQ1	=	0x7077
_FRCOSCFREQ	=	0x7076
_FRCOSCKFILT0	=	0x7072
_FRCOSCKFILT1	=	0x7073
_FRCOSCKFILT	=	0x7072
_FRCOSCPER0	=	0x7078
_FRCOSCPER1	=	0x7079
_FRCOSCPER	=	0x7078
_FRCOSCREF0	=	0x7074
_FRCOSCREF1	=	0x7075
_FRCOSCREF	=	0x7074
_ANALOGA	=	0x7007
_GPIOENABLE	=	0x700c
_EXTIRQ	=	0x7003
_INTCHGA	=	0x7000
_INTCHGB	=	0x7001
_INTCHGC	=	0x7002
_PALTA	=	0x7008
_PALTB	=	0x7009
_PALTC	=	0x700a
_PALTRADIO	=	0x7046
_PINCHGA	=	0x7004
_PINCHGB	=	0x7005
_PINCHGC	=	0x7006
_PINSEL	=	0x700b
_LPOSCCONFIG	=	0x7060
_LPOSCFREQ0	=	0x7066
_LPOSCFREQ1	=	0x7067
_LPOSCFREQ	=	0x7066
_LPOSCKFILT0	=	0x7062
_LPOSCKFILT1	=	0x7063
_LPOSCKFILT	=	0x7062
_LPOSCPER0	=	0x7068
_LPOSCPER1	=	0x7069
_LPOSCPER	=	0x7068
_LPOSCREF0	=	0x7064
_LPOSCREF1	=	0x7065
_LPOSCREF	=	0x7064
_LPXOSCGM	=	0x7054
_MISCCTRL	=	0x7f01
_OSCCALIB	=	0x7053
_OSCFORCERUN	=	0x7050
_OSCREADY	=	0x7052
_OSCRUN	=	0x7051
_RADIOFDATAADDR0	=	0x7040
_RADIOFDATAADDR1	=	0x7041
_RADIOFDATAADDR	=	0x7040
_RADIOFSTATADDR0	=	0x7042
_RADIOFSTATADDR1	=	0x7043
_RADIOFSTATADDR	=	0x7042
_RADIOMUX	=	0x7044
_SCRATCH0	=	0x7084
_SCRATCH1	=	0x7085
_SCRATCH2	=	0x7086
_SCRATCH3	=	0x7087
_SILICONREV	=	0x7f00
_XTALAMPL	=	0x7f19
_XTALOSC	=	0x7f18
_XTALREADY	=	0x7f1a
_RNGBYTE	=	0x7081
_AESCONFIG	=	0x7091
_AESCURBLOCK	=	0x7098
_AESINADDR0	=	0x7094
_AESINADDR1	=	0x7095
_AESINADDR	=	0x7094
_AESKEYADDR0	=	0x7092
_AESKEYADDR1	=	0x7093
_AESKEYADDR	=	0x7092
_AESMODE	=	0x7090
_AESOUTADDR0	=	0x7096
_AESOUTADDR1	=	0x7097
_AESOUTADDR	=	0x7096
_RNGMODE	=	0x7080
_RNGCLKSRC0	=	0x7082
_RNGCLKSRC1	=	0x7083
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area XABS    (ABS,XDATA)
;--------------------------------------------------------
; external initialized ram data
;--------------------------------------------------------
	.area XISEG   (XDATA)
	.area HOME    (CODE)
	.area GSINIT0 (CODE)
	.area GSINIT1 (CODE)
	.area GSINIT2 (CODE)
	.area GSINIT3 (CODE)
	.area GSINIT4 (CODE)
	.area GSINIT5 (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area CSEG    (CODE)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME    (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area GSINIT  (CODE)
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME    (CODE)
	.area HOME    (CODE)
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CSEG    (CODE)
;------------------------------------------------------------
;Allocation info for local variables in function 'GOLDSEQUENCE_Init'
;------------------------------------------------------------
;	..\src\peripherals\GoldSequence.c:26: __xdata void GOLDSEQUENCE_Init(void)
;	-----------------------------------------
;	 function GOLDSEQUENCE_Init
;	-----------------------------------------
_GOLDSEQUENCE_Init:
	ar7 = 0x07
	ar6 = 0x06
	ar5 = 0x05
	ar4 = 0x04
	ar3 = 0x03
	ar2 = 0x02
	ar1 = 0x01
	ar0 = 0x00
;	..\src\peripherals\GoldSequence.c:28: flash_unlock();
	lcall	_flash_unlock
;	..\src\peripherals\GoldSequence.c:29: EraseMemoryPage(MEM_GOLD_SEQUENCES_START+1);
	mov	dptr,#0x1c01
	lcall	_EraseMemoryPage
;	..\src\peripherals\GoldSequence.c:30: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0000,0b1111100110100100,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0xa4
	movx	@dptr,a
	mov	a,#0xf9
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c00
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:31: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0002,0b001010111011000,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0xd8
	movx	@dptr,a
	mov	a,#0x15
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c02
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:32: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0004,0b1111100100110000,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x30
	movx	@dptr,a
	mov	a,#0xf9
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c04
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:33: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0006,0b101101010001110,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x8e
	movx	@dptr,a
	mov	a,#0x5a
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c06
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:34: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0008,0b0000000010010100,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x94
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	movx	@dptr,a
	mov	dptr,#0x1c08
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:35: WriteMemory(MEM_GOLD_SEQUENCES_START+0x000A,0b100111101010110,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x56
	movx	@dptr,a
	mov	a,#0x4f
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c0a
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:36: WriteMemory(MEM_GOLD_SEQUENCES_START+0x000C,0b1000010100111100,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x3c
	movx	@dptr,a
	mov	a,#0x85
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c0c
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:37: WriteMemory(MEM_GOLD_SEQUENCES_START+0x000E,0b011100010011111,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x9f
	movx	@dptr,a
	mov	a,#0x38
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c0e
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:38: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0010,0b0100011111101000,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0xe8
	movx	@dptr,a
	mov	a,#0x47
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c10
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:39: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0012,0b000001101111011,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x7b
	movx	@dptr,a
	mov	a,#0x03
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c12
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:40: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0014,0b0010011010000010,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x82
	movx	@dptr,a
	mov	a,#0x26
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c14
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:41: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0016,0b001111010001001,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x89
	movx	@dptr,a
	mov	a,#0x1e
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c16
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:42: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0018,0b0001011000110111,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x37
	movx	@dptr,a
	mov	a,#0x16
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c18
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:43: WriteMemory(MEM_GOLD_SEQUENCES_START+0x001A,0b001000001110000,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x70
	movx	@dptr,a
	mov	a,#0x10
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c1a
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:44: WriteMemory(MEM_GOLD_SEQUENCES_START+0x001C,0b1000111001101101,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x6d
	movx	@dptr,a
	mov	a,#0x8e
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c1c
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:45: WriteMemory(MEM_GOLD_SEQUENCES_START+0x001E,0b101011100001100,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x0c
	movx	@dptr,a
	mov	a,#0x57
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c1e
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:46: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0020,0b1100001001000000,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x40
	movx	@dptr,a
	mov	a,#0xc2
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c20
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:47: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0022,0b111010010110010,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0xb2
	movx	@dptr,a
	mov	a,#0x74
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c22
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:48: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0024,0b1110010001010110,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x56
	movx	@dptr,a
	mov	a,#0xe4
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c24
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:49: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0026,0b010010101101101,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x6d
	movx	@dptr,a
	mov	a,#0x25
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c26
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:50: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0028,0b0111011101011101,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x5d
	movx	@dptr,a
	mov	a,#0x77
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c28
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:51: WriteMemory(MEM_GOLD_SEQUENCES_START+0x002A,0b000110110000010,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x82
	movx	@dptr,a
	mov	a,#0x0d
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c2a
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:52: WriteMemory(MEM_GOLD_SEQUENCES_START+0x002C,0b1011111011011000,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0xd8
	movx	@dptr,a
	mov	a,#0xbe
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c2c
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:53: WriteMemory(MEM_GOLD_SEQUENCES_START+0x002E,0b101100111110101,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0xf5
	movx	@dptr,a
	mov	a,#0x59
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c2e
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:54: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0030,0b0101101000011010,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x1a
	movx	@dptr,a
	mov	a,#0x5a
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c30
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:55: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0032,0b011001111001110,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0xce
	movx	@dptr,a
	mov	a,#0x33
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c32
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:56: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0034,0b1010100001111011,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x7b
	movx	@dptr,a
	mov	a,#0xa8
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c34
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:57: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0036,0b000011011010011,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0xd3
	movx	@dptr,a
	mov	a,#0x06
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c36
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:58: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0038,0b0101000101001011,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x4b
	movx	@dptr,a
	mov	a,#0x51
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c38
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:59: WriteMemory(MEM_GOLD_SEQUENCES_START+0x003A,0b101110001011101,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x5d
	movx	@dptr,a
	dec	a
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c3a
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:60: WriteMemory(MEM_GOLD_SEQUENCES_START+0x003C,0b0010110111010011,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0xd3
	movx	@dptr,a
	mov	a,#0x2d
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c3c
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:61: WriteMemory(MEM_GOLD_SEQUENCES_START+0x003E,0b111000100011010,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x1a
	movx	@dptr,a
	mov	a,#0x71
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c3e
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:62: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0040,0b1001001110011111,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x9f
	movx	@dptr,a
	mov	a,#0x93
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c40
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:63: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0042,0b110011110111001,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0xb9
	movx	@dptr,a
	mov	a,#0x67
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c42
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:64: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0044,0b0100110010111001,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0xb9
	movx	@dptr,a
	mov	a,#0x4c
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c44
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:65: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0046,0b110110011101000,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0xe8
	movx	@dptr,a
	mov	a,#0x6c
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c46
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:66: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0048,0b1010001100101010,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x2a
	movx	@dptr,a
	mov	a,#0xa3
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c48
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:67: WriteMemory(MEM_GOLD_SEQUENCES_START+0x004A,0b110100101000000,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x40
	movx	@dptr,a
	mov	a,#0x69
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c4a
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:68: WriteMemory(MEM_GOLD_SEQUENCES_START+0x004C,0b1101010011100011,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0xe3
	movx	@dptr,a
	mov	a,#0xd4
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c4c
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:69: WriteMemory(MEM_GOLD_SEQUENCES_START+0x004E,0b010101110010100,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x94
	movx	@dptr,a
	mov	a,#0x2b
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c4e
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:70: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0050,0b1110111100000111,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x07
	movx	@dptr,a
	mov	a,#0xef
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c50
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:71: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0052,0b100101011111110,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0xfe
	movx	@dptr,a
	mov	a,#0x4a
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c52
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:72: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0054,0b1111001011110101,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0xf5
	movx	@dptr,a
	mov	a,#0xf2
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c54
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:73: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0056,0b111101001001011,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x4b
	movx	@dptr,a
	mov	a,#0x7a
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c56
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:74: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0058,0b0111110000001100,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x0c
	movx	@dptr,a
	mov	a,#0x7c
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c58
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:75: WriteMemory(MEM_GOLD_SEQUENCES_START+0x005A,0b110001000010001,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x11
	movx	@dptr,a
	mov	a,#0x62
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c5a
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:76: WriteMemory(MEM_GOLD_SEQUENCES_START+0x005C,0b0011101101110000,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x70
	movx	@dptr,a
	mov	a,#0x3b
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c5c
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:77: WriteMemory(MEM_GOLD_SEQUENCES_START+0x005E,0b010111000111100,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x3c
	movx	@dptr,a
	mov	a,#0x2e
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c5e
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:78: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0060,0b1001100011001110,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0xce
	movx	@dptr,a
	mov	a,#0x98
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c60
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:79: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0062,0b000100000101010,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x2a
	movx	@dptr,a
	mov	a,#0x08
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c62
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:80: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0064,0b1100100100010001,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x11
	movx	@dptr,a
	mov	a,#0xc9
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c64
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:81: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0066,0b001101100100001,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x21
	movx	@dptr,a
	mov	a,#0x1b
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c66
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:82: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0068,0b0110000111111110,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0xfe
	movx	@dptr,a
	mov	a,#0x61
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c68
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:83: WriteMemory(MEM_GOLD_SEQUENCES_START+0x006A,0b101001010100100,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0xa4
	movx	@dptr,a
	rr	a
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c6a
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:84: WriteMemory(MEM_GOLD_SEQUENCES_START+0x006C,0b1011010110001001,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x89
	movx	@dptr,a
	mov	a,#0xb5
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c6c
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:85: WriteMemory(MEM_GOLD_SEQUENCES_START+0x006E,0b011011001100110,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x66
	movx	@dptr,a
	mov	a,#0x36
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c6e
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:86: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0070,0b1101111110110010,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0xb2
	movx	@dptr,a
	mov	a,#0xdf
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c70
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:87: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0072,0b100010000000111,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x07
	movx	@dptr,a
	mov	a,#0x44
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c72
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:88: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0074,0b0110101010101111,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0xaf
	movx	@dptr,a
	mov	a,#0x6a
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c74
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:89: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0076,0b011110100110111,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x37
	movx	@dptr,a
	mov	a,#0x3d
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c76
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:90: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0078,0b0011000000100001,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x21
	movx	@dptr,a
	mov	a,#0x30
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c78
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:91: WriteMemory(MEM_GOLD_SEQUENCES_START+0x007A,0b100000110101111,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0xaf
	movx	@dptr,a
	mov	a,#0x41
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c7a
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:92: WriteMemory(MEM_GOLD_SEQUENCES_START+0x007C,0b0001110101100110,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0x66
	movx	@dptr,a
	mov	a,#0x1d
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c7c
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:93: WriteMemory(MEM_GOLD_SEQUENCES_START+0x007E,0b111111111100011,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0xe3
	movx	@dptr,a
	mov	a,#0x7f
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c7e
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:94: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0080,0b0000101111000101,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0xc5
	movx	@dptr,a
	mov	a,#0x0b
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c80
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:95: WriteMemory(MEM_GOLD_SEQUENCES_START+0x0082,0b010000011000101,FALSE);
	mov	dptr,#_WriteMemory_PARM_2
	mov	a,#0xc5
	movx	@dptr,a
	mov	a,#0x20
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_WriteMemory_PARM_3
	clr	a
	movx	@dptr,a
	mov	dptr,#0x1c82
	lcall	_WriteMemory
;	..\src\peripherals\GoldSequence.c:96: flash_lock();
;	..\src\peripherals\GoldSequence.c:99: }
	ljmp	_flash_lock
;------------------------------------------------------------
;Allocation info for local variables in function 'GOLDSEQUENCE_Obtain'
;------------------------------------------------------------
;Rnd                       Allocated with name '_GOLDSEQUENCE_Obtain_Rnd_65536_132'
;RetVal                    Allocated with name '_GOLDSEQUENCE_Obtain_RetVal_65536_132'
;------------------------------------------------------------
;	..\src\peripherals\GoldSequence.c:108: __xdata uint32_t GOLDSEQUENCE_Obtain(void)
;	-----------------------------------------
;	 function GOLDSEQUENCE_Obtain
;	-----------------------------------------
_GOLDSEQUENCE_Obtain:
;	..\src\peripherals\GoldSequence.c:113: Rnd = RNGBYTE;
	mov	dptr,#_RNGBYTE
	movx	a,@dptr
	mov	r7,a
	mov	r6,#0x00
;	..\src\peripherals\GoldSequence.c:114: flash_unlock();
	push	ar7
	push	ar6
	lcall	_flash_unlock
	pop	ar6
	pop	ar7
;	..\src\peripherals\GoldSequence.c:115: Rnd = ((Rnd % 33)*4);  // MOD operation is done so that the number is between 0 and 33, once a number between 0 and 32 has been obtained, its multiplied by 4 to match the memory address
	mov	dptr,#__moduint_PARM_2
	mov	a,#0x21
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
	mov	dpl,r7
	mov	dph,r6
	lcall	__moduint
	mov	r6,dpl
	mov	r7,dph
	mov	a,r6
	add	a,r6
	mov	r6,a
	mov	a,r7
	rlc	a
	mov	r7,a
	mov	a,r6
	add	a,r6
	mov	r6,a
	mov	a,r7
	rlc	a
	mov	r7,a
;	..\src\peripherals\GoldSequence.c:116: RetVal = ((uint32_t)flash_read(MEM_GOLD_SEQUENCES_START+Rnd))<<16;
	mov	ar4,r6
	mov	a,#0x1c
	add	a,r7
	mov	r5,a
	mov	dpl,r4
	mov	dph,r5
	push	ar7
	push	ar6
	lcall	_flash_read
	mov	r4,dpl
	mov	r5,dph
	pop	ar6
	pop	ar7
	mov	ar2,r5
	mov	ar3,r4
;	..\src\peripherals\GoldSequence.c:117: RetVal |= (((uint32_t)flash_read(MEM_GOLD_SEQUENCES_START+Rnd+2)) & 0x0000FFFF)<<1;
	clr	a
	mov	r4,a
	mov	r5,a
	mov	a,#0x02
	add	a,r6
	mov	r6,a
	mov	a,#0x1c
	addc	a,r7
	mov	r7,a
	mov	dpl,r6
	mov	dph,r7
	push	ar5
	push	ar4
	push	ar3
	push	ar2
	lcall	_flash_read
	mov	r6,dpl
	mov	r7,dph
	pop	ar2
	pop	ar3
	pop	ar4
	pop	ar5
	mov	ar0,r6
	mov	ar1,r7
	clr	a
	mov	r6,a
	mov	r7,a
	mov	a,r0
	add	a,r0
	mov	r0,a
	mov	a,r1
	rlc	a
	mov	r1,a
	mov	a,r6
	rlc	a
	mov	r6,a
	mov	a,r7
	rlc	a
	mov	r7,a
	mov	a,r0
	orl	ar4,a
	mov	a,r1
	orl	ar5,a
	mov	a,r6
	orl	ar3,a
	mov	a,r7
	orl	ar2,a
;	..\src\peripherals\GoldSequence.c:118: flash_lock();
	push	ar5
	push	ar4
	push	ar3
	push	ar2
	lcall	_flash_lock
	pop	ar2
	pop	ar3
	pop	ar4
	pop	ar5
;	..\src\peripherals\GoldSequence.c:119: return(RetVal);
	mov	dpl,r4
	mov	dph,r5
	mov	b,r3
	mov	a,r2
;	..\src\peripherals\GoldSequence.c:120: }
	ret
	.area CSEG    (CODE)
	.area CONST   (CODE)
	.area XINIT   (CODE)
	.area CABS    (ABS,CODE)
