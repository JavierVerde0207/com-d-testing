                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.9.3 #11354 (MINGW32)
                                      4 ;--------------------------------------------------------
                                      5 	.module cryptography
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _aes_sbox
                                     12 	.globl _CRYPTO_EncryptData
                                     13 	.globl _CRYPTO_KeySelectAndExpansion
                                     14 	.globl _CRYPTO_InitRegisters
                                     15 	.globl _dbglink_writestr
                                     16 	.globl _malloc
                                     17 	.globl _delay_ms
                                     18 	.globl _memcpy
                                     19 	.globl _PORTC_7
                                     20 	.globl _PORTC_6
                                     21 	.globl _PORTC_5
                                     22 	.globl _PORTC_4
                                     23 	.globl _PORTC_3
                                     24 	.globl _PORTC_2
                                     25 	.globl _PORTC_1
                                     26 	.globl _PORTC_0
                                     27 	.globl _PORTB_7
                                     28 	.globl _PORTB_6
                                     29 	.globl _PORTB_5
                                     30 	.globl _PORTB_4
                                     31 	.globl _PORTB_3
                                     32 	.globl _PORTB_2
                                     33 	.globl _PORTB_1
                                     34 	.globl _PORTB_0
                                     35 	.globl _PORTA_7
                                     36 	.globl _PORTA_6
                                     37 	.globl _PORTA_5
                                     38 	.globl _PORTA_4
                                     39 	.globl _PORTA_3
                                     40 	.globl _PORTA_2
                                     41 	.globl _PORTA_1
                                     42 	.globl _PORTA_0
                                     43 	.globl _PINC_7
                                     44 	.globl _PINC_6
                                     45 	.globl _PINC_5
                                     46 	.globl _PINC_4
                                     47 	.globl _PINC_3
                                     48 	.globl _PINC_2
                                     49 	.globl _PINC_1
                                     50 	.globl _PINC_0
                                     51 	.globl _PINB_7
                                     52 	.globl _PINB_6
                                     53 	.globl _PINB_5
                                     54 	.globl _PINB_4
                                     55 	.globl _PINB_3
                                     56 	.globl _PINB_2
                                     57 	.globl _PINB_1
                                     58 	.globl _PINB_0
                                     59 	.globl _PINA_7
                                     60 	.globl _PINA_6
                                     61 	.globl _PINA_5
                                     62 	.globl _PINA_4
                                     63 	.globl _PINA_3
                                     64 	.globl _PINA_2
                                     65 	.globl _PINA_1
                                     66 	.globl _PINA_0
                                     67 	.globl _CY
                                     68 	.globl _AC
                                     69 	.globl _F0
                                     70 	.globl _RS1
                                     71 	.globl _RS0
                                     72 	.globl _OV
                                     73 	.globl _F1
                                     74 	.globl _P
                                     75 	.globl _IP_7
                                     76 	.globl _IP_6
                                     77 	.globl _IP_5
                                     78 	.globl _IP_4
                                     79 	.globl _IP_3
                                     80 	.globl _IP_2
                                     81 	.globl _IP_1
                                     82 	.globl _IP_0
                                     83 	.globl _EA
                                     84 	.globl _IE_7
                                     85 	.globl _IE_6
                                     86 	.globl _IE_5
                                     87 	.globl _IE_4
                                     88 	.globl _IE_3
                                     89 	.globl _IE_2
                                     90 	.globl _IE_1
                                     91 	.globl _IE_0
                                     92 	.globl _EIP_7
                                     93 	.globl _EIP_6
                                     94 	.globl _EIP_5
                                     95 	.globl _EIP_4
                                     96 	.globl _EIP_3
                                     97 	.globl _EIP_2
                                     98 	.globl _EIP_1
                                     99 	.globl _EIP_0
                                    100 	.globl _EIE_7
                                    101 	.globl _EIE_6
                                    102 	.globl _EIE_5
                                    103 	.globl _EIE_4
                                    104 	.globl _EIE_3
                                    105 	.globl _EIE_2
                                    106 	.globl _EIE_1
                                    107 	.globl _EIE_0
                                    108 	.globl _E2IP_7
                                    109 	.globl _E2IP_6
                                    110 	.globl _E2IP_5
                                    111 	.globl _E2IP_4
                                    112 	.globl _E2IP_3
                                    113 	.globl _E2IP_2
                                    114 	.globl _E2IP_1
                                    115 	.globl _E2IP_0
                                    116 	.globl _E2IE_7
                                    117 	.globl _E2IE_6
                                    118 	.globl _E2IE_5
                                    119 	.globl _E2IE_4
                                    120 	.globl _E2IE_3
                                    121 	.globl _E2IE_2
                                    122 	.globl _E2IE_1
                                    123 	.globl _E2IE_0
                                    124 	.globl _B_7
                                    125 	.globl _B_6
                                    126 	.globl _B_5
                                    127 	.globl _B_4
                                    128 	.globl _B_3
                                    129 	.globl _B_2
                                    130 	.globl _B_1
                                    131 	.globl _B_0
                                    132 	.globl _ACC_7
                                    133 	.globl _ACC_6
                                    134 	.globl _ACC_5
                                    135 	.globl _ACC_4
                                    136 	.globl _ACC_3
                                    137 	.globl _ACC_2
                                    138 	.globl _ACC_1
                                    139 	.globl _ACC_0
                                    140 	.globl _WTSTAT
                                    141 	.globl _WTIRQEN
                                    142 	.globl _WTEVTD
                                    143 	.globl _WTEVTD1
                                    144 	.globl _WTEVTD0
                                    145 	.globl _WTEVTC
                                    146 	.globl _WTEVTC1
                                    147 	.globl _WTEVTC0
                                    148 	.globl _WTEVTB
                                    149 	.globl _WTEVTB1
                                    150 	.globl _WTEVTB0
                                    151 	.globl _WTEVTA
                                    152 	.globl _WTEVTA1
                                    153 	.globl _WTEVTA0
                                    154 	.globl _WTCNTR1
                                    155 	.globl _WTCNTB
                                    156 	.globl _WTCNTB1
                                    157 	.globl _WTCNTB0
                                    158 	.globl _WTCNTA
                                    159 	.globl _WTCNTA1
                                    160 	.globl _WTCNTA0
                                    161 	.globl _WTCFGB
                                    162 	.globl _WTCFGA
                                    163 	.globl _WDTRESET
                                    164 	.globl _WDTCFG
                                    165 	.globl _U1STATUS
                                    166 	.globl _U1SHREG
                                    167 	.globl _U1MODE
                                    168 	.globl _U1CTRL
                                    169 	.globl _U0STATUS
                                    170 	.globl _U0SHREG
                                    171 	.globl _U0MODE
                                    172 	.globl _U0CTRL
                                    173 	.globl _T2STATUS
                                    174 	.globl _T2PERIOD
                                    175 	.globl _T2PERIOD1
                                    176 	.globl _T2PERIOD0
                                    177 	.globl _T2MODE
                                    178 	.globl _T2CNT
                                    179 	.globl _T2CNT1
                                    180 	.globl _T2CNT0
                                    181 	.globl _T2CLKSRC
                                    182 	.globl _T1STATUS
                                    183 	.globl _T1PERIOD
                                    184 	.globl _T1PERIOD1
                                    185 	.globl _T1PERIOD0
                                    186 	.globl _T1MODE
                                    187 	.globl _T1CNT
                                    188 	.globl _T1CNT1
                                    189 	.globl _T1CNT0
                                    190 	.globl _T1CLKSRC
                                    191 	.globl _T0STATUS
                                    192 	.globl _T0PERIOD
                                    193 	.globl _T0PERIOD1
                                    194 	.globl _T0PERIOD0
                                    195 	.globl _T0MODE
                                    196 	.globl _T0CNT
                                    197 	.globl _T0CNT1
                                    198 	.globl _T0CNT0
                                    199 	.globl _T0CLKSRC
                                    200 	.globl _SPSTATUS
                                    201 	.globl _SPSHREG
                                    202 	.globl _SPMODE
                                    203 	.globl _SPCLKSRC
                                    204 	.globl _RADIOSTAT
                                    205 	.globl _RADIOSTAT1
                                    206 	.globl _RADIOSTAT0
                                    207 	.globl _RADIODATA
                                    208 	.globl _RADIODATA3
                                    209 	.globl _RADIODATA2
                                    210 	.globl _RADIODATA1
                                    211 	.globl _RADIODATA0
                                    212 	.globl _RADIOADDR
                                    213 	.globl _RADIOADDR1
                                    214 	.globl _RADIOADDR0
                                    215 	.globl _RADIOACC
                                    216 	.globl _OC1STATUS
                                    217 	.globl _OC1PIN
                                    218 	.globl _OC1MODE
                                    219 	.globl _OC1COMP
                                    220 	.globl _OC1COMP1
                                    221 	.globl _OC1COMP0
                                    222 	.globl _OC0STATUS
                                    223 	.globl _OC0PIN
                                    224 	.globl _OC0MODE
                                    225 	.globl _OC0COMP
                                    226 	.globl _OC0COMP1
                                    227 	.globl _OC0COMP0
                                    228 	.globl _NVSTATUS
                                    229 	.globl _NVKEY
                                    230 	.globl _NVDATA
                                    231 	.globl _NVDATA1
                                    232 	.globl _NVDATA0
                                    233 	.globl _NVADDR
                                    234 	.globl _NVADDR1
                                    235 	.globl _NVADDR0
                                    236 	.globl _IC1STATUS
                                    237 	.globl _IC1MODE
                                    238 	.globl _IC1CAPT
                                    239 	.globl _IC1CAPT1
                                    240 	.globl _IC1CAPT0
                                    241 	.globl _IC0STATUS
                                    242 	.globl _IC0MODE
                                    243 	.globl _IC0CAPT
                                    244 	.globl _IC0CAPT1
                                    245 	.globl _IC0CAPT0
                                    246 	.globl _PORTR
                                    247 	.globl _PORTC
                                    248 	.globl _PORTB
                                    249 	.globl _PORTA
                                    250 	.globl _PINR
                                    251 	.globl _PINC
                                    252 	.globl _PINB
                                    253 	.globl _PINA
                                    254 	.globl _DIRR
                                    255 	.globl _DIRC
                                    256 	.globl _DIRB
                                    257 	.globl _DIRA
                                    258 	.globl _DBGLNKSTAT
                                    259 	.globl _DBGLNKBUF
                                    260 	.globl _CODECONFIG
                                    261 	.globl _CLKSTAT
                                    262 	.globl _CLKCON
                                    263 	.globl _ANALOGCOMP
                                    264 	.globl _ADCCONV
                                    265 	.globl _ADCCLKSRC
                                    266 	.globl _ADCCH3CONFIG
                                    267 	.globl _ADCCH2CONFIG
                                    268 	.globl _ADCCH1CONFIG
                                    269 	.globl _ADCCH0CONFIG
                                    270 	.globl __XPAGE
                                    271 	.globl _XPAGE
                                    272 	.globl _SP
                                    273 	.globl _PSW
                                    274 	.globl _PCON
                                    275 	.globl _IP
                                    276 	.globl _IE
                                    277 	.globl _EIP
                                    278 	.globl _EIE
                                    279 	.globl _E2IP
                                    280 	.globl _E2IE
                                    281 	.globl _DPS
                                    282 	.globl _DPTR1
                                    283 	.globl _DPTR0
                                    284 	.globl _DPL1
                                    285 	.globl _DPL
                                    286 	.globl _DPH1
                                    287 	.globl _DPH
                                    288 	.globl _B
                                    289 	.globl _ACC
                                    290 	.globl _CRYPTO_InitRegisters_PARM_2
                                    291 	.globl _AX5043_TIMEGAIN3NB
                                    292 	.globl _AX5043_TIMEGAIN2NB
                                    293 	.globl _AX5043_TIMEGAIN1NB
                                    294 	.globl _AX5043_TIMEGAIN0NB
                                    295 	.globl _AX5043_RXPARAMSETSNB
                                    296 	.globl _AX5043_RXPARAMCURSETNB
                                    297 	.globl _AX5043_PKTMAXLENNB
                                    298 	.globl _AX5043_PKTLENOFFSETNB
                                    299 	.globl _AX5043_PKTLENCFGNB
                                    300 	.globl _AX5043_PKTADDRMASK3NB
                                    301 	.globl _AX5043_PKTADDRMASK2NB
                                    302 	.globl _AX5043_PKTADDRMASK1NB
                                    303 	.globl _AX5043_PKTADDRMASK0NB
                                    304 	.globl _AX5043_PKTADDRCFGNB
                                    305 	.globl _AX5043_PKTADDR3NB
                                    306 	.globl _AX5043_PKTADDR2NB
                                    307 	.globl _AX5043_PKTADDR1NB
                                    308 	.globl _AX5043_PKTADDR0NB
                                    309 	.globl _AX5043_PHASEGAIN3NB
                                    310 	.globl _AX5043_PHASEGAIN2NB
                                    311 	.globl _AX5043_PHASEGAIN1NB
                                    312 	.globl _AX5043_PHASEGAIN0NB
                                    313 	.globl _AX5043_FREQUENCYLEAKNB
                                    314 	.globl _AX5043_FREQUENCYGAIND3NB
                                    315 	.globl _AX5043_FREQUENCYGAIND2NB
                                    316 	.globl _AX5043_FREQUENCYGAIND1NB
                                    317 	.globl _AX5043_FREQUENCYGAIND0NB
                                    318 	.globl _AX5043_FREQUENCYGAINC3NB
                                    319 	.globl _AX5043_FREQUENCYGAINC2NB
                                    320 	.globl _AX5043_FREQUENCYGAINC1NB
                                    321 	.globl _AX5043_FREQUENCYGAINC0NB
                                    322 	.globl _AX5043_FREQUENCYGAINB3NB
                                    323 	.globl _AX5043_FREQUENCYGAINB2NB
                                    324 	.globl _AX5043_FREQUENCYGAINB1NB
                                    325 	.globl _AX5043_FREQUENCYGAINB0NB
                                    326 	.globl _AX5043_FREQUENCYGAINA3NB
                                    327 	.globl _AX5043_FREQUENCYGAINA2NB
                                    328 	.globl _AX5043_FREQUENCYGAINA1NB
                                    329 	.globl _AX5043_FREQUENCYGAINA0NB
                                    330 	.globl _AX5043_FREQDEV13NB
                                    331 	.globl _AX5043_FREQDEV12NB
                                    332 	.globl _AX5043_FREQDEV11NB
                                    333 	.globl _AX5043_FREQDEV10NB
                                    334 	.globl _AX5043_FREQDEV03NB
                                    335 	.globl _AX5043_FREQDEV02NB
                                    336 	.globl _AX5043_FREQDEV01NB
                                    337 	.globl _AX5043_FREQDEV00NB
                                    338 	.globl _AX5043_FOURFSK3NB
                                    339 	.globl _AX5043_FOURFSK2NB
                                    340 	.globl _AX5043_FOURFSK1NB
                                    341 	.globl _AX5043_FOURFSK0NB
                                    342 	.globl _AX5043_DRGAIN3NB
                                    343 	.globl _AX5043_DRGAIN2NB
                                    344 	.globl _AX5043_DRGAIN1NB
                                    345 	.globl _AX5043_DRGAIN0NB
                                    346 	.globl _AX5043_BBOFFSRES3NB
                                    347 	.globl _AX5043_BBOFFSRES2NB
                                    348 	.globl _AX5043_BBOFFSRES1NB
                                    349 	.globl _AX5043_BBOFFSRES0NB
                                    350 	.globl _AX5043_AMPLITUDEGAIN3NB
                                    351 	.globl _AX5043_AMPLITUDEGAIN2NB
                                    352 	.globl _AX5043_AMPLITUDEGAIN1NB
                                    353 	.globl _AX5043_AMPLITUDEGAIN0NB
                                    354 	.globl _AX5043_AGCTARGET3NB
                                    355 	.globl _AX5043_AGCTARGET2NB
                                    356 	.globl _AX5043_AGCTARGET1NB
                                    357 	.globl _AX5043_AGCTARGET0NB
                                    358 	.globl _AX5043_AGCMINMAX3NB
                                    359 	.globl _AX5043_AGCMINMAX2NB
                                    360 	.globl _AX5043_AGCMINMAX1NB
                                    361 	.globl _AX5043_AGCMINMAX0NB
                                    362 	.globl _AX5043_AGCGAIN3NB
                                    363 	.globl _AX5043_AGCGAIN2NB
                                    364 	.globl _AX5043_AGCGAIN1NB
                                    365 	.globl _AX5043_AGCGAIN0NB
                                    366 	.globl _AX5043_AGCAHYST3NB
                                    367 	.globl _AX5043_AGCAHYST2NB
                                    368 	.globl _AX5043_AGCAHYST1NB
                                    369 	.globl _AX5043_AGCAHYST0NB
                                    370 	.globl _AX5043_0xF44NB
                                    371 	.globl _AX5043_0xF35NB
                                    372 	.globl _AX5043_0xF34NB
                                    373 	.globl _AX5043_0xF33NB
                                    374 	.globl _AX5043_0xF32NB
                                    375 	.globl _AX5043_0xF31NB
                                    376 	.globl _AX5043_0xF30NB
                                    377 	.globl _AX5043_0xF26NB
                                    378 	.globl _AX5043_0xF23NB
                                    379 	.globl _AX5043_0xF22NB
                                    380 	.globl _AX5043_0xF21NB
                                    381 	.globl _AX5043_0xF1CNB
                                    382 	.globl _AX5043_0xF18NB
                                    383 	.globl _AX5043_0xF0CNB
                                    384 	.globl _AX5043_0xF00NB
                                    385 	.globl _AX5043_XTALSTATUSNB
                                    386 	.globl _AX5043_XTALOSCNB
                                    387 	.globl _AX5043_XTALCAPNB
                                    388 	.globl _AX5043_XTALAMPLNB
                                    389 	.globl _AX5043_WAKEUPXOEARLYNB
                                    390 	.globl _AX5043_WAKEUPTIMER1NB
                                    391 	.globl _AX5043_WAKEUPTIMER0NB
                                    392 	.globl _AX5043_WAKEUPFREQ1NB
                                    393 	.globl _AX5043_WAKEUPFREQ0NB
                                    394 	.globl _AX5043_WAKEUP1NB
                                    395 	.globl _AX5043_WAKEUP0NB
                                    396 	.globl _AX5043_TXRATE2NB
                                    397 	.globl _AX5043_TXRATE1NB
                                    398 	.globl _AX5043_TXRATE0NB
                                    399 	.globl _AX5043_TXPWRCOEFFE1NB
                                    400 	.globl _AX5043_TXPWRCOEFFE0NB
                                    401 	.globl _AX5043_TXPWRCOEFFD1NB
                                    402 	.globl _AX5043_TXPWRCOEFFD0NB
                                    403 	.globl _AX5043_TXPWRCOEFFC1NB
                                    404 	.globl _AX5043_TXPWRCOEFFC0NB
                                    405 	.globl _AX5043_TXPWRCOEFFB1NB
                                    406 	.globl _AX5043_TXPWRCOEFFB0NB
                                    407 	.globl _AX5043_TXPWRCOEFFA1NB
                                    408 	.globl _AX5043_TXPWRCOEFFA0NB
                                    409 	.globl _AX5043_TRKRFFREQ2NB
                                    410 	.globl _AX5043_TRKRFFREQ1NB
                                    411 	.globl _AX5043_TRKRFFREQ0NB
                                    412 	.globl _AX5043_TRKPHASE1NB
                                    413 	.globl _AX5043_TRKPHASE0NB
                                    414 	.globl _AX5043_TRKFSKDEMOD1NB
                                    415 	.globl _AX5043_TRKFSKDEMOD0NB
                                    416 	.globl _AX5043_TRKFREQ1NB
                                    417 	.globl _AX5043_TRKFREQ0NB
                                    418 	.globl _AX5043_TRKDATARATE2NB
                                    419 	.globl _AX5043_TRKDATARATE1NB
                                    420 	.globl _AX5043_TRKDATARATE0NB
                                    421 	.globl _AX5043_TRKAMPLITUDE1NB
                                    422 	.globl _AX5043_TRKAMPLITUDE0NB
                                    423 	.globl _AX5043_TRKAFSKDEMOD1NB
                                    424 	.globl _AX5043_TRKAFSKDEMOD0NB
                                    425 	.globl _AX5043_TMGTXSETTLENB
                                    426 	.globl _AX5043_TMGTXBOOSTNB
                                    427 	.globl _AX5043_TMGRXSETTLENB
                                    428 	.globl _AX5043_TMGRXRSSINB
                                    429 	.globl _AX5043_TMGRXPREAMBLE3NB
                                    430 	.globl _AX5043_TMGRXPREAMBLE2NB
                                    431 	.globl _AX5043_TMGRXPREAMBLE1NB
                                    432 	.globl _AX5043_TMGRXOFFSACQNB
                                    433 	.globl _AX5043_TMGRXCOARSEAGCNB
                                    434 	.globl _AX5043_TMGRXBOOSTNB
                                    435 	.globl _AX5043_TMGRXAGCNB
                                    436 	.globl _AX5043_TIMER2NB
                                    437 	.globl _AX5043_TIMER1NB
                                    438 	.globl _AX5043_TIMER0NB
                                    439 	.globl _AX5043_SILICONREVISIONNB
                                    440 	.globl _AX5043_SCRATCHNB
                                    441 	.globl _AX5043_RXDATARATE2NB
                                    442 	.globl _AX5043_RXDATARATE1NB
                                    443 	.globl _AX5043_RXDATARATE0NB
                                    444 	.globl _AX5043_RSSIREFERENCENB
                                    445 	.globl _AX5043_RSSIABSTHRNB
                                    446 	.globl _AX5043_RSSINB
                                    447 	.globl _AX5043_REFNB
                                    448 	.globl _AX5043_RADIOSTATENB
                                    449 	.globl _AX5043_RADIOEVENTREQ1NB
                                    450 	.globl _AX5043_RADIOEVENTREQ0NB
                                    451 	.globl _AX5043_RADIOEVENTMASK1NB
                                    452 	.globl _AX5043_RADIOEVENTMASK0NB
                                    453 	.globl _AX5043_PWRMODENB
                                    454 	.globl _AX5043_PWRAMPNB
                                    455 	.globl _AX5043_POWSTICKYSTATNB
                                    456 	.globl _AX5043_POWSTATNB
                                    457 	.globl _AX5043_POWIRQMASKNB
                                    458 	.globl _AX5043_POWCTRL1NB
                                    459 	.globl _AX5043_PLLVCOIRNB
                                    460 	.globl _AX5043_PLLVCOINB
                                    461 	.globl _AX5043_PLLVCODIVNB
                                    462 	.globl _AX5043_PLLRNGCLKNB
                                    463 	.globl _AX5043_PLLRANGINGBNB
                                    464 	.globl _AX5043_PLLRANGINGANB
                                    465 	.globl _AX5043_PLLLOOPBOOSTNB
                                    466 	.globl _AX5043_PLLLOOPNB
                                    467 	.globl _AX5043_PLLLOCKDETNB
                                    468 	.globl _AX5043_PLLCPIBOOSTNB
                                    469 	.globl _AX5043_PLLCPINB
                                    470 	.globl _AX5043_PKTSTOREFLAGSNB
                                    471 	.globl _AX5043_PKTMISCFLAGSNB
                                    472 	.globl _AX5043_PKTCHUNKSIZENB
                                    473 	.globl _AX5043_PKTACCEPTFLAGSNB
                                    474 	.globl _AX5043_PINSTATENB
                                    475 	.globl _AX5043_PINFUNCSYSCLKNB
                                    476 	.globl _AX5043_PINFUNCPWRAMPNB
                                    477 	.globl _AX5043_PINFUNCIRQNB
                                    478 	.globl _AX5043_PINFUNCDCLKNB
                                    479 	.globl _AX5043_PINFUNCDATANB
                                    480 	.globl _AX5043_PINFUNCANTSELNB
                                    481 	.globl _AX5043_MODULATIONNB
                                    482 	.globl _AX5043_MODCFGPNB
                                    483 	.globl _AX5043_MODCFGFNB
                                    484 	.globl _AX5043_MODCFGANB
                                    485 	.globl _AX5043_MAXRFOFFSET2NB
                                    486 	.globl _AX5043_MAXRFOFFSET1NB
                                    487 	.globl _AX5043_MAXRFOFFSET0NB
                                    488 	.globl _AX5043_MAXDROFFSET2NB
                                    489 	.globl _AX5043_MAXDROFFSET1NB
                                    490 	.globl _AX5043_MAXDROFFSET0NB
                                    491 	.globl _AX5043_MATCH1PAT1NB
                                    492 	.globl _AX5043_MATCH1PAT0NB
                                    493 	.globl _AX5043_MATCH1MINNB
                                    494 	.globl _AX5043_MATCH1MAXNB
                                    495 	.globl _AX5043_MATCH1LENNB
                                    496 	.globl _AX5043_MATCH0PAT3NB
                                    497 	.globl _AX5043_MATCH0PAT2NB
                                    498 	.globl _AX5043_MATCH0PAT1NB
                                    499 	.globl _AX5043_MATCH0PAT0NB
                                    500 	.globl _AX5043_MATCH0MINNB
                                    501 	.globl _AX5043_MATCH0MAXNB
                                    502 	.globl _AX5043_MATCH0LENNB
                                    503 	.globl _AX5043_LPOSCSTATUSNB
                                    504 	.globl _AX5043_LPOSCREF1NB
                                    505 	.globl _AX5043_LPOSCREF0NB
                                    506 	.globl _AX5043_LPOSCPER1NB
                                    507 	.globl _AX5043_LPOSCPER0NB
                                    508 	.globl _AX5043_LPOSCKFILT1NB
                                    509 	.globl _AX5043_LPOSCKFILT0NB
                                    510 	.globl _AX5043_LPOSCFREQ1NB
                                    511 	.globl _AX5043_LPOSCFREQ0NB
                                    512 	.globl _AX5043_LPOSCCONFIGNB
                                    513 	.globl _AX5043_IRQREQUEST1NB
                                    514 	.globl _AX5043_IRQREQUEST0NB
                                    515 	.globl _AX5043_IRQMASK1NB
                                    516 	.globl _AX5043_IRQMASK0NB
                                    517 	.globl _AX5043_IRQINVERSION1NB
                                    518 	.globl _AX5043_IRQINVERSION0NB
                                    519 	.globl _AX5043_IFFREQ1NB
                                    520 	.globl _AX5043_IFFREQ0NB
                                    521 	.globl _AX5043_GPADCPERIODNB
                                    522 	.globl _AX5043_GPADCCTRLNB
                                    523 	.globl _AX5043_GPADC13VALUE1NB
                                    524 	.globl _AX5043_GPADC13VALUE0NB
                                    525 	.globl _AX5043_FSKDMIN1NB
                                    526 	.globl _AX5043_FSKDMIN0NB
                                    527 	.globl _AX5043_FSKDMAX1NB
                                    528 	.globl _AX5043_FSKDMAX0NB
                                    529 	.globl _AX5043_FSKDEV2NB
                                    530 	.globl _AX5043_FSKDEV1NB
                                    531 	.globl _AX5043_FSKDEV0NB
                                    532 	.globl _AX5043_FREQB3NB
                                    533 	.globl _AX5043_FREQB2NB
                                    534 	.globl _AX5043_FREQB1NB
                                    535 	.globl _AX5043_FREQB0NB
                                    536 	.globl _AX5043_FREQA3NB
                                    537 	.globl _AX5043_FREQA2NB
                                    538 	.globl _AX5043_FREQA1NB
                                    539 	.globl _AX5043_FREQA0NB
                                    540 	.globl _AX5043_FRAMINGNB
                                    541 	.globl _AX5043_FIFOTHRESH1NB
                                    542 	.globl _AX5043_FIFOTHRESH0NB
                                    543 	.globl _AX5043_FIFOSTATNB
                                    544 	.globl _AX5043_FIFOFREE1NB
                                    545 	.globl _AX5043_FIFOFREE0NB
                                    546 	.globl _AX5043_FIFODATANB
                                    547 	.globl _AX5043_FIFOCOUNT1NB
                                    548 	.globl _AX5043_FIFOCOUNT0NB
                                    549 	.globl _AX5043_FECSYNCNB
                                    550 	.globl _AX5043_FECSTATUSNB
                                    551 	.globl _AX5043_FECNB
                                    552 	.globl _AX5043_ENCODINGNB
                                    553 	.globl _AX5043_DIVERSITYNB
                                    554 	.globl _AX5043_DECIMATIONNB
                                    555 	.globl _AX5043_DACVALUE1NB
                                    556 	.globl _AX5043_DACVALUE0NB
                                    557 	.globl _AX5043_DACCONFIGNB
                                    558 	.globl _AX5043_CRCINIT3NB
                                    559 	.globl _AX5043_CRCINIT2NB
                                    560 	.globl _AX5043_CRCINIT1NB
                                    561 	.globl _AX5043_CRCINIT0NB
                                    562 	.globl _AX5043_BGNDRSSITHRNB
                                    563 	.globl _AX5043_BGNDRSSIGAINNB
                                    564 	.globl _AX5043_BGNDRSSINB
                                    565 	.globl _AX5043_BBTUNENB
                                    566 	.globl _AX5043_BBOFFSCAPNB
                                    567 	.globl _AX5043_AMPLFILTERNB
                                    568 	.globl _AX5043_AGCCOUNTERNB
                                    569 	.globl _AX5043_AFSKSPACE1NB
                                    570 	.globl _AX5043_AFSKSPACE0NB
                                    571 	.globl _AX5043_AFSKMARK1NB
                                    572 	.globl _AX5043_AFSKMARK0NB
                                    573 	.globl _AX5043_AFSKCTRLNB
                                    574 	.globl _AX5043_TIMEGAIN3
                                    575 	.globl _AX5043_TIMEGAIN2
                                    576 	.globl _AX5043_TIMEGAIN1
                                    577 	.globl _AX5043_TIMEGAIN0
                                    578 	.globl _AX5043_RXPARAMSETS
                                    579 	.globl _AX5043_RXPARAMCURSET
                                    580 	.globl _AX5043_PKTMAXLEN
                                    581 	.globl _AX5043_PKTLENOFFSET
                                    582 	.globl _AX5043_PKTLENCFG
                                    583 	.globl _AX5043_PKTADDRMASK3
                                    584 	.globl _AX5043_PKTADDRMASK2
                                    585 	.globl _AX5043_PKTADDRMASK1
                                    586 	.globl _AX5043_PKTADDRMASK0
                                    587 	.globl _AX5043_PKTADDRCFG
                                    588 	.globl _AX5043_PKTADDR3
                                    589 	.globl _AX5043_PKTADDR2
                                    590 	.globl _AX5043_PKTADDR1
                                    591 	.globl _AX5043_PKTADDR0
                                    592 	.globl _AX5043_PHASEGAIN3
                                    593 	.globl _AX5043_PHASEGAIN2
                                    594 	.globl _AX5043_PHASEGAIN1
                                    595 	.globl _AX5043_PHASEGAIN0
                                    596 	.globl _AX5043_FREQUENCYLEAK
                                    597 	.globl _AX5043_FREQUENCYGAIND3
                                    598 	.globl _AX5043_FREQUENCYGAIND2
                                    599 	.globl _AX5043_FREQUENCYGAIND1
                                    600 	.globl _AX5043_FREQUENCYGAIND0
                                    601 	.globl _AX5043_FREQUENCYGAINC3
                                    602 	.globl _AX5043_FREQUENCYGAINC2
                                    603 	.globl _AX5043_FREQUENCYGAINC1
                                    604 	.globl _AX5043_FREQUENCYGAINC0
                                    605 	.globl _AX5043_FREQUENCYGAINB3
                                    606 	.globl _AX5043_FREQUENCYGAINB2
                                    607 	.globl _AX5043_FREQUENCYGAINB1
                                    608 	.globl _AX5043_FREQUENCYGAINB0
                                    609 	.globl _AX5043_FREQUENCYGAINA3
                                    610 	.globl _AX5043_FREQUENCYGAINA2
                                    611 	.globl _AX5043_FREQUENCYGAINA1
                                    612 	.globl _AX5043_FREQUENCYGAINA0
                                    613 	.globl _AX5043_FREQDEV13
                                    614 	.globl _AX5043_FREQDEV12
                                    615 	.globl _AX5043_FREQDEV11
                                    616 	.globl _AX5043_FREQDEV10
                                    617 	.globl _AX5043_FREQDEV03
                                    618 	.globl _AX5043_FREQDEV02
                                    619 	.globl _AX5043_FREQDEV01
                                    620 	.globl _AX5043_FREQDEV00
                                    621 	.globl _AX5043_FOURFSK3
                                    622 	.globl _AX5043_FOURFSK2
                                    623 	.globl _AX5043_FOURFSK1
                                    624 	.globl _AX5043_FOURFSK0
                                    625 	.globl _AX5043_DRGAIN3
                                    626 	.globl _AX5043_DRGAIN2
                                    627 	.globl _AX5043_DRGAIN1
                                    628 	.globl _AX5043_DRGAIN0
                                    629 	.globl _AX5043_BBOFFSRES3
                                    630 	.globl _AX5043_BBOFFSRES2
                                    631 	.globl _AX5043_BBOFFSRES1
                                    632 	.globl _AX5043_BBOFFSRES0
                                    633 	.globl _AX5043_AMPLITUDEGAIN3
                                    634 	.globl _AX5043_AMPLITUDEGAIN2
                                    635 	.globl _AX5043_AMPLITUDEGAIN1
                                    636 	.globl _AX5043_AMPLITUDEGAIN0
                                    637 	.globl _AX5043_AGCTARGET3
                                    638 	.globl _AX5043_AGCTARGET2
                                    639 	.globl _AX5043_AGCTARGET1
                                    640 	.globl _AX5043_AGCTARGET0
                                    641 	.globl _AX5043_AGCMINMAX3
                                    642 	.globl _AX5043_AGCMINMAX2
                                    643 	.globl _AX5043_AGCMINMAX1
                                    644 	.globl _AX5043_AGCMINMAX0
                                    645 	.globl _AX5043_AGCGAIN3
                                    646 	.globl _AX5043_AGCGAIN2
                                    647 	.globl _AX5043_AGCGAIN1
                                    648 	.globl _AX5043_AGCGAIN0
                                    649 	.globl _AX5043_AGCAHYST3
                                    650 	.globl _AX5043_AGCAHYST2
                                    651 	.globl _AX5043_AGCAHYST1
                                    652 	.globl _AX5043_AGCAHYST0
                                    653 	.globl _AX5043_0xF44
                                    654 	.globl _AX5043_0xF35
                                    655 	.globl _AX5043_0xF34
                                    656 	.globl _AX5043_0xF33
                                    657 	.globl _AX5043_0xF32
                                    658 	.globl _AX5043_0xF31
                                    659 	.globl _AX5043_0xF30
                                    660 	.globl _AX5043_0xF26
                                    661 	.globl _AX5043_0xF23
                                    662 	.globl _AX5043_0xF22
                                    663 	.globl _AX5043_0xF21
                                    664 	.globl _AX5043_0xF1C
                                    665 	.globl _AX5043_0xF18
                                    666 	.globl _AX5043_0xF0C
                                    667 	.globl _AX5043_0xF00
                                    668 	.globl _AX5043_XTALSTATUS
                                    669 	.globl _AX5043_XTALOSC
                                    670 	.globl _AX5043_XTALCAP
                                    671 	.globl _AX5043_XTALAMPL
                                    672 	.globl _AX5043_WAKEUPXOEARLY
                                    673 	.globl _AX5043_WAKEUPTIMER1
                                    674 	.globl _AX5043_WAKEUPTIMER0
                                    675 	.globl _AX5043_WAKEUPFREQ1
                                    676 	.globl _AX5043_WAKEUPFREQ0
                                    677 	.globl _AX5043_WAKEUP1
                                    678 	.globl _AX5043_WAKEUP0
                                    679 	.globl _AX5043_TXRATE2
                                    680 	.globl _AX5043_TXRATE1
                                    681 	.globl _AX5043_TXRATE0
                                    682 	.globl _AX5043_TXPWRCOEFFE1
                                    683 	.globl _AX5043_TXPWRCOEFFE0
                                    684 	.globl _AX5043_TXPWRCOEFFD1
                                    685 	.globl _AX5043_TXPWRCOEFFD0
                                    686 	.globl _AX5043_TXPWRCOEFFC1
                                    687 	.globl _AX5043_TXPWRCOEFFC0
                                    688 	.globl _AX5043_TXPWRCOEFFB1
                                    689 	.globl _AX5043_TXPWRCOEFFB0
                                    690 	.globl _AX5043_TXPWRCOEFFA1
                                    691 	.globl _AX5043_TXPWRCOEFFA0
                                    692 	.globl _AX5043_TRKRFFREQ2
                                    693 	.globl _AX5043_TRKRFFREQ1
                                    694 	.globl _AX5043_TRKRFFREQ0
                                    695 	.globl _AX5043_TRKPHASE1
                                    696 	.globl _AX5043_TRKPHASE0
                                    697 	.globl _AX5043_TRKFSKDEMOD1
                                    698 	.globl _AX5043_TRKFSKDEMOD0
                                    699 	.globl _AX5043_TRKFREQ1
                                    700 	.globl _AX5043_TRKFREQ0
                                    701 	.globl _AX5043_TRKDATARATE2
                                    702 	.globl _AX5043_TRKDATARATE1
                                    703 	.globl _AX5043_TRKDATARATE0
                                    704 	.globl _AX5043_TRKAMPLITUDE1
                                    705 	.globl _AX5043_TRKAMPLITUDE0
                                    706 	.globl _AX5043_TRKAFSKDEMOD1
                                    707 	.globl _AX5043_TRKAFSKDEMOD0
                                    708 	.globl _AX5043_TMGTXSETTLE
                                    709 	.globl _AX5043_TMGTXBOOST
                                    710 	.globl _AX5043_TMGRXSETTLE
                                    711 	.globl _AX5043_TMGRXRSSI
                                    712 	.globl _AX5043_TMGRXPREAMBLE3
                                    713 	.globl _AX5043_TMGRXPREAMBLE2
                                    714 	.globl _AX5043_TMGRXPREAMBLE1
                                    715 	.globl _AX5043_TMGRXOFFSACQ
                                    716 	.globl _AX5043_TMGRXCOARSEAGC
                                    717 	.globl _AX5043_TMGRXBOOST
                                    718 	.globl _AX5043_TMGRXAGC
                                    719 	.globl _AX5043_TIMER2
                                    720 	.globl _AX5043_TIMER1
                                    721 	.globl _AX5043_TIMER0
                                    722 	.globl _AX5043_SILICONREVISION
                                    723 	.globl _AX5043_SCRATCH
                                    724 	.globl _AX5043_RXDATARATE2
                                    725 	.globl _AX5043_RXDATARATE1
                                    726 	.globl _AX5043_RXDATARATE0
                                    727 	.globl _AX5043_RSSIREFERENCE
                                    728 	.globl _AX5043_RSSIABSTHR
                                    729 	.globl _AX5043_RSSI
                                    730 	.globl _AX5043_REF
                                    731 	.globl _AX5043_RADIOSTATE
                                    732 	.globl _AX5043_RADIOEVENTREQ1
                                    733 	.globl _AX5043_RADIOEVENTREQ0
                                    734 	.globl _AX5043_RADIOEVENTMASK1
                                    735 	.globl _AX5043_RADIOEVENTMASK0
                                    736 	.globl _AX5043_PWRMODE
                                    737 	.globl _AX5043_PWRAMP
                                    738 	.globl _AX5043_POWSTICKYSTAT
                                    739 	.globl _AX5043_POWSTAT
                                    740 	.globl _AX5043_POWIRQMASK
                                    741 	.globl _AX5043_POWCTRL1
                                    742 	.globl _AX5043_PLLVCOIR
                                    743 	.globl _AX5043_PLLVCOI
                                    744 	.globl _AX5043_PLLVCODIV
                                    745 	.globl _AX5043_PLLRNGCLK
                                    746 	.globl _AX5043_PLLRANGINGB
                                    747 	.globl _AX5043_PLLRANGINGA
                                    748 	.globl _AX5043_PLLLOOPBOOST
                                    749 	.globl _AX5043_PLLLOOP
                                    750 	.globl _AX5043_PLLLOCKDET
                                    751 	.globl _AX5043_PLLCPIBOOST
                                    752 	.globl _AX5043_PLLCPI
                                    753 	.globl _AX5043_PKTSTOREFLAGS
                                    754 	.globl _AX5043_PKTMISCFLAGS
                                    755 	.globl _AX5043_PKTCHUNKSIZE
                                    756 	.globl _AX5043_PKTACCEPTFLAGS
                                    757 	.globl _AX5043_PINSTATE
                                    758 	.globl _AX5043_PINFUNCSYSCLK
                                    759 	.globl _AX5043_PINFUNCPWRAMP
                                    760 	.globl _AX5043_PINFUNCIRQ
                                    761 	.globl _AX5043_PINFUNCDCLK
                                    762 	.globl _AX5043_PINFUNCDATA
                                    763 	.globl _AX5043_PINFUNCANTSEL
                                    764 	.globl _AX5043_MODULATION
                                    765 	.globl _AX5043_MODCFGP
                                    766 	.globl _AX5043_MODCFGF
                                    767 	.globl _AX5043_MODCFGA
                                    768 	.globl _AX5043_MAXRFOFFSET2
                                    769 	.globl _AX5043_MAXRFOFFSET1
                                    770 	.globl _AX5043_MAXRFOFFSET0
                                    771 	.globl _AX5043_MAXDROFFSET2
                                    772 	.globl _AX5043_MAXDROFFSET1
                                    773 	.globl _AX5043_MAXDROFFSET0
                                    774 	.globl _AX5043_MATCH1PAT1
                                    775 	.globl _AX5043_MATCH1PAT0
                                    776 	.globl _AX5043_MATCH1MIN
                                    777 	.globl _AX5043_MATCH1MAX
                                    778 	.globl _AX5043_MATCH1LEN
                                    779 	.globl _AX5043_MATCH0PAT3
                                    780 	.globl _AX5043_MATCH0PAT2
                                    781 	.globl _AX5043_MATCH0PAT1
                                    782 	.globl _AX5043_MATCH0PAT0
                                    783 	.globl _AX5043_MATCH0MIN
                                    784 	.globl _AX5043_MATCH0MAX
                                    785 	.globl _AX5043_MATCH0LEN
                                    786 	.globl _AX5043_LPOSCSTATUS
                                    787 	.globl _AX5043_LPOSCREF1
                                    788 	.globl _AX5043_LPOSCREF0
                                    789 	.globl _AX5043_LPOSCPER1
                                    790 	.globl _AX5043_LPOSCPER0
                                    791 	.globl _AX5043_LPOSCKFILT1
                                    792 	.globl _AX5043_LPOSCKFILT0
                                    793 	.globl _AX5043_LPOSCFREQ1
                                    794 	.globl _AX5043_LPOSCFREQ0
                                    795 	.globl _AX5043_LPOSCCONFIG
                                    796 	.globl _AX5043_IRQREQUEST1
                                    797 	.globl _AX5043_IRQREQUEST0
                                    798 	.globl _AX5043_IRQMASK1
                                    799 	.globl _AX5043_IRQMASK0
                                    800 	.globl _AX5043_IRQINVERSION1
                                    801 	.globl _AX5043_IRQINVERSION0
                                    802 	.globl _AX5043_IFFREQ1
                                    803 	.globl _AX5043_IFFREQ0
                                    804 	.globl _AX5043_GPADCPERIOD
                                    805 	.globl _AX5043_GPADCCTRL
                                    806 	.globl _AX5043_GPADC13VALUE1
                                    807 	.globl _AX5043_GPADC13VALUE0
                                    808 	.globl _AX5043_FSKDMIN1
                                    809 	.globl _AX5043_FSKDMIN0
                                    810 	.globl _AX5043_FSKDMAX1
                                    811 	.globl _AX5043_FSKDMAX0
                                    812 	.globl _AX5043_FSKDEV2
                                    813 	.globl _AX5043_FSKDEV1
                                    814 	.globl _AX5043_FSKDEV0
                                    815 	.globl _AX5043_FREQB3
                                    816 	.globl _AX5043_FREQB2
                                    817 	.globl _AX5043_FREQB1
                                    818 	.globl _AX5043_FREQB0
                                    819 	.globl _AX5043_FREQA3
                                    820 	.globl _AX5043_FREQA2
                                    821 	.globl _AX5043_FREQA1
                                    822 	.globl _AX5043_FREQA0
                                    823 	.globl _AX5043_FRAMING
                                    824 	.globl _AX5043_FIFOTHRESH1
                                    825 	.globl _AX5043_FIFOTHRESH0
                                    826 	.globl _AX5043_FIFOSTAT
                                    827 	.globl _AX5043_FIFOFREE1
                                    828 	.globl _AX5043_FIFOFREE0
                                    829 	.globl _AX5043_FIFODATA
                                    830 	.globl _AX5043_FIFOCOUNT1
                                    831 	.globl _AX5043_FIFOCOUNT0
                                    832 	.globl _AX5043_FECSYNC
                                    833 	.globl _AX5043_FECSTATUS
                                    834 	.globl _AX5043_FEC
                                    835 	.globl _AX5043_ENCODING
                                    836 	.globl _AX5043_DIVERSITY
                                    837 	.globl _AX5043_DECIMATION
                                    838 	.globl _AX5043_DACVALUE1
                                    839 	.globl _AX5043_DACVALUE0
                                    840 	.globl _AX5043_DACCONFIG
                                    841 	.globl _AX5043_CRCINIT3
                                    842 	.globl _AX5043_CRCINIT2
                                    843 	.globl _AX5043_CRCINIT1
                                    844 	.globl _AX5043_CRCINIT0
                                    845 	.globl _AX5043_BGNDRSSITHR
                                    846 	.globl _AX5043_BGNDRSSIGAIN
                                    847 	.globl _AX5043_BGNDRSSI
                                    848 	.globl _AX5043_BBTUNE
                                    849 	.globl _AX5043_BBOFFSCAP
                                    850 	.globl _AX5043_AMPLFILTER
                                    851 	.globl _AX5043_AGCCOUNTER
                                    852 	.globl _AX5043_AFSKSPACE1
                                    853 	.globl _AX5043_AFSKSPACE0
                                    854 	.globl _AX5043_AFSKMARK1
                                    855 	.globl _AX5043_AFSKMARK0
                                    856 	.globl _AX5043_AFSKCTRL
                                    857 	.globl _RNGCLKSRC1
                                    858 	.globl _RNGCLKSRC0
                                    859 	.globl _RNGMODE
                                    860 	.globl _AESOUTADDR
                                    861 	.globl _AESOUTADDR1
                                    862 	.globl _AESOUTADDR0
                                    863 	.globl _AESMODE
                                    864 	.globl _AESKEYADDR
                                    865 	.globl _AESKEYADDR1
                                    866 	.globl _AESKEYADDR0
                                    867 	.globl _AESINADDR
                                    868 	.globl _AESINADDR1
                                    869 	.globl _AESINADDR0
                                    870 	.globl _AESCURBLOCK
                                    871 	.globl _AESCONFIG
                                    872 	.globl _RNGBYTE
                                    873 	.globl _XTALREADY
                                    874 	.globl _XTALOSC
                                    875 	.globl _XTALAMPL
                                    876 	.globl _SILICONREV
                                    877 	.globl _SCRATCH3
                                    878 	.globl _SCRATCH2
                                    879 	.globl _SCRATCH1
                                    880 	.globl _SCRATCH0
                                    881 	.globl _RADIOMUX
                                    882 	.globl _RADIOFSTATADDR
                                    883 	.globl _RADIOFSTATADDR1
                                    884 	.globl _RADIOFSTATADDR0
                                    885 	.globl _RADIOFDATAADDR
                                    886 	.globl _RADIOFDATAADDR1
                                    887 	.globl _RADIOFDATAADDR0
                                    888 	.globl _OSCRUN
                                    889 	.globl _OSCREADY
                                    890 	.globl _OSCFORCERUN
                                    891 	.globl _OSCCALIB
                                    892 	.globl _MISCCTRL
                                    893 	.globl _LPXOSCGM
                                    894 	.globl _LPOSCREF
                                    895 	.globl _LPOSCREF1
                                    896 	.globl _LPOSCREF0
                                    897 	.globl _LPOSCPER
                                    898 	.globl _LPOSCPER1
                                    899 	.globl _LPOSCPER0
                                    900 	.globl _LPOSCKFILT
                                    901 	.globl _LPOSCKFILT1
                                    902 	.globl _LPOSCKFILT0
                                    903 	.globl _LPOSCFREQ
                                    904 	.globl _LPOSCFREQ1
                                    905 	.globl _LPOSCFREQ0
                                    906 	.globl _LPOSCCONFIG
                                    907 	.globl _PINSEL
                                    908 	.globl _PINCHGC
                                    909 	.globl _PINCHGB
                                    910 	.globl _PINCHGA
                                    911 	.globl _PALTRADIO
                                    912 	.globl _PALTC
                                    913 	.globl _PALTB
                                    914 	.globl _PALTA
                                    915 	.globl _INTCHGC
                                    916 	.globl _INTCHGB
                                    917 	.globl _INTCHGA
                                    918 	.globl _EXTIRQ
                                    919 	.globl _GPIOENABLE
                                    920 	.globl _ANALOGA
                                    921 	.globl _FRCOSCREF
                                    922 	.globl _FRCOSCREF1
                                    923 	.globl _FRCOSCREF0
                                    924 	.globl _FRCOSCPER
                                    925 	.globl _FRCOSCPER1
                                    926 	.globl _FRCOSCPER0
                                    927 	.globl _FRCOSCKFILT
                                    928 	.globl _FRCOSCKFILT1
                                    929 	.globl _FRCOSCKFILT0
                                    930 	.globl _FRCOSCFREQ
                                    931 	.globl _FRCOSCFREQ1
                                    932 	.globl _FRCOSCFREQ0
                                    933 	.globl _FRCOSCCTRL
                                    934 	.globl _FRCOSCCONFIG
                                    935 	.globl _DMA1CONFIG
                                    936 	.globl _DMA1ADDR
                                    937 	.globl _DMA1ADDR1
                                    938 	.globl _DMA1ADDR0
                                    939 	.globl _DMA0CONFIG
                                    940 	.globl _DMA0ADDR
                                    941 	.globl _DMA0ADDR1
                                    942 	.globl _DMA0ADDR0
                                    943 	.globl _ADCTUNE2
                                    944 	.globl _ADCTUNE1
                                    945 	.globl _ADCTUNE0
                                    946 	.globl _ADCCH3VAL
                                    947 	.globl _ADCCH3VAL1
                                    948 	.globl _ADCCH3VAL0
                                    949 	.globl _ADCCH2VAL
                                    950 	.globl _ADCCH2VAL1
                                    951 	.globl _ADCCH2VAL0
                                    952 	.globl _ADCCH1VAL
                                    953 	.globl _ADCCH1VAL1
                                    954 	.globl _ADCCH1VAL0
                                    955 	.globl _ADCCH0VAL
                                    956 	.globl _ADCCH0VAL1
                                    957 	.globl _ADCCH0VAL0
                                    958 ;--------------------------------------------------------
                                    959 ; special function registers
                                    960 ;--------------------------------------------------------
                                    961 	.area RSEG    (ABS,DATA)
      000000                        962 	.org 0x0000
                           0000E0   963 _ACC	=	0x00e0
                           0000F0   964 _B	=	0x00f0
                           000083   965 _DPH	=	0x0083
                           000085   966 _DPH1	=	0x0085
                           000082   967 _DPL	=	0x0082
                           000084   968 _DPL1	=	0x0084
                           008382   969 _DPTR0	=	0x8382
                           008584   970 _DPTR1	=	0x8584
                           000086   971 _DPS	=	0x0086
                           0000A0   972 _E2IE	=	0x00a0
                           0000C0   973 _E2IP	=	0x00c0
                           000098   974 _EIE	=	0x0098
                           0000B0   975 _EIP	=	0x00b0
                           0000A8   976 _IE	=	0x00a8
                           0000B8   977 _IP	=	0x00b8
                           000087   978 _PCON	=	0x0087
                           0000D0   979 _PSW	=	0x00d0
                           000081   980 _SP	=	0x0081
                           0000D9   981 _XPAGE	=	0x00d9
                           0000D9   982 __XPAGE	=	0x00d9
                           0000CA   983 _ADCCH0CONFIG	=	0x00ca
                           0000CB   984 _ADCCH1CONFIG	=	0x00cb
                           0000D2   985 _ADCCH2CONFIG	=	0x00d2
                           0000D3   986 _ADCCH3CONFIG	=	0x00d3
                           0000D1   987 _ADCCLKSRC	=	0x00d1
                           0000C9   988 _ADCCONV	=	0x00c9
                           0000E1   989 _ANALOGCOMP	=	0x00e1
                           0000C6   990 _CLKCON	=	0x00c6
                           0000C7   991 _CLKSTAT	=	0x00c7
                           000097   992 _CODECONFIG	=	0x0097
                           0000E3   993 _DBGLNKBUF	=	0x00e3
                           0000E2   994 _DBGLNKSTAT	=	0x00e2
                           000089   995 _DIRA	=	0x0089
                           00008A   996 _DIRB	=	0x008a
                           00008B   997 _DIRC	=	0x008b
                           00008E   998 _DIRR	=	0x008e
                           0000C8   999 _PINA	=	0x00c8
                           0000E8  1000 _PINB	=	0x00e8
                           0000F8  1001 _PINC	=	0x00f8
                           00008D  1002 _PINR	=	0x008d
                           000080  1003 _PORTA	=	0x0080
                           000088  1004 _PORTB	=	0x0088
                           000090  1005 _PORTC	=	0x0090
                           00008C  1006 _PORTR	=	0x008c
                           0000CE  1007 _IC0CAPT0	=	0x00ce
                           0000CF  1008 _IC0CAPT1	=	0x00cf
                           00CFCE  1009 _IC0CAPT	=	0xcfce
                           0000CC  1010 _IC0MODE	=	0x00cc
                           0000CD  1011 _IC0STATUS	=	0x00cd
                           0000D6  1012 _IC1CAPT0	=	0x00d6
                           0000D7  1013 _IC1CAPT1	=	0x00d7
                           00D7D6  1014 _IC1CAPT	=	0xd7d6
                           0000D4  1015 _IC1MODE	=	0x00d4
                           0000D5  1016 _IC1STATUS	=	0x00d5
                           000092  1017 _NVADDR0	=	0x0092
                           000093  1018 _NVADDR1	=	0x0093
                           009392  1019 _NVADDR	=	0x9392
                           000094  1020 _NVDATA0	=	0x0094
                           000095  1021 _NVDATA1	=	0x0095
                           009594  1022 _NVDATA	=	0x9594
                           000096  1023 _NVKEY	=	0x0096
                           000091  1024 _NVSTATUS	=	0x0091
                           0000BC  1025 _OC0COMP0	=	0x00bc
                           0000BD  1026 _OC0COMP1	=	0x00bd
                           00BDBC  1027 _OC0COMP	=	0xbdbc
                           0000B9  1028 _OC0MODE	=	0x00b9
                           0000BA  1029 _OC0PIN	=	0x00ba
                           0000BB  1030 _OC0STATUS	=	0x00bb
                           0000C4  1031 _OC1COMP0	=	0x00c4
                           0000C5  1032 _OC1COMP1	=	0x00c5
                           00C5C4  1033 _OC1COMP	=	0xc5c4
                           0000C1  1034 _OC1MODE	=	0x00c1
                           0000C2  1035 _OC1PIN	=	0x00c2
                           0000C3  1036 _OC1STATUS	=	0x00c3
                           0000B1  1037 _RADIOACC	=	0x00b1
                           0000B3  1038 _RADIOADDR0	=	0x00b3
                           0000B2  1039 _RADIOADDR1	=	0x00b2
                           00B2B3  1040 _RADIOADDR	=	0xb2b3
                           0000B7  1041 _RADIODATA0	=	0x00b7
                           0000B6  1042 _RADIODATA1	=	0x00b6
                           0000B5  1043 _RADIODATA2	=	0x00b5
                           0000B4  1044 _RADIODATA3	=	0x00b4
                           B4B5B6B7  1045 _RADIODATA	=	0xb4b5b6b7
                           0000BE  1046 _RADIOSTAT0	=	0x00be
                           0000BF  1047 _RADIOSTAT1	=	0x00bf
                           00BFBE  1048 _RADIOSTAT	=	0xbfbe
                           0000DF  1049 _SPCLKSRC	=	0x00df
                           0000DC  1050 _SPMODE	=	0x00dc
                           0000DE  1051 _SPSHREG	=	0x00de
                           0000DD  1052 _SPSTATUS	=	0x00dd
                           00009A  1053 _T0CLKSRC	=	0x009a
                           00009C  1054 _T0CNT0	=	0x009c
                           00009D  1055 _T0CNT1	=	0x009d
                           009D9C  1056 _T0CNT	=	0x9d9c
                           000099  1057 _T0MODE	=	0x0099
                           00009E  1058 _T0PERIOD0	=	0x009e
                           00009F  1059 _T0PERIOD1	=	0x009f
                           009F9E  1060 _T0PERIOD	=	0x9f9e
                           00009B  1061 _T0STATUS	=	0x009b
                           0000A2  1062 _T1CLKSRC	=	0x00a2
                           0000A4  1063 _T1CNT0	=	0x00a4
                           0000A5  1064 _T1CNT1	=	0x00a5
                           00A5A4  1065 _T1CNT	=	0xa5a4
                           0000A1  1066 _T1MODE	=	0x00a1
                           0000A6  1067 _T1PERIOD0	=	0x00a6
                           0000A7  1068 _T1PERIOD1	=	0x00a7
                           00A7A6  1069 _T1PERIOD	=	0xa7a6
                           0000A3  1070 _T1STATUS	=	0x00a3
                           0000AA  1071 _T2CLKSRC	=	0x00aa
                           0000AC  1072 _T2CNT0	=	0x00ac
                           0000AD  1073 _T2CNT1	=	0x00ad
                           00ADAC  1074 _T2CNT	=	0xadac
                           0000A9  1075 _T2MODE	=	0x00a9
                           0000AE  1076 _T2PERIOD0	=	0x00ae
                           0000AF  1077 _T2PERIOD1	=	0x00af
                           00AFAE  1078 _T2PERIOD	=	0xafae
                           0000AB  1079 _T2STATUS	=	0x00ab
                           0000E4  1080 _U0CTRL	=	0x00e4
                           0000E7  1081 _U0MODE	=	0x00e7
                           0000E6  1082 _U0SHREG	=	0x00e6
                           0000E5  1083 _U0STATUS	=	0x00e5
                           0000EC  1084 _U1CTRL	=	0x00ec
                           0000EF  1085 _U1MODE	=	0x00ef
                           0000EE  1086 _U1SHREG	=	0x00ee
                           0000ED  1087 _U1STATUS	=	0x00ed
                           0000DA  1088 _WDTCFG	=	0x00da
                           0000DB  1089 _WDTRESET	=	0x00db
                           0000F1  1090 _WTCFGA	=	0x00f1
                           0000F9  1091 _WTCFGB	=	0x00f9
                           0000F2  1092 _WTCNTA0	=	0x00f2
                           0000F3  1093 _WTCNTA1	=	0x00f3
                           00F3F2  1094 _WTCNTA	=	0xf3f2
                           0000FA  1095 _WTCNTB0	=	0x00fa
                           0000FB  1096 _WTCNTB1	=	0x00fb
                           00FBFA  1097 _WTCNTB	=	0xfbfa
                           0000EB  1098 _WTCNTR1	=	0x00eb
                           0000F4  1099 _WTEVTA0	=	0x00f4
                           0000F5  1100 _WTEVTA1	=	0x00f5
                           00F5F4  1101 _WTEVTA	=	0xf5f4
                           0000F6  1102 _WTEVTB0	=	0x00f6
                           0000F7  1103 _WTEVTB1	=	0x00f7
                           00F7F6  1104 _WTEVTB	=	0xf7f6
                           0000FC  1105 _WTEVTC0	=	0x00fc
                           0000FD  1106 _WTEVTC1	=	0x00fd
                           00FDFC  1107 _WTEVTC	=	0xfdfc
                           0000FE  1108 _WTEVTD0	=	0x00fe
                           0000FF  1109 _WTEVTD1	=	0x00ff
                           00FFFE  1110 _WTEVTD	=	0xfffe
                           0000E9  1111 _WTIRQEN	=	0x00e9
                           0000EA  1112 _WTSTAT	=	0x00ea
                                   1113 ;--------------------------------------------------------
                                   1114 ; special function bits
                                   1115 ;--------------------------------------------------------
                                   1116 	.area RSEG    (ABS,DATA)
      000000                       1117 	.org 0x0000
                           0000E0  1118 _ACC_0	=	0x00e0
                           0000E1  1119 _ACC_1	=	0x00e1
                           0000E2  1120 _ACC_2	=	0x00e2
                           0000E3  1121 _ACC_3	=	0x00e3
                           0000E4  1122 _ACC_4	=	0x00e4
                           0000E5  1123 _ACC_5	=	0x00e5
                           0000E6  1124 _ACC_6	=	0x00e6
                           0000E7  1125 _ACC_7	=	0x00e7
                           0000F0  1126 _B_0	=	0x00f0
                           0000F1  1127 _B_1	=	0x00f1
                           0000F2  1128 _B_2	=	0x00f2
                           0000F3  1129 _B_3	=	0x00f3
                           0000F4  1130 _B_4	=	0x00f4
                           0000F5  1131 _B_5	=	0x00f5
                           0000F6  1132 _B_6	=	0x00f6
                           0000F7  1133 _B_7	=	0x00f7
                           0000A0  1134 _E2IE_0	=	0x00a0
                           0000A1  1135 _E2IE_1	=	0x00a1
                           0000A2  1136 _E2IE_2	=	0x00a2
                           0000A3  1137 _E2IE_3	=	0x00a3
                           0000A4  1138 _E2IE_4	=	0x00a4
                           0000A5  1139 _E2IE_5	=	0x00a5
                           0000A6  1140 _E2IE_6	=	0x00a6
                           0000A7  1141 _E2IE_7	=	0x00a7
                           0000C0  1142 _E2IP_0	=	0x00c0
                           0000C1  1143 _E2IP_1	=	0x00c1
                           0000C2  1144 _E2IP_2	=	0x00c2
                           0000C3  1145 _E2IP_3	=	0x00c3
                           0000C4  1146 _E2IP_4	=	0x00c4
                           0000C5  1147 _E2IP_5	=	0x00c5
                           0000C6  1148 _E2IP_6	=	0x00c6
                           0000C7  1149 _E2IP_7	=	0x00c7
                           000098  1150 _EIE_0	=	0x0098
                           000099  1151 _EIE_1	=	0x0099
                           00009A  1152 _EIE_2	=	0x009a
                           00009B  1153 _EIE_3	=	0x009b
                           00009C  1154 _EIE_4	=	0x009c
                           00009D  1155 _EIE_5	=	0x009d
                           00009E  1156 _EIE_6	=	0x009e
                           00009F  1157 _EIE_7	=	0x009f
                           0000B0  1158 _EIP_0	=	0x00b0
                           0000B1  1159 _EIP_1	=	0x00b1
                           0000B2  1160 _EIP_2	=	0x00b2
                           0000B3  1161 _EIP_3	=	0x00b3
                           0000B4  1162 _EIP_4	=	0x00b4
                           0000B5  1163 _EIP_5	=	0x00b5
                           0000B6  1164 _EIP_6	=	0x00b6
                           0000B7  1165 _EIP_7	=	0x00b7
                           0000A8  1166 _IE_0	=	0x00a8
                           0000A9  1167 _IE_1	=	0x00a9
                           0000AA  1168 _IE_2	=	0x00aa
                           0000AB  1169 _IE_3	=	0x00ab
                           0000AC  1170 _IE_4	=	0x00ac
                           0000AD  1171 _IE_5	=	0x00ad
                           0000AE  1172 _IE_6	=	0x00ae
                           0000AF  1173 _IE_7	=	0x00af
                           0000AF  1174 _EA	=	0x00af
                           0000B8  1175 _IP_0	=	0x00b8
                           0000B9  1176 _IP_1	=	0x00b9
                           0000BA  1177 _IP_2	=	0x00ba
                           0000BB  1178 _IP_3	=	0x00bb
                           0000BC  1179 _IP_4	=	0x00bc
                           0000BD  1180 _IP_5	=	0x00bd
                           0000BE  1181 _IP_6	=	0x00be
                           0000BF  1182 _IP_7	=	0x00bf
                           0000D0  1183 _P	=	0x00d0
                           0000D1  1184 _F1	=	0x00d1
                           0000D2  1185 _OV	=	0x00d2
                           0000D3  1186 _RS0	=	0x00d3
                           0000D4  1187 _RS1	=	0x00d4
                           0000D5  1188 _F0	=	0x00d5
                           0000D6  1189 _AC	=	0x00d6
                           0000D7  1190 _CY	=	0x00d7
                           0000C8  1191 _PINA_0	=	0x00c8
                           0000C9  1192 _PINA_1	=	0x00c9
                           0000CA  1193 _PINA_2	=	0x00ca
                           0000CB  1194 _PINA_3	=	0x00cb
                           0000CC  1195 _PINA_4	=	0x00cc
                           0000CD  1196 _PINA_5	=	0x00cd
                           0000CE  1197 _PINA_6	=	0x00ce
                           0000CF  1198 _PINA_7	=	0x00cf
                           0000E8  1199 _PINB_0	=	0x00e8
                           0000E9  1200 _PINB_1	=	0x00e9
                           0000EA  1201 _PINB_2	=	0x00ea
                           0000EB  1202 _PINB_3	=	0x00eb
                           0000EC  1203 _PINB_4	=	0x00ec
                           0000ED  1204 _PINB_5	=	0x00ed
                           0000EE  1205 _PINB_6	=	0x00ee
                           0000EF  1206 _PINB_7	=	0x00ef
                           0000F8  1207 _PINC_0	=	0x00f8
                           0000F9  1208 _PINC_1	=	0x00f9
                           0000FA  1209 _PINC_2	=	0x00fa
                           0000FB  1210 _PINC_3	=	0x00fb
                           0000FC  1211 _PINC_4	=	0x00fc
                           0000FD  1212 _PINC_5	=	0x00fd
                           0000FE  1213 _PINC_6	=	0x00fe
                           0000FF  1214 _PINC_7	=	0x00ff
                           000080  1215 _PORTA_0	=	0x0080
                           000081  1216 _PORTA_1	=	0x0081
                           000082  1217 _PORTA_2	=	0x0082
                           000083  1218 _PORTA_3	=	0x0083
                           000084  1219 _PORTA_4	=	0x0084
                           000085  1220 _PORTA_5	=	0x0085
                           000086  1221 _PORTA_6	=	0x0086
                           000087  1222 _PORTA_7	=	0x0087
                           000088  1223 _PORTB_0	=	0x0088
                           000089  1224 _PORTB_1	=	0x0089
                           00008A  1225 _PORTB_2	=	0x008a
                           00008B  1226 _PORTB_3	=	0x008b
                           00008C  1227 _PORTB_4	=	0x008c
                           00008D  1228 _PORTB_5	=	0x008d
                           00008E  1229 _PORTB_6	=	0x008e
                           00008F  1230 _PORTB_7	=	0x008f
                           000090  1231 _PORTC_0	=	0x0090
                           000091  1232 _PORTC_1	=	0x0091
                           000092  1233 _PORTC_2	=	0x0092
                           000093  1234 _PORTC_3	=	0x0093
                           000094  1235 _PORTC_4	=	0x0094
                           000095  1236 _PORTC_5	=	0x0095
                           000096  1237 _PORTC_6	=	0x0096
                           000097  1238 _PORTC_7	=	0x0097
                                   1239 ;--------------------------------------------------------
                                   1240 ; overlayable register banks
                                   1241 ;--------------------------------------------------------
                                   1242 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                       1243 	.ds 8
                                   1244 ;--------------------------------------------------------
                                   1245 ; internal ram data
                                   1246 ;--------------------------------------------------------
                                   1247 	.area DSEG    (DATA)
      000032                       1248 _CRYPTO_KeySelectAndExpansion_wcnt_65536_150:
      000032                       1249 	.ds 1
      000033                       1250 _CRYPTO_KeySelectAndExpansion_w_65536_150:
      000033                       1251 	.ds 4
      000037                       1252 _CRYPTO_KeySelectAndExpansion_rcon_65536_150:
      000037                       1253 	.ds 1
      000038                       1254 _CRYPTO_KeySelectAndExpansion_sloc0_1_0:
      000038                       1255 	.ds 1
      000039                       1256 _CRYPTO_KeySelectAndExpansion_sloc1_1_0:
      000039                       1257 	.ds 2
                                   1258 ;--------------------------------------------------------
                                   1259 ; overlayable items in internal ram 
                                   1260 ;--------------------------------------------------------
                                   1261 ;--------------------------------------------------------
                                   1262 ; indirectly addressable internal ram data
                                   1263 ;--------------------------------------------------------
                                   1264 	.area ISEG    (DATA)
                                   1265 ;--------------------------------------------------------
                                   1266 ; absolute internal ram data
                                   1267 ;--------------------------------------------------------
                                   1268 	.area IABS    (ABS,DATA)
                                   1269 	.area IABS    (ABS,DATA)
                                   1270 ;--------------------------------------------------------
                                   1271 ; bit data
                                   1272 ;--------------------------------------------------------
                                   1273 	.area BSEG    (BIT)
                                   1274 ;--------------------------------------------------------
                                   1275 ; paged external ram data
                                   1276 ;--------------------------------------------------------
                                   1277 	.area PSEG    (PAG,XDATA)
                                   1278 ;--------------------------------------------------------
                                   1279 ; external ram data
                                   1280 ;--------------------------------------------------------
                                   1281 	.area XSEG    (XDATA)
                           007020  1282 _ADCCH0VAL0	=	0x7020
                           007021  1283 _ADCCH0VAL1	=	0x7021
                           007020  1284 _ADCCH0VAL	=	0x7020
                           007022  1285 _ADCCH1VAL0	=	0x7022
                           007023  1286 _ADCCH1VAL1	=	0x7023
                           007022  1287 _ADCCH1VAL	=	0x7022
                           007024  1288 _ADCCH2VAL0	=	0x7024
                           007025  1289 _ADCCH2VAL1	=	0x7025
                           007024  1290 _ADCCH2VAL	=	0x7024
                           007026  1291 _ADCCH3VAL0	=	0x7026
                           007027  1292 _ADCCH3VAL1	=	0x7027
                           007026  1293 _ADCCH3VAL	=	0x7026
                           007028  1294 _ADCTUNE0	=	0x7028
                           007029  1295 _ADCTUNE1	=	0x7029
                           00702A  1296 _ADCTUNE2	=	0x702a
                           007010  1297 _DMA0ADDR0	=	0x7010
                           007011  1298 _DMA0ADDR1	=	0x7011
                           007010  1299 _DMA0ADDR	=	0x7010
                           007014  1300 _DMA0CONFIG	=	0x7014
                           007012  1301 _DMA1ADDR0	=	0x7012
                           007013  1302 _DMA1ADDR1	=	0x7013
                           007012  1303 _DMA1ADDR	=	0x7012
                           007015  1304 _DMA1CONFIG	=	0x7015
                           007070  1305 _FRCOSCCONFIG	=	0x7070
                           007071  1306 _FRCOSCCTRL	=	0x7071
                           007076  1307 _FRCOSCFREQ0	=	0x7076
                           007077  1308 _FRCOSCFREQ1	=	0x7077
                           007076  1309 _FRCOSCFREQ	=	0x7076
                           007072  1310 _FRCOSCKFILT0	=	0x7072
                           007073  1311 _FRCOSCKFILT1	=	0x7073
                           007072  1312 _FRCOSCKFILT	=	0x7072
                           007078  1313 _FRCOSCPER0	=	0x7078
                           007079  1314 _FRCOSCPER1	=	0x7079
                           007078  1315 _FRCOSCPER	=	0x7078
                           007074  1316 _FRCOSCREF0	=	0x7074
                           007075  1317 _FRCOSCREF1	=	0x7075
                           007074  1318 _FRCOSCREF	=	0x7074
                           007007  1319 _ANALOGA	=	0x7007
                           00700C  1320 _GPIOENABLE	=	0x700c
                           007003  1321 _EXTIRQ	=	0x7003
                           007000  1322 _INTCHGA	=	0x7000
                           007001  1323 _INTCHGB	=	0x7001
                           007002  1324 _INTCHGC	=	0x7002
                           007008  1325 _PALTA	=	0x7008
                           007009  1326 _PALTB	=	0x7009
                           00700A  1327 _PALTC	=	0x700a
                           007046  1328 _PALTRADIO	=	0x7046
                           007004  1329 _PINCHGA	=	0x7004
                           007005  1330 _PINCHGB	=	0x7005
                           007006  1331 _PINCHGC	=	0x7006
                           00700B  1332 _PINSEL	=	0x700b
                           007060  1333 _LPOSCCONFIG	=	0x7060
                           007066  1334 _LPOSCFREQ0	=	0x7066
                           007067  1335 _LPOSCFREQ1	=	0x7067
                           007066  1336 _LPOSCFREQ	=	0x7066
                           007062  1337 _LPOSCKFILT0	=	0x7062
                           007063  1338 _LPOSCKFILT1	=	0x7063
                           007062  1339 _LPOSCKFILT	=	0x7062
                           007068  1340 _LPOSCPER0	=	0x7068
                           007069  1341 _LPOSCPER1	=	0x7069
                           007068  1342 _LPOSCPER	=	0x7068
                           007064  1343 _LPOSCREF0	=	0x7064
                           007065  1344 _LPOSCREF1	=	0x7065
                           007064  1345 _LPOSCREF	=	0x7064
                           007054  1346 _LPXOSCGM	=	0x7054
                           007F01  1347 _MISCCTRL	=	0x7f01
                           007053  1348 _OSCCALIB	=	0x7053
                           007050  1349 _OSCFORCERUN	=	0x7050
                           007052  1350 _OSCREADY	=	0x7052
                           007051  1351 _OSCRUN	=	0x7051
                           007040  1352 _RADIOFDATAADDR0	=	0x7040
                           007041  1353 _RADIOFDATAADDR1	=	0x7041
                           007040  1354 _RADIOFDATAADDR	=	0x7040
                           007042  1355 _RADIOFSTATADDR0	=	0x7042
                           007043  1356 _RADIOFSTATADDR1	=	0x7043
                           007042  1357 _RADIOFSTATADDR	=	0x7042
                           007044  1358 _RADIOMUX	=	0x7044
                           007084  1359 _SCRATCH0	=	0x7084
                           007085  1360 _SCRATCH1	=	0x7085
                           007086  1361 _SCRATCH2	=	0x7086
                           007087  1362 _SCRATCH3	=	0x7087
                           007F00  1363 _SILICONREV	=	0x7f00
                           007F19  1364 _XTALAMPL	=	0x7f19
                           007F18  1365 _XTALOSC	=	0x7f18
                           007F1A  1366 _XTALREADY	=	0x7f1a
                           007081  1367 _RNGBYTE	=	0x7081
                           007091  1368 _AESCONFIG	=	0x7091
                           007098  1369 _AESCURBLOCK	=	0x7098
                           007094  1370 _AESINADDR0	=	0x7094
                           007095  1371 _AESINADDR1	=	0x7095
                           007094  1372 _AESINADDR	=	0x7094
                           007092  1373 _AESKEYADDR0	=	0x7092
                           007093  1374 _AESKEYADDR1	=	0x7093
                           007092  1375 _AESKEYADDR	=	0x7092
                           007090  1376 _AESMODE	=	0x7090
                           007096  1377 _AESOUTADDR0	=	0x7096
                           007097  1378 _AESOUTADDR1	=	0x7097
                           007096  1379 _AESOUTADDR	=	0x7096
                           007080  1380 _RNGMODE	=	0x7080
                           007082  1381 _RNGCLKSRC0	=	0x7082
                           007083  1382 _RNGCLKSRC1	=	0x7083
                           004114  1383 _AX5043_AFSKCTRL	=	0x4114
                           004113  1384 _AX5043_AFSKMARK0	=	0x4113
                           004112  1385 _AX5043_AFSKMARK1	=	0x4112
                           004111  1386 _AX5043_AFSKSPACE0	=	0x4111
                           004110  1387 _AX5043_AFSKSPACE1	=	0x4110
                           004043  1388 _AX5043_AGCCOUNTER	=	0x4043
                           004115  1389 _AX5043_AMPLFILTER	=	0x4115
                           004189  1390 _AX5043_BBOFFSCAP	=	0x4189
                           004188  1391 _AX5043_BBTUNE	=	0x4188
                           004041  1392 _AX5043_BGNDRSSI	=	0x4041
                           00422E  1393 _AX5043_BGNDRSSIGAIN	=	0x422e
                           00422F  1394 _AX5043_BGNDRSSITHR	=	0x422f
                           004017  1395 _AX5043_CRCINIT0	=	0x4017
                           004016  1396 _AX5043_CRCINIT1	=	0x4016
                           004015  1397 _AX5043_CRCINIT2	=	0x4015
                           004014  1398 _AX5043_CRCINIT3	=	0x4014
                           004332  1399 _AX5043_DACCONFIG	=	0x4332
                           004331  1400 _AX5043_DACVALUE0	=	0x4331
                           004330  1401 _AX5043_DACVALUE1	=	0x4330
                           004102  1402 _AX5043_DECIMATION	=	0x4102
                           004042  1403 _AX5043_DIVERSITY	=	0x4042
                           004011  1404 _AX5043_ENCODING	=	0x4011
                           004018  1405 _AX5043_FEC	=	0x4018
                           00401A  1406 _AX5043_FECSTATUS	=	0x401a
                           004019  1407 _AX5043_FECSYNC	=	0x4019
                           00402B  1408 _AX5043_FIFOCOUNT0	=	0x402b
                           00402A  1409 _AX5043_FIFOCOUNT1	=	0x402a
                           004029  1410 _AX5043_FIFODATA	=	0x4029
                           00402D  1411 _AX5043_FIFOFREE0	=	0x402d
                           00402C  1412 _AX5043_FIFOFREE1	=	0x402c
                           004028  1413 _AX5043_FIFOSTAT	=	0x4028
                           00402F  1414 _AX5043_FIFOTHRESH0	=	0x402f
                           00402E  1415 _AX5043_FIFOTHRESH1	=	0x402e
                           004012  1416 _AX5043_FRAMING	=	0x4012
                           004037  1417 _AX5043_FREQA0	=	0x4037
                           004036  1418 _AX5043_FREQA1	=	0x4036
                           004035  1419 _AX5043_FREQA2	=	0x4035
                           004034  1420 _AX5043_FREQA3	=	0x4034
                           00403F  1421 _AX5043_FREQB0	=	0x403f
                           00403E  1422 _AX5043_FREQB1	=	0x403e
                           00403D  1423 _AX5043_FREQB2	=	0x403d
                           00403C  1424 _AX5043_FREQB3	=	0x403c
                           004163  1425 _AX5043_FSKDEV0	=	0x4163
                           004162  1426 _AX5043_FSKDEV1	=	0x4162
                           004161  1427 _AX5043_FSKDEV2	=	0x4161
                           00410D  1428 _AX5043_FSKDMAX0	=	0x410d
                           00410C  1429 _AX5043_FSKDMAX1	=	0x410c
                           00410F  1430 _AX5043_FSKDMIN0	=	0x410f
                           00410E  1431 _AX5043_FSKDMIN1	=	0x410e
                           004309  1432 _AX5043_GPADC13VALUE0	=	0x4309
                           004308  1433 _AX5043_GPADC13VALUE1	=	0x4308
                           004300  1434 _AX5043_GPADCCTRL	=	0x4300
                           004301  1435 _AX5043_GPADCPERIOD	=	0x4301
                           004101  1436 _AX5043_IFFREQ0	=	0x4101
                           004100  1437 _AX5043_IFFREQ1	=	0x4100
                           00400B  1438 _AX5043_IRQINVERSION0	=	0x400b
                           00400A  1439 _AX5043_IRQINVERSION1	=	0x400a
                           004007  1440 _AX5043_IRQMASK0	=	0x4007
                           004006  1441 _AX5043_IRQMASK1	=	0x4006
                           00400D  1442 _AX5043_IRQREQUEST0	=	0x400d
                           00400C  1443 _AX5043_IRQREQUEST1	=	0x400c
                           004310  1444 _AX5043_LPOSCCONFIG	=	0x4310
                           004317  1445 _AX5043_LPOSCFREQ0	=	0x4317
                           004316  1446 _AX5043_LPOSCFREQ1	=	0x4316
                           004313  1447 _AX5043_LPOSCKFILT0	=	0x4313
                           004312  1448 _AX5043_LPOSCKFILT1	=	0x4312
                           004319  1449 _AX5043_LPOSCPER0	=	0x4319
                           004318  1450 _AX5043_LPOSCPER1	=	0x4318
                           004315  1451 _AX5043_LPOSCREF0	=	0x4315
                           004314  1452 _AX5043_LPOSCREF1	=	0x4314
                           004311  1453 _AX5043_LPOSCSTATUS	=	0x4311
                           004214  1454 _AX5043_MATCH0LEN	=	0x4214
                           004216  1455 _AX5043_MATCH0MAX	=	0x4216
                           004215  1456 _AX5043_MATCH0MIN	=	0x4215
                           004213  1457 _AX5043_MATCH0PAT0	=	0x4213
                           004212  1458 _AX5043_MATCH0PAT1	=	0x4212
                           004211  1459 _AX5043_MATCH0PAT2	=	0x4211
                           004210  1460 _AX5043_MATCH0PAT3	=	0x4210
                           00421C  1461 _AX5043_MATCH1LEN	=	0x421c
                           00421E  1462 _AX5043_MATCH1MAX	=	0x421e
                           00421D  1463 _AX5043_MATCH1MIN	=	0x421d
                           004219  1464 _AX5043_MATCH1PAT0	=	0x4219
                           004218  1465 _AX5043_MATCH1PAT1	=	0x4218
                           004108  1466 _AX5043_MAXDROFFSET0	=	0x4108
                           004107  1467 _AX5043_MAXDROFFSET1	=	0x4107
                           004106  1468 _AX5043_MAXDROFFSET2	=	0x4106
                           00410B  1469 _AX5043_MAXRFOFFSET0	=	0x410b
                           00410A  1470 _AX5043_MAXRFOFFSET1	=	0x410a
                           004109  1471 _AX5043_MAXRFOFFSET2	=	0x4109
                           004164  1472 _AX5043_MODCFGA	=	0x4164
                           004160  1473 _AX5043_MODCFGF	=	0x4160
                           004F5F  1474 _AX5043_MODCFGP	=	0x4f5f
                           004010  1475 _AX5043_MODULATION	=	0x4010
                           004025  1476 _AX5043_PINFUNCANTSEL	=	0x4025
                           004023  1477 _AX5043_PINFUNCDATA	=	0x4023
                           004022  1478 _AX5043_PINFUNCDCLK	=	0x4022
                           004024  1479 _AX5043_PINFUNCIRQ	=	0x4024
                           004026  1480 _AX5043_PINFUNCPWRAMP	=	0x4026
                           004021  1481 _AX5043_PINFUNCSYSCLK	=	0x4021
                           004020  1482 _AX5043_PINSTATE	=	0x4020
                           004233  1483 _AX5043_PKTACCEPTFLAGS	=	0x4233
                           004230  1484 _AX5043_PKTCHUNKSIZE	=	0x4230
                           004231  1485 _AX5043_PKTMISCFLAGS	=	0x4231
                           004232  1486 _AX5043_PKTSTOREFLAGS	=	0x4232
                           004031  1487 _AX5043_PLLCPI	=	0x4031
                           004039  1488 _AX5043_PLLCPIBOOST	=	0x4039
                           004182  1489 _AX5043_PLLLOCKDET	=	0x4182
                           004030  1490 _AX5043_PLLLOOP	=	0x4030
                           004038  1491 _AX5043_PLLLOOPBOOST	=	0x4038
                           004033  1492 _AX5043_PLLRANGINGA	=	0x4033
                           00403B  1493 _AX5043_PLLRANGINGB	=	0x403b
                           004183  1494 _AX5043_PLLRNGCLK	=	0x4183
                           004032  1495 _AX5043_PLLVCODIV	=	0x4032
                           004180  1496 _AX5043_PLLVCOI	=	0x4180
                           004181  1497 _AX5043_PLLVCOIR	=	0x4181
                           004F08  1498 _AX5043_POWCTRL1	=	0x4f08
                           004005  1499 _AX5043_POWIRQMASK	=	0x4005
                           004003  1500 _AX5043_POWSTAT	=	0x4003
                           004004  1501 _AX5043_POWSTICKYSTAT	=	0x4004
                           004027  1502 _AX5043_PWRAMP	=	0x4027
                           004002  1503 _AX5043_PWRMODE	=	0x4002
                           004009  1504 _AX5043_RADIOEVENTMASK0	=	0x4009
                           004008  1505 _AX5043_RADIOEVENTMASK1	=	0x4008
                           00400F  1506 _AX5043_RADIOEVENTREQ0	=	0x400f
                           00400E  1507 _AX5043_RADIOEVENTREQ1	=	0x400e
                           00401C  1508 _AX5043_RADIOSTATE	=	0x401c
                           004F0D  1509 _AX5043_REF	=	0x4f0d
                           004040  1510 _AX5043_RSSI	=	0x4040
                           00422D  1511 _AX5043_RSSIABSTHR	=	0x422d
                           00422C  1512 _AX5043_RSSIREFERENCE	=	0x422c
                           004105  1513 _AX5043_RXDATARATE0	=	0x4105
                           004104  1514 _AX5043_RXDATARATE1	=	0x4104
                           004103  1515 _AX5043_RXDATARATE2	=	0x4103
                           004001  1516 _AX5043_SCRATCH	=	0x4001
                           004000  1517 _AX5043_SILICONREVISION	=	0x4000
                           00405B  1518 _AX5043_TIMER0	=	0x405b
                           00405A  1519 _AX5043_TIMER1	=	0x405a
                           004059  1520 _AX5043_TIMER2	=	0x4059
                           004227  1521 _AX5043_TMGRXAGC	=	0x4227
                           004223  1522 _AX5043_TMGRXBOOST	=	0x4223
                           004226  1523 _AX5043_TMGRXCOARSEAGC	=	0x4226
                           004225  1524 _AX5043_TMGRXOFFSACQ	=	0x4225
                           004229  1525 _AX5043_TMGRXPREAMBLE1	=	0x4229
                           00422A  1526 _AX5043_TMGRXPREAMBLE2	=	0x422a
                           00422B  1527 _AX5043_TMGRXPREAMBLE3	=	0x422b
                           004228  1528 _AX5043_TMGRXRSSI	=	0x4228
                           004224  1529 _AX5043_TMGRXSETTLE	=	0x4224
                           004220  1530 _AX5043_TMGTXBOOST	=	0x4220
                           004221  1531 _AX5043_TMGTXSETTLE	=	0x4221
                           004055  1532 _AX5043_TRKAFSKDEMOD0	=	0x4055
                           004054  1533 _AX5043_TRKAFSKDEMOD1	=	0x4054
                           004049  1534 _AX5043_TRKAMPLITUDE0	=	0x4049
                           004048  1535 _AX5043_TRKAMPLITUDE1	=	0x4048
                           004047  1536 _AX5043_TRKDATARATE0	=	0x4047
                           004046  1537 _AX5043_TRKDATARATE1	=	0x4046
                           004045  1538 _AX5043_TRKDATARATE2	=	0x4045
                           004051  1539 _AX5043_TRKFREQ0	=	0x4051
                           004050  1540 _AX5043_TRKFREQ1	=	0x4050
                           004053  1541 _AX5043_TRKFSKDEMOD0	=	0x4053
                           004052  1542 _AX5043_TRKFSKDEMOD1	=	0x4052
                           00404B  1543 _AX5043_TRKPHASE0	=	0x404b
                           00404A  1544 _AX5043_TRKPHASE1	=	0x404a
                           00404F  1545 _AX5043_TRKRFFREQ0	=	0x404f
                           00404E  1546 _AX5043_TRKRFFREQ1	=	0x404e
                           00404D  1547 _AX5043_TRKRFFREQ2	=	0x404d
                           004169  1548 _AX5043_TXPWRCOEFFA0	=	0x4169
                           004168  1549 _AX5043_TXPWRCOEFFA1	=	0x4168
                           00416B  1550 _AX5043_TXPWRCOEFFB0	=	0x416b
                           00416A  1551 _AX5043_TXPWRCOEFFB1	=	0x416a
                           00416D  1552 _AX5043_TXPWRCOEFFC0	=	0x416d
                           00416C  1553 _AX5043_TXPWRCOEFFC1	=	0x416c
                           00416F  1554 _AX5043_TXPWRCOEFFD0	=	0x416f
                           00416E  1555 _AX5043_TXPWRCOEFFD1	=	0x416e
                           004171  1556 _AX5043_TXPWRCOEFFE0	=	0x4171
                           004170  1557 _AX5043_TXPWRCOEFFE1	=	0x4170
                           004167  1558 _AX5043_TXRATE0	=	0x4167
                           004166  1559 _AX5043_TXRATE1	=	0x4166
                           004165  1560 _AX5043_TXRATE2	=	0x4165
                           00406B  1561 _AX5043_WAKEUP0	=	0x406b
                           00406A  1562 _AX5043_WAKEUP1	=	0x406a
                           00406D  1563 _AX5043_WAKEUPFREQ0	=	0x406d
                           00406C  1564 _AX5043_WAKEUPFREQ1	=	0x406c
                           004069  1565 _AX5043_WAKEUPTIMER0	=	0x4069
                           004068  1566 _AX5043_WAKEUPTIMER1	=	0x4068
                           00406E  1567 _AX5043_WAKEUPXOEARLY	=	0x406e
                           004F11  1568 _AX5043_XTALAMPL	=	0x4f11
                           004184  1569 _AX5043_XTALCAP	=	0x4184
                           004F10  1570 _AX5043_XTALOSC	=	0x4f10
                           00401D  1571 _AX5043_XTALSTATUS	=	0x401d
                           004F00  1572 _AX5043_0xF00	=	0x4f00
                           004F0C  1573 _AX5043_0xF0C	=	0x4f0c
                           004F18  1574 _AX5043_0xF18	=	0x4f18
                           004F1C  1575 _AX5043_0xF1C	=	0x4f1c
                           004F21  1576 _AX5043_0xF21	=	0x4f21
                           004F22  1577 _AX5043_0xF22	=	0x4f22
                           004F23  1578 _AX5043_0xF23	=	0x4f23
                           004F26  1579 _AX5043_0xF26	=	0x4f26
                           004F30  1580 _AX5043_0xF30	=	0x4f30
                           004F31  1581 _AX5043_0xF31	=	0x4f31
                           004F32  1582 _AX5043_0xF32	=	0x4f32
                           004F33  1583 _AX5043_0xF33	=	0x4f33
                           004F34  1584 _AX5043_0xF34	=	0x4f34
                           004F35  1585 _AX5043_0xF35	=	0x4f35
                           004F44  1586 _AX5043_0xF44	=	0x4f44
                           004122  1587 _AX5043_AGCAHYST0	=	0x4122
                           004132  1588 _AX5043_AGCAHYST1	=	0x4132
                           004142  1589 _AX5043_AGCAHYST2	=	0x4142
                           004152  1590 _AX5043_AGCAHYST3	=	0x4152
                           004120  1591 _AX5043_AGCGAIN0	=	0x4120
                           004130  1592 _AX5043_AGCGAIN1	=	0x4130
                           004140  1593 _AX5043_AGCGAIN2	=	0x4140
                           004150  1594 _AX5043_AGCGAIN3	=	0x4150
                           004123  1595 _AX5043_AGCMINMAX0	=	0x4123
                           004133  1596 _AX5043_AGCMINMAX1	=	0x4133
                           004143  1597 _AX5043_AGCMINMAX2	=	0x4143
                           004153  1598 _AX5043_AGCMINMAX3	=	0x4153
                           004121  1599 _AX5043_AGCTARGET0	=	0x4121
                           004131  1600 _AX5043_AGCTARGET1	=	0x4131
                           004141  1601 _AX5043_AGCTARGET2	=	0x4141
                           004151  1602 _AX5043_AGCTARGET3	=	0x4151
                           00412B  1603 _AX5043_AMPLITUDEGAIN0	=	0x412b
                           00413B  1604 _AX5043_AMPLITUDEGAIN1	=	0x413b
                           00414B  1605 _AX5043_AMPLITUDEGAIN2	=	0x414b
                           00415B  1606 _AX5043_AMPLITUDEGAIN3	=	0x415b
                           00412F  1607 _AX5043_BBOFFSRES0	=	0x412f
                           00413F  1608 _AX5043_BBOFFSRES1	=	0x413f
                           00414F  1609 _AX5043_BBOFFSRES2	=	0x414f
                           00415F  1610 _AX5043_BBOFFSRES3	=	0x415f
                           004125  1611 _AX5043_DRGAIN0	=	0x4125
                           004135  1612 _AX5043_DRGAIN1	=	0x4135
                           004145  1613 _AX5043_DRGAIN2	=	0x4145
                           004155  1614 _AX5043_DRGAIN3	=	0x4155
                           00412E  1615 _AX5043_FOURFSK0	=	0x412e
                           00413E  1616 _AX5043_FOURFSK1	=	0x413e
                           00414E  1617 _AX5043_FOURFSK2	=	0x414e
                           00415E  1618 _AX5043_FOURFSK3	=	0x415e
                           00412D  1619 _AX5043_FREQDEV00	=	0x412d
                           00413D  1620 _AX5043_FREQDEV01	=	0x413d
                           00414D  1621 _AX5043_FREQDEV02	=	0x414d
                           00415D  1622 _AX5043_FREQDEV03	=	0x415d
                           00412C  1623 _AX5043_FREQDEV10	=	0x412c
                           00413C  1624 _AX5043_FREQDEV11	=	0x413c
                           00414C  1625 _AX5043_FREQDEV12	=	0x414c
                           00415C  1626 _AX5043_FREQDEV13	=	0x415c
                           004127  1627 _AX5043_FREQUENCYGAINA0	=	0x4127
                           004137  1628 _AX5043_FREQUENCYGAINA1	=	0x4137
                           004147  1629 _AX5043_FREQUENCYGAINA2	=	0x4147
                           004157  1630 _AX5043_FREQUENCYGAINA3	=	0x4157
                           004128  1631 _AX5043_FREQUENCYGAINB0	=	0x4128
                           004138  1632 _AX5043_FREQUENCYGAINB1	=	0x4138
                           004148  1633 _AX5043_FREQUENCYGAINB2	=	0x4148
                           004158  1634 _AX5043_FREQUENCYGAINB3	=	0x4158
                           004129  1635 _AX5043_FREQUENCYGAINC0	=	0x4129
                           004139  1636 _AX5043_FREQUENCYGAINC1	=	0x4139
                           004149  1637 _AX5043_FREQUENCYGAINC2	=	0x4149
                           004159  1638 _AX5043_FREQUENCYGAINC3	=	0x4159
                           00412A  1639 _AX5043_FREQUENCYGAIND0	=	0x412a
                           00413A  1640 _AX5043_FREQUENCYGAIND1	=	0x413a
                           00414A  1641 _AX5043_FREQUENCYGAIND2	=	0x414a
                           00415A  1642 _AX5043_FREQUENCYGAIND3	=	0x415a
                           004116  1643 _AX5043_FREQUENCYLEAK	=	0x4116
                           004126  1644 _AX5043_PHASEGAIN0	=	0x4126
                           004136  1645 _AX5043_PHASEGAIN1	=	0x4136
                           004146  1646 _AX5043_PHASEGAIN2	=	0x4146
                           004156  1647 _AX5043_PHASEGAIN3	=	0x4156
                           004207  1648 _AX5043_PKTADDR0	=	0x4207
                           004206  1649 _AX5043_PKTADDR1	=	0x4206
                           004205  1650 _AX5043_PKTADDR2	=	0x4205
                           004204  1651 _AX5043_PKTADDR3	=	0x4204
                           004200  1652 _AX5043_PKTADDRCFG	=	0x4200
                           00420B  1653 _AX5043_PKTADDRMASK0	=	0x420b
                           00420A  1654 _AX5043_PKTADDRMASK1	=	0x420a
                           004209  1655 _AX5043_PKTADDRMASK2	=	0x4209
                           004208  1656 _AX5043_PKTADDRMASK3	=	0x4208
                           004201  1657 _AX5043_PKTLENCFG	=	0x4201
                           004202  1658 _AX5043_PKTLENOFFSET	=	0x4202
                           004203  1659 _AX5043_PKTMAXLEN	=	0x4203
                           004118  1660 _AX5043_RXPARAMCURSET	=	0x4118
                           004117  1661 _AX5043_RXPARAMSETS	=	0x4117
                           004124  1662 _AX5043_TIMEGAIN0	=	0x4124
                           004134  1663 _AX5043_TIMEGAIN1	=	0x4134
                           004144  1664 _AX5043_TIMEGAIN2	=	0x4144
                           004154  1665 _AX5043_TIMEGAIN3	=	0x4154
                           005114  1666 _AX5043_AFSKCTRLNB	=	0x5114
                           005113  1667 _AX5043_AFSKMARK0NB	=	0x5113
                           005112  1668 _AX5043_AFSKMARK1NB	=	0x5112
                           005111  1669 _AX5043_AFSKSPACE0NB	=	0x5111
                           005110  1670 _AX5043_AFSKSPACE1NB	=	0x5110
                           005043  1671 _AX5043_AGCCOUNTERNB	=	0x5043
                           005115  1672 _AX5043_AMPLFILTERNB	=	0x5115
                           005189  1673 _AX5043_BBOFFSCAPNB	=	0x5189
                           005188  1674 _AX5043_BBTUNENB	=	0x5188
                           005041  1675 _AX5043_BGNDRSSINB	=	0x5041
                           00522E  1676 _AX5043_BGNDRSSIGAINNB	=	0x522e
                           00522F  1677 _AX5043_BGNDRSSITHRNB	=	0x522f
                           005017  1678 _AX5043_CRCINIT0NB	=	0x5017
                           005016  1679 _AX5043_CRCINIT1NB	=	0x5016
                           005015  1680 _AX5043_CRCINIT2NB	=	0x5015
                           005014  1681 _AX5043_CRCINIT3NB	=	0x5014
                           005332  1682 _AX5043_DACCONFIGNB	=	0x5332
                           005331  1683 _AX5043_DACVALUE0NB	=	0x5331
                           005330  1684 _AX5043_DACVALUE1NB	=	0x5330
                           005102  1685 _AX5043_DECIMATIONNB	=	0x5102
                           005042  1686 _AX5043_DIVERSITYNB	=	0x5042
                           005011  1687 _AX5043_ENCODINGNB	=	0x5011
                           005018  1688 _AX5043_FECNB	=	0x5018
                           00501A  1689 _AX5043_FECSTATUSNB	=	0x501a
                           005019  1690 _AX5043_FECSYNCNB	=	0x5019
                           00502B  1691 _AX5043_FIFOCOUNT0NB	=	0x502b
                           00502A  1692 _AX5043_FIFOCOUNT1NB	=	0x502a
                           005029  1693 _AX5043_FIFODATANB	=	0x5029
                           00502D  1694 _AX5043_FIFOFREE0NB	=	0x502d
                           00502C  1695 _AX5043_FIFOFREE1NB	=	0x502c
                           005028  1696 _AX5043_FIFOSTATNB	=	0x5028
                           00502F  1697 _AX5043_FIFOTHRESH0NB	=	0x502f
                           00502E  1698 _AX5043_FIFOTHRESH1NB	=	0x502e
                           005012  1699 _AX5043_FRAMINGNB	=	0x5012
                           005037  1700 _AX5043_FREQA0NB	=	0x5037
                           005036  1701 _AX5043_FREQA1NB	=	0x5036
                           005035  1702 _AX5043_FREQA2NB	=	0x5035
                           005034  1703 _AX5043_FREQA3NB	=	0x5034
                           00503F  1704 _AX5043_FREQB0NB	=	0x503f
                           00503E  1705 _AX5043_FREQB1NB	=	0x503e
                           00503D  1706 _AX5043_FREQB2NB	=	0x503d
                           00503C  1707 _AX5043_FREQB3NB	=	0x503c
                           005163  1708 _AX5043_FSKDEV0NB	=	0x5163
                           005162  1709 _AX5043_FSKDEV1NB	=	0x5162
                           005161  1710 _AX5043_FSKDEV2NB	=	0x5161
                           00510D  1711 _AX5043_FSKDMAX0NB	=	0x510d
                           00510C  1712 _AX5043_FSKDMAX1NB	=	0x510c
                           00510F  1713 _AX5043_FSKDMIN0NB	=	0x510f
                           00510E  1714 _AX5043_FSKDMIN1NB	=	0x510e
                           005309  1715 _AX5043_GPADC13VALUE0NB	=	0x5309
                           005308  1716 _AX5043_GPADC13VALUE1NB	=	0x5308
                           005300  1717 _AX5043_GPADCCTRLNB	=	0x5300
                           005301  1718 _AX5043_GPADCPERIODNB	=	0x5301
                           005101  1719 _AX5043_IFFREQ0NB	=	0x5101
                           005100  1720 _AX5043_IFFREQ1NB	=	0x5100
                           00500B  1721 _AX5043_IRQINVERSION0NB	=	0x500b
                           00500A  1722 _AX5043_IRQINVERSION1NB	=	0x500a
                           005007  1723 _AX5043_IRQMASK0NB	=	0x5007
                           005006  1724 _AX5043_IRQMASK1NB	=	0x5006
                           00500D  1725 _AX5043_IRQREQUEST0NB	=	0x500d
                           00500C  1726 _AX5043_IRQREQUEST1NB	=	0x500c
                           005310  1727 _AX5043_LPOSCCONFIGNB	=	0x5310
                           005317  1728 _AX5043_LPOSCFREQ0NB	=	0x5317
                           005316  1729 _AX5043_LPOSCFREQ1NB	=	0x5316
                           005313  1730 _AX5043_LPOSCKFILT0NB	=	0x5313
                           005312  1731 _AX5043_LPOSCKFILT1NB	=	0x5312
                           005319  1732 _AX5043_LPOSCPER0NB	=	0x5319
                           005318  1733 _AX5043_LPOSCPER1NB	=	0x5318
                           005315  1734 _AX5043_LPOSCREF0NB	=	0x5315
                           005314  1735 _AX5043_LPOSCREF1NB	=	0x5314
                           005311  1736 _AX5043_LPOSCSTATUSNB	=	0x5311
                           005214  1737 _AX5043_MATCH0LENNB	=	0x5214
                           005216  1738 _AX5043_MATCH0MAXNB	=	0x5216
                           005215  1739 _AX5043_MATCH0MINNB	=	0x5215
                           005213  1740 _AX5043_MATCH0PAT0NB	=	0x5213
                           005212  1741 _AX5043_MATCH0PAT1NB	=	0x5212
                           005211  1742 _AX5043_MATCH0PAT2NB	=	0x5211
                           005210  1743 _AX5043_MATCH0PAT3NB	=	0x5210
                           00521C  1744 _AX5043_MATCH1LENNB	=	0x521c
                           00521E  1745 _AX5043_MATCH1MAXNB	=	0x521e
                           00521D  1746 _AX5043_MATCH1MINNB	=	0x521d
                           005219  1747 _AX5043_MATCH1PAT0NB	=	0x5219
                           005218  1748 _AX5043_MATCH1PAT1NB	=	0x5218
                           005108  1749 _AX5043_MAXDROFFSET0NB	=	0x5108
                           005107  1750 _AX5043_MAXDROFFSET1NB	=	0x5107
                           005106  1751 _AX5043_MAXDROFFSET2NB	=	0x5106
                           00510B  1752 _AX5043_MAXRFOFFSET0NB	=	0x510b
                           00510A  1753 _AX5043_MAXRFOFFSET1NB	=	0x510a
                           005109  1754 _AX5043_MAXRFOFFSET2NB	=	0x5109
                           005164  1755 _AX5043_MODCFGANB	=	0x5164
                           005160  1756 _AX5043_MODCFGFNB	=	0x5160
                           005F5F  1757 _AX5043_MODCFGPNB	=	0x5f5f
                           005010  1758 _AX5043_MODULATIONNB	=	0x5010
                           005025  1759 _AX5043_PINFUNCANTSELNB	=	0x5025
                           005023  1760 _AX5043_PINFUNCDATANB	=	0x5023
                           005022  1761 _AX5043_PINFUNCDCLKNB	=	0x5022
                           005024  1762 _AX5043_PINFUNCIRQNB	=	0x5024
                           005026  1763 _AX5043_PINFUNCPWRAMPNB	=	0x5026
                           005021  1764 _AX5043_PINFUNCSYSCLKNB	=	0x5021
                           005020  1765 _AX5043_PINSTATENB	=	0x5020
                           005233  1766 _AX5043_PKTACCEPTFLAGSNB	=	0x5233
                           005230  1767 _AX5043_PKTCHUNKSIZENB	=	0x5230
                           005231  1768 _AX5043_PKTMISCFLAGSNB	=	0x5231
                           005232  1769 _AX5043_PKTSTOREFLAGSNB	=	0x5232
                           005031  1770 _AX5043_PLLCPINB	=	0x5031
                           005039  1771 _AX5043_PLLCPIBOOSTNB	=	0x5039
                           005182  1772 _AX5043_PLLLOCKDETNB	=	0x5182
                           005030  1773 _AX5043_PLLLOOPNB	=	0x5030
                           005038  1774 _AX5043_PLLLOOPBOOSTNB	=	0x5038
                           005033  1775 _AX5043_PLLRANGINGANB	=	0x5033
                           00503B  1776 _AX5043_PLLRANGINGBNB	=	0x503b
                           005183  1777 _AX5043_PLLRNGCLKNB	=	0x5183
                           005032  1778 _AX5043_PLLVCODIVNB	=	0x5032
                           005180  1779 _AX5043_PLLVCOINB	=	0x5180
                           005181  1780 _AX5043_PLLVCOIRNB	=	0x5181
                           005F08  1781 _AX5043_POWCTRL1NB	=	0x5f08
                           005005  1782 _AX5043_POWIRQMASKNB	=	0x5005
                           005003  1783 _AX5043_POWSTATNB	=	0x5003
                           005004  1784 _AX5043_POWSTICKYSTATNB	=	0x5004
                           005027  1785 _AX5043_PWRAMPNB	=	0x5027
                           005002  1786 _AX5043_PWRMODENB	=	0x5002
                           005009  1787 _AX5043_RADIOEVENTMASK0NB	=	0x5009
                           005008  1788 _AX5043_RADIOEVENTMASK1NB	=	0x5008
                           00500F  1789 _AX5043_RADIOEVENTREQ0NB	=	0x500f
                           00500E  1790 _AX5043_RADIOEVENTREQ1NB	=	0x500e
                           00501C  1791 _AX5043_RADIOSTATENB	=	0x501c
                           005F0D  1792 _AX5043_REFNB	=	0x5f0d
                           005040  1793 _AX5043_RSSINB	=	0x5040
                           00522D  1794 _AX5043_RSSIABSTHRNB	=	0x522d
                           00522C  1795 _AX5043_RSSIREFERENCENB	=	0x522c
                           005105  1796 _AX5043_RXDATARATE0NB	=	0x5105
                           005104  1797 _AX5043_RXDATARATE1NB	=	0x5104
                           005103  1798 _AX5043_RXDATARATE2NB	=	0x5103
                           005001  1799 _AX5043_SCRATCHNB	=	0x5001
                           005000  1800 _AX5043_SILICONREVISIONNB	=	0x5000
                           00505B  1801 _AX5043_TIMER0NB	=	0x505b
                           00505A  1802 _AX5043_TIMER1NB	=	0x505a
                           005059  1803 _AX5043_TIMER2NB	=	0x5059
                           005227  1804 _AX5043_TMGRXAGCNB	=	0x5227
                           005223  1805 _AX5043_TMGRXBOOSTNB	=	0x5223
                           005226  1806 _AX5043_TMGRXCOARSEAGCNB	=	0x5226
                           005225  1807 _AX5043_TMGRXOFFSACQNB	=	0x5225
                           005229  1808 _AX5043_TMGRXPREAMBLE1NB	=	0x5229
                           00522A  1809 _AX5043_TMGRXPREAMBLE2NB	=	0x522a
                           00522B  1810 _AX5043_TMGRXPREAMBLE3NB	=	0x522b
                           005228  1811 _AX5043_TMGRXRSSINB	=	0x5228
                           005224  1812 _AX5043_TMGRXSETTLENB	=	0x5224
                           005220  1813 _AX5043_TMGTXBOOSTNB	=	0x5220
                           005221  1814 _AX5043_TMGTXSETTLENB	=	0x5221
                           005055  1815 _AX5043_TRKAFSKDEMOD0NB	=	0x5055
                           005054  1816 _AX5043_TRKAFSKDEMOD1NB	=	0x5054
                           005049  1817 _AX5043_TRKAMPLITUDE0NB	=	0x5049
                           005048  1818 _AX5043_TRKAMPLITUDE1NB	=	0x5048
                           005047  1819 _AX5043_TRKDATARATE0NB	=	0x5047
                           005046  1820 _AX5043_TRKDATARATE1NB	=	0x5046
                           005045  1821 _AX5043_TRKDATARATE2NB	=	0x5045
                           005051  1822 _AX5043_TRKFREQ0NB	=	0x5051
                           005050  1823 _AX5043_TRKFREQ1NB	=	0x5050
                           005053  1824 _AX5043_TRKFSKDEMOD0NB	=	0x5053
                           005052  1825 _AX5043_TRKFSKDEMOD1NB	=	0x5052
                           00504B  1826 _AX5043_TRKPHASE0NB	=	0x504b
                           00504A  1827 _AX5043_TRKPHASE1NB	=	0x504a
                           00504F  1828 _AX5043_TRKRFFREQ0NB	=	0x504f
                           00504E  1829 _AX5043_TRKRFFREQ1NB	=	0x504e
                           00504D  1830 _AX5043_TRKRFFREQ2NB	=	0x504d
                           005169  1831 _AX5043_TXPWRCOEFFA0NB	=	0x5169
                           005168  1832 _AX5043_TXPWRCOEFFA1NB	=	0x5168
                           00516B  1833 _AX5043_TXPWRCOEFFB0NB	=	0x516b
                           00516A  1834 _AX5043_TXPWRCOEFFB1NB	=	0x516a
                           00516D  1835 _AX5043_TXPWRCOEFFC0NB	=	0x516d
                           00516C  1836 _AX5043_TXPWRCOEFFC1NB	=	0x516c
                           00516F  1837 _AX5043_TXPWRCOEFFD0NB	=	0x516f
                           00516E  1838 _AX5043_TXPWRCOEFFD1NB	=	0x516e
                           005171  1839 _AX5043_TXPWRCOEFFE0NB	=	0x5171
                           005170  1840 _AX5043_TXPWRCOEFFE1NB	=	0x5170
                           005167  1841 _AX5043_TXRATE0NB	=	0x5167
                           005166  1842 _AX5043_TXRATE1NB	=	0x5166
                           005165  1843 _AX5043_TXRATE2NB	=	0x5165
                           00506B  1844 _AX5043_WAKEUP0NB	=	0x506b
                           00506A  1845 _AX5043_WAKEUP1NB	=	0x506a
                           00506D  1846 _AX5043_WAKEUPFREQ0NB	=	0x506d
                           00506C  1847 _AX5043_WAKEUPFREQ1NB	=	0x506c
                           005069  1848 _AX5043_WAKEUPTIMER0NB	=	0x5069
                           005068  1849 _AX5043_WAKEUPTIMER1NB	=	0x5068
                           00506E  1850 _AX5043_WAKEUPXOEARLYNB	=	0x506e
                           005F11  1851 _AX5043_XTALAMPLNB	=	0x5f11
                           005184  1852 _AX5043_XTALCAPNB	=	0x5184
                           005F10  1853 _AX5043_XTALOSCNB	=	0x5f10
                           00501D  1854 _AX5043_XTALSTATUSNB	=	0x501d
                           005F00  1855 _AX5043_0xF00NB	=	0x5f00
                           005F0C  1856 _AX5043_0xF0CNB	=	0x5f0c
                           005F18  1857 _AX5043_0xF18NB	=	0x5f18
                           005F1C  1858 _AX5043_0xF1CNB	=	0x5f1c
                           005F21  1859 _AX5043_0xF21NB	=	0x5f21
                           005F22  1860 _AX5043_0xF22NB	=	0x5f22
                           005F23  1861 _AX5043_0xF23NB	=	0x5f23
                           005F26  1862 _AX5043_0xF26NB	=	0x5f26
                           005F30  1863 _AX5043_0xF30NB	=	0x5f30
                           005F31  1864 _AX5043_0xF31NB	=	0x5f31
                           005F32  1865 _AX5043_0xF32NB	=	0x5f32
                           005F33  1866 _AX5043_0xF33NB	=	0x5f33
                           005F34  1867 _AX5043_0xF34NB	=	0x5f34
                           005F35  1868 _AX5043_0xF35NB	=	0x5f35
                           005F44  1869 _AX5043_0xF44NB	=	0x5f44
                           005122  1870 _AX5043_AGCAHYST0NB	=	0x5122
                           005132  1871 _AX5043_AGCAHYST1NB	=	0x5132
                           005142  1872 _AX5043_AGCAHYST2NB	=	0x5142
                           005152  1873 _AX5043_AGCAHYST3NB	=	0x5152
                           005120  1874 _AX5043_AGCGAIN0NB	=	0x5120
                           005130  1875 _AX5043_AGCGAIN1NB	=	0x5130
                           005140  1876 _AX5043_AGCGAIN2NB	=	0x5140
                           005150  1877 _AX5043_AGCGAIN3NB	=	0x5150
                           005123  1878 _AX5043_AGCMINMAX0NB	=	0x5123
                           005133  1879 _AX5043_AGCMINMAX1NB	=	0x5133
                           005143  1880 _AX5043_AGCMINMAX2NB	=	0x5143
                           005153  1881 _AX5043_AGCMINMAX3NB	=	0x5153
                           005121  1882 _AX5043_AGCTARGET0NB	=	0x5121
                           005131  1883 _AX5043_AGCTARGET1NB	=	0x5131
                           005141  1884 _AX5043_AGCTARGET2NB	=	0x5141
                           005151  1885 _AX5043_AGCTARGET3NB	=	0x5151
                           00512B  1886 _AX5043_AMPLITUDEGAIN0NB	=	0x512b
                           00513B  1887 _AX5043_AMPLITUDEGAIN1NB	=	0x513b
                           00514B  1888 _AX5043_AMPLITUDEGAIN2NB	=	0x514b
                           00515B  1889 _AX5043_AMPLITUDEGAIN3NB	=	0x515b
                           00512F  1890 _AX5043_BBOFFSRES0NB	=	0x512f
                           00513F  1891 _AX5043_BBOFFSRES1NB	=	0x513f
                           00514F  1892 _AX5043_BBOFFSRES2NB	=	0x514f
                           00515F  1893 _AX5043_BBOFFSRES3NB	=	0x515f
                           005125  1894 _AX5043_DRGAIN0NB	=	0x5125
                           005135  1895 _AX5043_DRGAIN1NB	=	0x5135
                           005145  1896 _AX5043_DRGAIN2NB	=	0x5145
                           005155  1897 _AX5043_DRGAIN3NB	=	0x5155
                           00512E  1898 _AX5043_FOURFSK0NB	=	0x512e
                           00513E  1899 _AX5043_FOURFSK1NB	=	0x513e
                           00514E  1900 _AX5043_FOURFSK2NB	=	0x514e
                           00515E  1901 _AX5043_FOURFSK3NB	=	0x515e
                           00512D  1902 _AX5043_FREQDEV00NB	=	0x512d
                           00513D  1903 _AX5043_FREQDEV01NB	=	0x513d
                           00514D  1904 _AX5043_FREQDEV02NB	=	0x514d
                           00515D  1905 _AX5043_FREQDEV03NB	=	0x515d
                           00512C  1906 _AX5043_FREQDEV10NB	=	0x512c
                           00513C  1907 _AX5043_FREQDEV11NB	=	0x513c
                           00514C  1908 _AX5043_FREQDEV12NB	=	0x514c
                           00515C  1909 _AX5043_FREQDEV13NB	=	0x515c
                           005127  1910 _AX5043_FREQUENCYGAINA0NB	=	0x5127
                           005137  1911 _AX5043_FREQUENCYGAINA1NB	=	0x5137
                           005147  1912 _AX5043_FREQUENCYGAINA2NB	=	0x5147
                           005157  1913 _AX5043_FREQUENCYGAINA3NB	=	0x5157
                           005128  1914 _AX5043_FREQUENCYGAINB0NB	=	0x5128
                           005138  1915 _AX5043_FREQUENCYGAINB1NB	=	0x5138
                           005148  1916 _AX5043_FREQUENCYGAINB2NB	=	0x5148
                           005158  1917 _AX5043_FREQUENCYGAINB3NB	=	0x5158
                           005129  1918 _AX5043_FREQUENCYGAINC0NB	=	0x5129
                           005139  1919 _AX5043_FREQUENCYGAINC1NB	=	0x5139
                           005149  1920 _AX5043_FREQUENCYGAINC2NB	=	0x5149
                           005159  1921 _AX5043_FREQUENCYGAINC3NB	=	0x5159
                           00512A  1922 _AX5043_FREQUENCYGAIND0NB	=	0x512a
                           00513A  1923 _AX5043_FREQUENCYGAIND1NB	=	0x513a
                           00514A  1924 _AX5043_FREQUENCYGAIND2NB	=	0x514a
                           00515A  1925 _AX5043_FREQUENCYGAIND3NB	=	0x515a
                           005116  1926 _AX5043_FREQUENCYLEAKNB	=	0x5116
                           005126  1927 _AX5043_PHASEGAIN0NB	=	0x5126
                           005136  1928 _AX5043_PHASEGAIN1NB	=	0x5136
                           005146  1929 _AX5043_PHASEGAIN2NB	=	0x5146
                           005156  1930 _AX5043_PHASEGAIN3NB	=	0x5156
                           005207  1931 _AX5043_PKTADDR0NB	=	0x5207
                           005206  1932 _AX5043_PKTADDR1NB	=	0x5206
                           005205  1933 _AX5043_PKTADDR2NB	=	0x5205
                           005204  1934 _AX5043_PKTADDR3NB	=	0x5204
                           005200  1935 _AX5043_PKTADDRCFGNB	=	0x5200
                           00520B  1936 _AX5043_PKTADDRMASK0NB	=	0x520b
                           00520A  1937 _AX5043_PKTADDRMASK1NB	=	0x520a
                           005209  1938 _AX5043_PKTADDRMASK2NB	=	0x5209
                           005208  1939 _AX5043_PKTADDRMASK3NB	=	0x5208
                           005201  1940 _AX5043_PKTLENCFGNB	=	0x5201
                           005202  1941 _AX5043_PKTLENOFFSETNB	=	0x5202
                           005203  1942 _AX5043_PKTMAXLENNB	=	0x5203
                           005118  1943 _AX5043_RXPARAMCURSETNB	=	0x5118
                           005117  1944 _AX5043_RXPARAMSETSNB	=	0x5117
                           005124  1945 _AX5043_TIMEGAIN0NB	=	0x5124
                           005134  1946 _AX5043_TIMEGAIN1NB	=	0x5134
                           005144  1947 _AX5043_TIMEGAIN2NB	=	0x5144
                           005154  1948 _AX5043_TIMEGAIN3NB	=	0x5154
      0003BD                       1949 _CRYPTO_InitRegisters_PARM_2:
      0003BD                       1950 	.ds 2
      0003BF                       1951 _CRYPTO_InitRegisters_AES_KEY_ADDRESS_65536_147:
      0003BF                       1952 	.ds 2
      0003C1                       1953 _CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149:
      0003C1                       1954 	.ds 2
      0003C3                       1955 _CRYPTO_EncryptData_AES_IN_ADDR_65536_153:
      0003C3                       1956 	.ds 2
                                   1957 ;--------------------------------------------------------
                                   1958 ; absolute external ram data
                                   1959 ;--------------------------------------------------------
                                   1960 	.area XABS    (ABS,XDATA)
                                   1961 ;--------------------------------------------------------
                                   1962 ; external initialized ram data
                                   1963 ;--------------------------------------------------------
                                   1964 	.area XISEG   (XDATA)
                                   1965 	.area HOME    (CODE)
                                   1966 	.area GSINIT0 (CODE)
                                   1967 	.area GSINIT1 (CODE)
                                   1968 	.area GSINIT2 (CODE)
                                   1969 	.area GSINIT3 (CODE)
                                   1970 	.area GSINIT4 (CODE)
                                   1971 	.area GSINIT5 (CODE)
                                   1972 	.area GSINIT  (CODE)
                                   1973 	.area GSFINAL (CODE)
                                   1974 	.area CSEG    (CODE)
                                   1975 ;--------------------------------------------------------
                                   1976 ; global & static initialisations
                                   1977 ;--------------------------------------------------------
                                   1978 	.area HOME    (CODE)
                                   1979 	.area GSINIT  (CODE)
                                   1980 	.area GSFINAL (CODE)
                                   1981 	.area GSINIT  (CODE)
                                   1982 ;--------------------------------------------------------
                                   1983 ; Home
                                   1984 ;--------------------------------------------------------
                                   1985 	.area HOME    (CODE)
                                   1986 	.area HOME    (CODE)
                                   1987 ;--------------------------------------------------------
                                   1988 ; code
                                   1989 ;--------------------------------------------------------
                                   1990 	.area CSEG    (CODE)
                                   1991 ;------------------------------------------------------------
                                   1992 ;Allocation info for local variables in function 'CRYPTO_InitRegisters'
                                   1993 ;------------------------------------------------------------
                                   1994 ;AES_OUT_ADDR              Allocated with name '_CRYPTO_InitRegisters_PARM_2'
                                   1995 ;AES_KEY_ADDRESS           Allocated with name '_CRYPTO_InitRegisters_AES_KEY_ADDRESS_65536_147'
                                   1996 ;------------------------------------------------------------
                                   1997 ;	..\src\peripherals\cryptography.c:49: void CRYPTO_InitRegisters(uint16_t AES_KEY_ADDRESS, uint16_t AES_OUT_ADDR)
                                   1998 ;	-----------------------------------------
                                   1999 ;	 function CRYPTO_InitRegisters
                                   2000 ;	-----------------------------------------
      00544D                       2001 _CRYPTO_InitRegisters:
                           000007  2002 	ar7 = 0x07
                           000006  2003 	ar6 = 0x06
                           000005  2004 	ar5 = 0x05
                           000004  2005 	ar4 = 0x04
                           000003  2006 	ar3 = 0x03
                           000002  2007 	ar2 = 0x02
                           000001  2008 	ar1 = 0x01
                           000000  2009 	ar0 = 0x00
      00544D AF 83            [24] 2010 	mov	r7,dph
      00544F E5 82            [12] 2011 	mov	a,dpl
      005451 90 03 BF         [24] 2012 	mov	dptr,#_CRYPTO_InitRegisters_AES_KEY_ADDRESS_65536_147
      005454 F0               [24] 2013 	movx	@dptr,a
      005455 EF               [12] 2014 	mov	a,r7
      005456 A3               [24] 2015 	inc	dptr
      005457 F0               [24] 2016 	movx	@dptr,a
                                   2017 ;	..\src\peripherals\cryptography.c:51: AESCONFIG   = 0x8A; //10 rounds + CFB mode
      005458 90 70 91         [24] 2018 	mov	dptr,#_AESCONFIG
      00545B 74 8A            [12] 2019 	mov	a,#0x8a
      00545D F0               [24] 2020 	movx	@dptr,a
                                   2021 ;	..\src\peripherals\cryptography.c:52: AESKEYADDR0 = AES_KEY_ADDRESS & 0x00FF;
      00545E 90 03 BF         [24] 2022 	mov	dptr,#_CRYPTO_InitRegisters_AES_KEY_ADDRESS_65536_147
      005461 E0               [24] 2023 	movx	a,@dptr
      005462 FE               [12] 2024 	mov	r6,a
      005463 A3               [24] 2025 	inc	dptr
      005464 E0               [24] 2026 	movx	a,@dptr
      005465 FF               [12] 2027 	mov	r7,a
      005466 90 70 92         [24] 2028 	mov	dptr,#_AESKEYADDR0
      005469 EE               [12] 2029 	mov	a,r6
      00546A F0               [24] 2030 	movx	@dptr,a
                                   2031 ;	..\src\peripherals\cryptography.c:53: AESKEYADDR1 = (AES_KEY_ADDRESS & 0xFF00) >> 8;
      00546B 90 70 93         [24] 2032 	mov	dptr,#_AESKEYADDR1
      00546E EF               [12] 2033 	mov	a,r7
      00546F F0               [24] 2034 	movx	@dptr,a
                                   2035 ;	..\src\peripherals\cryptography.c:54: AESOUTADDR0 = AES_OUT_ADDR & 0x00FF;
      005470 90 03 BD         [24] 2036 	mov	dptr,#_CRYPTO_InitRegisters_PARM_2
      005473 E0               [24] 2037 	movx	a,@dptr
      005474 FE               [12] 2038 	mov	r6,a
      005475 A3               [24] 2039 	inc	dptr
      005476 E0               [24] 2040 	movx	a,@dptr
      005477 FF               [12] 2041 	mov	r7,a
      005478 90 70 96         [24] 2042 	mov	dptr,#_AESOUTADDR0
      00547B EE               [12] 2043 	mov	a,r6
      00547C F0               [24] 2044 	movx	@dptr,a
                                   2045 ;	..\src\peripherals\cryptography.c:55: AESOUTADDR1 = (AES_OUT_ADDR & 0xFF00) >> 8;
      00547D 90 70 97         [24] 2046 	mov	dptr,#_AESOUTADDR1
      005480 EF               [12] 2047 	mov	a,r7
      005481 F0               [24] 2048 	movx	@dptr,a
                                   2049 ;	..\src\peripherals\cryptography.c:56: return;
                                   2050 ;	..\src\peripherals\cryptography.c:57: }
      005482 22               [24] 2051 	ret
                                   2052 ;------------------------------------------------------------
                                   2053 ;Allocation info for local variables in function 'CRYPTO_KeySelectAndExpansion'
                                   2054 ;------------------------------------------------------------
                                   2055 ;wcnt                      Allocated with name '_CRYPTO_KeySelectAndExpansion_wcnt_65536_150'
                                   2056 ;w                         Allocated with name '_CRYPTO_KeySelectAndExpansion_w_65536_150'
                                   2057 ;bnum                      Allocated to registers r4 
                                   2058 ;rcon                      Allocated with name '_CRYPTO_KeySelectAndExpansion_rcon_65536_150'
                                   2059 ;sloc0                     Allocated with name '_CRYPTO_KeySelectAndExpansion_sloc0_1_0'
                                   2060 ;sloc1                     Allocated with name '_CRYPTO_KeySelectAndExpansion_sloc1_1_0'
                                   2061 ;AES_KEY_BUFFER            Allocated with name '_CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149'
                                   2062 ;Rnd                       Allocated with name '_CRYPTO_KeySelectAndExpansion_Rnd_65536_150'
                                   2063 ;Key                       Allocated with name '_CRYPTO_KeySelectAndExpansion_Key_65536_150'
                                   2064 ;i                         Allocated with name '_CRYPTO_KeySelectAndExpansion_i_65536_150'
                                   2065 ;------------------------------------------------------------
                                   2066 ;	..\src\peripherals\cryptography.c:67: uint8_t CRYPTO_KeySelectAndExpansion (uint8_t __xdata *AES_KEY_BUFFER)
                                   2067 ;	-----------------------------------------
                                   2068 ;	 function CRYPTO_KeySelectAndExpansion
                                   2069 ;	-----------------------------------------
      005483                       2070 _CRYPTO_KeySelectAndExpansion:
      005483 AF 83            [24] 2071 	mov	r7,dph
      005485 E5 82            [12] 2072 	mov	a,dpl
      005487 90 03 C1         [24] 2073 	mov	dptr,#_CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149
      00548A F0               [24] 2074 	movx	@dptr,a
      00548B EF               [12] 2075 	mov	a,r7
      00548C A3               [24] 2076 	inc	dptr
      00548D F0               [24] 2077 	movx	@dptr,a
                                   2078 ;	..\src\peripherals\cryptography.c:76: Key = malloc(8*sizeof(uint16_t));
      00548E 90 00 10         [24] 2079 	mov	dptr,#0x0010
      005491 12 7E 60         [24] 2080 	lcall	_malloc
      005494 AE 82            [24] 2081 	mov	r6,dpl
      005496 AF 83            [24] 2082 	mov	r7,dph
      005498 7D 00            [12] 2083 	mov	r5,#0x00
                                   2084 ;	..\src\peripherals\cryptography.c:77: Rnd = RNGBYTE;
      00549A 90 70 81         [24] 2085 	mov	dptr,#_RNGBYTE
      00549D E0               [24] 2086 	movx	a,@dptr
      00549E FC               [12] 2087 	mov	r4,a
                                   2088 ;	..\src\peripherals\cryptography.c:78: Rnd = (Rnd % 8);
      00549F 53 04 07         [24] 2089 	anl	ar4,#0x07
      0054A2 8C 38            [24] 2090 	mov	_CRYPTO_KeySelectAndExpansion_sloc0_1_0,r4
                                   2091 ;	..\src\peripherals\cryptography.c:79: *Key = 0x0100;
      0054A4 8E 82            [24] 2092 	mov	dpl,r6
      0054A6 8F 83            [24] 2093 	mov	dph,r7
      0054A8 8D F0            [24] 2094 	mov	b,r5
      0054AA E4               [12] 2095 	clr	a
      0054AB 12 7C 62         [24] 2096 	lcall	__gptrput
      0054AE A3               [24] 2097 	inc	dptr
      0054AF 04               [12] 2098 	inc	a
      0054B0 12 7C 62         [24] 2099 	lcall	__gptrput
                                   2100 ;	..\src\peripherals\cryptography.c:80: *(Key+1) = 0x0302;
      0054B3 04               [12] 2101 	inc	a
      0054B4 2E               [12] 2102 	add	a,r6
      0054B5 FA               [12] 2103 	mov	r2,a
      0054B6 E4               [12] 2104 	clr	a
      0054B7 3F               [12] 2105 	addc	a,r7
      0054B8 FB               [12] 2106 	mov	r3,a
      0054B9 8D 04            [24] 2107 	mov	ar4,r5
      0054BB 8A 82            [24] 2108 	mov	dpl,r2
      0054BD 8B 83            [24] 2109 	mov	dph,r3
      0054BF 8C F0            [24] 2110 	mov	b,r4
      0054C1 74 02            [12] 2111 	mov	a,#0x02
      0054C3 12 7C 62         [24] 2112 	lcall	__gptrput
      0054C6 A3               [24] 2113 	inc	dptr
      0054C7 04               [12] 2114 	inc	a
      0054C8 12 7C 62         [24] 2115 	lcall	__gptrput
                                   2116 ;	..\src\peripherals\cryptography.c:81: *(Key+2) = 0x0504;
      0054CB 04               [12] 2117 	inc	a
      0054CC 2E               [12] 2118 	add	a,r6
      0054CD FA               [12] 2119 	mov	r2,a
      0054CE E4               [12] 2120 	clr	a
      0054CF 3F               [12] 2121 	addc	a,r7
      0054D0 FB               [12] 2122 	mov	r3,a
      0054D1 8D 04            [24] 2123 	mov	ar4,r5
      0054D3 8A 82            [24] 2124 	mov	dpl,r2
      0054D5 8B 83            [24] 2125 	mov	dph,r3
      0054D7 8C F0            [24] 2126 	mov	b,r4
      0054D9 74 04            [12] 2127 	mov	a,#0x04
      0054DB 12 7C 62         [24] 2128 	lcall	__gptrput
      0054DE A3               [24] 2129 	inc	dptr
      0054DF 04               [12] 2130 	inc	a
      0054E0 12 7C 62         [24] 2131 	lcall	__gptrput
                                   2132 ;	..\src\peripherals\cryptography.c:82: *(Key+3) = 0x0706;
      0054E3 04               [12] 2133 	inc	a
      0054E4 2E               [12] 2134 	add	a,r6
      0054E5 FA               [12] 2135 	mov	r2,a
      0054E6 E4               [12] 2136 	clr	a
      0054E7 3F               [12] 2137 	addc	a,r7
      0054E8 FB               [12] 2138 	mov	r3,a
      0054E9 8D 04            [24] 2139 	mov	ar4,r5
      0054EB 8A 82            [24] 2140 	mov	dpl,r2
      0054ED 8B 83            [24] 2141 	mov	dph,r3
      0054EF 8C F0            [24] 2142 	mov	b,r4
      0054F1 74 06            [12] 2143 	mov	a,#0x06
      0054F3 12 7C 62         [24] 2144 	lcall	__gptrput
      0054F6 A3               [24] 2145 	inc	dptr
      0054F7 04               [12] 2146 	inc	a
      0054F8 12 7C 62         [24] 2147 	lcall	__gptrput
                                   2148 ;	..\src\peripherals\cryptography.c:83: *(Key+4) = 0x0908;
      0054FB 04               [12] 2149 	inc	a
      0054FC 2E               [12] 2150 	add	a,r6
      0054FD FA               [12] 2151 	mov	r2,a
      0054FE E4               [12] 2152 	clr	a
      0054FF 3F               [12] 2153 	addc	a,r7
      005500 FB               [12] 2154 	mov	r3,a
      005501 8D 04            [24] 2155 	mov	ar4,r5
      005503 8A 82            [24] 2156 	mov	dpl,r2
      005505 8B 83            [24] 2157 	mov	dph,r3
      005507 8C F0            [24] 2158 	mov	b,r4
      005509 74 08            [12] 2159 	mov	a,#0x08
      00550B 12 7C 62         [24] 2160 	lcall	__gptrput
      00550E A3               [24] 2161 	inc	dptr
      00550F 04               [12] 2162 	inc	a
      005510 12 7C 62         [24] 2163 	lcall	__gptrput
                                   2164 ;	..\src\peripherals\cryptography.c:84: *(Key+5) = 0x0B0A;
      005513 04               [12] 2165 	inc	a
      005514 2E               [12] 2166 	add	a,r6
      005515 FA               [12] 2167 	mov	r2,a
      005516 E4               [12] 2168 	clr	a
      005517 3F               [12] 2169 	addc	a,r7
      005518 FB               [12] 2170 	mov	r3,a
      005519 8D 04            [24] 2171 	mov	ar4,r5
      00551B 8A 82            [24] 2172 	mov	dpl,r2
      00551D 8B 83            [24] 2173 	mov	dph,r3
      00551F 8C F0            [24] 2174 	mov	b,r4
      005521 74 0A            [12] 2175 	mov	a,#0x0a
      005523 12 7C 62         [24] 2176 	lcall	__gptrput
      005526 A3               [24] 2177 	inc	dptr
      005527 04               [12] 2178 	inc	a
      005528 12 7C 62         [24] 2179 	lcall	__gptrput
                                   2180 ;	..\src\peripherals\cryptography.c:85: *(Key+6) = 0x0D0C;
      00552B 04               [12] 2181 	inc	a
      00552C 2E               [12] 2182 	add	a,r6
      00552D FA               [12] 2183 	mov	r2,a
      00552E E4               [12] 2184 	clr	a
      00552F 3F               [12] 2185 	addc	a,r7
      005530 FB               [12] 2186 	mov	r3,a
      005531 8D 04            [24] 2187 	mov	ar4,r5
      005533 8A 82            [24] 2188 	mov	dpl,r2
      005535 8B 83            [24] 2189 	mov	dph,r3
      005537 8C F0            [24] 2190 	mov	b,r4
      005539 74 0C            [12] 2191 	mov	a,#0x0c
      00553B 12 7C 62         [24] 2192 	lcall	__gptrput
      00553E A3               [24] 2193 	inc	dptr
      00553F 04               [12] 2194 	inc	a
      005540 12 7C 62         [24] 2195 	lcall	__gptrput
                                   2196 ;	..\src\peripherals\cryptography.c:86: *(Key+7) = 0x0F0E;
      005543 04               [12] 2197 	inc	a
      005544 2E               [12] 2198 	add	a,r6
      005545 FA               [12] 2199 	mov	r2,a
      005546 E4               [12] 2200 	clr	a
      005547 3F               [12] 2201 	addc	a,r7
      005548 FB               [12] 2202 	mov	r3,a
      005549 8D 04            [24] 2203 	mov	ar4,r5
      00554B 8A 82            [24] 2204 	mov	dpl,r2
      00554D 8B 83            [24] 2205 	mov	dph,r3
      00554F 8C F0            [24] 2206 	mov	b,r4
      005551 74 0E            [12] 2207 	mov	a,#0x0e
      005553 12 7C 62         [24] 2208 	lcall	__gptrput
      005556 A3               [24] 2209 	inc	dptr
      005557 04               [12] 2210 	inc	a
      005558 12 7C 62         [24] 2211 	lcall	__gptrput
                                   2212 ;	..\src\peripherals\cryptography.c:87: memcpy((void*)AES_KEY_BUFFER,Key, 16); // agregar free
      00555B 90 03 C1         [24] 2213 	mov	dptr,#_CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149
      00555E E0               [24] 2214 	movx	a,@dptr
      00555F F5 39            [12] 2215 	mov	_CRYPTO_KeySelectAndExpansion_sloc1_1_0,a
      005561 A3               [24] 2216 	inc	dptr
      005562 E0               [24] 2217 	movx	a,@dptr
      005563 F5 3A            [12] 2218 	mov	(_CRYPTO_KeySelectAndExpansion_sloc1_1_0 + 1),a
      005565 AA 39            [24] 2219 	mov	r2,_CRYPTO_KeySelectAndExpansion_sloc1_1_0
      005567 AC 3A            [24] 2220 	mov	r4,(_CRYPTO_KeySelectAndExpansion_sloc1_1_0 + 1)
      005569 7B 00            [12] 2221 	mov	r3,#0x00
      00556B 90 04 6D         [24] 2222 	mov	dptr,#_memcpy_PARM_2
      00556E EE               [12] 2223 	mov	a,r6
      00556F F0               [24] 2224 	movx	@dptr,a
      005570 EF               [12] 2225 	mov	a,r7
      005571 A3               [24] 2226 	inc	dptr
      005572 F0               [24] 2227 	movx	@dptr,a
      005573 ED               [12] 2228 	mov	a,r5
      005574 A3               [24] 2229 	inc	dptr
      005575 F0               [24] 2230 	movx	@dptr,a
      005576 90 04 70         [24] 2231 	mov	dptr,#_memcpy_PARM_3
      005579 74 10            [12] 2232 	mov	a,#0x10
      00557B F0               [24] 2233 	movx	@dptr,a
      00557C E4               [12] 2234 	clr	a
      00557D A3               [24] 2235 	inc	dptr
      00557E F0               [24] 2236 	movx	@dptr,a
      00557F 8A 82            [24] 2237 	mov	dpl,r2
      005581 8C 83            [24] 2238 	mov	dph,r4
      005583 8B F0            [24] 2239 	mov	b,r3
      005585 12 78 14         [24] 2240 	lcall	_memcpy
                                   2241 ;	..\src\peripherals\cryptography.c:91: wcnt -= 4;
      005588 75 32 28         [24] 2242 	mov	_CRYPTO_KeySelectAndExpansion_wcnt_65536_150,#0x28
                                   2243 ;	..\src\peripherals\cryptography.c:92: AES_KEY_BUFFER += 4 * 4;
      00558B 90 03 C1         [24] 2244 	mov	dptr,#_CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149
      00558E 74 10            [12] 2245 	mov	a,#0x10
      005590 25 39            [12] 2246 	add	a,_CRYPTO_KeySelectAndExpansion_sloc1_1_0
      005592 F0               [24] 2247 	movx	@dptr,a
      005593 E4               [12] 2248 	clr	a
      005594 35 3A            [12] 2249 	addc	a,(_CRYPTO_KeySelectAndExpansion_sloc1_1_0 + 1)
      005596 A3               [24] 2250 	inc	dptr
      005597 F0               [24] 2251 	movx	@dptr,a
                                   2252 ;	..\src\peripherals\cryptography.c:93: bnum = 0;
      005598 7E 00            [12] 2253 	mov	r6,#0x00
                                   2254 ;	..\src\peripherals\cryptography.c:94: rcon = 1;
      00559A 75 37 01         [24] 2255 	mov	_CRYPTO_KeySelectAndExpansion_rcon_65536_150,#0x01
                                   2256 ;	..\src\peripherals\cryptography.c:95: do {
      00559D                       2257 00109$:
                                   2258 ;	..\src\peripherals\cryptography.c:96: AES_KEY_BUFFER -= 4;
      00559D 90 03 C1         [24] 2259 	mov	dptr,#_CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149
      0055A0 E0               [24] 2260 	movx	a,@dptr
      0055A1 FB               [12] 2261 	mov	r3,a
      0055A2 A3               [24] 2262 	inc	dptr
      0055A3 E0               [24] 2263 	movx	a,@dptr
      0055A4 FC               [12] 2264 	mov	r4,a
      0055A5 EB               [12] 2265 	mov	a,r3
      0055A6 24 FC            [12] 2266 	add	a,#0xfc
      0055A8 FB               [12] 2267 	mov	r3,a
      0055A9 EC               [12] 2268 	mov	a,r4
      0055AA 34 FF            [12] 2269 	addc	a,#0xff
      0055AC FC               [12] 2270 	mov	r4,a
      0055AD 90 03 C1         [24] 2271 	mov	dptr,#_CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149
      0055B0 EB               [12] 2272 	mov	a,r3
      0055B1 F0               [24] 2273 	movx	@dptr,a
      0055B2 EC               [12] 2274 	mov	a,r4
      0055B3 A3               [24] 2275 	inc	dptr
      0055B4 F0               [24] 2276 	movx	@dptr,a
                                   2277 ;	..\src\peripherals\cryptography.c:97: w[0] = *AES_KEY_BUFFER++;
      0055B5 90 03 C1         [24] 2278 	mov	dptr,#_CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149
      0055B8 E0               [24] 2279 	movx	a,@dptr
      0055B9 FB               [12] 2280 	mov	r3,a
      0055BA A3               [24] 2281 	inc	dptr
      0055BB E0               [24] 2282 	movx	a,@dptr
      0055BC FC               [12] 2283 	mov	r4,a
      0055BD 8B 82            [24] 2284 	mov	dpl,r3
      0055BF 8C 83            [24] 2285 	mov	dph,r4
      0055C1 E0               [24] 2286 	movx	a,@dptr
      0055C2 FA               [12] 2287 	mov	r2,a
      0055C3 90 03 C1         [24] 2288 	mov	dptr,#_CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149
      0055C6 74 01            [12] 2289 	mov	a,#0x01
      0055C8 2B               [12] 2290 	add	a,r3
      0055C9 F0               [24] 2291 	movx	@dptr,a
      0055CA E4               [12] 2292 	clr	a
      0055CB 3C               [12] 2293 	addc	a,r4
      0055CC A3               [24] 2294 	inc	dptr
      0055CD F0               [24] 2295 	movx	@dptr,a
      0055CE 8A 33            [24] 2296 	mov	_CRYPTO_KeySelectAndExpansion_w_65536_150,r2
                                   2297 ;	..\src\peripherals\cryptography.c:98: w[1] = *AES_KEY_BUFFER++;
      0055D0 90 03 C1         [24] 2298 	mov	dptr,#_CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149
      0055D3 E0               [24] 2299 	movx	a,@dptr
      0055D4 FB               [12] 2300 	mov	r3,a
      0055D5 A3               [24] 2301 	inc	dptr
      0055D6 E0               [24] 2302 	movx	a,@dptr
      0055D7 FC               [12] 2303 	mov	r4,a
      0055D8 8B 82            [24] 2304 	mov	dpl,r3
      0055DA 8C 83            [24] 2305 	mov	dph,r4
      0055DC E0               [24] 2306 	movx	a,@dptr
      0055DD FA               [12] 2307 	mov	r2,a
      0055DE 90 03 C1         [24] 2308 	mov	dptr,#_CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149
      0055E1 74 01            [12] 2309 	mov	a,#0x01
      0055E3 2B               [12] 2310 	add	a,r3
      0055E4 F0               [24] 2311 	movx	@dptr,a
      0055E5 E4               [12] 2312 	clr	a
      0055E6 3C               [12] 2313 	addc	a,r4
      0055E7 A3               [24] 2314 	inc	dptr
      0055E8 F0               [24] 2315 	movx	@dptr,a
      0055E9 8A 34            [24] 2316 	mov	(_CRYPTO_KeySelectAndExpansion_w_65536_150 + 0x0001),r2
                                   2317 ;	..\src\peripherals\cryptography.c:99: w[2] = *AES_KEY_BUFFER++;
      0055EB 90 03 C1         [24] 2318 	mov	dptr,#_CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149
      0055EE E0               [24] 2319 	movx	a,@dptr
      0055EF FB               [12] 2320 	mov	r3,a
      0055F0 A3               [24] 2321 	inc	dptr
      0055F1 E0               [24] 2322 	movx	a,@dptr
      0055F2 FC               [12] 2323 	mov	r4,a
      0055F3 8B 82            [24] 2324 	mov	dpl,r3
      0055F5 8C 83            [24] 2325 	mov	dph,r4
      0055F7 E0               [24] 2326 	movx	a,@dptr
      0055F8 FA               [12] 2327 	mov	r2,a
      0055F9 90 03 C1         [24] 2328 	mov	dptr,#_CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149
      0055FC 74 01            [12] 2329 	mov	a,#0x01
      0055FE 2B               [12] 2330 	add	a,r3
      0055FF F0               [24] 2331 	movx	@dptr,a
      005600 E4               [12] 2332 	clr	a
      005601 3C               [12] 2333 	addc	a,r4
      005602 A3               [24] 2334 	inc	dptr
      005603 F0               [24] 2335 	movx	@dptr,a
      005604 8A 35            [24] 2336 	mov	(_CRYPTO_KeySelectAndExpansion_w_65536_150 + 0x0002),r2
                                   2337 ;	..\src\peripherals\cryptography.c:100: w[3] = *AES_KEY_BUFFER++;
      005606 90 03 C1         [24] 2338 	mov	dptr,#_CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149
      005609 E0               [24] 2339 	movx	a,@dptr
      00560A FB               [12] 2340 	mov	r3,a
      00560B A3               [24] 2341 	inc	dptr
      00560C E0               [24] 2342 	movx	a,@dptr
      00560D FC               [12] 2343 	mov	r4,a
      00560E 8B 82            [24] 2344 	mov	dpl,r3
      005610 8C 83            [24] 2345 	mov	dph,r4
      005612 E0               [24] 2346 	movx	a,@dptr
      005613 FA               [12] 2347 	mov	r2,a
      005614 90 03 C1         [24] 2348 	mov	dptr,#_CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149
      005617 74 01            [12] 2349 	mov	a,#0x01
      005619 2B               [12] 2350 	add	a,r3
      00561A F0               [24] 2351 	movx	@dptr,a
      00561B E4               [12] 2352 	clr	a
      00561C 3C               [12] 2353 	addc	a,r4
      00561D A3               [24] 2354 	inc	dptr
      00561E F0               [24] 2355 	movx	@dptr,a
      00561F 8A 36            [24] 2356 	mov	(_CRYPTO_KeySelectAndExpansion_w_65536_150 + 0x0003),r2
                                   2357 ;	..\src\peripherals\cryptography.c:102: if (!bnum) {
      005621 EE               [12] 2358 	mov	a,r6
      005622 70 4F            [24] 2359 	jnz	00106$
                                   2360 ;	..\src\peripherals\cryptography.c:104: bnum = w[0];
      005624 AC 33            [24] 2361 	mov	r4,_CRYPTO_KeySelectAndExpansion_w_65536_150
                                   2362 ;	..\src\peripherals\cryptography.c:105: w[0] = w[1];
      005626 AB 34            [24] 2363 	mov	r3,(_CRYPTO_KeySelectAndExpansion_w_65536_150 + 0x0001)
      005628 8B 33            [24] 2364 	mov	_CRYPTO_KeySelectAndExpansion_w_65536_150,r3
                                   2365 ;	..\src\peripherals\cryptography.c:106: w[1] = w[2];
      00562A AA 35            [24] 2366 	mov	r2,(_CRYPTO_KeySelectAndExpansion_w_65536_150 + 0x0002)
      00562C 8A 34            [24] 2367 	mov	(_CRYPTO_KeySelectAndExpansion_w_65536_150 + 0x0001),r2
                                   2368 ;	..\src\peripherals\cryptography.c:107: w[2] = w[3];
      00562E AF 36            [24] 2369 	mov	r7,(_CRYPTO_KeySelectAndExpansion_w_65536_150 + 0x0003)
      005630 8F 35            [24] 2370 	mov	(_CRYPTO_KeySelectAndExpansion_w_65536_150 + 0x0002),r7
                                   2371 ;	..\src\peripherals\cryptography.c:108: w[3] = bnum;
      005632 8C 36            [24] 2372 	mov	(_CRYPTO_KeySelectAndExpansion_w_65536_150 + 0x0003),r4
                                   2373 ;	..\src\peripherals\cryptography.c:110: w[0] = aes_sbox[w[0]];
      005634 EB               [12] 2374 	mov	a,r3
      005635 90 91 F8         [24] 2375 	mov	dptr,#_aes_sbox
      005638 93               [24] 2376 	movc	a,@a+dptr
      005639 FB               [12] 2377 	mov	r3,a
      00563A 8B 33            [24] 2378 	mov	_CRYPTO_KeySelectAndExpansion_w_65536_150,r3
                                   2379 ;	..\src\peripherals\cryptography.c:111: w[1] = aes_sbox[w[1]];
      00563C EA               [12] 2380 	mov	a,r2
      00563D 90 91 F8         [24] 2381 	mov	dptr,#_aes_sbox
      005640 93               [24] 2382 	movc	a,@a+dptr
      005641 FA               [12] 2383 	mov	r2,a
      005642 8A 34            [24] 2384 	mov	(_CRYPTO_KeySelectAndExpansion_w_65536_150 + 0x0001),r2
                                   2385 ;	..\src\peripherals\cryptography.c:112: w[2] = aes_sbox[w[2]];
      005644 EF               [12] 2386 	mov	a,r7
      005645 90 91 F8         [24] 2387 	mov	dptr,#_aes_sbox
      005648 93               [24] 2388 	movc	a,@a+dptr
      005649 FF               [12] 2389 	mov	r7,a
      00564A 8F 35            [24] 2390 	mov	(_CRYPTO_KeySelectAndExpansion_w_65536_150 + 0x0002),r7
                                   2391 ;	..\src\peripherals\cryptography.c:113: w[3] = aes_sbox[w[3]];
      00564C EC               [12] 2392 	mov	a,r4
      00564D 90 91 F8         [24] 2393 	mov	dptr,#_aes_sbox
      005650 93               [24] 2394 	movc	a,@a+dptr
      005651 FF               [12] 2395 	mov	r7,a
      005652 8F 36            [24] 2396 	mov	(_CRYPTO_KeySelectAndExpansion_w_65536_150 + 0x0003),r7
                                   2397 ;	..\src\peripherals\cryptography.c:115: w[0] ^= rcon;
      005654 E5 37            [12] 2398 	mov	a,_CRYPTO_KeySelectAndExpansion_rcon_65536_150
      005656 6B               [12] 2399 	xrl	a,r3
      005657 F5 33            [12] 2400 	mov	_CRYPTO_KeySelectAndExpansion_w_65536_150,a
                                   2401 ;	..\src\peripherals\cryptography.c:117: bnum = (rcon & 0x80);
      005659 74 80            [12] 2402 	mov	a,#0x80
      00565B 55 37            [12] 2403 	anl	a,_CRYPTO_KeySelectAndExpansion_rcon_65536_150
      00565D FF               [12] 2404 	mov	r7,a
                                   2405 ;	..\src\peripherals\cryptography.c:118: rcon <<= 1;
      00565E E5 37            [12] 2406 	mov	a,_CRYPTO_KeySelectAndExpansion_rcon_65536_150
      005660 FC               [12] 2407 	mov	r4,a
      005661 25 E0            [12] 2408 	add	a,acc
      005663 F5 37            [12] 2409 	mov	_CRYPTO_KeySelectAndExpansion_rcon_65536_150,a
                                   2410 ;	..\src\peripherals\cryptography.c:119: if (bnum)
      005665 EF               [12] 2411 	mov	a,r7
      005666 60 09            [24] 2412 	jz	00104$
                                   2413 ;	..\src\peripherals\cryptography.c:120: rcon ^= 0x1b;
      005668 AC 37            [24] 2414 	mov	r4,_CRYPTO_KeySelectAndExpansion_rcon_65536_150
      00566A 7F 00            [12] 2415 	mov	r7,#0x00
      00566C 63 04 1B         [24] 2416 	xrl	ar4,#0x1b
      00566F 8C 37            [24] 2417 	mov	_CRYPTO_KeySelectAndExpansion_rcon_65536_150,r4
      005671                       2418 00104$:
                                   2419 ;	..\src\peripherals\cryptography.c:121: bnum = 0;
      005671 7E 00            [12] 2420 	mov	r6,#0x00
      005673                       2421 00106$:
                                   2422 ;	..\src\peripherals\cryptography.c:123: AES_KEY_BUFFER -= 4 * 4;
      005673 90 03 C1         [24] 2423 	mov	dptr,#_CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149
      005676 E0               [24] 2424 	movx	a,@dptr
      005677 FC               [12] 2425 	mov	r4,a
      005678 A3               [24] 2426 	inc	dptr
      005679 E0               [24] 2427 	movx	a,@dptr
      00567A FF               [12] 2428 	mov	r7,a
      00567B EC               [12] 2429 	mov	a,r4
      00567C 24 F0            [12] 2430 	add	a,#0xf0
      00567E FC               [12] 2431 	mov	r4,a
      00567F EF               [12] 2432 	mov	a,r7
      005680 34 FF            [12] 2433 	addc	a,#0xff
      005682 FF               [12] 2434 	mov	r7,a
      005683 90 03 C1         [24] 2435 	mov	dptr,#_CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149
      005686 EC               [12] 2436 	mov	a,r4
      005687 F0               [24] 2437 	movx	@dptr,a
      005688 EF               [12] 2438 	mov	a,r7
      005689 A3               [24] 2439 	inc	dptr
      00568A F0               [24] 2440 	movx	@dptr,a
                                   2441 ;	..\src\peripherals\cryptography.c:124: w[0] ^= *AES_KEY_BUFFER++;
      00568B 90 03 C1         [24] 2442 	mov	dptr,#_CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149
      00568E E0               [24] 2443 	movx	a,@dptr
      00568F FC               [12] 2444 	mov	r4,a
      005690 A3               [24] 2445 	inc	dptr
      005691 E0               [24] 2446 	movx	a,@dptr
      005692 FF               [12] 2447 	mov	r7,a
      005693 8C 82            [24] 2448 	mov	dpl,r4
      005695 8F 83            [24] 2449 	mov	dph,r7
      005697 E0               [24] 2450 	movx	a,@dptr
      005698 FB               [12] 2451 	mov	r3,a
      005699 90 03 C1         [24] 2452 	mov	dptr,#_CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149
      00569C 74 01            [12] 2453 	mov	a,#0x01
      00569E 2C               [12] 2454 	add	a,r4
      00569F F0               [24] 2455 	movx	@dptr,a
      0056A0 E4               [12] 2456 	clr	a
      0056A1 3F               [12] 2457 	addc	a,r7
      0056A2 A3               [24] 2458 	inc	dptr
      0056A3 F0               [24] 2459 	movx	@dptr,a
      0056A4 E5 33            [12] 2460 	mov	a,_CRYPTO_KeySelectAndExpansion_w_65536_150
      0056A6 62 03            [12] 2461 	xrl	ar3,a
      0056A8 8B 33            [24] 2462 	mov	_CRYPTO_KeySelectAndExpansion_w_65536_150,r3
                                   2463 ;	..\src\peripherals\cryptography.c:125: w[1] ^= *AES_KEY_BUFFER++;
      0056AA 90 03 C1         [24] 2464 	mov	dptr,#_CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149
      0056AD E0               [24] 2465 	movx	a,@dptr
      0056AE FC               [12] 2466 	mov	r4,a
      0056AF A3               [24] 2467 	inc	dptr
      0056B0 E0               [24] 2468 	movx	a,@dptr
      0056B1 FF               [12] 2469 	mov	r7,a
      0056B2 8C 82            [24] 2470 	mov	dpl,r4
      0056B4 8F 83            [24] 2471 	mov	dph,r7
      0056B6 E0               [24] 2472 	movx	a,@dptr
      0056B7 FA               [12] 2473 	mov	r2,a
      0056B8 90 03 C1         [24] 2474 	mov	dptr,#_CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149
      0056BB 74 01            [12] 2475 	mov	a,#0x01
      0056BD 2C               [12] 2476 	add	a,r4
      0056BE F0               [24] 2477 	movx	@dptr,a
      0056BF E4               [12] 2478 	clr	a
      0056C0 3F               [12] 2479 	addc	a,r7
      0056C1 A3               [24] 2480 	inc	dptr
      0056C2 F0               [24] 2481 	movx	@dptr,a
      0056C3 E5 34            [12] 2482 	mov	a,(_CRYPTO_KeySelectAndExpansion_w_65536_150 + 0x0001)
      0056C5 62 02            [12] 2483 	xrl	ar2,a
      0056C7 8A 34            [24] 2484 	mov	(_CRYPTO_KeySelectAndExpansion_w_65536_150 + 0x0001),r2
                                   2485 ;	..\src\peripherals\cryptography.c:126: w[2] ^= *AES_KEY_BUFFER++;
      0056C9 90 03 C1         [24] 2486 	mov	dptr,#_CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149
      0056CC E0               [24] 2487 	movx	a,@dptr
      0056CD FC               [12] 2488 	mov	r4,a
      0056CE A3               [24] 2489 	inc	dptr
      0056CF E0               [24] 2490 	movx	a,@dptr
      0056D0 FF               [12] 2491 	mov	r7,a
      0056D1 8C 82            [24] 2492 	mov	dpl,r4
      0056D3 8F 83            [24] 2493 	mov	dph,r7
      0056D5 E0               [24] 2494 	movx	a,@dptr
      0056D6 FD               [12] 2495 	mov	r5,a
      0056D7 90 03 C1         [24] 2496 	mov	dptr,#_CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149
      0056DA 74 01            [12] 2497 	mov	a,#0x01
      0056DC 2C               [12] 2498 	add	a,r4
      0056DD F0               [24] 2499 	movx	@dptr,a
      0056DE E4               [12] 2500 	clr	a
      0056DF 3F               [12] 2501 	addc	a,r7
      0056E0 A3               [24] 2502 	inc	dptr
      0056E1 F0               [24] 2503 	movx	@dptr,a
      0056E2 ED               [12] 2504 	mov	a,r5
      0056E3 65 35            [12] 2505 	xrl	a,(_CRYPTO_KeySelectAndExpansion_w_65536_150 + 0x0002)
      0056E5 F5 39            [12] 2506 	mov	_CRYPTO_KeySelectAndExpansion_sloc1_1_0,a
      0056E7 85 39 35         [24] 2507 	mov	(_CRYPTO_KeySelectAndExpansion_w_65536_150 + 0x0002),_CRYPTO_KeySelectAndExpansion_sloc1_1_0
                                   2508 ;	..\src\peripherals\cryptography.c:127: w[3] ^= *AES_KEY_BUFFER++;
      0056EA 90 03 C1         [24] 2509 	mov	dptr,#_CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149
      0056ED E0               [24] 2510 	movx	a,@dptr
      0056EE FC               [12] 2511 	mov	r4,a
      0056EF A3               [24] 2512 	inc	dptr
      0056F0 E0               [24] 2513 	movx	a,@dptr
      0056F1 FF               [12] 2514 	mov	r7,a
      0056F2 8C 82            [24] 2515 	mov	dpl,r4
      0056F4 8F 83            [24] 2516 	mov	dph,r7
      0056F6 E0               [24] 2517 	movx	a,@dptr
      0056F7 FD               [12] 2518 	mov	r5,a
      0056F8 90 03 C1         [24] 2519 	mov	dptr,#_CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149
      0056FB 74 01            [12] 2520 	mov	a,#0x01
      0056FD 2C               [12] 2521 	add	a,r4
      0056FE F0               [24] 2522 	movx	@dptr,a
      0056FF E4               [12] 2523 	clr	a
      005700 3F               [12] 2524 	addc	a,r7
      005701 A3               [24] 2525 	inc	dptr
      005702 F0               [24] 2526 	movx	@dptr,a
      005703 E5 36            [12] 2527 	mov	a,(_CRYPTO_KeySelectAndExpansion_w_65536_150 + 0x0003)
      005705 62 05            [12] 2528 	xrl	ar5,a
      005707 8D 36            [24] 2529 	mov	(_CRYPTO_KeySelectAndExpansion_w_65536_150 + 0x0003),r5
                                   2530 ;	..\src\peripherals\cryptography.c:128: AES_KEY_BUFFER += 4 * 4 - 4;
      005709 90 03 C1         [24] 2531 	mov	dptr,#_CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149
      00570C E0               [24] 2532 	movx	a,@dptr
      00570D FC               [12] 2533 	mov	r4,a
      00570E A3               [24] 2534 	inc	dptr
      00570F E0               [24] 2535 	movx	a,@dptr
      005710 FF               [12] 2536 	mov	r7,a
      005711 90 03 C1         [24] 2537 	mov	dptr,#_CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149
      005714 74 0C            [12] 2538 	mov	a,#0x0c
      005716 2C               [12] 2539 	add	a,r4
      005717 F0               [24] 2540 	movx	@dptr,a
      005718 E4               [12] 2541 	clr	a
      005719 3F               [12] 2542 	addc	a,r7
      00571A A3               [24] 2543 	inc	dptr
      00571B F0               [24] 2544 	movx	@dptr,a
                                   2545 ;	..\src\peripherals\cryptography.c:129: *AES_KEY_BUFFER++ = w[0];
      00571C 90 03 C1         [24] 2546 	mov	dptr,#_CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149
      00571F E0               [24] 2547 	movx	a,@dptr
      005720 FC               [12] 2548 	mov	r4,a
      005721 A3               [24] 2549 	inc	dptr
      005722 E0               [24] 2550 	movx	a,@dptr
      005723 FF               [12] 2551 	mov	r7,a
      005724 8C 82            [24] 2552 	mov	dpl,r4
      005726 8F 83            [24] 2553 	mov	dph,r7
      005728 EB               [12] 2554 	mov	a,r3
      005729 F0               [24] 2555 	movx	@dptr,a
      00572A 90 03 C1         [24] 2556 	mov	dptr,#_CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149
      00572D 74 01            [12] 2557 	mov	a,#0x01
      00572F 2C               [12] 2558 	add	a,r4
      005730 F0               [24] 2559 	movx	@dptr,a
      005731 E4               [12] 2560 	clr	a
      005732 3F               [12] 2561 	addc	a,r7
      005733 A3               [24] 2562 	inc	dptr
      005734 F0               [24] 2563 	movx	@dptr,a
                                   2564 ;	..\src\peripherals\cryptography.c:130: *AES_KEY_BUFFER++ = w[1];
      005735 90 03 C1         [24] 2565 	mov	dptr,#_CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149
      005738 E0               [24] 2566 	movx	a,@dptr
      005739 FC               [12] 2567 	mov	r4,a
      00573A A3               [24] 2568 	inc	dptr
      00573B E0               [24] 2569 	movx	a,@dptr
      00573C FF               [12] 2570 	mov	r7,a
      00573D 8C 82            [24] 2571 	mov	dpl,r4
      00573F 8F 83            [24] 2572 	mov	dph,r7
      005741 EA               [12] 2573 	mov	a,r2
      005742 F0               [24] 2574 	movx	@dptr,a
      005743 90 03 C1         [24] 2575 	mov	dptr,#_CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149
      005746 74 01            [12] 2576 	mov	a,#0x01
      005748 2C               [12] 2577 	add	a,r4
      005749 F0               [24] 2578 	movx	@dptr,a
      00574A E4               [12] 2579 	clr	a
      00574B 3F               [12] 2580 	addc	a,r7
      00574C A3               [24] 2581 	inc	dptr
      00574D F0               [24] 2582 	movx	@dptr,a
                                   2583 ;	..\src\peripherals\cryptography.c:131: *AES_KEY_BUFFER++ = w[2];
      00574E 90 03 C1         [24] 2584 	mov	dptr,#_CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149
      005751 E0               [24] 2585 	movx	a,@dptr
      005752 FC               [12] 2586 	mov	r4,a
      005753 A3               [24] 2587 	inc	dptr
      005754 E0               [24] 2588 	movx	a,@dptr
      005755 FF               [12] 2589 	mov	r7,a
      005756 8C 82            [24] 2590 	mov	dpl,r4
      005758 8F 83            [24] 2591 	mov	dph,r7
      00575A E5 39            [12] 2592 	mov	a,_CRYPTO_KeySelectAndExpansion_sloc1_1_0
      00575C F0               [24] 2593 	movx	@dptr,a
      00575D 90 03 C1         [24] 2594 	mov	dptr,#_CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149
      005760 74 01            [12] 2595 	mov	a,#0x01
      005762 2C               [12] 2596 	add	a,r4
      005763 F0               [24] 2597 	movx	@dptr,a
      005764 E4               [12] 2598 	clr	a
      005765 3F               [12] 2599 	addc	a,r7
      005766 A3               [24] 2600 	inc	dptr
      005767 F0               [24] 2601 	movx	@dptr,a
                                   2602 ;	..\src\peripherals\cryptography.c:132: *AES_KEY_BUFFER++ = w[3];
      005768 90 03 C1         [24] 2603 	mov	dptr,#_CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149
      00576B E0               [24] 2604 	movx	a,@dptr
      00576C FC               [12] 2605 	mov	r4,a
      00576D A3               [24] 2606 	inc	dptr
      00576E E0               [24] 2607 	movx	a,@dptr
      00576F FF               [12] 2608 	mov	r7,a
      005770 8C 82            [24] 2609 	mov	dpl,r4
      005772 8F 83            [24] 2610 	mov	dph,r7
      005774 ED               [12] 2611 	mov	a,r5
      005775 F0               [24] 2612 	movx	@dptr,a
      005776 90 03 C1         [24] 2613 	mov	dptr,#_CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149
      005779 74 01            [12] 2614 	mov	a,#0x01
      00577B 2C               [12] 2615 	add	a,r4
      00577C F0               [24] 2616 	movx	@dptr,a
      00577D E4               [12] 2617 	clr	a
      00577E 3F               [12] 2618 	addc	a,r7
      00577F A3               [24] 2619 	inc	dptr
      005780 F0               [24] 2620 	movx	@dptr,a
                                   2621 ;	..\src\peripherals\cryptography.c:134: ++bnum;
      005781 0E               [12] 2622 	inc	r6
                                   2623 ;	..\src\peripherals\cryptography.c:135: if (bnum >= 4)
      005782 BE 04 00         [24] 2624 	cjne	r6,#0x04,00142$
      005785                       2625 00142$:
      005785 40 02            [24] 2626 	jc	00110$
                                   2627 ;	..\src\peripherals\cryptography.c:136: bnum = 0;
      005787 7E 00            [12] 2628 	mov	r6,#0x00
      005789                       2629 00110$:
                                   2630 ;	..\src\peripherals\cryptography.c:137: } while (--wcnt);
      005789 E5 32            [12] 2631 	mov	a,_CRYPTO_KeySelectAndExpansion_wcnt_65536_150
      00578B 14               [12] 2632 	dec	a
      00578C FF               [12] 2633 	mov	r7,a
      00578D 8F 32            [24] 2634 	mov	_CRYPTO_KeySelectAndExpansion_wcnt_65536_150,r7
      00578F 60 03            [24] 2635 	jz	00144$
      005791 02 55 9D         [24] 2636 	ljmp	00109$
      005794                       2637 00144$:
                                   2638 ;	..\src\peripherals\cryptography.c:138: AES_KEY_BUFFER = AES_KEY_BUFFER-176;
      005794 90 03 C1         [24] 2639 	mov	dptr,#_CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149
      005797 E0               [24] 2640 	movx	a,@dptr
      005798 FE               [12] 2641 	mov	r6,a
      005799 A3               [24] 2642 	inc	dptr
      00579A E0               [24] 2643 	movx	a,@dptr
      00579B FF               [12] 2644 	mov	r7,a
      00579C EE               [12] 2645 	mov	a,r6
      00579D 24 50            [12] 2646 	add	a,#0x50
      00579F FE               [12] 2647 	mov	r6,a
      0057A0 EF               [12] 2648 	mov	a,r7
      0057A1 34 FF            [12] 2649 	addc	a,#0xff
      0057A3 FF               [12] 2650 	mov	r7,a
      0057A4 90 03 C1         [24] 2651 	mov	dptr,#_CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149
      0057A7 EE               [12] 2652 	mov	a,r6
      0057A8 F0               [24] 2653 	movx	@dptr,a
      0057A9 EF               [12] 2654 	mov	a,r7
      0057AA A3               [24] 2655 	inc	dptr
      0057AB F0               [24] 2656 	movx	@dptr,a
                                   2657 ;	..\src\peripherals\cryptography.c:140: AESCONFIG = (AESCONFIG & 0xC0) | 10;
      0057AC 90 70 91         [24] 2658 	mov	dptr,#_AESCONFIG
      0057AF E0               [24] 2659 	movx	a,@dptr
      0057B0 54 C0            [12] 2660 	anl	a,#0xc0
      0057B2 44 0A            [12] 2661 	orl	a,#0x0a
      0057B4 F0               [24] 2662 	movx	@dptr,a
                                   2663 ;	..\src\peripherals\cryptography.c:141: AESKEYADDR0 = (uint16_t)AES_KEY_BUFFER;
      0057B5 90 03 C1         [24] 2664 	mov	dptr,#_CRYPTO_KeySelectAndExpansion_AES_KEY_BUFFER_65536_149
      0057B8 E0               [24] 2665 	movx	a,@dptr
      0057B9 FE               [12] 2666 	mov	r6,a
      0057BA A3               [24] 2667 	inc	dptr
      0057BB E0               [24] 2668 	movx	a,@dptr
      0057BC FF               [12] 2669 	mov	r7,a
      0057BD 90 70 92         [24] 2670 	mov	dptr,#_AESKEYADDR0
      0057C0 EE               [12] 2671 	mov	a,r6
      0057C1 F0               [24] 2672 	movx	@dptr,a
                                   2673 ;	..\src\peripherals\cryptography.c:142: AESKEYADDR1 = ((uint16_t)AES_KEY_BUFFER) >> 8;
      0057C2 90 70 93         [24] 2674 	mov	dptr,#_AESKEYADDR1
      0057C5 EF               [12] 2675 	mov	a,r7
      0057C6 F0               [24] 2676 	movx	@dptr,a
                                   2677 ;	..\src\peripherals\cryptography.c:143: return Rnd;
      0057C7 85 38 82         [24] 2678 	mov	dpl,_CRYPTO_KeySelectAndExpansion_sloc0_1_0
                                   2679 ;	..\src\peripherals\cryptography.c:144: }
      0057CA 22               [24] 2680 	ret
                                   2681 ;------------------------------------------------------------
                                   2682 ;Allocation info for local variables in function 'CRYPTO_EncryptData'
                                   2683 ;------------------------------------------------------------
                                   2684 ;AES_IN_ADDR               Allocated with name '_CRYPTO_EncryptData_AES_IN_ADDR_65536_153'
                                   2685 ;------------------------------------------------------------
                                   2686 ;	..\src\peripherals\cryptography.c:156: uint8_t CRYPTO_EncryptData(uint16_t AES_IN_ADDR)
                                   2687 ;	-----------------------------------------
                                   2688 ;	 function CRYPTO_EncryptData
                                   2689 ;	-----------------------------------------
      0057CB                       2690 _CRYPTO_EncryptData:
      0057CB AF 83            [24] 2691 	mov	r7,dph
      0057CD E5 82            [12] 2692 	mov	a,dpl
      0057CF 90 03 C3         [24] 2693 	mov	dptr,#_CRYPTO_EncryptData_AES_IN_ADDR_65536_153
      0057D2 F0               [24] 2694 	movx	@dptr,a
      0057D3 EF               [12] 2695 	mov	a,r7
      0057D4 A3               [24] 2696 	inc	dptr
      0057D5 F0               [24] 2697 	movx	@dptr,a
                                   2698 ;	..\src\peripherals\cryptography.c:159: AESINADDR0  = AES_IN_ADDR & 0x00FF;
      0057D6 90 03 C3         [24] 2699 	mov	dptr,#_CRYPTO_EncryptData_AES_IN_ADDR_65536_153
      0057D9 E0               [24] 2700 	movx	a,@dptr
      0057DA FE               [12] 2701 	mov	r6,a
      0057DB A3               [24] 2702 	inc	dptr
      0057DC E0               [24] 2703 	movx	a,@dptr
      0057DD FF               [12] 2704 	mov	r7,a
      0057DE 90 70 94         [24] 2705 	mov	dptr,#_AESINADDR0
      0057E1 EE               [12] 2706 	mov	a,r6
      0057E2 F0               [24] 2707 	movx	@dptr,a
                                   2708 ;	..\src\peripherals\cryptography.c:160: AESINADDR1  = (AES_IN_ADDR & 0xFF00) >> 8;
      0057E3 90 70 95         [24] 2709 	mov	dptr,#_AESINADDR1
      0057E6 EF               [12] 2710 	mov	a,r7
      0057E7 F0               [24] 2711 	movx	@dptr,a
                                   2712 ;	..\src\peripherals\cryptography.c:161: AESMODE = 0x82; //starts encryption + encrypts 2 blocks of 16 bytes
      0057E8 90 70 90         [24] 2713 	mov	dptr,#_AESMODE
      0057EB 74 82            [12] 2714 	mov	a,#0x82
      0057ED F0               [24] 2715 	movx	@dptr,a
                                   2716 ;	..\src\peripherals\cryptography.c:162: dbglink_writestr(" a intentar encriptar");
      0057EE 90 92 F8         [24] 2717 	mov	dptr,#___str_0
      0057F1 75 F0 80         [24] 2718 	mov	b,#0x80
      0057F4 12 82 8A         [24] 2719 	lcall	_dbglink_writestr
                                   2720 ;	..\src\peripherals\cryptography.c:163: do
      0057F7                       2721 00101$:
                                   2722 ;	..\src\peripherals\cryptography.c:165: delay_ms(1);
      0057F7 90 00 01         [24] 2723 	mov	dptr,#0x0001
      0057FA 12 09 31         [24] 2724 	lcall	_delay_ms
                                   2725 ;	..\src\peripherals\cryptography.c:166: }while(AESCURBLOCK & 0x80);
      0057FD 90 70 98         [24] 2726 	mov	dptr,#_AESCURBLOCK
      005800 E0               [24] 2727 	movx	a,@dptr
      005801 20 E7 F3         [24] 2728 	jb	acc.7,00101$
                                   2729 ;	..\src\peripherals\cryptography.c:167: return 1;
      005804 75 82 01         [24] 2730 	mov	dpl,#0x01
                                   2731 ;	..\src\peripherals\cryptography.c:168: }
      005807 22               [24] 2732 	ret
                                   2733 	.area CSEG    (CODE)
                                   2734 	.area CONST   (CODE)
      0091F8                       2735 _aes_sbox:
      0091F8 63                    2736 	.db #0x63	; 99	'c'
      0091F9 7C                    2737 	.db #0x7c	; 124
      0091FA 77                    2738 	.db #0x77	; 119	'w'
      0091FB 7B                    2739 	.db #0x7b	; 123
      0091FC F2                    2740 	.db #0xf2	; 242
      0091FD 6B                    2741 	.db #0x6b	; 107	'k'
      0091FE 6F                    2742 	.db #0x6f	; 111	'o'
      0091FF C5                    2743 	.db #0xc5	; 197
      009200 30                    2744 	.db #0x30	; 48	'0'
      009201 01                    2745 	.db #0x01	; 1
      009202 67                    2746 	.db #0x67	; 103	'g'
      009203 2B                    2747 	.db #0x2b	; 43
      009204 FE                    2748 	.db #0xfe	; 254
      009205 D7                    2749 	.db #0xd7	; 215
      009206 AB                    2750 	.db #0xab	; 171
      009207 76                    2751 	.db #0x76	; 118	'v'
      009208 CA                    2752 	.db #0xca	; 202
      009209 82                    2753 	.db #0x82	; 130
      00920A C9                    2754 	.db #0xc9	; 201
      00920B 7D                    2755 	.db #0x7d	; 125
      00920C FA                    2756 	.db #0xfa	; 250
      00920D 59                    2757 	.db #0x59	; 89	'Y'
      00920E 47                    2758 	.db #0x47	; 71	'G'
      00920F F0                    2759 	.db #0xf0	; 240
      009210 AD                    2760 	.db #0xad	; 173
      009211 D4                    2761 	.db #0xd4	; 212
      009212 A2                    2762 	.db #0xa2	; 162
      009213 AF                    2763 	.db #0xaf	; 175
      009214 9C                    2764 	.db #0x9c	; 156
      009215 A4                    2765 	.db #0xa4	; 164
      009216 72                    2766 	.db #0x72	; 114	'r'
      009217 C0                    2767 	.db #0xc0	; 192
      009218 B7                    2768 	.db #0xb7	; 183
      009219 FD                    2769 	.db #0xfd	; 253
      00921A 93                    2770 	.db #0x93	; 147
      00921B 26                    2771 	.db #0x26	; 38
      00921C 36                    2772 	.db #0x36	; 54	'6'
      00921D 3F                    2773 	.db #0x3f	; 63
      00921E F7                    2774 	.db #0xf7	; 247
      00921F CC                    2775 	.db #0xcc	; 204
      009220 34                    2776 	.db #0x34	; 52	'4'
      009221 A5                    2777 	.db #0xa5	; 165
      009222 E5                    2778 	.db #0xe5	; 229
      009223 F1                    2779 	.db #0xf1	; 241
      009224 71                    2780 	.db #0x71	; 113	'q'
      009225 D8                    2781 	.db #0xd8	; 216
      009226 31                    2782 	.db #0x31	; 49	'1'
      009227 15                    2783 	.db #0x15	; 21
      009228 04                    2784 	.db #0x04	; 4
      009229 C7                    2785 	.db #0xc7	; 199
      00922A 23                    2786 	.db #0x23	; 35
      00922B C3                    2787 	.db #0xc3	; 195
      00922C 18                    2788 	.db #0x18	; 24
      00922D 96                    2789 	.db #0x96	; 150
      00922E 05                    2790 	.db #0x05	; 5
      00922F 9A                    2791 	.db #0x9a	; 154
      009230 07                    2792 	.db #0x07	; 7
      009231 12                    2793 	.db #0x12	; 18
      009232 80                    2794 	.db #0x80	; 128
      009233 E2                    2795 	.db #0xe2	; 226
      009234 EB                    2796 	.db #0xeb	; 235
      009235 27                    2797 	.db #0x27	; 39
      009236 B2                    2798 	.db #0xb2	; 178
      009237 75                    2799 	.db #0x75	; 117	'u'
      009238 09                    2800 	.db #0x09	; 9
      009239 83                    2801 	.db #0x83	; 131
      00923A 2C                    2802 	.db #0x2c	; 44
      00923B 1A                    2803 	.db #0x1a	; 26
      00923C 1B                    2804 	.db #0x1b	; 27
      00923D 6E                    2805 	.db #0x6e	; 110	'n'
      00923E 5A                    2806 	.db #0x5a	; 90	'Z'
      00923F A0                    2807 	.db #0xa0	; 160
      009240 52                    2808 	.db #0x52	; 82	'R'
      009241 3B                    2809 	.db #0x3b	; 59
      009242 D6                    2810 	.db #0xd6	; 214
      009243 B3                    2811 	.db #0xb3	; 179
      009244 29                    2812 	.db #0x29	; 41
      009245 E3                    2813 	.db #0xe3	; 227
      009246 2F                    2814 	.db #0x2f	; 47
      009247 84                    2815 	.db #0x84	; 132
      009248 53                    2816 	.db #0x53	; 83	'S'
      009249 D1                    2817 	.db #0xd1	; 209
      00924A 00                    2818 	.db #0x00	; 0
      00924B ED                    2819 	.db #0xed	; 237
      00924C 20                    2820 	.db #0x20	; 32
      00924D FC                    2821 	.db #0xfc	; 252
      00924E B1                    2822 	.db #0xb1	; 177
      00924F 5B                    2823 	.db #0x5b	; 91
      009250 6A                    2824 	.db #0x6a	; 106	'j'
      009251 CB                    2825 	.db #0xcb	; 203
      009252 BE                    2826 	.db #0xbe	; 190
      009253 39                    2827 	.db #0x39	; 57	'9'
      009254 4A                    2828 	.db #0x4a	; 74	'J'
      009255 4C                    2829 	.db #0x4c	; 76	'L'
      009256 58                    2830 	.db #0x58	; 88	'X'
      009257 CF                    2831 	.db #0xcf	; 207
      009258 D0                    2832 	.db #0xd0	; 208
      009259 EF                    2833 	.db #0xef	; 239
      00925A AA                    2834 	.db #0xaa	; 170
      00925B FB                    2835 	.db #0xfb	; 251
      00925C 43                    2836 	.db #0x43	; 67	'C'
      00925D 4D                    2837 	.db #0x4d	; 77	'M'
      00925E 33                    2838 	.db #0x33	; 51	'3'
      00925F 85                    2839 	.db #0x85	; 133
      009260 45                    2840 	.db #0x45	; 69	'E'
      009261 F9                    2841 	.db #0xf9	; 249
      009262 02                    2842 	.db #0x02	; 2
      009263 7F                    2843 	.db #0x7f	; 127
      009264 50                    2844 	.db #0x50	; 80	'P'
      009265 3C                    2845 	.db #0x3c	; 60
      009266 9F                    2846 	.db #0x9f	; 159
      009267 A8                    2847 	.db #0xa8	; 168
      009268 51                    2848 	.db #0x51	; 81	'Q'
      009269 A3                    2849 	.db #0xa3	; 163
      00926A 40                    2850 	.db #0x40	; 64
      00926B 8F                    2851 	.db #0x8f	; 143
      00926C 92                    2852 	.db #0x92	; 146
      00926D 9D                    2853 	.db #0x9d	; 157
      00926E 38                    2854 	.db #0x38	; 56	'8'
      00926F F5                    2855 	.db #0xf5	; 245
      009270 BC                    2856 	.db #0xbc	; 188
      009271 B6                    2857 	.db #0xb6	; 182
      009272 DA                    2858 	.db #0xda	; 218
      009273 21                    2859 	.db #0x21	; 33
      009274 10                    2860 	.db #0x10	; 16
      009275 FF                    2861 	.db #0xff	; 255
      009276 F3                    2862 	.db #0xf3	; 243
      009277 D2                    2863 	.db #0xd2	; 210
      009278 CD                    2864 	.db #0xcd	; 205
      009279 0C                    2865 	.db #0x0c	; 12
      00927A 13                    2866 	.db #0x13	; 19
      00927B EC                    2867 	.db #0xec	; 236
      00927C 5F                    2868 	.db #0x5f	; 95
      00927D 97                    2869 	.db #0x97	; 151
      00927E 44                    2870 	.db #0x44	; 68	'D'
      00927F 17                    2871 	.db #0x17	; 23
      009280 C4                    2872 	.db #0xc4	; 196
      009281 A7                    2873 	.db #0xa7	; 167
      009282 7E                    2874 	.db #0x7e	; 126
      009283 3D                    2875 	.db #0x3d	; 61
      009284 64                    2876 	.db #0x64	; 100	'd'
      009285 5D                    2877 	.db #0x5d	; 93
      009286 19                    2878 	.db #0x19	; 25
      009287 73                    2879 	.db #0x73	; 115	's'
      009288 60                    2880 	.db #0x60	; 96
      009289 81                    2881 	.db #0x81	; 129
      00928A 4F                    2882 	.db #0x4f	; 79	'O'
      00928B DC                    2883 	.db #0xdc	; 220
      00928C 22                    2884 	.db #0x22	; 34
      00928D 2A                    2885 	.db #0x2a	; 42
      00928E 90                    2886 	.db #0x90	; 144
      00928F 88                    2887 	.db #0x88	; 136
      009290 46                    2888 	.db #0x46	; 70	'F'
      009291 EE                    2889 	.db #0xee	; 238
      009292 B8                    2890 	.db #0xb8	; 184
      009293 14                    2891 	.db #0x14	; 20
      009294 DE                    2892 	.db #0xde	; 222
      009295 5E                    2893 	.db #0x5e	; 94
      009296 0B                    2894 	.db #0x0b	; 11
      009297 DB                    2895 	.db #0xdb	; 219
      009298 E0                    2896 	.db #0xe0	; 224
      009299 32                    2897 	.db #0x32	; 50	'2'
      00929A 3A                    2898 	.db #0x3a	; 58
      00929B 0A                    2899 	.db #0x0a	; 10
      00929C 49                    2900 	.db #0x49	; 73	'I'
      00929D 06                    2901 	.db #0x06	; 6
      00929E 24                    2902 	.db #0x24	; 36
      00929F 5C                    2903 	.db #0x5c	; 92
      0092A0 C2                    2904 	.db #0xc2	; 194
      0092A1 D3                    2905 	.db #0xd3	; 211
      0092A2 AC                    2906 	.db #0xac	; 172
      0092A3 62                    2907 	.db #0x62	; 98	'b'
      0092A4 91                    2908 	.db #0x91	; 145
      0092A5 95                    2909 	.db #0x95	; 149
      0092A6 E4                    2910 	.db #0xe4	; 228
      0092A7 79                    2911 	.db #0x79	; 121	'y'
      0092A8 E7                    2912 	.db #0xe7	; 231
      0092A9 C8                    2913 	.db #0xc8	; 200
      0092AA 37                    2914 	.db #0x37	; 55	'7'
      0092AB 6D                    2915 	.db #0x6d	; 109	'm'
      0092AC 8D                    2916 	.db #0x8d	; 141
      0092AD D5                    2917 	.db #0xd5	; 213
      0092AE 4E                    2918 	.db #0x4e	; 78	'N'
      0092AF A9                    2919 	.db #0xa9	; 169
      0092B0 6C                    2920 	.db #0x6c	; 108	'l'
      0092B1 56                    2921 	.db #0x56	; 86	'V'
      0092B2 F4                    2922 	.db #0xf4	; 244
      0092B3 EA                    2923 	.db #0xea	; 234
      0092B4 65                    2924 	.db #0x65	; 101	'e'
      0092B5 7A                    2925 	.db #0x7a	; 122	'z'
      0092B6 AE                    2926 	.db #0xae	; 174
      0092B7 08                    2927 	.db #0x08	; 8
      0092B8 BA                    2928 	.db #0xba	; 186
      0092B9 78                    2929 	.db #0x78	; 120	'x'
      0092BA 25                    2930 	.db #0x25	; 37
      0092BB 2E                    2931 	.db #0x2e	; 46
      0092BC 1C                    2932 	.db #0x1c	; 28
      0092BD A6                    2933 	.db #0xa6	; 166
      0092BE B4                    2934 	.db #0xb4	; 180
      0092BF C6                    2935 	.db #0xc6	; 198
      0092C0 E8                    2936 	.db #0xe8	; 232
      0092C1 DD                    2937 	.db #0xdd	; 221
      0092C2 74                    2938 	.db #0x74	; 116	't'
      0092C3 1F                    2939 	.db #0x1f	; 31
      0092C4 4B                    2940 	.db #0x4b	; 75	'K'
      0092C5 BD                    2941 	.db #0xbd	; 189
      0092C6 8B                    2942 	.db #0x8b	; 139
      0092C7 8A                    2943 	.db #0x8a	; 138
      0092C8 70                    2944 	.db #0x70	; 112	'p'
      0092C9 3E                    2945 	.db #0x3e	; 62
      0092CA B5                    2946 	.db #0xb5	; 181
      0092CB 66                    2947 	.db #0x66	; 102	'f'
      0092CC 48                    2948 	.db #0x48	; 72	'H'
      0092CD 03                    2949 	.db #0x03	; 3
      0092CE F6                    2950 	.db #0xf6	; 246
      0092CF 0E                    2951 	.db #0x0e	; 14
      0092D0 61                    2952 	.db #0x61	; 97	'a'
      0092D1 35                    2953 	.db #0x35	; 53	'5'
      0092D2 57                    2954 	.db #0x57	; 87	'W'
      0092D3 B9                    2955 	.db #0xb9	; 185
      0092D4 86                    2956 	.db #0x86	; 134
      0092D5 C1                    2957 	.db #0xc1	; 193
      0092D6 1D                    2958 	.db #0x1d	; 29
      0092D7 9E                    2959 	.db #0x9e	; 158
      0092D8 E1                    2960 	.db #0xe1	; 225
      0092D9 F8                    2961 	.db #0xf8	; 248
      0092DA 98                    2962 	.db #0x98	; 152
      0092DB 11                    2963 	.db #0x11	; 17
      0092DC 69                    2964 	.db #0x69	; 105	'i'
      0092DD D9                    2965 	.db #0xd9	; 217
      0092DE 8E                    2966 	.db #0x8e	; 142
      0092DF 94                    2967 	.db #0x94	; 148
      0092E0 9B                    2968 	.db #0x9b	; 155
      0092E1 1E                    2969 	.db #0x1e	; 30
      0092E2 87                    2970 	.db #0x87	; 135
      0092E3 E9                    2971 	.db #0xe9	; 233
      0092E4 CE                    2972 	.db #0xce	; 206
      0092E5 55                    2973 	.db #0x55	; 85	'U'
      0092E6 28                    2974 	.db #0x28	; 40
      0092E7 DF                    2975 	.db #0xdf	; 223
      0092E8 8C                    2976 	.db #0x8c	; 140
      0092E9 A1                    2977 	.db #0xa1	; 161
      0092EA 89                    2978 	.db #0x89	; 137
      0092EB 0D                    2979 	.db #0x0d	; 13
      0092EC BF                    2980 	.db #0xbf	; 191
      0092ED E6                    2981 	.db #0xe6	; 230
      0092EE 42                    2982 	.db #0x42	; 66	'B'
      0092EF 68                    2983 	.db #0x68	; 104	'h'
      0092F0 41                    2984 	.db #0x41	; 65	'A'
      0092F1 99                    2985 	.db #0x99	; 153
      0092F2 2D                    2986 	.db #0x2d	; 45
      0092F3 0F                    2987 	.db #0x0f	; 15
      0092F4 B0                    2988 	.db #0xb0	; 176
      0092F5 54                    2989 	.db #0x54	; 84	'T'
      0092F6 BB                    2990 	.db #0xbb	; 187
      0092F7 16                    2991 	.db #0x16	; 22
                                   2992 	.area CONST   (CODE)
      0092F8                       2993 ___str_0:
      0092F8 20 61 20 69 6E 74 65  2994 	.ascii " a intentar encriptar"
             6E 74 61 72 20 65 6E
             63 72 69 70 74 61 72
      00930D 00                    2995 	.db 0x00
                                   2996 	.area CSEG    (CODE)
                                   2997 	.area XINIT   (CODE)
                                   2998 	.area CABS    (ABS,CODE)
