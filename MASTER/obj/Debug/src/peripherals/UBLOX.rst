                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.9.3 #11354 (MINGW32)
                                      4 ;--------------------------------------------------------
                                      5 	.module UBLOX
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _wtimer_runcallbacks
                                     12 	.globl _dbglink_writestr
                                     13 	.globl _memcpy
                                     14 	.globl _free
                                     15 	.globl _malloc
                                     16 	.globl _uart_timer1_baud
                                     17 	.globl _uart1_tx
                                     18 	.globl _uart1_rx
                                     19 	.globl _uart1_init
                                     20 	.globl _uart1_rxcount
                                     21 	.globl _delay
                                     22 	.globl _PORTC_7
                                     23 	.globl _PORTC_6
                                     24 	.globl _PORTC_5
                                     25 	.globl _PORTC_4
                                     26 	.globl _PORTC_3
                                     27 	.globl _PORTC_2
                                     28 	.globl _PORTC_1
                                     29 	.globl _PORTC_0
                                     30 	.globl _PORTB_7
                                     31 	.globl _PORTB_6
                                     32 	.globl _PORTB_5
                                     33 	.globl _PORTB_4
                                     34 	.globl _PORTB_3
                                     35 	.globl _PORTB_2
                                     36 	.globl _PORTB_1
                                     37 	.globl _PORTB_0
                                     38 	.globl _PORTA_7
                                     39 	.globl _PORTA_6
                                     40 	.globl _PORTA_5
                                     41 	.globl _PORTA_4
                                     42 	.globl _PORTA_3
                                     43 	.globl _PORTA_2
                                     44 	.globl _PORTA_1
                                     45 	.globl _PORTA_0
                                     46 	.globl _PINC_7
                                     47 	.globl _PINC_6
                                     48 	.globl _PINC_5
                                     49 	.globl _PINC_4
                                     50 	.globl _PINC_3
                                     51 	.globl _PINC_2
                                     52 	.globl _PINC_1
                                     53 	.globl _PINC_0
                                     54 	.globl _PINB_7
                                     55 	.globl _PINB_6
                                     56 	.globl _PINB_5
                                     57 	.globl _PINB_4
                                     58 	.globl _PINB_3
                                     59 	.globl _PINB_2
                                     60 	.globl _PINB_1
                                     61 	.globl _PINB_0
                                     62 	.globl _PINA_7
                                     63 	.globl _PINA_6
                                     64 	.globl _PINA_5
                                     65 	.globl _PINA_4
                                     66 	.globl _PINA_3
                                     67 	.globl _PINA_2
                                     68 	.globl _PINA_1
                                     69 	.globl _PINA_0
                                     70 	.globl _CY
                                     71 	.globl _AC
                                     72 	.globl _F0
                                     73 	.globl _RS1
                                     74 	.globl _RS0
                                     75 	.globl _OV
                                     76 	.globl _F1
                                     77 	.globl _P
                                     78 	.globl _IP_7
                                     79 	.globl _IP_6
                                     80 	.globl _IP_5
                                     81 	.globl _IP_4
                                     82 	.globl _IP_3
                                     83 	.globl _IP_2
                                     84 	.globl _IP_1
                                     85 	.globl _IP_0
                                     86 	.globl _EA
                                     87 	.globl _IE_7
                                     88 	.globl _IE_6
                                     89 	.globl _IE_5
                                     90 	.globl _IE_4
                                     91 	.globl _IE_3
                                     92 	.globl _IE_2
                                     93 	.globl _IE_1
                                     94 	.globl _IE_0
                                     95 	.globl _EIP_7
                                     96 	.globl _EIP_6
                                     97 	.globl _EIP_5
                                     98 	.globl _EIP_4
                                     99 	.globl _EIP_3
                                    100 	.globl _EIP_2
                                    101 	.globl _EIP_1
                                    102 	.globl _EIP_0
                                    103 	.globl _EIE_7
                                    104 	.globl _EIE_6
                                    105 	.globl _EIE_5
                                    106 	.globl _EIE_4
                                    107 	.globl _EIE_3
                                    108 	.globl _EIE_2
                                    109 	.globl _EIE_1
                                    110 	.globl _EIE_0
                                    111 	.globl _E2IP_7
                                    112 	.globl _E2IP_6
                                    113 	.globl _E2IP_5
                                    114 	.globl _E2IP_4
                                    115 	.globl _E2IP_3
                                    116 	.globl _E2IP_2
                                    117 	.globl _E2IP_1
                                    118 	.globl _E2IP_0
                                    119 	.globl _E2IE_7
                                    120 	.globl _E2IE_6
                                    121 	.globl _E2IE_5
                                    122 	.globl _E2IE_4
                                    123 	.globl _E2IE_3
                                    124 	.globl _E2IE_2
                                    125 	.globl _E2IE_1
                                    126 	.globl _E2IE_0
                                    127 	.globl _B_7
                                    128 	.globl _B_6
                                    129 	.globl _B_5
                                    130 	.globl _B_4
                                    131 	.globl _B_3
                                    132 	.globl _B_2
                                    133 	.globl _B_1
                                    134 	.globl _B_0
                                    135 	.globl _ACC_7
                                    136 	.globl _ACC_6
                                    137 	.globl _ACC_5
                                    138 	.globl _ACC_4
                                    139 	.globl _ACC_3
                                    140 	.globl _ACC_2
                                    141 	.globl _ACC_1
                                    142 	.globl _ACC_0
                                    143 	.globl _WTSTAT
                                    144 	.globl _WTIRQEN
                                    145 	.globl _WTEVTD
                                    146 	.globl _WTEVTD1
                                    147 	.globl _WTEVTD0
                                    148 	.globl _WTEVTC
                                    149 	.globl _WTEVTC1
                                    150 	.globl _WTEVTC0
                                    151 	.globl _WTEVTB
                                    152 	.globl _WTEVTB1
                                    153 	.globl _WTEVTB0
                                    154 	.globl _WTEVTA
                                    155 	.globl _WTEVTA1
                                    156 	.globl _WTEVTA0
                                    157 	.globl _WTCNTR1
                                    158 	.globl _WTCNTB
                                    159 	.globl _WTCNTB1
                                    160 	.globl _WTCNTB0
                                    161 	.globl _WTCNTA
                                    162 	.globl _WTCNTA1
                                    163 	.globl _WTCNTA0
                                    164 	.globl _WTCFGB
                                    165 	.globl _WTCFGA
                                    166 	.globl _WDTRESET
                                    167 	.globl _WDTCFG
                                    168 	.globl _U1STATUS
                                    169 	.globl _U1SHREG
                                    170 	.globl _U1MODE
                                    171 	.globl _U1CTRL
                                    172 	.globl _U0STATUS
                                    173 	.globl _U0SHREG
                                    174 	.globl _U0MODE
                                    175 	.globl _U0CTRL
                                    176 	.globl _T2STATUS
                                    177 	.globl _T2PERIOD
                                    178 	.globl _T2PERIOD1
                                    179 	.globl _T2PERIOD0
                                    180 	.globl _T2MODE
                                    181 	.globl _T2CNT
                                    182 	.globl _T2CNT1
                                    183 	.globl _T2CNT0
                                    184 	.globl _T2CLKSRC
                                    185 	.globl _T1STATUS
                                    186 	.globl _T1PERIOD
                                    187 	.globl _T1PERIOD1
                                    188 	.globl _T1PERIOD0
                                    189 	.globl _T1MODE
                                    190 	.globl _T1CNT
                                    191 	.globl _T1CNT1
                                    192 	.globl _T1CNT0
                                    193 	.globl _T1CLKSRC
                                    194 	.globl _T0STATUS
                                    195 	.globl _T0PERIOD
                                    196 	.globl _T0PERIOD1
                                    197 	.globl _T0PERIOD0
                                    198 	.globl _T0MODE
                                    199 	.globl _T0CNT
                                    200 	.globl _T0CNT1
                                    201 	.globl _T0CNT0
                                    202 	.globl _T0CLKSRC
                                    203 	.globl _SPSTATUS
                                    204 	.globl _SPSHREG
                                    205 	.globl _SPMODE
                                    206 	.globl _SPCLKSRC
                                    207 	.globl _RADIOSTAT
                                    208 	.globl _RADIOSTAT1
                                    209 	.globl _RADIOSTAT0
                                    210 	.globl _RADIODATA
                                    211 	.globl _RADIODATA3
                                    212 	.globl _RADIODATA2
                                    213 	.globl _RADIODATA1
                                    214 	.globl _RADIODATA0
                                    215 	.globl _RADIOADDR
                                    216 	.globl _RADIOADDR1
                                    217 	.globl _RADIOADDR0
                                    218 	.globl _RADIOACC
                                    219 	.globl _OC1STATUS
                                    220 	.globl _OC1PIN
                                    221 	.globl _OC1MODE
                                    222 	.globl _OC1COMP
                                    223 	.globl _OC1COMP1
                                    224 	.globl _OC1COMP0
                                    225 	.globl _OC0STATUS
                                    226 	.globl _OC0PIN
                                    227 	.globl _OC0MODE
                                    228 	.globl _OC0COMP
                                    229 	.globl _OC0COMP1
                                    230 	.globl _OC0COMP0
                                    231 	.globl _NVSTATUS
                                    232 	.globl _NVKEY
                                    233 	.globl _NVDATA
                                    234 	.globl _NVDATA1
                                    235 	.globl _NVDATA0
                                    236 	.globl _NVADDR
                                    237 	.globl _NVADDR1
                                    238 	.globl _NVADDR0
                                    239 	.globl _IC1STATUS
                                    240 	.globl _IC1MODE
                                    241 	.globl _IC1CAPT
                                    242 	.globl _IC1CAPT1
                                    243 	.globl _IC1CAPT0
                                    244 	.globl _IC0STATUS
                                    245 	.globl _IC0MODE
                                    246 	.globl _IC0CAPT
                                    247 	.globl _IC0CAPT1
                                    248 	.globl _IC0CAPT0
                                    249 	.globl _PORTR
                                    250 	.globl _PORTC
                                    251 	.globl _PORTB
                                    252 	.globl _PORTA
                                    253 	.globl _PINR
                                    254 	.globl _PINC
                                    255 	.globl _PINB
                                    256 	.globl _PINA
                                    257 	.globl _DIRR
                                    258 	.globl _DIRC
                                    259 	.globl _DIRB
                                    260 	.globl _DIRA
                                    261 	.globl _DBGLNKSTAT
                                    262 	.globl _DBGLNKBUF
                                    263 	.globl _CODECONFIG
                                    264 	.globl _CLKSTAT
                                    265 	.globl _CLKCON
                                    266 	.globl _ANALOGCOMP
                                    267 	.globl _ADCCONV
                                    268 	.globl _ADCCLKSRC
                                    269 	.globl _ADCCH3CONFIG
                                    270 	.globl _ADCCH2CONFIG
                                    271 	.globl _ADCCH1CONFIG
                                    272 	.globl _ADCCH0CONFIG
                                    273 	.globl __XPAGE
                                    274 	.globl _XPAGE
                                    275 	.globl _SP
                                    276 	.globl _PSW
                                    277 	.globl _PCON
                                    278 	.globl _IP
                                    279 	.globl _IE
                                    280 	.globl _EIP
                                    281 	.globl _EIE
                                    282 	.globl _E2IP
                                    283 	.globl _E2IE
                                    284 	.globl _DPS
                                    285 	.globl _DPTR1
                                    286 	.globl _DPTR0
                                    287 	.globl _DPL1
                                    288 	.globl _DPL
                                    289 	.globl _DPH1
                                    290 	.globl _DPH
                                    291 	.globl _B
                                    292 	.globl _ACC
                                    293 	.globl _UBLOX_GPS_SendCommand_WaitACK_PARM_6
                                    294 	.globl _UBLOX_GPS_SendCommand_WaitACK_PARM_5
                                    295 	.globl _UBLOX_GPS_SendCommand_WaitACK_PARM_4
                                    296 	.globl _UBLOX_GPS_SendCommand_WaitACK_PARM_3
                                    297 	.globl _UBLOX_GPS_SendCommand_WaitACK_PARM_2
                                    298 	.globl _UBLOX_GPS_FletcherChecksum8_PARM_4
                                    299 	.globl _UBLOX_GPS_FletcherChecksum8_PARM_3
                                    300 	.globl _UBLOX_GPS_FletcherChecksum8_PARM_2
                                    301 	.globl _AX5043_TIMEGAIN3NB
                                    302 	.globl _AX5043_TIMEGAIN2NB
                                    303 	.globl _AX5043_TIMEGAIN1NB
                                    304 	.globl _AX5043_TIMEGAIN0NB
                                    305 	.globl _AX5043_RXPARAMSETSNB
                                    306 	.globl _AX5043_RXPARAMCURSETNB
                                    307 	.globl _AX5043_PKTMAXLENNB
                                    308 	.globl _AX5043_PKTLENOFFSETNB
                                    309 	.globl _AX5043_PKTLENCFGNB
                                    310 	.globl _AX5043_PKTADDRMASK3NB
                                    311 	.globl _AX5043_PKTADDRMASK2NB
                                    312 	.globl _AX5043_PKTADDRMASK1NB
                                    313 	.globl _AX5043_PKTADDRMASK0NB
                                    314 	.globl _AX5043_PKTADDRCFGNB
                                    315 	.globl _AX5043_PKTADDR3NB
                                    316 	.globl _AX5043_PKTADDR2NB
                                    317 	.globl _AX5043_PKTADDR1NB
                                    318 	.globl _AX5043_PKTADDR0NB
                                    319 	.globl _AX5043_PHASEGAIN3NB
                                    320 	.globl _AX5043_PHASEGAIN2NB
                                    321 	.globl _AX5043_PHASEGAIN1NB
                                    322 	.globl _AX5043_PHASEGAIN0NB
                                    323 	.globl _AX5043_FREQUENCYLEAKNB
                                    324 	.globl _AX5043_FREQUENCYGAIND3NB
                                    325 	.globl _AX5043_FREQUENCYGAIND2NB
                                    326 	.globl _AX5043_FREQUENCYGAIND1NB
                                    327 	.globl _AX5043_FREQUENCYGAIND0NB
                                    328 	.globl _AX5043_FREQUENCYGAINC3NB
                                    329 	.globl _AX5043_FREQUENCYGAINC2NB
                                    330 	.globl _AX5043_FREQUENCYGAINC1NB
                                    331 	.globl _AX5043_FREQUENCYGAINC0NB
                                    332 	.globl _AX5043_FREQUENCYGAINB3NB
                                    333 	.globl _AX5043_FREQUENCYGAINB2NB
                                    334 	.globl _AX5043_FREQUENCYGAINB1NB
                                    335 	.globl _AX5043_FREQUENCYGAINB0NB
                                    336 	.globl _AX5043_FREQUENCYGAINA3NB
                                    337 	.globl _AX5043_FREQUENCYGAINA2NB
                                    338 	.globl _AX5043_FREQUENCYGAINA1NB
                                    339 	.globl _AX5043_FREQUENCYGAINA0NB
                                    340 	.globl _AX5043_FREQDEV13NB
                                    341 	.globl _AX5043_FREQDEV12NB
                                    342 	.globl _AX5043_FREQDEV11NB
                                    343 	.globl _AX5043_FREQDEV10NB
                                    344 	.globl _AX5043_FREQDEV03NB
                                    345 	.globl _AX5043_FREQDEV02NB
                                    346 	.globl _AX5043_FREQDEV01NB
                                    347 	.globl _AX5043_FREQDEV00NB
                                    348 	.globl _AX5043_FOURFSK3NB
                                    349 	.globl _AX5043_FOURFSK2NB
                                    350 	.globl _AX5043_FOURFSK1NB
                                    351 	.globl _AX5043_FOURFSK0NB
                                    352 	.globl _AX5043_DRGAIN3NB
                                    353 	.globl _AX5043_DRGAIN2NB
                                    354 	.globl _AX5043_DRGAIN1NB
                                    355 	.globl _AX5043_DRGAIN0NB
                                    356 	.globl _AX5043_BBOFFSRES3NB
                                    357 	.globl _AX5043_BBOFFSRES2NB
                                    358 	.globl _AX5043_BBOFFSRES1NB
                                    359 	.globl _AX5043_BBOFFSRES0NB
                                    360 	.globl _AX5043_AMPLITUDEGAIN3NB
                                    361 	.globl _AX5043_AMPLITUDEGAIN2NB
                                    362 	.globl _AX5043_AMPLITUDEGAIN1NB
                                    363 	.globl _AX5043_AMPLITUDEGAIN0NB
                                    364 	.globl _AX5043_AGCTARGET3NB
                                    365 	.globl _AX5043_AGCTARGET2NB
                                    366 	.globl _AX5043_AGCTARGET1NB
                                    367 	.globl _AX5043_AGCTARGET0NB
                                    368 	.globl _AX5043_AGCMINMAX3NB
                                    369 	.globl _AX5043_AGCMINMAX2NB
                                    370 	.globl _AX5043_AGCMINMAX1NB
                                    371 	.globl _AX5043_AGCMINMAX0NB
                                    372 	.globl _AX5043_AGCGAIN3NB
                                    373 	.globl _AX5043_AGCGAIN2NB
                                    374 	.globl _AX5043_AGCGAIN1NB
                                    375 	.globl _AX5043_AGCGAIN0NB
                                    376 	.globl _AX5043_AGCAHYST3NB
                                    377 	.globl _AX5043_AGCAHYST2NB
                                    378 	.globl _AX5043_AGCAHYST1NB
                                    379 	.globl _AX5043_AGCAHYST0NB
                                    380 	.globl _AX5043_0xF44NB
                                    381 	.globl _AX5043_0xF35NB
                                    382 	.globl _AX5043_0xF34NB
                                    383 	.globl _AX5043_0xF33NB
                                    384 	.globl _AX5043_0xF32NB
                                    385 	.globl _AX5043_0xF31NB
                                    386 	.globl _AX5043_0xF30NB
                                    387 	.globl _AX5043_0xF26NB
                                    388 	.globl _AX5043_0xF23NB
                                    389 	.globl _AX5043_0xF22NB
                                    390 	.globl _AX5043_0xF21NB
                                    391 	.globl _AX5043_0xF1CNB
                                    392 	.globl _AX5043_0xF18NB
                                    393 	.globl _AX5043_0xF0CNB
                                    394 	.globl _AX5043_0xF00NB
                                    395 	.globl _AX5043_XTALSTATUSNB
                                    396 	.globl _AX5043_XTALOSCNB
                                    397 	.globl _AX5043_XTALCAPNB
                                    398 	.globl _AX5043_XTALAMPLNB
                                    399 	.globl _AX5043_WAKEUPXOEARLYNB
                                    400 	.globl _AX5043_WAKEUPTIMER1NB
                                    401 	.globl _AX5043_WAKEUPTIMER0NB
                                    402 	.globl _AX5043_WAKEUPFREQ1NB
                                    403 	.globl _AX5043_WAKEUPFREQ0NB
                                    404 	.globl _AX5043_WAKEUP1NB
                                    405 	.globl _AX5043_WAKEUP0NB
                                    406 	.globl _AX5043_TXRATE2NB
                                    407 	.globl _AX5043_TXRATE1NB
                                    408 	.globl _AX5043_TXRATE0NB
                                    409 	.globl _AX5043_TXPWRCOEFFE1NB
                                    410 	.globl _AX5043_TXPWRCOEFFE0NB
                                    411 	.globl _AX5043_TXPWRCOEFFD1NB
                                    412 	.globl _AX5043_TXPWRCOEFFD0NB
                                    413 	.globl _AX5043_TXPWRCOEFFC1NB
                                    414 	.globl _AX5043_TXPWRCOEFFC0NB
                                    415 	.globl _AX5043_TXPWRCOEFFB1NB
                                    416 	.globl _AX5043_TXPWRCOEFFB0NB
                                    417 	.globl _AX5043_TXPWRCOEFFA1NB
                                    418 	.globl _AX5043_TXPWRCOEFFA0NB
                                    419 	.globl _AX5043_TRKRFFREQ2NB
                                    420 	.globl _AX5043_TRKRFFREQ1NB
                                    421 	.globl _AX5043_TRKRFFREQ0NB
                                    422 	.globl _AX5043_TRKPHASE1NB
                                    423 	.globl _AX5043_TRKPHASE0NB
                                    424 	.globl _AX5043_TRKFSKDEMOD1NB
                                    425 	.globl _AX5043_TRKFSKDEMOD0NB
                                    426 	.globl _AX5043_TRKFREQ1NB
                                    427 	.globl _AX5043_TRKFREQ0NB
                                    428 	.globl _AX5043_TRKDATARATE2NB
                                    429 	.globl _AX5043_TRKDATARATE1NB
                                    430 	.globl _AX5043_TRKDATARATE0NB
                                    431 	.globl _AX5043_TRKAMPLITUDE1NB
                                    432 	.globl _AX5043_TRKAMPLITUDE0NB
                                    433 	.globl _AX5043_TRKAFSKDEMOD1NB
                                    434 	.globl _AX5043_TRKAFSKDEMOD0NB
                                    435 	.globl _AX5043_TMGTXSETTLENB
                                    436 	.globl _AX5043_TMGTXBOOSTNB
                                    437 	.globl _AX5043_TMGRXSETTLENB
                                    438 	.globl _AX5043_TMGRXRSSINB
                                    439 	.globl _AX5043_TMGRXPREAMBLE3NB
                                    440 	.globl _AX5043_TMGRXPREAMBLE2NB
                                    441 	.globl _AX5043_TMGRXPREAMBLE1NB
                                    442 	.globl _AX5043_TMGRXOFFSACQNB
                                    443 	.globl _AX5043_TMGRXCOARSEAGCNB
                                    444 	.globl _AX5043_TMGRXBOOSTNB
                                    445 	.globl _AX5043_TMGRXAGCNB
                                    446 	.globl _AX5043_TIMER2NB
                                    447 	.globl _AX5043_TIMER1NB
                                    448 	.globl _AX5043_TIMER0NB
                                    449 	.globl _AX5043_SILICONREVISIONNB
                                    450 	.globl _AX5043_SCRATCHNB
                                    451 	.globl _AX5043_RXDATARATE2NB
                                    452 	.globl _AX5043_RXDATARATE1NB
                                    453 	.globl _AX5043_RXDATARATE0NB
                                    454 	.globl _AX5043_RSSIREFERENCENB
                                    455 	.globl _AX5043_RSSIABSTHRNB
                                    456 	.globl _AX5043_RSSINB
                                    457 	.globl _AX5043_REFNB
                                    458 	.globl _AX5043_RADIOSTATENB
                                    459 	.globl _AX5043_RADIOEVENTREQ1NB
                                    460 	.globl _AX5043_RADIOEVENTREQ0NB
                                    461 	.globl _AX5043_RADIOEVENTMASK1NB
                                    462 	.globl _AX5043_RADIOEVENTMASK0NB
                                    463 	.globl _AX5043_PWRMODENB
                                    464 	.globl _AX5043_PWRAMPNB
                                    465 	.globl _AX5043_POWSTICKYSTATNB
                                    466 	.globl _AX5043_POWSTATNB
                                    467 	.globl _AX5043_POWIRQMASKNB
                                    468 	.globl _AX5043_POWCTRL1NB
                                    469 	.globl _AX5043_PLLVCOIRNB
                                    470 	.globl _AX5043_PLLVCOINB
                                    471 	.globl _AX5043_PLLVCODIVNB
                                    472 	.globl _AX5043_PLLRNGCLKNB
                                    473 	.globl _AX5043_PLLRANGINGBNB
                                    474 	.globl _AX5043_PLLRANGINGANB
                                    475 	.globl _AX5043_PLLLOOPBOOSTNB
                                    476 	.globl _AX5043_PLLLOOPNB
                                    477 	.globl _AX5043_PLLLOCKDETNB
                                    478 	.globl _AX5043_PLLCPIBOOSTNB
                                    479 	.globl _AX5043_PLLCPINB
                                    480 	.globl _AX5043_PKTSTOREFLAGSNB
                                    481 	.globl _AX5043_PKTMISCFLAGSNB
                                    482 	.globl _AX5043_PKTCHUNKSIZENB
                                    483 	.globl _AX5043_PKTACCEPTFLAGSNB
                                    484 	.globl _AX5043_PINSTATENB
                                    485 	.globl _AX5043_PINFUNCSYSCLKNB
                                    486 	.globl _AX5043_PINFUNCPWRAMPNB
                                    487 	.globl _AX5043_PINFUNCIRQNB
                                    488 	.globl _AX5043_PINFUNCDCLKNB
                                    489 	.globl _AX5043_PINFUNCDATANB
                                    490 	.globl _AX5043_PINFUNCANTSELNB
                                    491 	.globl _AX5043_MODULATIONNB
                                    492 	.globl _AX5043_MODCFGPNB
                                    493 	.globl _AX5043_MODCFGFNB
                                    494 	.globl _AX5043_MODCFGANB
                                    495 	.globl _AX5043_MAXRFOFFSET2NB
                                    496 	.globl _AX5043_MAXRFOFFSET1NB
                                    497 	.globl _AX5043_MAXRFOFFSET0NB
                                    498 	.globl _AX5043_MAXDROFFSET2NB
                                    499 	.globl _AX5043_MAXDROFFSET1NB
                                    500 	.globl _AX5043_MAXDROFFSET0NB
                                    501 	.globl _AX5043_MATCH1PAT1NB
                                    502 	.globl _AX5043_MATCH1PAT0NB
                                    503 	.globl _AX5043_MATCH1MINNB
                                    504 	.globl _AX5043_MATCH1MAXNB
                                    505 	.globl _AX5043_MATCH1LENNB
                                    506 	.globl _AX5043_MATCH0PAT3NB
                                    507 	.globl _AX5043_MATCH0PAT2NB
                                    508 	.globl _AX5043_MATCH0PAT1NB
                                    509 	.globl _AX5043_MATCH0PAT0NB
                                    510 	.globl _AX5043_MATCH0MINNB
                                    511 	.globl _AX5043_MATCH0MAXNB
                                    512 	.globl _AX5043_MATCH0LENNB
                                    513 	.globl _AX5043_LPOSCSTATUSNB
                                    514 	.globl _AX5043_LPOSCREF1NB
                                    515 	.globl _AX5043_LPOSCREF0NB
                                    516 	.globl _AX5043_LPOSCPER1NB
                                    517 	.globl _AX5043_LPOSCPER0NB
                                    518 	.globl _AX5043_LPOSCKFILT1NB
                                    519 	.globl _AX5043_LPOSCKFILT0NB
                                    520 	.globl _AX5043_LPOSCFREQ1NB
                                    521 	.globl _AX5043_LPOSCFREQ0NB
                                    522 	.globl _AX5043_LPOSCCONFIGNB
                                    523 	.globl _AX5043_IRQREQUEST1NB
                                    524 	.globl _AX5043_IRQREQUEST0NB
                                    525 	.globl _AX5043_IRQMASK1NB
                                    526 	.globl _AX5043_IRQMASK0NB
                                    527 	.globl _AX5043_IRQINVERSION1NB
                                    528 	.globl _AX5043_IRQINVERSION0NB
                                    529 	.globl _AX5043_IFFREQ1NB
                                    530 	.globl _AX5043_IFFREQ0NB
                                    531 	.globl _AX5043_GPADCPERIODNB
                                    532 	.globl _AX5043_GPADCCTRLNB
                                    533 	.globl _AX5043_GPADC13VALUE1NB
                                    534 	.globl _AX5043_GPADC13VALUE0NB
                                    535 	.globl _AX5043_FSKDMIN1NB
                                    536 	.globl _AX5043_FSKDMIN0NB
                                    537 	.globl _AX5043_FSKDMAX1NB
                                    538 	.globl _AX5043_FSKDMAX0NB
                                    539 	.globl _AX5043_FSKDEV2NB
                                    540 	.globl _AX5043_FSKDEV1NB
                                    541 	.globl _AX5043_FSKDEV0NB
                                    542 	.globl _AX5043_FREQB3NB
                                    543 	.globl _AX5043_FREQB2NB
                                    544 	.globl _AX5043_FREQB1NB
                                    545 	.globl _AX5043_FREQB0NB
                                    546 	.globl _AX5043_FREQA3NB
                                    547 	.globl _AX5043_FREQA2NB
                                    548 	.globl _AX5043_FREQA1NB
                                    549 	.globl _AX5043_FREQA0NB
                                    550 	.globl _AX5043_FRAMINGNB
                                    551 	.globl _AX5043_FIFOTHRESH1NB
                                    552 	.globl _AX5043_FIFOTHRESH0NB
                                    553 	.globl _AX5043_FIFOSTATNB
                                    554 	.globl _AX5043_FIFOFREE1NB
                                    555 	.globl _AX5043_FIFOFREE0NB
                                    556 	.globl _AX5043_FIFODATANB
                                    557 	.globl _AX5043_FIFOCOUNT1NB
                                    558 	.globl _AX5043_FIFOCOUNT0NB
                                    559 	.globl _AX5043_FECSYNCNB
                                    560 	.globl _AX5043_FECSTATUSNB
                                    561 	.globl _AX5043_FECNB
                                    562 	.globl _AX5043_ENCODINGNB
                                    563 	.globl _AX5043_DIVERSITYNB
                                    564 	.globl _AX5043_DECIMATIONNB
                                    565 	.globl _AX5043_DACVALUE1NB
                                    566 	.globl _AX5043_DACVALUE0NB
                                    567 	.globl _AX5043_DACCONFIGNB
                                    568 	.globl _AX5043_CRCINIT3NB
                                    569 	.globl _AX5043_CRCINIT2NB
                                    570 	.globl _AX5043_CRCINIT1NB
                                    571 	.globl _AX5043_CRCINIT0NB
                                    572 	.globl _AX5043_BGNDRSSITHRNB
                                    573 	.globl _AX5043_BGNDRSSIGAINNB
                                    574 	.globl _AX5043_BGNDRSSINB
                                    575 	.globl _AX5043_BBTUNENB
                                    576 	.globl _AX5043_BBOFFSCAPNB
                                    577 	.globl _AX5043_AMPLFILTERNB
                                    578 	.globl _AX5043_AGCCOUNTERNB
                                    579 	.globl _AX5043_AFSKSPACE1NB
                                    580 	.globl _AX5043_AFSKSPACE0NB
                                    581 	.globl _AX5043_AFSKMARK1NB
                                    582 	.globl _AX5043_AFSKMARK0NB
                                    583 	.globl _AX5043_AFSKCTRLNB
                                    584 	.globl _AX5043_TIMEGAIN3
                                    585 	.globl _AX5043_TIMEGAIN2
                                    586 	.globl _AX5043_TIMEGAIN1
                                    587 	.globl _AX5043_TIMEGAIN0
                                    588 	.globl _AX5043_RXPARAMSETS
                                    589 	.globl _AX5043_RXPARAMCURSET
                                    590 	.globl _AX5043_PKTMAXLEN
                                    591 	.globl _AX5043_PKTLENOFFSET
                                    592 	.globl _AX5043_PKTLENCFG
                                    593 	.globl _AX5043_PKTADDRMASK3
                                    594 	.globl _AX5043_PKTADDRMASK2
                                    595 	.globl _AX5043_PKTADDRMASK1
                                    596 	.globl _AX5043_PKTADDRMASK0
                                    597 	.globl _AX5043_PKTADDRCFG
                                    598 	.globl _AX5043_PKTADDR3
                                    599 	.globl _AX5043_PKTADDR2
                                    600 	.globl _AX5043_PKTADDR1
                                    601 	.globl _AX5043_PKTADDR0
                                    602 	.globl _AX5043_PHASEGAIN3
                                    603 	.globl _AX5043_PHASEGAIN2
                                    604 	.globl _AX5043_PHASEGAIN1
                                    605 	.globl _AX5043_PHASEGAIN0
                                    606 	.globl _AX5043_FREQUENCYLEAK
                                    607 	.globl _AX5043_FREQUENCYGAIND3
                                    608 	.globl _AX5043_FREQUENCYGAIND2
                                    609 	.globl _AX5043_FREQUENCYGAIND1
                                    610 	.globl _AX5043_FREQUENCYGAIND0
                                    611 	.globl _AX5043_FREQUENCYGAINC3
                                    612 	.globl _AX5043_FREQUENCYGAINC2
                                    613 	.globl _AX5043_FREQUENCYGAINC1
                                    614 	.globl _AX5043_FREQUENCYGAINC0
                                    615 	.globl _AX5043_FREQUENCYGAINB3
                                    616 	.globl _AX5043_FREQUENCYGAINB2
                                    617 	.globl _AX5043_FREQUENCYGAINB1
                                    618 	.globl _AX5043_FREQUENCYGAINB0
                                    619 	.globl _AX5043_FREQUENCYGAINA3
                                    620 	.globl _AX5043_FREQUENCYGAINA2
                                    621 	.globl _AX5043_FREQUENCYGAINA1
                                    622 	.globl _AX5043_FREQUENCYGAINA0
                                    623 	.globl _AX5043_FREQDEV13
                                    624 	.globl _AX5043_FREQDEV12
                                    625 	.globl _AX5043_FREQDEV11
                                    626 	.globl _AX5043_FREQDEV10
                                    627 	.globl _AX5043_FREQDEV03
                                    628 	.globl _AX5043_FREQDEV02
                                    629 	.globl _AX5043_FREQDEV01
                                    630 	.globl _AX5043_FREQDEV00
                                    631 	.globl _AX5043_FOURFSK3
                                    632 	.globl _AX5043_FOURFSK2
                                    633 	.globl _AX5043_FOURFSK1
                                    634 	.globl _AX5043_FOURFSK0
                                    635 	.globl _AX5043_DRGAIN3
                                    636 	.globl _AX5043_DRGAIN2
                                    637 	.globl _AX5043_DRGAIN1
                                    638 	.globl _AX5043_DRGAIN0
                                    639 	.globl _AX5043_BBOFFSRES3
                                    640 	.globl _AX5043_BBOFFSRES2
                                    641 	.globl _AX5043_BBOFFSRES1
                                    642 	.globl _AX5043_BBOFFSRES0
                                    643 	.globl _AX5043_AMPLITUDEGAIN3
                                    644 	.globl _AX5043_AMPLITUDEGAIN2
                                    645 	.globl _AX5043_AMPLITUDEGAIN1
                                    646 	.globl _AX5043_AMPLITUDEGAIN0
                                    647 	.globl _AX5043_AGCTARGET3
                                    648 	.globl _AX5043_AGCTARGET2
                                    649 	.globl _AX5043_AGCTARGET1
                                    650 	.globl _AX5043_AGCTARGET0
                                    651 	.globl _AX5043_AGCMINMAX3
                                    652 	.globl _AX5043_AGCMINMAX2
                                    653 	.globl _AX5043_AGCMINMAX1
                                    654 	.globl _AX5043_AGCMINMAX0
                                    655 	.globl _AX5043_AGCGAIN3
                                    656 	.globl _AX5043_AGCGAIN2
                                    657 	.globl _AX5043_AGCGAIN1
                                    658 	.globl _AX5043_AGCGAIN0
                                    659 	.globl _AX5043_AGCAHYST3
                                    660 	.globl _AX5043_AGCAHYST2
                                    661 	.globl _AX5043_AGCAHYST1
                                    662 	.globl _AX5043_AGCAHYST0
                                    663 	.globl _AX5043_0xF44
                                    664 	.globl _AX5043_0xF35
                                    665 	.globl _AX5043_0xF34
                                    666 	.globl _AX5043_0xF33
                                    667 	.globl _AX5043_0xF32
                                    668 	.globl _AX5043_0xF31
                                    669 	.globl _AX5043_0xF30
                                    670 	.globl _AX5043_0xF26
                                    671 	.globl _AX5043_0xF23
                                    672 	.globl _AX5043_0xF22
                                    673 	.globl _AX5043_0xF21
                                    674 	.globl _AX5043_0xF1C
                                    675 	.globl _AX5043_0xF18
                                    676 	.globl _AX5043_0xF0C
                                    677 	.globl _AX5043_0xF00
                                    678 	.globl _AX5043_XTALSTATUS
                                    679 	.globl _AX5043_XTALOSC
                                    680 	.globl _AX5043_XTALCAP
                                    681 	.globl _AX5043_XTALAMPL
                                    682 	.globl _AX5043_WAKEUPXOEARLY
                                    683 	.globl _AX5043_WAKEUPTIMER1
                                    684 	.globl _AX5043_WAKEUPTIMER0
                                    685 	.globl _AX5043_WAKEUPFREQ1
                                    686 	.globl _AX5043_WAKEUPFREQ0
                                    687 	.globl _AX5043_WAKEUP1
                                    688 	.globl _AX5043_WAKEUP0
                                    689 	.globl _AX5043_TXRATE2
                                    690 	.globl _AX5043_TXRATE1
                                    691 	.globl _AX5043_TXRATE0
                                    692 	.globl _AX5043_TXPWRCOEFFE1
                                    693 	.globl _AX5043_TXPWRCOEFFE0
                                    694 	.globl _AX5043_TXPWRCOEFFD1
                                    695 	.globl _AX5043_TXPWRCOEFFD0
                                    696 	.globl _AX5043_TXPWRCOEFFC1
                                    697 	.globl _AX5043_TXPWRCOEFFC0
                                    698 	.globl _AX5043_TXPWRCOEFFB1
                                    699 	.globl _AX5043_TXPWRCOEFFB0
                                    700 	.globl _AX5043_TXPWRCOEFFA1
                                    701 	.globl _AX5043_TXPWRCOEFFA0
                                    702 	.globl _AX5043_TRKRFFREQ2
                                    703 	.globl _AX5043_TRKRFFREQ1
                                    704 	.globl _AX5043_TRKRFFREQ0
                                    705 	.globl _AX5043_TRKPHASE1
                                    706 	.globl _AX5043_TRKPHASE0
                                    707 	.globl _AX5043_TRKFSKDEMOD1
                                    708 	.globl _AX5043_TRKFSKDEMOD0
                                    709 	.globl _AX5043_TRKFREQ1
                                    710 	.globl _AX5043_TRKFREQ0
                                    711 	.globl _AX5043_TRKDATARATE2
                                    712 	.globl _AX5043_TRKDATARATE1
                                    713 	.globl _AX5043_TRKDATARATE0
                                    714 	.globl _AX5043_TRKAMPLITUDE1
                                    715 	.globl _AX5043_TRKAMPLITUDE0
                                    716 	.globl _AX5043_TRKAFSKDEMOD1
                                    717 	.globl _AX5043_TRKAFSKDEMOD0
                                    718 	.globl _AX5043_TMGTXSETTLE
                                    719 	.globl _AX5043_TMGTXBOOST
                                    720 	.globl _AX5043_TMGRXSETTLE
                                    721 	.globl _AX5043_TMGRXRSSI
                                    722 	.globl _AX5043_TMGRXPREAMBLE3
                                    723 	.globl _AX5043_TMGRXPREAMBLE2
                                    724 	.globl _AX5043_TMGRXPREAMBLE1
                                    725 	.globl _AX5043_TMGRXOFFSACQ
                                    726 	.globl _AX5043_TMGRXCOARSEAGC
                                    727 	.globl _AX5043_TMGRXBOOST
                                    728 	.globl _AX5043_TMGRXAGC
                                    729 	.globl _AX5043_TIMER2
                                    730 	.globl _AX5043_TIMER1
                                    731 	.globl _AX5043_TIMER0
                                    732 	.globl _AX5043_SILICONREVISION
                                    733 	.globl _AX5043_SCRATCH
                                    734 	.globl _AX5043_RXDATARATE2
                                    735 	.globl _AX5043_RXDATARATE1
                                    736 	.globl _AX5043_RXDATARATE0
                                    737 	.globl _AX5043_RSSIREFERENCE
                                    738 	.globl _AX5043_RSSIABSTHR
                                    739 	.globl _AX5043_RSSI
                                    740 	.globl _AX5043_REF
                                    741 	.globl _AX5043_RADIOSTATE
                                    742 	.globl _AX5043_RADIOEVENTREQ1
                                    743 	.globl _AX5043_RADIOEVENTREQ0
                                    744 	.globl _AX5043_RADIOEVENTMASK1
                                    745 	.globl _AX5043_RADIOEVENTMASK0
                                    746 	.globl _AX5043_PWRMODE
                                    747 	.globl _AX5043_PWRAMP
                                    748 	.globl _AX5043_POWSTICKYSTAT
                                    749 	.globl _AX5043_POWSTAT
                                    750 	.globl _AX5043_POWIRQMASK
                                    751 	.globl _AX5043_POWCTRL1
                                    752 	.globl _AX5043_PLLVCOIR
                                    753 	.globl _AX5043_PLLVCOI
                                    754 	.globl _AX5043_PLLVCODIV
                                    755 	.globl _AX5043_PLLRNGCLK
                                    756 	.globl _AX5043_PLLRANGINGB
                                    757 	.globl _AX5043_PLLRANGINGA
                                    758 	.globl _AX5043_PLLLOOPBOOST
                                    759 	.globl _AX5043_PLLLOOP
                                    760 	.globl _AX5043_PLLLOCKDET
                                    761 	.globl _AX5043_PLLCPIBOOST
                                    762 	.globl _AX5043_PLLCPI
                                    763 	.globl _AX5043_PKTSTOREFLAGS
                                    764 	.globl _AX5043_PKTMISCFLAGS
                                    765 	.globl _AX5043_PKTCHUNKSIZE
                                    766 	.globl _AX5043_PKTACCEPTFLAGS
                                    767 	.globl _AX5043_PINSTATE
                                    768 	.globl _AX5043_PINFUNCSYSCLK
                                    769 	.globl _AX5043_PINFUNCPWRAMP
                                    770 	.globl _AX5043_PINFUNCIRQ
                                    771 	.globl _AX5043_PINFUNCDCLK
                                    772 	.globl _AX5043_PINFUNCDATA
                                    773 	.globl _AX5043_PINFUNCANTSEL
                                    774 	.globl _AX5043_MODULATION
                                    775 	.globl _AX5043_MODCFGP
                                    776 	.globl _AX5043_MODCFGF
                                    777 	.globl _AX5043_MODCFGA
                                    778 	.globl _AX5043_MAXRFOFFSET2
                                    779 	.globl _AX5043_MAXRFOFFSET1
                                    780 	.globl _AX5043_MAXRFOFFSET0
                                    781 	.globl _AX5043_MAXDROFFSET2
                                    782 	.globl _AX5043_MAXDROFFSET1
                                    783 	.globl _AX5043_MAXDROFFSET0
                                    784 	.globl _AX5043_MATCH1PAT1
                                    785 	.globl _AX5043_MATCH1PAT0
                                    786 	.globl _AX5043_MATCH1MIN
                                    787 	.globl _AX5043_MATCH1MAX
                                    788 	.globl _AX5043_MATCH1LEN
                                    789 	.globl _AX5043_MATCH0PAT3
                                    790 	.globl _AX5043_MATCH0PAT2
                                    791 	.globl _AX5043_MATCH0PAT1
                                    792 	.globl _AX5043_MATCH0PAT0
                                    793 	.globl _AX5043_MATCH0MIN
                                    794 	.globl _AX5043_MATCH0MAX
                                    795 	.globl _AX5043_MATCH0LEN
                                    796 	.globl _AX5043_LPOSCSTATUS
                                    797 	.globl _AX5043_LPOSCREF1
                                    798 	.globl _AX5043_LPOSCREF0
                                    799 	.globl _AX5043_LPOSCPER1
                                    800 	.globl _AX5043_LPOSCPER0
                                    801 	.globl _AX5043_LPOSCKFILT1
                                    802 	.globl _AX5043_LPOSCKFILT0
                                    803 	.globl _AX5043_LPOSCFREQ1
                                    804 	.globl _AX5043_LPOSCFREQ0
                                    805 	.globl _AX5043_LPOSCCONFIG
                                    806 	.globl _AX5043_IRQREQUEST1
                                    807 	.globl _AX5043_IRQREQUEST0
                                    808 	.globl _AX5043_IRQMASK1
                                    809 	.globl _AX5043_IRQMASK0
                                    810 	.globl _AX5043_IRQINVERSION1
                                    811 	.globl _AX5043_IRQINVERSION0
                                    812 	.globl _AX5043_IFFREQ1
                                    813 	.globl _AX5043_IFFREQ0
                                    814 	.globl _AX5043_GPADCPERIOD
                                    815 	.globl _AX5043_GPADCCTRL
                                    816 	.globl _AX5043_GPADC13VALUE1
                                    817 	.globl _AX5043_GPADC13VALUE0
                                    818 	.globl _AX5043_FSKDMIN1
                                    819 	.globl _AX5043_FSKDMIN0
                                    820 	.globl _AX5043_FSKDMAX1
                                    821 	.globl _AX5043_FSKDMAX0
                                    822 	.globl _AX5043_FSKDEV2
                                    823 	.globl _AX5043_FSKDEV1
                                    824 	.globl _AX5043_FSKDEV0
                                    825 	.globl _AX5043_FREQB3
                                    826 	.globl _AX5043_FREQB2
                                    827 	.globl _AX5043_FREQB1
                                    828 	.globl _AX5043_FREQB0
                                    829 	.globl _AX5043_FREQA3
                                    830 	.globl _AX5043_FREQA2
                                    831 	.globl _AX5043_FREQA1
                                    832 	.globl _AX5043_FREQA0
                                    833 	.globl _AX5043_FRAMING
                                    834 	.globl _AX5043_FIFOTHRESH1
                                    835 	.globl _AX5043_FIFOTHRESH0
                                    836 	.globl _AX5043_FIFOSTAT
                                    837 	.globl _AX5043_FIFOFREE1
                                    838 	.globl _AX5043_FIFOFREE0
                                    839 	.globl _AX5043_FIFODATA
                                    840 	.globl _AX5043_FIFOCOUNT1
                                    841 	.globl _AX5043_FIFOCOUNT0
                                    842 	.globl _AX5043_FECSYNC
                                    843 	.globl _AX5043_FECSTATUS
                                    844 	.globl _AX5043_FEC
                                    845 	.globl _AX5043_ENCODING
                                    846 	.globl _AX5043_DIVERSITY
                                    847 	.globl _AX5043_DECIMATION
                                    848 	.globl _AX5043_DACVALUE1
                                    849 	.globl _AX5043_DACVALUE0
                                    850 	.globl _AX5043_DACCONFIG
                                    851 	.globl _AX5043_CRCINIT3
                                    852 	.globl _AX5043_CRCINIT2
                                    853 	.globl _AX5043_CRCINIT1
                                    854 	.globl _AX5043_CRCINIT0
                                    855 	.globl _AX5043_BGNDRSSITHR
                                    856 	.globl _AX5043_BGNDRSSIGAIN
                                    857 	.globl _AX5043_BGNDRSSI
                                    858 	.globl _AX5043_BBTUNE
                                    859 	.globl _AX5043_BBOFFSCAP
                                    860 	.globl _AX5043_AMPLFILTER
                                    861 	.globl _AX5043_AGCCOUNTER
                                    862 	.globl _AX5043_AFSKSPACE1
                                    863 	.globl _AX5043_AFSKSPACE0
                                    864 	.globl _AX5043_AFSKMARK1
                                    865 	.globl _AX5043_AFSKMARK0
                                    866 	.globl _AX5043_AFSKCTRL
                                    867 	.globl _XWTSTAT
                                    868 	.globl _XWTIRQEN
                                    869 	.globl _XWTEVTD
                                    870 	.globl _XWTEVTD1
                                    871 	.globl _XWTEVTD0
                                    872 	.globl _XWTEVTC
                                    873 	.globl _XWTEVTC1
                                    874 	.globl _XWTEVTC0
                                    875 	.globl _XWTEVTB
                                    876 	.globl _XWTEVTB1
                                    877 	.globl _XWTEVTB0
                                    878 	.globl _XWTEVTA
                                    879 	.globl _XWTEVTA1
                                    880 	.globl _XWTEVTA0
                                    881 	.globl _XWTCNTR1
                                    882 	.globl _XWTCNTB
                                    883 	.globl _XWTCNTB1
                                    884 	.globl _XWTCNTB0
                                    885 	.globl _XWTCNTA
                                    886 	.globl _XWTCNTA1
                                    887 	.globl _XWTCNTA0
                                    888 	.globl _XWTCFGB
                                    889 	.globl _XWTCFGA
                                    890 	.globl _XWDTRESET
                                    891 	.globl _XWDTCFG
                                    892 	.globl _XU1STATUS
                                    893 	.globl _XU1SHREG
                                    894 	.globl _XU1MODE
                                    895 	.globl _XU1CTRL
                                    896 	.globl _XU0STATUS
                                    897 	.globl _XU0SHREG
                                    898 	.globl _XU0MODE
                                    899 	.globl _XU0CTRL
                                    900 	.globl _XT2STATUS
                                    901 	.globl _XT2PERIOD
                                    902 	.globl _XT2PERIOD1
                                    903 	.globl _XT2PERIOD0
                                    904 	.globl _XT2MODE
                                    905 	.globl _XT2CNT
                                    906 	.globl _XT2CNT1
                                    907 	.globl _XT2CNT0
                                    908 	.globl _XT2CLKSRC
                                    909 	.globl _XT1STATUS
                                    910 	.globl _XT1PERIOD
                                    911 	.globl _XT1PERIOD1
                                    912 	.globl _XT1PERIOD0
                                    913 	.globl _XT1MODE
                                    914 	.globl _XT1CNT
                                    915 	.globl _XT1CNT1
                                    916 	.globl _XT1CNT0
                                    917 	.globl _XT1CLKSRC
                                    918 	.globl _XT0STATUS
                                    919 	.globl _XT0PERIOD
                                    920 	.globl _XT0PERIOD1
                                    921 	.globl _XT0PERIOD0
                                    922 	.globl _XT0MODE
                                    923 	.globl _XT0CNT
                                    924 	.globl _XT0CNT1
                                    925 	.globl _XT0CNT0
                                    926 	.globl _XT0CLKSRC
                                    927 	.globl _XSPSTATUS
                                    928 	.globl _XSPSHREG
                                    929 	.globl _XSPMODE
                                    930 	.globl _XSPCLKSRC
                                    931 	.globl _XRADIOSTAT
                                    932 	.globl _XRADIOSTAT1
                                    933 	.globl _XRADIOSTAT0
                                    934 	.globl _XRADIODATA3
                                    935 	.globl _XRADIODATA2
                                    936 	.globl _XRADIODATA1
                                    937 	.globl _XRADIODATA0
                                    938 	.globl _XRADIOADDR1
                                    939 	.globl _XRADIOADDR0
                                    940 	.globl _XRADIOACC
                                    941 	.globl _XOC1STATUS
                                    942 	.globl _XOC1PIN
                                    943 	.globl _XOC1MODE
                                    944 	.globl _XOC1COMP
                                    945 	.globl _XOC1COMP1
                                    946 	.globl _XOC1COMP0
                                    947 	.globl _XOC0STATUS
                                    948 	.globl _XOC0PIN
                                    949 	.globl _XOC0MODE
                                    950 	.globl _XOC0COMP
                                    951 	.globl _XOC0COMP1
                                    952 	.globl _XOC0COMP0
                                    953 	.globl _XNVSTATUS
                                    954 	.globl _XNVKEY
                                    955 	.globl _XNVDATA
                                    956 	.globl _XNVDATA1
                                    957 	.globl _XNVDATA0
                                    958 	.globl _XNVADDR
                                    959 	.globl _XNVADDR1
                                    960 	.globl _XNVADDR0
                                    961 	.globl _XIC1STATUS
                                    962 	.globl _XIC1MODE
                                    963 	.globl _XIC1CAPT
                                    964 	.globl _XIC1CAPT1
                                    965 	.globl _XIC1CAPT0
                                    966 	.globl _XIC0STATUS
                                    967 	.globl _XIC0MODE
                                    968 	.globl _XIC0CAPT
                                    969 	.globl _XIC0CAPT1
                                    970 	.globl _XIC0CAPT0
                                    971 	.globl _XPORTR
                                    972 	.globl _XPORTC
                                    973 	.globl _XPORTB
                                    974 	.globl _XPORTA
                                    975 	.globl _XPINR
                                    976 	.globl _XPINC
                                    977 	.globl _XPINB
                                    978 	.globl _XPINA
                                    979 	.globl _XDIRR
                                    980 	.globl _XDIRC
                                    981 	.globl _XDIRB
                                    982 	.globl _XDIRA
                                    983 	.globl _XDBGLNKSTAT
                                    984 	.globl _XDBGLNKBUF
                                    985 	.globl _XCODECONFIG
                                    986 	.globl _XCLKSTAT
                                    987 	.globl _XCLKCON
                                    988 	.globl _XANALOGCOMP
                                    989 	.globl _XADCCONV
                                    990 	.globl _XADCCLKSRC
                                    991 	.globl _XADCCH3CONFIG
                                    992 	.globl _XADCCH2CONFIG
                                    993 	.globl _XADCCH1CONFIG
                                    994 	.globl _XADCCH0CONFIG
                                    995 	.globl _XPCON
                                    996 	.globl _XIP
                                    997 	.globl _XIE
                                    998 	.globl _XDPTR1
                                    999 	.globl _XDPTR0
                                   1000 	.globl _RNGCLKSRC1
                                   1001 	.globl _RNGCLKSRC0
                                   1002 	.globl _RNGMODE
                                   1003 	.globl _AESOUTADDR
                                   1004 	.globl _AESOUTADDR1
                                   1005 	.globl _AESOUTADDR0
                                   1006 	.globl _AESMODE
                                   1007 	.globl _AESKEYADDR
                                   1008 	.globl _AESKEYADDR1
                                   1009 	.globl _AESKEYADDR0
                                   1010 	.globl _AESINADDR
                                   1011 	.globl _AESINADDR1
                                   1012 	.globl _AESINADDR0
                                   1013 	.globl _AESCURBLOCK
                                   1014 	.globl _AESCONFIG
                                   1015 	.globl _RNGBYTE
                                   1016 	.globl _XTALREADY
                                   1017 	.globl _XTALOSC
                                   1018 	.globl _XTALAMPL
                                   1019 	.globl _SILICONREV
                                   1020 	.globl _SCRATCH3
                                   1021 	.globl _SCRATCH2
                                   1022 	.globl _SCRATCH1
                                   1023 	.globl _SCRATCH0
                                   1024 	.globl _RADIOMUX
                                   1025 	.globl _RADIOFSTATADDR
                                   1026 	.globl _RADIOFSTATADDR1
                                   1027 	.globl _RADIOFSTATADDR0
                                   1028 	.globl _RADIOFDATAADDR
                                   1029 	.globl _RADIOFDATAADDR1
                                   1030 	.globl _RADIOFDATAADDR0
                                   1031 	.globl _OSCRUN
                                   1032 	.globl _OSCREADY
                                   1033 	.globl _OSCFORCERUN
                                   1034 	.globl _OSCCALIB
                                   1035 	.globl _MISCCTRL
                                   1036 	.globl _LPXOSCGM
                                   1037 	.globl _LPOSCREF
                                   1038 	.globl _LPOSCREF1
                                   1039 	.globl _LPOSCREF0
                                   1040 	.globl _LPOSCPER
                                   1041 	.globl _LPOSCPER1
                                   1042 	.globl _LPOSCPER0
                                   1043 	.globl _LPOSCKFILT
                                   1044 	.globl _LPOSCKFILT1
                                   1045 	.globl _LPOSCKFILT0
                                   1046 	.globl _LPOSCFREQ
                                   1047 	.globl _LPOSCFREQ1
                                   1048 	.globl _LPOSCFREQ0
                                   1049 	.globl _LPOSCCONFIG
                                   1050 	.globl _PINSEL
                                   1051 	.globl _PINCHGC
                                   1052 	.globl _PINCHGB
                                   1053 	.globl _PINCHGA
                                   1054 	.globl _PALTRADIO
                                   1055 	.globl _PALTC
                                   1056 	.globl _PALTB
                                   1057 	.globl _PALTA
                                   1058 	.globl _INTCHGC
                                   1059 	.globl _INTCHGB
                                   1060 	.globl _INTCHGA
                                   1061 	.globl _EXTIRQ
                                   1062 	.globl _GPIOENABLE
                                   1063 	.globl _ANALOGA
                                   1064 	.globl _FRCOSCREF
                                   1065 	.globl _FRCOSCREF1
                                   1066 	.globl _FRCOSCREF0
                                   1067 	.globl _FRCOSCPER
                                   1068 	.globl _FRCOSCPER1
                                   1069 	.globl _FRCOSCPER0
                                   1070 	.globl _FRCOSCKFILT
                                   1071 	.globl _FRCOSCKFILT1
                                   1072 	.globl _FRCOSCKFILT0
                                   1073 	.globl _FRCOSCFREQ
                                   1074 	.globl _FRCOSCFREQ1
                                   1075 	.globl _FRCOSCFREQ0
                                   1076 	.globl _FRCOSCCTRL
                                   1077 	.globl _FRCOSCCONFIG
                                   1078 	.globl _DMA1CONFIG
                                   1079 	.globl _DMA1ADDR
                                   1080 	.globl _DMA1ADDR1
                                   1081 	.globl _DMA1ADDR0
                                   1082 	.globl _DMA0CONFIG
                                   1083 	.globl _DMA0ADDR
                                   1084 	.globl _DMA0ADDR1
                                   1085 	.globl _DMA0ADDR0
                                   1086 	.globl _ADCTUNE2
                                   1087 	.globl _ADCTUNE1
                                   1088 	.globl _ADCTUNE0
                                   1089 	.globl _ADCCH3VAL
                                   1090 	.globl _ADCCH3VAL1
                                   1091 	.globl _ADCCH3VAL0
                                   1092 	.globl _ADCCH2VAL
                                   1093 	.globl _ADCCH2VAL1
                                   1094 	.globl _ADCCH2VAL0
                                   1095 	.globl _ADCCH1VAL
                                   1096 	.globl _ADCCH1VAL1
                                   1097 	.globl _ADCCH1VAL0
                                   1098 	.globl _ADCCH0VAL
                                   1099 	.globl _ADCCH0VAL1
                                   1100 	.globl _ADCCH0VAL0
                                   1101 	.globl _UBLOX_GPS_PortInit
                                   1102 	.globl _UBLOX_GPS_FletcherChecksum8
                                   1103 	.globl _UBLOX_GPS_SendCommand_WaitACK
                                   1104 ;--------------------------------------------------------
                                   1105 ; special function registers
                                   1106 ;--------------------------------------------------------
                                   1107 	.area RSEG    (ABS,DATA)
      000000                       1108 	.org 0x0000
                           0000E0  1109 _ACC	=	0x00e0
                           0000F0  1110 _B	=	0x00f0
                           000083  1111 _DPH	=	0x0083
                           000085  1112 _DPH1	=	0x0085
                           000082  1113 _DPL	=	0x0082
                           000084  1114 _DPL1	=	0x0084
                           008382  1115 _DPTR0	=	0x8382
                           008584  1116 _DPTR1	=	0x8584
                           000086  1117 _DPS	=	0x0086
                           0000A0  1118 _E2IE	=	0x00a0
                           0000C0  1119 _E2IP	=	0x00c0
                           000098  1120 _EIE	=	0x0098
                           0000B0  1121 _EIP	=	0x00b0
                           0000A8  1122 _IE	=	0x00a8
                           0000B8  1123 _IP	=	0x00b8
                           000087  1124 _PCON	=	0x0087
                           0000D0  1125 _PSW	=	0x00d0
                           000081  1126 _SP	=	0x0081
                           0000D9  1127 _XPAGE	=	0x00d9
                           0000D9  1128 __XPAGE	=	0x00d9
                           0000CA  1129 _ADCCH0CONFIG	=	0x00ca
                           0000CB  1130 _ADCCH1CONFIG	=	0x00cb
                           0000D2  1131 _ADCCH2CONFIG	=	0x00d2
                           0000D3  1132 _ADCCH3CONFIG	=	0x00d3
                           0000D1  1133 _ADCCLKSRC	=	0x00d1
                           0000C9  1134 _ADCCONV	=	0x00c9
                           0000E1  1135 _ANALOGCOMP	=	0x00e1
                           0000C6  1136 _CLKCON	=	0x00c6
                           0000C7  1137 _CLKSTAT	=	0x00c7
                           000097  1138 _CODECONFIG	=	0x0097
                           0000E3  1139 _DBGLNKBUF	=	0x00e3
                           0000E2  1140 _DBGLNKSTAT	=	0x00e2
                           000089  1141 _DIRA	=	0x0089
                           00008A  1142 _DIRB	=	0x008a
                           00008B  1143 _DIRC	=	0x008b
                           00008E  1144 _DIRR	=	0x008e
                           0000C8  1145 _PINA	=	0x00c8
                           0000E8  1146 _PINB	=	0x00e8
                           0000F8  1147 _PINC	=	0x00f8
                           00008D  1148 _PINR	=	0x008d
                           000080  1149 _PORTA	=	0x0080
                           000088  1150 _PORTB	=	0x0088
                           000090  1151 _PORTC	=	0x0090
                           00008C  1152 _PORTR	=	0x008c
                           0000CE  1153 _IC0CAPT0	=	0x00ce
                           0000CF  1154 _IC0CAPT1	=	0x00cf
                           00CFCE  1155 _IC0CAPT	=	0xcfce
                           0000CC  1156 _IC0MODE	=	0x00cc
                           0000CD  1157 _IC0STATUS	=	0x00cd
                           0000D6  1158 _IC1CAPT0	=	0x00d6
                           0000D7  1159 _IC1CAPT1	=	0x00d7
                           00D7D6  1160 _IC1CAPT	=	0xd7d6
                           0000D4  1161 _IC1MODE	=	0x00d4
                           0000D5  1162 _IC1STATUS	=	0x00d5
                           000092  1163 _NVADDR0	=	0x0092
                           000093  1164 _NVADDR1	=	0x0093
                           009392  1165 _NVADDR	=	0x9392
                           000094  1166 _NVDATA0	=	0x0094
                           000095  1167 _NVDATA1	=	0x0095
                           009594  1168 _NVDATA	=	0x9594
                           000096  1169 _NVKEY	=	0x0096
                           000091  1170 _NVSTATUS	=	0x0091
                           0000BC  1171 _OC0COMP0	=	0x00bc
                           0000BD  1172 _OC0COMP1	=	0x00bd
                           00BDBC  1173 _OC0COMP	=	0xbdbc
                           0000B9  1174 _OC0MODE	=	0x00b9
                           0000BA  1175 _OC0PIN	=	0x00ba
                           0000BB  1176 _OC0STATUS	=	0x00bb
                           0000C4  1177 _OC1COMP0	=	0x00c4
                           0000C5  1178 _OC1COMP1	=	0x00c5
                           00C5C4  1179 _OC1COMP	=	0xc5c4
                           0000C1  1180 _OC1MODE	=	0x00c1
                           0000C2  1181 _OC1PIN	=	0x00c2
                           0000C3  1182 _OC1STATUS	=	0x00c3
                           0000B1  1183 _RADIOACC	=	0x00b1
                           0000B3  1184 _RADIOADDR0	=	0x00b3
                           0000B2  1185 _RADIOADDR1	=	0x00b2
                           00B2B3  1186 _RADIOADDR	=	0xb2b3
                           0000B7  1187 _RADIODATA0	=	0x00b7
                           0000B6  1188 _RADIODATA1	=	0x00b6
                           0000B5  1189 _RADIODATA2	=	0x00b5
                           0000B4  1190 _RADIODATA3	=	0x00b4
                           B4B5B6B7  1191 _RADIODATA	=	0xb4b5b6b7
                           0000BE  1192 _RADIOSTAT0	=	0x00be
                           0000BF  1193 _RADIOSTAT1	=	0x00bf
                           00BFBE  1194 _RADIOSTAT	=	0xbfbe
                           0000DF  1195 _SPCLKSRC	=	0x00df
                           0000DC  1196 _SPMODE	=	0x00dc
                           0000DE  1197 _SPSHREG	=	0x00de
                           0000DD  1198 _SPSTATUS	=	0x00dd
                           00009A  1199 _T0CLKSRC	=	0x009a
                           00009C  1200 _T0CNT0	=	0x009c
                           00009D  1201 _T0CNT1	=	0x009d
                           009D9C  1202 _T0CNT	=	0x9d9c
                           000099  1203 _T0MODE	=	0x0099
                           00009E  1204 _T0PERIOD0	=	0x009e
                           00009F  1205 _T0PERIOD1	=	0x009f
                           009F9E  1206 _T0PERIOD	=	0x9f9e
                           00009B  1207 _T0STATUS	=	0x009b
                           0000A2  1208 _T1CLKSRC	=	0x00a2
                           0000A4  1209 _T1CNT0	=	0x00a4
                           0000A5  1210 _T1CNT1	=	0x00a5
                           00A5A4  1211 _T1CNT	=	0xa5a4
                           0000A1  1212 _T1MODE	=	0x00a1
                           0000A6  1213 _T1PERIOD0	=	0x00a6
                           0000A7  1214 _T1PERIOD1	=	0x00a7
                           00A7A6  1215 _T1PERIOD	=	0xa7a6
                           0000A3  1216 _T1STATUS	=	0x00a3
                           0000AA  1217 _T2CLKSRC	=	0x00aa
                           0000AC  1218 _T2CNT0	=	0x00ac
                           0000AD  1219 _T2CNT1	=	0x00ad
                           00ADAC  1220 _T2CNT	=	0xadac
                           0000A9  1221 _T2MODE	=	0x00a9
                           0000AE  1222 _T2PERIOD0	=	0x00ae
                           0000AF  1223 _T2PERIOD1	=	0x00af
                           00AFAE  1224 _T2PERIOD	=	0xafae
                           0000AB  1225 _T2STATUS	=	0x00ab
                           0000E4  1226 _U0CTRL	=	0x00e4
                           0000E7  1227 _U0MODE	=	0x00e7
                           0000E6  1228 _U0SHREG	=	0x00e6
                           0000E5  1229 _U0STATUS	=	0x00e5
                           0000EC  1230 _U1CTRL	=	0x00ec
                           0000EF  1231 _U1MODE	=	0x00ef
                           0000EE  1232 _U1SHREG	=	0x00ee
                           0000ED  1233 _U1STATUS	=	0x00ed
                           0000DA  1234 _WDTCFG	=	0x00da
                           0000DB  1235 _WDTRESET	=	0x00db
                           0000F1  1236 _WTCFGA	=	0x00f1
                           0000F9  1237 _WTCFGB	=	0x00f9
                           0000F2  1238 _WTCNTA0	=	0x00f2
                           0000F3  1239 _WTCNTA1	=	0x00f3
                           00F3F2  1240 _WTCNTA	=	0xf3f2
                           0000FA  1241 _WTCNTB0	=	0x00fa
                           0000FB  1242 _WTCNTB1	=	0x00fb
                           00FBFA  1243 _WTCNTB	=	0xfbfa
                           0000EB  1244 _WTCNTR1	=	0x00eb
                           0000F4  1245 _WTEVTA0	=	0x00f4
                           0000F5  1246 _WTEVTA1	=	0x00f5
                           00F5F4  1247 _WTEVTA	=	0xf5f4
                           0000F6  1248 _WTEVTB0	=	0x00f6
                           0000F7  1249 _WTEVTB1	=	0x00f7
                           00F7F6  1250 _WTEVTB	=	0xf7f6
                           0000FC  1251 _WTEVTC0	=	0x00fc
                           0000FD  1252 _WTEVTC1	=	0x00fd
                           00FDFC  1253 _WTEVTC	=	0xfdfc
                           0000FE  1254 _WTEVTD0	=	0x00fe
                           0000FF  1255 _WTEVTD1	=	0x00ff
                           00FFFE  1256 _WTEVTD	=	0xfffe
                           0000E9  1257 _WTIRQEN	=	0x00e9
                           0000EA  1258 _WTSTAT	=	0x00ea
                                   1259 ;--------------------------------------------------------
                                   1260 ; special function bits
                                   1261 ;--------------------------------------------------------
                                   1262 	.area RSEG    (ABS,DATA)
      000000                       1263 	.org 0x0000
                           0000E0  1264 _ACC_0	=	0x00e0
                           0000E1  1265 _ACC_1	=	0x00e1
                           0000E2  1266 _ACC_2	=	0x00e2
                           0000E3  1267 _ACC_3	=	0x00e3
                           0000E4  1268 _ACC_4	=	0x00e4
                           0000E5  1269 _ACC_5	=	0x00e5
                           0000E6  1270 _ACC_6	=	0x00e6
                           0000E7  1271 _ACC_7	=	0x00e7
                           0000F0  1272 _B_0	=	0x00f0
                           0000F1  1273 _B_1	=	0x00f1
                           0000F2  1274 _B_2	=	0x00f2
                           0000F3  1275 _B_3	=	0x00f3
                           0000F4  1276 _B_4	=	0x00f4
                           0000F5  1277 _B_5	=	0x00f5
                           0000F6  1278 _B_6	=	0x00f6
                           0000F7  1279 _B_7	=	0x00f7
                           0000A0  1280 _E2IE_0	=	0x00a0
                           0000A1  1281 _E2IE_1	=	0x00a1
                           0000A2  1282 _E2IE_2	=	0x00a2
                           0000A3  1283 _E2IE_3	=	0x00a3
                           0000A4  1284 _E2IE_4	=	0x00a4
                           0000A5  1285 _E2IE_5	=	0x00a5
                           0000A6  1286 _E2IE_6	=	0x00a6
                           0000A7  1287 _E2IE_7	=	0x00a7
                           0000C0  1288 _E2IP_0	=	0x00c0
                           0000C1  1289 _E2IP_1	=	0x00c1
                           0000C2  1290 _E2IP_2	=	0x00c2
                           0000C3  1291 _E2IP_3	=	0x00c3
                           0000C4  1292 _E2IP_4	=	0x00c4
                           0000C5  1293 _E2IP_5	=	0x00c5
                           0000C6  1294 _E2IP_6	=	0x00c6
                           0000C7  1295 _E2IP_7	=	0x00c7
                           000098  1296 _EIE_0	=	0x0098
                           000099  1297 _EIE_1	=	0x0099
                           00009A  1298 _EIE_2	=	0x009a
                           00009B  1299 _EIE_3	=	0x009b
                           00009C  1300 _EIE_4	=	0x009c
                           00009D  1301 _EIE_5	=	0x009d
                           00009E  1302 _EIE_6	=	0x009e
                           00009F  1303 _EIE_7	=	0x009f
                           0000B0  1304 _EIP_0	=	0x00b0
                           0000B1  1305 _EIP_1	=	0x00b1
                           0000B2  1306 _EIP_2	=	0x00b2
                           0000B3  1307 _EIP_3	=	0x00b3
                           0000B4  1308 _EIP_4	=	0x00b4
                           0000B5  1309 _EIP_5	=	0x00b5
                           0000B6  1310 _EIP_6	=	0x00b6
                           0000B7  1311 _EIP_7	=	0x00b7
                           0000A8  1312 _IE_0	=	0x00a8
                           0000A9  1313 _IE_1	=	0x00a9
                           0000AA  1314 _IE_2	=	0x00aa
                           0000AB  1315 _IE_3	=	0x00ab
                           0000AC  1316 _IE_4	=	0x00ac
                           0000AD  1317 _IE_5	=	0x00ad
                           0000AE  1318 _IE_6	=	0x00ae
                           0000AF  1319 _IE_7	=	0x00af
                           0000AF  1320 _EA	=	0x00af
                           0000B8  1321 _IP_0	=	0x00b8
                           0000B9  1322 _IP_1	=	0x00b9
                           0000BA  1323 _IP_2	=	0x00ba
                           0000BB  1324 _IP_3	=	0x00bb
                           0000BC  1325 _IP_4	=	0x00bc
                           0000BD  1326 _IP_5	=	0x00bd
                           0000BE  1327 _IP_6	=	0x00be
                           0000BF  1328 _IP_7	=	0x00bf
                           0000D0  1329 _P	=	0x00d0
                           0000D1  1330 _F1	=	0x00d1
                           0000D2  1331 _OV	=	0x00d2
                           0000D3  1332 _RS0	=	0x00d3
                           0000D4  1333 _RS1	=	0x00d4
                           0000D5  1334 _F0	=	0x00d5
                           0000D6  1335 _AC	=	0x00d6
                           0000D7  1336 _CY	=	0x00d7
                           0000C8  1337 _PINA_0	=	0x00c8
                           0000C9  1338 _PINA_1	=	0x00c9
                           0000CA  1339 _PINA_2	=	0x00ca
                           0000CB  1340 _PINA_3	=	0x00cb
                           0000CC  1341 _PINA_4	=	0x00cc
                           0000CD  1342 _PINA_5	=	0x00cd
                           0000CE  1343 _PINA_6	=	0x00ce
                           0000CF  1344 _PINA_7	=	0x00cf
                           0000E8  1345 _PINB_0	=	0x00e8
                           0000E9  1346 _PINB_1	=	0x00e9
                           0000EA  1347 _PINB_2	=	0x00ea
                           0000EB  1348 _PINB_3	=	0x00eb
                           0000EC  1349 _PINB_4	=	0x00ec
                           0000ED  1350 _PINB_5	=	0x00ed
                           0000EE  1351 _PINB_6	=	0x00ee
                           0000EF  1352 _PINB_7	=	0x00ef
                           0000F8  1353 _PINC_0	=	0x00f8
                           0000F9  1354 _PINC_1	=	0x00f9
                           0000FA  1355 _PINC_2	=	0x00fa
                           0000FB  1356 _PINC_3	=	0x00fb
                           0000FC  1357 _PINC_4	=	0x00fc
                           0000FD  1358 _PINC_5	=	0x00fd
                           0000FE  1359 _PINC_6	=	0x00fe
                           0000FF  1360 _PINC_7	=	0x00ff
                           000080  1361 _PORTA_0	=	0x0080
                           000081  1362 _PORTA_1	=	0x0081
                           000082  1363 _PORTA_2	=	0x0082
                           000083  1364 _PORTA_3	=	0x0083
                           000084  1365 _PORTA_4	=	0x0084
                           000085  1366 _PORTA_5	=	0x0085
                           000086  1367 _PORTA_6	=	0x0086
                           000087  1368 _PORTA_7	=	0x0087
                           000088  1369 _PORTB_0	=	0x0088
                           000089  1370 _PORTB_1	=	0x0089
                           00008A  1371 _PORTB_2	=	0x008a
                           00008B  1372 _PORTB_3	=	0x008b
                           00008C  1373 _PORTB_4	=	0x008c
                           00008D  1374 _PORTB_5	=	0x008d
                           00008E  1375 _PORTB_6	=	0x008e
                           00008F  1376 _PORTB_7	=	0x008f
                           000090  1377 _PORTC_0	=	0x0090
                           000091  1378 _PORTC_1	=	0x0091
                           000092  1379 _PORTC_2	=	0x0092
                           000093  1380 _PORTC_3	=	0x0093
                           000094  1381 _PORTC_4	=	0x0094
                           000095  1382 _PORTC_5	=	0x0095
                           000096  1383 _PORTC_6	=	0x0096
                           000097  1384 _PORTC_7	=	0x0097
                                   1385 ;--------------------------------------------------------
                                   1386 ; overlayable register banks
                                   1387 ;--------------------------------------------------------
                                   1388 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                       1389 	.ds 8
                                   1390 ;--------------------------------------------------------
                                   1391 ; internal ram data
                                   1392 ;--------------------------------------------------------
                                   1393 	.area DSEG    (DATA)
      00004A                       1394 _UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0:
      00004A                       1395 	.ds 1
      00004B                       1396 _UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0:
      00004B                       1397 	.ds 1
      00004C                       1398 _UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0:
      00004C                       1399 	.ds 3
      00004F                       1400 _UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0:
      00004F                       1401 	.ds 2
      000051                       1402 _UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0:
      000051                       1403 	.ds 2
      000053                       1404 _UBLOX_GPS_SendCommand_WaitACK_sloc5_1_0:
      000053                       1405 	.ds 3
                                   1406 ;--------------------------------------------------------
                                   1407 ; overlayable items in internal ram 
                                   1408 ;--------------------------------------------------------
                                   1409 	.area	OSEG    (OVR,DATA)
      00005E                       1410 _UBLOX_GPS_FletcherChecksum8_sloc0_1_0:
      00005E                       1411 	.ds 3
      000061                       1412 _UBLOX_GPS_FletcherChecksum8_sloc1_1_0:
      000061                       1413 	.ds 1
      000062                       1414 _UBLOX_GPS_FletcherChecksum8_sloc2_1_0:
      000062                       1415 	.ds 1
      000063                       1416 _UBLOX_GPS_FletcherChecksum8_sloc3_1_0:
      000063                       1417 	.ds 3
                                   1418 ;--------------------------------------------------------
                                   1419 ; indirectly addressable internal ram data
                                   1420 ;--------------------------------------------------------
                                   1421 	.area ISEG    (DATA)
                                   1422 ;--------------------------------------------------------
                                   1423 ; absolute internal ram data
                                   1424 ;--------------------------------------------------------
                                   1425 	.area IABS    (ABS,DATA)
                                   1426 	.area IABS    (ABS,DATA)
                                   1427 ;--------------------------------------------------------
                                   1428 ; bit data
                                   1429 ;--------------------------------------------------------
                                   1430 	.area BSEG    (BIT)
                                   1431 ;--------------------------------------------------------
                                   1432 ; paged external ram data
                                   1433 ;--------------------------------------------------------
                                   1434 	.area PSEG    (PAG,XDATA)
                                   1435 ;--------------------------------------------------------
                                   1436 ; external ram data
                                   1437 ;--------------------------------------------------------
                                   1438 	.area XSEG    (XDATA)
                           007020  1439 _ADCCH0VAL0	=	0x7020
                           007021  1440 _ADCCH0VAL1	=	0x7021
                           007020  1441 _ADCCH0VAL	=	0x7020
                           007022  1442 _ADCCH1VAL0	=	0x7022
                           007023  1443 _ADCCH1VAL1	=	0x7023
                           007022  1444 _ADCCH1VAL	=	0x7022
                           007024  1445 _ADCCH2VAL0	=	0x7024
                           007025  1446 _ADCCH2VAL1	=	0x7025
                           007024  1447 _ADCCH2VAL	=	0x7024
                           007026  1448 _ADCCH3VAL0	=	0x7026
                           007027  1449 _ADCCH3VAL1	=	0x7027
                           007026  1450 _ADCCH3VAL	=	0x7026
                           007028  1451 _ADCTUNE0	=	0x7028
                           007029  1452 _ADCTUNE1	=	0x7029
                           00702A  1453 _ADCTUNE2	=	0x702a
                           007010  1454 _DMA0ADDR0	=	0x7010
                           007011  1455 _DMA0ADDR1	=	0x7011
                           007010  1456 _DMA0ADDR	=	0x7010
                           007014  1457 _DMA0CONFIG	=	0x7014
                           007012  1458 _DMA1ADDR0	=	0x7012
                           007013  1459 _DMA1ADDR1	=	0x7013
                           007012  1460 _DMA1ADDR	=	0x7012
                           007015  1461 _DMA1CONFIG	=	0x7015
                           007070  1462 _FRCOSCCONFIG	=	0x7070
                           007071  1463 _FRCOSCCTRL	=	0x7071
                           007076  1464 _FRCOSCFREQ0	=	0x7076
                           007077  1465 _FRCOSCFREQ1	=	0x7077
                           007076  1466 _FRCOSCFREQ	=	0x7076
                           007072  1467 _FRCOSCKFILT0	=	0x7072
                           007073  1468 _FRCOSCKFILT1	=	0x7073
                           007072  1469 _FRCOSCKFILT	=	0x7072
                           007078  1470 _FRCOSCPER0	=	0x7078
                           007079  1471 _FRCOSCPER1	=	0x7079
                           007078  1472 _FRCOSCPER	=	0x7078
                           007074  1473 _FRCOSCREF0	=	0x7074
                           007075  1474 _FRCOSCREF1	=	0x7075
                           007074  1475 _FRCOSCREF	=	0x7074
                           007007  1476 _ANALOGA	=	0x7007
                           00700C  1477 _GPIOENABLE	=	0x700c
                           007003  1478 _EXTIRQ	=	0x7003
                           007000  1479 _INTCHGA	=	0x7000
                           007001  1480 _INTCHGB	=	0x7001
                           007002  1481 _INTCHGC	=	0x7002
                           007008  1482 _PALTA	=	0x7008
                           007009  1483 _PALTB	=	0x7009
                           00700A  1484 _PALTC	=	0x700a
                           007046  1485 _PALTRADIO	=	0x7046
                           007004  1486 _PINCHGA	=	0x7004
                           007005  1487 _PINCHGB	=	0x7005
                           007006  1488 _PINCHGC	=	0x7006
                           00700B  1489 _PINSEL	=	0x700b
                           007060  1490 _LPOSCCONFIG	=	0x7060
                           007066  1491 _LPOSCFREQ0	=	0x7066
                           007067  1492 _LPOSCFREQ1	=	0x7067
                           007066  1493 _LPOSCFREQ	=	0x7066
                           007062  1494 _LPOSCKFILT0	=	0x7062
                           007063  1495 _LPOSCKFILT1	=	0x7063
                           007062  1496 _LPOSCKFILT	=	0x7062
                           007068  1497 _LPOSCPER0	=	0x7068
                           007069  1498 _LPOSCPER1	=	0x7069
                           007068  1499 _LPOSCPER	=	0x7068
                           007064  1500 _LPOSCREF0	=	0x7064
                           007065  1501 _LPOSCREF1	=	0x7065
                           007064  1502 _LPOSCREF	=	0x7064
                           007054  1503 _LPXOSCGM	=	0x7054
                           007F01  1504 _MISCCTRL	=	0x7f01
                           007053  1505 _OSCCALIB	=	0x7053
                           007050  1506 _OSCFORCERUN	=	0x7050
                           007052  1507 _OSCREADY	=	0x7052
                           007051  1508 _OSCRUN	=	0x7051
                           007040  1509 _RADIOFDATAADDR0	=	0x7040
                           007041  1510 _RADIOFDATAADDR1	=	0x7041
                           007040  1511 _RADIOFDATAADDR	=	0x7040
                           007042  1512 _RADIOFSTATADDR0	=	0x7042
                           007043  1513 _RADIOFSTATADDR1	=	0x7043
                           007042  1514 _RADIOFSTATADDR	=	0x7042
                           007044  1515 _RADIOMUX	=	0x7044
                           007084  1516 _SCRATCH0	=	0x7084
                           007085  1517 _SCRATCH1	=	0x7085
                           007086  1518 _SCRATCH2	=	0x7086
                           007087  1519 _SCRATCH3	=	0x7087
                           007F00  1520 _SILICONREV	=	0x7f00
                           007F19  1521 _XTALAMPL	=	0x7f19
                           007F18  1522 _XTALOSC	=	0x7f18
                           007F1A  1523 _XTALREADY	=	0x7f1a
                           007081  1524 _RNGBYTE	=	0x7081
                           007091  1525 _AESCONFIG	=	0x7091
                           007098  1526 _AESCURBLOCK	=	0x7098
                           007094  1527 _AESINADDR0	=	0x7094
                           007095  1528 _AESINADDR1	=	0x7095
                           007094  1529 _AESINADDR	=	0x7094
                           007092  1530 _AESKEYADDR0	=	0x7092
                           007093  1531 _AESKEYADDR1	=	0x7093
                           007092  1532 _AESKEYADDR	=	0x7092
                           007090  1533 _AESMODE	=	0x7090
                           007096  1534 _AESOUTADDR0	=	0x7096
                           007097  1535 _AESOUTADDR1	=	0x7097
                           007096  1536 _AESOUTADDR	=	0x7096
                           007080  1537 _RNGMODE	=	0x7080
                           007082  1538 _RNGCLKSRC0	=	0x7082
                           007083  1539 _RNGCLKSRC1	=	0x7083
                           003F82  1540 _XDPTR0	=	0x3f82
                           003F84  1541 _XDPTR1	=	0x3f84
                           003FA8  1542 _XIE	=	0x3fa8
                           003FB8  1543 _XIP	=	0x3fb8
                           003F87  1544 _XPCON	=	0x3f87
                           003FCA  1545 _XADCCH0CONFIG	=	0x3fca
                           003FCB  1546 _XADCCH1CONFIG	=	0x3fcb
                           003FD2  1547 _XADCCH2CONFIG	=	0x3fd2
                           003FD3  1548 _XADCCH3CONFIG	=	0x3fd3
                           003FD1  1549 _XADCCLKSRC	=	0x3fd1
                           003FC9  1550 _XADCCONV	=	0x3fc9
                           003FE1  1551 _XANALOGCOMP	=	0x3fe1
                           003FC6  1552 _XCLKCON	=	0x3fc6
                           003FC7  1553 _XCLKSTAT	=	0x3fc7
                           003F97  1554 _XCODECONFIG	=	0x3f97
                           003FE3  1555 _XDBGLNKBUF	=	0x3fe3
                           003FE2  1556 _XDBGLNKSTAT	=	0x3fe2
                           003F89  1557 _XDIRA	=	0x3f89
                           003F8A  1558 _XDIRB	=	0x3f8a
                           003F8B  1559 _XDIRC	=	0x3f8b
                           003F8E  1560 _XDIRR	=	0x3f8e
                           003FC8  1561 _XPINA	=	0x3fc8
                           003FE8  1562 _XPINB	=	0x3fe8
                           003FF8  1563 _XPINC	=	0x3ff8
                           003F8D  1564 _XPINR	=	0x3f8d
                           003F80  1565 _XPORTA	=	0x3f80
                           003F88  1566 _XPORTB	=	0x3f88
                           003F90  1567 _XPORTC	=	0x3f90
                           003F8C  1568 _XPORTR	=	0x3f8c
                           003FCE  1569 _XIC0CAPT0	=	0x3fce
                           003FCF  1570 _XIC0CAPT1	=	0x3fcf
                           003FCE  1571 _XIC0CAPT	=	0x3fce
                           003FCC  1572 _XIC0MODE	=	0x3fcc
                           003FCD  1573 _XIC0STATUS	=	0x3fcd
                           003FD6  1574 _XIC1CAPT0	=	0x3fd6
                           003FD7  1575 _XIC1CAPT1	=	0x3fd7
                           003FD6  1576 _XIC1CAPT	=	0x3fd6
                           003FD4  1577 _XIC1MODE	=	0x3fd4
                           003FD5  1578 _XIC1STATUS	=	0x3fd5
                           003F92  1579 _XNVADDR0	=	0x3f92
                           003F93  1580 _XNVADDR1	=	0x3f93
                           003F92  1581 _XNVADDR	=	0x3f92
                           003F94  1582 _XNVDATA0	=	0x3f94
                           003F95  1583 _XNVDATA1	=	0x3f95
                           003F94  1584 _XNVDATA	=	0x3f94
                           003F96  1585 _XNVKEY	=	0x3f96
                           003F91  1586 _XNVSTATUS	=	0x3f91
                           003FBC  1587 _XOC0COMP0	=	0x3fbc
                           003FBD  1588 _XOC0COMP1	=	0x3fbd
                           003FBC  1589 _XOC0COMP	=	0x3fbc
                           003FB9  1590 _XOC0MODE	=	0x3fb9
                           003FBA  1591 _XOC0PIN	=	0x3fba
                           003FBB  1592 _XOC0STATUS	=	0x3fbb
                           003FC4  1593 _XOC1COMP0	=	0x3fc4
                           003FC5  1594 _XOC1COMP1	=	0x3fc5
                           003FC4  1595 _XOC1COMP	=	0x3fc4
                           003FC1  1596 _XOC1MODE	=	0x3fc1
                           003FC2  1597 _XOC1PIN	=	0x3fc2
                           003FC3  1598 _XOC1STATUS	=	0x3fc3
                           003FB1  1599 _XRADIOACC	=	0x3fb1
                           003FB3  1600 _XRADIOADDR0	=	0x3fb3
                           003FB2  1601 _XRADIOADDR1	=	0x3fb2
                           003FB7  1602 _XRADIODATA0	=	0x3fb7
                           003FB6  1603 _XRADIODATA1	=	0x3fb6
                           003FB5  1604 _XRADIODATA2	=	0x3fb5
                           003FB4  1605 _XRADIODATA3	=	0x3fb4
                           003FBE  1606 _XRADIOSTAT0	=	0x3fbe
                           003FBF  1607 _XRADIOSTAT1	=	0x3fbf
                           003FBE  1608 _XRADIOSTAT	=	0x3fbe
                           003FDF  1609 _XSPCLKSRC	=	0x3fdf
                           003FDC  1610 _XSPMODE	=	0x3fdc
                           003FDE  1611 _XSPSHREG	=	0x3fde
                           003FDD  1612 _XSPSTATUS	=	0x3fdd
                           003F9A  1613 _XT0CLKSRC	=	0x3f9a
                           003F9C  1614 _XT0CNT0	=	0x3f9c
                           003F9D  1615 _XT0CNT1	=	0x3f9d
                           003F9C  1616 _XT0CNT	=	0x3f9c
                           003F99  1617 _XT0MODE	=	0x3f99
                           003F9E  1618 _XT0PERIOD0	=	0x3f9e
                           003F9F  1619 _XT0PERIOD1	=	0x3f9f
                           003F9E  1620 _XT0PERIOD	=	0x3f9e
                           003F9B  1621 _XT0STATUS	=	0x3f9b
                           003FA2  1622 _XT1CLKSRC	=	0x3fa2
                           003FA4  1623 _XT1CNT0	=	0x3fa4
                           003FA5  1624 _XT1CNT1	=	0x3fa5
                           003FA4  1625 _XT1CNT	=	0x3fa4
                           003FA1  1626 _XT1MODE	=	0x3fa1
                           003FA6  1627 _XT1PERIOD0	=	0x3fa6
                           003FA7  1628 _XT1PERIOD1	=	0x3fa7
                           003FA6  1629 _XT1PERIOD	=	0x3fa6
                           003FA3  1630 _XT1STATUS	=	0x3fa3
                           003FAA  1631 _XT2CLKSRC	=	0x3faa
                           003FAC  1632 _XT2CNT0	=	0x3fac
                           003FAD  1633 _XT2CNT1	=	0x3fad
                           003FAC  1634 _XT2CNT	=	0x3fac
                           003FA9  1635 _XT2MODE	=	0x3fa9
                           003FAE  1636 _XT2PERIOD0	=	0x3fae
                           003FAF  1637 _XT2PERIOD1	=	0x3faf
                           003FAE  1638 _XT2PERIOD	=	0x3fae
                           003FAB  1639 _XT2STATUS	=	0x3fab
                           003FE4  1640 _XU0CTRL	=	0x3fe4
                           003FE7  1641 _XU0MODE	=	0x3fe7
                           003FE6  1642 _XU0SHREG	=	0x3fe6
                           003FE5  1643 _XU0STATUS	=	0x3fe5
                           003FEC  1644 _XU1CTRL	=	0x3fec
                           003FEF  1645 _XU1MODE	=	0x3fef
                           003FEE  1646 _XU1SHREG	=	0x3fee
                           003FED  1647 _XU1STATUS	=	0x3fed
                           003FDA  1648 _XWDTCFG	=	0x3fda
                           003FDB  1649 _XWDTRESET	=	0x3fdb
                           003FF1  1650 _XWTCFGA	=	0x3ff1
                           003FF9  1651 _XWTCFGB	=	0x3ff9
                           003FF2  1652 _XWTCNTA0	=	0x3ff2
                           003FF3  1653 _XWTCNTA1	=	0x3ff3
                           003FF2  1654 _XWTCNTA	=	0x3ff2
                           003FFA  1655 _XWTCNTB0	=	0x3ffa
                           003FFB  1656 _XWTCNTB1	=	0x3ffb
                           003FFA  1657 _XWTCNTB	=	0x3ffa
                           003FEB  1658 _XWTCNTR1	=	0x3feb
                           003FF4  1659 _XWTEVTA0	=	0x3ff4
                           003FF5  1660 _XWTEVTA1	=	0x3ff5
                           003FF4  1661 _XWTEVTA	=	0x3ff4
                           003FF6  1662 _XWTEVTB0	=	0x3ff6
                           003FF7  1663 _XWTEVTB1	=	0x3ff7
                           003FF6  1664 _XWTEVTB	=	0x3ff6
                           003FFC  1665 _XWTEVTC0	=	0x3ffc
                           003FFD  1666 _XWTEVTC1	=	0x3ffd
                           003FFC  1667 _XWTEVTC	=	0x3ffc
                           003FFE  1668 _XWTEVTD0	=	0x3ffe
                           003FFF  1669 _XWTEVTD1	=	0x3fff
                           003FFE  1670 _XWTEVTD	=	0x3ffe
                           003FE9  1671 _XWTIRQEN	=	0x3fe9
                           003FEA  1672 _XWTSTAT	=	0x3fea
                           004114  1673 _AX5043_AFSKCTRL	=	0x4114
                           004113  1674 _AX5043_AFSKMARK0	=	0x4113
                           004112  1675 _AX5043_AFSKMARK1	=	0x4112
                           004111  1676 _AX5043_AFSKSPACE0	=	0x4111
                           004110  1677 _AX5043_AFSKSPACE1	=	0x4110
                           004043  1678 _AX5043_AGCCOUNTER	=	0x4043
                           004115  1679 _AX5043_AMPLFILTER	=	0x4115
                           004189  1680 _AX5043_BBOFFSCAP	=	0x4189
                           004188  1681 _AX5043_BBTUNE	=	0x4188
                           004041  1682 _AX5043_BGNDRSSI	=	0x4041
                           00422E  1683 _AX5043_BGNDRSSIGAIN	=	0x422e
                           00422F  1684 _AX5043_BGNDRSSITHR	=	0x422f
                           004017  1685 _AX5043_CRCINIT0	=	0x4017
                           004016  1686 _AX5043_CRCINIT1	=	0x4016
                           004015  1687 _AX5043_CRCINIT2	=	0x4015
                           004014  1688 _AX5043_CRCINIT3	=	0x4014
                           004332  1689 _AX5043_DACCONFIG	=	0x4332
                           004331  1690 _AX5043_DACVALUE0	=	0x4331
                           004330  1691 _AX5043_DACVALUE1	=	0x4330
                           004102  1692 _AX5043_DECIMATION	=	0x4102
                           004042  1693 _AX5043_DIVERSITY	=	0x4042
                           004011  1694 _AX5043_ENCODING	=	0x4011
                           004018  1695 _AX5043_FEC	=	0x4018
                           00401A  1696 _AX5043_FECSTATUS	=	0x401a
                           004019  1697 _AX5043_FECSYNC	=	0x4019
                           00402B  1698 _AX5043_FIFOCOUNT0	=	0x402b
                           00402A  1699 _AX5043_FIFOCOUNT1	=	0x402a
                           004029  1700 _AX5043_FIFODATA	=	0x4029
                           00402D  1701 _AX5043_FIFOFREE0	=	0x402d
                           00402C  1702 _AX5043_FIFOFREE1	=	0x402c
                           004028  1703 _AX5043_FIFOSTAT	=	0x4028
                           00402F  1704 _AX5043_FIFOTHRESH0	=	0x402f
                           00402E  1705 _AX5043_FIFOTHRESH1	=	0x402e
                           004012  1706 _AX5043_FRAMING	=	0x4012
                           004037  1707 _AX5043_FREQA0	=	0x4037
                           004036  1708 _AX5043_FREQA1	=	0x4036
                           004035  1709 _AX5043_FREQA2	=	0x4035
                           004034  1710 _AX5043_FREQA3	=	0x4034
                           00403F  1711 _AX5043_FREQB0	=	0x403f
                           00403E  1712 _AX5043_FREQB1	=	0x403e
                           00403D  1713 _AX5043_FREQB2	=	0x403d
                           00403C  1714 _AX5043_FREQB3	=	0x403c
                           004163  1715 _AX5043_FSKDEV0	=	0x4163
                           004162  1716 _AX5043_FSKDEV1	=	0x4162
                           004161  1717 _AX5043_FSKDEV2	=	0x4161
                           00410D  1718 _AX5043_FSKDMAX0	=	0x410d
                           00410C  1719 _AX5043_FSKDMAX1	=	0x410c
                           00410F  1720 _AX5043_FSKDMIN0	=	0x410f
                           00410E  1721 _AX5043_FSKDMIN1	=	0x410e
                           004309  1722 _AX5043_GPADC13VALUE0	=	0x4309
                           004308  1723 _AX5043_GPADC13VALUE1	=	0x4308
                           004300  1724 _AX5043_GPADCCTRL	=	0x4300
                           004301  1725 _AX5043_GPADCPERIOD	=	0x4301
                           004101  1726 _AX5043_IFFREQ0	=	0x4101
                           004100  1727 _AX5043_IFFREQ1	=	0x4100
                           00400B  1728 _AX5043_IRQINVERSION0	=	0x400b
                           00400A  1729 _AX5043_IRQINVERSION1	=	0x400a
                           004007  1730 _AX5043_IRQMASK0	=	0x4007
                           004006  1731 _AX5043_IRQMASK1	=	0x4006
                           00400D  1732 _AX5043_IRQREQUEST0	=	0x400d
                           00400C  1733 _AX5043_IRQREQUEST1	=	0x400c
                           004310  1734 _AX5043_LPOSCCONFIG	=	0x4310
                           004317  1735 _AX5043_LPOSCFREQ0	=	0x4317
                           004316  1736 _AX5043_LPOSCFREQ1	=	0x4316
                           004313  1737 _AX5043_LPOSCKFILT0	=	0x4313
                           004312  1738 _AX5043_LPOSCKFILT1	=	0x4312
                           004319  1739 _AX5043_LPOSCPER0	=	0x4319
                           004318  1740 _AX5043_LPOSCPER1	=	0x4318
                           004315  1741 _AX5043_LPOSCREF0	=	0x4315
                           004314  1742 _AX5043_LPOSCREF1	=	0x4314
                           004311  1743 _AX5043_LPOSCSTATUS	=	0x4311
                           004214  1744 _AX5043_MATCH0LEN	=	0x4214
                           004216  1745 _AX5043_MATCH0MAX	=	0x4216
                           004215  1746 _AX5043_MATCH0MIN	=	0x4215
                           004213  1747 _AX5043_MATCH0PAT0	=	0x4213
                           004212  1748 _AX5043_MATCH0PAT1	=	0x4212
                           004211  1749 _AX5043_MATCH0PAT2	=	0x4211
                           004210  1750 _AX5043_MATCH0PAT3	=	0x4210
                           00421C  1751 _AX5043_MATCH1LEN	=	0x421c
                           00421E  1752 _AX5043_MATCH1MAX	=	0x421e
                           00421D  1753 _AX5043_MATCH1MIN	=	0x421d
                           004219  1754 _AX5043_MATCH1PAT0	=	0x4219
                           004218  1755 _AX5043_MATCH1PAT1	=	0x4218
                           004108  1756 _AX5043_MAXDROFFSET0	=	0x4108
                           004107  1757 _AX5043_MAXDROFFSET1	=	0x4107
                           004106  1758 _AX5043_MAXDROFFSET2	=	0x4106
                           00410B  1759 _AX5043_MAXRFOFFSET0	=	0x410b
                           00410A  1760 _AX5043_MAXRFOFFSET1	=	0x410a
                           004109  1761 _AX5043_MAXRFOFFSET2	=	0x4109
                           004164  1762 _AX5043_MODCFGA	=	0x4164
                           004160  1763 _AX5043_MODCFGF	=	0x4160
                           004F5F  1764 _AX5043_MODCFGP	=	0x4f5f
                           004010  1765 _AX5043_MODULATION	=	0x4010
                           004025  1766 _AX5043_PINFUNCANTSEL	=	0x4025
                           004023  1767 _AX5043_PINFUNCDATA	=	0x4023
                           004022  1768 _AX5043_PINFUNCDCLK	=	0x4022
                           004024  1769 _AX5043_PINFUNCIRQ	=	0x4024
                           004026  1770 _AX5043_PINFUNCPWRAMP	=	0x4026
                           004021  1771 _AX5043_PINFUNCSYSCLK	=	0x4021
                           004020  1772 _AX5043_PINSTATE	=	0x4020
                           004233  1773 _AX5043_PKTACCEPTFLAGS	=	0x4233
                           004230  1774 _AX5043_PKTCHUNKSIZE	=	0x4230
                           004231  1775 _AX5043_PKTMISCFLAGS	=	0x4231
                           004232  1776 _AX5043_PKTSTOREFLAGS	=	0x4232
                           004031  1777 _AX5043_PLLCPI	=	0x4031
                           004039  1778 _AX5043_PLLCPIBOOST	=	0x4039
                           004182  1779 _AX5043_PLLLOCKDET	=	0x4182
                           004030  1780 _AX5043_PLLLOOP	=	0x4030
                           004038  1781 _AX5043_PLLLOOPBOOST	=	0x4038
                           004033  1782 _AX5043_PLLRANGINGA	=	0x4033
                           00403B  1783 _AX5043_PLLRANGINGB	=	0x403b
                           004183  1784 _AX5043_PLLRNGCLK	=	0x4183
                           004032  1785 _AX5043_PLLVCODIV	=	0x4032
                           004180  1786 _AX5043_PLLVCOI	=	0x4180
                           004181  1787 _AX5043_PLLVCOIR	=	0x4181
                           004F08  1788 _AX5043_POWCTRL1	=	0x4f08
                           004005  1789 _AX5043_POWIRQMASK	=	0x4005
                           004003  1790 _AX5043_POWSTAT	=	0x4003
                           004004  1791 _AX5043_POWSTICKYSTAT	=	0x4004
                           004027  1792 _AX5043_PWRAMP	=	0x4027
                           004002  1793 _AX5043_PWRMODE	=	0x4002
                           004009  1794 _AX5043_RADIOEVENTMASK0	=	0x4009
                           004008  1795 _AX5043_RADIOEVENTMASK1	=	0x4008
                           00400F  1796 _AX5043_RADIOEVENTREQ0	=	0x400f
                           00400E  1797 _AX5043_RADIOEVENTREQ1	=	0x400e
                           00401C  1798 _AX5043_RADIOSTATE	=	0x401c
                           004F0D  1799 _AX5043_REF	=	0x4f0d
                           004040  1800 _AX5043_RSSI	=	0x4040
                           00422D  1801 _AX5043_RSSIABSTHR	=	0x422d
                           00422C  1802 _AX5043_RSSIREFERENCE	=	0x422c
                           004105  1803 _AX5043_RXDATARATE0	=	0x4105
                           004104  1804 _AX5043_RXDATARATE1	=	0x4104
                           004103  1805 _AX5043_RXDATARATE2	=	0x4103
                           004001  1806 _AX5043_SCRATCH	=	0x4001
                           004000  1807 _AX5043_SILICONREVISION	=	0x4000
                           00405B  1808 _AX5043_TIMER0	=	0x405b
                           00405A  1809 _AX5043_TIMER1	=	0x405a
                           004059  1810 _AX5043_TIMER2	=	0x4059
                           004227  1811 _AX5043_TMGRXAGC	=	0x4227
                           004223  1812 _AX5043_TMGRXBOOST	=	0x4223
                           004226  1813 _AX5043_TMGRXCOARSEAGC	=	0x4226
                           004225  1814 _AX5043_TMGRXOFFSACQ	=	0x4225
                           004229  1815 _AX5043_TMGRXPREAMBLE1	=	0x4229
                           00422A  1816 _AX5043_TMGRXPREAMBLE2	=	0x422a
                           00422B  1817 _AX5043_TMGRXPREAMBLE3	=	0x422b
                           004228  1818 _AX5043_TMGRXRSSI	=	0x4228
                           004224  1819 _AX5043_TMGRXSETTLE	=	0x4224
                           004220  1820 _AX5043_TMGTXBOOST	=	0x4220
                           004221  1821 _AX5043_TMGTXSETTLE	=	0x4221
                           004055  1822 _AX5043_TRKAFSKDEMOD0	=	0x4055
                           004054  1823 _AX5043_TRKAFSKDEMOD1	=	0x4054
                           004049  1824 _AX5043_TRKAMPLITUDE0	=	0x4049
                           004048  1825 _AX5043_TRKAMPLITUDE1	=	0x4048
                           004047  1826 _AX5043_TRKDATARATE0	=	0x4047
                           004046  1827 _AX5043_TRKDATARATE1	=	0x4046
                           004045  1828 _AX5043_TRKDATARATE2	=	0x4045
                           004051  1829 _AX5043_TRKFREQ0	=	0x4051
                           004050  1830 _AX5043_TRKFREQ1	=	0x4050
                           004053  1831 _AX5043_TRKFSKDEMOD0	=	0x4053
                           004052  1832 _AX5043_TRKFSKDEMOD1	=	0x4052
                           00404B  1833 _AX5043_TRKPHASE0	=	0x404b
                           00404A  1834 _AX5043_TRKPHASE1	=	0x404a
                           00404F  1835 _AX5043_TRKRFFREQ0	=	0x404f
                           00404E  1836 _AX5043_TRKRFFREQ1	=	0x404e
                           00404D  1837 _AX5043_TRKRFFREQ2	=	0x404d
                           004169  1838 _AX5043_TXPWRCOEFFA0	=	0x4169
                           004168  1839 _AX5043_TXPWRCOEFFA1	=	0x4168
                           00416B  1840 _AX5043_TXPWRCOEFFB0	=	0x416b
                           00416A  1841 _AX5043_TXPWRCOEFFB1	=	0x416a
                           00416D  1842 _AX5043_TXPWRCOEFFC0	=	0x416d
                           00416C  1843 _AX5043_TXPWRCOEFFC1	=	0x416c
                           00416F  1844 _AX5043_TXPWRCOEFFD0	=	0x416f
                           00416E  1845 _AX5043_TXPWRCOEFFD1	=	0x416e
                           004171  1846 _AX5043_TXPWRCOEFFE0	=	0x4171
                           004170  1847 _AX5043_TXPWRCOEFFE1	=	0x4170
                           004167  1848 _AX5043_TXRATE0	=	0x4167
                           004166  1849 _AX5043_TXRATE1	=	0x4166
                           004165  1850 _AX5043_TXRATE2	=	0x4165
                           00406B  1851 _AX5043_WAKEUP0	=	0x406b
                           00406A  1852 _AX5043_WAKEUP1	=	0x406a
                           00406D  1853 _AX5043_WAKEUPFREQ0	=	0x406d
                           00406C  1854 _AX5043_WAKEUPFREQ1	=	0x406c
                           004069  1855 _AX5043_WAKEUPTIMER0	=	0x4069
                           004068  1856 _AX5043_WAKEUPTIMER1	=	0x4068
                           00406E  1857 _AX5043_WAKEUPXOEARLY	=	0x406e
                           004F11  1858 _AX5043_XTALAMPL	=	0x4f11
                           004184  1859 _AX5043_XTALCAP	=	0x4184
                           004F10  1860 _AX5043_XTALOSC	=	0x4f10
                           00401D  1861 _AX5043_XTALSTATUS	=	0x401d
                           004F00  1862 _AX5043_0xF00	=	0x4f00
                           004F0C  1863 _AX5043_0xF0C	=	0x4f0c
                           004F18  1864 _AX5043_0xF18	=	0x4f18
                           004F1C  1865 _AX5043_0xF1C	=	0x4f1c
                           004F21  1866 _AX5043_0xF21	=	0x4f21
                           004F22  1867 _AX5043_0xF22	=	0x4f22
                           004F23  1868 _AX5043_0xF23	=	0x4f23
                           004F26  1869 _AX5043_0xF26	=	0x4f26
                           004F30  1870 _AX5043_0xF30	=	0x4f30
                           004F31  1871 _AX5043_0xF31	=	0x4f31
                           004F32  1872 _AX5043_0xF32	=	0x4f32
                           004F33  1873 _AX5043_0xF33	=	0x4f33
                           004F34  1874 _AX5043_0xF34	=	0x4f34
                           004F35  1875 _AX5043_0xF35	=	0x4f35
                           004F44  1876 _AX5043_0xF44	=	0x4f44
                           004122  1877 _AX5043_AGCAHYST0	=	0x4122
                           004132  1878 _AX5043_AGCAHYST1	=	0x4132
                           004142  1879 _AX5043_AGCAHYST2	=	0x4142
                           004152  1880 _AX5043_AGCAHYST3	=	0x4152
                           004120  1881 _AX5043_AGCGAIN0	=	0x4120
                           004130  1882 _AX5043_AGCGAIN1	=	0x4130
                           004140  1883 _AX5043_AGCGAIN2	=	0x4140
                           004150  1884 _AX5043_AGCGAIN3	=	0x4150
                           004123  1885 _AX5043_AGCMINMAX0	=	0x4123
                           004133  1886 _AX5043_AGCMINMAX1	=	0x4133
                           004143  1887 _AX5043_AGCMINMAX2	=	0x4143
                           004153  1888 _AX5043_AGCMINMAX3	=	0x4153
                           004121  1889 _AX5043_AGCTARGET0	=	0x4121
                           004131  1890 _AX5043_AGCTARGET1	=	0x4131
                           004141  1891 _AX5043_AGCTARGET2	=	0x4141
                           004151  1892 _AX5043_AGCTARGET3	=	0x4151
                           00412B  1893 _AX5043_AMPLITUDEGAIN0	=	0x412b
                           00413B  1894 _AX5043_AMPLITUDEGAIN1	=	0x413b
                           00414B  1895 _AX5043_AMPLITUDEGAIN2	=	0x414b
                           00415B  1896 _AX5043_AMPLITUDEGAIN3	=	0x415b
                           00412F  1897 _AX5043_BBOFFSRES0	=	0x412f
                           00413F  1898 _AX5043_BBOFFSRES1	=	0x413f
                           00414F  1899 _AX5043_BBOFFSRES2	=	0x414f
                           00415F  1900 _AX5043_BBOFFSRES3	=	0x415f
                           004125  1901 _AX5043_DRGAIN0	=	0x4125
                           004135  1902 _AX5043_DRGAIN1	=	0x4135
                           004145  1903 _AX5043_DRGAIN2	=	0x4145
                           004155  1904 _AX5043_DRGAIN3	=	0x4155
                           00412E  1905 _AX5043_FOURFSK0	=	0x412e
                           00413E  1906 _AX5043_FOURFSK1	=	0x413e
                           00414E  1907 _AX5043_FOURFSK2	=	0x414e
                           00415E  1908 _AX5043_FOURFSK3	=	0x415e
                           00412D  1909 _AX5043_FREQDEV00	=	0x412d
                           00413D  1910 _AX5043_FREQDEV01	=	0x413d
                           00414D  1911 _AX5043_FREQDEV02	=	0x414d
                           00415D  1912 _AX5043_FREQDEV03	=	0x415d
                           00412C  1913 _AX5043_FREQDEV10	=	0x412c
                           00413C  1914 _AX5043_FREQDEV11	=	0x413c
                           00414C  1915 _AX5043_FREQDEV12	=	0x414c
                           00415C  1916 _AX5043_FREQDEV13	=	0x415c
                           004127  1917 _AX5043_FREQUENCYGAINA0	=	0x4127
                           004137  1918 _AX5043_FREQUENCYGAINA1	=	0x4137
                           004147  1919 _AX5043_FREQUENCYGAINA2	=	0x4147
                           004157  1920 _AX5043_FREQUENCYGAINA3	=	0x4157
                           004128  1921 _AX5043_FREQUENCYGAINB0	=	0x4128
                           004138  1922 _AX5043_FREQUENCYGAINB1	=	0x4138
                           004148  1923 _AX5043_FREQUENCYGAINB2	=	0x4148
                           004158  1924 _AX5043_FREQUENCYGAINB3	=	0x4158
                           004129  1925 _AX5043_FREQUENCYGAINC0	=	0x4129
                           004139  1926 _AX5043_FREQUENCYGAINC1	=	0x4139
                           004149  1927 _AX5043_FREQUENCYGAINC2	=	0x4149
                           004159  1928 _AX5043_FREQUENCYGAINC3	=	0x4159
                           00412A  1929 _AX5043_FREQUENCYGAIND0	=	0x412a
                           00413A  1930 _AX5043_FREQUENCYGAIND1	=	0x413a
                           00414A  1931 _AX5043_FREQUENCYGAIND2	=	0x414a
                           00415A  1932 _AX5043_FREQUENCYGAIND3	=	0x415a
                           004116  1933 _AX5043_FREQUENCYLEAK	=	0x4116
                           004126  1934 _AX5043_PHASEGAIN0	=	0x4126
                           004136  1935 _AX5043_PHASEGAIN1	=	0x4136
                           004146  1936 _AX5043_PHASEGAIN2	=	0x4146
                           004156  1937 _AX5043_PHASEGAIN3	=	0x4156
                           004207  1938 _AX5043_PKTADDR0	=	0x4207
                           004206  1939 _AX5043_PKTADDR1	=	0x4206
                           004205  1940 _AX5043_PKTADDR2	=	0x4205
                           004204  1941 _AX5043_PKTADDR3	=	0x4204
                           004200  1942 _AX5043_PKTADDRCFG	=	0x4200
                           00420B  1943 _AX5043_PKTADDRMASK0	=	0x420b
                           00420A  1944 _AX5043_PKTADDRMASK1	=	0x420a
                           004209  1945 _AX5043_PKTADDRMASK2	=	0x4209
                           004208  1946 _AX5043_PKTADDRMASK3	=	0x4208
                           004201  1947 _AX5043_PKTLENCFG	=	0x4201
                           004202  1948 _AX5043_PKTLENOFFSET	=	0x4202
                           004203  1949 _AX5043_PKTMAXLEN	=	0x4203
                           004118  1950 _AX5043_RXPARAMCURSET	=	0x4118
                           004117  1951 _AX5043_RXPARAMSETS	=	0x4117
                           004124  1952 _AX5043_TIMEGAIN0	=	0x4124
                           004134  1953 _AX5043_TIMEGAIN1	=	0x4134
                           004144  1954 _AX5043_TIMEGAIN2	=	0x4144
                           004154  1955 _AX5043_TIMEGAIN3	=	0x4154
                           005114  1956 _AX5043_AFSKCTRLNB	=	0x5114
                           005113  1957 _AX5043_AFSKMARK0NB	=	0x5113
                           005112  1958 _AX5043_AFSKMARK1NB	=	0x5112
                           005111  1959 _AX5043_AFSKSPACE0NB	=	0x5111
                           005110  1960 _AX5043_AFSKSPACE1NB	=	0x5110
                           005043  1961 _AX5043_AGCCOUNTERNB	=	0x5043
                           005115  1962 _AX5043_AMPLFILTERNB	=	0x5115
                           005189  1963 _AX5043_BBOFFSCAPNB	=	0x5189
                           005188  1964 _AX5043_BBTUNENB	=	0x5188
                           005041  1965 _AX5043_BGNDRSSINB	=	0x5041
                           00522E  1966 _AX5043_BGNDRSSIGAINNB	=	0x522e
                           00522F  1967 _AX5043_BGNDRSSITHRNB	=	0x522f
                           005017  1968 _AX5043_CRCINIT0NB	=	0x5017
                           005016  1969 _AX5043_CRCINIT1NB	=	0x5016
                           005015  1970 _AX5043_CRCINIT2NB	=	0x5015
                           005014  1971 _AX5043_CRCINIT3NB	=	0x5014
                           005332  1972 _AX5043_DACCONFIGNB	=	0x5332
                           005331  1973 _AX5043_DACVALUE0NB	=	0x5331
                           005330  1974 _AX5043_DACVALUE1NB	=	0x5330
                           005102  1975 _AX5043_DECIMATIONNB	=	0x5102
                           005042  1976 _AX5043_DIVERSITYNB	=	0x5042
                           005011  1977 _AX5043_ENCODINGNB	=	0x5011
                           005018  1978 _AX5043_FECNB	=	0x5018
                           00501A  1979 _AX5043_FECSTATUSNB	=	0x501a
                           005019  1980 _AX5043_FECSYNCNB	=	0x5019
                           00502B  1981 _AX5043_FIFOCOUNT0NB	=	0x502b
                           00502A  1982 _AX5043_FIFOCOUNT1NB	=	0x502a
                           005029  1983 _AX5043_FIFODATANB	=	0x5029
                           00502D  1984 _AX5043_FIFOFREE0NB	=	0x502d
                           00502C  1985 _AX5043_FIFOFREE1NB	=	0x502c
                           005028  1986 _AX5043_FIFOSTATNB	=	0x5028
                           00502F  1987 _AX5043_FIFOTHRESH0NB	=	0x502f
                           00502E  1988 _AX5043_FIFOTHRESH1NB	=	0x502e
                           005012  1989 _AX5043_FRAMINGNB	=	0x5012
                           005037  1990 _AX5043_FREQA0NB	=	0x5037
                           005036  1991 _AX5043_FREQA1NB	=	0x5036
                           005035  1992 _AX5043_FREQA2NB	=	0x5035
                           005034  1993 _AX5043_FREQA3NB	=	0x5034
                           00503F  1994 _AX5043_FREQB0NB	=	0x503f
                           00503E  1995 _AX5043_FREQB1NB	=	0x503e
                           00503D  1996 _AX5043_FREQB2NB	=	0x503d
                           00503C  1997 _AX5043_FREQB3NB	=	0x503c
                           005163  1998 _AX5043_FSKDEV0NB	=	0x5163
                           005162  1999 _AX5043_FSKDEV1NB	=	0x5162
                           005161  2000 _AX5043_FSKDEV2NB	=	0x5161
                           00510D  2001 _AX5043_FSKDMAX0NB	=	0x510d
                           00510C  2002 _AX5043_FSKDMAX1NB	=	0x510c
                           00510F  2003 _AX5043_FSKDMIN0NB	=	0x510f
                           00510E  2004 _AX5043_FSKDMIN1NB	=	0x510e
                           005309  2005 _AX5043_GPADC13VALUE0NB	=	0x5309
                           005308  2006 _AX5043_GPADC13VALUE1NB	=	0x5308
                           005300  2007 _AX5043_GPADCCTRLNB	=	0x5300
                           005301  2008 _AX5043_GPADCPERIODNB	=	0x5301
                           005101  2009 _AX5043_IFFREQ0NB	=	0x5101
                           005100  2010 _AX5043_IFFREQ1NB	=	0x5100
                           00500B  2011 _AX5043_IRQINVERSION0NB	=	0x500b
                           00500A  2012 _AX5043_IRQINVERSION1NB	=	0x500a
                           005007  2013 _AX5043_IRQMASK0NB	=	0x5007
                           005006  2014 _AX5043_IRQMASK1NB	=	0x5006
                           00500D  2015 _AX5043_IRQREQUEST0NB	=	0x500d
                           00500C  2016 _AX5043_IRQREQUEST1NB	=	0x500c
                           005310  2017 _AX5043_LPOSCCONFIGNB	=	0x5310
                           005317  2018 _AX5043_LPOSCFREQ0NB	=	0x5317
                           005316  2019 _AX5043_LPOSCFREQ1NB	=	0x5316
                           005313  2020 _AX5043_LPOSCKFILT0NB	=	0x5313
                           005312  2021 _AX5043_LPOSCKFILT1NB	=	0x5312
                           005319  2022 _AX5043_LPOSCPER0NB	=	0x5319
                           005318  2023 _AX5043_LPOSCPER1NB	=	0x5318
                           005315  2024 _AX5043_LPOSCREF0NB	=	0x5315
                           005314  2025 _AX5043_LPOSCREF1NB	=	0x5314
                           005311  2026 _AX5043_LPOSCSTATUSNB	=	0x5311
                           005214  2027 _AX5043_MATCH0LENNB	=	0x5214
                           005216  2028 _AX5043_MATCH0MAXNB	=	0x5216
                           005215  2029 _AX5043_MATCH0MINNB	=	0x5215
                           005213  2030 _AX5043_MATCH0PAT0NB	=	0x5213
                           005212  2031 _AX5043_MATCH0PAT1NB	=	0x5212
                           005211  2032 _AX5043_MATCH0PAT2NB	=	0x5211
                           005210  2033 _AX5043_MATCH0PAT3NB	=	0x5210
                           00521C  2034 _AX5043_MATCH1LENNB	=	0x521c
                           00521E  2035 _AX5043_MATCH1MAXNB	=	0x521e
                           00521D  2036 _AX5043_MATCH1MINNB	=	0x521d
                           005219  2037 _AX5043_MATCH1PAT0NB	=	0x5219
                           005218  2038 _AX5043_MATCH1PAT1NB	=	0x5218
                           005108  2039 _AX5043_MAXDROFFSET0NB	=	0x5108
                           005107  2040 _AX5043_MAXDROFFSET1NB	=	0x5107
                           005106  2041 _AX5043_MAXDROFFSET2NB	=	0x5106
                           00510B  2042 _AX5043_MAXRFOFFSET0NB	=	0x510b
                           00510A  2043 _AX5043_MAXRFOFFSET1NB	=	0x510a
                           005109  2044 _AX5043_MAXRFOFFSET2NB	=	0x5109
                           005164  2045 _AX5043_MODCFGANB	=	0x5164
                           005160  2046 _AX5043_MODCFGFNB	=	0x5160
                           005F5F  2047 _AX5043_MODCFGPNB	=	0x5f5f
                           005010  2048 _AX5043_MODULATIONNB	=	0x5010
                           005025  2049 _AX5043_PINFUNCANTSELNB	=	0x5025
                           005023  2050 _AX5043_PINFUNCDATANB	=	0x5023
                           005022  2051 _AX5043_PINFUNCDCLKNB	=	0x5022
                           005024  2052 _AX5043_PINFUNCIRQNB	=	0x5024
                           005026  2053 _AX5043_PINFUNCPWRAMPNB	=	0x5026
                           005021  2054 _AX5043_PINFUNCSYSCLKNB	=	0x5021
                           005020  2055 _AX5043_PINSTATENB	=	0x5020
                           005233  2056 _AX5043_PKTACCEPTFLAGSNB	=	0x5233
                           005230  2057 _AX5043_PKTCHUNKSIZENB	=	0x5230
                           005231  2058 _AX5043_PKTMISCFLAGSNB	=	0x5231
                           005232  2059 _AX5043_PKTSTOREFLAGSNB	=	0x5232
                           005031  2060 _AX5043_PLLCPINB	=	0x5031
                           005039  2061 _AX5043_PLLCPIBOOSTNB	=	0x5039
                           005182  2062 _AX5043_PLLLOCKDETNB	=	0x5182
                           005030  2063 _AX5043_PLLLOOPNB	=	0x5030
                           005038  2064 _AX5043_PLLLOOPBOOSTNB	=	0x5038
                           005033  2065 _AX5043_PLLRANGINGANB	=	0x5033
                           00503B  2066 _AX5043_PLLRANGINGBNB	=	0x503b
                           005183  2067 _AX5043_PLLRNGCLKNB	=	0x5183
                           005032  2068 _AX5043_PLLVCODIVNB	=	0x5032
                           005180  2069 _AX5043_PLLVCOINB	=	0x5180
                           005181  2070 _AX5043_PLLVCOIRNB	=	0x5181
                           005F08  2071 _AX5043_POWCTRL1NB	=	0x5f08
                           005005  2072 _AX5043_POWIRQMASKNB	=	0x5005
                           005003  2073 _AX5043_POWSTATNB	=	0x5003
                           005004  2074 _AX5043_POWSTICKYSTATNB	=	0x5004
                           005027  2075 _AX5043_PWRAMPNB	=	0x5027
                           005002  2076 _AX5043_PWRMODENB	=	0x5002
                           005009  2077 _AX5043_RADIOEVENTMASK0NB	=	0x5009
                           005008  2078 _AX5043_RADIOEVENTMASK1NB	=	0x5008
                           00500F  2079 _AX5043_RADIOEVENTREQ0NB	=	0x500f
                           00500E  2080 _AX5043_RADIOEVENTREQ1NB	=	0x500e
                           00501C  2081 _AX5043_RADIOSTATENB	=	0x501c
                           005F0D  2082 _AX5043_REFNB	=	0x5f0d
                           005040  2083 _AX5043_RSSINB	=	0x5040
                           00522D  2084 _AX5043_RSSIABSTHRNB	=	0x522d
                           00522C  2085 _AX5043_RSSIREFERENCENB	=	0x522c
                           005105  2086 _AX5043_RXDATARATE0NB	=	0x5105
                           005104  2087 _AX5043_RXDATARATE1NB	=	0x5104
                           005103  2088 _AX5043_RXDATARATE2NB	=	0x5103
                           005001  2089 _AX5043_SCRATCHNB	=	0x5001
                           005000  2090 _AX5043_SILICONREVISIONNB	=	0x5000
                           00505B  2091 _AX5043_TIMER0NB	=	0x505b
                           00505A  2092 _AX5043_TIMER1NB	=	0x505a
                           005059  2093 _AX5043_TIMER2NB	=	0x5059
                           005227  2094 _AX5043_TMGRXAGCNB	=	0x5227
                           005223  2095 _AX5043_TMGRXBOOSTNB	=	0x5223
                           005226  2096 _AX5043_TMGRXCOARSEAGCNB	=	0x5226
                           005225  2097 _AX5043_TMGRXOFFSACQNB	=	0x5225
                           005229  2098 _AX5043_TMGRXPREAMBLE1NB	=	0x5229
                           00522A  2099 _AX5043_TMGRXPREAMBLE2NB	=	0x522a
                           00522B  2100 _AX5043_TMGRXPREAMBLE3NB	=	0x522b
                           005228  2101 _AX5043_TMGRXRSSINB	=	0x5228
                           005224  2102 _AX5043_TMGRXSETTLENB	=	0x5224
                           005220  2103 _AX5043_TMGTXBOOSTNB	=	0x5220
                           005221  2104 _AX5043_TMGTXSETTLENB	=	0x5221
                           005055  2105 _AX5043_TRKAFSKDEMOD0NB	=	0x5055
                           005054  2106 _AX5043_TRKAFSKDEMOD1NB	=	0x5054
                           005049  2107 _AX5043_TRKAMPLITUDE0NB	=	0x5049
                           005048  2108 _AX5043_TRKAMPLITUDE1NB	=	0x5048
                           005047  2109 _AX5043_TRKDATARATE0NB	=	0x5047
                           005046  2110 _AX5043_TRKDATARATE1NB	=	0x5046
                           005045  2111 _AX5043_TRKDATARATE2NB	=	0x5045
                           005051  2112 _AX5043_TRKFREQ0NB	=	0x5051
                           005050  2113 _AX5043_TRKFREQ1NB	=	0x5050
                           005053  2114 _AX5043_TRKFSKDEMOD0NB	=	0x5053
                           005052  2115 _AX5043_TRKFSKDEMOD1NB	=	0x5052
                           00504B  2116 _AX5043_TRKPHASE0NB	=	0x504b
                           00504A  2117 _AX5043_TRKPHASE1NB	=	0x504a
                           00504F  2118 _AX5043_TRKRFFREQ0NB	=	0x504f
                           00504E  2119 _AX5043_TRKRFFREQ1NB	=	0x504e
                           00504D  2120 _AX5043_TRKRFFREQ2NB	=	0x504d
                           005169  2121 _AX5043_TXPWRCOEFFA0NB	=	0x5169
                           005168  2122 _AX5043_TXPWRCOEFFA1NB	=	0x5168
                           00516B  2123 _AX5043_TXPWRCOEFFB0NB	=	0x516b
                           00516A  2124 _AX5043_TXPWRCOEFFB1NB	=	0x516a
                           00516D  2125 _AX5043_TXPWRCOEFFC0NB	=	0x516d
                           00516C  2126 _AX5043_TXPWRCOEFFC1NB	=	0x516c
                           00516F  2127 _AX5043_TXPWRCOEFFD0NB	=	0x516f
                           00516E  2128 _AX5043_TXPWRCOEFFD1NB	=	0x516e
                           005171  2129 _AX5043_TXPWRCOEFFE0NB	=	0x5171
                           005170  2130 _AX5043_TXPWRCOEFFE1NB	=	0x5170
                           005167  2131 _AX5043_TXRATE0NB	=	0x5167
                           005166  2132 _AX5043_TXRATE1NB	=	0x5166
                           005165  2133 _AX5043_TXRATE2NB	=	0x5165
                           00506B  2134 _AX5043_WAKEUP0NB	=	0x506b
                           00506A  2135 _AX5043_WAKEUP1NB	=	0x506a
                           00506D  2136 _AX5043_WAKEUPFREQ0NB	=	0x506d
                           00506C  2137 _AX5043_WAKEUPFREQ1NB	=	0x506c
                           005069  2138 _AX5043_WAKEUPTIMER0NB	=	0x5069
                           005068  2139 _AX5043_WAKEUPTIMER1NB	=	0x5068
                           00506E  2140 _AX5043_WAKEUPXOEARLYNB	=	0x506e
                           005F11  2141 _AX5043_XTALAMPLNB	=	0x5f11
                           005184  2142 _AX5043_XTALCAPNB	=	0x5184
                           005F10  2143 _AX5043_XTALOSCNB	=	0x5f10
                           00501D  2144 _AX5043_XTALSTATUSNB	=	0x501d
                           005F00  2145 _AX5043_0xF00NB	=	0x5f00
                           005F0C  2146 _AX5043_0xF0CNB	=	0x5f0c
                           005F18  2147 _AX5043_0xF18NB	=	0x5f18
                           005F1C  2148 _AX5043_0xF1CNB	=	0x5f1c
                           005F21  2149 _AX5043_0xF21NB	=	0x5f21
                           005F22  2150 _AX5043_0xF22NB	=	0x5f22
                           005F23  2151 _AX5043_0xF23NB	=	0x5f23
                           005F26  2152 _AX5043_0xF26NB	=	0x5f26
                           005F30  2153 _AX5043_0xF30NB	=	0x5f30
                           005F31  2154 _AX5043_0xF31NB	=	0x5f31
                           005F32  2155 _AX5043_0xF32NB	=	0x5f32
                           005F33  2156 _AX5043_0xF33NB	=	0x5f33
                           005F34  2157 _AX5043_0xF34NB	=	0x5f34
                           005F35  2158 _AX5043_0xF35NB	=	0x5f35
                           005F44  2159 _AX5043_0xF44NB	=	0x5f44
                           005122  2160 _AX5043_AGCAHYST0NB	=	0x5122
                           005132  2161 _AX5043_AGCAHYST1NB	=	0x5132
                           005142  2162 _AX5043_AGCAHYST2NB	=	0x5142
                           005152  2163 _AX5043_AGCAHYST3NB	=	0x5152
                           005120  2164 _AX5043_AGCGAIN0NB	=	0x5120
                           005130  2165 _AX5043_AGCGAIN1NB	=	0x5130
                           005140  2166 _AX5043_AGCGAIN2NB	=	0x5140
                           005150  2167 _AX5043_AGCGAIN3NB	=	0x5150
                           005123  2168 _AX5043_AGCMINMAX0NB	=	0x5123
                           005133  2169 _AX5043_AGCMINMAX1NB	=	0x5133
                           005143  2170 _AX5043_AGCMINMAX2NB	=	0x5143
                           005153  2171 _AX5043_AGCMINMAX3NB	=	0x5153
                           005121  2172 _AX5043_AGCTARGET0NB	=	0x5121
                           005131  2173 _AX5043_AGCTARGET1NB	=	0x5131
                           005141  2174 _AX5043_AGCTARGET2NB	=	0x5141
                           005151  2175 _AX5043_AGCTARGET3NB	=	0x5151
                           00512B  2176 _AX5043_AMPLITUDEGAIN0NB	=	0x512b
                           00513B  2177 _AX5043_AMPLITUDEGAIN1NB	=	0x513b
                           00514B  2178 _AX5043_AMPLITUDEGAIN2NB	=	0x514b
                           00515B  2179 _AX5043_AMPLITUDEGAIN3NB	=	0x515b
                           00512F  2180 _AX5043_BBOFFSRES0NB	=	0x512f
                           00513F  2181 _AX5043_BBOFFSRES1NB	=	0x513f
                           00514F  2182 _AX5043_BBOFFSRES2NB	=	0x514f
                           00515F  2183 _AX5043_BBOFFSRES3NB	=	0x515f
                           005125  2184 _AX5043_DRGAIN0NB	=	0x5125
                           005135  2185 _AX5043_DRGAIN1NB	=	0x5135
                           005145  2186 _AX5043_DRGAIN2NB	=	0x5145
                           005155  2187 _AX5043_DRGAIN3NB	=	0x5155
                           00512E  2188 _AX5043_FOURFSK0NB	=	0x512e
                           00513E  2189 _AX5043_FOURFSK1NB	=	0x513e
                           00514E  2190 _AX5043_FOURFSK2NB	=	0x514e
                           00515E  2191 _AX5043_FOURFSK3NB	=	0x515e
                           00512D  2192 _AX5043_FREQDEV00NB	=	0x512d
                           00513D  2193 _AX5043_FREQDEV01NB	=	0x513d
                           00514D  2194 _AX5043_FREQDEV02NB	=	0x514d
                           00515D  2195 _AX5043_FREQDEV03NB	=	0x515d
                           00512C  2196 _AX5043_FREQDEV10NB	=	0x512c
                           00513C  2197 _AX5043_FREQDEV11NB	=	0x513c
                           00514C  2198 _AX5043_FREQDEV12NB	=	0x514c
                           00515C  2199 _AX5043_FREQDEV13NB	=	0x515c
                           005127  2200 _AX5043_FREQUENCYGAINA0NB	=	0x5127
                           005137  2201 _AX5043_FREQUENCYGAINA1NB	=	0x5137
                           005147  2202 _AX5043_FREQUENCYGAINA2NB	=	0x5147
                           005157  2203 _AX5043_FREQUENCYGAINA3NB	=	0x5157
                           005128  2204 _AX5043_FREQUENCYGAINB0NB	=	0x5128
                           005138  2205 _AX5043_FREQUENCYGAINB1NB	=	0x5138
                           005148  2206 _AX5043_FREQUENCYGAINB2NB	=	0x5148
                           005158  2207 _AX5043_FREQUENCYGAINB3NB	=	0x5158
                           005129  2208 _AX5043_FREQUENCYGAINC0NB	=	0x5129
                           005139  2209 _AX5043_FREQUENCYGAINC1NB	=	0x5139
                           005149  2210 _AX5043_FREQUENCYGAINC2NB	=	0x5149
                           005159  2211 _AX5043_FREQUENCYGAINC3NB	=	0x5159
                           00512A  2212 _AX5043_FREQUENCYGAIND0NB	=	0x512a
                           00513A  2213 _AX5043_FREQUENCYGAIND1NB	=	0x513a
                           00514A  2214 _AX5043_FREQUENCYGAIND2NB	=	0x514a
                           00515A  2215 _AX5043_FREQUENCYGAIND3NB	=	0x515a
                           005116  2216 _AX5043_FREQUENCYLEAKNB	=	0x5116
                           005126  2217 _AX5043_PHASEGAIN0NB	=	0x5126
                           005136  2218 _AX5043_PHASEGAIN1NB	=	0x5136
                           005146  2219 _AX5043_PHASEGAIN2NB	=	0x5146
                           005156  2220 _AX5043_PHASEGAIN3NB	=	0x5156
                           005207  2221 _AX5043_PKTADDR0NB	=	0x5207
                           005206  2222 _AX5043_PKTADDR1NB	=	0x5206
                           005205  2223 _AX5043_PKTADDR2NB	=	0x5205
                           005204  2224 _AX5043_PKTADDR3NB	=	0x5204
                           005200  2225 _AX5043_PKTADDRCFGNB	=	0x5200
                           00520B  2226 _AX5043_PKTADDRMASK0NB	=	0x520b
                           00520A  2227 _AX5043_PKTADDRMASK1NB	=	0x520a
                           005209  2228 _AX5043_PKTADDRMASK2NB	=	0x5209
                           005208  2229 _AX5043_PKTADDRMASK3NB	=	0x5208
                           005201  2230 _AX5043_PKTLENCFGNB	=	0x5201
                           005202  2231 _AX5043_PKTLENOFFSETNB	=	0x5202
                           005203  2232 _AX5043_PKTMAXLENNB	=	0x5203
                           005118  2233 _AX5043_RXPARAMCURSETNB	=	0x5118
                           005117  2234 _AX5043_RXPARAMSETSNB	=	0x5117
                           005124  2235 _AX5043_TIMEGAIN0NB	=	0x5124
                           005134  2236 _AX5043_TIMEGAIN1NB	=	0x5134
                           005144  2237 _AX5043_TIMEGAIN2NB	=	0x5144
                           005154  2238 _AX5043_TIMEGAIN3NB	=	0x5154
      0003FC                       2239 _UBLOX_GPS_PortInit_UBLOX_setNav1_65536_198:
      0003FC                       2240 	.ds 3
      0003FF                       2241 _UBLOX_GPS_PortInit_UBLOX_setNav2_65536_198:
      0003FF                       2242 	.ds 8
      000407                       2243 _UBLOX_GPS_PortInit_UBLOX_setNav3_65536_198:
      000407                       2244 	.ds 8
      00040F                       2245 _UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198:
      00040F                       2246 	.ds 36
      000433                       2247 _UBLOX_GPS_FletcherChecksum8_PARM_2:
      000433                       2248 	.ds 3
      000436                       2249 _UBLOX_GPS_FletcherChecksum8_PARM_3:
      000436                       2250 	.ds 3
      000439                       2251 _UBLOX_GPS_FletcherChecksum8_PARM_4:
      000439                       2252 	.ds 2
      00043B                       2253 _UBLOX_GPS_FletcherChecksum8_buffer_65536_207:
      00043B                       2254 	.ds 3
      00043E                       2255 _UBLOX_GPS_SendCommand_WaitACK_PARM_2:
      00043E                       2256 	.ds 1
      00043F                       2257 _UBLOX_GPS_SendCommand_WaitACK_PARM_3:
      00043F                       2258 	.ds 2
      000441                       2259 _UBLOX_GPS_SendCommand_WaitACK_PARM_4:
      000441                       2260 	.ds 3
      000444                       2261 _UBLOX_GPS_SendCommand_WaitACK_PARM_5:
      000444                       2262 	.ds 1
      000445                       2263 _UBLOX_GPS_SendCommand_WaitACK_PARM_6:
      000445                       2264 	.ds 3
      000448                       2265 _UBLOX_GPS_SendCommand_WaitACK_msg_class_65536_211:
      000448                       2266 	.ds 1
      000449                       2267 _UBLOX_GPS_SendCommand_WaitACK_RetVal_65536_212:
      000449                       2268 	.ds 1
      00044A                       2269 _UBLOX_GPS_SendCommand_WaitACK_CK_A_65536_212:
      00044A                       2270 	.ds 1
      00044B                       2271 _UBLOX_GPS_SendCommand_WaitACK_CK_B_65536_212:
      00044B                       2272 	.ds 1
                                   2273 ;--------------------------------------------------------
                                   2274 ; absolute external ram data
                                   2275 ;--------------------------------------------------------
                                   2276 	.area XABS    (ABS,XDATA)
                                   2277 ;--------------------------------------------------------
                                   2278 ; external initialized ram data
                                   2279 ;--------------------------------------------------------
                                   2280 	.area XISEG   (XDATA)
                                   2281 	.area HOME    (CODE)
                                   2282 	.area GSINIT0 (CODE)
                                   2283 	.area GSINIT1 (CODE)
                                   2284 	.area GSINIT2 (CODE)
                                   2285 	.area GSINIT3 (CODE)
                                   2286 	.area GSINIT4 (CODE)
                                   2287 	.area GSINIT5 (CODE)
                                   2288 	.area GSINIT  (CODE)
                                   2289 	.area GSFINAL (CODE)
                                   2290 	.area CSEG    (CODE)
                                   2291 ;--------------------------------------------------------
                                   2292 ; global & static initialisations
                                   2293 ;--------------------------------------------------------
                                   2294 	.area HOME    (CODE)
                                   2295 	.area GSINIT  (CODE)
                                   2296 	.area GSFINAL (CODE)
                                   2297 	.area GSINIT  (CODE)
                                   2298 ;--------------------------------------------------------
                                   2299 ; Home
                                   2300 ;--------------------------------------------------------
                                   2301 	.area HOME    (CODE)
                                   2302 	.area HOME    (CODE)
                                   2303 ;--------------------------------------------------------
                                   2304 ; code
                                   2305 ;--------------------------------------------------------
                                   2306 	.area CSEG    (CODE)
                                   2307 ;------------------------------------------------------------
                                   2308 ;Allocation info for local variables in function 'UBLOX_GPS_PortInit'
                                   2309 ;------------------------------------------------------------
                                   2310 ;UBLOX_setNav1             Allocated with name '_UBLOX_GPS_PortInit_UBLOX_setNav1_65536_198'
                                   2311 ;UBLOX_setNav2             Allocated with name '_UBLOX_GPS_PortInit_UBLOX_setNav2_65536_198'
                                   2312 ;UBLOX_setNav3             Allocated with name '_UBLOX_GPS_PortInit_UBLOX_setNav3_65536_198'
                                   2313 ;UBLOX_setNav5             Allocated with name '_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198'
                                   2314 ;------------------------------------------------------------
                                   2315 ;	..\src\peripherals\UBLOX.c:28: void UBLOX_GPS_PortInit(void)
                                   2316 ;	-----------------------------------------
                                   2317 ;	 function UBLOX_GPS_PortInit
                                   2318 ;	-----------------------------------------
      00662B                       2319 _UBLOX_GPS_PortInit:
                           000007  2320 	ar7 = 0x07
                           000006  2321 	ar6 = 0x06
                           000005  2322 	ar5 = 0x05
                           000004  2323 	ar4 = 0x04
                           000003  2324 	ar3 = 0x03
                           000002  2325 	ar2 = 0x02
                           000001  2326 	ar1 = 0x01
                           000000  2327 	ar0 = 0x00
                                   2328 ;	..\src\peripherals\UBLOX.c:30: __xdata uint8_t  UBLOX_setNav1[]= {0xF0 ,0x04 ,0x00};/**< \brief Initial cofiguration for UBLOX GPS*/
      00662B 90 03 FC         [24] 2329 	mov	dptr,#_UBLOX_GPS_PortInit_UBLOX_setNav1_65536_198
      00662E 74 F0            [12] 2330 	mov	a,#0xf0
      006630 F0               [24] 2331 	movx	@dptr,a
      006631 90 03 FD         [24] 2332 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav1_65536_198 + 0x0001)
      006634 74 04            [12] 2333 	mov	a,#0x04
      006636 F0               [24] 2334 	movx	@dptr,a
      006637 90 03 FE         [24] 2335 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav1_65536_198 + 0x0002)
      00663A E4               [12] 2336 	clr	a
      00663B F0               [24] 2337 	movx	@dptr,a
                                   2338 ;	..\src\peripherals\UBLOX.c:31: __xdata uint8_t  UBLOX_setNav2[]= {0xF0, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01}; /**< \brief Initial cofiguration for UBLOX GPS*/
      00663C 90 03 FF         [24] 2339 	mov	dptr,#_UBLOX_GPS_PortInit_UBLOX_setNav2_65536_198
      00663F 74 F0            [12] 2340 	mov	a,#0xf0
      006641 F0               [24] 2341 	movx	@dptr,a
      006642 90 04 00         [24] 2342 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav2_65536_198 + 0x0001)
      006645 74 01            [12] 2343 	mov	a,#0x01
      006647 F0               [24] 2344 	movx	@dptr,a
      006648 90 04 01         [24] 2345 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav2_65536_198 + 0x0002)
      00664B E4               [12] 2346 	clr	a
      00664C F0               [24] 2347 	movx	@dptr,a
      00664D 90 04 02         [24] 2348 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav2_65536_198 + 0x0003)
      006650 F0               [24] 2349 	movx	@dptr,a
      006651 90 04 03         [24] 2350 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav2_65536_198 + 0x0004)
      006654 F0               [24] 2351 	movx	@dptr,a
      006655 90 04 04         [24] 2352 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav2_65536_198 + 0x0005)
      006658 F0               [24] 2353 	movx	@dptr,a
      006659 90 04 05         [24] 2354 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav2_65536_198 + 0x0006)
      00665C F0               [24] 2355 	movx	@dptr,a
      00665D 90 04 06         [24] 2356 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav2_65536_198 + 0x0007)
      006660 04               [12] 2357 	inc	a
      006661 F0               [24] 2358 	movx	@dptr,a
                                   2359 ;	..\src\peripherals\UBLOX.c:32: __xdata uint8_t  UBLOX_setNav3[] = {0xF0, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01};/**< \brief Initial cofiguration for UBLOX GPS*/
      006662 90 04 07         [24] 2360 	mov	dptr,#_UBLOX_GPS_PortInit_UBLOX_setNav3_65536_198
      006665 74 F0            [12] 2361 	mov	a,#0xf0
      006667 F0               [24] 2362 	movx	@dptr,a
      006668 90 04 08         [24] 2363 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav3_65536_198 + 0x0001)
      00666B 74 04            [12] 2364 	mov	a,#0x04
      00666D F0               [24] 2365 	movx	@dptr,a
      00666E 90 04 09         [24] 2366 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav3_65536_198 + 0x0002)
      006671 E4               [12] 2367 	clr	a
      006672 F0               [24] 2368 	movx	@dptr,a
      006673 90 04 0A         [24] 2369 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav3_65536_198 + 0x0003)
      006676 F0               [24] 2370 	movx	@dptr,a
      006677 90 04 0B         [24] 2371 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav3_65536_198 + 0x0004)
      00667A F0               [24] 2372 	movx	@dptr,a
      00667B 90 04 0C         [24] 2373 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav3_65536_198 + 0x0005)
      00667E F0               [24] 2374 	movx	@dptr,a
      00667F 90 04 0D         [24] 2375 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav3_65536_198 + 0x0006)
      006682 F0               [24] 2376 	movx	@dptr,a
      006683 90 04 0E         [24] 2377 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav3_65536_198 + 0x0007)
      006686 04               [12] 2378 	inc	a
      006687 F0               [24] 2379 	movx	@dptr,a
                                   2380 ;	..\src\peripherals\UBLOX.c:33: __xdata uint8_t  UBLOX_setNav5[] ={0xFF, 0xFF, 0x06, 0x03, 0x00, 0x00, 0x00, 0x00, 0x10, 0x27, 0x00, 0x00,
      006688 90 04 0F         [24] 2381 	mov	dptr,#_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198
      00668B 74 FF            [12] 2382 	mov	a,#0xff
      00668D F0               [24] 2383 	movx	@dptr,a
      00668E 90 04 10         [24] 2384 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198 + 0x0001)
      006691 F0               [24] 2385 	movx	@dptr,a
      006692 90 04 11         [24] 2386 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198 + 0x0002)
      006695 74 06            [12] 2387 	mov	a,#0x06
      006697 F0               [24] 2388 	movx	@dptr,a
      006698 90 04 12         [24] 2389 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198 + 0x0003)
      00669B 03               [12] 2390 	rr	a
      00669C F0               [24] 2391 	movx	@dptr,a
      00669D 90 04 13         [24] 2392 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198 + 0x0004)
      0066A0 E4               [12] 2393 	clr	a
      0066A1 F0               [24] 2394 	movx	@dptr,a
      0066A2 90 04 14         [24] 2395 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198 + 0x0005)
      0066A5 F0               [24] 2396 	movx	@dptr,a
      0066A6 90 04 15         [24] 2397 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198 + 0x0006)
      0066A9 F0               [24] 2398 	movx	@dptr,a
      0066AA 90 04 16         [24] 2399 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198 + 0x0007)
      0066AD F0               [24] 2400 	movx	@dptr,a
      0066AE 90 04 17         [24] 2401 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198 + 0x0008)
      0066B1 74 10            [12] 2402 	mov	a,#0x10
      0066B3 F0               [24] 2403 	movx	@dptr,a
      0066B4 90 04 18         [24] 2404 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198 + 0x0009)
      0066B7 74 27            [12] 2405 	mov	a,#0x27
      0066B9 F0               [24] 2406 	movx	@dptr,a
      0066BA 90 04 19         [24] 2407 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198 + 0x000a)
      0066BD E4               [12] 2408 	clr	a
      0066BE F0               [24] 2409 	movx	@dptr,a
      0066BF 90 04 1A         [24] 2410 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198 + 0x000b)
      0066C2 F0               [24] 2411 	movx	@dptr,a
      0066C3 90 04 1B         [24] 2412 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198 + 0x000c)
      0066C6 74 05            [12] 2413 	mov	a,#0x05
      0066C8 F0               [24] 2414 	movx	@dptr,a
      0066C9 90 04 1C         [24] 2415 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198 + 0x000d)
      0066CC E4               [12] 2416 	clr	a
      0066CD F0               [24] 2417 	movx	@dptr,a
      0066CE 90 04 1D         [24] 2418 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198 + 0x000e)
      0066D1 74 FA            [12] 2419 	mov	a,#0xfa
      0066D3 F0               [24] 2420 	movx	@dptr,a
      0066D4 90 04 1E         [24] 2421 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198 + 0x000f)
      0066D7 E4               [12] 2422 	clr	a
      0066D8 F0               [24] 2423 	movx	@dptr,a
      0066D9 90 04 1F         [24] 2424 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198 + 0x0010)
      0066DC 74 FA            [12] 2425 	mov	a,#0xfa
      0066DE F0               [24] 2426 	movx	@dptr,a
      0066DF 90 04 20         [24] 2427 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198 + 0x0011)
      0066E2 E4               [12] 2428 	clr	a
      0066E3 F0               [24] 2429 	movx	@dptr,a
      0066E4 90 04 21         [24] 2430 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198 + 0x0012)
      0066E7 74 64            [12] 2431 	mov	a,#0x64
      0066E9 F0               [24] 2432 	movx	@dptr,a
      0066EA 90 04 22         [24] 2433 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198 + 0x0013)
      0066ED E4               [12] 2434 	clr	a
      0066EE F0               [24] 2435 	movx	@dptr,a
      0066EF 90 04 23         [24] 2436 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198 + 0x0014)
      0066F2 74 2C            [12] 2437 	mov	a,#0x2c
      0066F4 F0               [24] 2438 	movx	@dptr,a
      0066F5 90 04 24         [24] 2439 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198 + 0x0015)
      0066F8 74 01            [12] 2440 	mov	a,#0x01
      0066FA F0               [24] 2441 	movx	@dptr,a
      0066FB 90 04 25         [24] 2442 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198 + 0x0016)
      0066FE E4               [12] 2443 	clr	a
      0066FF F0               [24] 2444 	movx	@dptr,a
      006700 90 04 26         [24] 2445 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198 + 0x0017)
      006703 F0               [24] 2446 	movx	@dptr,a
      006704 90 04 27         [24] 2447 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198 + 0x0018)
      006707 F0               [24] 2448 	movx	@dptr,a
      006708 90 04 28         [24] 2449 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198 + 0x0019)
      00670B F0               [24] 2450 	movx	@dptr,a
      00670C 90 04 29         [24] 2451 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198 + 0x001a)
      00670F F0               [24] 2452 	movx	@dptr,a
      006710 90 04 2A         [24] 2453 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198 + 0x001b)
      006713 F0               [24] 2454 	movx	@dptr,a
      006714 90 04 2B         [24] 2455 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198 + 0x001c)
      006717 F0               [24] 2456 	movx	@dptr,a
      006718 90 04 2C         [24] 2457 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198 + 0x001d)
      00671B F0               [24] 2458 	movx	@dptr,a
      00671C 90 04 2D         [24] 2459 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198 + 0x001e)
      00671F F0               [24] 2460 	movx	@dptr,a
      006720 90 04 2E         [24] 2461 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198 + 0x001f)
      006723 F0               [24] 2462 	movx	@dptr,a
      006724 90 04 2F         [24] 2463 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198 + 0x0020)
      006727 F0               [24] 2464 	movx	@dptr,a
      006728 90 04 30         [24] 2465 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198 + 0x0021)
      00672B F0               [24] 2466 	movx	@dptr,a
      00672C 90 04 31         [24] 2467 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198 + 0x0022)
      00672F F0               [24] 2468 	movx	@dptr,a
      006730 90 04 32         [24] 2469 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198 + 0x0023)
      006733 F0               [24] 2470 	movx	@dptr,a
                                   2471 ;	..\src\peripherals\UBLOX.c:36: uart_timer1_baud(CLKSRC_FRCOSC, 9600, 20000000UL);
      006734 C0 E0            [24] 2472 	push	acc
      006736 74 2D            [12] 2473 	mov	a,#0x2d
      006738 C0 E0            [24] 2474 	push	acc
      00673A 74 31            [12] 2475 	mov	a,#0x31
      00673C C0 E0            [24] 2476 	push	acc
      00673E 74 01            [12] 2477 	mov	a,#0x01
      006740 C0 E0            [24] 2478 	push	acc
      006742 03               [12] 2479 	rr	a
      006743 C0 E0            [24] 2480 	push	acc
      006745 74 25            [12] 2481 	mov	a,#0x25
      006747 C0 E0            [24] 2482 	push	acc
      006749 E4               [12] 2483 	clr	a
      00674A C0 E0            [24] 2484 	push	acc
      00674C C0 E0            [24] 2485 	push	acc
      00674E 75 82 00         [24] 2486 	mov	dpl,#0x00
      006751 12 73 23         [24] 2487 	lcall	_uart_timer1_baud
      006754 E5 81            [12] 2488 	mov	a,sp
      006756 24 F8            [12] 2489 	add	a,#0xf8
      006758 F5 81            [12] 2490 	mov	sp,a
                                   2491 ;	..\src\peripherals\UBLOX.c:37: uart1_init(1, 8, 1);
      00675A 90 04 4C         [24] 2492 	mov	dptr,#_uart1_init_PARM_2
      00675D 74 08            [12] 2493 	mov	a,#0x08
      00675F F0               [24] 2494 	movx	@dptr,a
      006760 90 04 4D         [24] 2495 	mov	dptr,#_uart1_init_PARM_3
      006763 74 01            [12] 2496 	mov	a,#0x01
      006765 F0               [24] 2497 	movx	@dptr,a
      006766 75 82 01         [24] 2498 	mov	dpl,#0x01
      006769 12 6D F0         [24] 2499 	lcall	_uart1_init
                                   2500 ;	..\src\peripherals\UBLOX.c:40: dbglink_writestr(" GPS init start ");
      00676C 90 93 0E         [24] 2501 	mov	dptr,#___str_0
      00676F 75 F0 80         [24] 2502 	mov	b,#0x80
      006772 12 82 8A         [24] 2503 	lcall	_dbglink_writestr
                                   2504 ;	..\src\peripherals\UBLOX.c:42: UBLOX_setNav1[1]=0x02;
      006775 90 03 FD         [24] 2505 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav1_65536_198 + 0x0001)
      006778 74 02            [12] 2506 	mov	a,#0x02
      00677A F0               [24] 2507 	movx	@dptr,a
                                   2508 ;	..\src\peripherals\UBLOX.c:43: do{
      00677B                       2509 00101$:
                                   2510 ;	..\src\peripherals\UBLOX.c:44: delay(5000);
      00677B 90 13 88         [24] 2511 	mov	dptr,#0x1388
      00677E 12 87 20         [24] 2512 	lcall	_delay
                                   2513 ;	..\src\peripherals\UBLOX.c:45: }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x03,UBLOX_setNav1,ACK,NULL));
      006781 90 04 3E         [24] 2514 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_2
      006784 74 01            [12] 2515 	mov	a,#0x01
      006786 F0               [24] 2516 	movx	@dptr,a
      006787 90 04 3F         [24] 2517 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_3
      00678A 74 03            [12] 2518 	mov	a,#0x03
      00678C F0               [24] 2519 	movx	@dptr,a
      00678D E4               [12] 2520 	clr	a
      00678E A3               [24] 2521 	inc	dptr
      00678F F0               [24] 2522 	movx	@dptr,a
      006790 90 04 41         [24] 2523 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_4
      006793 74 FC            [12] 2524 	mov	a,#_UBLOX_GPS_PortInit_UBLOX_setNav1_65536_198
      006795 F0               [24] 2525 	movx	@dptr,a
      006796 74 03            [12] 2526 	mov	a,#(_UBLOX_GPS_PortInit_UBLOX_setNav1_65536_198 >> 8)
      006798 A3               [24] 2527 	inc	dptr
      006799 F0               [24] 2528 	movx	@dptr,a
      00679A E4               [12] 2529 	clr	a
      00679B A3               [24] 2530 	inc	dptr
      00679C F0               [24] 2531 	movx	@dptr,a
      00679D 90 04 44         [24] 2532 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_5
      0067A0 04               [12] 2533 	inc	a
      0067A1 F0               [24] 2534 	movx	@dptr,a
      0067A2 90 04 45         [24] 2535 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_6
      0067A5 E4               [12] 2536 	clr	a
      0067A6 F0               [24] 2537 	movx	@dptr,a
      0067A7 A3               [24] 2538 	inc	dptr
      0067A8 F0               [24] 2539 	movx	@dptr,a
      0067A9 A3               [24] 2540 	inc	dptr
      0067AA F0               [24] 2541 	movx	@dptr,a
      0067AB 75 82 06         [24] 2542 	mov	dpl,#0x06
      0067AE 12 6A 1F         [24] 2543 	lcall	_UBLOX_GPS_SendCommand_WaitACK
      0067B1 E5 82            [12] 2544 	mov	a,dpl
      0067B3 60 C6            [24] 2545 	jz	00101$
                                   2546 ;	..\src\peripherals\UBLOX.c:46: UBLOX_setNav1[1]=0x01;
      0067B5 90 03 FD         [24] 2547 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav1_65536_198 + 0x0001)
      0067B8 74 01            [12] 2548 	mov	a,#0x01
      0067BA F0               [24] 2549 	movx	@dptr,a
                                   2550 ;	..\src\peripherals\UBLOX.c:47: do{
      0067BB                       2551 00104$:
                                   2552 ;	..\src\peripherals\UBLOX.c:48: delay(5000);
      0067BB 90 13 88         [24] 2553 	mov	dptr,#0x1388
      0067BE 12 87 20         [24] 2554 	lcall	_delay
                                   2555 ;	..\src\peripherals\UBLOX.c:49: }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x03,UBLOX_setNav1,ACK,NULL));
      0067C1 90 04 3E         [24] 2556 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_2
      0067C4 74 01            [12] 2557 	mov	a,#0x01
      0067C6 F0               [24] 2558 	movx	@dptr,a
      0067C7 90 04 3F         [24] 2559 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_3
      0067CA 74 03            [12] 2560 	mov	a,#0x03
      0067CC F0               [24] 2561 	movx	@dptr,a
      0067CD E4               [12] 2562 	clr	a
      0067CE A3               [24] 2563 	inc	dptr
      0067CF F0               [24] 2564 	movx	@dptr,a
      0067D0 90 04 41         [24] 2565 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_4
      0067D3 74 FC            [12] 2566 	mov	a,#_UBLOX_GPS_PortInit_UBLOX_setNav1_65536_198
      0067D5 F0               [24] 2567 	movx	@dptr,a
      0067D6 74 03            [12] 2568 	mov	a,#(_UBLOX_GPS_PortInit_UBLOX_setNav1_65536_198 >> 8)
      0067D8 A3               [24] 2569 	inc	dptr
      0067D9 F0               [24] 2570 	movx	@dptr,a
      0067DA E4               [12] 2571 	clr	a
      0067DB A3               [24] 2572 	inc	dptr
      0067DC F0               [24] 2573 	movx	@dptr,a
      0067DD 90 04 44         [24] 2574 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_5
      0067E0 04               [12] 2575 	inc	a
      0067E1 F0               [24] 2576 	movx	@dptr,a
      0067E2 90 04 45         [24] 2577 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_6
      0067E5 E4               [12] 2578 	clr	a
      0067E6 F0               [24] 2579 	movx	@dptr,a
      0067E7 A3               [24] 2580 	inc	dptr
      0067E8 F0               [24] 2581 	movx	@dptr,a
      0067E9 A3               [24] 2582 	inc	dptr
      0067EA F0               [24] 2583 	movx	@dptr,a
      0067EB 75 82 06         [24] 2584 	mov	dpl,#0x06
      0067EE 12 6A 1F         [24] 2585 	lcall	_UBLOX_GPS_SendCommand_WaitACK
      0067F1 E5 82            [12] 2586 	mov	a,dpl
      0067F3 60 C6            [24] 2587 	jz	00104$
                                   2588 ;	..\src\peripherals\UBLOX.c:50: UBLOX_setNav1[1]=0x00;
      0067F5 90 03 FD         [24] 2589 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav1_65536_198 + 0x0001)
      0067F8 E4               [12] 2590 	clr	a
      0067F9 F0               [24] 2591 	movx	@dptr,a
                                   2592 ;	..\src\peripherals\UBLOX.c:51: do{
      0067FA                       2593 00107$:
                                   2594 ;	..\src\peripherals\UBLOX.c:52: delay(5000);
      0067FA 90 13 88         [24] 2595 	mov	dptr,#0x1388
      0067FD 12 87 20         [24] 2596 	lcall	_delay
                                   2597 ;	..\src\peripherals\UBLOX.c:53: }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x03,UBLOX_setNav1,ACK,NULL));
      006800 90 04 3E         [24] 2598 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_2
      006803 74 01            [12] 2599 	mov	a,#0x01
      006805 F0               [24] 2600 	movx	@dptr,a
      006806 90 04 3F         [24] 2601 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_3
      006809 74 03            [12] 2602 	mov	a,#0x03
      00680B F0               [24] 2603 	movx	@dptr,a
      00680C E4               [12] 2604 	clr	a
      00680D A3               [24] 2605 	inc	dptr
      00680E F0               [24] 2606 	movx	@dptr,a
      00680F 90 04 41         [24] 2607 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_4
      006812 74 FC            [12] 2608 	mov	a,#_UBLOX_GPS_PortInit_UBLOX_setNav1_65536_198
      006814 F0               [24] 2609 	movx	@dptr,a
      006815 74 03            [12] 2610 	mov	a,#(_UBLOX_GPS_PortInit_UBLOX_setNav1_65536_198 >> 8)
      006817 A3               [24] 2611 	inc	dptr
      006818 F0               [24] 2612 	movx	@dptr,a
      006819 E4               [12] 2613 	clr	a
      00681A A3               [24] 2614 	inc	dptr
      00681B F0               [24] 2615 	movx	@dptr,a
      00681C 90 04 44         [24] 2616 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_5
      00681F 04               [12] 2617 	inc	a
      006820 F0               [24] 2618 	movx	@dptr,a
      006821 90 04 45         [24] 2619 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_6
      006824 E4               [12] 2620 	clr	a
      006825 F0               [24] 2621 	movx	@dptr,a
      006826 A3               [24] 2622 	inc	dptr
      006827 F0               [24] 2623 	movx	@dptr,a
      006828 A3               [24] 2624 	inc	dptr
      006829 F0               [24] 2625 	movx	@dptr,a
      00682A 75 82 06         [24] 2626 	mov	dpl,#0x06
      00682D 12 6A 1F         [24] 2627 	lcall	_UBLOX_GPS_SendCommand_WaitACK
      006830 E5 82            [12] 2628 	mov	a,dpl
      006832 60 C6            [24] 2629 	jz	00107$
                                   2630 ;	..\src\peripherals\UBLOX.c:54: UBLOX_setNav1[1]=0x03;
      006834 90 03 FD         [24] 2631 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav1_65536_198 + 0x0001)
      006837 74 03            [12] 2632 	mov	a,#0x03
      006839 F0               [24] 2633 	movx	@dptr,a
                                   2634 ;	..\src\peripherals\UBLOX.c:55: do{
      00683A                       2635 00110$:
                                   2636 ;	..\src\peripherals\UBLOX.c:56: delay(5000);
      00683A 90 13 88         [24] 2637 	mov	dptr,#0x1388
      00683D 12 87 20         [24] 2638 	lcall	_delay
                                   2639 ;	..\src\peripherals\UBLOX.c:57: }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x03,UBLOX_setNav1,ACK,NULL));
      006840 90 04 3E         [24] 2640 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_2
      006843 74 01            [12] 2641 	mov	a,#0x01
      006845 F0               [24] 2642 	movx	@dptr,a
      006846 90 04 3F         [24] 2643 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_3
      006849 74 03            [12] 2644 	mov	a,#0x03
      00684B F0               [24] 2645 	movx	@dptr,a
      00684C E4               [12] 2646 	clr	a
      00684D A3               [24] 2647 	inc	dptr
      00684E F0               [24] 2648 	movx	@dptr,a
      00684F 90 04 41         [24] 2649 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_4
      006852 74 FC            [12] 2650 	mov	a,#_UBLOX_GPS_PortInit_UBLOX_setNav1_65536_198
      006854 F0               [24] 2651 	movx	@dptr,a
      006855 74 03            [12] 2652 	mov	a,#(_UBLOX_GPS_PortInit_UBLOX_setNav1_65536_198 >> 8)
      006857 A3               [24] 2653 	inc	dptr
      006858 F0               [24] 2654 	movx	@dptr,a
      006859 E4               [12] 2655 	clr	a
      00685A A3               [24] 2656 	inc	dptr
      00685B F0               [24] 2657 	movx	@dptr,a
      00685C 90 04 44         [24] 2658 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_5
      00685F 04               [12] 2659 	inc	a
      006860 F0               [24] 2660 	movx	@dptr,a
      006861 90 04 45         [24] 2661 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_6
      006864 E4               [12] 2662 	clr	a
      006865 F0               [24] 2663 	movx	@dptr,a
      006866 A3               [24] 2664 	inc	dptr
      006867 F0               [24] 2665 	movx	@dptr,a
      006868 A3               [24] 2666 	inc	dptr
      006869 F0               [24] 2667 	movx	@dptr,a
      00686A 75 82 06         [24] 2668 	mov	dpl,#0x06
      00686D 12 6A 1F         [24] 2669 	lcall	_UBLOX_GPS_SendCommand_WaitACK
      006870 E5 82            [12] 2670 	mov	a,dpl
      006872 60 C6            [24] 2671 	jz	00110$
                                   2672 ;	..\src\peripherals\UBLOX.c:58: UBLOX_setNav1[1]=0x05;
      006874 90 03 FD         [24] 2673 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav1_65536_198 + 0x0001)
      006877 74 05            [12] 2674 	mov	a,#0x05
      006879 F0               [24] 2675 	movx	@dptr,a
                                   2676 ;	..\src\peripherals\UBLOX.c:59: do{
      00687A                       2677 00113$:
                                   2678 ;	..\src\peripherals\UBLOX.c:60: delay(5000);
      00687A 90 13 88         [24] 2679 	mov	dptr,#0x1388
      00687D 12 87 20         [24] 2680 	lcall	_delay
                                   2681 ;	..\src\peripherals\UBLOX.c:61: }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x03,UBLOX_setNav1,ACK,NULL));
      006880 90 04 3E         [24] 2682 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_2
      006883 74 01            [12] 2683 	mov	a,#0x01
      006885 F0               [24] 2684 	movx	@dptr,a
      006886 90 04 3F         [24] 2685 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_3
      006889 74 03            [12] 2686 	mov	a,#0x03
      00688B F0               [24] 2687 	movx	@dptr,a
      00688C E4               [12] 2688 	clr	a
      00688D A3               [24] 2689 	inc	dptr
      00688E F0               [24] 2690 	movx	@dptr,a
      00688F 90 04 41         [24] 2691 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_4
      006892 74 FC            [12] 2692 	mov	a,#_UBLOX_GPS_PortInit_UBLOX_setNav1_65536_198
      006894 F0               [24] 2693 	movx	@dptr,a
      006895 74 03            [12] 2694 	mov	a,#(_UBLOX_GPS_PortInit_UBLOX_setNav1_65536_198 >> 8)
      006897 A3               [24] 2695 	inc	dptr
      006898 F0               [24] 2696 	movx	@dptr,a
      006899 E4               [12] 2697 	clr	a
      00689A A3               [24] 2698 	inc	dptr
      00689B F0               [24] 2699 	movx	@dptr,a
      00689C 90 04 44         [24] 2700 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_5
      00689F 04               [12] 2701 	inc	a
      0068A0 F0               [24] 2702 	movx	@dptr,a
      0068A1 90 04 45         [24] 2703 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_6
      0068A4 E4               [12] 2704 	clr	a
      0068A5 F0               [24] 2705 	movx	@dptr,a
      0068A6 A3               [24] 2706 	inc	dptr
      0068A7 F0               [24] 2707 	movx	@dptr,a
      0068A8 A3               [24] 2708 	inc	dptr
      0068A9 F0               [24] 2709 	movx	@dptr,a
      0068AA 75 82 06         [24] 2710 	mov	dpl,#0x06
      0068AD 12 6A 1F         [24] 2711 	lcall	_UBLOX_GPS_SendCommand_WaitACK
      0068B0 E5 82            [12] 2712 	mov	a,dpl
      0068B2 60 C6            [24] 2713 	jz	00113$
                                   2714 ;	..\src\peripherals\UBLOX.c:62: do{
      0068B4                       2715 00116$:
                                   2716 ;	..\src\peripherals\UBLOX.c:63: delay(5000);
      0068B4 90 13 88         [24] 2717 	mov	dptr,#0x1388
      0068B7 12 87 20         [24] 2718 	lcall	_delay
                                   2719 ;	..\src\peripherals\UBLOX.c:64: }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x08,UBLOX_setNav2,ACK,NULL));
      0068BA 90 04 3E         [24] 2720 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_2
      0068BD 74 01            [12] 2721 	mov	a,#0x01
      0068BF F0               [24] 2722 	movx	@dptr,a
      0068C0 90 04 3F         [24] 2723 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_3
      0068C3 74 08            [12] 2724 	mov	a,#0x08
      0068C5 F0               [24] 2725 	movx	@dptr,a
      0068C6 E4               [12] 2726 	clr	a
      0068C7 A3               [24] 2727 	inc	dptr
      0068C8 F0               [24] 2728 	movx	@dptr,a
      0068C9 90 04 41         [24] 2729 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_4
      0068CC 74 FF            [12] 2730 	mov	a,#_UBLOX_GPS_PortInit_UBLOX_setNav2_65536_198
      0068CE F0               [24] 2731 	movx	@dptr,a
      0068CF 74 03            [12] 2732 	mov	a,#(_UBLOX_GPS_PortInit_UBLOX_setNav2_65536_198 >> 8)
      0068D1 A3               [24] 2733 	inc	dptr
      0068D2 F0               [24] 2734 	movx	@dptr,a
      0068D3 E4               [12] 2735 	clr	a
      0068D4 A3               [24] 2736 	inc	dptr
      0068D5 F0               [24] 2737 	movx	@dptr,a
      0068D6 90 04 44         [24] 2738 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_5
      0068D9 04               [12] 2739 	inc	a
      0068DA F0               [24] 2740 	movx	@dptr,a
      0068DB 90 04 45         [24] 2741 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_6
      0068DE E4               [12] 2742 	clr	a
      0068DF F0               [24] 2743 	movx	@dptr,a
      0068E0 A3               [24] 2744 	inc	dptr
      0068E1 F0               [24] 2745 	movx	@dptr,a
      0068E2 A3               [24] 2746 	inc	dptr
      0068E3 F0               [24] 2747 	movx	@dptr,a
      0068E4 75 82 06         [24] 2748 	mov	dpl,#0x06
      0068E7 12 6A 1F         [24] 2749 	lcall	_UBLOX_GPS_SendCommand_WaitACK
      0068EA E5 82            [12] 2750 	mov	a,dpl
      0068EC 60 C6            [24] 2751 	jz	00116$
                                   2752 ;	..\src\peripherals\UBLOX.c:65: do{
      0068EE                       2753 00119$:
                                   2754 ;	..\src\peripherals\UBLOX.c:66: delay(5000);
      0068EE 90 13 88         [24] 2755 	mov	dptr,#0x1388
      0068F1 12 87 20         [24] 2756 	lcall	_delay
                                   2757 ;	..\src\peripherals\UBLOX.c:67: }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x08,UBLOX_setNav3,ACK,NULL));
      0068F4 90 04 3E         [24] 2758 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_2
      0068F7 74 01            [12] 2759 	mov	a,#0x01
      0068F9 F0               [24] 2760 	movx	@dptr,a
      0068FA 90 04 3F         [24] 2761 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_3
      0068FD 74 08            [12] 2762 	mov	a,#0x08
      0068FF F0               [24] 2763 	movx	@dptr,a
      006900 E4               [12] 2764 	clr	a
      006901 A3               [24] 2765 	inc	dptr
      006902 F0               [24] 2766 	movx	@dptr,a
      006903 90 04 41         [24] 2767 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_4
      006906 74 07            [12] 2768 	mov	a,#_UBLOX_GPS_PortInit_UBLOX_setNav3_65536_198
      006908 F0               [24] 2769 	movx	@dptr,a
      006909 74 04            [12] 2770 	mov	a,#(_UBLOX_GPS_PortInit_UBLOX_setNav3_65536_198 >> 8)
      00690B A3               [24] 2771 	inc	dptr
      00690C F0               [24] 2772 	movx	@dptr,a
      00690D E4               [12] 2773 	clr	a
      00690E A3               [24] 2774 	inc	dptr
      00690F F0               [24] 2775 	movx	@dptr,a
      006910 90 04 44         [24] 2776 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_5
      006913 04               [12] 2777 	inc	a
      006914 F0               [24] 2778 	movx	@dptr,a
      006915 90 04 45         [24] 2779 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_6
      006918 E4               [12] 2780 	clr	a
      006919 F0               [24] 2781 	movx	@dptr,a
      00691A A3               [24] 2782 	inc	dptr
      00691B F0               [24] 2783 	movx	@dptr,a
      00691C A3               [24] 2784 	inc	dptr
      00691D F0               [24] 2785 	movx	@dptr,a
      00691E 75 82 06         [24] 2786 	mov	dpl,#0x06
      006921 12 6A 1F         [24] 2787 	lcall	_UBLOX_GPS_SendCommand_WaitACK
      006924 E5 82            [12] 2788 	mov	a,dpl
      006926 60 C6            [24] 2789 	jz	00119$
                                   2790 ;	..\src\peripherals\UBLOX.c:68: do{
      006928                       2791 00122$:
                                   2792 ;	..\src\peripherals\UBLOX.c:69: delay(5000);
      006928 90 13 88         [24] 2793 	mov	dptr,#0x1388
      00692B 12 87 20         [24] 2794 	lcall	_delay
                                   2795 ;	..\src\peripherals\UBLOX.c:70: }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x24,sizeof(UBLOX_setNav5),UBLOX_setNav5,ACK,NULL));
      00692E 90 04 3E         [24] 2796 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_2
      006931 74 24            [12] 2797 	mov	a,#0x24
      006933 F0               [24] 2798 	movx	@dptr,a
      006934 90 04 3F         [24] 2799 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_3
      006937 F0               [24] 2800 	movx	@dptr,a
      006938 E4               [12] 2801 	clr	a
      006939 A3               [24] 2802 	inc	dptr
      00693A F0               [24] 2803 	movx	@dptr,a
      00693B 90 04 41         [24] 2804 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_4
      00693E 74 0F            [12] 2805 	mov	a,#_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198
      006940 F0               [24] 2806 	movx	@dptr,a
      006941 74 04            [12] 2807 	mov	a,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_65536_198 >> 8)
      006943 A3               [24] 2808 	inc	dptr
      006944 F0               [24] 2809 	movx	@dptr,a
      006945 E4               [12] 2810 	clr	a
      006946 A3               [24] 2811 	inc	dptr
      006947 F0               [24] 2812 	movx	@dptr,a
      006948 90 04 44         [24] 2813 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_5
      00694B 04               [12] 2814 	inc	a
      00694C F0               [24] 2815 	movx	@dptr,a
      00694D 90 04 45         [24] 2816 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_6
      006950 E4               [12] 2817 	clr	a
      006951 F0               [24] 2818 	movx	@dptr,a
      006952 A3               [24] 2819 	inc	dptr
      006953 F0               [24] 2820 	movx	@dptr,a
      006954 A3               [24] 2821 	inc	dptr
      006955 F0               [24] 2822 	movx	@dptr,a
      006956 75 82 06         [24] 2823 	mov	dpl,#0x06
      006959 12 6A 1F         [24] 2824 	lcall	_UBLOX_GPS_SendCommand_WaitACK
      00695C E5 82            [12] 2825 	mov	a,dpl
      00695E 60 C8            [24] 2826 	jz	00122$
                                   2827 ;	..\src\peripherals\UBLOX.c:72: dbglink_writestr("GPS init END");
      006960 90 93 1F         [24] 2828 	mov	dptr,#___str_1
      006963 75 F0 80         [24] 2829 	mov	b,#0x80
                                   2830 ;	..\src\peripherals\UBLOX.c:74: }
      006966 02 82 8A         [24] 2831 	ljmp	_dbglink_writestr
                                   2832 ;------------------------------------------------------------
                                   2833 ;Allocation info for local variables in function 'UBLOX_GPS_FletcherChecksum8'
                                   2834 ;------------------------------------------------------------
                                   2835 ;CK_A                      Allocated with name '_UBLOX_GPS_FletcherChecksum8_PARM_2'
                                   2836 ;CK_B                      Allocated with name '_UBLOX_GPS_FletcherChecksum8_PARM_3'
                                   2837 ;length                    Allocated with name '_UBLOX_GPS_FletcherChecksum8_PARM_4'
                                   2838 ;buffer                    Allocated with name '_UBLOX_GPS_FletcherChecksum8_buffer_65536_207'
                                   2839 ;i                         Allocated with name '_UBLOX_GPS_FletcherChecksum8_i_65536_208'
                                   2840 ;sloc0                     Allocated with name '_UBLOX_GPS_FletcherChecksum8_sloc0_1_0'
                                   2841 ;sloc1                     Allocated with name '_UBLOX_GPS_FletcherChecksum8_sloc1_1_0'
                                   2842 ;sloc2                     Allocated with name '_UBLOX_GPS_FletcherChecksum8_sloc2_1_0'
                                   2843 ;sloc3                     Allocated with name '_UBLOX_GPS_FletcherChecksum8_sloc3_1_0'
                                   2844 ;------------------------------------------------------------
                                   2845 ;	..\src\peripherals\UBLOX.c:87: void UBLOX_GPS_FletcherChecksum8 ( uint8_t * buffer, uint8_t *CK_A, uint8_t *CK_B, uint16_t length)
                                   2846 ;	-----------------------------------------
                                   2847 ;	 function UBLOX_GPS_FletcherChecksum8
                                   2848 ;	-----------------------------------------
      006969                       2849 _UBLOX_GPS_FletcherChecksum8:
      006969 AF F0            [24] 2850 	mov	r7,b
      00696B AE 83            [24] 2851 	mov	r6,dph
      00696D E5 82            [12] 2852 	mov	a,dpl
      00696F 90 04 3B         [24] 2853 	mov	dptr,#_UBLOX_GPS_FletcherChecksum8_buffer_65536_207
      006972 F0               [24] 2854 	movx	@dptr,a
      006973 EE               [12] 2855 	mov	a,r6
      006974 A3               [24] 2856 	inc	dptr
      006975 F0               [24] 2857 	movx	@dptr,a
      006976 EF               [12] 2858 	mov	a,r7
      006977 A3               [24] 2859 	inc	dptr
      006978 F0               [24] 2860 	movx	@dptr,a
                                   2861 ;	..\src\peripherals\UBLOX.c:90: *CK_A = 0;
      006979 90 04 33         [24] 2862 	mov	dptr,#_UBLOX_GPS_FletcherChecksum8_PARM_2
      00697C E0               [24] 2863 	movx	a,@dptr
      00697D FD               [12] 2864 	mov	r5,a
      00697E A3               [24] 2865 	inc	dptr
      00697F E0               [24] 2866 	movx	a,@dptr
      006980 FE               [12] 2867 	mov	r6,a
      006981 A3               [24] 2868 	inc	dptr
      006982 E0               [24] 2869 	movx	a,@dptr
      006983 FF               [12] 2870 	mov	r7,a
      006984 8D 82            [24] 2871 	mov	dpl,r5
      006986 8E 83            [24] 2872 	mov	dph,r6
      006988 8F F0            [24] 2873 	mov	b,r7
      00698A E4               [12] 2874 	clr	a
      00698B 12 7C 62         [24] 2875 	lcall	__gptrput
                                   2876 ;	..\src\peripherals\UBLOX.c:91: *CK_B = 0;
      00698E 90 04 36         [24] 2877 	mov	dptr,#_UBLOX_GPS_FletcherChecksum8_PARM_3
      006991 E0               [24] 2878 	movx	a,@dptr
      006992 F5 63            [12] 2879 	mov	_UBLOX_GPS_FletcherChecksum8_sloc3_1_0,a
      006994 A3               [24] 2880 	inc	dptr
      006995 E0               [24] 2881 	movx	a,@dptr
      006996 F5 64            [12] 2882 	mov	(_UBLOX_GPS_FletcherChecksum8_sloc3_1_0 + 1),a
      006998 A3               [24] 2883 	inc	dptr
      006999 E0               [24] 2884 	movx	a,@dptr
      00699A F5 65            [12] 2885 	mov	(_UBLOX_GPS_FletcherChecksum8_sloc3_1_0 + 2),a
      00699C 85 63 82         [24] 2886 	mov	dpl,_UBLOX_GPS_FletcherChecksum8_sloc3_1_0
      00699F 85 64 83         [24] 2887 	mov	dph,(_UBLOX_GPS_FletcherChecksum8_sloc3_1_0 + 1)
      0069A2 85 65 F0         [24] 2888 	mov	b,(_UBLOX_GPS_FletcherChecksum8_sloc3_1_0 + 2)
      0069A5 E4               [12] 2889 	clr	a
      0069A6 12 7C 62         [24] 2890 	lcall	__gptrput
                                   2891 ;	..\src\peripherals\UBLOX.c:92: for (i = 0; i < length; i++)
      0069A9 90 04 3B         [24] 2892 	mov	dptr,#_UBLOX_GPS_FletcherChecksum8_buffer_65536_207
      0069AC E0               [24] 2893 	movx	a,@dptr
      0069AD F5 5E            [12] 2894 	mov	_UBLOX_GPS_FletcherChecksum8_sloc0_1_0,a
      0069AF A3               [24] 2895 	inc	dptr
      0069B0 E0               [24] 2896 	movx	a,@dptr
      0069B1 F5 5F            [12] 2897 	mov	(_UBLOX_GPS_FletcherChecksum8_sloc0_1_0 + 1),a
      0069B3 A3               [24] 2898 	inc	dptr
      0069B4 E0               [24] 2899 	movx	a,@dptr
      0069B5 F5 60            [12] 2900 	mov	(_UBLOX_GPS_FletcherChecksum8_sloc0_1_0 + 2),a
      0069B7 90 04 39         [24] 2901 	mov	dptr,#_UBLOX_GPS_FletcherChecksum8_PARM_4
      0069BA E0               [24] 2902 	movx	a,@dptr
      0069BB F8               [12] 2903 	mov	r0,a
      0069BC A3               [24] 2904 	inc	dptr
      0069BD E0               [24] 2905 	movx	a,@dptr
      0069BE F9               [12] 2906 	mov	r1,a
      0069BF 75 61 00         [24] 2907 	mov	_UBLOX_GPS_FletcherChecksum8_sloc1_1_0,#0x00
      0069C2                       2908 00103$:
      0069C2 AB 61            [24] 2909 	mov	r3,_UBLOX_GPS_FletcherChecksum8_sloc1_1_0
      0069C4 7C 00            [12] 2910 	mov	r4,#0x00
      0069C6 C3               [12] 2911 	clr	c
      0069C7 EB               [12] 2912 	mov	a,r3
      0069C8 98               [12] 2913 	subb	a,r0
      0069C9 EC               [12] 2914 	mov	a,r4
      0069CA 99               [12] 2915 	subb	a,r1
      0069CB 50 51            [24] 2916 	jnc	00101$
                                   2917 ;	..\src\peripherals\UBLOX.c:94: *CK_A += buffer[i];
      0069CD C0 00            [24] 2918 	push	ar0
      0069CF C0 01            [24] 2919 	push	ar1
      0069D1 8D 82            [24] 2920 	mov	dpl,r5
      0069D3 8E 83            [24] 2921 	mov	dph,r6
      0069D5 8F F0            [24] 2922 	mov	b,r7
      0069D7 12 8D 8E         [24] 2923 	lcall	__gptrget
      0069DA F5 62            [12] 2924 	mov	_UBLOX_GPS_FletcherChecksum8_sloc2_1_0,a
      0069DC E5 61            [12] 2925 	mov	a,_UBLOX_GPS_FletcherChecksum8_sloc1_1_0
      0069DE 25 5E            [12] 2926 	add	a,_UBLOX_GPS_FletcherChecksum8_sloc0_1_0
      0069E0 F8               [12] 2927 	mov	r0,a
      0069E1 E4               [12] 2928 	clr	a
      0069E2 35 5F            [12] 2929 	addc	a,(_UBLOX_GPS_FletcherChecksum8_sloc0_1_0 + 1)
      0069E4 F9               [12] 2930 	mov	r1,a
      0069E5 AC 60            [24] 2931 	mov	r4,(_UBLOX_GPS_FletcherChecksum8_sloc0_1_0 + 2)
      0069E7 88 82            [24] 2932 	mov	dpl,r0
      0069E9 89 83            [24] 2933 	mov	dph,r1
      0069EB 8C F0            [24] 2934 	mov	b,r4
      0069ED 12 8D 8E         [24] 2935 	lcall	__gptrget
      0069F0 25 62            [12] 2936 	add	a,_UBLOX_GPS_FletcherChecksum8_sloc2_1_0
      0069F2 F8               [12] 2937 	mov	r0,a
      0069F3 8D 82            [24] 2938 	mov	dpl,r5
      0069F5 8E 83            [24] 2939 	mov	dph,r6
      0069F7 8F F0            [24] 2940 	mov	b,r7
      0069F9 12 7C 62         [24] 2941 	lcall	__gptrput
                                   2942 ;	..\src\peripherals\UBLOX.c:95: *CK_B += *CK_A;
      0069FC 85 63 82         [24] 2943 	mov	dpl,_UBLOX_GPS_FletcherChecksum8_sloc3_1_0
      0069FF 85 64 83         [24] 2944 	mov	dph,(_UBLOX_GPS_FletcherChecksum8_sloc3_1_0 + 1)
      006A02 85 65 F0         [24] 2945 	mov	b,(_UBLOX_GPS_FletcherChecksum8_sloc3_1_0 + 2)
      006A05 12 8D 8E         [24] 2946 	lcall	__gptrget
      006A08 FC               [12] 2947 	mov	r4,a
      006A09 28               [12] 2948 	add	a,r0
      006A0A 85 63 82         [24] 2949 	mov	dpl,_UBLOX_GPS_FletcherChecksum8_sloc3_1_0
      006A0D 85 64 83         [24] 2950 	mov	dph,(_UBLOX_GPS_FletcherChecksum8_sloc3_1_0 + 1)
      006A10 85 65 F0         [24] 2951 	mov	b,(_UBLOX_GPS_FletcherChecksum8_sloc3_1_0 + 2)
      006A13 12 7C 62         [24] 2952 	lcall	__gptrput
                                   2953 ;	..\src\peripherals\UBLOX.c:92: for (i = 0; i < length; i++)
      006A16 05 61            [12] 2954 	inc	_UBLOX_GPS_FletcherChecksum8_sloc1_1_0
      006A18 D0 01            [24] 2955 	pop	ar1
      006A1A D0 00            [24] 2956 	pop	ar0
      006A1C 80 A4            [24] 2957 	sjmp	00103$
      006A1E                       2958 00101$:
                                   2959 ;	..\src\peripherals\UBLOX.c:97: return;
                                   2960 ;	..\src\peripherals\UBLOX.c:98: }
      006A1E 22               [24] 2961 	ret
                                   2962 ;------------------------------------------------------------
                                   2963 ;Allocation info for local variables in function 'UBLOX_GPS_SendCommand_WaitACK'
                                   2964 ;------------------------------------------------------------
                                   2965 ;sloc0                     Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0'
                                   2966 ;sloc1                     Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0'
                                   2967 ;sloc2                     Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0'
                                   2968 ;sloc3                     Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0'
                                   2969 ;sloc4                     Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0'
                                   2970 ;sloc5                     Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_sloc5_1_0'
                                   2971 ;msg_id                    Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_PARM_2'
                                   2972 ;msg_length                Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_PARM_3'
                                   2973 ;payload                   Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_PARM_4'
                                   2974 ;AnsOrAck                  Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_PARM_5'
                                   2975 ;RxLength                  Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_PARM_6'
                                   2976 ;msg_class                 Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_msg_class_65536_211'
                                   2977 ;i                         Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_i_65536_212'
                                   2978 ;k                         Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_k_65536_212'
                                   2979 ;RetVal                    Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_RetVal_65536_212'
                                   2980 ;buffer_aux                Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_65536_212'
                                   2981 ;rx_buffer                 Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_65536_212'
                                   2982 ;CK_A                      Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_CK_A_65536_212'
                                   2983 ;CK_B                      Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_CK_B_65536_212'
                                   2984 ;------------------------------------------------------------
                                   2985 ;	..\src\peripherals\UBLOX.c:112: uint8_t UBLOX_GPS_SendCommand_WaitACK(uint8_t msg_class, uint8_t msg_id, uint16_t msg_length,uint8_t *payload,uint8_t AnsOrAck,uint16_t *RxLength)
                                   2986 ;	-----------------------------------------
                                   2987 ;	 function UBLOX_GPS_SendCommand_WaitACK
                                   2988 ;	-----------------------------------------
      006A1F                       2989 _UBLOX_GPS_SendCommand_WaitACK:
      006A1F E5 82            [12] 2990 	mov	a,dpl
      006A21 90 04 48         [24] 2991 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_msg_class_65536_211
      006A24 F0               [24] 2992 	movx	@dptr,a
                                   2993 ;	..\src\peripherals\UBLOX.c:115: uint8_t RetVal = 0;
      006A25 90 04 49         [24] 2994 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_RetVal_65536_212
      006A28 E4               [12] 2995 	clr	a
      006A29 F0               [24] 2996 	movx	@dptr,a
                                   2997 ;	..\src\peripherals\UBLOX.c:119: buffer_aux =(uint8_t *) malloc((msg_length)+10);
      006A2A 90 04 3F         [24] 2998 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_3
      006A2D E0               [24] 2999 	movx	a,@dptr
      006A2E FE               [12] 3000 	mov	r6,a
      006A2F A3               [24] 3001 	inc	dptr
      006A30 E0               [24] 3002 	movx	a,@dptr
      006A31 FF               [12] 3003 	mov	r7,a
      006A32 74 0A            [12] 3004 	mov	a,#0x0a
      006A34 2E               [12] 3005 	add	a,r6
      006A35 FC               [12] 3006 	mov	r4,a
      006A36 E4               [12] 3007 	clr	a
      006A37 3F               [12] 3008 	addc	a,r7
      006A38 FD               [12] 3009 	mov	r5,a
      006A39 8C 82            [24] 3010 	mov	dpl,r4
      006A3B 8D 83            [24] 3011 	mov	dph,r5
      006A3D C0 07            [24] 3012 	push	ar7
      006A3F C0 06            [24] 3013 	push	ar6
      006A41 12 7E 60         [24] 3014 	lcall	_malloc
      006A44 AC 82            [24] 3015 	mov	r4,dpl
      006A46 AD 83            [24] 3016 	mov	r5,dph
                                   3017 ;	..\src\peripherals\UBLOX.c:120: rx_buffer =(uint8_t *) malloc(40);
      006A48 90 00 28         [24] 3018 	mov	dptr,#0x0028
      006A4B C0 05            [24] 3019 	push	ar5
      006A4D C0 04            [24] 3020 	push	ar4
      006A4F 12 7E 60         [24] 3021 	lcall	_malloc
      006A52 AA 82            [24] 3022 	mov	r2,dpl
      006A54 AB 83            [24] 3023 	mov	r3,dph
      006A56 D0 04            [24] 3024 	pop	ar4
      006A58 D0 05            [24] 3025 	pop	ar5
      006A5A D0 06            [24] 3026 	pop	ar6
      006A5C D0 07            [24] 3027 	pop	ar7
                                   3028 ;	..\src\peripherals\UBLOX.c:121: for(i = 0; i<40;i++)
      006A5E 79 00            [12] 3029 	mov	r1,#0x00
      006A60                       3030 00125$:
                                   3031 ;	..\src\peripherals\UBLOX.c:123: *(rx_buffer+i)=0;
      006A60 E9               [12] 3032 	mov	a,r1
      006A61 2A               [12] 3033 	add	a,r2
      006A62 F5 82            [12] 3034 	mov	dpl,a
      006A64 E4               [12] 3035 	clr	a
      006A65 3B               [12] 3036 	addc	a,r3
      006A66 F5 83            [12] 3037 	mov	dph,a
      006A68 E4               [12] 3038 	clr	a
      006A69 F0               [24] 3039 	movx	@dptr,a
                                   3040 ;	..\src\peripherals\UBLOX.c:121: for(i = 0; i<40;i++)
      006A6A 09               [12] 3041 	inc	r1
      006A6B B9 28 00         [24] 3042 	cjne	r1,#0x28,00203$
      006A6E                       3043 00203$:
      006A6E 40 F0            [24] 3044 	jc	00125$
                                   3045 ;	..\src\peripherals\UBLOX.c:126: *buffer_aux = msg_class;
      006A70 C0 02            [24] 3046 	push	ar2
      006A72 C0 03            [24] 3047 	push	ar3
      006A74 90 04 48         [24] 3048 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_msg_class_65536_211
      006A77 E0               [24] 3049 	movx	a,@dptr
      006A78 F9               [12] 3050 	mov	r1,a
      006A79 8C 82            [24] 3051 	mov	dpl,r4
      006A7B 8D 83            [24] 3052 	mov	dph,r5
      006A7D F0               [24] 3053 	movx	@dptr,a
                                   3054 ;	..\src\peripherals\UBLOX.c:127: buffer_aux++;
      006A7E 74 01            [12] 3055 	mov	a,#0x01
      006A80 2C               [12] 3056 	add	a,r4
      006A81 F8               [12] 3057 	mov	r0,a
      006A82 E4               [12] 3058 	clr	a
      006A83 3D               [12] 3059 	addc	a,r5
      006A84 FB               [12] 3060 	mov	r3,a
                                   3061 ;	..\src\peripherals\UBLOX.c:128: *buffer_aux = msg_id;
      006A85 90 04 3E         [24] 3062 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_2
      006A88 E0               [24] 3063 	movx	a,@dptr
      006A89 F5 4A            [12] 3064 	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0,a
      006A8B 88 82            [24] 3065 	mov	dpl,r0
      006A8D 8B 83            [24] 3066 	mov	dph,r3
      006A8F F0               [24] 3067 	movx	@dptr,a
                                   3068 ;	..\src\peripherals\UBLOX.c:129: buffer_aux++;
      006A90 8C 82            [24] 3069 	mov	dpl,r4
      006A92 8D 83            [24] 3070 	mov	dph,r5
      006A94 A3               [24] 3071 	inc	dptr
      006A95 A3               [24] 3072 	inc	dptr
                                   3073 ;	..\src\peripherals\UBLOX.c:130: *buffer_aux = msg_length;
      006A96 EE               [12] 3074 	mov	a,r6
      006A97 F5 4B            [12] 3075 	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0,a
      006A99 F0               [24] 3076 	movx	@dptr,a
                                   3077 ;	..\src\peripherals\UBLOX.c:131: buffer_aux +=2;
      006A9A 74 04            [12] 3078 	mov	a,#0x04
      006A9C 2C               [12] 3079 	add	a,r4
      006A9D F8               [12] 3080 	mov	r0,a
      006A9E E4               [12] 3081 	clr	a
      006A9F 3D               [12] 3082 	addc	a,r5
      006AA0 FA               [12] 3083 	mov	r2,a
                                   3084 ;	..\src\peripherals\UBLOX.c:132: memcpy(buffer_aux,payload,msg_length);
      006AA1 7B 00            [12] 3085 	mov	r3,#0x00
      006AA3 90 04 41         [24] 3086 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_4
      006AA6 E0               [24] 3087 	movx	a,@dptr
      006AA7 F5 4C            [12] 3088 	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0,a
      006AA9 A3               [24] 3089 	inc	dptr
      006AAA E0               [24] 3090 	movx	a,@dptr
      006AAB F5 4D            [12] 3091 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0 + 1),a
      006AAD A3               [24] 3092 	inc	dptr
      006AAE E0               [24] 3093 	movx	a,@dptr
      006AAF F5 4E            [12] 3094 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0 + 2),a
      006AB1 90 04 6D         [24] 3095 	mov	dptr,#_memcpy_PARM_2
      006AB4 E5 4C            [12] 3096 	mov	a,_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0
      006AB6 F0               [24] 3097 	movx	@dptr,a
      006AB7 E5 4D            [12] 3098 	mov	a,(_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0 + 1)
      006AB9 A3               [24] 3099 	inc	dptr
      006ABA F0               [24] 3100 	movx	@dptr,a
      006ABB E5 4E            [12] 3101 	mov	a,(_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0 + 2)
      006ABD A3               [24] 3102 	inc	dptr
      006ABE F0               [24] 3103 	movx	@dptr,a
      006ABF 90 04 70         [24] 3104 	mov	dptr,#_memcpy_PARM_3
      006AC2 EE               [12] 3105 	mov	a,r6
      006AC3 F0               [24] 3106 	movx	@dptr,a
      006AC4 EF               [12] 3107 	mov	a,r7
      006AC5 A3               [24] 3108 	inc	dptr
      006AC6 F0               [24] 3109 	movx	@dptr,a
      006AC7 88 82            [24] 3110 	mov	dpl,r0
      006AC9 8A 83            [24] 3111 	mov	dph,r2
      006ACB 8B F0            [24] 3112 	mov	b,r3
      006ACD C0 07            [24] 3113 	push	ar7
      006ACF C0 06            [24] 3114 	push	ar6
      006AD1 C0 05            [24] 3115 	push	ar5
      006AD3 C0 04            [24] 3116 	push	ar4
      006AD5 C0 03            [24] 3117 	push	ar3
      006AD7 C0 02            [24] 3118 	push	ar2
      006AD9 C0 01            [24] 3119 	push	ar1
      006ADB 12 78 14         [24] 3120 	lcall	_memcpy
      006ADE D0 01            [24] 3121 	pop	ar1
      006AE0 D0 02            [24] 3122 	pop	ar2
      006AE2 D0 03            [24] 3123 	pop	ar3
      006AE4 D0 04            [24] 3124 	pop	ar4
      006AE6 D0 05            [24] 3125 	pop	ar5
      006AE8 D0 06            [24] 3126 	pop	ar6
      006AEA D0 07            [24] 3127 	pop	ar7
                                   3128 ;	..\src\peripherals\UBLOX.c:133: buffer_aux -=4;
      006AEC 8C 4F            [24] 3129 	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0,r4
      006AEE 8D 50            [24] 3130 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0 + 1),r5
                                   3131 ;	..\src\peripherals\UBLOX.c:134: UBLOX_GPS_FletcherChecksum8(buffer_aux,&CK_A,&CK_B,(msg_length)+4);
      006AF0 7B 00            [12] 3132 	mov	r3,#0x00
      006AF2 74 04            [12] 3133 	mov	a,#0x04
      006AF4 2E               [12] 3134 	add	a,r6
      006AF5 F8               [12] 3135 	mov	r0,a
      006AF6 E4               [12] 3136 	clr	a
      006AF7 3F               [12] 3137 	addc	a,r7
      006AF8 FA               [12] 3138 	mov	r2,a
      006AF9 90 04 33         [24] 3139 	mov	dptr,#_UBLOX_GPS_FletcherChecksum8_PARM_2
      006AFC 74 4A            [12] 3140 	mov	a,#_UBLOX_GPS_SendCommand_WaitACK_CK_A_65536_212
      006AFE F0               [24] 3141 	movx	@dptr,a
      006AFF 74 04            [12] 3142 	mov	a,#(_UBLOX_GPS_SendCommand_WaitACK_CK_A_65536_212 >> 8)
      006B01 A3               [24] 3143 	inc	dptr
      006B02 F0               [24] 3144 	movx	@dptr,a
      006B03 E4               [12] 3145 	clr	a
      006B04 A3               [24] 3146 	inc	dptr
      006B05 F0               [24] 3147 	movx	@dptr,a
      006B06 90 04 36         [24] 3148 	mov	dptr,#_UBLOX_GPS_FletcherChecksum8_PARM_3
      006B09 74 4B            [12] 3149 	mov	a,#_UBLOX_GPS_SendCommand_WaitACK_CK_B_65536_212
      006B0B F0               [24] 3150 	movx	@dptr,a
      006B0C 74 04            [12] 3151 	mov	a,#(_UBLOX_GPS_SendCommand_WaitACK_CK_B_65536_212 >> 8)
      006B0E A3               [24] 3152 	inc	dptr
      006B0F F0               [24] 3153 	movx	@dptr,a
      006B10 E4               [12] 3154 	clr	a
      006B11 A3               [24] 3155 	inc	dptr
      006B12 F0               [24] 3156 	movx	@dptr,a
      006B13 90 04 39         [24] 3157 	mov	dptr,#_UBLOX_GPS_FletcherChecksum8_PARM_4
      006B16 E8               [12] 3158 	mov	a,r0
      006B17 F0               [24] 3159 	movx	@dptr,a
      006B18 EA               [12] 3160 	mov	a,r2
      006B19 A3               [24] 3161 	inc	dptr
      006B1A F0               [24] 3162 	movx	@dptr,a
      006B1B 8C 82            [24] 3163 	mov	dpl,r4
      006B1D 8D 83            [24] 3164 	mov	dph,r5
      006B1F 8B F0            [24] 3165 	mov	b,r3
      006B21 C0 07            [24] 3166 	push	ar7
      006B23 C0 06            [24] 3167 	push	ar6
      006B25 C0 03            [24] 3168 	push	ar3
      006B27 C0 02            [24] 3169 	push	ar2
      006B29 C0 01            [24] 3170 	push	ar1
      006B2B 12 69 69         [24] 3171 	lcall	_UBLOX_GPS_FletcherChecksum8
      006B2E D0 01            [24] 3172 	pop	ar1
      006B30 D0 02            [24] 3173 	pop	ar2
      006B32 D0 03            [24] 3174 	pop	ar3
      006B34 D0 06            [24] 3175 	pop	ar6
      006B36 D0 07            [24] 3176 	pop	ar7
                                   3177 ;	..\src\peripherals\UBLOX.c:135: uart1_tx(UBX_HEADER1_VAL);
      006B38 75 82 B5         [24] 3178 	mov	dpl,#0xb5
      006B3B 12 7D DA         [24] 3179 	lcall	_uart1_tx
                                   3180 ;	..\src\peripherals\UBLOX.c:136: uart1_tx(UBX_HEADER2_VAL);
      006B3E 75 82 62         [24] 3181 	mov	dpl,#0x62
      006B41 12 7D DA         [24] 3182 	lcall	_uart1_tx
                                   3183 ;	..\src\peripherals\UBLOX.c:137: uart1_tx(msg_class);
      006B44 89 82            [24] 3184 	mov	dpl,r1
      006B46 12 7D DA         [24] 3185 	lcall	_uart1_tx
                                   3186 ;	..\src\peripherals\UBLOX.c:138: uart1_tx(msg_id);
      006B49 85 4A 82         [24] 3187 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0
      006B4C 12 7D DA         [24] 3188 	lcall	_uart1_tx
                                   3189 ;	..\src\peripherals\UBLOX.c:139: uart1_tx((uint8_t)(msg_length & 0x00FF));//LSB
      006B4F 85 4B 82         [24] 3190 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0
      006B52 12 7D DA         [24] 3191 	lcall	_uart1_tx
                                   3192 ;	..\src\peripherals\UBLOX.c:140: uart1_tx((uint8_t)((msg_length & 0xFF00)>>8));//MSB
      006B55 8F 05            [24] 3193 	mov	ar5,r7
      006B57 8D 04            [24] 3194 	mov	ar4,r5
      006B59 8C 82            [24] 3195 	mov	dpl,r4
      006B5B 12 7D DA         [24] 3196 	lcall	_uart1_tx
                                   3197 ;	..\src\peripherals\UBLOX.c:141: for(i = 0;i<msg_length;i++)
      006B5E 75 4B 00         [24] 3198 	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0,#0x00
                                   3199 ;	..\src\peripherals\UBLOX.c:190: return RetVal;
      006B61 D0 03            [24] 3200 	pop	ar3
      006B63 D0 02            [24] 3201 	pop	ar2
                                   3202 ;	..\src\peripherals\UBLOX.c:141: for(i = 0;i<msg_length;i++)
      006B65                       3203 00128$:
      006B65 AC 4B            [24] 3204 	mov	r4,_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0
      006B67 7D 00            [12] 3205 	mov	r5,#0x00
      006B69 C3               [12] 3206 	clr	c
      006B6A EC               [12] 3207 	mov	a,r4
      006B6B 9E               [12] 3208 	subb	a,r6
      006B6C ED               [12] 3209 	mov	a,r5
      006B6D 9F               [12] 3210 	subb	a,r7
      006B6E 50 1E            [24] 3211 	jnc	00102$
                                   3212 ;	..\src\peripherals\UBLOX.c:143: uart1_tx(*(payload+i));
      006B70 E5 4B            [12] 3213 	mov	a,_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0
      006B72 25 4C            [12] 3214 	add	a,_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0
      006B74 F9               [12] 3215 	mov	r1,a
      006B75 E4               [12] 3216 	clr	a
      006B76 35 4D            [12] 3217 	addc	a,(_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0 + 1)
      006B78 FC               [12] 3218 	mov	r4,a
      006B79 AD 4E            [24] 3219 	mov	r5,(_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0 + 2)
      006B7B 89 82            [24] 3220 	mov	dpl,r1
      006B7D 8C 83            [24] 3221 	mov	dph,r4
      006B7F 8D F0            [24] 3222 	mov	b,r5
      006B81 12 8D 8E         [24] 3223 	lcall	__gptrget
      006B84 F9               [12] 3224 	mov	r1,a
      006B85 F5 82            [12] 3225 	mov	dpl,a
      006B87 12 7D DA         [24] 3226 	lcall	_uart1_tx
                                   3227 ;	..\src\peripherals\UBLOX.c:141: for(i = 0;i<msg_length;i++)
      006B8A 05 4B            [12] 3228 	inc	_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0
      006B8C 80 D7            [24] 3229 	sjmp	00128$
      006B8E                       3230 00102$:
                                   3231 ;	..\src\peripherals\UBLOX.c:145: uart1_tx(CK_A);
      006B8E 90 04 4A         [24] 3232 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_CK_A_65536_212
      006B91 E0               [24] 3233 	movx	a,@dptr
      006B92 F5 82            [12] 3234 	mov	dpl,a
      006B94 12 7D DA         [24] 3235 	lcall	_uart1_tx
                                   3236 ;	..\src\peripherals\UBLOX.c:146: uart1_tx(CK_B);
      006B97 90 04 4B         [24] 3237 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_CK_B_65536_212
      006B9A E0               [24] 3238 	movx	a,@dptr
      006B9B F5 82            [12] 3239 	mov	dpl,a
      006B9D 12 7D DA         [24] 3240 	lcall	_uart1_tx
                                   3241 ;	..\src\peripherals\UBLOX.c:148: do{
      006BA0                       3242 00103$:
                                   3243 ;	..\src\peripherals\UBLOX.c:149: delay(2500);
      006BA0 90 09 C4         [24] 3244 	mov	dptr,#0x09c4
      006BA3 12 87 20         [24] 3245 	lcall	_delay
                                   3246 ;	..\src\peripherals\UBLOX.c:150: }while(!uart1_rxcount());
      006BA6 12 9D F9         [24] 3247 	lcall	_uart1_rxcount
      006BA9 E5 82            [12] 3248 	mov	a,dpl
      006BAB 60 F3            [24] 3249 	jz	00103$
                                   3250 ;	..\src\peripherals\UBLOX.c:151: delay(25000);
      006BAD 90 61 A8         [24] 3251 	mov	dptr,#0x61a8
      006BB0 12 87 20         [24] 3252 	lcall	_delay
                                   3253 ;	..\src\peripherals\UBLOX.c:153: do
      006BB3 7F 00            [12] 3254 	mov	r7,#0x00
      006BB5                       3255 00106$:
                                   3256 ;	..\src\peripherals\UBLOX.c:155: wtimer_runcallbacks(); // si no pongo esto por algun motivo se pierden los eventos de timer , cuidado con los delays !!
      006BB5 C0 07            [24] 3257 	push	ar7
      006BB7 C0 03            [24] 3258 	push	ar3
      006BB9 C0 02            [24] 3259 	push	ar2
      006BBB 12 77 0F         [24] 3260 	lcall	_wtimer_runcallbacks
      006BBE D0 02            [24] 3261 	pop	ar2
      006BC0 D0 03            [24] 3262 	pop	ar3
      006BC2 D0 07            [24] 3263 	pop	ar7
                                   3264 ;	..\src\peripherals\UBLOX.c:156: *(rx_buffer+k)=uart1_rx();
      006BC4 EF               [12] 3265 	mov	a,r7
      006BC5 2A               [12] 3266 	add	a,r2
      006BC6 FD               [12] 3267 	mov	r5,a
      006BC7 E4               [12] 3268 	clr	a
      006BC8 3B               [12] 3269 	addc	a,r3
      006BC9 FE               [12] 3270 	mov	r6,a
      006BCA 12 7C FC         [24] 3271 	lcall	_uart1_rx
      006BCD AC 82            [24] 3272 	mov	r4,dpl
      006BCF 8D 82            [24] 3273 	mov	dpl,r5
      006BD1 8E 83            [24] 3274 	mov	dph,r6
      006BD3 EC               [12] 3275 	mov	a,r4
      006BD4 F0               [24] 3276 	movx	@dptr,a
                                   3277 ;	..\src\peripherals\UBLOX.c:157: k++;
      006BD5 0F               [12] 3278 	inc	r7
                                   3279 ;	..\src\peripherals\UBLOX.c:158: delay(2500);
      006BD6 90 09 C4         [24] 3280 	mov	dptr,#0x09c4
      006BD9 12 87 20         [24] 3281 	lcall	_delay
                                   3282 ;	..\src\peripherals\UBLOX.c:159: }while(uart1_rxcount());
      006BDC 12 9D F9         [24] 3283 	lcall	_uart1_rxcount
      006BDF E5 82            [12] 3284 	mov	a,dpl
      006BE1 70 D2            [24] 3285 	jnz	00106$
                                   3286 ;	..\src\peripherals\UBLOX.c:162: if(rx_buffer[UBX_HEADER1_POS] == UBX_HEADER1_VAL && rx_buffer[UBX_HEADER2_POS] == UBX_HEADER2_VAL)
      006BE3 8A 82            [24] 3287 	mov	dpl,r2
      006BE5 8B 83            [24] 3288 	mov	dph,r3
      006BE7 E0               [24] 3289 	movx	a,@dptr
      006BE8 FE               [12] 3290 	mov	r6,a
      006BE9 BE B5 02         [24] 3291 	cjne	r6,#0xb5,00208$
      006BEC 80 03            [24] 3292 	sjmp	00209$
      006BEE                       3293 00208$:
      006BEE 02 6D 84         [24] 3294 	ljmp	00123$
      006BF1                       3295 00209$:
      006BF1 8A 82            [24] 3296 	mov	dpl,r2
      006BF3 8B 83            [24] 3297 	mov	dph,r3
      006BF5 A3               [24] 3298 	inc	dptr
      006BF6 E0               [24] 3299 	movx	a,@dptr
      006BF7 FE               [12] 3300 	mov	r6,a
      006BF8 BE 62 02         [24] 3301 	cjne	r6,#0x62,00210$
      006BFB 80 03            [24] 3302 	sjmp	00211$
      006BFD                       3303 00210$:
      006BFD 02 6D 84         [24] 3304 	ljmp	00123$
      006C00                       3305 00211$:
                                   3306 ;	..\src\peripherals\UBLOX.c:164: CK_A = 0;
      006C00 90 04 4A         [24] 3307 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_CK_A_65536_212
      006C03 E4               [12] 3308 	clr	a
      006C04 F0               [24] 3309 	movx	@dptr,a
                                   3310 ;	..\src\peripherals\UBLOX.c:165: CK_B = 0;
      006C05 90 04 4B         [24] 3311 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_CK_B_65536_212
      006C08 F0               [24] 3312 	movx	@dptr,a
                                   3313 ;	..\src\peripherals\UBLOX.c:166: UBLOX_GPS_FletcherChecksum8(&rx_buffer[UBX_MESSAGE_CLASS],&CK_A,&CK_B,k-4);
      006C09 74 02            [12] 3314 	mov	a,#0x02
      006C0B 2A               [12] 3315 	add	a,r2
      006C0C FD               [12] 3316 	mov	r5,a
      006C0D E4               [12] 3317 	clr	a
      006C0E 3B               [12] 3318 	addc	a,r3
      006C0F FE               [12] 3319 	mov	r6,a
      006C10 8D 53            [24] 3320 	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc5_1_0,r5
      006C12 8E 54            [24] 3321 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc5_1_0 + 1),r6
      006C14 75 55 00         [24] 3322 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc5_1_0 + 2),#0x00
      006C17 8F 51            [24] 3323 	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0,r7
      006C19 75 52 00         [24] 3324 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0 + 1),#0x00
      006C1C E5 51            [12] 3325 	mov	a,_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0
      006C1E 24 FC            [12] 3326 	add	a,#0xfc
      006C20 FC               [12] 3327 	mov	r4,a
      006C21 E5 52            [12] 3328 	mov	a,(_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0 + 1)
      006C23 34 FF            [12] 3329 	addc	a,#0xff
      006C25 FF               [12] 3330 	mov	r7,a
      006C26 90 04 33         [24] 3331 	mov	dptr,#_UBLOX_GPS_FletcherChecksum8_PARM_2
      006C29 74 4A            [12] 3332 	mov	a,#_UBLOX_GPS_SendCommand_WaitACK_CK_A_65536_212
      006C2B F0               [24] 3333 	movx	@dptr,a
      006C2C 74 04            [12] 3334 	mov	a,#(_UBLOX_GPS_SendCommand_WaitACK_CK_A_65536_212 >> 8)
      006C2E A3               [24] 3335 	inc	dptr
      006C2F F0               [24] 3336 	movx	@dptr,a
      006C30 E4               [12] 3337 	clr	a
      006C31 A3               [24] 3338 	inc	dptr
      006C32 F0               [24] 3339 	movx	@dptr,a
      006C33 90 04 36         [24] 3340 	mov	dptr,#_UBLOX_GPS_FletcherChecksum8_PARM_3
      006C36 74 4B            [12] 3341 	mov	a,#_UBLOX_GPS_SendCommand_WaitACK_CK_B_65536_212
      006C38 F0               [24] 3342 	movx	@dptr,a
      006C39 74 04            [12] 3343 	mov	a,#(_UBLOX_GPS_SendCommand_WaitACK_CK_B_65536_212 >> 8)
      006C3B A3               [24] 3344 	inc	dptr
      006C3C F0               [24] 3345 	movx	@dptr,a
      006C3D E4               [12] 3346 	clr	a
      006C3E A3               [24] 3347 	inc	dptr
      006C3F F0               [24] 3348 	movx	@dptr,a
      006C40 90 04 39         [24] 3349 	mov	dptr,#_UBLOX_GPS_FletcherChecksum8_PARM_4
      006C43 EC               [12] 3350 	mov	a,r4
      006C44 F0               [24] 3351 	movx	@dptr,a
      006C45 EF               [12] 3352 	mov	a,r7
      006C46 A3               [24] 3353 	inc	dptr
      006C47 F0               [24] 3354 	movx	@dptr,a
      006C48 85 53 82         [24] 3355 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_sloc5_1_0
      006C4B 85 54 83         [24] 3356 	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_sloc5_1_0 + 1)
      006C4E 85 55 F0         [24] 3357 	mov	b,(_UBLOX_GPS_SendCommand_WaitACK_sloc5_1_0 + 2)
      006C51 C0 06            [24] 3358 	push	ar6
      006C53 C0 05            [24] 3359 	push	ar5
      006C55 C0 03            [24] 3360 	push	ar3
      006C57 C0 02            [24] 3361 	push	ar2
      006C59 12 69 69         [24] 3362 	lcall	_UBLOX_GPS_FletcherChecksum8
      006C5C D0 02            [24] 3363 	pop	ar2
      006C5E D0 03            [24] 3364 	pop	ar3
      006C60 D0 05            [24] 3365 	pop	ar5
      006C62 D0 06            [24] 3366 	pop	ar6
                                   3367 ;	..\src\peripherals\UBLOX.c:167: if(CK_A == rx_buffer[k-2] && CK_B == rx_buffer[k-1]) // verifico el checksum calculado
      006C64 E5 51            [12] 3368 	mov	a,_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0
      006C66 24 FE            [12] 3369 	add	a,#0xfe
      006C68 FC               [12] 3370 	mov	r4,a
      006C69 E5 52            [12] 3371 	mov	a,(_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0 + 1)
      006C6B 34 FF            [12] 3372 	addc	a,#0xff
      006C6D FF               [12] 3373 	mov	r7,a
      006C6E EC               [12] 3374 	mov	a,r4
      006C6F 2A               [12] 3375 	add	a,r2
      006C70 F5 82            [12] 3376 	mov	dpl,a
      006C72 EF               [12] 3377 	mov	a,r7
      006C73 3B               [12] 3378 	addc	a,r3
      006C74 F5 83            [12] 3379 	mov	dph,a
      006C76 E0               [24] 3380 	movx	a,@dptr
      006C77 FF               [12] 3381 	mov	r7,a
      006C78 90 04 4A         [24] 3382 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_CK_A_65536_212
      006C7B E0               [24] 3383 	movx	a,@dptr
      006C7C FC               [12] 3384 	mov	r4,a
      006C7D B5 07 02         [24] 3385 	cjne	a,ar7,00212$
      006C80 80 03            [24] 3386 	sjmp	00213$
      006C82                       3387 00212$:
      006C82 02 6D 84         [24] 3388 	ljmp	00123$
      006C85                       3389 00213$:
      006C85 E5 51            [12] 3390 	mov	a,_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0
      006C87 24 FF            [12] 3391 	add	a,#0xff
      006C89 FC               [12] 3392 	mov	r4,a
      006C8A E5 52            [12] 3393 	mov	a,(_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0 + 1)
      006C8C 34 FF            [12] 3394 	addc	a,#0xff
      006C8E FF               [12] 3395 	mov	r7,a
      006C8F EC               [12] 3396 	mov	a,r4
      006C90 2A               [12] 3397 	add	a,r2
      006C91 F5 82            [12] 3398 	mov	dpl,a
      006C93 EF               [12] 3399 	mov	a,r7
      006C94 3B               [12] 3400 	addc	a,r3
      006C95 F5 83            [12] 3401 	mov	dph,a
      006C97 E0               [24] 3402 	movx	a,@dptr
      006C98 FF               [12] 3403 	mov	r7,a
      006C99 90 04 4B         [24] 3404 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_CK_B_65536_212
      006C9C E0               [24] 3405 	movx	a,@dptr
      006C9D FC               [12] 3406 	mov	r4,a
      006C9E B5 07 02         [24] 3407 	cjne	a,ar7,00214$
      006CA1 80 03            [24] 3408 	sjmp	00215$
      006CA3                       3409 00214$:
      006CA3 02 6D 84         [24] 3410 	ljmp	00123$
      006CA6                       3411 00215$:
                                   3412 ;	..\src\peripherals\UBLOX.c:170: if(!AnsOrAck)
      006CA6 90 04 44         [24] 3413 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_5
      006CA9 E0               [24] 3414 	movx	a,@dptr
      006CAA 60 03            [24] 3415 	jz	00216$
      006CAC 02 6D 62         [24] 3416 	ljmp	00117$
      006CAF                       3417 00216$:
                                   3418 ;	..\src\peripherals\UBLOX.c:173: if(msg_class == rx_buffer[UBX_MESSAGE_CLASS] && msg_id == rx_buffer[UBX_MESSAGE_ID])
      006CAF 8D 82            [24] 3419 	mov	dpl,r5
      006CB1 8E 83            [24] 3420 	mov	dph,r6
      006CB3 E0               [24] 3421 	movx	a,@dptr
      006CB4 FF               [12] 3422 	mov	r7,a
      006CB5 90 04 48         [24] 3423 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_msg_class_65536_211
      006CB8 E0               [24] 3424 	movx	a,@dptr
      006CB9 FC               [12] 3425 	mov	r4,a
      006CBA B5 07 02         [24] 3426 	cjne	a,ar7,00217$
      006CBD 80 03            [24] 3427 	sjmp	00218$
      006CBF                       3428 00217$:
      006CBF 02 6D 84         [24] 3429 	ljmp	00123$
      006CC2                       3430 00218$:
      006CC2 8A 82            [24] 3431 	mov	dpl,r2
      006CC4 8B 83            [24] 3432 	mov	dph,r3
      006CC6 A3               [24] 3433 	inc	dptr
      006CC7 A3               [24] 3434 	inc	dptr
      006CC8 A3               [24] 3435 	inc	dptr
      006CC9 E0               [24] 3436 	movx	a,@dptr
      006CCA FF               [12] 3437 	mov	r7,a
      006CCB B5 4A 02         [24] 3438 	cjne	a,_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0,00219$
      006CCE 80 03            [24] 3439 	sjmp	00220$
      006CD0                       3440 00219$:
      006CD0 02 6D 84         [24] 3441 	ljmp	00123$
      006CD3                       3442 00220$:
                                   3443 ;	..\src\peripherals\UBLOX.c:175: *RxLength = rx_buffer[UBX_MESSAGE_LENGTH_MSB]<<8 | rx_buffer[UBX_MESSAGE_LENGTH_LSB];
      006CD3 90 04 45         [24] 3444 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_6
      006CD6 E0               [24] 3445 	movx	a,@dptr
      006CD7 F5 53            [12] 3446 	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc5_1_0,a
      006CD9 A3               [24] 3447 	inc	dptr
      006CDA E0               [24] 3448 	movx	a,@dptr
      006CDB F5 54            [12] 3449 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc5_1_0 + 1),a
      006CDD A3               [24] 3450 	inc	dptr
      006CDE E0               [24] 3451 	movx	a,@dptr
      006CDF F5 55            [12] 3452 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc5_1_0 + 2),a
      006CE1 8A 82            [24] 3453 	mov	dpl,r2
      006CE3 8B 83            [24] 3454 	mov	dph,r3
      006CE5 A3               [24] 3455 	inc	dptr
      006CE6 A3               [24] 3456 	inc	dptr
      006CE7 A3               [24] 3457 	inc	dptr
      006CE8 A3               [24] 3458 	inc	dptr
      006CE9 A3               [24] 3459 	inc	dptr
      006CEA E0               [24] 3460 	movx	a,@dptr
      006CEB FF               [12] 3461 	mov	r7,a
      006CEC 78 00            [12] 3462 	mov	r0,#0x00
      006CEE 8A 82            [24] 3463 	mov	dpl,r2
      006CF0 8B 83            [24] 3464 	mov	dph,r3
      006CF2 A3               [24] 3465 	inc	dptr
      006CF3 A3               [24] 3466 	inc	dptr
      006CF4 A3               [24] 3467 	inc	dptr
      006CF5 A3               [24] 3468 	inc	dptr
      006CF6 E0               [24] 3469 	movx	a,@dptr
      006CF7 7C 00            [12] 3470 	mov	r4,#0x00
      006CF9 42 00            [12] 3471 	orl	ar0,a
      006CFB EC               [12] 3472 	mov	a,r4
      006CFC 42 07            [12] 3473 	orl	ar7,a
      006CFE 85 53 82         [24] 3474 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_sloc5_1_0
      006D01 85 54 83         [24] 3475 	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_sloc5_1_0 + 1)
      006D04 85 55 F0         [24] 3476 	mov	b,(_UBLOX_GPS_SendCommand_WaitACK_sloc5_1_0 + 2)
      006D07 E8               [12] 3477 	mov	a,r0
      006D08 12 7C 62         [24] 3478 	lcall	__gptrput
      006D0B A3               [24] 3479 	inc	dptr
      006D0C EF               [12] 3480 	mov	a,r7
      006D0D 12 7C 62         [24] 3481 	lcall	__gptrput
                                   3482 ;	..\src\peripherals\UBLOX.c:176: memcpy(payload,&rx_buffer[UBX_MESSAGE_PAYLOAD],*RxLength);
      006D10 74 06            [12] 3483 	mov	a,#0x06
      006D12 2A               [12] 3484 	add	a,r2
      006D13 F8               [12] 3485 	mov	r0,a
      006D14 E4               [12] 3486 	clr	a
      006D15 3B               [12] 3487 	addc	a,r3
      006D16 FF               [12] 3488 	mov	r7,a
      006D17 7C 00            [12] 3489 	mov	r4,#0x00
      006D19 C0 02            [24] 3490 	push	ar2
      006D1B C0 03            [24] 3491 	push	ar3
      006D1D 85 53 82         [24] 3492 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_sloc5_1_0
      006D20 85 54 83         [24] 3493 	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_sloc5_1_0 + 1)
      006D23 85 55 F0         [24] 3494 	mov	b,(_UBLOX_GPS_SendCommand_WaitACK_sloc5_1_0 + 2)
      006D26 12 8D 8E         [24] 3495 	lcall	__gptrget
      006D29 F9               [12] 3496 	mov	r1,a
      006D2A A3               [24] 3497 	inc	dptr
      006D2B 12 8D 8E         [24] 3498 	lcall	__gptrget
      006D2E FB               [12] 3499 	mov	r3,a
      006D2F 90 04 6D         [24] 3500 	mov	dptr,#_memcpy_PARM_2
      006D32 E8               [12] 3501 	mov	a,r0
      006D33 F0               [24] 3502 	movx	@dptr,a
      006D34 EF               [12] 3503 	mov	a,r7
      006D35 A3               [24] 3504 	inc	dptr
      006D36 F0               [24] 3505 	movx	@dptr,a
      006D37 EC               [12] 3506 	mov	a,r4
      006D38 A3               [24] 3507 	inc	dptr
      006D39 F0               [24] 3508 	movx	@dptr,a
      006D3A 90 04 70         [24] 3509 	mov	dptr,#_memcpy_PARM_3
      006D3D E9               [12] 3510 	mov	a,r1
      006D3E F0               [24] 3511 	movx	@dptr,a
      006D3F EB               [12] 3512 	mov	a,r3
      006D40 A3               [24] 3513 	inc	dptr
      006D41 F0               [24] 3514 	movx	@dptr,a
      006D42 85 4C 82         [24] 3515 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0
      006D45 85 4D 83         [24] 3516 	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0 + 1)
      006D48 85 4E F0         [24] 3517 	mov	b,(_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0 + 2)
      006D4B C0 03            [24] 3518 	push	ar3
      006D4D C0 02            [24] 3519 	push	ar2
      006D4F 12 78 14         [24] 3520 	lcall	_memcpy
      006D52 D0 02            [24] 3521 	pop	ar2
      006D54 D0 03            [24] 3522 	pop	ar3
                                   3523 ;	..\src\peripherals\UBLOX.c:177: RetVal = OK;
      006D56 90 04 49         [24] 3524 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_RetVal_65536_212
      006D59 74 01            [12] 3525 	mov	a,#0x01
      006D5B F0               [24] 3526 	movx	@dptr,a
      006D5C D0 03            [24] 3527 	pop	ar3
      006D5E D0 02            [24] 3528 	pop	ar2
      006D60 80 22            [24] 3529 	sjmp	00123$
      006D62                       3530 00117$:
                                   3531 ;	..\src\peripherals\UBLOX.c:183: if(rx_buffer[UBX_MESSAGE_CLASS] == eACK && rx_buffer[UBX_MESSAGE_ID] == ACK) RetVal = ACK;
      006D62 8D 82            [24] 3532 	mov	dpl,r5
      006D64 8E 83            [24] 3533 	mov	dph,r6
      006D66 E0               [24] 3534 	movx	a,@dptr
      006D67 FD               [12] 3535 	mov	r5,a
      006D68 BD 05 14         [24] 3536 	cjne	r5,#0x05,00113$
      006D6B 8A 82            [24] 3537 	mov	dpl,r2
      006D6D 8B 83            [24] 3538 	mov	dph,r3
      006D6F A3               [24] 3539 	inc	dptr
      006D70 A3               [24] 3540 	inc	dptr
      006D71 A3               [24] 3541 	inc	dptr
      006D72 E0               [24] 3542 	movx	a,@dptr
      006D73 FF               [12] 3543 	mov	r7,a
      006D74 BF 01 08         [24] 3544 	cjne	r7,#0x01,00113$
      006D77 90 04 49         [24] 3545 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_RetVal_65536_212
      006D7A 74 01            [12] 3546 	mov	a,#0x01
      006D7C F0               [24] 3547 	movx	@dptr,a
      006D7D 80 05            [24] 3548 	sjmp	00123$
      006D7F                       3549 00113$:
                                   3550 ;	..\src\peripherals\UBLOX.c:184: else RetVal = NAK;
      006D7F 90 04 49         [24] 3551 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_RetVal_65536_212
      006D82 E4               [12] 3552 	clr	a
      006D83 F0               [24] 3553 	movx	@dptr,a
      006D84                       3554 00123$:
                                   3555 ;	..\src\peripherals\UBLOX.c:188: free(buffer_aux);
      006D84 AE 4F            [24] 3556 	mov	r6,_UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0
      006D86 AF 50            [24] 3557 	mov	r7,(_UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0 + 1)
      006D88 7D 00            [12] 3558 	mov	r5,#0x00
      006D8A 8E 82            [24] 3559 	mov	dpl,r6
      006D8C 8F 83            [24] 3560 	mov	dph,r7
      006D8E 8D F0            [24] 3561 	mov	b,r5
      006D90 C0 03            [24] 3562 	push	ar3
      006D92 C0 02            [24] 3563 	push	ar2
      006D94 12 6E 35         [24] 3564 	lcall	_free
      006D97 D0 02            [24] 3565 	pop	ar2
      006D99 D0 03            [24] 3566 	pop	ar3
                                   3567 ;	..\src\peripherals\UBLOX.c:189: free(rx_buffer);
      006D9B 7F 00            [12] 3568 	mov	r7,#0x00
      006D9D 8A 82            [24] 3569 	mov	dpl,r2
      006D9F 8B 83            [24] 3570 	mov	dph,r3
      006DA1 8F F0            [24] 3571 	mov	b,r7
      006DA3 12 6E 35         [24] 3572 	lcall	_free
                                   3573 ;	..\src\peripherals\UBLOX.c:190: return RetVal;
      006DA6 90 04 49         [24] 3574 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_RetVal_65536_212
      006DA9 E0               [24] 3575 	movx	a,@dptr
                                   3576 ;	..\src\peripherals\UBLOX.c:191: }
      006DAA F5 82            [12] 3577 	mov	dpl,a
      006DAC 22               [24] 3578 	ret
                                   3579 	.area CSEG    (CODE)
                                   3580 	.area CONST   (CODE)
                                   3581 	.area CONST   (CODE)
      00930E                       3582 ___str_0:
      00930E 20 47 50 53 20 69 6E  3583 	.ascii " GPS init start "
             69 74 20 73 74 61 72
             74 20
      00931E 00                    3584 	.db 0x00
                                   3585 	.area CSEG    (CODE)
                                   3586 	.area CONST   (CODE)
      00931F                       3587 ___str_1:
      00931F 47 50 53 20 69 6E 69  3588 	.ascii "GPS init END"
             74 20 45 4E 44
      00932B 00                    3589 	.db 0x00
                                   3590 	.area CSEG    (CODE)
                                   3591 	.area XINIT   (CODE)
                                   3592 	.area CABS    (ABS,CODE)
