                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.6.0 #9615 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module UBLOX
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _memcpy
                                     12 	.globl _free
                                     13 	.globl _malloc
                                     14 	.globl _uart_timer1_baud
                                     15 	.globl _uart1_tx
                                     16 	.globl _uart1_rx
                                     17 	.globl _uart1_init
                                     18 	.globl _uart1_rxcount
                                     19 	.globl _delay
                                     20 	.globl _PORTC_7
                                     21 	.globl _PORTC_6
                                     22 	.globl _PORTC_5
                                     23 	.globl _PORTC_4
                                     24 	.globl _PORTC_3
                                     25 	.globl _PORTC_2
                                     26 	.globl _PORTC_1
                                     27 	.globl _PORTC_0
                                     28 	.globl _PORTB_7
                                     29 	.globl _PORTB_6
                                     30 	.globl _PORTB_5
                                     31 	.globl _PORTB_4
                                     32 	.globl _PORTB_3
                                     33 	.globl _PORTB_2
                                     34 	.globl _PORTB_1
                                     35 	.globl _PORTB_0
                                     36 	.globl _PORTA_7
                                     37 	.globl _PORTA_6
                                     38 	.globl _PORTA_5
                                     39 	.globl _PORTA_4
                                     40 	.globl _PORTA_3
                                     41 	.globl _PORTA_2
                                     42 	.globl _PORTA_1
                                     43 	.globl _PORTA_0
                                     44 	.globl _PINC_7
                                     45 	.globl _PINC_6
                                     46 	.globl _PINC_5
                                     47 	.globl _PINC_4
                                     48 	.globl _PINC_3
                                     49 	.globl _PINC_2
                                     50 	.globl _PINC_1
                                     51 	.globl _PINC_0
                                     52 	.globl _PINB_7
                                     53 	.globl _PINB_6
                                     54 	.globl _PINB_5
                                     55 	.globl _PINB_4
                                     56 	.globl _PINB_3
                                     57 	.globl _PINB_2
                                     58 	.globl _PINB_1
                                     59 	.globl _PINB_0
                                     60 	.globl _PINA_7
                                     61 	.globl _PINA_6
                                     62 	.globl _PINA_5
                                     63 	.globl _PINA_4
                                     64 	.globl _PINA_3
                                     65 	.globl _PINA_2
                                     66 	.globl _PINA_1
                                     67 	.globl _PINA_0
                                     68 	.globl _CY
                                     69 	.globl _AC
                                     70 	.globl _F0
                                     71 	.globl _RS1
                                     72 	.globl _RS0
                                     73 	.globl _OV
                                     74 	.globl _F1
                                     75 	.globl _P
                                     76 	.globl _IP_7
                                     77 	.globl _IP_6
                                     78 	.globl _IP_5
                                     79 	.globl _IP_4
                                     80 	.globl _IP_3
                                     81 	.globl _IP_2
                                     82 	.globl _IP_1
                                     83 	.globl _IP_0
                                     84 	.globl _EA
                                     85 	.globl _IE_7
                                     86 	.globl _IE_6
                                     87 	.globl _IE_5
                                     88 	.globl _IE_4
                                     89 	.globl _IE_3
                                     90 	.globl _IE_2
                                     91 	.globl _IE_1
                                     92 	.globl _IE_0
                                     93 	.globl _EIP_7
                                     94 	.globl _EIP_6
                                     95 	.globl _EIP_5
                                     96 	.globl _EIP_4
                                     97 	.globl _EIP_3
                                     98 	.globl _EIP_2
                                     99 	.globl _EIP_1
                                    100 	.globl _EIP_0
                                    101 	.globl _EIE_7
                                    102 	.globl _EIE_6
                                    103 	.globl _EIE_5
                                    104 	.globl _EIE_4
                                    105 	.globl _EIE_3
                                    106 	.globl _EIE_2
                                    107 	.globl _EIE_1
                                    108 	.globl _EIE_0
                                    109 	.globl _E2IP_7
                                    110 	.globl _E2IP_6
                                    111 	.globl _E2IP_5
                                    112 	.globl _E2IP_4
                                    113 	.globl _E2IP_3
                                    114 	.globl _E2IP_2
                                    115 	.globl _E2IP_1
                                    116 	.globl _E2IP_0
                                    117 	.globl _E2IE_7
                                    118 	.globl _E2IE_6
                                    119 	.globl _E2IE_5
                                    120 	.globl _E2IE_4
                                    121 	.globl _E2IE_3
                                    122 	.globl _E2IE_2
                                    123 	.globl _E2IE_1
                                    124 	.globl _E2IE_0
                                    125 	.globl _B_7
                                    126 	.globl _B_6
                                    127 	.globl _B_5
                                    128 	.globl _B_4
                                    129 	.globl _B_3
                                    130 	.globl _B_2
                                    131 	.globl _B_1
                                    132 	.globl _B_0
                                    133 	.globl _ACC_7
                                    134 	.globl _ACC_6
                                    135 	.globl _ACC_5
                                    136 	.globl _ACC_4
                                    137 	.globl _ACC_3
                                    138 	.globl _ACC_2
                                    139 	.globl _ACC_1
                                    140 	.globl _ACC_0
                                    141 	.globl _WTSTAT
                                    142 	.globl _WTIRQEN
                                    143 	.globl _WTEVTD
                                    144 	.globl _WTEVTD1
                                    145 	.globl _WTEVTD0
                                    146 	.globl _WTEVTC
                                    147 	.globl _WTEVTC1
                                    148 	.globl _WTEVTC0
                                    149 	.globl _WTEVTB
                                    150 	.globl _WTEVTB1
                                    151 	.globl _WTEVTB0
                                    152 	.globl _WTEVTA
                                    153 	.globl _WTEVTA1
                                    154 	.globl _WTEVTA0
                                    155 	.globl _WTCNTR1
                                    156 	.globl _WTCNTB
                                    157 	.globl _WTCNTB1
                                    158 	.globl _WTCNTB0
                                    159 	.globl _WTCNTA
                                    160 	.globl _WTCNTA1
                                    161 	.globl _WTCNTA0
                                    162 	.globl _WTCFGB
                                    163 	.globl _WTCFGA
                                    164 	.globl _WDTRESET
                                    165 	.globl _WDTCFG
                                    166 	.globl _U1STATUS
                                    167 	.globl _U1SHREG
                                    168 	.globl _U1MODE
                                    169 	.globl _U1CTRL
                                    170 	.globl _U0STATUS
                                    171 	.globl _U0SHREG
                                    172 	.globl _U0MODE
                                    173 	.globl _U0CTRL
                                    174 	.globl _T2STATUS
                                    175 	.globl _T2PERIOD
                                    176 	.globl _T2PERIOD1
                                    177 	.globl _T2PERIOD0
                                    178 	.globl _T2MODE
                                    179 	.globl _T2CNT
                                    180 	.globl _T2CNT1
                                    181 	.globl _T2CNT0
                                    182 	.globl _T2CLKSRC
                                    183 	.globl _T1STATUS
                                    184 	.globl _T1PERIOD
                                    185 	.globl _T1PERIOD1
                                    186 	.globl _T1PERIOD0
                                    187 	.globl _T1MODE
                                    188 	.globl _T1CNT
                                    189 	.globl _T1CNT1
                                    190 	.globl _T1CNT0
                                    191 	.globl _T1CLKSRC
                                    192 	.globl _T0STATUS
                                    193 	.globl _T0PERIOD
                                    194 	.globl _T0PERIOD1
                                    195 	.globl _T0PERIOD0
                                    196 	.globl _T0MODE
                                    197 	.globl _T0CNT
                                    198 	.globl _T0CNT1
                                    199 	.globl _T0CNT0
                                    200 	.globl _T0CLKSRC
                                    201 	.globl _SPSTATUS
                                    202 	.globl _SPSHREG
                                    203 	.globl _SPMODE
                                    204 	.globl _SPCLKSRC
                                    205 	.globl _RADIOSTAT
                                    206 	.globl _RADIOSTAT1
                                    207 	.globl _RADIOSTAT0
                                    208 	.globl _RADIODATA
                                    209 	.globl _RADIODATA3
                                    210 	.globl _RADIODATA2
                                    211 	.globl _RADIODATA1
                                    212 	.globl _RADIODATA0
                                    213 	.globl _RADIOADDR
                                    214 	.globl _RADIOADDR1
                                    215 	.globl _RADIOADDR0
                                    216 	.globl _RADIOACC
                                    217 	.globl _OC1STATUS
                                    218 	.globl _OC1PIN
                                    219 	.globl _OC1MODE
                                    220 	.globl _OC1COMP
                                    221 	.globl _OC1COMP1
                                    222 	.globl _OC1COMP0
                                    223 	.globl _OC0STATUS
                                    224 	.globl _OC0PIN
                                    225 	.globl _OC0MODE
                                    226 	.globl _OC0COMP
                                    227 	.globl _OC0COMP1
                                    228 	.globl _OC0COMP0
                                    229 	.globl _NVSTATUS
                                    230 	.globl _NVKEY
                                    231 	.globl _NVDATA
                                    232 	.globl _NVDATA1
                                    233 	.globl _NVDATA0
                                    234 	.globl _NVADDR
                                    235 	.globl _NVADDR1
                                    236 	.globl _NVADDR0
                                    237 	.globl _IC1STATUS
                                    238 	.globl _IC1MODE
                                    239 	.globl _IC1CAPT
                                    240 	.globl _IC1CAPT1
                                    241 	.globl _IC1CAPT0
                                    242 	.globl _IC0STATUS
                                    243 	.globl _IC0MODE
                                    244 	.globl _IC0CAPT
                                    245 	.globl _IC0CAPT1
                                    246 	.globl _IC0CAPT0
                                    247 	.globl _PORTR
                                    248 	.globl _PORTC
                                    249 	.globl _PORTB
                                    250 	.globl _PORTA
                                    251 	.globl _PINR
                                    252 	.globl _PINC
                                    253 	.globl _PINB
                                    254 	.globl _PINA
                                    255 	.globl _DIRR
                                    256 	.globl _DIRC
                                    257 	.globl _DIRB
                                    258 	.globl _DIRA
                                    259 	.globl _DBGLNKSTAT
                                    260 	.globl _DBGLNKBUF
                                    261 	.globl _CODECONFIG
                                    262 	.globl _CLKSTAT
                                    263 	.globl _CLKCON
                                    264 	.globl _ANALOGCOMP
                                    265 	.globl _ADCCONV
                                    266 	.globl _ADCCLKSRC
                                    267 	.globl _ADCCH3CONFIG
                                    268 	.globl _ADCCH2CONFIG
                                    269 	.globl _ADCCH1CONFIG
                                    270 	.globl _ADCCH0CONFIG
                                    271 	.globl __XPAGE
                                    272 	.globl _XPAGE
                                    273 	.globl _SP
                                    274 	.globl _PSW
                                    275 	.globl _PCON
                                    276 	.globl _IP
                                    277 	.globl _IE
                                    278 	.globl _EIP
                                    279 	.globl _EIE
                                    280 	.globl _E2IP
                                    281 	.globl _E2IE
                                    282 	.globl _DPS
                                    283 	.globl _DPTR1
                                    284 	.globl _DPTR0
                                    285 	.globl _DPL1
                                    286 	.globl _DPL
                                    287 	.globl _DPH1
                                    288 	.globl _DPH
                                    289 	.globl _B
                                    290 	.globl _ACC
                                    291 	.globl _UBLOX_GPS_SendCommand_WaitACK_PARM_6
                                    292 	.globl _UBLOX_GPS_SendCommand_WaitACK_PARM_5
                                    293 	.globl _UBLOX_GPS_SendCommand_WaitACK_PARM_4
                                    294 	.globl _UBLOX_GPS_SendCommand_WaitACK_PARM_3
                                    295 	.globl _UBLOX_GPS_SendCommand_WaitACK_PARM_2
                                    296 	.globl _UBLOX_GPS_FletcherChecksum8_PARM_4
                                    297 	.globl _UBLOX_GPS_FletcherChecksum8_PARM_3
                                    298 	.globl _UBLOX_GPS_FletcherChecksum8_PARM_2
                                    299 	.globl _AX5043_TIMEGAIN3NB
                                    300 	.globl _AX5043_TIMEGAIN2NB
                                    301 	.globl _AX5043_TIMEGAIN1NB
                                    302 	.globl _AX5043_TIMEGAIN0NB
                                    303 	.globl _AX5043_RXPARAMSETSNB
                                    304 	.globl _AX5043_RXPARAMCURSETNB
                                    305 	.globl _AX5043_PKTMAXLENNB
                                    306 	.globl _AX5043_PKTLENOFFSETNB
                                    307 	.globl _AX5043_PKTLENCFGNB
                                    308 	.globl _AX5043_PKTADDRMASK3NB
                                    309 	.globl _AX5043_PKTADDRMASK2NB
                                    310 	.globl _AX5043_PKTADDRMASK1NB
                                    311 	.globl _AX5043_PKTADDRMASK0NB
                                    312 	.globl _AX5043_PKTADDRCFGNB
                                    313 	.globl _AX5043_PKTADDR3NB
                                    314 	.globl _AX5043_PKTADDR2NB
                                    315 	.globl _AX5043_PKTADDR1NB
                                    316 	.globl _AX5043_PKTADDR0NB
                                    317 	.globl _AX5043_PHASEGAIN3NB
                                    318 	.globl _AX5043_PHASEGAIN2NB
                                    319 	.globl _AX5043_PHASEGAIN1NB
                                    320 	.globl _AX5043_PHASEGAIN0NB
                                    321 	.globl _AX5043_FREQUENCYLEAKNB
                                    322 	.globl _AX5043_FREQUENCYGAIND3NB
                                    323 	.globl _AX5043_FREQUENCYGAIND2NB
                                    324 	.globl _AX5043_FREQUENCYGAIND1NB
                                    325 	.globl _AX5043_FREQUENCYGAIND0NB
                                    326 	.globl _AX5043_FREQUENCYGAINC3NB
                                    327 	.globl _AX5043_FREQUENCYGAINC2NB
                                    328 	.globl _AX5043_FREQUENCYGAINC1NB
                                    329 	.globl _AX5043_FREQUENCYGAINC0NB
                                    330 	.globl _AX5043_FREQUENCYGAINB3NB
                                    331 	.globl _AX5043_FREQUENCYGAINB2NB
                                    332 	.globl _AX5043_FREQUENCYGAINB1NB
                                    333 	.globl _AX5043_FREQUENCYGAINB0NB
                                    334 	.globl _AX5043_FREQUENCYGAINA3NB
                                    335 	.globl _AX5043_FREQUENCYGAINA2NB
                                    336 	.globl _AX5043_FREQUENCYGAINA1NB
                                    337 	.globl _AX5043_FREQUENCYGAINA0NB
                                    338 	.globl _AX5043_FREQDEV13NB
                                    339 	.globl _AX5043_FREQDEV12NB
                                    340 	.globl _AX5043_FREQDEV11NB
                                    341 	.globl _AX5043_FREQDEV10NB
                                    342 	.globl _AX5043_FREQDEV03NB
                                    343 	.globl _AX5043_FREQDEV02NB
                                    344 	.globl _AX5043_FREQDEV01NB
                                    345 	.globl _AX5043_FREQDEV00NB
                                    346 	.globl _AX5043_FOURFSK3NB
                                    347 	.globl _AX5043_FOURFSK2NB
                                    348 	.globl _AX5043_FOURFSK1NB
                                    349 	.globl _AX5043_FOURFSK0NB
                                    350 	.globl _AX5043_DRGAIN3NB
                                    351 	.globl _AX5043_DRGAIN2NB
                                    352 	.globl _AX5043_DRGAIN1NB
                                    353 	.globl _AX5043_DRGAIN0NB
                                    354 	.globl _AX5043_BBOFFSRES3NB
                                    355 	.globl _AX5043_BBOFFSRES2NB
                                    356 	.globl _AX5043_BBOFFSRES1NB
                                    357 	.globl _AX5043_BBOFFSRES0NB
                                    358 	.globl _AX5043_AMPLITUDEGAIN3NB
                                    359 	.globl _AX5043_AMPLITUDEGAIN2NB
                                    360 	.globl _AX5043_AMPLITUDEGAIN1NB
                                    361 	.globl _AX5043_AMPLITUDEGAIN0NB
                                    362 	.globl _AX5043_AGCTARGET3NB
                                    363 	.globl _AX5043_AGCTARGET2NB
                                    364 	.globl _AX5043_AGCTARGET1NB
                                    365 	.globl _AX5043_AGCTARGET0NB
                                    366 	.globl _AX5043_AGCMINMAX3NB
                                    367 	.globl _AX5043_AGCMINMAX2NB
                                    368 	.globl _AX5043_AGCMINMAX1NB
                                    369 	.globl _AX5043_AGCMINMAX0NB
                                    370 	.globl _AX5043_AGCGAIN3NB
                                    371 	.globl _AX5043_AGCGAIN2NB
                                    372 	.globl _AX5043_AGCGAIN1NB
                                    373 	.globl _AX5043_AGCGAIN0NB
                                    374 	.globl _AX5043_AGCAHYST3NB
                                    375 	.globl _AX5043_AGCAHYST2NB
                                    376 	.globl _AX5043_AGCAHYST1NB
                                    377 	.globl _AX5043_AGCAHYST0NB
                                    378 	.globl _AX5043_0xF44NB
                                    379 	.globl _AX5043_0xF35NB
                                    380 	.globl _AX5043_0xF34NB
                                    381 	.globl _AX5043_0xF33NB
                                    382 	.globl _AX5043_0xF32NB
                                    383 	.globl _AX5043_0xF31NB
                                    384 	.globl _AX5043_0xF30NB
                                    385 	.globl _AX5043_0xF26NB
                                    386 	.globl _AX5043_0xF23NB
                                    387 	.globl _AX5043_0xF22NB
                                    388 	.globl _AX5043_0xF21NB
                                    389 	.globl _AX5043_0xF1CNB
                                    390 	.globl _AX5043_0xF18NB
                                    391 	.globl _AX5043_0xF0CNB
                                    392 	.globl _AX5043_0xF00NB
                                    393 	.globl _AX5043_XTALSTATUSNB
                                    394 	.globl _AX5043_XTALOSCNB
                                    395 	.globl _AX5043_XTALCAPNB
                                    396 	.globl _AX5043_XTALAMPLNB
                                    397 	.globl _AX5043_WAKEUPXOEARLYNB
                                    398 	.globl _AX5043_WAKEUPTIMER1NB
                                    399 	.globl _AX5043_WAKEUPTIMER0NB
                                    400 	.globl _AX5043_WAKEUPFREQ1NB
                                    401 	.globl _AX5043_WAKEUPFREQ0NB
                                    402 	.globl _AX5043_WAKEUP1NB
                                    403 	.globl _AX5043_WAKEUP0NB
                                    404 	.globl _AX5043_TXRATE2NB
                                    405 	.globl _AX5043_TXRATE1NB
                                    406 	.globl _AX5043_TXRATE0NB
                                    407 	.globl _AX5043_TXPWRCOEFFE1NB
                                    408 	.globl _AX5043_TXPWRCOEFFE0NB
                                    409 	.globl _AX5043_TXPWRCOEFFD1NB
                                    410 	.globl _AX5043_TXPWRCOEFFD0NB
                                    411 	.globl _AX5043_TXPWRCOEFFC1NB
                                    412 	.globl _AX5043_TXPWRCOEFFC0NB
                                    413 	.globl _AX5043_TXPWRCOEFFB1NB
                                    414 	.globl _AX5043_TXPWRCOEFFB0NB
                                    415 	.globl _AX5043_TXPWRCOEFFA1NB
                                    416 	.globl _AX5043_TXPWRCOEFFA0NB
                                    417 	.globl _AX5043_TRKRFFREQ2NB
                                    418 	.globl _AX5043_TRKRFFREQ1NB
                                    419 	.globl _AX5043_TRKRFFREQ0NB
                                    420 	.globl _AX5043_TRKPHASE1NB
                                    421 	.globl _AX5043_TRKPHASE0NB
                                    422 	.globl _AX5043_TRKFSKDEMOD1NB
                                    423 	.globl _AX5043_TRKFSKDEMOD0NB
                                    424 	.globl _AX5043_TRKFREQ1NB
                                    425 	.globl _AX5043_TRKFREQ0NB
                                    426 	.globl _AX5043_TRKDATARATE2NB
                                    427 	.globl _AX5043_TRKDATARATE1NB
                                    428 	.globl _AX5043_TRKDATARATE0NB
                                    429 	.globl _AX5043_TRKAMPLITUDE1NB
                                    430 	.globl _AX5043_TRKAMPLITUDE0NB
                                    431 	.globl _AX5043_TRKAFSKDEMOD1NB
                                    432 	.globl _AX5043_TRKAFSKDEMOD0NB
                                    433 	.globl _AX5043_TMGTXSETTLENB
                                    434 	.globl _AX5043_TMGTXBOOSTNB
                                    435 	.globl _AX5043_TMGRXSETTLENB
                                    436 	.globl _AX5043_TMGRXRSSINB
                                    437 	.globl _AX5043_TMGRXPREAMBLE3NB
                                    438 	.globl _AX5043_TMGRXPREAMBLE2NB
                                    439 	.globl _AX5043_TMGRXPREAMBLE1NB
                                    440 	.globl _AX5043_TMGRXOFFSACQNB
                                    441 	.globl _AX5043_TMGRXCOARSEAGCNB
                                    442 	.globl _AX5043_TMGRXBOOSTNB
                                    443 	.globl _AX5043_TMGRXAGCNB
                                    444 	.globl _AX5043_TIMER2NB
                                    445 	.globl _AX5043_TIMER1NB
                                    446 	.globl _AX5043_TIMER0NB
                                    447 	.globl _AX5043_SILICONREVISIONNB
                                    448 	.globl _AX5043_SCRATCHNB
                                    449 	.globl _AX5043_RXDATARATE2NB
                                    450 	.globl _AX5043_RXDATARATE1NB
                                    451 	.globl _AX5043_RXDATARATE0NB
                                    452 	.globl _AX5043_RSSIREFERENCENB
                                    453 	.globl _AX5043_RSSIABSTHRNB
                                    454 	.globl _AX5043_RSSINB
                                    455 	.globl _AX5043_REFNB
                                    456 	.globl _AX5043_RADIOSTATENB
                                    457 	.globl _AX5043_RADIOEVENTREQ1NB
                                    458 	.globl _AX5043_RADIOEVENTREQ0NB
                                    459 	.globl _AX5043_RADIOEVENTMASK1NB
                                    460 	.globl _AX5043_RADIOEVENTMASK0NB
                                    461 	.globl _AX5043_PWRMODENB
                                    462 	.globl _AX5043_PWRAMPNB
                                    463 	.globl _AX5043_POWSTICKYSTATNB
                                    464 	.globl _AX5043_POWSTATNB
                                    465 	.globl _AX5043_POWIRQMASKNB
                                    466 	.globl _AX5043_POWCTRL1NB
                                    467 	.globl _AX5043_PLLVCOIRNB
                                    468 	.globl _AX5043_PLLVCOINB
                                    469 	.globl _AX5043_PLLVCODIVNB
                                    470 	.globl _AX5043_PLLRNGCLKNB
                                    471 	.globl _AX5043_PLLRANGINGBNB
                                    472 	.globl _AX5043_PLLRANGINGANB
                                    473 	.globl _AX5043_PLLLOOPBOOSTNB
                                    474 	.globl _AX5043_PLLLOOPNB
                                    475 	.globl _AX5043_PLLLOCKDETNB
                                    476 	.globl _AX5043_PLLCPIBOOSTNB
                                    477 	.globl _AX5043_PLLCPINB
                                    478 	.globl _AX5043_PKTSTOREFLAGSNB
                                    479 	.globl _AX5043_PKTMISCFLAGSNB
                                    480 	.globl _AX5043_PKTCHUNKSIZENB
                                    481 	.globl _AX5043_PKTACCEPTFLAGSNB
                                    482 	.globl _AX5043_PINSTATENB
                                    483 	.globl _AX5043_PINFUNCSYSCLKNB
                                    484 	.globl _AX5043_PINFUNCPWRAMPNB
                                    485 	.globl _AX5043_PINFUNCIRQNB
                                    486 	.globl _AX5043_PINFUNCDCLKNB
                                    487 	.globl _AX5043_PINFUNCDATANB
                                    488 	.globl _AX5043_PINFUNCANTSELNB
                                    489 	.globl _AX5043_MODULATIONNB
                                    490 	.globl _AX5043_MODCFGPNB
                                    491 	.globl _AX5043_MODCFGFNB
                                    492 	.globl _AX5043_MODCFGANB
                                    493 	.globl _AX5043_MAXRFOFFSET2NB
                                    494 	.globl _AX5043_MAXRFOFFSET1NB
                                    495 	.globl _AX5043_MAXRFOFFSET0NB
                                    496 	.globl _AX5043_MAXDROFFSET2NB
                                    497 	.globl _AX5043_MAXDROFFSET1NB
                                    498 	.globl _AX5043_MAXDROFFSET0NB
                                    499 	.globl _AX5043_MATCH1PAT1NB
                                    500 	.globl _AX5043_MATCH1PAT0NB
                                    501 	.globl _AX5043_MATCH1MINNB
                                    502 	.globl _AX5043_MATCH1MAXNB
                                    503 	.globl _AX5043_MATCH1LENNB
                                    504 	.globl _AX5043_MATCH0PAT3NB
                                    505 	.globl _AX5043_MATCH0PAT2NB
                                    506 	.globl _AX5043_MATCH0PAT1NB
                                    507 	.globl _AX5043_MATCH0PAT0NB
                                    508 	.globl _AX5043_MATCH0MINNB
                                    509 	.globl _AX5043_MATCH0MAXNB
                                    510 	.globl _AX5043_MATCH0LENNB
                                    511 	.globl _AX5043_LPOSCSTATUSNB
                                    512 	.globl _AX5043_LPOSCREF1NB
                                    513 	.globl _AX5043_LPOSCREF0NB
                                    514 	.globl _AX5043_LPOSCPER1NB
                                    515 	.globl _AX5043_LPOSCPER0NB
                                    516 	.globl _AX5043_LPOSCKFILT1NB
                                    517 	.globl _AX5043_LPOSCKFILT0NB
                                    518 	.globl _AX5043_LPOSCFREQ1NB
                                    519 	.globl _AX5043_LPOSCFREQ0NB
                                    520 	.globl _AX5043_LPOSCCONFIGNB
                                    521 	.globl _AX5043_IRQREQUEST1NB
                                    522 	.globl _AX5043_IRQREQUEST0NB
                                    523 	.globl _AX5043_IRQMASK1NB
                                    524 	.globl _AX5043_IRQMASK0NB
                                    525 	.globl _AX5043_IRQINVERSION1NB
                                    526 	.globl _AX5043_IRQINVERSION0NB
                                    527 	.globl _AX5043_IFFREQ1NB
                                    528 	.globl _AX5043_IFFREQ0NB
                                    529 	.globl _AX5043_GPADCPERIODNB
                                    530 	.globl _AX5043_GPADCCTRLNB
                                    531 	.globl _AX5043_GPADC13VALUE1NB
                                    532 	.globl _AX5043_GPADC13VALUE0NB
                                    533 	.globl _AX5043_FSKDMIN1NB
                                    534 	.globl _AX5043_FSKDMIN0NB
                                    535 	.globl _AX5043_FSKDMAX1NB
                                    536 	.globl _AX5043_FSKDMAX0NB
                                    537 	.globl _AX5043_FSKDEV2NB
                                    538 	.globl _AX5043_FSKDEV1NB
                                    539 	.globl _AX5043_FSKDEV0NB
                                    540 	.globl _AX5043_FREQB3NB
                                    541 	.globl _AX5043_FREQB2NB
                                    542 	.globl _AX5043_FREQB1NB
                                    543 	.globl _AX5043_FREQB0NB
                                    544 	.globl _AX5043_FREQA3NB
                                    545 	.globl _AX5043_FREQA2NB
                                    546 	.globl _AX5043_FREQA1NB
                                    547 	.globl _AX5043_FREQA0NB
                                    548 	.globl _AX5043_FRAMINGNB
                                    549 	.globl _AX5043_FIFOTHRESH1NB
                                    550 	.globl _AX5043_FIFOTHRESH0NB
                                    551 	.globl _AX5043_FIFOSTATNB
                                    552 	.globl _AX5043_FIFOFREE1NB
                                    553 	.globl _AX5043_FIFOFREE0NB
                                    554 	.globl _AX5043_FIFODATANB
                                    555 	.globl _AX5043_FIFOCOUNT1NB
                                    556 	.globl _AX5043_FIFOCOUNT0NB
                                    557 	.globl _AX5043_FECSYNCNB
                                    558 	.globl _AX5043_FECSTATUSNB
                                    559 	.globl _AX5043_FECNB
                                    560 	.globl _AX5043_ENCODINGNB
                                    561 	.globl _AX5043_DIVERSITYNB
                                    562 	.globl _AX5043_DECIMATIONNB
                                    563 	.globl _AX5043_DACVALUE1NB
                                    564 	.globl _AX5043_DACVALUE0NB
                                    565 	.globl _AX5043_DACCONFIGNB
                                    566 	.globl _AX5043_CRCINIT3NB
                                    567 	.globl _AX5043_CRCINIT2NB
                                    568 	.globl _AX5043_CRCINIT1NB
                                    569 	.globl _AX5043_CRCINIT0NB
                                    570 	.globl _AX5043_BGNDRSSITHRNB
                                    571 	.globl _AX5043_BGNDRSSIGAINNB
                                    572 	.globl _AX5043_BGNDRSSINB
                                    573 	.globl _AX5043_BBTUNENB
                                    574 	.globl _AX5043_BBOFFSCAPNB
                                    575 	.globl _AX5043_AMPLFILTERNB
                                    576 	.globl _AX5043_AGCCOUNTERNB
                                    577 	.globl _AX5043_AFSKSPACE1NB
                                    578 	.globl _AX5043_AFSKSPACE0NB
                                    579 	.globl _AX5043_AFSKMARK1NB
                                    580 	.globl _AX5043_AFSKMARK0NB
                                    581 	.globl _AX5043_AFSKCTRLNB
                                    582 	.globl _AX5043_TIMEGAIN3
                                    583 	.globl _AX5043_TIMEGAIN2
                                    584 	.globl _AX5043_TIMEGAIN1
                                    585 	.globl _AX5043_TIMEGAIN0
                                    586 	.globl _AX5043_RXPARAMSETS
                                    587 	.globl _AX5043_RXPARAMCURSET
                                    588 	.globl _AX5043_PKTMAXLEN
                                    589 	.globl _AX5043_PKTLENOFFSET
                                    590 	.globl _AX5043_PKTLENCFG
                                    591 	.globl _AX5043_PKTADDRMASK3
                                    592 	.globl _AX5043_PKTADDRMASK2
                                    593 	.globl _AX5043_PKTADDRMASK1
                                    594 	.globl _AX5043_PKTADDRMASK0
                                    595 	.globl _AX5043_PKTADDRCFG
                                    596 	.globl _AX5043_PKTADDR3
                                    597 	.globl _AX5043_PKTADDR2
                                    598 	.globl _AX5043_PKTADDR1
                                    599 	.globl _AX5043_PKTADDR0
                                    600 	.globl _AX5043_PHASEGAIN3
                                    601 	.globl _AX5043_PHASEGAIN2
                                    602 	.globl _AX5043_PHASEGAIN1
                                    603 	.globl _AX5043_PHASEGAIN0
                                    604 	.globl _AX5043_FREQUENCYLEAK
                                    605 	.globl _AX5043_FREQUENCYGAIND3
                                    606 	.globl _AX5043_FREQUENCYGAIND2
                                    607 	.globl _AX5043_FREQUENCYGAIND1
                                    608 	.globl _AX5043_FREQUENCYGAIND0
                                    609 	.globl _AX5043_FREQUENCYGAINC3
                                    610 	.globl _AX5043_FREQUENCYGAINC2
                                    611 	.globl _AX5043_FREQUENCYGAINC1
                                    612 	.globl _AX5043_FREQUENCYGAINC0
                                    613 	.globl _AX5043_FREQUENCYGAINB3
                                    614 	.globl _AX5043_FREQUENCYGAINB2
                                    615 	.globl _AX5043_FREQUENCYGAINB1
                                    616 	.globl _AX5043_FREQUENCYGAINB0
                                    617 	.globl _AX5043_FREQUENCYGAINA3
                                    618 	.globl _AX5043_FREQUENCYGAINA2
                                    619 	.globl _AX5043_FREQUENCYGAINA1
                                    620 	.globl _AX5043_FREQUENCYGAINA0
                                    621 	.globl _AX5043_FREQDEV13
                                    622 	.globl _AX5043_FREQDEV12
                                    623 	.globl _AX5043_FREQDEV11
                                    624 	.globl _AX5043_FREQDEV10
                                    625 	.globl _AX5043_FREQDEV03
                                    626 	.globl _AX5043_FREQDEV02
                                    627 	.globl _AX5043_FREQDEV01
                                    628 	.globl _AX5043_FREQDEV00
                                    629 	.globl _AX5043_FOURFSK3
                                    630 	.globl _AX5043_FOURFSK2
                                    631 	.globl _AX5043_FOURFSK1
                                    632 	.globl _AX5043_FOURFSK0
                                    633 	.globl _AX5043_DRGAIN3
                                    634 	.globl _AX5043_DRGAIN2
                                    635 	.globl _AX5043_DRGAIN1
                                    636 	.globl _AX5043_DRGAIN0
                                    637 	.globl _AX5043_BBOFFSRES3
                                    638 	.globl _AX5043_BBOFFSRES2
                                    639 	.globl _AX5043_BBOFFSRES1
                                    640 	.globl _AX5043_BBOFFSRES0
                                    641 	.globl _AX5043_AMPLITUDEGAIN3
                                    642 	.globl _AX5043_AMPLITUDEGAIN2
                                    643 	.globl _AX5043_AMPLITUDEGAIN1
                                    644 	.globl _AX5043_AMPLITUDEGAIN0
                                    645 	.globl _AX5043_AGCTARGET3
                                    646 	.globl _AX5043_AGCTARGET2
                                    647 	.globl _AX5043_AGCTARGET1
                                    648 	.globl _AX5043_AGCTARGET0
                                    649 	.globl _AX5043_AGCMINMAX3
                                    650 	.globl _AX5043_AGCMINMAX2
                                    651 	.globl _AX5043_AGCMINMAX1
                                    652 	.globl _AX5043_AGCMINMAX0
                                    653 	.globl _AX5043_AGCGAIN3
                                    654 	.globl _AX5043_AGCGAIN2
                                    655 	.globl _AX5043_AGCGAIN1
                                    656 	.globl _AX5043_AGCGAIN0
                                    657 	.globl _AX5043_AGCAHYST3
                                    658 	.globl _AX5043_AGCAHYST2
                                    659 	.globl _AX5043_AGCAHYST1
                                    660 	.globl _AX5043_AGCAHYST0
                                    661 	.globl _AX5043_0xF44
                                    662 	.globl _AX5043_0xF35
                                    663 	.globl _AX5043_0xF34
                                    664 	.globl _AX5043_0xF33
                                    665 	.globl _AX5043_0xF32
                                    666 	.globl _AX5043_0xF31
                                    667 	.globl _AX5043_0xF30
                                    668 	.globl _AX5043_0xF26
                                    669 	.globl _AX5043_0xF23
                                    670 	.globl _AX5043_0xF22
                                    671 	.globl _AX5043_0xF21
                                    672 	.globl _AX5043_0xF1C
                                    673 	.globl _AX5043_0xF18
                                    674 	.globl _AX5043_0xF0C
                                    675 	.globl _AX5043_0xF00
                                    676 	.globl _AX5043_XTALSTATUS
                                    677 	.globl _AX5043_XTALOSC
                                    678 	.globl _AX5043_XTALCAP
                                    679 	.globl _AX5043_XTALAMPL
                                    680 	.globl _AX5043_WAKEUPXOEARLY
                                    681 	.globl _AX5043_WAKEUPTIMER1
                                    682 	.globl _AX5043_WAKEUPTIMER0
                                    683 	.globl _AX5043_WAKEUPFREQ1
                                    684 	.globl _AX5043_WAKEUPFREQ0
                                    685 	.globl _AX5043_WAKEUP1
                                    686 	.globl _AX5043_WAKEUP0
                                    687 	.globl _AX5043_TXRATE2
                                    688 	.globl _AX5043_TXRATE1
                                    689 	.globl _AX5043_TXRATE0
                                    690 	.globl _AX5043_TXPWRCOEFFE1
                                    691 	.globl _AX5043_TXPWRCOEFFE0
                                    692 	.globl _AX5043_TXPWRCOEFFD1
                                    693 	.globl _AX5043_TXPWRCOEFFD0
                                    694 	.globl _AX5043_TXPWRCOEFFC1
                                    695 	.globl _AX5043_TXPWRCOEFFC0
                                    696 	.globl _AX5043_TXPWRCOEFFB1
                                    697 	.globl _AX5043_TXPWRCOEFFB0
                                    698 	.globl _AX5043_TXPWRCOEFFA1
                                    699 	.globl _AX5043_TXPWRCOEFFA0
                                    700 	.globl _AX5043_TRKRFFREQ2
                                    701 	.globl _AX5043_TRKRFFREQ1
                                    702 	.globl _AX5043_TRKRFFREQ0
                                    703 	.globl _AX5043_TRKPHASE1
                                    704 	.globl _AX5043_TRKPHASE0
                                    705 	.globl _AX5043_TRKFSKDEMOD1
                                    706 	.globl _AX5043_TRKFSKDEMOD0
                                    707 	.globl _AX5043_TRKFREQ1
                                    708 	.globl _AX5043_TRKFREQ0
                                    709 	.globl _AX5043_TRKDATARATE2
                                    710 	.globl _AX5043_TRKDATARATE1
                                    711 	.globl _AX5043_TRKDATARATE0
                                    712 	.globl _AX5043_TRKAMPLITUDE1
                                    713 	.globl _AX5043_TRKAMPLITUDE0
                                    714 	.globl _AX5043_TRKAFSKDEMOD1
                                    715 	.globl _AX5043_TRKAFSKDEMOD0
                                    716 	.globl _AX5043_TMGTXSETTLE
                                    717 	.globl _AX5043_TMGTXBOOST
                                    718 	.globl _AX5043_TMGRXSETTLE
                                    719 	.globl _AX5043_TMGRXRSSI
                                    720 	.globl _AX5043_TMGRXPREAMBLE3
                                    721 	.globl _AX5043_TMGRXPREAMBLE2
                                    722 	.globl _AX5043_TMGRXPREAMBLE1
                                    723 	.globl _AX5043_TMGRXOFFSACQ
                                    724 	.globl _AX5043_TMGRXCOARSEAGC
                                    725 	.globl _AX5043_TMGRXBOOST
                                    726 	.globl _AX5043_TMGRXAGC
                                    727 	.globl _AX5043_TIMER2
                                    728 	.globl _AX5043_TIMER1
                                    729 	.globl _AX5043_TIMER0
                                    730 	.globl _AX5043_SILICONREVISION
                                    731 	.globl _AX5043_SCRATCH
                                    732 	.globl _AX5043_RXDATARATE2
                                    733 	.globl _AX5043_RXDATARATE1
                                    734 	.globl _AX5043_RXDATARATE0
                                    735 	.globl _AX5043_RSSIREFERENCE
                                    736 	.globl _AX5043_RSSIABSTHR
                                    737 	.globl _AX5043_RSSI
                                    738 	.globl _AX5043_REF
                                    739 	.globl _AX5043_RADIOSTATE
                                    740 	.globl _AX5043_RADIOEVENTREQ1
                                    741 	.globl _AX5043_RADIOEVENTREQ0
                                    742 	.globl _AX5043_RADIOEVENTMASK1
                                    743 	.globl _AX5043_RADIOEVENTMASK0
                                    744 	.globl _AX5043_PWRMODE
                                    745 	.globl _AX5043_PWRAMP
                                    746 	.globl _AX5043_POWSTICKYSTAT
                                    747 	.globl _AX5043_POWSTAT
                                    748 	.globl _AX5043_POWIRQMASK
                                    749 	.globl _AX5043_POWCTRL1
                                    750 	.globl _AX5043_PLLVCOIR
                                    751 	.globl _AX5043_PLLVCOI
                                    752 	.globl _AX5043_PLLVCODIV
                                    753 	.globl _AX5043_PLLRNGCLK
                                    754 	.globl _AX5043_PLLRANGINGB
                                    755 	.globl _AX5043_PLLRANGINGA
                                    756 	.globl _AX5043_PLLLOOPBOOST
                                    757 	.globl _AX5043_PLLLOOP
                                    758 	.globl _AX5043_PLLLOCKDET
                                    759 	.globl _AX5043_PLLCPIBOOST
                                    760 	.globl _AX5043_PLLCPI
                                    761 	.globl _AX5043_PKTSTOREFLAGS
                                    762 	.globl _AX5043_PKTMISCFLAGS
                                    763 	.globl _AX5043_PKTCHUNKSIZE
                                    764 	.globl _AX5043_PKTACCEPTFLAGS
                                    765 	.globl _AX5043_PINSTATE
                                    766 	.globl _AX5043_PINFUNCSYSCLK
                                    767 	.globl _AX5043_PINFUNCPWRAMP
                                    768 	.globl _AX5043_PINFUNCIRQ
                                    769 	.globl _AX5043_PINFUNCDCLK
                                    770 	.globl _AX5043_PINFUNCDATA
                                    771 	.globl _AX5043_PINFUNCANTSEL
                                    772 	.globl _AX5043_MODULATION
                                    773 	.globl _AX5043_MODCFGP
                                    774 	.globl _AX5043_MODCFGF
                                    775 	.globl _AX5043_MODCFGA
                                    776 	.globl _AX5043_MAXRFOFFSET2
                                    777 	.globl _AX5043_MAXRFOFFSET1
                                    778 	.globl _AX5043_MAXRFOFFSET0
                                    779 	.globl _AX5043_MAXDROFFSET2
                                    780 	.globl _AX5043_MAXDROFFSET1
                                    781 	.globl _AX5043_MAXDROFFSET0
                                    782 	.globl _AX5043_MATCH1PAT1
                                    783 	.globl _AX5043_MATCH1PAT0
                                    784 	.globl _AX5043_MATCH1MIN
                                    785 	.globl _AX5043_MATCH1MAX
                                    786 	.globl _AX5043_MATCH1LEN
                                    787 	.globl _AX5043_MATCH0PAT3
                                    788 	.globl _AX5043_MATCH0PAT2
                                    789 	.globl _AX5043_MATCH0PAT1
                                    790 	.globl _AX5043_MATCH0PAT0
                                    791 	.globl _AX5043_MATCH0MIN
                                    792 	.globl _AX5043_MATCH0MAX
                                    793 	.globl _AX5043_MATCH0LEN
                                    794 	.globl _AX5043_LPOSCSTATUS
                                    795 	.globl _AX5043_LPOSCREF1
                                    796 	.globl _AX5043_LPOSCREF0
                                    797 	.globl _AX5043_LPOSCPER1
                                    798 	.globl _AX5043_LPOSCPER0
                                    799 	.globl _AX5043_LPOSCKFILT1
                                    800 	.globl _AX5043_LPOSCKFILT0
                                    801 	.globl _AX5043_LPOSCFREQ1
                                    802 	.globl _AX5043_LPOSCFREQ0
                                    803 	.globl _AX5043_LPOSCCONFIG
                                    804 	.globl _AX5043_IRQREQUEST1
                                    805 	.globl _AX5043_IRQREQUEST0
                                    806 	.globl _AX5043_IRQMASK1
                                    807 	.globl _AX5043_IRQMASK0
                                    808 	.globl _AX5043_IRQINVERSION1
                                    809 	.globl _AX5043_IRQINVERSION0
                                    810 	.globl _AX5043_IFFREQ1
                                    811 	.globl _AX5043_IFFREQ0
                                    812 	.globl _AX5043_GPADCPERIOD
                                    813 	.globl _AX5043_GPADCCTRL
                                    814 	.globl _AX5043_GPADC13VALUE1
                                    815 	.globl _AX5043_GPADC13VALUE0
                                    816 	.globl _AX5043_FSKDMIN1
                                    817 	.globl _AX5043_FSKDMIN0
                                    818 	.globl _AX5043_FSKDMAX1
                                    819 	.globl _AX5043_FSKDMAX0
                                    820 	.globl _AX5043_FSKDEV2
                                    821 	.globl _AX5043_FSKDEV1
                                    822 	.globl _AX5043_FSKDEV0
                                    823 	.globl _AX5043_FREQB3
                                    824 	.globl _AX5043_FREQB2
                                    825 	.globl _AX5043_FREQB1
                                    826 	.globl _AX5043_FREQB0
                                    827 	.globl _AX5043_FREQA3
                                    828 	.globl _AX5043_FREQA2
                                    829 	.globl _AX5043_FREQA1
                                    830 	.globl _AX5043_FREQA0
                                    831 	.globl _AX5043_FRAMING
                                    832 	.globl _AX5043_FIFOTHRESH1
                                    833 	.globl _AX5043_FIFOTHRESH0
                                    834 	.globl _AX5043_FIFOSTAT
                                    835 	.globl _AX5043_FIFOFREE1
                                    836 	.globl _AX5043_FIFOFREE0
                                    837 	.globl _AX5043_FIFODATA
                                    838 	.globl _AX5043_FIFOCOUNT1
                                    839 	.globl _AX5043_FIFOCOUNT0
                                    840 	.globl _AX5043_FECSYNC
                                    841 	.globl _AX5043_FECSTATUS
                                    842 	.globl _AX5043_FEC
                                    843 	.globl _AX5043_ENCODING
                                    844 	.globl _AX5043_DIVERSITY
                                    845 	.globl _AX5043_DECIMATION
                                    846 	.globl _AX5043_DACVALUE1
                                    847 	.globl _AX5043_DACVALUE0
                                    848 	.globl _AX5043_DACCONFIG
                                    849 	.globl _AX5043_CRCINIT3
                                    850 	.globl _AX5043_CRCINIT2
                                    851 	.globl _AX5043_CRCINIT1
                                    852 	.globl _AX5043_CRCINIT0
                                    853 	.globl _AX5043_BGNDRSSITHR
                                    854 	.globl _AX5043_BGNDRSSIGAIN
                                    855 	.globl _AX5043_BGNDRSSI
                                    856 	.globl _AX5043_BBTUNE
                                    857 	.globl _AX5043_BBOFFSCAP
                                    858 	.globl _AX5043_AMPLFILTER
                                    859 	.globl _AX5043_AGCCOUNTER
                                    860 	.globl _AX5043_AFSKSPACE1
                                    861 	.globl _AX5043_AFSKSPACE0
                                    862 	.globl _AX5043_AFSKMARK1
                                    863 	.globl _AX5043_AFSKMARK0
                                    864 	.globl _AX5043_AFSKCTRL
                                    865 	.globl _XWTSTAT
                                    866 	.globl _XWTIRQEN
                                    867 	.globl _XWTEVTD
                                    868 	.globl _XWTEVTD1
                                    869 	.globl _XWTEVTD0
                                    870 	.globl _XWTEVTC
                                    871 	.globl _XWTEVTC1
                                    872 	.globl _XWTEVTC0
                                    873 	.globl _XWTEVTB
                                    874 	.globl _XWTEVTB1
                                    875 	.globl _XWTEVTB0
                                    876 	.globl _XWTEVTA
                                    877 	.globl _XWTEVTA1
                                    878 	.globl _XWTEVTA0
                                    879 	.globl _XWTCNTR1
                                    880 	.globl _XWTCNTB
                                    881 	.globl _XWTCNTB1
                                    882 	.globl _XWTCNTB0
                                    883 	.globl _XWTCNTA
                                    884 	.globl _XWTCNTA1
                                    885 	.globl _XWTCNTA0
                                    886 	.globl _XWTCFGB
                                    887 	.globl _XWTCFGA
                                    888 	.globl _XWDTRESET
                                    889 	.globl _XWDTCFG
                                    890 	.globl _XU1STATUS
                                    891 	.globl _XU1SHREG
                                    892 	.globl _XU1MODE
                                    893 	.globl _XU1CTRL
                                    894 	.globl _XU0STATUS
                                    895 	.globl _XU0SHREG
                                    896 	.globl _XU0MODE
                                    897 	.globl _XU0CTRL
                                    898 	.globl _XT2STATUS
                                    899 	.globl _XT2PERIOD
                                    900 	.globl _XT2PERIOD1
                                    901 	.globl _XT2PERIOD0
                                    902 	.globl _XT2MODE
                                    903 	.globl _XT2CNT
                                    904 	.globl _XT2CNT1
                                    905 	.globl _XT2CNT0
                                    906 	.globl _XT2CLKSRC
                                    907 	.globl _XT1STATUS
                                    908 	.globl _XT1PERIOD
                                    909 	.globl _XT1PERIOD1
                                    910 	.globl _XT1PERIOD0
                                    911 	.globl _XT1MODE
                                    912 	.globl _XT1CNT
                                    913 	.globl _XT1CNT1
                                    914 	.globl _XT1CNT0
                                    915 	.globl _XT1CLKSRC
                                    916 	.globl _XT0STATUS
                                    917 	.globl _XT0PERIOD
                                    918 	.globl _XT0PERIOD1
                                    919 	.globl _XT0PERIOD0
                                    920 	.globl _XT0MODE
                                    921 	.globl _XT0CNT
                                    922 	.globl _XT0CNT1
                                    923 	.globl _XT0CNT0
                                    924 	.globl _XT0CLKSRC
                                    925 	.globl _XSPSTATUS
                                    926 	.globl _XSPSHREG
                                    927 	.globl _XSPMODE
                                    928 	.globl _XSPCLKSRC
                                    929 	.globl _XRADIOSTAT
                                    930 	.globl _XRADIOSTAT1
                                    931 	.globl _XRADIOSTAT0
                                    932 	.globl _XRADIODATA3
                                    933 	.globl _XRADIODATA2
                                    934 	.globl _XRADIODATA1
                                    935 	.globl _XRADIODATA0
                                    936 	.globl _XRADIOADDR1
                                    937 	.globl _XRADIOADDR0
                                    938 	.globl _XRADIOACC
                                    939 	.globl _XOC1STATUS
                                    940 	.globl _XOC1PIN
                                    941 	.globl _XOC1MODE
                                    942 	.globl _XOC1COMP
                                    943 	.globl _XOC1COMP1
                                    944 	.globl _XOC1COMP0
                                    945 	.globl _XOC0STATUS
                                    946 	.globl _XOC0PIN
                                    947 	.globl _XOC0MODE
                                    948 	.globl _XOC0COMP
                                    949 	.globl _XOC0COMP1
                                    950 	.globl _XOC0COMP0
                                    951 	.globl _XNVSTATUS
                                    952 	.globl _XNVKEY
                                    953 	.globl _XNVDATA
                                    954 	.globl _XNVDATA1
                                    955 	.globl _XNVDATA0
                                    956 	.globl _XNVADDR
                                    957 	.globl _XNVADDR1
                                    958 	.globl _XNVADDR0
                                    959 	.globl _XIC1STATUS
                                    960 	.globl _XIC1MODE
                                    961 	.globl _XIC1CAPT
                                    962 	.globl _XIC1CAPT1
                                    963 	.globl _XIC1CAPT0
                                    964 	.globl _XIC0STATUS
                                    965 	.globl _XIC0MODE
                                    966 	.globl _XIC0CAPT
                                    967 	.globl _XIC0CAPT1
                                    968 	.globl _XIC0CAPT0
                                    969 	.globl _XPORTR
                                    970 	.globl _XPORTC
                                    971 	.globl _XPORTB
                                    972 	.globl _XPORTA
                                    973 	.globl _XPINR
                                    974 	.globl _XPINC
                                    975 	.globl _XPINB
                                    976 	.globl _XPINA
                                    977 	.globl _XDIRR
                                    978 	.globl _XDIRC
                                    979 	.globl _XDIRB
                                    980 	.globl _XDIRA
                                    981 	.globl _XDBGLNKSTAT
                                    982 	.globl _XDBGLNKBUF
                                    983 	.globl _XCODECONFIG
                                    984 	.globl _XCLKSTAT
                                    985 	.globl _XCLKCON
                                    986 	.globl _XANALOGCOMP
                                    987 	.globl _XADCCONV
                                    988 	.globl _XADCCLKSRC
                                    989 	.globl _XADCCH3CONFIG
                                    990 	.globl _XADCCH2CONFIG
                                    991 	.globl _XADCCH1CONFIG
                                    992 	.globl _XADCCH0CONFIG
                                    993 	.globl _XPCON
                                    994 	.globl _XIP
                                    995 	.globl _XIE
                                    996 	.globl _XDPTR1
                                    997 	.globl _XDPTR0
                                    998 	.globl _XTALREADY
                                    999 	.globl _XTALOSC
                                   1000 	.globl _XTALAMPL
                                   1001 	.globl _SILICONREV
                                   1002 	.globl _SCRATCH3
                                   1003 	.globl _SCRATCH2
                                   1004 	.globl _SCRATCH1
                                   1005 	.globl _SCRATCH0
                                   1006 	.globl _RADIOMUX
                                   1007 	.globl _RADIOFSTATADDR
                                   1008 	.globl _RADIOFSTATADDR1
                                   1009 	.globl _RADIOFSTATADDR0
                                   1010 	.globl _RADIOFDATAADDR
                                   1011 	.globl _RADIOFDATAADDR1
                                   1012 	.globl _RADIOFDATAADDR0
                                   1013 	.globl _OSCRUN
                                   1014 	.globl _OSCREADY
                                   1015 	.globl _OSCFORCERUN
                                   1016 	.globl _OSCCALIB
                                   1017 	.globl _MISCCTRL
                                   1018 	.globl _LPXOSCGM
                                   1019 	.globl _LPOSCREF
                                   1020 	.globl _LPOSCREF1
                                   1021 	.globl _LPOSCREF0
                                   1022 	.globl _LPOSCPER
                                   1023 	.globl _LPOSCPER1
                                   1024 	.globl _LPOSCPER0
                                   1025 	.globl _LPOSCKFILT
                                   1026 	.globl _LPOSCKFILT1
                                   1027 	.globl _LPOSCKFILT0
                                   1028 	.globl _LPOSCFREQ
                                   1029 	.globl _LPOSCFREQ1
                                   1030 	.globl _LPOSCFREQ0
                                   1031 	.globl _LPOSCCONFIG
                                   1032 	.globl _PINSEL
                                   1033 	.globl _PINCHGC
                                   1034 	.globl _PINCHGB
                                   1035 	.globl _PINCHGA
                                   1036 	.globl _PALTRADIO
                                   1037 	.globl _PALTC
                                   1038 	.globl _PALTB
                                   1039 	.globl _PALTA
                                   1040 	.globl _INTCHGC
                                   1041 	.globl _INTCHGB
                                   1042 	.globl _INTCHGA
                                   1043 	.globl _EXTIRQ
                                   1044 	.globl _GPIOENABLE
                                   1045 	.globl _ANALOGA
                                   1046 	.globl _FRCOSCREF
                                   1047 	.globl _FRCOSCREF1
                                   1048 	.globl _FRCOSCREF0
                                   1049 	.globl _FRCOSCPER
                                   1050 	.globl _FRCOSCPER1
                                   1051 	.globl _FRCOSCPER0
                                   1052 	.globl _FRCOSCKFILT
                                   1053 	.globl _FRCOSCKFILT1
                                   1054 	.globl _FRCOSCKFILT0
                                   1055 	.globl _FRCOSCFREQ
                                   1056 	.globl _FRCOSCFREQ1
                                   1057 	.globl _FRCOSCFREQ0
                                   1058 	.globl _FRCOSCCTRL
                                   1059 	.globl _FRCOSCCONFIG
                                   1060 	.globl _DMA1CONFIG
                                   1061 	.globl _DMA1ADDR
                                   1062 	.globl _DMA1ADDR1
                                   1063 	.globl _DMA1ADDR0
                                   1064 	.globl _DMA0CONFIG
                                   1065 	.globl _DMA0ADDR
                                   1066 	.globl _DMA0ADDR1
                                   1067 	.globl _DMA0ADDR0
                                   1068 	.globl _ADCTUNE2
                                   1069 	.globl _ADCTUNE1
                                   1070 	.globl _ADCTUNE0
                                   1071 	.globl _ADCCH3VAL
                                   1072 	.globl _ADCCH3VAL1
                                   1073 	.globl _ADCCH3VAL0
                                   1074 	.globl _ADCCH2VAL
                                   1075 	.globl _ADCCH2VAL1
                                   1076 	.globl _ADCCH2VAL0
                                   1077 	.globl _ADCCH1VAL
                                   1078 	.globl _ADCCH1VAL1
                                   1079 	.globl _ADCCH1VAL0
                                   1080 	.globl _ADCCH0VAL
                                   1081 	.globl _ADCCH0VAL1
                                   1082 	.globl _ADCCH0VAL0
                                   1083 	.globl _aligned_alloc_PARM_2
                                   1084 	.globl _UBLOX_GPS_PortInit
                                   1085 	.globl _UBLOX_GPS_FletcherChecksum8
                                   1086 	.globl _UBLOX_GPS_SendCommand_WaitACK
                                   1087 ;--------------------------------------------------------
                                   1088 ; special function registers
                                   1089 ;--------------------------------------------------------
                                   1090 	.area RSEG    (ABS,DATA)
      000000                       1091 	.org 0x0000
                           0000E0  1092 _ACC	=	0x00e0
                           0000F0  1093 _B	=	0x00f0
                           000083  1094 _DPH	=	0x0083
                           000085  1095 _DPH1	=	0x0085
                           000082  1096 _DPL	=	0x0082
                           000084  1097 _DPL1	=	0x0084
                           008382  1098 _DPTR0	=	0x8382
                           008584  1099 _DPTR1	=	0x8584
                           000086  1100 _DPS	=	0x0086
                           0000A0  1101 _E2IE	=	0x00a0
                           0000C0  1102 _E2IP	=	0x00c0
                           000098  1103 _EIE	=	0x0098
                           0000B0  1104 _EIP	=	0x00b0
                           0000A8  1105 _IE	=	0x00a8
                           0000B8  1106 _IP	=	0x00b8
                           000087  1107 _PCON	=	0x0087
                           0000D0  1108 _PSW	=	0x00d0
                           000081  1109 _SP	=	0x0081
                           0000D9  1110 _XPAGE	=	0x00d9
                           0000D9  1111 __XPAGE	=	0x00d9
                           0000CA  1112 _ADCCH0CONFIG	=	0x00ca
                           0000CB  1113 _ADCCH1CONFIG	=	0x00cb
                           0000D2  1114 _ADCCH2CONFIG	=	0x00d2
                           0000D3  1115 _ADCCH3CONFIG	=	0x00d3
                           0000D1  1116 _ADCCLKSRC	=	0x00d1
                           0000C9  1117 _ADCCONV	=	0x00c9
                           0000E1  1118 _ANALOGCOMP	=	0x00e1
                           0000C6  1119 _CLKCON	=	0x00c6
                           0000C7  1120 _CLKSTAT	=	0x00c7
                           000097  1121 _CODECONFIG	=	0x0097
                           0000E3  1122 _DBGLNKBUF	=	0x00e3
                           0000E2  1123 _DBGLNKSTAT	=	0x00e2
                           000089  1124 _DIRA	=	0x0089
                           00008A  1125 _DIRB	=	0x008a
                           00008B  1126 _DIRC	=	0x008b
                           00008E  1127 _DIRR	=	0x008e
                           0000C8  1128 _PINA	=	0x00c8
                           0000E8  1129 _PINB	=	0x00e8
                           0000F8  1130 _PINC	=	0x00f8
                           00008D  1131 _PINR	=	0x008d
                           000080  1132 _PORTA	=	0x0080
                           000088  1133 _PORTB	=	0x0088
                           000090  1134 _PORTC	=	0x0090
                           00008C  1135 _PORTR	=	0x008c
                           0000CE  1136 _IC0CAPT0	=	0x00ce
                           0000CF  1137 _IC0CAPT1	=	0x00cf
                           00CFCE  1138 _IC0CAPT	=	0xcfce
                           0000CC  1139 _IC0MODE	=	0x00cc
                           0000CD  1140 _IC0STATUS	=	0x00cd
                           0000D6  1141 _IC1CAPT0	=	0x00d6
                           0000D7  1142 _IC1CAPT1	=	0x00d7
                           00D7D6  1143 _IC1CAPT	=	0xd7d6
                           0000D4  1144 _IC1MODE	=	0x00d4
                           0000D5  1145 _IC1STATUS	=	0x00d5
                           000092  1146 _NVADDR0	=	0x0092
                           000093  1147 _NVADDR1	=	0x0093
                           009392  1148 _NVADDR	=	0x9392
                           000094  1149 _NVDATA0	=	0x0094
                           000095  1150 _NVDATA1	=	0x0095
                           009594  1151 _NVDATA	=	0x9594
                           000096  1152 _NVKEY	=	0x0096
                           000091  1153 _NVSTATUS	=	0x0091
                           0000BC  1154 _OC0COMP0	=	0x00bc
                           0000BD  1155 _OC0COMP1	=	0x00bd
                           00BDBC  1156 _OC0COMP	=	0xbdbc
                           0000B9  1157 _OC0MODE	=	0x00b9
                           0000BA  1158 _OC0PIN	=	0x00ba
                           0000BB  1159 _OC0STATUS	=	0x00bb
                           0000C4  1160 _OC1COMP0	=	0x00c4
                           0000C5  1161 _OC1COMP1	=	0x00c5
                           00C5C4  1162 _OC1COMP	=	0xc5c4
                           0000C1  1163 _OC1MODE	=	0x00c1
                           0000C2  1164 _OC1PIN	=	0x00c2
                           0000C3  1165 _OC1STATUS	=	0x00c3
                           0000B1  1166 _RADIOACC	=	0x00b1
                           0000B3  1167 _RADIOADDR0	=	0x00b3
                           0000B2  1168 _RADIOADDR1	=	0x00b2
                           00B2B3  1169 _RADIOADDR	=	0xb2b3
                           0000B7  1170 _RADIODATA0	=	0x00b7
                           0000B6  1171 _RADIODATA1	=	0x00b6
                           0000B5  1172 _RADIODATA2	=	0x00b5
                           0000B4  1173 _RADIODATA3	=	0x00b4
                           B4B5B6B7  1174 _RADIODATA	=	0xb4b5b6b7
                           0000BE  1175 _RADIOSTAT0	=	0x00be
                           0000BF  1176 _RADIOSTAT1	=	0x00bf
                           00BFBE  1177 _RADIOSTAT	=	0xbfbe
                           0000DF  1178 _SPCLKSRC	=	0x00df
                           0000DC  1179 _SPMODE	=	0x00dc
                           0000DE  1180 _SPSHREG	=	0x00de
                           0000DD  1181 _SPSTATUS	=	0x00dd
                           00009A  1182 _T0CLKSRC	=	0x009a
                           00009C  1183 _T0CNT0	=	0x009c
                           00009D  1184 _T0CNT1	=	0x009d
                           009D9C  1185 _T0CNT	=	0x9d9c
                           000099  1186 _T0MODE	=	0x0099
                           00009E  1187 _T0PERIOD0	=	0x009e
                           00009F  1188 _T0PERIOD1	=	0x009f
                           009F9E  1189 _T0PERIOD	=	0x9f9e
                           00009B  1190 _T0STATUS	=	0x009b
                           0000A2  1191 _T1CLKSRC	=	0x00a2
                           0000A4  1192 _T1CNT0	=	0x00a4
                           0000A5  1193 _T1CNT1	=	0x00a5
                           00A5A4  1194 _T1CNT	=	0xa5a4
                           0000A1  1195 _T1MODE	=	0x00a1
                           0000A6  1196 _T1PERIOD0	=	0x00a6
                           0000A7  1197 _T1PERIOD1	=	0x00a7
                           00A7A6  1198 _T1PERIOD	=	0xa7a6
                           0000A3  1199 _T1STATUS	=	0x00a3
                           0000AA  1200 _T2CLKSRC	=	0x00aa
                           0000AC  1201 _T2CNT0	=	0x00ac
                           0000AD  1202 _T2CNT1	=	0x00ad
                           00ADAC  1203 _T2CNT	=	0xadac
                           0000A9  1204 _T2MODE	=	0x00a9
                           0000AE  1205 _T2PERIOD0	=	0x00ae
                           0000AF  1206 _T2PERIOD1	=	0x00af
                           00AFAE  1207 _T2PERIOD	=	0xafae
                           0000AB  1208 _T2STATUS	=	0x00ab
                           0000E4  1209 _U0CTRL	=	0x00e4
                           0000E7  1210 _U0MODE	=	0x00e7
                           0000E6  1211 _U0SHREG	=	0x00e6
                           0000E5  1212 _U0STATUS	=	0x00e5
                           0000EC  1213 _U1CTRL	=	0x00ec
                           0000EF  1214 _U1MODE	=	0x00ef
                           0000EE  1215 _U1SHREG	=	0x00ee
                           0000ED  1216 _U1STATUS	=	0x00ed
                           0000DA  1217 _WDTCFG	=	0x00da
                           0000DB  1218 _WDTRESET	=	0x00db
                           0000F1  1219 _WTCFGA	=	0x00f1
                           0000F9  1220 _WTCFGB	=	0x00f9
                           0000F2  1221 _WTCNTA0	=	0x00f2
                           0000F3  1222 _WTCNTA1	=	0x00f3
                           00F3F2  1223 _WTCNTA	=	0xf3f2
                           0000FA  1224 _WTCNTB0	=	0x00fa
                           0000FB  1225 _WTCNTB1	=	0x00fb
                           00FBFA  1226 _WTCNTB	=	0xfbfa
                           0000EB  1227 _WTCNTR1	=	0x00eb
                           0000F4  1228 _WTEVTA0	=	0x00f4
                           0000F5  1229 _WTEVTA1	=	0x00f5
                           00F5F4  1230 _WTEVTA	=	0xf5f4
                           0000F6  1231 _WTEVTB0	=	0x00f6
                           0000F7  1232 _WTEVTB1	=	0x00f7
                           00F7F6  1233 _WTEVTB	=	0xf7f6
                           0000FC  1234 _WTEVTC0	=	0x00fc
                           0000FD  1235 _WTEVTC1	=	0x00fd
                           00FDFC  1236 _WTEVTC	=	0xfdfc
                           0000FE  1237 _WTEVTD0	=	0x00fe
                           0000FF  1238 _WTEVTD1	=	0x00ff
                           00FFFE  1239 _WTEVTD	=	0xfffe
                           0000E9  1240 _WTIRQEN	=	0x00e9
                           0000EA  1241 _WTSTAT	=	0x00ea
                                   1242 ;--------------------------------------------------------
                                   1243 ; special function bits
                                   1244 ;--------------------------------------------------------
                                   1245 	.area RSEG    (ABS,DATA)
      000000                       1246 	.org 0x0000
                           0000E0  1247 _ACC_0	=	0x00e0
                           0000E1  1248 _ACC_1	=	0x00e1
                           0000E2  1249 _ACC_2	=	0x00e2
                           0000E3  1250 _ACC_3	=	0x00e3
                           0000E4  1251 _ACC_4	=	0x00e4
                           0000E5  1252 _ACC_5	=	0x00e5
                           0000E6  1253 _ACC_6	=	0x00e6
                           0000E7  1254 _ACC_7	=	0x00e7
                           0000F0  1255 _B_0	=	0x00f0
                           0000F1  1256 _B_1	=	0x00f1
                           0000F2  1257 _B_2	=	0x00f2
                           0000F3  1258 _B_3	=	0x00f3
                           0000F4  1259 _B_4	=	0x00f4
                           0000F5  1260 _B_5	=	0x00f5
                           0000F6  1261 _B_6	=	0x00f6
                           0000F7  1262 _B_7	=	0x00f7
                           0000A0  1263 _E2IE_0	=	0x00a0
                           0000A1  1264 _E2IE_1	=	0x00a1
                           0000A2  1265 _E2IE_2	=	0x00a2
                           0000A3  1266 _E2IE_3	=	0x00a3
                           0000A4  1267 _E2IE_4	=	0x00a4
                           0000A5  1268 _E2IE_5	=	0x00a5
                           0000A6  1269 _E2IE_6	=	0x00a6
                           0000A7  1270 _E2IE_7	=	0x00a7
                           0000C0  1271 _E2IP_0	=	0x00c0
                           0000C1  1272 _E2IP_1	=	0x00c1
                           0000C2  1273 _E2IP_2	=	0x00c2
                           0000C3  1274 _E2IP_3	=	0x00c3
                           0000C4  1275 _E2IP_4	=	0x00c4
                           0000C5  1276 _E2IP_5	=	0x00c5
                           0000C6  1277 _E2IP_6	=	0x00c6
                           0000C7  1278 _E2IP_7	=	0x00c7
                           000098  1279 _EIE_0	=	0x0098
                           000099  1280 _EIE_1	=	0x0099
                           00009A  1281 _EIE_2	=	0x009a
                           00009B  1282 _EIE_3	=	0x009b
                           00009C  1283 _EIE_4	=	0x009c
                           00009D  1284 _EIE_5	=	0x009d
                           00009E  1285 _EIE_6	=	0x009e
                           00009F  1286 _EIE_7	=	0x009f
                           0000B0  1287 _EIP_0	=	0x00b0
                           0000B1  1288 _EIP_1	=	0x00b1
                           0000B2  1289 _EIP_2	=	0x00b2
                           0000B3  1290 _EIP_3	=	0x00b3
                           0000B4  1291 _EIP_4	=	0x00b4
                           0000B5  1292 _EIP_5	=	0x00b5
                           0000B6  1293 _EIP_6	=	0x00b6
                           0000B7  1294 _EIP_7	=	0x00b7
                           0000A8  1295 _IE_0	=	0x00a8
                           0000A9  1296 _IE_1	=	0x00a9
                           0000AA  1297 _IE_2	=	0x00aa
                           0000AB  1298 _IE_3	=	0x00ab
                           0000AC  1299 _IE_4	=	0x00ac
                           0000AD  1300 _IE_5	=	0x00ad
                           0000AE  1301 _IE_6	=	0x00ae
                           0000AF  1302 _IE_7	=	0x00af
                           0000AF  1303 _EA	=	0x00af
                           0000B8  1304 _IP_0	=	0x00b8
                           0000B9  1305 _IP_1	=	0x00b9
                           0000BA  1306 _IP_2	=	0x00ba
                           0000BB  1307 _IP_3	=	0x00bb
                           0000BC  1308 _IP_4	=	0x00bc
                           0000BD  1309 _IP_5	=	0x00bd
                           0000BE  1310 _IP_6	=	0x00be
                           0000BF  1311 _IP_7	=	0x00bf
                           0000D0  1312 _P	=	0x00d0
                           0000D1  1313 _F1	=	0x00d1
                           0000D2  1314 _OV	=	0x00d2
                           0000D3  1315 _RS0	=	0x00d3
                           0000D4  1316 _RS1	=	0x00d4
                           0000D5  1317 _F0	=	0x00d5
                           0000D6  1318 _AC	=	0x00d6
                           0000D7  1319 _CY	=	0x00d7
                           0000C8  1320 _PINA_0	=	0x00c8
                           0000C9  1321 _PINA_1	=	0x00c9
                           0000CA  1322 _PINA_2	=	0x00ca
                           0000CB  1323 _PINA_3	=	0x00cb
                           0000CC  1324 _PINA_4	=	0x00cc
                           0000CD  1325 _PINA_5	=	0x00cd
                           0000CE  1326 _PINA_6	=	0x00ce
                           0000CF  1327 _PINA_7	=	0x00cf
                           0000E8  1328 _PINB_0	=	0x00e8
                           0000E9  1329 _PINB_1	=	0x00e9
                           0000EA  1330 _PINB_2	=	0x00ea
                           0000EB  1331 _PINB_3	=	0x00eb
                           0000EC  1332 _PINB_4	=	0x00ec
                           0000ED  1333 _PINB_5	=	0x00ed
                           0000EE  1334 _PINB_6	=	0x00ee
                           0000EF  1335 _PINB_7	=	0x00ef
                           0000F8  1336 _PINC_0	=	0x00f8
                           0000F9  1337 _PINC_1	=	0x00f9
                           0000FA  1338 _PINC_2	=	0x00fa
                           0000FB  1339 _PINC_3	=	0x00fb
                           0000FC  1340 _PINC_4	=	0x00fc
                           0000FD  1341 _PINC_5	=	0x00fd
                           0000FE  1342 _PINC_6	=	0x00fe
                           0000FF  1343 _PINC_7	=	0x00ff
                           000080  1344 _PORTA_0	=	0x0080
                           000081  1345 _PORTA_1	=	0x0081
                           000082  1346 _PORTA_2	=	0x0082
                           000083  1347 _PORTA_3	=	0x0083
                           000084  1348 _PORTA_4	=	0x0084
                           000085  1349 _PORTA_5	=	0x0085
                           000086  1350 _PORTA_6	=	0x0086
                           000087  1351 _PORTA_7	=	0x0087
                           000088  1352 _PORTB_0	=	0x0088
                           000089  1353 _PORTB_1	=	0x0089
                           00008A  1354 _PORTB_2	=	0x008a
                           00008B  1355 _PORTB_3	=	0x008b
                           00008C  1356 _PORTB_4	=	0x008c
                           00008D  1357 _PORTB_5	=	0x008d
                           00008E  1358 _PORTB_6	=	0x008e
                           00008F  1359 _PORTB_7	=	0x008f
                           000090  1360 _PORTC_0	=	0x0090
                           000091  1361 _PORTC_1	=	0x0091
                           000092  1362 _PORTC_2	=	0x0092
                           000093  1363 _PORTC_3	=	0x0093
                           000094  1364 _PORTC_4	=	0x0094
                           000095  1365 _PORTC_5	=	0x0095
                           000096  1366 _PORTC_6	=	0x0096
                           000097  1367 _PORTC_7	=	0x0097
                                   1368 ;--------------------------------------------------------
                                   1369 ; overlayable register banks
                                   1370 ;--------------------------------------------------------
                                   1371 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                       1372 	.ds 8
                                   1373 ;--------------------------------------------------------
                                   1374 ; internal ram data
                                   1375 ;--------------------------------------------------------
                                   1376 	.area DSEG    (DATA)
      00002D                       1377 _UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0:
      00002D                       1378 	.ds 1
      00002E                       1379 _UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0:
      00002E                       1380 	.ds 3
      000031                       1381 _UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0:
      000031                       1382 	.ds 3
      000034                       1383 _UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0:
      000034                       1384 	.ds 2
      000036                       1385 _UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0:
      000036                       1386 	.ds 3
                                   1387 ;--------------------------------------------------------
                                   1388 ; overlayable items in internal ram 
                                   1389 ;--------------------------------------------------------
                                   1390 	.area	OSEG    (OVR,DATA)
      00006A                       1391 _UBLOX_GPS_FletcherChecksum8_sloc0_1_0:
      00006A                       1392 	.ds 3
      00006D                       1393 _UBLOX_GPS_FletcherChecksum8_sloc1_1_0:
      00006D                       1394 	.ds 1
      00006E                       1395 _UBLOX_GPS_FletcherChecksum8_sloc2_1_0:
      00006E                       1396 	.ds 1
      00006F                       1397 _UBLOX_GPS_FletcherChecksum8_sloc3_1_0:
      00006F                       1398 	.ds 3
                                   1399 ;--------------------------------------------------------
                                   1400 ; indirectly addressable internal ram data
                                   1401 ;--------------------------------------------------------
                                   1402 	.area ISEG    (DATA)
                                   1403 ;--------------------------------------------------------
                                   1404 ; absolute internal ram data
                                   1405 ;--------------------------------------------------------
                                   1406 	.area IABS    (ABS,DATA)
                                   1407 	.area IABS    (ABS,DATA)
                                   1408 ;--------------------------------------------------------
                                   1409 ; bit data
                                   1410 ;--------------------------------------------------------
                                   1411 	.area BSEG    (BIT)
                                   1412 ;--------------------------------------------------------
                                   1413 ; paged external ram data
                                   1414 ;--------------------------------------------------------
                                   1415 	.area PSEG    (PAG,XDATA)
                                   1416 ;--------------------------------------------------------
                                   1417 ; external ram data
                                   1418 ;--------------------------------------------------------
                                   1419 	.area XSEG    (XDATA)
      0003B0                       1420 _aligned_alloc_PARM_2:
      0003B0                       1421 	.ds 2
                           007020  1422 _ADCCH0VAL0	=	0x7020
                           007021  1423 _ADCCH0VAL1	=	0x7021
                           007020  1424 _ADCCH0VAL	=	0x7020
                           007022  1425 _ADCCH1VAL0	=	0x7022
                           007023  1426 _ADCCH1VAL1	=	0x7023
                           007022  1427 _ADCCH1VAL	=	0x7022
                           007024  1428 _ADCCH2VAL0	=	0x7024
                           007025  1429 _ADCCH2VAL1	=	0x7025
                           007024  1430 _ADCCH2VAL	=	0x7024
                           007026  1431 _ADCCH3VAL0	=	0x7026
                           007027  1432 _ADCCH3VAL1	=	0x7027
                           007026  1433 _ADCCH3VAL	=	0x7026
                           007028  1434 _ADCTUNE0	=	0x7028
                           007029  1435 _ADCTUNE1	=	0x7029
                           00702A  1436 _ADCTUNE2	=	0x702a
                           007010  1437 _DMA0ADDR0	=	0x7010
                           007011  1438 _DMA0ADDR1	=	0x7011
                           007010  1439 _DMA0ADDR	=	0x7010
                           007014  1440 _DMA0CONFIG	=	0x7014
                           007012  1441 _DMA1ADDR0	=	0x7012
                           007013  1442 _DMA1ADDR1	=	0x7013
                           007012  1443 _DMA1ADDR	=	0x7012
                           007015  1444 _DMA1CONFIG	=	0x7015
                           007070  1445 _FRCOSCCONFIG	=	0x7070
                           007071  1446 _FRCOSCCTRL	=	0x7071
                           007076  1447 _FRCOSCFREQ0	=	0x7076
                           007077  1448 _FRCOSCFREQ1	=	0x7077
                           007076  1449 _FRCOSCFREQ	=	0x7076
                           007072  1450 _FRCOSCKFILT0	=	0x7072
                           007073  1451 _FRCOSCKFILT1	=	0x7073
                           007072  1452 _FRCOSCKFILT	=	0x7072
                           007078  1453 _FRCOSCPER0	=	0x7078
                           007079  1454 _FRCOSCPER1	=	0x7079
                           007078  1455 _FRCOSCPER	=	0x7078
                           007074  1456 _FRCOSCREF0	=	0x7074
                           007075  1457 _FRCOSCREF1	=	0x7075
                           007074  1458 _FRCOSCREF	=	0x7074
                           007007  1459 _ANALOGA	=	0x7007
                           00700C  1460 _GPIOENABLE	=	0x700c
                           007003  1461 _EXTIRQ	=	0x7003
                           007000  1462 _INTCHGA	=	0x7000
                           007001  1463 _INTCHGB	=	0x7001
                           007002  1464 _INTCHGC	=	0x7002
                           007008  1465 _PALTA	=	0x7008
                           007009  1466 _PALTB	=	0x7009
                           00700A  1467 _PALTC	=	0x700a
                           007046  1468 _PALTRADIO	=	0x7046
                           007004  1469 _PINCHGA	=	0x7004
                           007005  1470 _PINCHGB	=	0x7005
                           007006  1471 _PINCHGC	=	0x7006
                           00700B  1472 _PINSEL	=	0x700b
                           007060  1473 _LPOSCCONFIG	=	0x7060
                           007066  1474 _LPOSCFREQ0	=	0x7066
                           007067  1475 _LPOSCFREQ1	=	0x7067
                           007066  1476 _LPOSCFREQ	=	0x7066
                           007062  1477 _LPOSCKFILT0	=	0x7062
                           007063  1478 _LPOSCKFILT1	=	0x7063
                           007062  1479 _LPOSCKFILT	=	0x7062
                           007068  1480 _LPOSCPER0	=	0x7068
                           007069  1481 _LPOSCPER1	=	0x7069
                           007068  1482 _LPOSCPER	=	0x7068
                           007064  1483 _LPOSCREF0	=	0x7064
                           007065  1484 _LPOSCREF1	=	0x7065
                           007064  1485 _LPOSCREF	=	0x7064
                           007054  1486 _LPXOSCGM	=	0x7054
                           007F01  1487 _MISCCTRL	=	0x7f01
                           007053  1488 _OSCCALIB	=	0x7053
                           007050  1489 _OSCFORCERUN	=	0x7050
                           007052  1490 _OSCREADY	=	0x7052
                           007051  1491 _OSCRUN	=	0x7051
                           007040  1492 _RADIOFDATAADDR0	=	0x7040
                           007041  1493 _RADIOFDATAADDR1	=	0x7041
                           007040  1494 _RADIOFDATAADDR	=	0x7040
                           007042  1495 _RADIOFSTATADDR0	=	0x7042
                           007043  1496 _RADIOFSTATADDR1	=	0x7043
                           007042  1497 _RADIOFSTATADDR	=	0x7042
                           007044  1498 _RADIOMUX	=	0x7044
                           007084  1499 _SCRATCH0	=	0x7084
                           007085  1500 _SCRATCH1	=	0x7085
                           007086  1501 _SCRATCH2	=	0x7086
                           007087  1502 _SCRATCH3	=	0x7087
                           007F00  1503 _SILICONREV	=	0x7f00
                           007F19  1504 _XTALAMPL	=	0x7f19
                           007F18  1505 _XTALOSC	=	0x7f18
                           007F1A  1506 _XTALREADY	=	0x7f1a
                           003F82  1507 _XDPTR0	=	0x3f82
                           003F84  1508 _XDPTR1	=	0x3f84
                           003FA8  1509 _XIE	=	0x3fa8
                           003FB8  1510 _XIP	=	0x3fb8
                           003F87  1511 _XPCON	=	0x3f87
                           003FCA  1512 _XADCCH0CONFIG	=	0x3fca
                           003FCB  1513 _XADCCH1CONFIG	=	0x3fcb
                           003FD2  1514 _XADCCH2CONFIG	=	0x3fd2
                           003FD3  1515 _XADCCH3CONFIG	=	0x3fd3
                           003FD1  1516 _XADCCLKSRC	=	0x3fd1
                           003FC9  1517 _XADCCONV	=	0x3fc9
                           003FE1  1518 _XANALOGCOMP	=	0x3fe1
                           003FC6  1519 _XCLKCON	=	0x3fc6
                           003FC7  1520 _XCLKSTAT	=	0x3fc7
                           003F97  1521 _XCODECONFIG	=	0x3f97
                           003FE3  1522 _XDBGLNKBUF	=	0x3fe3
                           003FE2  1523 _XDBGLNKSTAT	=	0x3fe2
                           003F89  1524 _XDIRA	=	0x3f89
                           003F8A  1525 _XDIRB	=	0x3f8a
                           003F8B  1526 _XDIRC	=	0x3f8b
                           003F8E  1527 _XDIRR	=	0x3f8e
                           003FC8  1528 _XPINA	=	0x3fc8
                           003FE8  1529 _XPINB	=	0x3fe8
                           003FF8  1530 _XPINC	=	0x3ff8
                           003F8D  1531 _XPINR	=	0x3f8d
                           003F80  1532 _XPORTA	=	0x3f80
                           003F88  1533 _XPORTB	=	0x3f88
                           003F90  1534 _XPORTC	=	0x3f90
                           003F8C  1535 _XPORTR	=	0x3f8c
                           003FCE  1536 _XIC0CAPT0	=	0x3fce
                           003FCF  1537 _XIC0CAPT1	=	0x3fcf
                           003FCE  1538 _XIC0CAPT	=	0x3fce
                           003FCC  1539 _XIC0MODE	=	0x3fcc
                           003FCD  1540 _XIC0STATUS	=	0x3fcd
                           003FD6  1541 _XIC1CAPT0	=	0x3fd6
                           003FD7  1542 _XIC1CAPT1	=	0x3fd7
                           003FD6  1543 _XIC1CAPT	=	0x3fd6
                           003FD4  1544 _XIC1MODE	=	0x3fd4
                           003FD5  1545 _XIC1STATUS	=	0x3fd5
                           003F92  1546 _XNVADDR0	=	0x3f92
                           003F93  1547 _XNVADDR1	=	0x3f93
                           003F92  1548 _XNVADDR	=	0x3f92
                           003F94  1549 _XNVDATA0	=	0x3f94
                           003F95  1550 _XNVDATA1	=	0x3f95
                           003F94  1551 _XNVDATA	=	0x3f94
                           003F96  1552 _XNVKEY	=	0x3f96
                           003F91  1553 _XNVSTATUS	=	0x3f91
                           003FBC  1554 _XOC0COMP0	=	0x3fbc
                           003FBD  1555 _XOC0COMP1	=	0x3fbd
                           003FBC  1556 _XOC0COMP	=	0x3fbc
                           003FB9  1557 _XOC0MODE	=	0x3fb9
                           003FBA  1558 _XOC0PIN	=	0x3fba
                           003FBB  1559 _XOC0STATUS	=	0x3fbb
                           003FC4  1560 _XOC1COMP0	=	0x3fc4
                           003FC5  1561 _XOC1COMP1	=	0x3fc5
                           003FC4  1562 _XOC1COMP	=	0x3fc4
                           003FC1  1563 _XOC1MODE	=	0x3fc1
                           003FC2  1564 _XOC1PIN	=	0x3fc2
                           003FC3  1565 _XOC1STATUS	=	0x3fc3
                           003FB1  1566 _XRADIOACC	=	0x3fb1
                           003FB3  1567 _XRADIOADDR0	=	0x3fb3
                           003FB2  1568 _XRADIOADDR1	=	0x3fb2
                           003FB7  1569 _XRADIODATA0	=	0x3fb7
                           003FB6  1570 _XRADIODATA1	=	0x3fb6
                           003FB5  1571 _XRADIODATA2	=	0x3fb5
                           003FB4  1572 _XRADIODATA3	=	0x3fb4
                           003FBE  1573 _XRADIOSTAT0	=	0x3fbe
                           003FBF  1574 _XRADIOSTAT1	=	0x3fbf
                           003FBE  1575 _XRADIOSTAT	=	0x3fbe
                           003FDF  1576 _XSPCLKSRC	=	0x3fdf
                           003FDC  1577 _XSPMODE	=	0x3fdc
                           003FDE  1578 _XSPSHREG	=	0x3fde
                           003FDD  1579 _XSPSTATUS	=	0x3fdd
                           003F9A  1580 _XT0CLKSRC	=	0x3f9a
                           003F9C  1581 _XT0CNT0	=	0x3f9c
                           003F9D  1582 _XT0CNT1	=	0x3f9d
                           003F9C  1583 _XT0CNT	=	0x3f9c
                           003F99  1584 _XT0MODE	=	0x3f99
                           003F9E  1585 _XT0PERIOD0	=	0x3f9e
                           003F9F  1586 _XT0PERIOD1	=	0x3f9f
                           003F9E  1587 _XT0PERIOD	=	0x3f9e
                           003F9B  1588 _XT0STATUS	=	0x3f9b
                           003FA2  1589 _XT1CLKSRC	=	0x3fa2
                           003FA4  1590 _XT1CNT0	=	0x3fa4
                           003FA5  1591 _XT1CNT1	=	0x3fa5
                           003FA4  1592 _XT1CNT	=	0x3fa4
                           003FA1  1593 _XT1MODE	=	0x3fa1
                           003FA6  1594 _XT1PERIOD0	=	0x3fa6
                           003FA7  1595 _XT1PERIOD1	=	0x3fa7
                           003FA6  1596 _XT1PERIOD	=	0x3fa6
                           003FA3  1597 _XT1STATUS	=	0x3fa3
                           003FAA  1598 _XT2CLKSRC	=	0x3faa
                           003FAC  1599 _XT2CNT0	=	0x3fac
                           003FAD  1600 _XT2CNT1	=	0x3fad
                           003FAC  1601 _XT2CNT	=	0x3fac
                           003FA9  1602 _XT2MODE	=	0x3fa9
                           003FAE  1603 _XT2PERIOD0	=	0x3fae
                           003FAF  1604 _XT2PERIOD1	=	0x3faf
                           003FAE  1605 _XT2PERIOD	=	0x3fae
                           003FAB  1606 _XT2STATUS	=	0x3fab
                           003FE4  1607 _XU0CTRL	=	0x3fe4
                           003FE7  1608 _XU0MODE	=	0x3fe7
                           003FE6  1609 _XU0SHREG	=	0x3fe6
                           003FE5  1610 _XU0STATUS	=	0x3fe5
                           003FEC  1611 _XU1CTRL	=	0x3fec
                           003FEF  1612 _XU1MODE	=	0x3fef
                           003FEE  1613 _XU1SHREG	=	0x3fee
                           003FED  1614 _XU1STATUS	=	0x3fed
                           003FDA  1615 _XWDTCFG	=	0x3fda
                           003FDB  1616 _XWDTRESET	=	0x3fdb
                           003FF1  1617 _XWTCFGA	=	0x3ff1
                           003FF9  1618 _XWTCFGB	=	0x3ff9
                           003FF2  1619 _XWTCNTA0	=	0x3ff2
                           003FF3  1620 _XWTCNTA1	=	0x3ff3
                           003FF2  1621 _XWTCNTA	=	0x3ff2
                           003FFA  1622 _XWTCNTB0	=	0x3ffa
                           003FFB  1623 _XWTCNTB1	=	0x3ffb
                           003FFA  1624 _XWTCNTB	=	0x3ffa
                           003FEB  1625 _XWTCNTR1	=	0x3feb
                           003FF4  1626 _XWTEVTA0	=	0x3ff4
                           003FF5  1627 _XWTEVTA1	=	0x3ff5
                           003FF4  1628 _XWTEVTA	=	0x3ff4
                           003FF6  1629 _XWTEVTB0	=	0x3ff6
                           003FF7  1630 _XWTEVTB1	=	0x3ff7
                           003FF6  1631 _XWTEVTB	=	0x3ff6
                           003FFC  1632 _XWTEVTC0	=	0x3ffc
                           003FFD  1633 _XWTEVTC1	=	0x3ffd
                           003FFC  1634 _XWTEVTC	=	0x3ffc
                           003FFE  1635 _XWTEVTD0	=	0x3ffe
                           003FFF  1636 _XWTEVTD1	=	0x3fff
                           003FFE  1637 _XWTEVTD	=	0x3ffe
                           003FE9  1638 _XWTIRQEN	=	0x3fe9
                           003FEA  1639 _XWTSTAT	=	0x3fea
                           004114  1640 _AX5043_AFSKCTRL	=	0x4114
                           004113  1641 _AX5043_AFSKMARK0	=	0x4113
                           004112  1642 _AX5043_AFSKMARK1	=	0x4112
                           004111  1643 _AX5043_AFSKSPACE0	=	0x4111
                           004110  1644 _AX5043_AFSKSPACE1	=	0x4110
                           004043  1645 _AX5043_AGCCOUNTER	=	0x4043
                           004115  1646 _AX5043_AMPLFILTER	=	0x4115
                           004189  1647 _AX5043_BBOFFSCAP	=	0x4189
                           004188  1648 _AX5043_BBTUNE	=	0x4188
                           004041  1649 _AX5043_BGNDRSSI	=	0x4041
                           00422E  1650 _AX5043_BGNDRSSIGAIN	=	0x422e
                           00422F  1651 _AX5043_BGNDRSSITHR	=	0x422f
                           004017  1652 _AX5043_CRCINIT0	=	0x4017
                           004016  1653 _AX5043_CRCINIT1	=	0x4016
                           004015  1654 _AX5043_CRCINIT2	=	0x4015
                           004014  1655 _AX5043_CRCINIT3	=	0x4014
                           004332  1656 _AX5043_DACCONFIG	=	0x4332
                           004331  1657 _AX5043_DACVALUE0	=	0x4331
                           004330  1658 _AX5043_DACVALUE1	=	0x4330
                           004102  1659 _AX5043_DECIMATION	=	0x4102
                           004042  1660 _AX5043_DIVERSITY	=	0x4042
                           004011  1661 _AX5043_ENCODING	=	0x4011
                           004018  1662 _AX5043_FEC	=	0x4018
                           00401A  1663 _AX5043_FECSTATUS	=	0x401a
                           004019  1664 _AX5043_FECSYNC	=	0x4019
                           00402B  1665 _AX5043_FIFOCOUNT0	=	0x402b
                           00402A  1666 _AX5043_FIFOCOUNT1	=	0x402a
                           004029  1667 _AX5043_FIFODATA	=	0x4029
                           00402D  1668 _AX5043_FIFOFREE0	=	0x402d
                           00402C  1669 _AX5043_FIFOFREE1	=	0x402c
                           004028  1670 _AX5043_FIFOSTAT	=	0x4028
                           00402F  1671 _AX5043_FIFOTHRESH0	=	0x402f
                           00402E  1672 _AX5043_FIFOTHRESH1	=	0x402e
                           004012  1673 _AX5043_FRAMING	=	0x4012
                           004037  1674 _AX5043_FREQA0	=	0x4037
                           004036  1675 _AX5043_FREQA1	=	0x4036
                           004035  1676 _AX5043_FREQA2	=	0x4035
                           004034  1677 _AX5043_FREQA3	=	0x4034
                           00403F  1678 _AX5043_FREQB0	=	0x403f
                           00403E  1679 _AX5043_FREQB1	=	0x403e
                           00403D  1680 _AX5043_FREQB2	=	0x403d
                           00403C  1681 _AX5043_FREQB3	=	0x403c
                           004163  1682 _AX5043_FSKDEV0	=	0x4163
                           004162  1683 _AX5043_FSKDEV1	=	0x4162
                           004161  1684 _AX5043_FSKDEV2	=	0x4161
                           00410D  1685 _AX5043_FSKDMAX0	=	0x410d
                           00410C  1686 _AX5043_FSKDMAX1	=	0x410c
                           00410F  1687 _AX5043_FSKDMIN0	=	0x410f
                           00410E  1688 _AX5043_FSKDMIN1	=	0x410e
                           004309  1689 _AX5043_GPADC13VALUE0	=	0x4309
                           004308  1690 _AX5043_GPADC13VALUE1	=	0x4308
                           004300  1691 _AX5043_GPADCCTRL	=	0x4300
                           004301  1692 _AX5043_GPADCPERIOD	=	0x4301
                           004101  1693 _AX5043_IFFREQ0	=	0x4101
                           004100  1694 _AX5043_IFFREQ1	=	0x4100
                           00400B  1695 _AX5043_IRQINVERSION0	=	0x400b
                           00400A  1696 _AX5043_IRQINVERSION1	=	0x400a
                           004007  1697 _AX5043_IRQMASK0	=	0x4007
                           004006  1698 _AX5043_IRQMASK1	=	0x4006
                           00400D  1699 _AX5043_IRQREQUEST0	=	0x400d
                           00400C  1700 _AX5043_IRQREQUEST1	=	0x400c
                           004310  1701 _AX5043_LPOSCCONFIG	=	0x4310
                           004317  1702 _AX5043_LPOSCFREQ0	=	0x4317
                           004316  1703 _AX5043_LPOSCFREQ1	=	0x4316
                           004313  1704 _AX5043_LPOSCKFILT0	=	0x4313
                           004312  1705 _AX5043_LPOSCKFILT1	=	0x4312
                           004319  1706 _AX5043_LPOSCPER0	=	0x4319
                           004318  1707 _AX5043_LPOSCPER1	=	0x4318
                           004315  1708 _AX5043_LPOSCREF0	=	0x4315
                           004314  1709 _AX5043_LPOSCREF1	=	0x4314
                           004311  1710 _AX5043_LPOSCSTATUS	=	0x4311
                           004214  1711 _AX5043_MATCH0LEN	=	0x4214
                           004216  1712 _AX5043_MATCH0MAX	=	0x4216
                           004215  1713 _AX5043_MATCH0MIN	=	0x4215
                           004213  1714 _AX5043_MATCH0PAT0	=	0x4213
                           004212  1715 _AX5043_MATCH0PAT1	=	0x4212
                           004211  1716 _AX5043_MATCH0PAT2	=	0x4211
                           004210  1717 _AX5043_MATCH0PAT3	=	0x4210
                           00421C  1718 _AX5043_MATCH1LEN	=	0x421c
                           00421E  1719 _AX5043_MATCH1MAX	=	0x421e
                           00421D  1720 _AX5043_MATCH1MIN	=	0x421d
                           004219  1721 _AX5043_MATCH1PAT0	=	0x4219
                           004218  1722 _AX5043_MATCH1PAT1	=	0x4218
                           004108  1723 _AX5043_MAXDROFFSET0	=	0x4108
                           004107  1724 _AX5043_MAXDROFFSET1	=	0x4107
                           004106  1725 _AX5043_MAXDROFFSET2	=	0x4106
                           00410B  1726 _AX5043_MAXRFOFFSET0	=	0x410b
                           00410A  1727 _AX5043_MAXRFOFFSET1	=	0x410a
                           004109  1728 _AX5043_MAXRFOFFSET2	=	0x4109
                           004164  1729 _AX5043_MODCFGA	=	0x4164
                           004160  1730 _AX5043_MODCFGF	=	0x4160
                           004F5F  1731 _AX5043_MODCFGP	=	0x4f5f
                           004010  1732 _AX5043_MODULATION	=	0x4010
                           004025  1733 _AX5043_PINFUNCANTSEL	=	0x4025
                           004023  1734 _AX5043_PINFUNCDATA	=	0x4023
                           004022  1735 _AX5043_PINFUNCDCLK	=	0x4022
                           004024  1736 _AX5043_PINFUNCIRQ	=	0x4024
                           004026  1737 _AX5043_PINFUNCPWRAMP	=	0x4026
                           004021  1738 _AX5043_PINFUNCSYSCLK	=	0x4021
                           004020  1739 _AX5043_PINSTATE	=	0x4020
                           004233  1740 _AX5043_PKTACCEPTFLAGS	=	0x4233
                           004230  1741 _AX5043_PKTCHUNKSIZE	=	0x4230
                           004231  1742 _AX5043_PKTMISCFLAGS	=	0x4231
                           004232  1743 _AX5043_PKTSTOREFLAGS	=	0x4232
                           004031  1744 _AX5043_PLLCPI	=	0x4031
                           004039  1745 _AX5043_PLLCPIBOOST	=	0x4039
                           004182  1746 _AX5043_PLLLOCKDET	=	0x4182
                           004030  1747 _AX5043_PLLLOOP	=	0x4030
                           004038  1748 _AX5043_PLLLOOPBOOST	=	0x4038
                           004033  1749 _AX5043_PLLRANGINGA	=	0x4033
                           00403B  1750 _AX5043_PLLRANGINGB	=	0x403b
                           004183  1751 _AX5043_PLLRNGCLK	=	0x4183
                           004032  1752 _AX5043_PLLVCODIV	=	0x4032
                           004180  1753 _AX5043_PLLVCOI	=	0x4180
                           004181  1754 _AX5043_PLLVCOIR	=	0x4181
                           004F08  1755 _AX5043_POWCTRL1	=	0x4f08
                           004005  1756 _AX5043_POWIRQMASK	=	0x4005
                           004003  1757 _AX5043_POWSTAT	=	0x4003
                           004004  1758 _AX5043_POWSTICKYSTAT	=	0x4004
                           004027  1759 _AX5043_PWRAMP	=	0x4027
                           004002  1760 _AX5043_PWRMODE	=	0x4002
                           004009  1761 _AX5043_RADIOEVENTMASK0	=	0x4009
                           004008  1762 _AX5043_RADIOEVENTMASK1	=	0x4008
                           00400F  1763 _AX5043_RADIOEVENTREQ0	=	0x400f
                           00400E  1764 _AX5043_RADIOEVENTREQ1	=	0x400e
                           00401C  1765 _AX5043_RADIOSTATE	=	0x401c
                           004F0D  1766 _AX5043_REF	=	0x4f0d
                           004040  1767 _AX5043_RSSI	=	0x4040
                           00422D  1768 _AX5043_RSSIABSTHR	=	0x422d
                           00422C  1769 _AX5043_RSSIREFERENCE	=	0x422c
                           004105  1770 _AX5043_RXDATARATE0	=	0x4105
                           004104  1771 _AX5043_RXDATARATE1	=	0x4104
                           004103  1772 _AX5043_RXDATARATE2	=	0x4103
                           004001  1773 _AX5043_SCRATCH	=	0x4001
                           004000  1774 _AX5043_SILICONREVISION	=	0x4000
                           00405B  1775 _AX5043_TIMER0	=	0x405b
                           00405A  1776 _AX5043_TIMER1	=	0x405a
                           004059  1777 _AX5043_TIMER2	=	0x4059
                           004227  1778 _AX5043_TMGRXAGC	=	0x4227
                           004223  1779 _AX5043_TMGRXBOOST	=	0x4223
                           004226  1780 _AX5043_TMGRXCOARSEAGC	=	0x4226
                           004225  1781 _AX5043_TMGRXOFFSACQ	=	0x4225
                           004229  1782 _AX5043_TMGRXPREAMBLE1	=	0x4229
                           00422A  1783 _AX5043_TMGRXPREAMBLE2	=	0x422a
                           00422B  1784 _AX5043_TMGRXPREAMBLE3	=	0x422b
                           004228  1785 _AX5043_TMGRXRSSI	=	0x4228
                           004224  1786 _AX5043_TMGRXSETTLE	=	0x4224
                           004220  1787 _AX5043_TMGTXBOOST	=	0x4220
                           004221  1788 _AX5043_TMGTXSETTLE	=	0x4221
                           004055  1789 _AX5043_TRKAFSKDEMOD0	=	0x4055
                           004054  1790 _AX5043_TRKAFSKDEMOD1	=	0x4054
                           004049  1791 _AX5043_TRKAMPLITUDE0	=	0x4049
                           004048  1792 _AX5043_TRKAMPLITUDE1	=	0x4048
                           004047  1793 _AX5043_TRKDATARATE0	=	0x4047
                           004046  1794 _AX5043_TRKDATARATE1	=	0x4046
                           004045  1795 _AX5043_TRKDATARATE2	=	0x4045
                           004051  1796 _AX5043_TRKFREQ0	=	0x4051
                           004050  1797 _AX5043_TRKFREQ1	=	0x4050
                           004053  1798 _AX5043_TRKFSKDEMOD0	=	0x4053
                           004052  1799 _AX5043_TRKFSKDEMOD1	=	0x4052
                           00404B  1800 _AX5043_TRKPHASE0	=	0x404b
                           00404A  1801 _AX5043_TRKPHASE1	=	0x404a
                           00404F  1802 _AX5043_TRKRFFREQ0	=	0x404f
                           00404E  1803 _AX5043_TRKRFFREQ1	=	0x404e
                           00404D  1804 _AX5043_TRKRFFREQ2	=	0x404d
                           004169  1805 _AX5043_TXPWRCOEFFA0	=	0x4169
                           004168  1806 _AX5043_TXPWRCOEFFA1	=	0x4168
                           00416B  1807 _AX5043_TXPWRCOEFFB0	=	0x416b
                           00416A  1808 _AX5043_TXPWRCOEFFB1	=	0x416a
                           00416D  1809 _AX5043_TXPWRCOEFFC0	=	0x416d
                           00416C  1810 _AX5043_TXPWRCOEFFC1	=	0x416c
                           00416F  1811 _AX5043_TXPWRCOEFFD0	=	0x416f
                           00416E  1812 _AX5043_TXPWRCOEFFD1	=	0x416e
                           004171  1813 _AX5043_TXPWRCOEFFE0	=	0x4171
                           004170  1814 _AX5043_TXPWRCOEFFE1	=	0x4170
                           004167  1815 _AX5043_TXRATE0	=	0x4167
                           004166  1816 _AX5043_TXRATE1	=	0x4166
                           004165  1817 _AX5043_TXRATE2	=	0x4165
                           00406B  1818 _AX5043_WAKEUP0	=	0x406b
                           00406A  1819 _AX5043_WAKEUP1	=	0x406a
                           00406D  1820 _AX5043_WAKEUPFREQ0	=	0x406d
                           00406C  1821 _AX5043_WAKEUPFREQ1	=	0x406c
                           004069  1822 _AX5043_WAKEUPTIMER0	=	0x4069
                           004068  1823 _AX5043_WAKEUPTIMER1	=	0x4068
                           00406E  1824 _AX5043_WAKEUPXOEARLY	=	0x406e
                           004F11  1825 _AX5043_XTALAMPL	=	0x4f11
                           004184  1826 _AX5043_XTALCAP	=	0x4184
                           004F10  1827 _AX5043_XTALOSC	=	0x4f10
                           00401D  1828 _AX5043_XTALSTATUS	=	0x401d
                           004F00  1829 _AX5043_0xF00	=	0x4f00
                           004F0C  1830 _AX5043_0xF0C	=	0x4f0c
                           004F18  1831 _AX5043_0xF18	=	0x4f18
                           004F1C  1832 _AX5043_0xF1C	=	0x4f1c
                           004F21  1833 _AX5043_0xF21	=	0x4f21
                           004F22  1834 _AX5043_0xF22	=	0x4f22
                           004F23  1835 _AX5043_0xF23	=	0x4f23
                           004F26  1836 _AX5043_0xF26	=	0x4f26
                           004F30  1837 _AX5043_0xF30	=	0x4f30
                           004F31  1838 _AX5043_0xF31	=	0x4f31
                           004F32  1839 _AX5043_0xF32	=	0x4f32
                           004F33  1840 _AX5043_0xF33	=	0x4f33
                           004F34  1841 _AX5043_0xF34	=	0x4f34
                           004F35  1842 _AX5043_0xF35	=	0x4f35
                           004F44  1843 _AX5043_0xF44	=	0x4f44
                           004122  1844 _AX5043_AGCAHYST0	=	0x4122
                           004132  1845 _AX5043_AGCAHYST1	=	0x4132
                           004142  1846 _AX5043_AGCAHYST2	=	0x4142
                           004152  1847 _AX5043_AGCAHYST3	=	0x4152
                           004120  1848 _AX5043_AGCGAIN0	=	0x4120
                           004130  1849 _AX5043_AGCGAIN1	=	0x4130
                           004140  1850 _AX5043_AGCGAIN2	=	0x4140
                           004150  1851 _AX5043_AGCGAIN3	=	0x4150
                           004123  1852 _AX5043_AGCMINMAX0	=	0x4123
                           004133  1853 _AX5043_AGCMINMAX1	=	0x4133
                           004143  1854 _AX5043_AGCMINMAX2	=	0x4143
                           004153  1855 _AX5043_AGCMINMAX3	=	0x4153
                           004121  1856 _AX5043_AGCTARGET0	=	0x4121
                           004131  1857 _AX5043_AGCTARGET1	=	0x4131
                           004141  1858 _AX5043_AGCTARGET2	=	0x4141
                           004151  1859 _AX5043_AGCTARGET3	=	0x4151
                           00412B  1860 _AX5043_AMPLITUDEGAIN0	=	0x412b
                           00413B  1861 _AX5043_AMPLITUDEGAIN1	=	0x413b
                           00414B  1862 _AX5043_AMPLITUDEGAIN2	=	0x414b
                           00415B  1863 _AX5043_AMPLITUDEGAIN3	=	0x415b
                           00412F  1864 _AX5043_BBOFFSRES0	=	0x412f
                           00413F  1865 _AX5043_BBOFFSRES1	=	0x413f
                           00414F  1866 _AX5043_BBOFFSRES2	=	0x414f
                           00415F  1867 _AX5043_BBOFFSRES3	=	0x415f
                           004125  1868 _AX5043_DRGAIN0	=	0x4125
                           004135  1869 _AX5043_DRGAIN1	=	0x4135
                           004145  1870 _AX5043_DRGAIN2	=	0x4145
                           004155  1871 _AX5043_DRGAIN3	=	0x4155
                           00412E  1872 _AX5043_FOURFSK0	=	0x412e
                           00413E  1873 _AX5043_FOURFSK1	=	0x413e
                           00414E  1874 _AX5043_FOURFSK2	=	0x414e
                           00415E  1875 _AX5043_FOURFSK3	=	0x415e
                           00412D  1876 _AX5043_FREQDEV00	=	0x412d
                           00413D  1877 _AX5043_FREQDEV01	=	0x413d
                           00414D  1878 _AX5043_FREQDEV02	=	0x414d
                           00415D  1879 _AX5043_FREQDEV03	=	0x415d
                           00412C  1880 _AX5043_FREQDEV10	=	0x412c
                           00413C  1881 _AX5043_FREQDEV11	=	0x413c
                           00414C  1882 _AX5043_FREQDEV12	=	0x414c
                           00415C  1883 _AX5043_FREQDEV13	=	0x415c
                           004127  1884 _AX5043_FREQUENCYGAINA0	=	0x4127
                           004137  1885 _AX5043_FREQUENCYGAINA1	=	0x4137
                           004147  1886 _AX5043_FREQUENCYGAINA2	=	0x4147
                           004157  1887 _AX5043_FREQUENCYGAINA3	=	0x4157
                           004128  1888 _AX5043_FREQUENCYGAINB0	=	0x4128
                           004138  1889 _AX5043_FREQUENCYGAINB1	=	0x4138
                           004148  1890 _AX5043_FREQUENCYGAINB2	=	0x4148
                           004158  1891 _AX5043_FREQUENCYGAINB3	=	0x4158
                           004129  1892 _AX5043_FREQUENCYGAINC0	=	0x4129
                           004139  1893 _AX5043_FREQUENCYGAINC1	=	0x4139
                           004149  1894 _AX5043_FREQUENCYGAINC2	=	0x4149
                           004159  1895 _AX5043_FREQUENCYGAINC3	=	0x4159
                           00412A  1896 _AX5043_FREQUENCYGAIND0	=	0x412a
                           00413A  1897 _AX5043_FREQUENCYGAIND1	=	0x413a
                           00414A  1898 _AX5043_FREQUENCYGAIND2	=	0x414a
                           00415A  1899 _AX5043_FREQUENCYGAIND3	=	0x415a
                           004116  1900 _AX5043_FREQUENCYLEAK	=	0x4116
                           004126  1901 _AX5043_PHASEGAIN0	=	0x4126
                           004136  1902 _AX5043_PHASEGAIN1	=	0x4136
                           004146  1903 _AX5043_PHASEGAIN2	=	0x4146
                           004156  1904 _AX5043_PHASEGAIN3	=	0x4156
                           004207  1905 _AX5043_PKTADDR0	=	0x4207
                           004206  1906 _AX5043_PKTADDR1	=	0x4206
                           004205  1907 _AX5043_PKTADDR2	=	0x4205
                           004204  1908 _AX5043_PKTADDR3	=	0x4204
                           004200  1909 _AX5043_PKTADDRCFG	=	0x4200
                           00420B  1910 _AX5043_PKTADDRMASK0	=	0x420b
                           00420A  1911 _AX5043_PKTADDRMASK1	=	0x420a
                           004209  1912 _AX5043_PKTADDRMASK2	=	0x4209
                           004208  1913 _AX5043_PKTADDRMASK3	=	0x4208
                           004201  1914 _AX5043_PKTLENCFG	=	0x4201
                           004202  1915 _AX5043_PKTLENOFFSET	=	0x4202
                           004203  1916 _AX5043_PKTMAXLEN	=	0x4203
                           004118  1917 _AX5043_RXPARAMCURSET	=	0x4118
                           004117  1918 _AX5043_RXPARAMSETS	=	0x4117
                           004124  1919 _AX5043_TIMEGAIN0	=	0x4124
                           004134  1920 _AX5043_TIMEGAIN1	=	0x4134
                           004144  1921 _AX5043_TIMEGAIN2	=	0x4144
                           004154  1922 _AX5043_TIMEGAIN3	=	0x4154
                           005114  1923 _AX5043_AFSKCTRLNB	=	0x5114
                           005113  1924 _AX5043_AFSKMARK0NB	=	0x5113
                           005112  1925 _AX5043_AFSKMARK1NB	=	0x5112
                           005111  1926 _AX5043_AFSKSPACE0NB	=	0x5111
                           005110  1927 _AX5043_AFSKSPACE1NB	=	0x5110
                           005043  1928 _AX5043_AGCCOUNTERNB	=	0x5043
                           005115  1929 _AX5043_AMPLFILTERNB	=	0x5115
                           005189  1930 _AX5043_BBOFFSCAPNB	=	0x5189
                           005188  1931 _AX5043_BBTUNENB	=	0x5188
                           005041  1932 _AX5043_BGNDRSSINB	=	0x5041
                           00522E  1933 _AX5043_BGNDRSSIGAINNB	=	0x522e
                           00522F  1934 _AX5043_BGNDRSSITHRNB	=	0x522f
                           005017  1935 _AX5043_CRCINIT0NB	=	0x5017
                           005016  1936 _AX5043_CRCINIT1NB	=	0x5016
                           005015  1937 _AX5043_CRCINIT2NB	=	0x5015
                           005014  1938 _AX5043_CRCINIT3NB	=	0x5014
                           005332  1939 _AX5043_DACCONFIGNB	=	0x5332
                           005331  1940 _AX5043_DACVALUE0NB	=	0x5331
                           005330  1941 _AX5043_DACVALUE1NB	=	0x5330
                           005102  1942 _AX5043_DECIMATIONNB	=	0x5102
                           005042  1943 _AX5043_DIVERSITYNB	=	0x5042
                           005011  1944 _AX5043_ENCODINGNB	=	0x5011
                           005018  1945 _AX5043_FECNB	=	0x5018
                           00501A  1946 _AX5043_FECSTATUSNB	=	0x501a
                           005019  1947 _AX5043_FECSYNCNB	=	0x5019
                           00502B  1948 _AX5043_FIFOCOUNT0NB	=	0x502b
                           00502A  1949 _AX5043_FIFOCOUNT1NB	=	0x502a
                           005029  1950 _AX5043_FIFODATANB	=	0x5029
                           00502D  1951 _AX5043_FIFOFREE0NB	=	0x502d
                           00502C  1952 _AX5043_FIFOFREE1NB	=	0x502c
                           005028  1953 _AX5043_FIFOSTATNB	=	0x5028
                           00502F  1954 _AX5043_FIFOTHRESH0NB	=	0x502f
                           00502E  1955 _AX5043_FIFOTHRESH1NB	=	0x502e
                           005012  1956 _AX5043_FRAMINGNB	=	0x5012
                           005037  1957 _AX5043_FREQA0NB	=	0x5037
                           005036  1958 _AX5043_FREQA1NB	=	0x5036
                           005035  1959 _AX5043_FREQA2NB	=	0x5035
                           005034  1960 _AX5043_FREQA3NB	=	0x5034
                           00503F  1961 _AX5043_FREQB0NB	=	0x503f
                           00503E  1962 _AX5043_FREQB1NB	=	0x503e
                           00503D  1963 _AX5043_FREQB2NB	=	0x503d
                           00503C  1964 _AX5043_FREQB3NB	=	0x503c
                           005163  1965 _AX5043_FSKDEV0NB	=	0x5163
                           005162  1966 _AX5043_FSKDEV1NB	=	0x5162
                           005161  1967 _AX5043_FSKDEV2NB	=	0x5161
                           00510D  1968 _AX5043_FSKDMAX0NB	=	0x510d
                           00510C  1969 _AX5043_FSKDMAX1NB	=	0x510c
                           00510F  1970 _AX5043_FSKDMIN0NB	=	0x510f
                           00510E  1971 _AX5043_FSKDMIN1NB	=	0x510e
                           005309  1972 _AX5043_GPADC13VALUE0NB	=	0x5309
                           005308  1973 _AX5043_GPADC13VALUE1NB	=	0x5308
                           005300  1974 _AX5043_GPADCCTRLNB	=	0x5300
                           005301  1975 _AX5043_GPADCPERIODNB	=	0x5301
                           005101  1976 _AX5043_IFFREQ0NB	=	0x5101
                           005100  1977 _AX5043_IFFREQ1NB	=	0x5100
                           00500B  1978 _AX5043_IRQINVERSION0NB	=	0x500b
                           00500A  1979 _AX5043_IRQINVERSION1NB	=	0x500a
                           005007  1980 _AX5043_IRQMASK0NB	=	0x5007
                           005006  1981 _AX5043_IRQMASK1NB	=	0x5006
                           00500D  1982 _AX5043_IRQREQUEST0NB	=	0x500d
                           00500C  1983 _AX5043_IRQREQUEST1NB	=	0x500c
                           005310  1984 _AX5043_LPOSCCONFIGNB	=	0x5310
                           005317  1985 _AX5043_LPOSCFREQ0NB	=	0x5317
                           005316  1986 _AX5043_LPOSCFREQ1NB	=	0x5316
                           005313  1987 _AX5043_LPOSCKFILT0NB	=	0x5313
                           005312  1988 _AX5043_LPOSCKFILT1NB	=	0x5312
                           005319  1989 _AX5043_LPOSCPER0NB	=	0x5319
                           005318  1990 _AX5043_LPOSCPER1NB	=	0x5318
                           005315  1991 _AX5043_LPOSCREF0NB	=	0x5315
                           005314  1992 _AX5043_LPOSCREF1NB	=	0x5314
                           005311  1993 _AX5043_LPOSCSTATUSNB	=	0x5311
                           005214  1994 _AX5043_MATCH0LENNB	=	0x5214
                           005216  1995 _AX5043_MATCH0MAXNB	=	0x5216
                           005215  1996 _AX5043_MATCH0MINNB	=	0x5215
                           005213  1997 _AX5043_MATCH0PAT0NB	=	0x5213
                           005212  1998 _AX5043_MATCH0PAT1NB	=	0x5212
                           005211  1999 _AX5043_MATCH0PAT2NB	=	0x5211
                           005210  2000 _AX5043_MATCH0PAT3NB	=	0x5210
                           00521C  2001 _AX5043_MATCH1LENNB	=	0x521c
                           00521E  2002 _AX5043_MATCH1MAXNB	=	0x521e
                           00521D  2003 _AX5043_MATCH1MINNB	=	0x521d
                           005219  2004 _AX5043_MATCH1PAT0NB	=	0x5219
                           005218  2005 _AX5043_MATCH1PAT1NB	=	0x5218
                           005108  2006 _AX5043_MAXDROFFSET0NB	=	0x5108
                           005107  2007 _AX5043_MAXDROFFSET1NB	=	0x5107
                           005106  2008 _AX5043_MAXDROFFSET2NB	=	0x5106
                           00510B  2009 _AX5043_MAXRFOFFSET0NB	=	0x510b
                           00510A  2010 _AX5043_MAXRFOFFSET1NB	=	0x510a
                           005109  2011 _AX5043_MAXRFOFFSET2NB	=	0x5109
                           005164  2012 _AX5043_MODCFGANB	=	0x5164
                           005160  2013 _AX5043_MODCFGFNB	=	0x5160
                           005F5F  2014 _AX5043_MODCFGPNB	=	0x5f5f
                           005010  2015 _AX5043_MODULATIONNB	=	0x5010
                           005025  2016 _AX5043_PINFUNCANTSELNB	=	0x5025
                           005023  2017 _AX5043_PINFUNCDATANB	=	0x5023
                           005022  2018 _AX5043_PINFUNCDCLKNB	=	0x5022
                           005024  2019 _AX5043_PINFUNCIRQNB	=	0x5024
                           005026  2020 _AX5043_PINFUNCPWRAMPNB	=	0x5026
                           005021  2021 _AX5043_PINFUNCSYSCLKNB	=	0x5021
                           005020  2022 _AX5043_PINSTATENB	=	0x5020
                           005233  2023 _AX5043_PKTACCEPTFLAGSNB	=	0x5233
                           005230  2024 _AX5043_PKTCHUNKSIZENB	=	0x5230
                           005231  2025 _AX5043_PKTMISCFLAGSNB	=	0x5231
                           005232  2026 _AX5043_PKTSTOREFLAGSNB	=	0x5232
                           005031  2027 _AX5043_PLLCPINB	=	0x5031
                           005039  2028 _AX5043_PLLCPIBOOSTNB	=	0x5039
                           005182  2029 _AX5043_PLLLOCKDETNB	=	0x5182
                           005030  2030 _AX5043_PLLLOOPNB	=	0x5030
                           005038  2031 _AX5043_PLLLOOPBOOSTNB	=	0x5038
                           005033  2032 _AX5043_PLLRANGINGANB	=	0x5033
                           00503B  2033 _AX5043_PLLRANGINGBNB	=	0x503b
                           005183  2034 _AX5043_PLLRNGCLKNB	=	0x5183
                           005032  2035 _AX5043_PLLVCODIVNB	=	0x5032
                           005180  2036 _AX5043_PLLVCOINB	=	0x5180
                           005181  2037 _AX5043_PLLVCOIRNB	=	0x5181
                           005F08  2038 _AX5043_POWCTRL1NB	=	0x5f08
                           005005  2039 _AX5043_POWIRQMASKNB	=	0x5005
                           005003  2040 _AX5043_POWSTATNB	=	0x5003
                           005004  2041 _AX5043_POWSTICKYSTATNB	=	0x5004
                           005027  2042 _AX5043_PWRAMPNB	=	0x5027
                           005002  2043 _AX5043_PWRMODENB	=	0x5002
                           005009  2044 _AX5043_RADIOEVENTMASK0NB	=	0x5009
                           005008  2045 _AX5043_RADIOEVENTMASK1NB	=	0x5008
                           00500F  2046 _AX5043_RADIOEVENTREQ0NB	=	0x500f
                           00500E  2047 _AX5043_RADIOEVENTREQ1NB	=	0x500e
                           00501C  2048 _AX5043_RADIOSTATENB	=	0x501c
                           005F0D  2049 _AX5043_REFNB	=	0x5f0d
                           005040  2050 _AX5043_RSSINB	=	0x5040
                           00522D  2051 _AX5043_RSSIABSTHRNB	=	0x522d
                           00522C  2052 _AX5043_RSSIREFERENCENB	=	0x522c
                           005105  2053 _AX5043_RXDATARATE0NB	=	0x5105
                           005104  2054 _AX5043_RXDATARATE1NB	=	0x5104
                           005103  2055 _AX5043_RXDATARATE2NB	=	0x5103
                           005001  2056 _AX5043_SCRATCHNB	=	0x5001
                           005000  2057 _AX5043_SILICONREVISIONNB	=	0x5000
                           00505B  2058 _AX5043_TIMER0NB	=	0x505b
                           00505A  2059 _AX5043_TIMER1NB	=	0x505a
                           005059  2060 _AX5043_TIMER2NB	=	0x5059
                           005227  2061 _AX5043_TMGRXAGCNB	=	0x5227
                           005223  2062 _AX5043_TMGRXBOOSTNB	=	0x5223
                           005226  2063 _AX5043_TMGRXCOARSEAGCNB	=	0x5226
                           005225  2064 _AX5043_TMGRXOFFSACQNB	=	0x5225
                           005229  2065 _AX5043_TMGRXPREAMBLE1NB	=	0x5229
                           00522A  2066 _AX5043_TMGRXPREAMBLE2NB	=	0x522a
                           00522B  2067 _AX5043_TMGRXPREAMBLE3NB	=	0x522b
                           005228  2068 _AX5043_TMGRXRSSINB	=	0x5228
                           005224  2069 _AX5043_TMGRXSETTLENB	=	0x5224
                           005220  2070 _AX5043_TMGTXBOOSTNB	=	0x5220
                           005221  2071 _AX5043_TMGTXSETTLENB	=	0x5221
                           005055  2072 _AX5043_TRKAFSKDEMOD0NB	=	0x5055
                           005054  2073 _AX5043_TRKAFSKDEMOD1NB	=	0x5054
                           005049  2074 _AX5043_TRKAMPLITUDE0NB	=	0x5049
                           005048  2075 _AX5043_TRKAMPLITUDE1NB	=	0x5048
                           005047  2076 _AX5043_TRKDATARATE0NB	=	0x5047
                           005046  2077 _AX5043_TRKDATARATE1NB	=	0x5046
                           005045  2078 _AX5043_TRKDATARATE2NB	=	0x5045
                           005051  2079 _AX5043_TRKFREQ0NB	=	0x5051
                           005050  2080 _AX5043_TRKFREQ1NB	=	0x5050
                           005053  2081 _AX5043_TRKFSKDEMOD0NB	=	0x5053
                           005052  2082 _AX5043_TRKFSKDEMOD1NB	=	0x5052
                           00504B  2083 _AX5043_TRKPHASE0NB	=	0x504b
                           00504A  2084 _AX5043_TRKPHASE1NB	=	0x504a
                           00504F  2085 _AX5043_TRKRFFREQ0NB	=	0x504f
                           00504E  2086 _AX5043_TRKRFFREQ1NB	=	0x504e
                           00504D  2087 _AX5043_TRKRFFREQ2NB	=	0x504d
                           005169  2088 _AX5043_TXPWRCOEFFA0NB	=	0x5169
                           005168  2089 _AX5043_TXPWRCOEFFA1NB	=	0x5168
                           00516B  2090 _AX5043_TXPWRCOEFFB0NB	=	0x516b
                           00516A  2091 _AX5043_TXPWRCOEFFB1NB	=	0x516a
                           00516D  2092 _AX5043_TXPWRCOEFFC0NB	=	0x516d
                           00516C  2093 _AX5043_TXPWRCOEFFC1NB	=	0x516c
                           00516F  2094 _AX5043_TXPWRCOEFFD0NB	=	0x516f
                           00516E  2095 _AX5043_TXPWRCOEFFD1NB	=	0x516e
                           005171  2096 _AX5043_TXPWRCOEFFE0NB	=	0x5171
                           005170  2097 _AX5043_TXPWRCOEFFE1NB	=	0x5170
                           005167  2098 _AX5043_TXRATE0NB	=	0x5167
                           005166  2099 _AX5043_TXRATE1NB	=	0x5166
                           005165  2100 _AX5043_TXRATE2NB	=	0x5165
                           00506B  2101 _AX5043_WAKEUP0NB	=	0x506b
                           00506A  2102 _AX5043_WAKEUP1NB	=	0x506a
                           00506D  2103 _AX5043_WAKEUPFREQ0NB	=	0x506d
                           00506C  2104 _AX5043_WAKEUPFREQ1NB	=	0x506c
                           005069  2105 _AX5043_WAKEUPTIMER0NB	=	0x5069
                           005068  2106 _AX5043_WAKEUPTIMER1NB	=	0x5068
                           00506E  2107 _AX5043_WAKEUPXOEARLYNB	=	0x506e
                           005F11  2108 _AX5043_XTALAMPLNB	=	0x5f11
                           005184  2109 _AX5043_XTALCAPNB	=	0x5184
                           005F10  2110 _AX5043_XTALOSCNB	=	0x5f10
                           00501D  2111 _AX5043_XTALSTATUSNB	=	0x501d
                           005F00  2112 _AX5043_0xF00NB	=	0x5f00
                           005F0C  2113 _AX5043_0xF0CNB	=	0x5f0c
                           005F18  2114 _AX5043_0xF18NB	=	0x5f18
                           005F1C  2115 _AX5043_0xF1CNB	=	0x5f1c
                           005F21  2116 _AX5043_0xF21NB	=	0x5f21
                           005F22  2117 _AX5043_0xF22NB	=	0x5f22
                           005F23  2118 _AX5043_0xF23NB	=	0x5f23
                           005F26  2119 _AX5043_0xF26NB	=	0x5f26
                           005F30  2120 _AX5043_0xF30NB	=	0x5f30
                           005F31  2121 _AX5043_0xF31NB	=	0x5f31
                           005F32  2122 _AX5043_0xF32NB	=	0x5f32
                           005F33  2123 _AX5043_0xF33NB	=	0x5f33
                           005F34  2124 _AX5043_0xF34NB	=	0x5f34
                           005F35  2125 _AX5043_0xF35NB	=	0x5f35
                           005F44  2126 _AX5043_0xF44NB	=	0x5f44
                           005122  2127 _AX5043_AGCAHYST0NB	=	0x5122
                           005132  2128 _AX5043_AGCAHYST1NB	=	0x5132
                           005142  2129 _AX5043_AGCAHYST2NB	=	0x5142
                           005152  2130 _AX5043_AGCAHYST3NB	=	0x5152
                           005120  2131 _AX5043_AGCGAIN0NB	=	0x5120
                           005130  2132 _AX5043_AGCGAIN1NB	=	0x5130
                           005140  2133 _AX5043_AGCGAIN2NB	=	0x5140
                           005150  2134 _AX5043_AGCGAIN3NB	=	0x5150
                           005123  2135 _AX5043_AGCMINMAX0NB	=	0x5123
                           005133  2136 _AX5043_AGCMINMAX1NB	=	0x5133
                           005143  2137 _AX5043_AGCMINMAX2NB	=	0x5143
                           005153  2138 _AX5043_AGCMINMAX3NB	=	0x5153
                           005121  2139 _AX5043_AGCTARGET0NB	=	0x5121
                           005131  2140 _AX5043_AGCTARGET1NB	=	0x5131
                           005141  2141 _AX5043_AGCTARGET2NB	=	0x5141
                           005151  2142 _AX5043_AGCTARGET3NB	=	0x5151
                           00512B  2143 _AX5043_AMPLITUDEGAIN0NB	=	0x512b
                           00513B  2144 _AX5043_AMPLITUDEGAIN1NB	=	0x513b
                           00514B  2145 _AX5043_AMPLITUDEGAIN2NB	=	0x514b
                           00515B  2146 _AX5043_AMPLITUDEGAIN3NB	=	0x515b
                           00512F  2147 _AX5043_BBOFFSRES0NB	=	0x512f
                           00513F  2148 _AX5043_BBOFFSRES1NB	=	0x513f
                           00514F  2149 _AX5043_BBOFFSRES2NB	=	0x514f
                           00515F  2150 _AX5043_BBOFFSRES3NB	=	0x515f
                           005125  2151 _AX5043_DRGAIN0NB	=	0x5125
                           005135  2152 _AX5043_DRGAIN1NB	=	0x5135
                           005145  2153 _AX5043_DRGAIN2NB	=	0x5145
                           005155  2154 _AX5043_DRGAIN3NB	=	0x5155
                           00512E  2155 _AX5043_FOURFSK0NB	=	0x512e
                           00513E  2156 _AX5043_FOURFSK1NB	=	0x513e
                           00514E  2157 _AX5043_FOURFSK2NB	=	0x514e
                           00515E  2158 _AX5043_FOURFSK3NB	=	0x515e
                           00512D  2159 _AX5043_FREQDEV00NB	=	0x512d
                           00513D  2160 _AX5043_FREQDEV01NB	=	0x513d
                           00514D  2161 _AX5043_FREQDEV02NB	=	0x514d
                           00515D  2162 _AX5043_FREQDEV03NB	=	0x515d
                           00512C  2163 _AX5043_FREQDEV10NB	=	0x512c
                           00513C  2164 _AX5043_FREQDEV11NB	=	0x513c
                           00514C  2165 _AX5043_FREQDEV12NB	=	0x514c
                           00515C  2166 _AX5043_FREQDEV13NB	=	0x515c
                           005127  2167 _AX5043_FREQUENCYGAINA0NB	=	0x5127
                           005137  2168 _AX5043_FREQUENCYGAINA1NB	=	0x5137
                           005147  2169 _AX5043_FREQUENCYGAINA2NB	=	0x5147
                           005157  2170 _AX5043_FREQUENCYGAINA3NB	=	0x5157
                           005128  2171 _AX5043_FREQUENCYGAINB0NB	=	0x5128
                           005138  2172 _AX5043_FREQUENCYGAINB1NB	=	0x5138
                           005148  2173 _AX5043_FREQUENCYGAINB2NB	=	0x5148
                           005158  2174 _AX5043_FREQUENCYGAINB3NB	=	0x5158
                           005129  2175 _AX5043_FREQUENCYGAINC0NB	=	0x5129
                           005139  2176 _AX5043_FREQUENCYGAINC1NB	=	0x5139
                           005149  2177 _AX5043_FREQUENCYGAINC2NB	=	0x5149
                           005159  2178 _AX5043_FREQUENCYGAINC3NB	=	0x5159
                           00512A  2179 _AX5043_FREQUENCYGAIND0NB	=	0x512a
                           00513A  2180 _AX5043_FREQUENCYGAIND1NB	=	0x513a
                           00514A  2181 _AX5043_FREQUENCYGAIND2NB	=	0x514a
                           00515A  2182 _AX5043_FREQUENCYGAIND3NB	=	0x515a
                           005116  2183 _AX5043_FREQUENCYLEAKNB	=	0x5116
                           005126  2184 _AX5043_PHASEGAIN0NB	=	0x5126
                           005136  2185 _AX5043_PHASEGAIN1NB	=	0x5136
                           005146  2186 _AX5043_PHASEGAIN2NB	=	0x5146
                           005156  2187 _AX5043_PHASEGAIN3NB	=	0x5156
                           005207  2188 _AX5043_PKTADDR0NB	=	0x5207
                           005206  2189 _AX5043_PKTADDR1NB	=	0x5206
                           005205  2190 _AX5043_PKTADDR2NB	=	0x5205
                           005204  2191 _AX5043_PKTADDR3NB	=	0x5204
                           005200  2192 _AX5043_PKTADDRCFGNB	=	0x5200
                           00520B  2193 _AX5043_PKTADDRMASK0NB	=	0x520b
                           00520A  2194 _AX5043_PKTADDRMASK1NB	=	0x520a
                           005209  2195 _AX5043_PKTADDRMASK2NB	=	0x5209
                           005208  2196 _AX5043_PKTADDRMASK3NB	=	0x5208
                           005201  2197 _AX5043_PKTLENCFGNB	=	0x5201
                           005202  2198 _AX5043_PKTLENOFFSETNB	=	0x5202
                           005203  2199 _AX5043_PKTMAXLENNB	=	0x5203
                           005118  2200 _AX5043_RXPARAMCURSETNB	=	0x5118
                           005117  2201 _AX5043_RXPARAMSETSNB	=	0x5117
                           005124  2202 _AX5043_TIMEGAIN0NB	=	0x5124
                           005134  2203 _AX5043_TIMEGAIN1NB	=	0x5134
                           005144  2204 _AX5043_TIMEGAIN2NB	=	0x5144
                           005154  2205 _AX5043_TIMEGAIN3NB	=	0x5154
      0003B2                       2206 _UBLOX_GPS_FletcherChecksum8_PARM_2:
      0003B2                       2207 	.ds 3
      0003B5                       2208 _UBLOX_GPS_FletcherChecksum8_PARM_3:
      0003B5                       2209 	.ds 3
      0003B8                       2210 _UBLOX_GPS_FletcherChecksum8_PARM_4:
      0003B8                       2211 	.ds 2
      0003BA                       2212 _UBLOX_GPS_FletcherChecksum8_buffer_1_170:
      0003BA                       2213 	.ds 3
      0003BD                       2214 _UBLOX_GPS_SendCommand_WaitACK_PARM_2:
      0003BD                       2215 	.ds 1
      0003BE                       2216 _UBLOX_GPS_SendCommand_WaitACK_PARM_3:
      0003BE                       2217 	.ds 2
      0003C0                       2218 _UBLOX_GPS_SendCommand_WaitACK_PARM_4:
      0003C0                       2219 	.ds 3
      0003C3                       2220 _UBLOX_GPS_SendCommand_WaitACK_PARM_5:
      0003C3                       2221 	.ds 1
      0003C4                       2222 _UBLOX_GPS_SendCommand_WaitACK_PARM_6:
      0003C4                       2223 	.ds 3
      0003C7                       2224 _UBLOX_GPS_SendCommand_WaitACK_msg_class_1_173:
      0003C7                       2225 	.ds 1
      0003C8                       2226 _UBLOX_GPS_SendCommand_WaitACK_RetVal_1_174:
      0003C8                       2227 	.ds 1
      0003C9                       2228 _UBLOX_GPS_SendCommand_WaitACK_CK_A_1_174:
      0003C9                       2229 	.ds 1
      0003CA                       2230 _UBLOX_GPS_SendCommand_WaitACK_CK_B_1_174:
      0003CA                       2231 	.ds 1
                                   2232 ;--------------------------------------------------------
                                   2233 ; absolute external ram data
                                   2234 ;--------------------------------------------------------
                                   2235 	.area XABS    (ABS,XDATA)
                                   2236 ;--------------------------------------------------------
                                   2237 ; external initialized ram data
                                   2238 ;--------------------------------------------------------
                                   2239 	.area XISEG   (XDATA)
                                   2240 	.area HOME    (CODE)
                                   2241 	.area GSINIT0 (CODE)
                                   2242 	.area GSINIT1 (CODE)
                                   2243 	.area GSINIT2 (CODE)
                                   2244 	.area GSINIT3 (CODE)
                                   2245 	.area GSINIT4 (CODE)
                                   2246 	.area GSINIT5 (CODE)
                                   2247 	.area GSINIT  (CODE)
                                   2248 	.area GSFINAL (CODE)
                                   2249 	.area CSEG    (CODE)
                                   2250 ;--------------------------------------------------------
                                   2251 ; global & static initialisations
                                   2252 ;--------------------------------------------------------
                                   2253 	.area HOME    (CODE)
                                   2254 	.area GSINIT  (CODE)
                                   2255 	.area GSFINAL (CODE)
                                   2256 	.area GSINIT  (CODE)
                                   2257 ;--------------------------------------------------------
                                   2258 ; Home
                                   2259 ;--------------------------------------------------------
                                   2260 	.area HOME    (CODE)
                                   2261 	.area HOME    (CODE)
                                   2262 ;--------------------------------------------------------
                                   2263 ; code
                                   2264 ;--------------------------------------------------------
                                   2265 	.area CSEG    (CODE)
                                   2266 ;------------------------------------------------------------
                                   2267 ;Allocation info for local variables in function 'UBLOX_GPS_PortInit'
                                   2268 ;------------------------------------------------------------
                                   2269 ;	..\src\COMMON\UBLOX.c:29: __reentrantb void UBLOX_GPS_PortInit(void) __reentrant
                                   2270 ;	-----------------------------------------
                                   2271 ;	 function UBLOX_GPS_PortInit
                                   2272 ;	-----------------------------------------
      005DE5                       2273 _UBLOX_GPS_PortInit:
                           000007  2274 	ar7 = 0x07
                           000006  2275 	ar6 = 0x06
                           000005  2276 	ar5 = 0x05
                           000004  2277 	ar4 = 0x04
                           000003  2278 	ar3 = 0x03
                           000002  2279 	ar2 = 0x02
                           000001  2280 	ar1 = 0x01
                           000000  2281 	ar0 = 0x00
                                   2282 ;	..\src\COMMON\UBLOX.c:31: uart_timer1_baud(CLKSRC_SYSCLK, 9600, 20000000UL);
      005DE5 E4               [12] 2283 	clr	a
      005DE6 C0 E0            [24] 2284 	push	acc
      005DE8 74 2D            [12] 2285 	mov	a,#0x2d
      005DEA C0 E0            [24] 2286 	push	acc
      005DEC 74 31            [12] 2287 	mov	a,#0x31
      005DEE C0 E0            [24] 2288 	push	acc
      005DF0 74 01            [12] 2289 	mov	a,#0x01
      005DF2 C0 E0            [24] 2290 	push	acc
      005DF4 03               [12] 2291 	rr	a
      005DF5 C0 E0            [24] 2292 	push	acc
      005DF7 74 25            [12] 2293 	mov	a,#0x25
      005DF9 C0 E0            [24] 2294 	push	acc
      005DFB E4               [12] 2295 	clr	a
      005DFC C0 E0            [24] 2296 	push	acc
      005DFE C0 E0            [24] 2297 	push	acc
      005E00 75 82 06         [24] 2298 	mov	dpl,#0x06
      005E03 12 7E 10         [24] 2299 	lcall	_uart_timer1_baud
      005E06 E5 81            [12] 2300 	mov	a,sp
      005E08 24 F8            [12] 2301 	add	a,#0xf8
      005E0A F5 81            [12] 2302 	mov	sp,a
                                   2303 ;	..\src\COMMON\UBLOX.c:32: uart1_init(1, 8, 1);
      005E0C 90 04 1C         [24] 2304 	mov	dptr,#_uart1_init_PARM_2
      005E0F 74 08            [12] 2305 	mov	a,#0x08
      005E11 F0               [24] 2306 	movx	@dptr,a
      005E12 90 04 1D         [24] 2307 	mov	dptr,#_uart1_init_PARM_3
      005E15 74 01            [12] 2308 	mov	a,#0x01
      005E17 F0               [24] 2309 	movx	@dptr,a
      005E18 75 82 01         [24] 2310 	mov	dpl,#0x01
      005E1B 02 78 72         [24] 2311 	ljmp	_uart1_init
                                   2312 ;------------------------------------------------------------
                                   2313 ;Allocation info for local variables in function 'UBLOX_GPS_FletcherChecksum8'
                                   2314 ;------------------------------------------------------------
                                   2315 ;CK_A                      Allocated with name '_UBLOX_GPS_FletcherChecksum8_PARM_2'
                                   2316 ;CK_B                      Allocated with name '_UBLOX_GPS_FletcherChecksum8_PARM_3'
                                   2317 ;length                    Allocated with name '_UBLOX_GPS_FletcherChecksum8_PARM_4'
                                   2318 ;buffer                    Allocated with name '_UBLOX_GPS_FletcherChecksum8_buffer_1_170'
                                   2319 ;i                         Allocated with name '_UBLOX_GPS_FletcherChecksum8_i_1_171'
                                   2320 ;sloc0                     Allocated with name '_UBLOX_GPS_FletcherChecksum8_sloc0_1_0'
                                   2321 ;sloc1                     Allocated with name '_UBLOX_GPS_FletcherChecksum8_sloc1_1_0'
                                   2322 ;sloc2                     Allocated with name '_UBLOX_GPS_FletcherChecksum8_sloc2_1_0'
                                   2323 ;sloc3                     Allocated with name '_UBLOX_GPS_FletcherChecksum8_sloc3_1_0'
                                   2324 ;------------------------------------------------------------
                                   2325 ;	..\src\COMMON\UBLOX.c:51: void UBLOX_GPS_FletcherChecksum8 ( uint8_t * buffer, uint8_t *CK_A, uint8_t *CK_B, uint16_t length)
                                   2326 ;	-----------------------------------------
                                   2327 ;	 function UBLOX_GPS_FletcherChecksum8
                                   2328 ;	-----------------------------------------
      005E1E                       2329 _UBLOX_GPS_FletcherChecksum8:
      005E1E AF F0            [24] 2330 	mov	r7,b
      005E20 AE 83            [24] 2331 	mov	r6,dph
      005E22 E5 82            [12] 2332 	mov	a,dpl
      005E24 90 03 BA         [24] 2333 	mov	dptr,#_UBLOX_GPS_FletcherChecksum8_buffer_1_170
      005E27 F0               [24] 2334 	movx	@dptr,a
      005E28 EE               [12] 2335 	mov	a,r6
      005E29 A3               [24] 2336 	inc	dptr
      005E2A F0               [24] 2337 	movx	@dptr,a
      005E2B EF               [12] 2338 	mov	a,r7
      005E2C A3               [24] 2339 	inc	dptr
      005E2D F0               [24] 2340 	movx	@dptr,a
                                   2341 ;	..\src\COMMON\UBLOX.c:54: *CK_A = 0;
      005E2E 90 03 B2         [24] 2342 	mov	dptr,#_UBLOX_GPS_FletcherChecksum8_PARM_2
      005E31 E0               [24] 2343 	movx	a,@dptr
      005E32 FD               [12] 2344 	mov	r5,a
      005E33 A3               [24] 2345 	inc	dptr
      005E34 E0               [24] 2346 	movx	a,@dptr
      005E35 FE               [12] 2347 	mov	r6,a
      005E36 A3               [24] 2348 	inc	dptr
      005E37 E0               [24] 2349 	movx	a,@dptr
      005E38 FF               [12] 2350 	mov	r7,a
      005E39 8D 82            [24] 2351 	mov	dpl,r5
      005E3B 8E 83            [24] 2352 	mov	dph,r6
      005E3D 8F F0            [24] 2353 	mov	b,r7
      005E3F E4               [12] 2354 	clr	a
      005E40 12 87 88         [24] 2355 	lcall	__gptrput
                                   2356 ;	..\src\COMMON\UBLOX.c:55: *CK_B = 0;
      005E43 90 03 B5         [24] 2357 	mov	dptr,#_UBLOX_GPS_FletcherChecksum8_PARM_3
      005E46 E0               [24] 2358 	movx	a,@dptr
      005E47 F5 6F            [12] 2359 	mov	_UBLOX_GPS_FletcherChecksum8_sloc3_1_0,a
      005E49 A3               [24] 2360 	inc	dptr
      005E4A E0               [24] 2361 	movx	a,@dptr
      005E4B F5 70            [12] 2362 	mov	(_UBLOX_GPS_FletcherChecksum8_sloc3_1_0 + 1),a
      005E4D A3               [24] 2363 	inc	dptr
      005E4E E0               [24] 2364 	movx	a,@dptr
      005E4F F5 71            [12] 2365 	mov	(_UBLOX_GPS_FletcherChecksum8_sloc3_1_0 + 2),a
      005E51 85 6F 82         [24] 2366 	mov	dpl,_UBLOX_GPS_FletcherChecksum8_sloc3_1_0
      005E54 85 70 83         [24] 2367 	mov	dph,(_UBLOX_GPS_FletcherChecksum8_sloc3_1_0 + 1)
      005E57 85 71 F0         [24] 2368 	mov	b,(_UBLOX_GPS_FletcherChecksum8_sloc3_1_0 + 2)
      005E5A E4               [12] 2369 	clr	a
      005E5B 12 87 88         [24] 2370 	lcall	__gptrput
                                   2371 ;	..\src\COMMON\UBLOX.c:56: for (i = 0; i < length; i++)
      005E5E 90 03 BA         [24] 2372 	mov	dptr,#_UBLOX_GPS_FletcherChecksum8_buffer_1_170
      005E61 E0               [24] 2373 	movx	a,@dptr
      005E62 F5 6A            [12] 2374 	mov	_UBLOX_GPS_FletcherChecksum8_sloc0_1_0,a
      005E64 A3               [24] 2375 	inc	dptr
      005E65 E0               [24] 2376 	movx	a,@dptr
      005E66 F5 6B            [12] 2377 	mov	(_UBLOX_GPS_FletcherChecksum8_sloc0_1_0 + 1),a
      005E68 A3               [24] 2378 	inc	dptr
      005E69 E0               [24] 2379 	movx	a,@dptr
      005E6A F5 6C            [12] 2380 	mov	(_UBLOX_GPS_FletcherChecksum8_sloc0_1_0 + 2),a
      005E6C 90 03 B8         [24] 2381 	mov	dptr,#_UBLOX_GPS_FletcherChecksum8_PARM_4
      005E6F E0               [24] 2382 	movx	a,@dptr
      005E70 F8               [12] 2383 	mov	r0,a
      005E71 A3               [24] 2384 	inc	dptr
      005E72 E0               [24] 2385 	movx	a,@dptr
      005E73 F9               [12] 2386 	mov	r1,a
      005E74 75 6D 00         [24] 2387 	mov	_UBLOX_GPS_FletcherChecksum8_sloc1_1_0,#0x00
      005E77                       2388 00103$:
      005E77 AB 6D            [24] 2389 	mov	r3,_UBLOX_GPS_FletcherChecksum8_sloc1_1_0
      005E79 7C 00            [12] 2390 	mov	r4,#0x00
      005E7B C3               [12] 2391 	clr	c
      005E7C EB               [12] 2392 	mov	a,r3
      005E7D 98               [12] 2393 	subb	a,r0
      005E7E EC               [12] 2394 	mov	a,r4
      005E7F 99               [12] 2395 	subb	a,r1
      005E80 50 51            [24] 2396 	jnc	00101$
                                   2397 ;	..\src\COMMON\UBLOX.c:58: *CK_A += buffer[i];
      005E82 C0 00            [24] 2398 	push	ar0
      005E84 C0 01            [24] 2399 	push	ar1
      005E86 8D 82            [24] 2400 	mov	dpl,r5
      005E88 8E 83            [24] 2401 	mov	dph,r6
      005E8A 8F F0            [24] 2402 	mov	b,r7
      005E8C 12 96 F0         [24] 2403 	lcall	__gptrget
      005E8F F5 6E            [12] 2404 	mov	_UBLOX_GPS_FletcherChecksum8_sloc2_1_0,a
      005E91 E5 6D            [12] 2405 	mov	a,_UBLOX_GPS_FletcherChecksum8_sloc1_1_0
      005E93 25 6A            [12] 2406 	add	a,_UBLOX_GPS_FletcherChecksum8_sloc0_1_0
      005E95 F8               [12] 2407 	mov	r0,a
      005E96 E4               [12] 2408 	clr	a
      005E97 35 6B            [12] 2409 	addc	a,(_UBLOX_GPS_FletcherChecksum8_sloc0_1_0 + 1)
      005E99 F9               [12] 2410 	mov	r1,a
      005E9A AC 6C            [24] 2411 	mov	r4,(_UBLOX_GPS_FletcherChecksum8_sloc0_1_0 + 2)
      005E9C 88 82            [24] 2412 	mov	dpl,r0
      005E9E 89 83            [24] 2413 	mov	dph,r1
      005EA0 8C F0            [24] 2414 	mov	b,r4
      005EA2 12 96 F0         [24] 2415 	lcall	__gptrget
      005EA5 25 6E            [12] 2416 	add	a,_UBLOX_GPS_FletcherChecksum8_sloc2_1_0
      005EA7 F8               [12] 2417 	mov	r0,a
      005EA8 8D 82            [24] 2418 	mov	dpl,r5
      005EAA 8E 83            [24] 2419 	mov	dph,r6
      005EAC 8F F0            [24] 2420 	mov	b,r7
      005EAE 12 87 88         [24] 2421 	lcall	__gptrput
                                   2422 ;	..\src\COMMON\UBLOX.c:60: *CK_B += *CK_A;
      005EB1 85 6F 82         [24] 2423 	mov	dpl,_UBLOX_GPS_FletcherChecksum8_sloc3_1_0
      005EB4 85 70 83         [24] 2424 	mov	dph,(_UBLOX_GPS_FletcherChecksum8_sloc3_1_0 + 1)
      005EB7 85 71 F0         [24] 2425 	mov	b,(_UBLOX_GPS_FletcherChecksum8_sloc3_1_0 + 2)
      005EBA 12 96 F0         [24] 2426 	lcall	__gptrget
      005EBD FC               [12] 2427 	mov	r4,a
      005EBE 28               [12] 2428 	add	a,r0
      005EBF 85 6F 82         [24] 2429 	mov	dpl,_UBLOX_GPS_FletcherChecksum8_sloc3_1_0
      005EC2 85 70 83         [24] 2430 	mov	dph,(_UBLOX_GPS_FletcherChecksum8_sloc3_1_0 + 1)
      005EC5 85 71 F0         [24] 2431 	mov	b,(_UBLOX_GPS_FletcherChecksum8_sloc3_1_0 + 2)
      005EC8 12 87 88         [24] 2432 	lcall	__gptrput
                                   2433 ;	..\src\COMMON\UBLOX.c:56: for (i = 0; i < length; i++)
      005ECB 05 6D            [12] 2434 	inc	_UBLOX_GPS_FletcherChecksum8_sloc1_1_0
      005ECD D0 01            [24] 2435 	pop	ar1
      005ECF D0 00            [24] 2436 	pop	ar0
      005ED1 80 A4            [24] 2437 	sjmp	00103$
      005ED3                       2438 00101$:
                                   2439 ;	..\src\COMMON\UBLOX.c:62: return;
      005ED3 22               [24] 2440 	ret
                                   2441 ;------------------------------------------------------------
                                   2442 ;Allocation info for local variables in function 'UBLOX_GPS_SendCommand_WaitACK'
                                   2443 ;------------------------------------------------------------
                                   2444 ;sloc0                     Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0'
                                   2445 ;sloc1                     Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0'
                                   2446 ;sloc2                     Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0'
                                   2447 ;sloc3                     Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0'
                                   2448 ;sloc4                     Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0'
                                   2449 ;msg_id                    Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_PARM_2'
                                   2450 ;msg_length                Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_PARM_3'
                                   2451 ;payload                   Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_PARM_4'
                                   2452 ;AnsOrAck                  Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_PARM_5'
                                   2453 ;RxLength                  Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_PARM_6'
                                   2454 ;msg_class                 Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_msg_class_1_173'
                                   2455 ;i                         Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_i_1_174'
                                   2456 ;j                         Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_j_1_174'
                                   2457 ;k                         Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_k_1_174'
                                   2458 ;RetVal                    Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_RetVal_1_174'
                                   2459 ;buffer_aux                Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_174'
                                   2460 ;rx_buffer                 Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_174'
                                   2461 ;CK_A                      Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_CK_A_1_174'
                                   2462 ;CK_B                      Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_CK_B_1_174'
                                   2463 ;aux                       Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_aux_1_174'
                                   2464 ;------------------------------------------------------------
                                   2465 ;	..\src\COMMON\UBLOX.c:131: uint8_t UBLOX_GPS_SendCommand_WaitACK(uint8_t msg_class, uint8_t msg_id, uint16_t msg_length,uint8_t *payload,uint8_t AnsOrAck,uint16_t *RxLength)
                                   2466 ;	-----------------------------------------
                                   2467 ;	 function UBLOX_GPS_SendCommand_WaitACK
                                   2468 ;	-----------------------------------------
      005ED4                       2469 _UBLOX_GPS_SendCommand_WaitACK:
      005ED4 E5 82            [12] 2470 	mov	a,dpl
      005ED6 90 03 C7         [24] 2471 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_msg_class_1_173
      005ED9 F0               [24] 2472 	movx	@dptr,a
                                   2473 ;	..\src\COMMON\UBLOX.c:134: uint8_t RetVal = 0;
      005EDA 90 03 C8         [24] 2474 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_RetVal_1_174
      005EDD E4               [12] 2475 	clr	a
      005EDE F0               [24] 2476 	movx	@dptr,a
                                   2477 ;	..\src\COMMON\UBLOX.c:139: buffer_aux =(uint8_t *) malloc((msg_length)+10);
      005EDF 90 03 BE         [24] 2478 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_3
      005EE2 E0               [24] 2479 	movx	a,@dptr
      005EE3 FE               [12] 2480 	mov	r6,a
      005EE4 A3               [24] 2481 	inc	dptr
      005EE5 E0               [24] 2482 	movx	a,@dptr
      005EE6 FF               [12] 2483 	mov	r7,a
      005EE7 74 0A            [12] 2484 	mov	a,#0x0a
      005EE9 2E               [12] 2485 	add	a,r6
      005EEA FC               [12] 2486 	mov	r4,a
      005EEB E4               [12] 2487 	clr	a
      005EEC 3F               [12] 2488 	addc	a,r7
      005EED FD               [12] 2489 	mov	r5,a
      005EEE 8C 82            [24] 2490 	mov	dpl,r4
      005EF0 8D 83            [24] 2491 	mov	dph,r5
      005EF2 C0 07            [24] 2492 	push	ar7
      005EF4 C0 06            [24] 2493 	push	ar6
      005EF6 12 89 2E         [24] 2494 	lcall	_malloc
      005EF9 AC 82            [24] 2495 	mov	r4,dpl
      005EFB AD 83            [24] 2496 	mov	r5,dph
      005EFD 8C 34            [24] 2497 	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0,r4
      005EFF 8D 35            [24] 2498 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0 + 1),r5
                                   2499 ;	..\src\COMMON\UBLOX.c:140: rx_buffer =(uint8_t *) malloc(30);
      005F01 90 00 1E         [24] 2500 	mov	dptr,#0x001e
      005F04 12 89 2E         [24] 2501 	lcall	_malloc
      005F07 AA 82            [24] 2502 	mov	r2,dpl
      005F09 AB 83            [24] 2503 	mov	r3,dph
      005F0B D0 06            [24] 2504 	pop	ar6
      005F0D D0 07            [24] 2505 	pop	ar7
                                   2506 ;	..\src\COMMON\UBLOX.c:141: for(i = 0; i<40;i++)
      005F0F 79 00            [12] 2507 	mov	r1,#0x00
      005F11                       2508 00125$:
                                   2509 ;	..\src\COMMON\UBLOX.c:143: *(rx_buffer+i)=0;
      005F11 E9               [12] 2510 	mov	a,r1
      005F12 2A               [12] 2511 	add	a,r2
      005F13 F5 82            [12] 2512 	mov	dpl,a
      005F15 E4               [12] 2513 	clr	a
      005F16 3B               [12] 2514 	addc	a,r3
      005F17 F5 83            [12] 2515 	mov	dph,a
      005F19 E4               [12] 2516 	clr	a
      005F1A F0               [24] 2517 	movx	@dptr,a
                                   2518 ;	..\src\COMMON\UBLOX.c:141: for(i = 0; i<40;i++)
      005F1B 09               [12] 2519 	inc	r1
      005F1C B9 28 00         [24] 2520 	cjne	r1,#0x28,00186$
      005F1F                       2521 00186$:
      005F1F 40 F0            [24] 2522 	jc	00125$
                                   2523 ;	..\src\COMMON\UBLOX.c:146: *buffer_aux = msg_class;
      005F21 C0 02            [24] 2524 	push	ar2
      005F23 C0 03            [24] 2525 	push	ar3
      005F25 90 03 C7         [24] 2526 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_msg_class_1_173
      005F28 E0               [24] 2527 	movx	a,@dptr
      005F29 F9               [12] 2528 	mov	r1,a
      005F2A 85 34 82         [24] 2529 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0
      005F2D 85 35 83         [24] 2530 	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0 + 1)
      005F30 F0               [24] 2531 	movx	@dptr,a
                                   2532 ;	..\src\COMMON\UBLOX.c:147: buffer_aux++;
      005F31 74 01            [12] 2533 	mov	a,#0x01
      005F33 25 34            [12] 2534 	add	a,_UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0
      005F35 F8               [12] 2535 	mov	r0,a
      005F36 E4               [12] 2536 	clr	a
      005F37 35 35            [12] 2537 	addc	a,(_UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0 + 1)
      005F39 FB               [12] 2538 	mov	r3,a
                                   2539 ;	..\src\COMMON\UBLOX.c:148: *buffer_aux = msg_id;
      005F3A 90 03 BD         [24] 2540 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_2
      005F3D E0               [24] 2541 	movx	a,@dptr
      005F3E F5 2D            [12] 2542 	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0,a
      005F40 88 82            [24] 2543 	mov	dpl,r0
      005F42 8B 83            [24] 2544 	mov	dph,r3
      005F44 F0               [24] 2545 	movx	@dptr,a
                                   2546 ;	..\src\COMMON\UBLOX.c:149: buffer_aux++;
      005F45 85 34 82         [24] 2547 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0
      005F48 85 35 83         [24] 2548 	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0 + 1)
      005F4B A3               [24] 2549 	inc	dptr
      005F4C A3               [24] 2550 	inc	dptr
                                   2551 ;	..\src\COMMON\UBLOX.c:150: *buffer_aux = msg_length;
      005F4D 8E 03            [24] 2552 	mov	ar3,r6
      005F4F EB               [12] 2553 	mov	a,r3
      005F50 F0               [24] 2554 	movx	@dptr,a
                                   2555 ;	..\src\COMMON\UBLOX.c:151: buffer_aux +=2;
      005F51 74 04            [12] 2556 	mov	a,#0x04
      005F53 25 34            [12] 2557 	add	a,_UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0
      005F55 FA               [12] 2558 	mov	r2,a
      005F56 E4               [12] 2559 	clr	a
      005F57 35 35            [12] 2560 	addc	a,(_UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0 + 1)
                                   2561 ;	..\src\COMMON\UBLOX.c:152: memcpy(buffer_aux,payload,msg_length);
      005F59 F8               [12] 2562 	mov	r0,a
      005F5A 7B 00            [12] 2563 	mov	r3,#0x00
      005F5C 90 03 C0         [24] 2564 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_4
      005F5F E0               [24] 2565 	movx	a,@dptr
      005F60 F5 2E            [12] 2566 	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0,a
      005F62 A3               [24] 2567 	inc	dptr
      005F63 E0               [24] 2568 	movx	a,@dptr
      005F64 F5 2F            [12] 2569 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0 + 1),a
      005F66 A3               [24] 2570 	inc	dptr
      005F67 E0               [24] 2571 	movx	a,@dptr
      005F68 F5 30            [12] 2572 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0 + 2),a
      005F6A 90 04 3D         [24] 2573 	mov	dptr,#_memcpy_PARM_2
      005F6D E5 2E            [12] 2574 	mov	a,_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0
      005F6F F0               [24] 2575 	movx	@dptr,a
      005F70 E5 2F            [12] 2576 	mov	a,(_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0 + 1)
      005F72 A3               [24] 2577 	inc	dptr
      005F73 F0               [24] 2578 	movx	@dptr,a
      005F74 E5 30            [12] 2579 	mov	a,(_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0 + 2)
      005F76 A3               [24] 2580 	inc	dptr
      005F77 F0               [24] 2581 	movx	@dptr,a
      005F78 90 04 40         [24] 2582 	mov	dptr,#_memcpy_PARM_3
      005F7B EE               [12] 2583 	mov	a,r6
      005F7C F0               [24] 2584 	movx	@dptr,a
      005F7D EF               [12] 2585 	mov	a,r7
      005F7E A3               [24] 2586 	inc	dptr
      005F7F F0               [24] 2587 	movx	@dptr,a
      005F80 8A 82            [24] 2588 	mov	dpl,r2
      005F82 88 83            [24] 2589 	mov	dph,r0
      005F84 8B F0            [24] 2590 	mov	b,r3
      005F86 C0 07            [24] 2591 	push	ar7
      005F88 C0 06            [24] 2592 	push	ar6
      005F8A C0 03            [24] 2593 	push	ar3
      005F8C C0 02            [24] 2594 	push	ar2
      005F8E C0 01            [24] 2595 	push	ar1
      005F90 12 83 01         [24] 2596 	lcall	_memcpy
      005F93 D0 01            [24] 2597 	pop	ar1
      005F95 D0 02            [24] 2598 	pop	ar2
      005F97 D0 03            [24] 2599 	pop	ar3
      005F99 D0 06            [24] 2600 	pop	ar6
      005F9B D0 07            [24] 2601 	pop	ar7
                                   2602 ;	..\src\COMMON\UBLOX.c:154: UBLOX_GPS_FletcherChecksum8(buffer_aux,&CK_A,&CK_B,(msg_length)+4);
      005F9D AA 34            [24] 2603 	mov	r2,_UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0
      005F9F AB 35            [24] 2604 	mov	r3,(_UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0 + 1)
      005FA1 8A 31            [24] 2605 	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0,r2
      005FA3 8B 32            [24] 2606 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0 + 1),r3
      005FA5 75 33 00         [24] 2607 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0 + 2),#0x00
      005FA8 74 04            [12] 2608 	mov	a,#0x04
      005FAA 2E               [12] 2609 	add	a,r6
      005FAB FA               [12] 2610 	mov	r2,a
      005FAC E4               [12] 2611 	clr	a
      005FAD 3F               [12] 2612 	addc	a,r7
      005FAE FB               [12] 2613 	mov	r3,a
      005FAF 90 03 B2         [24] 2614 	mov	dptr,#_UBLOX_GPS_FletcherChecksum8_PARM_2
      005FB2 74 C9            [12] 2615 	mov	a,#_UBLOX_GPS_SendCommand_WaitACK_CK_A_1_174
      005FB4 F0               [24] 2616 	movx	@dptr,a
      005FB5 74 03            [12] 2617 	mov	a,#(_UBLOX_GPS_SendCommand_WaitACK_CK_A_1_174 >> 8)
      005FB7 A3               [24] 2618 	inc	dptr
      005FB8 F0               [24] 2619 	movx	@dptr,a
      005FB9 E4               [12] 2620 	clr	a
      005FBA A3               [24] 2621 	inc	dptr
      005FBB F0               [24] 2622 	movx	@dptr,a
      005FBC 90 03 B5         [24] 2623 	mov	dptr,#_UBLOX_GPS_FletcherChecksum8_PARM_3
      005FBF 74 CA            [12] 2624 	mov	a,#_UBLOX_GPS_SendCommand_WaitACK_CK_B_1_174
      005FC1 F0               [24] 2625 	movx	@dptr,a
      005FC2 74 03            [12] 2626 	mov	a,#(_UBLOX_GPS_SendCommand_WaitACK_CK_B_1_174 >> 8)
      005FC4 A3               [24] 2627 	inc	dptr
      005FC5 F0               [24] 2628 	movx	@dptr,a
      005FC6 E4               [12] 2629 	clr	a
      005FC7 A3               [24] 2630 	inc	dptr
      005FC8 F0               [24] 2631 	movx	@dptr,a
      005FC9 90 03 B8         [24] 2632 	mov	dptr,#_UBLOX_GPS_FletcherChecksum8_PARM_4
      005FCC EA               [12] 2633 	mov	a,r2
      005FCD F0               [24] 2634 	movx	@dptr,a
      005FCE EB               [12] 2635 	mov	a,r3
      005FCF A3               [24] 2636 	inc	dptr
      005FD0 F0               [24] 2637 	movx	@dptr,a
      005FD1 85 31 82         [24] 2638 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0
      005FD4 85 32 83         [24] 2639 	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0 + 1)
      005FD7 85 33 F0         [24] 2640 	mov	b,(_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0 + 2)
      005FDA C0 07            [24] 2641 	push	ar7
      005FDC C0 06            [24] 2642 	push	ar6
      005FDE C0 03            [24] 2643 	push	ar3
      005FE0 C0 02            [24] 2644 	push	ar2
      005FE2 C0 01            [24] 2645 	push	ar1
      005FE4 12 5E 1E         [24] 2646 	lcall	_UBLOX_GPS_FletcherChecksum8
      005FE7 D0 01            [24] 2647 	pop	ar1
      005FE9 D0 02            [24] 2648 	pop	ar2
      005FEB D0 03            [24] 2649 	pop	ar3
      005FED D0 06            [24] 2650 	pop	ar6
      005FEF D0 07            [24] 2651 	pop	ar7
                                   2652 ;	..\src\COMMON\UBLOX.c:156: uart1_tx(UBX_HEADER1_VAL);
      005FF1 75 82 B5         [24] 2653 	mov	dpl,#0xb5
      005FF4 12 88 B3         [24] 2654 	lcall	_uart1_tx
                                   2655 ;	..\src\COMMON\UBLOX.c:157: uart1_tx(UBX_HEADER2_VAL);
      005FF7 75 82 62         [24] 2656 	mov	dpl,#0x62
      005FFA 12 88 B3         [24] 2657 	lcall	_uart1_tx
                                   2658 ;	..\src\COMMON\UBLOX.c:158: uart1_tx(msg_class);
      005FFD 89 82            [24] 2659 	mov	dpl,r1
      005FFF 12 88 B3         [24] 2660 	lcall	_uart1_tx
                                   2661 ;	..\src\COMMON\UBLOX.c:159: uart1_tx(msg_id);
      006002 85 2D 82         [24] 2662 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0
      006005 12 88 B3         [24] 2663 	lcall	_uart1_tx
                                   2664 ;	..\src\COMMON\UBLOX.c:160: uart1_tx((uint8_t)(msg_length & 0x00FF));//LSB
      006008 8E 02            [24] 2665 	mov	ar2,r6
      00600A 8A 82            [24] 2666 	mov	dpl,r2
      00600C 12 88 B3         [24] 2667 	lcall	_uart1_tx
                                   2668 ;	..\src\COMMON\UBLOX.c:161: uart1_tx((uint8_t)((msg_length & 0xFF00)>>8));//MSB
      00600F 8F 03            [24] 2669 	mov	ar3,r7
      006011 8B 02            [24] 2670 	mov	ar2,r3
      006013 8A 82            [24] 2671 	mov	dpl,r2
      006015 12 88 B3         [24] 2672 	lcall	_uart1_tx
                                   2673 ;	..\src\COMMON\UBLOX.c:164: for(i = 0;i<msg_length;i++)
      006018 75 31 00         [24] 2674 	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0,#0x00
                                   2675 ;	..\src\COMMON\UBLOX.c:216: return RetVal;
      00601B D0 03            [24] 2676 	pop	ar3
      00601D D0 02            [24] 2677 	pop	ar2
                                   2678 ;	..\src\COMMON\UBLOX.c:164: for(i = 0;i<msg_length;i++)
      00601F                       2679 00128$:
      00601F C0 01            [24] 2680 	push	ar1
      006021 A8 31            [24] 2681 	mov	r0,_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0
      006023 79 00            [12] 2682 	mov	r1,#0x00
      006025 C3               [12] 2683 	clr	c
      006026 E8               [12] 2684 	mov	a,r0
      006027 9E               [12] 2685 	subb	a,r6
      006028 E9               [12] 2686 	mov	a,r1
      006029 9F               [12] 2687 	subb	a,r7
      00602A D0 01            [24] 2688 	pop	ar1
      00602C 50 22            [24] 2689 	jnc	00102$
                                   2690 ;	..\src\COMMON\UBLOX.c:166: uart1_tx(*(payload+i));
      00602E C0 01            [24] 2691 	push	ar1
      006030 E5 31            [12] 2692 	mov	a,_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0
      006032 25 2E            [12] 2693 	add	a,_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0
      006034 F8               [12] 2694 	mov	r0,a
      006035 E4               [12] 2695 	clr	a
      006036 35 2F            [12] 2696 	addc	a,(_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0 + 1)
      006038 F9               [12] 2697 	mov	r1,a
      006039 AD 30            [24] 2698 	mov	r5,(_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0 + 2)
      00603B 88 82            [24] 2699 	mov	dpl,r0
      00603D 89 83            [24] 2700 	mov	dph,r1
      00603F 8D F0            [24] 2701 	mov	b,r5
      006041 12 96 F0         [24] 2702 	lcall	__gptrget
      006044 F8               [12] 2703 	mov	r0,a
      006045 F5 82            [12] 2704 	mov	dpl,a
      006047 12 88 B3         [24] 2705 	lcall	_uart1_tx
                                   2706 ;	..\src\COMMON\UBLOX.c:164: for(i = 0;i<msg_length;i++)
      00604A 05 31            [12] 2707 	inc	_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0
      00604C D0 01            [24] 2708 	pop	ar1
      00604E 80 CF            [24] 2709 	sjmp	00128$
      006050                       2710 00102$:
                                   2711 ;	..\src\COMMON\UBLOX.c:169: uart1_tx(CK_A);
      006050 90 03 C9         [24] 2712 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_CK_A_1_174
      006053 E0               [24] 2713 	movx	a,@dptr
      006054 F5 82            [12] 2714 	mov	dpl,a
      006056 12 88 B3         [24] 2715 	lcall	_uart1_tx
                                   2716 ;	..\src\COMMON\UBLOX.c:170: uart1_tx(CK_B);
      006059 90 03 CA         [24] 2717 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_CK_B_1_174
      00605C E0               [24] 2718 	movx	a,@dptr
      00605D FF               [12] 2719 	mov	r7,a
      00605E F5 82            [12] 2720 	mov	dpl,a
      006060 12 88 B3         [24] 2721 	lcall	_uart1_tx
                                   2722 ;	..\src\COMMON\UBLOX.c:173: do{
      006063                       2723 00103$:
                                   2724 ;	..\src\COMMON\UBLOX.c:174: delay(2500);
      006063 90 09 C4         [24] 2725 	mov	dptr,#0x09c4
      006066 12 90 78         [24] 2726 	lcall	_delay
                                   2727 ;	..\src\COMMON\UBLOX.c:175: }while(!uart1_rxcount());
      006069 12 A0 E9         [24] 2728 	lcall	_uart1_rxcount
      00606C E5 82            [12] 2729 	mov	a,dpl
      00606E 60 F3            [24] 2730 	jz	00103$
                                   2731 ;	..\src\COMMON\UBLOX.c:176: delay(25000);
      006070 90 61 A8         [24] 2732 	mov	dptr,#0x61a8
      006073 12 90 78         [24] 2733 	lcall	_delay
                                   2734 ;	..\src\COMMON\UBLOX.c:179: do
      006076 7F 00            [12] 2735 	mov	r7,#0x00
      006078                       2736 00106$:
                                   2737 ;	..\src\COMMON\UBLOX.c:181: wtimer_runcallbacks(); // si no pongo esto por algun motivo se pierden los eventos de timer , cuidado con los delays !!
      006078 C0 07            [24] 2738 	push	ar7
      00607A C0 03            [24] 2739 	push	ar3
      00607C C0 02            [24] 2740 	push	ar2
      00607E C0 01            [24] 2741 	push	ar1
      006080 12 81 FC         [24] 2742 	lcall	_wtimer_runcallbacks
      006083 D0 01            [24] 2743 	pop	ar1
      006085 D0 02            [24] 2744 	pop	ar2
      006087 D0 03            [24] 2745 	pop	ar3
      006089 D0 07            [24] 2746 	pop	ar7
                                   2747 ;	..\src\COMMON\UBLOX.c:182: *(rx_buffer+k)=uart1_rx();
      00608B EF               [12] 2748 	mov	a,r7
      00608C 2A               [12] 2749 	add	a,r2
      00608D FD               [12] 2750 	mov	r5,a
      00608E E4               [12] 2751 	clr	a
      00608F 3B               [12] 2752 	addc	a,r3
      006090 FE               [12] 2753 	mov	r6,a
      006091 12 87 D5         [24] 2754 	lcall	_uart1_rx
      006094 AC 82            [24] 2755 	mov	r4,dpl
      006096 8D 82            [24] 2756 	mov	dpl,r5
      006098 8E 83            [24] 2757 	mov	dph,r6
      00609A EC               [12] 2758 	mov	a,r4
      00609B F0               [24] 2759 	movx	@dptr,a
                                   2760 ;	..\src\COMMON\UBLOX.c:183: k++;
      00609C 0F               [12] 2761 	inc	r7
                                   2762 ;	..\src\COMMON\UBLOX.c:184: delay(2500);
      00609D 90 09 C4         [24] 2763 	mov	dptr,#0x09c4
      0060A0 12 90 78         [24] 2764 	lcall	_delay
                                   2765 ;	..\src\COMMON\UBLOX.c:185: }while(uart1_rxcount());
      0060A3 12 A0 E9         [24] 2766 	lcall	_uart1_rxcount
      0060A6 E5 82            [12] 2767 	mov	a,dpl
      0060A8 70 CE            [24] 2768 	jnz	00106$
                                   2769 ;	..\src\COMMON\UBLOX.c:188: if(rx_buffer[UBX_HEADER1_POS] == UBX_HEADER1_VAL && rx_buffer[UBX_HEADER2_POS] == UBX_HEADER2_VAL)
      0060AA 8A 82            [24] 2770 	mov	dpl,r2
      0060AC 8B 83            [24] 2771 	mov	dph,r3
      0060AE E0               [24] 2772 	movx	a,@dptr
      0060AF FE               [12] 2773 	mov	r6,a
      0060B0 BE B5 02         [24] 2774 	cjne	r6,#0xb5,00191$
      0060B3 80 03            [24] 2775 	sjmp	00192$
      0060B5                       2776 00191$:
      0060B5 02 62 5D         [24] 2777 	ljmp	00123$
      0060B8                       2778 00192$:
      0060B8 8A 82            [24] 2779 	mov	dpl,r2
      0060BA 8B 83            [24] 2780 	mov	dph,r3
      0060BC A3               [24] 2781 	inc	dptr
      0060BD E0               [24] 2782 	movx	a,@dptr
      0060BE FE               [12] 2783 	mov	r6,a
      0060BF BE 62 02         [24] 2784 	cjne	r6,#0x62,00193$
      0060C2 80 03            [24] 2785 	sjmp	00194$
      0060C4                       2786 00193$:
      0060C4 02 62 5D         [24] 2787 	ljmp	00123$
      0060C7                       2788 00194$:
                                   2789 ;	..\src\COMMON\UBLOX.c:190: CK_A = 0;
      0060C7 C0 01            [24] 2790 	push	ar1
      0060C9 90 03 C9         [24] 2791 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_CK_A_1_174
      0060CC E4               [12] 2792 	clr	a
      0060CD F0               [24] 2793 	movx	@dptr,a
                                   2794 ;	..\src\COMMON\UBLOX.c:191: CK_B = 0;
      0060CE 90 03 CA         [24] 2795 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_CK_B_1_174
      0060D1 F0               [24] 2796 	movx	@dptr,a
                                   2797 ;	..\src\COMMON\UBLOX.c:192: UBLOX_GPS_FletcherChecksum8(&rx_buffer[UBX_MESSAGE_CLASS],&CK_A,&CK_B,k-4);
      0060D2 74 02            [12] 2798 	mov	a,#0x02
      0060D4 2A               [12] 2799 	add	a,r2
      0060D5 FD               [12] 2800 	mov	r5,a
      0060D6 E4               [12] 2801 	clr	a
      0060D7 3B               [12] 2802 	addc	a,r3
      0060D8 FE               [12] 2803 	mov	r6,a
      0060D9 8D 36            [24] 2804 	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0,r5
      0060DB 8E 37            [24] 2805 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0 + 1),r6
      0060DD 75 38 00         [24] 2806 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0 + 2),#0x00
      0060E0 8F 31            [24] 2807 	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0,r7
      0060E2 75 32 00         [24] 2808 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0 + 1),#0x00
      0060E5 E5 31            [12] 2809 	mov	a,_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0
      0060E7 24 FC            [12] 2810 	add	a,#0xfc
      0060E9 FC               [12] 2811 	mov	r4,a
      0060EA E5 32            [12] 2812 	mov	a,(_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0 + 1)
      0060EC 34 FF            [12] 2813 	addc	a,#0xff
      0060EE FF               [12] 2814 	mov	r7,a
      0060EF 90 03 B2         [24] 2815 	mov	dptr,#_UBLOX_GPS_FletcherChecksum8_PARM_2
      0060F2 74 C9            [12] 2816 	mov	a,#_UBLOX_GPS_SendCommand_WaitACK_CK_A_1_174
      0060F4 F0               [24] 2817 	movx	@dptr,a
      0060F5 74 03            [12] 2818 	mov	a,#(_UBLOX_GPS_SendCommand_WaitACK_CK_A_1_174 >> 8)
      0060F7 A3               [24] 2819 	inc	dptr
      0060F8 F0               [24] 2820 	movx	@dptr,a
      0060F9 E4               [12] 2821 	clr	a
      0060FA A3               [24] 2822 	inc	dptr
      0060FB F0               [24] 2823 	movx	@dptr,a
      0060FC 90 03 B5         [24] 2824 	mov	dptr,#_UBLOX_GPS_FletcherChecksum8_PARM_3
      0060FF 74 CA            [12] 2825 	mov	a,#_UBLOX_GPS_SendCommand_WaitACK_CK_B_1_174
      006101 F0               [24] 2826 	movx	@dptr,a
      006102 74 03            [12] 2827 	mov	a,#(_UBLOX_GPS_SendCommand_WaitACK_CK_B_1_174 >> 8)
      006104 A3               [24] 2828 	inc	dptr
      006105 F0               [24] 2829 	movx	@dptr,a
      006106 E4               [12] 2830 	clr	a
      006107 A3               [24] 2831 	inc	dptr
      006108 F0               [24] 2832 	movx	@dptr,a
      006109 90 03 B8         [24] 2833 	mov	dptr,#_UBLOX_GPS_FletcherChecksum8_PARM_4
      00610C EC               [12] 2834 	mov	a,r4
      00610D F0               [24] 2835 	movx	@dptr,a
      00610E EF               [12] 2836 	mov	a,r7
      00610F A3               [24] 2837 	inc	dptr
      006110 F0               [24] 2838 	movx	@dptr,a
      006111 85 36 82         [24] 2839 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0
      006114 85 37 83         [24] 2840 	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0 + 1)
      006117 85 38 F0         [24] 2841 	mov	b,(_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0 + 2)
      00611A C0 06            [24] 2842 	push	ar6
      00611C C0 05            [24] 2843 	push	ar5
      00611E C0 03            [24] 2844 	push	ar3
      006120 C0 02            [24] 2845 	push	ar2
      006122 C0 01            [24] 2846 	push	ar1
      006124 12 5E 1E         [24] 2847 	lcall	_UBLOX_GPS_FletcherChecksum8
      006127 D0 01            [24] 2848 	pop	ar1
      006129 D0 02            [24] 2849 	pop	ar2
      00612B D0 03            [24] 2850 	pop	ar3
      00612D D0 05            [24] 2851 	pop	ar5
      00612F D0 06            [24] 2852 	pop	ar6
                                   2853 ;	..\src\COMMON\UBLOX.c:193: if(CK_A == rx_buffer[k-2] && CK_B == rx_buffer[k-1]) // verifico el checksum calculado
      006131 E5 31            [12] 2854 	mov	a,_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0
      006133 24 FE            [12] 2855 	add	a,#0xfe
      006135 FC               [12] 2856 	mov	r4,a
      006136 E5 32            [12] 2857 	mov	a,(_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0 + 1)
      006138 34 FF            [12] 2858 	addc	a,#0xff
      00613A FF               [12] 2859 	mov	r7,a
      00613B EC               [12] 2860 	mov	a,r4
      00613C 2A               [12] 2861 	add	a,r2
      00613D F5 82            [12] 2862 	mov	dpl,a
      00613F EF               [12] 2863 	mov	a,r7
      006140 3B               [12] 2864 	addc	a,r3
      006141 F5 83            [12] 2865 	mov	dph,a
      006143 E0               [24] 2866 	movx	a,@dptr
      006144 FF               [12] 2867 	mov	r7,a
      006145 90 03 C9         [24] 2868 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_CK_A_1_174
      006148 E0               [24] 2869 	movx	a,@dptr
      006149 FC               [12] 2870 	mov	r4,a
      00614A B5 07 02         [24] 2871 	cjne	a,ar7,00195$
      00614D 80 05            [24] 2872 	sjmp	00196$
      00614F                       2873 00195$:
      00614F D0 01            [24] 2874 	pop	ar1
      006151 02 62 5D         [24] 2875 	ljmp	00123$
      006154                       2876 00196$:
      006154 D0 01            [24] 2877 	pop	ar1
      006156 E5 31            [12] 2878 	mov	a,_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0
      006158 24 FF            [12] 2879 	add	a,#0xff
      00615A FC               [12] 2880 	mov	r4,a
      00615B E5 32            [12] 2881 	mov	a,(_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0 + 1)
      00615D 34 FF            [12] 2882 	addc	a,#0xff
      00615F FF               [12] 2883 	mov	r7,a
      006160 EC               [12] 2884 	mov	a,r4
      006161 2A               [12] 2885 	add	a,r2
      006162 F5 82            [12] 2886 	mov	dpl,a
      006164 EF               [12] 2887 	mov	a,r7
      006165 3B               [12] 2888 	addc	a,r3
      006166 F5 83            [12] 2889 	mov	dph,a
      006168 E0               [24] 2890 	movx	a,@dptr
      006169 FF               [12] 2891 	mov	r7,a
      00616A 90 03 CA         [24] 2892 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_CK_B_1_174
      00616D E0               [24] 2893 	movx	a,@dptr
      00616E FC               [12] 2894 	mov	r4,a
      00616F B5 07 02         [24] 2895 	cjne	a,ar7,00197$
      006172 80 03            [24] 2896 	sjmp	00198$
      006174                       2897 00197$:
      006174 02 62 5D         [24] 2898 	ljmp	00123$
      006177                       2899 00198$:
                                   2900 ;	..\src\COMMON\UBLOX.c:196: if(!AnsOrAck)
      006177 90 03 C3         [24] 2901 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_5
      00617A E0               [24] 2902 	movx	a,@dptr
      00617B 60 03            [24] 2903 	jz	00199$
      00617D 02 62 3B         [24] 2904 	ljmp	00117$
      006180                       2905 00199$:
                                   2906 ;	..\src\COMMON\UBLOX.c:199: if(msg_class == rx_buffer[UBX_MESSAGE_CLASS] && msg_id == rx_buffer[UBX_MESSAGE_ID])
      006180 8D 82            [24] 2907 	mov	dpl,r5
      006182 8E 83            [24] 2908 	mov	dph,r6
      006184 E0               [24] 2909 	movx	a,@dptr
      006185 FF               [12] 2910 	mov	r7,a
      006186 E9               [12] 2911 	mov	a,r1
      006187 B5 07 02         [24] 2912 	cjne	a,ar7,00200$
      00618A 80 03            [24] 2913 	sjmp	00201$
      00618C                       2914 00200$:
      00618C 02 62 5D         [24] 2915 	ljmp	00123$
      00618F                       2916 00201$:
      00618F 8A 82            [24] 2917 	mov	dpl,r2
      006191 8B 83            [24] 2918 	mov	dph,r3
      006193 A3               [24] 2919 	inc	dptr
      006194 A3               [24] 2920 	inc	dptr
      006195 A3               [24] 2921 	inc	dptr
      006196 E0               [24] 2922 	movx	a,@dptr
      006197 FF               [12] 2923 	mov	r7,a
      006198 B5 2D 02         [24] 2924 	cjne	a,_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0,00202$
      00619B 80 03            [24] 2925 	sjmp	00203$
      00619D                       2926 00202$:
      00619D 02 62 5D         [24] 2927 	ljmp	00123$
      0061A0                       2928 00203$:
                                   2929 ;	..\src\COMMON\UBLOX.c:201: *RxLength = rx_buffer[UBX_MESSAGE_LENGTH_MSB]<<8 | rx_buffer[UBX_MESSAGE_LENGTH_LSB];
      0061A0 90 03 C4         [24] 2930 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_PARM_6
      0061A3 E0               [24] 2931 	movx	a,@dptr
      0061A4 F5 36            [12] 2932 	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0,a
      0061A6 A3               [24] 2933 	inc	dptr
      0061A7 E0               [24] 2934 	movx	a,@dptr
      0061A8 F5 37            [12] 2935 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0 + 1),a
      0061AA A3               [24] 2936 	inc	dptr
      0061AB E0               [24] 2937 	movx	a,@dptr
      0061AC F5 38            [12] 2938 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0 + 2),a
      0061AE 8A 82            [24] 2939 	mov	dpl,r2
      0061B0 8B 83            [24] 2940 	mov	dph,r3
      0061B2 A3               [24] 2941 	inc	dptr
      0061B3 A3               [24] 2942 	inc	dptr
      0061B4 A3               [24] 2943 	inc	dptr
      0061B5 A3               [24] 2944 	inc	dptr
      0061B6 A3               [24] 2945 	inc	dptr
      0061B7 E0               [24] 2946 	movx	a,@dptr
      0061B8 FF               [12] 2947 	mov	r7,a
      0061B9 78 00            [12] 2948 	mov	r0,#0x00
      0061BB 8A 82            [24] 2949 	mov	dpl,r2
      0061BD 8B 83            [24] 2950 	mov	dph,r3
      0061BF A3               [24] 2951 	inc	dptr
      0061C0 A3               [24] 2952 	inc	dptr
      0061C1 A3               [24] 2953 	inc	dptr
      0061C2 A3               [24] 2954 	inc	dptr
      0061C3 E0               [24] 2955 	movx	a,@dptr
      0061C4 7C 00            [12] 2956 	mov	r4,#0x00
      0061C6 42 00            [12] 2957 	orl	ar0,a
      0061C8 EC               [12] 2958 	mov	a,r4
      0061C9 42 07            [12] 2959 	orl	ar7,a
      0061CB 85 36 82         [24] 2960 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0
      0061CE 85 37 83         [24] 2961 	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0 + 1)
      0061D1 85 38 F0         [24] 2962 	mov	b,(_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0 + 2)
      0061D4 E8               [12] 2963 	mov	a,r0
      0061D5 12 87 88         [24] 2964 	lcall	__gptrput
      0061D8 A3               [24] 2965 	inc	dptr
      0061D9 EF               [12] 2966 	mov	a,r7
      0061DA 12 87 88         [24] 2967 	lcall	__gptrput
                                   2968 ;	..\src\COMMON\UBLOX.c:202: memcpy(payload,&rx_buffer[UBX_MESSAGE_PAYLOAD],*RxLength);
      0061DD A9 2E            [24] 2969 	mov	r1,_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0
      0061DF AC 2F            [24] 2970 	mov	r4,(_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0 + 1)
      0061E1 AF 30            [24] 2971 	mov	r7,(_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0 + 2)
      0061E3 89 31            [24] 2972 	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0,r1
      0061E5 8C 32            [24] 2973 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0 + 1),r4
      0061E7 8F 33            [24] 2974 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0 + 2),r7
      0061E9 74 06            [12] 2975 	mov	a,#0x06
      0061EB 2A               [12] 2976 	add	a,r2
      0061EC F8               [12] 2977 	mov	r0,a
      0061ED E4               [12] 2978 	clr	a
      0061EE 3B               [12] 2979 	addc	a,r3
      0061EF FF               [12] 2980 	mov	r7,a
      0061F0 7C 00            [12] 2981 	mov	r4,#0x00
      0061F2 C0 02            [24] 2982 	push	ar2
      0061F4 C0 03            [24] 2983 	push	ar3
      0061F6 85 36 82         [24] 2984 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0
      0061F9 85 37 83         [24] 2985 	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0 + 1)
      0061FC 85 38 F0         [24] 2986 	mov	b,(_UBLOX_GPS_SendCommand_WaitACK_sloc4_1_0 + 2)
      0061FF 12 96 F0         [24] 2987 	lcall	__gptrget
      006202 F9               [12] 2988 	mov	r1,a
      006203 A3               [24] 2989 	inc	dptr
      006204 12 96 F0         [24] 2990 	lcall	__gptrget
      006207 FB               [12] 2991 	mov	r3,a
      006208 90 04 3D         [24] 2992 	mov	dptr,#_memcpy_PARM_2
      00620B E8               [12] 2993 	mov	a,r0
      00620C F0               [24] 2994 	movx	@dptr,a
      00620D EF               [12] 2995 	mov	a,r7
      00620E A3               [24] 2996 	inc	dptr
      00620F F0               [24] 2997 	movx	@dptr,a
      006210 EC               [12] 2998 	mov	a,r4
      006211 A3               [24] 2999 	inc	dptr
      006212 F0               [24] 3000 	movx	@dptr,a
      006213 90 04 40         [24] 3001 	mov	dptr,#_memcpy_PARM_3
      006216 E9               [12] 3002 	mov	a,r1
      006217 F0               [24] 3003 	movx	@dptr,a
      006218 EB               [12] 3004 	mov	a,r3
      006219 A3               [24] 3005 	inc	dptr
      00621A F0               [24] 3006 	movx	@dptr,a
      00621B 85 31 82         [24] 3007 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0
      00621E 85 32 83         [24] 3008 	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0 + 1)
      006221 85 33 F0         [24] 3009 	mov	b,(_UBLOX_GPS_SendCommand_WaitACK_sloc2_1_0 + 2)
      006224 C0 03            [24] 3010 	push	ar3
      006226 C0 02            [24] 3011 	push	ar2
      006228 12 83 01         [24] 3012 	lcall	_memcpy
      00622B D0 02            [24] 3013 	pop	ar2
      00622D D0 03            [24] 3014 	pop	ar3
                                   3015 ;	..\src\COMMON\UBLOX.c:203: RetVal = OK;
      00622F 90 03 C8         [24] 3016 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_RetVal_1_174
      006232 74 01            [12] 3017 	mov	a,#0x01
      006234 F0               [24] 3018 	movx	@dptr,a
      006235 D0 03            [24] 3019 	pop	ar3
      006237 D0 02            [24] 3020 	pop	ar2
      006239 80 22            [24] 3021 	sjmp	00123$
      00623B                       3022 00117$:
                                   3023 ;	..\src\COMMON\UBLOX.c:209: if(rx_buffer[UBX_MESSAGE_CLASS] == eACK && rx_buffer[UBX_MESSAGE_ID] == ACK) RetVal = ACK;
      00623B 8D 82            [24] 3024 	mov	dpl,r5
      00623D 8E 83            [24] 3025 	mov	dph,r6
      00623F E0               [24] 3026 	movx	a,@dptr
      006240 FD               [12] 3027 	mov	r5,a
      006241 BD 05 14         [24] 3028 	cjne	r5,#0x05,00113$
      006244 8A 82            [24] 3029 	mov	dpl,r2
      006246 8B 83            [24] 3030 	mov	dph,r3
      006248 A3               [24] 3031 	inc	dptr
      006249 A3               [24] 3032 	inc	dptr
      00624A A3               [24] 3033 	inc	dptr
      00624B E0               [24] 3034 	movx	a,@dptr
      00624C FF               [12] 3035 	mov	r7,a
      00624D BF 01 08         [24] 3036 	cjne	r7,#0x01,00113$
      006250 90 03 C8         [24] 3037 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_RetVal_1_174
      006253 74 01            [12] 3038 	mov	a,#0x01
      006255 F0               [24] 3039 	movx	@dptr,a
      006256 80 05            [24] 3040 	sjmp	00123$
      006258                       3041 00113$:
                                   3042 ;	..\src\COMMON\UBLOX.c:210: else RetVal = NAK;
      006258 90 03 C8         [24] 3043 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_RetVal_1_174
      00625B E4               [12] 3044 	clr	a
      00625C F0               [24] 3045 	movx	@dptr,a
      00625D                       3046 00123$:
                                   3047 ;	..\src\COMMON\UBLOX.c:214: free(buffer_aux);
      00625D AE 34            [24] 3048 	mov	r6,_UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0
      00625F AF 35            [24] 3049 	mov	r7,(_UBLOX_GPS_SendCommand_WaitACK_sloc3_1_0 + 1)
      006261 7D 00            [12] 3050 	mov	r5,#0x00
      006263 8E 82            [24] 3051 	mov	dpl,r6
      006265 8F 83            [24] 3052 	mov	dph,r7
      006267 8D F0            [24] 3053 	mov	b,r5
      006269 C0 03            [24] 3054 	push	ar3
      00626B C0 02            [24] 3055 	push	ar2
      00626D 12 78 B7         [24] 3056 	lcall	_free
      006270 D0 02            [24] 3057 	pop	ar2
      006272 D0 03            [24] 3058 	pop	ar3
                                   3059 ;	..\src\COMMON\UBLOX.c:215: free(rx_buffer);
      006274 7F 00            [12] 3060 	mov	r7,#0x00
      006276 8A 82            [24] 3061 	mov	dpl,r2
      006278 8B 83            [24] 3062 	mov	dph,r3
      00627A 8F F0            [24] 3063 	mov	b,r7
      00627C 12 78 B7         [24] 3064 	lcall	_free
                                   3065 ;	..\src\COMMON\UBLOX.c:216: return RetVal;
      00627F 90 03 C8         [24] 3066 	mov	dptr,#_UBLOX_GPS_SendCommand_WaitACK_RetVal_1_174
      006282 E0               [24] 3067 	movx	a,@dptr
      006283 F5 82            [12] 3068 	mov	dpl,a
      006285 22               [24] 3069 	ret
                                   3070 	.area CSEG    (CODE)
                                   3071 	.area CONST   (CODE)
                                   3072 	.area XINIT   (CODE)
                                   3073 	.area CABS    (ABS,CODE)
