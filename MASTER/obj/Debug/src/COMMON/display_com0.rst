                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.9.3 #11354 (MINGW32)
                                      4 ;--------------------------------------------------------
                                      5 	.module display_com0
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _com0_newline
                                     12 	.globl _com0_writestr2
                                     13 	.globl _uart0_writestr
                                     14 	.globl _uart0_init
                                     15 	.globl _uart_timer0_baud
                                     16 	.globl _PORTC_7
                                     17 	.globl _PORTC_6
                                     18 	.globl _PORTC_5
                                     19 	.globl _PORTC_4
                                     20 	.globl _PORTC_3
                                     21 	.globl _PORTC_2
                                     22 	.globl _PORTC_1
                                     23 	.globl _PORTC_0
                                     24 	.globl _PORTB_7
                                     25 	.globl _PORTB_6
                                     26 	.globl _PORTB_5
                                     27 	.globl _PORTB_4
                                     28 	.globl _PORTB_3
                                     29 	.globl _PORTB_2
                                     30 	.globl _PORTB_1
                                     31 	.globl _PORTB_0
                                     32 	.globl _PORTA_7
                                     33 	.globl _PORTA_6
                                     34 	.globl _PORTA_5
                                     35 	.globl _PORTA_4
                                     36 	.globl _PORTA_3
                                     37 	.globl _PORTA_2
                                     38 	.globl _PORTA_1
                                     39 	.globl _PORTA_0
                                     40 	.globl _PINC_7
                                     41 	.globl _PINC_6
                                     42 	.globl _PINC_5
                                     43 	.globl _PINC_4
                                     44 	.globl _PINC_3
                                     45 	.globl _PINC_2
                                     46 	.globl _PINC_1
                                     47 	.globl _PINC_0
                                     48 	.globl _PINB_7
                                     49 	.globl _PINB_6
                                     50 	.globl _PINB_5
                                     51 	.globl _PINB_4
                                     52 	.globl _PINB_3
                                     53 	.globl _PINB_2
                                     54 	.globl _PINB_1
                                     55 	.globl _PINB_0
                                     56 	.globl _PINA_7
                                     57 	.globl _PINA_6
                                     58 	.globl _PINA_5
                                     59 	.globl _PINA_4
                                     60 	.globl _PINA_3
                                     61 	.globl _PINA_2
                                     62 	.globl _PINA_1
                                     63 	.globl _PINA_0
                                     64 	.globl _CY
                                     65 	.globl _AC
                                     66 	.globl _F0
                                     67 	.globl _RS1
                                     68 	.globl _RS0
                                     69 	.globl _OV
                                     70 	.globl _F1
                                     71 	.globl _P
                                     72 	.globl _IP_7
                                     73 	.globl _IP_6
                                     74 	.globl _IP_5
                                     75 	.globl _IP_4
                                     76 	.globl _IP_3
                                     77 	.globl _IP_2
                                     78 	.globl _IP_1
                                     79 	.globl _IP_0
                                     80 	.globl _EA
                                     81 	.globl _IE_7
                                     82 	.globl _IE_6
                                     83 	.globl _IE_5
                                     84 	.globl _IE_4
                                     85 	.globl _IE_3
                                     86 	.globl _IE_2
                                     87 	.globl _IE_1
                                     88 	.globl _IE_0
                                     89 	.globl _EIP_7
                                     90 	.globl _EIP_6
                                     91 	.globl _EIP_5
                                     92 	.globl _EIP_4
                                     93 	.globl _EIP_3
                                     94 	.globl _EIP_2
                                     95 	.globl _EIP_1
                                     96 	.globl _EIP_0
                                     97 	.globl _EIE_7
                                     98 	.globl _EIE_6
                                     99 	.globl _EIE_5
                                    100 	.globl _EIE_4
                                    101 	.globl _EIE_3
                                    102 	.globl _EIE_2
                                    103 	.globl _EIE_1
                                    104 	.globl _EIE_0
                                    105 	.globl _E2IP_7
                                    106 	.globl _E2IP_6
                                    107 	.globl _E2IP_5
                                    108 	.globl _E2IP_4
                                    109 	.globl _E2IP_3
                                    110 	.globl _E2IP_2
                                    111 	.globl _E2IP_1
                                    112 	.globl _E2IP_0
                                    113 	.globl _E2IE_7
                                    114 	.globl _E2IE_6
                                    115 	.globl _E2IE_5
                                    116 	.globl _E2IE_4
                                    117 	.globl _E2IE_3
                                    118 	.globl _E2IE_2
                                    119 	.globl _E2IE_1
                                    120 	.globl _E2IE_0
                                    121 	.globl _B_7
                                    122 	.globl _B_6
                                    123 	.globl _B_5
                                    124 	.globl _B_4
                                    125 	.globl _B_3
                                    126 	.globl _B_2
                                    127 	.globl _B_1
                                    128 	.globl _B_0
                                    129 	.globl _ACC_7
                                    130 	.globl _ACC_6
                                    131 	.globl _ACC_5
                                    132 	.globl _ACC_4
                                    133 	.globl _ACC_3
                                    134 	.globl _ACC_2
                                    135 	.globl _ACC_1
                                    136 	.globl _ACC_0
                                    137 	.globl _WTSTAT
                                    138 	.globl _WTIRQEN
                                    139 	.globl _WTEVTD
                                    140 	.globl _WTEVTD1
                                    141 	.globl _WTEVTD0
                                    142 	.globl _WTEVTC
                                    143 	.globl _WTEVTC1
                                    144 	.globl _WTEVTC0
                                    145 	.globl _WTEVTB
                                    146 	.globl _WTEVTB1
                                    147 	.globl _WTEVTB0
                                    148 	.globl _WTEVTA
                                    149 	.globl _WTEVTA1
                                    150 	.globl _WTEVTA0
                                    151 	.globl _WTCNTR1
                                    152 	.globl _WTCNTB
                                    153 	.globl _WTCNTB1
                                    154 	.globl _WTCNTB0
                                    155 	.globl _WTCNTA
                                    156 	.globl _WTCNTA1
                                    157 	.globl _WTCNTA0
                                    158 	.globl _WTCFGB
                                    159 	.globl _WTCFGA
                                    160 	.globl _WDTRESET
                                    161 	.globl _WDTCFG
                                    162 	.globl _U1STATUS
                                    163 	.globl _U1SHREG
                                    164 	.globl _U1MODE
                                    165 	.globl _U1CTRL
                                    166 	.globl _U0STATUS
                                    167 	.globl _U0SHREG
                                    168 	.globl _U0MODE
                                    169 	.globl _U0CTRL
                                    170 	.globl _T2STATUS
                                    171 	.globl _T2PERIOD
                                    172 	.globl _T2PERIOD1
                                    173 	.globl _T2PERIOD0
                                    174 	.globl _T2MODE
                                    175 	.globl _T2CNT
                                    176 	.globl _T2CNT1
                                    177 	.globl _T2CNT0
                                    178 	.globl _T2CLKSRC
                                    179 	.globl _T1STATUS
                                    180 	.globl _T1PERIOD
                                    181 	.globl _T1PERIOD1
                                    182 	.globl _T1PERIOD0
                                    183 	.globl _T1MODE
                                    184 	.globl _T1CNT
                                    185 	.globl _T1CNT1
                                    186 	.globl _T1CNT0
                                    187 	.globl _T1CLKSRC
                                    188 	.globl _T0STATUS
                                    189 	.globl _T0PERIOD
                                    190 	.globl _T0PERIOD1
                                    191 	.globl _T0PERIOD0
                                    192 	.globl _T0MODE
                                    193 	.globl _T0CNT
                                    194 	.globl _T0CNT1
                                    195 	.globl _T0CNT0
                                    196 	.globl _T0CLKSRC
                                    197 	.globl _SPSTATUS
                                    198 	.globl _SPSHREG
                                    199 	.globl _SPMODE
                                    200 	.globl _SPCLKSRC
                                    201 	.globl _RADIOSTAT
                                    202 	.globl _RADIOSTAT1
                                    203 	.globl _RADIOSTAT0
                                    204 	.globl _RADIODATA
                                    205 	.globl _RADIODATA3
                                    206 	.globl _RADIODATA2
                                    207 	.globl _RADIODATA1
                                    208 	.globl _RADIODATA0
                                    209 	.globl _RADIOADDR
                                    210 	.globl _RADIOADDR1
                                    211 	.globl _RADIOADDR0
                                    212 	.globl _RADIOACC
                                    213 	.globl _OC1STATUS
                                    214 	.globl _OC1PIN
                                    215 	.globl _OC1MODE
                                    216 	.globl _OC1COMP
                                    217 	.globl _OC1COMP1
                                    218 	.globl _OC1COMP0
                                    219 	.globl _OC0STATUS
                                    220 	.globl _OC0PIN
                                    221 	.globl _OC0MODE
                                    222 	.globl _OC0COMP
                                    223 	.globl _OC0COMP1
                                    224 	.globl _OC0COMP0
                                    225 	.globl _NVSTATUS
                                    226 	.globl _NVKEY
                                    227 	.globl _NVDATA
                                    228 	.globl _NVDATA1
                                    229 	.globl _NVDATA0
                                    230 	.globl _NVADDR
                                    231 	.globl _NVADDR1
                                    232 	.globl _NVADDR0
                                    233 	.globl _IC1STATUS
                                    234 	.globl _IC1MODE
                                    235 	.globl _IC1CAPT
                                    236 	.globl _IC1CAPT1
                                    237 	.globl _IC1CAPT0
                                    238 	.globl _IC0STATUS
                                    239 	.globl _IC0MODE
                                    240 	.globl _IC0CAPT
                                    241 	.globl _IC0CAPT1
                                    242 	.globl _IC0CAPT0
                                    243 	.globl _PORTR
                                    244 	.globl _PORTC
                                    245 	.globl _PORTB
                                    246 	.globl _PORTA
                                    247 	.globl _PINR
                                    248 	.globl _PINC
                                    249 	.globl _PINB
                                    250 	.globl _PINA
                                    251 	.globl _DIRR
                                    252 	.globl _DIRC
                                    253 	.globl _DIRB
                                    254 	.globl _DIRA
                                    255 	.globl _DBGLNKSTAT
                                    256 	.globl _DBGLNKBUF
                                    257 	.globl _CODECONFIG
                                    258 	.globl _CLKSTAT
                                    259 	.globl _CLKCON
                                    260 	.globl _ANALOGCOMP
                                    261 	.globl _ADCCONV
                                    262 	.globl _ADCCLKSRC
                                    263 	.globl _ADCCH3CONFIG
                                    264 	.globl _ADCCH2CONFIG
                                    265 	.globl _ADCCH1CONFIG
                                    266 	.globl _ADCCH0CONFIG
                                    267 	.globl __XPAGE
                                    268 	.globl _XPAGE
                                    269 	.globl _SP
                                    270 	.globl _PSW
                                    271 	.globl _PCON
                                    272 	.globl _IP
                                    273 	.globl _IE
                                    274 	.globl _EIP
                                    275 	.globl _EIE
                                    276 	.globl _E2IP
                                    277 	.globl _E2IE
                                    278 	.globl _DPS
                                    279 	.globl _DPTR1
                                    280 	.globl _DPTR0
                                    281 	.globl _DPL1
                                    282 	.globl _DPL
                                    283 	.globl _DPH1
                                    284 	.globl _DPH
                                    285 	.globl _B
                                    286 	.globl _ACC
                                    287 	.globl _column
                                    288 	.globl _row
                                    289 	.globl _AX5043_TIMEGAIN3NB
                                    290 	.globl _AX5043_TIMEGAIN2NB
                                    291 	.globl _AX5043_TIMEGAIN1NB
                                    292 	.globl _AX5043_TIMEGAIN0NB
                                    293 	.globl _AX5043_RXPARAMSETSNB
                                    294 	.globl _AX5043_RXPARAMCURSETNB
                                    295 	.globl _AX5043_PKTMAXLENNB
                                    296 	.globl _AX5043_PKTLENOFFSETNB
                                    297 	.globl _AX5043_PKTLENCFGNB
                                    298 	.globl _AX5043_PKTADDRMASK3NB
                                    299 	.globl _AX5043_PKTADDRMASK2NB
                                    300 	.globl _AX5043_PKTADDRMASK1NB
                                    301 	.globl _AX5043_PKTADDRMASK0NB
                                    302 	.globl _AX5043_PKTADDRCFGNB
                                    303 	.globl _AX5043_PKTADDR3NB
                                    304 	.globl _AX5043_PKTADDR2NB
                                    305 	.globl _AX5043_PKTADDR1NB
                                    306 	.globl _AX5043_PKTADDR0NB
                                    307 	.globl _AX5043_PHASEGAIN3NB
                                    308 	.globl _AX5043_PHASEGAIN2NB
                                    309 	.globl _AX5043_PHASEGAIN1NB
                                    310 	.globl _AX5043_PHASEGAIN0NB
                                    311 	.globl _AX5043_FREQUENCYLEAKNB
                                    312 	.globl _AX5043_FREQUENCYGAIND3NB
                                    313 	.globl _AX5043_FREQUENCYGAIND2NB
                                    314 	.globl _AX5043_FREQUENCYGAIND1NB
                                    315 	.globl _AX5043_FREQUENCYGAIND0NB
                                    316 	.globl _AX5043_FREQUENCYGAINC3NB
                                    317 	.globl _AX5043_FREQUENCYGAINC2NB
                                    318 	.globl _AX5043_FREQUENCYGAINC1NB
                                    319 	.globl _AX5043_FREQUENCYGAINC0NB
                                    320 	.globl _AX5043_FREQUENCYGAINB3NB
                                    321 	.globl _AX5043_FREQUENCYGAINB2NB
                                    322 	.globl _AX5043_FREQUENCYGAINB1NB
                                    323 	.globl _AX5043_FREQUENCYGAINB0NB
                                    324 	.globl _AX5043_FREQUENCYGAINA3NB
                                    325 	.globl _AX5043_FREQUENCYGAINA2NB
                                    326 	.globl _AX5043_FREQUENCYGAINA1NB
                                    327 	.globl _AX5043_FREQUENCYGAINA0NB
                                    328 	.globl _AX5043_FREQDEV13NB
                                    329 	.globl _AX5043_FREQDEV12NB
                                    330 	.globl _AX5043_FREQDEV11NB
                                    331 	.globl _AX5043_FREQDEV10NB
                                    332 	.globl _AX5043_FREQDEV03NB
                                    333 	.globl _AX5043_FREQDEV02NB
                                    334 	.globl _AX5043_FREQDEV01NB
                                    335 	.globl _AX5043_FREQDEV00NB
                                    336 	.globl _AX5043_FOURFSK3NB
                                    337 	.globl _AX5043_FOURFSK2NB
                                    338 	.globl _AX5043_FOURFSK1NB
                                    339 	.globl _AX5043_FOURFSK0NB
                                    340 	.globl _AX5043_DRGAIN3NB
                                    341 	.globl _AX5043_DRGAIN2NB
                                    342 	.globl _AX5043_DRGAIN1NB
                                    343 	.globl _AX5043_DRGAIN0NB
                                    344 	.globl _AX5043_BBOFFSRES3NB
                                    345 	.globl _AX5043_BBOFFSRES2NB
                                    346 	.globl _AX5043_BBOFFSRES1NB
                                    347 	.globl _AX5043_BBOFFSRES0NB
                                    348 	.globl _AX5043_AMPLITUDEGAIN3NB
                                    349 	.globl _AX5043_AMPLITUDEGAIN2NB
                                    350 	.globl _AX5043_AMPLITUDEGAIN1NB
                                    351 	.globl _AX5043_AMPLITUDEGAIN0NB
                                    352 	.globl _AX5043_AGCTARGET3NB
                                    353 	.globl _AX5043_AGCTARGET2NB
                                    354 	.globl _AX5043_AGCTARGET1NB
                                    355 	.globl _AX5043_AGCTARGET0NB
                                    356 	.globl _AX5043_AGCMINMAX3NB
                                    357 	.globl _AX5043_AGCMINMAX2NB
                                    358 	.globl _AX5043_AGCMINMAX1NB
                                    359 	.globl _AX5043_AGCMINMAX0NB
                                    360 	.globl _AX5043_AGCGAIN3NB
                                    361 	.globl _AX5043_AGCGAIN2NB
                                    362 	.globl _AX5043_AGCGAIN1NB
                                    363 	.globl _AX5043_AGCGAIN0NB
                                    364 	.globl _AX5043_AGCAHYST3NB
                                    365 	.globl _AX5043_AGCAHYST2NB
                                    366 	.globl _AX5043_AGCAHYST1NB
                                    367 	.globl _AX5043_AGCAHYST0NB
                                    368 	.globl _AX5043_0xF44NB
                                    369 	.globl _AX5043_0xF35NB
                                    370 	.globl _AX5043_0xF34NB
                                    371 	.globl _AX5043_0xF33NB
                                    372 	.globl _AX5043_0xF32NB
                                    373 	.globl _AX5043_0xF31NB
                                    374 	.globl _AX5043_0xF30NB
                                    375 	.globl _AX5043_0xF26NB
                                    376 	.globl _AX5043_0xF23NB
                                    377 	.globl _AX5043_0xF22NB
                                    378 	.globl _AX5043_0xF21NB
                                    379 	.globl _AX5043_0xF1CNB
                                    380 	.globl _AX5043_0xF18NB
                                    381 	.globl _AX5043_0xF0CNB
                                    382 	.globl _AX5043_0xF00NB
                                    383 	.globl _AX5043_XTALSTATUSNB
                                    384 	.globl _AX5043_XTALOSCNB
                                    385 	.globl _AX5043_XTALCAPNB
                                    386 	.globl _AX5043_XTALAMPLNB
                                    387 	.globl _AX5043_WAKEUPXOEARLYNB
                                    388 	.globl _AX5043_WAKEUPTIMER1NB
                                    389 	.globl _AX5043_WAKEUPTIMER0NB
                                    390 	.globl _AX5043_WAKEUPFREQ1NB
                                    391 	.globl _AX5043_WAKEUPFREQ0NB
                                    392 	.globl _AX5043_WAKEUP1NB
                                    393 	.globl _AX5043_WAKEUP0NB
                                    394 	.globl _AX5043_TXRATE2NB
                                    395 	.globl _AX5043_TXRATE1NB
                                    396 	.globl _AX5043_TXRATE0NB
                                    397 	.globl _AX5043_TXPWRCOEFFE1NB
                                    398 	.globl _AX5043_TXPWRCOEFFE0NB
                                    399 	.globl _AX5043_TXPWRCOEFFD1NB
                                    400 	.globl _AX5043_TXPWRCOEFFD0NB
                                    401 	.globl _AX5043_TXPWRCOEFFC1NB
                                    402 	.globl _AX5043_TXPWRCOEFFC0NB
                                    403 	.globl _AX5043_TXPWRCOEFFB1NB
                                    404 	.globl _AX5043_TXPWRCOEFFB0NB
                                    405 	.globl _AX5043_TXPWRCOEFFA1NB
                                    406 	.globl _AX5043_TXPWRCOEFFA0NB
                                    407 	.globl _AX5043_TRKRFFREQ2NB
                                    408 	.globl _AX5043_TRKRFFREQ1NB
                                    409 	.globl _AX5043_TRKRFFREQ0NB
                                    410 	.globl _AX5043_TRKPHASE1NB
                                    411 	.globl _AX5043_TRKPHASE0NB
                                    412 	.globl _AX5043_TRKFSKDEMOD1NB
                                    413 	.globl _AX5043_TRKFSKDEMOD0NB
                                    414 	.globl _AX5043_TRKFREQ1NB
                                    415 	.globl _AX5043_TRKFREQ0NB
                                    416 	.globl _AX5043_TRKDATARATE2NB
                                    417 	.globl _AX5043_TRKDATARATE1NB
                                    418 	.globl _AX5043_TRKDATARATE0NB
                                    419 	.globl _AX5043_TRKAMPLITUDE1NB
                                    420 	.globl _AX5043_TRKAMPLITUDE0NB
                                    421 	.globl _AX5043_TRKAFSKDEMOD1NB
                                    422 	.globl _AX5043_TRKAFSKDEMOD0NB
                                    423 	.globl _AX5043_TMGTXSETTLENB
                                    424 	.globl _AX5043_TMGTXBOOSTNB
                                    425 	.globl _AX5043_TMGRXSETTLENB
                                    426 	.globl _AX5043_TMGRXRSSINB
                                    427 	.globl _AX5043_TMGRXPREAMBLE3NB
                                    428 	.globl _AX5043_TMGRXPREAMBLE2NB
                                    429 	.globl _AX5043_TMGRXPREAMBLE1NB
                                    430 	.globl _AX5043_TMGRXOFFSACQNB
                                    431 	.globl _AX5043_TMGRXCOARSEAGCNB
                                    432 	.globl _AX5043_TMGRXBOOSTNB
                                    433 	.globl _AX5043_TMGRXAGCNB
                                    434 	.globl _AX5043_TIMER2NB
                                    435 	.globl _AX5043_TIMER1NB
                                    436 	.globl _AX5043_TIMER0NB
                                    437 	.globl _AX5043_SILICONREVISIONNB
                                    438 	.globl _AX5043_SCRATCHNB
                                    439 	.globl _AX5043_RXDATARATE2NB
                                    440 	.globl _AX5043_RXDATARATE1NB
                                    441 	.globl _AX5043_RXDATARATE0NB
                                    442 	.globl _AX5043_RSSIREFERENCENB
                                    443 	.globl _AX5043_RSSIABSTHRNB
                                    444 	.globl _AX5043_RSSINB
                                    445 	.globl _AX5043_REFNB
                                    446 	.globl _AX5043_RADIOSTATENB
                                    447 	.globl _AX5043_RADIOEVENTREQ1NB
                                    448 	.globl _AX5043_RADIOEVENTREQ0NB
                                    449 	.globl _AX5043_RADIOEVENTMASK1NB
                                    450 	.globl _AX5043_RADIOEVENTMASK0NB
                                    451 	.globl _AX5043_PWRMODENB
                                    452 	.globl _AX5043_PWRAMPNB
                                    453 	.globl _AX5043_POWSTICKYSTATNB
                                    454 	.globl _AX5043_POWSTATNB
                                    455 	.globl _AX5043_POWIRQMASKNB
                                    456 	.globl _AX5043_POWCTRL1NB
                                    457 	.globl _AX5043_PLLVCOIRNB
                                    458 	.globl _AX5043_PLLVCOINB
                                    459 	.globl _AX5043_PLLVCODIVNB
                                    460 	.globl _AX5043_PLLRNGCLKNB
                                    461 	.globl _AX5043_PLLRANGINGBNB
                                    462 	.globl _AX5043_PLLRANGINGANB
                                    463 	.globl _AX5043_PLLLOOPBOOSTNB
                                    464 	.globl _AX5043_PLLLOOPNB
                                    465 	.globl _AX5043_PLLLOCKDETNB
                                    466 	.globl _AX5043_PLLCPIBOOSTNB
                                    467 	.globl _AX5043_PLLCPINB
                                    468 	.globl _AX5043_PKTSTOREFLAGSNB
                                    469 	.globl _AX5043_PKTMISCFLAGSNB
                                    470 	.globl _AX5043_PKTCHUNKSIZENB
                                    471 	.globl _AX5043_PKTACCEPTFLAGSNB
                                    472 	.globl _AX5043_PINSTATENB
                                    473 	.globl _AX5043_PINFUNCSYSCLKNB
                                    474 	.globl _AX5043_PINFUNCPWRAMPNB
                                    475 	.globl _AX5043_PINFUNCIRQNB
                                    476 	.globl _AX5043_PINFUNCDCLKNB
                                    477 	.globl _AX5043_PINFUNCDATANB
                                    478 	.globl _AX5043_PINFUNCANTSELNB
                                    479 	.globl _AX5043_MODULATIONNB
                                    480 	.globl _AX5043_MODCFGPNB
                                    481 	.globl _AX5043_MODCFGFNB
                                    482 	.globl _AX5043_MODCFGANB
                                    483 	.globl _AX5043_MAXRFOFFSET2NB
                                    484 	.globl _AX5043_MAXRFOFFSET1NB
                                    485 	.globl _AX5043_MAXRFOFFSET0NB
                                    486 	.globl _AX5043_MAXDROFFSET2NB
                                    487 	.globl _AX5043_MAXDROFFSET1NB
                                    488 	.globl _AX5043_MAXDROFFSET0NB
                                    489 	.globl _AX5043_MATCH1PAT1NB
                                    490 	.globl _AX5043_MATCH1PAT0NB
                                    491 	.globl _AX5043_MATCH1MINNB
                                    492 	.globl _AX5043_MATCH1MAXNB
                                    493 	.globl _AX5043_MATCH1LENNB
                                    494 	.globl _AX5043_MATCH0PAT3NB
                                    495 	.globl _AX5043_MATCH0PAT2NB
                                    496 	.globl _AX5043_MATCH0PAT1NB
                                    497 	.globl _AX5043_MATCH0PAT0NB
                                    498 	.globl _AX5043_MATCH0MINNB
                                    499 	.globl _AX5043_MATCH0MAXNB
                                    500 	.globl _AX5043_MATCH0LENNB
                                    501 	.globl _AX5043_LPOSCSTATUSNB
                                    502 	.globl _AX5043_LPOSCREF1NB
                                    503 	.globl _AX5043_LPOSCREF0NB
                                    504 	.globl _AX5043_LPOSCPER1NB
                                    505 	.globl _AX5043_LPOSCPER0NB
                                    506 	.globl _AX5043_LPOSCKFILT1NB
                                    507 	.globl _AX5043_LPOSCKFILT0NB
                                    508 	.globl _AX5043_LPOSCFREQ1NB
                                    509 	.globl _AX5043_LPOSCFREQ0NB
                                    510 	.globl _AX5043_LPOSCCONFIGNB
                                    511 	.globl _AX5043_IRQREQUEST1NB
                                    512 	.globl _AX5043_IRQREQUEST0NB
                                    513 	.globl _AX5043_IRQMASK1NB
                                    514 	.globl _AX5043_IRQMASK0NB
                                    515 	.globl _AX5043_IRQINVERSION1NB
                                    516 	.globl _AX5043_IRQINVERSION0NB
                                    517 	.globl _AX5043_IFFREQ1NB
                                    518 	.globl _AX5043_IFFREQ0NB
                                    519 	.globl _AX5043_GPADCPERIODNB
                                    520 	.globl _AX5043_GPADCCTRLNB
                                    521 	.globl _AX5043_GPADC13VALUE1NB
                                    522 	.globl _AX5043_GPADC13VALUE0NB
                                    523 	.globl _AX5043_FSKDMIN1NB
                                    524 	.globl _AX5043_FSKDMIN0NB
                                    525 	.globl _AX5043_FSKDMAX1NB
                                    526 	.globl _AX5043_FSKDMAX0NB
                                    527 	.globl _AX5043_FSKDEV2NB
                                    528 	.globl _AX5043_FSKDEV1NB
                                    529 	.globl _AX5043_FSKDEV0NB
                                    530 	.globl _AX5043_FREQB3NB
                                    531 	.globl _AX5043_FREQB2NB
                                    532 	.globl _AX5043_FREQB1NB
                                    533 	.globl _AX5043_FREQB0NB
                                    534 	.globl _AX5043_FREQA3NB
                                    535 	.globl _AX5043_FREQA2NB
                                    536 	.globl _AX5043_FREQA1NB
                                    537 	.globl _AX5043_FREQA0NB
                                    538 	.globl _AX5043_FRAMINGNB
                                    539 	.globl _AX5043_FIFOTHRESH1NB
                                    540 	.globl _AX5043_FIFOTHRESH0NB
                                    541 	.globl _AX5043_FIFOSTATNB
                                    542 	.globl _AX5043_FIFOFREE1NB
                                    543 	.globl _AX5043_FIFOFREE0NB
                                    544 	.globl _AX5043_FIFODATANB
                                    545 	.globl _AX5043_FIFOCOUNT1NB
                                    546 	.globl _AX5043_FIFOCOUNT0NB
                                    547 	.globl _AX5043_FECSYNCNB
                                    548 	.globl _AX5043_FECSTATUSNB
                                    549 	.globl _AX5043_FECNB
                                    550 	.globl _AX5043_ENCODINGNB
                                    551 	.globl _AX5043_DIVERSITYNB
                                    552 	.globl _AX5043_DECIMATIONNB
                                    553 	.globl _AX5043_DACVALUE1NB
                                    554 	.globl _AX5043_DACVALUE0NB
                                    555 	.globl _AX5043_DACCONFIGNB
                                    556 	.globl _AX5043_CRCINIT3NB
                                    557 	.globl _AX5043_CRCINIT2NB
                                    558 	.globl _AX5043_CRCINIT1NB
                                    559 	.globl _AX5043_CRCINIT0NB
                                    560 	.globl _AX5043_BGNDRSSITHRNB
                                    561 	.globl _AX5043_BGNDRSSIGAINNB
                                    562 	.globl _AX5043_BGNDRSSINB
                                    563 	.globl _AX5043_BBTUNENB
                                    564 	.globl _AX5043_BBOFFSCAPNB
                                    565 	.globl _AX5043_AMPLFILTERNB
                                    566 	.globl _AX5043_AGCCOUNTERNB
                                    567 	.globl _AX5043_AFSKSPACE1NB
                                    568 	.globl _AX5043_AFSKSPACE0NB
                                    569 	.globl _AX5043_AFSKMARK1NB
                                    570 	.globl _AX5043_AFSKMARK0NB
                                    571 	.globl _AX5043_AFSKCTRLNB
                                    572 	.globl _AX5043_TIMEGAIN3
                                    573 	.globl _AX5043_TIMEGAIN2
                                    574 	.globl _AX5043_TIMEGAIN1
                                    575 	.globl _AX5043_TIMEGAIN0
                                    576 	.globl _AX5043_RXPARAMSETS
                                    577 	.globl _AX5043_RXPARAMCURSET
                                    578 	.globl _AX5043_PKTMAXLEN
                                    579 	.globl _AX5043_PKTLENOFFSET
                                    580 	.globl _AX5043_PKTLENCFG
                                    581 	.globl _AX5043_PKTADDRMASK3
                                    582 	.globl _AX5043_PKTADDRMASK2
                                    583 	.globl _AX5043_PKTADDRMASK1
                                    584 	.globl _AX5043_PKTADDRMASK0
                                    585 	.globl _AX5043_PKTADDRCFG
                                    586 	.globl _AX5043_PKTADDR3
                                    587 	.globl _AX5043_PKTADDR2
                                    588 	.globl _AX5043_PKTADDR1
                                    589 	.globl _AX5043_PKTADDR0
                                    590 	.globl _AX5043_PHASEGAIN3
                                    591 	.globl _AX5043_PHASEGAIN2
                                    592 	.globl _AX5043_PHASEGAIN1
                                    593 	.globl _AX5043_PHASEGAIN0
                                    594 	.globl _AX5043_FREQUENCYLEAK
                                    595 	.globl _AX5043_FREQUENCYGAIND3
                                    596 	.globl _AX5043_FREQUENCYGAIND2
                                    597 	.globl _AX5043_FREQUENCYGAIND1
                                    598 	.globl _AX5043_FREQUENCYGAIND0
                                    599 	.globl _AX5043_FREQUENCYGAINC3
                                    600 	.globl _AX5043_FREQUENCYGAINC2
                                    601 	.globl _AX5043_FREQUENCYGAINC1
                                    602 	.globl _AX5043_FREQUENCYGAINC0
                                    603 	.globl _AX5043_FREQUENCYGAINB3
                                    604 	.globl _AX5043_FREQUENCYGAINB2
                                    605 	.globl _AX5043_FREQUENCYGAINB1
                                    606 	.globl _AX5043_FREQUENCYGAINB0
                                    607 	.globl _AX5043_FREQUENCYGAINA3
                                    608 	.globl _AX5043_FREQUENCYGAINA2
                                    609 	.globl _AX5043_FREQUENCYGAINA1
                                    610 	.globl _AX5043_FREQUENCYGAINA0
                                    611 	.globl _AX5043_FREQDEV13
                                    612 	.globl _AX5043_FREQDEV12
                                    613 	.globl _AX5043_FREQDEV11
                                    614 	.globl _AX5043_FREQDEV10
                                    615 	.globl _AX5043_FREQDEV03
                                    616 	.globl _AX5043_FREQDEV02
                                    617 	.globl _AX5043_FREQDEV01
                                    618 	.globl _AX5043_FREQDEV00
                                    619 	.globl _AX5043_FOURFSK3
                                    620 	.globl _AX5043_FOURFSK2
                                    621 	.globl _AX5043_FOURFSK1
                                    622 	.globl _AX5043_FOURFSK0
                                    623 	.globl _AX5043_DRGAIN3
                                    624 	.globl _AX5043_DRGAIN2
                                    625 	.globl _AX5043_DRGAIN1
                                    626 	.globl _AX5043_DRGAIN0
                                    627 	.globl _AX5043_BBOFFSRES3
                                    628 	.globl _AX5043_BBOFFSRES2
                                    629 	.globl _AX5043_BBOFFSRES1
                                    630 	.globl _AX5043_BBOFFSRES0
                                    631 	.globl _AX5043_AMPLITUDEGAIN3
                                    632 	.globl _AX5043_AMPLITUDEGAIN2
                                    633 	.globl _AX5043_AMPLITUDEGAIN1
                                    634 	.globl _AX5043_AMPLITUDEGAIN0
                                    635 	.globl _AX5043_AGCTARGET3
                                    636 	.globl _AX5043_AGCTARGET2
                                    637 	.globl _AX5043_AGCTARGET1
                                    638 	.globl _AX5043_AGCTARGET0
                                    639 	.globl _AX5043_AGCMINMAX3
                                    640 	.globl _AX5043_AGCMINMAX2
                                    641 	.globl _AX5043_AGCMINMAX1
                                    642 	.globl _AX5043_AGCMINMAX0
                                    643 	.globl _AX5043_AGCGAIN3
                                    644 	.globl _AX5043_AGCGAIN2
                                    645 	.globl _AX5043_AGCGAIN1
                                    646 	.globl _AX5043_AGCGAIN0
                                    647 	.globl _AX5043_AGCAHYST3
                                    648 	.globl _AX5043_AGCAHYST2
                                    649 	.globl _AX5043_AGCAHYST1
                                    650 	.globl _AX5043_AGCAHYST0
                                    651 	.globl _AX5043_0xF44
                                    652 	.globl _AX5043_0xF35
                                    653 	.globl _AX5043_0xF34
                                    654 	.globl _AX5043_0xF33
                                    655 	.globl _AX5043_0xF32
                                    656 	.globl _AX5043_0xF31
                                    657 	.globl _AX5043_0xF30
                                    658 	.globl _AX5043_0xF26
                                    659 	.globl _AX5043_0xF23
                                    660 	.globl _AX5043_0xF22
                                    661 	.globl _AX5043_0xF21
                                    662 	.globl _AX5043_0xF1C
                                    663 	.globl _AX5043_0xF18
                                    664 	.globl _AX5043_0xF0C
                                    665 	.globl _AX5043_0xF00
                                    666 	.globl _AX5043_XTALSTATUS
                                    667 	.globl _AX5043_XTALOSC
                                    668 	.globl _AX5043_XTALCAP
                                    669 	.globl _AX5043_XTALAMPL
                                    670 	.globl _AX5043_WAKEUPXOEARLY
                                    671 	.globl _AX5043_WAKEUPTIMER1
                                    672 	.globl _AX5043_WAKEUPTIMER0
                                    673 	.globl _AX5043_WAKEUPFREQ1
                                    674 	.globl _AX5043_WAKEUPFREQ0
                                    675 	.globl _AX5043_WAKEUP1
                                    676 	.globl _AX5043_WAKEUP0
                                    677 	.globl _AX5043_TXRATE2
                                    678 	.globl _AX5043_TXRATE1
                                    679 	.globl _AX5043_TXRATE0
                                    680 	.globl _AX5043_TXPWRCOEFFE1
                                    681 	.globl _AX5043_TXPWRCOEFFE0
                                    682 	.globl _AX5043_TXPWRCOEFFD1
                                    683 	.globl _AX5043_TXPWRCOEFFD0
                                    684 	.globl _AX5043_TXPWRCOEFFC1
                                    685 	.globl _AX5043_TXPWRCOEFFC0
                                    686 	.globl _AX5043_TXPWRCOEFFB1
                                    687 	.globl _AX5043_TXPWRCOEFFB0
                                    688 	.globl _AX5043_TXPWRCOEFFA1
                                    689 	.globl _AX5043_TXPWRCOEFFA0
                                    690 	.globl _AX5043_TRKRFFREQ2
                                    691 	.globl _AX5043_TRKRFFREQ1
                                    692 	.globl _AX5043_TRKRFFREQ0
                                    693 	.globl _AX5043_TRKPHASE1
                                    694 	.globl _AX5043_TRKPHASE0
                                    695 	.globl _AX5043_TRKFSKDEMOD1
                                    696 	.globl _AX5043_TRKFSKDEMOD0
                                    697 	.globl _AX5043_TRKFREQ1
                                    698 	.globl _AX5043_TRKFREQ0
                                    699 	.globl _AX5043_TRKDATARATE2
                                    700 	.globl _AX5043_TRKDATARATE1
                                    701 	.globl _AX5043_TRKDATARATE0
                                    702 	.globl _AX5043_TRKAMPLITUDE1
                                    703 	.globl _AX5043_TRKAMPLITUDE0
                                    704 	.globl _AX5043_TRKAFSKDEMOD1
                                    705 	.globl _AX5043_TRKAFSKDEMOD0
                                    706 	.globl _AX5043_TMGTXSETTLE
                                    707 	.globl _AX5043_TMGTXBOOST
                                    708 	.globl _AX5043_TMGRXSETTLE
                                    709 	.globl _AX5043_TMGRXRSSI
                                    710 	.globl _AX5043_TMGRXPREAMBLE3
                                    711 	.globl _AX5043_TMGRXPREAMBLE2
                                    712 	.globl _AX5043_TMGRXPREAMBLE1
                                    713 	.globl _AX5043_TMGRXOFFSACQ
                                    714 	.globl _AX5043_TMGRXCOARSEAGC
                                    715 	.globl _AX5043_TMGRXBOOST
                                    716 	.globl _AX5043_TMGRXAGC
                                    717 	.globl _AX5043_TIMER2
                                    718 	.globl _AX5043_TIMER1
                                    719 	.globl _AX5043_TIMER0
                                    720 	.globl _AX5043_SILICONREVISION
                                    721 	.globl _AX5043_SCRATCH
                                    722 	.globl _AX5043_RXDATARATE2
                                    723 	.globl _AX5043_RXDATARATE1
                                    724 	.globl _AX5043_RXDATARATE0
                                    725 	.globl _AX5043_RSSIREFERENCE
                                    726 	.globl _AX5043_RSSIABSTHR
                                    727 	.globl _AX5043_RSSI
                                    728 	.globl _AX5043_REF
                                    729 	.globl _AX5043_RADIOSTATE
                                    730 	.globl _AX5043_RADIOEVENTREQ1
                                    731 	.globl _AX5043_RADIOEVENTREQ0
                                    732 	.globl _AX5043_RADIOEVENTMASK1
                                    733 	.globl _AX5043_RADIOEVENTMASK0
                                    734 	.globl _AX5043_PWRMODE
                                    735 	.globl _AX5043_PWRAMP
                                    736 	.globl _AX5043_POWSTICKYSTAT
                                    737 	.globl _AX5043_POWSTAT
                                    738 	.globl _AX5043_POWIRQMASK
                                    739 	.globl _AX5043_POWCTRL1
                                    740 	.globl _AX5043_PLLVCOIR
                                    741 	.globl _AX5043_PLLVCOI
                                    742 	.globl _AX5043_PLLVCODIV
                                    743 	.globl _AX5043_PLLRNGCLK
                                    744 	.globl _AX5043_PLLRANGINGB
                                    745 	.globl _AX5043_PLLRANGINGA
                                    746 	.globl _AX5043_PLLLOOPBOOST
                                    747 	.globl _AX5043_PLLLOOP
                                    748 	.globl _AX5043_PLLLOCKDET
                                    749 	.globl _AX5043_PLLCPIBOOST
                                    750 	.globl _AX5043_PLLCPI
                                    751 	.globl _AX5043_PKTSTOREFLAGS
                                    752 	.globl _AX5043_PKTMISCFLAGS
                                    753 	.globl _AX5043_PKTCHUNKSIZE
                                    754 	.globl _AX5043_PKTACCEPTFLAGS
                                    755 	.globl _AX5043_PINSTATE
                                    756 	.globl _AX5043_PINFUNCSYSCLK
                                    757 	.globl _AX5043_PINFUNCPWRAMP
                                    758 	.globl _AX5043_PINFUNCIRQ
                                    759 	.globl _AX5043_PINFUNCDCLK
                                    760 	.globl _AX5043_PINFUNCDATA
                                    761 	.globl _AX5043_PINFUNCANTSEL
                                    762 	.globl _AX5043_MODULATION
                                    763 	.globl _AX5043_MODCFGP
                                    764 	.globl _AX5043_MODCFGF
                                    765 	.globl _AX5043_MODCFGA
                                    766 	.globl _AX5043_MAXRFOFFSET2
                                    767 	.globl _AX5043_MAXRFOFFSET1
                                    768 	.globl _AX5043_MAXRFOFFSET0
                                    769 	.globl _AX5043_MAXDROFFSET2
                                    770 	.globl _AX5043_MAXDROFFSET1
                                    771 	.globl _AX5043_MAXDROFFSET0
                                    772 	.globl _AX5043_MATCH1PAT1
                                    773 	.globl _AX5043_MATCH1PAT0
                                    774 	.globl _AX5043_MATCH1MIN
                                    775 	.globl _AX5043_MATCH1MAX
                                    776 	.globl _AX5043_MATCH1LEN
                                    777 	.globl _AX5043_MATCH0PAT3
                                    778 	.globl _AX5043_MATCH0PAT2
                                    779 	.globl _AX5043_MATCH0PAT1
                                    780 	.globl _AX5043_MATCH0PAT0
                                    781 	.globl _AX5043_MATCH0MIN
                                    782 	.globl _AX5043_MATCH0MAX
                                    783 	.globl _AX5043_MATCH0LEN
                                    784 	.globl _AX5043_LPOSCSTATUS
                                    785 	.globl _AX5043_LPOSCREF1
                                    786 	.globl _AX5043_LPOSCREF0
                                    787 	.globl _AX5043_LPOSCPER1
                                    788 	.globl _AX5043_LPOSCPER0
                                    789 	.globl _AX5043_LPOSCKFILT1
                                    790 	.globl _AX5043_LPOSCKFILT0
                                    791 	.globl _AX5043_LPOSCFREQ1
                                    792 	.globl _AX5043_LPOSCFREQ0
                                    793 	.globl _AX5043_LPOSCCONFIG
                                    794 	.globl _AX5043_IRQREQUEST1
                                    795 	.globl _AX5043_IRQREQUEST0
                                    796 	.globl _AX5043_IRQMASK1
                                    797 	.globl _AX5043_IRQMASK0
                                    798 	.globl _AX5043_IRQINVERSION1
                                    799 	.globl _AX5043_IRQINVERSION0
                                    800 	.globl _AX5043_IFFREQ1
                                    801 	.globl _AX5043_IFFREQ0
                                    802 	.globl _AX5043_GPADCPERIOD
                                    803 	.globl _AX5043_GPADCCTRL
                                    804 	.globl _AX5043_GPADC13VALUE1
                                    805 	.globl _AX5043_GPADC13VALUE0
                                    806 	.globl _AX5043_FSKDMIN1
                                    807 	.globl _AX5043_FSKDMIN0
                                    808 	.globl _AX5043_FSKDMAX1
                                    809 	.globl _AX5043_FSKDMAX0
                                    810 	.globl _AX5043_FSKDEV2
                                    811 	.globl _AX5043_FSKDEV1
                                    812 	.globl _AX5043_FSKDEV0
                                    813 	.globl _AX5043_FREQB3
                                    814 	.globl _AX5043_FREQB2
                                    815 	.globl _AX5043_FREQB1
                                    816 	.globl _AX5043_FREQB0
                                    817 	.globl _AX5043_FREQA3
                                    818 	.globl _AX5043_FREQA2
                                    819 	.globl _AX5043_FREQA1
                                    820 	.globl _AX5043_FREQA0
                                    821 	.globl _AX5043_FRAMING
                                    822 	.globl _AX5043_FIFOTHRESH1
                                    823 	.globl _AX5043_FIFOTHRESH0
                                    824 	.globl _AX5043_FIFOSTAT
                                    825 	.globl _AX5043_FIFOFREE1
                                    826 	.globl _AX5043_FIFOFREE0
                                    827 	.globl _AX5043_FIFODATA
                                    828 	.globl _AX5043_FIFOCOUNT1
                                    829 	.globl _AX5043_FIFOCOUNT0
                                    830 	.globl _AX5043_FECSYNC
                                    831 	.globl _AX5043_FECSTATUS
                                    832 	.globl _AX5043_FEC
                                    833 	.globl _AX5043_ENCODING
                                    834 	.globl _AX5043_DIVERSITY
                                    835 	.globl _AX5043_DECIMATION
                                    836 	.globl _AX5043_DACVALUE1
                                    837 	.globl _AX5043_DACVALUE0
                                    838 	.globl _AX5043_DACCONFIG
                                    839 	.globl _AX5043_CRCINIT3
                                    840 	.globl _AX5043_CRCINIT2
                                    841 	.globl _AX5043_CRCINIT1
                                    842 	.globl _AX5043_CRCINIT0
                                    843 	.globl _AX5043_BGNDRSSITHR
                                    844 	.globl _AX5043_BGNDRSSIGAIN
                                    845 	.globl _AX5043_BGNDRSSI
                                    846 	.globl _AX5043_BBTUNE
                                    847 	.globl _AX5043_BBOFFSCAP
                                    848 	.globl _AX5043_AMPLFILTER
                                    849 	.globl _AX5043_AGCCOUNTER
                                    850 	.globl _AX5043_AFSKSPACE1
                                    851 	.globl _AX5043_AFSKSPACE0
                                    852 	.globl _AX5043_AFSKMARK1
                                    853 	.globl _AX5043_AFSKMARK0
                                    854 	.globl _AX5043_AFSKCTRL
                                    855 	.globl _XWTSTAT
                                    856 	.globl _XWTIRQEN
                                    857 	.globl _XWTEVTD
                                    858 	.globl _XWTEVTD1
                                    859 	.globl _XWTEVTD0
                                    860 	.globl _XWTEVTC
                                    861 	.globl _XWTEVTC1
                                    862 	.globl _XWTEVTC0
                                    863 	.globl _XWTEVTB
                                    864 	.globl _XWTEVTB1
                                    865 	.globl _XWTEVTB0
                                    866 	.globl _XWTEVTA
                                    867 	.globl _XWTEVTA1
                                    868 	.globl _XWTEVTA0
                                    869 	.globl _XWTCNTR1
                                    870 	.globl _XWTCNTB
                                    871 	.globl _XWTCNTB1
                                    872 	.globl _XWTCNTB0
                                    873 	.globl _XWTCNTA
                                    874 	.globl _XWTCNTA1
                                    875 	.globl _XWTCNTA0
                                    876 	.globl _XWTCFGB
                                    877 	.globl _XWTCFGA
                                    878 	.globl _XWDTRESET
                                    879 	.globl _XWDTCFG
                                    880 	.globl _XU1STATUS
                                    881 	.globl _XU1SHREG
                                    882 	.globl _XU1MODE
                                    883 	.globl _XU1CTRL
                                    884 	.globl _XU0STATUS
                                    885 	.globl _XU0SHREG
                                    886 	.globl _XU0MODE
                                    887 	.globl _XU0CTRL
                                    888 	.globl _XT2STATUS
                                    889 	.globl _XT2PERIOD
                                    890 	.globl _XT2PERIOD1
                                    891 	.globl _XT2PERIOD0
                                    892 	.globl _XT2MODE
                                    893 	.globl _XT2CNT
                                    894 	.globl _XT2CNT1
                                    895 	.globl _XT2CNT0
                                    896 	.globl _XT2CLKSRC
                                    897 	.globl _XT1STATUS
                                    898 	.globl _XT1PERIOD
                                    899 	.globl _XT1PERIOD1
                                    900 	.globl _XT1PERIOD0
                                    901 	.globl _XT1MODE
                                    902 	.globl _XT1CNT
                                    903 	.globl _XT1CNT1
                                    904 	.globl _XT1CNT0
                                    905 	.globl _XT1CLKSRC
                                    906 	.globl _XT0STATUS
                                    907 	.globl _XT0PERIOD
                                    908 	.globl _XT0PERIOD1
                                    909 	.globl _XT0PERIOD0
                                    910 	.globl _XT0MODE
                                    911 	.globl _XT0CNT
                                    912 	.globl _XT0CNT1
                                    913 	.globl _XT0CNT0
                                    914 	.globl _XT0CLKSRC
                                    915 	.globl _XSPSTATUS
                                    916 	.globl _XSPSHREG
                                    917 	.globl _XSPMODE
                                    918 	.globl _XSPCLKSRC
                                    919 	.globl _XRADIOSTAT
                                    920 	.globl _XRADIOSTAT1
                                    921 	.globl _XRADIOSTAT0
                                    922 	.globl _XRADIODATA3
                                    923 	.globl _XRADIODATA2
                                    924 	.globl _XRADIODATA1
                                    925 	.globl _XRADIODATA0
                                    926 	.globl _XRADIOADDR1
                                    927 	.globl _XRADIOADDR0
                                    928 	.globl _XRADIOACC
                                    929 	.globl _XOC1STATUS
                                    930 	.globl _XOC1PIN
                                    931 	.globl _XOC1MODE
                                    932 	.globl _XOC1COMP
                                    933 	.globl _XOC1COMP1
                                    934 	.globl _XOC1COMP0
                                    935 	.globl _XOC0STATUS
                                    936 	.globl _XOC0PIN
                                    937 	.globl _XOC0MODE
                                    938 	.globl _XOC0COMP
                                    939 	.globl _XOC0COMP1
                                    940 	.globl _XOC0COMP0
                                    941 	.globl _XNVSTATUS
                                    942 	.globl _XNVKEY
                                    943 	.globl _XNVDATA
                                    944 	.globl _XNVDATA1
                                    945 	.globl _XNVDATA0
                                    946 	.globl _XNVADDR
                                    947 	.globl _XNVADDR1
                                    948 	.globl _XNVADDR0
                                    949 	.globl _XIC1STATUS
                                    950 	.globl _XIC1MODE
                                    951 	.globl _XIC1CAPT
                                    952 	.globl _XIC1CAPT1
                                    953 	.globl _XIC1CAPT0
                                    954 	.globl _XIC0STATUS
                                    955 	.globl _XIC0MODE
                                    956 	.globl _XIC0CAPT
                                    957 	.globl _XIC0CAPT1
                                    958 	.globl _XIC0CAPT0
                                    959 	.globl _XPORTR
                                    960 	.globl _XPORTC
                                    961 	.globl _XPORTB
                                    962 	.globl _XPORTA
                                    963 	.globl _XPINR
                                    964 	.globl _XPINC
                                    965 	.globl _XPINB
                                    966 	.globl _XPINA
                                    967 	.globl _XDIRR
                                    968 	.globl _XDIRC
                                    969 	.globl _XDIRB
                                    970 	.globl _XDIRA
                                    971 	.globl _XDBGLNKSTAT
                                    972 	.globl _XDBGLNKBUF
                                    973 	.globl _XCODECONFIG
                                    974 	.globl _XCLKSTAT
                                    975 	.globl _XCLKCON
                                    976 	.globl _XANALOGCOMP
                                    977 	.globl _XADCCONV
                                    978 	.globl _XADCCLKSRC
                                    979 	.globl _XADCCH3CONFIG
                                    980 	.globl _XADCCH2CONFIG
                                    981 	.globl _XADCCH1CONFIG
                                    982 	.globl _XADCCH0CONFIG
                                    983 	.globl _XPCON
                                    984 	.globl _XIP
                                    985 	.globl _XIE
                                    986 	.globl _XDPTR1
                                    987 	.globl _XDPTR0
                                    988 	.globl _RNGCLKSRC1
                                    989 	.globl _RNGCLKSRC0
                                    990 	.globl _RNGMODE
                                    991 	.globl _AESOUTADDR
                                    992 	.globl _AESOUTADDR1
                                    993 	.globl _AESOUTADDR0
                                    994 	.globl _AESMODE
                                    995 	.globl _AESKEYADDR
                                    996 	.globl _AESKEYADDR1
                                    997 	.globl _AESKEYADDR0
                                    998 	.globl _AESINADDR
                                    999 	.globl _AESINADDR1
                                   1000 	.globl _AESINADDR0
                                   1001 	.globl _AESCURBLOCK
                                   1002 	.globl _AESCONFIG
                                   1003 	.globl _RNGBYTE
                                   1004 	.globl _XTALREADY
                                   1005 	.globl _XTALOSC
                                   1006 	.globl _XTALAMPL
                                   1007 	.globl _SILICONREV
                                   1008 	.globl _SCRATCH3
                                   1009 	.globl _SCRATCH2
                                   1010 	.globl _SCRATCH1
                                   1011 	.globl _SCRATCH0
                                   1012 	.globl _RADIOMUX
                                   1013 	.globl _RADIOFSTATADDR
                                   1014 	.globl _RADIOFSTATADDR1
                                   1015 	.globl _RADIOFSTATADDR0
                                   1016 	.globl _RADIOFDATAADDR
                                   1017 	.globl _RADIOFDATAADDR1
                                   1018 	.globl _RADIOFDATAADDR0
                                   1019 	.globl _OSCRUN
                                   1020 	.globl _OSCREADY
                                   1021 	.globl _OSCFORCERUN
                                   1022 	.globl _OSCCALIB
                                   1023 	.globl _MISCCTRL
                                   1024 	.globl _LPXOSCGM
                                   1025 	.globl _LPOSCREF
                                   1026 	.globl _LPOSCREF1
                                   1027 	.globl _LPOSCREF0
                                   1028 	.globl _LPOSCPER
                                   1029 	.globl _LPOSCPER1
                                   1030 	.globl _LPOSCPER0
                                   1031 	.globl _LPOSCKFILT
                                   1032 	.globl _LPOSCKFILT1
                                   1033 	.globl _LPOSCKFILT0
                                   1034 	.globl _LPOSCFREQ
                                   1035 	.globl _LPOSCFREQ1
                                   1036 	.globl _LPOSCFREQ0
                                   1037 	.globl _LPOSCCONFIG
                                   1038 	.globl _PINSEL
                                   1039 	.globl _PINCHGC
                                   1040 	.globl _PINCHGB
                                   1041 	.globl _PINCHGA
                                   1042 	.globl _PALTRADIO
                                   1043 	.globl _PALTC
                                   1044 	.globl _PALTB
                                   1045 	.globl _PALTA
                                   1046 	.globl _INTCHGC
                                   1047 	.globl _INTCHGB
                                   1048 	.globl _INTCHGA
                                   1049 	.globl _EXTIRQ
                                   1050 	.globl _GPIOENABLE
                                   1051 	.globl _ANALOGA
                                   1052 	.globl _FRCOSCREF
                                   1053 	.globl _FRCOSCREF1
                                   1054 	.globl _FRCOSCREF0
                                   1055 	.globl _FRCOSCPER
                                   1056 	.globl _FRCOSCPER1
                                   1057 	.globl _FRCOSCPER0
                                   1058 	.globl _FRCOSCKFILT
                                   1059 	.globl _FRCOSCKFILT1
                                   1060 	.globl _FRCOSCKFILT0
                                   1061 	.globl _FRCOSCFREQ
                                   1062 	.globl _FRCOSCFREQ1
                                   1063 	.globl _FRCOSCFREQ0
                                   1064 	.globl _FRCOSCCTRL
                                   1065 	.globl _FRCOSCCONFIG
                                   1066 	.globl _DMA1CONFIG
                                   1067 	.globl _DMA1ADDR
                                   1068 	.globl _DMA1ADDR1
                                   1069 	.globl _DMA1ADDR0
                                   1070 	.globl _DMA0CONFIG
                                   1071 	.globl _DMA0ADDR
                                   1072 	.globl _DMA0ADDR1
                                   1073 	.globl _DMA0ADDR0
                                   1074 	.globl _ADCTUNE2
                                   1075 	.globl _ADCTUNE1
                                   1076 	.globl _ADCTUNE0
                                   1077 	.globl _ADCCH3VAL
                                   1078 	.globl _ADCCH3VAL1
                                   1079 	.globl _ADCCH3VAL0
                                   1080 	.globl _ADCCH2VAL
                                   1081 	.globl _ADCCH2VAL1
                                   1082 	.globl _ADCCH2VAL0
                                   1083 	.globl _ADCCH1VAL
                                   1084 	.globl _ADCCH1VAL1
                                   1085 	.globl _ADCCH1VAL0
                                   1086 	.globl _ADCCH0VAL
                                   1087 	.globl _ADCCH0VAL1
                                   1088 	.globl _ADCCH0VAL0
                                   1089 	.globl _com0_portinit
                                   1090 	.globl _com0_init
                                   1091 	.globl _com0_setpos
                                   1092 	.globl _com0_writestr
                                   1093 	.globl _com0_tx
                                   1094 	.globl _com0_clear
                                   1095 ;--------------------------------------------------------
                                   1096 ; special function registers
                                   1097 ;--------------------------------------------------------
                                   1098 	.area RSEG    (ABS,DATA)
      000000                       1099 	.org 0x0000
                           0000E0  1100 _ACC	=	0x00e0
                           0000F0  1101 _B	=	0x00f0
                           000083  1102 _DPH	=	0x0083
                           000085  1103 _DPH1	=	0x0085
                           000082  1104 _DPL	=	0x0082
                           000084  1105 _DPL1	=	0x0084
                           008382  1106 _DPTR0	=	0x8382
                           008584  1107 _DPTR1	=	0x8584
                           000086  1108 _DPS	=	0x0086
                           0000A0  1109 _E2IE	=	0x00a0
                           0000C0  1110 _E2IP	=	0x00c0
                           000098  1111 _EIE	=	0x0098
                           0000B0  1112 _EIP	=	0x00b0
                           0000A8  1113 _IE	=	0x00a8
                           0000B8  1114 _IP	=	0x00b8
                           000087  1115 _PCON	=	0x0087
                           0000D0  1116 _PSW	=	0x00d0
                           000081  1117 _SP	=	0x0081
                           0000D9  1118 _XPAGE	=	0x00d9
                           0000D9  1119 __XPAGE	=	0x00d9
                           0000CA  1120 _ADCCH0CONFIG	=	0x00ca
                           0000CB  1121 _ADCCH1CONFIG	=	0x00cb
                           0000D2  1122 _ADCCH2CONFIG	=	0x00d2
                           0000D3  1123 _ADCCH3CONFIG	=	0x00d3
                           0000D1  1124 _ADCCLKSRC	=	0x00d1
                           0000C9  1125 _ADCCONV	=	0x00c9
                           0000E1  1126 _ANALOGCOMP	=	0x00e1
                           0000C6  1127 _CLKCON	=	0x00c6
                           0000C7  1128 _CLKSTAT	=	0x00c7
                           000097  1129 _CODECONFIG	=	0x0097
                           0000E3  1130 _DBGLNKBUF	=	0x00e3
                           0000E2  1131 _DBGLNKSTAT	=	0x00e2
                           000089  1132 _DIRA	=	0x0089
                           00008A  1133 _DIRB	=	0x008a
                           00008B  1134 _DIRC	=	0x008b
                           00008E  1135 _DIRR	=	0x008e
                           0000C8  1136 _PINA	=	0x00c8
                           0000E8  1137 _PINB	=	0x00e8
                           0000F8  1138 _PINC	=	0x00f8
                           00008D  1139 _PINR	=	0x008d
                           000080  1140 _PORTA	=	0x0080
                           000088  1141 _PORTB	=	0x0088
                           000090  1142 _PORTC	=	0x0090
                           00008C  1143 _PORTR	=	0x008c
                           0000CE  1144 _IC0CAPT0	=	0x00ce
                           0000CF  1145 _IC0CAPT1	=	0x00cf
                           00CFCE  1146 _IC0CAPT	=	0xcfce
                           0000CC  1147 _IC0MODE	=	0x00cc
                           0000CD  1148 _IC0STATUS	=	0x00cd
                           0000D6  1149 _IC1CAPT0	=	0x00d6
                           0000D7  1150 _IC1CAPT1	=	0x00d7
                           00D7D6  1151 _IC1CAPT	=	0xd7d6
                           0000D4  1152 _IC1MODE	=	0x00d4
                           0000D5  1153 _IC1STATUS	=	0x00d5
                           000092  1154 _NVADDR0	=	0x0092
                           000093  1155 _NVADDR1	=	0x0093
                           009392  1156 _NVADDR	=	0x9392
                           000094  1157 _NVDATA0	=	0x0094
                           000095  1158 _NVDATA1	=	0x0095
                           009594  1159 _NVDATA	=	0x9594
                           000096  1160 _NVKEY	=	0x0096
                           000091  1161 _NVSTATUS	=	0x0091
                           0000BC  1162 _OC0COMP0	=	0x00bc
                           0000BD  1163 _OC0COMP1	=	0x00bd
                           00BDBC  1164 _OC0COMP	=	0xbdbc
                           0000B9  1165 _OC0MODE	=	0x00b9
                           0000BA  1166 _OC0PIN	=	0x00ba
                           0000BB  1167 _OC0STATUS	=	0x00bb
                           0000C4  1168 _OC1COMP0	=	0x00c4
                           0000C5  1169 _OC1COMP1	=	0x00c5
                           00C5C4  1170 _OC1COMP	=	0xc5c4
                           0000C1  1171 _OC1MODE	=	0x00c1
                           0000C2  1172 _OC1PIN	=	0x00c2
                           0000C3  1173 _OC1STATUS	=	0x00c3
                           0000B1  1174 _RADIOACC	=	0x00b1
                           0000B3  1175 _RADIOADDR0	=	0x00b3
                           0000B2  1176 _RADIOADDR1	=	0x00b2
                           00B2B3  1177 _RADIOADDR	=	0xb2b3
                           0000B7  1178 _RADIODATA0	=	0x00b7
                           0000B6  1179 _RADIODATA1	=	0x00b6
                           0000B5  1180 _RADIODATA2	=	0x00b5
                           0000B4  1181 _RADIODATA3	=	0x00b4
                           B4B5B6B7  1182 _RADIODATA	=	0xb4b5b6b7
                           0000BE  1183 _RADIOSTAT0	=	0x00be
                           0000BF  1184 _RADIOSTAT1	=	0x00bf
                           00BFBE  1185 _RADIOSTAT	=	0xbfbe
                           0000DF  1186 _SPCLKSRC	=	0x00df
                           0000DC  1187 _SPMODE	=	0x00dc
                           0000DE  1188 _SPSHREG	=	0x00de
                           0000DD  1189 _SPSTATUS	=	0x00dd
                           00009A  1190 _T0CLKSRC	=	0x009a
                           00009C  1191 _T0CNT0	=	0x009c
                           00009D  1192 _T0CNT1	=	0x009d
                           009D9C  1193 _T0CNT	=	0x9d9c
                           000099  1194 _T0MODE	=	0x0099
                           00009E  1195 _T0PERIOD0	=	0x009e
                           00009F  1196 _T0PERIOD1	=	0x009f
                           009F9E  1197 _T0PERIOD	=	0x9f9e
                           00009B  1198 _T0STATUS	=	0x009b
                           0000A2  1199 _T1CLKSRC	=	0x00a2
                           0000A4  1200 _T1CNT0	=	0x00a4
                           0000A5  1201 _T1CNT1	=	0x00a5
                           00A5A4  1202 _T1CNT	=	0xa5a4
                           0000A1  1203 _T1MODE	=	0x00a1
                           0000A6  1204 _T1PERIOD0	=	0x00a6
                           0000A7  1205 _T1PERIOD1	=	0x00a7
                           00A7A6  1206 _T1PERIOD	=	0xa7a6
                           0000A3  1207 _T1STATUS	=	0x00a3
                           0000AA  1208 _T2CLKSRC	=	0x00aa
                           0000AC  1209 _T2CNT0	=	0x00ac
                           0000AD  1210 _T2CNT1	=	0x00ad
                           00ADAC  1211 _T2CNT	=	0xadac
                           0000A9  1212 _T2MODE	=	0x00a9
                           0000AE  1213 _T2PERIOD0	=	0x00ae
                           0000AF  1214 _T2PERIOD1	=	0x00af
                           00AFAE  1215 _T2PERIOD	=	0xafae
                           0000AB  1216 _T2STATUS	=	0x00ab
                           0000E4  1217 _U0CTRL	=	0x00e4
                           0000E7  1218 _U0MODE	=	0x00e7
                           0000E6  1219 _U0SHREG	=	0x00e6
                           0000E5  1220 _U0STATUS	=	0x00e5
                           0000EC  1221 _U1CTRL	=	0x00ec
                           0000EF  1222 _U1MODE	=	0x00ef
                           0000EE  1223 _U1SHREG	=	0x00ee
                           0000ED  1224 _U1STATUS	=	0x00ed
                           0000DA  1225 _WDTCFG	=	0x00da
                           0000DB  1226 _WDTRESET	=	0x00db
                           0000F1  1227 _WTCFGA	=	0x00f1
                           0000F9  1228 _WTCFGB	=	0x00f9
                           0000F2  1229 _WTCNTA0	=	0x00f2
                           0000F3  1230 _WTCNTA1	=	0x00f3
                           00F3F2  1231 _WTCNTA	=	0xf3f2
                           0000FA  1232 _WTCNTB0	=	0x00fa
                           0000FB  1233 _WTCNTB1	=	0x00fb
                           00FBFA  1234 _WTCNTB	=	0xfbfa
                           0000EB  1235 _WTCNTR1	=	0x00eb
                           0000F4  1236 _WTEVTA0	=	0x00f4
                           0000F5  1237 _WTEVTA1	=	0x00f5
                           00F5F4  1238 _WTEVTA	=	0xf5f4
                           0000F6  1239 _WTEVTB0	=	0x00f6
                           0000F7  1240 _WTEVTB1	=	0x00f7
                           00F7F6  1241 _WTEVTB	=	0xf7f6
                           0000FC  1242 _WTEVTC0	=	0x00fc
                           0000FD  1243 _WTEVTC1	=	0x00fd
                           00FDFC  1244 _WTEVTC	=	0xfdfc
                           0000FE  1245 _WTEVTD0	=	0x00fe
                           0000FF  1246 _WTEVTD1	=	0x00ff
                           00FFFE  1247 _WTEVTD	=	0xfffe
                           0000E9  1248 _WTIRQEN	=	0x00e9
                           0000EA  1249 _WTSTAT	=	0x00ea
                                   1250 ;--------------------------------------------------------
                                   1251 ; special function bits
                                   1252 ;--------------------------------------------------------
                                   1253 	.area RSEG    (ABS,DATA)
      000000                       1254 	.org 0x0000
                           0000E0  1255 _ACC_0	=	0x00e0
                           0000E1  1256 _ACC_1	=	0x00e1
                           0000E2  1257 _ACC_2	=	0x00e2
                           0000E3  1258 _ACC_3	=	0x00e3
                           0000E4  1259 _ACC_4	=	0x00e4
                           0000E5  1260 _ACC_5	=	0x00e5
                           0000E6  1261 _ACC_6	=	0x00e6
                           0000E7  1262 _ACC_7	=	0x00e7
                           0000F0  1263 _B_0	=	0x00f0
                           0000F1  1264 _B_1	=	0x00f1
                           0000F2  1265 _B_2	=	0x00f2
                           0000F3  1266 _B_3	=	0x00f3
                           0000F4  1267 _B_4	=	0x00f4
                           0000F5  1268 _B_5	=	0x00f5
                           0000F6  1269 _B_6	=	0x00f6
                           0000F7  1270 _B_7	=	0x00f7
                           0000A0  1271 _E2IE_0	=	0x00a0
                           0000A1  1272 _E2IE_1	=	0x00a1
                           0000A2  1273 _E2IE_2	=	0x00a2
                           0000A3  1274 _E2IE_3	=	0x00a3
                           0000A4  1275 _E2IE_4	=	0x00a4
                           0000A5  1276 _E2IE_5	=	0x00a5
                           0000A6  1277 _E2IE_6	=	0x00a6
                           0000A7  1278 _E2IE_7	=	0x00a7
                           0000C0  1279 _E2IP_0	=	0x00c0
                           0000C1  1280 _E2IP_1	=	0x00c1
                           0000C2  1281 _E2IP_2	=	0x00c2
                           0000C3  1282 _E2IP_3	=	0x00c3
                           0000C4  1283 _E2IP_4	=	0x00c4
                           0000C5  1284 _E2IP_5	=	0x00c5
                           0000C6  1285 _E2IP_6	=	0x00c6
                           0000C7  1286 _E2IP_7	=	0x00c7
                           000098  1287 _EIE_0	=	0x0098
                           000099  1288 _EIE_1	=	0x0099
                           00009A  1289 _EIE_2	=	0x009a
                           00009B  1290 _EIE_3	=	0x009b
                           00009C  1291 _EIE_4	=	0x009c
                           00009D  1292 _EIE_5	=	0x009d
                           00009E  1293 _EIE_6	=	0x009e
                           00009F  1294 _EIE_7	=	0x009f
                           0000B0  1295 _EIP_0	=	0x00b0
                           0000B1  1296 _EIP_1	=	0x00b1
                           0000B2  1297 _EIP_2	=	0x00b2
                           0000B3  1298 _EIP_3	=	0x00b3
                           0000B4  1299 _EIP_4	=	0x00b4
                           0000B5  1300 _EIP_5	=	0x00b5
                           0000B6  1301 _EIP_6	=	0x00b6
                           0000B7  1302 _EIP_7	=	0x00b7
                           0000A8  1303 _IE_0	=	0x00a8
                           0000A9  1304 _IE_1	=	0x00a9
                           0000AA  1305 _IE_2	=	0x00aa
                           0000AB  1306 _IE_3	=	0x00ab
                           0000AC  1307 _IE_4	=	0x00ac
                           0000AD  1308 _IE_5	=	0x00ad
                           0000AE  1309 _IE_6	=	0x00ae
                           0000AF  1310 _IE_7	=	0x00af
                           0000AF  1311 _EA	=	0x00af
                           0000B8  1312 _IP_0	=	0x00b8
                           0000B9  1313 _IP_1	=	0x00b9
                           0000BA  1314 _IP_2	=	0x00ba
                           0000BB  1315 _IP_3	=	0x00bb
                           0000BC  1316 _IP_4	=	0x00bc
                           0000BD  1317 _IP_5	=	0x00bd
                           0000BE  1318 _IP_6	=	0x00be
                           0000BF  1319 _IP_7	=	0x00bf
                           0000D0  1320 _P	=	0x00d0
                           0000D1  1321 _F1	=	0x00d1
                           0000D2  1322 _OV	=	0x00d2
                           0000D3  1323 _RS0	=	0x00d3
                           0000D4  1324 _RS1	=	0x00d4
                           0000D5  1325 _F0	=	0x00d5
                           0000D6  1326 _AC	=	0x00d6
                           0000D7  1327 _CY	=	0x00d7
                           0000C8  1328 _PINA_0	=	0x00c8
                           0000C9  1329 _PINA_1	=	0x00c9
                           0000CA  1330 _PINA_2	=	0x00ca
                           0000CB  1331 _PINA_3	=	0x00cb
                           0000CC  1332 _PINA_4	=	0x00cc
                           0000CD  1333 _PINA_5	=	0x00cd
                           0000CE  1334 _PINA_6	=	0x00ce
                           0000CF  1335 _PINA_7	=	0x00cf
                           0000E8  1336 _PINB_0	=	0x00e8
                           0000E9  1337 _PINB_1	=	0x00e9
                           0000EA  1338 _PINB_2	=	0x00ea
                           0000EB  1339 _PINB_3	=	0x00eb
                           0000EC  1340 _PINB_4	=	0x00ec
                           0000ED  1341 _PINB_5	=	0x00ed
                           0000EE  1342 _PINB_6	=	0x00ee
                           0000EF  1343 _PINB_7	=	0x00ef
                           0000F8  1344 _PINC_0	=	0x00f8
                           0000F9  1345 _PINC_1	=	0x00f9
                           0000FA  1346 _PINC_2	=	0x00fa
                           0000FB  1347 _PINC_3	=	0x00fb
                           0000FC  1348 _PINC_4	=	0x00fc
                           0000FD  1349 _PINC_5	=	0x00fd
                           0000FE  1350 _PINC_6	=	0x00fe
                           0000FF  1351 _PINC_7	=	0x00ff
                           000080  1352 _PORTA_0	=	0x0080
                           000081  1353 _PORTA_1	=	0x0081
                           000082  1354 _PORTA_2	=	0x0082
                           000083  1355 _PORTA_3	=	0x0083
                           000084  1356 _PORTA_4	=	0x0084
                           000085  1357 _PORTA_5	=	0x0085
                           000086  1358 _PORTA_6	=	0x0086
                           000087  1359 _PORTA_7	=	0x0087
                           000088  1360 _PORTB_0	=	0x0088
                           000089  1361 _PORTB_1	=	0x0089
                           00008A  1362 _PORTB_2	=	0x008a
                           00008B  1363 _PORTB_3	=	0x008b
                           00008C  1364 _PORTB_4	=	0x008c
                           00008D  1365 _PORTB_5	=	0x008d
                           00008E  1366 _PORTB_6	=	0x008e
                           00008F  1367 _PORTB_7	=	0x008f
                           000090  1368 _PORTC_0	=	0x0090
                           000091  1369 _PORTC_1	=	0x0091
                           000092  1370 _PORTC_2	=	0x0092
                           000093  1371 _PORTC_3	=	0x0093
                           000094  1372 _PORTC_4	=	0x0094
                           000095  1373 _PORTC_5	=	0x0095
                           000096  1374 _PORTC_6	=	0x0096
                           000097  1375 _PORTC_7	=	0x0097
                                   1376 ;--------------------------------------------------------
                                   1377 ; overlayable register banks
                                   1378 ;--------------------------------------------------------
                                   1379 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                       1380 	.ds 8
                                   1381 ;--------------------------------------------------------
                                   1382 ; internal ram data
                                   1383 ;--------------------------------------------------------
                                   1384 	.area DSEG    (DATA)
                                   1385 ;--------------------------------------------------------
                                   1386 ; overlayable items in internal ram 
                                   1387 ;--------------------------------------------------------
                                   1388 ;--------------------------------------------------------
                                   1389 ; indirectly addressable internal ram data
                                   1390 ;--------------------------------------------------------
                                   1391 	.area ISEG    (DATA)
                                   1392 ;--------------------------------------------------------
                                   1393 ; absolute internal ram data
                                   1394 ;--------------------------------------------------------
                                   1395 	.area IABS    (ABS,DATA)
                                   1396 	.area IABS    (ABS,DATA)
                                   1397 ;--------------------------------------------------------
                                   1398 ; bit data
                                   1399 ;--------------------------------------------------------
                                   1400 	.area BSEG    (BIT)
                                   1401 ;--------------------------------------------------------
                                   1402 ; paged external ram data
                                   1403 ;--------------------------------------------------------
                                   1404 	.area PSEG    (PAG,XDATA)
                                   1405 ;--------------------------------------------------------
                                   1406 ; external ram data
                                   1407 ;--------------------------------------------------------
                                   1408 	.area XSEG    (XDATA)
                           00FC06  1409 _flash_deviceid	=	0xfc06
                           00FC00  1410 _flash_calsector	=	0xfc00
                           007020  1411 _ADCCH0VAL0	=	0x7020
                           007021  1412 _ADCCH0VAL1	=	0x7021
                           007020  1413 _ADCCH0VAL	=	0x7020
                           007022  1414 _ADCCH1VAL0	=	0x7022
                           007023  1415 _ADCCH1VAL1	=	0x7023
                           007022  1416 _ADCCH1VAL	=	0x7022
                           007024  1417 _ADCCH2VAL0	=	0x7024
                           007025  1418 _ADCCH2VAL1	=	0x7025
                           007024  1419 _ADCCH2VAL	=	0x7024
                           007026  1420 _ADCCH3VAL0	=	0x7026
                           007027  1421 _ADCCH3VAL1	=	0x7027
                           007026  1422 _ADCCH3VAL	=	0x7026
                           007028  1423 _ADCTUNE0	=	0x7028
                           007029  1424 _ADCTUNE1	=	0x7029
                           00702A  1425 _ADCTUNE2	=	0x702a
                           007010  1426 _DMA0ADDR0	=	0x7010
                           007011  1427 _DMA0ADDR1	=	0x7011
                           007010  1428 _DMA0ADDR	=	0x7010
                           007014  1429 _DMA0CONFIG	=	0x7014
                           007012  1430 _DMA1ADDR0	=	0x7012
                           007013  1431 _DMA1ADDR1	=	0x7013
                           007012  1432 _DMA1ADDR	=	0x7012
                           007015  1433 _DMA1CONFIG	=	0x7015
                           007070  1434 _FRCOSCCONFIG	=	0x7070
                           007071  1435 _FRCOSCCTRL	=	0x7071
                           007076  1436 _FRCOSCFREQ0	=	0x7076
                           007077  1437 _FRCOSCFREQ1	=	0x7077
                           007076  1438 _FRCOSCFREQ	=	0x7076
                           007072  1439 _FRCOSCKFILT0	=	0x7072
                           007073  1440 _FRCOSCKFILT1	=	0x7073
                           007072  1441 _FRCOSCKFILT	=	0x7072
                           007078  1442 _FRCOSCPER0	=	0x7078
                           007079  1443 _FRCOSCPER1	=	0x7079
                           007078  1444 _FRCOSCPER	=	0x7078
                           007074  1445 _FRCOSCREF0	=	0x7074
                           007075  1446 _FRCOSCREF1	=	0x7075
                           007074  1447 _FRCOSCREF	=	0x7074
                           007007  1448 _ANALOGA	=	0x7007
                           00700C  1449 _GPIOENABLE	=	0x700c
                           007003  1450 _EXTIRQ	=	0x7003
                           007000  1451 _INTCHGA	=	0x7000
                           007001  1452 _INTCHGB	=	0x7001
                           007002  1453 _INTCHGC	=	0x7002
                           007008  1454 _PALTA	=	0x7008
                           007009  1455 _PALTB	=	0x7009
                           00700A  1456 _PALTC	=	0x700a
                           007046  1457 _PALTRADIO	=	0x7046
                           007004  1458 _PINCHGA	=	0x7004
                           007005  1459 _PINCHGB	=	0x7005
                           007006  1460 _PINCHGC	=	0x7006
                           00700B  1461 _PINSEL	=	0x700b
                           007060  1462 _LPOSCCONFIG	=	0x7060
                           007066  1463 _LPOSCFREQ0	=	0x7066
                           007067  1464 _LPOSCFREQ1	=	0x7067
                           007066  1465 _LPOSCFREQ	=	0x7066
                           007062  1466 _LPOSCKFILT0	=	0x7062
                           007063  1467 _LPOSCKFILT1	=	0x7063
                           007062  1468 _LPOSCKFILT	=	0x7062
                           007068  1469 _LPOSCPER0	=	0x7068
                           007069  1470 _LPOSCPER1	=	0x7069
                           007068  1471 _LPOSCPER	=	0x7068
                           007064  1472 _LPOSCREF0	=	0x7064
                           007065  1473 _LPOSCREF1	=	0x7065
                           007064  1474 _LPOSCREF	=	0x7064
                           007054  1475 _LPXOSCGM	=	0x7054
                           007F01  1476 _MISCCTRL	=	0x7f01
                           007053  1477 _OSCCALIB	=	0x7053
                           007050  1478 _OSCFORCERUN	=	0x7050
                           007052  1479 _OSCREADY	=	0x7052
                           007051  1480 _OSCRUN	=	0x7051
                           007040  1481 _RADIOFDATAADDR0	=	0x7040
                           007041  1482 _RADIOFDATAADDR1	=	0x7041
                           007040  1483 _RADIOFDATAADDR	=	0x7040
                           007042  1484 _RADIOFSTATADDR0	=	0x7042
                           007043  1485 _RADIOFSTATADDR1	=	0x7043
                           007042  1486 _RADIOFSTATADDR	=	0x7042
                           007044  1487 _RADIOMUX	=	0x7044
                           007084  1488 _SCRATCH0	=	0x7084
                           007085  1489 _SCRATCH1	=	0x7085
                           007086  1490 _SCRATCH2	=	0x7086
                           007087  1491 _SCRATCH3	=	0x7087
                           007F00  1492 _SILICONREV	=	0x7f00
                           007F19  1493 _XTALAMPL	=	0x7f19
                           007F18  1494 _XTALOSC	=	0x7f18
                           007F1A  1495 _XTALREADY	=	0x7f1a
                           007081  1496 _RNGBYTE	=	0x7081
                           007091  1497 _AESCONFIG	=	0x7091
                           007098  1498 _AESCURBLOCK	=	0x7098
                           007094  1499 _AESINADDR0	=	0x7094
                           007095  1500 _AESINADDR1	=	0x7095
                           007094  1501 _AESINADDR	=	0x7094
                           007092  1502 _AESKEYADDR0	=	0x7092
                           007093  1503 _AESKEYADDR1	=	0x7093
                           007092  1504 _AESKEYADDR	=	0x7092
                           007090  1505 _AESMODE	=	0x7090
                           007096  1506 _AESOUTADDR0	=	0x7096
                           007097  1507 _AESOUTADDR1	=	0x7097
                           007096  1508 _AESOUTADDR	=	0x7096
                           007080  1509 _RNGMODE	=	0x7080
                           007082  1510 _RNGCLKSRC0	=	0x7082
                           007083  1511 _RNGCLKSRC1	=	0x7083
                           003F82  1512 _XDPTR0	=	0x3f82
                           003F84  1513 _XDPTR1	=	0x3f84
                           003FA8  1514 _XIE	=	0x3fa8
                           003FB8  1515 _XIP	=	0x3fb8
                           003F87  1516 _XPCON	=	0x3f87
                           003FCA  1517 _XADCCH0CONFIG	=	0x3fca
                           003FCB  1518 _XADCCH1CONFIG	=	0x3fcb
                           003FD2  1519 _XADCCH2CONFIG	=	0x3fd2
                           003FD3  1520 _XADCCH3CONFIG	=	0x3fd3
                           003FD1  1521 _XADCCLKSRC	=	0x3fd1
                           003FC9  1522 _XADCCONV	=	0x3fc9
                           003FE1  1523 _XANALOGCOMP	=	0x3fe1
                           003FC6  1524 _XCLKCON	=	0x3fc6
                           003FC7  1525 _XCLKSTAT	=	0x3fc7
                           003F97  1526 _XCODECONFIG	=	0x3f97
                           003FE3  1527 _XDBGLNKBUF	=	0x3fe3
                           003FE2  1528 _XDBGLNKSTAT	=	0x3fe2
                           003F89  1529 _XDIRA	=	0x3f89
                           003F8A  1530 _XDIRB	=	0x3f8a
                           003F8B  1531 _XDIRC	=	0x3f8b
                           003F8E  1532 _XDIRR	=	0x3f8e
                           003FC8  1533 _XPINA	=	0x3fc8
                           003FE8  1534 _XPINB	=	0x3fe8
                           003FF8  1535 _XPINC	=	0x3ff8
                           003F8D  1536 _XPINR	=	0x3f8d
                           003F80  1537 _XPORTA	=	0x3f80
                           003F88  1538 _XPORTB	=	0x3f88
                           003F90  1539 _XPORTC	=	0x3f90
                           003F8C  1540 _XPORTR	=	0x3f8c
                           003FCE  1541 _XIC0CAPT0	=	0x3fce
                           003FCF  1542 _XIC0CAPT1	=	0x3fcf
                           003FCE  1543 _XIC0CAPT	=	0x3fce
                           003FCC  1544 _XIC0MODE	=	0x3fcc
                           003FCD  1545 _XIC0STATUS	=	0x3fcd
                           003FD6  1546 _XIC1CAPT0	=	0x3fd6
                           003FD7  1547 _XIC1CAPT1	=	0x3fd7
                           003FD6  1548 _XIC1CAPT	=	0x3fd6
                           003FD4  1549 _XIC1MODE	=	0x3fd4
                           003FD5  1550 _XIC1STATUS	=	0x3fd5
                           003F92  1551 _XNVADDR0	=	0x3f92
                           003F93  1552 _XNVADDR1	=	0x3f93
                           003F92  1553 _XNVADDR	=	0x3f92
                           003F94  1554 _XNVDATA0	=	0x3f94
                           003F95  1555 _XNVDATA1	=	0x3f95
                           003F94  1556 _XNVDATA	=	0x3f94
                           003F96  1557 _XNVKEY	=	0x3f96
                           003F91  1558 _XNVSTATUS	=	0x3f91
                           003FBC  1559 _XOC0COMP0	=	0x3fbc
                           003FBD  1560 _XOC0COMP1	=	0x3fbd
                           003FBC  1561 _XOC0COMP	=	0x3fbc
                           003FB9  1562 _XOC0MODE	=	0x3fb9
                           003FBA  1563 _XOC0PIN	=	0x3fba
                           003FBB  1564 _XOC0STATUS	=	0x3fbb
                           003FC4  1565 _XOC1COMP0	=	0x3fc4
                           003FC5  1566 _XOC1COMP1	=	0x3fc5
                           003FC4  1567 _XOC1COMP	=	0x3fc4
                           003FC1  1568 _XOC1MODE	=	0x3fc1
                           003FC2  1569 _XOC1PIN	=	0x3fc2
                           003FC3  1570 _XOC1STATUS	=	0x3fc3
                           003FB1  1571 _XRADIOACC	=	0x3fb1
                           003FB3  1572 _XRADIOADDR0	=	0x3fb3
                           003FB2  1573 _XRADIOADDR1	=	0x3fb2
                           003FB7  1574 _XRADIODATA0	=	0x3fb7
                           003FB6  1575 _XRADIODATA1	=	0x3fb6
                           003FB5  1576 _XRADIODATA2	=	0x3fb5
                           003FB4  1577 _XRADIODATA3	=	0x3fb4
                           003FBE  1578 _XRADIOSTAT0	=	0x3fbe
                           003FBF  1579 _XRADIOSTAT1	=	0x3fbf
                           003FBE  1580 _XRADIOSTAT	=	0x3fbe
                           003FDF  1581 _XSPCLKSRC	=	0x3fdf
                           003FDC  1582 _XSPMODE	=	0x3fdc
                           003FDE  1583 _XSPSHREG	=	0x3fde
                           003FDD  1584 _XSPSTATUS	=	0x3fdd
                           003F9A  1585 _XT0CLKSRC	=	0x3f9a
                           003F9C  1586 _XT0CNT0	=	0x3f9c
                           003F9D  1587 _XT0CNT1	=	0x3f9d
                           003F9C  1588 _XT0CNT	=	0x3f9c
                           003F99  1589 _XT0MODE	=	0x3f99
                           003F9E  1590 _XT0PERIOD0	=	0x3f9e
                           003F9F  1591 _XT0PERIOD1	=	0x3f9f
                           003F9E  1592 _XT0PERIOD	=	0x3f9e
                           003F9B  1593 _XT0STATUS	=	0x3f9b
                           003FA2  1594 _XT1CLKSRC	=	0x3fa2
                           003FA4  1595 _XT1CNT0	=	0x3fa4
                           003FA5  1596 _XT1CNT1	=	0x3fa5
                           003FA4  1597 _XT1CNT	=	0x3fa4
                           003FA1  1598 _XT1MODE	=	0x3fa1
                           003FA6  1599 _XT1PERIOD0	=	0x3fa6
                           003FA7  1600 _XT1PERIOD1	=	0x3fa7
                           003FA6  1601 _XT1PERIOD	=	0x3fa6
                           003FA3  1602 _XT1STATUS	=	0x3fa3
                           003FAA  1603 _XT2CLKSRC	=	0x3faa
                           003FAC  1604 _XT2CNT0	=	0x3fac
                           003FAD  1605 _XT2CNT1	=	0x3fad
                           003FAC  1606 _XT2CNT	=	0x3fac
                           003FA9  1607 _XT2MODE	=	0x3fa9
                           003FAE  1608 _XT2PERIOD0	=	0x3fae
                           003FAF  1609 _XT2PERIOD1	=	0x3faf
                           003FAE  1610 _XT2PERIOD	=	0x3fae
                           003FAB  1611 _XT2STATUS	=	0x3fab
                           003FE4  1612 _XU0CTRL	=	0x3fe4
                           003FE7  1613 _XU0MODE	=	0x3fe7
                           003FE6  1614 _XU0SHREG	=	0x3fe6
                           003FE5  1615 _XU0STATUS	=	0x3fe5
                           003FEC  1616 _XU1CTRL	=	0x3fec
                           003FEF  1617 _XU1MODE	=	0x3fef
                           003FEE  1618 _XU1SHREG	=	0x3fee
                           003FED  1619 _XU1STATUS	=	0x3fed
                           003FDA  1620 _XWDTCFG	=	0x3fda
                           003FDB  1621 _XWDTRESET	=	0x3fdb
                           003FF1  1622 _XWTCFGA	=	0x3ff1
                           003FF9  1623 _XWTCFGB	=	0x3ff9
                           003FF2  1624 _XWTCNTA0	=	0x3ff2
                           003FF3  1625 _XWTCNTA1	=	0x3ff3
                           003FF2  1626 _XWTCNTA	=	0x3ff2
                           003FFA  1627 _XWTCNTB0	=	0x3ffa
                           003FFB  1628 _XWTCNTB1	=	0x3ffb
                           003FFA  1629 _XWTCNTB	=	0x3ffa
                           003FEB  1630 _XWTCNTR1	=	0x3feb
                           003FF4  1631 _XWTEVTA0	=	0x3ff4
                           003FF5  1632 _XWTEVTA1	=	0x3ff5
                           003FF4  1633 _XWTEVTA	=	0x3ff4
                           003FF6  1634 _XWTEVTB0	=	0x3ff6
                           003FF7  1635 _XWTEVTB1	=	0x3ff7
                           003FF6  1636 _XWTEVTB	=	0x3ff6
                           003FFC  1637 _XWTEVTC0	=	0x3ffc
                           003FFD  1638 _XWTEVTC1	=	0x3ffd
                           003FFC  1639 _XWTEVTC	=	0x3ffc
                           003FFE  1640 _XWTEVTD0	=	0x3ffe
                           003FFF  1641 _XWTEVTD1	=	0x3fff
                           003FFE  1642 _XWTEVTD	=	0x3ffe
                           003FE9  1643 _XWTIRQEN	=	0x3fe9
                           003FEA  1644 _XWTSTAT	=	0x3fea
                           004114  1645 _AX5043_AFSKCTRL	=	0x4114
                           004113  1646 _AX5043_AFSKMARK0	=	0x4113
                           004112  1647 _AX5043_AFSKMARK1	=	0x4112
                           004111  1648 _AX5043_AFSKSPACE0	=	0x4111
                           004110  1649 _AX5043_AFSKSPACE1	=	0x4110
                           004043  1650 _AX5043_AGCCOUNTER	=	0x4043
                           004115  1651 _AX5043_AMPLFILTER	=	0x4115
                           004189  1652 _AX5043_BBOFFSCAP	=	0x4189
                           004188  1653 _AX5043_BBTUNE	=	0x4188
                           004041  1654 _AX5043_BGNDRSSI	=	0x4041
                           00422E  1655 _AX5043_BGNDRSSIGAIN	=	0x422e
                           00422F  1656 _AX5043_BGNDRSSITHR	=	0x422f
                           004017  1657 _AX5043_CRCINIT0	=	0x4017
                           004016  1658 _AX5043_CRCINIT1	=	0x4016
                           004015  1659 _AX5043_CRCINIT2	=	0x4015
                           004014  1660 _AX5043_CRCINIT3	=	0x4014
                           004332  1661 _AX5043_DACCONFIG	=	0x4332
                           004331  1662 _AX5043_DACVALUE0	=	0x4331
                           004330  1663 _AX5043_DACVALUE1	=	0x4330
                           004102  1664 _AX5043_DECIMATION	=	0x4102
                           004042  1665 _AX5043_DIVERSITY	=	0x4042
                           004011  1666 _AX5043_ENCODING	=	0x4011
                           004018  1667 _AX5043_FEC	=	0x4018
                           00401A  1668 _AX5043_FECSTATUS	=	0x401a
                           004019  1669 _AX5043_FECSYNC	=	0x4019
                           00402B  1670 _AX5043_FIFOCOUNT0	=	0x402b
                           00402A  1671 _AX5043_FIFOCOUNT1	=	0x402a
                           004029  1672 _AX5043_FIFODATA	=	0x4029
                           00402D  1673 _AX5043_FIFOFREE0	=	0x402d
                           00402C  1674 _AX5043_FIFOFREE1	=	0x402c
                           004028  1675 _AX5043_FIFOSTAT	=	0x4028
                           00402F  1676 _AX5043_FIFOTHRESH0	=	0x402f
                           00402E  1677 _AX5043_FIFOTHRESH1	=	0x402e
                           004012  1678 _AX5043_FRAMING	=	0x4012
                           004037  1679 _AX5043_FREQA0	=	0x4037
                           004036  1680 _AX5043_FREQA1	=	0x4036
                           004035  1681 _AX5043_FREQA2	=	0x4035
                           004034  1682 _AX5043_FREQA3	=	0x4034
                           00403F  1683 _AX5043_FREQB0	=	0x403f
                           00403E  1684 _AX5043_FREQB1	=	0x403e
                           00403D  1685 _AX5043_FREQB2	=	0x403d
                           00403C  1686 _AX5043_FREQB3	=	0x403c
                           004163  1687 _AX5043_FSKDEV0	=	0x4163
                           004162  1688 _AX5043_FSKDEV1	=	0x4162
                           004161  1689 _AX5043_FSKDEV2	=	0x4161
                           00410D  1690 _AX5043_FSKDMAX0	=	0x410d
                           00410C  1691 _AX5043_FSKDMAX1	=	0x410c
                           00410F  1692 _AX5043_FSKDMIN0	=	0x410f
                           00410E  1693 _AX5043_FSKDMIN1	=	0x410e
                           004309  1694 _AX5043_GPADC13VALUE0	=	0x4309
                           004308  1695 _AX5043_GPADC13VALUE1	=	0x4308
                           004300  1696 _AX5043_GPADCCTRL	=	0x4300
                           004301  1697 _AX5043_GPADCPERIOD	=	0x4301
                           004101  1698 _AX5043_IFFREQ0	=	0x4101
                           004100  1699 _AX5043_IFFREQ1	=	0x4100
                           00400B  1700 _AX5043_IRQINVERSION0	=	0x400b
                           00400A  1701 _AX5043_IRQINVERSION1	=	0x400a
                           004007  1702 _AX5043_IRQMASK0	=	0x4007
                           004006  1703 _AX5043_IRQMASK1	=	0x4006
                           00400D  1704 _AX5043_IRQREQUEST0	=	0x400d
                           00400C  1705 _AX5043_IRQREQUEST1	=	0x400c
                           004310  1706 _AX5043_LPOSCCONFIG	=	0x4310
                           004317  1707 _AX5043_LPOSCFREQ0	=	0x4317
                           004316  1708 _AX5043_LPOSCFREQ1	=	0x4316
                           004313  1709 _AX5043_LPOSCKFILT0	=	0x4313
                           004312  1710 _AX5043_LPOSCKFILT1	=	0x4312
                           004319  1711 _AX5043_LPOSCPER0	=	0x4319
                           004318  1712 _AX5043_LPOSCPER1	=	0x4318
                           004315  1713 _AX5043_LPOSCREF0	=	0x4315
                           004314  1714 _AX5043_LPOSCREF1	=	0x4314
                           004311  1715 _AX5043_LPOSCSTATUS	=	0x4311
                           004214  1716 _AX5043_MATCH0LEN	=	0x4214
                           004216  1717 _AX5043_MATCH0MAX	=	0x4216
                           004215  1718 _AX5043_MATCH0MIN	=	0x4215
                           004213  1719 _AX5043_MATCH0PAT0	=	0x4213
                           004212  1720 _AX5043_MATCH0PAT1	=	0x4212
                           004211  1721 _AX5043_MATCH0PAT2	=	0x4211
                           004210  1722 _AX5043_MATCH0PAT3	=	0x4210
                           00421C  1723 _AX5043_MATCH1LEN	=	0x421c
                           00421E  1724 _AX5043_MATCH1MAX	=	0x421e
                           00421D  1725 _AX5043_MATCH1MIN	=	0x421d
                           004219  1726 _AX5043_MATCH1PAT0	=	0x4219
                           004218  1727 _AX5043_MATCH1PAT1	=	0x4218
                           004108  1728 _AX5043_MAXDROFFSET0	=	0x4108
                           004107  1729 _AX5043_MAXDROFFSET1	=	0x4107
                           004106  1730 _AX5043_MAXDROFFSET2	=	0x4106
                           00410B  1731 _AX5043_MAXRFOFFSET0	=	0x410b
                           00410A  1732 _AX5043_MAXRFOFFSET1	=	0x410a
                           004109  1733 _AX5043_MAXRFOFFSET2	=	0x4109
                           004164  1734 _AX5043_MODCFGA	=	0x4164
                           004160  1735 _AX5043_MODCFGF	=	0x4160
                           004F5F  1736 _AX5043_MODCFGP	=	0x4f5f
                           004010  1737 _AX5043_MODULATION	=	0x4010
                           004025  1738 _AX5043_PINFUNCANTSEL	=	0x4025
                           004023  1739 _AX5043_PINFUNCDATA	=	0x4023
                           004022  1740 _AX5043_PINFUNCDCLK	=	0x4022
                           004024  1741 _AX5043_PINFUNCIRQ	=	0x4024
                           004026  1742 _AX5043_PINFUNCPWRAMP	=	0x4026
                           004021  1743 _AX5043_PINFUNCSYSCLK	=	0x4021
                           004020  1744 _AX5043_PINSTATE	=	0x4020
                           004233  1745 _AX5043_PKTACCEPTFLAGS	=	0x4233
                           004230  1746 _AX5043_PKTCHUNKSIZE	=	0x4230
                           004231  1747 _AX5043_PKTMISCFLAGS	=	0x4231
                           004232  1748 _AX5043_PKTSTOREFLAGS	=	0x4232
                           004031  1749 _AX5043_PLLCPI	=	0x4031
                           004039  1750 _AX5043_PLLCPIBOOST	=	0x4039
                           004182  1751 _AX5043_PLLLOCKDET	=	0x4182
                           004030  1752 _AX5043_PLLLOOP	=	0x4030
                           004038  1753 _AX5043_PLLLOOPBOOST	=	0x4038
                           004033  1754 _AX5043_PLLRANGINGA	=	0x4033
                           00403B  1755 _AX5043_PLLRANGINGB	=	0x403b
                           004183  1756 _AX5043_PLLRNGCLK	=	0x4183
                           004032  1757 _AX5043_PLLVCODIV	=	0x4032
                           004180  1758 _AX5043_PLLVCOI	=	0x4180
                           004181  1759 _AX5043_PLLVCOIR	=	0x4181
                           004F08  1760 _AX5043_POWCTRL1	=	0x4f08
                           004005  1761 _AX5043_POWIRQMASK	=	0x4005
                           004003  1762 _AX5043_POWSTAT	=	0x4003
                           004004  1763 _AX5043_POWSTICKYSTAT	=	0x4004
                           004027  1764 _AX5043_PWRAMP	=	0x4027
                           004002  1765 _AX5043_PWRMODE	=	0x4002
                           004009  1766 _AX5043_RADIOEVENTMASK0	=	0x4009
                           004008  1767 _AX5043_RADIOEVENTMASK1	=	0x4008
                           00400F  1768 _AX5043_RADIOEVENTREQ0	=	0x400f
                           00400E  1769 _AX5043_RADIOEVENTREQ1	=	0x400e
                           00401C  1770 _AX5043_RADIOSTATE	=	0x401c
                           004F0D  1771 _AX5043_REF	=	0x4f0d
                           004040  1772 _AX5043_RSSI	=	0x4040
                           00422D  1773 _AX5043_RSSIABSTHR	=	0x422d
                           00422C  1774 _AX5043_RSSIREFERENCE	=	0x422c
                           004105  1775 _AX5043_RXDATARATE0	=	0x4105
                           004104  1776 _AX5043_RXDATARATE1	=	0x4104
                           004103  1777 _AX5043_RXDATARATE2	=	0x4103
                           004001  1778 _AX5043_SCRATCH	=	0x4001
                           004000  1779 _AX5043_SILICONREVISION	=	0x4000
                           00405B  1780 _AX5043_TIMER0	=	0x405b
                           00405A  1781 _AX5043_TIMER1	=	0x405a
                           004059  1782 _AX5043_TIMER2	=	0x4059
                           004227  1783 _AX5043_TMGRXAGC	=	0x4227
                           004223  1784 _AX5043_TMGRXBOOST	=	0x4223
                           004226  1785 _AX5043_TMGRXCOARSEAGC	=	0x4226
                           004225  1786 _AX5043_TMGRXOFFSACQ	=	0x4225
                           004229  1787 _AX5043_TMGRXPREAMBLE1	=	0x4229
                           00422A  1788 _AX5043_TMGRXPREAMBLE2	=	0x422a
                           00422B  1789 _AX5043_TMGRXPREAMBLE3	=	0x422b
                           004228  1790 _AX5043_TMGRXRSSI	=	0x4228
                           004224  1791 _AX5043_TMGRXSETTLE	=	0x4224
                           004220  1792 _AX5043_TMGTXBOOST	=	0x4220
                           004221  1793 _AX5043_TMGTXSETTLE	=	0x4221
                           004055  1794 _AX5043_TRKAFSKDEMOD0	=	0x4055
                           004054  1795 _AX5043_TRKAFSKDEMOD1	=	0x4054
                           004049  1796 _AX5043_TRKAMPLITUDE0	=	0x4049
                           004048  1797 _AX5043_TRKAMPLITUDE1	=	0x4048
                           004047  1798 _AX5043_TRKDATARATE0	=	0x4047
                           004046  1799 _AX5043_TRKDATARATE1	=	0x4046
                           004045  1800 _AX5043_TRKDATARATE2	=	0x4045
                           004051  1801 _AX5043_TRKFREQ0	=	0x4051
                           004050  1802 _AX5043_TRKFREQ1	=	0x4050
                           004053  1803 _AX5043_TRKFSKDEMOD0	=	0x4053
                           004052  1804 _AX5043_TRKFSKDEMOD1	=	0x4052
                           00404B  1805 _AX5043_TRKPHASE0	=	0x404b
                           00404A  1806 _AX5043_TRKPHASE1	=	0x404a
                           00404F  1807 _AX5043_TRKRFFREQ0	=	0x404f
                           00404E  1808 _AX5043_TRKRFFREQ1	=	0x404e
                           00404D  1809 _AX5043_TRKRFFREQ2	=	0x404d
                           004169  1810 _AX5043_TXPWRCOEFFA0	=	0x4169
                           004168  1811 _AX5043_TXPWRCOEFFA1	=	0x4168
                           00416B  1812 _AX5043_TXPWRCOEFFB0	=	0x416b
                           00416A  1813 _AX5043_TXPWRCOEFFB1	=	0x416a
                           00416D  1814 _AX5043_TXPWRCOEFFC0	=	0x416d
                           00416C  1815 _AX5043_TXPWRCOEFFC1	=	0x416c
                           00416F  1816 _AX5043_TXPWRCOEFFD0	=	0x416f
                           00416E  1817 _AX5043_TXPWRCOEFFD1	=	0x416e
                           004171  1818 _AX5043_TXPWRCOEFFE0	=	0x4171
                           004170  1819 _AX5043_TXPWRCOEFFE1	=	0x4170
                           004167  1820 _AX5043_TXRATE0	=	0x4167
                           004166  1821 _AX5043_TXRATE1	=	0x4166
                           004165  1822 _AX5043_TXRATE2	=	0x4165
                           00406B  1823 _AX5043_WAKEUP0	=	0x406b
                           00406A  1824 _AX5043_WAKEUP1	=	0x406a
                           00406D  1825 _AX5043_WAKEUPFREQ0	=	0x406d
                           00406C  1826 _AX5043_WAKEUPFREQ1	=	0x406c
                           004069  1827 _AX5043_WAKEUPTIMER0	=	0x4069
                           004068  1828 _AX5043_WAKEUPTIMER1	=	0x4068
                           00406E  1829 _AX5043_WAKEUPXOEARLY	=	0x406e
                           004F11  1830 _AX5043_XTALAMPL	=	0x4f11
                           004184  1831 _AX5043_XTALCAP	=	0x4184
                           004F10  1832 _AX5043_XTALOSC	=	0x4f10
                           00401D  1833 _AX5043_XTALSTATUS	=	0x401d
                           004F00  1834 _AX5043_0xF00	=	0x4f00
                           004F0C  1835 _AX5043_0xF0C	=	0x4f0c
                           004F18  1836 _AX5043_0xF18	=	0x4f18
                           004F1C  1837 _AX5043_0xF1C	=	0x4f1c
                           004F21  1838 _AX5043_0xF21	=	0x4f21
                           004F22  1839 _AX5043_0xF22	=	0x4f22
                           004F23  1840 _AX5043_0xF23	=	0x4f23
                           004F26  1841 _AX5043_0xF26	=	0x4f26
                           004F30  1842 _AX5043_0xF30	=	0x4f30
                           004F31  1843 _AX5043_0xF31	=	0x4f31
                           004F32  1844 _AX5043_0xF32	=	0x4f32
                           004F33  1845 _AX5043_0xF33	=	0x4f33
                           004F34  1846 _AX5043_0xF34	=	0x4f34
                           004F35  1847 _AX5043_0xF35	=	0x4f35
                           004F44  1848 _AX5043_0xF44	=	0x4f44
                           004122  1849 _AX5043_AGCAHYST0	=	0x4122
                           004132  1850 _AX5043_AGCAHYST1	=	0x4132
                           004142  1851 _AX5043_AGCAHYST2	=	0x4142
                           004152  1852 _AX5043_AGCAHYST3	=	0x4152
                           004120  1853 _AX5043_AGCGAIN0	=	0x4120
                           004130  1854 _AX5043_AGCGAIN1	=	0x4130
                           004140  1855 _AX5043_AGCGAIN2	=	0x4140
                           004150  1856 _AX5043_AGCGAIN3	=	0x4150
                           004123  1857 _AX5043_AGCMINMAX0	=	0x4123
                           004133  1858 _AX5043_AGCMINMAX1	=	0x4133
                           004143  1859 _AX5043_AGCMINMAX2	=	0x4143
                           004153  1860 _AX5043_AGCMINMAX3	=	0x4153
                           004121  1861 _AX5043_AGCTARGET0	=	0x4121
                           004131  1862 _AX5043_AGCTARGET1	=	0x4131
                           004141  1863 _AX5043_AGCTARGET2	=	0x4141
                           004151  1864 _AX5043_AGCTARGET3	=	0x4151
                           00412B  1865 _AX5043_AMPLITUDEGAIN0	=	0x412b
                           00413B  1866 _AX5043_AMPLITUDEGAIN1	=	0x413b
                           00414B  1867 _AX5043_AMPLITUDEGAIN2	=	0x414b
                           00415B  1868 _AX5043_AMPLITUDEGAIN3	=	0x415b
                           00412F  1869 _AX5043_BBOFFSRES0	=	0x412f
                           00413F  1870 _AX5043_BBOFFSRES1	=	0x413f
                           00414F  1871 _AX5043_BBOFFSRES2	=	0x414f
                           00415F  1872 _AX5043_BBOFFSRES3	=	0x415f
                           004125  1873 _AX5043_DRGAIN0	=	0x4125
                           004135  1874 _AX5043_DRGAIN1	=	0x4135
                           004145  1875 _AX5043_DRGAIN2	=	0x4145
                           004155  1876 _AX5043_DRGAIN3	=	0x4155
                           00412E  1877 _AX5043_FOURFSK0	=	0x412e
                           00413E  1878 _AX5043_FOURFSK1	=	0x413e
                           00414E  1879 _AX5043_FOURFSK2	=	0x414e
                           00415E  1880 _AX5043_FOURFSK3	=	0x415e
                           00412D  1881 _AX5043_FREQDEV00	=	0x412d
                           00413D  1882 _AX5043_FREQDEV01	=	0x413d
                           00414D  1883 _AX5043_FREQDEV02	=	0x414d
                           00415D  1884 _AX5043_FREQDEV03	=	0x415d
                           00412C  1885 _AX5043_FREQDEV10	=	0x412c
                           00413C  1886 _AX5043_FREQDEV11	=	0x413c
                           00414C  1887 _AX5043_FREQDEV12	=	0x414c
                           00415C  1888 _AX5043_FREQDEV13	=	0x415c
                           004127  1889 _AX5043_FREQUENCYGAINA0	=	0x4127
                           004137  1890 _AX5043_FREQUENCYGAINA1	=	0x4137
                           004147  1891 _AX5043_FREQUENCYGAINA2	=	0x4147
                           004157  1892 _AX5043_FREQUENCYGAINA3	=	0x4157
                           004128  1893 _AX5043_FREQUENCYGAINB0	=	0x4128
                           004138  1894 _AX5043_FREQUENCYGAINB1	=	0x4138
                           004148  1895 _AX5043_FREQUENCYGAINB2	=	0x4148
                           004158  1896 _AX5043_FREQUENCYGAINB3	=	0x4158
                           004129  1897 _AX5043_FREQUENCYGAINC0	=	0x4129
                           004139  1898 _AX5043_FREQUENCYGAINC1	=	0x4139
                           004149  1899 _AX5043_FREQUENCYGAINC2	=	0x4149
                           004159  1900 _AX5043_FREQUENCYGAINC3	=	0x4159
                           00412A  1901 _AX5043_FREQUENCYGAIND0	=	0x412a
                           00413A  1902 _AX5043_FREQUENCYGAIND1	=	0x413a
                           00414A  1903 _AX5043_FREQUENCYGAIND2	=	0x414a
                           00415A  1904 _AX5043_FREQUENCYGAIND3	=	0x415a
                           004116  1905 _AX5043_FREQUENCYLEAK	=	0x4116
                           004126  1906 _AX5043_PHASEGAIN0	=	0x4126
                           004136  1907 _AX5043_PHASEGAIN1	=	0x4136
                           004146  1908 _AX5043_PHASEGAIN2	=	0x4146
                           004156  1909 _AX5043_PHASEGAIN3	=	0x4156
                           004207  1910 _AX5043_PKTADDR0	=	0x4207
                           004206  1911 _AX5043_PKTADDR1	=	0x4206
                           004205  1912 _AX5043_PKTADDR2	=	0x4205
                           004204  1913 _AX5043_PKTADDR3	=	0x4204
                           004200  1914 _AX5043_PKTADDRCFG	=	0x4200
                           00420B  1915 _AX5043_PKTADDRMASK0	=	0x420b
                           00420A  1916 _AX5043_PKTADDRMASK1	=	0x420a
                           004209  1917 _AX5043_PKTADDRMASK2	=	0x4209
                           004208  1918 _AX5043_PKTADDRMASK3	=	0x4208
                           004201  1919 _AX5043_PKTLENCFG	=	0x4201
                           004202  1920 _AX5043_PKTLENOFFSET	=	0x4202
                           004203  1921 _AX5043_PKTMAXLEN	=	0x4203
                           004118  1922 _AX5043_RXPARAMCURSET	=	0x4118
                           004117  1923 _AX5043_RXPARAMSETS	=	0x4117
                           004124  1924 _AX5043_TIMEGAIN0	=	0x4124
                           004134  1925 _AX5043_TIMEGAIN1	=	0x4134
                           004144  1926 _AX5043_TIMEGAIN2	=	0x4144
                           004154  1927 _AX5043_TIMEGAIN3	=	0x4154
                           005114  1928 _AX5043_AFSKCTRLNB	=	0x5114
                           005113  1929 _AX5043_AFSKMARK0NB	=	0x5113
                           005112  1930 _AX5043_AFSKMARK1NB	=	0x5112
                           005111  1931 _AX5043_AFSKSPACE0NB	=	0x5111
                           005110  1932 _AX5043_AFSKSPACE1NB	=	0x5110
                           005043  1933 _AX5043_AGCCOUNTERNB	=	0x5043
                           005115  1934 _AX5043_AMPLFILTERNB	=	0x5115
                           005189  1935 _AX5043_BBOFFSCAPNB	=	0x5189
                           005188  1936 _AX5043_BBTUNENB	=	0x5188
                           005041  1937 _AX5043_BGNDRSSINB	=	0x5041
                           00522E  1938 _AX5043_BGNDRSSIGAINNB	=	0x522e
                           00522F  1939 _AX5043_BGNDRSSITHRNB	=	0x522f
                           005017  1940 _AX5043_CRCINIT0NB	=	0x5017
                           005016  1941 _AX5043_CRCINIT1NB	=	0x5016
                           005015  1942 _AX5043_CRCINIT2NB	=	0x5015
                           005014  1943 _AX5043_CRCINIT3NB	=	0x5014
                           005332  1944 _AX5043_DACCONFIGNB	=	0x5332
                           005331  1945 _AX5043_DACVALUE0NB	=	0x5331
                           005330  1946 _AX5043_DACVALUE1NB	=	0x5330
                           005102  1947 _AX5043_DECIMATIONNB	=	0x5102
                           005042  1948 _AX5043_DIVERSITYNB	=	0x5042
                           005011  1949 _AX5043_ENCODINGNB	=	0x5011
                           005018  1950 _AX5043_FECNB	=	0x5018
                           00501A  1951 _AX5043_FECSTATUSNB	=	0x501a
                           005019  1952 _AX5043_FECSYNCNB	=	0x5019
                           00502B  1953 _AX5043_FIFOCOUNT0NB	=	0x502b
                           00502A  1954 _AX5043_FIFOCOUNT1NB	=	0x502a
                           005029  1955 _AX5043_FIFODATANB	=	0x5029
                           00502D  1956 _AX5043_FIFOFREE0NB	=	0x502d
                           00502C  1957 _AX5043_FIFOFREE1NB	=	0x502c
                           005028  1958 _AX5043_FIFOSTATNB	=	0x5028
                           00502F  1959 _AX5043_FIFOTHRESH0NB	=	0x502f
                           00502E  1960 _AX5043_FIFOTHRESH1NB	=	0x502e
                           005012  1961 _AX5043_FRAMINGNB	=	0x5012
                           005037  1962 _AX5043_FREQA0NB	=	0x5037
                           005036  1963 _AX5043_FREQA1NB	=	0x5036
                           005035  1964 _AX5043_FREQA2NB	=	0x5035
                           005034  1965 _AX5043_FREQA3NB	=	0x5034
                           00503F  1966 _AX5043_FREQB0NB	=	0x503f
                           00503E  1967 _AX5043_FREQB1NB	=	0x503e
                           00503D  1968 _AX5043_FREQB2NB	=	0x503d
                           00503C  1969 _AX5043_FREQB3NB	=	0x503c
                           005163  1970 _AX5043_FSKDEV0NB	=	0x5163
                           005162  1971 _AX5043_FSKDEV1NB	=	0x5162
                           005161  1972 _AX5043_FSKDEV2NB	=	0x5161
                           00510D  1973 _AX5043_FSKDMAX0NB	=	0x510d
                           00510C  1974 _AX5043_FSKDMAX1NB	=	0x510c
                           00510F  1975 _AX5043_FSKDMIN0NB	=	0x510f
                           00510E  1976 _AX5043_FSKDMIN1NB	=	0x510e
                           005309  1977 _AX5043_GPADC13VALUE0NB	=	0x5309
                           005308  1978 _AX5043_GPADC13VALUE1NB	=	0x5308
                           005300  1979 _AX5043_GPADCCTRLNB	=	0x5300
                           005301  1980 _AX5043_GPADCPERIODNB	=	0x5301
                           005101  1981 _AX5043_IFFREQ0NB	=	0x5101
                           005100  1982 _AX5043_IFFREQ1NB	=	0x5100
                           00500B  1983 _AX5043_IRQINVERSION0NB	=	0x500b
                           00500A  1984 _AX5043_IRQINVERSION1NB	=	0x500a
                           005007  1985 _AX5043_IRQMASK0NB	=	0x5007
                           005006  1986 _AX5043_IRQMASK1NB	=	0x5006
                           00500D  1987 _AX5043_IRQREQUEST0NB	=	0x500d
                           00500C  1988 _AX5043_IRQREQUEST1NB	=	0x500c
                           005310  1989 _AX5043_LPOSCCONFIGNB	=	0x5310
                           005317  1990 _AX5043_LPOSCFREQ0NB	=	0x5317
                           005316  1991 _AX5043_LPOSCFREQ1NB	=	0x5316
                           005313  1992 _AX5043_LPOSCKFILT0NB	=	0x5313
                           005312  1993 _AX5043_LPOSCKFILT1NB	=	0x5312
                           005319  1994 _AX5043_LPOSCPER0NB	=	0x5319
                           005318  1995 _AX5043_LPOSCPER1NB	=	0x5318
                           005315  1996 _AX5043_LPOSCREF0NB	=	0x5315
                           005314  1997 _AX5043_LPOSCREF1NB	=	0x5314
                           005311  1998 _AX5043_LPOSCSTATUSNB	=	0x5311
                           005214  1999 _AX5043_MATCH0LENNB	=	0x5214
                           005216  2000 _AX5043_MATCH0MAXNB	=	0x5216
                           005215  2001 _AX5043_MATCH0MINNB	=	0x5215
                           005213  2002 _AX5043_MATCH0PAT0NB	=	0x5213
                           005212  2003 _AX5043_MATCH0PAT1NB	=	0x5212
                           005211  2004 _AX5043_MATCH0PAT2NB	=	0x5211
                           005210  2005 _AX5043_MATCH0PAT3NB	=	0x5210
                           00521C  2006 _AX5043_MATCH1LENNB	=	0x521c
                           00521E  2007 _AX5043_MATCH1MAXNB	=	0x521e
                           00521D  2008 _AX5043_MATCH1MINNB	=	0x521d
                           005219  2009 _AX5043_MATCH1PAT0NB	=	0x5219
                           005218  2010 _AX5043_MATCH1PAT1NB	=	0x5218
                           005108  2011 _AX5043_MAXDROFFSET0NB	=	0x5108
                           005107  2012 _AX5043_MAXDROFFSET1NB	=	0x5107
                           005106  2013 _AX5043_MAXDROFFSET2NB	=	0x5106
                           00510B  2014 _AX5043_MAXRFOFFSET0NB	=	0x510b
                           00510A  2015 _AX5043_MAXRFOFFSET1NB	=	0x510a
                           005109  2016 _AX5043_MAXRFOFFSET2NB	=	0x5109
                           005164  2017 _AX5043_MODCFGANB	=	0x5164
                           005160  2018 _AX5043_MODCFGFNB	=	0x5160
                           005F5F  2019 _AX5043_MODCFGPNB	=	0x5f5f
                           005010  2020 _AX5043_MODULATIONNB	=	0x5010
                           005025  2021 _AX5043_PINFUNCANTSELNB	=	0x5025
                           005023  2022 _AX5043_PINFUNCDATANB	=	0x5023
                           005022  2023 _AX5043_PINFUNCDCLKNB	=	0x5022
                           005024  2024 _AX5043_PINFUNCIRQNB	=	0x5024
                           005026  2025 _AX5043_PINFUNCPWRAMPNB	=	0x5026
                           005021  2026 _AX5043_PINFUNCSYSCLKNB	=	0x5021
                           005020  2027 _AX5043_PINSTATENB	=	0x5020
                           005233  2028 _AX5043_PKTACCEPTFLAGSNB	=	0x5233
                           005230  2029 _AX5043_PKTCHUNKSIZENB	=	0x5230
                           005231  2030 _AX5043_PKTMISCFLAGSNB	=	0x5231
                           005232  2031 _AX5043_PKTSTOREFLAGSNB	=	0x5232
                           005031  2032 _AX5043_PLLCPINB	=	0x5031
                           005039  2033 _AX5043_PLLCPIBOOSTNB	=	0x5039
                           005182  2034 _AX5043_PLLLOCKDETNB	=	0x5182
                           005030  2035 _AX5043_PLLLOOPNB	=	0x5030
                           005038  2036 _AX5043_PLLLOOPBOOSTNB	=	0x5038
                           005033  2037 _AX5043_PLLRANGINGANB	=	0x5033
                           00503B  2038 _AX5043_PLLRANGINGBNB	=	0x503b
                           005183  2039 _AX5043_PLLRNGCLKNB	=	0x5183
                           005032  2040 _AX5043_PLLVCODIVNB	=	0x5032
                           005180  2041 _AX5043_PLLVCOINB	=	0x5180
                           005181  2042 _AX5043_PLLVCOIRNB	=	0x5181
                           005F08  2043 _AX5043_POWCTRL1NB	=	0x5f08
                           005005  2044 _AX5043_POWIRQMASKNB	=	0x5005
                           005003  2045 _AX5043_POWSTATNB	=	0x5003
                           005004  2046 _AX5043_POWSTICKYSTATNB	=	0x5004
                           005027  2047 _AX5043_PWRAMPNB	=	0x5027
                           005002  2048 _AX5043_PWRMODENB	=	0x5002
                           005009  2049 _AX5043_RADIOEVENTMASK0NB	=	0x5009
                           005008  2050 _AX5043_RADIOEVENTMASK1NB	=	0x5008
                           00500F  2051 _AX5043_RADIOEVENTREQ0NB	=	0x500f
                           00500E  2052 _AX5043_RADIOEVENTREQ1NB	=	0x500e
                           00501C  2053 _AX5043_RADIOSTATENB	=	0x501c
                           005F0D  2054 _AX5043_REFNB	=	0x5f0d
                           005040  2055 _AX5043_RSSINB	=	0x5040
                           00522D  2056 _AX5043_RSSIABSTHRNB	=	0x522d
                           00522C  2057 _AX5043_RSSIREFERENCENB	=	0x522c
                           005105  2058 _AX5043_RXDATARATE0NB	=	0x5105
                           005104  2059 _AX5043_RXDATARATE1NB	=	0x5104
                           005103  2060 _AX5043_RXDATARATE2NB	=	0x5103
                           005001  2061 _AX5043_SCRATCHNB	=	0x5001
                           005000  2062 _AX5043_SILICONREVISIONNB	=	0x5000
                           00505B  2063 _AX5043_TIMER0NB	=	0x505b
                           00505A  2064 _AX5043_TIMER1NB	=	0x505a
                           005059  2065 _AX5043_TIMER2NB	=	0x5059
                           005227  2066 _AX5043_TMGRXAGCNB	=	0x5227
                           005223  2067 _AX5043_TMGRXBOOSTNB	=	0x5223
                           005226  2068 _AX5043_TMGRXCOARSEAGCNB	=	0x5226
                           005225  2069 _AX5043_TMGRXOFFSACQNB	=	0x5225
                           005229  2070 _AX5043_TMGRXPREAMBLE1NB	=	0x5229
                           00522A  2071 _AX5043_TMGRXPREAMBLE2NB	=	0x522a
                           00522B  2072 _AX5043_TMGRXPREAMBLE3NB	=	0x522b
                           005228  2073 _AX5043_TMGRXRSSINB	=	0x5228
                           005224  2074 _AX5043_TMGRXSETTLENB	=	0x5224
                           005220  2075 _AX5043_TMGTXBOOSTNB	=	0x5220
                           005221  2076 _AX5043_TMGTXSETTLENB	=	0x5221
                           005055  2077 _AX5043_TRKAFSKDEMOD0NB	=	0x5055
                           005054  2078 _AX5043_TRKAFSKDEMOD1NB	=	0x5054
                           005049  2079 _AX5043_TRKAMPLITUDE0NB	=	0x5049
                           005048  2080 _AX5043_TRKAMPLITUDE1NB	=	0x5048
                           005047  2081 _AX5043_TRKDATARATE0NB	=	0x5047
                           005046  2082 _AX5043_TRKDATARATE1NB	=	0x5046
                           005045  2083 _AX5043_TRKDATARATE2NB	=	0x5045
                           005051  2084 _AX5043_TRKFREQ0NB	=	0x5051
                           005050  2085 _AX5043_TRKFREQ1NB	=	0x5050
                           005053  2086 _AX5043_TRKFSKDEMOD0NB	=	0x5053
                           005052  2087 _AX5043_TRKFSKDEMOD1NB	=	0x5052
                           00504B  2088 _AX5043_TRKPHASE0NB	=	0x504b
                           00504A  2089 _AX5043_TRKPHASE1NB	=	0x504a
                           00504F  2090 _AX5043_TRKRFFREQ0NB	=	0x504f
                           00504E  2091 _AX5043_TRKRFFREQ1NB	=	0x504e
                           00504D  2092 _AX5043_TRKRFFREQ2NB	=	0x504d
                           005169  2093 _AX5043_TXPWRCOEFFA0NB	=	0x5169
                           005168  2094 _AX5043_TXPWRCOEFFA1NB	=	0x5168
                           00516B  2095 _AX5043_TXPWRCOEFFB0NB	=	0x516b
                           00516A  2096 _AX5043_TXPWRCOEFFB1NB	=	0x516a
                           00516D  2097 _AX5043_TXPWRCOEFFC0NB	=	0x516d
                           00516C  2098 _AX5043_TXPWRCOEFFC1NB	=	0x516c
                           00516F  2099 _AX5043_TXPWRCOEFFD0NB	=	0x516f
                           00516E  2100 _AX5043_TXPWRCOEFFD1NB	=	0x516e
                           005171  2101 _AX5043_TXPWRCOEFFE0NB	=	0x5171
                           005170  2102 _AX5043_TXPWRCOEFFE1NB	=	0x5170
                           005167  2103 _AX5043_TXRATE0NB	=	0x5167
                           005166  2104 _AX5043_TXRATE1NB	=	0x5166
                           005165  2105 _AX5043_TXRATE2NB	=	0x5165
                           00506B  2106 _AX5043_WAKEUP0NB	=	0x506b
                           00506A  2107 _AX5043_WAKEUP1NB	=	0x506a
                           00506D  2108 _AX5043_WAKEUPFREQ0NB	=	0x506d
                           00506C  2109 _AX5043_WAKEUPFREQ1NB	=	0x506c
                           005069  2110 _AX5043_WAKEUPTIMER0NB	=	0x5069
                           005068  2111 _AX5043_WAKEUPTIMER1NB	=	0x5068
                           00506E  2112 _AX5043_WAKEUPXOEARLYNB	=	0x506e
                           005F11  2113 _AX5043_XTALAMPLNB	=	0x5f11
                           005184  2114 _AX5043_XTALCAPNB	=	0x5184
                           005F10  2115 _AX5043_XTALOSCNB	=	0x5f10
                           00501D  2116 _AX5043_XTALSTATUSNB	=	0x501d
                           005F00  2117 _AX5043_0xF00NB	=	0x5f00
                           005F0C  2118 _AX5043_0xF0CNB	=	0x5f0c
                           005F18  2119 _AX5043_0xF18NB	=	0x5f18
                           005F1C  2120 _AX5043_0xF1CNB	=	0x5f1c
                           005F21  2121 _AX5043_0xF21NB	=	0x5f21
                           005F22  2122 _AX5043_0xF22NB	=	0x5f22
                           005F23  2123 _AX5043_0xF23NB	=	0x5f23
                           005F26  2124 _AX5043_0xF26NB	=	0x5f26
                           005F30  2125 _AX5043_0xF30NB	=	0x5f30
                           005F31  2126 _AX5043_0xF31NB	=	0x5f31
                           005F32  2127 _AX5043_0xF32NB	=	0x5f32
                           005F33  2128 _AX5043_0xF33NB	=	0x5f33
                           005F34  2129 _AX5043_0xF34NB	=	0x5f34
                           005F35  2130 _AX5043_0xF35NB	=	0x5f35
                           005F44  2131 _AX5043_0xF44NB	=	0x5f44
                           005122  2132 _AX5043_AGCAHYST0NB	=	0x5122
                           005132  2133 _AX5043_AGCAHYST1NB	=	0x5132
                           005142  2134 _AX5043_AGCAHYST2NB	=	0x5142
                           005152  2135 _AX5043_AGCAHYST3NB	=	0x5152
                           005120  2136 _AX5043_AGCGAIN0NB	=	0x5120
                           005130  2137 _AX5043_AGCGAIN1NB	=	0x5130
                           005140  2138 _AX5043_AGCGAIN2NB	=	0x5140
                           005150  2139 _AX5043_AGCGAIN3NB	=	0x5150
                           005123  2140 _AX5043_AGCMINMAX0NB	=	0x5123
                           005133  2141 _AX5043_AGCMINMAX1NB	=	0x5133
                           005143  2142 _AX5043_AGCMINMAX2NB	=	0x5143
                           005153  2143 _AX5043_AGCMINMAX3NB	=	0x5153
                           005121  2144 _AX5043_AGCTARGET0NB	=	0x5121
                           005131  2145 _AX5043_AGCTARGET1NB	=	0x5131
                           005141  2146 _AX5043_AGCTARGET2NB	=	0x5141
                           005151  2147 _AX5043_AGCTARGET3NB	=	0x5151
                           00512B  2148 _AX5043_AMPLITUDEGAIN0NB	=	0x512b
                           00513B  2149 _AX5043_AMPLITUDEGAIN1NB	=	0x513b
                           00514B  2150 _AX5043_AMPLITUDEGAIN2NB	=	0x514b
                           00515B  2151 _AX5043_AMPLITUDEGAIN3NB	=	0x515b
                           00512F  2152 _AX5043_BBOFFSRES0NB	=	0x512f
                           00513F  2153 _AX5043_BBOFFSRES1NB	=	0x513f
                           00514F  2154 _AX5043_BBOFFSRES2NB	=	0x514f
                           00515F  2155 _AX5043_BBOFFSRES3NB	=	0x515f
                           005125  2156 _AX5043_DRGAIN0NB	=	0x5125
                           005135  2157 _AX5043_DRGAIN1NB	=	0x5135
                           005145  2158 _AX5043_DRGAIN2NB	=	0x5145
                           005155  2159 _AX5043_DRGAIN3NB	=	0x5155
                           00512E  2160 _AX5043_FOURFSK0NB	=	0x512e
                           00513E  2161 _AX5043_FOURFSK1NB	=	0x513e
                           00514E  2162 _AX5043_FOURFSK2NB	=	0x514e
                           00515E  2163 _AX5043_FOURFSK3NB	=	0x515e
                           00512D  2164 _AX5043_FREQDEV00NB	=	0x512d
                           00513D  2165 _AX5043_FREQDEV01NB	=	0x513d
                           00514D  2166 _AX5043_FREQDEV02NB	=	0x514d
                           00515D  2167 _AX5043_FREQDEV03NB	=	0x515d
                           00512C  2168 _AX5043_FREQDEV10NB	=	0x512c
                           00513C  2169 _AX5043_FREQDEV11NB	=	0x513c
                           00514C  2170 _AX5043_FREQDEV12NB	=	0x514c
                           00515C  2171 _AX5043_FREQDEV13NB	=	0x515c
                           005127  2172 _AX5043_FREQUENCYGAINA0NB	=	0x5127
                           005137  2173 _AX5043_FREQUENCYGAINA1NB	=	0x5137
                           005147  2174 _AX5043_FREQUENCYGAINA2NB	=	0x5147
                           005157  2175 _AX5043_FREQUENCYGAINA3NB	=	0x5157
                           005128  2176 _AX5043_FREQUENCYGAINB0NB	=	0x5128
                           005138  2177 _AX5043_FREQUENCYGAINB1NB	=	0x5138
                           005148  2178 _AX5043_FREQUENCYGAINB2NB	=	0x5148
                           005158  2179 _AX5043_FREQUENCYGAINB3NB	=	0x5158
                           005129  2180 _AX5043_FREQUENCYGAINC0NB	=	0x5129
                           005139  2181 _AX5043_FREQUENCYGAINC1NB	=	0x5139
                           005149  2182 _AX5043_FREQUENCYGAINC2NB	=	0x5149
                           005159  2183 _AX5043_FREQUENCYGAINC3NB	=	0x5159
                           00512A  2184 _AX5043_FREQUENCYGAIND0NB	=	0x512a
                           00513A  2185 _AX5043_FREQUENCYGAIND1NB	=	0x513a
                           00514A  2186 _AX5043_FREQUENCYGAIND2NB	=	0x514a
                           00515A  2187 _AX5043_FREQUENCYGAIND3NB	=	0x515a
                           005116  2188 _AX5043_FREQUENCYLEAKNB	=	0x5116
                           005126  2189 _AX5043_PHASEGAIN0NB	=	0x5126
                           005136  2190 _AX5043_PHASEGAIN1NB	=	0x5136
                           005146  2191 _AX5043_PHASEGAIN2NB	=	0x5146
                           005156  2192 _AX5043_PHASEGAIN3NB	=	0x5156
                           005207  2193 _AX5043_PKTADDR0NB	=	0x5207
                           005206  2194 _AX5043_PKTADDR1NB	=	0x5206
                           005205  2195 _AX5043_PKTADDR2NB	=	0x5205
                           005204  2196 _AX5043_PKTADDR3NB	=	0x5204
                           005200  2197 _AX5043_PKTADDRCFGNB	=	0x5200
                           00520B  2198 _AX5043_PKTADDRMASK0NB	=	0x520b
                           00520A  2199 _AX5043_PKTADDRMASK1NB	=	0x520a
                           005209  2200 _AX5043_PKTADDRMASK2NB	=	0x5209
                           005208  2201 _AX5043_PKTADDRMASK3NB	=	0x5208
                           005201  2202 _AX5043_PKTLENCFGNB	=	0x5201
                           005202  2203 _AX5043_PKTLENOFFSETNB	=	0x5202
                           005203  2204 _AX5043_PKTMAXLENNB	=	0x5203
                           005118  2205 _AX5043_RXPARAMCURSETNB	=	0x5118
                           005117  2206 _AX5043_RXPARAMSETSNB	=	0x5117
                           005124  2207 _AX5043_TIMEGAIN0NB	=	0x5124
                           005134  2208 _AX5043_TIMEGAIN1NB	=	0x5134
                           005144  2209 _AX5043_TIMEGAIN2NB	=	0x5144
                           005154  2210 _AX5043_TIMEGAIN3NB	=	0x5154
      0000E8                       2211 _row::
      0000E8                       2212 	.ds 1
      0000E9                       2213 _column::
      0000E9                       2214 	.ds 1
                                   2215 ;--------------------------------------------------------
                                   2216 ; absolute external ram data
                                   2217 ;--------------------------------------------------------
                                   2218 	.area XABS    (ABS,XDATA)
                                   2219 ;--------------------------------------------------------
                                   2220 ; external initialized ram data
                                   2221 ;--------------------------------------------------------
                                   2222 	.area XISEG   (XDATA)
                                   2223 	.area HOME    (CODE)
                                   2224 	.area GSINIT0 (CODE)
                                   2225 	.area GSINIT1 (CODE)
                                   2226 	.area GSINIT2 (CODE)
                                   2227 	.area GSINIT3 (CODE)
                                   2228 	.area GSINIT4 (CODE)
                                   2229 	.area GSINIT5 (CODE)
                                   2230 	.area GSINIT  (CODE)
                                   2231 	.area GSFINAL (CODE)
                                   2232 	.area CSEG    (CODE)
                                   2233 ;--------------------------------------------------------
                                   2234 ; global & static initialisations
                                   2235 ;--------------------------------------------------------
                                   2236 	.area HOME    (CODE)
                                   2237 	.area GSINIT  (CODE)
                                   2238 	.area GSFINAL (CODE)
                                   2239 	.area GSINIT  (CODE)
                                   2240 ;--------------------------------------------------------
                                   2241 ; Home
                                   2242 ;--------------------------------------------------------
                                   2243 	.area HOME    (CODE)
                                   2244 	.area HOME    (CODE)
                                   2245 ;--------------------------------------------------------
                                   2246 ; code
                                   2247 ;--------------------------------------------------------
                                   2248 	.area CSEG    (CODE)
                                   2249 ;------------------------------------------------------------
                                   2250 ;Allocation info for local variables in function 'com0_portinit'
                                   2251 ;------------------------------------------------------------
                                   2252 ;	..\src\COMMON\display_com0.c:27: __reentrantb void com0_portinit(void) __reentrant
                                   2253 ;	-----------------------------------------
                                   2254 ;	 function com0_portinit
                                   2255 ;	-----------------------------------------
      001D77                       2256 _com0_portinit:
                           000007  2257 	ar7 = 0x07
                           000006  2258 	ar6 = 0x06
                           000005  2259 	ar5 = 0x05
                           000004  2260 	ar4 = 0x04
                           000003  2261 	ar3 = 0x03
                           000002  2262 	ar2 = 0x02
                           000001  2263 	ar1 = 0x01
                           000000  2264 	ar0 = 0x00
                                   2265 ;	..\src\COMMON\display_com0.c:34: uart_timer0_baud(CLKSRC_SYSCLK, 9600, 20000000UL);
      001D77 E4               [12] 2266 	clr	a
      001D78 C0 E0            [24] 2267 	push	acc
      001D7A 74 2D            [12] 2268 	mov	a,#0x2d
      001D7C C0 E0            [24] 2269 	push	acc
      001D7E 74 31            [12] 2270 	mov	a,#0x31
      001D80 C0 E0            [24] 2271 	push	acc
      001D82 74 01            [12] 2272 	mov	a,#0x01
      001D84 C0 E0            [24] 2273 	push	acc
      001D86 03               [12] 2274 	rr	a
      001D87 C0 E0            [24] 2275 	push	acc
      001D89 74 25            [12] 2276 	mov	a,#0x25
      001D8B C0 E0            [24] 2277 	push	acc
      001D8D E4               [12] 2278 	clr	a
      001D8E C0 E0            [24] 2279 	push	acc
      001D90 C0 E0            [24] 2280 	push	acc
      001D92 75 82 06         [24] 2281 	mov	dpl,#0x06
      001D95 12 71 97         [24] 2282 	lcall	_uart_timer0_baud
      001D98 E5 81            [12] 2283 	mov	a,sp
      001D9A 24 F8            [12] 2284 	add	a,#0xf8
      001D9C F5 81            [12] 2285 	mov	sp,a
                                   2286 ;	..\src\COMMON\display_com0.c:35: uart0_init(0, 8, 1);
      001D9E 90 05 11         [24] 2287 	mov	dptr,#_uart0_init_PARM_2
      001DA1 74 08            [12] 2288 	mov	a,#0x08
      001DA3 F0               [24] 2289 	movx	@dptr,a
      001DA4 90 05 12         [24] 2290 	mov	dptr,#_uart0_init_PARM_3
      001DA7 74 01            [12] 2291 	mov	a,#0x01
      001DA9 F0               [24] 2292 	movx	@dptr,a
      001DAA 75 82 00         [24] 2293 	mov	dpl,#0x00
                                   2294 ;	..\src\COMMON\display_com0.c:36: }
      001DAD 02 83 F0         [24] 2295 	ljmp	_uart0_init
                                   2296 ;------------------------------------------------------------
                                   2297 ;Allocation info for local variables in function 'com0_init'
                                   2298 ;------------------------------------------------------------
                                   2299 ;	..\src\COMMON\display_com0.c:38: __reentrantb void com0_init(void) __reentrant
                                   2300 ;	-----------------------------------------
                                   2301 ;	 function com0_init
                                   2302 ;	-----------------------------------------
      001DB0                       2303 _com0_init:
                                   2304 ;	..\src\COMMON\display_com0.c:40: uart_timer0_baud(CLKSRC_SYSCLK, 9600, 20000000UL);
      001DB0 E4               [12] 2305 	clr	a
      001DB1 C0 E0            [24] 2306 	push	acc
      001DB3 74 2D            [12] 2307 	mov	a,#0x2d
      001DB5 C0 E0            [24] 2308 	push	acc
      001DB7 74 31            [12] 2309 	mov	a,#0x31
      001DB9 C0 E0            [24] 2310 	push	acc
      001DBB 74 01            [12] 2311 	mov	a,#0x01
      001DBD C0 E0            [24] 2312 	push	acc
      001DBF 03               [12] 2313 	rr	a
      001DC0 C0 E0            [24] 2314 	push	acc
      001DC2 74 25            [12] 2315 	mov	a,#0x25
      001DC4 C0 E0            [24] 2316 	push	acc
      001DC6 E4               [12] 2317 	clr	a
      001DC7 C0 E0            [24] 2318 	push	acc
      001DC9 C0 E0            [24] 2319 	push	acc
      001DCB 75 82 06         [24] 2320 	mov	dpl,#0x06
      001DCE 12 71 97         [24] 2321 	lcall	_uart_timer0_baud
      001DD1 E5 81            [12] 2322 	mov	a,sp
      001DD3 24 F8            [12] 2323 	add	a,#0xf8
      001DD5 F5 81            [12] 2324 	mov	sp,a
                                   2325 ;	..\src\COMMON\display_com0.c:41: uart0_init(0, 8, 1);
      001DD7 90 05 11         [24] 2326 	mov	dptr,#_uart0_init_PARM_2
      001DDA 74 08            [12] 2327 	mov	a,#0x08
      001DDC F0               [24] 2328 	movx	@dptr,a
      001DDD 90 05 12         [24] 2329 	mov	dptr,#_uart0_init_PARM_3
      001DE0 74 01            [12] 2330 	mov	a,#0x01
      001DE2 F0               [24] 2331 	movx	@dptr,a
      001DE3 75 82 00         [24] 2332 	mov	dpl,#0x00
      001DE6 12 83 F0         [24] 2333 	lcall	_uart0_init
                                   2334 ;	..\src\COMMON\display_com0.c:43: com0_writestr(lcd_border);
      001DE9 90 91 41         [24] 2335 	mov	dptr,#_lcd_border
      001DEC 75 F0 80         [24] 2336 	mov	b,#0x80
      001DEF 12 1E 30         [24] 2337 	lcall	_com0_writestr
                                   2338 ;	..\src\COMMON\display_com0.c:44: com0_setpos(0);
      001DF2 75 82 00         [24] 2339 	mov	dpl,#0x00
                                   2340 ;	..\src\COMMON\display_com0.c:45: }
      001DF5 02 1E 15         [24] 2341 	ljmp	_com0_setpos
                                   2342 ;------------------------------------------------------------
                                   2343 ;Allocation info for local variables in function 'com0_writestr2'
                                   2344 ;------------------------------------------------------------
                                   2345 ;msg                       Allocated to registers r5 r6 r7 
                                   2346 ;------------------------------------------------------------
                                   2347 ;	..\src\COMMON\display_com0.c:47: __reentrantb void com0_writestr2(const char* msg)  __reentrant
                                   2348 ;	-----------------------------------------
                                   2349 ;	 function com0_writestr2
                                   2350 ;	-----------------------------------------
      001DF8                       2351 _com0_writestr2:
                                   2352 ;	..\src\COMMON\display_com0.c:49: uart0_writestr(msg);
                                   2353 ;	..\src\COMMON\display_com0.c:50: }
      001DF8 02 87 34         [24] 2354 	ljmp	_uart0_writestr
                                   2355 ;------------------------------------------------------------
                                   2356 ;Allocation info for local variables in function 'com0_setrowcol'
                                   2357 ;------------------------------------------------------------
                                   2358 ;	..\src\COMMON\display_com0.c:52: static __reentrantb void com0_setrowcol(void) __reentrant
                                   2359 ;	-----------------------------------------
                                   2360 ;	 function com0_setrowcol
                                   2361 ;	-----------------------------------------
      001DFB                       2362 _com0_setrowcol:
                                   2363 ;	..\src\COMMON\display_com0.c:61: }
      001DFB 22               [24] 2364 	ret
                                   2365 ;------------------------------------------------------------
                                   2366 ;Allocation info for local variables in function 'com0_newline'
                                   2367 ;------------------------------------------------------------
                                   2368 ;	..\src\COMMON\display_com0.c:63: __reentrantb void com0_newline(void) __reentrant
                                   2369 ;	-----------------------------------------
                                   2370 ;	 function com0_newline
                                   2371 ;	-----------------------------------------
      001DFC                       2372 _com0_newline:
                                   2373 ;	..\src\COMMON\display_com0.c:65: if (row < 3)
      001DFC 90 00 E8         [24] 2374 	mov	dptr,#_row
      001DFF E0               [24] 2375 	movx	a,@dptr
      001E00 FF               [12] 2376 	mov	r7,a
      001E01 BF 03 00         [24] 2377 	cjne	r7,#0x03,00109$
      001E04                       2378 00109$:
      001E04 50 06            [24] 2379 	jnc	00102$
                                   2380 ;	..\src\COMMON\display_com0.c:66: ++row;
      001E06 90 00 E8         [24] 2381 	mov	dptr,#_row
      001E09 EF               [12] 2382 	mov	a,r7
      001E0A 04               [12] 2383 	inc	a
      001E0B F0               [24] 2384 	movx	@dptr,a
      001E0C                       2385 00102$:
                                   2386 ;	..\src\COMMON\display_com0.c:67: column = 2;
      001E0C 90 00 E9         [24] 2387 	mov	dptr,#_column
      001E0F 74 02            [12] 2388 	mov	a,#0x02
      001E11 F0               [24] 2389 	movx	@dptr,a
                                   2390 ;	..\src\COMMON\display_com0.c:68: com0_setrowcol();
                                   2391 ;	..\src\COMMON\display_com0.c:69: }
      001E12 02 1D FB         [24] 2392 	ljmp	_com0_setrowcol
                                   2393 ;------------------------------------------------------------
                                   2394 ;Allocation info for local variables in function 'com0_setpos'
                                   2395 ;------------------------------------------------------------
                                   2396 ;v                         Allocated to registers r7 
                                   2397 ;------------------------------------------------------------
                                   2398 ;	..\src\COMMON\display_com0.c:71: __reentrantb void com0_setpos(uint8_t v) __reentrant
                                   2399 ;	-----------------------------------------
                                   2400 ;	 function com0_setpos
                                   2401 ;	-----------------------------------------
      001E15                       2402 _com0_setpos:
                                   2403 ;	..\src\COMMON\display_com0.c:75: row = (v >> 6) + 2;
      001E15 E5 82            [12] 2404 	mov	a,dpl
      001E17 FF               [12] 2405 	mov	r7,a
      001E18 23               [12] 2406 	rl	a
      001E19 23               [12] 2407 	rl	a
      001E1A 54 03            [12] 2408 	anl	a,#0x03
      001E1C FE               [12] 2409 	mov	r6,a
      001E1D 90 00 E8         [24] 2410 	mov	dptr,#_row
      001E20 74 02            [12] 2411 	mov	a,#0x02
      001E22 2E               [12] 2412 	add	a,r6
      001E23 F0               [24] 2413 	movx	@dptr,a
                                   2414 ;	..\src\COMMON\display_com0.c:76: column = (v & 0x3F) + 2;
      001E24 74 3F            [12] 2415 	mov	a,#0x3f
      001E26 5F               [12] 2416 	anl	a,r7
      001E27 90 00 E9         [24] 2417 	mov	dptr,#_column
      001E2A 24 02            [12] 2418 	add	a,#0x02
      001E2C F0               [24] 2419 	movx	@dptr,a
                                   2420 ;	..\src\COMMON\display_com0.c:77: com0_setrowcol();
                                   2421 ;	..\src\COMMON\display_com0.c:78: }
      001E2D 02 1D FB         [24] 2422 	ljmp	_com0_setrowcol
                                   2423 ;------------------------------------------------------------
                                   2424 ;Allocation info for local variables in function 'com0_writestr'
                                   2425 ;------------------------------------------------------------
                                   2426 ;msg                       Allocated to registers 
                                   2427 ;ch                        Allocated to registers 
                                   2428 ;------------------------------------------------------------
                                   2429 ;	..\src\COMMON\display_com0.c:80: __reentrantb void com0_writestr(const char __generic *msg) __reentrant
                                   2430 ;	-----------------------------------------
                                   2431 ;	 function com0_writestr
                                   2432 ;	-----------------------------------------
      001E30                       2433 _com0_writestr:
      001E30 AD 82            [24] 2434 	mov	r5,dpl
      001E32 AE 83            [24] 2435 	mov	r6,dph
      001E34 AF F0            [24] 2436 	mov	r7,b
      001E36                       2437 00104$:
                                   2438 ;	..\src\COMMON\display_com0.c:83: char ch = *msg;
      001E36 8D 82            [24] 2439 	mov	dpl,r5
      001E38 8E 83            [24] 2440 	mov	dph,r6
      001E3A 8F F0            [24] 2441 	mov	b,r7
      001E3C 12 8D 8E         [24] 2442 	lcall	__gptrget
      001E3F 60 07            [24] 2443 	jz	00106$
                                   2444 ;	..\src\COMMON\display_com0.c:84: if (!ch)
                                   2445 ;	..\src\COMMON\display_com0.c:87: msg++;
      001E41 0D               [12] 2446 	inc	r5
      001E42 BD 00 F1         [24] 2447 	cjne	r5,#0x00,00104$
      001E45 0E               [12] 2448 	inc	r6
      001E46 80 EE            [24] 2449 	sjmp	00104$
      001E48                       2450 00106$:
                                   2451 ;	..\src\COMMON\display_com0.c:90: }
      001E48 22               [24] 2452 	ret
                                   2453 ;------------------------------------------------------------
                                   2454 ;Allocation info for local variables in function 'com0_tx'
                                   2455 ;------------------------------------------------------------
                                   2456 ;val                       Allocated to registers r7 
                                   2457 ;------------------------------------------------------------
                                   2458 ;	..\src\COMMON\display_com0.c:92: __reentrantb void com0_tx(uint8_t val)  __reentrant
                                   2459 ;	-----------------------------------------
                                   2460 ;	 function com0_tx
                                   2461 ;	-----------------------------------------
      001E49                       2462 _com0_tx:
      001E49 AF 82            [24] 2463 	mov	r7,dpl
                                   2464 ;	..\src\COMMON\display_com0.c:94: if (val == '\n')
      001E4B BF 0A 03         [24] 2465 	cjne	r7,#0x0a,00103$
                                   2466 ;	..\src\COMMON\display_com0.c:95: com0_newline();
                                   2467 ;	..\src\COMMON\display_com0.c:98: }
      001E4E 02 1D FC         [24] 2468 	ljmp	_com0_newline
      001E51                       2469 00103$:
      001E51 22               [24] 2470 	ret
                                   2471 ;------------------------------------------------------------
                                   2472 ;Allocation info for local variables in function 'com0_clear'
                                   2473 ;------------------------------------------------------------
                                   2474 ;len                       Allocated to stack - _bp -3
                                   2475 ;pos                       Allocated to registers r7 
                                   2476 ;------------------------------------------------------------
                                   2477 ;	..\src\COMMON\display_com0.c:100: __reentrantb void com0_clear(uint8_t pos, uint8_t len) __reentrant
                                   2478 ;	-----------------------------------------
                                   2479 ;	 function com0_clear
                                   2480 ;	-----------------------------------------
      001E52                       2481 _com0_clear:
      001E52 C0 1F            [24] 2482 	push	_bp
      001E54 85 81 1F         [24] 2483 	mov	_bp,sp
                                   2484 ;	..\src\COMMON\display_com0.c:102: com0_setpos(pos);
      001E57 12 1E 15         [24] 2485 	lcall	_com0_setpos
                                   2486 ;	..\src\COMMON\display_com0.c:103: if (!len)
      001E5A E5 1F            [12] 2487 	mov	a,_bp
      001E5C 24 FD            [12] 2488 	add	a,#0xfd
      001E5E F8               [12] 2489 	mov	r0,a
      001E5F E6               [12] 2490 	mov	a,@r0
      001E60 70 02            [24] 2491 	jnz	00108$
                                   2492 ;	..\src\COMMON\display_com0.c:104: return;
                                   2493 ;	..\src\COMMON\display_com0.c:105: do {
      001E62 80 09            [24] 2494 	sjmp	00105$
      001E64                       2495 00108$:
      001E64 E5 1F            [12] 2496 	mov	a,_bp
      001E66 24 FD            [12] 2497 	add	a,#0xfd
      001E68 F8               [12] 2498 	mov	r0,a
      001E69 86 07            [24] 2499 	mov	ar7,@r0
      001E6B                       2500 00103$:
                                   2501 ;	..\src\COMMON\display_com0.c:107: } while (--len);
      001E6B DF FE            [24] 2502 	djnz	r7,00103$
      001E6D                       2503 00105$:
                                   2504 ;	..\src\COMMON\display_com0.c:108: }
      001E6D D0 1F            [24] 2505 	pop	_bp
      001E6F 22               [24] 2506 	ret
                                   2507 	.area CSEG    (CODE)
                                   2508 	.area CONST   (CODE)
      009141                       2509 _lcd_border:
      009141 1B                    2510 	.db 0x1b
      009142 5B 30 3B 38 3B 34 34  2511 	.ascii "[0;8;44;30m"
             3B 33 30 6D
      00914D 1B                    2512 	.db 0x1b
      00914E 5B 31 3B 31 66 2A 2A  2513 	.ascii "[1;1f******************"
             2A 2A 2A 2A 2A 2A 2A
             2A 2A 2A 2A 2A 2A 2A
             2A 2A
      009165 1B                    2514 	.db 0x1b
      009166 5B 32 3B 31 66 2A 20  2515 	.ascii "[2;1f*                *"
             20 20 20 20 20 20 20
             20 20 20 20 20 20 20
             20 2A
      00917D 1B                    2516 	.db 0x1b
      00917E 5B 33 3B 31 66 2A 20  2517 	.ascii "[3;1f*                *"
             20 20 20 20 20 20 20
             20 20 20 20 20 20 20
             20 2A
      009195 1B                    2518 	.db 0x1b
      009196 5B 34 3B 31 66 2A 2A  2519 	.ascii "[4;1f******************"
             2A 2A 2A 2A 2A 2A 2A
             2A 2A 2A 2A 2A 2A 2A
             2A 2A
      0091AD 1B                    2520 	.db 0x1b
      0091AE 5B 35 3B 31 66        2521 	.ascii "[5;1f"
      0091B3 1B                    2522 	.db 0x1b
      0091B4 5B 33 37 6D           2523 	.ascii "[37m"
      0091B8 1B                    2524 	.db 0x1b
      0091B9 5B 3F 32 35 6C        2525 	.ascii "[?25l"
      0091BE 00                    2526 	.db 0x00
                                   2527 	.area XINIT   (CODE)
                                   2528 	.area CABS    (ABS,CODE)
