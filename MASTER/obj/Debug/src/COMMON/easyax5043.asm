;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.9.3 #11354 (MINGW32)
;--------------------------------------------------------
	.module easyax5043
	.optsdcc -mmcs51 --model-large
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _ax5043_init_registers_rx
	.globl _ax5043_init_registers_tx
	.globl _VerifyACK
	.globl _BEACON_decoding
	.globl _dbglink_writestr
	.globl _memset
	.globl _memcpy
	.globl _wtimer_remove_callback
	.globl _wtimer_add_callback
	.globl _wtimer_remove
	.globl _wtimer1_addrelative
	.globl _wtimer0_addrelative
	.globl _wtimer0_addabsolute
	.globl _wtimer0_curtime
	.globl _wtimer_runcallbacks
	.globl _wtimer_idle
	.globl _ax5043_writefifo
	.globl _ax5043_readfifo
	.globl _ax5043_wakeup_deepsleep
	.globl _ax5043_enter_deepsleep
	.globl _ax5043_reset
	.globl _radio_read24
	.globl _radio_read16
	.globl _pn9_buffer
	.globl _pn9_advance_byte
	.globl _pn9_advance_bits
	.globl _axradio_framing_append_crc
	.globl _axradio_framing_check_crc
	.globl _ax5043_set_registers_rxcont_singleparamset
	.globl _ax5043_set_registers_rxcont
	.globl _ax5043_set_registers_rxwor
	.globl _ax5043_set_registers_rx
	.globl _ax5043_set_registers_tx
	.globl _ax5043_set_registers
	.globl _axradio_conv_freq_fromreg
	.globl _axradio_statuschange
	.globl _axradio_conv_timeinterval_totimer0
	.globl _checksignedlimit32
	.globl _checksignedlimit16
	.globl _signedlimit16
	.globl _signextend24
	.globl _signextend20
	.globl _signextend16
	.globl _PORTC_7
	.globl _PORTC_6
	.globl _PORTC_5
	.globl _PORTC_4
	.globl _PORTC_3
	.globl _PORTC_2
	.globl _PORTC_1
	.globl _PORTC_0
	.globl _PORTB_7
	.globl _PORTB_6
	.globl _PORTB_5
	.globl _PORTB_4
	.globl _PORTB_3
	.globl _PORTB_2
	.globl _PORTB_1
	.globl _PORTB_0
	.globl _PORTA_7
	.globl _PORTA_6
	.globl _PORTA_5
	.globl _PORTA_4
	.globl _PORTA_3
	.globl _PORTA_2
	.globl _PORTA_1
	.globl _PORTA_0
	.globl _PINC_7
	.globl _PINC_6
	.globl _PINC_5
	.globl _PINC_4
	.globl _PINC_3
	.globl _PINC_2
	.globl _PINC_1
	.globl _PINC_0
	.globl _PINB_7
	.globl _PINB_6
	.globl _PINB_5
	.globl _PINB_4
	.globl _PINB_3
	.globl _PINB_2
	.globl _PINB_1
	.globl _PINB_0
	.globl _PINA_7
	.globl _PINA_6
	.globl _PINA_5
	.globl _PINA_4
	.globl _PINA_3
	.globl _PINA_2
	.globl _PINA_1
	.globl _PINA_0
	.globl _CY
	.globl _AC
	.globl _F0
	.globl _RS1
	.globl _RS0
	.globl _OV
	.globl _F1
	.globl _P
	.globl _IP_7
	.globl _IP_6
	.globl _IP_5
	.globl _IP_4
	.globl _IP_3
	.globl _IP_2
	.globl _IP_1
	.globl _IP_0
	.globl _EA
	.globl _IE_7
	.globl _IE_6
	.globl _IE_5
	.globl _IE_4
	.globl _IE_3
	.globl _IE_2
	.globl _IE_1
	.globl _IE_0
	.globl _EIP_7
	.globl _EIP_6
	.globl _EIP_5
	.globl _EIP_4
	.globl _EIP_3
	.globl _EIP_2
	.globl _EIP_1
	.globl _EIP_0
	.globl _EIE_7
	.globl _EIE_6
	.globl _EIE_5
	.globl _EIE_4
	.globl _EIE_3
	.globl _EIE_2
	.globl _EIE_1
	.globl _EIE_0
	.globl _E2IP_7
	.globl _E2IP_6
	.globl _E2IP_5
	.globl _E2IP_4
	.globl _E2IP_3
	.globl _E2IP_2
	.globl _E2IP_1
	.globl _E2IP_0
	.globl _E2IE_7
	.globl _E2IE_6
	.globl _E2IE_5
	.globl _E2IE_4
	.globl _E2IE_3
	.globl _E2IE_2
	.globl _E2IE_1
	.globl _E2IE_0
	.globl _B_7
	.globl _B_6
	.globl _B_5
	.globl _B_4
	.globl _B_3
	.globl _B_2
	.globl _B_1
	.globl _B_0
	.globl _ACC_7
	.globl _ACC_6
	.globl _ACC_5
	.globl _ACC_4
	.globl _ACC_3
	.globl _ACC_2
	.globl _ACC_1
	.globl _ACC_0
	.globl _WTSTAT
	.globl _WTIRQEN
	.globl _WTEVTD
	.globl _WTEVTD1
	.globl _WTEVTD0
	.globl _WTEVTC
	.globl _WTEVTC1
	.globl _WTEVTC0
	.globl _WTEVTB
	.globl _WTEVTB1
	.globl _WTEVTB0
	.globl _WTEVTA
	.globl _WTEVTA1
	.globl _WTEVTA0
	.globl _WTCNTR1
	.globl _WTCNTB
	.globl _WTCNTB1
	.globl _WTCNTB0
	.globl _WTCNTA
	.globl _WTCNTA1
	.globl _WTCNTA0
	.globl _WTCFGB
	.globl _WTCFGA
	.globl _WDTRESET
	.globl _WDTCFG
	.globl _U1STATUS
	.globl _U1SHREG
	.globl _U1MODE
	.globl _U1CTRL
	.globl _U0STATUS
	.globl _U0SHREG
	.globl _U0MODE
	.globl _U0CTRL
	.globl _T2STATUS
	.globl _T2PERIOD
	.globl _T2PERIOD1
	.globl _T2PERIOD0
	.globl _T2MODE
	.globl _T2CNT
	.globl _T2CNT1
	.globl _T2CNT0
	.globl _T2CLKSRC
	.globl _T1STATUS
	.globl _T1PERIOD
	.globl _T1PERIOD1
	.globl _T1PERIOD0
	.globl _T1MODE
	.globl _T1CNT
	.globl _T1CNT1
	.globl _T1CNT0
	.globl _T1CLKSRC
	.globl _T0STATUS
	.globl _T0PERIOD
	.globl _T0PERIOD1
	.globl _T0PERIOD0
	.globl _T0MODE
	.globl _T0CNT
	.globl _T0CNT1
	.globl _T0CNT0
	.globl _T0CLKSRC
	.globl _SPSTATUS
	.globl _SPSHREG
	.globl _SPMODE
	.globl _SPCLKSRC
	.globl _RADIOSTAT
	.globl _RADIOSTAT1
	.globl _RADIOSTAT0
	.globl _RADIODATA
	.globl _RADIODATA3
	.globl _RADIODATA2
	.globl _RADIODATA1
	.globl _RADIODATA0
	.globl _RADIOADDR
	.globl _RADIOADDR1
	.globl _RADIOADDR0
	.globl _RADIOACC
	.globl _OC1STATUS
	.globl _OC1PIN
	.globl _OC1MODE
	.globl _OC1COMP
	.globl _OC1COMP1
	.globl _OC1COMP0
	.globl _OC0STATUS
	.globl _OC0PIN
	.globl _OC0MODE
	.globl _OC0COMP
	.globl _OC0COMP1
	.globl _OC0COMP0
	.globl _NVSTATUS
	.globl _NVKEY
	.globl _NVDATA
	.globl _NVDATA1
	.globl _NVDATA0
	.globl _NVADDR
	.globl _NVADDR1
	.globl _NVADDR0
	.globl _IC1STATUS
	.globl _IC1MODE
	.globl _IC1CAPT
	.globl _IC1CAPT1
	.globl _IC1CAPT0
	.globl _IC0STATUS
	.globl _IC0MODE
	.globl _IC0CAPT
	.globl _IC0CAPT1
	.globl _IC0CAPT0
	.globl _PORTR
	.globl _PORTC
	.globl _PORTB
	.globl _PORTA
	.globl _PINR
	.globl _PINC
	.globl _PINB
	.globl _PINA
	.globl _DIRR
	.globl _DIRC
	.globl _DIRB
	.globl _DIRA
	.globl _DBGLNKSTAT
	.globl _DBGLNKBUF
	.globl _CODECONFIG
	.globl _CLKSTAT
	.globl _CLKCON
	.globl _ANALOGCOMP
	.globl _ADCCONV
	.globl _ADCCLKSRC
	.globl _ADCCH3CONFIG
	.globl _ADCCH2CONFIG
	.globl _ADCCH1CONFIG
	.globl _ADCCH0CONFIG
	.globl __XPAGE
	.globl _XPAGE
	.globl _SP
	.globl _PSW
	.globl _PCON
	.globl _IP
	.globl _IE
	.globl _EIP
	.globl _EIE
	.globl _E2IP
	.globl _E2IE
	.globl _DPS
	.globl _DPTR1
	.globl _DPTR0
	.globl _DPL1
	.globl _DPL
	.globl _DPH1
	.globl _DPH
	.globl _B
	.globl _ACC
	.globl _f33_saved
	.globl _f32_saved
	.globl _f31_saved
	.globl _f30_saved
	.globl _axradio_transmit_PARM_3
	.globl _axradio_transmit_PARM_2
	.globl _axradio_timer
	.globl _axradio_cb_transmitdata
	.globl _axradio_cb_transmitend
	.globl _axradio_cb_transmitstart
	.globl _axradio_cb_channelstate
	.globl _axradio_cb_receivesfd
	.globl _axradio_cb_receive
	.globl _axradio_rxbuffer
	.globl _axradio_txbuffer
	.globl _axradio_default_remoteaddr
	.globl _axradio_localaddr
	.globl _axradio_timeanchor
	.globl _axradio_sync_periodcorr
	.globl _axradio_sync_time
	.globl _axradio_ack_seqnr
	.globl _axradio_ack_count
	.globl _axradio_curfreqoffset
	.globl _axradio_curchannel
	.globl _axradio_txbuffer_cnt
	.globl _axradio_txbuffer_len
	.globl _axradio_syncstate
	.globl _AX5043_XTALAMPL
	.globl _AX5043_XTALOSC
	.globl _AX5043_MODCFGP
	.globl _AX5043_POWCTRL1
	.globl _AX5043_REF
	.globl _AX5043_0xF44
	.globl _AX5043_0xF35
	.globl _AX5043_0xF34
	.globl _AX5043_0xF33
	.globl _AX5043_0xF32
	.globl _AX5043_0xF31
	.globl _AX5043_0xF30
	.globl _AX5043_0xF26
	.globl _AX5043_0xF23
	.globl _AX5043_0xF22
	.globl _AX5043_0xF21
	.globl _AX5043_0xF1C
	.globl _AX5043_0xF18
	.globl _AX5043_0xF11
	.globl _AX5043_0xF10
	.globl _AX5043_0xF0C
	.globl _AX5043_0xF00
	.globl _AX5043_TIMEGAIN3NB
	.globl _AX5043_TIMEGAIN2NB
	.globl _AX5043_TIMEGAIN1NB
	.globl _AX5043_TIMEGAIN0NB
	.globl _AX5043_RXPARAMSETSNB
	.globl _AX5043_RXPARAMCURSETNB
	.globl _AX5043_PKTMAXLENNB
	.globl _AX5043_PKTLENOFFSETNB
	.globl _AX5043_PKTLENCFGNB
	.globl _AX5043_PKTADDRMASK3NB
	.globl _AX5043_PKTADDRMASK2NB
	.globl _AX5043_PKTADDRMASK1NB
	.globl _AX5043_PKTADDRMASK0NB
	.globl _AX5043_PKTADDRCFGNB
	.globl _AX5043_PKTADDR3NB
	.globl _AX5043_PKTADDR2NB
	.globl _AX5043_PKTADDR1NB
	.globl _AX5043_PKTADDR0NB
	.globl _AX5043_PHASEGAIN3NB
	.globl _AX5043_PHASEGAIN2NB
	.globl _AX5043_PHASEGAIN1NB
	.globl _AX5043_PHASEGAIN0NB
	.globl _AX5043_FREQUENCYLEAKNB
	.globl _AX5043_FREQUENCYGAIND3NB
	.globl _AX5043_FREQUENCYGAIND2NB
	.globl _AX5043_FREQUENCYGAIND1NB
	.globl _AX5043_FREQUENCYGAIND0NB
	.globl _AX5043_FREQUENCYGAINC3NB
	.globl _AX5043_FREQUENCYGAINC2NB
	.globl _AX5043_FREQUENCYGAINC1NB
	.globl _AX5043_FREQUENCYGAINC0NB
	.globl _AX5043_FREQUENCYGAINB3NB
	.globl _AX5043_FREQUENCYGAINB2NB
	.globl _AX5043_FREQUENCYGAINB1NB
	.globl _AX5043_FREQUENCYGAINB0NB
	.globl _AX5043_FREQUENCYGAINA3NB
	.globl _AX5043_FREQUENCYGAINA2NB
	.globl _AX5043_FREQUENCYGAINA1NB
	.globl _AX5043_FREQUENCYGAINA0NB
	.globl _AX5043_FREQDEV13NB
	.globl _AX5043_FREQDEV12NB
	.globl _AX5043_FREQDEV11NB
	.globl _AX5043_FREQDEV10NB
	.globl _AX5043_FREQDEV03NB
	.globl _AX5043_FREQDEV02NB
	.globl _AX5043_FREQDEV01NB
	.globl _AX5043_FREQDEV00NB
	.globl _AX5043_FOURFSK3NB
	.globl _AX5043_FOURFSK2NB
	.globl _AX5043_FOURFSK1NB
	.globl _AX5043_FOURFSK0NB
	.globl _AX5043_DRGAIN3NB
	.globl _AX5043_DRGAIN2NB
	.globl _AX5043_DRGAIN1NB
	.globl _AX5043_DRGAIN0NB
	.globl _AX5043_BBOFFSRES3NB
	.globl _AX5043_BBOFFSRES2NB
	.globl _AX5043_BBOFFSRES1NB
	.globl _AX5043_BBOFFSRES0NB
	.globl _AX5043_AMPLITUDEGAIN3NB
	.globl _AX5043_AMPLITUDEGAIN2NB
	.globl _AX5043_AMPLITUDEGAIN1NB
	.globl _AX5043_AMPLITUDEGAIN0NB
	.globl _AX5043_AGCTARGET3NB
	.globl _AX5043_AGCTARGET2NB
	.globl _AX5043_AGCTARGET1NB
	.globl _AX5043_AGCTARGET0NB
	.globl _AX5043_AGCMINMAX3NB
	.globl _AX5043_AGCMINMAX2NB
	.globl _AX5043_AGCMINMAX1NB
	.globl _AX5043_AGCMINMAX0NB
	.globl _AX5043_AGCGAIN3NB
	.globl _AX5043_AGCGAIN2NB
	.globl _AX5043_AGCGAIN1NB
	.globl _AX5043_AGCGAIN0NB
	.globl _AX5043_AGCAHYST3NB
	.globl _AX5043_AGCAHYST2NB
	.globl _AX5043_AGCAHYST1NB
	.globl _AX5043_AGCAHYST0NB
	.globl _AX5043_0xF44NB
	.globl _AX5043_0xF35NB
	.globl _AX5043_0xF34NB
	.globl _AX5043_0xF33NB
	.globl _AX5043_0xF32NB
	.globl _AX5043_0xF31NB
	.globl _AX5043_0xF30NB
	.globl _AX5043_0xF26NB
	.globl _AX5043_0xF23NB
	.globl _AX5043_0xF22NB
	.globl _AX5043_0xF21NB
	.globl _AX5043_0xF1CNB
	.globl _AX5043_0xF18NB
	.globl _AX5043_0xF0CNB
	.globl _AX5043_0xF00NB
	.globl _AX5043_XTALSTATUSNB
	.globl _AX5043_XTALOSCNB
	.globl _AX5043_XTALCAPNB
	.globl _AX5043_XTALAMPLNB
	.globl _AX5043_WAKEUPXOEARLYNB
	.globl _AX5043_WAKEUPTIMER1NB
	.globl _AX5043_WAKEUPTIMER0NB
	.globl _AX5043_WAKEUPFREQ1NB
	.globl _AX5043_WAKEUPFREQ0NB
	.globl _AX5043_WAKEUP1NB
	.globl _AX5043_WAKEUP0NB
	.globl _AX5043_TXRATE2NB
	.globl _AX5043_TXRATE1NB
	.globl _AX5043_TXRATE0NB
	.globl _AX5043_TXPWRCOEFFE1NB
	.globl _AX5043_TXPWRCOEFFE0NB
	.globl _AX5043_TXPWRCOEFFD1NB
	.globl _AX5043_TXPWRCOEFFD0NB
	.globl _AX5043_TXPWRCOEFFC1NB
	.globl _AX5043_TXPWRCOEFFC0NB
	.globl _AX5043_TXPWRCOEFFB1NB
	.globl _AX5043_TXPWRCOEFFB0NB
	.globl _AX5043_TXPWRCOEFFA1NB
	.globl _AX5043_TXPWRCOEFFA0NB
	.globl _AX5043_TRKRFFREQ2NB
	.globl _AX5043_TRKRFFREQ1NB
	.globl _AX5043_TRKRFFREQ0NB
	.globl _AX5043_TRKPHASE1NB
	.globl _AX5043_TRKPHASE0NB
	.globl _AX5043_TRKFSKDEMOD1NB
	.globl _AX5043_TRKFSKDEMOD0NB
	.globl _AX5043_TRKFREQ1NB
	.globl _AX5043_TRKFREQ0NB
	.globl _AX5043_TRKDATARATE2NB
	.globl _AX5043_TRKDATARATE1NB
	.globl _AX5043_TRKDATARATE0NB
	.globl _AX5043_TRKAMPLITUDE1NB
	.globl _AX5043_TRKAMPLITUDE0NB
	.globl _AX5043_TRKAFSKDEMOD1NB
	.globl _AX5043_TRKAFSKDEMOD0NB
	.globl _AX5043_TMGTXSETTLENB
	.globl _AX5043_TMGTXBOOSTNB
	.globl _AX5043_TMGRXSETTLENB
	.globl _AX5043_TMGRXRSSINB
	.globl _AX5043_TMGRXPREAMBLE3NB
	.globl _AX5043_TMGRXPREAMBLE2NB
	.globl _AX5043_TMGRXPREAMBLE1NB
	.globl _AX5043_TMGRXOFFSACQNB
	.globl _AX5043_TMGRXCOARSEAGCNB
	.globl _AX5043_TMGRXBOOSTNB
	.globl _AX5043_TMGRXAGCNB
	.globl _AX5043_TIMER2NB
	.globl _AX5043_TIMER1NB
	.globl _AX5043_TIMER0NB
	.globl _AX5043_SILICONREVISIONNB
	.globl _AX5043_SCRATCHNB
	.globl _AX5043_RXDATARATE2NB
	.globl _AX5043_RXDATARATE1NB
	.globl _AX5043_RXDATARATE0NB
	.globl _AX5043_RSSIREFERENCENB
	.globl _AX5043_RSSIABSTHRNB
	.globl _AX5043_RSSINB
	.globl _AX5043_REFNB
	.globl _AX5043_RADIOSTATENB
	.globl _AX5043_RADIOEVENTREQ1NB
	.globl _AX5043_RADIOEVENTREQ0NB
	.globl _AX5043_RADIOEVENTMASK1NB
	.globl _AX5043_RADIOEVENTMASK0NB
	.globl _AX5043_PWRMODENB
	.globl _AX5043_PWRAMPNB
	.globl _AX5043_POWSTICKYSTATNB
	.globl _AX5043_POWSTATNB
	.globl _AX5043_POWIRQMASKNB
	.globl _AX5043_POWCTRL1NB
	.globl _AX5043_PLLVCOIRNB
	.globl _AX5043_PLLVCOINB
	.globl _AX5043_PLLVCODIVNB
	.globl _AX5043_PLLRNGCLKNB
	.globl _AX5043_PLLRANGINGBNB
	.globl _AX5043_PLLRANGINGANB
	.globl _AX5043_PLLLOOPBOOSTNB
	.globl _AX5043_PLLLOOPNB
	.globl _AX5043_PLLLOCKDETNB
	.globl _AX5043_PLLCPIBOOSTNB
	.globl _AX5043_PLLCPINB
	.globl _AX5043_PKTSTOREFLAGSNB
	.globl _AX5043_PKTMISCFLAGSNB
	.globl _AX5043_PKTCHUNKSIZENB
	.globl _AX5043_PKTACCEPTFLAGSNB
	.globl _AX5043_PINSTATENB
	.globl _AX5043_PINFUNCSYSCLKNB
	.globl _AX5043_PINFUNCPWRAMPNB
	.globl _AX5043_PINFUNCIRQNB
	.globl _AX5043_PINFUNCDCLKNB
	.globl _AX5043_PINFUNCDATANB
	.globl _AX5043_PINFUNCANTSELNB
	.globl _AX5043_MODULATIONNB
	.globl _AX5043_MODCFGPNB
	.globl _AX5043_MODCFGFNB
	.globl _AX5043_MODCFGANB
	.globl _AX5043_MAXRFOFFSET2NB
	.globl _AX5043_MAXRFOFFSET1NB
	.globl _AX5043_MAXRFOFFSET0NB
	.globl _AX5043_MAXDROFFSET2NB
	.globl _AX5043_MAXDROFFSET1NB
	.globl _AX5043_MAXDROFFSET0NB
	.globl _AX5043_MATCH1PAT1NB
	.globl _AX5043_MATCH1PAT0NB
	.globl _AX5043_MATCH1MINNB
	.globl _AX5043_MATCH1MAXNB
	.globl _AX5043_MATCH1LENNB
	.globl _AX5043_MATCH0PAT3NB
	.globl _AX5043_MATCH0PAT2NB
	.globl _AX5043_MATCH0PAT1NB
	.globl _AX5043_MATCH0PAT0NB
	.globl _AX5043_MATCH0MINNB
	.globl _AX5043_MATCH0MAXNB
	.globl _AX5043_MATCH0LENNB
	.globl _AX5043_LPOSCSTATUSNB
	.globl _AX5043_LPOSCREF1NB
	.globl _AX5043_LPOSCREF0NB
	.globl _AX5043_LPOSCPER1NB
	.globl _AX5043_LPOSCPER0NB
	.globl _AX5043_LPOSCKFILT1NB
	.globl _AX5043_LPOSCKFILT0NB
	.globl _AX5043_LPOSCFREQ1NB
	.globl _AX5043_LPOSCFREQ0NB
	.globl _AX5043_LPOSCCONFIGNB
	.globl _AX5043_IRQREQUEST1NB
	.globl _AX5043_IRQREQUEST0NB
	.globl _AX5043_IRQMASK1NB
	.globl _AX5043_IRQMASK0NB
	.globl _AX5043_IRQINVERSION1NB
	.globl _AX5043_IRQINVERSION0NB
	.globl _AX5043_IFFREQ1NB
	.globl _AX5043_IFFREQ0NB
	.globl _AX5043_GPADCPERIODNB
	.globl _AX5043_GPADCCTRLNB
	.globl _AX5043_GPADC13VALUE1NB
	.globl _AX5043_GPADC13VALUE0NB
	.globl _AX5043_FSKDMIN1NB
	.globl _AX5043_FSKDMIN0NB
	.globl _AX5043_FSKDMAX1NB
	.globl _AX5043_FSKDMAX0NB
	.globl _AX5043_FSKDEV2NB
	.globl _AX5043_FSKDEV1NB
	.globl _AX5043_FSKDEV0NB
	.globl _AX5043_FREQB3NB
	.globl _AX5043_FREQB2NB
	.globl _AX5043_FREQB1NB
	.globl _AX5043_FREQB0NB
	.globl _AX5043_FREQA3NB
	.globl _AX5043_FREQA2NB
	.globl _AX5043_FREQA1NB
	.globl _AX5043_FREQA0NB
	.globl _AX5043_FRAMINGNB
	.globl _AX5043_FIFOTHRESH1NB
	.globl _AX5043_FIFOTHRESH0NB
	.globl _AX5043_FIFOSTATNB
	.globl _AX5043_FIFOFREE1NB
	.globl _AX5043_FIFOFREE0NB
	.globl _AX5043_FIFODATANB
	.globl _AX5043_FIFOCOUNT1NB
	.globl _AX5043_FIFOCOUNT0NB
	.globl _AX5043_FECSYNCNB
	.globl _AX5043_FECSTATUSNB
	.globl _AX5043_FECNB
	.globl _AX5043_ENCODINGNB
	.globl _AX5043_DIVERSITYNB
	.globl _AX5043_DECIMATIONNB
	.globl _AX5043_DACVALUE1NB
	.globl _AX5043_DACVALUE0NB
	.globl _AX5043_DACCONFIGNB
	.globl _AX5043_CRCINIT3NB
	.globl _AX5043_CRCINIT2NB
	.globl _AX5043_CRCINIT1NB
	.globl _AX5043_CRCINIT0NB
	.globl _AX5043_BGNDRSSITHRNB
	.globl _AX5043_BGNDRSSIGAINNB
	.globl _AX5043_BGNDRSSINB
	.globl _AX5043_BBTUNENB
	.globl _AX5043_BBOFFSCAPNB
	.globl _AX5043_AMPLFILTERNB
	.globl _AX5043_AGCCOUNTERNB
	.globl _AX5043_AFSKSPACE1NB
	.globl _AX5043_AFSKSPACE0NB
	.globl _AX5043_AFSKMARK1NB
	.globl _AX5043_AFSKMARK0NB
	.globl _AX5043_AFSKCTRLNB
	.globl _AX5043_TIMEGAIN3
	.globl _AX5043_TIMEGAIN2
	.globl _AX5043_TIMEGAIN1
	.globl _AX5043_TIMEGAIN0
	.globl _AX5043_RXPARAMSETS
	.globl _AX5043_RXPARAMCURSET
	.globl _AX5043_PKTMAXLEN
	.globl _AX5043_PKTLENOFFSET
	.globl _AX5043_PKTLENCFG
	.globl _AX5043_PKTADDRMASK3
	.globl _AX5043_PKTADDRMASK2
	.globl _AX5043_PKTADDRMASK1
	.globl _AX5043_PKTADDRMASK0
	.globl _AX5043_PKTADDRCFG
	.globl _AX5043_PKTADDR3
	.globl _AX5043_PKTADDR2
	.globl _AX5043_PKTADDR1
	.globl _AX5043_PKTADDR0
	.globl _AX5043_PHASEGAIN3
	.globl _AX5043_PHASEGAIN2
	.globl _AX5043_PHASEGAIN1
	.globl _AX5043_PHASEGAIN0
	.globl _AX5043_FREQUENCYLEAK
	.globl _AX5043_FREQUENCYGAIND3
	.globl _AX5043_FREQUENCYGAIND2
	.globl _AX5043_FREQUENCYGAIND1
	.globl _AX5043_FREQUENCYGAIND0
	.globl _AX5043_FREQUENCYGAINC3
	.globl _AX5043_FREQUENCYGAINC2
	.globl _AX5043_FREQUENCYGAINC1
	.globl _AX5043_FREQUENCYGAINC0
	.globl _AX5043_FREQUENCYGAINB3
	.globl _AX5043_FREQUENCYGAINB2
	.globl _AX5043_FREQUENCYGAINB1
	.globl _AX5043_FREQUENCYGAINB0
	.globl _AX5043_FREQUENCYGAINA3
	.globl _AX5043_FREQUENCYGAINA2
	.globl _AX5043_FREQUENCYGAINA1
	.globl _AX5043_FREQUENCYGAINA0
	.globl _AX5043_FREQDEV13
	.globl _AX5043_FREQDEV12
	.globl _AX5043_FREQDEV11
	.globl _AX5043_FREQDEV10
	.globl _AX5043_FREQDEV03
	.globl _AX5043_FREQDEV02
	.globl _AX5043_FREQDEV01
	.globl _AX5043_FREQDEV00
	.globl _AX5043_FOURFSK3
	.globl _AX5043_FOURFSK2
	.globl _AX5043_FOURFSK1
	.globl _AX5043_FOURFSK0
	.globl _AX5043_DRGAIN3
	.globl _AX5043_DRGAIN2
	.globl _AX5043_DRGAIN1
	.globl _AX5043_DRGAIN0
	.globl _AX5043_BBOFFSRES3
	.globl _AX5043_BBOFFSRES2
	.globl _AX5043_BBOFFSRES1
	.globl _AX5043_BBOFFSRES0
	.globl _AX5043_AMPLITUDEGAIN3
	.globl _AX5043_AMPLITUDEGAIN2
	.globl _AX5043_AMPLITUDEGAIN1
	.globl _AX5043_AMPLITUDEGAIN0
	.globl _AX5043_AGCTARGET3
	.globl _AX5043_AGCTARGET2
	.globl _AX5043_AGCTARGET1
	.globl _AX5043_AGCTARGET0
	.globl _AX5043_AGCMINMAX3
	.globl _AX5043_AGCMINMAX2
	.globl _AX5043_AGCMINMAX1
	.globl _AX5043_AGCMINMAX0
	.globl _AX5043_AGCGAIN3
	.globl _AX5043_AGCGAIN2
	.globl _AX5043_AGCGAIN1
	.globl _AX5043_AGCGAIN0
	.globl _AX5043_AGCAHYST3
	.globl _AX5043_AGCAHYST2
	.globl _AX5043_AGCAHYST1
	.globl _AX5043_AGCAHYST0
	.globl _AX5043_XTALSTATUS
	.globl _AX5043_XTALCAP
	.globl _AX5043_WAKEUPXOEARLY
	.globl _AX5043_WAKEUPTIMER1
	.globl _AX5043_WAKEUPTIMER0
	.globl _AX5043_WAKEUPFREQ1
	.globl _AX5043_WAKEUPFREQ0
	.globl _AX5043_WAKEUP1
	.globl _AX5043_WAKEUP0
	.globl _AX5043_TXRATE2
	.globl _AX5043_TXRATE1
	.globl _AX5043_TXRATE0
	.globl _AX5043_TXPWRCOEFFE1
	.globl _AX5043_TXPWRCOEFFE0
	.globl _AX5043_TXPWRCOEFFD1
	.globl _AX5043_TXPWRCOEFFD0
	.globl _AX5043_TXPWRCOEFFC1
	.globl _AX5043_TXPWRCOEFFC0
	.globl _AX5043_TXPWRCOEFFB1
	.globl _AX5043_TXPWRCOEFFB0
	.globl _AX5043_TXPWRCOEFFA1
	.globl _AX5043_TXPWRCOEFFA0
	.globl _AX5043_TRKRFFREQ2
	.globl _AX5043_TRKRFFREQ1
	.globl _AX5043_TRKRFFREQ0
	.globl _AX5043_TRKPHASE1
	.globl _AX5043_TRKPHASE0
	.globl _AX5043_TRKFSKDEMOD1
	.globl _AX5043_TRKFSKDEMOD0
	.globl _AX5043_TRKFREQ1
	.globl _AX5043_TRKFREQ0
	.globl _AX5043_TRKDATARATE2
	.globl _AX5043_TRKDATARATE1
	.globl _AX5043_TRKDATARATE0
	.globl _AX5043_TRKAMPLITUDE1
	.globl _AX5043_TRKAMPLITUDE0
	.globl _AX5043_TRKAFSKDEMOD1
	.globl _AX5043_TRKAFSKDEMOD0
	.globl _AX5043_TMGTXSETTLE
	.globl _AX5043_TMGTXBOOST
	.globl _AX5043_TMGRXSETTLE
	.globl _AX5043_TMGRXRSSI
	.globl _AX5043_TMGRXPREAMBLE3
	.globl _AX5043_TMGRXPREAMBLE2
	.globl _AX5043_TMGRXPREAMBLE1
	.globl _AX5043_TMGRXOFFSACQ
	.globl _AX5043_TMGRXCOARSEAGC
	.globl _AX5043_TMGRXBOOST
	.globl _AX5043_TMGRXAGC
	.globl _AX5043_TIMER2
	.globl _AX5043_TIMER1
	.globl _AX5043_TIMER0
	.globl _AX5043_SILICONREVISION
	.globl _AX5043_SCRATCH
	.globl _AX5043_RXDATARATE2
	.globl _AX5043_RXDATARATE1
	.globl _AX5043_RXDATARATE0
	.globl _AX5043_RSSIREFERENCE
	.globl _AX5043_RSSIABSTHR
	.globl _AX5043_RSSI
	.globl _AX5043_RADIOSTATE
	.globl _AX5043_RADIOEVENTREQ1
	.globl _AX5043_RADIOEVENTREQ0
	.globl _AX5043_RADIOEVENTMASK1
	.globl _AX5043_RADIOEVENTMASK0
	.globl _AX5043_PWRMODE
	.globl _AX5043_PWRAMP
	.globl _AX5043_POWSTICKYSTAT
	.globl _AX5043_POWSTAT
	.globl _AX5043_POWIRQMASK
	.globl _AX5043_PLLVCOIR
	.globl _AX5043_PLLVCOI
	.globl _AX5043_PLLVCODIV
	.globl _AX5043_PLLRNGCLK
	.globl _AX5043_PLLRANGINGB
	.globl _AX5043_PLLRANGINGA
	.globl _AX5043_PLLLOOPBOOST
	.globl _AX5043_PLLLOOP
	.globl _AX5043_PLLLOCKDET
	.globl _AX5043_PLLCPIBOOST
	.globl _AX5043_PLLCPI
	.globl _AX5043_PKTSTOREFLAGS
	.globl _AX5043_PKTMISCFLAGS
	.globl _AX5043_PKTCHUNKSIZE
	.globl _AX5043_PKTACCEPTFLAGS
	.globl _AX5043_PINSTATE
	.globl _AX5043_PINFUNCSYSCLK
	.globl _AX5043_PINFUNCPWRAMP
	.globl _AX5043_PINFUNCIRQ
	.globl _AX5043_PINFUNCDCLK
	.globl _AX5043_PINFUNCDATA
	.globl _AX5043_PINFUNCANTSEL
	.globl _AX5043_MODULATION
	.globl _AX5043_MODCFGF
	.globl _AX5043_MODCFGA
	.globl _AX5043_MAXRFOFFSET2
	.globl _AX5043_MAXRFOFFSET1
	.globl _AX5043_MAXRFOFFSET0
	.globl _AX5043_MAXDROFFSET2
	.globl _AX5043_MAXDROFFSET1
	.globl _AX5043_MAXDROFFSET0
	.globl _AX5043_MATCH1PAT1
	.globl _AX5043_MATCH1PAT0
	.globl _AX5043_MATCH1MIN
	.globl _AX5043_MATCH1MAX
	.globl _AX5043_MATCH1LEN
	.globl _AX5043_MATCH0PAT3
	.globl _AX5043_MATCH0PAT2
	.globl _AX5043_MATCH0PAT1
	.globl _AX5043_MATCH0PAT0
	.globl _AX5043_MATCH0MIN
	.globl _AX5043_MATCH0MAX
	.globl _AX5043_MATCH0LEN
	.globl _AX5043_LPOSCSTATUS
	.globl _AX5043_LPOSCREF1
	.globl _AX5043_LPOSCREF0
	.globl _AX5043_LPOSCPER1
	.globl _AX5043_LPOSCPER0
	.globl _AX5043_LPOSCKFILT1
	.globl _AX5043_LPOSCKFILT0
	.globl _AX5043_LPOSCFREQ1
	.globl _AX5043_LPOSCFREQ0
	.globl _AX5043_LPOSCCONFIG
	.globl _AX5043_IRQREQUEST1
	.globl _AX5043_IRQREQUEST0
	.globl _AX5043_IRQMASK1
	.globl _AX5043_IRQMASK0
	.globl _AX5043_IRQINVERSION1
	.globl _AX5043_IRQINVERSION0
	.globl _AX5043_IFFREQ1
	.globl _AX5043_IFFREQ0
	.globl _AX5043_GPADCPERIOD
	.globl _AX5043_GPADCCTRL
	.globl _AX5043_GPADC13VALUE1
	.globl _AX5043_GPADC13VALUE0
	.globl _AX5043_FSKDMIN1
	.globl _AX5043_FSKDMIN0
	.globl _AX5043_FSKDMAX1
	.globl _AX5043_FSKDMAX0
	.globl _AX5043_FSKDEV2
	.globl _AX5043_FSKDEV1
	.globl _AX5043_FSKDEV0
	.globl _AX5043_FREQB3
	.globl _AX5043_FREQB2
	.globl _AX5043_FREQB1
	.globl _AX5043_FREQB0
	.globl _AX5043_FREQA3
	.globl _AX5043_FREQA2
	.globl _AX5043_FREQA1
	.globl _AX5043_FREQA0
	.globl _AX5043_FRAMING
	.globl _AX5043_FIFOTHRESH1
	.globl _AX5043_FIFOTHRESH0
	.globl _AX5043_FIFOSTAT
	.globl _AX5043_FIFOFREE1
	.globl _AX5043_FIFOFREE0
	.globl _AX5043_FIFODATA
	.globl _AX5043_FIFOCOUNT1
	.globl _AX5043_FIFOCOUNT0
	.globl _AX5043_FECSYNC
	.globl _AX5043_FECSTATUS
	.globl _AX5043_FEC
	.globl _AX5043_ENCODING
	.globl _AX5043_DIVERSITY
	.globl _AX5043_DECIMATION
	.globl _AX5043_DACVALUE1
	.globl _AX5043_DACVALUE0
	.globl _AX5043_DACCONFIG
	.globl _AX5043_CRCINIT3
	.globl _AX5043_CRCINIT2
	.globl _AX5043_CRCINIT1
	.globl _AX5043_CRCINIT0
	.globl _AX5043_BGNDRSSITHR
	.globl _AX5043_BGNDRSSIGAIN
	.globl _AX5043_BGNDRSSI
	.globl _AX5043_BBTUNE
	.globl _AX5043_BBOFFSCAP
	.globl _AX5043_AMPLFILTER
	.globl _AX5043_AGCCOUNTER
	.globl _AX5043_AFSKSPACE1
	.globl _AX5043_AFSKSPACE0
	.globl _AX5043_AFSKMARK1
	.globl _AX5043_AFSKMARK0
	.globl _AX5043_AFSKCTRL
	.globl _XWTSTAT
	.globl _XWTIRQEN
	.globl _XWTEVTD
	.globl _XWTEVTD1
	.globl _XWTEVTD0
	.globl _XWTEVTC
	.globl _XWTEVTC1
	.globl _XWTEVTC0
	.globl _XWTEVTB
	.globl _XWTEVTB1
	.globl _XWTEVTB0
	.globl _XWTEVTA
	.globl _XWTEVTA1
	.globl _XWTEVTA0
	.globl _XWTCNTR1
	.globl _XWTCNTB
	.globl _XWTCNTB1
	.globl _XWTCNTB0
	.globl _XWTCNTA
	.globl _XWTCNTA1
	.globl _XWTCNTA0
	.globl _XWTCFGB
	.globl _XWTCFGA
	.globl _XWDTRESET
	.globl _XWDTCFG
	.globl _XU1STATUS
	.globl _XU1SHREG
	.globl _XU1MODE
	.globl _XU1CTRL
	.globl _XU0STATUS
	.globl _XU0SHREG
	.globl _XU0MODE
	.globl _XU0CTRL
	.globl _XT2STATUS
	.globl _XT2PERIOD
	.globl _XT2PERIOD1
	.globl _XT2PERIOD0
	.globl _XT2MODE
	.globl _XT2CNT
	.globl _XT2CNT1
	.globl _XT2CNT0
	.globl _XT2CLKSRC
	.globl _XT1STATUS
	.globl _XT1PERIOD
	.globl _XT1PERIOD1
	.globl _XT1PERIOD0
	.globl _XT1MODE
	.globl _XT1CNT
	.globl _XT1CNT1
	.globl _XT1CNT0
	.globl _XT1CLKSRC
	.globl _XT0STATUS
	.globl _XT0PERIOD
	.globl _XT0PERIOD1
	.globl _XT0PERIOD0
	.globl _XT0MODE
	.globl _XT0CNT
	.globl _XT0CNT1
	.globl _XT0CNT0
	.globl _XT0CLKSRC
	.globl _XSPSTATUS
	.globl _XSPSHREG
	.globl _XSPMODE
	.globl _XSPCLKSRC
	.globl _XRADIOSTAT
	.globl _XRADIOSTAT1
	.globl _XRADIOSTAT0
	.globl _XRADIODATA3
	.globl _XRADIODATA2
	.globl _XRADIODATA1
	.globl _XRADIODATA0
	.globl _XRADIOADDR1
	.globl _XRADIOADDR0
	.globl _XRADIOACC
	.globl _XOC1STATUS
	.globl _XOC1PIN
	.globl _XOC1MODE
	.globl _XOC1COMP
	.globl _XOC1COMP1
	.globl _XOC1COMP0
	.globl _XOC0STATUS
	.globl _XOC0PIN
	.globl _XOC0MODE
	.globl _XOC0COMP
	.globl _XOC0COMP1
	.globl _XOC0COMP0
	.globl _XNVSTATUS
	.globl _XNVKEY
	.globl _XNVDATA
	.globl _XNVDATA1
	.globl _XNVDATA0
	.globl _XNVADDR
	.globl _XNVADDR1
	.globl _XNVADDR0
	.globl _XIC1STATUS
	.globl _XIC1MODE
	.globl _XIC1CAPT
	.globl _XIC1CAPT1
	.globl _XIC1CAPT0
	.globl _XIC0STATUS
	.globl _XIC0MODE
	.globl _XIC0CAPT
	.globl _XIC0CAPT1
	.globl _XIC0CAPT0
	.globl _XPORTR
	.globl _XPORTC
	.globl _XPORTB
	.globl _XPORTA
	.globl _XPINR
	.globl _XPINC
	.globl _XPINB
	.globl _XPINA
	.globl _XDIRR
	.globl _XDIRC
	.globl _XDIRB
	.globl _XDIRA
	.globl _XDBGLNKSTAT
	.globl _XDBGLNKBUF
	.globl _XCODECONFIG
	.globl _XCLKSTAT
	.globl _XCLKCON
	.globl _XANALOGCOMP
	.globl _XADCCONV
	.globl _XADCCLKSRC
	.globl _XADCCH3CONFIG
	.globl _XADCCH2CONFIG
	.globl _XADCCH1CONFIG
	.globl _XADCCH0CONFIG
	.globl _XPCON
	.globl _XIP
	.globl _XIE
	.globl _XDPTR1
	.globl _XDPTR0
	.globl _RNGCLKSRC1
	.globl _RNGCLKSRC0
	.globl _RNGMODE
	.globl _AESOUTADDR
	.globl _AESOUTADDR1
	.globl _AESOUTADDR0
	.globl _AESMODE
	.globl _AESKEYADDR
	.globl _AESKEYADDR1
	.globl _AESKEYADDR0
	.globl _AESINADDR
	.globl _AESINADDR1
	.globl _AESINADDR0
	.globl _AESCURBLOCK
	.globl _AESCONFIG
	.globl _RNGBYTE
	.globl _XTALREADY
	.globl _XTALOSC
	.globl _XTALAMPL
	.globl _SILICONREV
	.globl _SCRATCH3
	.globl _SCRATCH2
	.globl _SCRATCH1
	.globl _SCRATCH0
	.globl _RADIOMUX
	.globl _RADIOFSTATADDR
	.globl _RADIOFSTATADDR1
	.globl _RADIOFSTATADDR0
	.globl _RADIOFDATAADDR
	.globl _RADIOFDATAADDR1
	.globl _RADIOFDATAADDR0
	.globl _OSCRUN
	.globl _OSCREADY
	.globl _OSCFORCERUN
	.globl _OSCCALIB
	.globl _MISCCTRL
	.globl _LPXOSCGM
	.globl _LPOSCREF
	.globl _LPOSCREF1
	.globl _LPOSCREF0
	.globl _LPOSCPER
	.globl _LPOSCPER1
	.globl _LPOSCPER0
	.globl _LPOSCKFILT
	.globl _LPOSCKFILT1
	.globl _LPOSCKFILT0
	.globl _LPOSCFREQ
	.globl _LPOSCFREQ1
	.globl _LPOSCFREQ0
	.globl _LPOSCCONFIG
	.globl _PINSEL
	.globl _PINCHGC
	.globl _PINCHGB
	.globl _PINCHGA
	.globl _PALTRADIO
	.globl _PALTC
	.globl _PALTB
	.globl _PALTA
	.globl _INTCHGC
	.globl _INTCHGB
	.globl _INTCHGA
	.globl _EXTIRQ
	.globl _GPIOENABLE
	.globl _ANALOGA
	.globl _FRCOSCREF
	.globl _FRCOSCREF1
	.globl _FRCOSCREF0
	.globl _FRCOSCPER
	.globl _FRCOSCPER1
	.globl _FRCOSCPER0
	.globl _FRCOSCKFILT
	.globl _FRCOSCKFILT1
	.globl _FRCOSCKFILT0
	.globl _FRCOSCFREQ
	.globl _FRCOSCFREQ1
	.globl _FRCOSCFREQ0
	.globl _FRCOSCCTRL
	.globl _FRCOSCCONFIG
	.globl _DMA1CONFIG
	.globl _DMA1ADDR
	.globl _DMA1ADDR1
	.globl _DMA1ADDR0
	.globl _DMA0CONFIG
	.globl _DMA0ADDR
	.globl _DMA0ADDR1
	.globl _DMA0ADDR0
	.globl _ADCTUNE2
	.globl _ADCTUNE1
	.globl _ADCTUNE0
	.globl _ADCCH3VAL
	.globl _ADCCH3VAL1
	.globl _ADCCH3VAL0
	.globl _ADCCH2VAL
	.globl _ADCCH2VAL1
	.globl _ADCCH2VAL0
	.globl _ADCCH1VAL
	.globl _ADCCH1VAL1
	.globl _ADCCH1VAL0
	.globl _ADCCH0VAL
	.globl _ADCCH0VAL1
	.globl _ADCCH0VAL0
	.globl _axradio_trxstate
	.globl _axradio_mode
	.globl _axradio_conv_time_totimer0
	.globl _axradio_isr
	.globl _ax5043_receiver_on_continuous
	.globl _ax5043_receiver_on_wor
	.globl _ax5043_prepare_tx
	.globl _ax5043_off
	.globl _ax5043_off_xtal
	.globl _axradio_wait_for_xtal
	.globl _axradio_init
	.globl _axradio_cansleep
	.globl _axradio_set_mode
	.globl _axradio_set_channel
	.globl _axradio_get_pllvcoi
	.globl _axradio_set_local_address
	.globl _axradio_set_default_remote_address
	.globl _axradio_transmit
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
_ACC	=	0x00e0
_B	=	0x00f0
_DPH	=	0x0083
_DPH1	=	0x0085
_DPL	=	0x0082
_DPL1	=	0x0084
_DPTR0	=	0x8382
_DPTR1	=	0x8584
_DPS	=	0x0086
_E2IE	=	0x00a0
_E2IP	=	0x00c0
_EIE	=	0x0098
_EIP	=	0x00b0
_IE	=	0x00a8
_IP	=	0x00b8
_PCON	=	0x0087
_PSW	=	0x00d0
_SP	=	0x0081
_XPAGE	=	0x00d9
__XPAGE	=	0x00d9
_ADCCH0CONFIG	=	0x00ca
_ADCCH1CONFIG	=	0x00cb
_ADCCH2CONFIG	=	0x00d2
_ADCCH3CONFIG	=	0x00d3
_ADCCLKSRC	=	0x00d1
_ADCCONV	=	0x00c9
_ANALOGCOMP	=	0x00e1
_CLKCON	=	0x00c6
_CLKSTAT	=	0x00c7
_CODECONFIG	=	0x0097
_DBGLNKBUF	=	0x00e3
_DBGLNKSTAT	=	0x00e2
_DIRA	=	0x0089
_DIRB	=	0x008a
_DIRC	=	0x008b
_DIRR	=	0x008e
_PINA	=	0x00c8
_PINB	=	0x00e8
_PINC	=	0x00f8
_PINR	=	0x008d
_PORTA	=	0x0080
_PORTB	=	0x0088
_PORTC	=	0x0090
_PORTR	=	0x008c
_IC0CAPT0	=	0x00ce
_IC0CAPT1	=	0x00cf
_IC0CAPT	=	0xcfce
_IC0MODE	=	0x00cc
_IC0STATUS	=	0x00cd
_IC1CAPT0	=	0x00d6
_IC1CAPT1	=	0x00d7
_IC1CAPT	=	0xd7d6
_IC1MODE	=	0x00d4
_IC1STATUS	=	0x00d5
_NVADDR0	=	0x0092
_NVADDR1	=	0x0093
_NVADDR	=	0x9392
_NVDATA0	=	0x0094
_NVDATA1	=	0x0095
_NVDATA	=	0x9594
_NVKEY	=	0x0096
_NVSTATUS	=	0x0091
_OC0COMP0	=	0x00bc
_OC0COMP1	=	0x00bd
_OC0COMP	=	0xbdbc
_OC0MODE	=	0x00b9
_OC0PIN	=	0x00ba
_OC0STATUS	=	0x00bb
_OC1COMP0	=	0x00c4
_OC1COMP1	=	0x00c5
_OC1COMP	=	0xc5c4
_OC1MODE	=	0x00c1
_OC1PIN	=	0x00c2
_OC1STATUS	=	0x00c3
_RADIOACC	=	0x00b1
_RADIOADDR0	=	0x00b3
_RADIOADDR1	=	0x00b2
_RADIOADDR	=	0xb2b3
_RADIODATA0	=	0x00b7
_RADIODATA1	=	0x00b6
_RADIODATA2	=	0x00b5
_RADIODATA3	=	0x00b4
_RADIODATA	=	0xb4b5b6b7
_RADIOSTAT0	=	0x00be
_RADIOSTAT1	=	0x00bf
_RADIOSTAT	=	0xbfbe
_SPCLKSRC	=	0x00df
_SPMODE	=	0x00dc
_SPSHREG	=	0x00de
_SPSTATUS	=	0x00dd
_T0CLKSRC	=	0x009a
_T0CNT0	=	0x009c
_T0CNT1	=	0x009d
_T0CNT	=	0x9d9c
_T0MODE	=	0x0099
_T0PERIOD0	=	0x009e
_T0PERIOD1	=	0x009f
_T0PERIOD	=	0x9f9e
_T0STATUS	=	0x009b
_T1CLKSRC	=	0x00a2
_T1CNT0	=	0x00a4
_T1CNT1	=	0x00a5
_T1CNT	=	0xa5a4
_T1MODE	=	0x00a1
_T1PERIOD0	=	0x00a6
_T1PERIOD1	=	0x00a7
_T1PERIOD	=	0xa7a6
_T1STATUS	=	0x00a3
_T2CLKSRC	=	0x00aa
_T2CNT0	=	0x00ac
_T2CNT1	=	0x00ad
_T2CNT	=	0xadac
_T2MODE	=	0x00a9
_T2PERIOD0	=	0x00ae
_T2PERIOD1	=	0x00af
_T2PERIOD	=	0xafae
_T2STATUS	=	0x00ab
_U0CTRL	=	0x00e4
_U0MODE	=	0x00e7
_U0SHREG	=	0x00e6
_U0STATUS	=	0x00e5
_U1CTRL	=	0x00ec
_U1MODE	=	0x00ef
_U1SHREG	=	0x00ee
_U1STATUS	=	0x00ed
_WDTCFG	=	0x00da
_WDTRESET	=	0x00db
_WTCFGA	=	0x00f1
_WTCFGB	=	0x00f9
_WTCNTA0	=	0x00f2
_WTCNTA1	=	0x00f3
_WTCNTA	=	0xf3f2
_WTCNTB0	=	0x00fa
_WTCNTB1	=	0x00fb
_WTCNTB	=	0xfbfa
_WTCNTR1	=	0x00eb
_WTEVTA0	=	0x00f4
_WTEVTA1	=	0x00f5
_WTEVTA	=	0xf5f4
_WTEVTB0	=	0x00f6
_WTEVTB1	=	0x00f7
_WTEVTB	=	0xf7f6
_WTEVTC0	=	0x00fc
_WTEVTC1	=	0x00fd
_WTEVTC	=	0xfdfc
_WTEVTD0	=	0x00fe
_WTEVTD1	=	0x00ff
_WTEVTD	=	0xfffe
_WTIRQEN	=	0x00e9
_WTSTAT	=	0x00ea
;--------------------------------------------------------
; special function bits
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
_ACC_0	=	0x00e0
_ACC_1	=	0x00e1
_ACC_2	=	0x00e2
_ACC_3	=	0x00e3
_ACC_4	=	0x00e4
_ACC_5	=	0x00e5
_ACC_6	=	0x00e6
_ACC_7	=	0x00e7
_B_0	=	0x00f0
_B_1	=	0x00f1
_B_2	=	0x00f2
_B_3	=	0x00f3
_B_4	=	0x00f4
_B_5	=	0x00f5
_B_6	=	0x00f6
_B_7	=	0x00f7
_E2IE_0	=	0x00a0
_E2IE_1	=	0x00a1
_E2IE_2	=	0x00a2
_E2IE_3	=	0x00a3
_E2IE_4	=	0x00a4
_E2IE_5	=	0x00a5
_E2IE_6	=	0x00a6
_E2IE_7	=	0x00a7
_E2IP_0	=	0x00c0
_E2IP_1	=	0x00c1
_E2IP_2	=	0x00c2
_E2IP_3	=	0x00c3
_E2IP_4	=	0x00c4
_E2IP_5	=	0x00c5
_E2IP_6	=	0x00c6
_E2IP_7	=	0x00c7
_EIE_0	=	0x0098
_EIE_1	=	0x0099
_EIE_2	=	0x009a
_EIE_3	=	0x009b
_EIE_4	=	0x009c
_EIE_5	=	0x009d
_EIE_6	=	0x009e
_EIE_7	=	0x009f
_EIP_0	=	0x00b0
_EIP_1	=	0x00b1
_EIP_2	=	0x00b2
_EIP_3	=	0x00b3
_EIP_4	=	0x00b4
_EIP_5	=	0x00b5
_EIP_6	=	0x00b6
_EIP_7	=	0x00b7
_IE_0	=	0x00a8
_IE_1	=	0x00a9
_IE_2	=	0x00aa
_IE_3	=	0x00ab
_IE_4	=	0x00ac
_IE_5	=	0x00ad
_IE_6	=	0x00ae
_IE_7	=	0x00af
_EA	=	0x00af
_IP_0	=	0x00b8
_IP_1	=	0x00b9
_IP_2	=	0x00ba
_IP_3	=	0x00bb
_IP_4	=	0x00bc
_IP_5	=	0x00bd
_IP_6	=	0x00be
_IP_7	=	0x00bf
_P	=	0x00d0
_F1	=	0x00d1
_OV	=	0x00d2
_RS0	=	0x00d3
_RS1	=	0x00d4
_F0	=	0x00d5
_AC	=	0x00d6
_CY	=	0x00d7
_PINA_0	=	0x00c8
_PINA_1	=	0x00c9
_PINA_2	=	0x00ca
_PINA_3	=	0x00cb
_PINA_4	=	0x00cc
_PINA_5	=	0x00cd
_PINA_6	=	0x00ce
_PINA_7	=	0x00cf
_PINB_0	=	0x00e8
_PINB_1	=	0x00e9
_PINB_2	=	0x00ea
_PINB_3	=	0x00eb
_PINB_4	=	0x00ec
_PINB_5	=	0x00ed
_PINB_6	=	0x00ee
_PINB_7	=	0x00ef
_PINC_0	=	0x00f8
_PINC_1	=	0x00f9
_PINC_2	=	0x00fa
_PINC_3	=	0x00fb
_PINC_4	=	0x00fc
_PINC_5	=	0x00fd
_PINC_6	=	0x00fe
_PINC_7	=	0x00ff
_PORTA_0	=	0x0080
_PORTA_1	=	0x0081
_PORTA_2	=	0x0082
_PORTA_3	=	0x0083
_PORTA_4	=	0x0084
_PORTA_5	=	0x0085
_PORTA_6	=	0x0086
_PORTA_7	=	0x0087
_PORTB_0	=	0x0088
_PORTB_1	=	0x0089
_PORTB_2	=	0x008a
_PORTB_3	=	0x008b
_PORTB_4	=	0x008c
_PORTB_5	=	0x008d
_PORTB_6	=	0x008e
_PORTB_7	=	0x008f
_PORTC_0	=	0x0090
_PORTC_1	=	0x0091
_PORTC_2	=	0x0092
_PORTC_3	=	0x0093
_PORTC_4	=	0x0094
_PORTC_5	=	0x0095
_PORTC_6	=	0x0096
_PORTC_7	=	0x0097
;--------------------------------------------------------
; overlayable register banks
;--------------------------------------------------------
	.area REG_BANK_0	(REL,OVR,DATA)
	.ds 8
;--------------------------------------------------------
; overlayable bit register bank
;--------------------------------------------------------
	.area BIT_BANK	(REL,OVR,DATA)
bits:
	.ds 1
	b0 = bits[0]
	b1 = bits[1]
	b2 = bits[2]
	b3 = bits[3]
	b4 = bits[4]
	b5 = bits[5]
	b6 = bits[6]
	b7 = bits[7]
;--------------------------------------------------------
; internal ram data
;--------------------------------------------------------
	.area DSEG    (DATA)
_axradio_mode::
	.ds 1
_axradio_trxstate::
	.ds 1
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
	.area	OSEG    (OVR,DATA)
	.area	OSEG    (OVR,DATA)
;--------------------------------------------------------
; indirectly addressable internal ram data
;--------------------------------------------------------
	.area ISEG    (DATA)
;--------------------------------------------------------
; absolute internal ram data
;--------------------------------------------------------
	.area IABS    (ABS,DATA)
	.area IABS    (ABS,DATA)
;--------------------------------------------------------
; bit data
;--------------------------------------------------------
	.area BSEG    (BIT)
;--------------------------------------------------------
; paged external ram data
;--------------------------------------------------------
	.area PSEG    (PAG,XDATA)
;--------------------------------------------------------
; external ram data
;--------------------------------------------------------
	.area XSEG    (XDATA)
_ADCCH0VAL0	=	0x7020
_ADCCH0VAL1	=	0x7021
_ADCCH0VAL	=	0x7020
_ADCCH1VAL0	=	0x7022
_ADCCH1VAL1	=	0x7023
_ADCCH1VAL	=	0x7022
_ADCCH2VAL0	=	0x7024
_ADCCH2VAL1	=	0x7025
_ADCCH2VAL	=	0x7024
_ADCCH3VAL0	=	0x7026
_ADCCH3VAL1	=	0x7027
_ADCCH3VAL	=	0x7026
_ADCTUNE0	=	0x7028
_ADCTUNE1	=	0x7029
_ADCTUNE2	=	0x702a
_DMA0ADDR0	=	0x7010
_DMA0ADDR1	=	0x7011
_DMA0ADDR	=	0x7010
_DMA0CONFIG	=	0x7014
_DMA1ADDR0	=	0x7012
_DMA1ADDR1	=	0x7013
_DMA1ADDR	=	0x7012
_DMA1CONFIG	=	0x7015
_FRCOSCCONFIG	=	0x7070
_FRCOSCCTRL	=	0x7071
_FRCOSCFREQ0	=	0x7076
_FRCOSCFREQ1	=	0x7077
_FRCOSCFREQ	=	0x7076
_FRCOSCKFILT0	=	0x7072
_FRCOSCKFILT1	=	0x7073
_FRCOSCKFILT	=	0x7072
_FRCOSCPER0	=	0x7078
_FRCOSCPER1	=	0x7079
_FRCOSCPER	=	0x7078
_FRCOSCREF0	=	0x7074
_FRCOSCREF1	=	0x7075
_FRCOSCREF	=	0x7074
_ANALOGA	=	0x7007
_GPIOENABLE	=	0x700c
_EXTIRQ	=	0x7003
_INTCHGA	=	0x7000
_INTCHGB	=	0x7001
_INTCHGC	=	0x7002
_PALTA	=	0x7008
_PALTB	=	0x7009
_PALTC	=	0x700a
_PALTRADIO	=	0x7046
_PINCHGA	=	0x7004
_PINCHGB	=	0x7005
_PINCHGC	=	0x7006
_PINSEL	=	0x700b
_LPOSCCONFIG	=	0x7060
_LPOSCFREQ0	=	0x7066
_LPOSCFREQ1	=	0x7067
_LPOSCFREQ	=	0x7066
_LPOSCKFILT0	=	0x7062
_LPOSCKFILT1	=	0x7063
_LPOSCKFILT	=	0x7062
_LPOSCPER0	=	0x7068
_LPOSCPER1	=	0x7069
_LPOSCPER	=	0x7068
_LPOSCREF0	=	0x7064
_LPOSCREF1	=	0x7065
_LPOSCREF	=	0x7064
_LPXOSCGM	=	0x7054
_MISCCTRL	=	0x7f01
_OSCCALIB	=	0x7053
_OSCFORCERUN	=	0x7050
_OSCREADY	=	0x7052
_OSCRUN	=	0x7051
_RADIOFDATAADDR0	=	0x7040
_RADIOFDATAADDR1	=	0x7041
_RADIOFDATAADDR	=	0x7040
_RADIOFSTATADDR0	=	0x7042
_RADIOFSTATADDR1	=	0x7043
_RADIOFSTATADDR	=	0x7042
_RADIOMUX	=	0x7044
_SCRATCH0	=	0x7084
_SCRATCH1	=	0x7085
_SCRATCH2	=	0x7086
_SCRATCH3	=	0x7087
_SILICONREV	=	0x7f00
_XTALAMPL	=	0x7f19
_XTALOSC	=	0x7f18
_XTALREADY	=	0x7f1a
_RNGBYTE	=	0x7081
_AESCONFIG	=	0x7091
_AESCURBLOCK	=	0x7098
_AESINADDR0	=	0x7094
_AESINADDR1	=	0x7095
_AESINADDR	=	0x7094
_AESKEYADDR0	=	0x7092
_AESKEYADDR1	=	0x7093
_AESKEYADDR	=	0x7092
_AESMODE	=	0x7090
_AESOUTADDR0	=	0x7096
_AESOUTADDR1	=	0x7097
_AESOUTADDR	=	0x7096
_RNGMODE	=	0x7080
_RNGCLKSRC0	=	0x7082
_RNGCLKSRC1	=	0x7083
_XDPTR0	=	0x3f82
_XDPTR1	=	0x3f84
_XIE	=	0x3fa8
_XIP	=	0x3fb8
_XPCON	=	0x3f87
_XADCCH0CONFIG	=	0x3fca
_XADCCH1CONFIG	=	0x3fcb
_XADCCH2CONFIG	=	0x3fd2
_XADCCH3CONFIG	=	0x3fd3
_XADCCLKSRC	=	0x3fd1
_XADCCONV	=	0x3fc9
_XANALOGCOMP	=	0x3fe1
_XCLKCON	=	0x3fc6
_XCLKSTAT	=	0x3fc7
_XCODECONFIG	=	0x3f97
_XDBGLNKBUF	=	0x3fe3
_XDBGLNKSTAT	=	0x3fe2
_XDIRA	=	0x3f89
_XDIRB	=	0x3f8a
_XDIRC	=	0x3f8b
_XDIRR	=	0x3f8e
_XPINA	=	0x3fc8
_XPINB	=	0x3fe8
_XPINC	=	0x3ff8
_XPINR	=	0x3f8d
_XPORTA	=	0x3f80
_XPORTB	=	0x3f88
_XPORTC	=	0x3f90
_XPORTR	=	0x3f8c
_XIC0CAPT0	=	0x3fce
_XIC0CAPT1	=	0x3fcf
_XIC0CAPT	=	0x3fce
_XIC0MODE	=	0x3fcc
_XIC0STATUS	=	0x3fcd
_XIC1CAPT0	=	0x3fd6
_XIC1CAPT1	=	0x3fd7
_XIC1CAPT	=	0x3fd6
_XIC1MODE	=	0x3fd4
_XIC1STATUS	=	0x3fd5
_XNVADDR0	=	0x3f92
_XNVADDR1	=	0x3f93
_XNVADDR	=	0x3f92
_XNVDATA0	=	0x3f94
_XNVDATA1	=	0x3f95
_XNVDATA	=	0x3f94
_XNVKEY	=	0x3f96
_XNVSTATUS	=	0x3f91
_XOC0COMP0	=	0x3fbc
_XOC0COMP1	=	0x3fbd
_XOC0COMP	=	0x3fbc
_XOC0MODE	=	0x3fb9
_XOC0PIN	=	0x3fba
_XOC0STATUS	=	0x3fbb
_XOC1COMP0	=	0x3fc4
_XOC1COMP1	=	0x3fc5
_XOC1COMP	=	0x3fc4
_XOC1MODE	=	0x3fc1
_XOC1PIN	=	0x3fc2
_XOC1STATUS	=	0x3fc3
_XRADIOACC	=	0x3fb1
_XRADIOADDR0	=	0x3fb3
_XRADIOADDR1	=	0x3fb2
_XRADIODATA0	=	0x3fb7
_XRADIODATA1	=	0x3fb6
_XRADIODATA2	=	0x3fb5
_XRADIODATA3	=	0x3fb4
_XRADIOSTAT0	=	0x3fbe
_XRADIOSTAT1	=	0x3fbf
_XRADIOSTAT	=	0x3fbe
_XSPCLKSRC	=	0x3fdf
_XSPMODE	=	0x3fdc
_XSPSHREG	=	0x3fde
_XSPSTATUS	=	0x3fdd
_XT0CLKSRC	=	0x3f9a
_XT0CNT0	=	0x3f9c
_XT0CNT1	=	0x3f9d
_XT0CNT	=	0x3f9c
_XT0MODE	=	0x3f99
_XT0PERIOD0	=	0x3f9e
_XT0PERIOD1	=	0x3f9f
_XT0PERIOD	=	0x3f9e
_XT0STATUS	=	0x3f9b
_XT1CLKSRC	=	0x3fa2
_XT1CNT0	=	0x3fa4
_XT1CNT1	=	0x3fa5
_XT1CNT	=	0x3fa4
_XT1MODE	=	0x3fa1
_XT1PERIOD0	=	0x3fa6
_XT1PERIOD1	=	0x3fa7
_XT1PERIOD	=	0x3fa6
_XT1STATUS	=	0x3fa3
_XT2CLKSRC	=	0x3faa
_XT2CNT0	=	0x3fac
_XT2CNT1	=	0x3fad
_XT2CNT	=	0x3fac
_XT2MODE	=	0x3fa9
_XT2PERIOD0	=	0x3fae
_XT2PERIOD1	=	0x3faf
_XT2PERIOD	=	0x3fae
_XT2STATUS	=	0x3fab
_XU0CTRL	=	0x3fe4
_XU0MODE	=	0x3fe7
_XU0SHREG	=	0x3fe6
_XU0STATUS	=	0x3fe5
_XU1CTRL	=	0x3fec
_XU1MODE	=	0x3fef
_XU1SHREG	=	0x3fee
_XU1STATUS	=	0x3fed
_XWDTCFG	=	0x3fda
_XWDTRESET	=	0x3fdb
_XWTCFGA	=	0x3ff1
_XWTCFGB	=	0x3ff9
_XWTCNTA0	=	0x3ff2
_XWTCNTA1	=	0x3ff3
_XWTCNTA	=	0x3ff2
_XWTCNTB0	=	0x3ffa
_XWTCNTB1	=	0x3ffb
_XWTCNTB	=	0x3ffa
_XWTCNTR1	=	0x3feb
_XWTEVTA0	=	0x3ff4
_XWTEVTA1	=	0x3ff5
_XWTEVTA	=	0x3ff4
_XWTEVTB0	=	0x3ff6
_XWTEVTB1	=	0x3ff7
_XWTEVTB	=	0x3ff6
_XWTEVTC0	=	0x3ffc
_XWTEVTC1	=	0x3ffd
_XWTEVTC	=	0x3ffc
_XWTEVTD0	=	0x3ffe
_XWTEVTD1	=	0x3fff
_XWTEVTD	=	0x3ffe
_XWTIRQEN	=	0x3fe9
_XWTSTAT	=	0x3fea
_AX5043_AFSKCTRL	=	0x4114
_AX5043_AFSKMARK0	=	0x4113
_AX5043_AFSKMARK1	=	0x4112
_AX5043_AFSKSPACE0	=	0x4111
_AX5043_AFSKSPACE1	=	0x4110
_AX5043_AGCCOUNTER	=	0x4043
_AX5043_AMPLFILTER	=	0x4115
_AX5043_BBOFFSCAP	=	0x4189
_AX5043_BBTUNE	=	0x4188
_AX5043_BGNDRSSI	=	0x4041
_AX5043_BGNDRSSIGAIN	=	0x422e
_AX5043_BGNDRSSITHR	=	0x422f
_AX5043_CRCINIT0	=	0x4017
_AX5043_CRCINIT1	=	0x4016
_AX5043_CRCINIT2	=	0x4015
_AX5043_CRCINIT3	=	0x4014
_AX5043_DACCONFIG	=	0x4332
_AX5043_DACVALUE0	=	0x4331
_AX5043_DACVALUE1	=	0x4330
_AX5043_DECIMATION	=	0x4102
_AX5043_DIVERSITY	=	0x4042
_AX5043_ENCODING	=	0x4011
_AX5043_FEC	=	0x4018
_AX5043_FECSTATUS	=	0x401a
_AX5043_FECSYNC	=	0x4019
_AX5043_FIFOCOUNT0	=	0x402b
_AX5043_FIFOCOUNT1	=	0x402a
_AX5043_FIFODATA	=	0x4029
_AX5043_FIFOFREE0	=	0x402d
_AX5043_FIFOFREE1	=	0x402c
_AX5043_FIFOSTAT	=	0x4028
_AX5043_FIFOTHRESH0	=	0x402f
_AX5043_FIFOTHRESH1	=	0x402e
_AX5043_FRAMING	=	0x4012
_AX5043_FREQA0	=	0x4037
_AX5043_FREQA1	=	0x4036
_AX5043_FREQA2	=	0x4035
_AX5043_FREQA3	=	0x4034
_AX5043_FREQB0	=	0x403f
_AX5043_FREQB1	=	0x403e
_AX5043_FREQB2	=	0x403d
_AX5043_FREQB3	=	0x403c
_AX5043_FSKDEV0	=	0x4163
_AX5043_FSKDEV1	=	0x4162
_AX5043_FSKDEV2	=	0x4161
_AX5043_FSKDMAX0	=	0x410d
_AX5043_FSKDMAX1	=	0x410c
_AX5043_FSKDMIN0	=	0x410f
_AX5043_FSKDMIN1	=	0x410e
_AX5043_GPADC13VALUE0	=	0x4309
_AX5043_GPADC13VALUE1	=	0x4308
_AX5043_GPADCCTRL	=	0x4300
_AX5043_GPADCPERIOD	=	0x4301
_AX5043_IFFREQ0	=	0x4101
_AX5043_IFFREQ1	=	0x4100
_AX5043_IRQINVERSION0	=	0x400b
_AX5043_IRQINVERSION1	=	0x400a
_AX5043_IRQMASK0	=	0x4007
_AX5043_IRQMASK1	=	0x4006
_AX5043_IRQREQUEST0	=	0x400d
_AX5043_IRQREQUEST1	=	0x400c
_AX5043_LPOSCCONFIG	=	0x4310
_AX5043_LPOSCFREQ0	=	0x4317
_AX5043_LPOSCFREQ1	=	0x4316
_AX5043_LPOSCKFILT0	=	0x4313
_AX5043_LPOSCKFILT1	=	0x4312
_AX5043_LPOSCPER0	=	0x4319
_AX5043_LPOSCPER1	=	0x4318
_AX5043_LPOSCREF0	=	0x4315
_AX5043_LPOSCREF1	=	0x4314
_AX5043_LPOSCSTATUS	=	0x4311
_AX5043_MATCH0LEN	=	0x4214
_AX5043_MATCH0MAX	=	0x4216
_AX5043_MATCH0MIN	=	0x4215
_AX5043_MATCH0PAT0	=	0x4213
_AX5043_MATCH0PAT1	=	0x4212
_AX5043_MATCH0PAT2	=	0x4211
_AX5043_MATCH0PAT3	=	0x4210
_AX5043_MATCH1LEN	=	0x421c
_AX5043_MATCH1MAX	=	0x421e
_AX5043_MATCH1MIN	=	0x421d
_AX5043_MATCH1PAT0	=	0x4219
_AX5043_MATCH1PAT1	=	0x4218
_AX5043_MAXDROFFSET0	=	0x4108
_AX5043_MAXDROFFSET1	=	0x4107
_AX5043_MAXDROFFSET2	=	0x4106
_AX5043_MAXRFOFFSET0	=	0x410b
_AX5043_MAXRFOFFSET1	=	0x410a
_AX5043_MAXRFOFFSET2	=	0x4109
_AX5043_MODCFGA	=	0x4164
_AX5043_MODCFGF	=	0x4160
_AX5043_MODULATION	=	0x4010
_AX5043_PINFUNCANTSEL	=	0x4025
_AX5043_PINFUNCDATA	=	0x4023
_AX5043_PINFUNCDCLK	=	0x4022
_AX5043_PINFUNCIRQ	=	0x4024
_AX5043_PINFUNCPWRAMP	=	0x4026
_AX5043_PINFUNCSYSCLK	=	0x4021
_AX5043_PINSTATE	=	0x4020
_AX5043_PKTACCEPTFLAGS	=	0x4233
_AX5043_PKTCHUNKSIZE	=	0x4230
_AX5043_PKTMISCFLAGS	=	0x4231
_AX5043_PKTSTOREFLAGS	=	0x4232
_AX5043_PLLCPI	=	0x4031
_AX5043_PLLCPIBOOST	=	0x4039
_AX5043_PLLLOCKDET	=	0x4182
_AX5043_PLLLOOP	=	0x4030
_AX5043_PLLLOOPBOOST	=	0x4038
_AX5043_PLLRANGINGA	=	0x4033
_AX5043_PLLRANGINGB	=	0x403b
_AX5043_PLLRNGCLK	=	0x4183
_AX5043_PLLVCODIV	=	0x4032
_AX5043_PLLVCOI	=	0x4180
_AX5043_PLLVCOIR	=	0x4181
_AX5043_POWIRQMASK	=	0x4005
_AX5043_POWSTAT	=	0x4003
_AX5043_POWSTICKYSTAT	=	0x4004
_AX5043_PWRAMP	=	0x4027
_AX5043_PWRMODE	=	0x4002
_AX5043_RADIOEVENTMASK0	=	0x4009
_AX5043_RADIOEVENTMASK1	=	0x4008
_AX5043_RADIOEVENTREQ0	=	0x400f
_AX5043_RADIOEVENTREQ1	=	0x400e
_AX5043_RADIOSTATE	=	0x401c
_AX5043_RSSI	=	0x4040
_AX5043_RSSIABSTHR	=	0x422d
_AX5043_RSSIREFERENCE	=	0x422c
_AX5043_RXDATARATE0	=	0x4105
_AX5043_RXDATARATE1	=	0x4104
_AX5043_RXDATARATE2	=	0x4103
_AX5043_SCRATCH	=	0x4001
_AX5043_SILICONREVISION	=	0x4000
_AX5043_TIMER0	=	0x405b
_AX5043_TIMER1	=	0x405a
_AX5043_TIMER2	=	0x4059
_AX5043_TMGRXAGC	=	0x4227
_AX5043_TMGRXBOOST	=	0x4223
_AX5043_TMGRXCOARSEAGC	=	0x4226
_AX5043_TMGRXOFFSACQ	=	0x4225
_AX5043_TMGRXPREAMBLE1	=	0x4229
_AX5043_TMGRXPREAMBLE2	=	0x422a
_AX5043_TMGRXPREAMBLE3	=	0x422b
_AX5043_TMGRXRSSI	=	0x4228
_AX5043_TMGRXSETTLE	=	0x4224
_AX5043_TMGTXBOOST	=	0x4220
_AX5043_TMGTXSETTLE	=	0x4221
_AX5043_TRKAFSKDEMOD0	=	0x4055
_AX5043_TRKAFSKDEMOD1	=	0x4054
_AX5043_TRKAMPLITUDE0	=	0x4049
_AX5043_TRKAMPLITUDE1	=	0x4048
_AX5043_TRKDATARATE0	=	0x4047
_AX5043_TRKDATARATE1	=	0x4046
_AX5043_TRKDATARATE2	=	0x4045
_AX5043_TRKFREQ0	=	0x4051
_AX5043_TRKFREQ1	=	0x4050
_AX5043_TRKFSKDEMOD0	=	0x4053
_AX5043_TRKFSKDEMOD1	=	0x4052
_AX5043_TRKPHASE0	=	0x404b
_AX5043_TRKPHASE1	=	0x404a
_AX5043_TRKRFFREQ0	=	0x404f
_AX5043_TRKRFFREQ1	=	0x404e
_AX5043_TRKRFFREQ2	=	0x404d
_AX5043_TXPWRCOEFFA0	=	0x4169
_AX5043_TXPWRCOEFFA1	=	0x4168
_AX5043_TXPWRCOEFFB0	=	0x416b
_AX5043_TXPWRCOEFFB1	=	0x416a
_AX5043_TXPWRCOEFFC0	=	0x416d
_AX5043_TXPWRCOEFFC1	=	0x416c
_AX5043_TXPWRCOEFFD0	=	0x416f
_AX5043_TXPWRCOEFFD1	=	0x416e
_AX5043_TXPWRCOEFFE0	=	0x4171
_AX5043_TXPWRCOEFFE1	=	0x4170
_AX5043_TXRATE0	=	0x4167
_AX5043_TXRATE1	=	0x4166
_AX5043_TXRATE2	=	0x4165
_AX5043_WAKEUP0	=	0x406b
_AX5043_WAKEUP1	=	0x406a
_AX5043_WAKEUPFREQ0	=	0x406d
_AX5043_WAKEUPFREQ1	=	0x406c
_AX5043_WAKEUPTIMER0	=	0x4069
_AX5043_WAKEUPTIMER1	=	0x4068
_AX5043_WAKEUPXOEARLY	=	0x406e
_AX5043_XTALCAP	=	0x4184
_AX5043_XTALSTATUS	=	0x401d
_AX5043_AGCAHYST0	=	0x4122
_AX5043_AGCAHYST1	=	0x4132
_AX5043_AGCAHYST2	=	0x4142
_AX5043_AGCAHYST3	=	0x4152
_AX5043_AGCGAIN0	=	0x4120
_AX5043_AGCGAIN1	=	0x4130
_AX5043_AGCGAIN2	=	0x4140
_AX5043_AGCGAIN3	=	0x4150
_AX5043_AGCMINMAX0	=	0x4123
_AX5043_AGCMINMAX1	=	0x4133
_AX5043_AGCMINMAX2	=	0x4143
_AX5043_AGCMINMAX3	=	0x4153
_AX5043_AGCTARGET0	=	0x4121
_AX5043_AGCTARGET1	=	0x4131
_AX5043_AGCTARGET2	=	0x4141
_AX5043_AGCTARGET3	=	0x4151
_AX5043_AMPLITUDEGAIN0	=	0x412b
_AX5043_AMPLITUDEGAIN1	=	0x413b
_AX5043_AMPLITUDEGAIN2	=	0x414b
_AX5043_AMPLITUDEGAIN3	=	0x415b
_AX5043_BBOFFSRES0	=	0x412f
_AX5043_BBOFFSRES1	=	0x413f
_AX5043_BBOFFSRES2	=	0x414f
_AX5043_BBOFFSRES3	=	0x415f
_AX5043_DRGAIN0	=	0x4125
_AX5043_DRGAIN1	=	0x4135
_AX5043_DRGAIN2	=	0x4145
_AX5043_DRGAIN3	=	0x4155
_AX5043_FOURFSK0	=	0x412e
_AX5043_FOURFSK1	=	0x413e
_AX5043_FOURFSK2	=	0x414e
_AX5043_FOURFSK3	=	0x415e
_AX5043_FREQDEV00	=	0x412d
_AX5043_FREQDEV01	=	0x413d
_AX5043_FREQDEV02	=	0x414d
_AX5043_FREQDEV03	=	0x415d
_AX5043_FREQDEV10	=	0x412c
_AX5043_FREQDEV11	=	0x413c
_AX5043_FREQDEV12	=	0x414c
_AX5043_FREQDEV13	=	0x415c
_AX5043_FREQUENCYGAINA0	=	0x4127
_AX5043_FREQUENCYGAINA1	=	0x4137
_AX5043_FREQUENCYGAINA2	=	0x4147
_AX5043_FREQUENCYGAINA3	=	0x4157
_AX5043_FREQUENCYGAINB0	=	0x4128
_AX5043_FREQUENCYGAINB1	=	0x4138
_AX5043_FREQUENCYGAINB2	=	0x4148
_AX5043_FREQUENCYGAINB3	=	0x4158
_AX5043_FREQUENCYGAINC0	=	0x4129
_AX5043_FREQUENCYGAINC1	=	0x4139
_AX5043_FREQUENCYGAINC2	=	0x4149
_AX5043_FREQUENCYGAINC3	=	0x4159
_AX5043_FREQUENCYGAIND0	=	0x412a
_AX5043_FREQUENCYGAIND1	=	0x413a
_AX5043_FREQUENCYGAIND2	=	0x414a
_AX5043_FREQUENCYGAIND3	=	0x415a
_AX5043_FREQUENCYLEAK	=	0x4116
_AX5043_PHASEGAIN0	=	0x4126
_AX5043_PHASEGAIN1	=	0x4136
_AX5043_PHASEGAIN2	=	0x4146
_AX5043_PHASEGAIN3	=	0x4156
_AX5043_PKTADDR0	=	0x4207
_AX5043_PKTADDR1	=	0x4206
_AX5043_PKTADDR2	=	0x4205
_AX5043_PKTADDR3	=	0x4204
_AX5043_PKTADDRCFG	=	0x4200
_AX5043_PKTADDRMASK0	=	0x420b
_AX5043_PKTADDRMASK1	=	0x420a
_AX5043_PKTADDRMASK2	=	0x4209
_AX5043_PKTADDRMASK3	=	0x4208
_AX5043_PKTLENCFG	=	0x4201
_AX5043_PKTLENOFFSET	=	0x4202
_AX5043_PKTMAXLEN	=	0x4203
_AX5043_RXPARAMCURSET	=	0x4118
_AX5043_RXPARAMSETS	=	0x4117
_AX5043_TIMEGAIN0	=	0x4124
_AX5043_TIMEGAIN1	=	0x4134
_AX5043_TIMEGAIN2	=	0x4144
_AX5043_TIMEGAIN3	=	0x4154
_AX5043_AFSKCTRLNB	=	0x5114
_AX5043_AFSKMARK0NB	=	0x5113
_AX5043_AFSKMARK1NB	=	0x5112
_AX5043_AFSKSPACE0NB	=	0x5111
_AX5043_AFSKSPACE1NB	=	0x5110
_AX5043_AGCCOUNTERNB	=	0x5043
_AX5043_AMPLFILTERNB	=	0x5115
_AX5043_BBOFFSCAPNB	=	0x5189
_AX5043_BBTUNENB	=	0x5188
_AX5043_BGNDRSSINB	=	0x5041
_AX5043_BGNDRSSIGAINNB	=	0x522e
_AX5043_BGNDRSSITHRNB	=	0x522f
_AX5043_CRCINIT0NB	=	0x5017
_AX5043_CRCINIT1NB	=	0x5016
_AX5043_CRCINIT2NB	=	0x5015
_AX5043_CRCINIT3NB	=	0x5014
_AX5043_DACCONFIGNB	=	0x5332
_AX5043_DACVALUE0NB	=	0x5331
_AX5043_DACVALUE1NB	=	0x5330
_AX5043_DECIMATIONNB	=	0x5102
_AX5043_DIVERSITYNB	=	0x5042
_AX5043_ENCODINGNB	=	0x5011
_AX5043_FECNB	=	0x5018
_AX5043_FECSTATUSNB	=	0x501a
_AX5043_FECSYNCNB	=	0x5019
_AX5043_FIFOCOUNT0NB	=	0x502b
_AX5043_FIFOCOUNT1NB	=	0x502a
_AX5043_FIFODATANB	=	0x5029
_AX5043_FIFOFREE0NB	=	0x502d
_AX5043_FIFOFREE1NB	=	0x502c
_AX5043_FIFOSTATNB	=	0x5028
_AX5043_FIFOTHRESH0NB	=	0x502f
_AX5043_FIFOTHRESH1NB	=	0x502e
_AX5043_FRAMINGNB	=	0x5012
_AX5043_FREQA0NB	=	0x5037
_AX5043_FREQA1NB	=	0x5036
_AX5043_FREQA2NB	=	0x5035
_AX5043_FREQA3NB	=	0x5034
_AX5043_FREQB0NB	=	0x503f
_AX5043_FREQB1NB	=	0x503e
_AX5043_FREQB2NB	=	0x503d
_AX5043_FREQB3NB	=	0x503c
_AX5043_FSKDEV0NB	=	0x5163
_AX5043_FSKDEV1NB	=	0x5162
_AX5043_FSKDEV2NB	=	0x5161
_AX5043_FSKDMAX0NB	=	0x510d
_AX5043_FSKDMAX1NB	=	0x510c
_AX5043_FSKDMIN0NB	=	0x510f
_AX5043_FSKDMIN1NB	=	0x510e
_AX5043_GPADC13VALUE0NB	=	0x5309
_AX5043_GPADC13VALUE1NB	=	0x5308
_AX5043_GPADCCTRLNB	=	0x5300
_AX5043_GPADCPERIODNB	=	0x5301
_AX5043_IFFREQ0NB	=	0x5101
_AX5043_IFFREQ1NB	=	0x5100
_AX5043_IRQINVERSION0NB	=	0x500b
_AX5043_IRQINVERSION1NB	=	0x500a
_AX5043_IRQMASK0NB	=	0x5007
_AX5043_IRQMASK1NB	=	0x5006
_AX5043_IRQREQUEST0NB	=	0x500d
_AX5043_IRQREQUEST1NB	=	0x500c
_AX5043_LPOSCCONFIGNB	=	0x5310
_AX5043_LPOSCFREQ0NB	=	0x5317
_AX5043_LPOSCFREQ1NB	=	0x5316
_AX5043_LPOSCKFILT0NB	=	0x5313
_AX5043_LPOSCKFILT1NB	=	0x5312
_AX5043_LPOSCPER0NB	=	0x5319
_AX5043_LPOSCPER1NB	=	0x5318
_AX5043_LPOSCREF0NB	=	0x5315
_AX5043_LPOSCREF1NB	=	0x5314
_AX5043_LPOSCSTATUSNB	=	0x5311
_AX5043_MATCH0LENNB	=	0x5214
_AX5043_MATCH0MAXNB	=	0x5216
_AX5043_MATCH0MINNB	=	0x5215
_AX5043_MATCH0PAT0NB	=	0x5213
_AX5043_MATCH0PAT1NB	=	0x5212
_AX5043_MATCH0PAT2NB	=	0x5211
_AX5043_MATCH0PAT3NB	=	0x5210
_AX5043_MATCH1LENNB	=	0x521c
_AX5043_MATCH1MAXNB	=	0x521e
_AX5043_MATCH1MINNB	=	0x521d
_AX5043_MATCH1PAT0NB	=	0x5219
_AX5043_MATCH1PAT1NB	=	0x5218
_AX5043_MAXDROFFSET0NB	=	0x5108
_AX5043_MAXDROFFSET1NB	=	0x5107
_AX5043_MAXDROFFSET2NB	=	0x5106
_AX5043_MAXRFOFFSET0NB	=	0x510b
_AX5043_MAXRFOFFSET1NB	=	0x510a
_AX5043_MAXRFOFFSET2NB	=	0x5109
_AX5043_MODCFGANB	=	0x5164
_AX5043_MODCFGFNB	=	0x5160
_AX5043_MODCFGPNB	=	0x5f5f
_AX5043_MODULATIONNB	=	0x5010
_AX5043_PINFUNCANTSELNB	=	0x5025
_AX5043_PINFUNCDATANB	=	0x5023
_AX5043_PINFUNCDCLKNB	=	0x5022
_AX5043_PINFUNCIRQNB	=	0x5024
_AX5043_PINFUNCPWRAMPNB	=	0x5026
_AX5043_PINFUNCSYSCLKNB	=	0x5021
_AX5043_PINSTATENB	=	0x5020
_AX5043_PKTACCEPTFLAGSNB	=	0x5233
_AX5043_PKTCHUNKSIZENB	=	0x5230
_AX5043_PKTMISCFLAGSNB	=	0x5231
_AX5043_PKTSTOREFLAGSNB	=	0x5232
_AX5043_PLLCPINB	=	0x5031
_AX5043_PLLCPIBOOSTNB	=	0x5039
_AX5043_PLLLOCKDETNB	=	0x5182
_AX5043_PLLLOOPNB	=	0x5030
_AX5043_PLLLOOPBOOSTNB	=	0x5038
_AX5043_PLLRANGINGANB	=	0x5033
_AX5043_PLLRANGINGBNB	=	0x503b
_AX5043_PLLRNGCLKNB	=	0x5183
_AX5043_PLLVCODIVNB	=	0x5032
_AX5043_PLLVCOINB	=	0x5180
_AX5043_PLLVCOIRNB	=	0x5181
_AX5043_POWCTRL1NB	=	0x5f08
_AX5043_POWIRQMASKNB	=	0x5005
_AX5043_POWSTATNB	=	0x5003
_AX5043_POWSTICKYSTATNB	=	0x5004
_AX5043_PWRAMPNB	=	0x5027
_AX5043_PWRMODENB	=	0x5002
_AX5043_RADIOEVENTMASK0NB	=	0x5009
_AX5043_RADIOEVENTMASK1NB	=	0x5008
_AX5043_RADIOEVENTREQ0NB	=	0x500f
_AX5043_RADIOEVENTREQ1NB	=	0x500e
_AX5043_RADIOSTATENB	=	0x501c
_AX5043_REFNB	=	0x5f0d
_AX5043_RSSINB	=	0x5040
_AX5043_RSSIABSTHRNB	=	0x522d
_AX5043_RSSIREFERENCENB	=	0x522c
_AX5043_RXDATARATE0NB	=	0x5105
_AX5043_RXDATARATE1NB	=	0x5104
_AX5043_RXDATARATE2NB	=	0x5103
_AX5043_SCRATCHNB	=	0x5001
_AX5043_SILICONREVISIONNB	=	0x5000
_AX5043_TIMER0NB	=	0x505b
_AX5043_TIMER1NB	=	0x505a
_AX5043_TIMER2NB	=	0x5059
_AX5043_TMGRXAGCNB	=	0x5227
_AX5043_TMGRXBOOSTNB	=	0x5223
_AX5043_TMGRXCOARSEAGCNB	=	0x5226
_AX5043_TMGRXOFFSACQNB	=	0x5225
_AX5043_TMGRXPREAMBLE1NB	=	0x5229
_AX5043_TMGRXPREAMBLE2NB	=	0x522a
_AX5043_TMGRXPREAMBLE3NB	=	0x522b
_AX5043_TMGRXRSSINB	=	0x5228
_AX5043_TMGRXSETTLENB	=	0x5224
_AX5043_TMGTXBOOSTNB	=	0x5220
_AX5043_TMGTXSETTLENB	=	0x5221
_AX5043_TRKAFSKDEMOD0NB	=	0x5055
_AX5043_TRKAFSKDEMOD1NB	=	0x5054
_AX5043_TRKAMPLITUDE0NB	=	0x5049
_AX5043_TRKAMPLITUDE1NB	=	0x5048
_AX5043_TRKDATARATE0NB	=	0x5047
_AX5043_TRKDATARATE1NB	=	0x5046
_AX5043_TRKDATARATE2NB	=	0x5045
_AX5043_TRKFREQ0NB	=	0x5051
_AX5043_TRKFREQ1NB	=	0x5050
_AX5043_TRKFSKDEMOD0NB	=	0x5053
_AX5043_TRKFSKDEMOD1NB	=	0x5052
_AX5043_TRKPHASE0NB	=	0x504b
_AX5043_TRKPHASE1NB	=	0x504a
_AX5043_TRKRFFREQ0NB	=	0x504f
_AX5043_TRKRFFREQ1NB	=	0x504e
_AX5043_TRKRFFREQ2NB	=	0x504d
_AX5043_TXPWRCOEFFA0NB	=	0x5169
_AX5043_TXPWRCOEFFA1NB	=	0x5168
_AX5043_TXPWRCOEFFB0NB	=	0x516b
_AX5043_TXPWRCOEFFB1NB	=	0x516a
_AX5043_TXPWRCOEFFC0NB	=	0x516d
_AX5043_TXPWRCOEFFC1NB	=	0x516c
_AX5043_TXPWRCOEFFD0NB	=	0x516f
_AX5043_TXPWRCOEFFD1NB	=	0x516e
_AX5043_TXPWRCOEFFE0NB	=	0x5171
_AX5043_TXPWRCOEFFE1NB	=	0x5170
_AX5043_TXRATE0NB	=	0x5167
_AX5043_TXRATE1NB	=	0x5166
_AX5043_TXRATE2NB	=	0x5165
_AX5043_WAKEUP0NB	=	0x506b
_AX5043_WAKEUP1NB	=	0x506a
_AX5043_WAKEUPFREQ0NB	=	0x506d
_AX5043_WAKEUPFREQ1NB	=	0x506c
_AX5043_WAKEUPTIMER0NB	=	0x5069
_AX5043_WAKEUPTIMER1NB	=	0x5068
_AX5043_WAKEUPXOEARLYNB	=	0x506e
_AX5043_XTALAMPLNB	=	0x5f11
_AX5043_XTALCAPNB	=	0x5184
_AX5043_XTALOSCNB	=	0x5f10
_AX5043_XTALSTATUSNB	=	0x501d
_AX5043_0xF00NB	=	0x5f00
_AX5043_0xF0CNB	=	0x5f0c
_AX5043_0xF18NB	=	0x5f18
_AX5043_0xF1CNB	=	0x5f1c
_AX5043_0xF21NB	=	0x5f21
_AX5043_0xF22NB	=	0x5f22
_AX5043_0xF23NB	=	0x5f23
_AX5043_0xF26NB	=	0x5f26
_AX5043_0xF30NB	=	0x5f30
_AX5043_0xF31NB	=	0x5f31
_AX5043_0xF32NB	=	0x5f32
_AX5043_0xF33NB	=	0x5f33
_AX5043_0xF34NB	=	0x5f34
_AX5043_0xF35NB	=	0x5f35
_AX5043_0xF44NB	=	0x5f44
_AX5043_AGCAHYST0NB	=	0x5122
_AX5043_AGCAHYST1NB	=	0x5132
_AX5043_AGCAHYST2NB	=	0x5142
_AX5043_AGCAHYST3NB	=	0x5152
_AX5043_AGCGAIN0NB	=	0x5120
_AX5043_AGCGAIN1NB	=	0x5130
_AX5043_AGCGAIN2NB	=	0x5140
_AX5043_AGCGAIN3NB	=	0x5150
_AX5043_AGCMINMAX0NB	=	0x5123
_AX5043_AGCMINMAX1NB	=	0x5133
_AX5043_AGCMINMAX2NB	=	0x5143
_AX5043_AGCMINMAX3NB	=	0x5153
_AX5043_AGCTARGET0NB	=	0x5121
_AX5043_AGCTARGET1NB	=	0x5131
_AX5043_AGCTARGET2NB	=	0x5141
_AX5043_AGCTARGET3NB	=	0x5151
_AX5043_AMPLITUDEGAIN0NB	=	0x512b
_AX5043_AMPLITUDEGAIN1NB	=	0x513b
_AX5043_AMPLITUDEGAIN2NB	=	0x514b
_AX5043_AMPLITUDEGAIN3NB	=	0x515b
_AX5043_BBOFFSRES0NB	=	0x512f
_AX5043_BBOFFSRES1NB	=	0x513f
_AX5043_BBOFFSRES2NB	=	0x514f
_AX5043_BBOFFSRES3NB	=	0x515f
_AX5043_DRGAIN0NB	=	0x5125
_AX5043_DRGAIN1NB	=	0x5135
_AX5043_DRGAIN2NB	=	0x5145
_AX5043_DRGAIN3NB	=	0x5155
_AX5043_FOURFSK0NB	=	0x512e
_AX5043_FOURFSK1NB	=	0x513e
_AX5043_FOURFSK2NB	=	0x514e
_AX5043_FOURFSK3NB	=	0x515e
_AX5043_FREQDEV00NB	=	0x512d
_AX5043_FREQDEV01NB	=	0x513d
_AX5043_FREQDEV02NB	=	0x514d
_AX5043_FREQDEV03NB	=	0x515d
_AX5043_FREQDEV10NB	=	0x512c
_AX5043_FREQDEV11NB	=	0x513c
_AX5043_FREQDEV12NB	=	0x514c
_AX5043_FREQDEV13NB	=	0x515c
_AX5043_FREQUENCYGAINA0NB	=	0x5127
_AX5043_FREQUENCYGAINA1NB	=	0x5137
_AX5043_FREQUENCYGAINA2NB	=	0x5147
_AX5043_FREQUENCYGAINA3NB	=	0x5157
_AX5043_FREQUENCYGAINB0NB	=	0x5128
_AX5043_FREQUENCYGAINB1NB	=	0x5138
_AX5043_FREQUENCYGAINB2NB	=	0x5148
_AX5043_FREQUENCYGAINB3NB	=	0x5158
_AX5043_FREQUENCYGAINC0NB	=	0x5129
_AX5043_FREQUENCYGAINC1NB	=	0x5139
_AX5043_FREQUENCYGAINC2NB	=	0x5149
_AX5043_FREQUENCYGAINC3NB	=	0x5159
_AX5043_FREQUENCYGAIND0NB	=	0x512a
_AX5043_FREQUENCYGAIND1NB	=	0x513a
_AX5043_FREQUENCYGAIND2NB	=	0x514a
_AX5043_FREQUENCYGAIND3NB	=	0x515a
_AX5043_FREQUENCYLEAKNB	=	0x5116
_AX5043_PHASEGAIN0NB	=	0x5126
_AX5043_PHASEGAIN1NB	=	0x5136
_AX5043_PHASEGAIN2NB	=	0x5146
_AX5043_PHASEGAIN3NB	=	0x5156
_AX5043_PKTADDR0NB	=	0x5207
_AX5043_PKTADDR1NB	=	0x5206
_AX5043_PKTADDR2NB	=	0x5205
_AX5043_PKTADDR3NB	=	0x5204
_AX5043_PKTADDRCFGNB	=	0x5200
_AX5043_PKTADDRMASK0NB	=	0x520b
_AX5043_PKTADDRMASK1NB	=	0x520a
_AX5043_PKTADDRMASK2NB	=	0x5209
_AX5043_PKTADDRMASK3NB	=	0x5208
_AX5043_PKTLENCFGNB	=	0x5201
_AX5043_PKTLENOFFSETNB	=	0x5202
_AX5043_PKTMAXLENNB	=	0x5203
_AX5043_RXPARAMCURSETNB	=	0x5118
_AX5043_RXPARAMSETSNB	=	0x5117
_AX5043_TIMEGAIN0NB	=	0x5124
_AX5043_TIMEGAIN1NB	=	0x5134
_AX5043_TIMEGAIN2NB	=	0x5144
_AX5043_TIMEGAIN3NB	=	0x5154
_AX5043_0xF00	=	0x4f00
_AX5043_0xF0C	=	0x4f0c
_AX5043_0xF10	=	0x4f10
_AX5043_0xF11	=	0x4f11
_AX5043_0xF18	=	0x4f18
_AX5043_0xF1C	=	0x4f1c
_AX5043_0xF21	=	0x4f21
_AX5043_0xF22	=	0x4f22
_AX5043_0xF23	=	0x4f23
_AX5043_0xF26	=	0x4f26
_AX5043_0xF30	=	0x4f30
_AX5043_0xF31	=	0x4f31
_AX5043_0xF32	=	0x4f32
_AX5043_0xF33	=	0x4f33
_AX5043_0xF34	=	0x4f34
_AX5043_0xF35	=	0x4f35
_AX5043_0xF44	=	0x4f44
_AX5043_REF	=	0x4f0d
_AX5043_POWCTRL1	=	0x4f08
_AX5043_MODCFGP	=	0x4f5f
_AX5043_XTALOSC	=	0x4f10
_AX5043_XTALAMPL	=	0x4f11
_axradio_syncstate::
	.ds 1
_axradio_txbuffer_len::
	.ds 2
_axradio_txbuffer_cnt::
	.ds 2
_axradio_curchannel::
	.ds 1
_axradio_curfreqoffset::
	.ds 4
_axradio_ack_count::
	.ds 1
_axradio_ack_seqnr::
	.ds 1
_axradio_sync_time::
	.ds 4
_axradio_sync_periodcorr::
	.ds 2
_axradio_timeanchor::
	.ds 8
_axradio_localaddr::
	.ds 8
_axradio_default_remoteaddr::
	.ds 4
_axradio_txbuffer::
	.ds 260
_axradio_rxbuffer::
	.ds 260
_axradio_cb_receive::
	.ds 34
_axradio_cb_receivesfd::
	.ds 10
_axradio_cb_channelstate::
	.ds 13
_axradio_cb_transmitstart::
	.ds 10
_axradio_cb_transmitend::
	.ds 10
_axradio_cb_transmitdata::
	.ds 10
_axradio_timer::
	.ds 8
_receive_isr_BeaconRx_65536_259:
	.ds 29
_axradio_init_x_196608_424:
	.ds 1
_axradio_init_j_196608_425:
	.ds 1
_axradio_init_x_458752_430:
	.ds 1
_axradio_set_mode_mode_65536_436:
	.ds 1
_axradio_set_channel_freq_65536_447:
	.ds 4
_axradio_get_pllvcoi_x_131072_454:
	.ds 1
_axradio_set_curfreqoffset_offs_65536_457:
	.ds 4
_axradio_set_local_address_addr_65536_459:
	.ds 3
_axradio_set_default_remote_address_addr_65536_461:
	.ds 3
_axradio_transmit_PARM_2:
	.ds 3
_axradio_transmit_PARM_3:
	.ds 2
_axradio_transmit_addr_65536_463:
	.ds 3
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area XABS    (ABS,XDATA)
;--------------------------------------------------------
; external initialized ram data
;--------------------------------------------------------
	.area XISEG   (XDATA)
_f30_saved::
	.ds 1
_f31_saved::
	.ds 1
_f32_saved::
	.ds 1
_f33_saved::
	.ds 1
	.area HOME    (CODE)
	.area GSINIT0 (CODE)
	.area GSINIT1 (CODE)
	.area GSINIT2 (CODE)
	.area GSINIT3 (CODE)
	.area GSINIT4 (CODE)
	.area GSINIT5 (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area CSEG    (CODE)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME    (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area GSINIT  (CODE)
;	..\src\COMMON\easyax5043.c:60: volatile uint8_t __data axradio_mode = AXRADIO_MODE_UNINIT;
	mov	_axradio_mode,#0x00
;	..\src\COMMON\easyax5043.c:61: volatile axradio_trxstate_t __data axradio_trxstate = trxstate_off;
	mov	_axradio_trxstate,#0x00
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME    (CODE)
	.area HOME    (CODE)
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CSEG    (CODE)
;------------------------------------------------------------
;Allocation info for local variables in function 'update_timeanchor'
;------------------------------------------------------------
;iesave                    Allocated to registers r7 
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:239: static __reentrantb void update_timeanchor(void) __reentrant
;	-----------------------------------------
;	 function update_timeanchor
;	-----------------------------------------
_update_timeanchor:
	ar7 = 0x07
	ar6 = 0x06
	ar5 = 0x05
	ar4 = 0x04
	ar3 = 0x03
	ar2 = 0x02
	ar1 = 0x01
	ar0 = 0x00
;	..\src\COMMON\easyax5043.c:241: uint8_t iesave = IE & 0x80;
	mov	r7,_IE
	anl	ar7,#0x80
;	..\src\COMMON\easyax5043.c:242: EA = 0;
;	assignBit
	clr	_EA
;	..\src\COMMON\easyax5043.c:243: axradio_timeanchor.timer0 = wtimer0_curtime();
	push	ar7
	lcall	_wtimer0_curtime
	mov	r3,dpl
	mov	r4,dph
	mov	r5,b
	mov	r6,a
	pop	ar7
	mov	dptr,#_axradio_timeanchor
	mov	a,r3
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:244: axradio_timeanchor.radiotimer = radio_read24((uint16_t)&AX5043_TIMER2);
	mov	r5,#_AX5043_TIMER2
	mov	r6,#(_AX5043_TIMER2 >> 8)
	mov	dpl,r5
	mov	dph,r6
	lcall	_radio_read24
	mov	r3,dpl
	mov	r4,dph
	mov	r5,b
	mov	r6,a
	mov	dptr,#(_axradio_timeanchor + 0x0004)
	mov	a,r3
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:245: IE |= iesave;
	mov	a,r7
	orl	_IE,a
;	..\src\COMMON\easyax5043.c:246: }
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'axradio_conv_time_totimer0'
;------------------------------------------------------------
;dt                        Allocated to registers r4 r5 r6 r7 
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:248: __reentrantb uint32_t axradio_conv_time_totimer0(uint32_t dt) __reentrant
;	-----------------------------------------
;	 function axradio_conv_time_totimer0
;	-----------------------------------------
_axradio_conv_time_totimer0:
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
;	..\src\COMMON\easyax5043.c:250: dt -= axradio_timeanchor.radiotimer;
	mov	dptr,#(_axradio_timeanchor + 0x0004)
	movx	a,@dptr
	mov	r0,a
	inc	dptr
	movx	a,@dptr
	mov	r1,a
	inc	dptr
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	mov	a,r4
	clr	c
	subb	a,r0
	mov	r0,a
	mov	a,r5
	subb	a,r1
	mov	r1,a
	mov	a,r6
	subb	a,r2
	mov	r2,a
	mov	a,r7
	subb	a,r3
;	..\src\COMMON\easyax5043.c:251: dt = axradio_conv_timeinterval_totimer0(signextend24(dt));
	mov	dpl,r0
	mov	dph,r1
	mov	b,r2
	lcall	_signextend24
	lcall	_axradio_conv_timeinterval_totimer0
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
;	..\src\COMMON\easyax5043.c:252: dt += axradio_timeanchor.timer0;
	mov	dptr,#_axradio_timeanchor
	movx	a,@dptr
	mov	r0,a
	inc	dptr
	movx	a,@dptr
	mov	r1,a
	inc	dptr
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	mov	a,r0
	add	a,r4
	mov	r4,a
	mov	a,r1
	addc	a,r5
	mov	r5,a
	mov	a,r2
	addc	a,r6
	mov	r6,a
	mov	a,r3
	addc	a,r7
;	..\src\COMMON\easyax5043.c:253: return dt;
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
;	..\src\COMMON\easyax5043.c:254: }
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'ax5043_init_registers_common'
;------------------------------------------------------------
;rng                       Allocated to registers r7 
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:256: static __reentrantb uint8_t ax5043_init_registers_common(void) __reentrant
;	-----------------------------------------
;	 function ax5043_init_registers_common
;	-----------------------------------------
_ax5043_init_registers_common:
;	..\src\COMMON\easyax5043.c:258: uint8_t rng = axradio_phy_chanpllrng[axradio_curchannel];
	mov	dptr,#_axradio_curchannel
	movx	a,@dptr
	add	a,#_axradio_phy_chanpllrng
	mov	dpl,a
	clr	a
	addc	a,#(_axradio_phy_chanpllrng >> 8)
	mov	dph,a
	movx	a,@dptr
;	..\src\COMMON\easyax5043.c:259: if (rng & 0x20)
	mov	r7,a
	jnb	acc.5,00102$
;	..\src\COMMON\easyax5043.c:260: return AXRADIO_ERR_RANGING;
	mov	dpl,#0x06
	ret
00102$:
;	..\src\COMMON\easyax5043.c:261: if (AX5043_PLLLOOP & 0x80) {
	mov	dptr,#_AX5043_PLLLOOP
	movx	a,@dptr
	jnb	acc.7,00104$
;	..\src\COMMON\easyax5043.c:262: AX5043_PLLRANGINGB = rng & 0x0F;
	mov	dptr,#_AX5043_PLLRANGINGB
	mov	a,#0x0f
	anl	a,r7
	movx	@dptr,a
	sjmp	00105$
00104$:
;	..\src\COMMON\easyax5043.c:264: AX5043_PLLRANGINGA = rng & 0x0F;
	mov	dptr,#_AX5043_PLLRANGINGA
	mov	a,#0x0f
	anl	a,r7
	movx	@dptr,a
00105$:
;	..\src\COMMON\easyax5043.c:266: rng = axradio_get_pllvcoi();
	lcall	_axradio_get_pllvcoi
;	..\src\COMMON\easyax5043.c:267: if (rng & 0x80)
	mov	a,dpl
	mov	r7,a
	jnb	acc.7,00107$
;	..\src\COMMON\easyax5043.c:268: AX5043_PLLVCOI = rng;
	mov	dptr,#_AX5043_PLLVCOI
	mov	a,r7
	movx	@dptr,a
00107$:
;	..\src\COMMON\easyax5043.c:269: return AXRADIO_ERR_NOERROR;
	mov	dpl,#0x00
;	..\src\COMMON\easyax5043.c:270: }
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'ax5043_init_registers_tx'
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:272: __reentrantb uint8_t ax5043_init_registers_tx(void) __reentrant
;	-----------------------------------------
;	 function ax5043_init_registers_tx
;	-----------------------------------------
_ax5043_init_registers_tx:
;	..\src\COMMON\easyax5043.c:274: ax5043_set_registers_tx();
	lcall	_ax5043_set_registers_tx
;	..\src\COMMON\easyax5043.c:275: return ax5043_init_registers_common();
;	..\src\COMMON\easyax5043.c:276: }
	ljmp	_ax5043_init_registers_common
;------------------------------------------------------------
;Allocation info for local variables in function 'ax5043_init_registers_rx'
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:278: __reentrantb uint8_t ax5043_init_registers_rx(void) __reentrant
;	-----------------------------------------
;	 function ax5043_init_registers_rx
;	-----------------------------------------
_ax5043_init_registers_rx:
;	..\src\COMMON\easyax5043.c:280: ax5043_set_registers_rx();
	lcall	_ax5043_set_registers_rx
;	..\src\COMMON\easyax5043.c:281: return ax5043_init_registers_common();
;	..\src\COMMON\easyax5043.c:282: }
	ljmp	_ax5043_init_registers_common
;------------------------------------------------------------
;Allocation info for local variables in function 'receive_isr'
;------------------------------------------------------------
;fifo_cmd                  Allocated to registers r7 
;flags                     Allocated to registers 
;i                         Allocated to registers r7 
;len                       Allocated to registers r7 
;FreqOffset                Allocated to stack - _bp +5
;r                         Allocated to registers r6 
;r                         Allocated to registers r7 
;r                         Allocated to registers r7 
;BeaconRx                  Allocated with name '_receive_isr_BeaconRx_65536_259'
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:284: static __reentrantb void receive_isr(void) __reentrant
;	-----------------------------------------
;	 function receive_isr
;	-----------------------------------------
_receive_isr:
;	..\src\COMMON\easyax5043.c:288: uint8_t len = AX5043_RADIOEVENTREQ0; // clear request so interrupt does not fire again. sync_rx enables interrupt on radio state changed in order to wake up on SDF detected
	mov	dptr,#_AX5043_RADIOEVENTREQ0
	movx	a,@dptr
	mov	r7,a
;	..\src\COMMON\easyax5043.c:301: dbglink_writestr(" \n Receive isr ");
	mov	dptr,#___str_0
	mov	b,#0x80
	push	ar7
	lcall	_dbglink_writestr
	pop	ar7
;	..\src\COMMON\easyax5043.c:303: if ((len & 0x04) && AX5043_RADIOSTATE == 0x0F) {
	mov	a,r7
	jnb	acc.2,00181$
	mov	dptr,#_AX5043_RADIOSTATE
	movx	a,@dptr
	mov	r7,a
	cjne	r7,#0x0f,00181$
;	..\src\COMMON\easyax5043.c:305: update_timeanchor();
	lcall	_update_timeanchor
;	..\src\COMMON\easyax5043.c:306: if(axradio_framing_enable_sfdcallback)
	mov	dptr,#_axradio_framing_enable_sfdcallback
	clr	a
	movc	a,@a+dptr
	jz	00181$
;	..\src\COMMON\easyax5043.c:308: wtimer_remove_callback(&axradio_cb_receivesfd.cb);
	mov	dptr,#_axradio_cb_receivesfd
	lcall	_wtimer_remove_callback
;	..\src\COMMON\easyax5043.c:309: axradio_cb_receivesfd.st.error = AXRADIO_ERR_NOERROR;
	mov	dptr,#(_axradio_cb_receivesfd + 0x0005)
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:310: axradio_cb_receivesfd.st.time.t = axradio_timeanchor.radiotimer;
	mov	dptr,#(_axradio_timeanchor + 0x0004)
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#(_axradio_cb_receivesfd + 0x0006)
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:311: wtimer_add_callback(&axradio_cb_receivesfd.cb);
	mov	dptr,#_axradio_cb_receivesfd
	lcall	_wtimer_add_callback
;	..\src\COMMON\easyax5043.c:323: while (AX5043_IRQREQUEST0 & 0x01) {    // while fifo not empty
00181$:
00165$:
	mov	dptr,#_AX5043_IRQREQUEST0
	movx	a,@dptr
	jb	acc.0,00350$
	ret
00350$:
;	..\src\COMMON\easyax5043.c:325: fifo_cmd = AX5043_FIFODATA; // read command
	mov	dptr,#_AX5043_FIFODATA
	movx	a,@dptr
;	..\src\COMMON\easyax5043.c:326: len = (fifo_cmd & 0xE0) >> 5; // top 3 bits encode payload len
	mov	r7,a
	mov	r5,a
	anl	ar5,#0xe0
	clr	a
	rr	a
	xch	a,r5
	swap	a
	rr	a
	anl	a,#0x07
	xrl	a,r5
	xch	a,r5
	anl	a,#0x07
	xch	a,r5
	xrl	a,r5
	xch	a,r5
	jnb	acc.2,00351$
	orl	a,#0xf8
00351$:
	mov	r6,a
;	..\src\COMMON\easyax5043.c:327: if (len == 7)
	cjne	r5,#0x07,00107$
;	..\src\COMMON\easyax5043.c:328: len = AX5043_FIFODATA; // 7 means variable length, -> get length byte
	mov	dptr,#_AX5043_FIFODATA
	movx	a,@dptr
	mov	r5,a
00107$:
;	..\src\COMMON\easyax5043.c:329: fifo_cmd &= 0x1F;
	anl	ar7,#0x1f
;	..\src\COMMON\easyax5043.c:330: switch (fifo_cmd) {
	cjne	r7,#0x01,00354$
	sjmp	00108$
00354$:
	cjne	r7,#0x10,00355$
	ljmp	00151$
00355$:
	cjne	r7,#0x11,00356$
	ljmp	00148$
00356$:
	cjne	r7,#0x12,00357$
	ljmp	00144$
00357$:
	cjne	r7,#0x13,00358$
	ljmp	00140$
00358$:
	cjne	r7,#0x15,00359$
	ljmp	00154$
00359$:
	ljmp	00158$
;	..\src\COMMON\easyax5043.c:331: case AX5043_FIFOCMD_DATA:
00108$:
;	..\src\COMMON\easyax5043.c:333: if (!len)
	mov	a,r5
	jz	00165$
;	..\src\COMMON\easyax5043.c:336: flags = AX5043_FIFODATA;
	mov	dptr,#_AX5043_FIFODATA
	movx	a,@dptr
;	..\src\COMMON\easyax5043.c:337: --len;
	mov	a,r5
	dec	a
	mov	r7,a
;	..\src\COMMON\easyax5043.c:338: ax5043_readfifo(axradio_rxbuffer, len);
	push	ar7
	push	ar7
	mov	dptr,#_axradio_rxbuffer
	mov	b,#0x00
	lcall	_ax5043_readfifo
	dec	sp
	pop	ar7
;	..\src\COMMON\easyax5043.c:339: if(axradio_mode == AXRADIO_MODE_WOR_RECEIVE || axradio_mode == AXRADIO_MODE_WOR_ACK_RECEIVE)
	mov	a,#0x21
	cjne	a,_axradio_mode,00361$
	sjmp	00111$
00361$:
	mov	a,#0x23
	cjne	a,_axradio_mode,00112$
00111$:
;	..\src\COMMON\easyax5043.c:341: f30_saved = AX5043_0xF30;
	mov	dptr,#_AX5043_0xF30
	movx	a,@dptr
	mov	dptr,#_f30_saved
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:342: f31_saved = AX5043_0xF31;
	mov	dptr,#_AX5043_0xF31
	movx	a,@dptr
	mov	dptr,#_f31_saved
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:343: f32_saved = AX5043_0xF32;
	mov	dptr,#_AX5043_0xF32
	movx	a,@dptr
	mov	dptr,#_f32_saved
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:344: f33_saved = AX5043_0xF33;
	mov	dptr,#_AX5043_0xF33
	movx	a,@dptr
	mov	dptr,#_f33_saved
	movx	@dptr,a
00112$:
;	..\src\COMMON\easyax5043.c:346: if (axradio_mode == AXRADIO_MODE_WOR_RECEIVE ||
	mov	a,#0x21
	cjne	a,_axradio_mode,00364$
	sjmp	00114$
00364$:
;	..\src\COMMON\easyax5043.c:347: axradio_mode == AXRADIO_MODE_SYNC_SLAVE)
	mov	a,#0x32
	cjne	a,_axradio_mode,00115$
00114$:
;	..\src\COMMON\easyax5043.c:348: AX5043_PWRMODE = AX5043_PWRSTATE_POWERDOWN;
	mov	dptr,#_AX5043_PWRMODE
	clr	a
	movx	@dptr,a
00115$:
;	..\src\COMMON\easyax5043.c:349: AX5043_IRQMASK0 &= (uint8_t)~0x01; // disable FIFO not empty irq
	mov	dptr,#_AX5043_IRQMASK0
	movx	a,@dptr
	anl	acc,#0xfe
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:350: wtimer_remove_callback(&axradio_cb_receive.cb);
	mov	dptr,#_axradio_cb_receive
	push	ar7
	lcall	_wtimer_remove_callback
	pop	ar7
;	..\src\COMMON\easyax5043.c:351: axradio_cb_receive.st.error = AXRADIO_ERR_NOERROR;
	mov	dptr,#(_axradio_cb_receive + 0x0005)
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:353: axradio_cb_receive.st.rx.mac.raw = axradio_rxbuffer;
	mov	dptr,#(_axradio_cb_receive + 0x001c)
	mov	a,#_axradio_rxbuffer
	movx	@dptr,a
	mov	a,#(_axradio_rxbuffer >> 8)
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:354: if (AXRADIO_MODE_IS_STREAM_RECEIVE(axradio_mode)) {
	mov	r4,_axradio_mode
	anl	ar4,#0xf8
	mov	r6,#0x00
	cjne	r4,#0x28,00367$
	cjne	r6,#0x00,00367$
	sjmp	00368$
00367$:
	ljmp	00121$
00368$:
;	..\src\COMMON\easyax5043.c:355: axradio_cb_receive.st.rx.pktdata = axradio_rxbuffer;
	mov	dptr,#(_axradio_cb_receive + 0x001e)
	mov	a,#_axradio_rxbuffer
	movx	@dptr,a
	mov	a,#(_axradio_rxbuffer >> 8)
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:356: axradio_cb_receive.st.rx.pktlen = len;
	mov	ar4,r7
	mov	r6,#0x00
	mov	dptr,#(_axradio_cb_receive + 0x0020)
	mov	a,r4
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:358: int8_t r = AX5043_RSSI;
	mov	dptr,#_AX5043_RSSI
	movx	a,@dptr
;	..\src\COMMON\easyax5043.c:359: axradio_cb_receive.st.rx.phy.rssi = r - (int16_t)axradio_phy_rssioffset;
	mov	r6,a
	rlc	a
	subb	a,acc
	mov	r4,a
	mov	dptr,#_axradio_phy_rssioffset
	clr	a
	movc	a,@a+dptr
	mov	r3,a
	rlc	a
	subb	a,acc
	mov	r2,a
	mov	a,r6
	clr	c
	subb	a,r3
	mov	r6,a
	mov	a,r4
	subb	a,r2
	mov	r4,a
	mov	dptr,#(_axradio_cb_receive + 0x000a)
	mov	a,r6
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:361: if (axradio_phy_innerfreqloop)
	mov	dptr,#_axradio_phy_innerfreqloop
	clr	a
	movc	a,@a+dptr
	jz	00118$
;	..\src\COMMON\easyax5043.c:363: axradio_cb_receive.st.rx.phy.offset.o = axradio_conv_freq_fromreg(signextend16(radio_read16((uint16_t)&AX5043_TRKFREQ1)));
	mov	r4,#_AX5043_TRKFREQ1
	mov	r6,#(_AX5043_TRKFREQ1 >> 8)
	mov	dpl,r4
	mov	dph,r6
	lcall	_radio_read16
	lcall	_signextend16
	lcall	_axradio_conv_freq_fromreg
	mov	r2,dpl
	mov	r3,dph
	mov	r4,b
	mov	r6,a
	mov	dptr,#(_axradio_cb_receive + 0x000c)
	mov	a,r2
	movx	@dptr,a
	mov	a,r3
	inc	dptr
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	sjmp	00119$
00118$:
;	..\src\COMMON\easyax5043.c:368: axradio_cb_receive.st.rx.phy.offset.o = signextend20(radio_read24((uint16_t)&AX5043_TRKRFFREQ2));
	mov	r4,#_AX5043_TRKRFFREQ2
	mov	r6,#(_AX5043_TRKRFFREQ2 >> 8)
	mov	dpl,r4
	mov	dph,r6
	lcall	_radio_read24
	lcall	_signextend20
	mov	r2,dpl
	mov	r3,dph
	mov	r4,b
	mov	r6,a
	mov	dptr,#(_axradio_cb_receive + 0x000c)
	mov	a,r2
	movx	@dptr,a
	mov	a,r3
	inc	dptr
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
00119$:
;	..\src\COMMON\easyax5043.c:370: wtimer_add_callback(&axradio_cb_receive.cb);
	mov	dptr,#_axradio_cb_receive
	lcall	_wtimer_add_callback
;	..\src\COMMON\easyax5043.c:371: break;
	ljmp	00165$
00121$:
;	..\src\COMMON\easyax5043.c:373: axradio_cb_receive.st.rx.pktdata = &axradio_rxbuffer[axradio_framing_maclen];
	mov	dptr,#_axradio_framing_maclen
	clr	a
	movc	a,@a+dptr
	mov	r6,a
	add	a,#_axradio_rxbuffer
	mov	r3,a
	clr	a
	addc	a,#(_axradio_rxbuffer >> 8)
	mov	r4,a
	mov	dptr,#(_axradio_cb_receive + 0x001e)
	mov	a,r3
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:374: if (len < axradio_framing_maclen) {
	clr	c
	mov	a,r7
	subb	a,r6
	jnc	00126$
;	..\src\COMMON\easyax5043.c:375: len = 0;
	mov	r7,#0x00
;	..\src\COMMON\easyax5043.c:376: axradio_cb_receive.st.rx.pktlen = 0;
	mov	dptr,#(_axradio_cb_receive + 0x0020)
	clr	a
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
	sjmp	00127$
00126$:
;	..\src\COMMON\easyax5043.c:378: len -= axradio_framing_maclen;
	mov	a,r7
	clr	c
	subb	a,r6
;	..\src\COMMON\easyax5043.c:379: axradio_cb_receive.st.rx.pktlen = len;
	mov	r7,a
	mov	r4,a
	mov	r6,#0x00
	mov	dptr,#(_axradio_cb_receive + 0x0020)
	mov	a,r4
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:380: wtimer_add_callback(&axradio_cb_receive.cb);
	mov	dptr,#_axradio_cb_receive
	push	ar7
	lcall	_wtimer_add_callback
	pop	ar7
;	..\src\COMMON\easyax5043.c:381: if (axradio_mode == AXRADIO_MODE_SYNC_SLAVE ||
	mov	a,#0x32
	cjne	a,_axradio_mode,00371$
	sjmp	00122$
00371$:
;	..\src\COMMON\easyax5043.c:382: axradio_mode == AXRADIO_MODE_SYNC_ACK_SLAVE)
	mov	a,#0x33
	cjne	a,_axradio_mode,00127$
00122$:
;	..\src\COMMON\easyax5043.c:383: wtimer_remove(&axradio_timer);
	mov	dptr,#_axradio_timer
	push	ar7
	lcall	_wtimer_remove
	pop	ar7
00127$:
;	..\src\COMMON\easyax5043.c:386: BEACON_decoding(axradio_rxbuffer+3,&BeaconRx,len);
	mov	dptr,#_BEACON_decoding_PARM_2
	mov	a,#_receive_isr_BeaconRx_65536_259
	movx	@dptr,a
	mov	a,#(_receive_isr_BeaconRx_65536_259 >> 8)
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_BEACON_decoding_PARM_3
	mov	a,r7
	movx	@dptr,a
	mov	dptr,#(_axradio_rxbuffer + 0x0003)
	mov	b,#0x00
	push	ar7
	lcall	_BEACON_decoding
	pop	ar7
;	..\src\COMMON\easyax5043.c:388: if(DownLinkIsrFlag)
	mov	dptr,#_DownLinkIsrFlag
	movx	a,@dptr
	mov	b,a
	inc	dptr
	movx	a,@dptr
	orl	a,b
	jz	00132$
;	..\src\COMMON\easyax5043.c:391: DownLinkIsrFlag = 0;
	mov	dptr,#_DownLinkIsrFlag
	clr	a
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:392: ACKReceivedFlag = VerifyACK(axradio_rxbuffer+3,len,NULL);
	mov	dptr,#_VerifyACK_PARM_2
	mov	a,r7
	movx	@dptr,a
	mov	dptr,#_VerifyACK_PARM_3
	clr	a
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
	mov	dptr,#(_axradio_rxbuffer + 0x0003)
	mov	b,#0x00
	lcall	_VerifyACK
	mov	r7,dpl
	mov	r6,#0x00
	mov	dptr,#_ACKReceivedFlag
	mov	a,r7
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:393: if(!ACKReceivedFlag && RfStateMachine == e_WaitForACK) BackOffCounter++;
	mov	a,r7
	orl	a,r6
	jz	00375$
	ljmp	00165$
00375$:
	mov	dptr,#_RfStateMachine
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	cjne	r6,#0x02,00376$
	cjne	r7,#0x00,00376$
	sjmp	00377$
00376$:
	ljmp	00165$
00377$:
	mov	dptr,#_BackOffCounter
	movx	a,@dptr
	add	a,#0x01
	movx	@dptr,a
	inc	dptr
	movx	a,@dptr
	addc	a,#0x00
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:394: break;
	ljmp	00165$
00132$:
;	..\src\COMMON\easyax5043.c:397: if(BeaconRx.FlagReceived == UPLINK_BEACON )
	mov	dptr,#_receive_isr_BeaconRx_65536_259
	movx	a,@dptr
	mov	r7,a
	cjne	r7,#0x0c,00378$
	sjmp	00379$
00378$:
	ljmp	00137$
00379$:
;	..\src\COMMON\easyax5043.c:399: if(GPSMessageReadyFlag | RfStateMachine == e_ReSendMessage){
	mov	dptr,#_RfStateMachine
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	clr	a
	cjne	r6,#0x03,00380$
	cjne	r7,#0x00,00380$
	inc	a
00380$:
	mov	r6,a
	mov	dptr,#_GPSMessageReadyFlag
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	r3,#0x00
	mov	a,r6
	orl	ar4,a
	mov	a,r3
	orl	ar7,a
	mov	a,r4
	orl	a,r7
	jnz	00382$
	ljmp	00134$
00382$:
;	..\src\COMMON\easyax5043.c:402: freq = freq;
;	..\src\COMMON\easyax5043.c:403: axradio_set_channel(FREQHZ_TO_AXFREQ(freq));
	mov	dptr,#_freq
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	dpl,r3
	mov	dph,r4
	mov	b,r6
	lcall	___ulong2fs
	mov	r3,dpl
	mov	r4,dph
	mov	r6,b
	mov	r7,a
	clr	a
	push	acc
	mov	a,#0x1b
	push	acc
	mov	a,#0x37
	push	acc
	mov	a,#0x4c
	push	acc
	mov	dpl,r3
	mov	dph,r4
	mov	b,r6
	mov	a,r7
	lcall	___fsdiv
	mov	r3,dpl
	mov	r4,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	push	ar3
	push	ar4
	push	ar6
	push	ar7
	mov	dptr,#0x0000
	mov	b,#0x80
	mov	a,#0x4b
	lcall	___fsmul
	mov	r3,dpl
	mov	r4,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	mov	dpl,r3
	mov	dph,r4
	mov	b,r6
	mov	a,r7
	lcall	___fs2ulong
	lcall	_axradio_set_channel
;	..\src\COMMON\easyax5043.c:405: UpLinkIsrFlag = 1;
	mov	dptr,#_UpLinkIsrFlag
	mov	a,#0x01
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:406: BeaconRx.FlagReceived = 0;
	mov	dptr,#_receive_isr_BeaconRx_65536_259
	movx	@dptr,a
	ljmp	00165$
00134$:
;	..\src\COMMON\easyax5043.c:411: dbglink_writestr(" discarded ");
	mov	dptr,#___str_1
	mov	b,#0x80
	lcall	_dbglink_writestr
;	..\src\COMMON\easyax5043.c:414: break;
	ljmp	00165$
00137$:
;	..\src\COMMON\easyax5043.c:416: if(BeaconRx.FlagReceived == DOWNLINK_BEACON)
	mov	dptr,#_receive_isr_BeaconRx_65536_259
	movx	a,@dptr
	mov	r7,a
	cjne	r7,#0x0a,00383$
	sjmp	00384$
00383$:
	ljmp	00165$
00384$:
;	..\src\COMMON\easyax5043.c:418: BeaconRx.FlagReceived = 0;
	mov	dptr,#_receive_isr_BeaconRx_65536_259
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:419: DownLinkIsrFlag = 1;
	mov	dptr,#_DownLinkIsrFlag
	inc	a
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:420: break;
	ljmp	00165$
;	..\src\COMMON\easyax5043.c:426: case AX5043_FIFOCMD_RFFREQOFFS:
00140$:
;	..\src\COMMON\easyax5043.c:427: if (axradio_phy_innerfreqloop || len != 3)
	mov	dptr,#_axradio_phy_innerfreqloop
	clr	a
	movc	a,@a+dptr
	jz	00385$
	ljmp	00158$
00385$:
	cjne	r5,#0x03,00386$
	sjmp	00387$
00386$:
	ljmp	00158$
00387$:
;	..\src\COMMON\easyax5043.c:429: i = AX5043_FIFODATA;
	mov	dptr,#_AX5043_FIFODATA
	movx	a,@dptr
	mov	r7,a
;	..\src\COMMON\easyax5043.c:430: i &= 0x0F;
	anl	ar7,#0x0f
;	..\src\COMMON\easyax5043.c:431: i |= 1 + (uint8_t)~(i & 0x08);
	mov	ar6,r7
	anl	ar6,#0x08
	mov	a,r6
	cpl	a
	mov	r6,a
	inc	r6
	mov	a,r7
	orl	ar6,a
;	..\src\COMMON\easyax5043.c:432: axradio_cb_receive.st.rx.phy.offset.b.b3 = ((int8_t)i) >> 8;
	mov	ar7,r6
	mov	a,r7
	rlc	a
	subb	a,acc
	mov	dptr,#(_axradio_cb_receive + 0x000f)
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:433: axradio_cb_receive.st.rx.phy.offset.b.b2 = i;
	mov	dptr,#(_axradio_cb_receive + 0x000e)
	mov	a,r6
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:434: axradio_cb_receive.st.rx.phy.offset.b.b1 = AX5043_FIFODATA;
	mov	dptr,#_AX5043_FIFODATA
	movx	a,@dptr
	mov	dptr,#(_axradio_cb_receive + 0x000d)
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:435: axradio_cb_receive.st.rx.phy.offset.b.b0 = AX5043_FIFODATA;
	mov	dptr,#_AX5043_FIFODATA
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#(_axradio_cb_receive + 0x000c)
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:436: break;
	ljmp	00165$
;	..\src\COMMON\easyax5043.c:438: case AX5043_FIFOCMD_FREQOFFS:
00144$:
;	..\src\COMMON\easyax5043.c:439: if (!axradio_phy_innerfreqloop || len != 2)
	mov	dptr,#_axradio_phy_innerfreqloop
	clr	a
	movc	a,@a+dptr
	jnz	00388$
	ljmp	00158$
00388$:
	cjne	r5,#0x02,00389$
	sjmp	00390$
00389$:
	ljmp	00158$
00390$:
;	..\src\COMMON\easyax5043.c:441: axradio_cb_receive.st.rx.phy.offset.b.b1 = AX5043_FIFODATA;
	mov	dptr,#_AX5043_FIFODATA
	movx	a,@dptr
	mov	dptr,#(_axradio_cb_receive + 0x000d)
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:442: axradio_cb_receive.st.rx.phy.offset.b.b0 = AX5043_FIFODATA;
	mov	dptr,#_AX5043_FIFODATA
	movx	a,@dptr
	mov	dptr,#(_axradio_cb_receive + 0x000c)
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:443: axradio_cb_receive.st.rx.phy.offset.o = axradio_conv_freq_fromreg(signextend16(axradio_cb_receive.st.rx.phy.offset.o));
	mov	dptr,#(_axradio_cb_receive + 0x000c)
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	inc	dptr
	movx	a,@dptr
	mov	dpl,r3
	mov	dph,r4
	lcall	_signextend16
	lcall	_axradio_conv_freq_fromreg
	mov	r3,dpl
	mov	r4,dph
	mov	r6,b
	mov	r7,a
	mov	dptr,#(_axradio_cb_receive + 0x000c)
	mov	a,r3
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:444: break;
	ljmp	00165$
;	..\src\COMMON\easyax5043.c:446: case AX5043_FIFOCMD_RSSI:
00148$:
;	..\src\COMMON\easyax5043.c:447: if (len != 1)
	cjne	r5,#0x01,00391$
	sjmp	00392$
00391$:
	ljmp	00158$
00392$:
;	..\src\COMMON\easyax5043.c:450: int8_t r = AX5043_FIFODATA;
	mov	dptr,#_AX5043_FIFODATA
	movx	a,@dptr
;	..\src\COMMON\easyax5043.c:451: axradio_cb_receive.st.rx.phy.rssi = r - (int16_t)axradio_phy_rssioffset;
	mov	r7,a
	rlc	a
	subb	a,acc
	mov	r6,a
	mov	dptr,#_axradio_phy_rssioffset
	clr	a
	movc	a,@a+dptr
	mov	r4,a
	rlc	a
	subb	a,acc
	mov	r3,a
	mov	a,r7
	clr	c
	subb	a,r4
	mov	r7,a
	mov	a,r6
	subb	a,r3
	mov	r6,a
	mov	dptr,#(_axradio_cb_receive + 0x000a)
	mov	a,r7
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:453: break;
	ljmp	00165$
;	..\src\COMMON\easyax5043.c:455: case AX5043_FIFOCMD_TIMER:
00151$:
;	..\src\COMMON\easyax5043.c:456: if (len != 3)
	cjne	r5,#0x03,00393$
	sjmp	00394$
00393$:
	ljmp	00158$
00394$:
;	..\src\COMMON\easyax5043.c:460: axradio_cb_receive.st.time.b.b3 = 0;
	mov	dptr,#(_axradio_cb_receive + 0x0009)
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:461: axradio_cb_receive.st.time.b.b2 = AX5043_FIFODATA;
	mov	dptr,#_AX5043_FIFODATA
	movx	a,@dptr
	mov	dptr,#(_axradio_cb_receive + 0x0008)
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:462: axradio_cb_receive.st.time.b.b1 = AX5043_FIFODATA;
	mov	dptr,#_AX5043_FIFODATA
	movx	a,@dptr
	mov	dptr,#(_axradio_cb_receive + 0x0007)
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:463: axradio_cb_receive.st.time.b.b0 = AX5043_FIFODATA;
	mov	dptr,#_AX5043_FIFODATA
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#(_axradio_cb_receive + 0x0006)
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:464: break;
	ljmp	00165$
;	..\src\COMMON\easyax5043.c:466: case AX5043_FIFOCMD_ANTRSSI:
00154$:
;	..\src\COMMON\easyax5043.c:467: if (!len)
	mov	a,r5
	jnz	00395$
	ljmp	00165$
00395$:
;	..\src\COMMON\easyax5043.c:469: update_timeanchor();
	push	ar5
	lcall	_update_timeanchor
;	..\src\COMMON\easyax5043.c:470: wtimer_remove_callback(&axradio_cb_channelstate.cb);
	mov	dptr,#_axradio_cb_channelstate
	lcall	_wtimer_remove_callback
;	..\src\COMMON\easyax5043.c:471: axradio_cb_channelstate.st.error = AXRADIO_ERR_NOERROR;
	mov	dptr,#(_axradio_cb_channelstate + 0x0005)
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:473: int8_t r = AX5043_FIFODATA;
	mov	dptr,#_AX5043_FIFODATA
	movx	a,@dptr
;	..\src\COMMON\easyax5043.c:474: axradio_cb_channelstate.st.cs.rssi = r - (int16_t)axradio_phy_rssioffset;
	mov	r7,a
	mov	r4,a
	rlc	a
	subb	a,acc
	mov	r6,a
	mov	dptr,#_axradio_phy_rssioffset
	clr	a
	movc	a,@a+dptr
	mov	r3,a
	rlc	a
	subb	a,acc
	mov	r2,a
	mov	a,r4
	clr	c
	subb	a,r3
	mov	r4,a
	mov	a,r6
	subb	a,r2
	mov	r6,a
	mov	dptr,#(_axradio_cb_channelstate + 0x000a)
	mov	a,r4
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:475: axradio_cb_channelstate.st.cs.busy = r >= axradio_phy_channelbusy;
	mov	dptr,#_axradio_phy_channelbusy
	clr	a
	movc	a,@a+dptr
	mov	r6,a
	clr	c
	mov	a,r7
	xrl	a,#0x80
	mov	b,r6
	xrl	b,#0x80
	subb	a,b
	cpl	c
	mov	b0,c
	clr	a
	rlc	a
	mov	dptr,#(_axradio_cb_channelstate + 0x000c)
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:477: axradio_cb_channelstate.st.time.t = axradio_timeanchor.radiotimer;
	mov	dptr,#(_axradio_timeanchor + 0x0004)
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#(_axradio_cb_channelstate + 0x0006)
	mov	a,r3
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:478: wtimer_add_callback(&axradio_cb_channelstate.cb);
	mov	dptr,#_axradio_cb_channelstate
	lcall	_wtimer_add_callback
	pop	ar5
;	..\src\COMMON\easyax5043.c:479: --len;
	dec	r5
;	..\src\COMMON\easyax5043.c:484: dropchunk:
00158$:
;	..\src\COMMON\easyax5043.c:485: if (!len)
	mov	a,r5
	jnz	00396$
	ljmp	00165$
00396$:
;	..\src\COMMON\easyax5043.c:488: do {
	mov	ar7,r5
00161$:
;	..\src\COMMON\easyax5043.c:489: AX5043_FIFODATA;        // purge FIFO
	mov	dptr,#_AX5043_FIFODATA
	movx	a,@dptr
;	..\src\COMMON\easyax5043.c:491: while (--i);
	djnz	r7,00161$
;	..\src\COMMON\easyax5043.c:493: } // end switch(fifo_cmd)
;	..\src\COMMON\easyax5043.c:495: }
	ljmp	00165$
;------------------------------------------------------------
;Allocation info for local variables in function 'transmit_isr'
;------------------------------------------------------------
;cnt                       Allocated to registers r4 
;byte                      Allocated to registers r2 
;len_byte                  Allocated to registers r4 
;i                         Allocated to registers r2 
;byte                      Allocated to registers r3 
;flags                     Allocated to registers r6 
;len                       Allocated to registers r4 r5 
;sloc0                     Allocated to stack - _bp +9
;sloc1                     Allocated to stack - _bp +10
;sloc2                     Allocated to stack - _bp +11
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:497: static __reentrantb void transmit_isr(void) __reentrant
;	-----------------------------------------
;	 function transmit_isr
;	-----------------------------------------
_transmit_isr:
;	..\src\COMMON\easyax5043.c:636: axradio_trxstate = trxstate_tx_waitdone;
00157$:
;	..\src\COMMON\easyax5043.c:500: uint8_t cnt = AX5043_FIFOFREE0;
	mov	dptr,#_AX5043_FIFOFREE0
	movx	a,@dptr
	mov	r7,a
;	..\src\COMMON\easyax5043.c:501: if (AX5043_FIFOFREE1)
	mov	dptr,#_AX5043_FIFOFREE1
	movx	a,@dptr
	jz	00102$
;	..\src\COMMON\easyax5043.c:502: cnt = 0xff;
	mov	r7,#0xff
00102$:
;	..\src\COMMON\easyax5043.c:503: switch (axradio_trxstate) {
	mov	r6,_axradio_trxstate
	cjne	r6,#0x0a,00300$
	sjmp	00103$
00300$:
	cjne	r6,#0x0b,00301$
	ljmp	00115$
00301$:
	cjne	r6,#0x0c,00302$
	ljmp	00138$
00302$:
	ret
;	..\src\COMMON\easyax5043.c:504: case trxstate_tx_longpreamble:
00103$:
;	..\src\COMMON\easyax5043.c:505: if (!axradio_txbuffer_cnt) {
	mov	dptr,#_axradio_txbuffer_cnt
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	orl	a,r5
	jnz	00109$
;	..\src\COMMON\easyax5043.c:506: axradio_trxstate = trxstate_tx_shortpreamble;
	mov	_axradio_trxstate,#0x0b
;	..\src\COMMON\easyax5043.c:507: if( axradio_mode == AXRADIO_MODE_WOR_TRANSMIT || axradio_mode == AXRADIO_MODE_WOR_ACK_TRANSMIT )
	mov	a,#0x11
	cjne	a,_axradio_mode,00304$
	sjmp	00104$
00304$:
	mov	a,#0x13
	cjne	a,_axradio_mode,00105$
00104$:
;	..\src\COMMON\easyax5043.c:508: axradio_txbuffer_cnt = axradio_phy_preamble_wor_len;
	mov	dptr,#_axradio_phy_preamble_wor_len
	clr	a
	movc	a,@a+dptr
	mov	r3,a
	mov	a,#0x01
	movc	a,@a+dptr
	mov	r4,a
	mov	dptr,#_axradio_txbuffer_cnt
	mov	a,r3
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
	sjmp	00115$
00105$:
;	..\src\COMMON\easyax5043.c:510: axradio_txbuffer_cnt = axradio_phy_preamble_len;
	mov	dptr,#_axradio_phy_preamble_len
	clr	a
	movc	a,@a+dptr
	mov	r3,a
	mov	a,#0x01
	movc	a,@a+dptr
	mov	r4,a
	mov	dptr,#_axradio_txbuffer_cnt
	mov	a,r3
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:511: goto shortpreamble;
	sjmp	00115$
00109$:
;	..\src\COMMON\easyax5043.c:513: if (cnt < 4)
	cjne	r7,#0x04,00307$
00307$:
	jnc	00308$
	ljmp	00153$
00308$:
;	..\src\COMMON\easyax5043.c:515: cnt = 7;
	mov	r4,#0x07
;	..\src\COMMON\easyax5043.c:516: if (axradio_txbuffer_cnt < 7)
	clr	c
	mov	a,r5
	subb	a,#0x07
	mov	a,r6
	subb	a,#0x00
	jnc	00113$
;	..\src\COMMON\easyax5043.c:517: cnt = axradio_txbuffer_cnt;
	mov	ar4,r5
00113$:
;	..\src\COMMON\easyax5043.c:518: axradio_txbuffer_cnt -= cnt;
	mov	ar5,r4
	mov	r6,#0x00
	mov	dptr,#_axradio_txbuffer_cnt
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	mov	dptr,#_axradio_txbuffer_cnt
	mov	a,r2
	clr	c
	subb	a,r5
	movx	@dptr,a
	mov	a,r3
	subb	a,r6
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:519: cnt <<= 5;
	mov	a,r4
	swap	a
	rl	a
	anl	a,#0xe0
	mov	r4,a
;	..\src\COMMON\easyax5043.c:520: AX5043_FIFODATA = AX5043_FIFOCMD_REPEATDATA | (3 << 5);
	mov	dptr,#_AX5043_FIFODATA
	mov	a,#0x62
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:521: AX5043_FIFODATA = axradio_phy_preamble_flags;
	mov	dptr,#_axradio_phy_preamble_flags
	clr	a
	movc	a,@a+dptr
	mov	dptr,#_AX5043_FIFODATA
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:522: AX5043_FIFODATA = cnt;
	mov	a,r4
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:523: AX5043_FIFODATA = axradio_phy_preamble_byte;
	mov	dptr,#_axradio_phy_preamble_byte
	clr	a
	movc	a,@a+dptr
	mov	dptr,#_AX5043_FIFODATA
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:524: break;
	ljmp	00157$
;	..\src\COMMON\easyax5043.c:527: shortpreamble:
00115$:
;	..\src\COMMON\easyax5043.c:528: if (!axradio_txbuffer_cnt) {
	mov	dptr,#_axradio_txbuffer_cnt
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	orl	a,r5
	jz	00310$
	ljmp	00128$
00310$:
;	..\src\COMMON\easyax5043.c:529: if (cnt < 15)
	cjne	r7,#0x0f,00311$
00311$:
	jnc	00312$
	ljmp	00153$
00312$:
;	..\src\COMMON\easyax5043.c:531: if (axradio_phy_preamble_appendbits) {
	mov	dptr,#_axradio_phy_preamble_appendbits
	clr	a
	movc	a,@a+dptr
	mov	r7,a
	jnz	00313$
	ljmp	00122$
00313$:
;	..\src\COMMON\easyax5043.c:533: AX5043_FIFODATA = AX5043_FIFOCMD_DATA | (2 << 5);
	mov	dptr,#_AX5043_FIFODATA
	mov	a,#0x41
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:534: AX5043_FIFODATA = 0x1C;
	mov	a,#0x1c
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:535: byte = axradio_phy_preamble_appendpattern;
	mov	dptr,#_axradio_phy_preamble_appendpattern
	clr	a
	movc	a,@a+dptr
	mov	r5,a
;	..\src\COMMON\easyax5043.c:536: if (AX5043_PKTADDRCFG & 0x80) {
	mov	dptr,#_AX5043_PKTADDRCFG
	movx	a,@dptr
	jnb	acc.7,00119$
;	..\src\COMMON\easyax5043.c:538: byte &= 0xFF << (8-axradio_phy_preamble_appendbits);
	mov	ar2,r7
	mov	a,#0x08
	clr	c
	subb	a,r2
	mov	b,a
	inc	b
	mov	a,#0xff
	sjmp	00317$
00315$:
	add	a,acc
00317$:
	djnz	b,00315$
	mov	r6,a
	mov	ar2,r5
	mov	a,r2
	anl	ar6,a
;	..\src\COMMON\easyax5043.c:539: byte |= 0x80 >> axradio_phy_preamble_appendbits;
	mov	b,r7
	inc	b
	mov	r2,#0x80
	clr	a
	mov	r4,a
	rlc	a
	mov	ov,c
	sjmp	00319$
00318$:
	mov	c,ov
	mov	a,r4
	rrc	a
	mov	r4,a
	mov	a,r2
	rrc	a
	mov	r2,a
00319$:
	djnz	b,00318$
	mov	a,r2
	orl	a,r6
	mov	r4,a
	sjmp	00120$
00119$:
;	..\src\COMMON\easyax5043.c:542: byte &= 0xFF >> (8-axradio_phy_preamble_appendbits);
	mov	a,#0x08
	clr	c
	subb	a,r7
	mov	r2,a
	mov	b,r2
	inc	b
	mov	r2,#0xff
	clr	a
	mov	r3,a
	rlc	a
	mov	ov,c
	sjmp	00321$
00320$:
	mov	c,ov
	mov	a,r3
	rrc	a
	mov	r3,a
	mov	a,r2
	rrc	a
	mov	r2,a
00321$:
	djnz	b,00320$
	mov	a,r5
	anl	ar2,a
;	..\src\COMMON\easyax5043.c:543: byte |= 0x01 << axradio_phy_preamble_appendbits;
	mov	ar3,r7
	mov	b,r3
	inc	b
	mov	a,#0x01
	sjmp	00324$
00322$:
	add	a,acc
00324$:
	djnz	b,00322$
	mov	r3,a
	orl	a,r2
	mov	r4,a
00120$:
;	..\src\COMMON\easyax5043.c:545: AX5043_FIFODATA = byte;
	mov	dptr,#_AX5043_FIFODATA
	mov	a,r4
	movx	@dptr,a
00122$:
;	..\src\COMMON\easyax5043.c:551: if ((AX5043_FRAMING & 0x0E) == 0x06 && axradio_framing_synclen) {
	mov	dptr,#_AX5043_FRAMING
	movx	a,@dptr
	mov	r4,a
	anl	ar4,#0x0e
	mov	r3,#0x00
	cjne	r4,#0x06,00125$
	cjne	r3,#0x00,00125$
	mov	dptr,#_axradio_framing_synclen
	clr	a
	movc	a,@a+dptr
	mov	r4,a
	clr	a
	movc	a,@a+dptr
	jz	00125$
;	..\src\COMMON\easyax5043.c:553: uint8_t len_byte = axradio_framing_synclen;
;	..\src\COMMON\easyax5043.c:554: uint8_t i = (len_byte & 0x07) ? 0x04 : 0;
	mov	a,r4
	anl	a,#0x07
	jz	00161$
	mov	r2,#0x04
	mov	r3,#0x00
	sjmp	00162$
00161$:
	mov	r2,#0x00
	mov	r3,#0x00
00162$:
;	..\src\COMMON\easyax5043.c:556: len_byte += 7;
	mov	a,#0x07
	add	a,r4
;	..\src\COMMON\easyax5043.c:557: len_byte >>= 3;
	swap	a
	rl	a
	anl	a,#0x1f
;	..\src\COMMON\easyax5043.c:558: AX5043_FIFODATA = AX5043_FIFOCMD_DATA | ((len_byte + 1) << 5);
	mov	r4,a
	mov	r3,a
	inc	r3
	mov	a,r3
	swap	a
	rl	a
	anl	a,#0xe0
	mov	r3,a
	mov	dptr,#_AX5043_FIFODATA
	mov	a,#0x01
	orl	a,r3
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:559: AX5043_FIFODATA = axradio_framing_syncflags | i;
	mov	dptr,#_axradio_framing_syncflags
	clr	a
	movc	a,@a+dptr
	mov	r3,a
	mov	dptr,#_AX5043_FIFODATA
	mov	a,r2
	orl	a,r3
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:560: for (i = 0; i < len_byte; ++i) {
	mov	r3,#0x00
00155$:
	clr	c
	mov	a,r3
	subb	a,r4
	jnc	00125$
;	..\src\COMMON\easyax5043.c:562: AX5043_FIFODATA = axradio_framing_syncword[i];
	mov	a,r3
	mov	dptr,#_axradio_framing_syncword
	movc	a,@a+dptr
	mov	r2,a
	mov	dptr,#_AX5043_FIFODATA
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:560: for (i = 0; i < len_byte; ++i) {
	inc	r3
	sjmp	00155$
00125$:
;	..\src\COMMON\easyax5043.c:569: axradio_trxstate = trxstate_tx_packet;
	mov	_axradio_trxstate,#0x0c
;	..\src\COMMON\easyax5043.c:570: break;
	ljmp	00157$
00128$:
;	..\src\COMMON\easyax5043.c:572: if (cnt < 4)
	cjne	r7,#0x04,00330$
00330$:
	jnc	00331$
	ljmp	00153$
00331$:
;	..\src\COMMON\easyax5043.c:574: cnt = 255;
	mov	r4,#0xff
;	..\src\COMMON\easyax5043.c:575: if (axradio_txbuffer_cnt < 255*8)
	clr	c
	mov	a,r5
	subb	a,#0xf8
	mov	a,r6
	subb	a,#0x07
	jnc	00132$
;	..\src\COMMON\easyax5043.c:576: cnt = axradio_txbuffer_cnt >> 3;
	mov	a,r6
	swap	a
	rl	a
	xch	a,r5
	swap	a
	rl	a
	anl	a,#0x1f
	xrl	a,r5
	xch	a,r5
	anl	a,#0x1f
	xch	a,r5
	xrl	a,r5
	xch	a,r5
	mov	r6,a
	mov	ar4,r5
00132$:
;	..\src\COMMON\easyax5043.c:577: if (cnt) {
	mov	a,r4
	jz	00134$
;	..\src\COMMON\easyax5043.c:578: axradio_txbuffer_cnt -= ((uint16_t)cnt) << 3;
	mov	ar5,r4
	clr	a
	rr	a
	anl	a,#0xf8
	xch	a,r5
	swap	a
	rr	a
	xch	a,r5
	xrl	a,r5
	xch	a,r5
	anl	a,#0xf8
	xch	a,r5
	xrl	a,r5
	mov	r6,a
	mov	dptr,#_axradio_txbuffer_cnt
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	mov	dptr,#_axradio_txbuffer_cnt
	mov	a,r2
	clr	c
	subb	a,r5
	movx	@dptr,a
	mov	a,r3
	subb	a,r6
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:579: AX5043_FIFODATA = AX5043_FIFOCMD_REPEATDATA | (3 << 5);
	mov	dptr,#_AX5043_FIFODATA
	mov	a,#0x62
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:580: AX5043_FIFODATA = axradio_phy_preamble_flags;
	mov	dptr,#_axradio_phy_preamble_flags
	clr	a
	movc	a,@a+dptr
	mov	dptr,#_AX5043_FIFODATA
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:581: AX5043_FIFODATA = cnt;
	mov	a,r4
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:582: AX5043_FIFODATA = axradio_phy_preamble_byte;
	mov	dptr,#_axradio_phy_preamble_byte
	clr	a
	movc	a,@a+dptr
	mov	dptr,#_AX5043_FIFODATA
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:583: break;
	ljmp	00157$
00134$:
;	..\src\COMMON\easyax5043.c:586: uint8_t byte = axradio_phy_preamble_byte;
	mov	dptr,#_axradio_phy_preamble_byte
	clr	a
	movc	a,@a+dptr
	mov	r6,a
;	..\src\COMMON\easyax5043.c:587: cnt = axradio_txbuffer_cnt;
	mov	dptr,#_axradio_txbuffer_cnt
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
;	..\src\COMMON\easyax5043.c:588: axradio_txbuffer_cnt = 0;
	mov	dptr,#_axradio_txbuffer_cnt
	clr	a
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:589: AX5043_FIFODATA = AX5043_FIFOCMD_DATA | (2 << 5);
	mov	dptr,#_AX5043_FIFODATA
	mov	a,#0x41
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:590: AX5043_FIFODATA = 0x1C;
	mov	a,#0x1c
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:591: if (AX5043_PKTADDRCFG & 0x80) {
	mov	dptr,#_AX5043_PKTADDRCFG
	movx	a,@dptr
	jnb	acc.7,00136$
;	..\src\COMMON\easyax5043.c:593: byte &= 0xFF << (8-cnt);
	mov	ar5,r4
	mov	a,#0x08
	clr	c
	subb	a,r5
	mov	b,a
	inc	b
	mov	a,#0xff
	sjmp	00337$
00335$:
	add	a,acc
00337$:
	djnz	b,00335$
	mov	r5,a
	mov	ar3,r6
	mov	a,r3
	anl	ar5,a
;	..\src\COMMON\easyax5043.c:594: byte |= 0x80 >> cnt;
	mov	b,r4
	inc	b
	mov	r2,#0x80
	clr	a
	mov	r3,a
	rlc	a
	mov	ov,c
	sjmp	00339$
00338$:
	mov	c,ov
	mov	a,r3
	rrc	a
	mov	r3,a
	mov	a,r2
	rrc	a
	mov	r2,a
00339$:
	djnz	b,00338$
	mov	a,r2
	orl	ar5,a
	sjmp	00137$
00136$:
;	..\src\COMMON\easyax5043.c:597: byte &= 0xFF >> (8-cnt);
	mov	a,#0x08
	clr	c
	subb	a,r4
	mov	r3,a
	mov	b,r3
	inc	b
	mov	r3,#0xff
	clr	a
	mov	r2,a
	rlc	a
	mov	ov,c
	sjmp	00341$
00340$:
	mov	c,ov
	mov	a,r2
	rrc	a
	mov	r2,a
	mov	a,r3
	rrc	a
	mov	r3,a
00341$:
	djnz	b,00340$
	mov	a,r6
	anl	ar3,a
;	..\src\COMMON\easyax5043.c:598: byte |= 0x01 << cnt;
	mov	b,r4
	inc	b
	mov	a,#0x01
	sjmp	00344$
00342$:
	add	a,acc
00344$:
	djnz	b,00342$
	mov	r4,a
	orl	a,r3
	mov	r5,a
00137$:
;	..\src\COMMON\easyax5043.c:600: AX5043_FIFODATA = byte;
	mov	dptr,#_AX5043_FIFODATA
	mov	a,r5
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:602: break;
	ljmp	00157$
;	..\src\COMMON\easyax5043.c:604: case trxstate_tx_packet:
00138$:
;	..\src\COMMON\easyax5043.c:605: if (cnt < 11)
	cjne	r7,#0x0b,00345$
00345$:
	jnc	00346$
	ljmp	00153$
00346$:
;	..\src\COMMON\easyax5043.c:608: uint8_t flags = 0;
	mov	r6,#0x00
;	..\src\COMMON\easyax5043.c:609: if (!axradio_txbuffer_cnt)
	mov	dptr,#_axradio_txbuffer_cnt
	movx	a,@dptr
	mov	b,a
	inc	dptr
	movx	a,@dptr
	orl	a,b
	jnz	00142$
;	..\src\COMMON\easyax5043.c:610: flags |= 0x01; // flag byte: pkt_start
	mov	r6,#0x01
00142$:
;	..\src\COMMON\easyax5043.c:612: uint16_t len = axradio_txbuffer_len - axradio_txbuffer_cnt;
	mov	dptr,#_axradio_txbuffer_cnt
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	mov	dptr,#_axradio_txbuffer_len
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	mov	a,r2
	clr	c
	subb	a,r4
	mov	r4,a
	mov	a,r3
	subb	a,r5
	mov	r5,a
;	..\src\COMMON\easyax5043.c:613: cnt -= 3;
	dec	r7
	dec	r7
	dec	r7
;	..\src\COMMON\easyax5043.c:614: if (cnt >= len) {
	mov	ar2,r7
	mov	r3,#0x00
	clr	c
	mov	a,r2
	subb	a,r4
	mov	a,r3
	subb	a,r5
	jc	00144$
;	..\src\COMMON\easyax5043.c:615: cnt = len;
	mov	ar7,r4
;	..\src\COMMON\easyax5043.c:616: flags |= 0x02; // flag byte: pkt_end
	orl	ar6,#0x02
00144$:
;	..\src\COMMON\easyax5043.c:619: if (!cnt)
	mov	a,r7
	jz	00152$
;	..\src\COMMON\easyax5043.c:621: AX5043_FIFODATA = AX5043_FIFOCMD_DATA | (7 << 5);
	mov	dptr,#_AX5043_FIFODATA
	mov	a,#0xe1
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:622: AX5043_FIFODATA = cnt + 1; // write FIFO chunk length byte (length includes the flag byte, thus the +1)
	mov	ar5,r7
	mov	a,r5
	inc	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:623: AX5043_FIFODATA = flags;
	mov	a,r6
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:624: ax5043_writefifo(&axradio_txbuffer[axradio_txbuffer_cnt], cnt);
	mov	dptr,#_axradio_txbuffer_cnt
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	mov	a,r4
	add	a,#_axradio_txbuffer
	mov	r4,a
	mov	a,r5
	addc	a,#(_axradio_txbuffer >> 8)
	mov	r5,a
	mov	r3,#0x00
	push	ar7
	push	ar6
	push	ar7
	mov	dpl,r4
	mov	dph,r5
	mov	b,r3
	lcall	_ax5043_writefifo
	dec	sp
	pop	ar6
	pop	ar7
;	..\src\COMMON\easyax5043.c:625: axradio_txbuffer_cnt += cnt;
	mov	r5,#0x00
	mov	dptr,#_axradio_txbuffer_cnt
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
	mov	dptr,#_axradio_txbuffer_cnt
	mov	a,r7
	add	a,r3
	movx	@dptr,a
	mov	a,r5
	addc	a,r4
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:626: if (flags & 0x02)
	mov	a,r6
	jb	acc.1,00152$
;	..\src\COMMON\easyax5043.c:627: goto pktend;
;	..\src\COMMON\easyax5043.c:631: default:
;	..\src\COMMON\easyax5043.c:632: return;
;	..\src\COMMON\easyax5043.c:635: pktend:
	ljmp	00157$
00152$:
;	..\src\COMMON\easyax5043.c:636: axradio_trxstate = trxstate_tx_waitdone;
	mov	_axradio_trxstate,#0x0d
;	..\src\COMMON\easyax5043.c:637: AX5043_RADIOEVENTMASK0 = 0x01; // enable REVRDONE event
	mov	dptr,#_AX5043_RADIOEVENTMASK0
	mov	a,#0x01
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:638: AX5043_IRQMASK0 = 0x40; // enable radio controller irq
	mov	dptr,#_AX5043_IRQMASK0
	mov	a,#0x40
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:639: fifocommit:
00153$:
;	..\src\COMMON\easyax5043.c:640: AX5043_FIFOSTAT = 4; // commit
	mov	dptr,#_AX5043_FIFOSTAT
	mov	a,#0x04
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:641: }
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'axradio_isr'
;------------------------------------------------------------
;evt                       Allocated to registers r7 
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:644: void axradio_isr(void) __interrupt INT_RADIO
;	-----------------------------------------
;	 function axradio_isr
;	-----------------------------------------
_axradio_isr:
	push	bits
	push	acc
	push	b
	push	dpl
	push	dph
	push	(0+7)
	push	(0+6)
	push	(0+5)
	push	(0+4)
	push	(0+3)
	push	(0+2)
	push	(0+1)
	push	(0+0)
	push	psw
	mov	psw,#0x00
;	..\src\COMMON\easyax5043.c:654: switch (axradio_trxstate) {
	mov	a,_axradio_trxstate
	mov	r7,a
	add	a,#0xff - 0x10
	jnc	00285$
	ljmp	00101$
00285$:
	mov	a,r7
	mov	b,a
	add	a,#(00286$-3-.)
	movc	a,@a+pc
	mov	dpl,a
	mov	a,b
	add	a,#(00287$-3-.)
	movc	a,@a+pc
	mov	dph,a
	clr	a
	jmp	@a+dptr
00286$:
	.db	00101$
	.db	00165$
	.db	00158$
	.db	00102$
	.db	00101$
	.db	00103$
	.db	00101$
	.db	00104$
	.db	00101$
	.db	00105$
	.db	00116$
	.db	00116$
	.db	00116$
	.db	00117$
	.db	00144$
	.db	00145$
	.db	00148$
00287$:
	.db	00101$>>8
	.db	00165$>>8
	.db	00158$>>8
	.db	00102$>>8
	.db	00101$>>8
	.db	00103$>>8
	.db	00101$>>8
	.db	00104$>>8
	.db	00101$>>8
	.db	00105$>>8
	.db	00116$>>8
	.db	00116$>>8
	.db	00116$>>8
	.db	00117$>>8
	.db	00144$>>8
	.db	00145$>>8
	.db	00148$>>8
;	..\src\COMMON\easyax5043.c:655: default:
00101$:
;	..\src\COMMON\easyax5043.c:656: AX5043_IRQMASK1 = 0x00;
	mov	dptr,#_AX5043_IRQMASK1
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:657: AX5043_IRQMASK0 = 0x00;
	mov	dptr,#_AX5043_IRQMASK0
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:658: break;
	ljmp	00167$
;	..\src\COMMON\easyax5043.c:660: case trxstate_wait_xtal:
00102$:
;	..\src\COMMON\easyax5043.c:661: AX5043_IRQMASK1 = 0x00; // otherwise crystal ready will fire all over again
	mov	dptr,#_AX5043_IRQMASK1
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:662: axradio_trxstate = trxstate_xtal_ready;
	mov	_axradio_trxstate,#0x04
;	..\src\COMMON\easyax5043.c:663: break;
	ljmp	00167$
;	..\src\COMMON\easyax5043.c:665: case trxstate_pll_ranging:
00103$:
;	..\src\COMMON\easyax5043.c:666: AX5043_IRQMASK1 = 0x00; // otherwise autoranging done will fire all over again
	mov	dptr,#_AX5043_IRQMASK1
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:667: axradio_trxstate = trxstate_pll_ranging_done;
	mov	_axradio_trxstate,#0x06
;	..\src\COMMON\easyax5043.c:668: break;
	ljmp	00167$
;	..\src\COMMON\easyax5043.c:670: case trxstate_pll_settling:
00104$:
;	..\src\COMMON\easyax5043.c:671: AX5043_RADIOEVENTMASK0 = 0x00;
	mov	dptr,#_AX5043_RADIOEVENTMASK0
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:672: axradio_trxstate = trxstate_pll_settled;
	mov	_axradio_trxstate,#0x08
;	..\src\COMMON\easyax5043.c:673: break;
	ljmp	00167$
;	..\src\COMMON\easyax5043.c:675: case trxstate_tx_xtalwait:
00105$:
;	..\src\COMMON\easyax5043.c:676: AX5043_RADIOEVENTREQ0; // make sure REVRDONE bit is cleared, so it is a reliable indicator that the packet is out
	mov	dptr,#_AX5043_RADIOEVENTREQ0
	movx	a,@dptr
;	..\src\COMMON\easyax5043.c:677: AX5043_FIFOSTAT = 3; // clear FIFO data & flags (prevent transmitting anything left over in the FIFO, this has no effect if the FIFO is not powerered, in this case it is reset any way)
	mov	dptr,#_AX5043_FIFOSTAT
	mov	a,#0x03
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:678: AX5043_IRQMASK1 = 0x00;
	mov	dptr,#_AX5043_IRQMASK1
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:679: AX5043_IRQMASK0 = 0x08; // enable fifo free threshold
	mov	dptr,#_AX5043_IRQMASK0
	mov	a,#0x08
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:680: axradio_trxstate = trxstate_tx_longpreamble;
	mov	_axradio_trxstate,#0x0a
;	..\src\COMMON\easyax5043.c:682: if ((AX5043_MODULATION & 0x0F) == 9) { // 4-FSK
	mov	dptr,#_AX5043_MODULATION
	movx	a,@dptr
	mov	r7,a
	anl	ar7,#0x0f
	mov	r6,#0x00
	cjne	r7,#0x09,00107$
	cjne	r6,#0x00,00107$
;	..\src\COMMON\easyax5043.c:683: AX5043_FIFODATA = AX5043_FIFOCMD_DATA | (7 << 5);
	mov	dptr,#_AX5043_FIFODATA
	mov	a,#0xe1
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:684: AX5043_FIFODATA = 2;  // length (including flags)
	mov	a,#0x02
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:685: AX5043_FIFODATA = 0x01;  // flag PKTSTART -> dibit sync
	dec	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:686: AX5043_FIFODATA = 0x11; // dummy byte for forcing dibit sync
	mov	a,#0x11
	movx	@dptr,a
00107$:
;	..\src\COMMON\easyax5043.c:693: transmit_isr();
	lcall	_transmit_isr
;	..\src\COMMON\easyax5043.c:694: AX5043_PWRMODE = AX5043_PWRSTATE_FULL_TX;
	mov	dptr,#_AX5043_PWRMODE
	mov	a,#0x0d
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:695: update_timeanchor();
	lcall	_update_timeanchor
;	..\src\COMMON\easyax5043.c:696: wtimer_remove_callback(&axradio_cb_transmitstart.cb);
	mov	dptr,#_axradio_cb_transmitstart
	lcall	_wtimer_remove_callback
;	..\src\COMMON\easyax5043.c:697: switch (axradio_mode) {
	mov	r7,_axradio_mode
	cjne	r7,#0x12,00290$
	sjmp	00109$
00290$:
	cjne	r7,#0x13,00112$
;	..\src\COMMON\easyax5043.c:699: case AXRADIO_MODE_WOR_ACK_TRANSMIT:
00109$:
;	..\src\COMMON\easyax5043.c:700: if (axradio_ack_count != axradio_framing_ack_retransmissions) {
	mov	dptr,#_axradio_ack_count
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#_axradio_framing_ack_retransmissions
	clr	a
	movc	a,@a+dptr
	mov	r6,a
	mov	a,r7
	cjne	a,ar6,00293$
	sjmp	00112$
00293$:
;	..\src\COMMON\easyax5043.c:701: axradio_cb_transmitstart.st.error = AXRADIO_ERR_RETRANSMISSION;
	mov	dptr,#(_axradio_cb_transmitstart + 0x0005)
	mov	a,#0x08
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:702: break;
;	..\src\COMMON\easyax5043.c:705: default:
	sjmp	00113$
00112$:
;	..\src\COMMON\easyax5043.c:706: axradio_cb_transmitstart.st.error = AXRADIO_ERR_NOERROR;
	mov	dptr,#(_axradio_cb_transmitstart + 0x0005)
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:708: }
00113$:
;	..\src\COMMON\easyax5043.c:709: axradio_cb_transmitstart.st.time.t = axradio_timeanchor.radiotimer;
	mov	dptr,#(_axradio_timeanchor + 0x0004)
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#(_axradio_cb_transmitstart + 0x0006)
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:710: wtimer_add_callback(&axradio_cb_transmitstart.cb);
	mov	dptr,#_axradio_cb_transmitstart
	lcall	_wtimer_add_callback
;	..\src\COMMON\easyax5043.c:711: break;
	ljmp	00167$
;	..\src\COMMON\easyax5043.c:715: case trxstate_tx_packet:
00116$:
;	..\src\COMMON\easyax5043.c:716: transmit_isr();
	lcall	_transmit_isr
;	..\src\COMMON\easyax5043.c:717: break;
	ljmp	00167$
;	..\src\COMMON\easyax5043.c:719: case trxstate_tx_waitdone:
00117$:
;	..\src\COMMON\easyax5043.c:720: AX5043_RADIOEVENTREQ0;
	mov	dptr,#_AX5043_RADIOEVENTREQ0
	movx	a,@dptr
;	..\src\COMMON\easyax5043.c:721: if (AX5043_RADIOSTATE != 0)
	mov	dptr,#_AX5043_RADIOSTATE
	movx	a,@dptr
	jz	00294$
	ljmp	00167$
00294$:
;	..\src\COMMON\easyax5043.c:723: AX5043_RADIOEVENTMASK0 = 0x00;
	mov	dptr,#_AX5043_RADIOEVENTMASK0
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:724: switch (axradio_mode) {
	mov	r7,_axradio_mode
	cjne	r7,#0x12,00295$
	sjmp	00131$
00295$:
	cjne	r7,#0x13,00296$
	sjmp	00131$
00296$:
	cjne	r7,#0x20,00297$
	sjmp	00120$
00297$:
	cjne	r7,#0x21,00298$
	sjmp	00125$
00298$:
	cjne	r7,#0x22,00299$
	sjmp	00121$
00299$:
	cjne	r7,#0x23,00300$
	sjmp	00128$
00300$:
	cjne	r7,#0x30,00301$
	ljmp	00132$
00301$:
	cjne	r7,#0x31,00302$
	sjmp	00129$
00302$:
	ljmp	00133$
;	..\src\COMMON\easyax5043.c:725: case AXRADIO_MODE_ASYNC_RECEIVE:
00120$:
;	..\src\COMMON\easyax5043.c:727: ax5043_init_registers_rx();
	lcall	_ax5043_init_registers_rx
;	..\src\COMMON\easyax5043.c:728: ax5043_receiver_on_continuous();
	lcall	_ax5043_receiver_on_continuous
;	..\src\COMMON\easyax5043.c:729: break;
	ljmp	00134$
;	..\src\COMMON\easyax5043.c:731: case AXRADIO_MODE_ACK_RECEIVE:
00121$:
;	..\src\COMMON\easyax5043.c:732: if (axradio_cb_receive.st.error == AXRADIO_ERR_PACKETDONE) {
	mov	dptr,#(_axradio_cb_receive + 0x0005)
	movx	a,@dptr
	mov	r7,a
	cjne	r7,#0xf0,00124$
;	..\src\COMMON\easyax5043.c:733: ax5043_init_registers_rx();
	lcall	_ax5043_init_registers_rx
;	..\src\COMMON\easyax5043.c:734: ax5043_receiver_on_continuous();
	lcall	_ax5043_receiver_on_continuous
;	..\src\COMMON\easyax5043.c:735: break;
;	..\src\COMMON\easyax5043.c:737: offxtal:
	sjmp	00134$
00124$:
;	..\src\COMMON\easyax5043.c:738: ax5043_off_xtal();
	lcall	_ax5043_off_xtal
;	..\src\COMMON\easyax5043.c:739: break;
;	..\src\COMMON\easyax5043.c:741: case AXRADIO_MODE_WOR_RECEIVE:
	sjmp	00134$
00125$:
;	..\src\COMMON\easyax5043.c:742: if (axradio_cb_receive.st.error == AXRADIO_ERR_PACKETDONE) {
	mov	dptr,#(_axradio_cb_receive + 0x0005)
	movx	a,@dptr
	mov	r7,a
	cjne	r7,#0xf0,00124$
;	..\src\COMMON\easyax5043.c:743: ax5043_init_registers_rx();
	lcall	_ax5043_init_registers_rx
;	..\src\COMMON\easyax5043.c:744: ax5043_receiver_on_wor();
	lcall	_ax5043_receiver_on_wor
;	..\src\COMMON\easyax5043.c:745: break;
;	..\src\COMMON\easyax5043.c:749: case AXRADIO_MODE_WOR_ACK_RECEIVE:
	sjmp	00134$
00128$:
;	..\src\COMMON\easyax5043.c:750: ax5043_init_registers_rx();
	lcall	_ax5043_init_registers_rx
;	..\src\COMMON\easyax5043.c:751: ax5043_receiver_on_wor();
	lcall	_ax5043_receiver_on_wor
;	..\src\COMMON\easyax5043.c:752: break;
;	..\src\COMMON\easyax5043.c:754: case AXRADIO_MODE_SYNC_ACK_MASTER:
	sjmp	00134$
00129$:
;	..\src\COMMON\easyax5043.c:755: axradio_txbuffer_len = axradio_framing_minpayloadlen;
	mov	dptr,#_axradio_framing_minpayloadlen
	clr	a
	movc	a,@a+dptr
	mov	r7,a
	mov	dptr,#_axradio_txbuffer_len
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:759: case AXRADIO_MODE_WOR_ACK_TRANSMIT:
00131$:
;	..\src\COMMON\easyax5043.c:760: ax5043_init_registers_rx();
	lcall	_ax5043_init_registers_rx
;	..\src\COMMON\easyax5043.c:761: ax5043_receiver_on_continuous();
	lcall	_ax5043_receiver_on_continuous
;	..\src\COMMON\easyax5043.c:762: wtimer_remove(&axradio_timer);
	mov	dptr,#_axradio_timer
	lcall	_wtimer_remove
;	..\src\COMMON\easyax5043.c:763: axradio_timer.time = axradio_framing_ack_timeout;
	mov	dptr,#_axradio_framing_ack_timeout
	clr	a
	movc	a,@a+dptr
	mov	r4,a
	mov	a,#0x01
	movc	a,@a+dptr
	mov	r5,a
	mov	a,#0x02
	movc	a,@a+dptr
	mov	r6,a
	mov	a,#0x03
	movc	a,@a+dptr
	mov	r7,a
	mov	dptr,#(_axradio_timer + 0x0004)
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:764: wtimer0_addrelative(&axradio_timer);
	mov	dptr,#_axradio_timer
	lcall	_wtimer0_addrelative
;	..\src\COMMON\easyax5043.c:765: break;
;	..\src\COMMON\easyax5043.c:767: case AXRADIO_MODE_SYNC_MASTER:
	sjmp	00134$
00132$:
;	..\src\COMMON\easyax5043.c:768: axradio_txbuffer_len = axradio_framing_minpayloadlen;
	mov	dptr,#_axradio_framing_minpayloadlen
	clr	a
	movc	a,@a+dptr
	mov	r7,a
	mov	dptr,#_axradio_txbuffer_len
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:771: default:
00133$:
;	..\src\COMMON\easyax5043.c:772: ax5043_off();
	lcall	_ax5043_off
;	..\src\COMMON\easyax5043.c:774: }
00134$:
;	..\src\COMMON\easyax5043.c:775: if (axradio_mode != AXRADIO_MODE_SYNC_MASTER &&
	mov	a,#0x30
	cjne	a,_axradio_mode,00307$
	sjmp	00136$
00307$:
;	..\src\COMMON\easyax5043.c:776: axradio_mode != AXRADIO_MODE_SYNC_ACK_MASTER &&
	mov	a,#0x31
	cjne	a,_axradio_mode,00308$
	sjmp	00136$
00308$:
;	..\src\COMMON\easyax5043.c:777: axradio_mode != AXRADIO_MODE_SYNC_SLAVE &&
	mov	a,#0x32
	cjne	a,_axradio_mode,00309$
	sjmp	00136$
00309$:
;	..\src\COMMON\easyax5043.c:778: axradio_mode != AXRADIO_MODE_SYNC_ACK_SLAVE)
	mov	a,#0x33
	cjne	a,_axradio_mode,00310$
	sjmp	00136$
00310$:
;	..\src\COMMON\easyax5043.c:779: axradio_syncstate = syncstate_off;
	mov	dptr,#_axradio_syncstate
	clr	a
	movx	@dptr,a
00136$:
;	..\src\COMMON\easyax5043.c:780: update_timeanchor();
	lcall	_update_timeanchor
;	..\src\COMMON\easyax5043.c:781: wtimer_remove_callback(&axradio_cb_transmitend.cb);
	mov	dptr,#_axradio_cb_transmitend
	lcall	_wtimer_remove_callback
;	..\src\COMMON\easyax5043.c:782: axradio_cb_transmitend.st.error = AXRADIO_ERR_NOERROR;
	mov	dptr,#(_axradio_cb_transmitend + 0x0005)
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:783: if (axradio_mode == AXRADIO_MODE_ACK_TRANSMIT ||
	mov	a,#0x12
	cjne	a,_axradio_mode,00311$
	sjmp	00140$
00311$:
;	..\src\COMMON\easyax5043.c:784: axradio_mode == AXRADIO_MODE_WOR_ACK_TRANSMIT ||
	mov	a,#0x13
	cjne	a,_axradio_mode,00312$
	sjmp	00140$
00312$:
;	..\src\COMMON\easyax5043.c:785: axradio_mode == AXRADIO_MODE_SYNC_ACK_MASTER)
	mov	a,#0x31
	cjne	a,_axradio_mode,00141$
00140$:
;	..\src\COMMON\easyax5043.c:786: axradio_cb_transmitend.st.error = AXRADIO_ERR_BUSY;
	mov	dptr,#(_axradio_cb_transmitend + 0x0005)
	mov	a,#0x02
	movx	@dptr,a
00141$:
;	..\src\COMMON\easyax5043.c:787: axradio_cb_transmitend.st.time.t = axradio_timeanchor.radiotimer;
	mov	dptr,#(_axradio_timeanchor + 0x0004)
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#(_axradio_cb_transmitend + 0x0006)
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:788: wtimer_add_callback(&axradio_cb_transmitend.cb);
	mov	dptr,#_axradio_cb_transmitend
	lcall	_wtimer_add_callback
;	..\src\COMMON\easyax5043.c:789: break;
	ljmp	00167$
;	..\src\COMMON\easyax5043.c:792: case trxstate_txcw_xtalwait:
00144$:
;	..\src\COMMON\easyax5043.c:793: AX5043_IRQMASK1 = 0x00;
	mov	dptr,#_AX5043_IRQMASK1
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:794: AX5043_IRQMASK0 = 0x00;
	mov	dptr,#_AX5043_IRQMASK0
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:795: AX5043_PWRMODE = AX5043_PWRSTATE_FULL_TX;
	mov	dptr,#_AX5043_PWRMODE
	mov	a,#0x0d
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:796: axradio_trxstate = trxstate_off;
	mov	_axradio_trxstate,#0x00
;	..\src\COMMON\easyax5043.c:797: update_timeanchor();
	lcall	_update_timeanchor
;	..\src\COMMON\easyax5043.c:798: wtimer_remove_callback(&axradio_cb_transmitstart.cb);
	mov	dptr,#_axradio_cb_transmitstart
	lcall	_wtimer_remove_callback
;	..\src\COMMON\easyax5043.c:799: axradio_cb_transmitstart.st.error = AXRADIO_ERR_NOERROR;
	mov	dptr,#(_axradio_cb_transmitstart + 0x0005)
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:800: axradio_cb_transmitstart.st.time.t = axradio_timeanchor.radiotimer;
	mov	dptr,#(_axradio_timeanchor + 0x0004)
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#(_axradio_cb_transmitstart + 0x0006)
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:801: wtimer_add_callback(&axradio_cb_transmitstart.cb);
	mov	dptr,#_axradio_cb_transmitstart
	lcall	_wtimer_add_callback
;	..\src\COMMON\easyax5043.c:802: break;
	ljmp	00167$
;	..\src\COMMON\easyax5043.c:804: case trxstate_txstream_xtalwait:
00145$:
;	..\src\COMMON\easyax5043.c:805: if (AX5043_IRQREQUEST1 & 0x01) {
	mov	dptr,#_AX5043_IRQREQUEST1
	movx	a,@dptr
	jb	acc.0,00315$
	ljmp	00155$
00315$:
;	..\src\COMMON\easyax5043.c:806: AX5043_RADIOEVENTMASK0 = 0x03; // enable PLL settled and done event
	mov	dptr,#_AX5043_RADIOEVENTMASK0
	mov	a,#0x03
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:807: AX5043_IRQMASK1 = 0x00;
	mov	dptr,#_AX5043_IRQMASK1
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:808: AX5043_IRQMASK0 = 0x40; // enable radio controller irq
	mov	dptr,#_AX5043_IRQMASK0
	mov	a,#0x40
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:809: AX5043_PWRMODE = AX5043_PWRSTATE_FULL_TX;
	mov	dptr,#_AX5043_PWRMODE
	mov	a,#0x0d
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:810: axradio_trxstate = trxstate_txstream;
	mov	_axradio_trxstate,#0x10
;	..\src\COMMON\easyax5043.c:812: goto txstreamdatacb;
;	..\src\COMMON\easyax5043.c:814: case trxstate_txstream:
	sjmp	00155$
00148$:
;	..\src\COMMON\easyax5043.c:816: uint8_t __autodata evt = AX5043_RADIOEVENTREQ0;
	mov	dptr,#_AX5043_RADIOEVENTREQ0
	movx	a,@dptr
;	..\src\COMMON\easyax5043.c:817: if (evt & 0x03)
	mov	r7,a
	anl	a,#0x03
	jz	00150$
;	..\src\COMMON\easyax5043.c:818: update_timeanchor();
	push	ar7
	lcall	_update_timeanchor
	pop	ar7
00150$:
;	..\src\COMMON\easyax5043.c:819: if (evt & 0x01) {
	mov	a,r7
	jnb	acc.0,00152$
;	..\src\COMMON\easyax5043.c:820: wtimer_remove_callback(&axradio_cb_transmitend.cb);
	mov	dptr,#_axradio_cb_transmitend
	push	ar7
	lcall	_wtimer_remove_callback
;	..\src\COMMON\easyax5043.c:821: axradio_cb_transmitend.st.error = AXRADIO_ERR_NOERROR;
	mov	dptr,#(_axradio_cb_transmitend + 0x0005)
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:822: axradio_cb_transmitend.st.time.t = axradio_timeanchor.radiotimer;
	mov	dptr,#(_axradio_timeanchor + 0x0004)
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	mov	dptr,#(_axradio_cb_transmitend + 0x0006)
	mov	a,r3
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:823: wtimer_add_callback(&axradio_cb_transmitend.cb);
	mov	dptr,#_axradio_cb_transmitend
	lcall	_wtimer_add_callback
	pop	ar7
00152$:
;	..\src\COMMON\easyax5043.c:825: if (evt & 0x02) {
	mov	a,r7
	jnb	acc.1,00155$
;	..\src\COMMON\easyax5043.c:826: wtimer_remove_callback(&axradio_cb_transmitstart.cb);
	mov	dptr,#_axradio_cb_transmitstart
	lcall	_wtimer_remove_callback
;	..\src\COMMON\easyax5043.c:827: axradio_cb_transmitstart.st.error = AXRADIO_ERR_NOERROR;
	mov	dptr,#(_axradio_cb_transmitstart + 0x0005)
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:828: axradio_cb_transmitstart.st.time.t = axradio_timeanchor.radiotimer;
	mov	dptr,#(_axradio_timeanchor + 0x0004)
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#(_axradio_cb_transmitstart + 0x0006)
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:829: wtimer_add_callback(&axradio_cb_transmitstart.cb);
	mov	dptr,#_axradio_cb_transmitstart
	lcall	_wtimer_add_callback
;	..\src\COMMON\easyax5043.c:832: txstreamdatacb:
00155$:
;	..\src\COMMON\easyax5043.c:833: if (AX5043_IRQREQUEST0 & AX5043_IRQMASK0 & 0x08) {
	mov	dptr,#_AX5043_IRQREQUEST0
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#_AX5043_IRQMASK0
	movx	a,@dptr
	mov	r6,a
	anl	a,r7
	jb	acc.3,00319$
	ljmp	00167$
00319$:
;	..\src\COMMON\easyax5043.c:834: AX5043_IRQMASK0 &= (uint8_t)~0x08;
	mov	dptr,#_AX5043_IRQMASK0
	movx	a,@dptr
	anl	acc,#0xf7
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:835: update_timeanchor();
	lcall	_update_timeanchor
;	..\src\COMMON\easyax5043.c:836: wtimer_remove_callback(&axradio_cb_transmitdata.cb);
	mov	dptr,#_axradio_cb_transmitdata
	lcall	_wtimer_remove_callback
;	..\src\COMMON\easyax5043.c:837: axradio_cb_transmitdata.st.error = AXRADIO_ERR_NOERROR;
	mov	dptr,#(_axradio_cb_transmitdata + 0x0005)
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:838: axradio_cb_transmitdata.st.time.t = axradio_timeanchor.radiotimer;
	mov	dptr,#(_axradio_timeanchor + 0x0004)
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#(_axradio_cb_transmitdata + 0x0006)
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:839: wtimer_add_callback(&axradio_cb_transmitdata.cb);
	mov	dptr,#_axradio_cb_transmitdata
	lcall	_wtimer_add_callback
;	..\src\COMMON\easyax5043.c:841: break;
;	..\src\COMMON\easyax5043.c:843: case trxstate_rxwor:
	sjmp	00167$
00158$:
;	..\src\COMMON\easyax5043.c:846: if( AX5043_IRQREQUEST0 & 0x80 ) // vdda ready (note irqinversion does not act upon AX5043_IRQREQUEST0)
	mov	dptr,#_AX5043_IRQREQUEST0
	movx	a,@dptr
	jnb	acc.7,00160$
;	..\src\COMMON\easyax5043.c:848: AX5043_IRQINVERSION0 |= 0x80; // invert pwr irq, so it does not fire continuously
	mov	dptr,#_AX5043_IRQINVERSION0
	movx	a,@dptr
	orl	acc,#0x80
	movx	@dptr,a
	sjmp	00161$
00160$:
;	..\src\COMMON\easyax5043.c:852: AX5043_IRQINVERSION0 &= (uint8_t)~0x80; // drop pwr irq inversion --> armed again
	mov	dptr,#_AX5043_IRQINVERSION0
	movx	a,@dptr
	anl	acc,#0x7f
	movx	@dptr,a
00161$:
;	..\src\COMMON\easyax5043.c:856: if( AX5043_IRQREQUEST1 & 0x01 ) // XTAL ready
	mov	dptr,#_AX5043_IRQREQUEST1
	movx	a,@dptr
	jnb	acc.0,00163$
;	..\src\COMMON\easyax5043.c:858: AX5043_IRQINVERSION1 |= 0x01; // invert the xtal ready irq so it does not fire continuously
	mov	dptr,#_AX5043_IRQINVERSION1
	movx	a,@dptr
	orl	acc,#0x01
	movx	@dptr,a
	sjmp	00165$
00163$:
;	..\src\COMMON\easyax5043.c:862: AX5043_IRQINVERSION1 &= (uint8_t)~0x01; // drop xtal ready irq inversion --> armed again for next wake-up
	mov	dptr,#_AX5043_IRQINVERSION1
	movx	a,@dptr
	anl	acc,#0xfe
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:863: AX5043_0xF30 = f30_saved;
	mov	dptr,#_f30_saved
	movx	a,@dptr
	mov	dptr,#_AX5043_0xF30
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:864: AX5043_0xF31 = f31_saved;
	mov	dptr,#_f31_saved
	movx	a,@dptr
	mov	dptr,#_AX5043_0xF31
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:865: AX5043_0xF32 = f32_saved;
	mov	dptr,#_f32_saved
	movx	a,@dptr
	mov	dptr,#_AX5043_0xF32
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:866: AX5043_0xF33 = f33_saved;
	mov	dptr,#_f33_saved
	movx	a,@dptr
	mov	dptr,#_AX5043_0xF33
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:870: case trxstate_rx:
00165$:
;	..\src\COMMON\easyax5043.c:871: receive_isr();
	lcall	_receive_isr
;	..\src\COMMON\easyax5043.c:874: } // end switch(axradio_trxstate)
00167$:
;	..\src\COMMON\easyax5043.c:875: } //end radio_isr
	pop	psw
	pop	(0+0)
	pop	(0+1)
	pop	(0+2)
	pop	(0+3)
	pop	(0+4)
	pop	(0+5)
	pop	(0+6)
	pop	(0+7)
	pop	dph
	pop	dpl
	pop	b
	pop	acc
	pop	bits
	reti
;------------------------------------------------------------
;Allocation info for local variables in function 'ax5043_receiver_on_continuous'
;------------------------------------------------------------
;rschanged_int             Allocated to registers r6 
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:878: __reentrantb void ax5043_receiver_on_continuous(void) __reentrant
;	-----------------------------------------
;	 function ax5043_receiver_on_continuous
;	-----------------------------------------
_ax5043_receiver_on_continuous:
;	..\src\COMMON\easyax5043.c:880: uint8_t rschanged_int = (axradio_framing_enable_sfdcallback | (axradio_mode == AXRADIO_MODE_SYNC_ACK_SLAVE) | (axradio_mode == AXRADIO_MODE_SYNC_SLAVE) );
	mov	a,#0x33
	cjne	a,_axradio_mode,00116$
	mov	a,#0x01
	sjmp	00117$
00116$:
	clr	a
00117$:
	mov	r7,a
	mov	dptr,#_axradio_framing_enable_sfdcallback
	clr	a
	movc	a,@a+dptr
	orl	ar7,a
	mov	a,#0x32
	cjne	a,_axradio_mode,00118$
	mov	a,#0x01
	sjmp	00119$
00118$:
	clr	a
00119$:
	orl	ar7,a
;	..\src\COMMON\easyax5043.c:881: if(rschanged_int)
	mov	a,r7
	mov	r6,a
	jz	00102$
;	..\src\COMMON\easyax5043.c:882: AX5043_RADIOEVENTMASK0 = 0x04;
	mov	dptr,#_AX5043_RADIOEVENTMASK0
	mov	a,#0x04
	movx	@dptr,a
00102$:
;	..\src\COMMON\easyax5043.c:883: AX5043_RSSIREFERENCE = axradio_phy_rssireference;
	mov	dptr,#_axradio_phy_rssireference
	clr	a
	movc	a,@a+dptr
	mov	dptr,#_AX5043_RSSIREFERENCE
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:884: ax5043_set_registers_rxcont();
	push	ar6
	lcall	_ax5043_set_registers_rxcont
	pop	ar6
;	..\src\COMMON\easyax5043.c:897: AX5043_PKTSTOREFLAGS &= (uint8_t)~0x40;
	mov	dptr,#_AX5043_PKTSTOREFLAGS
	movx	a,@dptr
	anl	acc,#0xbf
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:900: AX5043_FIFOSTAT = 3; // clear FIFO data & flags
	mov	dptr,#_AX5043_FIFOSTAT
	mov	a,#0x03
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:901: AX5043_PWRMODE = AX5043_PWRSTATE_FULL_RX;
	mov	dptr,#_AX5043_PWRMODE
	mov	a,#0x09
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:902: axradio_trxstate = trxstate_rx;
	mov	_axradio_trxstate,#0x01
;	..\src\COMMON\easyax5043.c:903: if(rschanged_int)
	mov	a,r6
	jz	00104$
;	..\src\COMMON\easyax5043.c:904: AX5043_IRQMASK0 = 0x41; //  enable FIFO not empty / radio controller irq
	mov	dptr,#_AX5043_IRQMASK0
	mov	a,#0x41
	movx	@dptr,a
	sjmp	00105$
00104$:
;	..\src\COMMON\easyax5043.c:906: AX5043_IRQMASK0 = 0x01; //  enable FIFO not empty
	mov	dptr,#_AX5043_IRQMASK0
	mov	a,#0x01
	movx	@dptr,a
00105$:
;	..\src\COMMON\easyax5043.c:907: AX5043_IRQMASK1 = 0x00;
	mov	dptr,#_AX5043_IRQMASK1
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:908: }
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'ax5043_receiver_on_wor'
;------------------------------------------------------------
;wp                        Allocated to registers r4 r5 
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:910: __reentrantb void ax5043_receiver_on_wor(void) __reentrant
;	-----------------------------------------
;	 function ax5043_receiver_on_wor
;	-----------------------------------------
_ax5043_receiver_on_wor:
;	..\src\COMMON\easyax5043.c:912: AX5043_BGNDRSSIGAIN = 0x02;
	mov	dptr,#_AX5043_BGNDRSSIGAIN
	mov	a,#0x02
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:913: if(axradio_framing_enable_sfdcallback)
	mov	dptr,#_axradio_framing_enable_sfdcallback
	clr	a
	movc	a,@a+dptr
	jz	00102$
;	..\src\COMMON\easyax5043.c:914: AX5043_RADIOEVENTMASK0 = 0x04;
	mov	dptr,#_AX5043_RADIOEVENTMASK0
	mov	a,#0x04
	movx	@dptr,a
00102$:
;	..\src\COMMON\easyax5043.c:915: AX5043_FIFOSTAT = 3; // clear FIFO data & flags
	mov	dptr,#_AX5043_FIFOSTAT
	mov	a,#0x03
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:916: AX5043_LPOSCCONFIG = 0x01; // start LPOSC, slow mode
	mov	dptr,#_AX5043_LPOSCCONFIG
	mov	a,#0x01
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:917: AX5043_RSSIREFERENCE = axradio_phy_rssireference;
	mov	dptr,#_axradio_phy_rssireference
	clr	a
	movc	a,@a+dptr
	mov	dptr,#_AX5043_RSSIREFERENCE
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:918: ax5043_set_registers_rxwor();
	lcall	_ax5043_set_registers_rxwor
;	..\src\COMMON\easyax5043.c:919: AX5043_PKTSTOREFLAGS &= (uint8_t)~0x40;
	mov	dptr,#_AX5043_PKTSTOREFLAGS
	movx	a,@dptr
	anl	acc,#0xbf
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:921: AX5043_PWRMODE = AX5043_PWRSTATE_WOR_RX;
	mov	dptr,#_AX5043_PWRMODE
	mov	a,#0x0b
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:922: axradio_trxstate = trxstate_rxwor;
	mov	_axradio_trxstate,#0x02
;	..\src\COMMON\easyax5043.c:923: if(axradio_framing_enable_sfdcallback)
	mov	dptr,#_axradio_framing_enable_sfdcallback
	clr	a
	movc	a,@a+dptr
	jz	00104$
;	..\src\COMMON\easyax5043.c:924: AX5043_IRQMASK0 = 0x41; //  enable FIFO not empty / radio controller irq
	mov	dptr,#_AX5043_IRQMASK0
	mov	a,#0x41
	movx	@dptr,a
	sjmp	00105$
00104$:
;	..\src\COMMON\easyax5043.c:926: AX5043_IRQMASK0 = 0x01; //  enable FIFO not empty
	mov	dptr,#_AX5043_IRQMASK0
	mov	a,#0x01
	movx	@dptr,a
00105$:
;	..\src\COMMON\easyax5043.c:928: if( ( (PALTRADIO & 0x40) && ((AX5043_PINFUNCPWRAMP & 0x0F) == 0x07) ) || ( (PALTRADIO & 0x80) && ( (AX5043_PINFUNCANTSEL & 0x07 ) == 0x04 ) ) ) // pass through of TCXO_EN
	mov	dptr,#_PALTRADIO
	movx	a,@dptr
	jnb	acc.6,00110$
	mov	dptr,#_AX5043_PINFUNCPWRAMP
	movx	a,@dptr
	mov	r7,a
	anl	ar7,#0x0f
	mov	r6,#0x00
	cjne	r7,#0x07,00136$
	cjne	r6,#0x00,00136$
	sjmp	00106$
00136$:
00110$:
	mov	dptr,#_PALTRADIO
	movx	a,@dptr
	jnb	acc.7,00107$
	mov	dptr,#_AX5043_PINFUNCANTSEL
	movx	a,@dptr
	mov	r7,a
	anl	ar7,#0x07
	mov	r6,#0x00
	cjne	r7,#0x04,00107$
	cjne	r6,#0x00,00107$
00106$:
;	..\src\COMMON\easyax5043.c:931: AX5043_IRQMASK0 |= 0x80; // power irq (AX8052F143 WOR with TCXO)
	mov	dptr,#_AX5043_IRQMASK0
	movx	a,@dptr
	orl	acc,#0x80
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:932: AX5043_POWIRQMASK = 0x90; // interrupt when vddana ready (AX8052F143 WOR with TCXO)
	mov	dptr,#_AX5043_POWIRQMASK
	mov	a,#0x90
	movx	@dptr,a
00107$:
;	..\src\COMMON\easyax5043.c:935: AX5043_IRQMASK1 = 0x01; // xtal ready
	mov	dptr,#_AX5043_IRQMASK1
	mov	a,#0x01
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:937: uint16_t wp = axradio_wor_period;
	mov	dptr,#_axradio_wor_period
	clr	a
	movc	a,@a+dptr
	mov	r6,a
	mov	a,#0x01
	movc	a,@a+dptr
;	..\src\COMMON\easyax5043.c:938: AX5043_WAKEUPFREQ1 = (wp >> 8) & 0xFF;
	mov	r7,a
	mov	dptr,#_AX5043_WAKEUPFREQ1
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:939: AX5043_WAKEUPFREQ0 = (wp >> 0) & 0xFF;  // actually wakeup period measured in LP OSC cycles
	mov	dptr,#_AX5043_WAKEUPFREQ0
	mov	a,r6
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:940: wp += radio_read16((uint16_t)&AX5043_WAKEUPTIMER1);
	mov	r4,#_AX5043_WAKEUPTIMER1
	mov	r5,#(_AX5043_WAKEUPTIMER1 >> 8)
	mov	dpl,r4
	mov	dph,r5
	lcall	_radio_read16
	mov	r4,dpl
	mov	r5,dph
	mov	a,r4
	add	a,r6
	mov	r4,a
	mov	a,r5
	addc	a,r7
;	..\src\COMMON\easyax5043.c:941: AX5043_WAKEUP1 = (wp >> 8) & 0xFF;
	mov	dptr,#_AX5043_WAKEUP1
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:942: AX5043_WAKEUP0 = (wp >> 0) & 0xFF;
	mov	dptr,#_AX5043_WAKEUP0
	mov	a,r4
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:944: }
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'ax5043_prepare_tx'
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:945: __reentrantb void ax5043_prepare_tx(void) __reentrant
;	-----------------------------------------
;	 function ax5043_prepare_tx
;	-----------------------------------------
_ax5043_prepare_tx:
;	..\src\COMMON\easyax5043.c:947: AX5043_PWRMODE = AX5043_PWRSTATE_XTAL_ON;
	mov	dptr,#_AX5043_PWRMODE
	mov	a,#0x05
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:948: AX5043_PWRMODE = AX5043_PWRSTATE_FIFO_ON;
	mov	a,#0x07
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:949: ax5043_init_registers_tx();
	lcall	_ax5043_init_registers_tx
;	..\src\COMMON\easyax5043.c:950: AX5043_FIFOTHRESH1 = 0;
	mov	dptr,#_AX5043_FIFOTHRESH1
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:951: AX5043_FIFOTHRESH0 = 0x80;
	mov	dptr,#_AX5043_FIFOTHRESH0
	mov	a,#0x80
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:952: axradio_trxstate = trxstate_tx_xtalwait;
	mov	_axradio_trxstate,#0x09
;	..\src\COMMON\easyax5043.c:953: AX5043_IRQMASK0 = 0x00;
	mov	dptr,#_AX5043_IRQMASK0
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:954: AX5043_IRQMASK1 = 0x01; // enable xtal ready interrupt
	mov	dptr,#_AX5043_IRQMASK1
	inc	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:955: AX5043_POWSTICKYSTAT; // clear pwr management sticky status --> brownout gate works
	mov	dptr,#_AX5043_POWSTICKYSTAT
	movx	a,@dptr
;	..\src\COMMON\easyax5043.c:956: }
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'ax5043_off'
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:958: __reentrantb void ax5043_off(void) __reentrant
;	-----------------------------------------
;	 function ax5043_off
;	-----------------------------------------
_ax5043_off:
;	..\src\COMMON\easyax5043.c:960: ax5043_off_xtal();
	lcall	_ax5043_off_xtal
;	..\src\COMMON\easyax5043.c:961: AX5043_PWRMODE = AX5043_PWRSTATE_POWERDOWN;
	mov	dptr,#_AX5043_PWRMODE
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:962: }
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'ax5043_off_xtal'
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:964: __reentrantb void ax5043_off_xtal(void) __reentrant
;	-----------------------------------------
;	 function ax5043_off_xtal
;	-----------------------------------------
_ax5043_off_xtal:
;	..\src\COMMON\easyax5043.c:966: AX5043_IRQMASK0 = 0x00; // IRQ off
	mov	dptr,#_AX5043_IRQMASK0
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:967: AX5043_IRQMASK1 = 0x00;
	mov	dptr,#_AX5043_IRQMASK1
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:968: AX5043_PWRMODE = AX5043_PWRSTATE_XTAL_ON;
	mov	dptr,#_AX5043_PWRMODE
	mov	a,#0x05
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:969: AX5043_LPOSCCONFIG = 0x00; // LPOSC off
	mov	dptr,#_AX5043_LPOSCCONFIG
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:970: axradio_trxstate = trxstate_off;
;	1-genFromRTrack replaced	mov	_axradio_trxstate,#0x00
	mov	_axradio_trxstate,a
;	..\src\COMMON\easyax5043.c:971: }
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'axradio_wait_for_xtal'
;------------------------------------------------------------
;iesave                    Allocated to registers r7 
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:973: void axradio_wait_for_xtal(void)
;	-----------------------------------------
;	 function axradio_wait_for_xtal
;	-----------------------------------------
_axradio_wait_for_xtal:
;	..\src\COMMON\easyax5043.c:975: uint8_t __autodata iesave = IE & 0x80;
	mov	a,_IE
	anl	a,#0x80
	mov	r7,a
;	..\src\COMMON\easyax5043.c:976: EA = 0;
;	assignBit
	clr	_EA
;	..\src\COMMON\easyax5043.c:977: axradio_trxstate = trxstate_wait_xtal;
	mov	_axradio_trxstate,#0x03
;	..\src\COMMON\easyax5043.c:978: AX5043_IRQMASK1 |= 0x01; // enable xtal ready interrupt
	mov	dptr,#_AX5043_IRQMASK1
	movx	a,@dptr
	orl	acc,#0x01
	movx	@dptr,a
00104$:
;	..\src\COMMON\easyax5043.c:980: EA = 0;
;	assignBit
	clr	_EA
;	..\src\COMMON\easyax5043.c:981: if (axradio_trxstate == trxstate_xtal_ready)
	mov	a,#0x04
	cjne	a,_axradio_trxstate,00116$
	sjmp	00103$
00116$:
;	..\src\COMMON\easyax5043.c:983: wtimer_idle(WTFLAG_CANSTANDBY);
	mov	dpl,#0x02
	push	ar7
	lcall	_wtimer_idle
;	..\src\COMMON\easyax5043.c:984: EA = 1;
;	assignBit
	setb	_EA
;	..\src\COMMON\easyax5043.c:985: wtimer_runcallbacks();
	lcall	_wtimer_runcallbacks
	pop	ar7
	sjmp	00104$
00103$:
;	..\src\COMMON\easyax5043.c:987: IE |= iesave;
	mov	a,r7
	orl	_IE,a
;	..\src\COMMON\easyax5043.c:988: }
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'axradio_setaddrregs'
;------------------------------------------------------------
;pn                        Allocated to registers r6 r7 
;inv                       Allocated to registers r5 
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:990: static void axradio_setaddrregs(void)
;	-----------------------------------------
;	 function axradio_setaddrregs
;	-----------------------------------------
_axradio_setaddrregs:
;	..\src\COMMON\easyax5043.c:992: AX5043_PKTADDR0 = axradio_localaddr.addr[0];
	mov	dptr,#_axradio_localaddr
	movx	a,@dptr
	mov	dptr,#_AX5043_PKTADDR0
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:993: AX5043_PKTADDR1 = axradio_localaddr.addr[1];
	mov	dptr,#(_axradio_localaddr + 0x0001)
	movx	a,@dptr
	mov	dptr,#_AX5043_PKTADDR1
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:994: AX5043_PKTADDR2 = axradio_localaddr.addr[2];
	mov	dptr,#(_axradio_localaddr + 0x0002)
	movx	a,@dptr
	mov	dptr,#_AX5043_PKTADDR2
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:995: AX5043_PKTADDR3 = axradio_localaddr.addr[3];
	mov	dptr,#(_axradio_localaddr + 0x0003)
	movx	a,@dptr
	mov	dptr,#_AX5043_PKTADDR3
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:997: AX5043_PKTADDRMASK0 = axradio_localaddr.mask[0];
	mov	dptr,#(_axradio_localaddr + 0x0004)
	movx	a,@dptr
	mov	dptr,#_AX5043_PKTADDRMASK0
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:998: AX5043_PKTADDRMASK1 = axradio_localaddr.mask[1];
	mov	dptr,#(_axradio_localaddr + 0x0005)
	movx	a,@dptr
	mov	dptr,#_AX5043_PKTADDRMASK1
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:999: AX5043_PKTADDRMASK2 = axradio_localaddr.mask[2];
	mov	dptr,#(_axradio_localaddr + 0x0006)
	movx	a,@dptr
	mov	dptr,#_AX5043_PKTADDRMASK2
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1000: AX5043_PKTADDRMASK3 = axradio_localaddr.mask[3];
	mov	dptr,#(_axradio_localaddr + 0x0007)
	movx	a,@dptr
	mov	dptr,#_AX5043_PKTADDRMASK3
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1002: if (axradio_phy_pn9 && axradio_framing_addrlen) {
	mov	dptr,#_axradio_phy_pn9
	clr	a
	movc	a,@a+dptr
	jnz	00123$
	ret
00123$:
	mov	dptr,#_axradio_framing_addrlen
	clr	a
	movc	a,@a+dptr
	jnz	00124$
	ret
00124$:
;	..\src\COMMON\easyax5043.c:1003: uint16_t __autodata pn = 0x1ff;
	mov	r6,#0xff
	mov	r7,#0x01
;	..\src\COMMON\easyax5043.c:1004: uint8_t __autodata inv = -(AX5043_ENCODING & 0x01);
	mov	dptr,#_AX5043_ENCODING
	movx	a,@dptr
	mov	r5,a
	anl	ar5,#0x01
	clr	c
	clr	a
	subb	a,r5
	mov	r5,a
;	..\src\COMMON\easyax5043.c:1005: if (axradio_framing_destaddrpos != 0xff)
	mov	dptr,#_axradio_framing_destaddrpos
	clr	a
	movc	a,@a+dptr
	mov	r4,a
	cjne	r4,#0xff,00125$
	sjmp	00102$
00125$:
;	..\src\COMMON\easyax5043.c:1006: pn = pn9_advance_bits(pn, axradio_framing_destaddrpos << 3);
	clr	a
	rr	a
	anl	a,#0xf8
	xch	a,r4
	swap	a
	rr	a
	xch	a,r4
	xrl	a,r4
	xch	a,r4
	anl	a,#0xf8
	xch	a,r4
	xrl	a,r4
	mov	r3,a
	push	ar5
	push	ar4
	push	ar3
	mov	dptr,#0x01ff
	lcall	_pn9_advance_bits
	mov	r6,dpl
	mov	r7,dph
	dec	sp
	dec	sp
	pop	ar5
00102$:
;	..\src\COMMON\easyax5043.c:1007: AX5043_PKTADDR0 ^= pn ^ inv;
	mov	r4,#0x00
	mov	a,r5
	xrl	a,r6
	mov	r2,a
	mov	a,r4
	xrl	a,r7
	mov	r3,a
	mov	dptr,#_AX5043_PKTADDR0
	movx	a,@dptr
	mov	r1,#0x00
	xrl	ar2,a
	mov	a,r1
	xrl	ar3,a
	mov	dptr,#_AX5043_PKTADDR0
	mov	a,r2
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1008: pn = pn9_advance_byte(pn);
	mov	dpl,r6
	mov	dph,r7
	push	ar5
	push	ar4
	lcall	_pn9_advance_byte
	mov	r6,dpl
	mov	r7,dph
	pop	ar4
	pop	ar5
;	..\src\COMMON\easyax5043.c:1009: AX5043_PKTADDR1 ^= pn ^ inv;
	mov	a,r5
	xrl	a,r6
	mov	r2,a
	mov	a,r4
	xrl	a,r7
	mov	r3,a
	mov	dptr,#_AX5043_PKTADDR1
	movx	a,@dptr
	mov	r1,#0x00
	xrl	ar2,a
	mov	a,r1
	xrl	ar3,a
	mov	dptr,#_AX5043_PKTADDR1
	mov	a,r2
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1010: pn = pn9_advance_byte(pn);
	mov	dpl,r6
	mov	dph,r7
	push	ar5
	push	ar4
	lcall	_pn9_advance_byte
	mov	r6,dpl
	mov	r7,dph
	pop	ar4
	pop	ar5
;	..\src\COMMON\easyax5043.c:1011: AX5043_PKTADDR2 ^= pn ^ inv;
	mov	a,r5
	xrl	a,r6
	mov	r2,a
	mov	a,r4
	xrl	a,r7
	mov	r3,a
	mov	dptr,#_AX5043_PKTADDR2
	movx	a,@dptr
	mov	r1,#0x00
	xrl	ar2,a
	mov	a,r1
	xrl	ar3,a
	mov	dptr,#_AX5043_PKTADDR2
	mov	a,r2
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1012: pn = pn9_advance_byte(pn);
	mov	dpl,r6
	mov	dph,r7
	push	ar5
	push	ar4
	lcall	_pn9_advance_byte
	mov	a,dpl
	mov	b,dph
	pop	ar4
	pop	ar5
;	..\src\COMMON\easyax5043.c:1013: AX5043_PKTADDR3 ^= pn ^ inv;
	xrl	ar5,a
	mov	a,b
	xrl	ar4,a
	mov	dptr,#_AX5043_PKTADDR3
	movx	a,@dptr
	mov	r6,#0x00
	xrl	ar5,a
	mov	a,r6
	xrl	ar4,a
	mov	dptr,#_AX5043_PKTADDR3
	mov	a,r5
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1015: }
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'ax5043_init_registers'
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:1017: static void ax5043_init_registers(void)
;	-----------------------------------------
;	 function ax5043_init_registers
;	-----------------------------------------
_ax5043_init_registers:
;	..\src\COMMON\easyax5043.c:1019: ax5043_set_registers();
	lcall	_ax5043_set_registers
;	..\src\COMMON\easyax5043.c:1024: AX5043_PKTLENOFFSET += axradio_framing_swcrclen; // add len offs for software CRC16 (used for both, fixed and variable length packets
	mov	dptr,#_axradio_framing_swcrclen
	clr	a
	movc	a,@a+dptr
	mov	r7,a
	mov	dptr,#_AX5043_PKTLENOFFSET
	movx	a,@dptr
	add	a,r7
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1025: AX5043_PINFUNCIRQ = 0x03; // use as IRQ pin
	mov	dptr,#_AX5043_PINFUNCIRQ
	mov	a,#0x03
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1026: AX5043_PKTSTOREFLAGS = axradio_phy_innerfreqloop ? 0x13 : 0x15; // store RF offset, RSSI and delimiter timing
	mov	dptr,#_axradio_phy_innerfreqloop
	clr	a
	movc	a,@a+dptr
	jz	00103$
	mov	r6,#0x13
	mov	r7,#0x00
	sjmp	00104$
00103$:
	mov	r6,#0x15
	mov	r7,#0x00
00104$:
	mov	dptr,#_AX5043_PKTSTOREFLAGS
	mov	a,r6
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1027: axradio_setaddrregs();
;	..\src\COMMON\easyax5043.c:1028: }
	ljmp	_axradio_setaddrregs
;------------------------------------------------------------
;Allocation info for local variables in function 'axradio_sync_addtime'
;------------------------------------------------------------
;dt                        Allocated to registers r4 r5 r6 r7 
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:1034: static __reentrantb void axradio_sync_addtime(uint32_t dt) __reentrant
;	-----------------------------------------
;	 function axradio_sync_addtime
;	-----------------------------------------
_axradio_sync_addtime:
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
;	..\src\COMMON\easyax5043.c:1036: axradio_sync_time += dt;
	mov	dptr,#_axradio_sync_time
	movx	a,@dptr
	mov	r0,a
	inc	dptr
	movx	a,@dptr
	mov	r1,a
	inc	dptr
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	mov	dptr,#_axradio_sync_time
	mov	a,r4
	add	a,r0
	movx	@dptr,a
	mov	a,r5
	addc	a,r1
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	addc	a,r2
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	addc	a,r3
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1037: }
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'axradio_sync_subtime'
;------------------------------------------------------------
;dt                        Allocated to registers r4 r5 r6 r7 
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:1039: static __reentrantb void axradio_sync_subtime(uint32_t dt) __reentrant
;	-----------------------------------------
;	 function axradio_sync_subtime
;	-----------------------------------------
_axradio_sync_subtime:
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
;	..\src\COMMON\easyax5043.c:1041: axradio_sync_time -= dt;
	mov	dptr,#_axradio_sync_time
	movx	a,@dptr
	mov	r0,a
	inc	dptr
	movx	a,@dptr
	mov	r1,a
	inc	dptr
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	mov	dptr,#_axradio_sync_time
	mov	a,r0
	clr	c
	subb	a,r4
	movx	@dptr,a
	mov	a,r1
	subb	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r2
	subb	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r3
	subb	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1042: }
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'axradio_sync_settimeradv'
;------------------------------------------------------------
;dt                        Allocated to registers r4 r5 r6 r7 
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:1044: static __reentrantb void axradio_sync_settimeradv(uint32_t dt) __reentrant
;	-----------------------------------------
;	 function axradio_sync_settimeradv
;	-----------------------------------------
_axradio_sync_settimeradv:
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
;	..\src\COMMON\easyax5043.c:1046: axradio_timer.time = axradio_sync_time;
	mov	dptr,#_axradio_sync_time
	movx	a,@dptr
	mov	r0,a
	inc	dptr
	movx	a,@dptr
	mov	r1,a
	inc	dptr
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	mov	dptr,#(_axradio_timer + 0x0004)
	mov	a,r0
	movx	@dptr,a
	mov	a,r1
	inc	dptr
	movx	@dptr,a
	mov	a,r2
	inc	dptr
	movx	@dptr,a
	mov	a,r3
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1047: axradio_timer.time -= dt;
	mov	a,r0
	clr	c
	subb	a,r4
	mov	r4,a
	mov	a,r1
	subb	a,r5
	mov	r5,a
	mov	a,r2
	subb	a,r6
	mov	r6,a
	mov	a,r3
	subb	a,r7
	mov	r7,a
	mov	dptr,#(_axradio_timer + 0x0004)
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1048: }
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'axradio_sync_adjustperiodcorr'
;------------------------------------------------------------
;dt                        Allocated to registers r0 r1 r2 r3 
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:1050: static void axradio_sync_adjustperiodcorr(void)
;	-----------------------------------------
;	 function axradio_sync_adjustperiodcorr
;	-----------------------------------------
_axradio_sync_adjustperiodcorr:
;	..\src\COMMON\easyax5043.c:1052: int32_t __autodata dt = axradio_conv_time_totimer0(axradio_cb_receive.st.time.t) - axradio_sync_time;
	mov	dptr,#(_axradio_cb_receive + 0x0006)
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	lcall	_axradio_conv_time_totimer0
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	dptr,#_axradio_sync_time
	movx	a,@dptr
	mov	r0,a
	inc	dptr
	movx	a,@dptr
	mov	r1,a
	inc	dptr
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	mov	a,r4
	clr	c
	subb	a,r0
	mov	r4,a
	mov	a,r5
	subb	a,r1
	mov	r5,a
	mov	a,r6
	subb	a,r2
	mov	r6,a
	mov	a,r7
	subb	a,r3
	mov	r7,a
;	..\src\COMMON\easyax5043.c:1053: axradio_cb_receive.st.rx.phy.timeoffset = dt;
	mov	ar2,r4
	mov	ar3,r5
	mov	dptr,#(_axradio_cb_receive + 0x0010)
	mov	a,r2
	movx	@dptr,a
	mov	a,r3
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1054: if (!checksignedlimit16(axradio_sync_periodcorr, axradio_sync_slave_maxperiod)) {
	mov	dptr,#_axradio_sync_periodcorr
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	mov	dptr,#_axradio_sync_slave_maxperiod
	clr	a
	movc	a,@a+dptr
	push	acc
	mov	a,#0x01
	movc	a,@a+dptr
	push	acc
	mov	dpl,r2
	mov	dph,r3
	lcall	_checksignedlimit16
	mov	r3,dpl
	dec	sp
	dec	sp
	mov	a,r3
	jnz	00102$
;	..\src\COMMON\easyax5043.c:1055: axradio_sync_addtime(dt);
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	mov	a,r7
	push	ar7
	push	ar6
	push	ar5
	push	ar4
	lcall	_axradio_sync_addtime
	pop	ar4
	pop	ar5
	pop	ar6
	pop	ar7
;	..\src\COMMON\easyax5043.c:1056: dt <<= SYNC_K1;
	mov	ar2,r6
	mov	a,r7
	swap	a
	rl	a
	anl	a,#0xe0
	xch	a,r2
	swap	a
	rl	a
	xch	a,r2
	xrl	a,r2
	xch	a,r2
	anl	a,#0xe0
	xch	a,r2
	xrl	a,r2
	mov	r3,a
	mov	a,r5
	swap	a
	rl	a
	anl	a,#0x1f
	orl	a,r2
	mov	r2,a
	mov	ar0,r4
	mov	a,r5
	swap	a
	rl	a
	anl	a,#0xe0
	xch	a,r0
	swap	a
	rl	a
	xch	a,r0
	xrl	a,r0
	xch	a,r0
	anl	a,#0xe0
	xch	a,r0
	xrl	a,r0
	mov	r1,a
;	..\src\COMMON\easyax5043.c:1057: axradio_sync_periodcorr = dt;
	mov	dptr,#_axradio_sync_periodcorr
	mov	a,r0
	movx	@dptr,a
	mov	a,r1
	inc	dptr
	movx	@dptr,a
	sjmp	00103$
00102$:
;	..\src\COMMON\easyax5043.c:1059: axradio_sync_periodcorr += dt;
	mov	dptr,#_axradio_sync_periodcorr
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	mov	ar0,r4
	mov	ar1,r5
	mov	dptr,#_axradio_sync_periodcorr
	mov	a,r0
	add	a,r2
	movx	@dptr,a
	mov	a,r1
	addc	a,r3
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1060: dt >>= SYNC_K0;
	mov	a,r7
	mov	c,acc.7
	rrc	a
	mov	r7,a
	mov	a,r6
	rrc	a
	mov	r6,a
	mov	a,r5
	rrc	a
	mov	r5,a
	mov	a,r4
	rrc	a
	mov	r4,a
	mov	a,r7
	mov	c,acc.7
	rrc	a
	mov	r7,a
	mov	a,r6
	rrc	a
	mov	r6,a
	mov	a,r5
	rrc	a
	mov	r5,a
	mov	a,r4
	rrc	a
;	..\src\COMMON\easyax5043.c:1061: axradio_sync_addtime(dt);
	mov	dpl,a
	mov	dph,r5
	mov	b,r6
	mov	a,r7
	lcall	_axradio_sync_addtime
00103$:
;	..\src\COMMON\easyax5043.c:1063: axradio_sync_periodcorr = signedlimit16(axradio_sync_periodcorr, axradio_sync_slave_maxperiod);
	mov	dptr,#_axradio_sync_periodcorr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#_axradio_sync_slave_maxperiod
	clr	a
	movc	a,@a+dptr
	push	acc
	mov	a,#0x01
	movc	a,@a+dptr
	push	acc
	mov	dpl,r6
	mov	dph,r7
	lcall	_signedlimit16
	mov	r6,dpl
	mov	r7,dph
	dec	sp
	dec	sp
	mov	dptr,#_axradio_sync_periodcorr
	mov	a,r6
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1064: }
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'axradio_sync_slave_nextperiod'
;------------------------------------------------------------
;c                         Allocated to registers r6 r7 
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:1066: static void axradio_sync_slave_nextperiod()
;	-----------------------------------------
;	 function axradio_sync_slave_nextperiod
;	-----------------------------------------
_axradio_sync_slave_nextperiod:
;	..\src\COMMON\easyax5043.c:1068: axradio_sync_addtime(axradio_sync_period);
	mov	dptr,#_axradio_sync_period
	clr	a
	movc	a,@a+dptr
	mov	r4,a
	mov	a,#0x01
	movc	a,@a+dptr
	mov	r5,a
	mov	a,#0x02
	movc	a,@a+dptr
	mov	r6,a
	mov	a,#0x03
	movc	a,@a+dptr
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	lcall	_axradio_sync_addtime
;	..\src\COMMON\easyax5043.c:1069: if (!checksignedlimit16(axradio_sync_periodcorr, axradio_sync_slave_maxperiod))
	mov	dptr,#_axradio_sync_periodcorr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#_axradio_sync_slave_maxperiod
	clr	a
	movc	a,@a+dptr
	push	acc
	mov	a,#0x01
	movc	a,@a+dptr
	push	acc
	mov	dpl,r6
	mov	dph,r7
	lcall	_checksignedlimit16
	mov	r7,dpl
	dec	sp
	dec	sp
	mov	a,r7
	jnz	00102$
;	..\src\COMMON\easyax5043.c:1070: return;
	ret
00102$:
;	..\src\COMMON\easyax5043.c:1072: int16_t __autodata c = axradio_sync_periodcorr;
	mov	dptr,#_axradio_sync_periodcorr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
;	..\src\COMMON\easyax5043.c:1073: axradio_sync_addtime(c >> SYNC_K1);
	swap	a
	rr	a
	xch	a,r6
	swap	a
	rr	a
	anl	a,#0x07
	xrl	a,r6
	xch	a,r6
	anl	a,#0x07
	xch	a,r6
	xrl	a,r6
	xch	a,r6
	jnb	acc.2,00110$
	orl	a,#0xf8
00110$:
	mov	r7,a
	rlc	a
	subb	a,acc
	mov	r5,a
	mov	dpl,r6
	mov	dph,r7
	mov	b,r5
;	..\src\COMMON\easyax5043.c:1075: }
	ljmp	_axradio_sync_addtime
;------------------------------------------------------------
;Allocation info for local variables in function 'axradio_timer_callback'
;------------------------------------------------------------
;r                         Allocated to registers r7 
;idx                       Allocated to registers r7 
;rs                        Allocated to registers r6 
;idx                       Allocated to registers r7 
;desc                      Allocated with name '_axradio_timer_callback_desc_65536_350'
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:1079: static void axradio_timer_callback(struct wtimer_desc __xdata *desc)
;	-----------------------------------------
;	 function axradio_timer_callback
;	-----------------------------------------
_axradio_timer_callback:
;	..\src\COMMON\easyax5043.c:1082: switch (axradio_mode) {
	mov	r7,_axradio_mode
	cjne	r7,#0x10,00295$
00295$:
	jnc	00296$
	ret
00296$:
	mov	a,r7
	add	a,#0xff - 0x33
	jnc	00297$
	ret
00297$:
	mov	a,r7
	add	a,#0xf0
	mov	r7,a
	add	a,#(00298$-3-.)
	movc	a,@a+pc
	mov	dpl,a
	mov	a,r7
	add	a,#(00299$-3-.)
	movc	a,@a+pc
	mov	dph,a
	clr	a
	jmp	@a+dptr
00298$:
	.db	00114$
	.db	00114$
	.db	00124$
	.db	00124$
	.db	00177$
	.db	00177$
	.db	00177$
	.db	00177$
	.db	00177$
	.db	00177$
	.db	00177$
	.db	00177$
	.db	00177$
	.db	00177$
	.db	00177$
	.db	00177$
	.db	00107$
	.db	00107$
	.db	00130$
	.db	00130$
	.db	00177$
	.db	00177$
	.db	00177$
	.db	00177$
	.db	00107$
	.db	00107$
	.db	00107$
	.db	00107$
	.db	00107$
	.db	00177$
	.db	00177$
	.db	00177$
	.db	00140$
	.db	00140$
	.db	00153$
	.db	00153$
00299$:
	.db	00114$>>8
	.db	00114$>>8
	.db	00124$>>8
	.db	00124$>>8
	.db	00177$>>8
	.db	00177$>>8
	.db	00177$>>8
	.db	00177$>>8
	.db	00177$>>8
	.db	00177$>>8
	.db	00177$>>8
	.db	00177$>>8
	.db	00177$>>8
	.db	00177$>>8
	.db	00177$>>8
	.db	00177$>>8
	.db	00107$>>8
	.db	00107$>>8
	.db	00130$>>8
	.db	00130$>>8
	.db	00177$>>8
	.db	00177$>>8
	.db	00177$>>8
	.db	00177$>>8
	.db	00107$>>8
	.db	00107$>>8
	.db	00107$>>8
	.db	00107$>>8
	.db	00107$>>8
	.db	00177$>>8
	.db	00177$>>8
	.db	00177$>>8
	.db	00140$>>8
	.db	00140$>>8
	.db	00153$>>8
	.db	00153$>>8
;	..\src\COMMON\easyax5043.c:1089: case AXRADIO_MODE_WOR_RECEIVE:
00107$:
;	..\src\COMMON\easyax5043.c:1090: if (axradio_syncstate == syncstate_asynctx)
	mov	dptr,#_axradio_syncstate
	movx	a,@dptr
	mov	r7,a
	cjne	r7,#0x02,00300$
	ljmp	00114$
00300$:
;	..\src\COMMON\easyax5043.c:1092: wtimer_remove(&axradio_timer);
	mov	dptr,#_axradio_timer
	lcall	_wtimer_remove
;	..\src\COMMON\easyax5043.c:1093: rearmcstimer:
00110$:
;	..\src\COMMON\easyax5043.c:1094: axradio_timer.time = axradio_phy_cs_period;
	mov	dptr,#_axradio_phy_cs_period
	clr	a
	movc	a,@a+dptr
	mov	r6,a
	mov	a,#0x01
	movc	a,@a+dptr
	mov	r7,a
	mov	r5,#0x00
	mov	r4,#0x00
	mov	dptr,#(_axradio_timer + 0x0004)
	mov	a,r6
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1095: wtimer0_addrelative(&axradio_timer);
	mov	dptr,#_axradio_timer
	lcall	_wtimer0_addrelative
;	..\src\COMMON\easyax5043.c:1096: chanstatecb:
00111$:
;	..\src\COMMON\easyax5043.c:1097: update_timeanchor();
	lcall	_update_timeanchor
;	..\src\COMMON\easyax5043.c:1098: wtimer_remove_callback(&axradio_cb_channelstate.cb);
	mov	dptr,#_axradio_cb_channelstate
	lcall	_wtimer_remove_callback
;	..\src\COMMON\easyax5043.c:1099: axradio_cb_channelstate.st.error = AXRADIO_ERR_NOERROR;
	mov	dptr,#(_axradio_cb_channelstate + 0x0005)
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1101: int8_t __autodata r = AX5043_RSSI;
	mov	dptr,#_AX5043_RSSI
	movx	a,@dptr
;	..\src\COMMON\easyax5043.c:1102: axradio_cb_channelstate.st.cs.rssi = r - (int16_t)axradio_phy_rssioffset;
	mov	r7,a
	mov	r5,a
	rlc	a
	subb	a,acc
	mov	r6,a
	mov	dptr,#_axradio_phy_rssioffset
	clr	a
	movc	a,@a+dptr
	mov	r4,a
	rlc	a
	subb	a,acc
	mov	r3,a
	mov	a,r5
	clr	c
	subb	a,r4
	mov	r5,a
	mov	a,r6
	subb	a,r3
	mov	r6,a
	mov	dptr,#(_axradio_cb_channelstate + 0x000a)
	mov	a,r5
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1103: axradio_cb_channelstate.st.cs.busy = r >= axradio_phy_channelbusy;
	mov	dptr,#_axradio_phy_channelbusy
	clr	a
	movc	a,@a+dptr
	mov	r6,a
	clr	c
	mov	a,r7
	xrl	a,#0x80
	mov	b,r6
	xrl	b,#0x80
	subb	a,b
	cpl	c
	clr	a
	rlc	a
	mov	dptr,#(_axradio_cb_channelstate + 0x000c)
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1105: axradio_cb_channelstate.st.time.t = axradio_timeanchor.radiotimer;
	mov	dptr,#(_axradio_timeanchor + 0x0004)
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#(_axradio_cb_channelstate + 0x0006)
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1106: wtimer_add_callback(&axradio_cb_channelstate.cb);
	mov	dptr,#_axradio_cb_channelstate
;	..\src\COMMON\easyax5043.c:1107: break;
	ljmp	_wtimer_add_callback
;	..\src\COMMON\easyax5043.c:1111: transmitcs:
00114$:
;	..\src\COMMON\easyax5043.c:1112: if (axradio_ack_count)
	mov	dptr,#_axradio_ack_count
	movx	a,@dptr
	mov	r7,a
	movx	a,@dptr
	jz	00116$
;	..\src\COMMON\easyax5043.c:1113: --axradio_ack_count;
	mov	a,r7
	dec	a
	mov	dptr,#_axradio_ack_count
	movx	@dptr,a
00116$:
;	..\src\COMMON\easyax5043.c:1114: wtimer_remove(&axradio_timer);
	mov	dptr,#_axradio_timer
	lcall	_wtimer_remove
;	..\src\COMMON\easyax5043.c:1115: if ((int8_t)AX5043_RSSI < axradio_phy_channelbusy ||
	mov	dptr,#_AX5043_RSSI
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#_axradio_phy_channelbusy
	clr	a
	movc	a,@a+dptr
	mov	r6,a
	clr	c
	mov	a,r7
	xrl	a,#0x80
	mov	b,r6
	xrl	b,#0x80
	subb	a,b
	jc	00117$
;	..\src\COMMON\easyax5043.c:1116: (!axradio_ack_count && axradio_phy_lbt_forcetx)) {
	mov	dptr,#_axradio_ack_count
	movx	a,@dptr
	mov	r7,a
	movx	a,@dptr
	jnz	00118$
	mov	dptr,#_axradio_phy_lbt_forcetx
	clr	a
	movc	a,@a+dptr
	jz	00118$
00117$:
;	..\src\COMMON\easyax5043.c:1117: axradio_syncstate = syncstate_off;
	mov	dptr,#_axradio_syncstate
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1118: axradio_txbuffer_cnt = axradio_phy_preamble_longlen;
	mov	dptr,#_axradio_phy_preamble_longlen
;	genFromRTrack removed	clr	a
	movc	a,@a+dptr
	mov	r5,a
	mov	a,#0x01
	movc	a,@a+dptr
	mov	r6,a
	mov	dptr,#_axradio_txbuffer_cnt
	mov	a,r5
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1119: ax5043_prepare_tx();
	lcall	_ax5043_prepare_tx
;	..\src\COMMON\easyax5043.c:1120: goto chanstatecb;
	ljmp	00111$
00118$:
;	..\src\COMMON\easyax5043.c:1122: if (axradio_ack_count)
	mov	a,r7
	jz	00306$
	ljmp	00110$
00306$:
;	..\src\COMMON\easyax5043.c:1124: update_timeanchor();
	lcall	_update_timeanchor
;	..\src\COMMON\easyax5043.c:1125: axradio_syncstate = syncstate_off;
	mov	dptr,#_axradio_syncstate
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1126: ax5043_off();
	lcall	_ax5043_off
;	..\src\COMMON\easyax5043.c:1127: wtimer_remove_callback(&axradio_cb_transmitstart.cb);
	mov	dptr,#_axradio_cb_transmitstart
	lcall	_wtimer_remove_callback
;	..\src\COMMON\easyax5043.c:1128: axradio_cb_transmitstart.st.error = AXRADIO_ERR_TIMEOUT;
	mov	dptr,#(_axradio_cb_transmitstart + 0x0005)
	mov	a,#0x03
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1129: axradio_cb_transmitstart.st.time.t = axradio_timeanchor.radiotimer;
	mov	dptr,#(_axradio_timeanchor + 0x0004)
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#(_axradio_cb_transmitstart + 0x0006)
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1130: wtimer_add_callback(&axradio_cb_transmitstart.cb);
	mov	dptr,#_axradio_cb_transmitstart
;	..\src\COMMON\easyax5043.c:1131: break;
	ljmp	_wtimer_add_callback
;	..\src\COMMON\easyax5043.c:1134: case AXRADIO_MODE_WOR_ACK_TRANSMIT:
00124$:
;	..\src\COMMON\easyax5043.c:1135: if (axradio_syncstate == syncstate_lbt)
	mov	dptr,#_axradio_syncstate
	movx	a,@dptr
	mov	r7,a
	cjne	r7,#0x01,00307$
	ljmp	00114$
00307$:
;	..\src\COMMON\easyax5043.c:1137: ax5043_off();
	lcall	_ax5043_off
;	..\src\COMMON\easyax5043.c:1138: if (!axradio_ack_count) {
	mov	dptr,#_axradio_ack_count
	movx	a,@dptr
	mov	r7,a
	movx	a,@dptr
	jnz	00128$
;	..\src\COMMON\easyax5043.c:1139: update_timeanchor();
	lcall	_update_timeanchor
;	..\src\COMMON\easyax5043.c:1140: wtimer_remove_callback(&axradio_cb_transmitend.cb);
	mov	dptr,#_axradio_cb_transmitend
	lcall	_wtimer_remove_callback
;	..\src\COMMON\easyax5043.c:1141: axradio_cb_transmitend.st.error = AXRADIO_ERR_TIMEOUT;
	mov	dptr,#(_axradio_cb_transmitend + 0x0005)
	mov	a,#0x03
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1142: axradio_cb_transmitend.st.time.t = axradio_timeanchor.radiotimer;
	mov	dptr,#(_axradio_timeanchor + 0x0004)
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	mov	dptr,#(_axradio_cb_transmitend + 0x0006)
	mov	a,r3
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1143: wtimer_add_callback(&axradio_cb_transmitend.cb);
	mov	dptr,#_axradio_cb_transmitend
;	..\src\COMMON\easyax5043.c:1144: break;
	ljmp	_wtimer_add_callback
00128$:
;	..\src\COMMON\easyax5043.c:1146: --axradio_ack_count;
	mov	a,r7
	dec	a
	mov	dptr,#_axradio_ack_count
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1147: axradio_txbuffer_cnt = axradio_phy_preamble_longlen;
	mov	dptr,#_axradio_phy_preamble_longlen
	clr	a
	movc	a,@a+dptr
	mov	r6,a
	mov	a,#0x01
	movc	a,@a+dptr
	mov	r7,a
	mov	dptr,#_axradio_txbuffer_cnt
	mov	a,r6
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1148: ax5043_prepare_tx();
;	..\src\COMMON\easyax5043.c:1149: break;
	ljmp	_ax5043_prepare_tx
;	..\src\COMMON\easyax5043.c:1152: case AXRADIO_MODE_WOR_ACK_RECEIVE:
00130$:
;	..\src\COMMON\easyax5043.c:1153: if (axradio_syncstate == syncstate_lbt)
	mov	dptr,#_axradio_syncstate
	movx	a,@dptr
	mov	r7,a
	cjne	r7,#0x01,00309$
	ljmp	00114$
00309$:
;	..\src\COMMON\easyax5043.c:1155: transmitack:
00133$:
;	..\src\COMMON\easyax5043.c:1156: AX5043_FIFOSTAT = 3;
	mov	dptr,#_AX5043_FIFOSTAT
	mov	a,#0x03
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1157: AX5043_PWRMODE = AX5043_PWRSTATE_FULL_TX;
	mov	dptr,#_AX5043_PWRMODE
	mov	a,#0x0d
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1158: while (!(AX5043_POWSTAT & 0x08)); // wait for modem vdd so writing the FIFO is safe
00134$:
	mov	dptr,#_AX5043_POWSTAT
	movx	a,@dptr
	jnb	acc.3,00134$
;	..\src\COMMON\easyax5043.c:1159: ax5043_init_registers_tx();
	lcall	_ax5043_init_registers_tx
;	..\src\COMMON\easyax5043.c:1160: AX5043_RADIOEVENTREQ0; // make sure REVRDONE bit is cleared, so it is a reliable indicator that the packet is out
	mov	dptr,#_AX5043_RADIOEVENTREQ0
	movx	a,@dptr
;	..\src\COMMON\easyax5043.c:1161: AX5043_FIFOTHRESH1 = 0;
	mov	dptr,#_AX5043_FIFOTHRESH1
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1162: AX5043_FIFOTHRESH0 = 0x80;
	mov	dptr,#_AX5043_FIFOTHRESH0
	mov	a,#0x80
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1163: axradio_trxstate = trxstate_tx_longpreamble;
	mov	_axradio_trxstate,#0x0a
;	..\src\COMMON\easyax5043.c:1164: axradio_txbuffer_cnt = axradio_phy_preamble_longlen;
	mov	dptr,#_axradio_phy_preamble_longlen
	clr	a
	movc	a,@a+dptr
	mov	r6,a
	mov	a,#0x01
	movc	a,@a+dptr
	mov	r7,a
	mov	dptr,#_axradio_txbuffer_cnt
	mov	a,r6
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1166: if ((AX5043_MODULATION & 0x0F) == 9) { // 4-FSK
	mov	dptr,#_AX5043_MODULATION
	movx	a,@dptr
	mov	r7,a
	anl	ar7,#0x0f
	mov	r6,#0x00
	cjne	r7,#0x09,00138$
	cjne	r6,#0x00,00138$
;	..\src\COMMON\easyax5043.c:1167: AX5043_FIFODATA = AX5043_FIFOCMD_DATA | (7 << 5);
	mov	dptr,#_AX5043_FIFODATA
	mov	a,#0xe1
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1168: AX5043_FIFODATA = 2;  // length (including flags)
	mov	a,#0x02
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1169: AX5043_FIFODATA = 0x01;  // flag PKTSTART -> dibit sync
	dec	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1170: AX5043_FIFODATA = 0x11; // dummy byte for forcing dibit sync
	mov	a,#0x11
	movx	@dptr,a
00138$:
;	..\src\COMMON\easyax5043.c:1177: AX5043_IRQMASK0 = 0x08; // enable fifo free threshold
	mov	dptr,#_AX5043_IRQMASK0
	mov	a,#0x08
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1178: update_timeanchor();
	lcall	_update_timeanchor
;	..\src\COMMON\easyax5043.c:1179: wtimer_remove_callback(&axradio_cb_transmitstart.cb);
	mov	dptr,#_axradio_cb_transmitstart
	lcall	_wtimer_remove_callback
;	..\src\COMMON\easyax5043.c:1180: axradio_cb_transmitstart.st.error = AXRADIO_ERR_NOERROR;
	mov	dptr,#(_axradio_cb_transmitstart + 0x0005)
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1181: axradio_cb_transmitstart.st.time.t = axradio_timeanchor.radiotimer;
	mov	dptr,#(_axradio_timeanchor + 0x0004)
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#(_axradio_cb_transmitstart + 0x0006)
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1182: wtimer_add_callback(&axradio_cb_transmitstart.cb);
	mov	dptr,#_axradio_cb_transmitstart
;	..\src\COMMON\easyax5043.c:1183: break;
	ljmp	_wtimer_add_callback
;	..\src\COMMON\easyax5043.c:1186: case AXRADIO_MODE_SYNC_ACK_MASTER:
00140$:
;	..\src\COMMON\easyax5043.c:1187: switch (axradio_syncstate) {
	mov	dptr,#_axradio_syncstate
	movx	a,@dptr
	mov	r7,a
	cjne	r7,#0x04,00313$
	sjmp	00142$
00313$:
	cjne	r7,#0x05,00314$
	ljmp	00150$
00314$:
;	..\src\COMMON\easyax5043.c:1189: AX5043_PWRMODE = AX5043_PWRSTATE_XTAL_ON;
	mov	dptr,#_AX5043_PWRMODE
	mov	a,#0x05
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1190: ax5043_init_registers_tx();
	lcall	_ax5043_init_registers_tx
;	..\src\COMMON\easyax5043.c:1191: axradio_syncstate = syncstate_master_xostartup;
	mov	dptr,#_axradio_syncstate
	mov	a,#0x04
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1192: wtimer_remove_callback(&axradio_cb_transmitdata.cb);
	mov	dptr,#_axradio_cb_transmitdata
	lcall	_wtimer_remove_callback
;	..\src\COMMON\easyax5043.c:1193: axradio_cb_transmitdata.st.error = AXRADIO_ERR_NOERROR;
	mov	dptr,#(_axradio_cb_transmitdata + 0x0005)
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1194: axradio_cb_transmitdata.st.time.t = 0;
	mov	dptr,#(_axradio_cb_transmitdata + 0x0006)
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1195: wtimer_add_callback(&axradio_cb_transmitdata.cb);
	mov	dptr,#_axradio_cb_transmitdata
	lcall	_wtimer_add_callback
;	..\src\COMMON\easyax5043.c:1196: wtimer_remove(&axradio_timer);
	mov	dptr,#_axradio_timer
	lcall	_wtimer_remove
;	..\src\COMMON\easyax5043.c:1197: axradio_timer.time = axradio_sync_time;
	mov	dptr,#_axradio_sync_time
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#(_axradio_timer + 0x0004)
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1198: wtimer0_addabsolute(&axradio_timer);
	mov	dptr,#_axradio_timer
;	..\src\COMMON\easyax5043.c:1199: break;
	ljmp	_wtimer0_addabsolute
;	..\src\COMMON\easyax5043.c:1201: case syncstate_master_xostartup:
00142$:
;	..\src\COMMON\easyax5043.c:1202: AX5043_FIFOSTAT = 3;
	mov	dptr,#_AX5043_FIFOSTAT
	mov	a,#0x03
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1203: AX5043_PWRMODE = AX5043_PWRSTATE_FULL_TX;
	mov	dptr,#_AX5043_PWRMODE
	mov	a,#0x0d
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1204: while (!(AX5043_POWSTAT & 0x08)); // wait for modem vdd so writing the FIFO is safe
00143$:
	mov	dptr,#_AX5043_POWSTAT
	movx	a,@dptr
	jnb	acc.3,00143$
;	..\src\COMMON\easyax5043.c:1205: AX5043_RADIOEVENTREQ0; // make sure REVRDONE bit is cleared, so it is a reliable indicator that the packet is out
	mov	dptr,#_AX5043_RADIOEVENTREQ0
	movx	a,@dptr
;	..\src\COMMON\easyax5043.c:1206: AX5043_FIFOTHRESH1 = 0;
	mov	dptr,#_AX5043_FIFOTHRESH1
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1207: AX5043_FIFOTHRESH0 = 0x80;
	mov	dptr,#_AX5043_FIFOTHRESH0
	mov	a,#0x80
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1208: axradio_trxstate = trxstate_tx_longpreamble;
	mov	_axradio_trxstate,#0x0a
;	..\src\COMMON\easyax5043.c:1209: axradio_txbuffer_cnt = axradio_phy_preamble_longlen;
	mov	dptr,#_axradio_phy_preamble_longlen
	clr	a
	movc	a,@a+dptr
	mov	r6,a
	mov	a,#0x01
	movc	a,@a+dptr
	mov	r7,a
	mov	dptr,#_axradio_txbuffer_cnt
	mov	a,r6
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1211: if ((AX5043_MODULATION & 0x0F) == 9) { // 4-FSK
	mov	dptr,#_AX5043_MODULATION
	movx	a,@dptr
	mov	r7,a
	anl	ar7,#0x0f
	mov	r6,#0x00
	cjne	r7,#0x09,00147$
	cjne	r6,#0x00,00147$
;	..\src\COMMON\easyax5043.c:1212: AX5043_FIFODATA = AX5043_FIFOCMD_DATA | (7 << 5);
	mov	dptr,#_AX5043_FIFODATA
	mov	a,#0xe1
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1213: AX5043_FIFODATA = 2;  // length (including flags)
	mov	a,#0x02
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1214: AX5043_FIFODATA = 0x01;  // flag PKTSTART -> dibit sync
	dec	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1215: AX5043_FIFODATA = 0x11; // dummy byte for forcing dibit sync
	mov	a,#0x11
	movx	@dptr,a
00147$:
;	..\src\COMMON\easyax5043.c:1222: wtimer_remove(&axradio_timer);
	mov	dptr,#_axradio_timer
	lcall	_wtimer_remove
;	..\src\COMMON\easyax5043.c:1223: update_timeanchor();
	lcall	_update_timeanchor
;	..\src\COMMON\easyax5043.c:1224: AX5043_IRQMASK0 = 0x08; // enable fifo free threshold
	mov	dptr,#_AX5043_IRQMASK0
	mov	a,#0x08
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1225: axradio_sync_addtime(axradio_sync_period);
	mov	dptr,#_axradio_sync_period
	clr	a
	movc	a,@a+dptr
	mov	r4,a
	mov	a,#0x01
	movc	a,@a+dptr
	mov	r5,a
	mov	a,#0x02
	movc	a,@a+dptr
	mov	r6,a
	mov	a,#0x03
	movc	a,@a+dptr
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	lcall	_axradio_sync_addtime
;	..\src\COMMON\easyax5043.c:1226: axradio_syncstate = syncstate_master_waitack;
	mov	dptr,#_axradio_syncstate
	mov	a,#0x05
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1227: if (axradio_mode != AXRADIO_MODE_SYNC_ACK_MASTER) {
	mov	a,#0x31
	cjne	a,_axradio_mode,00318$
	sjmp	00149$
00318$:
;	..\src\COMMON\easyax5043.c:1228: axradio_syncstate = syncstate_master_normal;
	mov	dptr,#_axradio_syncstate
	mov	a,#0x03
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1229: axradio_sync_settimeradv(axradio_sync_xoscstartup);
	mov	dptr,#_axradio_sync_xoscstartup
	clr	a
	movc	a,@a+dptr
	mov	r4,a
	mov	a,#0x01
	movc	a,@a+dptr
	mov	r5,a
	mov	a,#0x02
	movc	a,@a+dptr
	mov	r6,a
	mov	a,#0x03
	movc	a,@a+dptr
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	lcall	_axradio_sync_settimeradv
;	..\src\COMMON\easyax5043.c:1230: wtimer0_addabsolute(&axradio_timer);
	mov	dptr,#_axradio_timer
	lcall	_wtimer0_addabsolute
00149$:
;	..\src\COMMON\easyax5043.c:1232: wtimer_remove_callback(&axradio_cb_transmitstart.cb);
	mov	dptr,#_axradio_cb_transmitstart
	lcall	_wtimer_remove_callback
;	..\src\COMMON\easyax5043.c:1233: axradio_cb_transmitstart.st.error = AXRADIO_ERR_NOERROR;
	mov	dptr,#(_axradio_cb_transmitstart + 0x0005)
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1234: axradio_cb_transmitstart.st.time.t = axradio_timeanchor.radiotimer;
	mov	dptr,#(_axradio_timeanchor + 0x0004)
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#(_axradio_cb_transmitstart + 0x0006)
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1235: wtimer_add_callback(&axradio_cb_transmitstart.cb);
	mov	dptr,#_axradio_cb_transmitstart
;	..\src\COMMON\easyax5043.c:1236: break;
	ljmp	_wtimer_add_callback
;	..\src\COMMON\easyax5043.c:1238: case syncstate_master_waitack:
00150$:
;	..\src\COMMON\easyax5043.c:1239: ax5043_off();
	lcall	_ax5043_off
;	..\src\COMMON\easyax5043.c:1240: axradio_syncstate = syncstate_master_normal;
	mov	dptr,#_axradio_syncstate
	mov	a,#0x03
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1241: wtimer_remove(&axradio_timer);
	mov	dptr,#_axradio_timer
	lcall	_wtimer_remove
;	..\src\COMMON\easyax5043.c:1242: axradio_sync_settimeradv(axradio_sync_xoscstartup);
	mov	dptr,#_axradio_sync_xoscstartup
	clr	a
	movc	a,@a+dptr
	mov	r4,a
	mov	a,#0x01
	movc	a,@a+dptr
	mov	r5,a
	mov	a,#0x02
	movc	a,@a+dptr
	mov	r6,a
	mov	a,#0x03
	movc	a,@a+dptr
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	lcall	_axradio_sync_settimeradv
;	..\src\COMMON\easyax5043.c:1243: wtimer0_addabsolute(&axradio_timer);
	mov	dptr,#_axradio_timer
	lcall	_wtimer0_addabsolute
;	..\src\COMMON\easyax5043.c:1244: update_timeanchor();
	lcall	_update_timeanchor
;	..\src\COMMON\easyax5043.c:1245: wtimer_remove_callback(&axradio_cb_transmitend.cb);
	mov	dptr,#_axradio_cb_transmitend
	lcall	_wtimer_remove_callback
;	..\src\COMMON\easyax5043.c:1246: axradio_cb_transmitend.st.error = AXRADIO_ERR_TIMEOUT;
	mov	dptr,#(_axradio_cb_transmitend + 0x0005)
	mov	a,#0x03
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1247: axradio_cb_transmitend.st.time.t = axradio_timeanchor.radiotimer;
	mov	dptr,#(_axradio_timeanchor + 0x0004)
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#(_axradio_cb_transmitend + 0x0006)
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1248: wtimer_add_callback(&axradio_cb_transmitend.cb);
	mov	dptr,#_axradio_cb_transmitend
;	..\src\COMMON\easyax5043.c:1251: break;
	ljmp	_wtimer_add_callback
;	..\src\COMMON\easyax5043.c:1254: case AXRADIO_MODE_SYNC_ACK_SLAVE:
00153$:
;	..\src\COMMON\easyax5043.c:1255: switch (axradio_syncstate) {
	mov	dptr,#_axradio_syncstate
	movx	a,@dptr
	mov  r7,a
	add	a,#0xff - 0x0c
	jnc	00319$
	ljmp	00155$
00319$:
	mov	a,r7
	mov	b,a
	add	a,#(00320$-3-.)
	movc	a,@a+pc
	mov	dpl,a
	mov	a,b
	add	a,#(00321$-3-.)
	movc	a,@a+pc
	mov	dph,a
	clr	a
	jmp	@a+dptr
00320$:
	.db	00155$
	.db	00155$
	.db	00155$
	.db	00155$
	.db	00155$
	.db	00155$
	.db	00155$
	.db	00156$
	.db	00157$
	.db	00158$
	.db	00161$
	.db	00166$
	.db	00173$
00321$:
	.db	00155$>>8
	.db	00155$>>8
	.db	00155$>>8
	.db	00155$>>8
	.db	00155$>>8
	.db	00155$>>8
	.db	00155$>>8
	.db	00156$>>8
	.db	00157$>>8
	.db	00158$>>8
	.db	00161$>>8
	.db	00166$>>8
	.db	00173$>>8
;	..\src\COMMON\easyax5043.c:1257: case syncstate_slave_synchunt:
00155$:
;	..\src\COMMON\easyax5043.c:1258: ax5043_off();
	lcall	_ax5043_off
;	..\src\COMMON\easyax5043.c:1259: axradio_syncstate = syncstate_slave_syncpause;
	mov	dptr,#_axradio_syncstate
	mov	a,#0x07
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1260: axradio_sync_addtime(axradio_sync_slave_syncpause);
	mov	dptr,#_axradio_sync_slave_syncpause
	clr	a
	movc	a,@a+dptr
	mov	r4,a
	mov	a,#0x01
	movc	a,@a+dptr
	mov	r5,a
	mov	a,#0x02
	movc	a,@a+dptr
	mov	r6,a
	mov	a,#0x03
	movc	a,@a+dptr
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	lcall	_axradio_sync_addtime
;	..\src\COMMON\easyax5043.c:1261: wtimer_remove(&axradio_timer);
	mov	dptr,#_axradio_timer
	lcall	_wtimer_remove
;	..\src\COMMON\easyax5043.c:1262: axradio_timer.time = axradio_sync_time;
	mov	dptr,#_axradio_sync_time
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#(_axradio_timer + 0x0004)
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1263: wtimer0_addabsolute(&axradio_timer);
	mov	dptr,#_axradio_timer
	lcall	_wtimer0_addabsolute
;	..\src\COMMON\easyax5043.c:1264: wtimer_remove_callback(&axradio_cb_receive.cb);
	mov	dptr,#_axradio_cb_receive
	lcall	_wtimer_remove_callback
;	..\src\COMMON\easyax5043.c:1265: memset_xdata(&axradio_cb_receive.st, 0, sizeof(axradio_cb_receive.st));
	mov	dptr,#_memset_PARM_2
	clr	a
	movx	@dptr,a
	mov	dptr,#_memset_PARM_3
	mov	a,#0x1e
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
	mov	dptr,#(_axradio_cb_receive + 0x0004)
	mov	b,#0x00
	lcall	_memset
;	..\src\COMMON\easyax5043.c:1266: axradio_cb_receive.st.time.t = axradio_timeanchor.radiotimer;
	mov	dptr,#(_axradio_timeanchor + 0x0004)
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#(_axradio_cb_receive + 0x0006)
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1267: axradio_cb_receive.st.error = AXRADIO_ERR_RESYNCTIMEOUT;
	mov	dptr,#(_axradio_cb_receive + 0x0005)
	mov	a,#0x0a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1268: wtimer_add_callback(&axradio_cb_receive.cb);
	mov	dptr,#_axradio_cb_receive
;	..\src\COMMON\easyax5043.c:1269: break;
	ljmp	_wtimer_add_callback
;	..\src\COMMON\easyax5043.c:1271: case syncstate_slave_syncpause:
00156$:
;	..\src\COMMON\easyax5043.c:1272: ax5043_receiver_on_continuous();
	lcall	_ax5043_receiver_on_continuous
;	..\src\COMMON\easyax5043.c:1273: axradio_syncstate = syncstate_slave_synchunt;
	mov	dptr,#_axradio_syncstate
	mov	a,#0x06
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1274: axradio_sync_addtime(axradio_sync_slave_syncwindow);
	mov	dptr,#_axradio_sync_slave_syncwindow
	clr	a
	movc	a,@a+dptr
	mov	r4,a
	mov	a,#0x01
	movc	a,@a+dptr
	mov	r5,a
	mov	a,#0x02
	movc	a,@a+dptr
	mov	r6,a
	mov	a,#0x03
	movc	a,@a+dptr
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	lcall	_axradio_sync_addtime
;	..\src\COMMON\easyax5043.c:1275: wtimer_remove(&axradio_timer);
	mov	dptr,#_axradio_timer
	lcall	_wtimer_remove
;	..\src\COMMON\easyax5043.c:1276: axradio_timer.time = axradio_sync_time;
	mov	dptr,#_axradio_sync_time
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#(_axradio_timer + 0x0004)
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1277: wtimer0_addabsolute(&axradio_timer);
	mov	dptr,#_axradio_timer
	lcall	_wtimer0_addabsolute
;	..\src\COMMON\easyax5043.c:1278: update_timeanchor();
	lcall	_update_timeanchor
;	..\src\COMMON\easyax5043.c:1279: wtimer_remove_callback(&axradio_cb_receive.cb);
	mov	dptr,#_axradio_cb_receive
	lcall	_wtimer_remove_callback
;	..\src\COMMON\easyax5043.c:1280: memset_xdata(&axradio_cb_receive.st, 0, sizeof(axradio_cb_receive.st));
	mov	dptr,#_memset_PARM_2
	clr	a
	movx	@dptr,a
	mov	dptr,#_memset_PARM_3
	mov	a,#0x1e
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
	mov	dptr,#(_axradio_cb_receive + 0x0004)
	mov	b,#0x00
	lcall	_memset
;	..\src\COMMON\easyax5043.c:1281: axradio_cb_receive.st.time.t = axradio_timeanchor.radiotimer;
	mov	dptr,#(_axradio_timeanchor + 0x0004)
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#(_axradio_cb_receive + 0x0006)
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1282: axradio_cb_receive.st.error = AXRADIO_ERR_RESYNC;
	mov	dptr,#(_axradio_cb_receive + 0x0005)
	mov	a,#0x09
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1283: wtimer_add_callback(&axradio_cb_receive.cb);
	mov	dptr,#_axradio_cb_receive
;	..\src\COMMON\easyax5043.c:1284: break;
	ljmp	_wtimer_add_callback
;	..\src\COMMON\easyax5043.c:1286: case syncstate_slave_rxidle:
00157$:
;	..\src\COMMON\easyax5043.c:1287: AX5043_PWRMODE = AX5043_PWRSTATE_XTAL_ON;
	mov	dptr,#_AX5043_PWRMODE
	mov	a,#0x05
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1288: axradio_syncstate = syncstate_slave_rxxosc;
	mov	dptr,#_axradio_syncstate
	mov	a,#0x09
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1289: wtimer_remove(&axradio_timer);
	mov	dptr,#_axradio_timer
	lcall	_wtimer_remove
;	..\src\COMMON\easyax5043.c:1290: axradio_timer.time += axradio_sync_xoscstartup;
	mov	dptr,#(_axradio_timer + 0x0004)
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#_axradio_sync_xoscstartup
	clr	a
	movc	a,@a+dptr
	mov	r0,a
	mov	a,#0x01
	movc	a,@a+dptr
	mov	r1,a
	mov	a,#0x02
	movc	a,@a+dptr
	mov	r2,a
	mov	a,#0x03
	movc	a,@a+dptr
	mov	r3,a
	mov	a,r0
	add	a,r4
	mov	r4,a
	mov	a,r1
	addc	a,r5
	mov	r5,a
	mov	a,r2
	addc	a,r6
	mov	r6,a
	mov	a,r3
	addc	a,r7
	mov	r7,a
	mov	dptr,#(_axradio_timer + 0x0004)
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1291: wtimer0_addabsolute(&axradio_timer);
	mov	dptr,#_axradio_timer
;	..\src\COMMON\easyax5043.c:1292: break;
	ljmp	_wtimer0_addabsolute
;	..\src\COMMON\easyax5043.c:1294: case syncstate_slave_rxxosc:
00158$:
;	..\src\COMMON\easyax5043.c:1295: ax5043_receiver_on_continuous();
	lcall	_ax5043_receiver_on_continuous
;	..\src\COMMON\easyax5043.c:1296: axradio_syncstate = syncstate_slave_rxsfdwindow;
	mov	dptr,#_axradio_syncstate
	mov	a,#0x0a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1297: update_timeanchor();
	lcall	_update_timeanchor
;	..\src\COMMON\easyax5043.c:1298: wtimer_remove_callback(&axradio_cb_receive.cb);
	mov	dptr,#_axradio_cb_receive
	lcall	_wtimer_remove_callback
;	..\src\COMMON\easyax5043.c:1299: memset_xdata(&axradio_cb_receive.st, 0, sizeof(axradio_cb_receive.st));
	mov	dptr,#_memset_PARM_2
	clr	a
	movx	@dptr,a
	mov	dptr,#_memset_PARM_3
	mov	a,#0x1e
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
	mov	dptr,#(_axradio_cb_receive + 0x0004)
	mov	b,#0x00
	lcall	_memset
;	..\src\COMMON\easyax5043.c:1300: axradio_cb_receive.st.time.t = axradio_timeanchor.radiotimer;
	mov	dptr,#(_axradio_timeanchor + 0x0004)
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#(_axradio_cb_receive + 0x0006)
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1301: axradio_cb_receive.st.error = AXRADIO_ERR_RECEIVESTART;
	mov	dptr,#(_axradio_cb_receive + 0x0005)
	mov	a,#0x0b
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1302: wtimer_add_callback(&axradio_cb_receive.cb);
	mov	dptr,#_axradio_cb_receive
	lcall	_wtimer_add_callback
;	..\src\COMMON\easyax5043.c:1303: wtimer_remove(&axradio_timer);
	mov	dptr,#_axradio_timer
	lcall	_wtimer_remove
;	..\src\COMMON\easyax5043.c:1305: uint8_t __autodata idx = axradio_sync_seqnr;
	mov	dptr,#_axradio_ack_seqnr
	movx	a,@dptr
	mov	r7,a
;	..\src\COMMON\easyax5043.c:1306: if (idx >= axradio_sync_slave_nrrx)
	mov	dptr,#_axradio_sync_slave_nrrx
	clr	a
	movc	a,@a+dptr
	mov	r6,a
	clr	c
	mov	a,r7
	subb	a,r6
	jc	00160$
;	..\src\COMMON\easyax5043.c:1307: idx = axradio_sync_slave_nrrx - 1;
	mov	a,r6
	dec	a
	mov	r7,a
00160$:
;	..\src\COMMON\easyax5043.c:1308: axradio_timer.time += axradio_sync_slave_rxwindow[idx];
	mov	dptr,#(_axradio_timer + 0x0004)
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	mov	a,r7
	mov	b,#0x04
	mul	ab
	add	a,#_axradio_sync_slave_rxwindow
	mov	dpl,a
	mov	a,#(_axradio_sync_slave_rxwindow >> 8)
	addc	a,b
	mov	dph,a
	clr	a
	movc	a,@a+dptr
	mov	r0,a
	inc	dptr
	clr	a
	movc	a,@a+dptr
	mov	r1,a
	inc	dptr
	clr	a
	movc	a,@a+dptr
	mov	r2,a
	inc	dptr
	clr	a
	movc	a,@a+dptr
	mov	r7,a
	mov	a,r0
	add	a,r3
	mov	r3,a
	mov	a,r1
	addc	a,r4
	mov	r4,a
	mov	a,r2
	addc	a,r5
	mov	r5,a
	mov	a,r7
	addc	a,r6
	mov	r6,a
	mov	dptr,#(_axradio_timer + 0x0004)
	mov	a,r3
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1310: wtimer0_addabsolute(&axradio_timer);
	mov	dptr,#_axradio_timer
;	..\src\COMMON\easyax5043.c:1311: break;
	ljmp	_wtimer0_addabsolute
;	..\src\COMMON\easyax5043.c:1313: case syncstate_slave_rxsfdwindow:
00161$:
;	..\src\COMMON\easyax5043.c:1315: uint8_t __autodata rs = AX5043_RADIOSTATE;
	mov	dptr,#_AX5043_RADIOSTATE
	movx	a,@dptr
;	..\src\COMMON\easyax5043.c:1316: if( !rs )
	mov	r7,a
	mov	r6,a
	jnz	00323$
	ret
00323$:
;	..\src\COMMON\easyax5043.c:1319: if (!(0x0F & (uint8_t)~rs)) {
	mov	a,r6
	cpl	a
	mov	r6,a
	anl	a,#0x0f
	jz	00325$
	sjmp	00166$
00325$:
;	..\src\COMMON\easyax5043.c:1320: axradio_syncstate = syncstate_slave_rxpacket;
	mov	dptr,#_axradio_syncstate
	mov	a,#0x0b
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1321: wtimer_remove(&axradio_timer);
	mov	dptr,#_axradio_timer
	lcall	_wtimer_remove
;	..\src\COMMON\easyax5043.c:1322: axradio_timer.time += axradio_sync_slave_rxtimeout;
	mov	dptr,#(_axradio_timer + 0x0004)
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#_axradio_sync_slave_rxtimeout
	clr	a
	movc	a,@a+dptr
	mov	r0,a
	mov	a,#0x01
	movc	a,@a+dptr
	mov	r1,a
	mov	a,#0x02
	movc	a,@a+dptr
	mov	r2,a
	mov	a,#0x03
	movc	a,@a+dptr
	mov	r3,a
	mov	a,r0
	add	a,r4
	mov	r4,a
	mov	a,r1
	addc	a,r5
	mov	r5,a
	mov	a,r2
	addc	a,r6
	mov	r6,a
	mov	a,r3
	addc	a,r7
	mov	r7,a
	mov	dptr,#(_axradio_timer + 0x0004)
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1323: wtimer0_addabsolute(&axradio_timer);
	mov	dptr,#_axradio_timer
;	..\src\COMMON\easyax5043.c:1324: break;
	ljmp	_wtimer0_addabsolute
;	..\src\COMMON\easyax5043.c:1329: case syncstate_slave_rxpacket:
00166$:
;	..\src\COMMON\easyax5043.c:1330: ax5043_off();
	lcall	_ax5043_off
;	..\src\COMMON\easyax5043.c:1331: if (!axradio_sync_seqnr)
	mov	dptr,#_axradio_ack_seqnr
	movx	a,@dptr
	jnz	00168$
;	..\src\COMMON\easyax5043.c:1332: axradio_sync_seqnr = 1;
	mov	dptr,#_axradio_ack_seqnr
	mov	a,#0x01
	movx	@dptr,a
00168$:
;	..\src\COMMON\easyax5043.c:1333: ++axradio_sync_seqnr;
	mov	dptr,#_axradio_ack_seqnr
	movx	a,@dptr
	add	a,#0x01
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1334: update_timeanchor();
	lcall	_update_timeanchor
;	..\src\COMMON\easyax5043.c:1335: wtimer_remove_callback(&axradio_cb_receive.cb);
	mov	dptr,#_axradio_cb_receive
	lcall	_wtimer_remove_callback
;	..\src\COMMON\easyax5043.c:1336: memset_xdata(&axradio_cb_receive.st, 0, sizeof(axradio_cb_receive.st));
	mov	dptr,#_memset_PARM_2
	clr	a
	movx	@dptr,a
	mov	dptr,#_memset_PARM_3
	mov	a,#0x1e
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
	mov	dptr,#(_axradio_cb_receive + 0x0004)
	mov	b,#0x00
	lcall	_memset
;	..\src\COMMON\easyax5043.c:1337: axradio_cb_receive.st.time.t = axradio_timeanchor.radiotimer;
	mov	dptr,#(_axradio_timeanchor + 0x0004)
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#(_axradio_cb_receive + 0x0006)
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1338: axradio_cb_receive.st.error = AXRADIO_ERR_TIMEOUT;
	mov	dptr,#(_axradio_cb_receive + 0x0005)
	mov	a,#0x03
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1339: if (axradio_sync_seqnr <= axradio_sync_slave_resyncloss) {
	mov	dptr,#_axradio_ack_seqnr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#_axradio_sync_slave_resyncloss
	clr	a
	movc	a,@a+dptr
	clr	c
	subb	a,r7
	jc	00172$
;	..\src\COMMON\easyax5043.c:1340: wtimer_add_callback(&axradio_cb_receive.cb);
	mov	dptr,#_axradio_cb_receive
	lcall	_wtimer_add_callback
;	..\src\COMMON\easyax5043.c:1341: axradio_sync_slave_nextperiod();
	lcall	_axradio_sync_slave_nextperiod
;	..\src\COMMON\easyax5043.c:1342: axradio_syncstate = syncstate_slave_rxidle;
	mov	dptr,#_axradio_syncstate
	mov	a,#0x08
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1343: wtimer_remove(&axradio_timer);
	mov	dptr,#_axradio_timer
	lcall	_wtimer_remove
;	..\src\COMMON\easyax5043.c:1345: uint8_t __autodata idx = axradio_sync_seqnr;
	mov	dptr,#_axradio_ack_seqnr
	movx	a,@dptr
	mov	r7,a
;	..\src\COMMON\easyax5043.c:1346: if (idx >= axradio_sync_slave_nrrx)
	mov	dptr,#_axradio_sync_slave_nrrx
	clr	a
	movc	a,@a+dptr
	mov	r6,a
	clr	c
	mov	a,r7
	subb	a,r6
	jc	00170$
;	..\src\COMMON\easyax5043.c:1347: idx = axradio_sync_slave_nrrx - 1;
	mov	a,r6
	dec	a
	mov	r7,a
00170$:
;	..\src\COMMON\easyax5043.c:1348: axradio_sync_settimeradv(axradio_sync_slave_rxadvance[idx]);
	mov	a,r7
	mov	b,#0x04
	mul	ab
	add	a,#_axradio_sync_slave_rxadvance
	mov	dpl,a
	mov	a,#(_axradio_sync_slave_rxadvance >> 8)
	addc	a,b
	mov	dph,a
	clr	a
	movc	a,@a+dptr
	mov	r4,a
	inc	dptr
	clr	a
	movc	a,@a+dptr
	mov	r5,a
	inc	dptr
	clr	a
	movc	a,@a+dptr
	mov	r6,a
	inc	dptr
	clr	a
	movc	a,@a+dptr
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	lcall	_axradio_sync_settimeradv
;	..\src\COMMON\easyax5043.c:1350: wtimer0_addabsolute(&axradio_timer);
	mov	dptr,#_axradio_timer
;	..\src\COMMON\easyax5043.c:1351: break;
	ljmp	_wtimer0_addabsolute
00172$:
;	..\src\COMMON\easyax5043.c:1353: axradio_cb_receive.st.error = AXRADIO_ERR_RESYNC;
	mov	dptr,#(_axradio_cb_receive + 0x0005)
	mov	a,#0x09
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1354: wtimer_add_callback(&axradio_cb_receive.cb);
	mov	dptr,#_axradio_cb_receive
	lcall	_wtimer_add_callback
;	..\src\COMMON\easyax5043.c:1355: ax5043_receiver_on_continuous();
	lcall	_ax5043_receiver_on_continuous
;	..\src\COMMON\easyax5043.c:1356: axradio_syncstate = syncstate_slave_synchunt;
	mov	dptr,#_axradio_syncstate
	mov	a,#0x06
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1357: wtimer_remove(&axradio_timer);
	mov	dptr,#_axradio_timer
	lcall	_wtimer_remove
;	..\src\COMMON\easyax5043.c:1358: axradio_timer.time = axradio_sync_slave_syncwindow;
	mov	dptr,#_axradio_sync_slave_syncwindow
	clr	a
	movc	a,@a+dptr
	mov	r4,a
	mov	a,#0x01
	movc	a,@a+dptr
	mov	r5,a
	mov	a,#0x02
	movc	a,@a+dptr
	mov	r6,a
	mov	a,#0x03
	movc	a,@a+dptr
	mov	r7,a
	mov	dptr,#(_axradio_timer + 0x0004)
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1359: wtimer0_addrelative(&axradio_timer);
	mov	dptr,#_axradio_timer
	lcall	_wtimer0_addrelative
;	..\src\COMMON\easyax5043.c:1360: axradio_sync_time = axradio_timer.time;
	mov	dptr,#(_axradio_timer + 0x0004)
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#_axradio_sync_time
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1361: break;
;	..\src\COMMON\easyax5043.c:1363: case syncstate_slave_rxack:
	ret
00173$:
;	..\src\COMMON\easyax5043.c:1364: axradio_syncstate = syncstate_slave_rxidle;
	mov	dptr,#_axradio_syncstate
	mov	a,#0x08
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1365: wtimer_remove(&axradio_timer);
	mov	dptr,#_axradio_timer
	lcall	_wtimer_remove
;	..\src\COMMON\easyax5043.c:1366: axradio_sync_settimeradv(axradio_sync_slave_rxadvance[1]);
	mov	dptr,#(_axradio_sync_slave_rxadvance + 0x0004)
	clr	a
	movc	a,@a+dptr
	mov	r4,a
	inc	dptr
	clr	a
	movc	a,@a+dptr
	mov	r5,a
	inc	dptr
	clr	a
	movc	a,@a+dptr
	mov	r6,a
	inc	dptr
	clr	a
	movc	a,@a+dptr
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	lcall	_axradio_sync_settimeradv
;	..\src\COMMON\easyax5043.c:1367: wtimer0_addabsolute(&axradio_timer);
	mov	dptr,#_axradio_timer
	lcall	_wtimer0_addabsolute
;	..\src\COMMON\easyax5043.c:1368: goto transmitack;
	ljmp	00133$
;	..\src\COMMON\easyax5043.c:1374: }
00177$:
;	..\src\COMMON\easyax5043.c:1375: }
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'axradio_callback_fwd'
;------------------------------------------------------------
;desc                      Allocated to registers r6 r7 
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:1377: static __reentrantb void axradio_callback_fwd(struct wtimer_callback __xdata *desc) __reentrant
;	-----------------------------------------
;	 function axradio_callback_fwd
;	-----------------------------------------
_axradio_callback_fwd:
	mov	r6,dpl
	mov	r7,dph
;	..\src\COMMON\easyax5043.c:1379: axradio_statuschange((struct axradio_status __xdata *)(desc + 1));
	mov	a,#0x04
	add	a,r6
	mov	r6,a
	clr	a
	addc	a,r7
	mov	r7,a
	mov	dpl,r6
	mov	dph,r7
;	..\src\COMMON\easyax5043.c:1380: }
	ljmp	_axradio_statuschange
;------------------------------------------------------------
;Allocation info for local variables in function 'axradio_receive_callback_fwd'
;------------------------------------------------------------
;len                       Allocated to registers r5 r4 
;len                       Allocated to registers r6 r7 
;trxst                     Allocated to registers r6 
;iesave                    Allocated to registers r7 
;desc                      Allocated with name '_axradio_receive_callback_fwd_desc_65536_368'
;seqnr                     Allocated with name '_axradio_receive_callback_fwd_seqnr_196608_376'
;len_byte                  Allocated with name '_axradio_receive_callback_fwd_len_byte_196608_378'
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:1382: static void axradio_receive_callback_fwd(struct wtimer_callback __xdata *desc)
;	-----------------------------------------
;	 function axradio_receive_callback_fwd
;	-----------------------------------------
_axradio_receive_callback_fwd:
;	..\src\COMMON\easyax5043.c:1386: if (axradio_cb_receive.st.error != AXRADIO_ERR_NOERROR) {
	mov	dptr,#(_axradio_cb_receive + 0x0005)
	movx	a,@dptr
	jz	00102$
;	..\src\COMMON\easyax5043.c:1387: axradio_statuschange((struct axradio_status __xdata *)&axradio_cb_receive.st);
	mov	dptr,#(_axradio_cb_receive + 0x0004)
;	..\src\COMMON\easyax5043.c:1388: return;
	ljmp	_axradio_statuschange
00102$:
;	..\src\COMMON\easyax5043.c:1390: if (axradio_phy_pn9 && !AXRADIO_MODE_IS_STREAM_RECEIVE(axradio_mode)) {
	mov	dptr,#_axradio_phy_pn9
	clr	a
	movc	a,@a+dptr
	jz	00104$
	mov	r6,_axradio_mode
	anl	ar6,#0xf8
	mov	r7,#0x00
	cjne	r6,#0x28,00350$
	cjne	r7,#0x00,00350$
	sjmp	00104$
00350$:
;	..\src\COMMON\easyax5043.c:1391: uint16_t __autodata len = axradio_cb_receive.st.rx.pktlen;
	mov	dptr,#(_axradio_cb_receive + 0x0020)
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
;	..\src\COMMON\easyax5043.c:1392: len += axradio_framing_maclen;
	mov	dptr,#_axradio_framing_maclen
	clr	a
	movc	a,@a+dptr
	mov	r4,#0x00
	add	a,r6
	mov	r5,a
	mov	a,r4
	addc	a,r7
	mov	r4,a
;	..\src\COMMON\easyax5043.c:1393: pn9_buffer((__xdata uint8_t *)axradio_cb_receive.st.rx.mac.raw, len, 0x1ff, -(AX5043_ENCODING & 0x01));
	mov	dptr,#_AX5043_ENCODING
	movx	a,@dptr
	mov	r7,a
	anl	ar7,#0x01
	clr	c
	clr	a
	subb	a,r7
	mov	r7,a
	mov	dptr,#(_axradio_cb_receive + 0x001c)
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	mov	r2,#0x00
	push	ar7
	mov	a,#0xff
	push	acc
	mov	a,#0x01
	push	acc
	push	ar5
	push	ar4
	mov	dpl,r3
	mov	dph,r6
	mov	b,r2
	lcall	_pn9_buffer
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
00104$:
;	..\src\COMMON\easyax5043.c:1395: if (axradio_framing_swcrclen && !AXRADIO_MODE_IS_STREAM_RECEIVE(axradio_mode)) {
	mov	dptr,#_axradio_framing_swcrclen
	clr	a
	movc	a,@a+dptr
	jz	00109$
	mov	r6,_axradio_mode
	anl	ar6,#0xf8
	mov	r7,#0x00
	cjne	r6,#0x28,00352$
	cjne	r7,#0x00,00352$
	sjmp	00109$
00352$:
;	..\src\COMMON\easyax5043.c:1396: uint16_t __autodata len = axradio_cb_receive.st.rx.pktlen;
	mov	dptr,#(_axradio_cb_receive + 0x0020)
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
;	..\src\COMMON\easyax5043.c:1397: len += axradio_framing_maclen;
	mov	dptr,#_axradio_framing_maclen
	clr	a
	movc	a,@a+dptr
	mov	r4,#0x00
	add	a,r6
	mov	r5,a
	mov	a,r4
	addc	a,r7
	mov	r4,a
;	..\src\COMMON\easyax5043.c:1398: len = axradio_framing_check_crc((uint8_t __xdata *)axradio_cb_receive.st.rx.mac.raw, len);
	mov	dptr,#(_axradio_cb_receive + 0x001c)
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	push	ar5
	push	ar4
	mov	dpl,r6
	mov	dph,r7
	lcall	_axradio_framing_check_crc
	mov	r6,dpl
	mov	r7,dph
	dec	sp
	dec	sp
;	..\src\COMMON\easyax5043.c:1399: if (!len)
	mov	a,r6
	orl	a,r7
	jnz	00353$
	ljmp	00159$
00353$:
;	..\src\COMMON\easyax5043.c:1402: len -= axradio_framing_maclen;
	mov	dptr,#_axradio_framing_maclen
	clr	a
	movc	a,@a+dptr
	mov	r5,a
	mov	r4,#0x00
	mov	a,r6
	clr	c
	subb	a,r5
	mov	r6,a
	mov	a,r7
	subb	a,r4
	mov	r7,a
;	..\src\COMMON\easyax5043.c:1403: len -= axradio_framing_swcrclen; // drop crc
	mov	dptr,#_axradio_framing_swcrclen
	clr	a
	movc	a,@a+dptr
	mov	r5,a
	mov	r4,#0x00
	mov	a,r6
	clr	c
	subb	a,r5
	mov	r5,a
	mov	a,r7
	subb	a,r4
	mov	r4,a
;	..\src\COMMON\easyax5043.c:1404: axradio_cb_receive.st.rx.pktlen = len;
	mov	dptr,#(_axradio_cb_receive + 0x0020)
	mov	a,r5
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
00109$:
;	..\src\COMMON\easyax5043.c:1408: axradio_cb_receive.st.rx.phy.timeoffset = 0;
	mov	dptr,#(_axradio_cb_receive + 0x0010)
	clr	a
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1409: axradio_cb_receive.st.rx.phy.period = 0;
	mov	dptr,#(_axradio_cb_receive + 0x0012)
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1410: if (axradio_mode == AXRADIO_MODE_ACK_TRANSMIT ||
	mov	a,#0x12
	cjne	a,_axradio_mode,00354$
	sjmp	00113$
00354$:
;	..\src\COMMON\easyax5043.c:1411: axradio_mode == AXRADIO_MODE_WOR_ACK_TRANSMIT ||
	mov	a,#0x13
	cjne	a,_axradio_mode,00355$
	sjmp	00113$
00355$:
;	..\src\COMMON\easyax5043.c:1412: axradio_mode == AXRADIO_MODE_SYNC_ACK_MASTER) {
	mov	a,#0x31
	cjne	a,_axradio_mode,00114$
00113$:
;	..\src\COMMON\easyax5043.c:1413: ax5043_off();
	lcall	_ax5043_off
;	..\src\COMMON\easyax5043.c:1414: wtimer_remove(&axradio_timer);
	mov	dptr,#_axradio_timer
	lcall	_wtimer_remove
;	..\src\COMMON\easyax5043.c:1415: if (axradio_mode == AXRADIO_MODE_SYNC_ACK_MASTER) {
	mov	a,#0x31
	cjne	a,_axradio_mode,00112$
;	..\src\COMMON\easyax5043.c:1416: axradio_syncstate = syncstate_master_normal;
	mov	dptr,#_axradio_syncstate
	mov	a,#0x03
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1417: axradio_sync_settimeradv(axradio_sync_xoscstartup);
	mov	dptr,#_axradio_sync_xoscstartup
	clr	a
	movc	a,@a+dptr
	mov	r4,a
	mov	a,#0x01
	movc	a,@a+dptr
	mov	r5,a
	mov	a,#0x02
	movc	a,@a+dptr
	mov	r6,a
	mov	a,#0x03
	movc	a,@a+dptr
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	lcall	_axradio_sync_settimeradv
;	..\src\COMMON\easyax5043.c:1418: wtimer0_addabsolute(&axradio_timer);
	mov	dptr,#_axradio_timer
	lcall	_wtimer0_addabsolute
00112$:
;	..\src\COMMON\easyax5043.c:1420: wtimer_remove_callback(&axradio_cb_transmitend.cb);
	mov	dptr,#_axradio_cb_transmitend
	lcall	_wtimer_remove_callback
;	..\src\COMMON\easyax5043.c:1421: axradio_cb_transmitend.st.error = AXRADIO_ERR_NOERROR;
	mov	dptr,#(_axradio_cb_transmitend + 0x0005)
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1422: axradio_cb_transmitend.st.time.t = radio_read24((uint16_t)&AX5043_TIMER2);
	mov	r6,#_AX5043_TIMER2
	mov	r7,#(_AX5043_TIMER2 >> 8)
	mov	dpl,r6
	mov	dph,r7
	lcall	_radio_read24
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	dptr,#(_axradio_cb_transmitend + 0x0006)
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1423: wtimer_add_callback(&axradio_cb_transmitend.cb);
	mov	dptr,#_axradio_cb_transmitend
	lcall	_wtimer_add_callback
00114$:
;	..\src\COMMON\easyax5043.c:1425: if (axradio_framing_destaddrpos != 0xff)
	mov	dptr,#_axradio_framing_destaddrpos
	clr	a
	movc	a,@a+dptr
	mov	r7,a
	cjne	r7,#0xff,00360$
	sjmp	00118$
00360$:
;	..\src\COMMON\easyax5043.c:1426: memcpy_xdata(&axradio_cb_receive.st.rx.mac.localaddr, &axradio_cb_receive.st.rx.mac.raw[axradio_framing_destaddrpos], axradio_framing_addrlen);
	mov	dptr,#(_axradio_cb_receive + 0x001c)
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	mov	a,r7
	add	a,r5
	mov	r7,a
	clr	a
	addc	a,r6
	mov	r4,a
	mov	r6,#0x00
	mov	dptr,#_axradio_framing_addrlen
	clr	a
	movc	a,@a+dptr
	mov	r5,a
	mov	r3,#0x00
	mov	dptr,#_memcpy_PARM_2
	mov	a,r7
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_memcpy_PARM_3
	mov	a,r5
	movx	@dptr,a
	mov	a,r3
	inc	dptr
	movx	@dptr,a
	mov	dptr,#(_axradio_cb_receive + 0x0018)
	mov	b,#0x00
	lcall	_memcpy
00118$:
;	..\src\COMMON\easyax5043.c:1427: if (axradio_framing_sourceaddrpos != 0xff)
	mov	dptr,#_axradio_framing_sourceaddrpos
	clr	a
	movc	a,@a+dptr
	mov	r7,a
	cjne	r7,#0xff,00361$
	sjmp	00120$
00361$:
;	..\src\COMMON\easyax5043.c:1428: memcpy_xdata(&axradio_cb_receive.st.rx.mac.remoteaddr, &axradio_cb_receive.st.rx.mac.raw[axradio_framing_sourceaddrpos], axradio_framing_addrlen);
	mov	dptr,#(_axradio_cb_receive + 0x001c)
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	mov	a,r7
	add	a,r5
	mov	r7,a
	clr	a
	addc	a,r6
	mov	r4,a
	mov	r6,#0x00
	mov	dptr,#_axradio_framing_addrlen
	clr	a
	movc	a,@a+dptr
	mov	r5,a
	mov	r3,#0x00
	mov	dptr,#_memcpy_PARM_2
	mov	a,r7
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_memcpy_PARM_3
	mov	a,r5
	movx	@dptr,a
	mov	a,r3
	inc	dptr
	movx	@dptr,a
	mov	dptr,#(_axradio_cb_receive + 0x0014)
	mov	b,#0x00
	lcall	_memcpy
00120$:
;	..\src\COMMON\easyax5043.c:1429: if (axradio_mode == AXRADIO_MODE_ACK_RECEIVE ||
	mov	a,#0x22
	cjne	a,_axradio_mode,00362$
	sjmp	00142$
00362$:
;	..\src\COMMON\easyax5043.c:1430: axradio_mode == AXRADIO_MODE_WOR_ACK_RECEIVE ||
	mov	a,#0x23
	cjne	a,_axradio_mode,00363$
	sjmp	00142$
00363$:
;	..\src\COMMON\easyax5043.c:1431: axradio_mode == AXRADIO_MODE_SYNC_ACK_SLAVE) {
	mov	a,#0x33
	cjne	a,_axradio_mode,00364$
	sjmp	00365$
00364$:
	ljmp	00143$
00365$:
00142$:
;	..\src\COMMON\easyax5043.c:1432: axradio_ack_count = 0;
	mov	dptr,#_axradio_ack_count
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1433: axradio_txbuffer_len = axradio_framing_maclen + axradio_framing_minpayloadlen;
	mov	dptr,#_axradio_framing_maclen
;	genFromRTrack removed	clr	a
	movc	a,@a+dptr
	mov	r7,a
	mov	r5,a
	mov	r6,#0x00
	mov	dptr,#_axradio_framing_minpayloadlen
	clr	a
	movc	a,@a+dptr
	mov	r4,a
	mov	r3,#0x00
	mov	dptr,#_axradio_txbuffer_len
	mov	a,r4
	add	a,r5
	movx	@dptr,a
	mov	a,r3
	addc	a,r6
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1434: memset_xdata(axradio_txbuffer, 0, axradio_framing_maclen);
	mov	r6,#0x00
	mov	dptr,#_memset_PARM_2
	clr	a
	movx	@dptr,a
	mov	dptr,#_memset_PARM_3
	mov	a,r7
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_axradio_txbuffer
	mov	b,#0x00
	lcall	_memset
;	..\src\COMMON\easyax5043.c:1435: if (axradio_framing_ack_seqnrpos != 0xff) {
	mov	dptr,#_axradio_framing_ack_seqnrpos
	clr	a
	movc	a,@a+dptr
	mov	r7,a
	cjne	r7,#0xff,00366$
	sjmp	00125$
00366$:
;	..\src\COMMON\easyax5043.c:1436: uint8_t seqnr = axradio_cb_receive.st.rx.mac.raw[axradio_framing_ack_seqnrpos];
	mov	dptr,#(_axradio_cb_receive + 0x001c)
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	mov	a,r7
	add	a,r5
	mov	dpl,a
	clr	a
	addc	a,r6
	mov	dph,a
	movx	a,@dptr
	mov	r6,a
;	..\src\COMMON\easyax5043.c:1437: axradio_txbuffer[axradio_framing_ack_seqnrpos] = seqnr;
	mov	a,r7
	add	a,#_axradio_txbuffer
	mov	dpl,a
	clr	a
	addc	a,#(_axradio_txbuffer >> 8)
	mov	dph,a
	mov	a,r6
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1438: if (axradio_ack_seqnr != seqnr)
	mov	dptr,#_axradio_ack_seqnr
	movx	a,@dptr
	mov	r7,a
	cjne	a,ar6,00367$
	sjmp	00122$
00367$:
;	..\src\COMMON\easyax5043.c:1439: axradio_ack_seqnr = seqnr;
	mov	dptr,#_axradio_ack_seqnr
	mov	a,r6
	movx	@dptr,a
	sjmp	00125$
00122$:
;	..\src\COMMON\easyax5043.c:1441: axradio_cb_receive.st.error = AXRADIO_ERR_RETRANSMISSION;
	mov	dptr,#(_axradio_cb_receive + 0x0005)
	mov	a,#0x08
	movx	@dptr,a
00125$:
;	..\src\COMMON\easyax5043.c:1443: if (axradio_framing_destaddrpos != 0xff) {
	mov	dptr,#_axradio_framing_destaddrpos
	clr	a
	movc	a,@a+dptr
	mov	r7,a
	cjne	r7,#0xff,00368$
	sjmp	00130$
00368$:
;	..\src\COMMON\easyax5043.c:1444: if (axradio_framing_sourceaddrpos != 0xff)
	mov	dptr,#_axradio_framing_sourceaddrpos
	clr	a
	movc	a,@a+dptr
	mov	r6,a
	cjne	r6,#0xff,00369$
	sjmp	00127$
00369$:
;	..\src\COMMON\easyax5043.c:1445: memcpy_xdata(&axradio_txbuffer[axradio_framing_destaddrpos], &axradio_cb_receive.st.rx.mac.remoteaddr, axradio_framing_addrlen);
	mov	a,r7
	add	a,#_axradio_txbuffer
	mov	r5,a
	clr	a
	addc	a,#(_axradio_txbuffer >> 8)
	mov	r6,a
	mov	r4,#0x00
	mov	dptr,#_axradio_framing_addrlen
	clr	a
	movc	a,@a+dptr
	mov	r3,a
	mov	r2,#0x00
	mov	dptr,#_memcpy_PARM_2
	mov	a,#(_axradio_cb_receive + 0x0014)
	movx	@dptr,a
	mov	a,#((_axradio_cb_receive + 0x0014) >> 8)
	inc	dptr
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_memcpy_PARM_3
	mov	a,r3
	movx	@dptr,a
	mov	a,r2
	inc	dptr
	movx	@dptr,a
	mov	dpl,r5
	mov	dph,r6
	mov	b,r4
	lcall	_memcpy
	sjmp	00130$
00127$:
;	..\src\COMMON\easyax5043.c:1447: memcpy_xdata(&axradio_txbuffer[axradio_framing_destaddrpos], &axradio_default_remoteaddr, axradio_framing_addrlen);
	mov	a,r7
	add	a,#_axradio_txbuffer
	mov	r7,a
	clr	a
	addc	a,#(_axradio_txbuffer >> 8)
	mov	r6,a
	mov	r5,#0x00
	mov	dptr,#_axradio_framing_addrlen
	clr	a
	movc	a,@a+dptr
	mov	r4,a
	mov	r3,#0x00
	mov	dptr,#_memcpy_PARM_2
	mov	a,#_axradio_default_remoteaddr
	movx	@dptr,a
	mov	a,#(_axradio_default_remoteaddr >> 8)
	inc	dptr
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_memcpy_PARM_3
	mov	a,r4
	movx	@dptr,a
	mov	a,r3
	inc	dptr
	movx	@dptr,a
	mov	dpl,r7
	mov	dph,r6
	mov	b,r5
	lcall	_memcpy
00130$:
;	..\src\COMMON\easyax5043.c:1449: if (axradio_framing_sourceaddrpos != 0xff)
	mov	dptr,#_axradio_framing_sourceaddrpos
	clr	a
	movc	a,@a+dptr
	mov	r7,a
	cjne	r7,#0xff,00370$
	sjmp	00132$
00370$:
;	..\src\COMMON\easyax5043.c:1450: memcpy_xdata(&axradio_txbuffer[axradio_framing_sourceaddrpos], &axradio_localaddr.addr, axradio_framing_addrlen);
	mov	a,r7
	add	a,#_axradio_txbuffer
	mov	r7,a
	clr	a
	addc	a,#(_axradio_txbuffer >> 8)
	mov	r6,a
	mov	r5,#0x00
	mov	dptr,#_axradio_framing_addrlen
	clr	a
	movc	a,@a+dptr
	mov	r4,a
	mov	r3,#0x00
	mov	dptr,#_memcpy_PARM_2
	mov	a,#_axradio_localaddr
	movx	@dptr,a
	mov	a,#(_axradio_localaddr >> 8)
	inc	dptr
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_memcpy_PARM_3
	mov	a,r4
	movx	@dptr,a
	mov	a,r3
	inc	dptr
	movx	@dptr,a
	mov	dpl,r7
	mov	dph,r6
	mov	b,r5
	lcall	_memcpy
00132$:
;	..\src\COMMON\easyax5043.c:1451: if (axradio_framing_lenmask) {
	mov	dptr,#_axradio_framing_lenmask
	clr	a
	movc	a,@a+dptr
	mov	r7,a
	jz	00134$
;	..\src\COMMON\easyax5043.c:1452: uint8_t len_byte = (uint8_t)(axradio_txbuffer_len - axradio_framing_lenoffs) & axradio_framing_lenmask; // if you prefer not counting the len byte itself, set LENOFFS = 1
	mov	dptr,#_axradio_txbuffer_len
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	dptr,#_axradio_framing_lenoffs
	clr	a
	movc	a,@a+dptr
	mov	r6,a
	mov	a,r5
	clr	c
	subb	a,r6
	anl	a,r7
	mov	r6,a
;	..\src\COMMON\easyax5043.c:1453: axradio_txbuffer[axradio_framing_lenpos] = (axradio_txbuffer[axradio_framing_lenpos] & (uint8_t)~axradio_framing_lenmask) | len_byte;
	mov	dptr,#_axradio_framing_lenpos
	clr	a
	movc	a,@a+dptr
	add	a,#_axradio_txbuffer
	mov	r5,a
	clr	a
	addc	a,#(_axradio_txbuffer >> 8)
	mov	r4,a
	mov	dpl,r5
	mov	dph,r4
	movx	a,@dptr
	mov	r3,a
	mov	a,r7
	cpl	a
	mov	r7,a
	anl	a,r3
	orl	ar6,a
	mov	dpl,r5
	mov	dph,r4
	mov	a,r6
	movx	@dptr,a
00134$:
;	..\src\COMMON\easyax5043.c:1455: if (axradio_framing_swcrclen)
	mov	dptr,#_axradio_framing_swcrclen
	clr	a
	movc	a,@a+dptr
	jz	00136$
;	..\src\COMMON\easyax5043.c:1456: axradio_txbuffer_len = axradio_framing_append_crc(axradio_txbuffer, axradio_txbuffer_len);
	mov	dptr,#_axradio_txbuffer_len
	movx	a,@dptr
	push	acc
	inc	dptr
	movx	a,@dptr
	push	acc
	mov	dptr,#_axradio_txbuffer
	lcall	_axradio_framing_append_crc
	mov	r6,dpl
	mov	r7,dph
	dec	sp
	dec	sp
	mov	dptr,#_axradio_txbuffer_len
	mov	a,r6
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
00136$:
;	..\src\COMMON\easyax5043.c:1457: if (axradio_phy_pn9) {
	mov	dptr,#_axradio_phy_pn9
	clr	a
	movc	a,@a+dptr
	jz	00138$
;	..\src\COMMON\easyax5043.c:1458: pn9_buffer(axradio_txbuffer, axradio_txbuffer_len, 0x1ff, -(AX5043_ENCODING & 0x01));
	mov	dptr,#_AX5043_ENCODING
	movx	a,@dptr
	mov	r7,a
	anl	ar7,#0x01
	clr	c
	clr	a
	subb	a,r7
	mov	r7,a
	push	ar7
	mov	a,#0xff
	push	acc
	mov	a,#0x01
	push	acc
	mov	dptr,#_axradio_txbuffer_len
	movx	a,@dptr
	push	acc
	inc	dptr
	movx	a,@dptr
	push	acc
	mov	dptr,#_axradio_txbuffer
	mov	b,#0x00
	lcall	_pn9_buffer
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
00138$:
;	..\src\COMMON\easyax5043.c:1460: AX5043_IRQMASK1 = 0x00;
	mov	dptr,#_AX5043_IRQMASK1
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1461: AX5043_IRQMASK0 = 0x00;
	mov	dptr,#_AX5043_IRQMASK0
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1462: AX5043_PWRMODE = AX5043_PWRSTATE_XTAL_ON;
	mov	dptr,#_AX5043_PWRMODE
	mov	a,#0x05
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1463: AX5043_FIFOSTAT = 3;
	mov	dptr,#_AX5043_FIFOSTAT
	mov	a,#0x03
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1464: axradio_trxstate = trxstate_tx_longpreamble; // ensure that trxstate != off, otherwise we would prematurely enable the receiver, see below
	mov	_axradio_trxstate,#0x0a
;	..\src\COMMON\easyax5043.c:1465: while (AX5043_POWSTAT & 0x08);
00139$:
	mov	dptr,#_AX5043_POWSTAT
	movx	a,@dptr
	jb	acc.3,00139$
;	..\src\COMMON\easyax5043.c:1466: wtimer_remove(&axradio_timer);
	mov	dptr,#_axradio_timer
	lcall	_wtimer_remove
;	..\src\COMMON\easyax5043.c:1467: axradio_timer.time = axradio_framing_ack_delay;
	mov	dptr,#_axradio_framing_ack_delay
	clr	a
	movc	a,@a+dptr
	mov	r4,a
	mov	a,#0x01
	movc	a,@a+dptr
	mov	r5,a
	mov	a,#0x02
	movc	a,@a+dptr
	mov	r6,a
	mov	a,#0x03
	movc	a,@a+dptr
	mov	r7,a
	mov	dptr,#(_axradio_timer + 0x0004)
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1468: wtimer1_addrelative(&axradio_timer);
	mov	dptr,#_axradio_timer
	lcall	_wtimer1_addrelative
00143$:
;	..\src\COMMON\easyax5043.c:1470: if (axradio_mode == AXRADIO_MODE_SYNC_SLAVE ||
	mov	a,#0x32
	cjne	a,_axradio_mode,00375$
	sjmp	00156$
00375$:
;	..\src\COMMON\easyax5043.c:1471: axradio_mode == AXRADIO_MODE_SYNC_ACK_SLAVE) {
	mov	a,#0x33
	cjne	a,_axradio_mode,00376$
	sjmp	00377$
00376$:
	ljmp	00157$
00377$:
00156$:
;	..\src\COMMON\easyax5043.c:1472: if (axradio_mode != AXRADIO_MODE_SYNC_ACK_SLAVE)
	mov	a,#0x33
	cjne	a,_axradio_mode,00378$
	sjmp	00147$
00378$:
;	..\src\COMMON\easyax5043.c:1473: ax5043_off();
	lcall	_ax5043_off
00147$:
;	..\src\COMMON\easyax5043.c:1474: switch (axradio_syncstate) {
	mov	dptr,#_axradio_syncstate
	movx	a,@dptr
	mov	r7,a
	cjne	r7,#0x08,00379$
	sjmp	00151$
00379$:
	cjne	r7,#0x0a,00380$
	sjmp	00151$
00380$:
	cjne	r7,#0x0b,00381$
	sjmp	00151$
00381$:
;	..\src\COMMON\easyax5043.c:1478: axradio_sync_time = axradio_conv_time_totimer0(axradio_cb_receive.st.time.t);
	mov	dptr,#(_axradio_cb_receive + 0x0006)
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	lcall	_axradio_conv_time_totimer0
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	dptr,#_axradio_sync_time
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1479: axradio_sync_periodcorr = -32768;
	mov	dptr,#_axradio_sync_periodcorr
	clr	a
	movx	@dptr,a
	mov	a,#0x80
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1480: axradio_sync_seqnr = 0;
	mov	dptr,#_axradio_ack_seqnr
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1481: break;
;	..\src\COMMON\easyax5043.c:1485: case syncstate_slave_rxpacket:
	sjmp	00152$
00151$:
;	..\src\COMMON\easyax5043.c:1486: axradio_sync_adjustperiodcorr();
	lcall	_axradio_sync_adjustperiodcorr
;	..\src\COMMON\easyax5043.c:1487: axradio_cb_receive.st.rx.phy.period = axradio_sync_periodcorr >> SYNC_K1;
	mov	dptr,#_axradio_sync_periodcorr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	swap	a
	rr	a
	xch	a,r6
	swap	a
	rr	a
	anl	a,#0x07
	xrl	a,r6
	xch	a,r6
	anl	a,#0x07
	xch	a,r6
	xrl	a,r6
	xch	a,r6
	jnb	acc.2,00382$
	orl	a,#0xf8
00382$:
	mov	r7,a
	mov	dptr,#(_axradio_cb_receive + 0x0012)
	mov	a,r6
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1488: axradio_sync_seqnr = 1;
	mov	dptr,#_axradio_ack_seqnr
	mov	a,#0x01
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1490: };
00152$:
;	..\src\COMMON\easyax5043.c:1491: axradio_sync_slave_nextperiod();
	lcall	_axradio_sync_slave_nextperiod
;	..\src\COMMON\easyax5043.c:1492: if (axradio_mode != AXRADIO_MODE_SYNC_ACK_SLAVE) {
	mov	a,#0x33
	cjne	a,_axradio_mode,00383$
	sjmp	00154$
00383$:
;	..\src\COMMON\easyax5043.c:1493: axradio_syncstate = syncstate_slave_rxidle;
	mov	dptr,#_axradio_syncstate
	mov	a,#0x08
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1494: wtimer_remove(&axradio_timer);
	mov	dptr,#_axradio_timer
	lcall	_wtimer_remove
;	..\src\COMMON\easyax5043.c:1495: axradio_sync_settimeradv(axradio_sync_slave_rxadvance[axradio_sync_seqnr]);
	mov	dptr,#_axradio_ack_seqnr
	movx	a,@dptr
	mov	b,#0x04
	mul	ab
	add	a,#_axradio_sync_slave_rxadvance
	mov	dpl,a
	mov	a,#(_axradio_sync_slave_rxadvance >> 8)
	addc	a,b
	mov	dph,a
	clr	a
	movc	a,@a+dptr
	mov	r4,a
	inc	dptr
	clr	a
	movc	a,@a+dptr
	mov	r5,a
	inc	dptr
	clr	a
	movc	a,@a+dptr
	mov	r6,a
	inc	dptr
	clr	a
	movc	a,@a+dptr
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	lcall	_axradio_sync_settimeradv
;	..\src\COMMON\easyax5043.c:1496: wtimer0_addabsolute(&axradio_timer);
	mov	dptr,#_axradio_timer
	lcall	_wtimer0_addabsolute
	sjmp	00157$
00154$:
;	..\src\COMMON\easyax5043.c:1498: axradio_syncstate = syncstate_slave_rxack;
	mov	dptr,#_axradio_syncstate
	mov	a,#0x0c
	movx	@dptr,a
00157$:
;	..\src\COMMON\easyax5043.c:1501: axradio_statuschange((struct axradio_status __xdata *)&axradio_cb_receive.st);
	mov	dptr,#(_axradio_cb_receive + 0x0004)
	lcall	_axradio_statuschange
;	..\src\COMMON\easyax5043.c:1502: endcb:
00159$:
;	..\src\COMMON\easyax5043.c:1503: if (axradio_mode == AXRADIO_MODE_WOR_RECEIVE) {
	mov	a,#0x21
	cjne	a,_axradio_mode,00174$
;	..\src\COMMON\easyax5043.c:1504: ax5043_receiver_on_wor();
	ljmp	_ax5043_receiver_on_wor
00174$:
;	..\src\COMMON\easyax5043.c:1505: } else if (axradio_mode == AXRADIO_MODE_ACK_RECEIVE ||
	mov	a,#0x22
	cjne	a,_axradio_mode,00386$
	sjmp	00169$
00386$:
;	..\src\COMMON\easyax5043.c:1506: axradio_mode == AXRADIO_MODE_WOR_ACK_RECEIVE) {
	mov	a,#0x23
	cjne	a,_axradio_mode,00170$
00169$:
;	..\src\COMMON\easyax5043.c:1509: uint8_t __autodata iesave = IE & 0x80;
	mov	a,_IE
	anl	a,#0x80
	mov	r7,a
;	..\src\COMMON\easyax5043.c:1510: EA = 0;
;	assignBit
	clr	_EA
;	..\src\COMMON\easyax5043.c:1511: trxst = axradio_trxstate;
	mov	r6,_axradio_trxstate
;	..\src\COMMON\easyax5043.c:1512: axradio_cb_receive.st.error = AXRADIO_ERR_PACKETDONE;
	mov	dptr,#(_axradio_cb_receive + 0x0005)
	mov	a,#0xf0
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1513: IE |= iesave;
	mov	a,r7
	orl	_IE,a
;	..\src\COMMON\easyax5043.c:1515: if (trxst == trxstate_off) {
	mov	a,r6
	jnz	00176$
;	..\src\COMMON\easyax5043.c:1516: if (axradio_mode == AXRADIO_MODE_WOR_ACK_RECEIVE)
	mov	a,#0x23
	cjne	a,_axradio_mode,00161$
;	..\src\COMMON\easyax5043.c:1517: ax5043_receiver_on_wor();
	ljmp	_ax5043_receiver_on_wor
00161$:
;	..\src\COMMON\easyax5043.c:1519: ax5043_receiver_on_continuous();
	ljmp	_ax5043_receiver_on_continuous
00170$:
;	..\src\COMMON\easyax5043.c:1522: switch (axradio_trxstate) {
	mov	r7,_axradio_trxstate
	cjne	r7,#0x01,00392$
	sjmp	00166$
00392$:
	cjne	r7,#0x02,00176$
;	..\src\COMMON\easyax5043.c:1524: case trxstate_rxwor:
00166$:
;	..\src\COMMON\easyax5043.c:1525: AX5043_IRQMASK0 |= 0x01; // re-enable FIFO not empty irq
	mov	dptr,#_AX5043_IRQMASK0
	movx	a,@dptr
	orl	acc,#0x01
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1530: }
00176$:
;	..\src\COMMON\easyax5043.c:1532: }
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'axradio_killallcb'
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:1534: static void axradio_killallcb(void)
;	-----------------------------------------
;	 function axradio_killallcb
;	-----------------------------------------
_axradio_killallcb:
;	..\src\COMMON\easyax5043.c:1536: wtimer_remove_callback(&axradio_cb_receive.cb);
	mov	dptr,#_axradio_cb_receive
	lcall	_wtimer_remove_callback
;	..\src\COMMON\easyax5043.c:1537: wtimer_remove_callback(&axradio_cb_receivesfd.cb);
	mov	dptr,#_axradio_cb_receivesfd
	lcall	_wtimer_remove_callback
;	..\src\COMMON\easyax5043.c:1538: wtimer_remove_callback(&axradio_cb_channelstate.cb);
	mov	dptr,#_axradio_cb_channelstate
	lcall	_wtimer_remove_callback
;	..\src\COMMON\easyax5043.c:1539: wtimer_remove_callback(&axradio_cb_transmitstart.cb);
	mov	dptr,#_axradio_cb_transmitstart
	lcall	_wtimer_remove_callback
;	..\src\COMMON\easyax5043.c:1540: wtimer_remove_callback(&axradio_cb_transmitend.cb);
	mov	dptr,#_axradio_cb_transmitend
	lcall	_wtimer_remove_callback
;	..\src\COMMON\easyax5043.c:1541: wtimer_remove_callback(&axradio_cb_transmitdata.cb);
	mov	dptr,#_axradio_cb_transmitdata
	lcall	_wtimer_remove_callback
;	..\src\COMMON\easyax5043.c:1542: wtimer_remove(&axradio_timer);
	mov	dptr,#_axradio_timer
;	..\src\COMMON\easyax5043.c:1543: }
	ljmp	_wtimer_remove
;------------------------------------------------------------
;Allocation info for local variables in function 'axradio_tunevoltage'
;------------------------------------------------------------
;x                         Allocated with name '_axradio_tunevoltage_x_196608_398'
;r                         Allocated to registers r6 r7 
;cnt                       Allocated to registers r5 
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:1569: static int16_t axradio_tunevoltage(void)
;	-----------------------------------------
;	 function axradio_tunevoltage
;	-----------------------------------------
_axradio_tunevoltage:
;	..\src\COMMON\easyax5043.c:1571: int16_t __autodata r = 0;
	mov	r6,#0x00
	mov	r7,#0x00
;	..\src\COMMON\easyax5043.c:1573: do {
	mov	r5,#0x40
00103$:
;	..\src\COMMON\easyax5043.c:1574: AX5043_GPADCCTRL = 0x84;
	mov	dptr,#_AX5043_GPADCCTRL
	mov	a,#0x84
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1575: do {} while (AX5043_GPADCCTRL & 0x80);
00101$:
	mov	dptr,#_AX5043_GPADCCTRL
	movx	a,@dptr
	jb	acc.7,00101$
;	..\src\COMMON\easyax5043.c:1576: } while (--cnt);
	djnz	r5,00103$
;	..\src\COMMON\easyax5043.c:1578: do {
	mov	r5,#0x20
00108$:
;	..\src\COMMON\easyax5043.c:1579: AX5043_GPADCCTRL = 0x84;
	mov	dptr,#_AX5043_GPADCCTRL
	mov	a,#0x84
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1580: do {} while (AX5043_GPADCCTRL & 0x80);
00106$:
	mov	dptr,#_AX5043_GPADCCTRL
	movx	a,@dptr
	jb	acc.7,00106$
;	..\src\COMMON\easyax5043.c:1582: int16_t x = AX5043_GPADC13VALUE1 & 0x03;
	mov	dptr,#_AX5043_GPADC13VALUE1
	movx	a,@dptr
	mov	r4,a
	anl	ar4,#0x03
;	..\src\COMMON\easyax5043.c:1583: x <<= 8;
	mov	ar3,r4
	mov	r4,#0x00
;	..\src\COMMON\easyax5043.c:1584: x |= AX5043_GPADC13VALUE0;
	mov	dptr,#_AX5043_GPADC13VALUE0
	movx	a,@dptr
	mov	r1,a
	mov	r2,#0x00
	mov	a,r1
	orl	ar4,a
	mov	a,r2
	orl	ar3,a
;	..\src\COMMON\easyax5043.c:1585: r += x;
	mov	a,r4
	add	a,r6
	mov	r6,a
	mov	a,r3
	addc	a,r7
	mov	r7,a
;	..\src\COMMON\easyax5043.c:1587: } while (--cnt);
	djnz	r5,00108$
;	..\src\COMMON\easyax5043.c:1588: return r;
	mov	dpl,r6
	mov	dph,r7
;	..\src\COMMON\easyax5043.c:1589: }
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'axradio_adjustvcoi'
;------------------------------------------------------------
;rng                       Allocated to registers r7 
;offs                      Allocated to registers r3 
;bestrng                   Allocated to registers r4 
;bestval                   Allocated to registers r5 r6 
;val                       Allocated to stack - _bp +3
;sloc0                     Allocated to stack - _bp +1
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:1593: static __reentrantb uint8_t axradio_adjustvcoi(uint8_t rng) __reentrant
;	-----------------------------------------
;	 function axradio_adjustvcoi
;	-----------------------------------------
_axradio_adjustvcoi:
	push	_bp
	mov	a,sp
	mov	_bp,a
	add	a,#0x04
	mov	sp,a
	mov	r7,dpl
;	..\src\COMMON\easyax5043.c:1597: uint16_t bestval = ~0;
	mov	r5,#0xff
	mov	r6,#0xff
;	..\src\COMMON\easyax5043.c:1598: rng &= 0x7F;
	anl	ar7,#0x7f
;	..\src\COMMON\easyax5043.c:1599: bestrng = rng;
	mov	ar4,r7
;	..\src\COMMON\easyax5043.c:1600: for (offs = 0; offs != 16; ++offs) {
	mov	r3,#0x00
00115$:
;	..\src\COMMON\easyax5043.c:1602: if (!((uint8_t)(rng + offs) & 0xC0)) {
	mov	a,r3
	add	a,r7
	anl	a,#0xc0
	jz	00160$
	sjmp	00104$
00160$:
;	..\src\COMMON\easyax5043.c:1603: AX5043_PLLVCOI = 0x80 | (rng + offs);
	mov	a,r3
	add	a,r7
	mov	dptr,#_AX5043_PLLVCOI
	orl	a,#0x80
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1604: val = axradio_tunevoltage();
	push	ar7
	push	ar6
	push	ar5
	push	ar4
	push	ar3
	lcall	_axradio_tunevoltage
	mov	r0,_bp
	inc	r0
	mov	@r0,dpl
	inc	r0
	mov	@r0,dph
	pop	ar3
	pop	ar4
	pop	ar5
	pop	ar6
	pop	ar7
;	..\src\COMMON\easyax5043.c:1605: if (val < bestval) {
	mov	r0,_bp
	inc	r0
	clr	c
	mov	a,@r0
	subb	a,r5
	inc	r0
	mov	a,@r0
	subb	a,r6
	jnc	00104$
;	..\src\COMMON\easyax5043.c:1606: bestval = val;
	mov	r0,_bp
	inc	r0
	mov	ar5,@r0
	inc	r0
	mov	ar6,@r0
;	..\src\COMMON\easyax5043.c:1607: bestrng = rng + offs;
	mov	a,r3
	add	a,r7
	mov	r2,a
	mov	r4,a
00104$:
;	..\src\COMMON\easyax5043.c:1610: if (!offs)
	mov	a,r3
	jz	00111$
;	..\src\COMMON\easyax5043.c:1612: if (!((uint8_t)(rng - offs) & 0xC0)) {
	mov	a,r7
	clr	c
	subb	a,r3
	anl	a,#0xc0
	jz	00164$
	sjmp	00111$
00164$:
;	..\src\COMMON\easyax5043.c:1613: AX5043_PLLVCOI = 0x80 | (rng - offs);
	push	ar4
	mov	a,r7
	clr	c
	subb	a,r3
	mov	dptr,#_AX5043_PLLVCOI
	orl	a,#0x80
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1614: val = axradio_tunevoltage();
	push	ar7
	push	ar6
	push	ar5
	push	ar3
	lcall	_axradio_tunevoltage
	mov	r2,dpl
	mov	r4,dph
	pop	ar3
	pop	ar5
	pop	ar6
	pop	ar7
	mov	a,_bp
	add	a,#0x03
	mov	r0,a
	mov	@r0,ar2
	inc	r0
	mov	@r0,ar4
;	..\src\COMMON\easyax5043.c:1615: if (val < bestval) {
	mov	a,_bp
	add	a,#0x03
	mov	r0,a
	clr	c
	mov	a,@r0
	subb	a,r5
	inc	r0
	mov	a,@r0
	subb	a,r6
	pop	ar4
	jnc	00111$
;	..\src\COMMON\easyax5043.c:1616: bestval = val;
	mov	a,_bp
	add	a,#0x03
	mov	r0,a
	mov	ar5,@r0
	inc	r0
	mov	ar6,@r0
;	..\src\COMMON\easyax5043.c:1617: bestrng = rng - offs;
	mov	a,r7
	clr	c
	subb	a,r3
	mov	r2,a
	mov	r4,a
00111$:
;	..\src\COMMON\easyax5043.c:1600: for (offs = 0; offs != 16; ++offs) {
	inc	r3
	cjne	r3,#0x10,00166$
	sjmp	00167$
00166$:
	ljmp	00115$
00167$:
;	..\src\COMMON\easyax5043.c:1622: if (bestval <= 0x0010)
	clr	c
	mov	a,#0x10
	subb	a,r5
	clr	a
	subb	a,r6
	jc	00114$
;	..\src\COMMON\easyax5043.c:1623: return rng | 0x80;
	orl	ar7,#0x80
	mov	dpl,r7
	sjmp	00116$
00114$:
;	..\src\COMMON\easyax5043.c:1624: return bestrng | 0x80;
	orl	ar4,#0x80
	mov	dpl,r4
00116$:
;	..\src\COMMON\easyax5043.c:1625: }
	mov	sp,_bp
	pop	_bp
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'axradio_calvcoi'
;------------------------------------------------------------
;i                         Allocated to registers r2 
;r                         Allocated to registers r7 
;vmin                      Allocated to registers r5 r6 
;vmax                      Allocated to stack - _bp +1
;curtune                   Allocated to registers r3 r4 
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:1627: static __reentrantb uint8_t axradio_calvcoi(void) __reentrant
;	-----------------------------------------
;	 function axradio_calvcoi
;	-----------------------------------------
_axradio_calvcoi:
	push	_bp
	mov	_bp,sp
	inc	sp
	inc	sp
;	..\src\COMMON\easyax5043.c:1630: uint8_t r = 0;
	mov	r7,#0x00
;	..\src\COMMON\easyax5043.c:1631: uint16_t vmin = 0xffff;
	mov	r5,#0xff
	mov	r6,#0xff
;	..\src\COMMON\easyax5043.c:1632: uint16_t vmax = 0x0000;
	mov	r0,_bp
	inc	r0
	clr	a
	mov	@r0,a
	inc	r0
	mov	@r0,a
;	..\src\COMMON\easyax5043.c:1633: for (i = 0x40; i != 0;) {
	mov	r2,#0x40
00113$:
;	..\src\COMMON\easyax5043.c:1635: --i;
	push	ar7
	dec	r2
;	..\src\COMMON\easyax5043.c:1636: AX5043_PLLVCOI = 0x80 | i;
	mov	dptr,#_AX5043_PLLVCOI
	mov	a,#0x80
	orl	a,r2
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1637: AX5043_PLLRANGINGA; // clear PLL lock loss
	mov	dptr,#_AX5043_PLLRANGINGA
	movx	a,@dptr
;	..\src\COMMON\easyax5043.c:1638: curtune = axradio_tunevoltage();
	push	ar6
	push	ar5
	push	ar2
	lcall	_axradio_tunevoltage
	mov	r7,dpl
	mov	r4,dph
	pop	ar2
	pop	ar5
	pop	ar6
	mov	ar3,r7
;	..\src\COMMON\easyax5043.c:1639: AX5043_PLLRANGINGA; // clear PLL lock loss
	mov	dptr,#_AX5043_PLLRANGINGA
	movx	a,@dptr
;	..\src\COMMON\easyax5043.c:1640: ((uint16_t __xdata *)axradio_rxbuffer)[i] = curtune;
	mov	a,r2
	mov	b,#0x02
	mul	ab
	add	a,#_axradio_rxbuffer
	mov	dpl,a
	mov	a,#(_axradio_rxbuffer >> 8)
	addc	a,b
	mov	dph,a
	mov	a,r3
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1641: if (curtune > vmax)
	mov	r0,_bp
	inc	r0
	clr	c
	mov	a,@r0
	subb	a,r3
	inc	r0
	mov	a,@r0
	subb	a,r4
	pop	ar7
	jnc	00102$
;	..\src\COMMON\easyax5043.c:1642: vmax = curtune;
	mov	r0,_bp
	inc	r0
	mov	@r0,ar3
	inc	r0
	mov	@r0,ar4
00102$:
;	..\src\COMMON\easyax5043.c:1643: if (curtune < vmin) {
	clr	c
	mov	a,r3
	subb	a,r5
	mov	a,r4
	subb	a,r6
	jnc	00114$
;	..\src\COMMON\easyax5043.c:1644: vmin = curtune;
	mov	ar5,r3
	mov	ar6,r4
;	..\src\COMMON\easyax5043.c:1646: if (!(0xC0 & (uint8_t)~AX5043_PLLRANGINGA))
	mov	dptr,#_AX5043_PLLRANGINGA
	movx	a,@dptr
	cpl	a
	mov	r4,a
	anl	a,#0xc0
	jz	00153$
	sjmp	00114$
00153$:
;	..\src\COMMON\easyax5043.c:1647: r = i | 0x80;
	mov	a,#0x80
	orl	a,r2
	mov	r7,a
00114$:
;	..\src\COMMON\easyax5043.c:1633: for (i = 0x40; i != 0;) {
	mov	a,r2
	jnz	00113$
;	..\src\COMMON\easyax5043.c:1650: if (!(r & 0x80) || vmax >= 0xFF00 || vmin < 0x0100 || vmax - vmin < 0x6000)
	mov	a,r7
	jnb	acc.7,00108$
	mov	r0,_bp
	inc	r0
	clr	c
	inc	r0
	mov	a,@r0
	subb	a,#0xff
	jnc	00108$
	mov	a,#0x100 - 0x01
	add	a,r6
	jnc	00108$
	mov	r0,_bp
	inc	r0
	mov	a,@r0
	clr	c
	subb	a,r5
	mov	r5,a
	inc	r0
	mov	a,@r0
	subb	a,r6
	mov	r6,a
	clr	c
	subb	a,#0x60
	jnc	00109$
00108$:
;	..\src\COMMON\easyax5043.c:1651: return 0;
	mov	dpl,#0x00
	sjmp	00115$
00109$:
;	..\src\COMMON\easyax5043.c:1652: return r;
	mov	dpl,r7
00115$:
;	..\src\COMMON\easyax5043.c:1653: }
	mov	sp,_bp
	pop	_bp
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'axradio_init'
;------------------------------------------------------------
;i                         Allocated to registers r7 
;iesave                    Allocated to registers r6 
;f                         Allocated to registers r3 r4 r5 r6 
;r                         Allocated to registers r4 
;vcoisave                  Allocated to registers r7 
;f                         Allocated to registers r0 r1 r2 r3 
;f                         Allocated to registers r4 r5 r6 r7 
;x                         Allocated with name '_axradio_init_x_196608_424'
;j                         Allocated with name '_axradio_init_j_196608_425'
;x                         Allocated with name '_axradio_init_x_458752_430'
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:1659: uint8_t axradio_init(void)
;	-----------------------------------------
;	 function axradio_init
;	-----------------------------------------
_axradio_init:
;	..\src\COMMON\easyax5043.c:1662: axradio_mode = AXRADIO_MODE_UNINIT;
	mov	_axradio_mode,#0x00
;	..\src\COMMON\easyax5043.c:1663: axradio_killallcb();
	lcall	_axradio_killallcb
;	..\src\COMMON\easyax5043.c:1664: axradio_cb_receive.cb.handler = axradio_receive_callback_fwd;
	mov	dptr,#(_axradio_cb_receive + 0x0002)
	mov	a,#_axradio_receive_callback_fwd
	movx	@dptr,a
	mov	a,#(_axradio_receive_callback_fwd >> 8)
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1665: axradio_cb_receive.st.status = AXRADIO_STAT_RECEIVE;
	mov	dptr,#(_axradio_cb_receive + 0x0004)
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1666: memset_xdata(axradio_cb_receive.st.rx.mac.remoteaddr, 0, sizeof(axradio_cb_receive.st.rx.mac.remoteaddr));
	mov	dptr,#_memset_PARM_2
	movx	@dptr,a
	mov	dptr,#_memset_PARM_3
	mov	a,#0x04
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
	mov	dptr,#(_axradio_cb_receive + 0x0014)
	mov	b,#0x00
	lcall	_memset
;	..\src\COMMON\easyax5043.c:1667: memset_xdata(axradio_cb_receive.st.rx.mac.localaddr, 0, sizeof(axradio_cb_receive.st.rx.mac.localaddr));
	mov	dptr,#_memset_PARM_2
	clr	a
	movx	@dptr,a
	mov	dptr,#_memset_PARM_3
	mov	a,#0x04
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
	mov	dptr,#(_axradio_cb_receive + 0x0018)
	mov	b,#0x00
	lcall	_memset
;	..\src\COMMON\easyax5043.c:1668: axradio_cb_receivesfd.cb.handler = axradio_callback_fwd;
	mov	dptr,#(_axradio_cb_receivesfd + 0x0002)
	mov	a,#_axradio_callback_fwd
	movx	@dptr,a
	mov	a,#(_axradio_callback_fwd >> 8)
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1669: axradio_cb_receivesfd.st.status = AXRADIO_STAT_RECEIVESFD;
	mov	dptr,#(_axradio_cb_receivesfd + 0x0004)
	mov	a,#0x01
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1670: axradio_cb_channelstate.cb.handler = axradio_callback_fwd;
	mov	dptr,#(_axradio_cb_channelstate + 0x0002)
	mov	a,#_axradio_callback_fwd
	movx	@dptr,a
	mov	a,#(_axradio_callback_fwd >> 8)
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1671: axradio_cb_channelstate.st.status = AXRADIO_STAT_CHANNELSTATE;
	mov	dptr,#(_axradio_cb_channelstate + 0x0004)
	mov	a,#0x02
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1672: axradio_cb_transmitstart.cb.handler = axradio_callback_fwd;
	mov	dptr,#(_axradio_cb_transmitstart + 0x0002)
	mov	a,#_axradio_callback_fwd
	movx	@dptr,a
	mov	a,#(_axradio_callback_fwd >> 8)
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1673: axradio_cb_transmitstart.st.status = AXRADIO_STAT_TRANSMITSTART;
	mov	dptr,#(_axradio_cb_transmitstart + 0x0004)
	mov	a,#0x03
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1674: axradio_cb_transmitend.cb.handler = axradio_callback_fwd;
	mov	dptr,#(_axradio_cb_transmitend + 0x0002)
	mov	a,#_axradio_callback_fwd
	movx	@dptr,a
	mov	a,#(_axradio_callback_fwd >> 8)
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1675: axradio_cb_transmitend.st.status = AXRADIO_STAT_TRANSMITEND;
	mov	dptr,#(_axradio_cb_transmitend + 0x0004)
	mov	a,#0x04
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1676: axradio_cb_transmitdata.cb.handler = axradio_callback_fwd;
	mov	dptr,#(_axradio_cb_transmitdata + 0x0002)
	mov	a,#_axradio_callback_fwd
	movx	@dptr,a
	mov	a,#(_axradio_callback_fwd >> 8)
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1677: axradio_cb_transmitdata.st.status = AXRADIO_STAT_TRANSMITDATA;
	mov	dptr,#(_axradio_cb_transmitdata + 0x0004)
	mov	a,#0x05
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1678: axradio_timer.handler = axradio_timer_callback;
	mov	dptr,#(_axradio_timer + 0x0002)
	mov	a,#_axradio_timer_callback
	movx	@dptr,a
	mov	a,#(_axradio_timer_callback >> 8)
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1679: axradio_curchannel = 0;
	mov	dptr,#_axradio_curchannel
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1680: axradio_curfreqoffset = 0;
	mov	dptr,#_axradio_curfreqoffset
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1681: IE_4 = 0;
;	assignBit
	clr	_IE_4
;	..\src\COMMON\easyax5043.c:1682: axradio_trxstate = trxstate_off;
	mov	_axradio_trxstate,#0x00
;	..\src\COMMON\easyax5043.c:1683: if (ax5043_reset())
	lcall	_ax5043_reset
	mov	a,dpl
	jz	00102$
;	..\src\COMMON\easyax5043.c:1685: return AXRADIO_ERR_NOCHIP;
	mov	dpl,#0x05
	ret
00102$:
;	..\src\COMMON\easyax5043.c:1687: ax5043_init_registers();
	lcall	_ax5043_init_registers
;	..\src\COMMON\easyax5043.c:1688: ax5043_set_registers_tx();
	lcall	_ax5043_set_registers_tx
;	..\src\COMMON\easyax5043.c:1689: AX5043_PLLLOOP = 0x09; // default 100kHz loop BW for ranging
	mov	dptr,#_AX5043_PLLLOOP
	mov	a,#0x09
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1690: AX5043_PLLCPI = 0x08;
	mov	dptr,#_AX5043_PLLCPI
	dec	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1692: IE_4 = 1;
;	assignBit
	setb	_IE_4
;	..\src\COMMON\easyax5043.c:1694: AX5043_PWRMODE = AX5043_PWRSTATE_XTAL_ON;
	mov	dptr,#_AX5043_PWRMODE
	mov	a,#0x05
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1695: AX5043_MODULATION = 0x08;
	mov	dptr,#_AX5043_MODULATION
	mov	a,#0x08
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1696: AX5043_FSKDEV2 = 0x00;
	mov	dptr,#_AX5043_FSKDEV2
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1697: AX5043_FSKDEV1 = 0x00;
	mov	dptr,#_AX5043_FSKDEV1
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1698: AX5043_FSKDEV0 = 0x00;
	mov	dptr,#_AX5043_FSKDEV0
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1699: axradio_wait_for_xtal();
	lcall	_axradio_wait_for_xtal
;	..\src\COMMON\easyax5043.c:1701: for (i = 0; i < axradio_phy_nrchannels; ++i) {
	mov	r7,#0x00
00136$:
	mov	dptr,#_axradio_phy_nrchannels
	clr	a
	movc	a,@a+dptr
	mov	r6,a
	clr	c
	mov	a,r7
	subb	a,r6
	jc	00249$
	ljmp	00113$
00249$:
;	..\src\COMMON\easyax5043.c:1704: uint32_t __autodata f = axradio_phy_chanfreq[i];
	mov	a,r7
	mov	b,#0x04
	mul	ab
	add	a,#_axradio_phy_chanfreq
	mov	dpl,a
	mov	a,#(_axradio_phy_chanfreq >> 8)
	addc	a,b
	mov	dph,a
	clr	a
	movc	a,@a+dptr
	mov	r3,a
	inc	dptr
	clr	a
	movc	a,@a+dptr
	mov	r4,a
	inc	dptr
	clr	a
	movc	a,@a+dptr
	mov	r5,a
	inc	dptr
	clr	a
	movc	a,@a+dptr
	mov	r6,a
;	..\src\COMMON\easyax5043.c:1705: AX5043_FREQA0 = f;
	mov	dptr,#_AX5043_FREQA0
	mov	a,r3
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1706: AX5043_FREQA1 = f >> 8;
	mov	dptr,#_AX5043_FREQA1
	mov	a,r4
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1707: AX5043_FREQA2 = f >> 16;
	mov	dptr,#_AX5043_FREQA2
	mov	a,r5
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1708: AX5043_FREQA3 = f >> 24;
	mov	dptr,#_AX5043_FREQA3
	mov	a,r6
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1710: iesave = IE & 0x80;
	mov	a,_IE
	anl	a,#0x80
	mov	r6,a
;	..\src\COMMON\easyax5043.c:1711: EA = 0;
;	assignBit
	clr	_EA
;	..\src\COMMON\easyax5043.c:1712: axradio_trxstate = trxstate_pll_ranging;
	mov	_axradio_trxstate,#0x05
;	..\src\COMMON\easyax5043.c:1713: AX5043_IRQMASK1 = 0x10; // enable pll autoranging done interrupt
	mov	dptr,#_AX5043_IRQMASK1
	mov	a,#0x10
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1716: if( !(axradio_phy_chanpllrnginit[0] & 0xF0) ) { // start values for ranging available
	mov	dptr,#_axradio_phy_chanpllrnginit
	clr	a
	movc	a,@a+dptr
	anl	a,#0xf0
	jnz	00108$
;	..\src\COMMON\easyax5043.c:1717: r = axradio_phy_chanpllrnginit[i] | 0x10;
	mov	a,r7
	mov	dptr,#_axradio_phy_chanpllrnginit
	movc	a,@a+dptr
	mov	r5,a
	orl	ar5,#0x10
	sjmp	00109$
00108$:
;	..\src\COMMON\easyax5043.c:1720: r = 0x18;
	mov	r5,#0x18
;	..\src\COMMON\easyax5043.c:1721: if (i) {
	mov	a,r7
	jz	00109$
;	..\src\COMMON\easyax5043.c:1722: r = axradio_phy_chanpllrng[i - 1];
	mov	ar3,r7
	mov	r4,#0x00
	dec	r3
	cjne	r3,#0xff,00253$
	dec	r4
00253$:
	mov	a,r3
	add	a,#_axradio_phy_chanpllrng
	mov	dpl,a
	mov	a,r4
	addc	a,#(_axradio_phy_chanpllrng >> 8)
	mov	dph,a
	movx	a,@dptr
;	..\src\COMMON\easyax5043.c:1723: if (r & 0x20)
	mov	r4,a
	jnb	acc.5,00104$
;	..\src\COMMON\easyax5043.c:1724: r = 0x08;
	mov	r4,#0x08
00104$:
;	..\src\COMMON\easyax5043.c:1725: r &= 0x0F;
	mov	a,#0x0f
	anl	a,r4
;	..\src\COMMON\easyax5043.c:1726: r |= 0x10;
	orl	a,#0x10
	mov	r5,a
00109$:
;	..\src\COMMON\easyax5043.c:1729: AX5043_PLLRANGINGA = r; // init ranging process starting from "range"
	mov	dptr,#_AX5043_PLLRANGINGA
	mov	a,r5
	movx	@dptr,a
00133$:
;	..\src\COMMON\easyax5043.c:1732: EA = 0;
;	assignBit
	clr	_EA
;	..\src\COMMON\easyax5043.c:1733: if (axradio_trxstate == trxstate_pll_ranging_done)
	mov	a,#0x06
	cjne	a,_axradio_trxstate,00255$
	sjmp	00112$
00255$:
;	..\src\COMMON\easyax5043.c:1735: wtimer_idle(WTFLAG_CANSTANDBY);
	mov	dpl,#0x02
	push	ar7
	push	ar6
	lcall	_wtimer_idle
	pop	ar6
;	..\src\COMMON\easyax5043.c:1736: IE |= iesave;
	mov	a,r6
	orl	_IE,a
;	..\src\COMMON\easyax5043.c:1737: wtimer_runcallbacks();
	push	ar6
	lcall	_wtimer_runcallbacks
	pop	ar6
	pop	ar7
	sjmp	00133$
00112$:
;	..\src\COMMON\easyax5043.c:1739: axradio_trxstate = trxstate_off;
	mov	_axradio_trxstate,#0x00
;	..\src\COMMON\easyax5043.c:1740: AX5043_IRQMASK1 = 0x00;
	mov	dptr,#_AX5043_IRQMASK1
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1741: axradio_phy_chanpllrng[i] = AX5043_PLLRANGINGA;
	mov	a,r7
	add	a,#_axradio_phy_chanpllrng
	mov	r4,a
	clr	a
	addc	a,#(_axradio_phy_chanpllrng >> 8)
	mov	r5,a
	mov	dptr,#_AX5043_PLLRANGINGA
	movx	a,@dptr
	mov	r3,a
	mov	dpl,r4
	mov	dph,r5
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1742: IE |= iesave;
	mov	a,r6
	orl	_IE,a
;	..\src\COMMON\easyax5043.c:1701: for (i = 0; i < axradio_phy_nrchannels; ++i) {
	inc	r7
	ljmp	00136$
00113$:
;	..\src\COMMON\easyax5043.c:1745: if (axradio_phy_vcocalib) {
	mov	dptr,#_axradio_phy_vcocalib
	clr	a
	movc	a,@a+dptr
	jnz	00256$
	ljmp	00129$
00256$:
;	..\src\COMMON\easyax5043.c:1746: ax5043_set_registers_tx();
	lcall	_ax5043_set_registers_tx
;	..\src\COMMON\easyax5043.c:1747: AX5043_MODULATION = 0x08;
	mov	dptr,#_AX5043_MODULATION
	mov	a,#0x08
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1748: AX5043_FSKDEV2 = 0x00;
	mov	dptr,#_AX5043_FSKDEV2
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1749: AX5043_FSKDEV1 = 0x00;
	mov	dptr,#_AX5043_FSKDEV1
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1750: AX5043_FSKDEV0 = 0x00;
	mov	dptr,#_AX5043_FSKDEV0
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1751: AX5043_PLLLOOP |= 0x04;
	mov	dptr,#_AX5043_PLLLOOP
	movx	a,@dptr
	orl	acc,#0x04
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1753: uint8_t x = AX5043_0xF35;
	mov	dptr,#_AX5043_0xF35
	movx	a,@dptr
;	..\src\COMMON\easyax5043.c:1754: x |= 0x80;
	orl	a,#0x80
	mov	r7,a
	mov	dptr,#_axradio_init_x_196608_424
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1755: if (2 & (uint8_t)~x)
	mov	a,r7
	cpl	a
	jnb	acc.1,00115$
;	..\src\COMMON\easyax5043.c:1756: ++x;
	mov	dptr,#_axradio_init_x_196608_424
	mov	a,r7
	inc	a
	movx	@dptr,a
00115$:
;	..\src\COMMON\easyax5043.c:1757: AX5043_0xF35 = x;
	mov	dptr,#_axradio_init_x_196608_424
	movx	a,@dptr
	mov	dptr,#_AX5043_0xF35
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1759: AX5043_PWRMODE = AX5043_PWRSTATE_SYNTH_TX;
	mov	dptr,#_AX5043_PWRMODE
	mov	a,#0x0c
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1761: uint8_t __autodata vcoisave = AX5043_PLLVCOI;
	mov	dptr,#_AX5043_PLLVCOI
	movx	a,@dptr
	mov	r7,a
;	..\src\COMMON\easyax5043.c:1762: uint8_t j = 2;
	mov	dptr,#_axradio_init_j_196608_425
	mov	a,#0x02
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1763: for (i = 0; i < axradio_phy_nrchannels; ++i) {
	mov	r6,#0x00
00139$:
	mov	dptr,#_axradio_phy_nrchannels
	clr	a
	movc	a,@a+dptr
	mov	r5,a
	clr	c
	mov	a,r6
	subb	a,r5
	jc	00258$
	ljmp	00127$
00258$:
;	..\src\COMMON\easyax5043.c:1764: axradio_phy_chanvcoi[i] = 0;
	mov	a,r6
	add	a,#_axradio_phy_chanvcoi
	mov	dpl,a
	clr	a
	addc	a,#(_axradio_phy_chanvcoi >> 8)
	mov	dph,a
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1765: if (axradio_phy_chanpllrng[i] & 0x20)
	mov	a,r6
	add	a,#_axradio_phy_chanpllrng
	mov	r4,a
	clr	a
	addc	a,#(_axradio_phy_chanpllrng >> 8)
	mov	r5,a
	mov	dpl,r4
	mov	dph,r5
	movx	a,@dptr
	mov	r3,a
	jnb	acc.5,00259$
	ljmp	00126$
00259$:
;	..\src\COMMON\easyax5043.c:1767: AX5043_PLLRANGINGA = axradio_phy_chanpllrng[i] & 0x0F;
	mov	dptr,#_AX5043_PLLRANGINGA
	mov	a,#0x0f
	anl	a,r3
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1769: uint32_t __autodata f = axradio_phy_chanfreq[i];
	mov	a,r6
	mov	b,#0x04
	mul	ab
	add	a,#_axradio_phy_chanfreq
	mov	dpl,a
	mov	a,#(_axradio_phy_chanfreq >> 8)
	addc	a,b
	mov	dph,a
	clr	a
	movc	a,@a+dptr
	mov	r0,a
	inc	dptr
	clr	a
	movc	a,@a+dptr
	mov	r1,a
	inc	dptr
	clr	a
	movc	a,@a+dptr
	mov	r2,a
	inc	dptr
	clr	a
	movc	a,@a+dptr
	mov	r3,a
;	..\src\COMMON\easyax5043.c:1770: AX5043_FREQA0 = f;
	mov	dptr,#_AX5043_FREQA0
	mov	a,r0
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1771: AX5043_FREQA1 = f >> 8;
	mov	dptr,#_AX5043_FREQA1
	mov	a,r1
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1772: AX5043_FREQA2 = f >> 16;
	mov	dptr,#_AX5043_FREQA2
	mov	a,r2
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1773: AX5043_FREQA3 = f >> 24;
	mov	dptr,#_AX5043_FREQA3
	mov	a,r3
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1775: do {
	mov	dptr,#_axradio_init_j_196608_425
	movx	a,@dptr
	mov	r3,a
00123$:
;	..\src\COMMON\easyax5043.c:1776: if (axradio_phy_chanvcoiinit[0]) {
	mov	dptr,#_axradio_phy_chanvcoiinit
	clr	a
	movc	a,@a+dptr
	jz	00121$
;	..\src\COMMON\easyax5043.c:1777: uint8_t x = axradio_phy_chanvcoiinit[i];
	mov	a,r6
	mov	dptr,#_axradio_phy_chanvcoiinit
	movc	a,@a+dptr
	mov	r2,a
	mov	dptr,#_axradio_init_x_458752_430
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1778: if (!(axradio_phy_chanpllrnginit[0] & 0xF0))
	mov	dptr,#_axradio_phy_chanpllrnginit
	clr	a
	movc	a,@a+dptr
	anl	a,#0xf0
	jnz	00119$
;	..\src\COMMON\easyax5043.c:1779: x += (axradio_phy_chanpllrng[i] & 0x0F) - (axradio_phy_chanpllrnginit[i] & 0x0F);
	mov	dpl,r4
	mov	dph,r5
	movx	a,@dptr
	mov	r1,a
	anl	ar1,#0x0f
	mov	a,r6
	mov	dptr,#_axradio_phy_chanpllrnginit
	movc	a,@a+dptr
	mov	r0,a
	mov	a,#0x0f
	anl	a,r0
	setb	c
	subb	a,r1
	cpl	a
	mov	r1,a
	mov	dptr,#_axradio_init_x_458752_430
	add	a,r2
	movx	@dptr,a
00119$:
;	..\src\COMMON\easyax5043.c:1780: axradio_phy_chanvcoi[i] = axradio_adjustvcoi(x);
	mov	a,r6
	add	a,#_axradio_phy_chanvcoi
	mov	r1,a
	clr	a
	addc	a,#(_axradio_phy_chanvcoi >> 8)
	mov	r2,a
	mov	dptr,#_axradio_init_x_458752_430
	movx	a,@dptr
	mov	dpl,a
	push	ar7
	push	ar6
	push	ar5
	push	ar4
	push	ar3
	push	ar2
	push	ar1
	lcall	_axradio_adjustvcoi
	mov	r0,dpl
	pop	ar1
	pop	ar2
	pop	ar3
	pop	ar4
	pop	ar5
	pop	ar6
	pop	ar7
	mov	dpl,r1
	mov	dph,r2
	mov	a,r0
	movx	@dptr,a
	sjmp	00124$
00121$:
;	..\src\COMMON\easyax5043.c:1782: axradio_phy_chanvcoi[i] = axradio_calvcoi();
	mov	a,r6
	add	a,#_axradio_phy_chanvcoi
	mov	r1,a
	clr	a
	addc	a,#(_axradio_phy_chanvcoi >> 8)
	mov	r2,a
	push	ar7
	push	ar6
	push	ar5
	push	ar4
	push	ar3
	push	ar2
	push	ar1
	lcall	_axradio_calvcoi
	mov	r0,dpl
	pop	ar1
	pop	ar2
	pop	ar3
	pop	ar4
	pop	ar5
	pop	ar6
	pop	ar7
	mov	dpl,r1
	mov	dph,r2
	mov	a,r0
	movx	@dptr,a
00124$:
;	..\src\COMMON\easyax5043.c:1784: } while (--j);
	dec	r3
	mov	a,r3
	jz	00263$
	ljmp	00123$
00263$:
;	..\src\COMMON\easyax5043.c:1785: j = 1;
	mov	dptr,#_axradio_init_j_196608_425
	mov	a,#0x01
	movx	@dptr,a
00126$:
;	..\src\COMMON\easyax5043.c:1763: for (i = 0; i < axradio_phy_nrchannels; ++i) {
	inc	r6
	ljmp	00139$
00127$:
;	..\src\COMMON\easyax5043.c:1788: AX5043_PLLVCOI = vcoisave;
	mov	dptr,#_AX5043_PLLVCOI
	mov	a,r7
	movx	@dptr,a
00129$:
;	..\src\COMMON\easyax5043.c:1792: AX5043_PWRMODE = AX5043_PWRSTATE_POWERDOWN;
	mov	dptr,#_AX5043_PWRMODE
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1793: ax5043_init_registers();
	lcall	_ax5043_init_registers
;	..\src\COMMON\easyax5043.c:1794: ax5043_set_registers_rx();
	lcall	_ax5043_set_registers_rx
;	..\src\COMMON\easyax5043.c:1795: AX5043_PLLRANGINGA = axradio_phy_chanpllrng[0] & 0x0F;
	mov	dptr,#_axradio_phy_chanpllrng
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#_AX5043_PLLRANGINGA
	mov	a,#0x0f
	anl	a,r7
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1797: uint32_t __autodata f = axradio_phy_chanfreq[0];
	mov	dptr,#_axradio_phy_chanfreq
	clr	a
	movc	a,@a+dptr
	mov	r4,a
	inc	dptr
	clr	a
	movc	a,@a+dptr
	mov	r5,a
	inc	dptr
	clr	a
	movc	a,@a+dptr
	mov	r6,a
	inc	dptr
	clr	a
	movc	a,@a+dptr
	mov	r7,a
;	..\src\COMMON\easyax5043.c:1798: AX5043_FREQA0 = f;
	mov	dptr,#_AX5043_FREQA0
	mov	a,r4
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1799: AX5043_FREQA1 = f >> 8;
	mov	dptr,#_AX5043_FREQA1
	mov	a,r5
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1800: AX5043_FREQA2 = f >> 16;
	mov	dptr,#_AX5043_FREQA2
	mov	a,r6
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1801: AX5043_FREQA3 = f >> 24;
	mov	dptr,#_AX5043_FREQA3
	mov	a,r7
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1804: axradio_mode = AXRADIO_MODE_OFF;
	mov	_axradio_mode,#0x01
;	..\src\COMMON\easyax5043.c:1805: for (i = 0; i < axradio_phy_nrchannels; ++i)
	mov	r7,#0x00
00141$:
	mov	dptr,#_axradio_phy_nrchannels
	clr	a
	movc	a,@a+dptr
	mov	r6,a
	clr	c
	mov	a,r7
	subb	a,r6
	jnc	00132$
;	..\src\COMMON\easyax5043.c:1806: if (axradio_phy_chanpllrng[i] & 0x20)
	mov	a,r7
	add	a,#_axradio_phy_chanpllrng
	mov	dpl,a
	clr	a
	addc	a,#(_axradio_phy_chanpllrng >> 8)
	mov	dph,a
	movx	a,@dptr
	mov	r6,a
	jnb	acc.5,00142$
;	..\src\COMMON\easyax5043.c:1807: return AXRADIO_ERR_RANGING;
	mov	dpl,#0x06
	ret
00142$:
;	..\src\COMMON\easyax5043.c:1805: for (i = 0; i < axradio_phy_nrchannels; ++i)
	inc	r7
	sjmp	00141$
00132$:
;	..\src\COMMON\easyax5043.c:1808: return AXRADIO_ERR_NOERROR;
	mov	dpl,#0x00
;	..\src\COMMON\easyax5043.c:1809: }
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'axradio_cansleep'
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:1811: __reentrantb uint8_t axradio_cansleep(void) __reentrant
;	-----------------------------------------
;	 function axradio_cansleep
;	-----------------------------------------
_axradio_cansleep:
;	..\src\COMMON\easyax5043.c:1813: if (axradio_trxstate == trxstate_off || axradio_trxstate == trxstate_rxwor)
	mov	a,_axradio_trxstate
	jz	00101$
	mov	a,#0x02
	cjne	a,_axradio_trxstate,00102$
00101$:
;	..\src\COMMON\easyax5043.c:1814: return 1;
	mov	dpl,#0x01
	ret
00102$:
;	..\src\COMMON\easyax5043.c:1815: return 0;
	mov	dpl,#0x00
;	..\src\COMMON\easyax5043.c:1816: }
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'axradio_set_mode'
;------------------------------------------------------------
;r                         Allocated to registers r6 
;r                         Allocated to registers 
;iesave                    Allocated to registers r7 
;mode                      Allocated with name '_axradio_set_mode_mode_65536_436'
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:1819: uint8_t axradio_set_mode(uint8_t mode)
;	-----------------------------------------
;	 function axradio_set_mode
;	-----------------------------------------
_axradio_set_mode:
	mov	a,dpl
	mov	dptr,#_axradio_set_mode_mode_65536_436
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1821: if (mode == axradio_mode)
	movx	a,@dptr
	cjne	a,_axradio_mode,00102$
;	..\src\COMMON\easyax5043.c:1822: return AXRADIO_ERR_NOERROR;
	mov	dpl,#0x00
	ret
00102$:
;	..\src\COMMON\easyax5043.c:1823: switch (axradio_mode) {
	mov	r7,_axradio_mode
	cjne	r7,#0x00,00309$
	sjmp	00103$
00309$:
	cjne	r7,#0x02,00310$
	sjmp	00106$
00310$:
	cjne	r7,#0x03,00311$
	sjmp	00114$
00311$:
	cjne	r7,#0x18,00312$
	sjmp	00114$
00312$:
	cjne	r7,#0x19,00313$
	sjmp	00114$
00313$:
	cjne	r7,#0x1a,00314$
	sjmp	00114$
00314$:
	cjne	r7,#0x1b,00315$
	sjmp	00114$
00315$:
	cjne	r7,#0x1c,00316$
	sjmp	00114$
00316$:
	cjne	r7,#0x28,00317$
	ljmp	00122$
00317$:
	cjne	r7,#0x29,00318$
	ljmp	00122$
00318$:
	cjne	r7,#0x2a,00319$
	ljmp	00122$
00319$:
	cjne	r7,#0x2b,00320$
	ljmp	00122$
00320$:
	cjne	r7,#0x2c,00321$
	sjmp	00122$
00321$:
	cjne	r7,#0x2d,00322$
	sjmp	00122$
00322$:
;	..\src\COMMON\easyax5043.c:1824: case AXRADIO_MODE_UNINIT:
	sjmp	00123$
00103$:
;	..\src\COMMON\easyax5043.c:1826: uint8_t __autodata r = axradio_init();
	lcall	_axradio_init
;	..\src\COMMON\easyax5043.c:1827: if (r != AXRADIO_ERR_NOERROR)
	mov	a,dpl
	mov	r7,a
	mov	r6,a
	jz	00124$
;	..\src\COMMON\easyax5043.c:1828: return r;
	mov	dpl,r6
	ret
;	..\src\COMMON\easyax5043.c:1832: case AXRADIO_MODE_DEEPSLEEP:
00106$:
;	..\src\COMMON\easyax5043.c:1834: uint8_t __autodata r = ax5043_wakeup_deepsleep();
	lcall	_ax5043_wakeup_deepsleep
	mov	a,dpl
;	..\src\COMMON\easyax5043.c:1835: if (r)
	jz	00108$
;	..\src\COMMON\easyax5043.c:1836: return AXRADIO_ERR_NOCHIP;
	mov	dpl,#0x05
	ret
00108$:
;	..\src\COMMON\easyax5043.c:1837: ax5043_init_registers();
	lcall	_ax5043_init_registers
;	..\src\COMMON\easyax5043.c:1841: axradio_trxstate = trxstate_off;
	mov	_axradio_trxstate,#0x00
;	..\src\COMMON\easyax5043.c:1842: axradio_mode = AXRADIO_MODE_OFF;
	mov	_axradio_mode,#0x01
;	..\src\COMMON\easyax5043.c:1843: break;
;	..\src\COMMON\easyax5043.c:1851: case AXRADIO_MODE_CW_TRANSMIT:
	sjmp	00124$
00114$:
;	..\src\COMMON\easyax5043.c:1853: uint8_t __autodata iesave = IE & 0x80;
	mov	a,_IE
	anl	a,#0x80
	mov	r7,a
;	..\src\COMMON\easyax5043.c:1854: EA = 0;
;	assignBit
	clr	_EA
;	..\src\COMMON\easyax5043.c:1855: if (axradio_trxstate == trxstate_off) {
	mov	a,_axradio_trxstate
	jnz	00116$
;	..\src\COMMON\easyax5043.c:1856: update_timeanchor();
	push	ar7
	lcall	_update_timeanchor
;	..\src\COMMON\easyax5043.c:1857: wtimer_remove_callback(&axradio_cb_transmitend.cb);
	mov	dptr,#_axradio_cb_transmitend
	lcall	_wtimer_remove_callback
;	..\src\COMMON\easyax5043.c:1858: axradio_cb_transmitend.st.error = AXRADIO_ERR_NOERROR;
	mov	dptr,#(_axradio_cb_transmitend + 0x0005)
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1859: axradio_cb_transmitend.st.time.t = axradio_timeanchor.radiotimer;
	mov	dptr,#(_axradio_timeanchor + 0x0004)
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	mov	dptr,#(_axradio_cb_transmitend + 0x0006)
	mov	a,r3
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1860: wtimer_add_callback(&axradio_cb_transmitend.cb);
	mov	dptr,#_axradio_cb_transmitend
	lcall	_wtimer_add_callback
	pop	ar7
00116$:
;	..\src\COMMON\easyax5043.c:1862: ax5043_off();
	push	ar7
	lcall	_ax5043_off
	pop	ar7
;	..\src\COMMON\easyax5043.c:1863: IE |= iesave;
	mov	a,r7
	orl	_IE,a
;	..\src\COMMON\easyax5043.c:1865: ax5043_init_registers();
	lcall	_ax5043_init_registers
;	..\src\COMMON\easyax5043.c:1866: axradio_mode = AXRADIO_MODE_OFF;
	mov	_axradio_mode,#0x01
;	..\src\COMMON\easyax5043.c:1867: break;
;	..\src\COMMON\easyax5043.c:1875: case AXRADIO_MODE_STREAM_RECEIVE_DATAPIN:
	sjmp	00124$
00122$:
;	..\src\COMMON\easyax5043.c:1876: ax5043_off();
	lcall	_ax5043_off
;	..\src\COMMON\easyax5043.c:1877: ax5043_init_registers();
	lcall	_ax5043_init_registers
;	..\src\COMMON\easyax5043.c:1878: axradio_mode = AXRADIO_MODE_OFF;
	mov	_axradio_mode,#0x01
;	..\src\COMMON\easyax5043.c:1880: default:
00123$:
;	..\src\COMMON\easyax5043.c:1881: ax5043_off();
	lcall	_ax5043_off
;	..\src\COMMON\easyax5043.c:1882: axradio_mode = AXRADIO_MODE_OFF;
	mov	_axradio_mode,#0x01
;	..\src\COMMON\easyax5043.c:1884: }
00124$:
;	..\src\COMMON\easyax5043.c:1885: axradio_killallcb();
	lcall	_axradio_killallcb
;	..\src\COMMON\easyax5043.c:1886: if (mode == AXRADIO_MODE_UNINIT)
	mov	dptr,#_axradio_set_mode_mode_65536_436
	movx	a,@dptr
	mov	r7,a
	jnz	00126$
;	..\src\COMMON\easyax5043.c:1887: return AXRADIO_ERR_NOTSUPPORTED;
	mov	dpl,#0x01
	ret
00126$:
;	..\src\COMMON\easyax5043.c:1888: axradio_syncstate = syncstate_off;
	mov	dptr,#_axradio_syncstate
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1889: switch (mode) {
	mov	a,r7
	mov	r6,a
	add	a,#0xff - 0x33
	jnc	00327$
	ljmp	00179$
00327$:
	mov	a,r6
	add	a,#(00328$-3-.)
	movc	a,@a+pc
	mov	dpl,a
	mov	a,r6
	add	a,#(00329$-3-.)
	movc	a,@a+pc
	mov	dph,a
	clr	a
	jmp	@a+dptr
00328$:
	.db	00179$
	.db	00127$
	.db	00128$
	.db	00174$
	.db	00179$
	.db	00179$
	.db	00179$
	.db	00179$
	.db	00179$
	.db	00179$
	.db	00179$
	.db	00179$
	.db	00179$
	.db	00179$
	.db	00179$
	.db	00179$
	.db	00130$
	.db	00132$
	.db	00130$
	.db	00132$
	.db	00179$
	.db	00179$
	.db	00179$
	.db	00179$
	.db	00144$
	.db	00144$
	.db	00144$
	.db	00144$
	.db	00144$
	.db	00179$
	.db	00179$
	.db	00179$
	.db	00134$
	.db	00139$
	.db	00134$
	.db	00139$
	.db	00179$
	.db	00179$
	.db	00179$
	.db	00179$
	.db	00162$
	.db	00162$
	.db	00162$
	.db	00162$
	.db	00162$
	.db	00162$
	.db	00179$
	.db	00179$
	.db	00176$
	.db	00176$
	.db	00178$
	.db	00178$
00329$:
	.db	00179$>>8
	.db	00127$>>8
	.db	00128$>>8
	.db	00174$>>8
	.db	00179$>>8
	.db	00179$>>8
	.db	00179$>>8
	.db	00179$>>8
	.db	00179$>>8
	.db	00179$>>8
	.db	00179$>>8
	.db	00179$>>8
	.db	00179$>>8
	.db	00179$>>8
	.db	00179$>>8
	.db	00179$>>8
	.db	00130$>>8
	.db	00132$>>8
	.db	00130$>>8
	.db	00132$>>8
	.db	00179$>>8
	.db	00179$>>8
	.db	00179$>>8
	.db	00179$>>8
	.db	00144$>>8
	.db	00144$>>8
	.db	00144$>>8
	.db	00144$>>8
	.db	00144$>>8
	.db	00179$>>8
	.db	00179$>>8
	.db	00179$>>8
	.db	00134$>>8
	.db	00139$>>8
	.db	00134$>>8
	.db	00139$>>8
	.db	00179$>>8
	.db	00179$>>8
	.db	00179$>>8
	.db	00179$>>8
	.db	00162$>>8
	.db	00162$>>8
	.db	00162$>>8
	.db	00162$>>8
	.db	00162$>>8
	.db	00162$>>8
	.db	00179$>>8
	.db	00179$>>8
	.db	00176$>>8
	.db	00176$>>8
	.db	00178$>>8
	.db	00178$>>8
;	..\src\COMMON\easyax5043.c:1890: case AXRADIO_MODE_OFF:
00127$:
;	..\src\COMMON\easyax5043.c:1891: return AXRADIO_ERR_NOERROR;
	mov	dpl,#0x00
	ret
;	..\src\COMMON\easyax5043.c:1893: case AXRADIO_MODE_DEEPSLEEP:
00128$:
;	..\src\COMMON\easyax5043.c:1894: ax5043_enter_deepsleep();
	lcall	_ax5043_enter_deepsleep
;	..\src\COMMON\easyax5043.c:1895: axradio_mode = AXRADIO_MODE_DEEPSLEEP;
	mov	_axradio_mode,#0x02
;	..\src\COMMON\easyax5043.c:1896: return AXRADIO_ERR_NOERROR;
	mov	dpl,#0x00
	ret
;	..\src\COMMON\easyax5043.c:1899: case AXRADIO_MODE_ACK_TRANSMIT:
00130$:
;	..\src\COMMON\easyax5043.c:1900: axradio_mode = mode;
	mov	_axradio_mode,r7
;	..\src\COMMON\easyax5043.c:1901: axradio_ack_seqnr = 0xff;
	mov	dptr,#_axradio_ack_seqnr
	mov	a,#0xff
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1902: ax5043_init_registers_tx();
	lcall	_ax5043_init_registers_tx
;	..\src\COMMON\easyax5043.c:1903: return AXRADIO_ERR_NOERROR;
	mov	dpl,#0x00
	ret
;	..\src\COMMON\easyax5043.c:1906: case AXRADIO_MODE_WOR_ACK_TRANSMIT:
00132$:
;	..\src\COMMON\easyax5043.c:1907: axradio_mode = mode;
	mov	_axradio_mode,r7
;	..\src\COMMON\easyax5043.c:1908: axradio_ack_seqnr = 0xff;
	mov	dptr,#_axradio_ack_seqnr
	mov	a,#0xff
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1909: ax5043_init_registers_tx();
	lcall	_ax5043_init_registers_tx
;	..\src\COMMON\easyax5043.c:1910: return AXRADIO_ERR_NOERROR;
	mov	dpl,#0x00
	ret
;	..\src\COMMON\easyax5043.c:1913: case AXRADIO_MODE_ACK_RECEIVE:
00134$:
;	..\src\COMMON\easyax5043.c:1914: axradio_mode = mode;
	mov	_axradio_mode,r7
;	..\src\COMMON\easyax5043.c:1915: axradio_ack_seqnr = 0xff;
	mov	dptr,#_axradio_ack_seqnr
	mov	a,#0xff
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1916: ax5043_init_registers_rx();
	lcall	_ax5043_init_registers_rx
;	..\src\COMMON\easyax5043.c:1917: ax5043_receiver_on_continuous();
	lcall	_ax5043_receiver_on_continuous
;	..\src\COMMON\easyax5043.c:1918: enablecs:
00135$:
;	..\src\COMMON\easyax5043.c:1919: if (axradio_phy_cs_enabled) {
	mov	dptr,#_axradio_phy_cs_enabled
	clr	a
	movc	a,@a+dptr
	jz	00137$
;	..\src\COMMON\easyax5043.c:1920: wtimer_remove(&axradio_timer);
	mov	dptr,#_axradio_timer
	lcall	_wtimer_remove
;	..\src\COMMON\easyax5043.c:1921: axradio_timer.time = axradio_phy_cs_period;
	mov	dptr,#_axradio_phy_cs_period
	clr	a
	movc	a,@a+dptr
	mov	r5,a
	mov	a,#0x01
	movc	a,@a+dptr
	mov	r6,a
	mov	r4,#0x00
	mov	r3,#0x00
	mov	dptr,#(_axradio_timer + 0x0004)
	mov	a,r5
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
	mov	a,r3
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1922: wtimer0_addrelative(&axradio_timer);
	mov	dptr,#_axradio_timer
	lcall	_wtimer0_addrelative
00137$:
;	..\src\COMMON\easyax5043.c:1924: return AXRADIO_ERR_NOERROR;
	mov	dpl,#0x00
	ret
;	..\src\COMMON\easyax5043.c:1927: case AXRADIO_MODE_WOR_ACK_RECEIVE:
00139$:
;	..\src\COMMON\easyax5043.c:1928: axradio_ack_seqnr = 0xff;
	mov	dptr,#_axradio_ack_seqnr
	mov	a,#0xff
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1929: axradio_mode = mode;
	mov	_axradio_mode,r7
;	..\src\COMMON\easyax5043.c:1930: ax5043_init_registers_rx();
	lcall	_ax5043_init_registers_rx
;	..\src\COMMON\easyax5043.c:1931: ax5043_receiver_on_wor();
	lcall	_ax5043_receiver_on_wor
;	..\src\COMMON\easyax5043.c:1932: return AXRADIO_ERR_NOERROR;
	mov	dpl,#0x00
	ret
;	..\src\COMMON\easyax5043.c:1938: case AXRADIO_MODE_STREAM_TRANSMIT_SCRAM_LSB:
00144$:
;	..\src\COMMON\easyax5043.c:1939: axradio_mode = mode;
	mov	_axradio_mode,r7
;	..\src\COMMON\easyax5043.c:1940: if (axradio_mode == AXRADIO_MODE_STREAM_TRANSMIT_UNENC ||
	mov	a,#0x18
	cjne	a,_axradio_mode,00331$
	sjmp	00145$
00331$:
;	..\src\COMMON\easyax5043.c:1941: axradio_mode == AXRADIO_MODE_STREAM_TRANSMIT_UNENC_LSB)
	mov	a,#0x1a
	cjne	a,_axradio_mode,00146$
00145$:
;	..\src\COMMON\easyax5043.c:1942: AX5043_ENCODING = 0;
	mov	dptr,#_AX5043_ENCODING
	clr	a
	movx	@dptr,a
00146$:
;	..\src\COMMON\easyax5043.c:1943: if (axradio_mode == AXRADIO_MODE_STREAM_TRANSMIT_SCRAM ||
	mov	a,#0x19
	cjne	a,_axradio_mode,00334$
	sjmp	00148$
00334$:
;	..\src\COMMON\easyax5043.c:1944: axradio_mode == AXRADIO_MODE_STREAM_TRANSMIT_SCRAM_LSB)
	mov	a,#0x1b
	cjne	a,_axradio_mode,00149$
00148$:
;	..\src\COMMON\easyax5043.c:1945: AX5043_ENCODING = 4;
	mov	dptr,#_AX5043_ENCODING
	mov	a,#0x04
	movx	@dptr,a
00149$:
;	..\src\COMMON\easyax5043.c:1946: if (axradio_mode == AXRADIO_MODE_STREAM_TRANSMIT_UNENC_LSB ||
	mov	a,#0x1a
	cjne	a,_axradio_mode,00337$
	sjmp	00151$
00337$:
;	..\src\COMMON\easyax5043.c:1947: axradio_mode == AXRADIO_MODE_STREAM_TRANSMIT_SCRAM_LSB)
	mov	a,#0x1b
	cjne	a,_axradio_mode,00152$
00151$:
;	..\src\COMMON\easyax5043.c:1948: AX5043_PKTADDRCFG &= 0x7F;
	mov	dptr,#_AX5043_PKTADDRCFG
	movx	a,@dptr
	anl	acc,#0x7f
	movx	@dptr,a
00152$:
;	..\src\COMMON\easyax5043.c:1949: ax5043_init_registers_tx();
	lcall	_ax5043_init_registers_tx
;	..\src\COMMON\easyax5043.c:1950: AX5043_FRAMING = 0;
	mov	dptr,#_AX5043_FRAMING
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1951: ax5043_prepare_tx();
	lcall	_ax5043_prepare_tx
;	..\src\COMMON\easyax5043.c:1952: axradio_trxstate = trxstate_txstream_xtalwait;
	mov	_axradio_trxstate,#0x0f
;	..\src\COMMON\easyax5043.c:1953: while (!(AX5043_POWSTAT & 0x08)) {}; // wait for modem vdd so writing the FIFO is safe
00154$:
	mov	dptr,#_AX5043_POWSTAT
	movx	a,@dptr
	jnb	acc.3,00154$
;	..\src\COMMON\easyax5043.c:1954: AX5043_FIFOSTAT = 3; // clear FIFO data & flags (prevent transmitting anything left over in the FIFO, this has no effect if the FIFO is not powerered, in this case it is reset any way)
	mov	dptr,#_AX5043_FIFOSTAT
	mov	a,#0x03
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1955: AX5043_RADIOEVENTREQ0; // make sure REVRDONE bit is cleared, so it is a reliable indicator that the packet is out
	mov	dptr,#_AX5043_RADIOEVENTREQ0
	movx	a,@dptr
;	..\src\COMMON\easyax5043.c:1956: update_timeanchor();
	lcall	_update_timeanchor
;	..\src\COMMON\easyax5043.c:1957: wtimer_remove_callback(&axradio_cb_transmitdata.cb);
	mov	dptr,#_axradio_cb_transmitdata
	lcall	_wtimer_remove_callback
;	..\src\COMMON\easyax5043.c:1958: axradio_cb_transmitdata.st.error = AXRADIO_ERR_NOERROR;
	mov	dptr,#(_axradio_cb_transmitdata + 0x0005)
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1959: axradio_cb_transmitdata.st.time.t = axradio_timeanchor.radiotimer;
	mov	dptr,#(_axradio_timeanchor + 0x0004)
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	mov	dptr,#(_axradio_cb_transmitdata + 0x0006)
	mov	a,r3
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1960: wtimer_add_callback(&axradio_cb_transmitdata.cb);
	mov	dptr,#_axradio_cb_transmitdata
	lcall	_wtimer_add_callback
;	..\src\COMMON\easyax5043.c:1961: return AXRADIO_ERR_NOERROR;
	mov	dpl,#0x00
	ret
;	..\src\COMMON\easyax5043.c:1968: case AXRADIO_MODE_STREAM_RECEIVE_DATAPIN:
00162$:
;	..\src\COMMON\easyax5043.c:1969: axradio_mode = mode;
	mov	_axradio_mode,r7
;	..\src\COMMON\easyax5043.c:1970: ax5043_init_registers_rx();
	lcall	_ax5043_init_registers_rx
;	..\src\COMMON\easyax5043.c:1971: if (axradio_mode == AXRADIO_MODE_STREAM_RECEIVE_UNENC ||
	mov	a,#0x28
	cjne	a,_axradio_mode,00341$
	sjmp	00163$
00341$:
;	..\src\COMMON\easyax5043.c:1972: axradio_mode == AXRADIO_MODE_STREAM_RECEIVE_UNENC_LSB)
	mov	a,#0x2a
	cjne	a,_axradio_mode,00164$
00163$:
;	..\src\COMMON\easyax5043.c:1973: AX5043_ENCODING = 0;
	mov	dptr,#_AX5043_ENCODING
	clr	a
	movx	@dptr,a
00164$:
;	..\src\COMMON\easyax5043.c:1974: if (axradio_mode == AXRADIO_MODE_STREAM_RECEIVE_SCRAM ||
	mov	a,#0x29
	cjne	a,_axradio_mode,00344$
	sjmp	00166$
00344$:
;	..\src\COMMON\easyax5043.c:1975: axradio_mode == AXRADIO_MODE_STREAM_RECEIVE_SCRAM_LSB)
	mov	a,#0x2b
	cjne	a,_axradio_mode,00167$
00166$:
;	..\src\COMMON\easyax5043.c:1976: AX5043_ENCODING = 4;
	mov	dptr,#_AX5043_ENCODING
	mov	a,#0x04
	movx	@dptr,a
00167$:
;	..\src\COMMON\easyax5043.c:1977: if (axradio_mode == AXRADIO_MODE_STREAM_RECEIVE_UNENC_LSB ||
	mov	a,#0x2a
	cjne	a,_axradio_mode,00347$
	sjmp	00169$
00347$:
;	..\src\COMMON\easyax5043.c:1978: axradio_mode == AXRADIO_MODE_STREAM_RECEIVE_SCRAM_LSB)
	mov	a,#0x2b
	cjne	a,_axradio_mode,00170$
00169$:
;	..\src\COMMON\easyax5043.c:1979: AX5043_PKTADDRCFG &= 0x7F;
	mov	dptr,#_AX5043_PKTADDRCFG
	movx	a,@dptr
	anl	acc,#0x7f
	movx	@dptr,a
00170$:
;	..\src\COMMON\easyax5043.c:1980: AX5043_FRAMING = 0;
	mov	dptr,#_AX5043_FRAMING
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1981: AX5043_PKTCHUNKSIZE = 8; // 64 byte
	mov	dptr,#_AX5043_PKTCHUNKSIZE
	mov	a,#0x08
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1982: AX5043_RXPARAMSETS = 0x00;
	mov	dptr,#_AX5043_RXPARAMSETS
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1983: if( axradio_mode == AXRADIO_MODE_STREAM_RECEIVE_DATAPIN )
	mov	a,#0x2d
	cjne	a,_axradio_mode,00173$
;	..\src\COMMON\easyax5043.c:1985: ax5043_set_registers_rxcont_singleparamset();
	lcall	_ax5043_set_registers_rxcont_singleparamset
;	..\src\COMMON\easyax5043.c:1986: AX5043_PINFUNCDATA = 0x04;
	mov	dptr,#_AX5043_PINFUNCDATA
	mov	a,#0x04
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1987: AX5043_PINFUNCDCLK = 0x04;
	mov	dptr,#_AX5043_PINFUNCDCLK
	movx	@dptr,a
00173$:
;	..\src\COMMON\easyax5043.c:1989: ax5043_receiver_on_continuous();
	lcall	_ax5043_receiver_on_continuous
;	..\src\COMMON\easyax5043.c:1990: goto enablecs;
	ljmp	00135$
;	..\src\COMMON\easyax5043.c:1992: case AXRADIO_MODE_CW_TRANSMIT:
00174$:
;	..\src\COMMON\easyax5043.c:1993: axradio_mode = AXRADIO_MODE_CW_TRANSMIT;
	mov	_axradio_mode,#0x03
;	..\src\COMMON\easyax5043.c:1994: ax5043_init_registers_tx();
	lcall	_ax5043_init_registers_tx
;	..\src\COMMON\easyax5043.c:1995: AX5043_MODULATION = 8;   // Set an FSK mode
	mov	dptr,#_AX5043_MODULATION
	mov	a,#0x08
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1996: AX5043_FSKDEV2 = 0x00;
	mov	dptr,#_AX5043_FSKDEV2
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1997: AX5043_FSKDEV1 = 0x00;
	mov	dptr,#_AX5043_FSKDEV1
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1998: AX5043_FSKDEV0 = 0x00;
	mov	dptr,#_AX5043_FSKDEV0
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:1999: AX5043_TXRATE2 = 0x00;
	mov	dptr,#_AX5043_TXRATE2
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2000: AX5043_TXRATE1 = 0x00;
	mov	dptr,#_AX5043_TXRATE1
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2001: AX5043_TXRATE0 = 0x01;
	mov	dptr,#_AX5043_TXRATE0
	inc	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2002: AX5043_PINFUNCDATA = 0x04;
	mov	dptr,#_AX5043_PINFUNCDATA
	mov	a,#0x04
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2003: AX5043_PWRMODE = AX5043_PWRSTATE_FIFO_ON;
	mov	dptr,#_AX5043_PWRMODE
	mov	a,#0x07
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2004: axradio_trxstate = trxstate_txcw_xtalwait;
	mov	_axradio_trxstate,#0x0e
;	..\src\COMMON\easyax5043.c:2005: AX5043_IRQMASK0 = 0x00;
	mov	dptr,#_AX5043_IRQMASK0
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2006: AX5043_IRQMASK1 = 0x01; // enable xtal ready interrupt
	mov	dptr,#_AX5043_IRQMASK1
	inc	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2007: return AXRADIO_ERR_NOERROR;
	mov	dpl,#0x00
	ret
;	..\src\COMMON\easyax5043.c:2010: case AXRADIO_MODE_SYNC_ACK_MASTER:
00176$:
;	..\src\COMMON\easyax5043.c:2011: axradio_mode = mode;
	mov	_axradio_mode,r7
;	..\src\COMMON\easyax5043.c:2012: axradio_syncstate = syncstate_master_normal;
	mov	dptr,#_axradio_syncstate
	mov	a,#0x03
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2014: wtimer_remove(&axradio_timer);
	mov	dptr,#_axradio_timer
	lcall	_wtimer_remove
;	..\src\COMMON\easyax5043.c:2015: axradio_timer.time = 2;
	mov	dptr,#(_axradio_timer + 0x0004)
	mov	a,#0x02
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2016: wtimer0_addrelative(&axradio_timer);
	mov	dptr,#_axradio_timer
	lcall	_wtimer0_addrelative
;	..\src\COMMON\easyax5043.c:2017: axradio_sync_time = axradio_timer.time;
	mov	dptr,#(_axradio_timer + 0x0004)
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	mov	dptr,#_axradio_sync_time
	mov	a,r3
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2018: axradio_sync_addtime(axradio_sync_xoscstartup);
	mov	dptr,#_axradio_sync_xoscstartup
	clr	a
	movc	a,@a+dptr
	mov	r3,a
	mov	a,#0x01
	movc	a,@a+dptr
	mov	r4,a
	mov	a,#0x02
	movc	a,@a+dptr
	mov	r5,a
	mov	a,#0x03
	movc	a,@a+dptr
	mov	dpl,r3
	mov	dph,r4
	mov	b,r5
	lcall	_axradio_sync_addtime
;	..\src\COMMON\easyax5043.c:2019: return AXRADIO_ERR_NOERROR;
	mov	dpl,#0x00
	ret
;	..\src\COMMON\easyax5043.c:2022: case AXRADIO_MODE_SYNC_ACK_SLAVE:
00178$:
;	..\src\COMMON\easyax5043.c:2023: axradio_mode = mode;
	mov	_axradio_mode,r7
;	..\src\COMMON\easyax5043.c:2024: ax5043_init_registers_rx();
	lcall	_ax5043_init_registers_rx
;	..\src\COMMON\easyax5043.c:2025: ax5043_receiver_on_continuous();
	lcall	_ax5043_receiver_on_continuous
;	..\src\COMMON\easyax5043.c:2026: axradio_syncstate = syncstate_slave_synchunt;
	mov	dptr,#_axradio_syncstate
	mov	a,#0x06
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2027: wtimer_remove(&axradio_timer);
	mov	dptr,#_axradio_timer
	lcall	_wtimer_remove
;	..\src\COMMON\easyax5043.c:2028: axradio_timer.time = axradio_sync_slave_initialsyncwindow;
	mov	dptr,#_axradio_sync_slave_initialsyncwindow
	clr	a
	movc	a,@a+dptr
	mov	r4,a
	mov	a,#0x01
	movc	a,@a+dptr
	mov	r5,a
	mov	a,#0x02
	movc	a,@a+dptr
	mov	r6,a
	mov	a,#0x03
	movc	a,@a+dptr
	mov	r7,a
	mov	dptr,#(_axradio_timer + 0x0004)
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2029: wtimer0_addrelative(&axradio_timer);
	mov	dptr,#_axradio_timer
	lcall	_wtimer0_addrelative
;	..\src\COMMON\easyax5043.c:2030: axradio_sync_time = axradio_timer.time;
	mov	dptr,#(_axradio_timer + 0x0004)
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#_axradio_sync_time
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2031: wtimer_remove_callback(&axradio_cb_receive.cb);
	mov	dptr,#_axradio_cb_receive
	lcall	_wtimer_remove_callback
;	..\src\COMMON\easyax5043.c:2032: memset_xdata(&axradio_cb_receive.st, 0, sizeof(axradio_cb_receive.st));
	mov	dptr,#_memset_PARM_2
	clr	a
	movx	@dptr,a
	mov	dptr,#_memset_PARM_3
	mov	a,#0x1e
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
	mov	dptr,#(_axradio_cb_receive + 0x0004)
	mov	b,#0x00
	lcall	_memset
;	..\src\COMMON\easyax5043.c:2033: axradio_cb_receive.st.time.t = axradio_timeanchor.radiotimer;
	mov	dptr,#(_axradio_timeanchor + 0x0004)
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#(_axradio_cb_receive + 0x0006)
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2034: axradio_cb_receive.st.error = AXRADIO_ERR_RESYNC;
	mov	dptr,#(_axradio_cb_receive + 0x0005)
	mov	a,#0x09
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2035: wtimer_add_callback(&axradio_cb_receive.cb);
	mov	dptr,#_axradio_cb_receive
	lcall	_wtimer_add_callback
;	..\src\COMMON\easyax5043.c:2036: return AXRADIO_ERR_NOERROR;
	mov	dpl,#0x00
;	..\src\COMMON\easyax5043.c:2038: default:
	ret
00179$:
;	..\src\COMMON\easyax5043.c:2039: return AXRADIO_ERR_NOTSUPPORTED;
	mov	dpl,#0x01
;	..\src\COMMON\easyax5043.c:2040: }
;	..\src\COMMON\easyax5043.c:2041: }
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'axradio_set_channel'
;------------------------------------------------------------
;freq                      Allocated with name '_axradio_set_channel_freq_65536_447'
;rng                       Allocated to registers r7 
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:2043: uint8_t axradio_set_channel(uint32_t freq)
;	-----------------------------------------
;	 function axradio_set_channel
;	-----------------------------------------
_axradio_set_channel:
	mov	r7,dpl
	mov	r6,dph
	mov	r5,b
	mov	r4,a
	mov	dptr,#_axradio_set_channel_freq_65536_447
	mov	a,r7
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2049: EA = 0;
;	assignBit
	clr	_EA
;	..\src\COMMON\easyax5043.c:2052: rng = axradio_phy_chanpllrng[0]; // volver a poner si falla
	mov	dptr,#_axradio_phy_chanpllrng
	movx	a,@dptr
	mov	r7,a
;	..\src\COMMON\easyax5043.c:2058: if (AX5043_PLLLOOP & 0x80) {
	mov	dptr,#_AX5043_PLLLOOP
	movx	a,@dptr
	jnb	acc.7,00102$
;	..\src\COMMON\easyax5043.c:2059: AX5043_PLLRANGINGA = rng & 0x0F;// volver a poner si falla
	mov	dptr,#_AX5043_PLLRANGINGA
	mov	a,#0x0f
	anl	a,r7
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2060: AX5043_FREQA0 = freq;
	mov	dptr,#_axradio_set_channel_freq_65536_447
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	mov	dptr,#_AX5043_FREQA0
	mov	a,r3
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2061: AX5043_FREQA1 = freq >> 8;
	mov	dptr,#_AX5043_FREQA1
	mov	a,r4
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2062: AX5043_FREQA2 = freq >> 16;
	mov	dptr,#_AX5043_FREQA2
	mov	a,r5
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2063: AX5043_FREQA3 = freq >> 24;
	mov	dptr,#_AX5043_FREQA3
	mov	a,r6
	movx	@dptr,a
	sjmp	00103$
00102$:
;	..\src\COMMON\easyax5043.c:2065: AX5043_PLLRANGINGB = rng & 0x0F; //volver a poner si falla
	mov	dptr,#_AX5043_PLLRANGINGB
	mov	a,#0x0f
	anl	a,r7
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2066: AX5043_FREQB0 = freq;
	mov	dptr,#_axradio_set_channel_freq_65536_447
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#_AX5043_FREQB0
	mov	a,r4
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2067: AX5043_FREQB1 = freq >> 8;
	mov	dptr,#_AX5043_FREQB1
	mov	a,r5
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2068: AX5043_FREQB2 = freq >> 16;
	mov	dptr,#_AX5043_FREQB2
	mov	a,r6
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2069: AX5043_FREQB3 = freq >> 24;
	mov	dptr,#_AX5043_FREQB3
	mov	a,r7
	movx	@dptr,a
00103$:
;	..\src\COMMON\easyax5043.c:2072: AX5043_PLLLOOP ^= 0x80;  // volver a poner si falla
	mov	dptr,#_AX5043_PLLLOOP
	movx	a,@dptr
	mov	r7,a
	xrl	ar7,#0x80
	mov	dptr,#_AX5043_PLLLOOP
	mov	a,r7
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2073: EA = 1;
;	assignBit
	setb	_EA
;	..\src\COMMON\easyax5043.c:2076: return AXRADIO_ERR_NOERROR;
	mov	dpl,#0x00
;	..\src\COMMON\easyax5043.c:2077: }
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'axradio_get_pllvcoi'
;------------------------------------------------------------
;x                         Allocated with name '_axradio_get_pllvcoi_x_131072_453'
;x                         Allocated with name '_axradio_get_pllvcoi_x_131072_454'
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:2079: uint8_t axradio_get_pllvcoi(void)
;	-----------------------------------------
;	 function axradio_get_pllvcoi
;	-----------------------------------------
_axradio_get_pllvcoi:
;	..\src\COMMON\easyax5043.c:2081: if (axradio_phy_vcocalib) {
	mov	dptr,#_axradio_phy_vcocalib
	clr	a
	movc	a,@a+dptr
	jz	00104$
;	..\src\COMMON\easyax5043.c:2082: uint8_t x = axradio_phy_chanvcoi[axradio_curchannel];
	mov	dptr,#_axradio_curchannel
	movx	a,@dptr
	add	a,#_axradio_phy_chanvcoi
	mov	dpl,a
	clr	a
	addc	a,#(_axradio_phy_chanvcoi >> 8)
	mov	dph,a
	movx	a,@dptr
;	..\src\COMMON\easyax5043.c:2083: if (x & 0x80)
	mov	r7,a
	jnb	acc.7,00104$
;	..\src\COMMON\easyax5043.c:2084: return x;
	mov	dpl,r7
	ret
00104$:
;	..\src\COMMON\easyax5043.c:2087: uint8_t x = axradio_phy_chanvcoiinit[axradio_curchannel];
	mov	dptr,#_axradio_curchannel
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#_axradio_phy_chanvcoiinit
	movc	a,@a+dptr
	mov	r6,a
	mov	dptr,#_axradio_get_pllvcoi_x_131072_454
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2088: if (x & 0x80) {
	mov	a,r6
	jnb	acc.7,00108$
;	..\src\COMMON\easyax5043.c:2089: if (!(axradio_phy_chanpllrnginit[0] & 0xF0)) {
	mov	dptr,#_axradio_phy_chanpllrnginit
	clr	a
	movc	a,@a+dptr
	mov	r5,a
	anl	a,#0xf0
	jz	00131$
	sjmp	00106$
00131$:
;	..\src\COMMON\easyax5043.c:2090: x += (axradio_phy_chanpllrng[axradio_curchannel] & 0x0F) - (axradio_phy_chanpllrnginit[axradio_curchannel] & 0x0F);
	mov	a,r7
	add	a,#_axradio_phy_chanpllrng
	mov	dpl,a
	clr	a
	addc	a,#(_axradio_phy_chanpllrng >> 8)
	mov	dph,a
	movx	a,@dptr
	mov	r5,a
	anl	ar5,#0x0f
	mov	a,r7
	mov	dptr,#_axradio_phy_chanpllrnginit
	movc	a,@a+dptr
	mov	r7,a
	mov	a,#0x0f
	anl	a,r7
	setb	c
	subb	a,r5
	cpl	a
	mov	r5,a
	add	a,r6
;	..\src\COMMON\easyax5043.c:2091: x &= 0x3f;
	anl	a,#0x3f
;	..\src\COMMON\easyax5043.c:2092: x |= 0x80;
	mov	dptr,#_axradio_get_pllvcoi_x_131072_454
	orl	a,#0x80
	movx	@dptr,a
00106$:
;	..\src\COMMON\easyax5043.c:2094: return x;
	mov	dptr,#_axradio_get_pllvcoi_x_131072_454
	movx	a,@dptr
	mov	dpl,a
	ret
00108$:
;	..\src\COMMON\easyax5043.c:2097: return AX5043_PLLVCOI;
	mov	dptr,#_AX5043_PLLVCOI
	movx	a,@dptr
;	..\src\COMMON\easyax5043.c:2098: }
	mov	dpl,a
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'axradio_set_curfreqoffset'
;------------------------------------------------------------
;offs                      Allocated with name '_axradio_set_curfreqoffset_offs_65536_457'
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:2100: static uint8_t axradio_set_curfreqoffset(int32_t offs)
;	-----------------------------------------
;	 function axradio_set_curfreqoffset
;	-----------------------------------------
_axradio_set_curfreqoffset:
	mov	r7,dpl
	mov	r6,dph
	mov	r5,b
	mov	r4,a
	mov	dptr,#_axradio_set_curfreqoffset_offs_65536_457
	mov	a,r7
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2102: axradio_curfreqoffset = offs;
	mov	dptr,#_axradio_set_curfreqoffset_offs_65536_457
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#_axradio_curfreqoffset
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2103: if (checksignedlimit32(offs, axradio_phy_maxfreqoffset))
	mov	dptr,#_axradio_phy_maxfreqoffset
	clr	a
	movc	a,@a+dptr
	push	acc
	mov	a,#0x01
	movc	a,@a+dptr
	push	acc
	mov	a,#0x02
	movc	a,@a+dptr
	push	acc
	mov	a,#0x03
	movc	a,@a+dptr
	push	acc
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	mov	a,r7
	lcall	_checksignedlimit32
	mov	r7,dpl
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	mov	a,r7
	jz	00102$
;	..\src\COMMON\easyax5043.c:2104: return AXRADIO_ERR_NOERROR;
	mov	dpl,#0x00
	ret
00102$:
;	..\src\COMMON\easyax5043.c:2105: if (axradio_curfreqoffset < 0)
	mov	dptr,#_axradio_curfreqoffset
	movx	a,@dptr
	inc	dptr
	movx	a,@dptr
	inc	dptr
	movx	a,@dptr
	inc	dptr
	movx	a,@dptr
	jnb	acc.7,00104$
;	..\src\COMMON\easyax5043.c:2106: axradio_curfreqoffset = -axradio_phy_maxfreqoffset;
	mov	dptr,#_axradio_phy_maxfreqoffset
	clr	a
	movc	a,@a+dptr
	mov	r4,a
	mov	a,#0x01
	movc	a,@a+dptr
	mov	r5,a
	mov	a,#0x02
	movc	a,@a+dptr
	mov	r6,a
	mov	a,#0x03
	movc	a,@a+dptr
	mov	r7,a
	mov	dptr,#_axradio_curfreqoffset
	clr	c
	clr	a
	subb	a,r4
	movx	@dptr,a
	clr	a
	subb	a,r5
	inc	dptr
	movx	@dptr,a
	clr	a
	subb	a,r6
	inc	dptr
	movx	@dptr,a
	clr	a
	subb	a,r7
	inc	dptr
	movx	@dptr,a
	sjmp	00105$
00104$:
;	..\src\COMMON\easyax5043.c:2108: axradio_curfreqoffset = axradio_phy_maxfreqoffset;
	mov	dptr,#_axradio_phy_maxfreqoffset
	clr	a
	movc	a,@a+dptr
	mov	r4,a
	mov	a,#0x01
	movc	a,@a+dptr
	mov	r5,a
	mov	a,#0x02
	movc	a,@a+dptr
	mov	r6,a
	mov	a,#0x03
	movc	a,@a+dptr
	mov	r7,a
	mov	dptr,#_axradio_curfreqoffset
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
00105$:
;	..\src\COMMON\easyax5043.c:2109: return AXRADIO_ERR_INVALID;
	mov	dpl,#0x04
;	..\src\COMMON\easyax5043.c:2110: }
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'axradio_set_local_address'
;------------------------------------------------------------
;addr                      Allocated with name '_axradio_set_local_address_addr_65536_459'
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:2115: void axradio_set_local_address(const struct axradio_address_mask __generic *addr)
;	-----------------------------------------
;	 function axradio_set_local_address
;	-----------------------------------------
_axradio_set_local_address:
	mov	r7,b
	mov	r6,dph
	mov	a,dpl
	mov	dptr,#_axradio_set_local_address_addr_65536_459
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2117: memcpy_xdatageneric(&axradio_localaddr, addr, sizeof(axradio_localaddr));
	mov	dptr,#_axradio_set_local_address_addr_65536_459
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#_memcpy_PARM_2
	mov	a,r5
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_memcpy_PARM_3
	mov	a,#0x08
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_axradio_localaddr
	mov	b,#0x00
	lcall	_memcpy
;	..\src\COMMON\easyax5043.c:2118: axradio_setaddrregs();
;	..\src\COMMON\easyax5043.c:2119: }
	ljmp	_axradio_setaddrregs
;------------------------------------------------------------
;Allocation info for local variables in function 'axradio_set_default_remote_address'
;------------------------------------------------------------
;addr                      Allocated with name '_axradio_set_default_remote_address_addr_65536_461'
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:2122: void axradio_set_default_remote_address(const struct axradio_address __generic *addr)
;	-----------------------------------------
;	 function axradio_set_default_remote_address
;	-----------------------------------------
_axradio_set_default_remote_address:
	mov	r7,b
	mov	r6,dph
	mov	a,dpl
	mov	dptr,#_axradio_set_default_remote_address_addr_65536_461
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2124: memcpy_xdatageneric(&axradio_default_remoteaddr, addr, sizeof(axradio_default_remoteaddr));
	mov	dptr,#_axradio_set_default_remote_address_addr_65536_461
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#_memcpy_PARM_2
	mov	a,r5
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_memcpy_PARM_3
	mov	a,#0x04
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_axradio_default_remoteaddr
	mov	b,#0x00
;	..\src\COMMON\easyax5043.c:2125: }
	ljmp	_memcpy
;------------------------------------------------------------
;Allocation info for local variables in function 'axradio_transmit'
;------------------------------------------------------------
;fifofree                  Allocated to registers r6 r7 
;i                         Allocated to registers r4 
;iesave                    Allocated to registers r7 
;len_byte                  Allocated to registers r6 
;pkt                       Allocated with name '_axradio_transmit_PARM_2'
;pktlen                    Allocated with name '_axradio_transmit_PARM_3'
;addr                      Allocated with name '_axradio_transmit_addr_65536_463'
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:2128: uint8_t axradio_transmit(const struct axradio_address __generic *addr, const uint8_t __generic *pkt, uint16_t pktlen)
;	-----------------------------------------
;	 function axradio_transmit
;	-----------------------------------------
_axradio_transmit:
	mov	r7,b
	mov	r6,dph
	mov	a,dpl
	mov	dptr,#_axradio_transmit_addr_65536_463
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2130: switch (axradio_mode) {
	mov	r7,_axradio_mode
	cjne	r7,#0x10,00315$
	ljmp	00125$
00315$:
	cjne	r7,#0x11,00316$
	ljmp	00125$
00316$:
	cjne	r7,#0x12,00317$
	ljmp	00125$
00317$:
	cjne	r7,#0x13,00318$
	ljmp	00125$
00318$:
	cjne	r7,#0x18,00319$
	sjmp	00105$
00319$:
	cjne	r7,#0x19,00320$
	sjmp	00105$
00320$:
	cjne	r7,#0x1a,00321$
	sjmp	00105$
00321$:
	cjne	r7,#0x1b,00322$
	sjmp	00105$
00322$:
	cjne	r7,#0x1c,00323$
	sjmp	00105$
00323$:
	cjne	r7,#0x20,00324$
	ljmp	00116$
00324$:
	cjne	r7,#0x21,00325$
	ljmp	00116$
00325$:
	cjne	r7,#0x30,00326$
	ljmp	00128$
00326$:
	cjne	r7,#0x31,00327$
	ljmp	00128$
00327$:
	ljmp	00162$
;	..\src\COMMON\easyax5043.c:2135: case AXRADIO_MODE_STREAM_TRANSMIT_SCRAM_LSB:
00105$:
;	..\src\COMMON\easyax5043.c:2137: uint16_t __autodata fifofree = radio_read16((uint16_t)&AX5043_FIFOFREE1);
	mov	r6,#_AX5043_FIFOFREE1
	mov	r7,#(_AX5043_FIFOFREE1 >> 8)
	mov	dpl,r6
	mov	dph,r7
	lcall	_radio_read16
	mov	r6,dpl
	mov	r7,dph
;	..\src\COMMON\easyax5043.c:2138: if (fifofree < pktlen + 3)
	mov	dptr,#_axradio_transmit_PARM_3
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	mov	a,#0x03
	add	a,r4
	mov	r2,a
	clr	a
	addc	a,r5
	mov	r3,a
	clr	c
	mov	a,r6
	subb	a,r2
	mov	a,r7
	subb	a,r3
	jnc	00107$
;	..\src\COMMON\easyax5043.c:2139: return AXRADIO_ERR_INVALID;
	mov	dpl,#0x04
	ret
00107$:
;	..\src\COMMON\easyax5043.c:2141: if (pktlen) {
	mov	a,r4
	orl	a,r5
	jz	00112$
;	..\src\COMMON\easyax5043.c:2142: uint8_t __autodata i = pktlen;
;	..\src\COMMON\easyax5043.c:2143: AX5043_FIFODATA = AX5043_FIFOCMD_DATA | (7 << 5);
	mov	dptr,#_AX5043_FIFODATA
	mov	a,#0xe1
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2144: AX5043_FIFODATA = i + 1;
	mov	ar7,r4
	mov	a,r7
	inc	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2145: AX5043_FIFODATA = 0x08;
	mov	a,#0x08
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2146: do {
	mov	dptr,#_axradio_transmit_PARM_2
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
00108$:
;	..\src\COMMON\easyax5043.c:2147: AX5043_FIFODATA = *pkt++;
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	lcall	__gptrget
	mov	r3,a
	inc	dptr
	mov	r5,dpl
	mov	r6,dph
	mov	dptr,#_AX5043_FIFODATA
	mov	a,r3
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2148: } while (--i);
	djnz	r4,00108$
00112$:
;	..\src\COMMON\easyax5043.c:2150: AX5043_FIFOSTAT =  4; // FIFO commit
	mov	dptr,#_AX5043_FIFOSTAT
	mov	a,#0x04
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2152: uint8_t __autodata iesave = IE & 0x80;
	mov	a,_IE
	anl	a,#0x80
	mov	r7,a
;	..\src\COMMON\easyax5043.c:2153: EA = 0;
;	assignBit
	clr	_EA
;	..\src\COMMON\easyax5043.c:2154: AX5043_IRQMASK0 |= 0x08;
	mov	dptr,#_AX5043_IRQMASK0
	movx	a,@dptr
	orl	acc,#0x08
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2155: IE |= iesave;
	mov	a,r7
	orl	_IE,a
;	..\src\COMMON\easyax5043.c:2157: return AXRADIO_ERR_NOERROR;
	mov	dpl,#0x00
	ret
;	..\src\COMMON\easyax5043.c:2164: case AXRADIO_MODE_WOR_RECEIVE:
00116$:
;	..\src\COMMON\easyax5043.c:2165: if (axradio_syncstate != syncstate_off)
	mov	dptr,#_axradio_syncstate
	movx	a,@dptr
	jz	00118$
;	..\src\COMMON\easyax5043.c:2166: return AXRADIO_ERR_BUSY;
	mov	dpl,#0x02
	ret
00118$:
;	..\src\COMMON\easyax5043.c:2167: AX5043_IRQMASK1 = 0x00;
	mov	dptr,#_AX5043_IRQMASK1
	clr	a
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2168: AX5043_IRQMASK0 = 0x00;
	mov	dptr,#_AX5043_IRQMASK0
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2169: AX5043_PWRMODE = AX5043_PWRSTATE_XTAL_ON;
	mov	dptr,#_AX5043_PWRMODE
	mov	a,#0x05
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2170: AX5043_FIFOSTAT = 3;
	mov	dptr,#_AX5043_FIFOSTAT
	mov	a,#0x03
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2171: while (AX5043_POWSTAT & 0x08);
00119$:
	mov	dptr,#_AX5043_POWSTAT
	movx	a,@dptr
	jb	acc.3,00119$
;	..\src\COMMON\easyax5043.c:2172: ax5043_init_registers_tx();
	lcall	_ax5043_init_registers_tx
;	..\src\COMMON\easyax5043.c:2173: goto dotx;
;	..\src\COMMON\easyax5043.c:2178: case AXRADIO_MODE_WOR_ACK_TRANSMIT:
	sjmp	00128$
00125$:
;	..\src\COMMON\easyax5043.c:2179: if (axradio_syncstate != syncstate_off)
	mov	dptr,#_axradio_syncstate
	movx	a,@dptr
	jz	00128$
;	..\src\COMMON\easyax5043.c:2180: return AXRADIO_ERR_BUSY;
	mov	dpl,#0x02
	ret
;	..\src\COMMON\easyax5043.c:2181: dotx:
00128$:
;	..\src\COMMON\easyax5043.c:2182: axradio_ack_count = axradio_framing_ack_retransmissions;
	mov	dptr,#_axradio_framing_ack_retransmissions
	clr	a
	movc	a,@a+dptr
	mov	dptr,#_axradio_ack_count
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2183: ++axradio_ack_seqnr;
	mov	dptr,#_axradio_ack_seqnr
	movx	a,@dptr
	add	a,#0x01
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2184: axradio_txbuffer_len = pktlen + axradio_framing_maclen;
	mov	dptr,#_axradio_framing_maclen
	clr	a
	movc	a,@a+dptr
	mov	r7,a
	mov	r5,a
	mov	r6,#0x00
	mov	dptr,#_axradio_transmit_PARM_3
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
	mov	a,r5
	add	a,r3
	mov	r5,a
	mov	a,r6
	addc	a,r4
	mov	r6,a
	mov	dptr,#_axradio_txbuffer_len
	mov	a,r5
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2185: if (axradio_txbuffer_len > sizeof(axradio_txbuffer))
	clr	c
	mov	a,#0x04
	subb	a,r5
	mov	a,#0x01
	subb	a,r6
	jnc	00130$
;	..\src\COMMON\easyax5043.c:2186: return AXRADIO_ERR_INVALID;
	mov	dpl,#0x04
	ret
00130$:
;	..\src\COMMON\easyax5043.c:2187: memset_xdata(axradio_txbuffer, 0, axradio_framing_maclen);
	mov	r6,#0x00
	mov	dptr,#_memset_PARM_2
	clr	a
	movx	@dptr,a
	mov	dptr,#_memset_PARM_3
	mov	a,r7
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_axradio_txbuffer
	mov	b,#0x00
	push	ar4
	push	ar3
	lcall	_memset
	pop	ar3
	pop	ar4
;	..\src\COMMON\easyax5043.c:2188: memcpy_xdatageneric(&axradio_txbuffer[axradio_framing_maclen], pkt, pktlen);
	mov	dptr,#_axradio_framing_maclen
	clr	a
	movc	a,@a+dptr
	add	a,#_axradio_txbuffer
	mov	r7,a
	clr	a
	addc	a,#(_axradio_txbuffer >> 8)
	mov	r6,a
	mov	r5,#0x00
	mov	dptr,#_axradio_transmit_PARM_2
	movx	a,@dptr
	mov	r0,a
	inc	dptr
	movx	a,@dptr
	mov	r1,a
	inc	dptr
	movx	a,@dptr
	mov	r2,a
	mov	dptr,#_memcpy_PARM_2
	mov	a,r0
	movx	@dptr,a
	mov	a,r1
	inc	dptr
	movx	@dptr,a
	mov	a,r2
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_memcpy_PARM_3
	mov	a,r3
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
	mov	dpl,r7
	mov	dph,r6
	mov	b,r5
	lcall	_memcpy
;	..\src\COMMON\easyax5043.c:2189: if (axradio_framing_ack_seqnrpos != 0xff)
	mov	dptr,#_axradio_framing_ack_seqnrpos
	clr	a
	movc	a,@a+dptr
	mov	r7,a
	cjne	r7,#0xff,00335$
	sjmp	00132$
00335$:
;	..\src\COMMON\easyax5043.c:2190: axradio_txbuffer[axradio_framing_ack_seqnrpos] = axradio_ack_seqnr;
	mov	a,r7
	add	a,#_axradio_txbuffer
	mov	r7,a
	clr	a
	addc	a,#(_axradio_txbuffer >> 8)
	mov	r6,a
	mov	dptr,#_axradio_ack_seqnr
	movx	a,@dptr
	mov	r5,a
	mov	dpl,r7
	mov	dph,r6
	movx	@dptr,a
00132$:
;	..\src\COMMON\easyax5043.c:2191: if (axradio_framing_destaddrpos != 0xff)
	mov	dptr,#_axradio_framing_destaddrpos
	clr	a
	movc	a,@a+dptr
	mov	r7,a
	cjne	r7,#0xff,00336$
	sjmp	00134$
00336$:
;	..\src\COMMON\easyax5043.c:2192: memcpy_xdatageneric(&axradio_txbuffer[axradio_framing_destaddrpos], &addr->addr, axradio_framing_addrlen);
	mov	a,r7
	add	a,#_axradio_txbuffer
	mov	r7,a
	clr	a
	addc	a,#(_axradio_txbuffer >> 8)
	mov	r6,a
	mov	r5,#0x00
	mov	dptr,#_axradio_transmit_addr_65536_463
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
	mov	dptr,#_axradio_framing_addrlen
	clr	a
	movc	a,@a+dptr
	mov	r0,a
	mov	r1,#0x00
	mov	dptr,#_memcpy_PARM_2
	mov	a,r2
	movx	@dptr,a
	mov	a,r3
	inc	dptr
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_memcpy_PARM_3
	mov	a,r0
	movx	@dptr,a
	mov	a,r1
	inc	dptr
	movx	@dptr,a
	mov	dpl,r7
	mov	dph,r6
	mov	b,r5
	lcall	_memcpy
00134$:
;	..\src\COMMON\easyax5043.c:2193: if (axradio_framing_sourceaddrpos != 0xff)
	mov	dptr,#_axradio_framing_sourceaddrpos
	clr	a
	movc	a,@a+dptr
	mov	r7,a
	cjne	r7,#0xff,00337$
	sjmp	00136$
00337$:
;	..\src\COMMON\easyax5043.c:2194: memcpy_xdata(&axradio_txbuffer[axradio_framing_sourceaddrpos], &axradio_localaddr.addr, axradio_framing_addrlen);
	mov	a,r7
	add	a,#_axradio_txbuffer
	mov	r7,a
	clr	a
	addc	a,#(_axradio_txbuffer >> 8)
	mov	r6,a
	mov	r5,#0x00
	mov	dptr,#_axradio_framing_addrlen
	clr	a
	movc	a,@a+dptr
	mov	r4,a
	mov	r3,#0x00
	mov	dptr,#_memcpy_PARM_2
	mov	a,#_axradio_localaddr
	movx	@dptr,a
	mov	a,#(_axradio_localaddr >> 8)
	inc	dptr
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_memcpy_PARM_3
	mov	a,r4
	movx	@dptr,a
	mov	a,r3
	inc	dptr
	movx	@dptr,a
	mov	dpl,r7
	mov	dph,r6
	mov	b,r5
	lcall	_memcpy
00136$:
;	..\src\COMMON\easyax5043.c:2195: if (axradio_framing_lenmask) {
	mov	dptr,#_axradio_framing_lenmask
	clr	a
	movc	a,@a+dptr
	mov	r7,a
	jz	00138$
;	..\src\COMMON\easyax5043.c:2196: uint8_t __autodata len_byte = (uint8_t)(axradio_txbuffer_len - axradio_framing_lenoffs) & axradio_framing_lenmask; // if you prefer not counting the len byte itself, set LENOFFS = 1
	mov	dptr,#_axradio_txbuffer_len
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	dptr,#_axradio_framing_lenoffs
	clr	a
	movc	a,@a+dptr
	mov	r6,a
	mov	a,r5
	clr	c
	subb	a,r6
	anl	a,r7
	mov	r6,a
;	..\src\COMMON\easyax5043.c:2197: axradio_txbuffer[axradio_framing_lenpos] = (axradio_txbuffer[axradio_framing_lenpos] & (uint8_t)~axradio_framing_lenmask) | len_byte;
	mov	dptr,#_axradio_framing_lenpos
	clr	a
	movc	a,@a+dptr
	add	a,#_axradio_txbuffer
	mov	r5,a
	clr	a
	addc	a,#(_axradio_txbuffer >> 8)
	mov	r4,a
	mov	dpl,r5
	mov	dph,r4
	movx	a,@dptr
	mov	r3,a
	mov	a,r7
	cpl	a
	mov	r7,a
	anl	a,r3
	orl	ar6,a
	mov	dpl,r5
	mov	dph,r4
	mov	a,r6
	movx	@dptr,a
00138$:
;	..\src\COMMON\easyax5043.c:2199: if (axradio_framing_swcrclen)
	mov	dptr,#_axradio_framing_swcrclen
	clr	a
	movc	a,@a+dptr
	jz	00140$
;	..\src\COMMON\easyax5043.c:2200: axradio_txbuffer_len = axradio_framing_append_crc(axradio_txbuffer, axradio_txbuffer_len);
	mov	dptr,#_axradio_txbuffer_len
	movx	a,@dptr
	push	acc
	inc	dptr
	movx	a,@dptr
	push	acc
	mov	dptr,#_axradio_txbuffer
	lcall	_axradio_framing_append_crc
	mov	r6,dpl
	mov	r7,dph
	dec	sp
	dec	sp
	mov	dptr,#_axradio_txbuffer_len
	mov	a,r6
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
00140$:
;	..\src\COMMON\easyax5043.c:2201: if (axradio_phy_pn9)
	mov	dptr,#_axradio_phy_pn9
	clr	a
	movc	a,@a+dptr
	jz	00142$
;	..\src\COMMON\easyax5043.c:2202: pn9_buffer(axradio_txbuffer, axradio_txbuffer_len, 0x1ff, -(AX5043_ENCODING & 0x01));
	mov	dptr,#_AX5043_ENCODING
	movx	a,@dptr
	mov	r7,a
	anl	ar7,#0x01
	clr	c
	clr	a
	subb	a,r7
	mov	r7,a
	push	ar7
	mov	a,#0xff
	push	acc
	mov	a,#0x01
	push	acc
	mov	dptr,#_axradio_txbuffer_len
	movx	a,@dptr
	push	acc
	inc	dptr
	movx	a,@dptr
	push	acc
	mov	dptr,#_axradio_txbuffer
	mov	b,#0x00
	lcall	_pn9_buffer
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
00142$:
;	..\src\COMMON\easyax5043.c:2203: if (axradio_mode == AXRADIO_MODE_SYNC_MASTER ||
	mov	a,#0x30
	cjne	a,_axradio_mode,00341$
	sjmp	00143$
00341$:
;	..\src\COMMON\easyax5043.c:2204: axradio_mode == AXRADIO_MODE_SYNC_ACK_MASTER)
	mov	a,#0x31
	cjne	a,_axradio_mode,00144$
00143$:
;	..\src\COMMON\easyax5043.c:2205: return AXRADIO_ERR_NOERROR;
	mov	dpl,#0x00
	ret
00144$:
;	..\src\COMMON\easyax5043.c:2206: if (axradio_mode == AXRADIO_MODE_WOR_TRANSMIT ||
	mov	a,#0x11
	cjne	a,_axradio_mode,00344$
	sjmp	00146$
00344$:
;	..\src\COMMON\easyax5043.c:2207: axradio_mode == AXRADIO_MODE_WOR_ACK_TRANSMIT)
	mov	a,#0x13
	cjne	a,_axradio_mode,00147$
00146$:
;	..\src\COMMON\easyax5043.c:2208: axradio_txbuffer_cnt = axradio_phy_preamble_wor_longlen;
	mov	dptr,#_axradio_phy_preamble_wor_longlen
	clr	a
	movc	a,@a+dptr
	mov	r6,a
	mov	a,#0x01
	movc	a,@a+dptr
	mov	r7,a
	mov	dptr,#_axradio_txbuffer_cnt
	mov	a,r6
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
	sjmp	00148$
00147$:
;	..\src\COMMON\easyax5043.c:2210: axradio_txbuffer_cnt = axradio_phy_preamble_longlen;
	mov	dptr,#_axradio_phy_preamble_longlen
	clr	a
	movc	a,@a+dptr
	mov	r6,a
	mov	a,#0x01
	movc	a,@a+dptr
	mov	r7,a
	mov	dptr,#_axradio_txbuffer_cnt
	mov	a,r6
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
00148$:
;	..\src\COMMON\easyax5043.c:2211: if (axradio_phy_lbt_retries) {
	mov	dptr,#_axradio_phy_lbt_retries
	clr	a
	movc	a,@a+dptr
	jnz	00347$
	ljmp	00161$
00347$:
;	..\src\COMMON\easyax5043.c:2212: switch (axradio_mode) {
	mov	r7,_axradio_mode
	cjne	r7,#0x10,00348$
	sjmp	00157$
00348$:
	cjne	r7,#0x11,00349$
	sjmp	00157$
00349$:
	cjne	r7,#0x12,00350$
	sjmp	00157$
00350$:
	cjne	r7,#0x13,00351$
	sjmp	00157$
00351$:
	cjne	r7,#0x20,00352$
	sjmp	00157$
00352$:
	cjne	r7,#0x21,00353$
	sjmp	00157$
00353$:
	cjne	r7,#0x22,00354$
	sjmp	00157$
00354$:
	cjne	r7,#0x23,00161$
;	..\src\COMMON\easyax5043.c:2220: case AXRADIO_MODE_ACK_RECEIVE:
00157$:
;	..\src\COMMON\easyax5043.c:2221: ax5043_off_xtal();
	lcall	_ax5043_off_xtal
;	..\src\COMMON\easyax5043.c:2222: ax5043_init_registers_rx();
	lcall	_ax5043_init_registers_rx
;	..\src\COMMON\easyax5043.c:2223: AX5043_RSSIREFERENCE = axradio_phy_rssireference;
	mov	dptr,#_axradio_phy_rssireference
	clr	a
	movc	a,@a+dptr
	mov	dptr,#_AX5043_RSSIREFERENCE
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2224: AX5043_PWRMODE = AX5043_PWRSTATE_FULL_RX;
	mov	dptr,#_AX5043_PWRMODE
	mov	a,#0x09
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2225: axradio_ack_count = axradio_phy_lbt_retries;
	mov	dptr,#_axradio_phy_lbt_retries
	clr	a
	movc	a,@a+dptr
	mov	dptr,#_axradio_ack_count
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2226: axradio_syncstate = syncstate_lbt;
	mov	dptr,#_axradio_syncstate
	mov	a,#0x01
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2227: wtimer_remove(&axradio_timer);
	mov	dptr,#_axradio_timer
	lcall	_wtimer_remove
;	..\src\COMMON\easyax5043.c:2228: axradio_timer.time = axradio_phy_cs_period;
	mov	dptr,#_axradio_phy_cs_period
	clr	a
	movc	a,@a+dptr
	mov	r6,a
	mov	a,#0x01
	movc	a,@a+dptr
	mov	r7,a
	mov	r5,#0x00
	mov	r4,#0x00
	mov	dptr,#(_axradio_timer + 0x0004)
	mov	a,r6
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2229: wtimer0_addrelative(&axradio_timer);
	mov	dptr,#_axradio_timer
	lcall	_wtimer0_addrelative
;	..\src\COMMON\easyax5043.c:2230: return AXRADIO_ERR_NOERROR;
	mov	dpl,#0x00
;	..\src\COMMON\easyax5043.c:2234: }
	ret
00161$:
;	..\src\COMMON\easyax5043.c:2236: axradio_syncstate = syncstate_asynctx;
	mov	dptr,#_axradio_syncstate
	mov	a,#0x02
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2237: ax5043_prepare_tx();
	lcall	_ax5043_prepare_tx
;	..\src\COMMON\easyax5043.c:2238: return AXRADIO_ERR_NOERROR;
	mov	dpl,#0x00
;	..\src\COMMON\easyax5043.c:2240: default:
	ret
00162$:
;	..\src\COMMON\easyax5043.c:2241: return AXRADIO_ERR_NOTSUPPORTED;
	mov	dpl,#0x01
;	..\src\COMMON\easyax5043.c:2242: }
;	..\src\COMMON\easyax5043.c:2243: }
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'axradio_set_paramsets'
;------------------------------------------------------------
;val                       Allocated to registers r7 
;------------------------------------------------------------
;	..\src\COMMON\easyax5043.c:2245: static __reentrantb uint8_t axradio_set_paramsets(uint8_t val) __reentrant
;	-----------------------------------------
;	 function axradio_set_paramsets
;	-----------------------------------------
_axradio_set_paramsets:
	mov	r7,dpl
;	..\src\COMMON\easyax5043.c:2247: if (!AXRADIO_MODE_IS_STREAM_RECEIVE(axradio_mode))
	mov	r5,_axradio_mode
	anl	ar5,#0xf8
	mov	r6,#0x00
	cjne	r5,#0x28,00109$
	cjne	r6,#0x00,00109$
	sjmp	00102$
00109$:
;	..\src\COMMON\easyax5043.c:2248: return AXRADIO_ERR_NOTSUPPORTED;
	mov	dpl,#0x01
	ret
00102$:
;	..\src\COMMON\easyax5043.c:2249: AX5043_RXPARAMSETS = val;
	mov	dptr,#_AX5043_RXPARAMSETS
	mov	a,r7
	movx	@dptr,a
;	..\src\COMMON\easyax5043.c:2250: return AXRADIO_ERR_NOERROR;
	mov	dpl,#0x00
;	..\src\COMMON\easyax5043.c:2251: }
	ret
	.area CSEG    (CODE)
	.area CONST   (CODE)
	.area CONST   (CODE)
___str_0:
	.ascii " "
	.db 0x0a
	.ascii " Receive isr "
	.db 0x00
	.area CSEG    (CODE)
	.area CONST   (CODE)
___str_1:
	.ascii " discarded "
	.db 0x00
	.area CSEG    (CODE)
	.area XINIT   (CODE)
__xinit__f30_saved:
	.db #0x3f	; 63
__xinit__f31_saved:
	.db #0xf0	; 240
__xinit__f32_saved:
	.db #0x3f	; 63
__xinit__f33_saved:
	.db #0xf0	; 240
	.area CABS    (ABS,CODE)
