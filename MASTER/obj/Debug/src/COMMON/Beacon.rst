                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.6.0 #9615 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module Beacon
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _memcpy
                                     12 	.globl _BEACON_decoding_PARM_3
                                     13 	.globl _BEACON_decoding_PARM_2
                                     14 	.globl _BEACON_decoding
                                     15 ;--------------------------------------------------------
                                     16 ; special function registers
                                     17 ;--------------------------------------------------------
                                     18 	.area RSEG    (ABS,DATA)
      000000                         19 	.org 0x0000
                                     20 ;--------------------------------------------------------
                                     21 ; special function bits
                                     22 ;--------------------------------------------------------
                                     23 	.area RSEG    (ABS,DATA)
      000000                         24 	.org 0x0000
                                     25 ;--------------------------------------------------------
                                     26 ; overlayable register banks
                                     27 ;--------------------------------------------------------
                                     28 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                         29 	.ds 8
                                     30 ;--------------------------------------------------------
                                     31 ; internal ram data
                                     32 ;--------------------------------------------------------
                                     33 	.area DSEG    (DATA)
      00000B                         34 _BEACON_decoding_sloc0_1_0:
      00000B                         35 	.ds 2
      00000D                         36 _BEACON_decoding_sloc1_1_0:
      00000D                         37 	.ds 2
      00000F                         38 _BEACON_decoding_sloc2_1_0:
      00000F                         39 	.ds 2
      000011                         40 _BEACON_decoding_sloc3_1_0:
      000011                         41 	.ds 3
      000014                         42 _BEACON_decoding_sloc4_1_0:
      000014                         43 	.ds 3
      000017                         44 _BEACON_decoding_sloc5_1_0:
      000017                         45 	.ds 4
                                     46 ;--------------------------------------------------------
                                     47 ; overlayable items in internal ram 
                                     48 ;--------------------------------------------------------
                                     49 ;--------------------------------------------------------
                                     50 ; indirectly addressable internal ram data
                                     51 ;--------------------------------------------------------
                                     52 	.area ISEG    (DATA)
                                     53 ;--------------------------------------------------------
                                     54 ; absolute internal ram data
                                     55 ;--------------------------------------------------------
                                     56 	.area IABS    (ABS,DATA)
                                     57 	.area IABS    (ABS,DATA)
                                     58 ;--------------------------------------------------------
                                     59 ; bit data
                                     60 ;--------------------------------------------------------
                                     61 	.area BSEG    (BIT)
                                     62 ;--------------------------------------------------------
                                     63 ; paged external ram data
                                     64 ;--------------------------------------------------------
                                     65 	.area PSEG    (PAG,XDATA)
                                     66 ;--------------------------------------------------------
                                     67 ; external ram data
                                     68 ;--------------------------------------------------------
                                     69 	.area XSEG    (XDATA)
      0000BD                         70 _BEACON_decoding_PARM_2:
      0000BD                         71 	.ds 2
      0000BF                         72 _BEACON_decoding_PARM_3:
      0000BF                         73 	.ds 1
      0000C0                         74 _BEACON_decoding_Rxbuffer_1_79:
      0000C0                         75 	.ds 3
                                     76 ;--------------------------------------------------------
                                     77 ; absolute external ram data
                                     78 ;--------------------------------------------------------
                                     79 	.area XABS    (ABS,XDATA)
                                     80 ;--------------------------------------------------------
                                     81 ; external initialized ram data
                                     82 ;--------------------------------------------------------
                                     83 	.area XISEG   (XDATA)
                                     84 	.area HOME    (CODE)
                                     85 	.area GSINIT0 (CODE)
                                     86 	.area GSINIT1 (CODE)
                                     87 	.area GSINIT2 (CODE)
                                     88 	.area GSINIT3 (CODE)
                                     89 	.area GSINIT4 (CODE)
                                     90 	.area GSINIT5 (CODE)
                                     91 	.area GSINIT  (CODE)
                                     92 	.area GSFINAL (CODE)
                                     93 	.area CSEG    (CODE)
                                     94 ;--------------------------------------------------------
                                     95 ; global & static initialisations
                                     96 ;--------------------------------------------------------
                                     97 	.area HOME    (CODE)
                                     98 	.area GSINIT  (CODE)
                                     99 	.area GSFINAL (CODE)
                                    100 	.area GSINIT  (CODE)
                                    101 ;--------------------------------------------------------
                                    102 ; Home
                                    103 ;--------------------------------------------------------
                                    104 	.area HOME    (CODE)
                                    105 	.area HOME    (CODE)
                                    106 ;--------------------------------------------------------
                                    107 ; code
                                    108 ;--------------------------------------------------------
                                    109 	.area CSEG    (CODE)
                                    110 ;------------------------------------------------------------
                                    111 ;Allocation info for local variables in function 'BEACON_decoding'
                                    112 ;------------------------------------------------------------
                                    113 ;sloc0                     Allocated with name '_BEACON_decoding_sloc0_1_0'
                                    114 ;sloc1                     Allocated with name '_BEACON_decoding_sloc1_1_0'
                                    115 ;sloc2                     Allocated with name '_BEACON_decoding_sloc2_1_0'
                                    116 ;sloc3                     Allocated with name '_BEACON_decoding_sloc3_1_0'
                                    117 ;sloc4                     Allocated with name '_BEACON_decoding_sloc4_1_0'
                                    118 ;sloc5                     Allocated with name '_BEACON_decoding_sloc5_1_0'
                                    119 ;beacon                    Allocated with name '_BEACON_decoding_PARM_2'
                                    120 ;length                    Allocated with name '_BEACON_decoding_PARM_3'
                                    121 ;Rxbuffer                  Allocated with name '_BEACON_decoding_Rxbuffer_1_79'
                                    122 ;i                         Allocated with name '_BEACON_decoding_i_1_80'
                                    123 ;------------------------------------------------------------
                                    124 ;	..\src\COMMON\Beacon.c:29: __xdata void BEACON_decoding(uint8_t* Rxbuffer, RF_beacon_t* beacon, uint8_t length)
                                    125 ;	-----------------------------------------
                                    126 ;	 function BEACON_decoding
                                    127 ;	-----------------------------------------
      001C53                        128 _BEACON_decoding:
                           000007   129 	ar7 = 0x07
                           000006   130 	ar6 = 0x06
                           000005   131 	ar5 = 0x05
                           000004   132 	ar4 = 0x04
                           000003   133 	ar3 = 0x03
                           000002   134 	ar2 = 0x02
                           000001   135 	ar1 = 0x01
                           000000   136 	ar0 = 0x00
      001C53 AF F0            [24]  137 	mov	r7,b
      001C55 AE 83            [24]  138 	mov	r6,dph
      001C57 E5 82            [12]  139 	mov	a,dpl
      001C59 90 00 C0         [24]  140 	mov	dptr,#_BEACON_decoding_Rxbuffer_1_79
      001C5C F0               [24]  141 	movx	@dptr,a
      001C5D EE               [12]  142 	mov	a,r6
      001C5E A3               [24]  143 	inc	dptr
      001C5F F0               [24]  144 	movx	@dptr,a
      001C60 EF               [12]  145 	mov	a,r7
      001C61 A3               [24]  146 	inc	dptr
      001C62 F0               [24]  147 	movx	@dptr,a
                                    148 ;	..\src\COMMON\Beacon.c:32: beacon->FlagReceived = VALID_BEACON;
      001C63 90 00 BD         [24]  149 	mov	dptr,#_BEACON_decoding_PARM_2
      001C66 E0               [24]  150 	movx	a,@dptr
      001C67 F5 0F            [12]  151 	mov	_BEACON_decoding_sloc2_1_0,a
      001C69 A3               [24]  152 	inc	dptr
      001C6A E0               [24]  153 	movx	a,@dptr
      001C6B F5 10            [12]  154 	mov	(_BEACON_decoding_sloc2_1_0 + 1),a
      001C6D 85 0F 82         [24]  155 	mov	dpl,_BEACON_decoding_sloc2_1_0
      001C70 85 10 83         [24]  156 	mov	dph,(_BEACON_decoding_sloc2_1_0 + 1)
      001C73 74 01            [12]  157 	mov	a,#0x01
      001C75 F0               [24]  158 	movx	@dptr,a
                                    159 ;	..\src\COMMON\Beacon.c:33: if(length == 23)
      001C76 90 00 BF         [24]  160 	mov	dptr,#_BEACON_decoding_PARM_3
      001C79 E0               [24]  161 	movx	a,@dptr
      001C7A FD               [12]  162 	mov	r5,a
      001C7B BD 17 02         [24]  163 	cjne	r5,#0x17,00118$
      001C7E 80 03            [24]  164 	sjmp	00119$
      001C80                        165 00118$:
      001C80 02 21 3D         [24]  166 	ljmp	00108$
      001C83                        167 00119$:
                                    168 ;	..\src\COMMON\Beacon.c:35: beacon->SatId = *Rxbuffer <<8 | *(Rxbuffer+1);
      001C83 74 01            [12]  169 	mov	a,#0x01
      001C85 25 0F            [12]  170 	add	a,_BEACON_decoding_sloc2_1_0
      001C87 F5 0B            [12]  171 	mov	_BEACON_decoding_sloc0_1_0,a
      001C89 E4               [12]  172 	clr	a
      001C8A 35 10            [12]  173 	addc	a,(_BEACON_decoding_sloc2_1_0 + 1)
      001C8C F5 0C            [12]  174 	mov	(_BEACON_decoding_sloc0_1_0 + 1),a
      001C8E 90 00 C0         [24]  175 	mov	dptr,#_BEACON_decoding_Rxbuffer_1_79
      001C91 E0               [24]  176 	movx	a,@dptr
      001C92 F9               [12]  177 	mov	r1,a
      001C93 A3               [24]  178 	inc	dptr
      001C94 E0               [24]  179 	movx	a,@dptr
      001C95 FA               [12]  180 	mov	r2,a
      001C96 A3               [24]  181 	inc	dptr
      001C97 E0               [24]  182 	movx	a,@dptr
      001C98 FB               [12]  183 	mov	r3,a
      001C99 89 82            [24]  184 	mov	dpl,r1
      001C9B 8A 83            [24]  185 	mov	dph,r2
      001C9D 8B F0            [24]  186 	mov	b,r3
      001C9F 12 96 F0         [24]  187 	lcall	__gptrget
      001CA2 F8               [12]  188 	mov	r0,a
      001CA3 7D 00            [12]  189 	mov	r5,#0x00
      001CA5 88 0E            [24]  190 	mov	(_BEACON_decoding_sloc1_1_0 + 1),r0
                                    191 ;	1-genFromRTrack replaced	mov	_BEACON_decoding_sloc1_1_0,#0x00
      001CA7 8D 0D            [24]  192 	mov	_BEACON_decoding_sloc1_1_0,r5
      001CA9 74 01            [12]  193 	mov	a,#0x01
      001CAB 29               [12]  194 	add	a,r1
      001CAC F8               [12]  195 	mov	r0,a
      001CAD E4               [12]  196 	clr	a
      001CAE 3A               [12]  197 	addc	a,r2
      001CAF FC               [12]  198 	mov	r4,a
      001CB0 8B 05            [24]  199 	mov	ar5,r3
      001CB2 88 82            [24]  200 	mov	dpl,r0
      001CB4 8C 83            [24]  201 	mov	dph,r4
      001CB6 8D F0            [24]  202 	mov	b,r5
      001CB8 12 96 F0         [24]  203 	lcall	__gptrget
      001CBB F8               [12]  204 	mov	r0,a
      001CBC 7D 00            [12]  205 	mov	r5,#0x00
      001CBE E5 0D            [12]  206 	mov	a,_BEACON_decoding_sloc1_1_0
      001CC0 42 00            [12]  207 	orl	ar0,a
      001CC2 E5 0E            [12]  208 	mov	a,(_BEACON_decoding_sloc1_1_0 + 1)
      001CC4 42 05            [12]  209 	orl	ar5,a
      001CC6 85 0B 82         [24]  210 	mov	dpl,_BEACON_decoding_sloc0_1_0
      001CC9 85 0C 83         [24]  211 	mov	dph,(_BEACON_decoding_sloc0_1_0 + 1)
      001CCC E8               [12]  212 	mov	a,r0
      001CCD F0               [24]  213 	movx	@dptr,a
      001CCE ED               [12]  214 	mov	a,r5
      001CCF A3               [24]  215 	inc	dptr
      001CD0 F0               [24]  216 	movx	@dptr,a
                                    217 ;	..\src\COMMON\Beacon.c:36: beacon->BeaconID = (*(Rxbuffer+POS_BEACONID) & MASK_BEACONID)>> 4;
      001CD1 74 03            [12]  218 	mov	a,#0x03
      001CD3 25 0F            [12]  219 	add	a,_BEACON_decoding_sloc2_1_0
      001CD5 FC               [12]  220 	mov	r4,a
      001CD6 E4               [12]  221 	clr	a
      001CD7 35 10            [12]  222 	addc	a,(_BEACON_decoding_sloc2_1_0 + 1)
      001CD9 FD               [12]  223 	mov	r5,a
      001CDA 74 02            [12]  224 	mov	a,#0x02
      001CDC 29               [12]  225 	add	a,r1
      001CDD F8               [12]  226 	mov	r0,a
      001CDE E4               [12]  227 	clr	a
      001CDF 3A               [12]  228 	addc	a,r2
      001CE0 FE               [12]  229 	mov	r6,a
      001CE1 8B 07            [24]  230 	mov	ar7,r3
      001CE3 88 82            [24]  231 	mov	dpl,r0
      001CE5 8E 83            [24]  232 	mov	dph,r6
      001CE7 8F F0            [24]  233 	mov	b,r7
      001CE9 12 96 F0         [24]  234 	lcall	__gptrget
      001CEC 54 F0            [12]  235 	anl	a,#0xf0
      001CEE C4               [12]  236 	swap	a
      001CEF 54 0F            [12]  237 	anl	a,#0x0f
      001CF1 F8               [12]  238 	mov	r0,a
      001CF2 8C 82            [24]  239 	mov	dpl,r4
      001CF4 8D 83            [24]  240 	mov	dph,r5
      001CF6 F0               [24]  241 	movx	@dptr,a
                                    242 ;	..\src\COMMON\Beacon.c:39: if(beacon->BeaconID == UPLINK_BEACON)
      001CF7 B8 0C 35         [24]  243 	cjne	r0,#0x0c,00105$
                                    244 ;	..\src\COMMON\Beacon.c:41: beacon->NumPremSlots = *(Rxbuffer+POS_NUMPREM);
      001CFA 74 04            [12]  245 	mov	a,#0x04
      001CFC 25 0F            [12]  246 	add	a,_BEACON_decoding_sloc2_1_0
      001CFE F5 0D            [12]  247 	mov	_BEACON_decoding_sloc1_1_0,a
      001D00 E4               [12]  248 	clr	a
      001D01 35 10            [12]  249 	addc	a,(_BEACON_decoding_sloc2_1_0 + 1)
      001D03 F5 0E            [12]  250 	mov	(_BEACON_decoding_sloc1_1_0 + 1),a
      001D05 74 03            [12]  251 	mov	a,#0x03
      001D07 29               [12]  252 	add	a,r1
      001D08 F8               [12]  253 	mov	r0,a
      001D09 E4               [12]  254 	clr	a
      001D0A 3A               [12]  255 	addc	a,r2
      001D0B FE               [12]  256 	mov	r6,a
      001D0C 8B 07            [24]  257 	mov	ar7,r3
      001D0E 88 82            [24]  258 	mov	dpl,r0
      001D10 8E 83            [24]  259 	mov	dph,r6
      001D12 8F F0            [24]  260 	mov	b,r7
      001D14 12 96 F0         [24]  261 	lcall	__gptrget
      001D17 F8               [12]  262 	mov	r0,a
      001D18 85 0D 82         [24]  263 	mov	dpl,_BEACON_decoding_sloc1_1_0
      001D1B 85 0E 83         [24]  264 	mov	dph,(_BEACON_decoding_sloc1_1_0 + 1)
      001D1E F0               [24]  265 	movx	@dptr,a
                                    266 ;	..\src\COMMON\Beacon.c:42: beacon->NumACK = 0xFF; //invalid data
      001D1F 85 0F 82         [24]  267 	mov	dpl,_BEACON_decoding_sloc2_1_0
      001D22 85 10 83         [24]  268 	mov	dph,(_BEACON_decoding_sloc2_1_0 + 1)
      001D25 A3               [24]  269 	inc	dptr
      001D26 A3               [24]  270 	inc	dptr
      001D27 A3               [24]  271 	inc	dptr
      001D28 A3               [24]  272 	inc	dptr
      001D29 A3               [24]  273 	inc	dptr
      001D2A 74 FF            [12]  274 	mov	a,#0xff
      001D2C F0               [24]  275 	movx	@dptr,a
      001D2D 80 34            [24]  276 	sjmp	00106$
      001D2F                        277 00105$:
                                    278 ;	..\src\COMMON\Beacon.c:43: }else if(beacon->BeaconID = DOWNLINK_BEACON)
      001D2F 8C 82            [24]  279 	mov	dpl,r4
      001D31 8D 83            [24]  280 	mov	dph,r5
      001D33 74 0A            [12]  281 	mov	a,#0x0a
      001D35 F0               [24]  282 	movx	@dptr,a
                                    283 ;	..\src\COMMON\Beacon.c:45: beacon->NumACK = *(Rxbuffer+POS_NUMPREM);
      001D36 03               [12]  284 	rr	a
      001D37 25 0F            [12]  285 	add	a,_BEACON_decoding_sloc2_1_0
      001D39 FE               [12]  286 	mov	r6,a
      001D3A E4               [12]  287 	clr	a
      001D3B 35 10            [12]  288 	addc	a,(_BEACON_decoding_sloc2_1_0 + 1)
      001D3D FF               [12]  289 	mov	r7,a
      001D3E 74 03            [12]  290 	mov	a,#0x03
      001D40 29               [12]  291 	add	a,r1
      001D41 F8               [12]  292 	mov	r0,a
      001D42 E4               [12]  293 	clr	a
      001D43 3A               [12]  294 	addc	a,r2
      001D44 FC               [12]  295 	mov	r4,a
      001D45 8B 05            [24]  296 	mov	ar5,r3
      001D47 88 82            [24]  297 	mov	dpl,r0
      001D49 8C 83            [24]  298 	mov	dph,r4
      001D4B 8D F0            [24]  299 	mov	b,r5
      001D4D 12 96 F0         [24]  300 	lcall	__gptrget
      001D50 F8               [12]  301 	mov	r0,a
      001D51 8E 82            [24]  302 	mov	dpl,r6
      001D53 8F 83            [24]  303 	mov	dph,r7
      001D55 F0               [24]  304 	movx	@dptr,a
                                    305 ;	..\src\COMMON\Beacon.c:46: beacon->NumPremSlots = 0xFF; //invalid data
      001D56 85 0F 82         [24]  306 	mov	dpl,_BEACON_decoding_sloc2_1_0
      001D59 85 10 83         [24]  307 	mov	dph,(_BEACON_decoding_sloc2_1_0 + 1)
      001D5C A3               [24]  308 	inc	dptr
      001D5D A3               [24]  309 	inc	dptr
      001D5E A3               [24]  310 	inc	dptr
      001D5F A3               [24]  311 	inc	dptr
      001D60 74 FF            [12]  312 	mov	a,#0xff
      001D62 F0               [24]  313 	movx	@dptr,a
                                    314 ;	..\src\COMMON\Beacon.c:49: else beacon->FlagReceived = INVALID_BEACON;
      001D63                        315 00106$:
                                    316 ;	..\src\COMMON\Beacon.c:51: beacon->HMAC_flag =(*(Rxbuffer+POS_SECURITY) & MASK_HMAC_FLAG)>>7;
      001D63 74 06            [12]  317 	mov	a,#0x06
      001D65 25 0F            [12]  318 	add	a,_BEACON_decoding_sloc2_1_0
      001D67 F5 0D            [12]  319 	mov	_BEACON_decoding_sloc1_1_0,a
      001D69 E4               [12]  320 	clr	a
      001D6A 35 10            [12]  321 	addc	a,(_BEACON_decoding_sloc2_1_0 + 1)
      001D6C F5 0E            [12]  322 	mov	(_BEACON_decoding_sloc1_1_0 + 1),a
      001D6E 74 04            [12]  323 	mov	a,#0x04
      001D70 29               [12]  324 	add	a,r1
      001D71 F8               [12]  325 	mov	r0,a
      001D72 E4               [12]  326 	clr	a
      001D73 3A               [12]  327 	addc	a,r2
      001D74 FC               [12]  328 	mov	r4,a
      001D75 8B 05            [24]  329 	mov	ar5,r3
      001D77 88 82            [24]  330 	mov	dpl,r0
      001D79 8C 83            [24]  331 	mov	dph,r4
      001D7B 8D F0            [24]  332 	mov	b,r5
      001D7D 12 96 F0         [24]  333 	lcall	__gptrget
      001D80 54 80            [12]  334 	anl	a,#0x80
      001D82 23               [12]  335 	rl	a
      001D83 54 01            [12]  336 	anl	a,#0x01
      001D85 85 0D 82         [24]  337 	mov	dpl,_BEACON_decoding_sloc1_1_0
      001D88 85 0E 83         [24]  338 	mov	dph,(_BEACON_decoding_sloc1_1_0 + 1)
      001D8B F0               [24]  339 	movx	@dptr,a
                                    340 ;	..\src\COMMON\Beacon.c:52: beacon->HMAC_type = (*(Rxbuffer+POS_SECURITY) & MASK_HMAC_TYPE)>>4;
      001D8C 74 07            [12]  341 	mov	a,#0x07
      001D8E 25 0F            [12]  342 	add	a,_BEACON_decoding_sloc2_1_0
      001D90 F5 0D            [12]  343 	mov	_BEACON_decoding_sloc1_1_0,a
      001D92 E4               [12]  344 	clr	a
      001D93 35 10            [12]  345 	addc	a,(_BEACON_decoding_sloc2_1_0 + 1)
      001D95 F5 0E            [12]  346 	mov	(_BEACON_decoding_sloc1_1_0 + 1),a
      001D97 88 82            [24]  347 	mov	dpl,r0
      001D99 8C 83            [24]  348 	mov	dph,r4
      001D9B 8D F0            [24]  349 	mov	b,r5
      001D9D 12 96 F0         [24]  350 	lcall	__gptrget
      001DA0 54 70            [12]  351 	anl	a,#0x70
      001DA2 C4               [12]  352 	swap	a
      001DA3 54 0F            [12]  353 	anl	a,#0x0f
      001DA5 85 0D 82         [24]  354 	mov	dpl,_BEACON_decoding_sloc1_1_0
      001DA8 85 0E 83         [24]  355 	mov	dph,(_BEACON_decoding_sloc1_1_0 + 1)
      001DAB F0               [24]  356 	movx	@dptr,a
                                    357 ;	..\src\COMMON\Beacon.c:53: beacon->Encryption_flag = (*(Rxbuffer+POS_SECURITY) & MASK_ENCRYPTION_FLAG)>>3;
      001DAC 74 08            [12]  358 	mov	a,#0x08
      001DAE 25 0F            [12]  359 	add	a,_BEACON_decoding_sloc2_1_0
      001DB0 F5 0D            [12]  360 	mov	_BEACON_decoding_sloc1_1_0,a
      001DB2 E4               [12]  361 	clr	a
      001DB3 35 10            [12]  362 	addc	a,(_BEACON_decoding_sloc2_1_0 + 1)
      001DB5 F5 0E            [12]  363 	mov	(_BEACON_decoding_sloc1_1_0 + 1),a
      001DB7 88 82            [24]  364 	mov	dpl,r0
      001DB9 8C 83            [24]  365 	mov	dph,r4
      001DBB 8D F0            [24]  366 	mov	b,r5
      001DBD 12 96 F0         [24]  367 	lcall	__gptrget
      001DC0 54 08            [12]  368 	anl	a,#0x08
      001DC2 C4               [12]  369 	swap	a
      001DC3 23               [12]  370 	rl	a
      001DC4 54 1F            [12]  371 	anl	a,#0x1f
      001DC6 85 0D 82         [24]  372 	mov	dpl,_BEACON_decoding_sloc1_1_0
      001DC9 85 0E 83         [24]  373 	mov	dph,(_BEACON_decoding_sloc1_1_0 + 1)
      001DCC F0               [24]  374 	movx	@dptr,a
                                    375 ;	..\src\COMMON\Beacon.c:54: beacon->Encryption_type = *(Rxbuffer+POS_SECURITY) & MASK_ENCRYPTION_TYPE;
      001DCD 74 09            [12]  376 	mov	a,#0x09
      001DCF 25 0F            [12]  377 	add	a,_BEACON_decoding_sloc2_1_0
      001DD1 FE               [12]  378 	mov	r6,a
      001DD2 E4               [12]  379 	clr	a
      001DD3 35 10            [12]  380 	addc	a,(_BEACON_decoding_sloc2_1_0 + 1)
      001DD5 FF               [12]  381 	mov	r7,a
      001DD6 88 82            [24]  382 	mov	dpl,r0
      001DD8 8C 83            [24]  383 	mov	dph,r4
      001DDA 8D F0            [24]  384 	mov	b,r5
      001DDC 12 96 F0         [24]  385 	lcall	__gptrget
      001DDF F8               [12]  386 	mov	r0,a
      001DE0 53 00 07         [24]  387 	anl	ar0,#0x07
      001DE3 8E 82            [24]  388 	mov	dpl,r6
      001DE5 8F 83            [24]  389 	mov	dph,r7
      001DE7 E8               [12]  390 	mov	a,r0
      001DE8 F0               [24]  391 	movx	@dptr,a
                                    392 ;	..\src\COMMON\Beacon.c:56: *(Rxbuffer+POS_NONCE)   = *(Rxbuffer+POS_NONCE) + *(Rxbuffer+POS_NONCE+1);
      001DE9 74 05            [12]  393 	mov	a,#0x05
      001DEB 29               [12]  394 	add	a,r1
      001DEC F5 11            [12]  395 	mov	_BEACON_decoding_sloc3_1_0,a
      001DEE E4               [12]  396 	clr	a
      001DEF 3A               [12]  397 	addc	a,r2
      001DF0 F5 12            [12]  398 	mov	(_BEACON_decoding_sloc3_1_0 + 1),a
      001DF2 8B 13            [24]  399 	mov	(_BEACON_decoding_sloc3_1_0 + 2),r3
      001DF4 85 11 82         [24]  400 	mov	dpl,_BEACON_decoding_sloc3_1_0
      001DF7 85 12 83         [24]  401 	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
      001DFA 85 13 F0         [24]  402 	mov	b,(_BEACON_decoding_sloc3_1_0 + 2)
      001DFD 12 96 F0         [24]  403 	lcall	__gptrget
      001E00 F5 0D            [12]  404 	mov	_BEACON_decoding_sloc1_1_0,a
      001E02 74 06            [12]  405 	mov	a,#0x06
      001E04 29               [12]  406 	add	a,r1
      001E05 F8               [12]  407 	mov	r0,a
      001E06 E4               [12]  408 	clr	a
      001E07 3A               [12]  409 	addc	a,r2
      001E08 FC               [12]  410 	mov	r4,a
      001E09 8B 07            [24]  411 	mov	ar7,r3
      001E0B 88 82            [24]  412 	mov	dpl,r0
      001E0D 8C 83            [24]  413 	mov	dph,r4
      001E0F 8F F0            [24]  414 	mov	b,r7
      001E11 12 96 F0         [24]  415 	lcall	__gptrget
      001E14 25 0D            [12]  416 	add	a,_BEACON_decoding_sloc1_1_0
      001E16 FE               [12]  417 	mov	r6,a
      001E17 85 11 82         [24]  418 	mov	dpl,_BEACON_decoding_sloc3_1_0
      001E1A 85 12 83         [24]  419 	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
      001E1D 85 13 F0         [24]  420 	mov	b,(_BEACON_decoding_sloc3_1_0 + 2)
      001E20 12 87 88         [24]  421 	lcall	__gptrput
                                    422 ;	..\src\COMMON\Beacon.c:57: *(Rxbuffer+POS_NONCE+1) = *(Rxbuffer+POS_NONCE) - *(Rxbuffer+POS_NONCE+1);
      001E23 88 82            [24]  423 	mov	dpl,r0
      001E25 8C 83            [24]  424 	mov	dph,r4
      001E27 8F F0            [24]  425 	mov	b,r7
      001E29 12 96 F0         [24]  426 	lcall	__gptrget
      001E2C FD               [12]  427 	mov	r5,a
      001E2D EE               [12]  428 	mov	a,r6
      001E2E C3               [12]  429 	clr	c
      001E2F 9D               [12]  430 	subb	a,r5
      001E30 FD               [12]  431 	mov	r5,a
      001E31 88 82            [24]  432 	mov	dpl,r0
      001E33 8C 83            [24]  433 	mov	dph,r4
      001E35 8F F0            [24]  434 	mov	b,r7
      001E37 12 87 88         [24]  435 	lcall	__gptrput
                                    436 ;	..\src\COMMON\Beacon.c:58: *(Rxbuffer+POS_NONCE)   = *(Rxbuffer+POS_NONCE) - *(Rxbuffer+POS_NONCE+1);
      001E3A EE               [12]  437 	mov	a,r6
      001E3B C3               [12]  438 	clr	c
      001E3C 9D               [12]  439 	subb	a,r5
      001E3D 85 11 82         [24]  440 	mov	dpl,_BEACON_decoding_sloc3_1_0
      001E40 85 12 83         [24]  441 	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
      001E43 85 13 F0         [24]  442 	mov	b,(_BEACON_decoding_sloc3_1_0 + 2)
      001E46 12 87 88         [24]  443 	lcall	__gptrput
                                    444 ;	..\src\COMMON\Beacon.c:60: memcpy(&beacon->Nonce,Rxbuffer+POS_NONCE,sizeof(uint16_t));
      001E49 74 0A            [12]  445 	mov	a,#0x0a
      001E4B 25 0F            [12]  446 	add	a,_BEACON_decoding_sloc2_1_0
      001E4D FE               [12]  447 	mov	r6,a
      001E4E E4               [12]  448 	clr	a
      001E4F 35 10            [12]  449 	addc	a,(_BEACON_decoding_sloc2_1_0 + 1)
      001E51 FF               [12]  450 	mov	r7,a
      001E52 8E 14            [24]  451 	mov	_BEACON_decoding_sloc4_1_0,r6
      001E54 8F 15            [24]  452 	mov	(_BEACON_decoding_sloc4_1_0 + 1),r7
      001E56 75 16 00         [24]  453 	mov	(_BEACON_decoding_sloc4_1_0 + 2),#0x00
      001E59 A8 11            [24]  454 	mov	r0,_BEACON_decoding_sloc3_1_0
      001E5B AC 12            [24]  455 	mov	r4,(_BEACON_decoding_sloc3_1_0 + 1)
      001E5D AF 13            [24]  456 	mov	r7,(_BEACON_decoding_sloc3_1_0 + 2)
      001E5F 90 04 3D         [24]  457 	mov	dptr,#_memcpy_PARM_2
      001E62 E8               [12]  458 	mov	a,r0
      001E63 F0               [24]  459 	movx	@dptr,a
      001E64 EC               [12]  460 	mov	a,r4
      001E65 A3               [24]  461 	inc	dptr
      001E66 F0               [24]  462 	movx	@dptr,a
      001E67 EF               [12]  463 	mov	a,r7
      001E68 A3               [24]  464 	inc	dptr
      001E69 F0               [24]  465 	movx	@dptr,a
      001E6A 90 04 40         [24]  466 	mov	dptr,#_memcpy_PARM_3
      001E6D 74 02            [12]  467 	mov	a,#0x02
      001E6F F0               [24]  468 	movx	@dptr,a
      001E70 E4               [12]  469 	clr	a
      001E71 A3               [24]  470 	inc	dptr
      001E72 F0               [24]  471 	movx	@dptr,a
      001E73 85 14 82         [24]  472 	mov	dpl,_BEACON_decoding_sloc4_1_0
      001E76 85 15 83         [24]  473 	mov	dph,(_BEACON_decoding_sloc4_1_0 + 1)
      001E79 85 16 F0         [24]  474 	mov	b,(_BEACON_decoding_sloc4_1_0 + 2)
      001E7C C0 03            [24]  475 	push	ar3
      001E7E C0 02            [24]  476 	push	ar2
      001E80 C0 01            [24]  477 	push	ar1
      001E82 12 83 01         [24]  478 	lcall	_memcpy
      001E85 D0 01            [24]  479 	pop	ar1
      001E87 D0 02            [24]  480 	pop	ar2
      001E89 D0 03            [24]  481 	pop	ar3
                                    482 ;	..\src\COMMON\Beacon.c:62: memcpy(&beacon->reserved,Rxbuffer+POS_RESERVED,sizeof(uint8_t)*5);
      001E8B 74 0C            [12]  483 	mov	a,#0x0c
      001E8D 25 0F            [12]  484 	add	a,_BEACON_decoding_sloc2_1_0
      001E8F FE               [12]  485 	mov	r6,a
      001E90 E4               [12]  486 	clr	a
      001E91 35 10            [12]  487 	addc	a,(_BEACON_decoding_sloc2_1_0 + 1)
      001E93 FF               [12]  488 	mov	r7,a
      001E94 8E 14            [24]  489 	mov	_BEACON_decoding_sloc4_1_0,r6
      001E96 8F 15            [24]  490 	mov	(_BEACON_decoding_sloc4_1_0 + 1),r7
      001E98 75 16 00         [24]  491 	mov	(_BEACON_decoding_sloc4_1_0 + 2),#0x00
      001E9B 74 07            [12]  492 	mov	a,#0x07
      001E9D 29               [12]  493 	add	a,r1
      001E9E F8               [12]  494 	mov	r0,a
      001E9F E4               [12]  495 	clr	a
      001EA0 3A               [12]  496 	addc	a,r2
      001EA1 FC               [12]  497 	mov	r4,a
      001EA2 8B 07            [24]  498 	mov	ar7,r3
      001EA4 90 04 3D         [24]  499 	mov	dptr,#_memcpy_PARM_2
      001EA7 E8               [12]  500 	mov	a,r0
      001EA8 F0               [24]  501 	movx	@dptr,a
      001EA9 EC               [12]  502 	mov	a,r4
      001EAA A3               [24]  503 	inc	dptr
      001EAB F0               [24]  504 	movx	@dptr,a
      001EAC EF               [12]  505 	mov	a,r7
      001EAD A3               [24]  506 	inc	dptr
      001EAE F0               [24]  507 	movx	@dptr,a
      001EAF 90 04 40         [24]  508 	mov	dptr,#_memcpy_PARM_3
      001EB2 74 05            [12]  509 	mov	a,#0x05
      001EB4 F0               [24]  510 	movx	@dptr,a
      001EB5 E4               [12]  511 	clr	a
      001EB6 A3               [24]  512 	inc	dptr
      001EB7 F0               [24]  513 	movx	@dptr,a
      001EB8 85 14 82         [24]  514 	mov	dpl,_BEACON_decoding_sloc4_1_0
      001EBB 85 15 83         [24]  515 	mov	dph,(_BEACON_decoding_sloc4_1_0 + 1)
      001EBE 85 16 F0         [24]  516 	mov	b,(_BEACON_decoding_sloc4_1_0 + 2)
      001EC1 C0 03            [24]  517 	push	ar3
      001EC3 C0 02            [24]  518 	push	ar2
      001EC5 C0 01            [24]  519 	push	ar1
      001EC7 12 83 01         [24]  520 	lcall	_memcpy
      001ECA D0 01            [24]  521 	pop	ar1
      001ECC D0 02            [24]  522 	pop	ar2
      001ECE D0 03            [24]  523 	pop	ar3
                                    524 ;	..\src\COMMON\Beacon.c:64: beacon->CRC =  *(Rxbuffer+POS_CRC) <<8 | *(Rxbuffer+POS_CRC+1);
      001ED0 74 11            [12]  525 	mov	a,#0x11
      001ED2 25 0F            [12]  526 	add	a,_BEACON_decoding_sloc2_1_0
      001ED4 F5 11            [12]  527 	mov	_BEACON_decoding_sloc3_1_0,a
      001ED6 E4               [12]  528 	clr	a
      001ED7 35 10            [12]  529 	addc	a,(_BEACON_decoding_sloc2_1_0 + 1)
      001ED9 F5 12            [12]  530 	mov	(_BEACON_decoding_sloc3_1_0 + 1),a
      001EDB 74 0C            [12]  531 	mov	a,#0x0c
      001EDD 29               [12]  532 	add	a,r1
      001EDE F8               [12]  533 	mov	r0,a
      001EDF E4               [12]  534 	clr	a
      001EE0 3A               [12]  535 	addc	a,r2
      001EE1 FC               [12]  536 	mov	r4,a
      001EE2 8B 05            [24]  537 	mov	ar5,r3
      001EE4 88 82            [24]  538 	mov	dpl,r0
      001EE6 8C 83            [24]  539 	mov	dph,r4
      001EE8 8D F0            [24]  540 	mov	b,r5
      001EEA 12 96 F0         [24]  541 	lcall	__gptrget
      001EED F8               [12]  542 	mov	r0,a
      001EEE 7D 00            [12]  543 	mov	r5,#0x00
      001EF0 88 15            [24]  544 	mov	(_BEACON_decoding_sloc4_1_0 + 1),r0
                                    545 ;	1-genFromRTrack replaced	mov	_BEACON_decoding_sloc4_1_0,#0x00
      001EF2 8D 14            [24]  546 	mov	_BEACON_decoding_sloc4_1_0,r5
      001EF4 74 0D            [12]  547 	mov	a,#0x0d
      001EF6 29               [12]  548 	add	a,r1
      001EF7 F8               [12]  549 	mov	r0,a
      001EF8 E4               [12]  550 	clr	a
      001EF9 3A               [12]  551 	addc	a,r2
      001EFA FC               [12]  552 	mov	r4,a
      001EFB 8B 05            [24]  553 	mov	ar5,r3
      001EFD 88 82            [24]  554 	mov	dpl,r0
      001EFF 8C 83            [24]  555 	mov	dph,r4
      001F01 8D F0            [24]  556 	mov	b,r5
      001F03 12 96 F0         [24]  557 	lcall	__gptrget
      001F06 F8               [12]  558 	mov	r0,a
      001F07 7D 00            [12]  559 	mov	r5,#0x00
      001F09 E5 14            [12]  560 	mov	a,_BEACON_decoding_sloc4_1_0
      001F0B 42 00            [12]  561 	orl	ar0,a
      001F0D E5 15            [12]  562 	mov	a,(_BEACON_decoding_sloc4_1_0 + 1)
      001F0F 42 05            [12]  563 	orl	ar5,a
      001F11 ED               [12]  564 	mov	a,r5
      001F12 FC               [12]  565 	mov	r4,a
      001F13 33               [12]  566 	rlc	a
      001F14 95 E0            [12]  567 	subb	a,acc
      001F16 FD               [12]  568 	mov	r5,a
      001F17 FF               [12]  569 	mov	r7,a
      001F18 85 11 82         [24]  570 	mov	dpl,_BEACON_decoding_sloc3_1_0
      001F1B 85 12 83         [24]  571 	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
      001F1E E8               [12]  572 	mov	a,r0
      001F1F F0               [24]  573 	movx	@dptr,a
      001F20 EC               [12]  574 	mov	a,r4
      001F21 A3               [24]  575 	inc	dptr
      001F22 F0               [24]  576 	movx	@dptr,a
      001F23 ED               [12]  577 	mov	a,r5
      001F24 A3               [24]  578 	inc	dptr
      001F25 F0               [24]  579 	movx	@dptr,a
      001F26 EF               [12]  580 	mov	a,r7
      001F27 A3               [24]  581 	inc	dptr
      001F28 F0               [24]  582 	movx	@dptr,a
                                    583 ;	..\src\COMMON\Beacon.c:65: beacon->CRC = beacon->CRC << 16;
      001F29 8C 1A            [24]  584 	mov	(_BEACON_decoding_sloc5_1_0 + 3),r4
      001F2B 88 19            [24]  585 	mov	(_BEACON_decoding_sloc5_1_0 + 2),r0
      001F2D 75 18 00         [24]  586 	mov	(_BEACON_decoding_sloc5_1_0 + 1),#0x00
      001F30 75 17 00         [24]  587 	mov	_BEACON_decoding_sloc5_1_0,#0x00
      001F33 85 11 82         [24]  588 	mov	dpl,_BEACON_decoding_sloc3_1_0
      001F36 85 12 83         [24]  589 	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
      001F39 E5 17            [12]  590 	mov	a,_BEACON_decoding_sloc5_1_0
      001F3B F0               [24]  591 	movx	@dptr,a
      001F3C E5 18            [12]  592 	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
      001F3E A3               [24]  593 	inc	dptr
      001F3F F0               [24]  594 	movx	@dptr,a
      001F40 E5 19            [12]  595 	mov	a,(_BEACON_decoding_sloc5_1_0 + 2)
      001F42 A3               [24]  596 	inc	dptr
      001F43 F0               [24]  597 	movx	@dptr,a
      001F44 E5 1A            [12]  598 	mov	a,(_BEACON_decoding_sloc5_1_0 + 3)
      001F46 A3               [24]  599 	inc	dptr
      001F47 F0               [24]  600 	movx	@dptr,a
                                    601 ;	..\src\COMMON\Beacon.c:66: beacon->CRC = beacon->CRC | *(Rxbuffer+POS_CRC+2)<<8 | *(Rxbuffer+POS_CRC+3);
      001F48 74 0E            [12]  602 	mov	a,#0x0e
      001F4A 29               [12]  603 	add	a,r1
      001F4B FD               [12]  604 	mov	r5,a
      001F4C E4               [12]  605 	clr	a
      001F4D 3A               [12]  606 	addc	a,r2
      001F4E FE               [12]  607 	mov	r6,a
      001F4F 8B 07            [24]  608 	mov	ar7,r3
      001F51 8D 82            [24]  609 	mov	dpl,r5
      001F53 8E 83            [24]  610 	mov	dph,r6
      001F55 8F F0            [24]  611 	mov	b,r7
      001F57 12 96 F0         [24]  612 	lcall	__gptrget
      001F5A FF               [12]  613 	mov	r7,a
      001F5B 7D 00            [12]  614 	mov	r5,#0x00
      001F5D 33               [12]  615 	rlc	a
      001F5E 95 E0            [12]  616 	subb	a,acc
      001F60 FE               [12]  617 	mov	r6,a
      001F61 FC               [12]  618 	mov	r4,a
      001F62 ED               [12]  619 	mov	a,r5
      001F63 42 17            [12]  620 	orl	_BEACON_decoding_sloc5_1_0,a
      001F65 EF               [12]  621 	mov	a,r7
      001F66 42 18            [12]  622 	orl	(_BEACON_decoding_sloc5_1_0 + 1),a
      001F68 EE               [12]  623 	mov	a,r6
      001F69 42 19            [12]  624 	orl	(_BEACON_decoding_sloc5_1_0 + 2),a
      001F6B EC               [12]  625 	mov	a,r4
      001F6C 42 1A            [12]  626 	orl	(_BEACON_decoding_sloc5_1_0 + 3),a
      001F6E 74 0F            [12]  627 	mov	a,#0x0f
      001F70 29               [12]  628 	add	a,r1
      001F71 F8               [12]  629 	mov	r0,a
      001F72 E4               [12]  630 	clr	a
      001F73 3A               [12]  631 	addc	a,r2
      001F74 FE               [12]  632 	mov	r6,a
      001F75 8B 07            [24]  633 	mov	ar7,r3
      001F77 88 82            [24]  634 	mov	dpl,r0
      001F79 8E 83            [24]  635 	mov	dph,r6
      001F7B 8F F0            [24]  636 	mov	b,r7
      001F7D 12 96 F0         [24]  637 	lcall	__gptrget
      001F80 F8               [12]  638 	mov	r0,a
      001F81 E4               [12]  639 	clr	a
      001F82 FF               [12]  640 	mov	r7,a
      001F83 FE               [12]  641 	mov	r6,a
      001F84 FD               [12]  642 	mov	r5,a
      001F85 E5 17            [12]  643 	mov	a,_BEACON_decoding_sloc5_1_0
      001F87 42 00            [12]  644 	orl	ar0,a
      001F89 E5 18            [12]  645 	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
      001F8B 42 07            [12]  646 	orl	ar7,a
      001F8D E5 19            [12]  647 	mov	a,(_BEACON_decoding_sloc5_1_0 + 2)
      001F8F 42 06            [12]  648 	orl	ar6,a
      001F91 E5 1A            [12]  649 	mov	a,(_BEACON_decoding_sloc5_1_0 + 3)
      001F93 42 05            [12]  650 	orl	ar5,a
      001F95 85 11 82         [24]  651 	mov	dpl,_BEACON_decoding_sloc3_1_0
      001F98 85 12 83         [24]  652 	mov	dph,(_BEACON_decoding_sloc3_1_0 + 1)
      001F9B E8               [12]  653 	mov	a,r0
      001F9C F0               [24]  654 	movx	@dptr,a
      001F9D EF               [12]  655 	mov	a,r7
      001F9E A3               [24]  656 	inc	dptr
      001F9F F0               [24]  657 	movx	@dptr,a
      001FA0 EE               [12]  658 	mov	a,r6
      001FA1 A3               [24]  659 	inc	dptr
      001FA2 F0               [24]  660 	movx	@dptr,a
      001FA3 ED               [12]  661 	mov	a,r5
      001FA4 A3               [24]  662 	inc	dptr
      001FA5 F0               [24]  663 	movx	@dptr,a
                                    664 ;	..\src\COMMON\Beacon.c:68: beacon->HMAC_Msb = *(Rxbuffer+POS_HMAC_MSB)<<8 | *(Rxbuffer+POS_HMAC_MSB+1);
      001FA6 74 15            [12]  665 	mov	a,#0x15
      001FA8 25 0F            [12]  666 	add	a,_BEACON_decoding_sloc2_1_0
      001FAA F5 14            [12]  667 	mov	_BEACON_decoding_sloc4_1_0,a
      001FAC E4               [12]  668 	clr	a
      001FAD 35 10            [12]  669 	addc	a,(_BEACON_decoding_sloc2_1_0 + 1)
      001FAF F5 15            [12]  670 	mov	(_BEACON_decoding_sloc4_1_0 + 1),a
      001FB1 74 10            [12]  671 	mov	a,#0x10
      001FB3 29               [12]  672 	add	a,r1
      001FB4 F8               [12]  673 	mov	r0,a
      001FB5 E4               [12]  674 	clr	a
      001FB6 3A               [12]  675 	addc	a,r2
      001FB7 FC               [12]  676 	mov	r4,a
      001FB8 8B 05            [24]  677 	mov	ar5,r3
      001FBA 88 82            [24]  678 	mov	dpl,r0
      001FBC 8C 83            [24]  679 	mov	dph,r4
      001FBE 8D F0            [24]  680 	mov	b,r5
      001FC0 12 96 F0         [24]  681 	lcall	__gptrget
      001FC3 F8               [12]  682 	mov	r0,a
      001FC4 7D 00            [12]  683 	mov	r5,#0x00
      001FC6 88 18            [24]  684 	mov	(_BEACON_decoding_sloc5_1_0 + 1),r0
                                    685 ;	1-genFromRTrack replaced	mov	_BEACON_decoding_sloc5_1_0,#0x00
      001FC8 8D 17            [24]  686 	mov	_BEACON_decoding_sloc5_1_0,r5
      001FCA 74 11            [12]  687 	mov	a,#0x11
      001FCC 29               [12]  688 	add	a,r1
      001FCD F8               [12]  689 	mov	r0,a
      001FCE E4               [12]  690 	clr	a
      001FCF 3A               [12]  691 	addc	a,r2
      001FD0 FC               [12]  692 	mov	r4,a
      001FD1 8B 05            [24]  693 	mov	ar5,r3
      001FD3 88 82            [24]  694 	mov	dpl,r0
      001FD5 8C 83            [24]  695 	mov	dph,r4
      001FD7 8D F0            [24]  696 	mov	b,r5
      001FD9 12 96 F0         [24]  697 	lcall	__gptrget
      001FDC F8               [12]  698 	mov	r0,a
      001FDD 7D 00            [12]  699 	mov	r5,#0x00
      001FDF E5 17            [12]  700 	mov	a,_BEACON_decoding_sloc5_1_0
      001FE1 42 00            [12]  701 	orl	ar0,a
      001FE3 E5 18            [12]  702 	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
      001FE5 42 05            [12]  703 	orl	ar5,a
      001FE7 ED               [12]  704 	mov	a,r5
      001FE8 FC               [12]  705 	mov	r4,a
      001FE9 33               [12]  706 	rlc	a
      001FEA 95 E0            [12]  707 	subb	a,acc
      001FEC FD               [12]  708 	mov	r5,a
      001FED FF               [12]  709 	mov	r7,a
      001FEE 85 14 82         [24]  710 	mov	dpl,_BEACON_decoding_sloc4_1_0
      001FF1 85 15 83         [24]  711 	mov	dph,(_BEACON_decoding_sloc4_1_0 + 1)
      001FF4 E8               [12]  712 	mov	a,r0
      001FF5 F0               [24]  713 	movx	@dptr,a
      001FF6 EC               [12]  714 	mov	a,r4
      001FF7 A3               [24]  715 	inc	dptr
      001FF8 F0               [24]  716 	movx	@dptr,a
      001FF9 ED               [12]  717 	mov	a,r5
      001FFA A3               [24]  718 	inc	dptr
      001FFB F0               [24]  719 	movx	@dptr,a
      001FFC EF               [12]  720 	mov	a,r7
      001FFD A3               [24]  721 	inc	dptr
      001FFE F0               [24]  722 	movx	@dptr,a
                                    723 ;	..\src\COMMON\Beacon.c:69: beacon->HMAC_Msb = beacon->HMAC_Msb << 16;
      001FFF 8C 1A            [24]  724 	mov	(_BEACON_decoding_sloc5_1_0 + 3),r4
      002001 88 19            [24]  725 	mov	(_BEACON_decoding_sloc5_1_0 + 2),r0
      002003 75 18 00         [24]  726 	mov	(_BEACON_decoding_sloc5_1_0 + 1),#0x00
      002006 75 17 00         [24]  727 	mov	_BEACON_decoding_sloc5_1_0,#0x00
      002009 85 14 82         [24]  728 	mov	dpl,_BEACON_decoding_sloc4_1_0
      00200C 85 15 83         [24]  729 	mov	dph,(_BEACON_decoding_sloc4_1_0 + 1)
      00200F E5 17            [12]  730 	mov	a,_BEACON_decoding_sloc5_1_0
      002011 F0               [24]  731 	movx	@dptr,a
      002012 E5 18            [12]  732 	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
      002014 A3               [24]  733 	inc	dptr
      002015 F0               [24]  734 	movx	@dptr,a
      002016 E5 19            [12]  735 	mov	a,(_BEACON_decoding_sloc5_1_0 + 2)
      002018 A3               [24]  736 	inc	dptr
      002019 F0               [24]  737 	movx	@dptr,a
      00201A E5 1A            [12]  738 	mov	a,(_BEACON_decoding_sloc5_1_0 + 3)
      00201C A3               [24]  739 	inc	dptr
      00201D F0               [24]  740 	movx	@dptr,a
                                    741 ;	..\src\COMMON\Beacon.c:70: beacon->HMAC_Msb = beacon->HMAC_Msb | (*(Rxbuffer+POS_HMAC_MSB+2)<<8 | *(Rxbuffer+POS_HMAC_MSB+3) & 0x0000FFFF);
      00201E 74 12            [12]  742 	mov	a,#0x12
      002020 29               [12]  743 	add	a,r1
      002021 FD               [12]  744 	mov	r5,a
      002022 E4               [12]  745 	clr	a
      002023 3A               [12]  746 	addc	a,r2
      002024 FE               [12]  747 	mov	r6,a
      002025 8B 07            [24]  748 	mov	ar7,r3
      002027 8D 82            [24]  749 	mov	dpl,r5
      002029 8E 83            [24]  750 	mov	dph,r6
      00202B 8F F0            [24]  751 	mov	b,r7
      00202D 12 96 F0         [24]  752 	lcall	__gptrget
      002030 FF               [12]  753 	mov	r7,a
      002031 7D 00            [12]  754 	mov	r5,#0x00
      002033 74 13            [12]  755 	mov	a,#0x13
      002035 29               [12]  756 	add	a,r1
      002036 F8               [12]  757 	mov	r0,a
      002037 E4               [12]  758 	clr	a
      002038 3A               [12]  759 	addc	a,r2
      002039 FC               [12]  760 	mov	r4,a
      00203A 8B 06            [24]  761 	mov	ar6,r3
      00203C 88 82            [24]  762 	mov	dpl,r0
      00203E 8C 83            [24]  763 	mov	dph,r4
      002040 8E F0            [24]  764 	mov	b,r6
      002042 12 96 F0         [24]  765 	lcall	__gptrget
      002045 FE               [12]  766 	mov	r6,a
      002046 78 00            [12]  767 	mov	r0,#0x00
      002048 ED               [12]  768 	mov	a,r5
      002049 42 06            [12]  769 	orl	ar6,a
      00204B EF               [12]  770 	mov	a,r7
      00204C 42 00            [12]  771 	orl	ar0,a
      00204E E4               [12]  772 	clr	a
      00204F FF               [12]  773 	mov	r7,a
      002050 FD               [12]  774 	mov	r5,a
      002051 E5 17            [12]  775 	mov	a,_BEACON_decoding_sloc5_1_0
      002053 42 06            [12]  776 	orl	ar6,a
      002055 E5 18            [12]  777 	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
      002057 42 00            [12]  778 	orl	ar0,a
      002059 E5 19            [12]  779 	mov	a,(_BEACON_decoding_sloc5_1_0 + 2)
      00205B 42 07            [12]  780 	orl	ar7,a
      00205D E5 1A            [12]  781 	mov	a,(_BEACON_decoding_sloc5_1_0 + 3)
      00205F 42 05            [12]  782 	orl	ar5,a
      002061 85 14 82         [24]  783 	mov	dpl,_BEACON_decoding_sloc4_1_0
      002064 85 15 83         [24]  784 	mov	dph,(_BEACON_decoding_sloc4_1_0 + 1)
      002067 EE               [12]  785 	mov	a,r6
      002068 F0               [24]  786 	movx	@dptr,a
      002069 E8               [12]  787 	mov	a,r0
      00206A A3               [24]  788 	inc	dptr
      00206B F0               [24]  789 	movx	@dptr,a
      00206C EF               [12]  790 	mov	a,r7
      00206D A3               [24]  791 	inc	dptr
      00206E F0               [24]  792 	movx	@dptr,a
      00206F ED               [12]  793 	mov	a,r5
      002070 A3               [24]  794 	inc	dptr
      002071 F0               [24]  795 	movx	@dptr,a
                                    796 ;	..\src\COMMON\Beacon.c:72: beacon->HMAC_Lsb = *(Rxbuffer+POS_HMAC_LSB)<<8 | *(Rxbuffer+POS_HMAC_LSB+1);
      002072 74 19            [12]  797 	mov	a,#0x19
      002074 25 0F            [12]  798 	add	a,_BEACON_decoding_sloc2_1_0
      002076 F5 14            [12]  799 	mov	_BEACON_decoding_sloc4_1_0,a
      002078 E4               [12]  800 	clr	a
      002079 35 10            [12]  801 	addc	a,(_BEACON_decoding_sloc2_1_0 + 1)
      00207B F5 15            [12]  802 	mov	(_BEACON_decoding_sloc4_1_0 + 1),a
      00207D 74 14            [12]  803 	mov	a,#0x14
      00207F 29               [12]  804 	add	a,r1
      002080 F8               [12]  805 	mov	r0,a
      002081 E4               [12]  806 	clr	a
      002082 3A               [12]  807 	addc	a,r2
      002083 FC               [12]  808 	mov	r4,a
      002084 8B 05            [24]  809 	mov	ar5,r3
      002086 88 82            [24]  810 	mov	dpl,r0
      002088 8C 83            [24]  811 	mov	dph,r4
      00208A 8D F0            [24]  812 	mov	b,r5
      00208C 12 96 F0         [24]  813 	lcall	__gptrget
      00208F F8               [12]  814 	mov	r0,a
      002090 7D 00            [12]  815 	mov	r5,#0x00
      002092 88 18            [24]  816 	mov	(_BEACON_decoding_sloc5_1_0 + 1),r0
                                    817 ;	1-genFromRTrack replaced	mov	_BEACON_decoding_sloc5_1_0,#0x00
      002094 8D 17            [24]  818 	mov	_BEACON_decoding_sloc5_1_0,r5
      002096 74 15            [12]  819 	mov	a,#0x15
      002098 29               [12]  820 	add	a,r1
      002099 F8               [12]  821 	mov	r0,a
      00209A E4               [12]  822 	clr	a
      00209B 3A               [12]  823 	addc	a,r2
      00209C FC               [12]  824 	mov	r4,a
      00209D 8B 05            [24]  825 	mov	ar5,r3
      00209F 88 82            [24]  826 	mov	dpl,r0
      0020A1 8C 83            [24]  827 	mov	dph,r4
      0020A3 8D F0            [24]  828 	mov	b,r5
      0020A5 12 96 F0         [24]  829 	lcall	__gptrget
      0020A8 F8               [12]  830 	mov	r0,a
      0020A9 7D 00            [12]  831 	mov	r5,#0x00
      0020AB E5 17            [12]  832 	mov	a,_BEACON_decoding_sloc5_1_0
      0020AD 42 00            [12]  833 	orl	ar0,a
      0020AF E5 18            [12]  834 	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
      0020B1 42 05            [12]  835 	orl	ar5,a
      0020B3 ED               [12]  836 	mov	a,r5
      0020B4 FC               [12]  837 	mov	r4,a
      0020B5 33               [12]  838 	rlc	a
      0020B6 95 E0            [12]  839 	subb	a,acc
      0020B8 FD               [12]  840 	mov	r5,a
      0020B9 FF               [12]  841 	mov	r7,a
      0020BA 85 14 82         [24]  842 	mov	dpl,_BEACON_decoding_sloc4_1_0
      0020BD 85 15 83         [24]  843 	mov	dph,(_BEACON_decoding_sloc4_1_0 + 1)
      0020C0 E8               [12]  844 	mov	a,r0
      0020C1 F0               [24]  845 	movx	@dptr,a
      0020C2 EC               [12]  846 	mov	a,r4
      0020C3 A3               [24]  847 	inc	dptr
      0020C4 F0               [24]  848 	movx	@dptr,a
      0020C5 ED               [12]  849 	mov	a,r5
      0020C6 A3               [24]  850 	inc	dptr
      0020C7 F0               [24]  851 	movx	@dptr,a
      0020C8 EF               [12]  852 	mov	a,r7
      0020C9 A3               [24]  853 	inc	dptr
      0020CA F0               [24]  854 	movx	@dptr,a
                                    855 ;	..\src\COMMON\Beacon.c:73: beacon->HMAC_Lsb = beacon->HMAC_Lsb << 16;
      0020CB 8C 1A            [24]  856 	mov	(_BEACON_decoding_sloc5_1_0 + 3),r4
      0020CD 88 19            [24]  857 	mov	(_BEACON_decoding_sloc5_1_0 + 2),r0
      0020CF 75 18 00         [24]  858 	mov	(_BEACON_decoding_sloc5_1_0 + 1),#0x00
      0020D2 75 17 00         [24]  859 	mov	_BEACON_decoding_sloc5_1_0,#0x00
      0020D5 85 14 82         [24]  860 	mov	dpl,_BEACON_decoding_sloc4_1_0
      0020D8 85 15 83         [24]  861 	mov	dph,(_BEACON_decoding_sloc4_1_0 + 1)
      0020DB E5 17            [12]  862 	mov	a,_BEACON_decoding_sloc5_1_0
      0020DD F0               [24]  863 	movx	@dptr,a
      0020DE E5 18            [12]  864 	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
      0020E0 A3               [24]  865 	inc	dptr
      0020E1 F0               [24]  866 	movx	@dptr,a
      0020E2 E5 19            [12]  867 	mov	a,(_BEACON_decoding_sloc5_1_0 + 2)
      0020E4 A3               [24]  868 	inc	dptr
      0020E5 F0               [24]  869 	movx	@dptr,a
      0020E6 E5 1A            [12]  870 	mov	a,(_BEACON_decoding_sloc5_1_0 + 3)
      0020E8 A3               [24]  871 	inc	dptr
      0020E9 F0               [24]  872 	movx	@dptr,a
                                    873 ;	..\src\COMMON\Beacon.c:74: beacon->HMAC_Lsb = beacon->HMAC_Lsb | (*(Rxbuffer+POS_HMAC_LSB+2)<<8 | *(Rxbuffer+POS_HMAC_LSB+3) & 0x0000FFFF);
      0020EA 74 16            [12]  874 	mov	a,#0x16
      0020EC 29               [12]  875 	add	a,r1
      0020ED FD               [12]  876 	mov	r5,a
      0020EE E4               [12]  877 	clr	a
      0020EF 3A               [12]  878 	addc	a,r2
      0020F0 FE               [12]  879 	mov	r6,a
      0020F1 8B 07            [24]  880 	mov	ar7,r3
      0020F3 8D 82            [24]  881 	mov	dpl,r5
      0020F5 8E 83            [24]  882 	mov	dph,r6
      0020F7 8F F0            [24]  883 	mov	b,r7
      0020F9 12 96 F0         [24]  884 	lcall	__gptrget
      0020FC FF               [12]  885 	mov	r7,a
      0020FD 7D 00            [12]  886 	mov	r5,#0x00
      0020FF 74 17            [12]  887 	mov	a,#0x17
      002101 29               [12]  888 	add	a,r1
      002102 F9               [12]  889 	mov	r1,a
      002103 E4               [12]  890 	clr	a
      002104 3A               [12]  891 	addc	a,r2
      002105 FA               [12]  892 	mov	r2,a
      002106 89 82            [24]  893 	mov	dpl,r1
      002108 8A 83            [24]  894 	mov	dph,r2
      00210A 8B F0            [24]  895 	mov	b,r3
      00210C 12 96 F0         [24]  896 	lcall	__gptrget
      00210F FE               [12]  897 	mov	r6,a
      002110 79 00            [12]  898 	mov	r1,#0x00
      002112 ED               [12]  899 	mov	a,r5
      002113 42 06            [12]  900 	orl	ar6,a
      002115 EF               [12]  901 	mov	a,r7
      002116 42 01            [12]  902 	orl	ar1,a
      002118 E4               [12]  903 	clr	a
      002119 FF               [12]  904 	mov	r7,a
      00211A FD               [12]  905 	mov	r5,a
      00211B E5 17            [12]  906 	mov	a,_BEACON_decoding_sloc5_1_0
      00211D 42 06            [12]  907 	orl	ar6,a
      00211F E5 18            [12]  908 	mov	a,(_BEACON_decoding_sloc5_1_0 + 1)
      002121 42 01            [12]  909 	orl	ar1,a
      002123 E5 19            [12]  910 	mov	a,(_BEACON_decoding_sloc5_1_0 + 2)
      002125 42 07            [12]  911 	orl	ar7,a
      002127 E5 1A            [12]  912 	mov	a,(_BEACON_decoding_sloc5_1_0 + 3)
      002129 42 05            [12]  913 	orl	ar5,a
      00212B 85 14 82         [24]  914 	mov	dpl,_BEACON_decoding_sloc4_1_0
      00212E 85 15 83         [24]  915 	mov	dph,(_BEACON_decoding_sloc4_1_0 + 1)
      002131 EE               [12]  916 	mov	a,r6
      002132 F0               [24]  917 	movx	@dptr,a
      002133 E9               [12]  918 	mov	a,r1
      002134 A3               [24]  919 	inc	dptr
      002135 F0               [24]  920 	movx	@dptr,a
      002136 EF               [12]  921 	mov	a,r7
      002137 A3               [24]  922 	inc	dptr
      002138 F0               [24]  923 	movx	@dptr,a
      002139 ED               [12]  924 	mov	a,r5
      00213A A3               [24]  925 	inc	dptr
      00213B F0               [24]  926 	movx	@dptr,a
      00213C 22               [24]  927 	ret
      00213D                        928 00108$:
                                    929 ;	..\src\COMMON\Beacon.c:75: }else beacon->FlagReceived = INVALID_BEACON;
      00213D 85 0F 82         [24]  930 	mov	dpl,_BEACON_decoding_sloc2_1_0
      002140 85 10 83         [24]  931 	mov	dph,(_BEACON_decoding_sloc2_1_0 + 1)
      002143 E4               [12]  932 	clr	a
      002144 F0               [24]  933 	movx	@dptr,a
      002145 22               [24]  934 	ret
                                    935 	.area CSEG    (CODE)
                                    936 	.area CONST   (CODE)
                                    937 	.area XINIT   (CODE)
                                    938 	.area CABS    (ABS,CODE)
