                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.6.0 #9615 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module GoldSequence
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _dbglink_writehex32
                                     12 	.globl _dbglink_writestr
                                     13 	.globl _flash_read
                                     14 	.globl _flash_write
                                     15 	.globl _flash_lock
                                     16 	.globl _flash_unlock
                                     17 	.globl _random
                                     18 	.globl _PORTC_7
                                     19 	.globl _PORTC_6
                                     20 	.globl _PORTC_5
                                     21 	.globl _PORTC_4
                                     22 	.globl _PORTC_3
                                     23 	.globl _PORTC_2
                                     24 	.globl _PORTC_1
                                     25 	.globl _PORTC_0
                                     26 	.globl _PORTB_7
                                     27 	.globl _PORTB_6
                                     28 	.globl _PORTB_5
                                     29 	.globl _PORTB_4
                                     30 	.globl _PORTB_3
                                     31 	.globl _PORTB_2
                                     32 	.globl _PORTB_1
                                     33 	.globl _PORTB_0
                                     34 	.globl _PORTA_7
                                     35 	.globl _PORTA_6
                                     36 	.globl _PORTA_5
                                     37 	.globl _PORTA_4
                                     38 	.globl _PORTA_3
                                     39 	.globl _PORTA_2
                                     40 	.globl _PORTA_1
                                     41 	.globl _PORTA_0
                                     42 	.globl _PINC_7
                                     43 	.globl _PINC_6
                                     44 	.globl _PINC_5
                                     45 	.globl _PINC_4
                                     46 	.globl _PINC_3
                                     47 	.globl _PINC_2
                                     48 	.globl _PINC_1
                                     49 	.globl _PINC_0
                                     50 	.globl _PINB_7
                                     51 	.globl _PINB_6
                                     52 	.globl _PINB_5
                                     53 	.globl _PINB_4
                                     54 	.globl _PINB_3
                                     55 	.globl _PINB_2
                                     56 	.globl _PINB_1
                                     57 	.globl _PINB_0
                                     58 	.globl _PINA_7
                                     59 	.globl _PINA_6
                                     60 	.globl _PINA_5
                                     61 	.globl _PINA_4
                                     62 	.globl _PINA_3
                                     63 	.globl _PINA_2
                                     64 	.globl _PINA_1
                                     65 	.globl _PINA_0
                                     66 	.globl _CY
                                     67 	.globl _AC
                                     68 	.globl _F0
                                     69 	.globl _RS1
                                     70 	.globl _RS0
                                     71 	.globl _OV
                                     72 	.globl _F1
                                     73 	.globl _P
                                     74 	.globl _IP_7
                                     75 	.globl _IP_6
                                     76 	.globl _IP_5
                                     77 	.globl _IP_4
                                     78 	.globl _IP_3
                                     79 	.globl _IP_2
                                     80 	.globl _IP_1
                                     81 	.globl _IP_0
                                     82 	.globl _EA
                                     83 	.globl _IE_7
                                     84 	.globl _IE_6
                                     85 	.globl _IE_5
                                     86 	.globl _IE_4
                                     87 	.globl _IE_3
                                     88 	.globl _IE_2
                                     89 	.globl _IE_1
                                     90 	.globl _IE_0
                                     91 	.globl _EIP_7
                                     92 	.globl _EIP_6
                                     93 	.globl _EIP_5
                                     94 	.globl _EIP_4
                                     95 	.globl _EIP_3
                                     96 	.globl _EIP_2
                                     97 	.globl _EIP_1
                                     98 	.globl _EIP_0
                                     99 	.globl _EIE_7
                                    100 	.globl _EIE_6
                                    101 	.globl _EIE_5
                                    102 	.globl _EIE_4
                                    103 	.globl _EIE_3
                                    104 	.globl _EIE_2
                                    105 	.globl _EIE_1
                                    106 	.globl _EIE_0
                                    107 	.globl _E2IP_7
                                    108 	.globl _E2IP_6
                                    109 	.globl _E2IP_5
                                    110 	.globl _E2IP_4
                                    111 	.globl _E2IP_3
                                    112 	.globl _E2IP_2
                                    113 	.globl _E2IP_1
                                    114 	.globl _E2IP_0
                                    115 	.globl _E2IE_7
                                    116 	.globl _E2IE_6
                                    117 	.globl _E2IE_5
                                    118 	.globl _E2IE_4
                                    119 	.globl _E2IE_3
                                    120 	.globl _E2IE_2
                                    121 	.globl _E2IE_1
                                    122 	.globl _E2IE_0
                                    123 	.globl _B_7
                                    124 	.globl _B_6
                                    125 	.globl _B_5
                                    126 	.globl _B_4
                                    127 	.globl _B_3
                                    128 	.globl _B_2
                                    129 	.globl _B_1
                                    130 	.globl _B_0
                                    131 	.globl _ACC_7
                                    132 	.globl _ACC_6
                                    133 	.globl _ACC_5
                                    134 	.globl _ACC_4
                                    135 	.globl _ACC_3
                                    136 	.globl _ACC_2
                                    137 	.globl _ACC_1
                                    138 	.globl _ACC_0
                                    139 	.globl _WTSTAT
                                    140 	.globl _WTIRQEN
                                    141 	.globl _WTEVTD
                                    142 	.globl _WTEVTD1
                                    143 	.globl _WTEVTD0
                                    144 	.globl _WTEVTC
                                    145 	.globl _WTEVTC1
                                    146 	.globl _WTEVTC0
                                    147 	.globl _WTEVTB
                                    148 	.globl _WTEVTB1
                                    149 	.globl _WTEVTB0
                                    150 	.globl _WTEVTA
                                    151 	.globl _WTEVTA1
                                    152 	.globl _WTEVTA0
                                    153 	.globl _WTCNTR1
                                    154 	.globl _WTCNTB
                                    155 	.globl _WTCNTB1
                                    156 	.globl _WTCNTB0
                                    157 	.globl _WTCNTA
                                    158 	.globl _WTCNTA1
                                    159 	.globl _WTCNTA0
                                    160 	.globl _WTCFGB
                                    161 	.globl _WTCFGA
                                    162 	.globl _WDTRESET
                                    163 	.globl _WDTCFG
                                    164 	.globl _U1STATUS
                                    165 	.globl _U1SHREG
                                    166 	.globl _U1MODE
                                    167 	.globl _U1CTRL
                                    168 	.globl _U0STATUS
                                    169 	.globl _U0SHREG
                                    170 	.globl _U0MODE
                                    171 	.globl _U0CTRL
                                    172 	.globl _T2STATUS
                                    173 	.globl _T2PERIOD
                                    174 	.globl _T2PERIOD1
                                    175 	.globl _T2PERIOD0
                                    176 	.globl _T2MODE
                                    177 	.globl _T2CNT
                                    178 	.globl _T2CNT1
                                    179 	.globl _T2CNT0
                                    180 	.globl _T2CLKSRC
                                    181 	.globl _T1STATUS
                                    182 	.globl _T1PERIOD
                                    183 	.globl _T1PERIOD1
                                    184 	.globl _T1PERIOD0
                                    185 	.globl _T1MODE
                                    186 	.globl _T1CNT
                                    187 	.globl _T1CNT1
                                    188 	.globl _T1CNT0
                                    189 	.globl _T1CLKSRC
                                    190 	.globl _T0STATUS
                                    191 	.globl _T0PERIOD
                                    192 	.globl _T0PERIOD1
                                    193 	.globl _T0PERIOD0
                                    194 	.globl _T0MODE
                                    195 	.globl _T0CNT
                                    196 	.globl _T0CNT1
                                    197 	.globl _T0CNT0
                                    198 	.globl _T0CLKSRC
                                    199 	.globl _SPSTATUS
                                    200 	.globl _SPSHREG
                                    201 	.globl _SPMODE
                                    202 	.globl _SPCLKSRC
                                    203 	.globl _RADIOSTAT
                                    204 	.globl _RADIOSTAT1
                                    205 	.globl _RADIOSTAT0
                                    206 	.globl _RADIODATA
                                    207 	.globl _RADIODATA3
                                    208 	.globl _RADIODATA2
                                    209 	.globl _RADIODATA1
                                    210 	.globl _RADIODATA0
                                    211 	.globl _RADIOADDR
                                    212 	.globl _RADIOADDR1
                                    213 	.globl _RADIOADDR0
                                    214 	.globl _RADIOACC
                                    215 	.globl _OC1STATUS
                                    216 	.globl _OC1PIN
                                    217 	.globl _OC1MODE
                                    218 	.globl _OC1COMP
                                    219 	.globl _OC1COMP1
                                    220 	.globl _OC1COMP0
                                    221 	.globl _OC0STATUS
                                    222 	.globl _OC0PIN
                                    223 	.globl _OC0MODE
                                    224 	.globl _OC0COMP
                                    225 	.globl _OC0COMP1
                                    226 	.globl _OC0COMP0
                                    227 	.globl _NVSTATUS
                                    228 	.globl _NVKEY
                                    229 	.globl _NVDATA
                                    230 	.globl _NVDATA1
                                    231 	.globl _NVDATA0
                                    232 	.globl _NVADDR
                                    233 	.globl _NVADDR1
                                    234 	.globl _NVADDR0
                                    235 	.globl _IC1STATUS
                                    236 	.globl _IC1MODE
                                    237 	.globl _IC1CAPT
                                    238 	.globl _IC1CAPT1
                                    239 	.globl _IC1CAPT0
                                    240 	.globl _IC0STATUS
                                    241 	.globl _IC0MODE
                                    242 	.globl _IC0CAPT
                                    243 	.globl _IC0CAPT1
                                    244 	.globl _IC0CAPT0
                                    245 	.globl _PORTR
                                    246 	.globl _PORTC
                                    247 	.globl _PORTB
                                    248 	.globl _PORTA
                                    249 	.globl _PINR
                                    250 	.globl _PINC
                                    251 	.globl _PINB
                                    252 	.globl _PINA
                                    253 	.globl _DIRR
                                    254 	.globl _DIRC
                                    255 	.globl _DIRB
                                    256 	.globl _DIRA
                                    257 	.globl _DBGLNKSTAT
                                    258 	.globl _DBGLNKBUF
                                    259 	.globl _CODECONFIG
                                    260 	.globl _CLKSTAT
                                    261 	.globl _CLKCON
                                    262 	.globl _ANALOGCOMP
                                    263 	.globl _ADCCONV
                                    264 	.globl _ADCCLKSRC
                                    265 	.globl _ADCCH3CONFIG
                                    266 	.globl _ADCCH2CONFIG
                                    267 	.globl _ADCCH1CONFIG
                                    268 	.globl _ADCCH0CONFIG
                                    269 	.globl __XPAGE
                                    270 	.globl _XPAGE
                                    271 	.globl _SP
                                    272 	.globl _PSW
                                    273 	.globl _PCON
                                    274 	.globl _IP
                                    275 	.globl _IE
                                    276 	.globl _EIP
                                    277 	.globl _EIE
                                    278 	.globl _E2IP
                                    279 	.globl _E2IE
                                    280 	.globl _DPS
                                    281 	.globl _DPTR1
                                    282 	.globl _DPTR0
                                    283 	.globl _DPL1
                                    284 	.globl _DPL
                                    285 	.globl _DPH1
                                    286 	.globl _DPH
                                    287 	.globl _B
                                    288 	.globl _ACC
                                    289 	.globl _XTALREADY
                                    290 	.globl _XTALOSC
                                    291 	.globl _XTALAMPL
                                    292 	.globl _SILICONREV
                                    293 	.globl _SCRATCH3
                                    294 	.globl _SCRATCH2
                                    295 	.globl _SCRATCH1
                                    296 	.globl _SCRATCH0
                                    297 	.globl _RADIOMUX
                                    298 	.globl _RADIOFSTATADDR
                                    299 	.globl _RADIOFSTATADDR1
                                    300 	.globl _RADIOFSTATADDR0
                                    301 	.globl _RADIOFDATAADDR
                                    302 	.globl _RADIOFDATAADDR1
                                    303 	.globl _RADIOFDATAADDR0
                                    304 	.globl _OSCRUN
                                    305 	.globl _OSCREADY
                                    306 	.globl _OSCFORCERUN
                                    307 	.globl _OSCCALIB
                                    308 	.globl _MISCCTRL
                                    309 	.globl _LPXOSCGM
                                    310 	.globl _LPOSCREF
                                    311 	.globl _LPOSCREF1
                                    312 	.globl _LPOSCREF0
                                    313 	.globl _LPOSCPER
                                    314 	.globl _LPOSCPER1
                                    315 	.globl _LPOSCPER0
                                    316 	.globl _LPOSCKFILT
                                    317 	.globl _LPOSCKFILT1
                                    318 	.globl _LPOSCKFILT0
                                    319 	.globl _LPOSCFREQ
                                    320 	.globl _LPOSCFREQ1
                                    321 	.globl _LPOSCFREQ0
                                    322 	.globl _LPOSCCONFIG
                                    323 	.globl _PINSEL
                                    324 	.globl _PINCHGC
                                    325 	.globl _PINCHGB
                                    326 	.globl _PINCHGA
                                    327 	.globl _PALTRADIO
                                    328 	.globl _PALTC
                                    329 	.globl _PALTB
                                    330 	.globl _PALTA
                                    331 	.globl _INTCHGC
                                    332 	.globl _INTCHGB
                                    333 	.globl _INTCHGA
                                    334 	.globl _EXTIRQ
                                    335 	.globl _GPIOENABLE
                                    336 	.globl _ANALOGA
                                    337 	.globl _FRCOSCREF
                                    338 	.globl _FRCOSCREF1
                                    339 	.globl _FRCOSCREF0
                                    340 	.globl _FRCOSCPER
                                    341 	.globl _FRCOSCPER1
                                    342 	.globl _FRCOSCPER0
                                    343 	.globl _FRCOSCKFILT
                                    344 	.globl _FRCOSCKFILT1
                                    345 	.globl _FRCOSCKFILT0
                                    346 	.globl _FRCOSCFREQ
                                    347 	.globl _FRCOSCFREQ1
                                    348 	.globl _FRCOSCFREQ0
                                    349 	.globl _FRCOSCCTRL
                                    350 	.globl _FRCOSCCONFIG
                                    351 	.globl _DMA1CONFIG
                                    352 	.globl _DMA1ADDR
                                    353 	.globl _DMA1ADDR1
                                    354 	.globl _DMA1ADDR0
                                    355 	.globl _DMA0CONFIG
                                    356 	.globl _DMA0ADDR
                                    357 	.globl _DMA0ADDR1
                                    358 	.globl _DMA0ADDR0
                                    359 	.globl _ADCTUNE2
                                    360 	.globl _ADCTUNE1
                                    361 	.globl _ADCTUNE0
                                    362 	.globl _ADCCH3VAL
                                    363 	.globl _ADCCH3VAL1
                                    364 	.globl _ADCCH3VAL0
                                    365 	.globl _ADCCH2VAL
                                    366 	.globl _ADCCH2VAL1
                                    367 	.globl _ADCCH2VAL0
                                    368 	.globl _ADCCH1VAL
                                    369 	.globl _ADCCH1VAL1
                                    370 	.globl _ADCCH1VAL0
                                    371 	.globl _ADCCH0VAL
                                    372 	.globl _ADCCH0VAL1
                                    373 	.globl _ADCCH0VAL0
                                    374 	.globl _aligned_alloc_PARM_2
                                    375 	.globl _GOLDSEQUENCE_Init
                                    376 	.globl _GOLDSEQUENCE_Obtain
                                    377 ;--------------------------------------------------------
                                    378 ; special function registers
                                    379 ;--------------------------------------------------------
                                    380 	.area RSEG    (ABS,DATA)
      000000                        381 	.org 0x0000
                           0000E0   382 _ACC	=	0x00e0
                           0000F0   383 _B	=	0x00f0
                           000083   384 _DPH	=	0x0083
                           000085   385 _DPH1	=	0x0085
                           000082   386 _DPL	=	0x0082
                           000084   387 _DPL1	=	0x0084
                           008382   388 _DPTR0	=	0x8382
                           008584   389 _DPTR1	=	0x8584
                           000086   390 _DPS	=	0x0086
                           0000A0   391 _E2IE	=	0x00a0
                           0000C0   392 _E2IP	=	0x00c0
                           000098   393 _EIE	=	0x0098
                           0000B0   394 _EIP	=	0x00b0
                           0000A8   395 _IE	=	0x00a8
                           0000B8   396 _IP	=	0x00b8
                           000087   397 _PCON	=	0x0087
                           0000D0   398 _PSW	=	0x00d0
                           000081   399 _SP	=	0x0081
                           0000D9   400 _XPAGE	=	0x00d9
                           0000D9   401 __XPAGE	=	0x00d9
                           0000CA   402 _ADCCH0CONFIG	=	0x00ca
                           0000CB   403 _ADCCH1CONFIG	=	0x00cb
                           0000D2   404 _ADCCH2CONFIG	=	0x00d2
                           0000D3   405 _ADCCH3CONFIG	=	0x00d3
                           0000D1   406 _ADCCLKSRC	=	0x00d1
                           0000C9   407 _ADCCONV	=	0x00c9
                           0000E1   408 _ANALOGCOMP	=	0x00e1
                           0000C6   409 _CLKCON	=	0x00c6
                           0000C7   410 _CLKSTAT	=	0x00c7
                           000097   411 _CODECONFIG	=	0x0097
                           0000E3   412 _DBGLNKBUF	=	0x00e3
                           0000E2   413 _DBGLNKSTAT	=	0x00e2
                           000089   414 _DIRA	=	0x0089
                           00008A   415 _DIRB	=	0x008a
                           00008B   416 _DIRC	=	0x008b
                           00008E   417 _DIRR	=	0x008e
                           0000C8   418 _PINA	=	0x00c8
                           0000E8   419 _PINB	=	0x00e8
                           0000F8   420 _PINC	=	0x00f8
                           00008D   421 _PINR	=	0x008d
                           000080   422 _PORTA	=	0x0080
                           000088   423 _PORTB	=	0x0088
                           000090   424 _PORTC	=	0x0090
                           00008C   425 _PORTR	=	0x008c
                           0000CE   426 _IC0CAPT0	=	0x00ce
                           0000CF   427 _IC0CAPT1	=	0x00cf
                           00CFCE   428 _IC0CAPT	=	0xcfce
                           0000CC   429 _IC0MODE	=	0x00cc
                           0000CD   430 _IC0STATUS	=	0x00cd
                           0000D6   431 _IC1CAPT0	=	0x00d6
                           0000D7   432 _IC1CAPT1	=	0x00d7
                           00D7D6   433 _IC1CAPT	=	0xd7d6
                           0000D4   434 _IC1MODE	=	0x00d4
                           0000D5   435 _IC1STATUS	=	0x00d5
                           000092   436 _NVADDR0	=	0x0092
                           000093   437 _NVADDR1	=	0x0093
                           009392   438 _NVADDR	=	0x9392
                           000094   439 _NVDATA0	=	0x0094
                           000095   440 _NVDATA1	=	0x0095
                           009594   441 _NVDATA	=	0x9594
                           000096   442 _NVKEY	=	0x0096
                           000091   443 _NVSTATUS	=	0x0091
                           0000BC   444 _OC0COMP0	=	0x00bc
                           0000BD   445 _OC0COMP1	=	0x00bd
                           00BDBC   446 _OC0COMP	=	0xbdbc
                           0000B9   447 _OC0MODE	=	0x00b9
                           0000BA   448 _OC0PIN	=	0x00ba
                           0000BB   449 _OC0STATUS	=	0x00bb
                           0000C4   450 _OC1COMP0	=	0x00c4
                           0000C5   451 _OC1COMP1	=	0x00c5
                           00C5C4   452 _OC1COMP	=	0xc5c4
                           0000C1   453 _OC1MODE	=	0x00c1
                           0000C2   454 _OC1PIN	=	0x00c2
                           0000C3   455 _OC1STATUS	=	0x00c3
                           0000B1   456 _RADIOACC	=	0x00b1
                           0000B3   457 _RADIOADDR0	=	0x00b3
                           0000B2   458 _RADIOADDR1	=	0x00b2
                           00B2B3   459 _RADIOADDR	=	0xb2b3
                           0000B7   460 _RADIODATA0	=	0x00b7
                           0000B6   461 _RADIODATA1	=	0x00b6
                           0000B5   462 _RADIODATA2	=	0x00b5
                           0000B4   463 _RADIODATA3	=	0x00b4
                           B4B5B6B7   464 _RADIODATA	=	0xb4b5b6b7
                           0000BE   465 _RADIOSTAT0	=	0x00be
                           0000BF   466 _RADIOSTAT1	=	0x00bf
                           00BFBE   467 _RADIOSTAT	=	0xbfbe
                           0000DF   468 _SPCLKSRC	=	0x00df
                           0000DC   469 _SPMODE	=	0x00dc
                           0000DE   470 _SPSHREG	=	0x00de
                           0000DD   471 _SPSTATUS	=	0x00dd
                           00009A   472 _T0CLKSRC	=	0x009a
                           00009C   473 _T0CNT0	=	0x009c
                           00009D   474 _T0CNT1	=	0x009d
                           009D9C   475 _T0CNT	=	0x9d9c
                           000099   476 _T0MODE	=	0x0099
                           00009E   477 _T0PERIOD0	=	0x009e
                           00009F   478 _T0PERIOD1	=	0x009f
                           009F9E   479 _T0PERIOD	=	0x9f9e
                           00009B   480 _T0STATUS	=	0x009b
                           0000A2   481 _T1CLKSRC	=	0x00a2
                           0000A4   482 _T1CNT0	=	0x00a4
                           0000A5   483 _T1CNT1	=	0x00a5
                           00A5A4   484 _T1CNT	=	0xa5a4
                           0000A1   485 _T1MODE	=	0x00a1
                           0000A6   486 _T1PERIOD0	=	0x00a6
                           0000A7   487 _T1PERIOD1	=	0x00a7
                           00A7A6   488 _T1PERIOD	=	0xa7a6
                           0000A3   489 _T1STATUS	=	0x00a3
                           0000AA   490 _T2CLKSRC	=	0x00aa
                           0000AC   491 _T2CNT0	=	0x00ac
                           0000AD   492 _T2CNT1	=	0x00ad
                           00ADAC   493 _T2CNT	=	0xadac
                           0000A9   494 _T2MODE	=	0x00a9
                           0000AE   495 _T2PERIOD0	=	0x00ae
                           0000AF   496 _T2PERIOD1	=	0x00af
                           00AFAE   497 _T2PERIOD	=	0xafae
                           0000AB   498 _T2STATUS	=	0x00ab
                           0000E4   499 _U0CTRL	=	0x00e4
                           0000E7   500 _U0MODE	=	0x00e7
                           0000E6   501 _U0SHREG	=	0x00e6
                           0000E5   502 _U0STATUS	=	0x00e5
                           0000EC   503 _U1CTRL	=	0x00ec
                           0000EF   504 _U1MODE	=	0x00ef
                           0000EE   505 _U1SHREG	=	0x00ee
                           0000ED   506 _U1STATUS	=	0x00ed
                           0000DA   507 _WDTCFG	=	0x00da
                           0000DB   508 _WDTRESET	=	0x00db
                           0000F1   509 _WTCFGA	=	0x00f1
                           0000F9   510 _WTCFGB	=	0x00f9
                           0000F2   511 _WTCNTA0	=	0x00f2
                           0000F3   512 _WTCNTA1	=	0x00f3
                           00F3F2   513 _WTCNTA	=	0xf3f2
                           0000FA   514 _WTCNTB0	=	0x00fa
                           0000FB   515 _WTCNTB1	=	0x00fb
                           00FBFA   516 _WTCNTB	=	0xfbfa
                           0000EB   517 _WTCNTR1	=	0x00eb
                           0000F4   518 _WTEVTA0	=	0x00f4
                           0000F5   519 _WTEVTA1	=	0x00f5
                           00F5F4   520 _WTEVTA	=	0xf5f4
                           0000F6   521 _WTEVTB0	=	0x00f6
                           0000F7   522 _WTEVTB1	=	0x00f7
                           00F7F6   523 _WTEVTB	=	0xf7f6
                           0000FC   524 _WTEVTC0	=	0x00fc
                           0000FD   525 _WTEVTC1	=	0x00fd
                           00FDFC   526 _WTEVTC	=	0xfdfc
                           0000FE   527 _WTEVTD0	=	0x00fe
                           0000FF   528 _WTEVTD1	=	0x00ff
                           00FFFE   529 _WTEVTD	=	0xfffe
                           0000E9   530 _WTIRQEN	=	0x00e9
                           0000EA   531 _WTSTAT	=	0x00ea
                                    532 ;--------------------------------------------------------
                                    533 ; special function bits
                                    534 ;--------------------------------------------------------
                                    535 	.area RSEG    (ABS,DATA)
      000000                        536 	.org 0x0000
                           0000E0   537 _ACC_0	=	0x00e0
                           0000E1   538 _ACC_1	=	0x00e1
                           0000E2   539 _ACC_2	=	0x00e2
                           0000E3   540 _ACC_3	=	0x00e3
                           0000E4   541 _ACC_4	=	0x00e4
                           0000E5   542 _ACC_5	=	0x00e5
                           0000E6   543 _ACC_6	=	0x00e6
                           0000E7   544 _ACC_7	=	0x00e7
                           0000F0   545 _B_0	=	0x00f0
                           0000F1   546 _B_1	=	0x00f1
                           0000F2   547 _B_2	=	0x00f2
                           0000F3   548 _B_3	=	0x00f3
                           0000F4   549 _B_4	=	0x00f4
                           0000F5   550 _B_5	=	0x00f5
                           0000F6   551 _B_6	=	0x00f6
                           0000F7   552 _B_7	=	0x00f7
                           0000A0   553 _E2IE_0	=	0x00a0
                           0000A1   554 _E2IE_1	=	0x00a1
                           0000A2   555 _E2IE_2	=	0x00a2
                           0000A3   556 _E2IE_3	=	0x00a3
                           0000A4   557 _E2IE_4	=	0x00a4
                           0000A5   558 _E2IE_5	=	0x00a5
                           0000A6   559 _E2IE_6	=	0x00a6
                           0000A7   560 _E2IE_7	=	0x00a7
                           0000C0   561 _E2IP_0	=	0x00c0
                           0000C1   562 _E2IP_1	=	0x00c1
                           0000C2   563 _E2IP_2	=	0x00c2
                           0000C3   564 _E2IP_3	=	0x00c3
                           0000C4   565 _E2IP_4	=	0x00c4
                           0000C5   566 _E2IP_5	=	0x00c5
                           0000C6   567 _E2IP_6	=	0x00c6
                           0000C7   568 _E2IP_7	=	0x00c7
                           000098   569 _EIE_0	=	0x0098
                           000099   570 _EIE_1	=	0x0099
                           00009A   571 _EIE_2	=	0x009a
                           00009B   572 _EIE_3	=	0x009b
                           00009C   573 _EIE_4	=	0x009c
                           00009D   574 _EIE_5	=	0x009d
                           00009E   575 _EIE_6	=	0x009e
                           00009F   576 _EIE_7	=	0x009f
                           0000B0   577 _EIP_0	=	0x00b0
                           0000B1   578 _EIP_1	=	0x00b1
                           0000B2   579 _EIP_2	=	0x00b2
                           0000B3   580 _EIP_3	=	0x00b3
                           0000B4   581 _EIP_4	=	0x00b4
                           0000B5   582 _EIP_5	=	0x00b5
                           0000B6   583 _EIP_6	=	0x00b6
                           0000B7   584 _EIP_7	=	0x00b7
                           0000A8   585 _IE_0	=	0x00a8
                           0000A9   586 _IE_1	=	0x00a9
                           0000AA   587 _IE_2	=	0x00aa
                           0000AB   588 _IE_3	=	0x00ab
                           0000AC   589 _IE_4	=	0x00ac
                           0000AD   590 _IE_5	=	0x00ad
                           0000AE   591 _IE_6	=	0x00ae
                           0000AF   592 _IE_7	=	0x00af
                           0000AF   593 _EA	=	0x00af
                           0000B8   594 _IP_0	=	0x00b8
                           0000B9   595 _IP_1	=	0x00b9
                           0000BA   596 _IP_2	=	0x00ba
                           0000BB   597 _IP_3	=	0x00bb
                           0000BC   598 _IP_4	=	0x00bc
                           0000BD   599 _IP_5	=	0x00bd
                           0000BE   600 _IP_6	=	0x00be
                           0000BF   601 _IP_7	=	0x00bf
                           0000D0   602 _P	=	0x00d0
                           0000D1   603 _F1	=	0x00d1
                           0000D2   604 _OV	=	0x00d2
                           0000D3   605 _RS0	=	0x00d3
                           0000D4   606 _RS1	=	0x00d4
                           0000D5   607 _F0	=	0x00d5
                           0000D6   608 _AC	=	0x00d6
                           0000D7   609 _CY	=	0x00d7
                           0000C8   610 _PINA_0	=	0x00c8
                           0000C9   611 _PINA_1	=	0x00c9
                           0000CA   612 _PINA_2	=	0x00ca
                           0000CB   613 _PINA_3	=	0x00cb
                           0000CC   614 _PINA_4	=	0x00cc
                           0000CD   615 _PINA_5	=	0x00cd
                           0000CE   616 _PINA_6	=	0x00ce
                           0000CF   617 _PINA_7	=	0x00cf
                           0000E8   618 _PINB_0	=	0x00e8
                           0000E9   619 _PINB_1	=	0x00e9
                           0000EA   620 _PINB_2	=	0x00ea
                           0000EB   621 _PINB_3	=	0x00eb
                           0000EC   622 _PINB_4	=	0x00ec
                           0000ED   623 _PINB_5	=	0x00ed
                           0000EE   624 _PINB_6	=	0x00ee
                           0000EF   625 _PINB_7	=	0x00ef
                           0000F8   626 _PINC_0	=	0x00f8
                           0000F9   627 _PINC_1	=	0x00f9
                           0000FA   628 _PINC_2	=	0x00fa
                           0000FB   629 _PINC_3	=	0x00fb
                           0000FC   630 _PINC_4	=	0x00fc
                           0000FD   631 _PINC_5	=	0x00fd
                           0000FE   632 _PINC_6	=	0x00fe
                           0000FF   633 _PINC_7	=	0x00ff
                           000080   634 _PORTA_0	=	0x0080
                           000081   635 _PORTA_1	=	0x0081
                           000082   636 _PORTA_2	=	0x0082
                           000083   637 _PORTA_3	=	0x0083
                           000084   638 _PORTA_4	=	0x0084
                           000085   639 _PORTA_5	=	0x0085
                           000086   640 _PORTA_6	=	0x0086
                           000087   641 _PORTA_7	=	0x0087
                           000088   642 _PORTB_0	=	0x0088
                           000089   643 _PORTB_1	=	0x0089
                           00008A   644 _PORTB_2	=	0x008a
                           00008B   645 _PORTB_3	=	0x008b
                           00008C   646 _PORTB_4	=	0x008c
                           00008D   647 _PORTB_5	=	0x008d
                           00008E   648 _PORTB_6	=	0x008e
                           00008F   649 _PORTB_7	=	0x008f
                           000090   650 _PORTC_0	=	0x0090
                           000091   651 _PORTC_1	=	0x0091
                           000092   652 _PORTC_2	=	0x0092
                           000093   653 _PORTC_3	=	0x0093
                           000094   654 _PORTC_4	=	0x0094
                           000095   655 _PORTC_5	=	0x0095
                           000096   656 _PORTC_6	=	0x0096
                           000097   657 _PORTC_7	=	0x0097
                                    658 ;--------------------------------------------------------
                                    659 ; overlayable register banks
                                    660 ;--------------------------------------------------------
                                    661 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        662 	.ds 8
                                    663 ;--------------------------------------------------------
                                    664 ; internal ram data
                                    665 ;--------------------------------------------------------
                                    666 	.area DSEG    (DATA)
      000022                        667 _GOLDSEQUENCE_Obtain_sloc0_1_0:
      000022                        668 	.ds 4
                                    669 ;--------------------------------------------------------
                                    670 ; overlayable items in internal ram 
                                    671 ;--------------------------------------------------------
                                    672 ;--------------------------------------------------------
                                    673 ; indirectly addressable internal ram data
                                    674 ;--------------------------------------------------------
                                    675 	.area ISEG    (DATA)
                                    676 ;--------------------------------------------------------
                                    677 ; absolute internal ram data
                                    678 ;--------------------------------------------------------
                                    679 	.area IABS    (ABS,DATA)
                                    680 	.area IABS    (ABS,DATA)
                                    681 ;--------------------------------------------------------
                                    682 ; bit data
                                    683 ;--------------------------------------------------------
                                    684 	.area BSEG    (BIT)
                                    685 ;--------------------------------------------------------
                                    686 ; paged external ram data
                                    687 ;--------------------------------------------------------
                                    688 	.area PSEG    (PAG,XDATA)
                                    689 ;--------------------------------------------------------
                                    690 ; external ram data
                                    691 ;--------------------------------------------------------
                                    692 	.area XSEG    (XDATA)
      000389                        693 _aligned_alloc_PARM_2:
      000389                        694 	.ds 2
                           00FC06   695 _flash_deviceid	=	0xfc06
                           007020   696 _ADCCH0VAL0	=	0x7020
                           007021   697 _ADCCH0VAL1	=	0x7021
                           007020   698 _ADCCH0VAL	=	0x7020
                           007022   699 _ADCCH1VAL0	=	0x7022
                           007023   700 _ADCCH1VAL1	=	0x7023
                           007022   701 _ADCCH1VAL	=	0x7022
                           007024   702 _ADCCH2VAL0	=	0x7024
                           007025   703 _ADCCH2VAL1	=	0x7025
                           007024   704 _ADCCH2VAL	=	0x7024
                           007026   705 _ADCCH3VAL0	=	0x7026
                           007027   706 _ADCCH3VAL1	=	0x7027
                           007026   707 _ADCCH3VAL	=	0x7026
                           007028   708 _ADCTUNE0	=	0x7028
                           007029   709 _ADCTUNE1	=	0x7029
                           00702A   710 _ADCTUNE2	=	0x702a
                           007010   711 _DMA0ADDR0	=	0x7010
                           007011   712 _DMA0ADDR1	=	0x7011
                           007010   713 _DMA0ADDR	=	0x7010
                           007014   714 _DMA0CONFIG	=	0x7014
                           007012   715 _DMA1ADDR0	=	0x7012
                           007013   716 _DMA1ADDR1	=	0x7013
                           007012   717 _DMA1ADDR	=	0x7012
                           007015   718 _DMA1CONFIG	=	0x7015
                           007070   719 _FRCOSCCONFIG	=	0x7070
                           007071   720 _FRCOSCCTRL	=	0x7071
                           007076   721 _FRCOSCFREQ0	=	0x7076
                           007077   722 _FRCOSCFREQ1	=	0x7077
                           007076   723 _FRCOSCFREQ	=	0x7076
                           007072   724 _FRCOSCKFILT0	=	0x7072
                           007073   725 _FRCOSCKFILT1	=	0x7073
                           007072   726 _FRCOSCKFILT	=	0x7072
                           007078   727 _FRCOSCPER0	=	0x7078
                           007079   728 _FRCOSCPER1	=	0x7079
                           007078   729 _FRCOSCPER	=	0x7078
                           007074   730 _FRCOSCREF0	=	0x7074
                           007075   731 _FRCOSCREF1	=	0x7075
                           007074   732 _FRCOSCREF	=	0x7074
                           007007   733 _ANALOGA	=	0x7007
                           00700C   734 _GPIOENABLE	=	0x700c
                           007003   735 _EXTIRQ	=	0x7003
                           007000   736 _INTCHGA	=	0x7000
                           007001   737 _INTCHGB	=	0x7001
                           007002   738 _INTCHGC	=	0x7002
                           007008   739 _PALTA	=	0x7008
                           007009   740 _PALTB	=	0x7009
                           00700A   741 _PALTC	=	0x700a
                           007046   742 _PALTRADIO	=	0x7046
                           007004   743 _PINCHGA	=	0x7004
                           007005   744 _PINCHGB	=	0x7005
                           007006   745 _PINCHGC	=	0x7006
                           00700B   746 _PINSEL	=	0x700b
                           007060   747 _LPOSCCONFIG	=	0x7060
                           007066   748 _LPOSCFREQ0	=	0x7066
                           007067   749 _LPOSCFREQ1	=	0x7067
                           007066   750 _LPOSCFREQ	=	0x7066
                           007062   751 _LPOSCKFILT0	=	0x7062
                           007063   752 _LPOSCKFILT1	=	0x7063
                           007062   753 _LPOSCKFILT	=	0x7062
                           007068   754 _LPOSCPER0	=	0x7068
                           007069   755 _LPOSCPER1	=	0x7069
                           007068   756 _LPOSCPER	=	0x7068
                           007064   757 _LPOSCREF0	=	0x7064
                           007065   758 _LPOSCREF1	=	0x7065
                           007064   759 _LPOSCREF	=	0x7064
                           007054   760 _LPXOSCGM	=	0x7054
                           007F01   761 _MISCCTRL	=	0x7f01
                           007053   762 _OSCCALIB	=	0x7053
                           007050   763 _OSCFORCERUN	=	0x7050
                           007052   764 _OSCREADY	=	0x7052
                           007051   765 _OSCRUN	=	0x7051
                           007040   766 _RADIOFDATAADDR0	=	0x7040
                           007041   767 _RADIOFDATAADDR1	=	0x7041
                           007040   768 _RADIOFDATAADDR	=	0x7040
                           007042   769 _RADIOFSTATADDR0	=	0x7042
                           007043   770 _RADIOFSTATADDR1	=	0x7043
                           007042   771 _RADIOFSTATADDR	=	0x7042
                           007044   772 _RADIOMUX	=	0x7044
                           007084   773 _SCRATCH0	=	0x7084
                           007085   774 _SCRATCH1	=	0x7085
                           007086   775 _SCRATCH2	=	0x7086
                           007087   776 _SCRATCH3	=	0x7087
                           007F00   777 _SILICONREV	=	0x7f00
                           007F19   778 _XTALAMPL	=	0x7f19
                           007F18   779 _XTALOSC	=	0x7f18
                           007F1A   780 _XTALREADY	=	0x7f1a
                                    781 ;--------------------------------------------------------
                                    782 ; absolute external ram data
                                    783 ;--------------------------------------------------------
                                    784 	.area XABS    (ABS,XDATA)
                                    785 ;--------------------------------------------------------
                                    786 ; external initialized ram data
                                    787 ;--------------------------------------------------------
                                    788 	.area XISEG   (XDATA)
                                    789 	.area HOME    (CODE)
                                    790 	.area GSINIT0 (CODE)
                                    791 	.area GSINIT1 (CODE)
                                    792 	.area GSINIT2 (CODE)
                                    793 	.area GSINIT3 (CODE)
                                    794 	.area GSINIT4 (CODE)
                                    795 	.area GSINIT5 (CODE)
                                    796 	.area GSINIT  (CODE)
                                    797 	.area GSFINAL (CODE)
                                    798 	.area CSEG    (CODE)
                                    799 ;--------------------------------------------------------
                                    800 ; global & static initialisations
                                    801 ;--------------------------------------------------------
                                    802 	.area HOME    (CODE)
                                    803 	.area GSINIT  (CODE)
                                    804 	.area GSFINAL (CODE)
                                    805 	.area GSINIT  (CODE)
                                    806 ;--------------------------------------------------------
                                    807 ; Home
                                    808 ;--------------------------------------------------------
                                    809 	.area HOME    (CODE)
                                    810 	.area HOME    (CODE)
                                    811 ;--------------------------------------------------------
                                    812 ; code
                                    813 ;--------------------------------------------------------
                                    814 	.area CSEG    (CODE)
                                    815 ;------------------------------------------------------------
                                    816 ;Allocation info for local variables in function 'GOLDSEQUENCE_Init'
                                    817 ;------------------------------------------------------------
                                    818 ;	..\src\COMMON\GoldSequence.c:26: void GOLDSEQUENCE_Init(void)
                                    819 ;	-----------------------------------------
                                    820 ;	 function GOLDSEQUENCE_Init
                                    821 ;	-----------------------------------------
      005230                        822 _GOLDSEQUENCE_Init:
                           000007   823 	ar7 = 0x07
                           000006   824 	ar6 = 0x06
                           000005   825 	ar5 = 0x05
                           000004   826 	ar4 = 0x04
                           000003   827 	ar3 = 0x03
                           000002   828 	ar2 = 0x02
                           000001   829 	ar1 = 0x01
                           000000   830 	ar0 = 0x00
                                    831 ;	..\src\COMMON\GoldSequence.c:29: flash_unlock();
      005230 12 8A 97         [24]  832 	lcall	_flash_unlock
                                    833 ;	..\src\COMMON\GoldSequence.c:32: flash_write(0x0000,0b1111100110100100);
      005233 90 04 C6         [24]  834 	mov	dptr,#_flash_write_PARM_2
      005236 74 A4            [12]  835 	mov	a,#0xa4
      005238 F0               [24]  836 	movx	@dptr,a
      005239 74 F9            [12]  837 	mov	a,#0xf9
      00523B A3               [24]  838 	inc	dptr
      00523C F0               [24]  839 	movx	@dptr,a
      00523D 90 00 00         [24]  840 	mov	dptr,#0x0000
      005240 12 88 D0         [24]  841 	lcall	_flash_write
                                    842 ;	..\src\COMMON\GoldSequence.c:34: flash_write(0x0002,0b001010111011000);
      005243 90 04 C6         [24]  843 	mov	dptr,#_flash_write_PARM_2
      005246 74 D8            [12]  844 	mov	a,#0xd8
      005248 F0               [24]  845 	movx	@dptr,a
      005249 74 15            [12]  846 	mov	a,#0x15
      00524B A3               [24]  847 	inc	dptr
      00524C F0               [24]  848 	movx	@dptr,a
      00524D 90 00 02         [24]  849 	mov	dptr,#0x0002
      005250 12 88 D0         [24]  850 	lcall	_flash_write
                                    851 ;	..\src\COMMON\GoldSequence.c:37: flash_write(0x0004,0b1111100100110000);
      005253 90 04 C6         [24]  852 	mov	dptr,#_flash_write_PARM_2
      005256 74 30            [12]  853 	mov	a,#0x30
      005258 F0               [24]  854 	movx	@dptr,a
      005259 74 F9            [12]  855 	mov	a,#0xf9
      00525B A3               [24]  856 	inc	dptr
      00525C F0               [24]  857 	movx	@dptr,a
      00525D 90 00 04         [24]  858 	mov	dptr,#0x0004
      005260 12 88 D0         [24]  859 	lcall	_flash_write
                                    860 ;	..\src\COMMON\GoldSequence.c:39: flash_write(0x0006,0b101101010001110);
      005263 90 04 C6         [24]  861 	mov	dptr,#_flash_write_PARM_2
      005266 74 8E            [12]  862 	mov	a,#0x8e
      005268 F0               [24]  863 	movx	@dptr,a
      005269 74 5A            [12]  864 	mov	a,#0x5a
      00526B A3               [24]  865 	inc	dptr
      00526C F0               [24]  866 	movx	@dptr,a
      00526D 90 00 06         [24]  867 	mov	dptr,#0x0006
      005270 12 88 D0         [24]  868 	lcall	_flash_write
                                    869 ;	..\src\COMMON\GoldSequence.c:42: flash_write(0x0008,0b0000000010010100);
      005273 90 04 C6         [24]  870 	mov	dptr,#_flash_write_PARM_2
      005276 74 94            [12]  871 	mov	a,#0x94
      005278 F0               [24]  872 	movx	@dptr,a
      005279 E4               [12]  873 	clr	a
      00527A A3               [24]  874 	inc	dptr
      00527B F0               [24]  875 	movx	@dptr,a
      00527C 90 00 08         [24]  876 	mov	dptr,#0x0008
      00527F 12 88 D0         [24]  877 	lcall	_flash_write
                                    878 ;	..\src\COMMON\GoldSequence.c:44: flash_write(0x000A,0b100111101010110);
      005282 90 04 C6         [24]  879 	mov	dptr,#_flash_write_PARM_2
      005285 74 56            [12]  880 	mov	a,#0x56
      005287 F0               [24]  881 	movx	@dptr,a
      005288 74 4F            [12]  882 	mov	a,#0x4f
      00528A A3               [24]  883 	inc	dptr
      00528B F0               [24]  884 	movx	@dptr,a
      00528C 90 00 0A         [24]  885 	mov	dptr,#0x000a
      00528F 12 88 D0         [24]  886 	lcall	_flash_write
                                    887 ;	..\src\COMMON\GoldSequence.c:47: flash_write(0x000C,0b1000010100111100);
      005292 90 04 C6         [24]  888 	mov	dptr,#_flash_write_PARM_2
      005295 74 3C            [12]  889 	mov	a,#0x3c
      005297 F0               [24]  890 	movx	@dptr,a
      005298 74 85            [12]  891 	mov	a,#0x85
      00529A A3               [24]  892 	inc	dptr
      00529B F0               [24]  893 	movx	@dptr,a
      00529C 90 00 0C         [24]  894 	mov	dptr,#0x000c
      00529F 12 88 D0         [24]  895 	lcall	_flash_write
                                    896 ;	..\src\COMMON\GoldSequence.c:49: flash_write(0x000E,0b011100010011111);
      0052A2 90 04 C6         [24]  897 	mov	dptr,#_flash_write_PARM_2
      0052A5 74 9F            [12]  898 	mov	a,#0x9f
      0052A7 F0               [24]  899 	movx	@dptr,a
      0052A8 74 38            [12]  900 	mov	a,#0x38
      0052AA A3               [24]  901 	inc	dptr
      0052AB F0               [24]  902 	movx	@dptr,a
      0052AC 90 00 0E         [24]  903 	mov	dptr,#0x000e
      0052AF 12 88 D0         [24]  904 	lcall	_flash_write
                                    905 ;	..\src\COMMON\GoldSequence.c:52: flash_write(0x0010,0b0100011111101000);
      0052B2 90 04 C6         [24]  906 	mov	dptr,#_flash_write_PARM_2
      0052B5 74 E8            [12]  907 	mov	a,#0xe8
      0052B7 F0               [24]  908 	movx	@dptr,a
      0052B8 74 47            [12]  909 	mov	a,#0x47
      0052BA A3               [24]  910 	inc	dptr
      0052BB F0               [24]  911 	movx	@dptr,a
      0052BC 90 00 10         [24]  912 	mov	dptr,#0x0010
      0052BF 12 88 D0         [24]  913 	lcall	_flash_write
                                    914 ;	..\src\COMMON\GoldSequence.c:54: flash_write(0x0012,0b000001101111011);
      0052C2 90 04 C6         [24]  915 	mov	dptr,#_flash_write_PARM_2
      0052C5 74 7B            [12]  916 	mov	a,#0x7b
      0052C7 F0               [24]  917 	movx	@dptr,a
      0052C8 74 03            [12]  918 	mov	a,#0x03
      0052CA A3               [24]  919 	inc	dptr
      0052CB F0               [24]  920 	movx	@dptr,a
      0052CC 90 00 12         [24]  921 	mov	dptr,#0x0012
      0052CF 12 88 D0         [24]  922 	lcall	_flash_write
                                    923 ;	..\src\COMMON\GoldSequence.c:57: flash_write(0x0014,0b0010011010000010);
      0052D2 90 04 C6         [24]  924 	mov	dptr,#_flash_write_PARM_2
      0052D5 74 82            [12]  925 	mov	a,#0x82
      0052D7 F0               [24]  926 	movx	@dptr,a
      0052D8 74 26            [12]  927 	mov	a,#0x26
      0052DA A3               [24]  928 	inc	dptr
      0052DB F0               [24]  929 	movx	@dptr,a
      0052DC 90 00 14         [24]  930 	mov	dptr,#0x0014
      0052DF 12 88 D0         [24]  931 	lcall	_flash_write
                                    932 ;	..\src\COMMON\GoldSequence.c:59: flash_write(0x0016,0b001111010001001);
      0052E2 90 04 C6         [24]  933 	mov	dptr,#_flash_write_PARM_2
      0052E5 74 89            [12]  934 	mov	a,#0x89
      0052E7 F0               [24]  935 	movx	@dptr,a
      0052E8 74 1E            [12]  936 	mov	a,#0x1e
      0052EA A3               [24]  937 	inc	dptr
      0052EB F0               [24]  938 	movx	@dptr,a
      0052EC 90 00 16         [24]  939 	mov	dptr,#0x0016
      0052EF 12 88 D0         [24]  940 	lcall	_flash_write
                                    941 ;	..\src\COMMON\GoldSequence.c:62: flash_write(0x0018,0b0001011000110111);
      0052F2 90 04 C6         [24]  942 	mov	dptr,#_flash_write_PARM_2
      0052F5 74 37            [12]  943 	mov	a,#0x37
      0052F7 F0               [24]  944 	movx	@dptr,a
      0052F8 74 16            [12]  945 	mov	a,#0x16
      0052FA A3               [24]  946 	inc	dptr
      0052FB F0               [24]  947 	movx	@dptr,a
      0052FC 90 00 18         [24]  948 	mov	dptr,#0x0018
      0052FF 12 88 D0         [24]  949 	lcall	_flash_write
                                    950 ;	..\src\COMMON\GoldSequence.c:64: flash_write(0x001A,0b001000001110000);
      005302 90 04 C6         [24]  951 	mov	dptr,#_flash_write_PARM_2
      005305 74 70            [12]  952 	mov	a,#0x70
      005307 F0               [24]  953 	movx	@dptr,a
      005308 74 10            [12]  954 	mov	a,#0x10
      00530A A3               [24]  955 	inc	dptr
      00530B F0               [24]  956 	movx	@dptr,a
      00530C 90 00 1A         [24]  957 	mov	dptr,#0x001a
      00530F 12 88 D0         [24]  958 	lcall	_flash_write
                                    959 ;	..\src\COMMON\GoldSequence.c:67: flash_write(0x001C,0b1000111001101101);
      005312 90 04 C6         [24]  960 	mov	dptr,#_flash_write_PARM_2
      005315 74 6D            [12]  961 	mov	a,#0x6d
      005317 F0               [24]  962 	movx	@dptr,a
      005318 74 8E            [12]  963 	mov	a,#0x8e
      00531A A3               [24]  964 	inc	dptr
      00531B F0               [24]  965 	movx	@dptr,a
      00531C 90 00 1C         [24]  966 	mov	dptr,#0x001c
      00531F 12 88 D0         [24]  967 	lcall	_flash_write
                                    968 ;	..\src\COMMON\GoldSequence.c:69: flash_write(0x001E,0b101011100001100);
      005322 90 04 C6         [24]  969 	mov	dptr,#_flash_write_PARM_2
      005325 74 0C            [12]  970 	mov	a,#0x0c
      005327 F0               [24]  971 	movx	@dptr,a
      005328 74 57            [12]  972 	mov	a,#0x57
      00532A A3               [24]  973 	inc	dptr
      00532B F0               [24]  974 	movx	@dptr,a
      00532C 90 00 1E         [24]  975 	mov	dptr,#0x001e
      00532F 12 88 D0         [24]  976 	lcall	_flash_write
                                    977 ;	..\src\COMMON\GoldSequence.c:72: flash_write(0x0020,0b1100001001000000);
      005332 90 04 C6         [24]  978 	mov	dptr,#_flash_write_PARM_2
      005335 74 40            [12]  979 	mov	a,#0x40
      005337 F0               [24]  980 	movx	@dptr,a
      005338 74 C2            [12]  981 	mov	a,#0xc2
      00533A A3               [24]  982 	inc	dptr
      00533B F0               [24]  983 	movx	@dptr,a
      00533C 90 00 20         [24]  984 	mov	dptr,#0x0020
      00533F 12 88 D0         [24]  985 	lcall	_flash_write
                                    986 ;	..\src\COMMON\GoldSequence.c:74: flash_write(0x0022,0b111010010110010);
      005342 90 04 C6         [24]  987 	mov	dptr,#_flash_write_PARM_2
      005345 74 B2            [12]  988 	mov	a,#0xb2
      005347 F0               [24]  989 	movx	@dptr,a
      005348 74 74            [12]  990 	mov	a,#0x74
      00534A A3               [24]  991 	inc	dptr
      00534B F0               [24]  992 	movx	@dptr,a
      00534C 90 00 22         [24]  993 	mov	dptr,#0x0022
      00534F 12 88 D0         [24]  994 	lcall	_flash_write
                                    995 ;	..\src\COMMON\GoldSequence.c:77: flash_write(0x0024,0b1110010001010110);
      005352 90 04 C6         [24]  996 	mov	dptr,#_flash_write_PARM_2
      005355 74 56            [12]  997 	mov	a,#0x56
      005357 F0               [24]  998 	movx	@dptr,a
      005358 74 E4            [12]  999 	mov	a,#0xe4
      00535A A3               [24] 1000 	inc	dptr
      00535B F0               [24] 1001 	movx	@dptr,a
      00535C 90 00 24         [24] 1002 	mov	dptr,#0x0024
      00535F 12 88 D0         [24] 1003 	lcall	_flash_write
                                   1004 ;	..\src\COMMON\GoldSequence.c:79: flash_write(0x0026,0b010010101101101);
      005362 90 04 C6         [24] 1005 	mov	dptr,#_flash_write_PARM_2
      005365 74 6D            [12] 1006 	mov	a,#0x6d
      005367 F0               [24] 1007 	movx	@dptr,a
      005368 74 25            [12] 1008 	mov	a,#0x25
      00536A A3               [24] 1009 	inc	dptr
      00536B F0               [24] 1010 	movx	@dptr,a
      00536C 90 00 26         [24] 1011 	mov	dptr,#0x0026
      00536F 12 88 D0         [24] 1012 	lcall	_flash_write
                                   1013 ;	..\src\COMMON\GoldSequence.c:82: flash_write(0x0028,0b0111011101011101);
      005372 90 04 C6         [24] 1014 	mov	dptr,#_flash_write_PARM_2
      005375 74 5D            [12] 1015 	mov	a,#0x5d
      005377 F0               [24] 1016 	movx	@dptr,a
      005378 74 77            [12] 1017 	mov	a,#0x77
      00537A A3               [24] 1018 	inc	dptr
      00537B F0               [24] 1019 	movx	@dptr,a
      00537C 90 00 28         [24] 1020 	mov	dptr,#0x0028
      00537F 12 88 D0         [24] 1021 	lcall	_flash_write
                                   1022 ;	..\src\COMMON\GoldSequence.c:84: flash_write(0x002A,0b000110110000010);
      005382 90 04 C6         [24] 1023 	mov	dptr,#_flash_write_PARM_2
      005385 74 82            [12] 1024 	mov	a,#0x82
      005387 F0               [24] 1025 	movx	@dptr,a
      005388 74 0D            [12] 1026 	mov	a,#0x0d
      00538A A3               [24] 1027 	inc	dptr
      00538B F0               [24] 1028 	movx	@dptr,a
      00538C 90 00 2A         [24] 1029 	mov	dptr,#0x002a
      00538F 12 88 D0         [24] 1030 	lcall	_flash_write
                                   1031 ;	..\src\COMMON\GoldSequence.c:87: flash_write(0x002C,0b1011111011011000);
      005392 90 04 C6         [24] 1032 	mov	dptr,#_flash_write_PARM_2
      005395 74 D8            [12] 1033 	mov	a,#0xd8
      005397 F0               [24] 1034 	movx	@dptr,a
      005398 74 BE            [12] 1035 	mov	a,#0xbe
      00539A A3               [24] 1036 	inc	dptr
      00539B F0               [24] 1037 	movx	@dptr,a
      00539C 90 00 2C         [24] 1038 	mov	dptr,#0x002c
      00539F 12 88 D0         [24] 1039 	lcall	_flash_write
                                   1040 ;	..\src\COMMON\GoldSequence.c:89: flash_write(0x002E,0b101100111110101);
      0053A2 90 04 C6         [24] 1041 	mov	dptr,#_flash_write_PARM_2
      0053A5 74 F5            [12] 1042 	mov	a,#0xf5
      0053A7 F0               [24] 1043 	movx	@dptr,a
      0053A8 74 59            [12] 1044 	mov	a,#0x59
      0053AA A3               [24] 1045 	inc	dptr
      0053AB F0               [24] 1046 	movx	@dptr,a
      0053AC 90 00 2E         [24] 1047 	mov	dptr,#0x002e
      0053AF 12 88 D0         [24] 1048 	lcall	_flash_write
                                   1049 ;	..\src\COMMON\GoldSequence.c:92: flash_write(0x0030,0b0101101000011010);
      0053B2 90 04 C6         [24] 1050 	mov	dptr,#_flash_write_PARM_2
      0053B5 74 1A            [12] 1051 	mov	a,#0x1a
      0053B7 F0               [24] 1052 	movx	@dptr,a
      0053B8 74 5A            [12] 1053 	mov	a,#0x5a
      0053BA A3               [24] 1054 	inc	dptr
      0053BB F0               [24] 1055 	movx	@dptr,a
      0053BC 90 00 30         [24] 1056 	mov	dptr,#0x0030
      0053BF 12 88 D0         [24] 1057 	lcall	_flash_write
                                   1058 ;	..\src\COMMON\GoldSequence.c:94: flash_write(0x0032,0b011001111001110);
      0053C2 90 04 C6         [24] 1059 	mov	dptr,#_flash_write_PARM_2
      0053C5 74 CE            [12] 1060 	mov	a,#0xce
      0053C7 F0               [24] 1061 	movx	@dptr,a
      0053C8 74 33            [12] 1062 	mov	a,#0x33
      0053CA A3               [24] 1063 	inc	dptr
      0053CB F0               [24] 1064 	movx	@dptr,a
      0053CC 90 00 32         [24] 1065 	mov	dptr,#0x0032
      0053CF 12 88 D0         [24] 1066 	lcall	_flash_write
                                   1067 ;	..\src\COMMON\GoldSequence.c:97: flash_write(0x0034,0b1010100001111011);
      0053D2 90 04 C6         [24] 1068 	mov	dptr,#_flash_write_PARM_2
      0053D5 74 7B            [12] 1069 	mov	a,#0x7b
      0053D7 F0               [24] 1070 	movx	@dptr,a
      0053D8 74 A8            [12] 1071 	mov	a,#0xa8
      0053DA A3               [24] 1072 	inc	dptr
      0053DB F0               [24] 1073 	movx	@dptr,a
      0053DC 90 00 34         [24] 1074 	mov	dptr,#0x0034
      0053DF 12 88 D0         [24] 1075 	lcall	_flash_write
                                   1076 ;	..\src\COMMON\GoldSequence.c:99: flash_write(0x0036,0b000011011010011);
      0053E2 90 04 C6         [24] 1077 	mov	dptr,#_flash_write_PARM_2
      0053E5 74 D3            [12] 1078 	mov	a,#0xd3
      0053E7 F0               [24] 1079 	movx	@dptr,a
      0053E8 74 06            [12] 1080 	mov	a,#0x06
      0053EA A3               [24] 1081 	inc	dptr
      0053EB F0               [24] 1082 	movx	@dptr,a
      0053EC 90 00 36         [24] 1083 	mov	dptr,#0x0036
      0053EF 12 88 D0         [24] 1084 	lcall	_flash_write
                                   1085 ;	..\src\COMMON\GoldSequence.c:102: flash_write(0x0038,0b0101000101001011);
      0053F2 90 04 C6         [24] 1086 	mov	dptr,#_flash_write_PARM_2
      0053F5 74 4B            [12] 1087 	mov	a,#0x4b
      0053F7 F0               [24] 1088 	movx	@dptr,a
      0053F8 74 51            [12] 1089 	mov	a,#0x51
      0053FA A3               [24] 1090 	inc	dptr
      0053FB F0               [24] 1091 	movx	@dptr,a
      0053FC 90 00 38         [24] 1092 	mov	dptr,#0x0038
      0053FF 12 88 D0         [24] 1093 	lcall	_flash_write
                                   1094 ;	..\src\COMMON\GoldSequence.c:104: flash_write(0x003A,0b101110001011101);
      005402 90 04 C6         [24] 1095 	mov	dptr,#_flash_write_PARM_2
      005405 74 5D            [12] 1096 	mov	a,#0x5d
      005407 F0               [24] 1097 	movx	@dptr,a
      005408 14               [12] 1098 	dec	a
      005409 A3               [24] 1099 	inc	dptr
      00540A F0               [24] 1100 	movx	@dptr,a
      00540B 90 00 3A         [24] 1101 	mov	dptr,#0x003a
      00540E 12 88 D0         [24] 1102 	lcall	_flash_write
                                   1103 ;	..\src\COMMON\GoldSequence.c:107: flash_write(0x003C,0b0010110111010011);
      005411 90 04 C6         [24] 1104 	mov	dptr,#_flash_write_PARM_2
      005414 74 D3            [12] 1105 	mov	a,#0xd3
      005416 F0               [24] 1106 	movx	@dptr,a
      005417 74 2D            [12] 1107 	mov	a,#0x2d
      005419 A3               [24] 1108 	inc	dptr
      00541A F0               [24] 1109 	movx	@dptr,a
      00541B 90 00 3C         [24] 1110 	mov	dptr,#0x003c
      00541E 12 88 D0         [24] 1111 	lcall	_flash_write
                                   1112 ;	..\src\COMMON\GoldSequence.c:109: flash_write(0x003E,0b111000100011010);
      005421 90 04 C6         [24] 1113 	mov	dptr,#_flash_write_PARM_2
      005424 74 1A            [12] 1114 	mov	a,#0x1a
      005426 F0               [24] 1115 	movx	@dptr,a
      005427 74 71            [12] 1116 	mov	a,#0x71
      005429 A3               [24] 1117 	inc	dptr
      00542A F0               [24] 1118 	movx	@dptr,a
      00542B 90 00 3E         [24] 1119 	mov	dptr,#0x003e
      00542E 12 88 D0         [24] 1120 	lcall	_flash_write
                                   1121 ;	..\src\COMMON\GoldSequence.c:112: flash_write(0x0040,0b1001001110011111);
      005431 90 04 C6         [24] 1122 	mov	dptr,#_flash_write_PARM_2
      005434 74 9F            [12] 1123 	mov	a,#0x9f
      005436 F0               [24] 1124 	movx	@dptr,a
      005437 74 93            [12] 1125 	mov	a,#0x93
      005439 A3               [24] 1126 	inc	dptr
      00543A F0               [24] 1127 	movx	@dptr,a
      00543B 90 00 40         [24] 1128 	mov	dptr,#0x0040
      00543E 12 88 D0         [24] 1129 	lcall	_flash_write
                                   1130 ;	..\src\COMMON\GoldSequence.c:114: flash_write(0x0042,0b110011110111001);
      005441 90 04 C6         [24] 1131 	mov	dptr,#_flash_write_PARM_2
      005444 74 B9            [12] 1132 	mov	a,#0xb9
      005446 F0               [24] 1133 	movx	@dptr,a
      005447 74 67            [12] 1134 	mov	a,#0x67
      005449 A3               [24] 1135 	inc	dptr
      00544A F0               [24] 1136 	movx	@dptr,a
      00544B 90 00 42         [24] 1137 	mov	dptr,#0x0042
      00544E 12 88 D0         [24] 1138 	lcall	_flash_write
                                   1139 ;	..\src\COMMON\GoldSequence.c:117: flash_write(0x0044,0b0100110010111001);
      005451 90 04 C6         [24] 1140 	mov	dptr,#_flash_write_PARM_2
      005454 74 B9            [12] 1141 	mov	a,#0xb9
      005456 F0               [24] 1142 	movx	@dptr,a
      005457 74 4C            [12] 1143 	mov	a,#0x4c
      005459 A3               [24] 1144 	inc	dptr
      00545A F0               [24] 1145 	movx	@dptr,a
      00545B 90 00 44         [24] 1146 	mov	dptr,#0x0044
      00545E 12 88 D0         [24] 1147 	lcall	_flash_write
                                   1148 ;	..\src\COMMON\GoldSequence.c:119: flash_write(0x0046,0b110110011101000);
      005461 90 04 C6         [24] 1149 	mov	dptr,#_flash_write_PARM_2
      005464 74 E8            [12] 1150 	mov	a,#0xe8
      005466 F0               [24] 1151 	movx	@dptr,a
      005467 74 6C            [12] 1152 	mov	a,#0x6c
      005469 A3               [24] 1153 	inc	dptr
      00546A F0               [24] 1154 	movx	@dptr,a
      00546B 90 00 46         [24] 1155 	mov	dptr,#0x0046
      00546E 12 88 D0         [24] 1156 	lcall	_flash_write
                                   1157 ;	..\src\COMMON\GoldSequence.c:122: flash_write(0x0048,0b1010001100101010);
      005471 90 04 C6         [24] 1158 	mov	dptr,#_flash_write_PARM_2
      005474 74 2A            [12] 1159 	mov	a,#0x2a
      005476 F0               [24] 1160 	movx	@dptr,a
      005477 74 A3            [12] 1161 	mov	a,#0xa3
      005479 A3               [24] 1162 	inc	dptr
      00547A F0               [24] 1163 	movx	@dptr,a
      00547B 90 00 48         [24] 1164 	mov	dptr,#0x0048
      00547E 12 88 D0         [24] 1165 	lcall	_flash_write
                                   1166 ;	..\src\COMMON\GoldSequence.c:124: flash_write(0x004A,0b110100101000000);
      005481 90 04 C6         [24] 1167 	mov	dptr,#_flash_write_PARM_2
      005484 74 40            [12] 1168 	mov	a,#0x40
      005486 F0               [24] 1169 	movx	@dptr,a
      005487 74 69            [12] 1170 	mov	a,#0x69
      005489 A3               [24] 1171 	inc	dptr
      00548A F0               [24] 1172 	movx	@dptr,a
      00548B 90 00 4A         [24] 1173 	mov	dptr,#0x004a
      00548E 12 88 D0         [24] 1174 	lcall	_flash_write
                                   1175 ;	..\src\COMMON\GoldSequence.c:127: flash_write(0x004C,0b1101010011100011);
      005491 90 04 C6         [24] 1176 	mov	dptr,#_flash_write_PARM_2
      005494 74 E3            [12] 1177 	mov	a,#0xe3
      005496 F0               [24] 1178 	movx	@dptr,a
      005497 74 D4            [12] 1179 	mov	a,#0xd4
      005499 A3               [24] 1180 	inc	dptr
      00549A F0               [24] 1181 	movx	@dptr,a
      00549B 90 00 4C         [24] 1182 	mov	dptr,#0x004c
      00549E 12 88 D0         [24] 1183 	lcall	_flash_write
                                   1184 ;	..\src\COMMON\GoldSequence.c:129: flash_write(0x004E,0b010101110010100);
      0054A1 90 04 C6         [24] 1185 	mov	dptr,#_flash_write_PARM_2
      0054A4 74 94            [12] 1186 	mov	a,#0x94
      0054A6 F0               [24] 1187 	movx	@dptr,a
      0054A7 74 2B            [12] 1188 	mov	a,#0x2b
      0054A9 A3               [24] 1189 	inc	dptr
      0054AA F0               [24] 1190 	movx	@dptr,a
      0054AB 90 00 4E         [24] 1191 	mov	dptr,#0x004e
      0054AE 12 88 D0         [24] 1192 	lcall	_flash_write
                                   1193 ;	..\src\COMMON\GoldSequence.c:132: flash_write(0x0050,0b1110111100000111);
      0054B1 90 04 C6         [24] 1194 	mov	dptr,#_flash_write_PARM_2
      0054B4 74 07            [12] 1195 	mov	a,#0x07
      0054B6 F0               [24] 1196 	movx	@dptr,a
      0054B7 74 EF            [12] 1197 	mov	a,#0xef
      0054B9 A3               [24] 1198 	inc	dptr
      0054BA F0               [24] 1199 	movx	@dptr,a
      0054BB 90 00 50         [24] 1200 	mov	dptr,#0x0050
      0054BE 12 88 D0         [24] 1201 	lcall	_flash_write
                                   1202 ;	..\src\COMMON\GoldSequence.c:134: flash_write(0x0052,0b100101011111110);
      0054C1 90 04 C6         [24] 1203 	mov	dptr,#_flash_write_PARM_2
      0054C4 74 FE            [12] 1204 	mov	a,#0xfe
      0054C6 F0               [24] 1205 	movx	@dptr,a
      0054C7 74 4A            [12] 1206 	mov	a,#0x4a
      0054C9 A3               [24] 1207 	inc	dptr
      0054CA F0               [24] 1208 	movx	@dptr,a
      0054CB 90 00 52         [24] 1209 	mov	dptr,#0x0052
      0054CE 12 88 D0         [24] 1210 	lcall	_flash_write
                                   1211 ;	..\src\COMMON\GoldSequence.c:137: flash_write(0x0054,0b1111001011110101);
      0054D1 90 04 C6         [24] 1212 	mov	dptr,#_flash_write_PARM_2
      0054D4 74 F5            [12] 1213 	mov	a,#0xf5
      0054D6 F0               [24] 1214 	movx	@dptr,a
      0054D7 74 F2            [12] 1215 	mov	a,#0xf2
      0054D9 A3               [24] 1216 	inc	dptr
      0054DA F0               [24] 1217 	movx	@dptr,a
      0054DB 90 00 54         [24] 1218 	mov	dptr,#0x0054
      0054DE 12 88 D0         [24] 1219 	lcall	_flash_write
                                   1220 ;	..\src\COMMON\GoldSequence.c:139: flash_write(0x0056,0b111101001001011);
      0054E1 90 04 C6         [24] 1221 	mov	dptr,#_flash_write_PARM_2
      0054E4 74 4B            [12] 1222 	mov	a,#0x4b
      0054E6 F0               [24] 1223 	movx	@dptr,a
      0054E7 74 7A            [12] 1224 	mov	a,#0x7a
      0054E9 A3               [24] 1225 	inc	dptr
      0054EA F0               [24] 1226 	movx	@dptr,a
      0054EB 90 00 56         [24] 1227 	mov	dptr,#0x0056
      0054EE 12 88 D0         [24] 1228 	lcall	_flash_write
                                   1229 ;	..\src\COMMON\GoldSequence.c:141: flash_write(0x0058,0b0111110000001100);
      0054F1 90 04 C6         [24] 1230 	mov	dptr,#_flash_write_PARM_2
      0054F4 74 0C            [12] 1231 	mov	a,#0x0c
      0054F6 F0               [24] 1232 	movx	@dptr,a
      0054F7 74 7C            [12] 1233 	mov	a,#0x7c
      0054F9 A3               [24] 1234 	inc	dptr
      0054FA F0               [24] 1235 	movx	@dptr,a
      0054FB 90 00 58         [24] 1236 	mov	dptr,#0x0058
      0054FE 12 88 D0         [24] 1237 	lcall	_flash_write
                                   1238 ;	..\src\COMMON\GoldSequence.c:143: flash_write(0x005A,0b110001000010001);
      005501 90 04 C6         [24] 1239 	mov	dptr,#_flash_write_PARM_2
      005504 74 11            [12] 1240 	mov	a,#0x11
      005506 F0               [24] 1241 	movx	@dptr,a
      005507 74 62            [12] 1242 	mov	a,#0x62
      005509 A3               [24] 1243 	inc	dptr
      00550A F0               [24] 1244 	movx	@dptr,a
      00550B 90 00 5A         [24] 1245 	mov	dptr,#0x005a
      00550E 12 88 D0         [24] 1246 	lcall	_flash_write
                                   1247 ;	..\src\COMMON\GoldSequence.c:146: flash_write(0x005C,0b0011101101110000);
      005511 90 04 C6         [24] 1248 	mov	dptr,#_flash_write_PARM_2
      005514 74 70            [12] 1249 	mov	a,#0x70
      005516 F0               [24] 1250 	movx	@dptr,a
      005517 74 3B            [12] 1251 	mov	a,#0x3b
      005519 A3               [24] 1252 	inc	dptr
      00551A F0               [24] 1253 	movx	@dptr,a
      00551B 90 00 5C         [24] 1254 	mov	dptr,#0x005c
      00551E 12 88 D0         [24] 1255 	lcall	_flash_write
                                   1256 ;	..\src\COMMON\GoldSequence.c:148: flash_write(0x005E,0b010111000111100);
      005521 90 04 C6         [24] 1257 	mov	dptr,#_flash_write_PARM_2
      005524 74 3C            [12] 1258 	mov	a,#0x3c
      005526 F0               [24] 1259 	movx	@dptr,a
      005527 74 2E            [12] 1260 	mov	a,#0x2e
      005529 A3               [24] 1261 	inc	dptr
      00552A F0               [24] 1262 	movx	@dptr,a
      00552B 90 00 5E         [24] 1263 	mov	dptr,#0x005e
      00552E 12 88 D0         [24] 1264 	lcall	_flash_write
                                   1265 ;	..\src\COMMON\GoldSequence.c:151: flash_write(0x0060,0b1001100011001110);
      005531 90 04 C6         [24] 1266 	mov	dptr,#_flash_write_PARM_2
      005534 74 CE            [12] 1267 	mov	a,#0xce
      005536 F0               [24] 1268 	movx	@dptr,a
      005537 74 98            [12] 1269 	mov	a,#0x98
      005539 A3               [24] 1270 	inc	dptr
      00553A F0               [24] 1271 	movx	@dptr,a
      00553B 90 00 60         [24] 1272 	mov	dptr,#0x0060
      00553E 12 88 D0         [24] 1273 	lcall	_flash_write
                                   1274 ;	..\src\COMMON\GoldSequence.c:153: flash_write(0x0062,0b000100000101010);
      005541 90 04 C6         [24] 1275 	mov	dptr,#_flash_write_PARM_2
      005544 74 2A            [12] 1276 	mov	a,#0x2a
      005546 F0               [24] 1277 	movx	@dptr,a
      005547 74 08            [12] 1278 	mov	a,#0x08
      005549 A3               [24] 1279 	inc	dptr
      00554A F0               [24] 1280 	movx	@dptr,a
      00554B 90 00 62         [24] 1281 	mov	dptr,#0x0062
      00554E 12 88 D0         [24] 1282 	lcall	_flash_write
                                   1283 ;	..\src\COMMON\GoldSequence.c:156: flash_write(0x0064,0b1100100100010001);
      005551 90 04 C6         [24] 1284 	mov	dptr,#_flash_write_PARM_2
      005554 74 11            [12] 1285 	mov	a,#0x11
      005556 F0               [24] 1286 	movx	@dptr,a
      005557 74 C9            [12] 1287 	mov	a,#0xc9
      005559 A3               [24] 1288 	inc	dptr
      00555A F0               [24] 1289 	movx	@dptr,a
      00555B 90 00 64         [24] 1290 	mov	dptr,#0x0064
      00555E 12 88 D0         [24] 1291 	lcall	_flash_write
                                   1292 ;	..\src\COMMON\GoldSequence.c:158: flash_write(0x0066,0b001101100100001);
      005561 90 04 C6         [24] 1293 	mov	dptr,#_flash_write_PARM_2
      005564 74 21            [12] 1294 	mov	a,#0x21
      005566 F0               [24] 1295 	movx	@dptr,a
      005567 74 1B            [12] 1296 	mov	a,#0x1b
      005569 A3               [24] 1297 	inc	dptr
      00556A F0               [24] 1298 	movx	@dptr,a
      00556B 90 00 66         [24] 1299 	mov	dptr,#0x0066
      00556E 12 88 D0         [24] 1300 	lcall	_flash_write
                                   1301 ;	..\src\COMMON\GoldSequence.c:161: flash_write(0x0068,0b0110000111111110);
      005571 90 04 C6         [24] 1302 	mov	dptr,#_flash_write_PARM_2
      005574 74 FE            [12] 1303 	mov	a,#0xfe
      005576 F0               [24] 1304 	movx	@dptr,a
      005577 74 61            [12] 1305 	mov	a,#0x61
      005579 A3               [24] 1306 	inc	dptr
      00557A F0               [24] 1307 	movx	@dptr,a
      00557B 90 00 68         [24] 1308 	mov	dptr,#0x0068
      00557E 12 88 D0         [24] 1309 	lcall	_flash_write
                                   1310 ;	..\src\COMMON\GoldSequence.c:163: flash_write(0x006A,0b101001010100100);
      005581 90 04 C6         [24] 1311 	mov	dptr,#_flash_write_PARM_2
      005584 74 A4            [12] 1312 	mov	a,#0xa4
      005586 F0               [24] 1313 	movx	@dptr,a
      005587 03               [12] 1314 	rr	a
      005588 A3               [24] 1315 	inc	dptr
      005589 F0               [24] 1316 	movx	@dptr,a
      00558A 90 00 6A         [24] 1317 	mov	dptr,#0x006a
      00558D 12 88 D0         [24] 1318 	lcall	_flash_write
                                   1319 ;	..\src\COMMON\GoldSequence.c:166: flash_write(0x006C,0b1011010110001001);
      005590 90 04 C6         [24] 1320 	mov	dptr,#_flash_write_PARM_2
      005593 74 89            [12] 1321 	mov	a,#0x89
      005595 F0               [24] 1322 	movx	@dptr,a
      005596 74 B5            [12] 1323 	mov	a,#0xb5
      005598 A3               [24] 1324 	inc	dptr
      005599 F0               [24] 1325 	movx	@dptr,a
      00559A 90 00 6C         [24] 1326 	mov	dptr,#0x006c
      00559D 12 88 D0         [24] 1327 	lcall	_flash_write
                                   1328 ;	..\src\COMMON\GoldSequence.c:168: flash_write(0x006E,0b011011001100110);
      0055A0 90 04 C6         [24] 1329 	mov	dptr,#_flash_write_PARM_2
      0055A3 74 66            [12] 1330 	mov	a,#0x66
      0055A5 F0               [24] 1331 	movx	@dptr,a
      0055A6 74 36            [12] 1332 	mov	a,#0x36
      0055A8 A3               [24] 1333 	inc	dptr
      0055A9 F0               [24] 1334 	movx	@dptr,a
      0055AA 90 00 6E         [24] 1335 	mov	dptr,#0x006e
      0055AD 12 88 D0         [24] 1336 	lcall	_flash_write
                                   1337 ;	..\src\COMMON\GoldSequence.c:171: flash_write(0x0070,0b1101111110110010);
      0055B0 90 04 C6         [24] 1338 	mov	dptr,#_flash_write_PARM_2
      0055B3 74 B2            [12] 1339 	mov	a,#0xb2
      0055B5 F0               [24] 1340 	movx	@dptr,a
      0055B6 74 DF            [12] 1341 	mov	a,#0xdf
      0055B8 A3               [24] 1342 	inc	dptr
      0055B9 F0               [24] 1343 	movx	@dptr,a
      0055BA 90 00 70         [24] 1344 	mov	dptr,#0x0070
      0055BD 12 88 D0         [24] 1345 	lcall	_flash_write
                                   1346 ;	..\src\COMMON\GoldSequence.c:173: flash_write(0x0072,0b100010000000111);
      0055C0 90 04 C6         [24] 1347 	mov	dptr,#_flash_write_PARM_2
      0055C3 74 07            [12] 1348 	mov	a,#0x07
      0055C5 F0               [24] 1349 	movx	@dptr,a
      0055C6 74 44            [12] 1350 	mov	a,#0x44
      0055C8 A3               [24] 1351 	inc	dptr
      0055C9 F0               [24] 1352 	movx	@dptr,a
      0055CA 90 00 72         [24] 1353 	mov	dptr,#0x0072
      0055CD 12 88 D0         [24] 1354 	lcall	_flash_write
                                   1355 ;	..\src\COMMON\GoldSequence.c:176: flash_write(0x0074,0b0110101010101111);
      0055D0 90 04 C6         [24] 1356 	mov	dptr,#_flash_write_PARM_2
      0055D3 74 AF            [12] 1357 	mov	a,#0xaf
      0055D5 F0               [24] 1358 	movx	@dptr,a
      0055D6 74 6A            [12] 1359 	mov	a,#0x6a
      0055D8 A3               [24] 1360 	inc	dptr
      0055D9 F0               [24] 1361 	movx	@dptr,a
      0055DA 90 00 74         [24] 1362 	mov	dptr,#0x0074
      0055DD 12 88 D0         [24] 1363 	lcall	_flash_write
                                   1364 ;	..\src\COMMON\GoldSequence.c:178: flash_write(0x0076,0b011110100110111);
      0055E0 90 04 C6         [24] 1365 	mov	dptr,#_flash_write_PARM_2
      0055E3 74 37            [12] 1366 	mov	a,#0x37
      0055E5 F0               [24] 1367 	movx	@dptr,a
      0055E6 74 3D            [12] 1368 	mov	a,#0x3d
      0055E8 A3               [24] 1369 	inc	dptr
      0055E9 F0               [24] 1370 	movx	@dptr,a
      0055EA 90 00 76         [24] 1371 	mov	dptr,#0x0076
      0055ED 12 88 D0         [24] 1372 	lcall	_flash_write
                                   1373 ;	..\src\COMMON\GoldSequence.c:181: flash_write(0x0078,0b0011000000100001);
      0055F0 90 04 C6         [24] 1374 	mov	dptr,#_flash_write_PARM_2
      0055F3 74 21            [12] 1375 	mov	a,#0x21
      0055F5 F0               [24] 1376 	movx	@dptr,a
      0055F6 74 30            [12] 1377 	mov	a,#0x30
      0055F8 A3               [24] 1378 	inc	dptr
      0055F9 F0               [24] 1379 	movx	@dptr,a
      0055FA 90 00 78         [24] 1380 	mov	dptr,#0x0078
      0055FD 12 88 D0         [24] 1381 	lcall	_flash_write
                                   1382 ;	..\src\COMMON\GoldSequence.c:183: flash_write(0x007A,0b100000110101111);
      005600 90 04 C6         [24] 1383 	mov	dptr,#_flash_write_PARM_2
      005603 74 AF            [12] 1384 	mov	a,#0xaf
      005605 F0               [24] 1385 	movx	@dptr,a
      005606 74 41            [12] 1386 	mov	a,#0x41
      005608 A3               [24] 1387 	inc	dptr
      005609 F0               [24] 1388 	movx	@dptr,a
      00560A 90 00 7A         [24] 1389 	mov	dptr,#0x007a
      00560D 12 88 D0         [24] 1390 	lcall	_flash_write
                                   1391 ;	..\src\COMMON\GoldSequence.c:186: flash_write(0x007C,0b0001110101100110);
      005610 90 04 C6         [24] 1392 	mov	dptr,#_flash_write_PARM_2
      005613 74 66            [12] 1393 	mov	a,#0x66
      005615 F0               [24] 1394 	movx	@dptr,a
      005616 74 1D            [12] 1395 	mov	a,#0x1d
      005618 A3               [24] 1396 	inc	dptr
      005619 F0               [24] 1397 	movx	@dptr,a
      00561A 90 00 7C         [24] 1398 	mov	dptr,#0x007c
      00561D 12 88 D0         [24] 1399 	lcall	_flash_write
                                   1400 ;	..\src\COMMON\GoldSequence.c:188: flash_write(0x007E,0b111111111100011);
      005620 90 04 C6         [24] 1401 	mov	dptr,#_flash_write_PARM_2
      005623 74 E3            [12] 1402 	mov	a,#0xe3
      005625 F0               [24] 1403 	movx	@dptr,a
      005626 74 7F            [12] 1404 	mov	a,#0x7f
      005628 A3               [24] 1405 	inc	dptr
      005629 F0               [24] 1406 	movx	@dptr,a
      00562A 90 00 7E         [24] 1407 	mov	dptr,#0x007e
      00562D 12 88 D0         [24] 1408 	lcall	_flash_write
                                   1409 ;	..\src\COMMON\GoldSequence.c:191: flash_write(0x0080,0b0000101111000101);
      005630 90 04 C6         [24] 1410 	mov	dptr,#_flash_write_PARM_2
      005633 74 C5            [12] 1411 	mov	a,#0xc5
      005635 F0               [24] 1412 	movx	@dptr,a
      005636 74 0B            [12] 1413 	mov	a,#0x0b
      005638 A3               [24] 1414 	inc	dptr
      005639 F0               [24] 1415 	movx	@dptr,a
      00563A 90 00 80         [24] 1416 	mov	dptr,#0x0080
      00563D 12 88 D0         [24] 1417 	lcall	_flash_write
                                   1418 ;	..\src\COMMON\GoldSequence.c:193: flash_write(0x0082,0b010000011000101);
      005640 90 04 C6         [24] 1419 	mov	dptr,#_flash_write_PARM_2
      005643 74 C5            [12] 1420 	mov	a,#0xc5
      005645 F0               [24] 1421 	movx	@dptr,a
      005646 74 20            [12] 1422 	mov	a,#0x20
      005648 A3               [24] 1423 	inc	dptr
      005649 F0               [24] 1424 	movx	@dptr,a
      00564A 90 00 82         [24] 1425 	mov	dptr,#0x0082
      00564D 12 88 D0         [24] 1426 	lcall	_flash_write
                                   1427 ;	..\src\COMMON\GoldSequence.c:195: flash_lock();
      005650 02 94 74         [24] 1428 	ljmp	_flash_lock
                                   1429 ;------------------------------------------------------------
                                   1430 ;Allocation info for local variables in function 'GOLDSEQUENCE_Obtain'
                                   1431 ;------------------------------------------------------------
                                   1432 ;sloc0                     Allocated with name '_GOLDSEQUENCE_Obtain_sloc0_1_0'
                                   1433 ;Rnd                       Allocated with name '_GOLDSEQUENCE_Obtain_Rnd_1_123'
                                   1434 ;RetVal                    Allocated with name '_GOLDSEQUENCE_Obtain_RetVal_1_123'
                                   1435 ;------------------------------------------------------------
                                   1436 ;	..\src\COMMON\GoldSequence.c:212: __xdata uint32_t GOLDSEQUENCE_Obtain(void)
                                   1437 ;	-----------------------------------------
                                   1438 ;	 function GOLDSEQUENCE_Obtain
                                   1439 ;	-----------------------------------------
      005653                       1440 _GOLDSEQUENCE_Obtain:
                                   1441 ;	..\src\COMMON\GoldSequence.c:217: Rnd = random();
      005653 12 8E 47         [24] 1442 	lcall	_random
      005656 AE 82            [24] 1443 	mov	r6,dpl
      005658 AF 83            [24] 1444 	mov	r7,dph
                                   1445 ;	..\src\COMMON\GoldSequence.c:218: flash_unlock();
      00565A C0 07            [24] 1446 	push	ar7
      00565C C0 06            [24] 1447 	push	ar6
      00565E 12 8A 97         [24] 1448 	lcall	_flash_unlock
      005661 D0 06            [24] 1449 	pop	ar6
      005663 D0 07            [24] 1450 	pop	ar7
                                   1451 ;	..\src\COMMON\GoldSequence.c:220: Rnd = ((Rnd % 33)*4);  // MOD operation is done so that the number is between 0 and 33, once a number between 0 and 32 has been obtained, its multiplied by 4 to match the memory address
      005665 90 04 D2         [24] 1452 	mov	dptr,#__moduint_PARM_2
      005668 74 21            [12] 1453 	mov	a,#0x21
      00566A F0               [24] 1454 	movx	@dptr,a
      00566B E4               [12] 1455 	clr	a
      00566C A3               [24] 1456 	inc	dptr
      00566D F0               [24] 1457 	movx	@dptr,a
      00566E 8E 82            [24] 1458 	mov	dpl,r6
      005670 8F 83            [24] 1459 	mov	dph,r7
      005672 12 8C DB         [24] 1460 	lcall	__moduint
      005675 AE 82            [24] 1461 	mov	r6,dpl
      005677 E5 83            [12] 1462 	mov	a,dph
      005679 CE               [12] 1463 	xch	a,r6
      00567A 25 E0            [12] 1464 	add	a,acc
      00567C CE               [12] 1465 	xch	a,r6
      00567D 33               [12] 1466 	rlc	a
      00567E CE               [12] 1467 	xch	a,r6
      00567F 25 E0            [12] 1468 	add	a,acc
      005681 CE               [12] 1469 	xch	a,r6
      005682 33               [12] 1470 	rlc	a
      005683 FF               [12] 1471 	mov	r7,a
                                   1472 ;	..\src\COMMON\GoldSequence.c:221: RetVal = ((uint32_t)flash_read(Rnd))<<16;
      005684 8E 82            [24] 1473 	mov	dpl,r6
      005686 8F 83            [24] 1474 	mov	dph,r7
      005688 C0 07            [24] 1475 	push	ar7
      00568A C0 06            [24] 1476 	push	ar6
      00568C 12 90 53         [24] 1477 	lcall	_flash_read
      00568F AC 82            [24] 1478 	mov	r4,dpl
      005691 AD 83            [24] 1479 	mov	r5,dph
      005693 D0 06            [24] 1480 	pop	ar6
      005695 D0 07            [24] 1481 	pop	ar7
      005697 7B 00            [12] 1482 	mov	r3,#0x00
      005699 8D 25            [24] 1483 	mov	(_GOLDSEQUENCE_Obtain_sloc0_1_0 + 3),r5
      00569B 8C 24            [24] 1484 	mov	(_GOLDSEQUENCE_Obtain_sloc0_1_0 + 2),r4
                                   1485 ;	1-genFromRTrack replaced	mov	(_GOLDSEQUENCE_Obtain_sloc0_1_0 + 1),#0x00
      00569D 8B 23            [24] 1486 	mov	(_GOLDSEQUENCE_Obtain_sloc0_1_0 + 1),r3
                                   1487 ;	1-genFromRTrack replaced	mov	_GOLDSEQUENCE_Obtain_sloc0_1_0,#0x00
      00569F 8B 22            [24] 1488 	mov	_GOLDSEQUENCE_Obtain_sloc0_1_0,r3
                                   1489 ;	..\src\COMMON\GoldSequence.c:222: RetVal |= (((uint32_t)flash_read(Rnd+2)) & 0x0000FFFF)<<1;
      0056A1 74 02            [12] 1490 	mov	a,#0x02
      0056A3 2E               [12] 1491 	add	a,r6
      0056A4 F8               [12] 1492 	mov	r0,a
      0056A5 E4               [12] 1493 	clr	a
      0056A6 3F               [12] 1494 	addc	a,r7
      0056A7 F9               [12] 1495 	mov	r1,a
      0056A8 88 82            [24] 1496 	mov	dpl,r0
      0056AA 89 83            [24] 1497 	mov	dph,r1
      0056AC C0 07            [24] 1498 	push	ar7
      0056AE C0 06            [24] 1499 	push	ar6
      0056B0 12 90 53         [24] 1500 	lcall	_flash_read
      0056B3 A8 82            [24] 1501 	mov	r0,dpl
      0056B5 A9 83            [24] 1502 	mov	r1,dph
      0056B7 D0 06            [24] 1503 	pop	ar6
      0056B9 D0 07            [24] 1504 	pop	ar7
      0056BB E4               [12] 1505 	clr	a
      0056BC FC               [12] 1506 	mov	r4,a
      0056BD FD               [12] 1507 	mov	r5,a
      0056BE E8               [12] 1508 	mov	a,r0
      0056BF 28               [12] 1509 	add	a,r0
      0056C0 F8               [12] 1510 	mov	r0,a
      0056C1 E9               [12] 1511 	mov	a,r1
      0056C2 33               [12] 1512 	rlc	a
      0056C3 F9               [12] 1513 	mov	r1,a
      0056C4 EC               [12] 1514 	mov	a,r4
      0056C5 33               [12] 1515 	rlc	a
      0056C6 FC               [12] 1516 	mov	r4,a
      0056C7 ED               [12] 1517 	mov	a,r5
      0056C8 33               [12] 1518 	rlc	a
      0056C9 FD               [12] 1519 	mov	r5,a
      0056CA E5 22            [12] 1520 	mov	a,_GOLDSEQUENCE_Obtain_sloc0_1_0
      0056CC 42 00            [12] 1521 	orl	ar0,a
      0056CE E5 23            [12] 1522 	mov	a,(_GOLDSEQUENCE_Obtain_sloc0_1_0 + 1)
      0056D0 42 01            [12] 1523 	orl	ar1,a
      0056D2 E5 24            [12] 1524 	mov	a,(_GOLDSEQUENCE_Obtain_sloc0_1_0 + 2)
      0056D4 42 04            [12] 1525 	orl	ar4,a
      0056D6 E5 25            [12] 1526 	mov	a,(_GOLDSEQUENCE_Obtain_sloc0_1_0 + 3)
      0056D8 42 05            [12] 1527 	orl	ar5,a
                                   1528 ;	..\src\COMMON\GoldSequence.c:223: dbglink_writehex32(Rnd,1,WRNUM_PADZERO);
      0056DA 7B 00            [12] 1529 	mov	r3,#0x00
      0056DC 7A 00            [12] 1530 	mov	r2,#0x00
      0056DE C0 05            [24] 1531 	push	ar5
      0056E0 C0 04            [24] 1532 	push	ar4
      0056E2 C0 01            [24] 1533 	push	ar1
      0056E4 C0 00            [24] 1534 	push	ar0
      0056E6 74 08            [12] 1535 	mov	a,#0x08
      0056E8 C0 E0            [24] 1536 	push	acc
      0056EA 74 01            [12] 1537 	mov	a,#0x01
      0056EC C0 E0            [24] 1538 	push	acc
      0056EE 8E 82            [24] 1539 	mov	dpl,r6
      0056F0 8F 83            [24] 1540 	mov	dph,r7
      0056F2 8B F0            [24] 1541 	mov	b,r3
      0056F4 EA               [12] 1542 	mov	a,r2
      0056F5 12 91 03         [24] 1543 	lcall	_dbglink_writehex32
      0056F8 15 81            [12] 1544 	dec	sp
      0056FA 15 81            [12] 1545 	dec	sp
                                   1546 ;	..\src\COMMON\GoldSequence.c:224: dbglink_writestr("  ");
      0056FC 90 9A 8F         [24] 1547 	mov	dptr,#___str_0
      0056FF 75 F0 80         [24] 1548 	mov	b,#0x80
      005702 12 8C 64         [24] 1549 	lcall	_dbglink_writestr
      005705 D0 00            [24] 1550 	pop	ar0
      005707 D0 01            [24] 1551 	pop	ar1
      005709 D0 04            [24] 1552 	pop	ar4
      00570B D0 05            [24] 1553 	pop	ar5
                                   1554 ;	..\src\COMMON\GoldSequence.c:225: dbglink_writehex32(RetVal,1,WRNUM_PADZERO);
      00570D C0 05            [24] 1555 	push	ar5
      00570F C0 04            [24] 1556 	push	ar4
      005711 C0 01            [24] 1557 	push	ar1
      005713 C0 00            [24] 1558 	push	ar0
      005715 74 08            [12] 1559 	mov	a,#0x08
      005717 C0 E0            [24] 1560 	push	acc
      005719 74 01            [12] 1561 	mov	a,#0x01
      00571B C0 E0            [24] 1562 	push	acc
      00571D 88 82            [24] 1563 	mov	dpl,r0
      00571F 89 83            [24] 1564 	mov	dph,r1
      005721 8C F0            [24] 1565 	mov	b,r4
      005723 ED               [12] 1566 	mov	a,r5
      005724 12 91 03         [24] 1567 	lcall	_dbglink_writehex32
      005727 15 81            [12] 1568 	dec	sp
      005729 15 81            [12] 1569 	dec	sp
                                   1570 ;	..\src\COMMON\GoldSequence.c:226: flash_lock();
      00572B 12 94 74         [24] 1571 	lcall	_flash_lock
      00572E D0 00            [24] 1572 	pop	ar0
      005730 D0 01            [24] 1573 	pop	ar1
      005732 D0 04            [24] 1574 	pop	ar4
      005734 D0 05            [24] 1575 	pop	ar5
                                   1576 ;	..\src\COMMON\GoldSequence.c:227: return(RetVal);
      005736 88 82            [24] 1577 	mov	dpl,r0
      005738 89 83            [24] 1578 	mov	dph,r1
      00573A 8C F0            [24] 1579 	mov	b,r4
      00573C ED               [12] 1580 	mov	a,r5
      00573D 22               [24] 1581 	ret
                                   1582 	.area CSEG    (CODE)
                                   1583 	.area CONST   (CODE)
      009A8F                       1584 ___str_0:
      009A8F 20 20                 1585 	.ascii "  "
      009A91 00                    1586 	.db 0x00
                                   1587 	.area XINIT   (CODE)
                                   1588 	.area CABS    (ABS,CODE)
