                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.9.3 #11354 (MINGW32)
                                      4 ;--------------------------------------------------------
                                      5 	.module easyax5043
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _ax5043_init_registers_rx
                                     12 	.globl _ax5043_init_registers_tx
                                     13 	.globl _VerifyACK
                                     14 	.globl _BEACON_decoding
                                     15 	.globl _dbglink_writestr
                                     16 	.globl _memset
                                     17 	.globl _memcpy
                                     18 	.globl _wtimer_remove_callback
                                     19 	.globl _wtimer_add_callback
                                     20 	.globl _wtimer_remove
                                     21 	.globl _wtimer1_addrelative
                                     22 	.globl _wtimer0_addrelative
                                     23 	.globl _wtimer0_addabsolute
                                     24 	.globl _wtimer0_curtime
                                     25 	.globl _wtimer_runcallbacks
                                     26 	.globl _wtimer_idle
                                     27 	.globl _ax5043_writefifo
                                     28 	.globl _ax5043_readfifo
                                     29 	.globl _ax5043_wakeup_deepsleep
                                     30 	.globl _ax5043_enter_deepsleep
                                     31 	.globl _ax5043_reset
                                     32 	.globl _radio_read24
                                     33 	.globl _radio_read16
                                     34 	.globl _pn9_buffer
                                     35 	.globl _pn9_advance_byte
                                     36 	.globl _pn9_advance_bits
                                     37 	.globl _axradio_framing_append_crc
                                     38 	.globl _axradio_framing_check_crc
                                     39 	.globl _ax5043_set_registers_rxcont_singleparamset
                                     40 	.globl _ax5043_set_registers_rxcont
                                     41 	.globl _ax5043_set_registers_rxwor
                                     42 	.globl _ax5043_set_registers_rx
                                     43 	.globl _ax5043_set_registers_tx
                                     44 	.globl _ax5043_set_registers
                                     45 	.globl _axradio_conv_freq_fromreg
                                     46 	.globl _axradio_statuschange
                                     47 	.globl _axradio_conv_timeinterval_totimer0
                                     48 	.globl _checksignedlimit32
                                     49 	.globl _checksignedlimit16
                                     50 	.globl _signedlimit16
                                     51 	.globl _signextend24
                                     52 	.globl _signextend20
                                     53 	.globl _signextend16
                                     54 	.globl _PORTC_7
                                     55 	.globl _PORTC_6
                                     56 	.globl _PORTC_5
                                     57 	.globl _PORTC_4
                                     58 	.globl _PORTC_3
                                     59 	.globl _PORTC_2
                                     60 	.globl _PORTC_1
                                     61 	.globl _PORTC_0
                                     62 	.globl _PORTB_7
                                     63 	.globl _PORTB_6
                                     64 	.globl _PORTB_5
                                     65 	.globl _PORTB_4
                                     66 	.globl _PORTB_3
                                     67 	.globl _PORTB_2
                                     68 	.globl _PORTB_1
                                     69 	.globl _PORTB_0
                                     70 	.globl _PORTA_7
                                     71 	.globl _PORTA_6
                                     72 	.globl _PORTA_5
                                     73 	.globl _PORTA_4
                                     74 	.globl _PORTA_3
                                     75 	.globl _PORTA_2
                                     76 	.globl _PORTA_1
                                     77 	.globl _PORTA_0
                                     78 	.globl _PINC_7
                                     79 	.globl _PINC_6
                                     80 	.globl _PINC_5
                                     81 	.globl _PINC_4
                                     82 	.globl _PINC_3
                                     83 	.globl _PINC_2
                                     84 	.globl _PINC_1
                                     85 	.globl _PINC_0
                                     86 	.globl _PINB_7
                                     87 	.globl _PINB_6
                                     88 	.globl _PINB_5
                                     89 	.globl _PINB_4
                                     90 	.globl _PINB_3
                                     91 	.globl _PINB_2
                                     92 	.globl _PINB_1
                                     93 	.globl _PINB_0
                                     94 	.globl _PINA_7
                                     95 	.globl _PINA_6
                                     96 	.globl _PINA_5
                                     97 	.globl _PINA_4
                                     98 	.globl _PINA_3
                                     99 	.globl _PINA_2
                                    100 	.globl _PINA_1
                                    101 	.globl _PINA_0
                                    102 	.globl _CY
                                    103 	.globl _AC
                                    104 	.globl _F0
                                    105 	.globl _RS1
                                    106 	.globl _RS0
                                    107 	.globl _OV
                                    108 	.globl _F1
                                    109 	.globl _P
                                    110 	.globl _IP_7
                                    111 	.globl _IP_6
                                    112 	.globl _IP_5
                                    113 	.globl _IP_4
                                    114 	.globl _IP_3
                                    115 	.globl _IP_2
                                    116 	.globl _IP_1
                                    117 	.globl _IP_0
                                    118 	.globl _EA
                                    119 	.globl _IE_7
                                    120 	.globl _IE_6
                                    121 	.globl _IE_5
                                    122 	.globl _IE_4
                                    123 	.globl _IE_3
                                    124 	.globl _IE_2
                                    125 	.globl _IE_1
                                    126 	.globl _IE_0
                                    127 	.globl _EIP_7
                                    128 	.globl _EIP_6
                                    129 	.globl _EIP_5
                                    130 	.globl _EIP_4
                                    131 	.globl _EIP_3
                                    132 	.globl _EIP_2
                                    133 	.globl _EIP_1
                                    134 	.globl _EIP_0
                                    135 	.globl _EIE_7
                                    136 	.globl _EIE_6
                                    137 	.globl _EIE_5
                                    138 	.globl _EIE_4
                                    139 	.globl _EIE_3
                                    140 	.globl _EIE_2
                                    141 	.globl _EIE_1
                                    142 	.globl _EIE_0
                                    143 	.globl _E2IP_7
                                    144 	.globl _E2IP_6
                                    145 	.globl _E2IP_5
                                    146 	.globl _E2IP_4
                                    147 	.globl _E2IP_3
                                    148 	.globl _E2IP_2
                                    149 	.globl _E2IP_1
                                    150 	.globl _E2IP_0
                                    151 	.globl _E2IE_7
                                    152 	.globl _E2IE_6
                                    153 	.globl _E2IE_5
                                    154 	.globl _E2IE_4
                                    155 	.globl _E2IE_3
                                    156 	.globl _E2IE_2
                                    157 	.globl _E2IE_1
                                    158 	.globl _E2IE_0
                                    159 	.globl _B_7
                                    160 	.globl _B_6
                                    161 	.globl _B_5
                                    162 	.globl _B_4
                                    163 	.globl _B_3
                                    164 	.globl _B_2
                                    165 	.globl _B_1
                                    166 	.globl _B_0
                                    167 	.globl _ACC_7
                                    168 	.globl _ACC_6
                                    169 	.globl _ACC_5
                                    170 	.globl _ACC_4
                                    171 	.globl _ACC_3
                                    172 	.globl _ACC_2
                                    173 	.globl _ACC_1
                                    174 	.globl _ACC_0
                                    175 	.globl _WTSTAT
                                    176 	.globl _WTIRQEN
                                    177 	.globl _WTEVTD
                                    178 	.globl _WTEVTD1
                                    179 	.globl _WTEVTD0
                                    180 	.globl _WTEVTC
                                    181 	.globl _WTEVTC1
                                    182 	.globl _WTEVTC0
                                    183 	.globl _WTEVTB
                                    184 	.globl _WTEVTB1
                                    185 	.globl _WTEVTB0
                                    186 	.globl _WTEVTA
                                    187 	.globl _WTEVTA1
                                    188 	.globl _WTEVTA0
                                    189 	.globl _WTCNTR1
                                    190 	.globl _WTCNTB
                                    191 	.globl _WTCNTB1
                                    192 	.globl _WTCNTB0
                                    193 	.globl _WTCNTA
                                    194 	.globl _WTCNTA1
                                    195 	.globl _WTCNTA0
                                    196 	.globl _WTCFGB
                                    197 	.globl _WTCFGA
                                    198 	.globl _WDTRESET
                                    199 	.globl _WDTCFG
                                    200 	.globl _U1STATUS
                                    201 	.globl _U1SHREG
                                    202 	.globl _U1MODE
                                    203 	.globl _U1CTRL
                                    204 	.globl _U0STATUS
                                    205 	.globl _U0SHREG
                                    206 	.globl _U0MODE
                                    207 	.globl _U0CTRL
                                    208 	.globl _T2STATUS
                                    209 	.globl _T2PERIOD
                                    210 	.globl _T2PERIOD1
                                    211 	.globl _T2PERIOD0
                                    212 	.globl _T2MODE
                                    213 	.globl _T2CNT
                                    214 	.globl _T2CNT1
                                    215 	.globl _T2CNT0
                                    216 	.globl _T2CLKSRC
                                    217 	.globl _T1STATUS
                                    218 	.globl _T1PERIOD
                                    219 	.globl _T1PERIOD1
                                    220 	.globl _T1PERIOD0
                                    221 	.globl _T1MODE
                                    222 	.globl _T1CNT
                                    223 	.globl _T1CNT1
                                    224 	.globl _T1CNT0
                                    225 	.globl _T1CLKSRC
                                    226 	.globl _T0STATUS
                                    227 	.globl _T0PERIOD
                                    228 	.globl _T0PERIOD1
                                    229 	.globl _T0PERIOD0
                                    230 	.globl _T0MODE
                                    231 	.globl _T0CNT
                                    232 	.globl _T0CNT1
                                    233 	.globl _T0CNT0
                                    234 	.globl _T0CLKSRC
                                    235 	.globl _SPSTATUS
                                    236 	.globl _SPSHREG
                                    237 	.globl _SPMODE
                                    238 	.globl _SPCLKSRC
                                    239 	.globl _RADIOSTAT
                                    240 	.globl _RADIOSTAT1
                                    241 	.globl _RADIOSTAT0
                                    242 	.globl _RADIODATA
                                    243 	.globl _RADIODATA3
                                    244 	.globl _RADIODATA2
                                    245 	.globl _RADIODATA1
                                    246 	.globl _RADIODATA0
                                    247 	.globl _RADIOADDR
                                    248 	.globl _RADIOADDR1
                                    249 	.globl _RADIOADDR0
                                    250 	.globl _RADIOACC
                                    251 	.globl _OC1STATUS
                                    252 	.globl _OC1PIN
                                    253 	.globl _OC1MODE
                                    254 	.globl _OC1COMP
                                    255 	.globl _OC1COMP1
                                    256 	.globl _OC1COMP0
                                    257 	.globl _OC0STATUS
                                    258 	.globl _OC0PIN
                                    259 	.globl _OC0MODE
                                    260 	.globl _OC0COMP
                                    261 	.globl _OC0COMP1
                                    262 	.globl _OC0COMP0
                                    263 	.globl _NVSTATUS
                                    264 	.globl _NVKEY
                                    265 	.globl _NVDATA
                                    266 	.globl _NVDATA1
                                    267 	.globl _NVDATA0
                                    268 	.globl _NVADDR
                                    269 	.globl _NVADDR1
                                    270 	.globl _NVADDR0
                                    271 	.globl _IC1STATUS
                                    272 	.globl _IC1MODE
                                    273 	.globl _IC1CAPT
                                    274 	.globl _IC1CAPT1
                                    275 	.globl _IC1CAPT0
                                    276 	.globl _IC0STATUS
                                    277 	.globl _IC0MODE
                                    278 	.globl _IC0CAPT
                                    279 	.globl _IC0CAPT1
                                    280 	.globl _IC0CAPT0
                                    281 	.globl _PORTR
                                    282 	.globl _PORTC
                                    283 	.globl _PORTB
                                    284 	.globl _PORTA
                                    285 	.globl _PINR
                                    286 	.globl _PINC
                                    287 	.globl _PINB
                                    288 	.globl _PINA
                                    289 	.globl _DIRR
                                    290 	.globl _DIRC
                                    291 	.globl _DIRB
                                    292 	.globl _DIRA
                                    293 	.globl _DBGLNKSTAT
                                    294 	.globl _DBGLNKBUF
                                    295 	.globl _CODECONFIG
                                    296 	.globl _CLKSTAT
                                    297 	.globl _CLKCON
                                    298 	.globl _ANALOGCOMP
                                    299 	.globl _ADCCONV
                                    300 	.globl _ADCCLKSRC
                                    301 	.globl _ADCCH3CONFIG
                                    302 	.globl _ADCCH2CONFIG
                                    303 	.globl _ADCCH1CONFIG
                                    304 	.globl _ADCCH0CONFIG
                                    305 	.globl __XPAGE
                                    306 	.globl _XPAGE
                                    307 	.globl _SP
                                    308 	.globl _PSW
                                    309 	.globl _PCON
                                    310 	.globl _IP
                                    311 	.globl _IE
                                    312 	.globl _EIP
                                    313 	.globl _EIE
                                    314 	.globl _E2IP
                                    315 	.globl _E2IE
                                    316 	.globl _DPS
                                    317 	.globl _DPTR1
                                    318 	.globl _DPTR0
                                    319 	.globl _DPL1
                                    320 	.globl _DPL
                                    321 	.globl _DPH1
                                    322 	.globl _DPH
                                    323 	.globl _B
                                    324 	.globl _ACC
                                    325 	.globl _f33_saved
                                    326 	.globl _f32_saved
                                    327 	.globl _f31_saved
                                    328 	.globl _f30_saved
                                    329 	.globl _axradio_transmit_PARM_3
                                    330 	.globl _axradio_transmit_PARM_2
                                    331 	.globl _axradio_timer
                                    332 	.globl _axradio_cb_transmitdata
                                    333 	.globl _axradio_cb_transmitend
                                    334 	.globl _axradio_cb_transmitstart
                                    335 	.globl _axradio_cb_channelstate
                                    336 	.globl _axradio_cb_receivesfd
                                    337 	.globl _axradio_cb_receive
                                    338 	.globl _axradio_rxbuffer
                                    339 	.globl _axradio_txbuffer
                                    340 	.globl _axradio_default_remoteaddr
                                    341 	.globl _axradio_localaddr
                                    342 	.globl _axradio_timeanchor
                                    343 	.globl _axradio_sync_periodcorr
                                    344 	.globl _axradio_sync_time
                                    345 	.globl _axradio_ack_seqnr
                                    346 	.globl _axradio_ack_count
                                    347 	.globl _axradio_curfreqoffset
                                    348 	.globl _axradio_curchannel
                                    349 	.globl _axradio_txbuffer_cnt
                                    350 	.globl _axradio_txbuffer_len
                                    351 	.globl _axradio_syncstate
                                    352 	.globl _AX5043_XTALAMPL
                                    353 	.globl _AX5043_XTALOSC
                                    354 	.globl _AX5043_MODCFGP
                                    355 	.globl _AX5043_POWCTRL1
                                    356 	.globl _AX5043_REF
                                    357 	.globl _AX5043_0xF44
                                    358 	.globl _AX5043_0xF35
                                    359 	.globl _AX5043_0xF34
                                    360 	.globl _AX5043_0xF33
                                    361 	.globl _AX5043_0xF32
                                    362 	.globl _AX5043_0xF31
                                    363 	.globl _AX5043_0xF30
                                    364 	.globl _AX5043_0xF26
                                    365 	.globl _AX5043_0xF23
                                    366 	.globl _AX5043_0xF22
                                    367 	.globl _AX5043_0xF21
                                    368 	.globl _AX5043_0xF1C
                                    369 	.globl _AX5043_0xF18
                                    370 	.globl _AX5043_0xF11
                                    371 	.globl _AX5043_0xF10
                                    372 	.globl _AX5043_0xF0C
                                    373 	.globl _AX5043_0xF00
                                    374 	.globl _AX5043_TIMEGAIN3NB
                                    375 	.globl _AX5043_TIMEGAIN2NB
                                    376 	.globl _AX5043_TIMEGAIN1NB
                                    377 	.globl _AX5043_TIMEGAIN0NB
                                    378 	.globl _AX5043_RXPARAMSETSNB
                                    379 	.globl _AX5043_RXPARAMCURSETNB
                                    380 	.globl _AX5043_PKTMAXLENNB
                                    381 	.globl _AX5043_PKTLENOFFSETNB
                                    382 	.globl _AX5043_PKTLENCFGNB
                                    383 	.globl _AX5043_PKTADDRMASK3NB
                                    384 	.globl _AX5043_PKTADDRMASK2NB
                                    385 	.globl _AX5043_PKTADDRMASK1NB
                                    386 	.globl _AX5043_PKTADDRMASK0NB
                                    387 	.globl _AX5043_PKTADDRCFGNB
                                    388 	.globl _AX5043_PKTADDR3NB
                                    389 	.globl _AX5043_PKTADDR2NB
                                    390 	.globl _AX5043_PKTADDR1NB
                                    391 	.globl _AX5043_PKTADDR0NB
                                    392 	.globl _AX5043_PHASEGAIN3NB
                                    393 	.globl _AX5043_PHASEGAIN2NB
                                    394 	.globl _AX5043_PHASEGAIN1NB
                                    395 	.globl _AX5043_PHASEGAIN0NB
                                    396 	.globl _AX5043_FREQUENCYLEAKNB
                                    397 	.globl _AX5043_FREQUENCYGAIND3NB
                                    398 	.globl _AX5043_FREQUENCYGAIND2NB
                                    399 	.globl _AX5043_FREQUENCYGAIND1NB
                                    400 	.globl _AX5043_FREQUENCYGAIND0NB
                                    401 	.globl _AX5043_FREQUENCYGAINC3NB
                                    402 	.globl _AX5043_FREQUENCYGAINC2NB
                                    403 	.globl _AX5043_FREQUENCYGAINC1NB
                                    404 	.globl _AX5043_FREQUENCYGAINC0NB
                                    405 	.globl _AX5043_FREQUENCYGAINB3NB
                                    406 	.globl _AX5043_FREQUENCYGAINB2NB
                                    407 	.globl _AX5043_FREQUENCYGAINB1NB
                                    408 	.globl _AX5043_FREQUENCYGAINB0NB
                                    409 	.globl _AX5043_FREQUENCYGAINA3NB
                                    410 	.globl _AX5043_FREQUENCYGAINA2NB
                                    411 	.globl _AX5043_FREQUENCYGAINA1NB
                                    412 	.globl _AX5043_FREQUENCYGAINA0NB
                                    413 	.globl _AX5043_FREQDEV13NB
                                    414 	.globl _AX5043_FREQDEV12NB
                                    415 	.globl _AX5043_FREQDEV11NB
                                    416 	.globl _AX5043_FREQDEV10NB
                                    417 	.globl _AX5043_FREQDEV03NB
                                    418 	.globl _AX5043_FREQDEV02NB
                                    419 	.globl _AX5043_FREQDEV01NB
                                    420 	.globl _AX5043_FREQDEV00NB
                                    421 	.globl _AX5043_FOURFSK3NB
                                    422 	.globl _AX5043_FOURFSK2NB
                                    423 	.globl _AX5043_FOURFSK1NB
                                    424 	.globl _AX5043_FOURFSK0NB
                                    425 	.globl _AX5043_DRGAIN3NB
                                    426 	.globl _AX5043_DRGAIN2NB
                                    427 	.globl _AX5043_DRGAIN1NB
                                    428 	.globl _AX5043_DRGAIN0NB
                                    429 	.globl _AX5043_BBOFFSRES3NB
                                    430 	.globl _AX5043_BBOFFSRES2NB
                                    431 	.globl _AX5043_BBOFFSRES1NB
                                    432 	.globl _AX5043_BBOFFSRES0NB
                                    433 	.globl _AX5043_AMPLITUDEGAIN3NB
                                    434 	.globl _AX5043_AMPLITUDEGAIN2NB
                                    435 	.globl _AX5043_AMPLITUDEGAIN1NB
                                    436 	.globl _AX5043_AMPLITUDEGAIN0NB
                                    437 	.globl _AX5043_AGCTARGET3NB
                                    438 	.globl _AX5043_AGCTARGET2NB
                                    439 	.globl _AX5043_AGCTARGET1NB
                                    440 	.globl _AX5043_AGCTARGET0NB
                                    441 	.globl _AX5043_AGCMINMAX3NB
                                    442 	.globl _AX5043_AGCMINMAX2NB
                                    443 	.globl _AX5043_AGCMINMAX1NB
                                    444 	.globl _AX5043_AGCMINMAX0NB
                                    445 	.globl _AX5043_AGCGAIN3NB
                                    446 	.globl _AX5043_AGCGAIN2NB
                                    447 	.globl _AX5043_AGCGAIN1NB
                                    448 	.globl _AX5043_AGCGAIN0NB
                                    449 	.globl _AX5043_AGCAHYST3NB
                                    450 	.globl _AX5043_AGCAHYST2NB
                                    451 	.globl _AX5043_AGCAHYST1NB
                                    452 	.globl _AX5043_AGCAHYST0NB
                                    453 	.globl _AX5043_0xF44NB
                                    454 	.globl _AX5043_0xF35NB
                                    455 	.globl _AX5043_0xF34NB
                                    456 	.globl _AX5043_0xF33NB
                                    457 	.globl _AX5043_0xF32NB
                                    458 	.globl _AX5043_0xF31NB
                                    459 	.globl _AX5043_0xF30NB
                                    460 	.globl _AX5043_0xF26NB
                                    461 	.globl _AX5043_0xF23NB
                                    462 	.globl _AX5043_0xF22NB
                                    463 	.globl _AX5043_0xF21NB
                                    464 	.globl _AX5043_0xF1CNB
                                    465 	.globl _AX5043_0xF18NB
                                    466 	.globl _AX5043_0xF0CNB
                                    467 	.globl _AX5043_0xF00NB
                                    468 	.globl _AX5043_XTALSTATUSNB
                                    469 	.globl _AX5043_XTALOSCNB
                                    470 	.globl _AX5043_XTALCAPNB
                                    471 	.globl _AX5043_XTALAMPLNB
                                    472 	.globl _AX5043_WAKEUPXOEARLYNB
                                    473 	.globl _AX5043_WAKEUPTIMER1NB
                                    474 	.globl _AX5043_WAKEUPTIMER0NB
                                    475 	.globl _AX5043_WAKEUPFREQ1NB
                                    476 	.globl _AX5043_WAKEUPFREQ0NB
                                    477 	.globl _AX5043_WAKEUP1NB
                                    478 	.globl _AX5043_WAKEUP0NB
                                    479 	.globl _AX5043_TXRATE2NB
                                    480 	.globl _AX5043_TXRATE1NB
                                    481 	.globl _AX5043_TXRATE0NB
                                    482 	.globl _AX5043_TXPWRCOEFFE1NB
                                    483 	.globl _AX5043_TXPWRCOEFFE0NB
                                    484 	.globl _AX5043_TXPWRCOEFFD1NB
                                    485 	.globl _AX5043_TXPWRCOEFFD0NB
                                    486 	.globl _AX5043_TXPWRCOEFFC1NB
                                    487 	.globl _AX5043_TXPWRCOEFFC0NB
                                    488 	.globl _AX5043_TXPWRCOEFFB1NB
                                    489 	.globl _AX5043_TXPWRCOEFFB0NB
                                    490 	.globl _AX5043_TXPWRCOEFFA1NB
                                    491 	.globl _AX5043_TXPWRCOEFFA0NB
                                    492 	.globl _AX5043_TRKRFFREQ2NB
                                    493 	.globl _AX5043_TRKRFFREQ1NB
                                    494 	.globl _AX5043_TRKRFFREQ0NB
                                    495 	.globl _AX5043_TRKPHASE1NB
                                    496 	.globl _AX5043_TRKPHASE0NB
                                    497 	.globl _AX5043_TRKFSKDEMOD1NB
                                    498 	.globl _AX5043_TRKFSKDEMOD0NB
                                    499 	.globl _AX5043_TRKFREQ1NB
                                    500 	.globl _AX5043_TRKFREQ0NB
                                    501 	.globl _AX5043_TRKDATARATE2NB
                                    502 	.globl _AX5043_TRKDATARATE1NB
                                    503 	.globl _AX5043_TRKDATARATE0NB
                                    504 	.globl _AX5043_TRKAMPLITUDE1NB
                                    505 	.globl _AX5043_TRKAMPLITUDE0NB
                                    506 	.globl _AX5043_TRKAFSKDEMOD1NB
                                    507 	.globl _AX5043_TRKAFSKDEMOD0NB
                                    508 	.globl _AX5043_TMGTXSETTLENB
                                    509 	.globl _AX5043_TMGTXBOOSTNB
                                    510 	.globl _AX5043_TMGRXSETTLENB
                                    511 	.globl _AX5043_TMGRXRSSINB
                                    512 	.globl _AX5043_TMGRXPREAMBLE3NB
                                    513 	.globl _AX5043_TMGRXPREAMBLE2NB
                                    514 	.globl _AX5043_TMGRXPREAMBLE1NB
                                    515 	.globl _AX5043_TMGRXOFFSACQNB
                                    516 	.globl _AX5043_TMGRXCOARSEAGCNB
                                    517 	.globl _AX5043_TMGRXBOOSTNB
                                    518 	.globl _AX5043_TMGRXAGCNB
                                    519 	.globl _AX5043_TIMER2NB
                                    520 	.globl _AX5043_TIMER1NB
                                    521 	.globl _AX5043_TIMER0NB
                                    522 	.globl _AX5043_SILICONREVISIONNB
                                    523 	.globl _AX5043_SCRATCHNB
                                    524 	.globl _AX5043_RXDATARATE2NB
                                    525 	.globl _AX5043_RXDATARATE1NB
                                    526 	.globl _AX5043_RXDATARATE0NB
                                    527 	.globl _AX5043_RSSIREFERENCENB
                                    528 	.globl _AX5043_RSSIABSTHRNB
                                    529 	.globl _AX5043_RSSINB
                                    530 	.globl _AX5043_REFNB
                                    531 	.globl _AX5043_RADIOSTATENB
                                    532 	.globl _AX5043_RADIOEVENTREQ1NB
                                    533 	.globl _AX5043_RADIOEVENTREQ0NB
                                    534 	.globl _AX5043_RADIOEVENTMASK1NB
                                    535 	.globl _AX5043_RADIOEVENTMASK0NB
                                    536 	.globl _AX5043_PWRMODENB
                                    537 	.globl _AX5043_PWRAMPNB
                                    538 	.globl _AX5043_POWSTICKYSTATNB
                                    539 	.globl _AX5043_POWSTATNB
                                    540 	.globl _AX5043_POWIRQMASKNB
                                    541 	.globl _AX5043_POWCTRL1NB
                                    542 	.globl _AX5043_PLLVCOIRNB
                                    543 	.globl _AX5043_PLLVCOINB
                                    544 	.globl _AX5043_PLLVCODIVNB
                                    545 	.globl _AX5043_PLLRNGCLKNB
                                    546 	.globl _AX5043_PLLRANGINGBNB
                                    547 	.globl _AX5043_PLLRANGINGANB
                                    548 	.globl _AX5043_PLLLOOPBOOSTNB
                                    549 	.globl _AX5043_PLLLOOPNB
                                    550 	.globl _AX5043_PLLLOCKDETNB
                                    551 	.globl _AX5043_PLLCPIBOOSTNB
                                    552 	.globl _AX5043_PLLCPINB
                                    553 	.globl _AX5043_PKTSTOREFLAGSNB
                                    554 	.globl _AX5043_PKTMISCFLAGSNB
                                    555 	.globl _AX5043_PKTCHUNKSIZENB
                                    556 	.globl _AX5043_PKTACCEPTFLAGSNB
                                    557 	.globl _AX5043_PINSTATENB
                                    558 	.globl _AX5043_PINFUNCSYSCLKNB
                                    559 	.globl _AX5043_PINFUNCPWRAMPNB
                                    560 	.globl _AX5043_PINFUNCIRQNB
                                    561 	.globl _AX5043_PINFUNCDCLKNB
                                    562 	.globl _AX5043_PINFUNCDATANB
                                    563 	.globl _AX5043_PINFUNCANTSELNB
                                    564 	.globl _AX5043_MODULATIONNB
                                    565 	.globl _AX5043_MODCFGPNB
                                    566 	.globl _AX5043_MODCFGFNB
                                    567 	.globl _AX5043_MODCFGANB
                                    568 	.globl _AX5043_MAXRFOFFSET2NB
                                    569 	.globl _AX5043_MAXRFOFFSET1NB
                                    570 	.globl _AX5043_MAXRFOFFSET0NB
                                    571 	.globl _AX5043_MAXDROFFSET2NB
                                    572 	.globl _AX5043_MAXDROFFSET1NB
                                    573 	.globl _AX5043_MAXDROFFSET0NB
                                    574 	.globl _AX5043_MATCH1PAT1NB
                                    575 	.globl _AX5043_MATCH1PAT0NB
                                    576 	.globl _AX5043_MATCH1MINNB
                                    577 	.globl _AX5043_MATCH1MAXNB
                                    578 	.globl _AX5043_MATCH1LENNB
                                    579 	.globl _AX5043_MATCH0PAT3NB
                                    580 	.globl _AX5043_MATCH0PAT2NB
                                    581 	.globl _AX5043_MATCH0PAT1NB
                                    582 	.globl _AX5043_MATCH0PAT0NB
                                    583 	.globl _AX5043_MATCH0MINNB
                                    584 	.globl _AX5043_MATCH0MAXNB
                                    585 	.globl _AX5043_MATCH0LENNB
                                    586 	.globl _AX5043_LPOSCSTATUSNB
                                    587 	.globl _AX5043_LPOSCREF1NB
                                    588 	.globl _AX5043_LPOSCREF0NB
                                    589 	.globl _AX5043_LPOSCPER1NB
                                    590 	.globl _AX5043_LPOSCPER0NB
                                    591 	.globl _AX5043_LPOSCKFILT1NB
                                    592 	.globl _AX5043_LPOSCKFILT0NB
                                    593 	.globl _AX5043_LPOSCFREQ1NB
                                    594 	.globl _AX5043_LPOSCFREQ0NB
                                    595 	.globl _AX5043_LPOSCCONFIGNB
                                    596 	.globl _AX5043_IRQREQUEST1NB
                                    597 	.globl _AX5043_IRQREQUEST0NB
                                    598 	.globl _AX5043_IRQMASK1NB
                                    599 	.globl _AX5043_IRQMASK0NB
                                    600 	.globl _AX5043_IRQINVERSION1NB
                                    601 	.globl _AX5043_IRQINVERSION0NB
                                    602 	.globl _AX5043_IFFREQ1NB
                                    603 	.globl _AX5043_IFFREQ0NB
                                    604 	.globl _AX5043_GPADCPERIODNB
                                    605 	.globl _AX5043_GPADCCTRLNB
                                    606 	.globl _AX5043_GPADC13VALUE1NB
                                    607 	.globl _AX5043_GPADC13VALUE0NB
                                    608 	.globl _AX5043_FSKDMIN1NB
                                    609 	.globl _AX5043_FSKDMIN0NB
                                    610 	.globl _AX5043_FSKDMAX1NB
                                    611 	.globl _AX5043_FSKDMAX0NB
                                    612 	.globl _AX5043_FSKDEV2NB
                                    613 	.globl _AX5043_FSKDEV1NB
                                    614 	.globl _AX5043_FSKDEV0NB
                                    615 	.globl _AX5043_FREQB3NB
                                    616 	.globl _AX5043_FREQB2NB
                                    617 	.globl _AX5043_FREQB1NB
                                    618 	.globl _AX5043_FREQB0NB
                                    619 	.globl _AX5043_FREQA3NB
                                    620 	.globl _AX5043_FREQA2NB
                                    621 	.globl _AX5043_FREQA1NB
                                    622 	.globl _AX5043_FREQA0NB
                                    623 	.globl _AX5043_FRAMINGNB
                                    624 	.globl _AX5043_FIFOTHRESH1NB
                                    625 	.globl _AX5043_FIFOTHRESH0NB
                                    626 	.globl _AX5043_FIFOSTATNB
                                    627 	.globl _AX5043_FIFOFREE1NB
                                    628 	.globl _AX5043_FIFOFREE0NB
                                    629 	.globl _AX5043_FIFODATANB
                                    630 	.globl _AX5043_FIFOCOUNT1NB
                                    631 	.globl _AX5043_FIFOCOUNT0NB
                                    632 	.globl _AX5043_FECSYNCNB
                                    633 	.globl _AX5043_FECSTATUSNB
                                    634 	.globl _AX5043_FECNB
                                    635 	.globl _AX5043_ENCODINGNB
                                    636 	.globl _AX5043_DIVERSITYNB
                                    637 	.globl _AX5043_DECIMATIONNB
                                    638 	.globl _AX5043_DACVALUE1NB
                                    639 	.globl _AX5043_DACVALUE0NB
                                    640 	.globl _AX5043_DACCONFIGNB
                                    641 	.globl _AX5043_CRCINIT3NB
                                    642 	.globl _AX5043_CRCINIT2NB
                                    643 	.globl _AX5043_CRCINIT1NB
                                    644 	.globl _AX5043_CRCINIT0NB
                                    645 	.globl _AX5043_BGNDRSSITHRNB
                                    646 	.globl _AX5043_BGNDRSSIGAINNB
                                    647 	.globl _AX5043_BGNDRSSINB
                                    648 	.globl _AX5043_BBTUNENB
                                    649 	.globl _AX5043_BBOFFSCAPNB
                                    650 	.globl _AX5043_AMPLFILTERNB
                                    651 	.globl _AX5043_AGCCOUNTERNB
                                    652 	.globl _AX5043_AFSKSPACE1NB
                                    653 	.globl _AX5043_AFSKSPACE0NB
                                    654 	.globl _AX5043_AFSKMARK1NB
                                    655 	.globl _AX5043_AFSKMARK0NB
                                    656 	.globl _AX5043_AFSKCTRLNB
                                    657 	.globl _AX5043_TIMEGAIN3
                                    658 	.globl _AX5043_TIMEGAIN2
                                    659 	.globl _AX5043_TIMEGAIN1
                                    660 	.globl _AX5043_TIMEGAIN0
                                    661 	.globl _AX5043_RXPARAMSETS
                                    662 	.globl _AX5043_RXPARAMCURSET
                                    663 	.globl _AX5043_PKTMAXLEN
                                    664 	.globl _AX5043_PKTLENOFFSET
                                    665 	.globl _AX5043_PKTLENCFG
                                    666 	.globl _AX5043_PKTADDRMASK3
                                    667 	.globl _AX5043_PKTADDRMASK2
                                    668 	.globl _AX5043_PKTADDRMASK1
                                    669 	.globl _AX5043_PKTADDRMASK0
                                    670 	.globl _AX5043_PKTADDRCFG
                                    671 	.globl _AX5043_PKTADDR3
                                    672 	.globl _AX5043_PKTADDR2
                                    673 	.globl _AX5043_PKTADDR1
                                    674 	.globl _AX5043_PKTADDR0
                                    675 	.globl _AX5043_PHASEGAIN3
                                    676 	.globl _AX5043_PHASEGAIN2
                                    677 	.globl _AX5043_PHASEGAIN1
                                    678 	.globl _AX5043_PHASEGAIN0
                                    679 	.globl _AX5043_FREQUENCYLEAK
                                    680 	.globl _AX5043_FREQUENCYGAIND3
                                    681 	.globl _AX5043_FREQUENCYGAIND2
                                    682 	.globl _AX5043_FREQUENCYGAIND1
                                    683 	.globl _AX5043_FREQUENCYGAIND0
                                    684 	.globl _AX5043_FREQUENCYGAINC3
                                    685 	.globl _AX5043_FREQUENCYGAINC2
                                    686 	.globl _AX5043_FREQUENCYGAINC1
                                    687 	.globl _AX5043_FREQUENCYGAINC0
                                    688 	.globl _AX5043_FREQUENCYGAINB3
                                    689 	.globl _AX5043_FREQUENCYGAINB2
                                    690 	.globl _AX5043_FREQUENCYGAINB1
                                    691 	.globl _AX5043_FREQUENCYGAINB0
                                    692 	.globl _AX5043_FREQUENCYGAINA3
                                    693 	.globl _AX5043_FREQUENCYGAINA2
                                    694 	.globl _AX5043_FREQUENCYGAINA1
                                    695 	.globl _AX5043_FREQUENCYGAINA0
                                    696 	.globl _AX5043_FREQDEV13
                                    697 	.globl _AX5043_FREQDEV12
                                    698 	.globl _AX5043_FREQDEV11
                                    699 	.globl _AX5043_FREQDEV10
                                    700 	.globl _AX5043_FREQDEV03
                                    701 	.globl _AX5043_FREQDEV02
                                    702 	.globl _AX5043_FREQDEV01
                                    703 	.globl _AX5043_FREQDEV00
                                    704 	.globl _AX5043_FOURFSK3
                                    705 	.globl _AX5043_FOURFSK2
                                    706 	.globl _AX5043_FOURFSK1
                                    707 	.globl _AX5043_FOURFSK0
                                    708 	.globl _AX5043_DRGAIN3
                                    709 	.globl _AX5043_DRGAIN2
                                    710 	.globl _AX5043_DRGAIN1
                                    711 	.globl _AX5043_DRGAIN0
                                    712 	.globl _AX5043_BBOFFSRES3
                                    713 	.globl _AX5043_BBOFFSRES2
                                    714 	.globl _AX5043_BBOFFSRES1
                                    715 	.globl _AX5043_BBOFFSRES0
                                    716 	.globl _AX5043_AMPLITUDEGAIN3
                                    717 	.globl _AX5043_AMPLITUDEGAIN2
                                    718 	.globl _AX5043_AMPLITUDEGAIN1
                                    719 	.globl _AX5043_AMPLITUDEGAIN0
                                    720 	.globl _AX5043_AGCTARGET3
                                    721 	.globl _AX5043_AGCTARGET2
                                    722 	.globl _AX5043_AGCTARGET1
                                    723 	.globl _AX5043_AGCTARGET0
                                    724 	.globl _AX5043_AGCMINMAX3
                                    725 	.globl _AX5043_AGCMINMAX2
                                    726 	.globl _AX5043_AGCMINMAX1
                                    727 	.globl _AX5043_AGCMINMAX0
                                    728 	.globl _AX5043_AGCGAIN3
                                    729 	.globl _AX5043_AGCGAIN2
                                    730 	.globl _AX5043_AGCGAIN1
                                    731 	.globl _AX5043_AGCGAIN0
                                    732 	.globl _AX5043_AGCAHYST3
                                    733 	.globl _AX5043_AGCAHYST2
                                    734 	.globl _AX5043_AGCAHYST1
                                    735 	.globl _AX5043_AGCAHYST0
                                    736 	.globl _AX5043_XTALSTATUS
                                    737 	.globl _AX5043_XTALCAP
                                    738 	.globl _AX5043_WAKEUPXOEARLY
                                    739 	.globl _AX5043_WAKEUPTIMER1
                                    740 	.globl _AX5043_WAKEUPTIMER0
                                    741 	.globl _AX5043_WAKEUPFREQ1
                                    742 	.globl _AX5043_WAKEUPFREQ0
                                    743 	.globl _AX5043_WAKEUP1
                                    744 	.globl _AX5043_WAKEUP0
                                    745 	.globl _AX5043_TXRATE2
                                    746 	.globl _AX5043_TXRATE1
                                    747 	.globl _AX5043_TXRATE0
                                    748 	.globl _AX5043_TXPWRCOEFFE1
                                    749 	.globl _AX5043_TXPWRCOEFFE0
                                    750 	.globl _AX5043_TXPWRCOEFFD1
                                    751 	.globl _AX5043_TXPWRCOEFFD0
                                    752 	.globl _AX5043_TXPWRCOEFFC1
                                    753 	.globl _AX5043_TXPWRCOEFFC0
                                    754 	.globl _AX5043_TXPWRCOEFFB1
                                    755 	.globl _AX5043_TXPWRCOEFFB0
                                    756 	.globl _AX5043_TXPWRCOEFFA1
                                    757 	.globl _AX5043_TXPWRCOEFFA0
                                    758 	.globl _AX5043_TRKRFFREQ2
                                    759 	.globl _AX5043_TRKRFFREQ1
                                    760 	.globl _AX5043_TRKRFFREQ0
                                    761 	.globl _AX5043_TRKPHASE1
                                    762 	.globl _AX5043_TRKPHASE0
                                    763 	.globl _AX5043_TRKFSKDEMOD1
                                    764 	.globl _AX5043_TRKFSKDEMOD0
                                    765 	.globl _AX5043_TRKFREQ1
                                    766 	.globl _AX5043_TRKFREQ0
                                    767 	.globl _AX5043_TRKDATARATE2
                                    768 	.globl _AX5043_TRKDATARATE1
                                    769 	.globl _AX5043_TRKDATARATE0
                                    770 	.globl _AX5043_TRKAMPLITUDE1
                                    771 	.globl _AX5043_TRKAMPLITUDE0
                                    772 	.globl _AX5043_TRKAFSKDEMOD1
                                    773 	.globl _AX5043_TRKAFSKDEMOD0
                                    774 	.globl _AX5043_TMGTXSETTLE
                                    775 	.globl _AX5043_TMGTXBOOST
                                    776 	.globl _AX5043_TMGRXSETTLE
                                    777 	.globl _AX5043_TMGRXRSSI
                                    778 	.globl _AX5043_TMGRXPREAMBLE3
                                    779 	.globl _AX5043_TMGRXPREAMBLE2
                                    780 	.globl _AX5043_TMGRXPREAMBLE1
                                    781 	.globl _AX5043_TMGRXOFFSACQ
                                    782 	.globl _AX5043_TMGRXCOARSEAGC
                                    783 	.globl _AX5043_TMGRXBOOST
                                    784 	.globl _AX5043_TMGRXAGC
                                    785 	.globl _AX5043_TIMER2
                                    786 	.globl _AX5043_TIMER1
                                    787 	.globl _AX5043_TIMER0
                                    788 	.globl _AX5043_SILICONREVISION
                                    789 	.globl _AX5043_SCRATCH
                                    790 	.globl _AX5043_RXDATARATE2
                                    791 	.globl _AX5043_RXDATARATE1
                                    792 	.globl _AX5043_RXDATARATE0
                                    793 	.globl _AX5043_RSSIREFERENCE
                                    794 	.globl _AX5043_RSSIABSTHR
                                    795 	.globl _AX5043_RSSI
                                    796 	.globl _AX5043_RADIOSTATE
                                    797 	.globl _AX5043_RADIOEVENTREQ1
                                    798 	.globl _AX5043_RADIOEVENTREQ0
                                    799 	.globl _AX5043_RADIOEVENTMASK1
                                    800 	.globl _AX5043_RADIOEVENTMASK0
                                    801 	.globl _AX5043_PWRMODE
                                    802 	.globl _AX5043_PWRAMP
                                    803 	.globl _AX5043_POWSTICKYSTAT
                                    804 	.globl _AX5043_POWSTAT
                                    805 	.globl _AX5043_POWIRQMASK
                                    806 	.globl _AX5043_PLLVCOIR
                                    807 	.globl _AX5043_PLLVCOI
                                    808 	.globl _AX5043_PLLVCODIV
                                    809 	.globl _AX5043_PLLRNGCLK
                                    810 	.globl _AX5043_PLLRANGINGB
                                    811 	.globl _AX5043_PLLRANGINGA
                                    812 	.globl _AX5043_PLLLOOPBOOST
                                    813 	.globl _AX5043_PLLLOOP
                                    814 	.globl _AX5043_PLLLOCKDET
                                    815 	.globl _AX5043_PLLCPIBOOST
                                    816 	.globl _AX5043_PLLCPI
                                    817 	.globl _AX5043_PKTSTOREFLAGS
                                    818 	.globl _AX5043_PKTMISCFLAGS
                                    819 	.globl _AX5043_PKTCHUNKSIZE
                                    820 	.globl _AX5043_PKTACCEPTFLAGS
                                    821 	.globl _AX5043_PINSTATE
                                    822 	.globl _AX5043_PINFUNCSYSCLK
                                    823 	.globl _AX5043_PINFUNCPWRAMP
                                    824 	.globl _AX5043_PINFUNCIRQ
                                    825 	.globl _AX5043_PINFUNCDCLK
                                    826 	.globl _AX5043_PINFUNCDATA
                                    827 	.globl _AX5043_PINFUNCANTSEL
                                    828 	.globl _AX5043_MODULATION
                                    829 	.globl _AX5043_MODCFGF
                                    830 	.globl _AX5043_MODCFGA
                                    831 	.globl _AX5043_MAXRFOFFSET2
                                    832 	.globl _AX5043_MAXRFOFFSET1
                                    833 	.globl _AX5043_MAXRFOFFSET0
                                    834 	.globl _AX5043_MAXDROFFSET2
                                    835 	.globl _AX5043_MAXDROFFSET1
                                    836 	.globl _AX5043_MAXDROFFSET0
                                    837 	.globl _AX5043_MATCH1PAT1
                                    838 	.globl _AX5043_MATCH1PAT0
                                    839 	.globl _AX5043_MATCH1MIN
                                    840 	.globl _AX5043_MATCH1MAX
                                    841 	.globl _AX5043_MATCH1LEN
                                    842 	.globl _AX5043_MATCH0PAT3
                                    843 	.globl _AX5043_MATCH0PAT2
                                    844 	.globl _AX5043_MATCH0PAT1
                                    845 	.globl _AX5043_MATCH0PAT0
                                    846 	.globl _AX5043_MATCH0MIN
                                    847 	.globl _AX5043_MATCH0MAX
                                    848 	.globl _AX5043_MATCH0LEN
                                    849 	.globl _AX5043_LPOSCSTATUS
                                    850 	.globl _AX5043_LPOSCREF1
                                    851 	.globl _AX5043_LPOSCREF0
                                    852 	.globl _AX5043_LPOSCPER1
                                    853 	.globl _AX5043_LPOSCPER0
                                    854 	.globl _AX5043_LPOSCKFILT1
                                    855 	.globl _AX5043_LPOSCKFILT0
                                    856 	.globl _AX5043_LPOSCFREQ1
                                    857 	.globl _AX5043_LPOSCFREQ0
                                    858 	.globl _AX5043_LPOSCCONFIG
                                    859 	.globl _AX5043_IRQREQUEST1
                                    860 	.globl _AX5043_IRQREQUEST0
                                    861 	.globl _AX5043_IRQMASK1
                                    862 	.globl _AX5043_IRQMASK0
                                    863 	.globl _AX5043_IRQINVERSION1
                                    864 	.globl _AX5043_IRQINVERSION0
                                    865 	.globl _AX5043_IFFREQ1
                                    866 	.globl _AX5043_IFFREQ0
                                    867 	.globl _AX5043_GPADCPERIOD
                                    868 	.globl _AX5043_GPADCCTRL
                                    869 	.globl _AX5043_GPADC13VALUE1
                                    870 	.globl _AX5043_GPADC13VALUE0
                                    871 	.globl _AX5043_FSKDMIN1
                                    872 	.globl _AX5043_FSKDMIN0
                                    873 	.globl _AX5043_FSKDMAX1
                                    874 	.globl _AX5043_FSKDMAX0
                                    875 	.globl _AX5043_FSKDEV2
                                    876 	.globl _AX5043_FSKDEV1
                                    877 	.globl _AX5043_FSKDEV0
                                    878 	.globl _AX5043_FREQB3
                                    879 	.globl _AX5043_FREQB2
                                    880 	.globl _AX5043_FREQB1
                                    881 	.globl _AX5043_FREQB0
                                    882 	.globl _AX5043_FREQA3
                                    883 	.globl _AX5043_FREQA2
                                    884 	.globl _AX5043_FREQA1
                                    885 	.globl _AX5043_FREQA0
                                    886 	.globl _AX5043_FRAMING
                                    887 	.globl _AX5043_FIFOTHRESH1
                                    888 	.globl _AX5043_FIFOTHRESH0
                                    889 	.globl _AX5043_FIFOSTAT
                                    890 	.globl _AX5043_FIFOFREE1
                                    891 	.globl _AX5043_FIFOFREE0
                                    892 	.globl _AX5043_FIFODATA
                                    893 	.globl _AX5043_FIFOCOUNT1
                                    894 	.globl _AX5043_FIFOCOUNT0
                                    895 	.globl _AX5043_FECSYNC
                                    896 	.globl _AX5043_FECSTATUS
                                    897 	.globl _AX5043_FEC
                                    898 	.globl _AX5043_ENCODING
                                    899 	.globl _AX5043_DIVERSITY
                                    900 	.globl _AX5043_DECIMATION
                                    901 	.globl _AX5043_DACVALUE1
                                    902 	.globl _AX5043_DACVALUE0
                                    903 	.globl _AX5043_DACCONFIG
                                    904 	.globl _AX5043_CRCINIT3
                                    905 	.globl _AX5043_CRCINIT2
                                    906 	.globl _AX5043_CRCINIT1
                                    907 	.globl _AX5043_CRCINIT0
                                    908 	.globl _AX5043_BGNDRSSITHR
                                    909 	.globl _AX5043_BGNDRSSIGAIN
                                    910 	.globl _AX5043_BGNDRSSI
                                    911 	.globl _AX5043_BBTUNE
                                    912 	.globl _AX5043_BBOFFSCAP
                                    913 	.globl _AX5043_AMPLFILTER
                                    914 	.globl _AX5043_AGCCOUNTER
                                    915 	.globl _AX5043_AFSKSPACE1
                                    916 	.globl _AX5043_AFSKSPACE0
                                    917 	.globl _AX5043_AFSKMARK1
                                    918 	.globl _AX5043_AFSKMARK0
                                    919 	.globl _AX5043_AFSKCTRL
                                    920 	.globl _XWTSTAT
                                    921 	.globl _XWTIRQEN
                                    922 	.globl _XWTEVTD
                                    923 	.globl _XWTEVTD1
                                    924 	.globl _XWTEVTD0
                                    925 	.globl _XWTEVTC
                                    926 	.globl _XWTEVTC1
                                    927 	.globl _XWTEVTC0
                                    928 	.globl _XWTEVTB
                                    929 	.globl _XWTEVTB1
                                    930 	.globl _XWTEVTB0
                                    931 	.globl _XWTEVTA
                                    932 	.globl _XWTEVTA1
                                    933 	.globl _XWTEVTA0
                                    934 	.globl _XWTCNTR1
                                    935 	.globl _XWTCNTB
                                    936 	.globl _XWTCNTB1
                                    937 	.globl _XWTCNTB0
                                    938 	.globl _XWTCNTA
                                    939 	.globl _XWTCNTA1
                                    940 	.globl _XWTCNTA0
                                    941 	.globl _XWTCFGB
                                    942 	.globl _XWTCFGA
                                    943 	.globl _XWDTRESET
                                    944 	.globl _XWDTCFG
                                    945 	.globl _XU1STATUS
                                    946 	.globl _XU1SHREG
                                    947 	.globl _XU1MODE
                                    948 	.globl _XU1CTRL
                                    949 	.globl _XU0STATUS
                                    950 	.globl _XU0SHREG
                                    951 	.globl _XU0MODE
                                    952 	.globl _XU0CTRL
                                    953 	.globl _XT2STATUS
                                    954 	.globl _XT2PERIOD
                                    955 	.globl _XT2PERIOD1
                                    956 	.globl _XT2PERIOD0
                                    957 	.globl _XT2MODE
                                    958 	.globl _XT2CNT
                                    959 	.globl _XT2CNT1
                                    960 	.globl _XT2CNT0
                                    961 	.globl _XT2CLKSRC
                                    962 	.globl _XT1STATUS
                                    963 	.globl _XT1PERIOD
                                    964 	.globl _XT1PERIOD1
                                    965 	.globl _XT1PERIOD0
                                    966 	.globl _XT1MODE
                                    967 	.globl _XT1CNT
                                    968 	.globl _XT1CNT1
                                    969 	.globl _XT1CNT0
                                    970 	.globl _XT1CLKSRC
                                    971 	.globl _XT0STATUS
                                    972 	.globl _XT0PERIOD
                                    973 	.globl _XT0PERIOD1
                                    974 	.globl _XT0PERIOD0
                                    975 	.globl _XT0MODE
                                    976 	.globl _XT0CNT
                                    977 	.globl _XT0CNT1
                                    978 	.globl _XT0CNT0
                                    979 	.globl _XT0CLKSRC
                                    980 	.globl _XSPSTATUS
                                    981 	.globl _XSPSHREG
                                    982 	.globl _XSPMODE
                                    983 	.globl _XSPCLKSRC
                                    984 	.globl _XRADIOSTAT
                                    985 	.globl _XRADIOSTAT1
                                    986 	.globl _XRADIOSTAT0
                                    987 	.globl _XRADIODATA3
                                    988 	.globl _XRADIODATA2
                                    989 	.globl _XRADIODATA1
                                    990 	.globl _XRADIODATA0
                                    991 	.globl _XRADIOADDR1
                                    992 	.globl _XRADIOADDR0
                                    993 	.globl _XRADIOACC
                                    994 	.globl _XOC1STATUS
                                    995 	.globl _XOC1PIN
                                    996 	.globl _XOC1MODE
                                    997 	.globl _XOC1COMP
                                    998 	.globl _XOC1COMP1
                                    999 	.globl _XOC1COMP0
                                   1000 	.globl _XOC0STATUS
                                   1001 	.globl _XOC0PIN
                                   1002 	.globl _XOC0MODE
                                   1003 	.globl _XOC0COMP
                                   1004 	.globl _XOC0COMP1
                                   1005 	.globl _XOC0COMP0
                                   1006 	.globl _XNVSTATUS
                                   1007 	.globl _XNVKEY
                                   1008 	.globl _XNVDATA
                                   1009 	.globl _XNVDATA1
                                   1010 	.globl _XNVDATA0
                                   1011 	.globl _XNVADDR
                                   1012 	.globl _XNVADDR1
                                   1013 	.globl _XNVADDR0
                                   1014 	.globl _XIC1STATUS
                                   1015 	.globl _XIC1MODE
                                   1016 	.globl _XIC1CAPT
                                   1017 	.globl _XIC1CAPT1
                                   1018 	.globl _XIC1CAPT0
                                   1019 	.globl _XIC0STATUS
                                   1020 	.globl _XIC0MODE
                                   1021 	.globl _XIC0CAPT
                                   1022 	.globl _XIC0CAPT1
                                   1023 	.globl _XIC0CAPT0
                                   1024 	.globl _XPORTR
                                   1025 	.globl _XPORTC
                                   1026 	.globl _XPORTB
                                   1027 	.globl _XPORTA
                                   1028 	.globl _XPINR
                                   1029 	.globl _XPINC
                                   1030 	.globl _XPINB
                                   1031 	.globl _XPINA
                                   1032 	.globl _XDIRR
                                   1033 	.globl _XDIRC
                                   1034 	.globl _XDIRB
                                   1035 	.globl _XDIRA
                                   1036 	.globl _XDBGLNKSTAT
                                   1037 	.globl _XDBGLNKBUF
                                   1038 	.globl _XCODECONFIG
                                   1039 	.globl _XCLKSTAT
                                   1040 	.globl _XCLKCON
                                   1041 	.globl _XANALOGCOMP
                                   1042 	.globl _XADCCONV
                                   1043 	.globl _XADCCLKSRC
                                   1044 	.globl _XADCCH3CONFIG
                                   1045 	.globl _XADCCH2CONFIG
                                   1046 	.globl _XADCCH1CONFIG
                                   1047 	.globl _XADCCH0CONFIG
                                   1048 	.globl _XPCON
                                   1049 	.globl _XIP
                                   1050 	.globl _XIE
                                   1051 	.globl _XDPTR1
                                   1052 	.globl _XDPTR0
                                   1053 	.globl _RNGCLKSRC1
                                   1054 	.globl _RNGCLKSRC0
                                   1055 	.globl _RNGMODE
                                   1056 	.globl _AESOUTADDR
                                   1057 	.globl _AESOUTADDR1
                                   1058 	.globl _AESOUTADDR0
                                   1059 	.globl _AESMODE
                                   1060 	.globl _AESKEYADDR
                                   1061 	.globl _AESKEYADDR1
                                   1062 	.globl _AESKEYADDR0
                                   1063 	.globl _AESINADDR
                                   1064 	.globl _AESINADDR1
                                   1065 	.globl _AESINADDR0
                                   1066 	.globl _AESCURBLOCK
                                   1067 	.globl _AESCONFIG
                                   1068 	.globl _RNGBYTE
                                   1069 	.globl _XTALREADY
                                   1070 	.globl _XTALOSC
                                   1071 	.globl _XTALAMPL
                                   1072 	.globl _SILICONREV
                                   1073 	.globl _SCRATCH3
                                   1074 	.globl _SCRATCH2
                                   1075 	.globl _SCRATCH1
                                   1076 	.globl _SCRATCH0
                                   1077 	.globl _RADIOMUX
                                   1078 	.globl _RADIOFSTATADDR
                                   1079 	.globl _RADIOFSTATADDR1
                                   1080 	.globl _RADIOFSTATADDR0
                                   1081 	.globl _RADIOFDATAADDR
                                   1082 	.globl _RADIOFDATAADDR1
                                   1083 	.globl _RADIOFDATAADDR0
                                   1084 	.globl _OSCRUN
                                   1085 	.globl _OSCREADY
                                   1086 	.globl _OSCFORCERUN
                                   1087 	.globl _OSCCALIB
                                   1088 	.globl _MISCCTRL
                                   1089 	.globl _LPXOSCGM
                                   1090 	.globl _LPOSCREF
                                   1091 	.globl _LPOSCREF1
                                   1092 	.globl _LPOSCREF0
                                   1093 	.globl _LPOSCPER
                                   1094 	.globl _LPOSCPER1
                                   1095 	.globl _LPOSCPER0
                                   1096 	.globl _LPOSCKFILT
                                   1097 	.globl _LPOSCKFILT1
                                   1098 	.globl _LPOSCKFILT0
                                   1099 	.globl _LPOSCFREQ
                                   1100 	.globl _LPOSCFREQ1
                                   1101 	.globl _LPOSCFREQ0
                                   1102 	.globl _LPOSCCONFIG
                                   1103 	.globl _PINSEL
                                   1104 	.globl _PINCHGC
                                   1105 	.globl _PINCHGB
                                   1106 	.globl _PINCHGA
                                   1107 	.globl _PALTRADIO
                                   1108 	.globl _PALTC
                                   1109 	.globl _PALTB
                                   1110 	.globl _PALTA
                                   1111 	.globl _INTCHGC
                                   1112 	.globl _INTCHGB
                                   1113 	.globl _INTCHGA
                                   1114 	.globl _EXTIRQ
                                   1115 	.globl _GPIOENABLE
                                   1116 	.globl _ANALOGA
                                   1117 	.globl _FRCOSCREF
                                   1118 	.globl _FRCOSCREF1
                                   1119 	.globl _FRCOSCREF0
                                   1120 	.globl _FRCOSCPER
                                   1121 	.globl _FRCOSCPER1
                                   1122 	.globl _FRCOSCPER0
                                   1123 	.globl _FRCOSCKFILT
                                   1124 	.globl _FRCOSCKFILT1
                                   1125 	.globl _FRCOSCKFILT0
                                   1126 	.globl _FRCOSCFREQ
                                   1127 	.globl _FRCOSCFREQ1
                                   1128 	.globl _FRCOSCFREQ0
                                   1129 	.globl _FRCOSCCTRL
                                   1130 	.globl _FRCOSCCONFIG
                                   1131 	.globl _DMA1CONFIG
                                   1132 	.globl _DMA1ADDR
                                   1133 	.globl _DMA1ADDR1
                                   1134 	.globl _DMA1ADDR0
                                   1135 	.globl _DMA0CONFIG
                                   1136 	.globl _DMA0ADDR
                                   1137 	.globl _DMA0ADDR1
                                   1138 	.globl _DMA0ADDR0
                                   1139 	.globl _ADCTUNE2
                                   1140 	.globl _ADCTUNE1
                                   1141 	.globl _ADCTUNE0
                                   1142 	.globl _ADCCH3VAL
                                   1143 	.globl _ADCCH3VAL1
                                   1144 	.globl _ADCCH3VAL0
                                   1145 	.globl _ADCCH2VAL
                                   1146 	.globl _ADCCH2VAL1
                                   1147 	.globl _ADCCH2VAL0
                                   1148 	.globl _ADCCH1VAL
                                   1149 	.globl _ADCCH1VAL1
                                   1150 	.globl _ADCCH1VAL0
                                   1151 	.globl _ADCCH0VAL
                                   1152 	.globl _ADCCH0VAL1
                                   1153 	.globl _ADCCH0VAL0
                                   1154 	.globl _axradio_trxstate
                                   1155 	.globl _axradio_mode
                                   1156 	.globl _axradio_conv_time_totimer0
                                   1157 	.globl _axradio_isr
                                   1158 	.globl _ax5043_receiver_on_continuous
                                   1159 	.globl _ax5043_receiver_on_wor
                                   1160 	.globl _ax5043_prepare_tx
                                   1161 	.globl _ax5043_off
                                   1162 	.globl _ax5043_off_xtal
                                   1163 	.globl _axradio_wait_for_xtal
                                   1164 	.globl _axradio_init
                                   1165 	.globl _axradio_cansleep
                                   1166 	.globl _axradio_set_mode
                                   1167 	.globl _axradio_set_channel
                                   1168 	.globl _axradio_get_pllvcoi
                                   1169 	.globl _axradio_set_local_address
                                   1170 	.globl _axradio_set_default_remote_address
                                   1171 	.globl _axradio_transmit
                                   1172 ;--------------------------------------------------------
                                   1173 ; special function registers
                                   1174 ;--------------------------------------------------------
                                   1175 	.area RSEG    (ABS,DATA)
      000000                       1176 	.org 0x0000
                           0000E0  1177 _ACC	=	0x00e0
                           0000F0  1178 _B	=	0x00f0
                           000083  1179 _DPH	=	0x0083
                           000085  1180 _DPH1	=	0x0085
                           000082  1181 _DPL	=	0x0082
                           000084  1182 _DPL1	=	0x0084
                           008382  1183 _DPTR0	=	0x8382
                           008584  1184 _DPTR1	=	0x8584
                           000086  1185 _DPS	=	0x0086
                           0000A0  1186 _E2IE	=	0x00a0
                           0000C0  1187 _E2IP	=	0x00c0
                           000098  1188 _EIE	=	0x0098
                           0000B0  1189 _EIP	=	0x00b0
                           0000A8  1190 _IE	=	0x00a8
                           0000B8  1191 _IP	=	0x00b8
                           000087  1192 _PCON	=	0x0087
                           0000D0  1193 _PSW	=	0x00d0
                           000081  1194 _SP	=	0x0081
                           0000D9  1195 _XPAGE	=	0x00d9
                           0000D9  1196 __XPAGE	=	0x00d9
                           0000CA  1197 _ADCCH0CONFIG	=	0x00ca
                           0000CB  1198 _ADCCH1CONFIG	=	0x00cb
                           0000D2  1199 _ADCCH2CONFIG	=	0x00d2
                           0000D3  1200 _ADCCH3CONFIG	=	0x00d3
                           0000D1  1201 _ADCCLKSRC	=	0x00d1
                           0000C9  1202 _ADCCONV	=	0x00c9
                           0000E1  1203 _ANALOGCOMP	=	0x00e1
                           0000C6  1204 _CLKCON	=	0x00c6
                           0000C7  1205 _CLKSTAT	=	0x00c7
                           000097  1206 _CODECONFIG	=	0x0097
                           0000E3  1207 _DBGLNKBUF	=	0x00e3
                           0000E2  1208 _DBGLNKSTAT	=	0x00e2
                           000089  1209 _DIRA	=	0x0089
                           00008A  1210 _DIRB	=	0x008a
                           00008B  1211 _DIRC	=	0x008b
                           00008E  1212 _DIRR	=	0x008e
                           0000C8  1213 _PINA	=	0x00c8
                           0000E8  1214 _PINB	=	0x00e8
                           0000F8  1215 _PINC	=	0x00f8
                           00008D  1216 _PINR	=	0x008d
                           000080  1217 _PORTA	=	0x0080
                           000088  1218 _PORTB	=	0x0088
                           000090  1219 _PORTC	=	0x0090
                           00008C  1220 _PORTR	=	0x008c
                           0000CE  1221 _IC0CAPT0	=	0x00ce
                           0000CF  1222 _IC0CAPT1	=	0x00cf
                           00CFCE  1223 _IC0CAPT	=	0xcfce
                           0000CC  1224 _IC0MODE	=	0x00cc
                           0000CD  1225 _IC0STATUS	=	0x00cd
                           0000D6  1226 _IC1CAPT0	=	0x00d6
                           0000D7  1227 _IC1CAPT1	=	0x00d7
                           00D7D6  1228 _IC1CAPT	=	0xd7d6
                           0000D4  1229 _IC1MODE	=	0x00d4
                           0000D5  1230 _IC1STATUS	=	0x00d5
                           000092  1231 _NVADDR0	=	0x0092
                           000093  1232 _NVADDR1	=	0x0093
                           009392  1233 _NVADDR	=	0x9392
                           000094  1234 _NVDATA0	=	0x0094
                           000095  1235 _NVDATA1	=	0x0095
                           009594  1236 _NVDATA	=	0x9594
                           000096  1237 _NVKEY	=	0x0096
                           000091  1238 _NVSTATUS	=	0x0091
                           0000BC  1239 _OC0COMP0	=	0x00bc
                           0000BD  1240 _OC0COMP1	=	0x00bd
                           00BDBC  1241 _OC0COMP	=	0xbdbc
                           0000B9  1242 _OC0MODE	=	0x00b9
                           0000BA  1243 _OC0PIN	=	0x00ba
                           0000BB  1244 _OC0STATUS	=	0x00bb
                           0000C4  1245 _OC1COMP0	=	0x00c4
                           0000C5  1246 _OC1COMP1	=	0x00c5
                           00C5C4  1247 _OC1COMP	=	0xc5c4
                           0000C1  1248 _OC1MODE	=	0x00c1
                           0000C2  1249 _OC1PIN	=	0x00c2
                           0000C3  1250 _OC1STATUS	=	0x00c3
                           0000B1  1251 _RADIOACC	=	0x00b1
                           0000B3  1252 _RADIOADDR0	=	0x00b3
                           0000B2  1253 _RADIOADDR1	=	0x00b2
                           00B2B3  1254 _RADIOADDR	=	0xb2b3
                           0000B7  1255 _RADIODATA0	=	0x00b7
                           0000B6  1256 _RADIODATA1	=	0x00b6
                           0000B5  1257 _RADIODATA2	=	0x00b5
                           0000B4  1258 _RADIODATA3	=	0x00b4
                           B4B5B6B7  1259 _RADIODATA	=	0xb4b5b6b7
                           0000BE  1260 _RADIOSTAT0	=	0x00be
                           0000BF  1261 _RADIOSTAT1	=	0x00bf
                           00BFBE  1262 _RADIOSTAT	=	0xbfbe
                           0000DF  1263 _SPCLKSRC	=	0x00df
                           0000DC  1264 _SPMODE	=	0x00dc
                           0000DE  1265 _SPSHREG	=	0x00de
                           0000DD  1266 _SPSTATUS	=	0x00dd
                           00009A  1267 _T0CLKSRC	=	0x009a
                           00009C  1268 _T0CNT0	=	0x009c
                           00009D  1269 _T0CNT1	=	0x009d
                           009D9C  1270 _T0CNT	=	0x9d9c
                           000099  1271 _T0MODE	=	0x0099
                           00009E  1272 _T0PERIOD0	=	0x009e
                           00009F  1273 _T0PERIOD1	=	0x009f
                           009F9E  1274 _T0PERIOD	=	0x9f9e
                           00009B  1275 _T0STATUS	=	0x009b
                           0000A2  1276 _T1CLKSRC	=	0x00a2
                           0000A4  1277 _T1CNT0	=	0x00a4
                           0000A5  1278 _T1CNT1	=	0x00a5
                           00A5A4  1279 _T1CNT	=	0xa5a4
                           0000A1  1280 _T1MODE	=	0x00a1
                           0000A6  1281 _T1PERIOD0	=	0x00a6
                           0000A7  1282 _T1PERIOD1	=	0x00a7
                           00A7A6  1283 _T1PERIOD	=	0xa7a6
                           0000A3  1284 _T1STATUS	=	0x00a3
                           0000AA  1285 _T2CLKSRC	=	0x00aa
                           0000AC  1286 _T2CNT0	=	0x00ac
                           0000AD  1287 _T2CNT1	=	0x00ad
                           00ADAC  1288 _T2CNT	=	0xadac
                           0000A9  1289 _T2MODE	=	0x00a9
                           0000AE  1290 _T2PERIOD0	=	0x00ae
                           0000AF  1291 _T2PERIOD1	=	0x00af
                           00AFAE  1292 _T2PERIOD	=	0xafae
                           0000AB  1293 _T2STATUS	=	0x00ab
                           0000E4  1294 _U0CTRL	=	0x00e4
                           0000E7  1295 _U0MODE	=	0x00e7
                           0000E6  1296 _U0SHREG	=	0x00e6
                           0000E5  1297 _U0STATUS	=	0x00e5
                           0000EC  1298 _U1CTRL	=	0x00ec
                           0000EF  1299 _U1MODE	=	0x00ef
                           0000EE  1300 _U1SHREG	=	0x00ee
                           0000ED  1301 _U1STATUS	=	0x00ed
                           0000DA  1302 _WDTCFG	=	0x00da
                           0000DB  1303 _WDTRESET	=	0x00db
                           0000F1  1304 _WTCFGA	=	0x00f1
                           0000F9  1305 _WTCFGB	=	0x00f9
                           0000F2  1306 _WTCNTA0	=	0x00f2
                           0000F3  1307 _WTCNTA1	=	0x00f3
                           00F3F2  1308 _WTCNTA	=	0xf3f2
                           0000FA  1309 _WTCNTB0	=	0x00fa
                           0000FB  1310 _WTCNTB1	=	0x00fb
                           00FBFA  1311 _WTCNTB	=	0xfbfa
                           0000EB  1312 _WTCNTR1	=	0x00eb
                           0000F4  1313 _WTEVTA0	=	0x00f4
                           0000F5  1314 _WTEVTA1	=	0x00f5
                           00F5F4  1315 _WTEVTA	=	0xf5f4
                           0000F6  1316 _WTEVTB0	=	0x00f6
                           0000F7  1317 _WTEVTB1	=	0x00f7
                           00F7F6  1318 _WTEVTB	=	0xf7f6
                           0000FC  1319 _WTEVTC0	=	0x00fc
                           0000FD  1320 _WTEVTC1	=	0x00fd
                           00FDFC  1321 _WTEVTC	=	0xfdfc
                           0000FE  1322 _WTEVTD0	=	0x00fe
                           0000FF  1323 _WTEVTD1	=	0x00ff
                           00FFFE  1324 _WTEVTD	=	0xfffe
                           0000E9  1325 _WTIRQEN	=	0x00e9
                           0000EA  1326 _WTSTAT	=	0x00ea
                                   1327 ;--------------------------------------------------------
                                   1328 ; special function bits
                                   1329 ;--------------------------------------------------------
                                   1330 	.area RSEG    (ABS,DATA)
      000000                       1331 	.org 0x0000
                           0000E0  1332 _ACC_0	=	0x00e0
                           0000E1  1333 _ACC_1	=	0x00e1
                           0000E2  1334 _ACC_2	=	0x00e2
                           0000E3  1335 _ACC_3	=	0x00e3
                           0000E4  1336 _ACC_4	=	0x00e4
                           0000E5  1337 _ACC_5	=	0x00e5
                           0000E6  1338 _ACC_6	=	0x00e6
                           0000E7  1339 _ACC_7	=	0x00e7
                           0000F0  1340 _B_0	=	0x00f0
                           0000F1  1341 _B_1	=	0x00f1
                           0000F2  1342 _B_2	=	0x00f2
                           0000F3  1343 _B_3	=	0x00f3
                           0000F4  1344 _B_4	=	0x00f4
                           0000F5  1345 _B_5	=	0x00f5
                           0000F6  1346 _B_6	=	0x00f6
                           0000F7  1347 _B_7	=	0x00f7
                           0000A0  1348 _E2IE_0	=	0x00a0
                           0000A1  1349 _E2IE_1	=	0x00a1
                           0000A2  1350 _E2IE_2	=	0x00a2
                           0000A3  1351 _E2IE_3	=	0x00a3
                           0000A4  1352 _E2IE_4	=	0x00a4
                           0000A5  1353 _E2IE_5	=	0x00a5
                           0000A6  1354 _E2IE_6	=	0x00a6
                           0000A7  1355 _E2IE_7	=	0x00a7
                           0000C0  1356 _E2IP_0	=	0x00c0
                           0000C1  1357 _E2IP_1	=	0x00c1
                           0000C2  1358 _E2IP_2	=	0x00c2
                           0000C3  1359 _E2IP_3	=	0x00c3
                           0000C4  1360 _E2IP_4	=	0x00c4
                           0000C5  1361 _E2IP_5	=	0x00c5
                           0000C6  1362 _E2IP_6	=	0x00c6
                           0000C7  1363 _E2IP_7	=	0x00c7
                           000098  1364 _EIE_0	=	0x0098
                           000099  1365 _EIE_1	=	0x0099
                           00009A  1366 _EIE_2	=	0x009a
                           00009B  1367 _EIE_3	=	0x009b
                           00009C  1368 _EIE_4	=	0x009c
                           00009D  1369 _EIE_5	=	0x009d
                           00009E  1370 _EIE_6	=	0x009e
                           00009F  1371 _EIE_7	=	0x009f
                           0000B0  1372 _EIP_0	=	0x00b0
                           0000B1  1373 _EIP_1	=	0x00b1
                           0000B2  1374 _EIP_2	=	0x00b2
                           0000B3  1375 _EIP_3	=	0x00b3
                           0000B4  1376 _EIP_4	=	0x00b4
                           0000B5  1377 _EIP_5	=	0x00b5
                           0000B6  1378 _EIP_6	=	0x00b6
                           0000B7  1379 _EIP_7	=	0x00b7
                           0000A8  1380 _IE_0	=	0x00a8
                           0000A9  1381 _IE_1	=	0x00a9
                           0000AA  1382 _IE_2	=	0x00aa
                           0000AB  1383 _IE_3	=	0x00ab
                           0000AC  1384 _IE_4	=	0x00ac
                           0000AD  1385 _IE_5	=	0x00ad
                           0000AE  1386 _IE_6	=	0x00ae
                           0000AF  1387 _IE_7	=	0x00af
                           0000AF  1388 _EA	=	0x00af
                           0000B8  1389 _IP_0	=	0x00b8
                           0000B9  1390 _IP_1	=	0x00b9
                           0000BA  1391 _IP_2	=	0x00ba
                           0000BB  1392 _IP_3	=	0x00bb
                           0000BC  1393 _IP_4	=	0x00bc
                           0000BD  1394 _IP_5	=	0x00bd
                           0000BE  1395 _IP_6	=	0x00be
                           0000BF  1396 _IP_7	=	0x00bf
                           0000D0  1397 _P	=	0x00d0
                           0000D1  1398 _F1	=	0x00d1
                           0000D2  1399 _OV	=	0x00d2
                           0000D3  1400 _RS0	=	0x00d3
                           0000D4  1401 _RS1	=	0x00d4
                           0000D5  1402 _F0	=	0x00d5
                           0000D6  1403 _AC	=	0x00d6
                           0000D7  1404 _CY	=	0x00d7
                           0000C8  1405 _PINA_0	=	0x00c8
                           0000C9  1406 _PINA_1	=	0x00c9
                           0000CA  1407 _PINA_2	=	0x00ca
                           0000CB  1408 _PINA_3	=	0x00cb
                           0000CC  1409 _PINA_4	=	0x00cc
                           0000CD  1410 _PINA_5	=	0x00cd
                           0000CE  1411 _PINA_6	=	0x00ce
                           0000CF  1412 _PINA_7	=	0x00cf
                           0000E8  1413 _PINB_0	=	0x00e8
                           0000E9  1414 _PINB_1	=	0x00e9
                           0000EA  1415 _PINB_2	=	0x00ea
                           0000EB  1416 _PINB_3	=	0x00eb
                           0000EC  1417 _PINB_4	=	0x00ec
                           0000ED  1418 _PINB_5	=	0x00ed
                           0000EE  1419 _PINB_6	=	0x00ee
                           0000EF  1420 _PINB_7	=	0x00ef
                           0000F8  1421 _PINC_0	=	0x00f8
                           0000F9  1422 _PINC_1	=	0x00f9
                           0000FA  1423 _PINC_2	=	0x00fa
                           0000FB  1424 _PINC_3	=	0x00fb
                           0000FC  1425 _PINC_4	=	0x00fc
                           0000FD  1426 _PINC_5	=	0x00fd
                           0000FE  1427 _PINC_6	=	0x00fe
                           0000FF  1428 _PINC_7	=	0x00ff
                           000080  1429 _PORTA_0	=	0x0080
                           000081  1430 _PORTA_1	=	0x0081
                           000082  1431 _PORTA_2	=	0x0082
                           000083  1432 _PORTA_3	=	0x0083
                           000084  1433 _PORTA_4	=	0x0084
                           000085  1434 _PORTA_5	=	0x0085
                           000086  1435 _PORTA_6	=	0x0086
                           000087  1436 _PORTA_7	=	0x0087
                           000088  1437 _PORTB_0	=	0x0088
                           000089  1438 _PORTB_1	=	0x0089
                           00008A  1439 _PORTB_2	=	0x008a
                           00008B  1440 _PORTB_3	=	0x008b
                           00008C  1441 _PORTB_4	=	0x008c
                           00008D  1442 _PORTB_5	=	0x008d
                           00008E  1443 _PORTB_6	=	0x008e
                           00008F  1444 _PORTB_7	=	0x008f
                           000090  1445 _PORTC_0	=	0x0090
                           000091  1446 _PORTC_1	=	0x0091
                           000092  1447 _PORTC_2	=	0x0092
                           000093  1448 _PORTC_3	=	0x0093
                           000094  1449 _PORTC_4	=	0x0094
                           000095  1450 _PORTC_5	=	0x0095
                           000096  1451 _PORTC_6	=	0x0096
                           000097  1452 _PORTC_7	=	0x0097
                                   1453 ;--------------------------------------------------------
                                   1454 ; overlayable register banks
                                   1455 ;--------------------------------------------------------
                                   1456 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                       1457 	.ds 8
                                   1458 ;--------------------------------------------------------
                                   1459 ; overlayable bit register bank
                                   1460 ;--------------------------------------------------------
                                   1461 	.area BIT_BANK	(REL,OVR,DATA)
      000020                       1462 bits:
      000020                       1463 	.ds 1
                           008000  1464 	b0 = bits[0]
                           008100  1465 	b1 = bits[1]
                           008200  1466 	b2 = bits[2]
                           008300  1467 	b3 = bits[3]
                           008400  1468 	b4 = bits[4]
                           008500  1469 	b5 = bits[5]
                           008600  1470 	b6 = bits[6]
                           008700  1471 	b7 = bits[7]
                                   1472 ;--------------------------------------------------------
                                   1473 ; internal ram data
                                   1474 ;--------------------------------------------------------
                                   1475 	.area DSEG    (DATA)
      000017                       1476 _axradio_mode::
      000017                       1477 	.ds 1
      000018                       1478 _axradio_trxstate::
      000018                       1479 	.ds 1
                                   1480 ;--------------------------------------------------------
                                   1481 ; overlayable items in internal ram 
                                   1482 ;--------------------------------------------------------
                                   1483 	.area	OSEG    (OVR,DATA)
                                   1484 	.area	OSEG    (OVR,DATA)
                                   1485 ;--------------------------------------------------------
                                   1486 ; indirectly addressable internal ram data
                                   1487 ;--------------------------------------------------------
                                   1488 	.area ISEG    (DATA)
                                   1489 ;--------------------------------------------------------
                                   1490 ; absolute internal ram data
                                   1491 ;--------------------------------------------------------
                                   1492 	.area IABS    (ABS,DATA)
                                   1493 	.area IABS    (ABS,DATA)
                                   1494 ;--------------------------------------------------------
                                   1495 ; bit data
                                   1496 ;--------------------------------------------------------
                                   1497 	.area BSEG    (BIT)
                                   1498 ;--------------------------------------------------------
                                   1499 ; paged external ram data
                                   1500 ;--------------------------------------------------------
                                   1501 	.area PSEG    (PAG,XDATA)
                                   1502 ;--------------------------------------------------------
                                   1503 ; external ram data
                                   1504 ;--------------------------------------------------------
                                   1505 	.area XSEG    (XDATA)
                           007020  1506 _ADCCH0VAL0	=	0x7020
                           007021  1507 _ADCCH0VAL1	=	0x7021
                           007020  1508 _ADCCH0VAL	=	0x7020
                           007022  1509 _ADCCH1VAL0	=	0x7022
                           007023  1510 _ADCCH1VAL1	=	0x7023
                           007022  1511 _ADCCH1VAL	=	0x7022
                           007024  1512 _ADCCH2VAL0	=	0x7024
                           007025  1513 _ADCCH2VAL1	=	0x7025
                           007024  1514 _ADCCH2VAL	=	0x7024
                           007026  1515 _ADCCH3VAL0	=	0x7026
                           007027  1516 _ADCCH3VAL1	=	0x7027
                           007026  1517 _ADCCH3VAL	=	0x7026
                           007028  1518 _ADCTUNE0	=	0x7028
                           007029  1519 _ADCTUNE1	=	0x7029
                           00702A  1520 _ADCTUNE2	=	0x702a
                           007010  1521 _DMA0ADDR0	=	0x7010
                           007011  1522 _DMA0ADDR1	=	0x7011
                           007010  1523 _DMA0ADDR	=	0x7010
                           007014  1524 _DMA0CONFIG	=	0x7014
                           007012  1525 _DMA1ADDR0	=	0x7012
                           007013  1526 _DMA1ADDR1	=	0x7013
                           007012  1527 _DMA1ADDR	=	0x7012
                           007015  1528 _DMA1CONFIG	=	0x7015
                           007070  1529 _FRCOSCCONFIG	=	0x7070
                           007071  1530 _FRCOSCCTRL	=	0x7071
                           007076  1531 _FRCOSCFREQ0	=	0x7076
                           007077  1532 _FRCOSCFREQ1	=	0x7077
                           007076  1533 _FRCOSCFREQ	=	0x7076
                           007072  1534 _FRCOSCKFILT0	=	0x7072
                           007073  1535 _FRCOSCKFILT1	=	0x7073
                           007072  1536 _FRCOSCKFILT	=	0x7072
                           007078  1537 _FRCOSCPER0	=	0x7078
                           007079  1538 _FRCOSCPER1	=	0x7079
                           007078  1539 _FRCOSCPER	=	0x7078
                           007074  1540 _FRCOSCREF0	=	0x7074
                           007075  1541 _FRCOSCREF1	=	0x7075
                           007074  1542 _FRCOSCREF	=	0x7074
                           007007  1543 _ANALOGA	=	0x7007
                           00700C  1544 _GPIOENABLE	=	0x700c
                           007003  1545 _EXTIRQ	=	0x7003
                           007000  1546 _INTCHGA	=	0x7000
                           007001  1547 _INTCHGB	=	0x7001
                           007002  1548 _INTCHGC	=	0x7002
                           007008  1549 _PALTA	=	0x7008
                           007009  1550 _PALTB	=	0x7009
                           00700A  1551 _PALTC	=	0x700a
                           007046  1552 _PALTRADIO	=	0x7046
                           007004  1553 _PINCHGA	=	0x7004
                           007005  1554 _PINCHGB	=	0x7005
                           007006  1555 _PINCHGC	=	0x7006
                           00700B  1556 _PINSEL	=	0x700b
                           007060  1557 _LPOSCCONFIG	=	0x7060
                           007066  1558 _LPOSCFREQ0	=	0x7066
                           007067  1559 _LPOSCFREQ1	=	0x7067
                           007066  1560 _LPOSCFREQ	=	0x7066
                           007062  1561 _LPOSCKFILT0	=	0x7062
                           007063  1562 _LPOSCKFILT1	=	0x7063
                           007062  1563 _LPOSCKFILT	=	0x7062
                           007068  1564 _LPOSCPER0	=	0x7068
                           007069  1565 _LPOSCPER1	=	0x7069
                           007068  1566 _LPOSCPER	=	0x7068
                           007064  1567 _LPOSCREF0	=	0x7064
                           007065  1568 _LPOSCREF1	=	0x7065
                           007064  1569 _LPOSCREF	=	0x7064
                           007054  1570 _LPXOSCGM	=	0x7054
                           007F01  1571 _MISCCTRL	=	0x7f01
                           007053  1572 _OSCCALIB	=	0x7053
                           007050  1573 _OSCFORCERUN	=	0x7050
                           007052  1574 _OSCREADY	=	0x7052
                           007051  1575 _OSCRUN	=	0x7051
                           007040  1576 _RADIOFDATAADDR0	=	0x7040
                           007041  1577 _RADIOFDATAADDR1	=	0x7041
                           007040  1578 _RADIOFDATAADDR	=	0x7040
                           007042  1579 _RADIOFSTATADDR0	=	0x7042
                           007043  1580 _RADIOFSTATADDR1	=	0x7043
                           007042  1581 _RADIOFSTATADDR	=	0x7042
                           007044  1582 _RADIOMUX	=	0x7044
                           007084  1583 _SCRATCH0	=	0x7084
                           007085  1584 _SCRATCH1	=	0x7085
                           007086  1585 _SCRATCH2	=	0x7086
                           007087  1586 _SCRATCH3	=	0x7087
                           007F00  1587 _SILICONREV	=	0x7f00
                           007F19  1588 _XTALAMPL	=	0x7f19
                           007F18  1589 _XTALOSC	=	0x7f18
                           007F1A  1590 _XTALREADY	=	0x7f1a
                           007081  1591 _RNGBYTE	=	0x7081
                           007091  1592 _AESCONFIG	=	0x7091
                           007098  1593 _AESCURBLOCK	=	0x7098
                           007094  1594 _AESINADDR0	=	0x7094
                           007095  1595 _AESINADDR1	=	0x7095
                           007094  1596 _AESINADDR	=	0x7094
                           007092  1597 _AESKEYADDR0	=	0x7092
                           007093  1598 _AESKEYADDR1	=	0x7093
                           007092  1599 _AESKEYADDR	=	0x7092
                           007090  1600 _AESMODE	=	0x7090
                           007096  1601 _AESOUTADDR0	=	0x7096
                           007097  1602 _AESOUTADDR1	=	0x7097
                           007096  1603 _AESOUTADDR	=	0x7096
                           007080  1604 _RNGMODE	=	0x7080
                           007082  1605 _RNGCLKSRC0	=	0x7082
                           007083  1606 _RNGCLKSRC1	=	0x7083
                           003F82  1607 _XDPTR0	=	0x3f82
                           003F84  1608 _XDPTR1	=	0x3f84
                           003FA8  1609 _XIE	=	0x3fa8
                           003FB8  1610 _XIP	=	0x3fb8
                           003F87  1611 _XPCON	=	0x3f87
                           003FCA  1612 _XADCCH0CONFIG	=	0x3fca
                           003FCB  1613 _XADCCH1CONFIG	=	0x3fcb
                           003FD2  1614 _XADCCH2CONFIG	=	0x3fd2
                           003FD3  1615 _XADCCH3CONFIG	=	0x3fd3
                           003FD1  1616 _XADCCLKSRC	=	0x3fd1
                           003FC9  1617 _XADCCONV	=	0x3fc9
                           003FE1  1618 _XANALOGCOMP	=	0x3fe1
                           003FC6  1619 _XCLKCON	=	0x3fc6
                           003FC7  1620 _XCLKSTAT	=	0x3fc7
                           003F97  1621 _XCODECONFIG	=	0x3f97
                           003FE3  1622 _XDBGLNKBUF	=	0x3fe3
                           003FE2  1623 _XDBGLNKSTAT	=	0x3fe2
                           003F89  1624 _XDIRA	=	0x3f89
                           003F8A  1625 _XDIRB	=	0x3f8a
                           003F8B  1626 _XDIRC	=	0x3f8b
                           003F8E  1627 _XDIRR	=	0x3f8e
                           003FC8  1628 _XPINA	=	0x3fc8
                           003FE8  1629 _XPINB	=	0x3fe8
                           003FF8  1630 _XPINC	=	0x3ff8
                           003F8D  1631 _XPINR	=	0x3f8d
                           003F80  1632 _XPORTA	=	0x3f80
                           003F88  1633 _XPORTB	=	0x3f88
                           003F90  1634 _XPORTC	=	0x3f90
                           003F8C  1635 _XPORTR	=	0x3f8c
                           003FCE  1636 _XIC0CAPT0	=	0x3fce
                           003FCF  1637 _XIC0CAPT1	=	0x3fcf
                           003FCE  1638 _XIC0CAPT	=	0x3fce
                           003FCC  1639 _XIC0MODE	=	0x3fcc
                           003FCD  1640 _XIC0STATUS	=	0x3fcd
                           003FD6  1641 _XIC1CAPT0	=	0x3fd6
                           003FD7  1642 _XIC1CAPT1	=	0x3fd7
                           003FD6  1643 _XIC1CAPT	=	0x3fd6
                           003FD4  1644 _XIC1MODE	=	0x3fd4
                           003FD5  1645 _XIC1STATUS	=	0x3fd5
                           003F92  1646 _XNVADDR0	=	0x3f92
                           003F93  1647 _XNVADDR1	=	0x3f93
                           003F92  1648 _XNVADDR	=	0x3f92
                           003F94  1649 _XNVDATA0	=	0x3f94
                           003F95  1650 _XNVDATA1	=	0x3f95
                           003F94  1651 _XNVDATA	=	0x3f94
                           003F96  1652 _XNVKEY	=	0x3f96
                           003F91  1653 _XNVSTATUS	=	0x3f91
                           003FBC  1654 _XOC0COMP0	=	0x3fbc
                           003FBD  1655 _XOC0COMP1	=	0x3fbd
                           003FBC  1656 _XOC0COMP	=	0x3fbc
                           003FB9  1657 _XOC0MODE	=	0x3fb9
                           003FBA  1658 _XOC0PIN	=	0x3fba
                           003FBB  1659 _XOC0STATUS	=	0x3fbb
                           003FC4  1660 _XOC1COMP0	=	0x3fc4
                           003FC5  1661 _XOC1COMP1	=	0x3fc5
                           003FC4  1662 _XOC1COMP	=	0x3fc4
                           003FC1  1663 _XOC1MODE	=	0x3fc1
                           003FC2  1664 _XOC1PIN	=	0x3fc2
                           003FC3  1665 _XOC1STATUS	=	0x3fc3
                           003FB1  1666 _XRADIOACC	=	0x3fb1
                           003FB3  1667 _XRADIOADDR0	=	0x3fb3
                           003FB2  1668 _XRADIOADDR1	=	0x3fb2
                           003FB7  1669 _XRADIODATA0	=	0x3fb7
                           003FB6  1670 _XRADIODATA1	=	0x3fb6
                           003FB5  1671 _XRADIODATA2	=	0x3fb5
                           003FB4  1672 _XRADIODATA3	=	0x3fb4
                           003FBE  1673 _XRADIOSTAT0	=	0x3fbe
                           003FBF  1674 _XRADIOSTAT1	=	0x3fbf
                           003FBE  1675 _XRADIOSTAT	=	0x3fbe
                           003FDF  1676 _XSPCLKSRC	=	0x3fdf
                           003FDC  1677 _XSPMODE	=	0x3fdc
                           003FDE  1678 _XSPSHREG	=	0x3fde
                           003FDD  1679 _XSPSTATUS	=	0x3fdd
                           003F9A  1680 _XT0CLKSRC	=	0x3f9a
                           003F9C  1681 _XT0CNT0	=	0x3f9c
                           003F9D  1682 _XT0CNT1	=	0x3f9d
                           003F9C  1683 _XT0CNT	=	0x3f9c
                           003F99  1684 _XT0MODE	=	0x3f99
                           003F9E  1685 _XT0PERIOD0	=	0x3f9e
                           003F9F  1686 _XT0PERIOD1	=	0x3f9f
                           003F9E  1687 _XT0PERIOD	=	0x3f9e
                           003F9B  1688 _XT0STATUS	=	0x3f9b
                           003FA2  1689 _XT1CLKSRC	=	0x3fa2
                           003FA4  1690 _XT1CNT0	=	0x3fa4
                           003FA5  1691 _XT1CNT1	=	0x3fa5
                           003FA4  1692 _XT1CNT	=	0x3fa4
                           003FA1  1693 _XT1MODE	=	0x3fa1
                           003FA6  1694 _XT1PERIOD0	=	0x3fa6
                           003FA7  1695 _XT1PERIOD1	=	0x3fa7
                           003FA6  1696 _XT1PERIOD	=	0x3fa6
                           003FA3  1697 _XT1STATUS	=	0x3fa3
                           003FAA  1698 _XT2CLKSRC	=	0x3faa
                           003FAC  1699 _XT2CNT0	=	0x3fac
                           003FAD  1700 _XT2CNT1	=	0x3fad
                           003FAC  1701 _XT2CNT	=	0x3fac
                           003FA9  1702 _XT2MODE	=	0x3fa9
                           003FAE  1703 _XT2PERIOD0	=	0x3fae
                           003FAF  1704 _XT2PERIOD1	=	0x3faf
                           003FAE  1705 _XT2PERIOD	=	0x3fae
                           003FAB  1706 _XT2STATUS	=	0x3fab
                           003FE4  1707 _XU0CTRL	=	0x3fe4
                           003FE7  1708 _XU0MODE	=	0x3fe7
                           003FE6  1709 _XU0SHREG	=	0x3fe6
                           003FE5  1710 _XU0STATUS	=	0x3fe5
                           003FEC  1711 _XU1CTRL	=	0x3fec
                           003FEF  1712 _XU1MODE	=	0x3fef
                           003FEE  1713 _XU1SHREG	=	0x3fee
                           003FED  1714 _XU1STATUS	=	0x3fed
                           003FDA  1715 _XWDTCFG	=	0x3fda
                           003FDB  1716 _XWDTRESET	=	0x3fdb
                           003FF1  1717 _XWTCFGA	=	0x3ff1
                           003FF9  1718 _XWTCFGB	=	0x3ff9
                           003FF2  1719 _XWTCNTA0	=	0x3ff2
                           003FF3  1720 _XWTCNTA1	=	0x3ff3
                           003FF2  1721 _XWTCNTA	=	0x3ff2
                           003FFA  1722 _XWTCNTB0	=	0x3ffa
                           003FFB  1723 _XWTCNTB1	=	0x3ffb
                           003FFA  1724 _XWTCNTB	=	0x3ffa
                           003FEB  1725 _XWTCNTR1	=	0x3feb
                           003FF4  1726 _XWTEVTA0	=	0x3ff4
                           003FF5  1727 _XWTEVTA1	=	0x3ff5
                           003FF4  1728 _XWTEVTA	=	0x3ff4
                           003FF6  1729 _XWTEVTB0	=	0x3ff6
                           003FF7  1730 _XWTEVTB1	=	0x3ff7
                           003FF6  1731 _XWTEVTB	=	0x3ff6
                           003FFC  1732 _XWTEVTC0	=	0x3ffc
                           003FFD  1733 _XWTEVTC1	=	0x3ffd
                           003FFC  1734 _XWTEVTC	=	0x3ffc
                           003FFE  1735 _XWTEVTD0	=	0x3ffe
                           003FFF  1736 _XWTEVTD1	=	0x3fff
                           003FFE  1737 _XWTEVTD	=	0x3ffe
                           003FE9  1738 _XWTIRQEN	=	0x3fe9
                           003FEA  1739 _XWTSTAT	=	0x3fea
                           004114  1740 _AX5043_AFSKCTRL	=	0x4114
                           004113  1741 _AX5043_AFSKMARK0	=	0x4113
                           004112  1742 _AX5043_AFSKMARK1	=	0x4112
                           004111  1743 _AX5043_AFSKSPACE0	=	0x4111
                           004110  1744 _AX5043_AFSKSPACE1	=	0x4110
                           004043  1745 _AX5043_AGCCOUNTER	=	0x4043
                           004115  1746 _AX5043_AMPLFILTER	=	0x4115
                           004189  1747 _AX5043_BBOFFSCAP	=	0x4189
                           004188  1748 _AX5043_BBTUNE	=	0x4188
                           004041  1749 _AX5043_BGNDRSSI	=	0x4041
                           00422E  1750 _AX5043_BGNDRSSIGAIN	=	0x422e
                           00422F  1751 _AX5043_BGNDRSSITHR	=	0x422f
                           004017  1752 _AX5043_CRCINIT0	=	0x4017
                           004016  1753 _AX5043_CRCINIT1	=	0x4016
                           004015  1754 _AX5043_CRCINIT2	=	0x4015
                           004014  1755 _AX5043_CRCINIT3	=	0x4014
                           004332  1756 _AX5043_DACCONFIG	=	0x4332
                           004331  1757 _AX5043_DACVALUE0	=	0x4331
                           004330  1758 _AX5043_DACVALUE1	=	0x4330
                           004102  1759 _AX5043_DECIMATION	=	0x4102
                           004042  1760 _AX5043_DIVERSITY	=	0x4042
                           004011  1761 _AX5043_ENCODING	=	0x4011
                           004018  1762 _AX5043_FEC	=	0x4018
                           00401A  1763 _AX5043_FECSTATUS	=	0x401a
                           004019  1764 _AX5043_FECSYNC	=	0x4019
                           00402B  1765 _AX5043_FIFOCOUNT0	=	0x402b
                           00402A  1766 _AX5043_FIFOCOUNT1	=	0x402a
                           004029  1767 _AX5043_FIFODATA	=	0x4029
                           00402D  1768 _AX5043_FIFOFREE0	=	0x402d
                           00402C  1769 _AX5043_FIFOFREE1	=	0x402c
                           004028  1770 _AX5043_FIFOSTAT	=	0x4028
                           00402F  1771 _AX5043_FIFOTHRESH0	=	0x402f
                           00402E  1772 _AX5043_FIFOTHRESH1	=	0x402e
                           004012  1773 _AX5043_FRAMING	=	0x4012
                           004037  1774 _AX5043_FREQA0	=	0x4037
                           004036  1775 _AX5043_FREQA1	=	0x4036
                           004035  1776 _AX5043_FREQA2	=	0x4035
                           004034  1777 _AX5043_FREQA3	=	0x4034
                           00403F  1778 _AX5043_FREQB0	=	0x403f
                           00403E  1779 _AX5043_FREQB1	=	0x403e
                           00403D  1780 _AX5043_FREQB2	=	0x403d
                           00403C  1781 _AX5043_FREQB3	=	0x403c
                           004163  1782 _AX5043_FSKDEV0	=	0x4163
                           004162  1783 _AX5043_FSKDEV1	=	0x4162
                           004161  1784 _AX5043_FSKDEV2	=	0x4161
                           00410D  1785 _AX5043_FSKDMAX0	=	0x410d
                           00410C  1786 _AX5043_FSKDMAX1	=	0x410c
                           00410F  1787 _AX5043_FSKDMIN0	=	0x410f
                           00410E  1788 _AX5043_FSKDMIN1	=	0x410e
                           004309  1789 _AX5043_GPADC13VALUE0	=	0x4309
                           004308  1790 _AX5043_GPADC13VALUE1	=	0x4308
                           004300  1791 _AX5043_GPADCCTRL	=	0x4300
                           004301  1792 _AX5043_GPADCPERIOD	=	0x4301
                           004101  1793 _AX5043_IFFREQ0	=	0x4101
                           004100  1794 _AX5043_IFFREQ1	=	0x4100
                           00400B  1795 _AX5043_IRQINVERSION0	=	0x400b
                           00400A  1796 _AX5043_IRQINVERSION1	=	0x400a
                           004007  1797 _AX5043_IRQMASK0	=	0x4007
                           004006  1798 _AX5043_IRQMASK1	=	0x4006
                           00400D  1799 _AX5043_IRQREQUEST0	=	0x400d
                           00400C  1800 _AX5043_IRQREQUEST1	=	0x400c
                           004310  1801 _AX5043_LPOSCCONFIG	=	0x4310
                           004317  1802 _AX5043_LPOSCFREQ0	=	0x4317
                           004316  1803 _AX5043_LPOSCFREQ1	=	0x4316
                           004313  1804 _AX5043_LPOSCKFILT0	=	0x4313
                           004312  1805 _AX5043_LPOSCKFILT1	=	0x4312
                           004319  1806 _AX5043_LPOSCPER0	=	0x4319
                           004318  1807 _AX5043_LPOSCPER1	=	0x4318
                           004315  1808 _AX5043_LPOSCREF0	=	0x4315
                           004314  1809 _AX5043_LPOSCREF1	=	0x4314
                           004311  1810 _AX5043_LPOSCSTATUS	=	0x4311
                           004214  1811 _AX5043_MATCH0LEN	=	0x4214
                           004216  1812 _AX5043_MATCH0MAX	=	0x4216
                           004215  1813 _AX5043_MATCH0MIN	=	0x4215
                           004213  1814 _AX5043_MATCH0PAT0	=	0x4213
                           004212  1815 _AX5043_MATCH0PAT1	=	0x4212
                           004211  1816 _AX5043_MATCH0PAT2	=	0x4211
                           004210  1817 _AX5043_MATCH0PAT3	=	0x4210
                           00421C  1818 _AX5043_MATCH1LEN	=	0x421c
                           00421E  1819 _AX5043_MATCH1MAX	=	0x421e
                           00421D  1820 _AX5043_MATCH1MIN	=	0x421d
                           004219  1821 _AX5043_MATCH1PAT0	=	0x4219
                           004218  1822 _AX5043_MATCH1PAT1	=	0x4218
                           004108  1823 _AX5043_MAXDROFFSET0	=	0x4108
                           004107  1824 _AX5043_MAXDROFFSET1	=	0x4107
                           004106  1825 _AX5043_MAXDROFFSET2	=	0x4106
                           00410B  1826 _AX5043_MAXRFOFFSET0	=	0x410b
                           00410A  1827 _AX5043_MAXRFOFFSET1	=	0x410a
                           004109  1828 _AX5043_MAXRFOFFSET2	=	0x4109
                           004164  1829 _AX5043_MODCFGA	=	0x4164
                           004160  1830 _AX5043_MODCFGF	=	0x4160
                           004010  1831 _AX5043_MODULATION	=	0x4010
                           004025  1832 _AX5043_PINFUNCANTSEL	=	0x4025
                           004023  1833 _AX5043_PINFUNCDATA	=	0x4023
                           004022  1834 _AX5043_PINFUNCDCLK	=	0x4022
                           004024  1835 _AX5043_PINFUNCIRQ	=	0x4024
                           004026  1836 _AX5043_PINFUNCPWRAMP	=	0x4026
                           004021  1837 _AX5043_PINFUNCSYSCLK	=	0x4021
                           004020  1838 _AX5043_PINSTATE	=	0x4020
                           004233  1839 _AX5043_PKTACCEPTFLAGS	=	0x4233
                           004230  1840 _AX5043_PKTCHUNKSIZE	=	0x4230
                           004231  1841 _AX5043_PKTMISCFLAGS	=	0x4231
                           004232  1842 _AX5043_PKTSTOREFLAGS	=	0x4232
                           004031  1843 _AX5043_PLLCPI	=	0x4031
                           004039  1844 _AX5043_PLLCPIBOOST	=	0x4039
                           004182  1845 _AX5043_PLLLOCKDET	=	0x4182
                           004030  1846 _AX5043_PLLLOOP	=	0x4030
                           004038  1847 _AX5043_PLLLOOPBOOST	=	0x4038
                           004033  1848 _AX5043_PLLRANGINGA	=	0x4033
                           00403B  1849 _AX5043_PLLRANGINGB	=	0x403b
                           004183  1850 _AX5043_PLLRNGCLK	=	0x4183
                           004032  1851 _AX5043_PLLVCODIV	=	0x4032
                           004180  1852 _AX5043_PLLVCOI	=	0x4180
                           004181  1853 _AX5043_PLLVCOIR	=	0x4181
                           004005  1854 _AX5043_POWIRQMASK	=	0x4005
                           004003  1855 _AX5043_POWSTAT	=	0x4003
                           004004  1856 _AX5043_POWSTICKYSTAT	=	0x4004
                           004027  1857 _AX5043_PWRAMP	=	0x4027
                           004002  1858 _AX5043_PWRMODE	=	0x4002
                           004009  1859 _AX5043_RADIOEVENTMASK0	=	0x4009
                           004008  1860 _AX5043_RADIOEVENTMASK1	=	0x4008
                           00400F  1861 _AX5043_RADIOEVENTREQ0	=	0x400f
                           00400E  1862 _AX5043_RADIOEVENTREQ1	=	0x400e
                           00401C  1863 _AX5043_RADIOSTATE	=	0x401c
                           004040  1864 _AX5043_RSSI	=	0x4040
                           00422D  1865 _AX5043_RSSIABSTHR	=	0x422d
                           00422C  1866 _AX5043_RSSIREFERENCE	=	0x422c
                           004105  1867 _AX5043_RXDATARATE0	=	0x4105
                           004104  1868 _AX5043_RXDATARATE1	=	0x4104
                           004103  1869 _AX5043_RXDATARATE2	=	0x4103
                           004001  1870 _AX5043_SCRATCH	=	0x4001
                           004000  1871 _AX5043_SILICONREVISION	=	0x4000
                           00405B  1872 _AX5043_TIMER0	=	0x405b
                           00405A  1873 _AX5043_TIMER1	=	0x405a
                           004059  1874 _AX5043_TIMER2	=	0x4059
                           004227  1875 _AX5043_TMGRXAGC	=	0x4227
                           004223  1876 _AX5043_TMGRXBOOST	=	0x4223
                           004226  1877 _AX5043_TMGRXCOARSEAGC	=	0x4226
                           004225  1878 _AX5043_TMGRXOFFSACQ	=	0x4225
                           004229  1879 _AX5043_TMGRXPREAMBLE1	=	0x4229
                           00422A  1880 _AX5043_TMGRXPREAMBLE2	=	0x422a
                           00422B  1881 _AX5043_TMGRXPREAMBLE3	=	0x422b
                           004228  1882 _AX5043_TMGRXRSSI	=	0x4228
                           004224  1883 _AX5043_TMGRXSETTLE	=	0x4224
                           004220  1884 _AX5043_TMGTXBOOST	=	0x4220
                           004221  1885 _AX5043_TMGTXSETTLE	=	0x4221
                           004055  1886 _AX5043_TRKAFSKDEMOD0	=	0x4055
                           004054  1887 _AX5043_TRKAFSKDEMOD1	=	0x4054
                           004049  1888 _AX5043_TRKAMPLITUDE0	=	0x4049
                           004048  1889 _AX5043_TRKAMPLITUDE1	=	0x4048
                           004047  1890 _AX5043_TRKDATARATE0	=	0x4047
                           004046  1891 _AX5043_TRKDATARATE1	=	0x4046
                           004045  1892 _AX5043_TRKDATARATE2	=	0x4045
                           004051  1893 _AX5043_TRKFREQ0	=	0x4051
                           004050  1894 _AX5043_TRKFREQ1	=	0x4050
                           004053  1895 _AX5043_TRKFSKDEMOD0	=	0x4053
                           004052  1896 _AX5043_TRKFSKDEMOD1	=	0x4052
                           00404B  1897 _AX5043_TRKPHASE0	=	0x404b
                           00404A  1898 _AX5043_TRKPHASE1	=	0x404a
                           00404F  1899 _AX5043_TRKRFFREQ0	=	0x404f
                           00404E  1900 _AX5043_TRKRFFREQ1	=	0x404e
                           00404D  1901 _AX5043_TRKRFFREQ2	=	0x404d
                           004169  1902 _AX5043_TXPWRCOEFFA0	=	0x4169
                           004168  1903 _AX5043_TXPWRCOEFFA1	=	0x4168
                           00416B  1904 _AX5043_TXPWRCOEFFB0	=	0x416b
                           00416A  1905 _AX5043_TXPWRCOEFFB1	=	0x416a
                           00416D  1906 _AX5043_TXPWRCOEFFC0	=	0x416d
                           00416C  1907 _AX5043_TXPWRCOEFFC1	=	0x416c
                           00416F  1908 _AX5043_TXPWRCOEFFD0	=	0x416f
                           00416E  1909 _AX5043_TXPWRCOEFFD1	=	0x416e
                           004171  1910 _AX5043_TXPWRCOEFFE0	=	0x4171
                           004170  1911 _AX5043_TXPWRCOEFFE1	=	0x4170
                           004167  1912 _AX5043_TXRATE0	=	0x4167
                           004166  1913 _AX5043_TXRATE1	=	0x4166
                           004165  1914 _AX5043_TXRATE2	=	0x4165
                           00406B  1915 _AX5043_WAKEUP0	=	0x406b
                           00406A  1916 _AX5043_WAKEUP1	=	0x406a
                           00406D  1917 _AX5043_WAKEUPFREQ0	=	0x406d
                           00406C  1918 _AX5043_WAKEUPFREQ1	=	0x406c
                           004069  1919 _AX5043_WAKEUPTIMER0	=	0x4069
                           004068  1920 _AX5043_WAKEUPTIMER1	=	0x4068
                           00406E  1921 _AX5043_WAKEUPXOEARLY	=	0x406e
                           004184  1922 _AX5043_XTALCAP	=	0x4184
                           00401D  1923 _AX5043_XTALSTATUS	=	0x401d
                           004122  1924 _AX5043_AGCAHYST0	=	0x4122
                           004132  1925 _AX5043_AGCAHYST1	=	0x4132
                           004142  1926 _AX5043_AGCAHYST2	=	0x4142
                           004152  1927 _AX5043_AGCAHYST3	=	0x4152
                           004120  1928 _AX5043_AGCGAIN0	=	0x4120
                           004130  1929 _AX5043_AGCGAIN1	=	0x4130
                           004140  1930 _AX5043_AGCGAIN2	=	0x4140
                           004150  1931 _AX5043_AGCGAIN3	=	0x4150
                           004123  1932 _AX5043_AGCMINMAX0	=	0x4123
                           004133  1933 _AX5043_AGCMINMAX1	=	0x4133
                           004143  1934 _AX5043_AGCMINMAX2	=	0x4143
                           004153  1935 _AX5043_AGCMINMAX3	=	0x4153
                           004121  1936 _AX5043_AGCTARGET0	=	0x4121
                           004131  1937 _AX5043_AGCTARGET1	=	0x4131
                           004141  1938 _AX5043_AGCTARGET2	=	0x4141
                           004151  1939 _AX5043_AGCTARGET3	=	0x4151
                           00412B  1940 _AX5043_AMPLITUDEGAIN0	=	0x412b
                           00413B  1941 _AX5043_AMPLITUDEGAIN1	=	0x413b
                           00414B  1942 _AX5043_AMPLITUDEGAIN2	=	0x414b
                           00415B  1943 _AX5043_AMPLITUDEGAIN3	=	0x415b
                           00412F  1944 _AX5043_BBOFFSRES0	=	0x412f
                           00413F  1945 _AX5043_BBOFFSRES1	=	0x413f
                           00414F  1946 _AX5043_BBOFFSRES2	=	0x414f
                           00415F  1947 _AX5043_BBOFFSRES3	=	0x415f
                           004125  1948 _AX5043_DRGAIN0	=	0x4125
                           004135  1949 _AX5043_DRGAIN1	=	0x4135
                           004145  1950 _AX5043_DRGAIN2	=	0x4145
                           004155  1951 _AX5043_DRGAIN3	=	0x4155
                           00412E  1952 _AX5043_FOURFSK0	=	0x412e
                           00413E  1953 _AX5043_FOURFSK1	=	0x413e
                           00414E  1954 _AX5043_FOURFSK2	=	0x414e
                           00415E  1955 _AX5043_FOURFSK3	=	0x415e
                           00412D  1956 _AX5043_FREQDEV00	=	0x412d
                           00413D  1957 _AX5043_FREQDEV01	=	0x413d
                           00414D  1958 _AX5043_FREQDEV02	=	0x414d
                           00415D  1959 _AX5043_FREQDEV03	=	0x415d
                           00412C  1960 _AX5043_FREQDEV10	=	0x412c
                           00413C  1961 _AX5043_FREQDEV11	=	0x413c
                           00414C  1962 _AX5043_FREQDEV12	=	0x414c
                           00415C  1963 _AX5043_FREQDEV13	=	0x415c
                           004127  1964 _AX5043_FREQUENCYGAINA0	=	0x4127
                           004137  1965 _AX5043_FREQUENCYGAINA1	=	0x4137
                           004147  1966 _AX5043_FREQUENCYGAINA2	=	0x4147
                           004157  1967 _AX5043_FREQUENCYGAINA3	=	0x4157
                           004128  1968 _AX5043_FREQUENCYGAINB0	=	0x4128
                           004138  1969 _AX5043_FREQUENCYGAINB1	=	0x4138
                           004148  1970 _AX5043_FREQUENCYGAINB2	=	0x4148
                           004158  1971 _AX5043_FREQUENCYGAINB3	=	0x4158
                           004129  1972 _AX5043_FREQUENCYGAINC0	=	0x4129
                           004139  1973 _AX5043_FREQUENCYGAINC1	=	0x4139
                           004149  1974 _AX5043_FREQUENCYGAINC2	=	0x4149
                           004159  1975 _AX5043_FREQUENCYGAINC3	=	0x4159
                           00412A  1976 _AX5043_FREQUENCYGAIND0	=	0x412a
                           00413A  1977 _AX5043_FREQUENCYGAIND1	=	0x413a
                           00414A  1978 _AX5043_FREQUENCYGAIND2	=	0x414a
                           00415A  1979 _AX5043_FREQUENCYGAIND3	=	0x415a
                           004116  1980 _AX5043_FREQUENCYLEAK	=	0x4116
                           004126  1981 _AX5043_PHASEGAIN0	=	0x4126
                           004136  1982 _AX5043_PHASEGAIN1	=	0x4136
                           004146  1983 _AX5043_PHASEGAIN2	=	0x4146
                           004156  1984 _AX5043_PHASEGAIN3	=	0x4156
                           004207  1985 _AX5043_PKTADDR0	=	0x4207
                           004206  1986 _AX5043_PKTADDR1	=	0x4206
                           004205  1987 _AX5043_PKTADDR2	=	0x4205
                           004204  1988 _AX5043_PKTADDR3	=	0x4204
                           004200  1989 _AX5043_PKTADDRCFG	=	0x4200
                           00420B  1990 _AX5043_PKTADDRMASK0	=	0x420b
                           00420A  1991 _AX5043_PKTADDRMASK1	=	0x420a
                           004209  1992 _AX5043_PKTADDRMASK2	=	0x4209
                           004208  1993 _AX5043_PKTADDRMASK3	=	0x4208
                           004201  1994 _AX5043_PKTLENCFG	=	0x4201
                           004202  1995 _AX5043_PKTLENOFFSET	=	0x4202
                           004203  1996 _AX5043_PKTMAXLEN	=	0x4203
                           004118  1997 _AX5043_RXPARAMCURSET	=	0x4118
                           004117  1998 _AX5043_RXPARAMSETS	=	0x4117
                           004124  1999 _AX5043_TIMEGAIN0	=	0x4124
                           004134  2000 _AX5043_TIMEGAIN1	=	0x4134
                           004144  2001 _AX5043_TIMEGAIN2	=	0x4144
                           004154  2002 _AX5043_TIMEGAIN3	=	0x4154
                           005114  2003 _AX5043_AFSKCTRLNB	=	0x5114
                           005113  2004 _AX5043_AFSKMARK0NB	=	0x5113
                           005112  2005 _AX5043_AFSKMARK1NB	=	0x5112
                           005111  2006 _AX5043_AFSKSPACE0NB	=	0x5111
                           005110  2007 _AX5043_AFSKSPACE1NB	=	0x5110
                           005043  2008 _AX5043_AGCCOUNTERNB	=	0x5043
                           005115  2009 _AX5043_AMPLFILTERNB	=	0x5115
                           005189  2010 _AX5043_BBOFFSCAPNB	=	0x5189
                           005188  2011 _AX5043_BBTUNENB	=	0x5188
                           005041  2012 _AX5043_BGNDRSSINB	=	0x5041
                           00522E  2013 _AX5043_BGNDRSSIGAINNB	=	0x522e
                           00522F  2014 _AX5043_BGNDRSSITHRNB	=	0x522f
                           005017  2015 _AX5043_CRCINIT0NB	=	0x5017
                           005016  2016 _AX5043_CRCINIT1NB	=	0x5016
                           005015  2017 _AX5043_CRCINIT2NB	=	0x5015
                           005014  2018 _AX5043_CRCINIT3NB	=	0x5014
                           005332  2019 _AX5043_DACCONFIGNB	=	0x5332
                           005331  2020 _AX5043_DACVALUE0NB	=	0x5331
                           005330  2021 _AX5043_DACVALUE1NB	=	0x5330
                           005102  2022 _AX5043_DECIMATIONNB	=	0x5102
                           005042  2023 _AX5043_DIVERSITYNB	=	0x5042
                           005011  2024 _AX5043_ENCODINGNB	=	0x5011
                           005018  2025 _AX5043_FECNB	=	0x5018
                           00501A  2026 _AX5043_FECSTATUSNB	=	0x501a
                           005019  2027 _AX5043_FECSYNCNB	=	0x5019
                           00502B  2028 _AX5043_FIFOCOUNT0NB	=	0x502b
                           00502A  2029 _AX5043_FIFOCOUNT1NB	=	0x502a
                           005029  2030 _AX5043_FIFODATANB	=	0x5029
                           00502D  2031 _AX5043_FIFOFREE0NB	=	0x502d
                           00502C  2032 _AX5043_FIFOFREE1NB	=	0x502c
                           005028  2033 _AX5043_FIFOSTATNB	=	0x5028
                           00502F  2034 _AX5043_FIFOTHRESH0NB	=	0x502f
                           00502E  2035 _AX5043_FIFOTHRESH1NB	=	0x502e
                           005012  2036 _AX5043_FRAMINGNB	=	0x5012
                           005037  2037 _AX5043_FREQA0NB	=	0x5037
                           005036  2038 _AX5043_FREQA1NB	=	0x5036
                           005035  2039 _AX5043_FREQA2NB	=	0x5035
                           005034  2040 _AX5043_FREQA3NB	=	0x5034
                           00503F  2041 _AX5043_FREQB0NB	=	0x503f
                           00503E  2042 _AX5043_FREQB1NB	=	0x503e
                           00503D  2043 _AX5043_FREQB2NB	=	0x503d
                           00503C  2044 _AX5043_FREQB3NB	=	0x503c
                           005163  2045 _AX5043_FSKDEV0NB	=	0x5163
                           005162  2046 _AX5043_FSKDEV1NB	=	0x5162
                           005161  2047 _AX5043_FSKDEV2NB	=	0x5161
                           00510D  2048 _AX5043_FSKDMAX0NB	=	0x510d
                           00510C  2049 _AX5043_FSKDMAX1NB	=	0x510c
                           00510F  2050 _AX5043_FSKDMIN0NB	=	0x510f
                           00510E  2051 _AX5043_FSKDMIN1NB	=	0x510e
                           005309  2052 _AX5043_GPADC13VALUE0NB	=	0x5309
                           005308  2053 _AX5043_GPADC13VALUE1NB	=	0x5308
                           005300  2054 _AX5043_GPADCCTRLNB	=	0x5300
                           005301  2055 _AX5043_GPADCPERIODNB	=	0x5301
                           005101  2056 _AX5043_IFFREQ0NB	=	0x5101
                           005100  2057 _AX5043_IFFREQ1NB	=	0x5100
                           00500B  2058 _AX5043_IRQINVERSION0NB	=	0x500b
                           00500A  2059 _AX5043_IRQINVERSION1NB	=	0x500a
                           005007  2060 _AX5043_IRQMASK0NB	=	0x5007
                           005006  2061 _AX5043_IRQMASK1NB	=	0x5006
                           00500D  2062 _AX5043_IRQREQUEST0NB	=	0x500d
                           00500C  2063 _AX5043_IRQREQUEST1NB	=	0x500c
                           005310  2064 _AX5043_LPOSCCONFIGNB	=	0x5310
                           005317  2065 _AX5043_LPOSCFREQ0NB	=	0x5317
                           005316  2066 _AX5043_LPOSCFREQ1NB	=	0x5316
                           005313  2067 _AX5043_LPOSCKFILT0NB	=	0x5313
                           005312  2068 _AX5043_LPOSCKFILT1NB	=	0x5312
                           005319  2069 _AX5043_LPOSCPER0NB	=	0x5319
                           005318  2070 _AX5043_LPOSCPER1NB	=	0x5318
                           005315  2071 _AX5043_LPOSCREF0NB	=	0x5315
                           005314  2072 _AX5043_LPOSCREF1NB	=	0x5314
                           005311  2073 _AX5043_LPOSCSTATUSNB	=	0x5311
                           005214  2074 _AX5043_MATCH0LENNB	=	0x5214
                           005216  2075 _AX5043_MATCH0MAXNB	=	0x5216
                           005215  2076 _AX5043_MATCH0MINNB	=	0x5215
                           005213  2077 _AX5043_MATCH0PAT0NB	=	0x5213
                           005212  2078 _AX5043_MATCH0PAT1NB	=	0x5212
                           005211  2079 _AX5043_MATCH0PAT2NB	=	0x5211
                           005210  2080 _AX5043_MATCH0PAT3NB	=	0x5210
                           00521C  2081 _AX5043_MATCH1LENNB	=	0x521c
                           00521E  2082 _AX5043_MATCH1MAXNB	=	0x521e
                           00521D  2083 _AX5043_MATCH1MINNB	=	0x521d
                           005219  2084 _AX5043_MATCH1PAT0NB	=	0x5219
                           005218  2085 _AX5043_MATCH1PAT1NB	=	0x5218
                           005108  2086 _AX5043_MAXDROFFSET0NB	=	0x5108
                           005107  2087 _AX5043_MAXDROFFSET1NB	=	0x5107
                           005106  2088 _AX5043_MAXDROFFSET2NB	=	0x5106
                           00510B  2089 _AX5043_MAXRFOFFSET0NB	=	0x510b
                           00510A  2090 _AX5043_MAXRFOFFSET1NB	=	0x510a
                           005109  2091 _AX5043_MAXRFOFFSET2NB	=	0x5109
                           005164  2092 _AX5043_MODCFGANB	=	0x5164
                           005160  2093 _AX5043_MODCFGFNB	=	0x5160
                           005F5F  2094 _AX5043_MODCFGPNB	=	0x5f5f
                           005010  2095 _AX5043_MODULATIONNB	=	0x5010
                           005025  2096 _AX5043_PINFUNCANTSELNB	=	0x5025
                           005023  2097 _AX5043_PINFUNCDATANB	=	0x5023
                           005022  2098 _AX5043_PINFUNCDCLKNB	=	0x5022
                           005024  2099 _AX5043_PINFUNCIRQNB	=	0x5024
                           005026  2100 _AX5043_PINFUNCPWRAMPNB	=	0x5026
                           005021  2101 _AX5043_PINFUNCSYSCLKNB	=	0x5021
                           005020  2102 _AX5043_PINSTATENB	=	0x5020
                           005233  2103 _AX5043_PKTACCEPTFLAGSNB	=	0x5233
                           005230  2104 _AX5043_PKTCHUNKSIZENB	=	0x5230
                           005231  2105 _AX5043_PKTMISCFLAGSNB	=	0x5231
                           005232  2106 _AX5043_PKTSTOREFLAGSNB	=	0x5232
                           005031  2107 _AX5043_PLLCPINB	=	0x5031
                           005039  2108 _AX5043_PLLCPIBOOSTNB	=	0x5039
                           005182  2109 _AX5043_PLLLOCKDETNB	=	0x5182
                           005030  2110 _AX5043_PLLLOOPNB	=	0x5030
                           005038  2111 _AX5043_PLLLOOPBOOSTNB	=	0x5038
                           005033  2112 _AX5043_PLLRANGINGANB	=	0x5033
                           00503B  2113 _AX5043_PLLRANGINGBNB	=	0x503b
                           005183  2114 _AX5043_PLLRNGCLKNB	=	0x5183
                           005032  2115 _AX5043_PLLVCODIVNB	=	0x5032
                           005180  2116 _AX5043_PLLVCOINB	=	0x5180
                           005181  2117 _AX5043_PLLVCOIRNB	=	0x5181
                           005F08  2118 _AX5043_POWCTRL1NB	=	0x5f08
                           005005  2119 _AX5043_POWIRQMASKNB	=	0x5005
                           005003  2120 _AX5043_POWSTATNB	=	0x5003
                           005004  2121 _AX5043_POWSTICKYSTATNB	=	0x5004
                           005027  2122 _AX5043_PWRAMPNB	=	0x5027
                           005002  2123 _AX5043_PWRMODENB	=	0x5002
                           005009  2124 _AX5043_RADIOEVENTMASK0NB	=	0x5009
                           005008  2125 _AX5043_RADIOEVENTMASK1NB	=	0x5008
                           00500F  2126 _AX5043_RADIOEVENTREQ0NB	=	0x500f
                           00500E  2127 _AX5043_RADIOEVENTREQ1NB	=	0x500e
                           00501C  2128 _AX5043_RADIOSTATENB	=	0x501c
                           005F0D  2129 _AX5043_REFNB	=	0x5f0d
                           005040  2130 _AX5043_RSSINB	=	0x5040
                           00522D  2131 _AX5043_RSSIABSTHRNB	=	0x522d
                           00522C  2132 _AX5043_RSSIREFERENCENB	=	0x522c
                           005105  2133 _AX5043_RXDATARATE0NB	=	0x5105
                           005104  2134 _AX5043_RXDATARATE1NB	=	0x5104
                           005103  2135 _AX5043_RXDATARATE2NB	=	0x5103
                           005001  2136 _AX5043_SCRATCHNB	=	0x5001
                           005000  2137 _AX5043_SILICONREVISIONNB	=	0x5000
                           00505B  2138 _AX5043_TIMER0NB	=	0x505b
                           00505A  2139 _AX5043_TIMER1NB	=	0x505a
                           005059  2140 _AX5043_TIMER2NB	=	0x5059
                           005227  2141 _AX5043_TMGRXAGCNB	=	0x5227
                           005223  2142 _AX5043_TMGRXBOOSTNB	=	0x5223
                           005226  2143 _AX5043_TMGRXCOARSEAGCNB	=	0x5226
                           005225  2144 _AX5043_TMGRXOFFSACQNB	=	0x5225
                           005229  2145 _AX5043_TMGRXPREAMBLE1NB	=	0x5229
                           00522A  2146 _AX5043_TMGRXPREAMBLE2NB	=	0x522a
                           00522B  2147 _AX5043_TMGRXPREAMBLE3NB	=	0x522b
                           005228  2148 _AX5043_TMGRXRSSINB	=	0x5228
                           005224  2149 _AX5043_TMGRXSETTLENB	=	0x5224
                           005220  2150 _AX5043_TMGTXBOOSTNB	=	0x5220
                           005221  2151 _AX5043_TMGTXSETTLENB	=	0x5221
                           005055  2152 _AX5043_TRKAFSKDEMOD0NB	=	0x5055
                           005054  2153 _AX5043_TRKAFSKDEMOD1NB	=	0x5054
                           005049  2154 _AX5043_TRKAMPLITUDE0NB	=	0x5049
                           005048  2155 _AX5043_TRKAMPLITUDE1NB	=	0x5048
                           005047  2156 _AX5043_TRKDATARATE0NB	=	0x5047
                           005046  2157 _AX5043_TRKDATARATE1NB	=	0x5046
                           005045  2158 _AX5043_TRKDATARATE2NB	=	0x5045
                           005051  2159 _AX5043_TRKFREQ0NB	=	0x5051
                           005050  2160 _AX5043_TRKFREQ1NB	=	0x5050
                           005053  2161 _AX5043_TRKFSKDEMOD0NB	=	0x5053
                           005052  2162 _AX5043_TRKFSKDEMOD1NB	=	0x5052
                           00504B  2163 _AX5043_TRKPHASE0NB	=	0x504b
                           00504A  2164 _AX5043_TRKPHASE1NB	=	0x504a
                           00504F  2165 _AX5043_TRKRFFREQ0NB	=	0x504f
                           00504E  2166 _AX5043_TRKRFFREQ1NB	=	0x504e
                           00504D  2167 _AX5043_TRKRFFREQ2NB	=	0x504d
                           005169  2168 _AX5043_TXPWRCOEFFA0NB	=	0x5169
                           005168  2169 _AX5043_TXPWRCOEFFA1NB	=	0x5168
                           00516B  2170 _AX5043_TXPWRCOEFFB0NB	=	0x516b
                           00516A  2171 _AX5043_TXPWRCOEFFB1NB	=	0x516a
                           00516D  2172 _AX5043_TXPWRCOEFFC0NB	=	0x516d
                           00516C  2173 _AX5043_TXPWRCOEFFC1NB	=	0x516c
                           00516F  2174 _AX5043_TXPWRCOEFFD0NB	=	0x516f
                           00516E  2175 _AX5043_TXPWRCOEFFD1NB	=	0x516e
                           005171  2176 _AX5043_TXPWRCOEFFE0NB	=	0x5171
                           005170  2177 _AX5043_TXPWRCOEFFE1NB	=	0x5170
                           005167  2178 _AX5043_TXRATE0NB	=	0x5167
                           005166  2179 _AX5043_TXRATE1NB	=	0x5166
                           005165  2180 _AX5043_TXRATE2NB	=	0x5165
                           00506B  2181 _AX5043_WAKEUP0NB	=	0x506b
                           00506A  2182 _AX5043_WAKEUP1NB	=	0x506a
                           00506D  2183 _AX5043_WAKEUPFREQ0NB	=	0x506d
                           00506C  2184 _AX5043_WAKEUPFREQ1NB	=	0x506c
                           005069  2185 _AX5043_WAKEUPTIMER0NB	=	0x5069
                           005068  2186 _AX5043_WAKEUPTIMER1NB	=	0x5068
                           00506E  2187 _AX5043_WAKEUPXOEARLYNB	=	0x506e
                           005F11  2188 _AX5043_XTALAMPLNB	=	0x5f11
                           005184  2189 _AX5043_XTALCAPNB	=	0x5184
                           005F10  2190 _AX5043_XTALOSCNB	=	0x5f10
                           00501D  2191 _AX5043_XTALSTATUSNB	=	0x501d
                           005F00  2192 _AX5043_0xF00NB	=	0x5f00
                           005F0C  2193 _AX5043_0xF0CNB	=	0x5f0c
                           005F18  2194 _AX5043_0xF18NB	=	0x5f18
                           005F1C  2195 _AX5043_0xF1CNB	=	0x5f1c
                           005F21  2196 _AX5043_0xF21NB	=	0x5f21
                           005F22  2197 _AX5043_0xF22NB	=	0x5f22
                           005F23  2198 _AX5043_0xF23NB	=	0x5f23
                           005F26  2199 _AX5043_0xF26NB	=	0x5f26
                           005F30  2200 _AX5043_0xF30NB	=	0x5f30
                           005F31  2201 _AX5043_0xF31NB	=	0x5f31
                           005F32  2202 _AX5043_0xF32NB	=	0x5f32
                           005F33  2203 _AX5043_0xF33NB	=	0x5f33
                           005F34  2204 _AX5043_0xF34NB	=	0x5f34
                           005F35  2205 _AX5043_0xF35NB	=	0x5f35
                           005F44  2206 _AX5043_0xF44NB	=	0x5f44
                           005122  2207 _AX5043_AGCAHYST0NB	=	0x5122
                           005132  2208 _AX5043_AGCAHYST1NB	=	0x5132
                           005142  2209 _AX5043_AGCAHYST2NB	=	0x5142
                           005152  2210 _AX5043_AGCAHYST3NB	=	0x5152
                           005120  2211 _AX5043_AGCGAIN0NB	=	0x5120
                           005130  2212 _AX5043_AGCGAIN1NB	=	0x5130
                           005140  2213 _AX5043_AGCGAIN2NB	=	0x5140
                           005150  2214 _AX5043_AGCGAIN3NB	=	0x5150
                           005123  2215 _AX5043_AGCMINMAX0NB	=	0x5123
                           005133  2216 _AX5043_AGCMINMAX1NB	=	0x5133
                           005143  2217 _AX5043_AGCMINMAX2NB	=	0x5143
                           005153  2218 _AX5043_AGCMINMAX3NB	=	0x5153
                           005121  2219 _AX5043_AGCTARGET0NB	=	0x5121
                           005131  2220 _AX5043_AGCTARGET1NB	=	0x5131
                           005141  2221 _AX5043_AGCTARGET2NB	=	0x5141
                           005151  2222 _AX5043_AGCTARGET3NB	=	0x5151
                           00512B  2223 _AX5043_AMPLITUDEGAIN0NB	=	0x512b
                           00513B  2224 _AX5043_AMPLITUDEGAIN1NB	=	0x513b
                           00514B  2225 _AX5043_AMPLITUDEGAIN2NB	=	0x514b
                           00515B  2226 _AX5043_AMPLITUDEGAIN3NB	=	0x515b
                           00512F  2227 _AX5043_BBOFFSRES0NB	=	0x512f
                           00513F  2228 _AX5043_BBOFFSRES1NB	=	0x513f
                           00514F  2229 _AX5043_BBOFFSRES2NB	=	0x514f
                           00515F  2230 _AX5043_BBOFFSRES3NB	=	0x515f
                           005125  2231 _AX5043_DRGAIN0NB	=	0x5125
                           005135  2232 _AX5043_DRGAIN1NB	=	0x5135
                           005145  2233 _AX5043_DRGAIN2NB	=	0x5145
                           005155  2234 _AX5043_DRGAIN3NB	=	0x5155
                           00512E  2235 _AX5043_FOURFSK0NB	=	0x512e
                           00513E  2236 _AX5043_FOURFSK1NB	=	0x513e
                           00514E  2237 _AX5043_FOURFSK2NB	=	0x514e
                           00515E  2238 _AX5043_FOURFSK3NB	=	0x515e
                           00512D  2239 _AX5043_FREQDEV00NB	=	0x512d
                           00513D  2240 _AX5043_FREQDEV01NB	=	0x513d
                           00514D  2241 _AX5043_FREQDEV02NB	=	0x514d
                           00515D  2242 _AX5043_FREQDEV03NB	=	0x515d
                           00512C  2243 _AX5043_FREQDEV10NB	=	0x512c
                           00513C  2244 _AX5043_FREQDEV11NB	=	0x513c
                           00514C  2245 _AX5043_FREQDEV12NB	=	0x514c
                           00515C  2246 _AX5043_FREQDEV13NB	=	0x515c
                           005127  2247 _AX5043_FREQUENCYGAINA0NB	=	0x5127
                           005137  2248 _AX5043_FREQUENCYGAINA1NB	=	0x5137
                           005147  2249 _AX5043_FREQUENCYGAINA2NB	=	0x5147
                           005157  2250 _AX5043_FREQUENCYGAINA3NB	=	0x5157
                           005128  2251 _AX5043_FREQUENCYGAINB0NB	=	0x5128
                           005138  2252 _AX5043_FREQUENCYGAINB1NB	=	0x5138
                           005148  2253 _AX5043_FREQUENCYGAINB2NB	=	0x5148
                           005158  2254 _AX5043_FREQUENCYGAINB3NB	=	0x5158
                           005129  2255 _AX5043_FREQUENCYGAINC0NB	=	0x5129
                           005139  2256 _AX5043_FREQUENCYGAINC1NB	=	0x5139
                           005149  2257 _AX5043_FREQUENCYGAINC2NB	=	0x5149
                           005159  2258 _AX5043_FREQUENCYGAINC3NB	=	0x5159
                           00512A  2259 _AX5043_FREQUENCYGAIND0NB	=	0x512a
                           00513A  2260 _AX5043_FREQUENCYGAIND1NB	=	0x513a
                           00514A  2261 _AX5043_FREQUENCYGAIND2NB	=	0x514a
                           00515A  2262 _AX5043_FREQUENCYGAIND3NB	=	0x515a
                           005116  2263 _AX5043_FREQUENCYLEAKNB	=	0x5116
                           005126  2264 _AX5043_PHASEGAIN0NB	=	0x5126
                           005136  2265 _AX5043_PHASEGAIN1NB	=	0x5136
                           005146  2266 _AX5043_PHASEGAIN2NB	=	0x5146
                           005156  2267 _AX5043_PHASEGAIN3NB	=	0x5156
                           005207  2268 _AX5043_PKTADDR0NB	=	0x5207
                           005206  2269 _AX5043_PKTADDR1NB	=	0x5206
                           005205  2270 _AX5043_PKTADDR2NB	=	0x5205
                           005204  2271 _AX5043_PKTADDR3NB	=	0x5204
                           005200  2272 _AX5043_PKTADDRCFGNB	=	0x5200
                           00520B  2273 _AX5043_PKTADDRMASK0NB	=	0x520b
                           00520A  2274 _AX5043_PKTADDRMASK1NB	=	0x520a
                           005209  2275 _AX5043_PKTADDRMASK2NB	=	0x5209
                           005208  2276 _AX5043_PKTADDRMASK3NB	=	0x5208
                           005201  2277 _AX5043_PKTLENCFGNB	=	0x5201
                           005202  2278 _AX5043_PKTLENOFFSETNB	=	0x5202
                           005203  2279 _AX5043_PKTMAXLENNB	=	0x5203
                           005118  2280 _AX5043_RXPARAMCURSETNB	=	0x5118
                           005117  2281 _AX5043_RXPARAMSETSNB	=	0x5117
                           005124  2282 _AX5043_TIMEGAIN0NB	=	0x5124
                           005134  2283 _AX5043_TIMEGAIN1NB	=	0x5134
                           005144  2284 _AX5043_TIMEGAIN2NB	=	0x5144
                           005154  2285 _AX5043_TIMEGAIN3NB	=	0x5154
                           004F00  2286 _AX5043_0xF00	=	0x4f00
                           004F0C  2287 _AX5043_0xF0C	=	0x4f0c
                           004F10  2288 _AX5043_0xF10	=	0x4f10
                           004F11  2289 _AX5043_0xF11	=	0x4f11
                           004F18  2290 _AX5043_0xF18	=	0x4f18
                           004F1C  2291 _AX5043_0xF1C	=	0x4f1c
                           004F21  2292 _AX5043_0xF21	=	0x4f21
                           004F22  2293 _AX5043_0xF22	=	0x4f22
                           004F23  2294 _AX5043_0xF23	=	0x4f23
                           004F26  2295 _AX5043_0xF26	=	0x4f26
                           004F30  2296 _AX5043_0xF30	=	0x4f30
                           004F31  2297 _AX5043_0xF31	=	0x4f31
                           004F32  2298 _AX5043_0xF32	=	0x4f32
                           004F33  2299 _AX5043_0xF33	=	0x4f33
                           004F34  2300 _AX5043_0xF34	=	0x4f34
                           004F35  2301 _AX5043_0xF35	=	0x4f35
                           004F44  2302 _AX5043_0xF44	=	0x4f44
                           004F0D  2303 _AX5043_REF	=	0x4f0d
                           004F08  2304 _AX5043_POWCTRL1	=	0x4f08
                           004F5F  2305 _AX5043_MODCFGP	=	0x4f5f
                           004F10  2306 _AX5043_XTALOSC	=	0x4f10
                           004F11  2307 _AX5043_XTALAMPL	=	0x4f11
      0000EA                       2308 _axradio_syncstate::
      0000EA                       2309 	.ds 1
      0000EB                       2310 _axradio_txbuffer_len::
      0000EB                       2311 	.ds 2
      0000ED                       2312 _axradio_txbuffer_cnt::
      0000ED                       2313 	.ds 2
      0000EF                       2314 _axradio_curchannel::
      0000EF                       2315 	.ds 1
      0000F0                       2316 _axradio_curfreqoffset::
      0000F0                       2317 	.ds 4
      0000F4                       2318 _axradio_ack_count::
      0000F4                       2319 	.ds 1
      0000F5                       2320 _axradio_ack_seqnr::
      0000F5                       2321 	.ds 1
      0000F6                       2322 _axradio_sync_time::
      0000F6                       2323 	.ds 4
      0000FA                       2324 _axradio_sync_periodcorr::
      0000FA                       2325 	.ds 2
      0000FC                       2326 _axradio_timeanchor::
      0000FC                       2327 	.ds 8
      000104                       2328 _axradio_localaddr::
      000104                       2329 	.ds 8
      00010C                       2330 _axradio_default_remoteaddr::
      00010C                       2331 	.ds 4
      000110                       2332 _axradio_txbuffer::
      000110                       2333 	.ds 260
      000214                       2334 _axradio_rxbuffer::
      000214                       2335 	.ds 260
      000318                       2336 _axradio_cb_receive::
      000318                       2337 	.ds 34
      00033A                       2338 _axradio_cb_receivesfd::
      00033A                       2339 	.ds 10
      000344                       2340 _axradio_cb_channelstate::
      000344                       2341 	.ds 13
      000351                       2342 _axradio_cb_transmitstart::
      000351                       2343 	.ds 10
      00035B                       2344 _axradio_cb_transmitend::
      00035B                       2345 	.ds 10
      000365                       2346 _axradio_cb_transmitdata::
      000365                       2347 	.ds 10
      00036F                       2348 _axradio_timer::
      00036F                       2349 	.ds 8
      000377                       2350 _receive_isr_BeaconRx_65536_259:
      000377                       2351 	.ds 29
      000394                       2352 _axradio_init_x_196608_424:
      000394                       2353 	.ds 1
      000395                       2354 _axradio_init_j_196608_425:
      000395                       2355 	.ds 1
      000396                       2356 _axradio_init_x_458752_430:
      000396                       2357 	.ds 1
      000397                       2358 _axradio_set_mode_mode_65536_436:
      000397                       2359 	.ds 1
      000398                       2360 _axradio_set_channel_freq_65536_447:
      000398                       2361 	.ds 4
      00039C                       2362 _axradio_get_pllvcoi_x_131072_454:
      00039C                       2363 	.ds 1
      00039D                       2364 _axradio_set_curfreqoffset_offs_65536_457:
      00039D                       2365 	.ds 4
      0003A1                       2366 _axradio_set_local_address_addr_65536_459:
      0003A1                       2367 	.ds 3
      0003A4                       2368 _axradio_set_default_remote_address_addr_65536_461:
      0003A4                       2369 	.ds 3
      0003A7                       2370 _axradio_transmit_PARM_2:
      0003A7                       2371 	.ds 3
      0003AA                       2372 _axradio_transmit_PARM_3:
      0003AA                       2373 	.ds 2
      0003AC                       2374 _axradio_transmit_addr_65536_463:
      0003AC                       2375 	.ds 3
                                   2376 ;--------------------------------------------------------
                                   2377 ; absolute external ram data
                                   2378 ;--------------------------------------------------------
                                   2379 	.area XABS    (ABS,XDATA)
                                   2380 ;--------------------------------------------------------
                                   2381 ; external initialized ram data
                                   2382 ;--------------------------------------------------------
                                   2383 	.area XISEG   (XDATA)
      000A4E                       2384 _f30_saved::
      000A4E                       2385 	.ds 1
      000A4F                       2386 _f31_saved::
      000A4F                       2387 	.ds 1
      000A50                       2388 _f32_saved::
      000A50                       2389 	.ds 1
      000A51                       2390 _f33_saved::
      000A51                       2391 	.ds 1
                                   2392 	.area HOME    (CODE)
                                   2393 	.area GSINIT0 (CODE)
                                   2394 	.area GSINIT1 (CODE)
                                   2395 	.area GSINIT2 (CODE)
                                   2396 	.area GSINIT3 (CODE)
                                   2397 	.area GSINIT4 (CODE)
                                   2398 	.area GSINIT5 (CODE)
                                   2399 	.area GSINIT  (CODE)
                                   2400 	.area GSFINAL (CODE)
                                   2401 	.area CSEG    (CODE)
                                   2402 ;--------------------------------------------------------
                                   2403 ; global & static initialisations
                                   2404 ;--------------------------------------------------------
                                   2405 	.area HOME    (CODE)
                                   2406 	.area GSINIT  (CODE)
                                   2407 	.area GSFINAL (CODE)
                                   2408 	.area GSINIT  (CODE)
                                   2409 ;	..\src\COMMON\easyax5043.c:60: volatile uint8_t __data axradio_mode = AXRADIO_MODE_UNINIT;
      000391 75 17 00         [24] 2410 	mov	_axradio_mode,#0x00
                                   2411 ;	..\src\COMMON\easyax5043.c:61: volatile axradio_trxstate_t __data axradio_trxstate = trxstate_off;
      000394 75 18 00         [24] 2412 	mov	_axradio_trxstate,#0x00
                                   2413 ;--------------------------------------------------------
                                   2414 ; Home
                                   2415 ;--------------------------------------------------------
                                   2416 	.area HOME    (CODE)
                                   2417 	.area HOME    (CODE)
                                   2418 ;--------------------------------------------------------
                                   2419 ; code
                                   2420 ;--------------------------------------------------------
                                   2421 	.area CSEG    (CODE)
                                   2422 ;------------------------------------------------------------
                                   2423 ;Allocation info for local variables in function 'update_timeanchor'
                                   2424 ;------------------------------------------------------------
                                   2425 ;iesave                    Allocated to registers r7 
                                   2426 ;------------------------------------------------------------
                                   2427 ;	..\src\COMMON\easyax5043.c:239: static __reentrantb void update_timeanchor(void) __reentrant
                                   2428 ;	-----------------------------------------
                                   2429 ;	 function update_timeanchor
                                   2430 ;	-----------------------------------------
      001E70                       2431 _update_timeanchor:
                           000007  2432 	ar7 = 0x07
                           000006  2433 	ar6 = 0x06
                           000005  2434 	ar5 = 0x05
                           000004  2435 	ar4 = 0x04
                           000003  2436 	ar3 = 0x03
                           000002  2437 	ar2 = 0x02
                           000001  2438 	ar1 = 0x01
                           000000  2439 	ar0 = 0x00
                                   2440 ;	..\src\COMMON\easyax5043.c:241: uint8_t iesave = IE & 0x80;
      001E70 AF A8            [24] 2441 	mov	r7,_IE
      001E72 53 07 80         [24] 2442 	anl	ar7,#0x80
                                   2443 ;	..\src\COMMON\easyax5043.c:242: EA = 0;
                                   2444 ;	assignBit
      001E75 C2 AF            [12] 2445 	clr	_EA
                                   2446 ;	..\src\COMMON\easyax5043.c:243: axradio_timeanchor.timer0 = wtimer0_curtime();
      001E77 C0 07            [24] 2447 	push	ar7
      001E79 12 8D B0         [24] 2448 	lcall	_wtimer0_curtime
      001E7C AB 82            [24] 2449 	mov	r3,dpl
      001E7E AC 83            [24] 2450 	mov	r4,dph
      001E80 AD F0            [24] 2451 	mov	r5,b
      001E82 FE               [12] 2452 	mov	r6,a
      001E83 D0 07            [24] 2453 	pop	ar7
      001E85 90 00 FC         [24] 2454 	mov	dptr,#_axradio_timeanchor
      001E88 EB               [12] 2455 	mov	a,r3
      001E89 F0               [24] 2456 	movx	@dptr,a
      001E8A EC               [12] 2457 	mov	a,r4
      001E8B A3               [24] 2458 	inc	dptr
      001E8C F0               [24] 2459 	movx	@dptr,a
      001E8D ED               [12] 2460 	mov	a,r5
      001E8E A3               [24] 2461 	inc	dptr
      001E8F F0               [24] 2462 	movx	@dptr,a
      001E90 EE               [12] 2463 	mov	a,r6
      001E91 A3               [24] 2464 	inc	dptr
      001E92 F0               [24] 2465 	movx	@dptr,a
                                   2466 ;	..\src\COMMON\easyax5043.c:244: axradio_timeanchor.radiotimer = radio_read24((uint16_t)&AX5043_TIMER2);
      001E93 7D 59            [12] 2467 	mov	r5,#_AX5043_TIMER2
      001E95 7E 40            [12] 2468 	mov	r6,#(_AX5043_TIMER2 >> 8)
      001E97 8D 82            [24] 2469 	mov	dpl,r5
      001E99 8E 83            [24] 2470 	mov	dph,r6
      001E9B 12 79 BA         [24] 2471 	lcall	_radio_read24
      001E9E AB 82            [24] 2472 	mov	r3,dpl
      001EA0 AC 83            [24] 2473 	mov	r4,dph
      001EA2 AD F0            [24] 2474 	mov	r5,b
      001EA4 FE               [12] 2475 	mov	r6,a
      001EA5 90 01 00         [24] 2476 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      001EA8 EB               [12] 2477 	mov	a,r3
      001EA9 F0               [24] 2478 	movx	@dptr,a
      001EAA EC               [12] 2479 	mov	a,r4
      001EAB A3               [24] 2480 	inc	dptr
      001EAC F0               [24] 2481 	movx	@dptr,a
      001EAD ED               [12] 2482 	mov	a,r5
      001EAE A3               [24] 2483 	inc	dptr
      001EAF F0               [24] 2484 	movx	@dptr,a
      001EB0 EE               [12] 2485 	mov	a,r6
      001EB1 A3               [24] 2486 	inc	dptr
      001EB2 F0               [24] 2487 	movx	@dptr,a
                                   2488 ;	..\src\COMMON\easyax5043.c:245: IE |= iesave;
      001EB3 EF               [12] 2489 	mov	a,r7
      001EB4 42 A8            [12] 2490 	orl	_IE,a
                                   2491 ;	..\src\COMMON\easyax5043.c:246: }
      001EB6 22               [24] 2492 	ret
                                   2493 ;------------------------------------------------------------
                                   2494 ;Allocation info for local variables in function 'axradio_conv_time_totimer0'
                                   2495 ;------------------------------------------------------------
                                   2496 ;dt                        Allocated to registers r4 r5 r6 r7 
                                   2497 ;------------------------------------------------------------
                                   2498 ;	..\src\COMMON\easyax5043.c:248: __reentrantb uint32_t axradio_conv_time_totimer0(uint32_t dt) __reentrant
                                   2499 ;	-----------------------------------------
                                   2500 ;	 function axradio_conv_time_totimer0
                                   2501 ;	-----------------------------------------
      001EB7                       2502 _axradio_conv_time_totimer0:
      001EB7 AC 82            [24] 2503 	mov	r4,dpl
      001EB9 AD 83            [24] 2504 	mov	r5,dph
      001EBB AE F0            [24] 2505 	mov	r6,b
      001EBD FF               [12] 2506 	mov	r7,a
                                   2507 ;	..\src\COMMON\easyax5043.c:250: dt -= axradio_timeanchor.radiotimer;
      001EBE 90 01 00         [24] 2508 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      001EC1 E0               [24] 2509 	movx	a,@dptr
      001EC2 F8               [12] 2510 	mov	r0,a
      001EC3 A3               [24] 2511 	inc	dptr
      001EC4 E0               [24] 2512 	movx	a,@dptr
      001EC5 F9               [12] 2513 	mov	r1,a
      001EC6 A3               [24] 2514 	inc	dptr
      001EC7 E0               [24] 2515 	movx	a,@dptr
      001EC8 FA               [12] 2516 	mov	r2,a
      001EC9 A3               [24] 2517 	inc	dptr
      001ECA E0               [24] 2518 	movx	a,@dptr
      001ECB FB               [12] 2519 	mov	r3,a
      001ECC EC               [12] 2520 	mov	a,r4
      001ECD C3               [12] 2521 	clr	c
      001ECE 98               [12] 2522 	subb	a,r0
      001ECF F8               [12] 2523 	mov	r0,a
      001ED0 ED               [12] 2524 	mov	a,r5
      001ED1 99               [12] 2525 	subb	a,r1
      001ED2 F9               [12] 2526 	mov	r1,a
      001ED3 EE               [12] 2527 	mov	a,r6
      001ED4 9A               [12] 2528 	subb	a,r2
      001ED5 FA               [12] 2529 	mov	r2,a
      001ED6 EF               [12] 2530 	mov	a,r7
      001ED7 9B               [12] 2531 	subb	a,r3
                                   2532 ;	..\src\COMMON\easyax5043.c:251: dt = axradio_conv_timeinterval_totimer0(signextend24(dt));
      001ED8 88 82            [24] 2533 	mov	dpl,r0
      001EDA 89 83            [24] 2534 	mov	dph,r1
      001EDC 8A F0            [24] 2535 	mov	b,r2
      001EDE 12 8D AA         [24] 2536 	lcall	_signextend24
      001EE1 12 1A C5         [24] 2537 	lcall	_axradio_conv_timeinterval_totimer0
      001EE4 AC 82            [24] 2538 	mov	r4,dpl
      001EE6 AD 83            [24] 2539 	mov	r5,dph
      001EE8 AE F0            [24] 2540 	mov	r6,b
      001EEA FF               [12] 2541 	mov	r7,a
                                   2542 ;	..\src\COMMON\easyax5043.c:252: dt += axradio_timeanchor.timer0;
      001EEB 90 00 FC         [24] 2543 	mov	dptr,#_axradio_timeanchor
      001EEE E0               [24] 2544 	movx	a,@dptr
      001EEF F8               [12] 2545 	mov	r0,a
      001EF0 A3               [24] 2546 	inc	dptr
      001EF1 E0               [24] 2547 	movx	a,@dptr
      001EF2 F9               [12] 2548 	mov	r1,a
      001EF3 A3               [24] 2549 	inc	dptr
      001EF4 E0               [24] 2550 	movx	a,@dptr
      001EF5 FA               [12] 2551 	mov	r2,a
      001EF6 A3               [24] 2552 	inc	dptr
      001EF7 E0               [24] 2553 	movx	a,@dptr
      001EF8 FB               [12] 2554 	mov	r3,a
      001EF9 E8               [12] 2555 	mov	a,r0
      001EFA 2C               [12] 2556 	add	a,r4
      001EFB FC               [12] 2557 	mov	r4,a
      001EFC E9               [12] 2558 	mov	a,r1
      001EFD 3D               [12] 2559 	addc	a,r5
      001EFE FD               [12] 2560 	mov	r5,a
      001EFF EA               [12] 2561 	mov	a,r2
      001F00 3E               [12] 2562 	addc	a,r6
      001F01 FE               [12] 2563 	mov	r6,a
      001F02 EB               [12] 2564 	mov	a,r3
      001F03 3F               [12] 2565 	addc	a,r7
                                   2566 ;	..\src\COMMON\easyax5043.c:253: return dt;
      001F04 8C 82            [24] 2567 	mov	dpl,r4
      001F06 8D 83            [24] 2568 	mov	dph,r5
      001F08 8E F0            [24] 2569 	mov	b,r6
                                   2570 ;	..\src\COMMON\easyax5043.c:254: }
      001F0A 22               [24] 2571 	ret
                                   2572 ;------------------------------------------------------------
                                   2573 ;Allocation info for local variables in function 'ax5043_init_registers_common'
                                   2574 ;------------------------------------------------------------
                                   2575 ;rng                       Allocated to registers r7 
                                   2576 ;------------------------------------------------------------
                                   2577 ;	..\src\COMMON\easyax5043.c:256: static __reentrantb uint8_t ax5043_init_registers_common(void) __reentrant
                                   2578 ;	-----------------------------------------
                                   2579 ;	 function ax5043_init_registers_common
                                   2580 ;	-----------------------------------------
      001F0B                       2581 _ax5043_init_registers_common:
                                   2582 ;	..\src\COMMON\easyax5043.c:258: uint8_t rng = axradio_phy_chanpllrng[axradio_curchannel];
      001F0B 90 00 EF         [24] 2583 	mov	dptr,#_axradio_curchannel
      001F0E E0               [24] 2584 	movx	a,@dptr
      001F0F 24 E6            [12] 2585 	add	a,#_axradio_phy_chanpllrng
      001F11 F5 82            [12] 2586 	mov	dpl,a
      001F13 E4               [12] 2587 	clr	a
      001F14 34 00            [12] 2588 	addc	a,#(_axradio_phy_chanpllrng >> 8)
      001F16 F5 83            [12] 2589 	mov	dph,a
      001F18 E0               [24] 2590 	movx	a,@dptr
                                   2591 ;	..\src\COMMON\easyax5043.c:259: if (rng & 0x20)
      001F19 FF               [12] 2592 	mov	r7,a
      001F1A 30 E5 04         [24] 2593 	jnb	acc.5,00102$
                                   2594 ;	..\src\COMMON\easyax5043.c:260: return AXRADIO_ERR_RANGING;
      001F1D 75 82 06         [24] 2595 	mov	dpl,#0x06
      001F20 22               [24] 2596 	ret
      001F21                       2597 00102$:
                                   2598 ;	..\src\COMMON\easyax5043.c:261: if (AX5043_PLLLOOP & 0x80) {
      001F21 90 40 30         [24] 2599 	mov	dptr,#_AX5043_PLLLOOP
      001F24 E0               [24] 2600 	movx	a,@dptr
      001F25 30 E7 09         [24] 2601 	jnb	acc.7,00104$
                                   2602 ;	..\src\COMMON\easyax5043.c:262: AX5043_PLLRANGINGB = rng & 0x0F;
      001F28 90 40 3B         [24] 2603 	mov	dptr,#_AX5043_PLLRANGINGB
      001F2B 74 0F            [12] 2604 	mov	a,#0x0f
      001F2D 5F               [12] 2605 	anl	a,r7
      001F2E F0               [24] 2606 	movx	@dptr,a
      001F2F 80 07            [24] 2607 	sjmp	00105$
      001F31                       2608 00104$:
                                   2609 ;	..\src\COMMON\easyax5043.c:264: AX5043_PLLRANGINGA = rng & 0x0F;
      001F31 90 40 33         [24] 2610 	mov	dptr,#_AX5043_PLLRANGINGA
      001F34 74 0F            [12] 2611 	mov	a,#0x0f
      001F36 5F               [12] 2612 	anl	a,r7
      001F37 F0               [24] 2613 	movx	@dptr,a
      001F38                       2614 00105$:
                                   2615 ;	..\src\COMMON\easyax5043.c:266: rng = axradio_get_pllvcoi();
      001F38 12 48 7E         [24] 2616 	lcall	_axradio_get_pllvcoi
                                   2617 ;	..\src\COMMON\easyax5043.c:267: if (rng & 0x80)
      001F3B E5 82            [12] 2618 	mov	a,dpl
      001F3D FF               [12] 2619 	mov	r7,a
      001F3E 30 E7 05         [24] 2620 	jnb	acc.7,00107$
                                   2621 ;	..\src\COMMON\easyax5043.c:268: AX5043_PLLVCOI = rng;
      001F41 90 41 80         [24] 2622 	mov	dptr,#_AX5043_PLLVCOI
      001F44 EF               [12] 2623 	mov	a,r7
      001F45 F0               [24] 2624 	movx	@dptr,a
      001F46                       2625 00107$:
                                   2626 ;	..\src\COMMON\easyax5043.c:269: return AXRADIO_ERR_NOERROR;
      001F46 75 82 00         [24] 2627 	mov	dpl,#0x00
                                   2628 ;	..\src\COMMON\easyax5043.c:270: }
      001F49 22               [24] 2629 	ret
                                   2630 ;------------------------------------------------------------
                                   2631 ;Allocation info for local variables in function 'ax5043_init_registers_tx'
                                   2632 ;------------------------------------------------------------
                                   2633 ;	..\src\COMMON\easyax5043.c:272: __reentrantb uint8_t ax5043_init_registers_tx(void) __reentrant
                                   2634 ;	-----------------------------------------
                                   2635 ;	 function ax5043_init_registers_tx
                                   2636 ;	-----------------------------------------
      001F4A                       2637 _ax5043_init_registers_tx:
                                   2638 ;	..\src\COMMON\easyax5043.c:274: ax5043_set_registers_tx();
      001F4A 12 18 71         [24] 2639 	lcall	_ax5043_set_registers_tx
                                   2640 ;	..\src\COMMON\easyax5043.c:275: return ax5043_init_registers_common();
                                   2641 ;	..\src\COMMON\easyax5043.c:276: }
      001F4D 02 1F 0B         [24] 2642 	ljmp	_ax5043_init_registers_common
                                   2643 ;------------------------------------------------------------
                                   2644 ;Allocation info for local variables in function 'ax5043_init_registers_rx'
                                   2645 ;------------------------------------------------------------
                                   2646 ;	..\src\COMMON\easyax5043.c:278: __reentrantb uint8_t ax5043_init_registers_rx(void) __reentrant
                                   2647 ;	-----------------------------------------
                                   2648 ;	 function ax5043_init_registers_rx
                                   2649 ;	-----------------------------------------
      001F50                       2650 _ax5043_init_registers_rx:
                                   2651 ;	..\src\COMMON\easyax5043.c:280: ax5043_set_registers_rx();
      001F50 12 18 95         [24] 2652 	lcall	_ax5043_set_registers_rx
                                   2653 ;	..\src\COMMON\easyax5043.c:281: return ax5043_init_registers_common();
                                   2654 ;	..\src\COMMON\easyax5043.c:282: }
      001F53 02 1F 0B         [24] 2655 	ljmp	_ax5043_init_registers_common
                                   2656 ;------------------------------------------------------------
                                   2657 ;Allocation info for local variables in function 'receive_isr'
                                   2658 ;------------------------------------------------------------
                                   2659 ;fifo_cmd                  Allocated to registers r7 
                                   2660 ;flags                     Allocated to registers 
                                   2661 ;i                         Allocated to registers r7 
                                   2662 ;len                       Allocated to registers r7 
                                   2663 ;FreqOffset                Allocated to stack - _bp +5
                                   2664 ;r                         Allocated to registers r6 
                                   2665 ;r                         Allocated to registers r7 
                                   2666 ;r                         Allocated to registers r7 
                                   2667 ;BeaconRx                  Allocated with name '_receive_isr_BeaconRx_65536_259'
                                   2668 ;------------------------------------------------------------
                                   2669 ;	..\src\COMMON\easyax5043.c:284: static __reentrantb void receive_isr(void) __reentrant
                                   2670 ;	-----------------------------------------
                                   2671 ;	 function receive_isr
                                   2672 ;	-----------------------------------------
      001F56                       2673 _receive_isr:
                                   2674 ;	..\src\COMMON\easyax5043.c:288: uint8_t len = AX5043_RADIOEVENTREQ0; // clear request so interrupt does not fire again. sync_rx enables interrupt on radio state changed in order to wake up on SDF detected
      001F56 90 40 0F         [24] 2675 	mov	dptr,#_AX5043_RADIOEVENTREQ0
      001F59 E0               [24] 2676 	movx	a,@dptr
      001F5A FF               [12] 2677 	mov	r7,a
                                   2678 ;	..\src\COMMON\easyax5043.c:301: dbglink_writestr(" \n Receive isr ");
      001F5B 90 91 BF         [24] 2679 	mov	dptr,#___str_0
      001F5E 75 F0 80         [24] 2680 	mov	b,#0x80
      001F61 C0 07            [24] 2681 	push	ar7
      001F63 12 82 8A         [24] 2682 	lcall	_dbglink_writestr
      001F66 D0 07            [24] 2683 	pop	ar7
                                   2684 ;	..\src\COMMON\easyax5043.c:303: if ((len & 0x04) && AX5043_RADIOSTATE == 0x0F) {
      001F68 EF               [12] 2685 	mov	a,r7
      001F69 30 E2 3F         [24] 2686 	jnb	acc.2,00181$
      001F6C 90 40 1C         [24] 2687 	mov	dptr,#_AX5043_RADIOSTATE
      001F6F E0               [24] 2688 	movx	a,@dptr
      001F70 FF               [12] 2689 	mov	r7,a
      001F71 BF 0F 37         [24] 2690 	cjne	r7,#0x0f,00181$
                                   2691 ;	..\src\COMMON\easyax5043.c:305: update_timeanchor();
      001F74 12 1E 70         [24] 2692 	lcall	_update_timeanchor
                                   2693 ;	..\src\COMMON\easyax5043.c:306: if(axradio_framing_enable_sfdcallback)
      001F77 90 90 EF         [24] 2694 	mov	dptr,#_axradio_framing_enable_sfdcallback
      001F7A E4               [12] 2695 	clr	a
      001F7B 93               [24] 2696 	movc	a,@a+dptr
      001F7C 60 2D            [24] 2697 	jz	00181$
                                   2698 ;	..\src\COMMON\easyax5043.c:308: wtimer_remove_callback(&axradio_cb_receivesfd.cb);
      001F7E 90 03 3A         [24] 2699 	mov	dptr,#_axradio_cb_receivesfd
      001F81 12 87 AB         [24] 2700 	lcall	_wtimer_remove_callback
                                   2701 ;	..\src\COMMON\easyax5043.c:309: axradio_cb_receivesfd.st.error = AXRADIO_ERR_NOERROR;
      001F84 90 03 3F         [24] 2702 	mov	dptr,#(_axradio_cb_receivesfd + 0x0005)
      001F87 E4               [12] 2703 	clr	a
      001F88 F0               [24] 2704 	movx	@dptr,a
                                   2705 ;	..\src\COMMON\easyax5043.c:310: axradio_cb_receivesfd.st.time.t = axradio_timeanchor.radiotimer;
      001F89 90 01 00         [24] 2706 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      001F8C E0               [24] 2707 	movx	a,@dptr
      001F8D FC               [12] 2708 	mov	r4,a
      001F8E A3               [24] 2709 	inc	dptr
      001F8F E0               [24] 2710 	movx	a,@dptr
      001F90 FD               [12] 2711 	mov	r5,a
      001F91 A3               [24] 2712 	inc	dptr
      001F92 E0               [24] 2713 	movx	a,@dptr
      001F93 FE               [12] 2714 	mov	r6,a
      001F94 A3               [24] 2715 	inc	dptr
      001F95 E0               [24] 2716 	movx	a,@dptr
      001F96 FF               [12] 2717 	mov	r7,a
      001F97 90 03 40         [24] 2718 	mov	dptr,#(_axradio_cb_receivesfd + 0x0006)
      001F9A EC               [12] 2719 	mov	a,r4
      001F9B F0               [24] 2720 	movx	@dptr,a
      001F9C ED               [12] 2721 	mov	a,r5
      001F9D A3               [24] 2722 	inc	dptr
      001F9E F0               [24] 2723 	movx	@dptr,a
      001F9F EE               [12] 2724 	mov	a,r6
      001FA0 A3               [24] 2725 	inc	dptr
      001FA1 F0               [24] 2726 	movx	@dptr,a
      001FA2 EF               [12] 2727 	mov	a,r7
      001FA3 A3               [24] 2728 	inc	dptr
      001FA4 F0               [24] 2729 	movx	@dptr,a
                                   2730 ;	..\src\COMMON\easyax5043.c:311: wtimer_add_callback(&axradio_cb_receivesfd.cb);
      001FA5 90 03 3A         [24] 2731 	mov	dptr,#_axradio_cb_receivesfd
      001FA8 12 78 5E         [24] 2732 	lcall	_wtimer_add_callback
                                   2733 ;	..\src\COMMON\easyax5043.c:323: while (AX5043_IRQREQUEST0 & 0x01) {    // while fifo not empty
      001FAB                       2734 00181$:
      001FAB                       2735 00165$:
      001FAB 90 40 0D         [24] 2736 	mov	dptr,#_AX5043_IRQREQUEST0
      001FAE E0               [24] 2737 	movx	a,@dptr
      001FAF 20 E0 01         [24] 2738 	jb	acc.0,00350$
      001FB2 22               [24] 2739 	ret
      001FB3                       2740 00350$:
                                   2741 ;	..\src\COMMON\easyax5043.c:325: fifo_cmd = AX5043_FIFODATA; // read command
      001FB3 90 40 29         [24] 2742 	mov	dptr,#_AX5043_FIFODATA
      001FB6 E0               [24] 2743 	movx	a,@dptr
                                   2744 ;	..\src\COMMON\easyax5043.c:326: len = (fifo_cmd & 0xE0) >> 5; // top 3 bits encode payload len
      001FB7 FF               [12] 2745 	mov	r7,a
      001FB8 FD               [12] 2746 	mov	r5,a
      001FB9 53 05 E0         [24] 2747 	anl	ar5,#0xe0
      001FBC E4               [12] 2748 	clr	a
      001FBD 03               [12] 2749 	rr	a
      001FBE CD               [12] 2750 	xch	a,r5
      001FBF C4               [12] 2751 	swap	a
      001FC0 03               [12] 2752 	rr	a
      001FC1 54 07            [12] 2753 	anl	a,#0x07
      001FC3 6D               [12] 2754 	xrl	a,r5
      001FC4 CD               [12] 2755 	xch	a,r5
      001FC5 54 07            [12] 2756 	anl	a,#0x07
      001FC7 CD               [12] 2757 	xch	a,r5
      001FC8 6D               [12] 2758 	xrl	a,r5
      001FC9 CD               [12] 2759 	xch	a,r5
      001FCA 30 E2 02         [24] 2760 	jnb	acc.2,00351$
      001FCD 44 F8            [12] 2761 	orl	a,#0xf8
      001FCF                       2762 00351$:
      001FCF FE               [12] 2763 	mov	r6,a
                                   2764 ;	..\src\COMMON\easyax5043.c:327: if (len == 7)
      001FD0 BD 07 05         [24] 2765 	cjne	r5,#0x07,00107$
                                   2766 ;	..\src\COMMON\easyax5043.c:328: len = AX5043_FIFODATA; // 7 means variable length, -> get length byte
      001FD3 90 40 29         [24] 2767 	mov	dptr,#_AX5043_FIFODATA
      001FD6 E0               [24] 2768 	movx	a,@dptr
      001FD7 FD               [12] 2769 	mov	r5,a
      001FD8                       2770 00107$:
                                   2771 ;	..\src\COMMON\easyax5043.c:329: fifo_cmd &= 0x1F;
      001FD8 53 07 1F         [24] 2772 	anl	ar7,#0x1f
                                   2773 ;	..\src\COMMON\easyax5043.c:330: switch (fifo_cmd) {
      001FDB BF 01 02         [24] 2774 	cjne	r7,#0x01,00354$
      001FDE 80 21            [24] 2775 	sjmp	00108$
      001FE0                       2776 00354$:
      001FE0 BF 10 03         [24] 2777 	cjne	r7,#0x10,00355$
      001FE3 02 23 92         [24] 2778 	ljmp	00151$
      001FE6                       2779 00355$:
      001FE6 BF 11 03         [24] 2780 	cjne	r7,#0x11,00356$
      001FE9 02 23 65         [24] 2781 	ljmp	00148$
      001FEC                       2782 00356$:
      001FEC BF 12 03         [24] 2783 	cjne	r7,#0x12,00357$
      001FEF 02 23 15         [24] 2784 	ljmp	00144$
      001FF2                       2785 00357$:
      001FF2 BF 13 03         [24] 2786 	cjne	r7,#0x13,00358$
      001FF5 02 22 CC         [24] 2787 	ljmp	00140$
      001FF8                       2788 00358$:
      001FF8 BF 15 03         [24] 2789 	cjne	r7,#0x15,00359$
      001FFB 02 23 BB         [24] 2790 	ljmp	00154$
      001FFE                       2791 00359$:
      001FFE 02 24 33         [24] 2792 	ljmp	00158$
                                   2793 ;	..\src\COMMON\easyax5043.c:331: case AX5043_FIFOCMD_DATA:
      002001                       2794 00108$:
                                   2795 ;	..\src\COMMON\easyax5043.c:333: if (!len)
      002001 ED               [12] 2796 	mov	a,r5
      002002 60 A7            [24] 2797 	jz	00165$
                                   2798 ;	..\src\COMMON\easyax5043.c:336: flags = AX5043_FIFODATA;
      002004 90 40 29         [24] 2799 	mov	dptr,#_AX5043_FIFODATA
      002007 E0               [24] 2800 	movx	a,@dptr
                                   2801 ;	..\src\COMMON\easyax5043.c:337: --len;
      002008 ED               [12] 2802 	mov	a,r5
      002009 14               [12] 2803 	dec	a
      00200A FF               [12] 2804 	mov	r7,a
                                   2805 ;	..\src\COMMON\easyax5043.c:338: ax5043_readfifo(axradio_rxbuffer, len);
      00200B C0 07            [24] 2806 	push	ar7
      00200D C0 07            [24] 2807 	push	ar7
      00200F 90 02 14         [24] 2808 	mov	dptr,#_axradio_rxbuffer
      002012 75 F0 00         [24] 2809 	mov	b,#0x00
      002015 12 84 E5         [24] 2810 	lcall	_ax5043_readfifo
      002018 15 81            [12] 2811 	dec	sp
      00201A D0 07            [24] 2812 	pop	ar7
                                   2813 ;	..\src\COMMON\easyax5043.c:339: if(axradio_mode == AXRADIO_MODE_WOR_RECEIVE || axradio_mode == AXRADIO_MODE_WOR_ACK_RECEIVE)
      00201C 74 21            [12] 2814 	mov	a,#0x21
      00201E B5 17 02         [24] 2815 	cjne	a,_axradio_mode,00361$
      002021 80 05            [24] 2816 	sjmp	00111$
      002023                       2817 00361$:
      002023 74 23            [12] 2818 	mov	a,#0x23
      002025 B5 17 20         [24] 2819 	cjne	a,_axradio_mode,00112$
      002028                       2820 00111$:
                                   2821 ;	..\src\COMMON\easyax5043.c:341: f30_saved = AX5043_0xF30;
      002028 90 4F 30         [24] 2822 	mov	dptr,#_AX5043_0xF30
      00202B E0               [24] 2823 	movx	a,@dptr
      00202C 90 0A 4E         [24] 2824 	mov	dptr,#_f30_saved
      00202F F0               [24] 2825 	movx	@dptr,a
                                   2826 ;	..\src\COMMON\easyax5043.c:342: f31_saved = AX5043_0xF31;
      002030 90 4F 31         [24] 2827 	mov	dptr,#_AX5043_0xF31
      002033 E0               [24] 2828 	movx	a,@dptr
      002034 90 0A 4F         [24] 2829 	mov	dptr,#_f31_saved
      002037 F0               [24] 2830 	movx	@dptr,a
                                   2831 ;	..\src\COMMON\easyax5043.c:343: f32_saved = AX5043_0xF32;
      002038 90 4F 32         [24] 2832 	mov	dptr,#_AX5043_0xF32
      00203B E0               [24] 2833 	movx	a,@dptr
      00203C 90 0A 50         [24] 2834 	mov	dptr,#_f32_saved
      00203F F0               [24] 2835 	movx	@dptr,a
                                   2836 ;	..\src\COMMON\easyax5043.c:344: f33_saved = AX5043_0xF33;
      002040 90 4F 33         [24] 2837 	mov	dptr,#_AX5043_0xF33
      002043 E0               [24] 2838 	movx	a,@dptr
      002044 90 0A 51         [24] 2839 	mov	dptr,#_f33_saved
      002047 F0               [24] 2840 	movx	@dptr,a
      002048                       2841 00112$:
                                   2842 ;	..\src\COMMON\easyax5043.c:346: if (axradio_mode == AXRADIO_MODE_WOR_RECEIVE ||
      002048 74 21            [12] 2843 	mov	a,#0x21
      00204A B5 17 02         [24] 2844 	cjne	a,_axradio_mode,00364$
      00204D 80 05            [24] 2845 	sjmp	00114$
      00204F                       2846 00364$:
                                   2847 ;	..\src\COMMON\easyax5043.c:347: axradio_mode == AXRADIO_MODE_SYNC_SLAVE)
      00204F 74 32            [12] 2848 	mov	a,#0x32
      002051 B5 17 05         [24] 2849 	cjne	a,_axradio_mode,00115$
      002054                       2850 00114$:
                                   2851 ;	..\src\COMMON\easyax5043.c:348: AX5043_PWRMODE = AX5043_PWRSTATE_POWERDOWN;
      002054 90 40 02         [24] 2852 	mov	dptr,#_AX5043_PWRMODE
      002057 E4               [12] 2853 	clr	a
      002058 F0               [24] 2854 	movx	@dptr,a
      002059                       2855 00115$:
                                   2856 ;	..\src\COMMON\easyax5043.c:349: AX5043_IRQMASK0 &= (uint8_t)~0x01; // disable FIFO not empty irq
      002059 90 40 07         [24] 2857 	mov	dptr,#_AX5043_IRQMASK0
      00205C E0               [24] 2858 	movx	a,@dptr
      00205D 53 E0 FE         [24] 2859 	anl	acc,#0xfe
      002060 F0               [24] 2860 	movx	@dptr,a
                                   2861 ;	..\src\COMMON\easyax5043.c:350: wtimer_remove_callback(&axradio_cb_receive.cb);
      002061 90 03 18         [24] 2862 	mov	dptr,#_axradio_cb_receive
      002064 C0 07            [24] 2863 	push	ar7
      002066 12 87 AB         [24] 2864 	lcall	_wtimer_remove_callback
      002069 D0 07            [24] 2865 	pop	ar7
                                   2866 ;	..\src\COMMON\easyax5043.c:351: axradio_cb_receive.st.error = AXRADIO_ERR_NOERROR;
      00206B 90 03 1D         [24] 2867 	mov	dptr,#(_axradio_cb_receive + 0x0005)
      00206E E4               [12] 2868 	clr	a
      00206F F0               [24] 2869 	movx	@dptr,a
                                   2870 ;	..\src\COMMON\easyax5043.c:353: axradio_cb_receive.st.rx.mac.raw = axradio_rxbuffer;
      002070 90 03 34         [24] 2871 	mov	dptr,#(_axradio_cb_receive + 0x001c)
      002073 74 14            [12] 2872 	mov	a,#_axradio_rxbuffer
      002075 F0               [24] 2873 	movx	@dptr,a
      002076 74 02            [12] 2874 	mov	a,#(_axradio_rxbuffer >> 8)
      002078 A3               [24] 2875 	inc	dptr
      002079 F0               [24] 2876 	movx	@dptr,a
                                   2877 ;	..\src\COMMON\easyax5043.c:354: if (AXRADIO_MODE_IS_STREAM_RECEIVE(axradio_mode)) {
      00207A AC 17            [24] 2878 	mov	r4,_axradio_mode
      00207C 53 04 F8         [24] 2879 	anl	ar4,#0xf8
      00207F 7E 00            [12] 2880 	mov	r6,#0x00
      002081 BC 28 05         [24] 2881 	cjne	r4,#0x28,00367$
      002084 BE 00 02         [24] 2882 	cjne	r6,#0x00,00367$
      002087 80 03            [24] 2883 	sjmp	00368$
      002089                       2884 00367$:
      002089 02 21 1F         [24] 2885 	ljmp	00121$
      00208C                       2886 00368$:
                                   2887 ;	..\src\COMMON\easyax5043.c:355: axradio_cb_receive.st.rx.pktdata = axradio_rxbuffer;
      00208C 90 03 36         [24] 2888 	mov	dptr,#(_axradio_cb_receive + 0x001e)
      00208F 74 14            [12] 2889 	mov	a,#_axradio_rxbuffer
      002091 F0               [24] 2890 	movx	@dptr,a
      002092 74 02            [12] 2891 	mov	a,#(_axradio_rxbuffer >> 8)
      002094 A3               [24] 2892 	inc	dptr
      002095 F0               [24] 2893 	movx	@dptr,a
                                   2894 ;	..\src\COMMON\easyax5043.c:356: axradio_cb_receive.st.rx.pktlen = len;
      002096 8F 04            [24] 2895 	mov	ar4,r7
      002098 7E 00            [12] 2896 	mov	r6,#0x00
      00209A 90 03 38         [24] 2897 	mov	dptr,#(_axradio_cb_receive + 0x0020)
      00209D EC               [12] 2898 	mov	a,r4
      00209E F0               [24] 2899 	movx	@dptr,a
      00209F EE               [12] 2900 	mov	a,r6
      0020A0 A3               [24] 2901 	inc	dptr
      0020A1 F0               [24] 2902 	movx	@dptr,a
                                   2903 ;	..\src\COMMON\easyax5043.c:358: int8_t r = AX5043_RSSI;
      0020A2 90 40 40         [24] 2904 	mov	dptr,#_AX5043_RSSI
      0020A5 E0               [24] 2905 	movx	a,@dptr
                                   2906 ;	..\src\COMMON\easyax5043.c:359: axradio_cb_receive.st.rx.phy.rssi = r - (int16_t)axradio_phy_rssioffset;
      0020A6 FE               [12] 2907 	mov	r6,a
      0020A7 33               [12] 2908 	rlc	a
      0020A8 95 E0            [12] 2909 	subb	a,acc
      0020AA FC               [12] 2910 	mov	r4,a
      0020AB 90 90 CD         [24] 2911 	mov	dptr,#_axradio_phy_rssioffset
      0020AE E4               [12] 2912 	clr	a
      0020AF 93               [24] 2913 	movc	a,@a+dptr
      0020B0 FB               [12] 2914 	mov	r3,a
      0020B1 33               [12] 2915 	rlc	a
      0020B2 95 E0            [12] 2916 	subb	a,acc
      0020B4 FA               [12] 2917 	mov	r2,a
      0020B5 EE               [12] 2918 	mov	a,r6
      0020B6 C3               [12] 2919 	clr	c
      0020B7 9B               [12] 2920 	subb	a,r3
      0020B8 FE               [12] 2921 	mov	r6,a
      0020B9 EC               [12] 2922 	mov	a,r4
      0020BA 9A               [12] 2923 	subb	a,r2
      0020BB FC               [12] 2924 	mov	r4,a
      0020BC 90 03 22         [24] 2925 	mov	dptr,#(_axradio_cb_receive + 0x000a)
      0020BF EE               [12] 2926 	mov	a,r6
      0020C0 F0               [24] 2927 	movx	@dptr,a
      0020C1 EC               [12] 2928 	mov	a,r4
      0020C2 A3               [24] 2929 	inc	dptr
      0020C3 F0               [24] 2930 	movx	@dptr,a
                                   2931 ;	..\src\COMMON\easyax5043.c:361: if (axradio_phy_innerfreqloop)
      0020C4 90 90 BF         [24] 2932 	mov	dptr,#_axradio_phy_innerfreqloop
      0020C7 E4               [12] 2933 	clr	a
      0020C8 93               [24] 2934 	movc	a,@a+dptr
      0020C9 60 28            [24] 2935 	jz	00118$
                                   2936 ;	..\src\COMMON\easyax5043.c:363: axradio_cb_receive.st.rx.phy.offset.o = axradio_conv_freq_fromreg(signextend16(radio_read16((uint16_t)&AX5043_TRKFREQ1)));
      0020CB 7C 50            [12] 2937 	mov	r4,#_AX5043_TRKFREQ1
      0020CD 7E 40            [12] 2938 	mov	r6,#(_AX5043_TRKFREQ1 >> 8)
      0020CF 8C 82            [24] 2939 	mov	dpl,r4
      0020D1 8E 83            [24] 2940 	mov	dph,r6
      0020D3 12 7A F3         [24] 2941 	lcall	_radio_read16
      0020D6 12 8D D7         [24] 2942 	lcall	_signextend16
      0020D9 12 1A 73         [24] 2943 	lcall	_axradio_conv_freq_fromreg
      0020DC AA 82            [24] 2944 	mov	r2,dpl
      0020DE AB 83            [24] 2945 	mov	r3,dph
      0020E0 AC F0            [24] 2946 	mov	r4,b
      0020E2 FE               [12] 2947 	mov	r6,a
      0020E3 90 03 24         [24] 2948 	mov	dptr,#(_axradio_cb_receive + 0x000c)
      0020E6 EA               [12] 2949 	mov	a,r2
      0020E7 F0               [24] 2950 	movx	@dptr,a
      0020E8 EB               [12] 2951 	mov	a,r3
      0020E9 A3               [24] 2952 	inc	dptr
      0020EA F0               [24] 2953 	movx	@dptr,a
      0020EB EC               [12] 2954 	mov	a,r4
      0020EC A3               [24] 2955 	inc	dptr
      0020ED F0               [24] 2956 	movx	@dptr,a
      0020EE EE               [12] 2957 	mov	a,r6
      0020EF A3               [24] 2958 	inc	dptr
      0020F0 F0               [24] 2959 	movx	@dptr,a
      0020F1 80 23            [24] 2960 	sjmp	00119$
      0020F3                       2961 00118$:
                                   2962 ;	..\src\COMMON\easyax5043.c:368: axradio_cb_receive.st.rx.phy.offset.o = signextend20(radio_read24((uint16_t)&AX5043_TRKRFFREQ2));
      0020F3 7C 4D            [12] 2963 	mov	r4,#_AX5043_TRKRFFREQ2
      0020F5 7E 40            [12] 2964 	mov	r6,#(_AX5043_TRKRFFREQ2 >> 8)
      0020F7 8C 82            [24] 2965 	mov	dpl,r4
      0020F9 8E 83            [24] 2966 	mov	dph,r6
      0020FB 12 79 BA         [24] 2967 	lcall	_radio_read24
      0020FE 12 8D 0C         [24] 2968 	lcall	_signextend20
      002101 AA 82            [24] 2969 	mov	r2,dpl
      002103 AB 83            [24] 2970 	mov	r3,dph
      002105 AC F0            [24] 2971 	mov	r4,b
      002107 FE               [12] 2972 	mov	r6,a
      002108 90 03 24         [24] 2973 	mov	dptr,#(_axradio_cb_receive + 0x000c)
      00210B EA               [12] 2974 	mov	a,r2
      00210C F0               [24] 2975 	movx	@dptr,a
      00210D EB               [12] 2976 	mov	a,r3
      00210E A3               [24] 2977 	inc	dptr
      00210F F0               [24] 2978 	movx	@dptr,a
      002110 EC               [12] 2979 	mov	a,r4
      002111 A3               [24] 2980 	inc	dptr
      002112 F0               [24] 2981 	movx	@dptr,a
      002113 EE               [12] 2982 	mov	a,r6
      002114 A3               [24] 2983 	inc	dptr
      002115 F0               [24] 2984 	movx	@dptr,a
      002116                       2985 00119$:
                                   2986 ;	..\src\COMMON\easyax5043.c:370: wtimer_add_callback(&axradio_cb_receive.cb);
      002116 90 03 18         [24] 2987 	mov	dptr,#_axradio_cb_receive
      002119 12 78 5E         [24] 2988 	lcall	_wtimer_add_callback
                                   2989 ;	..\src\COMMON\easyax5043.c:371: break;
      00211C 02 1F AB         [24] 2990 	ljmp	00165$
      00211F                       2991 00121$:
                                   2992 ;	..\src\COMMON\easyax5043.c:373: axradio_cb_receive.st.rx.pktdata = &axradio_rxbuffer[axradio_framing_maclen];
      00211F 90 90 E1         [24] 2993 	mov	dptr,#_axradio_framing_maclen
      002122 E4               [12] 2994 	clr	a
      002123 93               [24] 2995 	movc	a,@a+dptr
      002124 FE               [12] 2996 	mov	r6,a
      002125 24 14            [12] 2997 	add	a,#_axradio_rxbuffer
      002127 FB               [12] 2998 	mov	r3,a
      002128 E4               [12] 2999 	clr	a
      002129 34 02            [12] 3000 	addc	a,#(_axradio_rxbuffer >> 8)
      00212B FC               [12] 3001 	mov	r4,a
      00212C 90 03 36         [24] 3002 	mov	dptr,#(_axradio_cb_receive + 0x001e)
      00212F EB               [12] 3003 	mov	a,r3
      002130 F0               [24] 3004 	movx	@dptr,a
      002131 EC               [12] 3005 	mov	a,r4
      002132 A3               [24] 3006 	inc	dptr
      002133 F0               [24] 3007 	movx	@dptr,a
                                   3008 ;	..\src\COMMON\easyax5043.c:374: if (len < axradio_framing_maclen) {
      002134 C3               [12] 3009 	clr	c
      002135 EF               [12] 3010 	mov	a,r7
      002136 9E               [12] 3011 	subb	a,r6
      002137 50 0B            [24] 3012 	jnc	00126$
                                   3013 ;	..\src\COMMON\easyax5043.c:375: len = 0;
      002139 7F 00            [12] 3014 	mov	r7,#0x00
                                   3015 ;	..\src\COMMON\easyax5043.c:376: axradio_cb_receive.st.rx.pktlen = 0;
      00213B 90 03 38         [24] 3016 	mov	dptr,#(_axradio_cb_receive + 0x0020)
      00213E E4               [12] 3017 	clr	a
      00213F F0               [24] 3018 	movx	@dptr,a
      002140 A3               [24] 3019 	inc	dptr
      002141 F0               [24] 3020 	movx	@dptr,a
      002142 80 2F            [24] 3021 	sjmp	00127$
      002144                       3022 00126$:
                                   3023 ;	..\src\COMMON\easyax5043.c:378: len -= axradio_framing_maclen;
      002144 EF               [12] 3024 	mov	a,r7
      002145 C3               [12] 3025 	clr	c
      002146 9E               [12] 3026 	subb	a,r6
                                   3027 ;	..\src\COMMON\easyax5043.c:379: axradio_cb_receive.st.rx.pktlen = len;
      002147 FF               [12] 3028 	mov	r7,a
      002148 FC               [12] 3029 	mov	r4,a
      002149 7E 00            [12] 3030 	mov	r6,#0x00
      00214B 90 03 38         [24] 3031 	mov	dptr,#(_axradio_cb_receive + 0x0020)
      00214E EC               [12] 3032 	mov	a,r4
      00214F F0               [24] 3033 	movx	@dptr,a
      002150 EE               [12] 3034 	mov	a,r6
      002151 A3               [24] 3035 	inc	dptr
      002152 F0               [24] 3036 	movx	@dptr,a
                                   3037 ;	..\src\COMMON\easyax5043.c:380: wtimer_add_callback(&axradio_cb_receive.cb);
      002153 90 03 18         [24] 3038 	mov	dptr,#_axradio_cb_receive
      002156 C0 07            [24] 3039 	push	ar7
      002158 12 78 5E         [24] 3040 	lcall	_wtimer_add_callback
      00215B D0 07            [24] 3041 	pop	ar7
                                   3042 ;	..\src\COMMON\easyax5043.c:381: if (axradio_mode == AXRADIO_MODE_SYNC_SLAVE ||
      00215D 74 32            [12] 3043 	mov	a,#0x32
      00215F B5 17 02         [24] 3044 	cjne	a,_axradio_mode,00371$
      002162 80 05            [24] 3045 	sjmp	00122$
      002164                       3046 00371$:
                                   3047 ;	..\src\COMMON\easyax5043.c:382: axradio_mode == AXRADIO_MODE_SYNC_ACK_SLAVE)
      002164 74 33            [12] 3048 	mov	a,#0x33
      002166 B5 17 0A         [24] 3049 	cjne	a,_axradio_mode,00127$
      002169                       3050 00122$:
                                   3051 ;	..\src\COMMON\easyax5043.c:383: wtimer_remove(&axradio_timer);
      002169 90 03 6F         [24] 3052 	mov	dptr,#_axradio_timer
      00216C C0 07            [24] 3053 	push	ar7
      00216E 12 85 39         [24] 3054 	lcall	_wtimer_remove
      002171 D0 07            [24] 3055 	pop	ar7
      002173                       3056 00127$:
                                   3057 ;	..\src\COMMON\easyax5043.c:386: BEACON_decoding(axradio_rxbuffer+3,&BeaconRx,len);
      002173 90 03 AF         [24] 3058 	mov	dptr,#_BEACON_decoding_PARM_2
      002176 74 77            [12] 3059 	mov	a,#_receive_isr_BeaconRx_65536_259
      002178 F0               [24] 3060 	movx	@dptr,a
      002179 74 03            [12] 3061 	mov	a,#(_receive_isr_BeaconRx_65536_259 >> 8)
      00217B A3               [24] 3062 	inc	dptr
      00217C F0               [24] 3063 	movx	@dptr,a
      00217D 90 03 B1         [24] 3064 	mov	dptr,#_BEACON_decoding_PARM_3
      002180 EF               [12] 3065 	mov	a,r7
      002181 F0               [24] 3066 	movx	@dptr,a
      002182 90 02 17         [24] 3067 	mov	dptr,#(_axradio_rxbuffer + 0x0003)
      002185 75 F0 00         [24] 3068 	mov	b,#0x00
      002188 C0 07            [24] 3069 	push	ar7
      00218A 12 4D D6         [24] 3070 	lcall	_BEACON_decoding
      00218D D0 07            [24] 3071 	pop	ar7
                                   3072 ;	..\src\COMMON\easyax5043.c:388: if(DownLinkIsrFlag)
      00218F 90 00 03         [24] 3073 	mov	dptr,#_DownLinkIsrFlag
      002192 E0               [24] 3074 	movx	a,@dptr
      002193 F5 F0            [12] 3075 	mov	b,a
      002195 A3               [24] 3076 	inc	dptr
      002196 E0               [24] 3077 	movx	a,@dptr
      002197 45 F0            [12] 3078 	orl	a,b
      002199 60 53            [24] 3079 	jz	00132$
                                   3080 ;	..\src\COMMON\easyax5043.c:391: DownLinkIsrFlag = 0;
      00219B 90 00 03         [24] 3081 	mov	dptr,#_DownLinkIsrFlag
      00219E E4               [12] 3082 	clr	a
      00219F F0               [24] 3083 	movx	@dptr,a
      0021A0 A3               [24] 3084 	inc	dptr
      0021A1 F0               [24] 3085 	movx	@dptr,a
                                   3086 ;	..\src\COMMON\easyax5043.c:392: ACKReceivedFlag = VerifyACK(axradio_rxbuffer+3,len,NULL);
      0021A2 90 03 B5         [24] 3087 	mov	dptr,#_VerifyACK_PARM_2
      0021A5 EF               [12] 3088 	mov	a,r7
      0021A6 F0               [24] 3089 	movx	@dptr,a
      0021A7 90 03 B6         [24] 3090 	mov	dptr,#_VerifyACK_PARM_3
      0021AA E4               [12] 3091 	clr	a
      0021AB F0               [24] 3092 	movx	@dptr,a
      0021AC A3               [24] 3093 	inc	dptr
      0021AD F0               [24] 3094 	movx	@dptr,a
      0021AE A3               [24] 3095 	inc	dptr
      0021AF F0               [24] 3096 	movx	@dptr,a
      0021B0 90 02 17         [24] 3097 	mov	dptr,#(_axradio_rxbuffer + 0x0003)
      0021B3 75 F0 00         [24] 3098 	mov	b,#0x00
      0021B6 12 53 A2         [24] 3099 	lcall	_VerifyACK
      0021B9 AF 82            [24] 3100 	mov	r7,dpl
      0021BB 7E 00            [12] 3101 	mov	r6,#0x00
      0021BD 90 00 05         [24] 3102 	mov	dptr,#_ACKReceivedFlag
      0021C0 EF               [12] 3103 	mov	a,r7
      0021C1 F0               [24] 3104 	movx	@dptr,a
      0021C2 EE               [12] 3105 	mov	a,r6
      0021C3 A3               [24] 3106 	inc	dptr
      0021C4 F0               [24] 3107 	movx	@dptr,a
                                   3108 ;	..\src\COMMON\easyax5043.c:393: if(!ACKReceivedFlag && RfStateMachine == e_WaitForACK) BackOffCounter++;
      0021C5 EF               [12] 3109 	mov	a,r7
      0021C6 4E               [12] 3110 	orl	a,r6
      0021C7 60 03            [24] 3111 	jz	00375$
      0021C9 02 1F AB         [24] 3112 	ljmp	00165$
      0021CC                       3113 00375$:
      0021CC 90 0A 2D         [24] 3114 	mov	dptr,#_RfStateMachine
      0021CF E0               [24] 3115 	movx	a,@dptr
      0021D0 FE               [12] 3116 	mov	r6,a
      0021D1 A3               [24] 3117 	inc	dptr
      0021D2 E0               [24] 3118 	movx	a,@dptr
      0021D3 FF               [12] 3119 	mov	r7,a
      0021D4 BE 02 05         [24] 3120 	cjne	r6,#0x02,00376$
      0021D7 BF 00 02         [24] 3121 	cjne	r7,#0x00,00376$
      0021DA 80 03            [24] 3122 	sjmp	00377$
      0021DC                       3123 00376$:
      0021DC 02 1F AB         [24] 3124 	ljmp	00165$
      0021DF                       3125 00377$:
      0021DF 90 00 07         [24] 3126 	mov	dptr,#_BackOffCounter
      0021E2 E0               [24] 3127 	movx	a,@dptr
      0021E3 24 01            [12] 3128 	add	a,#0x01
      0021E5 F0               [24] 3129 	movx	@dptr,a
      0021E6 A3               [24] 3130 	inc	dptr
      0021E7 E0               [24] 3131 	movx	a,@dptr
      0021E8 34 00            [12] 3132 	addc	a,#0x00
      0021EA F0               [24] 3133 	movx	@dptr,a
                                   3134 ;	..\src\COMMON\easyax5043.c:394: break;
      0021EB 02 1F AB         [24] 3135 	ljmp	00165$
      0021EE                       3136 00132$:
                                   3137 ;	..\src\COMMON\easyax5043.c:397: if(BeaconRx.FlagReceived == UPLINK_BEACON )
      0021EE 90 03 77         [24] 3138 	mov	dptr,#_receive_isr_BeaconRx_65536_259
      0021F1 E0               [24] 3139 	movx	a,@dptr
      0021F2 FF               [12] 3140 	mov	r7,a
      0021F3 BF 0C 02         [24] 3141 	cjne	r7,#0x0c,00378$
      0021F6 80 03            [24] 3142 	sjmp	00379$
      0021F8                       3143 00378$:
      0021F8 02 22 AF         [24] 3144 	ljmp	00137$
      0021FB                       3145 00379$:
                                   3146 ;	..\src\COMMON\easyax5043.c:399: if(GPSMessageReadyFlag | RfStateMachine == e_ReSendMessage){
      0021FB 90 0A 2D         [24] 3147 	mov	dptr,#_RfStateMachine
      0021FE E0               [24] 3148 	movx	a,@dptr
      0021FF FE               [12] 3149 	mov	r6,a
      002200 A3               [24] 3150 	inc	dptr
      002201 E0               [24] 3151 	movx	a,@dptr
      002202 FF               [12] 3152 	mov	r7,a
      002203 E4               [12] 3153 	clr	a
      002204 BE 03 04         [24] 3154 	cjne	r6,#0x03,00380$
      002207 BF 00 01         [24] 3155 	cjne	r7,#0x00,00380$
      00220A 04               [12] 3156 	inc	a
      00220B                       3157 00380$:
      00220B FE               [12] 3158 	mov	r6,a
      00220C 90 0A 2E         [24] 3159 	mov	dptr,#_GPSMessageReadyFlag
      00220F E0               [24] 3160 	movx	a,@dptr
      002210 FC               [12] 3161 	mov	r4,a
      002211 A3               [24] 3162 	inc	dptr
      002212 E0               [24] 3163 	movx	a,@dptr
      002213 FF               [12] 3164 	mov	r7,a
      002214 7B 00            [12] 3165 	mov	r3,#0x00
      002216 EE               [12] 3166 	mov	a,r6
      002217 42 04            [12] 3167 	orl	ar4,a
      002219 EB               [12] 3168 	mov	a,r3
      00221A 42 07            [12] 3169 	orl	ar7,a
      00221C EC               [12] 3170 	mov	a,r4
      00221D 4F               [12] 3171 	orl	a,r7
      00221E 70 03            [24] 3172 	jnz	00382$
      002220 02 22 A3         [24] 3173 	ljmp	00134$
      002223                       3174 00382$:
                                   3175 ;	..\src\COMMON\easyax5043.c:402: freq = freq;
                                   3176 ;	..\src\COMMON\easyax5043.c:403: axradio_set_channel(FREQHZ_TO_AXFREQ(freq));
      002223 90 0A 30         [24] 3177 	mov	dptr,#_freq
      002226 E0               [24] 3178 	movx	a,@dptr
      002227 FB               [12] 3179 	mov	r3,a
      002228 A3               [24] 3180 	inc	dptr
      002229 E0               [24] 3181 	movx	a,@dptr
      00222A FC               [12] 3182 	mov	r4,a
      00222B A3               [24] 3183 	inc	dptr
      00222C E0               [24] 3184 	movx	a,@dptr
      00222D FE               [12] 3185 	mov	r6,a
      00222E A3               [24] 3186 	inc	dptr
      00222F E0               [24] 3187 	movx	a,@dptr
      002230 8B 82            [24] 3188 	mov	dpl,r3
      002232 8C 83            [24] 3189 	mov	dph,r4
      002234 8E F0            [24] 3190 	mov	b,r6
      002236 12 79 06         [24] 3191 	lcall	___ulong2fs
      002239 AB 82            [24] 3192 	mov	r3,dpl
      00223B AC 83            [24] 3193 	mov	r4,dph
      00223D AE F0            [24] 3194 	mov	r6,b
      00223F FF               [12] 3195 	mov	r7,a
      002240 E4               [12] 3196 	clr	a
      002241 C0 E0            [24] 3197 	push	acc
      002243 74 1B            [12] 3198 	mov	a,#0x1b
      002245 C0 E0            [24] 3199 	push	acc
      002247 74 37            [12] 3200 	mov	a,#0x37
      002249 C0 E0            [24] 3201 	push	acc
      00224B 74 4C            [12] 3202 	mov	a,#0x4c
      00224D C0 E0            [24] 3203 	push	acc
      00224F 8B 82            [24] 3204 	mov	dpl,r3
      002251 8C 83            [24] 3205 	mov	dph,r4
      002253 8E F0            [24] 3206 	mov	b,r6
      002255 EF               [12] 3207 	mov	a,r7
      002256 12 8C 15         [24] 3208 	lcall	___fsdiv
      002259 AB 82            [24] 3209 	mov	r3,dpl
      00225B AC 83            [24] 3210 	mov	r4,dph
      00225D AE F0            [24] 3211 	mov	r6,b
      00225F FF               [12] 3212 	mov	r7,a
      002260 E5 81            [12] 3213 	mov	a,sp
      002262 24 FC            [12] 3214 	add	a,#0xfc
      002264 F5 81            [12] 3215 	mov	sp,a
      002266 C0 03            [24] 3216 	push	ar3
      002268 C0 04            [24] 3217 	push	ar4
      00226A C0 06            [24] 3218 	push	ar6
      00226C C0 07            [24] 3219 	push	ar7
      00226E 90 00 00         [24] 3220 	mov	dptr,#0x0000
      002271 75 F0 80         [24] 3221 	mov	b,#0x80
      002274 74 4B            [12] 3222 	mov	a,#0x4b
      002276 12 6F 8C         [24] 3223 	lcall	___fsmul
      002279 AB 82            [24] 3224 	mov	r3,dpl
      00227B AC 83            [24] 3225 	mov	r4,dph
      00227D AE F0            [24] 3226 	mov	r6,b
      00227F FF               [12] 3227 	mov	r7,a
      002280 E5 81            [12] 3228 	mov	a,sp
      002282 24 FC            [12] 3229 	add	a,#0xfc
      002284 F5 81            [12] 3230 	mov	sp,a
      002286 8B 82            [24] 3231 	mov	dpl,r3
      002288 8C 83            [24] 3232 	mov	dph,r4
      00228A 8E F0            [24] 3233 	mov	b,r6
      00228C EF               [12] 3234 	mov	a,r7
      00228D 12 79 19         [24] 3235 	lcall	___fs2ulong
      002290 12 47 F4         [24] 3236 	lcall	_axradio_set_channel
                                   3237 ;	..\src\COMMON\easyax5043.c:405: UpLinkIsrFlag = 1;
      002293 90 00 01         [24] 3238 	mov	dptr,#_UpLinkIsrFlag
      002296 74 01            [12] 3239 	mov	a,#0x01
      002298 F0               [24] 3240 	movx	@dptr,a
      002299 E4               [12] 3241 	clr	a
      00229A A3               [24] 3242 	inc	dptr
      00229B F0               [24] 3243 	movx	@dptr,a
                                   3244 ;	..\src\COMMON\easyax5043.c:406: BeaconRx.FlagReceived = 0;
      00229C 90 03 77         [24] 3245 	mov	dptr,#_receive_isr_BeaconRx_65536_259
      00229F F0               [24] 3246 	movx	@dptr,a
      0022A0 02 1F AB         [24] 3247 	ljmp	00165$
      0022A3                       3248 00134$:
                                   3249 ;	..\src\COMMON\easyax5043.c:411: dbglink_writestr(" discarded ");
      0022A3 90 91 CF         [24] 3250 	mov	dptr,#___str_1
      0022A6 75 F0 80         [24] 3251 	mov	b,#0x80
      0022A9 12 82 8A         [24] 3252 	lcall	_dbglink_writestr
                                   3253 ;	..\src\COMMON\easyax5043.c:414: break;
      0022AC 02 1F AB         [24] 3254 	ljmp	00165$
      0022AF                       3255 00137$:
                                   3256 ;	..\src\COMMON\easyax5043.c:416: if(BeaconRx.FlagReceived == DOWNLINK_BEACON)
      0022AF 90 03 77         [24] 3257 	mov	dptr,#_receive_isr_BeaconRx_65536_259
      0022B2 E0               [24] 3258 	movx	a,@dptr
      0022B3 FF               [12] 3259 	mov	r7,a
      0022B4 BF 0A 02         [24] 3260 	cjne	r7,#0x0a,00383$
      0022B7 80 03            [24] 3261 	sjmp	00384$
      0022B9                       3262 00383$:
      0022B9 02 1F AB         [24] 3263 	ljmp	00165$
      0022BC                       3264 00384$:
                                   3265 ;	..\src\COMMON\easyax5043.c:418: BeaconRx.FlagReceived = 0;
      0022BC 90 03 77         [24] 3266 	mov	dptr,#_receive_isr_BeaconRx_65536_259
      0022BF E4               [12] 3267 	clr	a
      0022C0 F0               [24] 3268 	movx	@dptr,a
                                   3269 ;	..\src\COMMON\easyax5043.c:419: DownLinkIsrFlag = 1;
      0022C1 90 00 03         [24] 3270 	mov	dptr,#_DownLinkIsrFlag
      0022C4 04               [12] 3271 	inc	a
      0022C5 F0               [24] 3272 	movx	@dptr,a
      0022C6 E4               [12] 3273 	clr	a
      0022C7 A3               [24] 3274 	inc	dptr
      0022C8 F0               [24] 3275 	movx	@dptr,a
                                   3276 ;	..\src\COMMON\easyax5043.c:420: break;
      0022C9 02 1F AB         [24] 3277 	ljmp	00165$
                                   3278 ;	..\src\COMMON\easyax5043.c:426: case AX5043_FIFOCMD_RFFREQOFFS:
      0022CC                       3279 00140$:
                                   3280 ;	..\src\COMMON\easyax5043.c:427: if (axradio_phy_innerfreqloop || len != 3)
      0022CC 90 90 BF         [24] 3281 	mov	dptr,#_axradio_phy_innerfreqloop
      0022CF E4               [12] 3282 	clr	a
      0022D0 93               [24] 3283 	movc	a,@a+dptr
      0022D1 60 03            [24] 3284 	jz	00385$
      0022D3 02 24 33         [24] 3285 	ljmp	00158$
      0022D6                       3286 00385$:
      0022D6 BD 03 02         [24] 3287 	cjne	r5,#0x03,00386$
      0022D9 80 03            [24] 3288 	sjmp	00387$
      0022DB                       3289 00386$:
      0022DB 02 24 33         [24] 3290 	ljmp	00158$
      0022DE                       3291 00387$:
                                   3292 ;	..\src\COMMON\easyax5043.c:429: i = AX5043_FIFODATA;
      0022DE 90 40 29         [24] 3293 	mov	dptr,#_AX5043_FIFODATA
      0022E1 E0               [24] 3294 	movx	a,@dptr
      0022E2 FF               [12] 3295 	mov	r7,a
                                   3296 ;	..\src\COMMON\easyax5043.c:430: i &= 0x0F;
      0022E3 53 07 0F         [24] 3297 	anl	ar7,#0x0f
                                   3298 ;	..\src\COMMON\easyax5043.c:431: i |= 1 + (uint8_t)~(i & 0x08);
      0022E6 8F 06            [24] 3299 	mov	ar6,r7
      0022E8 53 06 08         [24] 3300 	anl	ar6,#0x08
      0022EB EE               [12] 3301 	mov	a,r6
      0022EC F4               [12] 3302 	cpl	a
      0022ED FE               [12] 3303 	mov	r6,a
      0022EE 0E               [12] 3304 	inc	r6
      0022EF EF               [12] 3305 	mov	a,r7
      0022F0 42 06            [12] 3306 	orl	ar6,a
                                   3307 ;	..\src\COMMON\easyax5043.c:432: axradio_cb_receive.st.rx.phy.offset.b.b3 = ((int8_t)i) >> 8;
      0022F2 8E 07            [24] 3308 	mov	ar7,r6
      0022F4 EF               [12] 3309 	mov	a,r7
      0022F5 33               [12] 3310 	rlc	a
      0022F6 95 E0            [12] 3311 	subb	a,acc
      0022F8 90 03 27         [24] 3312 	mov	dptr,#(_axradio_cb_receive + 0x000f)
      0022FB F0               [24] 3313 	movx	@dptr,a
                                   3314 ;	..\src\COMMON\easyax5043.c:433: axradio_cb_receive.st.rx.phy.offset.b.b2 = i;
      0022FC 90 03 26         [24] 3315 	mov	dptr,#(_axradio_cb_receive + 0x000e)
      0022FF EE               [12] 3316 	mov	a,r6
      002300 F0               [24] 3317 	movx	@dptr,a
                                   3318 ;	..\src\COMMON\easyax5043.c:434: axradio_cb_receive.st.rx.phy.offset.b.b1 = AX5043_FIFODATA;
      002301 90 40 29         [24] 3319 	mov	dptr,#_AX5043_FIFODATA
      002304 E0               [24] 3320 	movx	a,@dptr
      002305 90 03 25         [24] 3321 	mov	dptr,#(_axradio_cb_receive + 0x000d)
      002308 F0               [24] 3322 	movx	@dptr,a
                                   3323 ;	..\src\COMMON\easyax5043.c:435: axradio_cb_receive.st.rx.phy.offset.b.b0 = AX5043_FIFODATA;
      002309 90 40 29         [24] 3324 	mov	dptr,#_AX5043_FIFODATA
      00230C E0               [24] 3325 	movx	a,@dptr
      00230D FF               [12] 3326 	mov	r7,a
      00230E 90 03 24         [24] 3327 	mov	dptr,#(_axradio_cb_receive + 0x000c)
      002311 F0               [24] 3328 	movx	@dptr,a
                                   3329 ;	..\src\COMMON\easyax5043.c:436: break;
      002312 02 1F AB         [24] 3330 	ljmp	00165$
                                   3331 ;	..\src\COMMON\easyax5043.c:438: case AX5043_FIFOCMD_FREQOFFS:
      002315                       3332 00144$:
                                   3333 ;	..\src\COMMON\easyax5043.c:439: if (!axradio_phy_innerfreqloop || len != 2)
      002315 90 90 BF         [24] 3334 	mov	dptr,#_axradio_phy_innerfreqloop
      002318 E4               [12] 3335 	clr	a
      002319 93               [24] 3336 	movc	a,@a+dptr
      00231A 70 03            [24] 3337 	jnz	00388$
      00231C 02 24 33         [24] 3338 	ljmp	00158$
      00231F                       3339 00388$:
      00231F BD 02 02         [24] 3340 	cjne	r5,#0x02,00389$
      002322 80 03            [24] 3341 	sjmp	00390$
      002324                       3342 00389$:
      002324 02 24 33         [24] 3343 	ljmp	00158$
      002327                       3344 00390$:
                                   3345 ;	..\src\COMMON\easyax5043.c:441: axradio_cb_receive.st.rx.phy.offset.b.b1 = AX5043_FIFODATA;
      002327 90 40 29         [24] 3346 	mov	dptr,#_AX5043_FIFODATA
      00232A E0               [24] 3347 	movx	a,@dptr
      00232B 90 03 25         [24] 3348 	mov	dptr,#(_axradio_cb_receive + 0x000d)
      00232E F0               [24] 3349 	movx	@dptr,a
                                   3350 ;	..\src\COMMON\easyax5043.c:442: axradio_cb_receive.st.rx.phy.offset.b.b0 = AX5043_FIFODATA;
      00232F 90 40 29         [24] 3351 	mov	dptr,#_AX5043_FIFODATA
      002332 E0               [24] 3352 	movx	a,@dptr
      002333 90 03 24         [24] 3353 	mov	dptr,#(_axradio_cb_receive + 0x000c)
      002336 F0               [24] 3354 	movx	@dptr,a
                                   3355 ;	..\src\COMMON\easyax5043.c:443: axradio_cb_receive.st.rx.phy.offset.o = axradio_conv_freq_fromreg(signextend16(axradio_cb_receive.st.rx.phy.offset.o));
      002337 90 03 24         [24] 3356 	mov	dptr,#(_axradio_cb_receive + 0x000c)
      00233A E0               [24] 3357 	movx	a,@dptr
      00233B FB               [12] 3358 	mov	r3,a
      00233C A3               [24] 3359 	inc	dptr
      00233D E0               [24] 3360 	movx	a,@dptr
      00233E FC               [12] 3361 	mov	r4,a
      00233F A3               [24] 3362 	inc	dptr
      002340 E0               [24] 3363 	movx	a,@dptr
      002341 A3               [24] 3364 	inc	dptr
      002342 E0               [24] 3365 	movx	a,@dptr
      002343 8B 82            [24] 3366 	mov	dpl,r3
      002345 8C 83            [24] 3367 	mov	dph,r4
      002347 12 8D D7         [24] 3368 	lcall	_signextend16
      00234A 12 1A 73         [24] 3369 	lcall	_axradio_conv_freq_fromreg
      00234D AB 82            [24] 3370 	mov	r3,dpl
      00234F AC 83            [24] 3371 	mov	r4,dph
      002351 AE F0            [24] 3372 	mov	r6,b
      002353 FF               [12] 3373 	mov	r7,a
      002354 90 03 24         [24] 3374 	mov	dptr,#(_axradio_cb_receive + 0x000c)
      002357 EB               [12] 3375 	mov	a,r3
      002358 F0               [24] 3376 	movx	@dptr,a
      002359 EC               [12] 3377 	mov	a,r4
      00235A A3               [24] 3378 	inc	dptr
      00235B F0               [24] 3379 	movx	@dptr,a
      00235C EE               [12] 3380 	mov	a,r6
      00235D A3               [24] 3381 	inc	dptr
      00235E F0               [24] 3382 	movx	@dptr,a
      00235F EF               [12] 3383 	mov	a,r7
      002360 A3               [24] 3384 	inc	dptr
      002361 F0               [24] 3385 	movx	@dptr,a
                                   3386 ;	..\src\COMMON\easyax5043.c:444: break;
      002362 02 1F AB         [24] 3387 	ljmp	00165$
                                   3388 ;	..\src\COMMON\easyax5043.c:446: case AX5043_FIFOCMD_RSSI:
      002365                       3389 00148$:
                                   3390 ;	..\src\COMMON\easyax5043.c:447: if (len != 1)
      002365 BD 01 02         [24] 3391 	cjne	r5,#0x01,00391$
      002368 80 03            [24] 3392 	sjmp	00392$
      00236A                       3393 00391$:
      00236A 02 24 33         [24] 3394 	ljmp	00158$
      00236D                       3395 00392$:
                                   3396 ;	..\src\COMMON\easyax5043.c:450: int8_t r = AX5043_FIFODATA;
      00236D 90 40 29         [24] 3397 	mov	dptr,#_AX5043_FIFODATA
      002370 E0               [24] 3398 	movx	a,@dptr
                                   3399 ;	..\src\COMMON\easyax5043.c:451: axradio_cb_receive.st.rx.phy.rssi = r - (int16_t)axradio_phy_rssioffset;
      002371 FF               [12] 3400 	mov	r7,a
      002372 33               [12] 3401 	rlc	a
      002373 95 E0            [12] 3402 	subb	a,acc
      002375 FE               [12] 3403 	mov	r6,a
      002376 90 90 CD         [24] 3404 	mov	dptr,#_axradio_phy_rssioffset
      002379 E4               [12] 3405 	clr	a
      00237A 93               [24] 3406 	movc	a,@a+dptr
      00237B FC               [12] 3407 	mov	r4,a
      00237C 33               [12] 3408 	rlc	a
      00237D 95 E0            [12] 3409 	subb	a,acc
      00237F FB               [12] 3410 	mov	r3,a
      002380 EF               [12] 3411 	mov	a,r7
      002381 C3               [12] 3412 	clr	c
      002382 9C               [12] 3413 	subb	a,r4
      002383 FF               [12] 3414 	mov	r7,a
      002384 EE               [12] 3415 	mov	a,r6
      002385 9B               [12] 3416 	subb	a,r3
      002386 FE               [12] 3417 	mov	r6,a
      002387 90 03 22         [24] 3418 	mov	dptr,#(_axradio_cb_receive + 0x000a)
      00238A EF               [12] 3419 	mov	a,r7
      00238B F0               [24] 3420 	movx	@dptr,a
      00238C EE               [12] 3421 	mov	a,r6
      00238D A3               [24] 3422 	inc	dptr
      00238E F0               [24] 3423 	movx	@dptr,a
                                   3424 ;	..\src\COMMON\easyax5043.c:453: break;
      00238F 02 1F AB         [24] 3425 	ljmp	00165$
                                   3426 ;	..\src\COMMON\easyax5043.c:455: case AX5043_FIFOCMD_TIMER:
      002392                       3427 00151$:
                                   3428 ;	..\src\COMMON\easyax5043.c:456: if (len != 3)
      002392 BD 03 02         [24] 3429 	cjne	r5,#0x03,00393$
      002395 80 03            [24] 3430 	sjmp	00394$
      002397                       3431 00393$:
      002397 02 24 33         [24] 3432 	ljmp	00158$
      00239A                       3433 00394$:
                                   3434 ;	..\src\COMMON\easyax5043.c:460: axradio_cb_receive.st.time.b.b3 = 0;
      00239A 90 03 21         [24] 3435 	mov	dptr,#(_axradio_cb_receive + 0x0009)
      00239D E4               [12] 3436 	clr	a
      00239E F0               [24] 3437 	movx	@dptr,a
                                   3438 ;	..\src\COMMON\easyax5043.c:461: axradio_cb_receive.st.time.b.b2 = AX5043_FIFODATA;
      00239F 90 40 29         [24] 3439 	mov	dptr,#_AX5043_FIFODATA
      0023A2 E0               [24] 3440 	movx	a,@dptr
      0023A3 90 03 20         [24] 3441 	mov	dptr,#(_axradio_cb_receive + 0x0008)
      0023A6 F0               [24] 3442 	movx	@dptr,a
                                   3443 ;	..\src\COMMON\easyax5043.c:462: axradio_cb_receive.st.time.b.b1 = AX5043_FIFODATA;
      0023A7 90 40 29         [24] 3444 	mov	dptr,#_AX5043_FIFODATA
      0023AA E0               [24] 3445 	movx	a,@dptr
      0023AB 90 03 1F         [24] 3446 	mov	dptr,#(_axradio_cb_receive + 0x0007)
      0023AE F0               [24] 3447 	movx	@dptr,a
                                   3448 ;	..\src\COMMON\easyax5043.c:463: axradio_cb_receive.st.time.b.b0 = AX5043_FIFODATA;
      0023AF 90 40 29         [24] 3449 	mov	dptr,#_AX5043_FIFODATA
      0023B2 E0               [24] 3450 	movx	a,@dptr
      0023B3 FF               [12] 3451 	mov	r7,a
      0023B4 90 03 1E         [24] 3452 	mov	dptr,#(_axradio_cb_receive + 0x0006)
      0023B7 F0               [24] 3453 	movx	@dptr,a
                                   3454 ;	..\src\COMMON\easyax5043.c:464: break;
      0023B8 02 1F AB         [24] 3455 	ljmp	00165$
                                   3456 ;	..\src\COMMON\easyax5043.c:466: case AX5043_FIFOCMD_ANTRSSI:
      0023BB                       3457 00154$:
                                   3458 ;	..\src\COMMON\easyax5043.c:467: if (!len)
      0023BB ED               [12] 3459 	mov	a,r5
      0023BC 70 03            [24] 3460 	jnz	00395$
      0023BE 02 1F AB         [24] 3461 	ljmp	00165$
      0023C1                       3462 00395$:
                                   3463 ;	..\src\COMMON\easyax5043.c:469: update_timeanchor();
      0023C1 C0 05            [24] 3464 	push	ar5
      0023C3 12 1E 70         [24] 3465 	lcall	_update_timeanchor
                                   3466 ;	..\src\COMMON\easyax5043.c:470: wtimer_remove_callback(&axradio_cb_channelstate.cb);
      0023C6 90 03 44         [24] 3467 	mov	dptr,#_axradio_cb_channelstate
      0023C9 12 87 AB         [24] 3468 	lcall	_wtimer_remove_callback
                                   3469 ;	..\src\COMMON\easyax5043.c:471: axradio_cb_channelstate.st.error = AXRADIO_ERR_NOERROR;
      0023CC 90 03 49         [24] 3470 	mov	dptr,#(_axradio_cb_channelstate + 0x0005)
      0023CF E4               [12] 3471 	clr	a
      0023D0 F0               [24] 3472 	movx	@dptr,a
                                   3473 ;	..\src\COMMON\easyax5043.c:473: int8_t r = AX5043_FIFODATA;
      0023D1 90 40 29         [24] 3474 	mov	dptr,#_AX5043_FIFODATA
      0023D4 E0               [24] 3475 	movx	a,@dptr
                                   3476 ;	..\src\COMMON\easyax5043.c:474: axradio_cb_channelstate.st.cs.rssi = r - (int16_t)axradio_phy_rssioffset;
      0023D5 FF               [12] 3477 	mov	r7,a
      0023D6 FC               [12] 3478 	mov	r4,a
      0023D7 33               [12] 3479 	rlc	a
      0023D8 95 E0            [12] 3480 	subb	a,acc
      0023DA FE               [12] 3481 	mov	r6,a
      0023DB 90 90 CD         [24] 3482 	mov	dptr,#_axradio_phy_rssioffset
      0023DE E4               [12] 3483 	clr	a
      0023DF 93               [24] 3484 	movc	a,@a+dptr
      0023E0 FB               [12] 3485 	mov	r3,a
      0023E1 33               [12] 3486 	rlc	a
      0023E2 95 E0            [12] 3487 	subb	a,acc
      0023E4 FA               [12] 3488 	mov	r2,a
      0023E5 EC               [12] 3489 	mov	a,r4
      0023E6 C3               [12] 3490 	clr	c
      0023E7 9B               [12] 3491 	subb	a,r3
      0023E8 FC               [12] 3492 	mov	r4,a
      0023E9 EE               [12] 3493 	mov	a,r6
      0023EA 9A               [12] 3494 	subb	a,r2
      0023EB FE               [12] 3495 	mov	r6,a
      0023EC 90 03 4E         [24] 3496 	mov	dptr,#(_axradio_cb_channelstate + 0x000a)
      0023EF EC               [12] 3497 	mov	a,r4
      0023F0 F0               [24] 3498 	movx	@dptr,a
      0023F1 EE               [12] 3499 	mov	a,r6
      0023F2 A3               [24] 3500 	inc	dptr
      0023F3 F0               [24] 3501 	movx	@dptr,a
                                   3502 ;	..\src\COMMON\easyax5043.c:475: axradio_cb_channelstate.st.cs.busy = r >= axradio_phy_channelbusy;
      0023F4 90 90 CF         [24] 3503 	mov	dptr,#_axradio_phy_channelbusy
      0023F7 E4               [12] 3504 	clr	a
      0023F8 93               [24] 3505 	movc	a,@a+dptr
      0023F9 FE               [12] 3506 	mov	r6,a
      0023FA C3               [12] 3507 	clr	c
      0023FB EF               [12] 3508 	mov	a,r7
      0023FC 64 80            [12] 3509 	xrl	a,#0x80
      0023FE 8E F0            [24] 3510 	mov	b,r6
      002400 63 F0 80         [24] 3511 	xrl	b,#0x80
      002403 95 F0            [12] 3512 	subb	a,b
      002405 B3               [12] 3513 	cpl	c
      002406 92 00            [24] 3514 	mov	b0,c
      002408 E4               [12] 3515 	clr	a
      002409 33               [12] 3516 	rlc	a
      00240A 90 03 50         [24] 3517 	mov	dptr,#(_axradio_cb_channelstate + 0x000c)
      00240D F0               [24] 3518 	movx	@dptr,a
                                   3519 ;	..\src\COMMON\easyax5043.c:477: axradio_cb_channelstate.st.time.t = axradio_timeanchor.radiotimer;
      00240E 90 01 00         [24] 3520 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      002411 E0               [24] 3521 	movx	a,@dptr
      002412 FB               [12] 3522 	mov	r3,a
      002413 A3               [24] 3523 	inc	dptr
      002414 E0               [24] 3524 	movx	a,@dptr
      002415 FC               [12] 3525 	mov	r4,a
      002416 A3               [24] 3526 	inc	dptr
      002417 E0               [24] 3527 	movx	a,@dptr
      002418 FE               [12] 3528 	mov	r6,a
      002419 A3               [24] 3529 	inc	dptr
      00241A E0               [24] 3530 	movx	a,@dptr
      00241B FF               [12] 3531 	mov	r7,a
      00241C 90 03 4A         [24] 3532 	mov	dptr,#(_axradio_cb_channelstate + 0x0006)
      00241F EB               [12] 3533 	mov	a,r3
      002420 F0               [24] 3534 	movx	@dptr,a
      002421 EC               [12] 3535 	mov	a,r4
      002422 A3               [24] 3536 	inc	dptr
      002423 F0               [24] 3537 	movx	@dptr,a
      002424 EE               [12] 3538 	mov	a,r6
      002425 A3               [24] 3539 	inc	dptr
      002426 F0               [24] 3540 	movx	@dptr,a
      002427 EF               [12] 3541 	mov	a,r7
      002428 A3               [24] 3542 	inc	dptr
      002429 F0               [24] 3543 	movx	@dptr,a
                                   3544 ;	..\src\COMMON\easyax5043.c:478: wtimer_add_callback(&axradio_cb_channelstate.cb);
      00242A 90 03 44         [24] 3545 	mov	dptr,#_axradio_cb_channelstate
      00242D 12 78 5E         [24] 3546 	lcall	_wtimer_add_callback
      002430 D0 05            [24] 3547 	pop	ar5
                                   3548 ;	..\src\COMMON\easyax5043.c:479: --len;
      002432 1D               [12] 3549 	dec	r5
                                   3550 ;	..\src\COMMON\easyax5043.c:484: dropchunk:
      002433                       3551 00158$:
                                   3552 ;	..\src\COMMON\easyax5043.c:485: if (!len)
      002433 ED               [12] 3553 	mov	a,r5
      002434 70 03            [24] 3554 	jnz	00396$
      002436 02 1F AB         [24] 3555 	ljmp	00165$
      002439                       3556 00396$:
                                   3557 ;	..\src\COMMON\easyax5043.c:488: do {
      002439 8D 07            [24] 3558 	mov	ar7,r5
      00243B                       3559 00161$:
                                   3560 ;	..\src\COMMON\easyax5043.c:489: AX5043_FIFODATA;        // purge FIFO
      00243B 90 40 29         [24] 3561 	mov	dptr,#_AX5043_FIFODATA
      00243E E0               [24] 3562 	movx	a,@dptr
                                   3563 ;	..\src\COMMON\easyax5043.c:491: while (--i);
      00243F DF FA            [24] 3564 	djnz	r7,00161$
                                   3565 ;	..\src\COMMON\easyax5043.c:493: } // end switch(fifo_cmd)
                                   3566 ;	..\src\COMMON\easyax5043.c:495: }
      002441 02 1F AB         [24] 3567 	ljmp	00165$
                                   3568 ;------------------------------------------------------------
                                   3569 ;Allocation info for local variables in function 'transmit_isr'
                                   3570 ;------------------------------------------------------------
                                   3571 ;cnt                       Allocated to registers r4 
                                   3572 ;byte                      Allocated to registers r2 
                                   3573 ;len_byte                  Allocated to registers r4 
                                   3574 ;i                         Allocated to registers r2 
                                   3575 ;byte                      Allocated to registers r3 
                                   3576 ;flags                     Allocated to registers r6 
                                   3577 ;len                       Allocated to registers r4 r5 
                                   3578 ;sloc0                     Allocated to stack - _bp +9
                                   3579 ;sloc1                     Allocated to stack - _bp +10
                                   3580 ;sloc2                     Allocated to stack - _bp +11
                                   3581 ;------------------------------------------------------------
                                   3582 ;	..\src\COMMON\easyax5043.c:497: static __reentrantb void transmit_isr(void) __reentrant
                                   3583 ;	-----------------------------------------
                                   3584 ;	 function transmit_isr
                                   3585 ;	-----------------------------------------
      002444                       3586 _transmit_isr:
                                   3587 ;	..\src\COMMON\easyax5043.c:636: axradio_trxstate = trxstate_tx_waitdone;
      002444                       3588 00157$:
                                   3589 ;	..\src\COMMON\easyax5043.c:500: uint8_t cnt = AX5043_FIFOFREE0;
      002444 90 40 2D         [24] 3590 	mov	dptr,#_AX5043_FIFOFREE0
      002447 E0               [24] 3591 	movx	a,@dptr
      002448 FF               [12] 3592 	mov	r7,a
                                   3593 ;	..\src\COMMON\easyax5043.c:501: if (AX5043_FIFOFREE1)
      002449 90 40 2C         [24] 3594 	mov	dptr,#_AX5043_FIFOFREE1
      00244C E0               [24] 3595 	movx	a,@dptr
      00244D 60 02            [24] 3596 	jz	00102$
                                   3597 ;	..\src\COMMON\easyax5043.c:502: cnt = 0xff;
      00244F 7F FF            [12] 3598 	mov	r7,#0xff
      002451                       3599 00102$:
                                   3600 ;	..\src\COMMON\easyax5043.c:503: switch (axradio_trxstate) {
      002451 AE 18            [24] 3601 	mov	r6,_axradio_trxstate
      002453 BE 0A 02         [24] 3602 	cjne	r6,#0x0a,00300$
      002456 80 0D            [24] 3603 	sjmp	00103$
      002458                       3604 00300$:
      002458 BE 0B 03         [24] 3605 	cjne	r6,#0x0b,00301$
      00245B 02 24 F6         [24] 3606 	ljmp	00115$
      00245E                       3607 00301$:
      00245E BE 0C 03         [24] 3608 	cjne	r6,#0x0c,00302$
      002461 02 26 FB         [24] 3609 	ljmp	00138$
      002464                       3610 00302$:
      002464 22               [24] 3611 	ret
                                   3612 ;	..\src\COMMON\easyax5043.c:504: case trxstate_tx_longpreamble:
      002465                       3613 00103$:
                                   3614 ;	..\src\COMMON\easyax5043.c:505: if (!axradio_txbuffer_cnt) {
      002465 90 00 ED         [24] 3615 	mov	dptr,#_axradio_txbuffer_cnt
      002468 E0               [24] 3616 	movx	a,@dptr
      002469 FD               [12] 3617 	mov	r5,a
      00246A A3               [24] 3618 	inc	dptr
      00246B E0               [24] 3619 	movx	a,@dptr
      00246C FE               [12] 3620 	mov	r6,a
      00246D 4D               [12] 3621 	orl	a,r5
      00246E 70 37            [24] 3622 	jnz	00109$
                                   3623 ;	..\src\COMMON\easyax5043.c:506: axradio_trxstate = trxstate_tx_shortpreamble;
      002470 75 18 0B         [24] 3624 	mov	_axradio_trxstate,#0x0b
                                   3625 ;	..\src\COMMON\easyax5043.c:507: if( axradio_mode == AXRADIO_MODE_WOR_TRANSMIT || axradio_mode == AXRADIO_MODE_WOR_ACK_TRANSMIT )
      002473 74 11            [12] 3626 	mov	a,#0x11
      002475 B5 17 02         [24] 3627 	cjne	a,_axradio_mode,00304$
      002478 80 05            [24] 3628 	sjmp	00104$
      00247A                       3629 00304$:
      00247A 74 13            [12] 3630 	mov	a,#0x13
      00247C B5 17 14         [24] 3631 	cjne	a,_axradio_mode,00105$
      00247F                       3632 00104$:
                                   3633 ;	..\src\COMMON\easyax5043.c:508: axradio_txbuffer_cnt = axradio_phy_preamble_wor_len;
      00247F 90 90 D7         [24] 3634 	mov	dptr,#_axradio_phy_preamble_wor_len
      002482 E4               [12] 3635 	clr	a
      002483 93               [24] 3636 	movc	a,@a+dptr
      002484 FB               [12] 3637 	mov	r3,a
      002485 74 01            [12] 3638 	mov	a,#0x01
      002487 93               [24] 3639 	movc	a,@a+dptr
      002488 FC               [12] 3640 	mov	r4,a
      002489 90 00 ED         [24] 3641 	mov	dptr,#_axradio_txbuffer_cnt
      00248C EB               [12] 3642 	mov	a,r3
      00248D F0               [24] 3643 	movx	@dptr,a
      00248E EC               [12] 3644 	mov	a,r4
      00248F A3               [24] 3645 	inc	dptr
      002490 F0               [24] 3646 	movx	@dptr,a
      002491 80 63            [24] 3647 	sjmp	00115$
      002493                       3648 00105$:
                                   3649 ;	..\src\COMMON\easyax5043.c:510: axradio_txbuffer_cnt = axradio_phy_preamble_len;
      002493 90 90 DB         [24] 3650 	mov	dptr,#_axradio_phy_preamble_len
      002496 E4               [12] 3651 	clr	a
      002497 93               [24] 3652 	movc	a,@a+dptr
      002498 FB               [12] 3653 	mov	r3,a
      002499 74 01            [12] 3654 	mov	a,#0x01
      00249B 93               [24] 3655 	movc	a,@a+dptr
      00249C FC               [12] 3656 	mov	r4,a
      00249D 90 00 ED         [24] 3657 	mov	dptr,#_axradio_txbuffer_cnt
      0024A0 EB               [12] 3658 	mov	a,r3
      0024A1 F0               [24] 3659 	movx	@dptr,a
      0024A2 EC               [12] 3660 	mov	a,r4
      0024A3 A3               [24] 3661 	inc	dptr
      0024A4 F0               [24] 3662 	movx	@dptr,a
                                   3663 ;	..\src\COMMON\easyax5043.c:511: goto shortpreamble;
      0024A5 80 4F            [24] 3664 	sjmp	00115$
      0024A7                       3665 00109$:
                                   3666 ;	..\src\COMMON\easyax5043.c:513: if (cnt < 4)
      0024A7 BF 04 00         [24] 3667 	cjne	r7,#0x04,00307$
      0024AA                       3668 00307$:
      0024AA 50 03            [24] 3669 	jnc	00308$
      0024AC 02 27 9E         [24] 3670 	ljmp	00153$
      0024AF                       3671 00308$:
                                   3672 ;	..\src\COMMON\easyax5043.c:515: cnt = 7;
      0024AF 7C 07            [12] 3673 	mov	r4,#0x07
                                   3674 ;	..\src\COMMON\easyax5043.c:516: if (axradio_txbuffer_cnt < 7)
      0024B1 C3               [12] 3675 	clr	c
      0024B2 ED               [12] 3676 	mov	a,r5
      0024B3 94 07            [12] 3677 	subb	a,#0x07
      0024B5 EE               [12] 3678 	mov	a,r6
      0024B6 94 00            [12] 3679 	subb	a,#0x00
      0024B8 50 02            [24] 3680 	jnc	00113$
                                   3681 ;	..\src\COMMON\easyax5043.c:517: cnt = axradio_txbuffer_cnt;
      0024BA 8D 04            [24] 3682 	mov	ar4,r5
      0024BC                       3683 00113$:
                                   3684 ;	..\src\COMMON\easyax5043.c:518: axradio_txbuffer_cnt -= cnt;
      0024BC 8C 05            [24] 3685 	mov	ar5,r4
      0024BE 7E 00            [12] 3686 	mov	r6,#0x00
      0024C0 90 00 ED         [24] 3687 	mov	dptr,#_axradio_txbuffer_cnt
      0024C3 E0               [24] 3688 	movx	a,@dptr
      0024C4 FA               [12] 3689 	mov	r2,a
      0024C5 A3               [24] 3690 	inc	dptr
      0024C6 E0               [24] 3691 	movx	a,@dptr
      0024C7 FB               [12] 3692 	mov	r3,a
      0024C8 90 00 ED         [24] 3693 	mov	dptr,#_axradio_txbuffer_cnt
      0024CB EA               [12] 3694 	mov	a,r2
      0024CC C3               [12] 3695 	clr	c
      0024CD 9D               [12] 3696 	subb	a,r5
      0024CE F0               [24] 3697 	movx	@dptr,a
      0024CF EB               [12] 3698 	mov	a,r3
      0024D0 9E               [12] 3699 	subb	a,r6
      0024D1 A3               [24] 3700 	inc	dptr
      0024D2 F0               [24] 3701 	movx	@dptr,a
                                   3702 ;	..\src\COMMON\easyax5043.c:519: cnt <<= 5;
      0024D3 EC               [12] 3703 	mov	a,r4
      0024D4 C4               [12] 3704 	swap	a
      0024D5 23               [12] 3705 	rl	a
      0024D6 54 E0            [12] 3706 	anl	a,#0xe0
      0024D8 FC               [12] 3707 	mov	r4,a
                                   3708 ;	..\src\COMMON\easyax5043.c:520: AX5043_FIFODATA = AX5043_FIFOCMD_REPEATDATA | (3 << 5);
      0024D9 90 40 29         [24] 3709 	mov	dptr,#_AX5043_FIFODATA
      0024DC 74 62            [12] 3710 	mov	a,#0x62
      0024DE F0               [24] 3711 	movx	@dptr,a
                                   3712 ;	..\src\COMMON\easyax5043.c:521: AX5043_FIFODATA = axradio_phy_preamble_flags;
      0024DF 90 90 DE         [24] 3713 	mov	dptr,#_axradio_phy_preamble_flags
      0024E2 E4               [12] 3714 	clr	a
      0024E3 93               [24] 3715 	movc	a,@a+dptr
      0024E4 90 40 29         [24] 3716 	mov	dptr,#_AX5043_FIFODATA
      0024E7 F0               [24] 3717 	movx	@dptr,a
                                   3718 ;	..\src\COMMON\easyax5043.c:522: AX5043_FIFODATA = cnt;
      0024E8 EC               [12] 3719 	mov	a,r4
      0024E9 F0               [24] 3720 	movx	@dptr,a
                                   3721 ;	..\src\COMMON\easyax5043.c:523: AX5043_FIFODATA = axradio_phy_preamble_byte;
      0024EA 90 90 DD         [24] 3722 	mov	dptr,#_axradio_phy_preamble_byte
      0024ED E4               [12] 3723 	clr	a
      0024EE 93               [24] 3724 	movc	a,@a+dptr
      0024EF 90 40 29         [24] 3725 	mov	dptr,#_AX5043_FIFODATA
      0024F2 F0               [24] 3726 	movx	@dptr,a
                                   3727 ;	..\src\COMMON\easyax5043.c:524: break;
      0024F3 02 24 44         [24] 3728 	ljmp	00157$
                                   3729 ;	..\src\COMMON\easyax5043.c:527: shortpreamble:
      0024F6                       3730 00115$:
                                   3731 ;	..\src\COMMON\easyax5043.c:528: if (!axradio_txbuffer_cnt) {
      0024F6 90 00 ED         [24] 3732 	mov	dptr,#_axradio_txbuffer_cnt
      0024F9 E0               [24] 3733 	movx	a,@dptr
      0024FA FD               [12] 3734 	mov	r5,a
      0024FB A3               [24] 3735 	inc	dptr
      0024FC E0               [24] 3736 	movx	a,@dptr
      0024FD FE               [12] 3737 	mov	r6,a
      0024FE 4D               [12] 3738 	orl	a,r5
      0024FF 60 03            [24] 3739 	jz	00310$
      002501 02 26 00         [24] 3740 	ljmp	00128$
      002504                       3741 00310$:
                                   3742 ;	..\src\COMMON\easyax5043.c:529: if (cnt < 15)
      002504 BF 0F 00         [24] 3743 	cjne	r7,#0x0f,00311$
      002507                       3744 00311$:
      002507 50 03            [24] 3745 	jnc	00312$
      002509 02 27 9E         [24] 3746 	ljmp	00153$
      00250C                       3747 00312$:
                                   3748 ;	..\src\COMMON\easyax5043.c:531: if (axradio_phy_preamble_appendbits) {
      00250C 90 90 DF         [24] 3749 	mov	dptr,#_axradio_phy_preamble_appendbits
      00250F E4               [12] 3750 	clr	a
      002510 93               [24] 3751 	movc	a,@a+dptr
      002511 FF               [12] 3752 	mov	r7,a
      002512 70 03            [24] 3753 	jnz	00313$
      002514 02 25 9A         [24] 3754 	ljmp	00122$
      002517                       3755 00313$:
                                   3756 ;	..\src\COMMON\easyax5043.c:533: AX5043_FIFODATA = AX5043_FIFOCMD_DATA | (2 << 5);
      002517 90 40 29         [24] 3757 	mov	dptr,#_AX5043_FIFODATA
      00251A 74 41            [12] 3758 	mov	a,#0x41
      00251C F0               [24] 3759 	movx	@dptr,a
                                   3760 ;	..\src\COMMON\easyax5043.c:534: AX5043_FIFODATA = 0x1C;
      00251D 74 1C            [12] 3761 	mov	a,#0x1c
      00251F F0               [24] 3762 	movx	@dptr,a
                                   3763 ;	..\src\COMMON\easyax5043.c:535: byte = axradio_phy_preamble_appendpattern;
      002520 90 90 E0         [24] 3764 	mov	dptr,#_axradio_phy_preamble_appendpattern
      002523 E4               [12] 3765 	clr	a
      002524 93               [24] 3766 	movc	a,@a+dptr
      002525 FD               [12] 3767 	mov	r5,a
                                   3768 ;	..\src\COMMON\easyax5043.c:536: if (AX5043_PKTADDRCFG & 0x80) {
      002526 90 42 00         [24] 3769 	mov	dptr,#_AX5043_PKTADDRCFG
      002529 E0               [24] 3770 	movx	a,@dptr
      00252A 30 E7 36         [24] 3771 	jnb	acc.7,00119$
                                   3772 ;	..\src\COMMON\easyax5043.c:538: byte &= 0xFF << (8-axradio_phy_preamble_appendbits);
      00252D 8F 02            [24] 3773 	mov	ar2,r7
      00252F 74 08            [12] 3774 	mov	a,#0x08
      002531 C3               [12] 3775 	clr	c
      002532 9A               [12] 3776 	subb	a,r2
      002533 F5 F0            [12] 3777 	mov	b,a
      002535 05 F0            [12] 3778 	inc	b
      002537 74 FF            [12] 3779 	mov	a,#0xff
      002539 80 02            [24] 3780 	sjmp	00317$
      00253B                       3781 00315$:
      00253B 25 E0            [12] 3782 	add	a,acc
      00253D                       3783 00317$:
      00253D D5 F0 FB         [24] 3784 	djnz	b,00315$
      002540 FE               [12] 3785 	mov	r6,a
      002541 8D 02            [24] 3786 	mov	ar2,r5
      002543 EA               [12] 3787 	mov	a,r2
      002544 52 06            [12] 3788 	anl	ar6,a
                                   3789 ;	..\src\COMMON\easyax5043.c:539: byte |= 0x80 >> axradio_phy_preamble_appendbits;
      002546 8F F0            [24] 3790 	mov	b,r7
      002548 05 F0            [12] 3791 	inc	b
      00254A 7A 80            [12] 3792 	mov	r2,#0x80
      00254C E4               [12] 3793 	clr	a
      00254D FC               [12] 3794 	mov	r4,a
      00254E 33               [12] 3795 	rlc	a
      00254F 92 D2            [24] 3796 	mov	ov,c
      002551 80 08            [24] 3797 	sjmp	00319$
      002553                       3798 00318$:
      002553 A2 D2            [12] 3799 	mov	c,ov
      002555 EC               [12] 3800 	mov	a,r4
      002556 13               [12] 3801 	rrc	a
      002557 FC               [12] 3802 	mov	r4,a
      002558 EA               [12] 3803 	mov	a,r2
      002559 13               [12] 3804 	rrc	a
      00255A FA               [12] 3805 	mov	r2,a
      00255B                       3806 00319$:
      00255B D5 F0 F5         [24] 3807 	djnz	b,00318$
      00255E EA               [12] 3808 	mov	a,r2
      00255F 4E               [12] 3809 	orl	a,r6
      002560 FC               [12] 3810 	mov	r4,a
      002561 80 32            [24] 3811 	sjmp	00120$
      002563                       3812 00119$:
                                   3813 ;	..\src\COMMON\easyax5043.c:542: byte &= 0xFF >> (8-axradio_phy_preamble_appendbits);
      002563 74 08            [12] 3814 	mov	a,#0x08
      002565 C3               [12] 3815 	clr	c
      002566 9F               [12] 3816 	subb	a,r7
      002567 FA               [12] 3817 	mov	r2,a
      002568 8A F0            [24] 3818 	mov	b,r2
      00256A 05 F0            [12] 3819 	inc	b
      00256C 7A FF            [12] 3820 	mov	r2,#0xff
      00256E E4               [12] 3821 	clr	a
      00256F FB               [12] 3822 	mov	r3,a
      002570 33               [12] 3823 	rlc	a
      002571 92 D2            [24] 3824 	mov	ov,c
      002573 80 08            [24] 3825 	sjmp	00321$
      002575                       3826 00320$:
      002575 A2 D2            [12] 3827 	mov	c,ov
      002577 EB               [12] 3828 	mov	a,r3
      002578 13               [12] 3829 	rrc	a
      002579 FB               [12] 3830 	mov	r3,a
      00257A EA               [12] 3831 	mov	a,r2
      00257B 13               [12] 3832 	rrc	a
      00257C FA               [12] 3833 	mov	r2,a
      00257D                       3834 00321$:
      00257D D5 F0 F5         [24] 3835 	djnz	b,00320$
      002580 ED               [12] 3836 	mov	a,r5
      002581 52 02            [12] 3837 	anl	ar2,a
                                   3838 ;	..\src\COMMON\easyax5043.c:543: byte |= 0x01 << axradio_phy_preamble_appendbits;
      002583 8F 03            [24] 3839 	mov	ar3,r7
      002585 8B F0            [24] 3840 	mov	b,r3
      002587 05 F0            [12] 3841 	inc	b
      002589 74 01            [12] 3842 	mov	a,#0x01
      00258B 80 02            [24] 3843 	sjmp	00324$
      00258D                       3844 00322$:
      00258D 25 E0            [12] 3845 	add	a,acc
      00258F                       3846 00324$:
      00258F D5 F0 FB         [24] 3847 	djnz	b,00322$
      002592 FB               [12] 3848 	mov	r3,a
      002593 4A               [12] 3849 	orl	a,r2
      002594 FC               [12] 3850 	mov	r4,a
      002595                       3851 00120$:
                                   3852 ;	..\src\COMMON\easyax5043.c:545: AX5043_FIFODATA = byte;
      002595 90 40 29         [24] 3853 	mov	dptr,#_AX5043_FIFODATA
      002598 EC               [12] 3854 	mov	a,r4
      002599 F0               [24] 3855 	movx	@dptr,a
      00259A                       3856 00122$:
                                   3857 ;	..\src\COMMON\easyax5043.c:551: if ((AX5043_FRAMING & 0x0E) == 0x06 && axradio_framing_synclen) {
      00259A 90 40 12         [24] 3858 	mov	dptr,#_AX5043_FRAMING
      00259D E0               [24] 3859 	movx	a,@dptr
      00259E FC               [12] 3860 	mov	r4,a
      00259F 53 04 0E         [24] 3861 	anl	ar4,#0x0e
      0025A2 7B 00            [12] 3862 	mov	r3,#0x00
      0025A4 BC 06 53         [24] 3863 	cjne	r4,#0x06,00125$
      0025A7 BB 00 50         [24] 3864 	cjne	r3,#0x00,00125$
      0025AA 90 90 E9         [24] 3865 	mov	dptr,#_axradio_framing_synclen
      0025AD E4               [12] 3866 	clr	a
      0025AE 93               [24] 3867 	movc	a,@a+dptr
      0025AF FC               [12] 3868 	mov	r4,a
      0025B0 E4               [12] 3869 	clr	a
      0025B1 93               [24] 3870 	movc	a,@a+dptr
      0025B2 60 46            [24] 3871 	jz	00125$
                                   3872 ;	..\src\COMMON\easyax5043.c:553: uint8_t len_byte = axradio_framing_synclen;
                                   3873 ;	..\src\COMMON\easyax5043.c:554: uint8_t i = (len_byte & 0x07) ? 0x04 : 0;
      0025B4 EC               [12] 3874 	mov	a,r4
      0025B5 54 07            [12] 3875 	anl	a,#0x07
      0025B7 60 06            [24] 3876 	jz	00161$
      0025B9 7A 04            [12] 3877 	mov	r2,#0x04
      0025BB 7B 00            [12] 3878 	mov	r3,#0x00
      0025BD 80 04            [24] 3879 	sjmp	00162$
      0025BF                       3880 00161$:
      0025BF 7A 00            [12] 3881 	mov	r2,#0x00
      0025C1 7B 00            [12] 3882 	mov	r3,#0x00
      0025C3                       3883 00162$:
                                   3884 ;	..\src\COMMON\easyax5043.c:556: len_byte += 7;
      0025C3 74 07            [12] 3885 	mov	a,#0x07
      0025C5 2C               [12] 3886 	add	a,r4
                                   3887 ;	..\src\COMMON\easyax5043.c:557: len_byte >>= 3;
      0025C6 C4               [12] 3888 	swap	a
      0025C7 23               [12] 3889 	rl	a
      0025C8 54 1F            [12] 3890 	anl	a,#0x1f
                                   3891 ;	..\src\COMMON\easyax5043.c:558: AX5043_FIFODATA = AX5043_FIFOCMD_DATA | ((len_byte + 1) << 5);
      0025CA FC               [12] 3892 	mov	r4,a
      0025CB FB               [12] 3893 	mov	r3,a
      0025CC 0B               [12] 3894 	inc	r3
      0025CD EB               [12] 3895 	mov	a,r3
      0025CE C4               [12] 3896 	swap	a
      0025CF 23               [12] 3897 	rl	a
      0025D0 54 E0            [12] 3898 	anl	a,#0xe0
      0025D2 FB               [12] 3899 	mov	r3,a
      0025D3 90 40 29         [24] 3900 	mov	dptr,#_AX5043_FIFODATA
      0025D6 74 01            [12] 3901 	mov	a,#0x01
      0025D8 4B               [12] 3902 	orl	a,r3
      0025D9 F0               [24] 3903 	movx	@dptr,a
                                   3904 ;	..\src\COMMON\easyax5043.c:559: AX5043_FIFODATA = axradio_framing_syncflags | i;
      0025DA 90 90 EE         [24] 3905 	mov	dptr,#_axradio_framing_syncflags
      0025DD E4               [12] 3906 	clr	a
      0025DE 93               [24] 3907 	movc	a,@a+dptr
      0025DF FB               [12] 3908 	mov	r3,a
      0025E0 90 40 29         [24] 3909 	mov	dptr,#_AX5043_FIFODATA
      0025E3 EA               [12] 3910 	mov	a,r2
      0025E4 4B               [12] 3911 	orl	a,r3
      0025E5 F0               [24] 3912 	movx	@dptr,a
                                   3913 ;	..\src\COMMON\easyax5043.c:560: for (i = 0; i < len_byte; ++i) {
      0025E6 7B 00            [12] 3914 	mov	r3,#0x00
      0025E8                       3915 00155$:
      0025E8 C3               [12] 3916 	clr	c
      0025E9 EB               [12] 3917 	mov	a,r3
      0025EA 9C               [12] 3918 	subb	a,r4
      0025EB 50 0D            [24] 3919 	jnc	00125$
                                   3920 ;	..\src\COMMON\easyax5043.c:562: AX5043_FIFODATA = axradio_framing_syncword[i];
      0025ED EB               [12] 3921 	mov	a,r3
      0025EE 90 90 EA         [24] 3922 	mov	dptr,#_axradio_framing_syncword
      0025F1 93               [24] 3923 	movc	a,@a+dptr
      0025F2 FA               [12] 3924 	mov	r2,a
      0025F3 90 40 29         [24] 3925 	mov	dptr,#_AX5043_FIFODATA
      0025F6 F0               [24] 3926 	movx	@dptr,a
                                   3927 ;	..\src\COMMON\easyax5043.c:560: for (i = 0; i < len_byte; ++i) {
      0025F7 0B               [12] 3928 	inc	r3
      0025F8 80 EE            [24] 3929 	sjmp	00155$
      0025FA                       3930 00125$:
                                   3931 ;	..\src\COMMON\easyax5043.c:569: axradio_trxstate = trxstate_tx_packet;
      0025FA 75 18 0C         [24] 3932 	mov	_axradio_trxstate,#0x0c
                                   3933 ;	..\src\COMMON\easyax5043.c:570: break;
      0025FD 02 24 44         [24] 3934 	ljmp	00157$
      002600                       3935 00128$:
                                   3936 ;	..\src\COMMON\easyax5043.c:572: if (cnt < 4)
      002600 BF 04 00         [24] 3937 	cjne	r7,#0x04,00330$
      002603                       3938 00330$:
      002603 50 03            [24] 3939 	jnc	00331$
      002605 02 27 9E         [24] 3940 	ljmp	00153$
      002608                       3941 00331$:
                                   3942 ;	..\src\COMMON\easyax5043.c:574: cnt = 255;
      002608 7C FF            [12] 3943 	mov	r4,#0xff
                                   3944 ;	..\src\COMMON\easyax5043.c:575: if (axradio_txbuffer_cnt < 255*8)
      00260A C3               [12] 3945 	clr	c
      00260B ED               [12] 3946 	mov	a,r5
      00260C 94 F8            [12] 3947 	subb	a,#0xf8
      00260E EE               [12] 3948 	mov	a,r6
      00260F 94 07            [12] 3949 	subb	a,#0x07
      002611 50 12            [24] 3950 	jnc	00132$
                                   3951 ;	..\src\COMMON\easyax5043.c:576: cnt = axradio_txbuffer_cnt >> 3;
      002613 EE               [12] 3952 	mov	a,r6
      002614 C4               [12] 3953 	swap	a
      002615 23               [12] 3954 	rl	a
      002616 CD               [12] 3955 	xch	a,r5
      002617 C4               [12] 3956 	swap	a
      002618 23               [12] 3957 	rl	a
      002619 54 1F            [12] 3958 	anl	a,#0x1f
      00261B 6D               [12] 3959 	xrl	a,r5
      00261C CD               [12] 3960 	xch	a,r5
      00261D 54 1F            [12] 3961 	anl	a,#0x1f
      00261F CD               [12] 3962 	xch	a,r5
      002620 6D               [12] 3963 	xrl	a,r5
      002621 CD               [12] 3964 	xch	a,r5
      002622 FE               [12] 3965 	mov	r6,a
      002623 8D 04            [24] 3966 	mov	ar4,r5
      002625                       3967 00132$:
                                   3968 ;	..\src\COMMON\easyax5043.c:577: if (cnt) {
      002625 EC               [12] 3969 	mov	a,r4
      002626 60 41            [24] 3970 	jz	00134$
                                   3971 ;	..\src\COMMON\easyax5043.c:578: axradio_txbuffer_cnt -= ((uint16_t)cnt) << 3;
      002628 8C 05            [24] 3972 	mov	ar5,r4
      00262A E4               [12] 3973 	clr	a
      00262B 03               [12] 3974 	rr	a
      00262C 54 F8            [12] 3975 	anl	a,#0xf8
      00262E CD               [12] 3976 	xch	a,r5
      00262F C4               [12] 3977 	swap	a
      002630 03               [12] 3978 	rr	a
      002631 CD               [12] 3979 	xch	a,r5
      002632 6D               [12] 3980 	xrl	a,r5
      002633 CD               [12] 3981 	xch	a,r5
      002634 54 F8            [12] 3982 	anl	a,#0xf8
      002636 CD               [12] 3983 	xch	a,r5
      002637 6D               [12] 3984 	xrl	a,r5
      002638 FE               [12] 3985 	mov	r6,a
      002639 90 00 ED         [24] 3986 	mov	dptr,#_axradio_txbuffer_cnt
      00263C E0               [24] 3987 	movx	a,@dptr
      00263D FA               [12] 3988 	mov	r2,a
      00263E A3               [24] 3989 	inc	dptr
      00263F E0               [24] 3990 	movx	a,@dptr
      002640 FB               [12] 3991 	mov	r3,a
      002641 90 00 ED         [24] 3992 	mov	dptr,#_axradio_txbuffer_cnt
      002644 EA               [12] 3993 	mov	a,r2
      002645 C3               [12] 3994 	clr	c
      002646 9D               [12] 3995 	subb	a,r5
      002647 F0               [24] 3996 	movx	@dptr,a
      002648 EB               [12] 3997 	mov	a,r3
      002649 9E               [12] 3998 	subb	a,r6
      00264A A3               [24] 3999 	inc	dptr
      00264B F0               [24] 4000 	movx	@dptr,a
                                   4001 ;	..\src\COMMON\easyax5043.c:579: AX5043_FIFODATA = AX5043_FIFOCMD_REPEATDATA | (3 << 5);
      00264C 90 40 29         [24] 4002 	mov	dptr,#_AX5043_FIFODATA
      00264F 74 62            [12] 4003 	mov	a,#0x62
      002651 F0               [24] 4004 	movx	@dptr,a
                                   4005 ;	..\src\COMMON\easyax5043.c:580: AX5043_FIFODATA = axradio_phy_preamble_flags;
      002652 90 90 DE         [24] 4006 	mov	dptr,#_axradio_phy_preamble_flags
      002655 E4               [12] 4007 	clr	a
      002656 93               [24] 4008 	movc	a,@a+dptr
      002657 90 40 29         [24] 4009 	mov	dptr,#_AX5043_FIFODATA
      00265A F0               [24] 4010 	movx	@dptr,a
                                   4011 ;	..\src\COMMON\easyax5043.c:581: AX5043_FIFODATA = cnt;
      00265B EC               [12] 4012 	mov	a,r4
      00265C F0               [24] 4013 	movx	@dptr,a
                                   4014 ;	..\src\COMMON\easyax5043.c:582: AX5043_FIFODATA = axradio_phy_preamble_byte;
      00265D 90 90 DD         [24] 4015 	mov	dptr,#_axradio_phy_preamble_byte
      002660 E4               [12] 4016 	clr	a
      002661 93               [24] 4017 	movc	a,@a+dptr
      002662 90 40 29         [24] 4018 	mov	dptr,#_AX5043_FIFODATA
      002665 F0               [24] 4019 	movx	@dptr,a
                                   4020 ;	..\src\COMMON\easyax5043.c:583: break;
      002666 02 24 44         [24] 4021 	ljmp	00157$
      002669                       4022 00134$:
                                   4023 ;	..\src\COMMON\easyax5043.c:586: uint8_t byte = axradio_phy_preamble_byte;
      002669 90 90 DD         [24] 4024 	mov	dptr,#_axradio_phy_preamble_byte
      00266C E4               [12] 4025 	clr	a
      00266D 93               [24] 4026 	movc	a,@a+dptr
      00266E FE               [12] 4027 	mov	r6,a
                                   4028 ;	..\src\COMMON\easyax5043.c:587: cnt = axradio_txbuffer_cnt;
      00266F 90 00 ED         [24] 4029 	mov	dptr,#_axradio_txbuffer_cnt
      002672 E0               [24] 4030 	movx	a,@dptr
      002673 FC               [12] 4031 	mov	r4,a
      002674 A3               [24] 4032 	inc	dptr
      002675 E0               [24] 4033 	movx	a,@dptr
                                   4034 ;	..\src\COMMON\easyax5043.c:588: axradio_txbuffer_cnt = 0;
      002676 90 00 ED         [24] 4035 	mov	dptr,#_axradio_txbuffer_cnt
      002679 E4               [12] 4036 	clr	a
      00267A F0               [24] 4037 	movx	@dptr,a
      00267B A3               [24] 4038 	inc	dptr
      00267C F0               [24] 4039 	movx	@dptr,a
                                   4040 ;	..\src\COMMON\easyax5043.c:589: AX5043_FIFODATA = AX5043_FIFOCMD_DATA | (2 << 5);
      00267D 90 40 29         [24] 4041 	mov	dptr,#_AX5043_FIFODATA
      002680 74 41            [12] 4042 	mov	a,#0x41
      002682 F0               [24] 4043 	movx	@dptr,a
                                   4044 ;	..\src\COMMON\easyax5043.c:590: AX5043_FIFODATA = 0x1C;
      002683 74 1C            [12] 4045 	mov	a,#0x1c
      002685 F0               [24] 4046 	movx	@dptr,a
                                   4047 ;	..\src\COMMON\easyax5043.c:591: if (AX5043_PKTADDRCFG & 0x80) {
      002686 90 42 00         [24] 4048 	mov	dptr,#_AX5043_PKTADDRCFG
      002689 E0               [24] 4049 	movx	a,@dptr
      00268A 30 E7 36         [24] 4050 	jnb	acc.7,00136$
                                   4051 ;	..\src\COMMON\easyax5043.c:593: byte &= 0xFF << (8-cnt);
      00268D 8C 05            [24] 4052 	mov	ar5,r4
      00268F 74 08            [12] 4053 	mov	a,#0x08
      002691 C3               [12] 4054 	clr	c
      002692 9D               [12] 4055 	subb	a,r5
      002693 F5 F0            [12] 4056 	mov	b,a
      002695 05 F0            [12] 4057 	inc	b
      002697 74 FF            [12] 4058 	mov	a,#0xff
      002699 80 02            [24] 4059 	sjmp	00337$
      00269B                       4060 00335$:
      00269B 25 E0            [12] 4061 	add	a,acc
      00269D                       4062 00337$:
      00269D D5 F0 FB         [24] 4063 	djnz	b,00335$
      0026A0 FD               [12] 4064 	mov	r5,a
      0026A1 8E 03            [24] 4065 	mov	ar3,r6
      0026A3 EB               [12] 4066 	mov	a,r3
      0026A4 52 05            [12] 4067 	anl	ar5,a
                                   4068 ;	..\src\COMMON\easyax5043.c:594: byte |= 0x80 >> cnt;
      0026A6 8C F0            [24] 4069 	mov	b,r4
      0026A8 05 F0            [12] 4070 	inc	b
      0026AA 7A 80            [12] 4071 	mov	r2,#0x80
      0026AC E4               [12] 4072 	clr	a
      0026AD FB               [12] 4073 	mov	r3,a
      0026AE 33               [12] 4074 	rlc	a
      0026AF 92 D2            [24] 4075 	mov	ov,c
      0026B1 80 08            [24] 4076 	sjmp	00339$
      0026B3                       4077 00338$:
      0026B3 A2 D2            [12] 4078 	mov	c,ov
      0026B5 EB               [12] 4079 	mov	a,r3
      0026B6 13               [12] 4080 	rrc	a
      0026B7 FB               [12] 4081 	mov	r3,a
      0026B8 EA               [12] 4082 	mov	a,r2
      0026B9 13               [12] 4083 	rrc	a
      0026BA FA               [12] 4084 	mov	r2,a
      0026BB                       4085 00339$:
      0026BB D5 F0 F5         [24] 4086 	djnz	b,00338$
      0026BE EA               [12] 4087 	mov	a,r2
      0026BF 42 05            [12] 4088 	orl	ar5,a
      0026C1 80 30            [24] 4089 	sjmp	00137$
      0026C3                       4090 00136$:
                                   4091 ;	..\src\COMMON\easyax5043.c:597: byte &= 0xFF >> (8-cnt);
      0026C3 74 08            [12] 4092 	mov	a,#0x08
      0026C5 C3               [12] 4093 	clr	c
      0026C6 9C               [12] 4094 	subb	a,r4
      0026C7 FB               [12] 4095 	mov	r3,a
      0026C8 8B F0            [24] 4096 	mov	b,r3
      0026CA 05 F0            [12] 4097 	inc	b
      0026CC 7B FF            [12] 4098 	mov	r3,#0xff
      0026CE E4               [12] 4099 	clr	a
      0026CF FA               [12] 4100 	mov	r2,a
      0026D0 33               [12] 4101 	rlc	a
      0026D1 92 D2            [24] 4102 	mov	ov,c
      0026D3 80 08            [24] 4103 	sjmp	00341$
      0026D5                       4104 00340$:
      0026D5 A2 D2            [12] 4105 	mov	c,ov
      0026D7 EA               [12] 4106 	mov	a,r2
      0026D8 13               [12] 4107 	rrc	a
      0026D9 FA               [12] 4108 	mov	r2,a
      0026DA EB               [12] 4109 	mov	a,r3
      0026DB 13               [12] 4110 	rrc	a
      0026DC FB               [12] 4111 	mov	r3,a
      0026DD                       4112 00341$:
      0026DD D5 F0 F5         [24] 4113 	djnz	b,00340$
      0026E0 EE               [12] 4114 	mov	a,r6
      0026E1 52 03            [12] 4115 	anl	ar3,a
                                   4116 ;	..\src\COMMON\easyax5043.c:598: byte |= 0x01 << cnt;
      0026E3 8C F0            [24] 4117 	mov	b,r4
      0026E5 05 F0            [12] 4118 	inc	b
      0026E7 74 01            [12] 4119 	mov	a,#0x01
      0026E9 80 02            [24] 4120 	sjmp	00344$
      0026EB                       4121 00342$:
      0026EB 25 E0            [12] 4122 	add	a,acc
      0026ED                       4123 00344$:
      0026ED D5 F0 FB         [24] 4124 	djnz	b,00342$
      0026F0 FC               [12] 4125 	mov	r4,a
      0026F1 4B               [12] 4126 	orl	a,r3
      0026F2 FD               [12] 4127 	mov	r5,a
      0026F3                       4128 00137$:
                                   4129 ;	..\src\COMMON\easyax5043.c:600: AX5043_FIFODATA = byte;
      0026F3 90 40 29         [24] 4130 	mov	dptr,#_AX5043_FIFODATA
      0026F6 ED               [12] 4131 	mov	a,r5
      0026F7 F0               [24] 4132 	movx	@dptr,a
                                   4133 ;	..\src\COMMON\easyax5043.c:602: break;
      0026F8 02 24 44         [24] 4134 	ljmp	00157$
                                   4135 ;	..\src\COMMON\easyax5043.c:604: case trxstate_tx_packet:
      0026FB                       4136 00138$:
                                   4137 ;	..\src\COMMON\easyax5043.c:605: if (cnt < 11)
      0026FB BF 0B 00         [24] 4138 	cjne	r7,#0x0b,00345$
      0026FE                       4139 00345$:
      0026FE 50 03            [24] 4140 	jnc	00346$
      002700 02 27 9E         [24] 4141 	ljmp	00153$
      002703                       4142 00346$:
                                   4143 ;	..\src\COMMON\easyax5043.c:608: uint8_t flags = 0;
      002703 7E 00            [12] 4144 	mov	r6,#0x00
                                   4145 ;	..\src\COMMON\easyax5043.c:609: if (!axradio_txbuffer_cnt)
      002705 90 00 ED         [24] 4146 	mov	dptr,#_axradio_txbuffer_cnt
      002708 E0               [24] 4147 	movx	a,@dptr
      002709 F5 F0            [12] 4148 	mov	b,a
      00270B A3               [24] 4149 	inc	dptr
      00270C E0               [24] 4150 	movx	a,@dptr
      00270D 45 F0            [12] 4151 	orl	a,b
      00270F 70 02            [24] 4152 	jnz	00142$
                                   4153 ;	..\src\COMMON\easyax5043.c:610: flags |= 0x01; // flag byte: pkt_start
      002711 7E 01            [12] 4154 	mov	r6,#0x01
      002713                       4155 00142$:
                                   4156 ;	..\src\COMMON\easyax5043.c:612: uint16_t len = axradio_txbuffer_len - axradio_txbuffer_cnt;
      002713 90 00 ED         [24] 4157 	mov	dptr,#_axradio_txbuffer_cnt
      002716 E0               [24] 4158 	movx	a,@dptr
      002717 FC               [12] 4159 	mov	r4,a
      002718 A3               [24] 4160 	inc	dptr
      002719 E0               [24] 4161 	movx	a,@dptr
      00271A FD               [12] 4162 	mov	r5,a
      00271B 90 00 EB         [24] 4163 	mov	dptr,#_axradio_txbuffer_len
      00271E E0               [24] 4164 	movx	a,@dptr
      00271F FA               [12] 4165 	mov	r2,a
      002720 A3               [24] 4166 	inc	dptr
      002721 E0               [24] 4167 	movx	a,@dptr
      002722 FB               [12] 4168 	mov	r3,a
      002723 EA               [12] 4169 	mov	a,r2
      002724 C3               [12] 4170 	clr	c
      002725 9C               [12] 4171 	subb	a,r4
      002726 FC               [12] 4172 	mov	r4,a
      002727 EB               [12] 4173 	mov	a,r3
      002728 9D               [12] 4174 	subb	a,r5
      002729 FD               [12] 4175 	mov	r5,a
                                   4176 ;	..\src\COMMON\easyax5043.c:613: cnt -= 3;
      00272A 1F               [12] 4177 	dec	r7
      00272B 1F               [12] 4178 	dec	r7
      00272C 1F               [12] 4179 	dec	r7
                                   4180 ;	..\src\COMMON\easyax5043.c:614: if (cnt >= len) {
      00272D 8F 02            [24] 4181 	mov	ar2,r7
      00272F 7B 00            [12] 4182 	mov	r3,#0x00
      002731 C3               [12] 4183 	clr	c
      002732 EA               [12] 4184 	mov	a,r2
      002733 9C               [12] 4185 	subb	a,r4
      002734 EB               [12] 4186 	mov	a,r3
      002735 9D               [12] 4187 	subb	a,r5
      002736 40 05            [24] 4188 	jc	00144$
                                   4189 ;	..\src\COMMON\easyax5043.c:615: cnt = len;
      002738 8C 07            [24] 4190 	mov	ar7,r4
                                   4191 ;	..\src\COMMON\easyax5043.c:616: flags |= 0x02; // flag byte: pkt_end
      00273A 43 06 02         [24] 4192 	orl	ar6,#0x02
      00273D                       4193 00144$:
                                   4194 ;	..\src\COMMON\easyax5043.c:619: if (!cnt)
      00273D EF               [12] 4195 	mov	a,r7
      00273E 60 4F            [24] 4196 	jz	00152$
                                   4197 ;	..\src\COMMON\easyax5043.c:621: AX5043_FIFODATA = AX5043_FIFOCMD_DATA | (7 << 5);
      002740 90 40 29         [24] 4198 	mov	dptr,#_AX5043_FIFODATA
      002743 74 E1            [12] 4199 	mov	a,#0xe1
      002745 F0               [24] 4200 	movx	@dptr,a
                                   4201 ;	..\src\COMMON\easyax5043.c:622: AX5043_FIFODATA = cnt + 1; // write FIFO chunk length byte (length includes the flag byte, thus the +1)
      002746 8F 05            [24] 4202 	mov	ar5,r7
      002748 ED               [12] 4203 	mov	a,r5
      002749 04               [12] 4204 	inc	a
      00274A F0               [24] 4205 	movx	@dptr,a
                                   4206 ;	..\src\COMMON\easyax5043.c:623: AX5043_FIFODATA = flags;
      00274B EE               [12] 4207 	mov	a,r6
      00274C F0               [24] 4208 	movx	@dptr,a
                                   4209 ;	..\src\COMMON\easyax5043.c:624: ax5043_writefifo(&axradio_txbuffer[axradio_txbuffer_cnt], cnt);
      00274D 90 00 ED         [24] 4210 	mov	dptr,#_axradio_txbuffer_cnt
      002750 E0               [24] 4211 	movx	a,@dptr
      002751 FC               [12] 4212 	mov	r4,a
      002752 A3               [24] 4213 	inc	dptr
      002753 E0               [24] 4214 	movx	a,@dptr
      002754 FD               [12] 4215 	mov	r5,a
      002755 EC               [12] 4216 	mov	a,r4
      002756 24 10            [12] 4217 	add	a,#_axradio_txbuffer
      002758 FC               [12] 4218 	mov	r4,a
      002759 ED               [12] 4219 	mov	a,r5
      00275A 34 01            [12] 4220 	addc	a,#(_axradio_txbuffer >> 8)
      00275C FD               [12] 4221 	mov	r5,a
      00275D 7B 00            [12] 4222 	mov	r3,#0x00
      00275F C0 07            [24] 4223 	push	ar7
      002761 C0 06            [24] 4224 	push	ar6
      002763 C0 07            [24] 4225 	push	ar7
      002765 8C 82            [24] 4226 	mov	dpl,r4
      002767 8D 83            [24] 4227 	mov	dph,r5
      002769 8B F0            [24] 4228 	mov	b,r3
      00276B 12 89 29         [24] 4229 	lcall	_ax5043_writefifo
      00276E 15 81            [12] 4230 	dec	sp
      002770 D0 06            [24] 4231 	pop	ar6
      002772 D0 07            [24] 4232 	pop	ar7
                                   4233 ;	..\src\COMMON\easyax5043.c:625: axradio_txbuffer_cnt += cnt;
      002774 7D 00            [12] 4234 	mov	r5,#0x00
      002776 90 00 ED         [24] 4235 	mov	dptr,#_axradio_txbuffer_cnt
      002779 E0               [24] 4236 	movx	a,@dptr
      00277A FB               [12] 4237 	mov	r3,a
      00277B A3               [24] 4238 	inc	dptr
      00277C E0               [24] 4239 	movx	a,@dptr
      00277D FC               [12] 4240 	mov	r4,a
      00277E 90 00 ED         [24] 4241 	mov	dptr,#_axradio_txbuffer_cnt
      002781 EF               [12] 4242 	mov	a,r7
      002782 2B               [12] 4243 	add	a,r3
      002783 F0               [24] 4244 	movx	@dptr,a
      002784 ED               [12] 4245 	mov	a,r5
      002785 3C               [12] 4246 	addc	a,r4
      002786 A3               [24] 4247 	inc	dptr
      002787 F0               [24] 4248 	movx	@dptr,a
                                   4249 ;	..\src\COMMON\easyax5043.c:626: if (flags & 0x02)
      002788 EE               [12] 4250 	mov	a,r6
      002789 20 E1 03         [24] 4251 	jb	acc.1,00152$
                                   4252 ;	..\src\COMMON\easyax5043.c:627: goto pktend;
                                   4253 ;	..\src\COMMON\easyax5043.c:631: default:
                                   4254 ;	..\src\COMMON\easyax5043.c:632: return;
                                   4255 ;	..\src\COMMON\easyax5043.c:635: pktend:
      00278C 02 24 44         [24] 4256 	ljmp	00157$
      00278F                       4257 00152$:
                                   4258 ;	..\src\COMMON\easyax5043.c:636: axradio_trxstate = trxstate_tx_waitdone;
      00278F 75 18 0D         [24] 4259 	mov	_axradio_trxstate,#0x0d
                                   4260 ;	..\src\COMMON\easyax5043.c:637: AX5043_RADIOEVENTMASK0 = 0x01; // enable REVRDONE event
      002792 90 40 09         [24] 4261 	mov	dptr,#_AX5043_RADIOEVENTMASK0
      002795 74 01            [12] 4262 	mov	a,#0x01
      002797 F0               [24] 4263 	movx	@dptr,a
                                   4264 ;	..\src\COMMON\easyax5043.c:638: AX5043_IRQMASK0 = 0x40; // enable radio controller irq
      002798 90 40 07         [24] 4265 	mov	dptr,#_AX5043_IRQMASK0
      00279B 74 40            [12] 4266 	mov	a,#0x40
      00279D F0               [24] 4267 	movx	@dptr,a
                                   4268 ;	..\src\COMMON\easyax5043.c:639: fifocommit:
      00279E                       4269 00153$:
                                   4270 ;	..\src\COMMON\easyax5043.c:640: AX5043_FIFOSTAT = 4; // commit
      00279E 90 40 28         [24] 4271 	mov	dptr,#_AX5043_FIFOSTAT
      0027A1 74 04            [12] 4272 	mov	a,#0x04
      0027A3 F0               [24] 4273 	movx	@dptr,a
                                   4274 ;	..\src\COMMON\easyax5043.c:641: }
      0027A4 22               [24] 4275 	ret
                                   4276 ;------------------------------------------------------------
                                   4277 ;Allocation info for local variables in function 'axradio_isr'
                                   4278 ;------------------------------------------------------------
                                   4279 ;evt                       Allocated to registers r7 
                                   4280 ;------------------------------------------------------------
                                   4281 ;	..\src\COMMON\easyax5043.c:644: void axradio_isr(void) __interrupt INT_RADIO
                                   4282 ;	-----------------------------------------
                                   4283 ;	 function axradio_isr
                                   4284 ;	-----------------------------------------
      0027A5                       4285 _axradio_isr:
      0027A5 C0 20            [24] 4286 	push	bits
      0027A7 C0 E0            [24] 4287 	push	acc
      0027A9 C0 F0            [24] 4288 	push	b
      0027AB C0 82            [24] 4289 	push	dpl
      0027AD C0 83            [24] 4290 	push	dph
      0027AF C0 07            [24] 4291 	push	(0+7)
      0027B1 C0 06            [24] 4292 	push	(0+6)
      0027B3 C0 05            [24] 4293 	push	(0+5)
      0027B5 C0 04            [24] 4294 	push	(0+4)
      0027B7 C0 03            [24] 4295 	push	(0+3)
      0027B9 C0 02            [24] 4296 	push	(0+2)
      0027BB C0 01            [24] 4297 	push	(0+1)
      0027BD C0 00            [24] 4298 	push	(0+0)
      0027BF C0 D0            [24] 4299 	push	psw
      0027C1 75 D0 00         [24] 4300 	mov	psw,#0x00
                                   4301 ;	..\src\COMMON\easyax5043.c:654: switch (axradio_trxstate) {
      0027C4 E5 18            [12] 4302 	mov	a,_axradio_trxstate
      0027C6 FF               [12] 4303 	mov	r7,a
      0027C7 24 EF            [12] 4304 	add	a,#0xff - 0x10
      0027C9 50 03            [24] 4305 	jnc	00285$
      0027CB 02 28 01         [24] 4306 	ljmp	00101$
      0027CE                       4307 00285$:
      0027CE EF               [12] 4308 	mov	a,r7
      0027CF F5 F0            [12] 4309 	mov	b,a
      0027D1 24 0B            [12] 4310 	add	a,#(00286$-3-.)
      0027D3 83               [24] 4311 	movc	a,@a+pc
      0027D4 F5 82            [12] 4312 	mov	dpl,a
      0027D6 E5 F0            [12] 4313 	mov	a,b
      0027D8 24 15            [12] 4314 	add	a,#(00287$-3-.)
      0027DA 83               [24] 4315 	movc	a,@a+pc
      0027DB F5 83            [12] 4316 	mov	dph,a
      0027DD E4               [12] 4317 	clr	a
      0027DE 73               [24] 4318 	jmp	@a+dptr
      0027DF                       4319 00286$:
      0027DF 01                    4320 	.db	00101$
      0027E0 7B                    4321 	.db	00165$
      0027E1 29                    4322 	.db	00158$
      0027E2 0D                    4323 	.db	00102$
      0027E3 01                    4324 	.db	00101$
      0027E4 18                    4325 	.db	00103$
      0027E5 01                    4326 	.db	00101$
      0027E6 23                    4327 	.db	00104$
      0027E7 01                    4328 	.db	00101$
      0027E8 2E                    4329 	.db	00105$
      0027E9 C3                    4330 	.db	00116$
      0027EA C3                    4331 	.db	00116$
      0027EB C3                    4332 	.db	00116$
      0027EC C9                    4333 	.db	00117$
      0027ED FD                    4334 	.db	00144$
      0027EE 42                    4335 	.db	00145$
      0027EF 68                    4336 	.db	00148$
      0027F0                       4337 00287$:
      0027F0 28                    4338 	.db	00101$>>8
      0027F1 2B                    4339 	.db	00165$>>8
      0027F2 2B                    4340 	.db	00158$>>8
      0027F3 28                    4341 	.db	00102$>>8
      0027F4 28                    4342 	.db	00101$>>8
      0027F5 28                    4343 	.db	00103$>>8
      0027F6 28                    4344 	.db	00101$>>8
      0027F7 28                    4345 	.db	00104$>>8
      0027F8 28                    4346 	.db	00101$>>8
      0027F9 28                    4347 	.db	00105$>>8
      0027FA 28                    4348 	.db	00116$>>8
      0027FB 28                    4349 	.db	00116$>>8
      0027FC 28                    4350 	.db	00116$>>8
      0027FD 28                    4351 	.db	00117$>>8
      0027FE 29                    4352 	.db	00144$>>8
      0027FF 2A                    4353 	.db	00145$>>8
      002800 2A                    4354 	.db	00148$>>8
                                   4355 ;	..\src\COMMON\easyax5043.c:655: default:
      002801                       4356 00101$:
                                   4357 ;	..\src\COMMON\easyax5043.c:656: AX5043_IRQMASK1 = 0x00;
      002801 90 40 06         [24] 4358 	mov	dptr,#_AX5043_IRQMASK1
      002804 E4               [12] 4359 	clr	a
      002805 F0               [24] 4360 	movx	@dptr,a
                                   4361 ;	..\src\COMMON\easyax5043.c:657: AX5043_IRQMASK0 = 0x00;
      002806 90 40 07         [24] 4362 	mov	dptr,#_AX5043_IRQMASK0
      002809 F0               [24] 4363 	movx	@dptr,a
                                   4364 ;	..\src\COMMON\easyax5043.c:658: break;
      00280A 02 2B 7E         [24] 4365 	ljmp	00167$
                                   4366 ;	..\src\COMMON\easyax5043.c:660: case trxstate_wait_xtal:
      00280D                       4367 00102$:
                                   4368 ;	..\src\COMMON\easyax5043.c:661: AX5043_IRQMASK1 = 0x00; // otherwise crystal ready will fire all over again
      00280D 90 40 06         [24] 4369 	mov	dptr,#_AX5043_IRQMASK1
      002810 E4               [12] 4370 	clr	a
      002811 F0               [24] 4371 	movx	@dptr,a
                                   4372 ;	..\src\COMMON\easyax5043.c:662: axradio_trxstate = trxstate_xtal_ready;
      002812 75 18 04         [24] 4373 	mov	_axradio_trxstate,#0x04
                                   4374 ;	..\src\COMMON\easyax5043.c:663: break;
      002815 02 2B 7E         [24] 4375 	ljmp	00167$
                                   4376 ;	..\src\COMMON\easyax5043.c:665: case trxstate_pll_ranging:
      002818                       4377 00103$:
                                   4378 ;	..\src\COMMON\easyax5043.c:666: AX5043_IRQMASK1 = 0x00; // otherwise autoranging done will fire all over again
      002818 90 40 06         [24] 4379 	mov	dptr,#_AX5043_IRQMASK1
      00281B E4               [12] 4380 	clr	a
      00281C F0               [24] 4381 	movx	@dptr,a
                                   4382 ;	..\src\COMMON\easyax5043.c:667: axradio_trxstate = trxstate_pll_ranging_done;
      00281D 75 18 06         [24] 4383 	mov	_axradio_trxstate,#0x06
                                   4384 ;	..\src\COMMON\easyax5043.c:668: break;
      002820 02 2B 7E         [24] 4385 	ljmp	00167$
                                   4386 ;	..\src\COMMON\easyax5043.c:670: case trxstate_pll_settling:
      002823                       4387 00104$:
                                   4388 ;	..\src\COMMON\easyax5043.c:671: AX5043_RADIOEVENTMASK0 = 0x00;
      002823 90 40 09         [24] 4389 	mov	dptr,#_AX5043_RADIOEVENTMASK0
      002826 E4               [12] 4390 	clr	a
      002827 F0               [24] 4391 	movx	@dptr,a
                                   4392 ;	..\src\COMMON\easyax5043.c:672: axradio_trxstate = trxstate_pll_settled;
      002828 75 18 08         [24] 4393 	mov	_axradio_trxstate,#0x08
                                   4394 ;	..\src\COMMON\easyax5043.c:673: break;
      00282B 02 2B 7E         [24] 4395 	ljmp	00167$
                                   4396 ;	..\src\COMMON\easyax5043.c:675: case trxstate_tx_xtalwait:
      00282E                       4397 00105$:
                                   4398 ;	..\src\COMMON\easyax5043.c:676: AX5043_RADIOEVENTREQ0; // make sure REVRDONE bit is cleared, so it is a reliable indicator that the packet is out
      00282E 90 40 0F         [24] 4399 	mov	dptr,#_AX5043_RADIOEVENTREQ0
      002831 E0               [24] 4400 	movx	a,@dptr
                                   4401 ;	..\src\COMMON\easyax5043.c:677: AX5043_FIFOSTAT = 3; // clear FIFO data & flags (prevent transmitting anything left over in the FIFO, this has no effect if the FIFO is not powerered, in this case it is reset any way)
      002832 90 40 28         [24] 4402 	mov	dptr,#_AX5043_FIFOSTAT
      002835 74 03            [12] 4403 	mov	a,#0x03
      002837 F0               [24] 4404 	movx	@dptr,a
                                   4405 ;	..\src\COMMON\easyax5043.c:678: AX5043_IRQMASK1 = 0x00;
      002838 90 40 06         [24] 4406 	mov	dptr,#_AX5043_IRQMASK1
      00283B E4               [12] 4407 	clr	a
      00283C F0               [24] 4408 	movx	@dptr,a
                                   4409 ;	..\src\COMMON\easyax5043.c:679: AX5043_IRQMASK0 = 0x08; // enable fifo free threshold
      00283D 90 40 07         [24] 4410 	mov	dptr,#_AX5043_IRQMASK0
      002840 74 08            [12] 4411 	mov	a,#0x08
      002842 F0               [24] 4412 	movx	@dptr,a
                                   4413 ;	..\src\COMMON\easyax5043.c:680: axradio_trxstate = trxstate_tx_longpreamble;
      002843 75 18 0A         [24] 4414 	mov	_axradio_trxstate,#0x0a
                                   4415 ;	..\src\COMMON\easyax5043.c:682: if ((AX5043_MODULATION & 0x0F) == 9) { // 4-FSK
      002846 90 40 10         [24] 4416 	mov	dptr,#_AX5043_MODULATION
      002849 E0               [24] 4417 	movx	a,@dptr
      00284A FF               [12] 4418 	mov	r7,a
      00284B 53 07 0F         [24] 4419 	anl	ar7,#0x0f
      00284E 7E 00            [12] 4420 	mov	r6,#0x00
      002850 BF 09 11         [24] 4421 	cjne	r7,#0x09,00107$
      002853 BE 00 0E         [24] 4422 	cjne	r6,#0x00,00107$
                                   4423 ;	..\src\COMMON\easyax5043.c:683: AX5043_FIFODATA = AX5043_FIFOCMD_DATA | (7 << 5);
      002856 90 40 29         [24] 4424 	mov	dptr,#_AX5043_FIFODATA
      002859 74 E1            [12] 4425 	mov	a,#0xe1
      00285B F0               [24] 4426 	movx	@dptr,a
                                   4427 ;	..\src\COMMON\easyax5043.c:684: AX5043_FIFODATA = 2;  // length (including flags)
      00285C 74 02            [12] 4428 	mov	a,#0x02
      00285E F0               [24] 4429 	movx	@dptr,a
                                   4430 ;	..\src\COMMON\easyax5043.c:685: AX5043_FIFODATA = 0x01;  // flag PKTSTART -> dibit sync
      00285F 14               [12] 4431 	dec	a
      002860 F0               [24] 4432 	movx	@dptr,a
                                   4433 ;	..\src\COMMON\easyax5043.c:686: AX5043_FIFODATA = 0x11; // dummy byte for forcing dibit sync
      002861 74 11            [12] 4434 	mov	a,#0x11
      002863 F0               [24] 4435 	movx	@dptr,a
      002864                       4436 00107$:
                                   4437 ;	..\src\COMMON\easyax5043.c:693: transmit_isr();
      002864 12 24 44         [24] 4438 	lcall	_transmit_isr
                                   4439 ;	..\src\COMMON\easyax5043.c:694: AX5043_PWRMODE = AX5043_PWRSTATE_FULL_TX;
      002867 90 40 02         [24] 4440 	mov	dptr,#_AX5043_PWRMODE
      00286A 74 0D            [12] 4441 	mov	a,#0x0d
      00286C F0               [24] 4442 	movx	@dptr,a
                                   4443 ;	..\src\COMMON\easyax5043.c:695: update_timeanchor();
      00286D 12 1E 70         [24] 4444 	lcall	_update_timeanchor
                                   4445 ;	..\src\COMMON\easyax5043.c:696: wtimer_remove_callback(&axradio_cb_transmitstart.cb);
      002870 90 03 51         [24] 4446 	mov	dptr,#_axradio_cb_transmitstart
      002873 12 87 AB         [24] 4447 	lcall	_wtimer_remove_callback
                                   4448 ;	..\src\COMMON\easyax5043.c:697: switch (axradio_mode) {
      002876 AF 17            [24] 4449 	mov	r7,_axradio_mode
      002878 BF 12 02         [24] 4450 	cjne	r7,#0x12,00290$
      00287B 80 03            [24] 4451 	sjmp	00109$
      00287D                       4452 00290$:
      00287D BF 13 19         [24] 4453 	cjne	r7,#0x13,00112$
                                   4454 ;	..\src\COMMON\easyax5043.c:699: case AXRADIO_MODE_WOR_ACK_TRANSMIT:
      002880                       4455 00109$:
                                   4456 ;	..\src\COMMON\easyax5043.c:700: if (axradio_ack_count != axradio_framing_ack_retransmissions) {
      002880 90 00 F4         [24] 4457 	mov	dptr,#_axradio_ack_count
      002883 E0               [24] 4458 	movx	a,@dptr
      002884 FF               [12] 4459 	mov	r7,a
      002885 90 90 F8         [24] 4460 	mov	dptr,#_axradio_framing_ack_retransmissions
      002888 E4               [12] 4461 	clr	a
      002889 93               [24] 4462 	movc	a,@a+dptr
      00288A FE               [12] 4463 	mov	r6,a
      00288B EF               [12] 4464 	mov	a,r7
      00288C B5 06 02         [24] 4465 	cjne	a,ar6,00293$
      00288F 80 08            [24] 4466 	sjmp	00112$
      002891                       4467 00293$:
                                   4468 ;	..\src\COMMON\easyax5043.c:701: axradio_cb_transmitstart.st.error = AXRADIO_ERR_RETRANSMISSION;
      002891 90 03 56         [24] 4469 	mov	dptr,#(_axradio_cb_transmitstart + 0x0005)
      002894 74 08            [12] 4470 	mov	a,#0x08
      002896 F0               [24] 4471 	movx	@dptr,a
                                   4472 ;	..\src\COMMON\easyax5043.c:702: break;
                                   4473 ;	..\src\COMMON\easyax5043.c:705: default:
      002897 80 05            [24] 4474 	sjmp	00113$
      002899                       4475 00112$:
                                   4476 ;	..\src\COMMON\easyax5043.c:706: axradio_cb_transmitstart.st.error = AXRADIO_ERR_NOERROR;
      002899 90 03 56         [24] 4477 	mov	dptr,#(_axradio_cb_transmitstart + 0x0005)
      00289C E4               [12] 4478 	clr	a
      00289D F0               [24] 4479 	movx	@dptr,a
                                   4480 ;	..\src\COMMON\easyax5043.c:708: }
      00289E                       4481 00113$:
                                   4482 ;	..\src\COMMON\easyax5043.c:709: axradio_cb_transmitstart.st.time.t = axradio_timeanchor.radiotimer;
      00289E 90 01 00         [24] 4483 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      0028A1 E0               [24] 4484 	movx	a,@dptr
      0028A2 FC               [12] 4485 	mov	r4,a
      0028A3 A3               [24] 4486 	inc	dptr
      0028A4 E0               [24] 4487 	movx	a,@dptr
      0028A5 FD               [12] 4488 	mov	r5,a
      0028A6 A3               [24] 4489 	inc	dptr
      0028A7 E0               [24] 4490 	movx	a,@dptr
      0028A8 FE               [12] 4491 	mov	r6,a
      0028A9 A3               [24] 4492 	inc	dptr
      0028AA E0               [24] 4493 	movx	a,@dptr
      0028AB FF               [12] 4494 	mov	r7,a
      0028AC 90 03 57         [24] 4495 	mov	dptr,#(_axradio_cb_transmitstart + 0x0006)
      0028AF EC               [12] 4496 	mov	a,r4
      0028B0 F0               [24] 4497 	movx	@dptr,a
      0028B1 ED               [12] 4498 	mov	a,r5
      0028B2 A3               [24] 4499 	inc	dptr
      0028B3 F0               [24] 4500 	movx	@dptr,a
      0028B4 EE               [12] 4501 	mov	a,r6
      0028B5 A3               [24] 4502 	inc	dptr
      0028B6 F0               [24] 4503 	movx	@dptr,a
      0028B7 EF               [12] 4504 	mov	a,r7
      0028B8 A3               [24] 4505 	inc	dptr
      0028B9 F0               [24] 4506 	movx	@dptr,a
                                   4507 ;	..\src\COMMON\easyax5043.c:710: wtimer_add_callback(&axradio_cb_transmitstart.cb);
      0028BA 90 03 51         [24] 4508 	mov	dptr,#_axradio_cb_transmitstart
      0028BD 12 78 5E         [24] 4509 	lcall	_wtimer_add_callback
                                   4510 ;	..\src\COMMON\easyax5043.c:711: break;
      0028C0 02 2B 7E         [24] 4511 	ljmp	00167$
                                   4512 ;	..\src\COMMON\easyax5043.c:715: case trxstate_tx_packet:
      0028C3                       4513 00116$:
                                   4514 ;	..\src\COMMON\easyax5043.c:716: transmit_isr();
      0028C3 12 24 44         [24] 4515 	lcall	_transmit_isr
                                   4516 ;	..\src\COMMON\easyax5043.c:717: break;
      0028C6 02 2B 7E         [24] 4517 	ljmp	00167$
                                   4518 ;	..\src\COMMON\easyax5043.c:719: case trxstate_tx_waitdone:
      0028C9                       4519 00117$:
                                   4520 ;	..\src\COMMON\easyax5043.c:720: AX5043_RADIOEVENTREQ0;
      0028C9 90 40 0F         [24] 4521 	mov	dptr,#_AX5043_RADIOEVENTREQ0
      0028CC E0               [24] 4522 	movx	a,@dptr
                                   4523 ;	..\src\COMMON\easyax5043.c:721: if (AX5043_RADIOSTATE != 0)
      0028CD 90 40 1C         [24] 4524 	mov	dptr,#_AX5043_RADIOSTATE
      0028D0 E0               [24] 4525 	movx	a,@dptr
      0028D1 60 03            [24] 4526 	jz	00294$
      0028D3 02 2B 7E         [24] 4527 	ljmp	00167$
      0028D6                       4528 00294$:
                                   4529 ;	..\src\COMMON\easyax5043.c:723: AX5043_RADIOEVENTMASK0 = 0x00;
      0028D6 90 40 09         [24] 4530 	mov	dptr,#_AX5043_RADIOEVENTMASK0
      0028D9 E4               [12] 4531 	clr	a
      0028DA F0               [24] 4532 	movx	@dptr,a
                                   4533 ;	..\src\COMMON\easyax5043.c:724: switch (axradio_mode) {
      0028DB AF 17            [24] 4534 	mov	r7,_axradio_mode
      0028DD BF 12 02         [24] 4535 	cjne	r7,#0x12,00295$
      0028E0 80 6A            [24] 4536 	sjmp	00131$
      0028E2                       4537 00295$:
      0028E2 BF 13 02         [24] 4538 	cjne	r7,#0x13,00296$
      0028E5 80 65            [24] 4539 	sjmp	00131$
      0028E7                       4540 00296$:
      0028E7 BF 20 02         [24] 4541 	cjne	r7,#0x20,00297$
      0028EA 80 1D            [24] 4542 	sjmp	00120$
      0028EC                       4543 00297$:
      0028EC BF 21 02         [24] 4544 	cjne	r7,#0x21,00298$
      0028EF 80 36            [24] 4545 	sjmp	00125$
      0028F1                       4546 00298$:
      0028F1 BF 22 02         [24] 4547 	cjne	r7,#0x22,00299$
      0028F4 80 1C            [24] 4548 	sjmp	00121$
      0028F6                       4549 00299$:
      0028F6 BF 23 02         [24] 4550 	cjne	r7,#0x23,00300$
      0028F9 80 3C            [24] 4551 	sjmp	00128$
      0028FB                       4552 00300$:
      0028FB BF 30 03         [24] 4553 	cjne	r7,#0x30,00301$
      0028FE 02 29 80         [24] 4554 	ljmp	00132$
      002901                       4555 00301$:
      002901 BF 31 02         [24] 4556 	cjne	r7,#0x31,00302$
      002904 80 39            [24] 4557 	sjmp	00129$
      002906                       4558 00302$:
      002906 02 29 8D         [24] 4559 	ljmp	00133$
                                   4560 ;	..\src\COMMON\easyax5043.c:725: case AXRADIO_MODE_ASYNC_RECEIVE:
      002909                       4561 00120$:
                                   4562 ;	..\src\COMMON\easyax5043.c:727: ax5043_init_registers_rx();
      002909 12 1F 50         [24] 4563 	lcall	_ax5043_init_registers_rx
                                   4564 ;	..\src\COMMON\easyax5043.c:728: ax5043_receiver_on_continuous();
      00290C 12 2B 9B         [24] 4565 	lcall	_ax5043_receiver_on_continuous
                                   4566 ;	..\src\COMMON\easyax5043.c:729: break;
      00290F 02 29 90         [24] 4567 	ljmp	00134$
                                   4568 ;	..\src\COMMON\easyax5043.c:731: case AXRADIO_MODE_ACK_RECEIVE:
      002912                       4569 00121$:
                                   4570 ;	..\src\COMMON\easyax5043.c:732: if (axradio_cb_receive.st.error == AXRADIO_ERR_PACKETDONE) {
      002912 90 03 1D         [24] 4571 	mov	dptr,#(_axradio_cb_receive + 0x0005)
      002915 E0               [24] 4572 	movx	a,@dptr
      002916 FF               [12] 4573 	mov	r7,a
      002917 BF F0 08         [24] 4574 	cjne	r7,#0xf0,00124$
                                   4575 ;	..\src\COMMON\easyax5043.c:733: ax5043_init_registers_rx();
      00291A 12 1F 50         [24] 4576 	lcall	_ax5043_init_registers_rx
                                   4577 ;	..\src\COMMON\easyax5043.c:734: ax5043_receiver_on_continuous();
      00291D 12 2B 9B         [24] 4578 	lcall	_ax5043_receiver_on_continuous
                                   4579 ;	..\src\COMMON\easyax5043.c:735: break;
                                   4580 ;	..\src\COMMON\easyax5043.c:737: offxtal:
      002920 80 6E            [24] 4581 	sjmp	00134$
      002922                       4582 00124$:
                                   4583 ;	..\src\COMMON\easyax5043.c:738: ax5043_off_xtal();
      002922 12 2C F9         [24] 4584 	lcall	_ax5043_off_xtal
                                   4585 ;	..\src\COMMON\easyax5043.c:739: break;
                                   4586 ;	..\src\COMMON\easyax5043.c:741: case AXRADIO_MODE_WOR_RECEIVE:
      002925 80 69            [24] 4587 	sjmp	00134$
      002927                       4588 00125$:
                                   4589 ;	..\src\COMMON\easyax5043.c:742: if (axradio_cb_receive.st.error == AXRADIO_ERR_PACKETDONE) {
      002927 90 03 1D         [24] 4590 	mov	dptr,#(_axradio_cb_receive + 0x0005)
      00292A E0               [24] 4591 	movx	a,@dptr
      00292B FF               [12] 4592 	mov	r7,a
      00292C BF F0 F3         [24] 4593 	cjne	r7,#0xf0,00124$
                                   4594 ;	..\src\COMMON\easyax5043.c:743: ax5043_init_registers_rx();
      00292F 12 1F 50         [24] 4595 	lcall	_ax5043_init_registers_rx
                                   4596 ;	..\src\COMMON\easyax5043.c:744: ax5043_receiver_on_wor();
      002932 12 2C 01         [24] 4597 	lcall	_ax5043_receiver_on_wor
                                   4598 ;	..\src\COMMON\easyax5043.c:745: break;
                                   4599 ;	..\src\COMMON\easyax5043.c:749: case AXRADIO_MODE_WOR_ACK_RECEIVE:
      002935 80 59            [24] 4600 	sjmp	00134$
      002937                       4601 00128$:
                                   4602 ;	..\src\COMMON\easyax5043.c:750: ax5043_init_registers_rx();
      002937 12 1F 50         [24] 4603 	lcall	_ax5043_init_registers_rx
                                   4604 ;	..\src\COMMON\easyax5043.c:751: ax5043_receiver_on_wor();
      00293A 12 2C 01         [24] 4605 	lcall	_ax5043_receiver_on_wor
                                   4606 ;	..\src\COMMON\easyax5043.c:752: break;
                                   4607 ;	..\src\COMMON\easyax5043.c:754: case AXRADIO_MODE_SYNC_ACK_MASTER:
      00293D 80 51            [24] 4608 	sjmp	00134$
      00293F                       4609 00129$:
                                   4610 ;	..\src\COMMON\easyax5043.c:755: axradio_txbuffer_len = axradio_framing_minpayloadlen;
      00293F 90 90 FA         [24] 4611 	mov	dptr,#_axradio_framing_minpayloadlen
      002942 E4               [12] 4612 	clr	a
      002943 93               [24] 4613 	movc	a,@a+dptr
      002944 FF               [12] 4614 	mov	r7,a
      002945 90 00 EB         [24] 4615 	mov	dptr,#_axradio_txbuffer_len
      002948 F0               [24] 4616 	movx	@dptr,a
      002949 E4               [12] 4617 	clr	a
      00294A A3               [24] 4618 	inc	dptr
      00294B F0               [24] 4619 	movx	@dptr,a
                                   4620 ;	..\src\COMMON\easyax5043.c:759: case AXRADIO_MODE_WOR_ACK_TRANSMIT:
      00294C                       4621 00131$:
                                   4622 ;	..\src\COMMON\easyax5043.c:760: ax5043_init_registers_rx();
      00294C 12 1F 50         [24] 4623 	lcall	_ax5043_init_registers_rx
                                   4624 ;	..\src\COMMON\easyax5043.c:761: ax5043_receiver_on_continuous();
      00294F 12 2B 9B         [24] 4625 	lcall	_ax5043_receiver_on_continuous
                                   4626 ;	..\src\COMMON\easyax5043.c:762: wtimer_remove(&axradio_timer);
      002952 90 03 6F         [24] 4627 	mov	dptr,#_axradio_timer
      002955 12 85 39         [24] 4628 	lcall	_wtimer_remove
                                   4629 ;	..\src\COMMON\easyax5043.c:763: axradio_timer.time = axradio_framing_ack_timeout;
      002958 90 90 F0         [24] 4630 	mov	dptr,#_axradio_framing_ack_timeout
      00295B E4               [12] 4631 	clr	a
      00295C 93               [24] 4632 	movc	a,@a+dptr
      00295D FC               [12] 4633 	mov	r4,a
      00295E 74 01            [12] 4634 	mov	a,#0x01
      002960 93               [24] 4635 	movc	a,@a+dptr
      002961 FD               [12] 4636 	mov	r5,a
      002962 74 02            [12] 4637 	mov	a,#0x02
      002964 93               [24] 4638 	movc	a,@a+dptr
      002965 FE               [12] 4639 	mov	r6,a
      002966 74 03            [12] 4640 	mov	a,#0x03
      002968 93               [24] 4641 	movc	a,@a+dptr
      002969 FF               [12] 4642 	mov	r7,a
      00296A 90 03 73         [24] 4643 	mov	dptr,#(_axradio_timer + 0x0004)
      00296D EC               [12] 4644 	mov	a,r4
      00296E F0               [24] 4645 	movx	@dptr,a
      00296F ED               [12] 4646 	mov	a,r5
      002970 A3               [24] 4647 	inc	dptr
      002971 F0               [24] 4648 	movx	@dptr,a
      002972 EE               [12] 4649 	mov	a,r6
      002973 A3               [24] 4650 	inc	dptr
      002974 F0               [24] 4651 	movx	@dptr,a
      002975 EF               [12] 4652 	mov	a,r7
      002976 A3               [24] 4653 	inc	dptr
      002977 F0               [24] 4654 	movx	@dptr,a
                                   4655 ;	..\src\COMMON\easyax5043.c:764: wtimer0_addrelative(&axradio_timer);
      002978 90 03 6F         [24] 4656 	mov	dptr,#_axradio_timer
      00297B 12 78 78         [24] 4657 	lcall	_wtimer0_addrelative
                                   4658 ;	..\src\COMMON\easyax5043.c:765: break;
                                   4659 ;	..\src\COMMON\easyax5043.c:767: case AXRADIO_MODE_SYNC_MASTER:
      00297E 80 10            [24] 4660 	sjmp	00134$
      002980                       4661 00132$:
                                   4662 ;	..\src\COMMON\easyax5043.c:768: axradio_txbuffer_len = axradio_framing_minpayloadlen;
      002980 90 90 FA         [24] 4663 	mov	dptr,#_axradio_framing_minpayloadlen
      002983 E4               [12] 4664 	clr	a
      002984 93               [24] 4665 	movc	a,@a+dptr
      002985 FF               [12] 4666 	mov	r7,a
      002986 90 00 EB         [24] 4667 	mov	dptr,#_axradio_txbuffer_len
      002989 F0               [24] 4668 	movx	@dptr,a
      00298A E4               [12] 4669 	clr	a
      00298B A3               [24] 4670 	inc	dptr
      00298C F0               [24] 4671 	movx	@dptr,a
                                   4672 ;	..\src\COMMON\easyax5043.c:771: default:
      00298D                       4673 00133$:
                                   4674 ;	..\src\COMMON\easyax5043.c:772: ax5043_off();
      00298D 12 2C F0         [24] 4675 	lcall	_ax5043_off
                                   4676 ;	..\src\COMMON\easyax5043.c:774: }
      002990                       4677 00134$:
                                   4678 ;	..\src\COMMON\easyax5043.c:775: if (axradio_mode != AXRADIO_MODE_SYNC_MASTER &&
      002990 74 30            [12] 4679 	mov	a,#0x30
      002992 B5 17 02         [24] 4680 	cjne	a,_axradio_mode,00307$
      002995 80 1A            [24] 4681 	sjmp	00136$
      002997                       4682 00307$:
                                   4683 ;	..\src\COMMON\easyax5043.c:776: axradio_mode != AXRADIO_MODE_SYNC_ACK_MASTER &&
      002997 74 31            [12] 4684 	mov	a,#0x31
      002999 B5 17 02         [24] 4685 	cjne	a,_axradio_mode,00308$
      00299C 80 13            [24] 4686 	sjmp	00136$
      00299E                       4687 00308$:
                                   4688 ;	..\src\COMMON\easyax5043.c:777: axradio_mode != AXRADIO_MODE_SYNC_SLAVE &&
      00299E 74 32            [12] 4689 	mov	a,#0x32
      0029A0 B5 17 02         [24] 4690 	cjne	a,_axradio_mode,00309$
      0029A3 80 0C            [24] 4691 	sjmp	00136$
      0029A5                       4692 00309$:
                                   4693 ;	..\src\COMMON\easyax5043.c:778: axradio_mode != AXRADIO_MODE_SYNC_ACK_SLAVE)
      0029A5 74 33            [12] 4694 	mov	a,#0x33
      0029A7 B5 17 02         [24] 4695 	cjne	a,_axradio_mode,00310$
      0029AA 80 05            [24] 4696 	sjmp	00136$
      0029AC                       4697 00310$:
                                   4698 ;	..\src\COMMON\easyax5043.c:779: axradio_syncstate = syncstate_off;
      0029AC 90 00 EA         [24] 4699 	mov	dptr,#_axradio_syncstate
      0029AF E4               [12] 4700 	clr	a
      0029B0 F0               [24] 4701 	movx	@dptr,a
      0029B1                       4702 00136$:
                                   4703 ;	..\src\COMMON\easyax5043.c:780: update_timeanchor();
      0029B1 12 1E 70         [24] 4704 	lcall	_update_timeanchor
                                   4705 ;	..\src\COMMON\easyax5043.c:781: wtimer_remove_callback(&axradio_cb_transmitend.cb);
      0029B4 90 03 5B         [24] 4706 	mov	dptr,#_axradio_cb_transmitend
      0029B7 12 87 AB         [24] 4707 	lcall	_wtimer_remove_callback
                                   4708 ;	..\src\COMMON\easyax5043.c:782: axradio_cb_transmitend.st.error = AXRADIO_ERR_NOERROR;
      0029BA 90 03 60         [24] 4709 	mov	dptr,#(_axradio_cb_transmitend + 0x0005)
      0029BD E4               [12] 4710 	clr	a
      0029BE F0               [24] 4711 	movx	@dptr,a
                                   4712 ;	..\src\COMMON\easyax5043.c:783: if (axradio_mode == AXRADIO_MODE_ACK_TRANSMIT ||
      0029BF 74 12            [12] 4713 	mov	a,#0x12
      0029C1 B5 17 02         [24] 4714 	cjne	a,_axradio_mode,00311$
      0029C4 80 0C            [24] 4715 	sjmp	00140$
      0029C6                       4716 00311$:
                                   4717 ;	..\src\COMMON\easyax5043.c:784: axradio_mode == AXRADIO_MODE_WOR_ACK_TRANSMIT ||
      0029C6 74 13            [12] 4718 	mov	a,#0x13
      0029C8 B5 17 02         [24] 4719 	cjne	a,_axradio_mode,00312$
      0029CB 80 05            [24] 4720 	sjmp	00140$
      0029CD                       4721 00312$:
                                   4722 ;	..\src\COMMON\easyax5043.c:785: axradio_mode == AXRADIO_MODE_SYNC_ACK_MASTER)
      0029CD 74 31            [12] 4723 	mov	a,#0x31
      0029CF B5 17 06         [24] 4724 	cjne	a,_axradio_mode,00141$
      0029D2                       4725 00140$:
                                   4726 ;	..\src\COMMON\easyax5043.c:786: axradio_cb_transmitend.st.error = AXRADIO_ERR_BUSY;
      0029D2 90 03 60         [24] 4727 	mov	dptr,#(_axradio_cb_transmitend + 0x0005)
      0029D5 74 02            [12] 4728 	mov	a,#0x02
      0029D7 F0               [24] 4729 	movx	@dptr,a
      0029D8                       4730 00141$:
                                   4731 ;	..\src\COMMON\easyax5043.c:787: axradio_cb_transmitend.st.time.t = axradio_timeanchor.radiotimer;
      0029D8 90 01 00         [24] 4732 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      0029DB E0               [24] 4733 	movx	a,@dptr
      0029DC FC               [12] 4734 	mov	r4,a
      0029DD A3               [24] 4735 	inc	dptr
      0029DE E0               [24] 4736 	movx	a,@dptr
      0029DF FD               [12] 4737 	mov	r5,a
      0029E0 A3               [24] 4738 	inc	dptr
      0029E1 E0               [24] 4739 	movx	a,@dptr
      0029E2 FE               [12] 4740 	mov	r6,a
      0029E3 A3               [24] 4741 	inc	dptr
      0029E4 E0               [24] 4742 	movx	a,@dptr
      0029E5 FF               [12] 4743 	mov	r7,a
      0029E6 90 03 61         [24] 4744 	mov	dptr,#(_axradio_cb_transmitend + 0x0006)
      0029E9 EC               [12] 4745 	mov	a,r4
      0029EA F0               [24] 4746 	movx	@dptr,a
      0029EB ED               [12] 4747 	mov	a,r5
      0029EC A3               [24] 4748 	inc	dptr
      0029ED F0               [24] 4749 	movx	@dptr,a
      0029EE EE               [12] 4750 	mov	a,r6
      0029EF A3               [24] 4751 	inc	dptr
      0029F0 F0               [24] 4752 	movx	@dptr,a
      0029F1 EF               [12] 4753 	mov	a,r7
      0029F2 A3               [24] 4754 	inc	dptr
      0029F3 F0               [24] 4755 	movx	@dptr,a
                                   4756 ;	..\src\COMMON\easyax5043.c:788: wtimer_add_callback(&axradio_cb_transmitend.cb);
      0029F4 90 03 5B         [24] 4757 	mov	dptr,#_axradio_cb_transmitend
      0029F7 12 78 5E         [24] 4758 	lcall	_wtimer_add_callback
                                   4759 ;	..\src\COMMON\easyax5043.c:789: break;
      0029FA 02 2B 7E         [24] 4760 	ljmp	00167$
                                   4761 ;	..\src\COMMON\easyax5043.c:792: case trxstate_txcw_xtalwait:
      0029FD                       4762 00144$:
                                   4763 ;	..\src\COMMON\easyax5043.c:793: AX5043_IRQMASK1 = 0x00;
      0029FD 90 40 06         [24] 4764 	mov	dptr,#_AX5043_IRQMASK1
      002A00 E4               [12] 4765 	clr	a
      002A01 F0               [24] 4766 	movx	@dptr,a
                                   4767 ;	..\src\COMMON\easyax5043.c:794: AX5043_IRQMASK0 = 0x00;
      002A02 90 40 07         [24] 4768 	mov	dptr,#_AX5043_IRQMASK0
      002A05 F0               [24] 4769 	movx	@dptr,a
                                   4770 ;	..\src\COMMON\easyax5043.c:795: AX5043_PWRMODE = AX5043_PWRSTATE_FULL_TX;
      002A06 90 40 02         [24] 4771 	mov	dptr,#_AX5043_PWRMODE
      002A09 74 0D            [12] 4772 	mov	a,#0x0d
      002A0B F0               [24] 4773 	movx	@dptr,a
                                   4774 ;	..\src\COMMON\easyax5043.c:796: axradio_trxstate = trxstate_off;
      002A0C 75 18 00         [24] 4775 	mov	_axradio_trxstate,#0x00
                                   4776 ;	..\src\COMMON\easyax5043.c:797: update_timeanchor();
      002A0F 12 1E 70         [24] 4777 	lcall	_update_timeanchor
                                   4778 ;	..\src\COMMON\easyax5043.c:798: wtimer_remove_callback(&axradio_cb_transmitstart.cb);
      002A12 90 03 51         [24] 4779 	mov	dptr,#_axradio_cb_transmitstart
      002A15 12 87 AB         [24] 4780 	lcall	_wtimer_remove_callback
                                   4781 ;	..\src\COMMON\easyax5043.c:799: axradio_cb_transmitstart.st.error = AXRADIO_ERR_NOERROR;
      002A18 90 03 56         [24] 4782 	mov	dptr,#(_axradio_cb_transmitstart + 0x0005)
      002A1B E4               [12] 4783 	clr	a
      002A1C F0               [24] 4784 	movx	@dptr,a
                                   4785 ;	..\src\COMMON\easyax5043.c:800: axradio_cb_transmitstart.st.time.t = axradio_timeanchor.radiotimer;
      002A1D 90 01 00         [24] 4786 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      002A20 E0               [24] 4787 	movx	a,@dptr
      002A21 FC               [12] 4788 	mov	r4,a
      002A22 A3               [24] 4789 	inc	dptr
      002A23 E0               [24] 4790 	movx	a,@dptr
      002A24 FD               [12] 4791 	mov	r5,a
      002A25 A3               [24] 4792 	inc	dptr
      002A26 E0               [24] 4793 	movx	a,@dptr
      002A27 FE               [12] 4794 	mov	r6,a
      002A28 A3               [24] 4795 	inc	dptr
      002A29 E0               [24] 4796 	movx	a,@dptr
      002A2A FF               [12] 4797 	mov	r7,a
      002A2B 90 03 57         [24] 4798 	mov	dptr,#(_axradio_cb_transmitstart + 0x0006)
      002A2E EC               [12] 4799 	mov	a,r4
      002A2F F0               [24] 4800 	movx	@dptr,a
      002A30 ED               [12] 4801 	mov	a,r5
      002A31 A3               [24] 4802 	inc	dptr
      002A32 F0               [24] 4803 	movx	@dptr,a
      002A33 EE               [12] 4804 	mov	a,r6
      002A34 A3               [24] 4805 	inc	dptr
      002A35 F0               [24] 4806 	movx	@dptr,a
      002A36 EF               [12] 4807 	mov	a,r7
      002A37 A3               [24] 4808 	inc	dptr
      002A38 F0               [24] 4809 	movx	@dptr,a
                                   4810 ;	..\src\COMMON\easyax5043.c:801: wtimer_add_callback(&axradio_cb_transmitstart.cb);
      002A39 90 03 51         [24] 4811 	mov	dptr,#_axradio_cb_transmitstart
      002A3C 12 78 5E         [24] 4812 	lcall	_wtimer_add_callback
                                   4813 ;	..\src\COMMON\easyax5043.c:802: break;
      002A3F 02 2B 7E         [24] 4814 	ljmp	00167$
                                   4815 ;	..\src\COMMON\easyax5043.c:804: case trxstate_txstream_xtalwait:
      002A42                       4816 00145$:
                                   4817 ;	..\src\COMMON\easyax5043.c:805: if (AX5043_IRQREQUEST1 & 0x01) {
      002A42 90 40 0C         [24] 4818 	mov	dptr,#_AX5043_IRQREQUEST1
      002A45 E0               [24] 4819 	movx	a,@dptr
      002A46 20 E0 03         [24] 4820 	jb	acc.0,00315$
      002A49 02 2A DE         [24] 4821 	ljmp	00155$
      002A4C                       4822 00315$:
                                   4823 ;	..\src\COMMON\easyax5043.c:806: AX5043_RADIOEVENTMASK0 = 0x03; // enable PLL settled and done event
      002A4C 90 40 09         [24] 4824 	mov	dptr,#_AX5043_RADIOEVENTMASK0
      002A4F 74 03            [12] 4825 	mov	a,#0x03
      002A51 F0               [24] 4826 	movx	@dptr,a
                                   4827 ;	..\src\COMMON\easyax5043.c:807: AX5043_IRQMASK1 = 0x00;
      002A52 90 40 06         [24] 4828 	mov	dptr,#_AX5043_IRQMASK1
      002A55 E4               [12] 4829 	clr	a
      002A56 F0               [24] 4830 	movx	@dptr,a
                                   4831 ;	..\src\COMMON\easyax5043.c:808: AX5043_IRQMASK0 = 0x40; // enable radio controller irq
      002A57 90 40 07         [24] 4832 	mov	dptr,#_AX5043_IRQMASK0
      002A5A 74 40            [12] 4833 	mov	a,#0x40
      002A5C F0               [24] 4834 	movx	@dptr,a
                                   4835 ;	..\src\COMMON\easyax5043.c:809: AX5043_PWRMODE = AX5043_PWRSTATE_FULL_TX;
      002A5D 90 40 02         [24] 4836 	mov	dptr,#_AX5043_PWRMODE
      002A60 74 0D            [12] 4837 	mov	a,#0x0d
      002A62 F0               [24] 4838 	movx	@dptr,a
                                   4839 ;	..\src\COMMON\easyax5043.c:810: axradio_trxstate = trxstate_txstream;
      002A63 75 18 10         [24] 4840 	mov	_axradio_trxstate,#0x10
                                   4841 ;	..\src\COMMON\easyax5043.c:812: goto txstreamdatacb;
                                   4842 ;	..\src\COMMON\easyax5043.c:814: case trxstate_txstream:
      002A66 80 76            [24] 4843 	sjmp	00155$
      002A68                       4844 00148$:
                                   4845 ;	..\src\COMMON\easyax5043.c:816: uint8_t __autodata evt = AX5043_RADIOEVENTREQ0;
      002A68 90 40 0F         [24] 4846 	mov	dptr,#_AX5043_RADIOEVENTREQ0
      002A6B E0               [24] 4847 	movx	a,@dptr
                                   4848 ;	..\src\COMMON\easyax5043.c:817: if (evt & 0x03)
      002A6C FF               [12] 4849 	mov	r7,a
      002A6D 54 03            [12] 4850 	anl	a,#0x03
      002A6F 60 07            [24] 4851 	jz	00150$
                                   4852 ;	..\src\COMMON\easyax5043.c:818: update_timeanchor();
      002A71 C0 07            [24] 4853 	push	ar7
      002A73 12 1E 70         [24] 4854 	lcall	_update_timeanchor
      002A76 D0 07            [24] 4855 	pop	ar7
      002A78                       4856 00150$:
                                   4857 ;	..\src\COMMON\easyax5043.c:819: if (evt & 0x01) {
      002A78 EF               [12] 4858 	mov	a,r7
      002A79 30 E0 31         [24] 4859 	jnb	acc.0,00152$
                                   4860 ;	..\src\COMMON\easyax5043.c:820: wtimer_remove_callback(&axradio_cb_transmitend.cb);
      002A7C 90 03 5B         [24] 4861 	mov	dptr,#_axradio_cb_transmitend
      002A7F C0 07            [24] 4862 	push	ar7
      002A81 12 87 AB         [24] 4863 	lcall	_wtimer_remove_callback
                                   4864 ;	..\src\COMMON\easyax5043.c:821: axradio_cb_transmitend.st.error = AXRADIO_ERR_NOERROR;
      002A84 90 03 60         [24] 4865 	mov	dptr,#(_axradio_cb_transmitend + 0x0005)
      002A87 E4               [12] 4866 	clr	a
      002A88 F0               [24] 4867 	movx	@dptr,a
                                   4868 ;	..\src\COMMON\easyax5043.c:822: axradio_cb_transmitend.st.time.t = axradio_timeanchor.radiotimer;
      002A89 90 01 00         [24] 4869 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      002A8C E0               [24] 4870 	movx	a,@dptr
      002A8D FB               [12] 4871 	mov	r3,a
      002A8E A3               [24] 4872 	inc	dptr
      002A8F E0               [24] 4873 	movx	a,@dptr
      002A90 FC               [12] 4874 	mov	r4,a
      002A91 A3               [24] 4875 	inc	dptr
      002A92 E0               [24] 4876 	movx	a,@dptr
      002A93 FD               [12] 4877 	mov	r5,a
      002A94 A3               [24] 4878 	inc	dptr
      002A95 E0               [24] 4879 	movx	a,@dptr
      002A96 FE               [12] 4880 	mov	r6,a
      002A97 90 03 61         [24] 4881 	mov	dptr,#(_axradio_cb_transmitend + 0x0006)
      002A9A EB               [12] 4882 	mov	a,r3
      002A9B F0               [24] 4883 	movx	@dptr,a
      002A9C EC               [12] 4884 	mov	a,r4
      002A9D A3               [24] 4885 	inc	dptr
      002A9E F0               [24] 4886 	movx	@dptr,a
      002A9F ED               [12] 4887 	mov	a,r5
      002AA0 A3               [24] 4888 	inc	dptr
      002AA1 F0               [24] 4889 	movx	@dptr,a
      002AA2 EE               [12] 4890 	mov	a,r6
      002AA3 A3               [24] 4891 	inc	dptr
      002AA4 F0               [24] 4892 	movx	@dptr,a
                                   4893 ;	..\src\COMMON\easyax5043.c:823: wtimer_add_callback(&axradio_cb_transmitend.cb);
      002AA5 90 03 5B         [24] 4894 	mov	dptr,#_axradio_cb_transmitend
      002AA8 12 78 5E         [24] 4895 	lcall	_wtimer_add_callback
      002AAB D0 07            [24] 4896 	pop	ar7
      002AAD                       4897 00152$:
                                   4898 ;	..\src\COMMON\easyax5043.c:825: if (evt & 0x02) {
      002AAD EF               [12] 4899 	mov	a,r7
      002AAE 30 E1 2D         [24] 4900 	jnb	acc.1,00155$
                                   4901 ;	..\src\COMMON\easyax5043.c:826: wtimer_remove_callback(&axradio_cb_transmitstart.cb);
      002AB1 90 03 51         [24] 4902 	mov	dptr,#_axradio_cb_transmitstart
      002AB4 12 87 AB         [24] 4903 	lcall	_wtimer_remove_callback
                                   4904 ;	..\src\COMMON\easyax5043.c:827: axradio_cb_transmitstart.st.error = AXRADIO_ERR_NOERROR;
      002AB7 90 03 56         [24] 4905 	mov	dptr,#(_axradio_cb_transmitstart + 0x0005)
      002ABA E4               [12] 4906 	clr	a
      002ABB F0               [24] 4907 	movx	@dptr,a
                                   4908 ;	..\src\COMMON\easyax5043.c:828: axradio_cb_transmitstart.st.time.t = axradio_timeanchor.radiotimer;
      002ABC 90 01 00         [24] 4909 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      002ABF E0               [24] 4910 	movx	a,@dptr
      002AC0 FC               [12] 4911 	mov	r4,a
      002AC1 A3               [24] 4912 	inc	dptr
      002AC2 E0               [24] 4913 	movx	a,@dptr
      002AC3 FD               [12] 4914 	mov	r5,a
      002AC4 A3               [24] 4915 	inc	dptr
      002AC5 E0               [24] 4916 	movx	a,@dptr
      002AC6 FE               [12] 4917 	mov	r6,a
      002AC7 A3               [24] 4918 	inc	dptr
      002AC8 E0               [24] 4919 	movx	a,@dptr
      002AC9 FF               [12] 4920 	mov	r7,a
      002ACA 90 03 57         [24] 4921 	mov	dptr,#(_axradio_cb_transmitstart + 0x0006)
      002ACD EC               [12] 4922 	mov	a,r4
      002ACE F0               [24] 4923 	movx	@dptr,a
      002ACF ED               [12] 4924 	mov	a,r5
      002AD0 A3               [24] 4925 	inc	dptr
      002AD1 F0               [24] 4926 	movx	@dptr,a
      002AD2 EE               [12] 4927 	mov	a,r6
      002AD3 A3               [24] 4928 	inc	dptr
      002AD4 F0               [24] 4929 	movx	@dptr,a
      002AD5 EF               [12] 4930 	mov	a,r7
      002AD6 A3               [24] 4931 	inc	dptr
      002AD7 F0               [24] 4932 	movx	@dptr,a
                                   4933 ;	..\src\COMMON\easyax5043.c:829: wtimer_add_callback(&axradio_cb_transmitstart.cb);
      002AD8 90 03 51         [24] 4934 	mov	dptr,#_axradio_cb_transmitstart
      002ADB 12 78 5E         [24] 4935 	lcall	_wtimer_add_callback
                                   4936 ;	..\src\COMMON\easyax5043.c:832: txstreamdatacb:
      002ADE                       4937 00155$:
                                   4938 ;	..\src\COMMON\easyax5043.c:833: if (AX5043_IRQREQUEST0 & AX5043_IRQMASK0 & 0x08) {
      002ADE 90 40 0D         [24] 4939 	mov	dptr,#_AX5043_IRQREQUEST0
      002AE1 E0               [24] 4940 	movx	a,@dptr
      002AE2 FF               [12] 4941 	mov	r7,a
      002AE3 90 40 07         [24] 4942 	mov	dptr,#_AX5043_IRQMASK0
      002AE6 E0               [24] 4943 	movx	a,@dptr
      002AE7 FE               [12] 4944 	mov	r6,a
      002AE8 5F               [12] 4945 	anl	a,r7
      002AE9 20 E3 03         [24] 4946 	jb	acc.3,00319$
      002AEC 02 2B 7E         [24] 4947 	ljmp	00167$
      002AEF                       4948 00319$:
                                   4949 ;	..\src\COMMON\easyax5043.c:834: AX5043_IRQMASK0 &= (uint8_t)~0x08;
      002AEF 90 40 07         [24] 4950 	mov	dptr,#_AX5043_IRQMASK0
      002AF2 E0               [24] 4951 	movx	a,@dptr
      002AF3 53 E0 F7         [24] 4952 	anl	acc,#0xf7
      002AF6 F0               [24] 4953 	movx	@dptr,a
                                   4954 ;	..\src\COMMON\easyax5043.c:835: update_timeanchor();
      002AF7 12 1E 70         [24] 4955 	lcall	_update_timeanchor
                                   4956 ;	..\src\COMMON\easyax5043.c:836: wtimer_remove_callback(&axradio_cb_transmitdata.cb);
      002AFA 90 03 65         [24] 4957 	mov	dptr,#_axradio_cb_transmitdata
      002AFD 12 87 AB         [24] 4958 	lcall	_wtimer_remove_callback
                                   4959 ;	..\src\COMMON\easyax5043.c:837: axradio_cb_transmitdata.st.error = AXRADIO_ERR_NOERROR;
      002B00 90 03 6A         [24] 4960 	mov	dptr,#(_axradio_cb_transmitdata + 0x0005)
      002B03 E4               [12] 4961 	clr	a
      002B04 F0               [24] 4962 	movx	@dptr,a
                                   4963 ;	..\src\COMMON\easyax5043.c:838: axradio_cb_transmitdata.st.time.t = axradio_timeanchor.radiotimer;
      002B05 90 01 00         [24] 4964 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      002B08 E0               [24] 4965 	movx	a,@dptr
      002B09 FC               [12] 4966 	mov	r4,a
      002B0A A3               [24] 4967 	inc	dptr
      002B0B E0               [24] 4968 	movx	a,@dptr
      002B0C FD               [12] 4969 	mov	r5,a
      002B0D A3               [24] 4970 	inc	dptr
      002B0E E0               [24] 4971 	movx	a,@dptr
      002B0F FE               [12] 4972 	mov	r6,a
      002B10 A3               [24] 4973 	inc	dptr
      002B11 E0               [24] 4974 	movx	a,@dptr
      002B12 FF               [12] 4975 	mov	r7,a
      002B13 90 03 6B         [24] 4976 	mov	dptr,#(_axradio_cb_transmitdata + 0x0006)
      002B16 EC               [12] 4977 	mov	a,r4
      002B17 F0               [24] 4978 	movx	@dptr,a
      002B18 ED               [12] 4979 	mov	a,r5
      002B19 A3               [24] 4980 	inc	dptr
      002B1A F0               [24] 4981 	movx	@dptr,a
      002B1B EE               [12] 4982 	mov	a,r6
      002B1C A3               [24] 4983 	inc	dptr
      002B1D F0               [24] 4984 	movx	@dptr,a
      002B1E EF               [12] 4985 	mov	a,r7
      002B1F A3               [24] 4986 	inc	dptr
      002B20 F0               [24] 4987 	movx	@dptr,a
                                   4988 ;	..\src\COMMON\easyax5043.c:839: wtimer_add_callback(&axradio_cb_transmitdata.cb);
      002B21 90 03 65         [24] 4989 	mov	dptr,#_axradio_cb_transmitdata
      002B24 12 78 5E         [24] 4990 	lcall	_wtimer_add_callback
                                   4991 ;	..\src\COMMON\easyax5043.c:841: break;
                                   4992 ;	..\src\COMMON\easyax5043.c:843: case trxstate_rxwor:
      002B27 80 55            [24] 4993 	sjmp	00167$
      002B29                       4994 00158$:
                                   4995 ;	..\src\COMMON\easyax5043.c:846: if( AX5043_IRQREQUEST0 & 0x80 ) // vdda ready (note irqinversion does not act upon AX5043_IRQREQUEST0)
      002B29 90 40 0D         [24] 4996 	mov	dptr,#_AX5043_IRQREQUEST0
      002B2C E0               [24] 4997 	movx	a,@dptr
      002B2D 30 E7 0A         [24] 4998 	jnb	acc.7,00160$
                                   4999 ;	..\src\COMMON\easyax5043.c:848: AX5043_IRQINVERSION0 |= 0x80; // invert pwr irq, so it does not fire continuously
      002B30 90 40 0B         [24] 5000 	mov	dptr,#_AX5043_IRQINVERSION0
      002B33 E0               [24] 5001 	movx	a,@dptr
      002B34 43 E0 80         [24] 5002 	orl	acc,#0x80
      002B37 F0               [24] 5003 	movx	@dptr,a
      002B38 80 08            [24] 5004 	sjmp	00161$
      002B3A                       5005 00160$:
                                   5006 ;	..\src\COMMON\easyax5043.c:852: AX5043_IRQINVERSION0 &= (uint8_t)~0x80; // drop pwr irq inversion --> armed again
      002B3A 90 40 0B         [24] 5007 	mov	dptr,#_AX5043_IRQINVERSION0
      002B3D E0               [24] 5008 	movx	a,@dptr
      002B3E 53 E0 7F         [24] 5009 	anl	acc,#0x7f
      002B41 F0               [24] 5010 	movx	@dptr,a
      002B42                       5011 00161$:
                                   5012 ;	..\src\COMMON\easyax5043.c:856: if( AX5043_IRQREQUEST1 & 0x01 ) // XTAL ready
      002B42 90 40 0C         [24] 5013 	mov	dptr,#_AX5043_IRQREQUEST1
      002B45 E0               [24] 5014 	movx	a,@dptr
      002B46 30 E0 0A         [24] 5015 	jnb	acc.0,00163$
                                   5016 ;	..\src\COMMON\easyax5043.c:858: AX5043_IRQINVERSION1 |= 0x01; // invert the xtal ready irq so it does not fire continuously
      002B49 90 40 0A         [24] 5017 	mov	dptr,#_AX5043_IRQINVERSION1
      002B4C E0               [24] 5018 	movx	a,@dptr
      002B4D 43 E0 01         [24] 5019 	orl	acc,#0x01
      002B50 F0               [24] 5020 	movx	@dptr,a
      002B51 80 28            [24] 5021 	sjmp	00165$
      002B53                       5022 00163$:
                                   5023 ;	..\src\COMMON\easyax5043.c:862: AX5043_IRQINVERSION1 &= (uint8_t)~0x01; // drop xtal ready irq inversion --> armed again for next wake-up
      002B53 90 40 0A         [24] 5024 	mov	dptr,#_AX5043_IRQINVERSION1
      002B56 E0               [24] 5025 	movx	a,@dptr
      002B57 53 E0 FE         [24] 5026 	anl	acc,#0xfe
      002B5A F0               [24] 5027 	movx	@dptr,a
                                   5028 ;	..\src\COMMON\easyax5043.c:863: AX5043_0xF30 = f30_saved;
      002B5B 90 0A 4E         [24] 5029 	mov	dptr,#_f30_saved
      002B5E E0               [24] 5030 	movx	a,@dptr
      002B5F 90 4F 30         [24] 5031 	mov	dptr,#_AX5043_0xF30
      002B62 F0               [24] 5032 	movx	@dptr,a
                                   5033 ;	..\src\COMMON\easyax5043.c:864: AX5043_0xF31 = f31_saved;
      002B63 90 0A 4F         [24] 5034 	mov	dptr,#_f31_saved
      002B66 E0               [24] 5035 	movx	a,@dptr
      002B67 90 4F 31         [24] 5036 	mov	dptr,#_AX5043_0xF31
      002B6A F0               [24] 5037 	movx	@dptr,a
                                   5038 ;	..\src\COMMON\easyax5043.c:865: AX5043_0xF32 = f32_saved;
      002B6B 90 0A 50         [24] 5039 	mov	dptr,#_f32_saved
      002B6E E0               [24] 5040 	movx	a,@dptr
      002B6F 90 4F 32         [24] 5041 	mov	dptr,#_AX5043_0xF32
      002B72 F0               [24] 5042 	movx	@dptr,a
                                   5043 ;	..\src\COMMON\easyax5043.c:866: AX5043_0xF33 = f33_saved;
      002B73 90 0A 51         [24] 5044 	mov	dptr,#_f33_saved
      002B76 E0               [24] 5045 	movx	a,@dptr
      002B77 90 4F 33         [24] 5046 	mov	dptr,#_AX5043_0xF33
      002B7A F0               [24] 5047 	movx	@dptr,a
                                   5048 ;	..\src\COMMON\easyax5043.c:870: case trxstate_rx:
      002B7B                       5049 00165$:
                                   5050 ;	..\src\COMMON\easyax5043.c:871: receive_isr();
      002B7B 12 1F 56         [24] 5051 	lcall	_receive_isr
                                   5052 ;	..\src\COMMON\easyax5043.c:874: } // end switch(axradio_trxstate)
      002B7E                       5053 00167$:
                                   5054 ;	..\src\COMMON\easyax5043.c:875: } //end radio_isr
      002B7E D0 D0            [24] 5055 	pop	psw
      002B80 D0 00            [24] 5056 	pop	(0+0)
      002B82 D0 01            [24] 5057 	pop	(0+1)
      002B84 D0 02            [24] 5058 	pop	(0+2)
      002B86 D0 03            [24] 5059 	pop	(0+3)
      002B88 D0 04            [24] 5060 	pop	(0+4)
      002B8A D0 05            [24] 5061 	pop	(0+5)
      002B8C D0 06            [24] 5062 	pop	(0+6)
      002B8E D0 07            [24] 5063 	pop	(0+7)
      002B90 D0 83            [24] 5064 	pop	dph
      002B92 D0 82            [24] 5065 	pop	dpl
      002B94 D0 F0            [24] 5066 	pop	b
      002B96 D0 E0            [24] 5067 	pop	acc
      002B98 D0 20            [24] 5068 	pop	bits
      002B9A 32               [24] 5069 	reti
                                   5070 ;------------------------------------------------------------
                                   5071 ;Allocation info for local variables in function 'ax5043_receiver_on_continuous'
                                   5072 ;------------------------------------------------------------
                                   5073 ;rschanged_int             Allocated to registers r6 
                                   5074 ;------------------------------------------------------------
                                   5075 ;	..\src\COMMON\easyax5043.c:878: __reentrantb void ax5043_receiver_on_continuous(void) __reentrant
                                   5076 ;	-----------------------------------------
                                   5077 ;	 function ax5043_receiver_on_continuous
                                   5078 ;	-----------------------------------------
      002B9B                       5079 _ax5043_receiver_on_continuous:
                                   5080 ;	..\src\COMMON\easyax5043.c:880: uint8_t rschanged_int = (axradio_framing_enable_sfdcallback | (axradio_mode == AXRADIO_MODE_SYNC_ACK_SLAVE) | (axradio_mode == AXRADIO_MODE_SYNC_SLAVE) );
      002B9B 74 33            [12] 5081 	mov	a,#0x33
      002B9D B5 17 04         [24] 5082 	cjne	a,_axradio_mode,00116$
      002BA0 74 01            [12] 5083 	mov	a,#0x01
      002BA2 80 01            [24] 5084 	sjmp	00117$
      002BA4                       5085 00116$:
      002BA4 E4               [12] 5086 	clr	a
      002BA5                       5087 00117$:
      002BA5 FF               [12] 5088 	mov	r7,a
      002BA6 90 90 EF         [24] 5089 	mov	dptr,#_axradio_framing_enable_sfdcallback
      002BA9 E4               [12] 5090 	clr	a
      002BAA 93               [24] 5091 	movc	a,@a+dptr
      002BAB 42 07            [12] 5092 	orl	ar7,a
      002BAD 74 32            [12] 5093 	mov	a,#0x32
      002BAF B5 17 04         [24] 5094 	cjne	a,_axradio_mode,00118$
      002BB2 74 01            [12] 5095 	mov	a,#0x01
      002BB4 80 01            [24] 5096 	sjmp	00119$
      002BB6                       5097 00118$:
      002BB6 E4               [12] 5098 	clr	a
      002BB7                       5099 00119$:
      002BB7 42 07            [12] 5100 	orl	ar7,a
                                   5101 ;	..\src\COMMON\easyax5043.c:881: if(rschanged_int)
      002BB9 EF               [12] 5102 	mov	a,r7
      002BBA FE               [12] 5103 	mov	r6,a
      002BBB 60 06            [24] 5104 	jz	00102$
                                   5105 ;	..\src\COMMON\easyax5043.c:882: AX5043_RADIOEVENTMASK0 = 0x04;
      002BBD 90 40 09         [24] 5106 	mov	dptr,#_AX5043_RADIOEVENTMASK0
      002BC0 74 04            [12] 5107 	mov	a,#0x04
      002BC2 F0               [24] 5108 	movx	@dptr,a
      002BC3                       5109 00102$:
                                   5110 ;	..\src\COMMON\easyax5043.c:883: AX5043_RSSIREFERENCE = axradio_phy_rssireference;
      002BC3 90 90 CE         [24] 5111 	mov	dptr,#_axradio_phy_rssireference
      002BC6 E4               [12] 5112 	clr	a
      002BC7 93               [24] 5113 	movc	a,@a+dptr
      002BC8 90 42 2C         [24] 5114 	mov	dptr,#_AX5043_RSSIREFERENCE
      002BCB F0               [24] 5115 	movx	@dptr,a
                                   5116 ;	..\src\COMMON\easyax5043.c:884: ax5043_set_registers_rxcont();
      002BCC C0 06            [24] 5117 	push	ar6
      002BCE 12 18 CC         [24] 5118 	lcall	_ax5043_set_registers_rxcont
      002BD1 D0 06            [24] 5119 	pop	ar6
                                   5120 ;	..\src\COMMON\easyax5043.c:897: AX5043_PKTSTOREFLAGS &= (uint8_t)~0x40;
      002BD3 90 42 32         [24] 5121 	mov	dptr,#_AX5043_PKTSTOREFLAGS
      002BD6 E0               [24] 5122 	movx	a,@dptr
      002BD7 53 E0 BF         [24] 5123 	anl	acc,#0xbf
      002BDA F0               [24] 5124 	movx	@dptr,a
                                   5125 ;	..\src\COMMON\easyax5043.c:900: AX5043_FIFOSTAT = 3; // clear FIFO data & flags
      002BDB 90 40 28         [24] 5126 	mov	dptr,#_AX5043_FIFOSTAT
      002BDE 74 03            [12] 5127 	mov	a,#0x03
      002BE0 F0               [24] 5128 	movx	@dptr,a
                                   5129 ;	..\src\COMMON\easyax5043.c:901: AX5043_PWRMODE = AX5043_PWRSTATE_FULL_RX;
      002BE1 90 40 02         [24] 5130 	mov	dptr,#_AX5043_PWRMODE
      002BE4 74 09            [12] 5131 	mov	a,#0x09
      002BE6 F0               [24] 5132 	movx	@dptr,a
                                   5133 ;	..\src\COMMON\easyax5043.c:902: axradio_trxstate = trxstate_rx;
      002BE7 75 18 01         [24] 5134 	mov	_axradio_trxstate,#0x01
                                   5135 ;	..\src\COMMON\easyax5043.c:903: if(rschanged_int)
      002BEA EE               [12] 5136 	mov	a,r6
      002BEB 60 08            [24] 5137 	jz	00104$
                                   5138 ;	..\src\COMMON\easyax5043.c:904: AX5043_IRQMASK0 = 0x41; //  enable FIFO not empty / radio controller irq
      002BED 90 40 07         [24] 5139 	mov	dptr,#_AX5043_IRQMASK0
      002BF0 74 41            [12] 5140 	mov	a,#0x41
      002BF2 F0               [24] 5141 	movx	@dptr,a
      002BF3 80 06            [24] 5142 	sjmp	00105$
      002BF5                       5143 00104$:
                                   5144 ;	..\src\COMMON\easyax5043.c:906: AX5043_IRQMASK0 = 0x01; //  enable FIFO not empty
      002BF5 90 40 07         [24] 5145 	mov	dptr,#_AX5043_IRQMASK0
      002BF8 74 01            [12] 5146 	mov	a,#0x01
      002BFA F0               [24] 5147 	movx	@dptr,a
      002BFB                       5148 00105$:
                                   5149 ;	..\src\COMMON\easyax5043.c:907: AX5043_IRQMASK1 = 0x00;
      002BFB 90 40 06         [24] 5150 	mov	dptr,#_AX5043_IRQMASK1
      002BFE E4               [12] 5151 	clr	a
      002BFF F0               [24] 5152 	movx	@dptr,a
                                   5153 ;	..\src\COMMON\easyax5043.c:908: }
      002C00 22               [24] 5154 	ret
                                   5155 ;------------------------------------------------------------
                                   5156 ;Allocation info for local variables in function 'ax5043_receiver_on_wor'
                                   5157 ;------------------------------------------------------------
                                   5158 ;wp                        Allocated to registers r4 r5 
                                   5159 ;------------------------------------------------------------
                                   5160 ;	..\src\COMMON\easyax5043.c:910: __reentrantb void ax5043_receiver_on_wor(void) __reentrant
                                   5161 ;	-----------------------------------------
                                   5162 ;	 function ax5043_receiver_on_wor
                                   5163 ;	-----------------------------------------
      002C01                       5164 _ax5043_receiver_on_wor:
                                   5165 ;	..\src\COMMON\easyax5043.c:912: AX5043_BGNDRSSIGAIN = 0x02;
      002C01 90 42 2E         [24] 5166 	mov	dptr,#_AX5043_BGNDRSSIGAIN
      002C04 74 02            [12] 5167 	mov	a,#0x02
      002C06 F0               [24] 5168 	movx	@dptr,a
                                   5169 ;	..\src\COMMON\easyax5043.c:913: if(axradio_framing_enable_sfdcallback)
      002C07 90 90 EF         [24] 5170 	mov	dptr,#_axradio_framing_enable_sfdcallback
      002C0A E4               [12] 5171 	clr	a
      002C0B 93               [24] 5172 	movc	a,@a+dptr
      002C0C 60 06            [24] 5173 	jz	00102$
                                   5174 ;	..\src\COMMON\easyax5043.c:914: AX5043_RADIOEVENTMASK0 = 0x04;
      002C0E 90 40 09         [24] 5175 	mov	dptr,#_AX5043_RADIOEVENTMASK0
      002C11 74 04            [12] 5176 	mov	a,#0x04
      002C13 F0               [24] 5177 	movx	@dptr,a
      002C14                       5178 00102$:
                                   5179 ;	..\src\COMMON\easyax5043.c:915: AX5043_FIFOSTAT = 3; // clear FIFO data & flags
      002C14 90 40 28         [24] 5180 	mov	dptr,#_AX5043_FIFOSTAT
      002C17 74 03            [12] 5181 	mov	a,#0x03
      002C19 F0               [24] 5182 	movx	@dptr,a
                                   5183 ;	..\src\COMMON\easyax5043.c:916: AX5043_LPOSCCONFIG = 0x01; // start LPOSC, slow mode
      002C1A 90 43 10         [24] 5184 	mov	dptr,#_AX5043_LPOSCCONFIG
      002C1D 74 01            [12] 5185 	mov	a,#0x01
      002C1F F0               [24] 5186 	movx	@dptr,a
                                   5187 ;	..\src\COMMON\easyax5043.c:917: AX5043_RSSIREFERENCE = axradio_phy_rssireference;
      002C20 90 90 CE         [24] 5188 	mov	dptr,#_axradio_phy_rssireference
      002C23 E4               [12] 5189 	clr	a
      002C24 93               [24] 5190 	movc	a,@a+dptr
      002C25 90 42 2C         [24] 5191 	mov	dptr,#_AX5043_RSSIREFERENCE
      002C28 F0               [24] 5192 	movx	@dptr,a
                                   5193 ;	..\src\COMMON\easyax5043.c:918: ax5043_set_registers_rxwor();
      002C29 12 18 B9         [24] 5194 	lcall	_ax5043_set_registers_rxwor
                                   5195 ;	..\src\COMMON\easyax5043.c:919: AX5043_PKTSTOREFLAGS &= (uint8_t)~0x40;
      002C2C 90 42 32         [24] 5196 	mov	dptr,#_AX5043_PKTSTOREFLAGS
      002C2F E0               [24] 5197 	movx	a,@dptr
      002C30 53 E0 BF         [24] 5198 	anl	acc,#0xbf
      002C33 F0               [24] 5199 	movx	@dptr,a
                                   5200 ;	..\src\COMMON\easyax5043.c:921: AX5043_PWRMODE = AX5043_PWRSTATE_WOR_RX;
      002C34 90 40 02         [24] 5201 	mov	dptr,#_AX5043_PWRMODE
      002C37 74 0B            [12] 5202 	mov	a,#0x0b
      002C39 F0               [24] 5203 	movx	@dptr,a
                                   5204 ;	..\src\COMMON\easyax5043.c:922: axradio_trxstate = trxstate_rxwor;
      002C3A 75 18 02         [24] 5205 	mov	_axradio_trxstate,#0x02
                                   5206 ;	..\src\COMMON\easyax5043.c:923: if(axradio_framing_enable_sfdcallback)
      002C3D 90 90 EF         [24] 5207 	mov	dptr,#_axradio_framing_enable_sfdcallback
      002C40 E4               [12] 5208 	clr	a
      002C41 93               [24] 5209 	movc	a,@a+dptr
      002C42 60 08            [24] 5210 	jz	00104$
                                   5211 ;	..\src\COMMON\easyax5043.c:924: AX5043_IRQMASK0 = 0x41; //  enable FIFO not empty / radio controller irq
      002C44 90 40 07         [24] 5212 	mov	dptr,#_AX5043_IRQMASK0
      002C47 74 41            [12] 5213 	mov	a,#0x41
      002C49 F0               [24] 5214 	movx	@dptr,a
      002C4A 80 06            [24] 5215 	sjmp	00105$
      002C4C                       5216 00104$:
                                   5217 ;	..\src\COMMON\easyax5043.c:926: AX5043_IRQMASK0 = 0x01; //  enable FIFO not empty
      002C4C 90 40 07         [24] 5218 	mov	dptr,#_AX5043_IRQMASK0
      002C4F 74 01            [12] 5219 	mov	a,#0x01
      002C51 F0               [24] 5220 	movx	@dptr,a
      002C52                       5221 00105$:
                                   5222 ;	..\src\COMMON\easyax5043.c:928: if( ( (PALTRADIO & 0x40) && ((AX5043_PINFUNCPWRAMP & 0x0F) == 0x07) ) || ( (PALTRADIO & 0x80) && ( (AX5043_PINFUNCANTSEL & 0x07 ) == 0x04 ) ) ) // pass through of TCXO_EN
      002C52 90 70 46         [24] 5223 	mov	dptr,#_PALTRADIO
      002C55 E0               [24] 5224 	movx	a,@dptr
      002C56 30 E6 12         [24] 5225 	jnb	acc.6,00110$
      002C59 90 40 26         [24] 5226 	mov	dptr,#_AX5043_PINFUNCPWRAMP
      002C5C E0               [24] 5227 	movx	a,@dptr
      002C5D FF               [12] 5228 	mov	r7,a
      002C5E 53 07 0F         [24] 5229 	anl	ar7,#0x0f
      002C61 7E 00            [12] 5230 	mov	r6,#0x00
      002C63 BF 07 05         [24] 5231 	cjne	r7,#0x07,00136$
      002C66 BE 00 02         [24] 5232 	cjne	r6,#0x00,00136$
      002C69 80 17            [24] 5233 	sjmp	00106$
      002C6B                       5234 00136$:
      002C6B                       5235 00110$:
      002C6B 90 70 46         [24] 5236 	mov	dptr,#_PALTRADIO
      002C6E E0               [24] 5237 	movx	a,@dptr
      002C6F 30 E7 1E         [24] 5238 	jnb	acc.7,00107$
      002C72 90 40 25         [24] 5239 	mov	dptr,#_AX5043_PINFUNCANTSEL
      002C75 E0               [24] 5240 	movx	a,@dptr
      002C76 FF               [12] 5241 	mov	r7,a
      002C77 53 07 07         [24] 5242 	anl	ar7,#0x07
      002C7A 7E 00            [12] 5243 	mov	r6,#0x00
      002C7C BF 04 11         [24] 5244 	cjne	r7,#0x04,00107$
      002C7F BE 00 0E         [24] 5245 	cjne	r6,#0x00,00107$
      002C82                       5246 00106$:
                                   5247 ;	..\src\COMMON\easyax5043.c:931: AX5043_IRQMASK0 |= 0x80; // power irq (AX8052F143 WOR with TCXO)
      002C82 90 40 07         [24] 5248 	mov	dptr,#_AX5043_IRQMASK0
      002C85 E0               [24] 5249 	movx	a,@dptr
      002C86 43 E0 80         [24] 5250 	orl	acc,#0x80
      002C89 F0               [24] 5251 	movx	@dptr,a
                                   5252 ;	..\src\COMMON\easyax5043.c:932: AX5043_POWIRQMASK = 0x90; // interrupt when vddana ready (AX8052F143 WOR with TCXO)
      002C8A 90 40 05         [24] 5253 	mov	dptr,#_AX5043_POWIRQMASK
      002C8D 74 90            [12] 5254 	mov	a,#0x90
      002C8F F0               [24] 5255 	movx	@dptr,a
      002C90                       5256 00107$:
                                   5257 ;	..\src\COMMON\easyax5043.c:935: AX5043_IRQMASK1 = 0x01; // xtal ready
      002C90 90 40 06         [24] 5258 	mov	dptr,#_AX5043_IRQMASK1
      002C93 74 01            [12] 5259 	mov	a,#0x01
      002C95 F0               [24] 5260 	movx	@dptr,a
                                   5261 ;	..\src\COMMON\easyax5043.c:937: uint16_t wp = axradio_wor_period;
      002C96 90 90 FB         [24] 5262 	mov	dptr,#_axradio_wor_period
      002C99 E4               [12] 5263 	clr	a
      002C9A 93               [24] 5264 	movc	a,@a+dptr
      002C9B FE               [12] 5265 	mov	r6,a
      002C9C 74 01            [12] 5266 	mov	a,#0x01
      002C9E 93               [24] 5267 	movc	a,@a+dptr
                                   5268 ;	..\src\COMMON\easyax5043.c:938: AX5043_WAKEUPFREQ1 = (wp >> 8) & 0xFF;
      002C9F FF               [12] 5269 	mov	r7,a
      002CA0 90 40 6C         [24] 5270 	mov	dptr,#_AX5043_WAKEUPFREQ1
      002CA3 F0               [24] 5271 	movx	@dptr,a
                                   5272 ;	..\src\COMMON\easyax5043.c:939: AX5043_WAKEUPFREQ0 = (wp >> 0) & 0xFF;  // actually wakeup period measured in LP OSC cycles
      002CA4 90 40 6D         [24] 5273 	mov	dptr,#_AX5043_WAKEUPFREQ0
      002CA7 EE               [12] 5274 	mov	a,r6
      002CA8 F0               [24] 5275 	movx	@dptr,a
                                   5276 ;	..\src\COMMON\easyax5043.c:940: wp += radio_read16((uint16_t)&AX5043_WAKEUPTIMER1);
      002CA9 7C 68            [12] 5277 	mov	r4,#_AX5043_WAKEUPTIMER1
      002CAB 7D 40            [12] 5278 	mov	r5,#(_AX5043_WAKEUPTIMER1 >> 8)
      002CAD 8C 82            [24] 5279 	mov	dpl,r4
      002CAF 8D 83            [24] 5280 	mov	dph,r5
      002CB1 12 7A F3         [24] 5281 	lcall	_radio_read16
      002CB4 AC 82            [24] 5282 	mov	r4,dpl
      002CB6 AD 83            [24] 5283 	mov	r5,dph
      002CB8 EC               [12] 5284 	mov	a,r4
      002CB9 2E               [12] 5285 	add	a,r6
      002CBA FC               [12] 5286 	mov	r4,a
      002CBB ED               [12] 5287 	mov	a,r5
      002CBC 3F               [12] 5288 	addc	a,r7
                                   5289 ;	..\src\COMMON\easyax5043.c:941: AX5043_WAKEUP1 = (wp >> 8) & 0xFF;
      002CBD 90 40 6A         [24] 5290 	mov	dptr,#_AX5043_WAKEUP1
      002CC0 F0               [24] 5291 	movx	@dptr,a
                                   5292 ;	..\src\COMMON\easyax5043.c:942: AX5043_WAKEUP0 = (wp >> 0) & 0xFF;
      002CC1 90 40 6B         [24] 5293 	mov	dptr,#_AX5043_WAKEUP0
      002CC4 EC               [12] 5294 	mov	a,r4
      002CC5 F0               [24] 5295 	movx	@dptr,a
                                   5296 ;	..\src\COMMON\easyax5043.c:944: }
      002CC6 22               [24] 5297 	ret
                                   5298 ;------------------------------------------------------------
                                   5299 ;Allocation info for local variables in function 'ax5043_prepare_tx'
                                   5300 ;------------------------------------------------------------
                                   5301 ;	..\src\COMMON\easyax5043.c:945: __reentrantb void ax5043_prepare_tx(void) __reentrant
                                   5302 ;	-----------------------------------------
                                   5303 ;	 function ax5043_prepare_tx
                                   5304 ;	-----------------------------------------
      002CC7                       5305 _ax5043_prepare_tx:
                                   5306 ;	..\src\COMMON\easyax5043.c:947: AX5043_PWRMODE = AX5043_PWRSTATE_XTAL_ON;
      002CC7 90 40 02         [24] 5307 	mov	dptr,#_AX5043_PWRMODE
      002CCA 74 05            [12] 5308 	mov	a,#0x05
      002CCC F0               [24] 5309 	movx	@dptr,a
                                   5310 ;	..\src\COMMON\easyax5043.c:948: AX5043_PWRMODE = AX5043_PWRSTATE_FIFO_ON;
      002CCD 74 07            [12] 5311 	mov	a,#0x07
      002CCF F0               [24] 5312 	movx	@dptr,a
                                   5313 ;	..\src\COMMON\easyax5043.c:949: ax5043_init_registers_tx();
      002CD0 12 1F 4A         [24] 5314 	lcall	_ax5043_init_registers_tx
                                   5315 ;	..\src\COMMON\easyax5043.c:950: AX5043_FIFOTHRESH1 = 0;
      002CD3 90 40 2E         [24] 5316 	mov	dptr,#_AX5043_FIFOTHRESH1
      002CD6 E4               [12] 5317 	clr	a
      002CD7 F0               [24] 5318 	movx	@dptr,a
                                   5319 ;	..\src\COMMON\easyax5043.c:951: AX5043_FIFOTHRESH0 = 0x80;
      002CD8 90 40 2F         [24] 5320 	mov	dptr,#_AX5043_FIFOTHRESH0
      002CDB 74 80            [12] 5321 	mov	a,#0x80
      002CDD F0               [24] 5322 	movx	@dptr,a
                                   5323 ;	..\src\COMMON\easyax5043.c:952: axradio_trxstate = trxstate_tx_xtalwait;
      002CDE 75 18 09         [24] 5324 	mov	_axradio_trxstate,#0x09
                                   5325 ;	..\src\COMMON\easyax5043.c:953: AX5043_IRQMASK0 = 0x00;
      002CE1 90 40 07         [24] 5326 	mov	dptr,#_AX5043_IRQMASK0
      002CE4 E4               [12] 5327 	clr	a
      002CE5 F0               [24] 5328 	movx	@dptr,a
                                   5329 ;	..\src\COMMON\easyax5043.c:954: AX5043_IRQMASK1 = 0x01; // enable xtal ready interrupt
      002CE6 90 40 06         [24] 5330 	mov	dptr,#_AX5043_IRQMASK1
      002CE9 04               [12] 5331 	inc	a
      002CEA F0               [24] 5332 	movx	@dptr,a
                                   5333 ;	..\src\COMMON\easyax5043.c:955: AX5043_POWSTICKYSTAT; // clear pwr management sticky status --> brownout gate works
      002CEB 90 40 04         [24] 5334 	mov	dptr,#_AX5043_POWSTICKYSTAT
      002CEE E0               [24] 5335 	movx	a,@dptr
                                   5336 ;	..\src\COMMON\easyax5043.c:956: }
      002CEF 22               [24] 5337 	ret
                                   5338 ;------------------------------------------------------------
                                   5339 ;Allocation info for local variables in function 'ax5043_off'
                                   5340 ;------------------------------------------------------------
                                   5341 ;	..\src\COMMON\easyax5043.c:958: __reentrantb void ax5043_off(void) __reentrant
                                   5342 ;	-----------------------------------------
                                   5343 ;	 function ax5043_off
                                   5344 ;	-----------------------------------------
      002CF0                       5345 _ax5043_off:
                                   5346 ;	..\src\COMMON\easyax5043.c:960: ax5043_off_xtal();
      002CF0 12 2C F9         [24] 5347 	lcall	_ax5043_off_xtal
                                   5348 ;	..\src\COMMON\easyax5043.c:961: AX5043_PWRMODE = AX5043_PWRSTATE_POWERDOWN;
      002CF3 90 40 02         [24] 5349 	mov	dptr,#_AX5043_PWRMODE
      002CF6 E4               [12] 5350 	clr	a
      002CF7 F0               [24] 5351 	movx	@dptr,a
                                   5352 ;	..\src\COMMON\easyax5043.c:962: }
      002CF8 22               [24] 5353 	ret
                                   5354 ;------------------------------------------------------------
                                   5355 ;Allocation info for local variables in function 'ax5043_off_xtal'
                                   5356 ;------------------------------------------------------------
                                   5357 ;	..\src\COMMON\easyax5043.c:964: __reentrantb void ax5043_off_xtal(void) __reentrant
                                   5358 ;	-----------------------------------------
                                   5359 ;	 function ax5043_off_xtal
                                   5360 ;	-----------------------------------------
      002CF9                       5361 _ax5043_off_xtal:
                                   5362 ;	..\src\COMMON\easyax5043.c:966: AX5043_IRQMASK0 = 0x00; // IRQ off
      002CF9 90 40 07         [24] 5363 	mov	dptr,#_AX5043_IRQMASK0
      002CFC E4               [12] 5364 	clr	a
      002CFD F0               [24] 5365 	movx	@dptr,a
                                   5366 ;	..\src\COMMON\easyax5043.c:967: AX5043_IRQMASK1 = 0x00;
      002CFE 90 40 06         [24] 5367 	mov	dptr,#_AX5043_IRQMASK1
      002D01 F0               [24] 5368 	movx	@dptr,a
                                   5369 ;	..\src\COMMON\easyax5043.c:968: AX5043_PWRMODE = AX5043_PWRSTATE_XTAL_ON;
      002D02 90 40 02         [24] 5370 	mov	dptr,#_AX5043_PWRMODE
      002D05 74 05            [12] 5371 	mov	a,#0x05
      002D07 F0               [24] 5372 	movx	@dptr,a
                                   5373 ;	..\src\COMMON\easyax5043.c:969: AX5043_LPOSCCONFIG = 0x00; // LPOSC off
      002D08 90 43 10         [24] 5374 	mov	dptr,#_AX5043_LPOSCCONFIG
      002D0B E4               [12] 5375 	clr	a
      002D0C F0               [24] 5376 	movx	@dptr,a
                                   5377 ;	..\src\COMMON\easyax5043.c:970: axradio_trxstate = trxstate_off;
                                   5378 ;	1-genFromRTrack replaced	mov	_axradio_trxstate,#0x00
      002D0D F5 18            [12] 5379 	mov	_axradio_trxstate,a
                                   5380 ;	..\src\COMMON\easyax5043.c:971: }
      002D0F 22               [24] 5381 	ret
                                   5382 ;------------------------------------------------------------
                                   5383 ;Allocation info for local variables in function 'axradio_wait_for_xtal'
                                   5384 ;------------------------------------------------------------
                                   5385 ;iesave                    Allocated to registers r7 
                                   5386 ;------------------------------------------------------------
                                   5387 ;	..\src\COMMON\easyax5043.c:973: void axradio_wait_for_xtal(void)
                                   5388 ;	-----------------------------------------
                                   5389 ;	 function axradio_wait_for_xtal
                                   5390 ;	-----------------------------------------
      002D10                       5391 _axradio_wait_for_xtal:
                                   5392 ;	..\src\COMMON\easyax5043.c:975: uint8_t __autodata iesave = IE & 0x80;
      002D10 E5 A8            [12] 5393 	mov	a,_IE
      002D12 54 80            [12] 5394 	anl	a,#0x80
      002D14 FF               [12] 5395 	mov	r7,a
                                   5396 ;	..\src\COMMON\easyax5043.c:976: EA = 0;
                                   5397 ;	assignBit
      002D15 C2 AF            [12] 5398 	clr	_EA
                                   5399 ;	..\src\COMMON\easyax5043.c:977: axradio_trxstate = trxstate_wait_xtal;
      002D17 75 18 03         [24] 5400 	mov	_axradio_trxstate,#0x03
                                   5401 ;	..\src\COMMON\easyax5043.c:978: AX5043_IRQMASK1 |= 0x01; // enable xtal ready interrupt
      002D1A 90 40 06         [24] 5402 	mov	dptr,#_AX5043_IRQMASK1
      002D1D E0               [24] 5403 	movx	a,@dptr
      002D1E 43 E0 01         [24] 5404 	orl	acc,#0x01
      002D21 F0               [24] 5405 	movx	@dptr,a
      002D22                       5406 00104$:
                                   5407 ;	..\src\COMMON\easyax5043.c:980: EA = 0;
                                   5408 ;	assignBit
      002D22 C2 AF            [12] 5409 	clr	_EA
                                   5410 ;	..\src\COMMON\easyax5043.c:981: if (axradio_trxstate == trxstate_xtal_ready)
      002D24 74 04            [12] 5411 	mov	a,#0x04
      002D26 B5 18 02         [24] 5412 	cjne	a,_axradio_trxstate,00116$
      002D29 80 11            [24] 5413 	sjmp	00103$
      002D2B                       5414 00116$:
                                   5415 ;	..\src\COMMON\easyax5043.c:983: wtimer_idle(WTFLAG_CANSTANDBY);
      002D2B 75 82 02         [24] 5416 	mov	dpl,#0x02
      002D2E C0 07            [24] 5417 	push	ar7
      002D30 12 77 90         [24] 5418 	lcall	_wtimer_idle
                                   5419 ;	..\src\COMMON\easyax5043.c:984: EA = 1;
                                   5420 ;	assignBit
      002D33 D2 AF            [12] 5421 	setb	_EA
                                   5422 ;	..\src\COMMON\easyax5043.c:985: wtimer_runcallbacks();
      002D35 12 77 0F         [24] 5423 	lcall	_wtimer_runcallbacks
      002D38 D0 07            [24] 5424 	pop	ar7
      002D3A 80 E6            [24] 5425 	sjmp	00104$
      002D3C                       5426 00103$:
                                   5427 ;	..\src\COMMON\easyax5043.c:987: IE |= iesave;
      002D3C EF               [12] 5428 	mov	a,r7
      002D3D 42 A8            [12] 5429 	orl	_IE,a
                                   5430 ;	..\src\COMMON\easyax5043.c:988: }
      002D3F 22               [24] 5431 	ret
                                   5432 ;------------------------------------------------------------
                                   5433 ;Allocation info for local variables in function 'axradio_setaddrregs'
                                   5434 ;------------------------------------------------------------
                                   5435 ;pn                        Allocated to registers r6 r7 
                                   5436 ;inv                       Allocated to registers r5 
                                   5437 ;------------------------------------------------------------
                                   5438 ;	..\src\COMMON\easyax5043.c:990: static void axradio_setaddrregs(void)
                                   5439 ;	-----------------------------------------
                                   5440 ;	 function axradio_setaddrregs
                                   5441 ;	-----------------------------------------
      002D40                       5442 _axradio_setaddrregs:
                                   5443 ;	..\src\COMMON\easyax5043.c:992: AX5043_PKTADDR0 = axradio_localaddr.addr[0];
      002D40 90 01 04         [24] 5444 	mov	dptr,#_axradio_localaddr
      002D43 E0               [24] 5445 	movx	a,@dptr
      002D44 90 42 07         [24] 5446 	mov	dptr,#_AX5043_PKTADDR0
      002D47 F0               [24] 5447 	movx	@dptr,a
                                   5448 ;	..\src\COMMON\easyax5043.c:993: AX5043_PKTADDR1 = axradio_localaddr.addr[1];
      002D48 90 01 05         [24] 5449 	mov	dptr,#(_axradio_localaddr + 0x0001)
      002D4B E0               [24] 5450 	movx	a,@dptr
      002D4C 90 42 06         [24] 5451 	mov	dptr,#_AX5043_PKTADDR1
      002D4F F0               [24] 5452 	movx	@dptr,a
                                   5453 ;	..\src\COMMON\easyax5043.c:994: AX5043_PKTADDR2 = axradio_localaddr.addr[2];
      002D50 90 01 06         [24] 5454 	mov	dptr,#(_axradio_localaddr + 0x0002)
      002D53 E0               [24] 5455 	movx	a,@dptr
      002D54 90 42 05         [24] 5456 	mov	dptr,#_AX5043_PKTADDR2
      002D57 F0               [24] 5457 	movx	@dptr,a
                                   5458 ;	..\src\COMMON\easyax5043.c:995: AX5043_PKTADDR3 = axradio_localaddr.addr[3];
      002D58 90 01 07         [24] 5459 	mov	dptr,#(_axradio_localaddr + 0x0003)
      002D5B E0               [24] 5460 	movx	a,@dptr
      002D5C 90 42 04         [24] 5461 	mov	dptr,#_AX5043_PKTADDR3
      002D5F F0               [24] 5462 	movx	@dptr,a
                                   5463 ;	..\src\COMMON\easyax5043.c:997: AX5043_PKTADDRMASK0 = axradio_localaddr.mask[0];
      002D60 90 01 08         [24] 5464 	mov	dptr,#(_axradio_localaddr + 0x0004)
      002D63 E0               [24] 5465 	movx	a,@dptr
      002D64 90 42 0B         [24] 5466 	mov	dptr,#_AX5043_PKTADDRMASK0
      002D67 F0               [24] 5467 	movx	@dptr,a
                                   5468 ;	..\src\COMMON\easyax5043.c:998: AX5043_PKTADDRMASK1 = axradio_localaddr.mask[1];
      002D68 90 01 09         [24] 5469 	mov	dptr,#(_axradio_localaddr + 0x0005)
      002D6B E0               [24] 5470 	movx	a,@dptr
      002D6C 90 42 0A         [24] 5471 	mov	dptr,#_AX5043_PKTADDRMASK1
      002D6F F0               [24] 5472 	movx	@dptr,a
                                   5473 ;	..\src\COMMON\easyax5043.c:999: AX5043_PKTADDRMASK2 = axradio_localaddr.mask[2];
      002D70 90 01 0A         [24] 5474 	mov	dptr,#(_axradio_localaddr + 0x0006)
      002D73 E0               [24] 5475 	movx	a,@dptr
      002D74 90 42 09         [24] 5476 	mov	dptr,#_AX5043_PKTADDRMASK2
      002D77 F0               [24] 5477 	movx	@dptr,a
                                   5478 ;	..\src\COMMON\easyax5043.c:1000: AX5043_PKTADDRMASK3 = axradio_localaddr.mask[3];
      002D78 90 01 0B         [24] 5479 	mov	dptr,#(_axradio_localaddr + 0x0007)
      002D7B E0               [24] 5480 	movx	a,@dptr
      002D7C 90 42 08         [24] 5481 	mov	dptr,#_AX5043_PKTADDRMASK3
      002D7F F0               [24] 5482 	movx	@dptr,a
                                   5483 ;	..\src\COMMON\easyax5043.c:1002: if (axradio_phy_pn9 && axradio_framing_addrlen) {
      002D80 90 90 C0         [24] 5484 	mov	dptr,#_axradio_phy_pn9
      002D83 E4               [12] 5485 	clr	a
      002D84 93               [24] 5486 	movc	a,@a+dptr
      002D85 70 01            [24] 5487 	jnz	00123$
      002D87 22               [24] 5488 	ret
      002D88                       5489 00123$:
      002D88 90 90 E2         [24] 5490 	mov	dptr,#_axradio_framing_addrlen
      002D8B E4               [12] 5491 	clr	a
      002D8C 93               [24] 5492 	movc	a,@a+dptr
      002D8D 70 01            [24] 5493 	jnz	00124$
      002D8F 22               [24] 5494 	ret
      002D90                       5495 00124$:
                                   5496 ;	..\src\COMMON\easyax5043.c:1003: uint16_t __autodata pn = 0x1ff;
      002D90 7E FF            [12] 5497 	mov	r6,#0xff
      002D92 7F 01            [12] 5498 	mov	r7,#0x01
                                   5499 ;	..\src\COMMON\easyax5043.c:1004: uint8_t __autodata inv = -(AX5043_ENCODING & 0x01);
      002D94 90 40 11         [24] 5500 	mov	dptr,#_AX5043_ENCODING
      002D97 E0               [24] 5501 	movx	a,@dptr
      002D98 FD               [12] 5502 	mov	r5,a
      002D99 53 05 01         [24] 5503 	anl	ar5,#0x01
      002D9C C3               [12] 5504 	clr	c
      002D9D E4               [12] 5505 	clr	a
      002D9E 9D               [12] 5506 	subb	a,r5
      002D9F FD               [12] 5507 	mov	r5,a
                                   5508 ;	..\src\COMMON\easyax5043.c:1005: if (axradio_framing_destaddrpos != 0xff)
      002DA0 90 90 E3         [24] 5509 	mov	dptr,#_axradio_framing_destaddrpos
      002DA3 E4               [12] 5510 	clr	a
      002DA4 93               [24] 5511 	movc	a,@a+dptr
      002DA5 FC               [12] 5512 	mov	r4,a
      002DA6 BC FF 02         [24] 5513 	cjne	r4,#0xff,00125$
      002DA9 80 25            [24] 5514 	sjmp	00102$
      002DAB                       5515 00125$:
                                   5516 ;	..\src\COMMON\easyax5043.c:1006: pn = pn9_advance_bits(pn, axradio_framing_destaddrpos << 3);
      002DAB E4               [12] 5517 	clr	a
      002DAC 03               [12] 5518 	rr	a
      002DAD 54 F8            [12] 5519 	anl	a,#0xf8
      002DAF CC               [12] 5520 	xch	a,r4
      002DB0 C4               [12] 5521 	swap	a
      002DB1 03               [12] 5522 	rr	a
      002DB2 CC               [12] 5523 	xch	a,r4
      002DB3 6C               [12] 5524 	xrl	a,r4
      002DB4 CC               [12] 5525 	xch	a,r4
      002DB5 54 F8            [12] 5526 	anl	a,#0xf8
      002DB7 CC               [12] 5527 	xch	a,r4
      002DB8 6C               [12] 5528 	xrl	a,r4
      002DB9 FB               [12] 5529 	mov	r3,a
      002DBA C0 05            [24] 5530 	push	ar5
      002DBC C0 04            [24] 5531 	push	ar4
      002DBE C0 03            [24] 5532 	push	ar3
      002DC0 90 01 FF         [24] 5533 	mov	dptr,#0x01ff
      002DC3 12 8A C7         [24] 5534 	lcall	_pn9_advance_bits
      002DC6 AE 82            [24] 5535 	mov	r6,dpl
      002DC8 AF 83            [24] 5536 	mov	r7,dph
      002DCA 15 81            [12] 5537 	dec	sp
      002DCC 15 81            [12] 5538 	dec	sp
      002DCE D0 05            [24] 5539 	pop	ar5
      002DD0                       5540 00102$:
                                   5541 ;	..\src\COMMON\easyax5043.c:1007: AX5043_PKTADDR0 ^= pn ^ inv;
      002DD0 7C 00            [12] 5542 	mov	r4,#0x00
      002DD2 ED               [12] 5543 	mov	a,r5
      002DD3 6E               [12] 5544 	xrl	a,r6
      002DD4 FA               [12] 5545 	mov	r2,a
      002DD5 EC               [12] 5546 	mov	a,r4
      002DD6 6F               [12] 5547 	xrl	a,r7
      002DD7 FB               [12] 5548 	mov	r3,a
      002DD8 90 42 07         [24] 5549 	mov	dptr,#_AX5043_PKTADDR0
      002DDB E0               [24] 5550 	movx	a,@dptr
      002DDC 79 00            [12] 5551 	mov	r1,#0x00
      002DDE 62 02            [12] 5552 	xrl	ar2,a
      002DE0 E9               [12] 5553 	mov	a,r1
      002DE1 62 03            [12] 5554 	xrl	ar3,a
      002DE3 90 42 07         [24] 5555 	mov	dptr,#_AX5043_PKTADDR0
      002DE6 EA               [12] 5556 	mov	a,r2
      002DE7 F0               [24] 5557 	movx	@dptr,a
                                   5558 ;	..\src\COMMON\easyax5043.c:1008: pn = pn9_advance_byte(pn);
      002DE8 8E 82            [24] 5559 	mov	dpl,r6
      002DEA 8F 83            [24] 5560 	mov	dph,r7
      002DEC C0 05            [24] 5561 	push	ar5
      002DEE C0 04            [24] 5562 	push	ar4
      002DF0 12 8C D8         [24] 5563 	lcall	_pn9_advance_byte
      002DF3 AE 82            [24] 5564 	mov	r6,dpl
      002DF5 AF 83            [24] 5565 	mov	r7,dph
      002DF7 D0 04            [24] 5566 	pop	ar4
      002DF9 D0 05            [24] 5567 	pop	ar5
                                   5568 ;	..\src\COMMON\easyax5043.c:1009: AX5043_PKTADDR1 ^= pn ^ inv;
      002DFB ED               [12] 5569 	mov	a,r5
      002DFC 6E               [12] 5570 	xrl	a,r6
      002DFD FA               [12] 5571 	mov	r2,a
      002DFE EC               [12] 5572 	mov	a,r4
      002DFF 6F               [12] 5573 	xrl	a,r7
      002E00 FB               [12] 5574 	mov	r3,a
      002E01 90 42 06         [24] 5575 	mov	dptr,#_AX5043_PKTADDR1
      002E04 E0               [24] 5576 	movx	a,@dptr
      002E05 79 00            [12] 5577 	mov	r1,#0x00
      002E07 62 02            [12] 5578 	xrl	ar2,a
      002E09 E9               [12] 5579 	mov	a,r1
      002E0A 62 03            [12] 5580 	xrl	ar3,a
      002E0C 90 42 06         [24] 5581 	mov	dptr,#_AX5043_PKTADDR1
      002E0F EA               [12] 5582 	mov	a,r2
      002E10 F0               [24] 5583 	movx	@dptr,a
                                   5584 ;	..\src\COMMON\easyax5043.c:1010: pn = pn9_advance_byte(pn);
      002E11 8E 82            [24] 5585 	mov	dpl,r6
      002E13 8F 83            [24] 5586 	mov	dph,r7
      002E15 C0 05            [24] 5587 	push	ar5
      002E17 C0 04            [24] 5588 	push	ar4
      002E19 12 8C D8         [24] 5589 	lcall	_pn9_advance_byte
      002E1C AE 82            [24] 5590 	mov	r6,dpl
      002E1E AF 83            [24] 5591 	mov	r7,dph
      002E20 D0 04            [24] 5592 	pop	ar4
      002E22 D0 05            [24] 5593 	pop	ar5
                                   5594 ;	..\src\COMMON\easyax5043.c:1011: AX5043_PKTADDR2 ^= pn ^ inv;
      002E24 ED               [12] 5595 	mov	a,r5
      002E25 6E               [12] 5596 	xrl	a,r6
      002E26 FA               [12] 5597 	mov	r2,a
      002E27 EC               [12] 5598 	mov	a,r4
      002E28 6F               [12] 5599 	xrl	a,r7
      002E29 FB               [12] 5600 	mov	r3,a
      002E2A 90 42 05         [24] 5601 	mov	dptr,#_AX5043_PKTADDR2
      002E2D E0               [24] 5602 	movx	a,@dptr
      002E2E 79 00            [12] 5603 	mov	r1,#0x00
      002E30 62 02            [12] 5604 	xrl	ar2,a
      002E32 E9               [12] 5605 	mov	a,r1
      002E33 62 03            [12] 5606 	xrl	ar3,a
      002E35 90 42 05         [24] 5607 	mov	dptr,#_AX5043_PKTADDR2
      002E38 EA               [12] 5608 	mov	a,r2
      002E39 F0               [24] 5609 	movx	@dptr,a
                                   5610 ;	..\src\COMMON\easyax5043.c:1012: pn = pn9_advance_byte(pn);
      002E3A 8E 82            [24] 5611 	mov	dpl,r6
      002E3C 8F 83            [24] 5612 	mov	dph,r7
      002E3E C0 05            [24] 5613 	push	ar5
      002E40 C0 04            [24] 5614 	push	ar4
      002E42 12 8C D8         [24] 5615 	lcall	_pn9_advance_byte
      002E45 E5 82            [12] 5616 	mov	a,dpl
      002E47 85 83 F0         [24] 5617 	mov	b,dph
      002E4A D0 04            [24] 5618 	pop	ar4
      002E4C D0 05            [24] 5619 	pop	ar5
                                   5620 ;	..\src\COMMON\easyax5043.c:1013: AX5043_PKTADDR3 ^= pn ^ inv;
      002E4E 62 05            [12] 5621 	xrl	ar5,a
      002E50 E5 F0            [12] 5622 	mov	a,b
      002E52 62 04            [12] 5623 	xrl	ar4,a
      002E54 90 42 04         [24] 5624 	mov	dptr,#_AX5043_PKTADDR3
      002E57 E0               [24] 5625 	movx	a,@dptr
      002E58 7E 00            [12] 5626 	mov	r6,#0x00
      002E5A 62 05            [12] 5627 	xrl	ar5,a
      002E5C EE               [12] 5628 	mov	a,r6
      002E5D 62 04            [12] 5629 	xrl	ar4,a
      002E5F 90 42 04         [24] 5630 	mov	dptr,#_AX5043_PKTADDR3
      002E62 ED               [12] 5631 	mov	a,r5
      002E63 F0               [24] 5632 	movx	@dptr,a
                                   5633 ;	..\src\COMMON\easyax5043.c:1015: }
      002E64 22               [24] 5634 	ret
                                   5635 ;------------------------------------------------------------
                                   5636 ;Allocation info for local variables in function 'ax5043_init_registers'
                                   5637 ;------------------------------------------------------------
                                   5638 ;	..\src\COMMON\easyax5043.c:1017: static void ax5043_init_registers(void)
                                   5639 ;	-----------------------------------------
                                   5640 ;	 function ax5043_init_registers
                                   5641 ;	-----------------------------------------
      002E65                       5642 _ax5043_init_registers:
                                   5643 ;	..\src\COMMON\easyax5043.c:1019: ax5043_set_registers();
      002E65 12 15 B5         [24] 5644 	lcall	_ax5043_set_registers
                                   5645 ;	..\src\COMMON\easyax5043.c:1024: AX5043_PKTLENOFFSET += axradio_framing_swcrclen; // add len offs for software CRC16 (used for both, fixed and variable length packets
      002E68 90 90 E8         [24] 5646 	mov	dptr,#_axradio_framing_swcrclen
      002E6B E4               [12] 5647 	clr	a
      002E6C 93               [24] 5648 	movc	a,@a+dptr
      002E6D FF               [12] 5649 	mov	r7,a
      002E6E 90 42 02         [24] 5650 	mov	dptr,#_AX5043_PKTLENOFFSET
      002E71 E0               [24] 5651 	movx	a,@dptr
      002E72 2F               [12] 5652 	add	a,r7
      002E73 F0               [24] 5653 	movx	@dptr,a
                                   5654 ;	..\src\COMMON\easyax5043.c:1025: AX5043_PINFUNCIRQ = 0x03; // use as IRQ pin
      002E74 90 40 24         [24] 5655 	mov	dptr,#_AX5043_PINFUNCIRQ
      002E77 74 03            [12] 5656 	mov	a,#0x03
      002E79 F0               [24] 5657 	movx	@dptr,a
                                   5658 ;	..\src\COMMON\easyax5043.c:1026: AX5043_PKTSTOREFLAGS = axradio_phy_innerfreqloop ? 0x13 : 0x15; // store RF offset, RSSI and delimiter timing
      002E7A 90 90 BF         [24] 5659 	mov	dptr,#_axradio_phy_innerfreqloop
      002E7D E4               [12] 5660 	clr	a
      002E7E 93               [24] 5661 	movc	a,@a+dptr
      002E7F 60 06            [24] 5662 	jz	00103$
      002E81 7E 13            [12] 5663 	mov	r6,#0x13
      002E83 7F 00            [12] 5664 	mov	r7,#0x00
      002E85 80 04            [24] 5665 	sjmp	00104$
      002E87                       5666 00103$:
      002E87 7E 15            [12] 5667 	mov	r6,#0x15
      002E89 7F 00            [12] 5668 	mov	r7,#0x00
      002E8B                       5669 00104$:
      002E8B 90 42 32         [24] 5670 	mov	dptr,#_AX5043_PKTSTOREFLAGS
      002E8E EE               [12] 5671 	mov	a,r6
      002E8F F0               [24] 5672 	movx	@dptr,a
                                   5673 ;	..\src\COMMON\easyax5043.c:1027: axradio_setaddrregs();
                                   5674 ;	..\src\COMMON\easyax5043.c:1028: }
      002E90 02 2D 40         [24] 5675 	ljmp	_axradio_setaddrregs
                                   5676 ;------------------------------------------------------------
                                   5677 ;Allocation info for local variables in function 'axradio_sync_addtime'
                                   5678 ;------------------------------------------------------------
                                   5679 ;dt                        Allocated to registers r4 r5 r6 r7 
                                   5680 ;------------------------------------------------------------
                                   5681 ;	..\src\COMMON\easyax5043.c:1034: static __reentrantb void axradio_sync_addtime(uint32_t dt) __reentrant
                                   5682 ;	-----------------------------------------
                                   5683 ;	 function axradio_sync_addtime
                                   5684 ;	-----------------------------------------
      002E93                       5685 _axradio_sync_addtime:
      002E93 AC 82            [24] 5686 	mov	r4,dpl
      002E95 AD 83            [24] 5687 	mov	r5,dph
      002E97 AE F0            [24] 5688 	mov	r6,b
      002E99 FF               [12] 5689 	mov	r7,a
                                   5690 ;	..\src\COMMON\easyax5043.c:1036: axradio_sync_time += dt;
      002E9A 90 00 F6         [24] 5691 	mov	dptr,#_axradio_sync_time
      002E9D E0               [24] 5692 	movx	a,@dptr
      002E9E F8               [12] 5693 	mov	r0,a
      002E9F A3               [24] 5694 	inc	dptr
      002EA0 E0               [24] 5695 	movx	a,@dptr
      002EA1 F9               [12] 5696 	mov	r1,a
      002EA2 A3               [24] 5697 	inc	dptr
      002EA3 E0               [24] 5698 	movx	a,@dptr
      002EA4 FA               [12] 5699 	mov	r2,a
      002EA5 A3               [24] 5700 	inc	dptr
      002EA6 E0               [24] 5701 	movx	a,@dptr
      002EA7 FB               [12] 5702 	mov	r3,a
      002EA8 90 00 F6         [24] 5703 	mov	dptr,#_axradio_sync_time
      002EAB EC               [12] 5704 	mov	a,r4
      002EAC 28               [12] 5705 	add	a,r0
      002EAD F0               [24] 5706 	movx	@dptr,a
      002EAE ED               [12] 5707 	mov	a,r5
      002EAF 39               [12] 5708 	addc	a,r1
      002EB0 A3               [24] 5709 	inc	dptr
      002EB1 F0               [24] 5710 	movx	@dptr,a
      002EB2 EE               [12] 5711 	mov	a,r6
      002EB3 3A               [12] 5712 	addc	a,r2
      002EB4 A3               [24] 5713 	inc	dptr
      002EB5 F0               [24] 5714 	movx	@dptr,a
      002EB6 EF               [12] 5715 	mov	a,r7
      002EB7 3B               [12] 5716 	addc	a,r3
      002EB8 A3               [24] 5717 	inc	dptr
      002EB9 F0               [24] 5718 	movx	@dptr,a
                                   5719 ;	..\src\COMMON\easyax5043.c:1037: }
      002EBA 22               [24] 5720 	ret
                                   5721 ;------------------------------------------------------------
                                   5722 ;Allocation info for local variables in function 'axradio_sync_subtime'
                                   5723 ;------------------------------------------------------------
                                   5724 ;dt                        Allocated to registers r4 r5 r6 r7 
                                   5725 ;------------------------------------------------------------
                                   5726 ;	..\src\COMMON\easyax5043.c:1039: static __reentrantb void axradio_sync_subtime(uint32_t dt) __reentrant
                                   5727 ;	-----------------------------------------
                                   5728 ;	 function axradio_sync_subtime
                                   5729 ;	-----------------------------------------
      002EBB                       5730 _axradio_sync_subtime:
      002EBB AC 82            [24] 5731 	mov	r4,dpl
      002EBD AD 83            [24] 5732 	mov	r5,dph
      002EBF AE F0            [24] 5733 	mov	r6,b
      002EC1 FF               [12] 5734 	mov	r7,a
                                   5735 ;	..\src\COMMON\easyax5043.c:1041: axradio_sync_time -= dt;
      002EC2 90 00 F6         [24] 5736 	mov	dptr,#_axradio_sync_time
      002EC5 E0               [24] 5737 	movx	a,@dptr
      002EC6 F8               [12] 5738 	mov	r0,a
      002EC7 A3               [24] 5739 	inc	dptr
      002EC8 E0               [24] 5740 	movx	a,@dptr
      002EC9 F9               [12] 5741 	mov	r1,a
      002ECA A3               [24] 5742 	inc	dptr
      002ECB E0               [24] 5743 	movx	a,@dptr
      002ECC FA               [12] 5744 	mov	r2,a
      002ECD A3               [24] 5745 	inc	dptr
      002ECE E0               [24] 5746 	movx	a,@dptr
      002ECF FB               [12] 5747 	mov	r3,a
      002ED0 90 00 F6         [24] 5748 	mov	dptr,#_axradio_sync_time
      002ED3 E8               [12] 5749 	mov	a,r0
      002ED4 C3               [12] 5750 	clr	c
      002ED5 9C               [12] 5751 	subb	a,r4
      002ED6 F0               [24] 5752 	movx	@dptr,a
      002ED7 E9               [12] 5753 	mov	a,r1
      002ED8 9D               [12] 5754 	subb	a,r5
      002ED9 A3               [24] 5755 	inc	dptr
      002EDA F0               [24] 5756 	movx	@dptr,a
      002EDB EA               [12] 5757 	mov	a,r2
      002EDC 9E               [12] 5758 	subb	a,r6
      002EDD A3               [24] 5759 	inc	dptr
      002EDE F0               [24] 5760 	movx	@dptr,a
      002EDF EB               [12] 5761 	mov	a,r3
      002EE0 9F               [12] 5762 	subb	a,r7
      002EE1 A3               [24] 5763 	inc	dptr
      002EE2 F0               [24] 5764 	movx	@dptr,a
                                   5765 ;	..\src\COMMON\easyax5043.c:1042: }
      002EE3 22               [24] 5766 	ret
                                   5767 ;------------------------------------------------------------
                                   5768 ;Allocation info for local variables in function 'axradio_sync_settimeradv'
                                   5769 ;------------------------------------------------------------
                                   5770 ;dt                        Allocated to registers r4 r5 r6 r7 
                                   5771 ;------------------------------------------------------------
                                   5772 ;	..\src\COMMON\easyax5043.c:1044: static __reentrantb void axradio_sync_settimeradv(uint32_t dt) __reentrant
                                   5773 ;	-----------------------------------------
                                   5774 ;	 function axradio_sync_settimeradv
                                   5775 ;	-----------------------------------------
      002EE4                       5776 _axradio_sync_settimeradv:
      002EE4 AC 82            [24] 5777 	mov	r4,dpl
      002EE6 AD 83            [24] 5778 	mov	r5,dph
      002EE8 AE F0            [24] 5779 	mov	r6,b
      002EEA FF               [12] 5780 	mov	r7,a
                                   5781 ;	..\src\COMMON\easyax5043.c:1046: axradio_timer.time = axradio_sync_time;
      002EEB 90 00 F6         [24] 5782 	mov	dptr,#_axradio_sync_time
      002EEE E0               [24] 5783 	movx	a,@dptr
      002EEF F8               [12] 5784 	mov	r0,a
      002EF0 A3               [24] 5785 	inc	dptr
      002EF1 E0               [24] 5786 	movx	a,@dptr
      002EF2 F9               [12] 5787 	mov	r1,a
      002EF3 A3               [24] 5788 	inc	dptr
      002EF4 E0               [24] 5789 	movx	a,@dptr
      002EF5 FA               [12] 5790 	mov	r2,a
      002EF6 A3               [24] 5791 	inc	dptr
      002EF7 E0               [24] 5792 	movx	a,@dptr
      002EF8 FB               [12] 5793 	mov	r3,a
      002EF9 90 03 73         [24] 5794 	mov	dptr,#(_axradio_timer + 0x0004)
      002EFC E8               [12] 5795 	mov	a,r0
      002EFD F0               [24] 5796 	movx	@dptr,a
      002EFE E9               [12] 5797 	mov	a,r1
      002EFF A3               [24] 5798 	inc	dptr
      002F00 F0               [24] 5799 	movx	@dptr,a
      002F01 EA               [12] 5800 	mov	a,r2
      002F02 A3               [24] 5801 	inc	dptr
      002F03 F0               [24] 5802 	movx	@dptr,a
      002F04 EB               [12] 5803 	mov	a,r3
      002F05 A3               [24] 5804 	inc	dptr
      002F06 F0               [24] 5805 	movx	@dptr,a
                                   5806 ;	..\src\COMMON\easyax5043.c:1047: axradio_timer.time -= dt;
      002F07 E8               [12] 5807 	mov	a,r0
      002F08 C3               [12] 5808 	clr	c
      002F09 9C               [12] 5809 	subb	a,r4
      002F0A FC               [12] 5810 	mov	r4,a
      002F0B E9               [12] 5811 	mov	a,r1
      002F0C 9D               [12] 5812 	subb	a,r5
      002F0D FD               [12] 5813 	mov	r5,a
      002F0E EA               [12] 5814 	mov	a,r2
      002F0F 9E               [12] 5815 	subb	a,r6
      002F10 FE               [12] 5816 	mov	r6,a
      002F11 EB               [12] 5817 	mov	a,r3
      002F12 9F               [12] 5818 	subb	a,r7
      002F13 FF               [12] 5819 	mov	r7,a
      002F14 90 03 73         [24] 5820 	mov	dptr,#(_axradio_timer + 0x0004)
      002F17 EC               [12] 5821 	mov	a,r4
      002F18 F0               [24] 5822 	movx	@dptr,a
      002F19 ED               [12] 5823 	mov	a,r5
      002F1A A3               [24] 5824 	inc	dptr
      002F1B F0               [24] 5825 	movx	@dptr,a
      002F1C EE               [12] 5826 	mov	a,r6
      002F1D A3               [24] 5827 	inc	dptr
      002F1E F0               [24] 5828 	movx	@dptr,a
      002F1F EF               [12] 5829 	mov	a,r7
      002F20 A3               [24] 5830 	inc	dptr
      002F21 F0               [24] 5831 	movx	@dptr,a
                                   5832 ;	..\src\COMMON\easyax5043.c:1048: }
      002F22 22               [24] 5833 	ret
                                   5834 ;------------------------------------------------------------
                                   5835 ;Allocation info for local variables in function 'axradio_sync_adjustperiodcorr'
                                   5836 ;------------------------------------------------------------
                                   5837 ;dt                        Allocated to registers r0 r1 r2 r3 
                                   5838 ;------------------------------------------------------------
                                   5839 ;	..\src\COMMON\easyax5043.c:1050: static void axradio_sync_adjustperiodcorr(void)
                                   5840 ;	-----------------------------------------
                                   5841 ;	 function axradio_sync_adjustperiodcorr
                                   5842 ;	-----------------------------------------
      002F23                       5843 _axradio_sync_adjustperiodcorr:
                                   5844 ;	..\src\COMMON\easyax5043.c:1052: int32_t __autodata dt = axradio_conv_time_totimer0(axradio_cb_receive.st.time.t) - axradio_sync_time;
      002F23 90 03 1E         [24] 5845 	mov	dptr,#(_axradio_cb_receive + 0x0006)
      002F26 E0               [24] 5846 	movx	a,@dptr
      002F27 FC               [12] 5847 	mov	r4,a
      002F28 A3               [24] 5848 	inc	dptr
      002F29 E0               [24] 5849 	movx	a,@dptr
      002F2A FD               [12] 5850 	mov	r5,a
      002F2B A3               [24] 5851 	inc	dptr
      002F2C E0               [24] 5852 	movx	a,@dptr
      002F2D FE               [12] 5853 	mov	r6,a
      002F2E A3               [24] 5854 	inc	dptr
      002F2F E0               [24] 5855 	movx	a,@dptr
      002F30 8C 82            [24] 5856 	mov	dpl,r4
      002F32 8D 83            [24] 5857 	mov	dph,r5
      002F34 8E F0            [24] 5858 	mov	b,r6
      002F36 12 1E B7         [24] 5859 	lcall	_axradio_conv_time_totimer0
      002F39 AC 82            [24] 5860 	mov	r4,dpl
      002F3B AD 83            [24] 5861 	mov	r5,dph
      002F3D AE F0            [24] 5862 	mov	r6,b
      002F3F FF               [12] 5863 	mov	r7,a
      002F40 90 00 F6         [24] 5864 	mov	dptr,#_axradio_sync_time
      002F43 E0               [24] 5865 	movx	a,@dptr
      002F44 F8               [12] 5866 	mov	r0,a
      002F45 A3               [24] 5867 	inc	dptr
      002F46 E0               [24] 5868 	movx	a,@dptr
      002F47 F9               [12] 5869 	mov	r1,a
      002F48 A3               [24] 5870 	inc	dptr
      002F49 E0               [24] 5871 	movx	a,@dptr
      002F4A FA               [12] 5872 	mov	r2,a
      002F4B A3               [24] 5873 	inc	dptr
      002F4C E0               [24] 5874 	movx	a,@dptr
      002F4D FB               [12] 5875 	mov	r3,a
      002F4E EC               [12] 5876 	mov	a,r4
      002F4F C3               [12] 5877 	clr	c
      002F50 98               [12] 5878 	subb	a,r0
      002F51 FC               [12] 5879 	mov	r4,a
      002F52 ED               [12] 5880 	mov	a,r5
      002F53 99               [12] 5881 	subb	a,r1
      002F54 FD               [12] 5882 	mov	r5,a
      002F55 EE               [12] 5883 	mov	a,r6
      002F56 9A               [12] 5884 	subb	a,r2
      002F57 FE               [12] 5885 	mov	r6,a
      002F58 EF               [12] 5886 	mov	a,r7
      002F59 9B               [12] 5887 	subb	a,r3
      002F5A FF               [12] 5888 	mov	r7,a
                                   5889 ;	..\src\COMMON\easyax5043.c:1053: axradio_cb_receive.st.rx.phy.timeoffset = dt;
      002F5B 8C 02            [24] 5890 	mov	ar2,r4
      002F5D 8D 03            [24] 5891 	mov	ar3,r5
      002F5F 90 03 28         [24] 5892 	mov	dptr,#(_axradio_cb_receive + 0x0010)
      002F62 EA               [12] 5893 	mov	a,r2
      002F63 F0               [24] 5894 	movx	@dptr,a
      002F64 EB               [12] 5895 	mov	a,r3
      002F65 A3               [24] 5896 	inc	dptr
      002F66 F0               [24] 5897 	movx	@dptr,a
                                   5898 ;	..\src\COMMON\easyax5043.c:1054: if (!checksignedlimit16(axradio_sync_periodcorr, axradio_sync_slave_maxperiod)) {
      002F67 90 00 FA         [24] 5899 	mov	dptr,#_axradio_sync_periodcorr
      002F6A E0               [24] 5900 	movx	a,@dptr
      002F6B FA               [12] 5901 	mov	r2,a
      002F6C A3               [24] 5902 	inc	dptr
      002F6D E0               [24] 5903 	movx	a,@dptr
      002F6E FB               [12] 5904 	mov	r3,a
      002F6F 90 91 11         [24] 5905 	mov	dptr,#_axradio_sync_slave_maxperiod
      002F72 E4               [12] 5906 	clr	a
      002F73 93               [24] 5907 	movc	a,@a+dptr
      002F74 C0 E0            [24] 5908 	push	acc
      002F76 74 01            [12] 5909 	mov	a,#0x01
      002F78 93               [24] 5910 	movc	a,@a+dptr
      002F79 C0 E0            [24] 5911 	push	acc
      002F7B 8A 82            [24] 5912 	mov	dpl,r2
      002F7D 8B 83            [24] 5913 	mov	dph,r3
      002F7F 12 82 63         [24] 5914 	lcall	_checksignedlimit16
      002F82 AB 82            [24] 5915 	mov	r3,dpl
      002F84 15 81            [12] 5916 	dec	sp
      002F86 15 81            [12] 5917 	dec	sp
      002F88 EB               [12] 5918 	mov	a,r3
      002F89 70 4F            [24] 5919 	jnz	00102$
                                   5920 ;	..\src\COMMON\easyax5043.c:1055: axradio_sync_addtime(dt);
      002F8B 8C 82            [24] 5921 	mov	dpl,r4
      002F8D 8D 83            [24] 5922 	mov	dph,r5
      002F8F 8E F0            [24] 5923 	mov	b,r6
      002F91 EF               [12] 5924 	mov	a,r7
      002F92 C0 07            [24] 5925 	push	ar7
      002F94 C0 06            [24] 5926 	push	ar6
      002F96 C0 05            [24] 5927 	push	ar5
      002F98 C0 04            [24] 5928 	push	ar4
      002F9A 12 2E 93         [24] 5929 	lcall	_axradio_sync_addtime
      002F9D D0 04            [24] 5930 	pop	ar4
      002F9F D0 05            [24] 5931 	pop	ar5
      002FA1 D0 06            [24] 5932 	pop	ar6
      002FA3 D0 07            [24] 5933 	pop	ar7
                                   5934 ;	..\src\COMMON\easyax5043.c:1056: dt <<= SYNC_K1;
      002FA5 8E 02            [24] 5935 	mov	ar2,r6
      002FA7 EF               [12] 5936 	mov	a,r7
      002FA8 C4               [12] 5937 	swap	a
      002FA9 23               [12] 5938 	rl	a
      002FAA 54 E0            [12] 5939 	anl	a,#0xe0
      002FAC CA               [12] 5940 	xch	a,r2
      002FAD C4               [12] 5941 	swap	a
      002FAE 23               [12] 5942 	rl	a
      002FAF CA               [12] 5943 	xch	a,r2
      002FB0 6A               [12] 5944 	xrl	a,r2
      002FB1 CA               [12] 5945 	xch	a,r2
      002FB2 54 E0            [12] 5946 	anl	a,#0xe0
      002FB4 CA               [12] 5947 	xch	a,r2
      002FB5 6A               [12] 5948 	xrl	a,r2
      002FB6 FB               [12] 5949 	mov	r3,a
      002FB7 ED               [12] 5950 	mov	a,r5
      002FB8 C4               [12] 5951 	swap	a
      002FB9 23               [12] 5952 	rl	a
      002FBA 54 1F            [12] 5953 	anl	a,#0x1f
      002FBC 4A               [12] 5954 	orl	a,r2
      002FBD FA               [12] 5955 	mov	r2,a
      002FBE 8C 00            [24] 5956 	mov	ar0,r4
      002FC0 ED               [12] 5957 	mov	a,r5
      002FC1 C4               [12] 5958 	swap	a
      002FC2 23               [12] 5959 	rl	a
      002FC3 54 E0            [12] 5960 	anl	a,#0xe0
      002FC5 C8               [12] 5961 	xch	a,r0
      002FC6 C4               [12] 5962 	swap	a
      002FC7 23               [12] 5963 	rl	a
      002FC8 C8               [12] 5964 	xch	a,r0
      002FC9 68               [12] 5965 	xrl	a,r0
      002FCA C8               [12] 5966 	xch	a,r0
      002FCB 54 E0            [12] 5967 	anl	a,#0xe0
      002FCD C8               [12] 5968 	xch	a,r0
      002FCE 68               [12] 5969 	xrl	a,r0
      002FCF F9               [12] 5970 	mov	r1,a
                                   5971 ;	..\src\COMMON\easyax5043.c:1057: axradio_sync_periodcorr = dt;
      002FD0 90 00 FA         [24] 5972 	mov	dptr,#_axradio_sync_periodcorr
      002FD3 E8               [12] 5973 	mov	a,r0
      002FD4 F0               [24] 5974 	movx	@dptr,a
      002FD5 E9               [12] 5975 	mov	a,r1
      002FD6 A3               [24] 5976 	inc	dptr
      002FD7 F0               [24] 5977 	movx	@dptr,a
      002FD8 80 3B            [24] 5978 	sjmp	00103$
      002FDA                       5979 00102$:
                                   5980 ;	..\src\COMMON\easyax5043.c:1059: axradio_sync_periodcorr += dt;
      002FDA 90 00 FA         [24] 5981 	mov	dptr,#_axradio_sync_periodcorr
      002FDD E0               [24] 5982 	movx	a,@dptr
      002FDE FA               [12] 5983 	mov	r2,a
      002FDF A3               [24] 5984 	inc	dptr
      002FE0 E0               [24] 5985 	movx	a,@dptr
      002FE1 FB               [12] 5986 	mov	r3,a
      002FE2 8C 00            [24] 5987 	mov	ar0,r4
      002FE4 8D 01            [24] 5988 	mov	ar1,r5
      002FE6 90 00 FA         [24] 5989 	mov	dptr,#_axradio_sync_periodcorr
      002FE9 E8               [12] 5990 	mov	a,r0
      002FEA 2A               [12] 5991 	add	a,r2
      002FEB F0               [24] 5992 	movx	@dptr,a
      002FEC E9               [12] 5993 	mov	a,r1
      002FED 3B               [12] 5994 	addc	a,r3
      002FEE A3               [24] 5995 	inc	dptr
      002FEF F0               [24] 5996 	movx	@dptr,a
                                   5997 ;	..\src\COMMON\easyax5043.c:1060: dt >>= SYNC_K0;
      002FF0 EF               [12] 5998 	mov	a,r7
      002FF1 A2 E7            [12] 5999 	mov	c,acc.7
      002FF3 13               [12] 6000 	rrc	a
      002FF4 FF               [12] 6001 	mov	r7,a
      002FF5 EE               [12] 6002 	mov	a,r6
      002FF6 13               [12] 6003 	rrc	a
      002FF7 FE               [12] 6004 	mov	r6,a
      002FF8 ED               [12] 6005 	mov	a,r5
      002FF9 13               [12] 6006 	rrc	a
      002FFA FD               [12] 6007 	mov	r5,a
      002FFB EC               [12] 6008 	mov	a,r4
      002FFC 13               [12] 6009 	rrc	a
      002FFD FC               [12] 6010 	mov	r4,a
      002FFE EF               [12] 6011 	mov	a,r7
      002FFF A2 E7            [12] 6012 	mov	c,acc.7
      003001 13               [12] 6013 	rrc	a
      003002 FF               [12] 6014 	mov	r7,a
      003003 EE               [12] 6015 	mov	a,r6
      003004 13               [12] 6016 	rrc	a
      003005 FE               [12] 6017 	mov	r6,a
      003006 ED               [12] 6018 	mov	a,r5
      003007 13               [12] 6019 	rrc	a
      003008 FD               [12] 6020 	mov	r5,a
      003009 EC               [12] 6021 	mov	a,r4
      00300A 13               [12] 6022 	rrc	a
                                   6023 ;	..\src\COMMON\easyax5043.c:1061: axradio_sync_addtime(dt);
      00300B F5 82            [12] 6024 	mov	dpl,a
      00300D 8D 83            [24] 6025 	mov	dph,r5
      00300F 8E F0            [24] 6026 	mov	b,r6
      003011 EF               [12] 6027 	mov	a,r7
      003012 12 2E 93         [24] 6028 	lcall	_axradio_sync_addtime
      003015                       6029 00103$:
                                   6030 ;	..\src\COMMON\easyax5043.c:1063: axradio_sync_periodcorr = signedlimit16(axradio_sync_periodcorr, axradio_sync_slave_maxperiod);
      003015 90 00 FA         [24] 6031 	mov	dptr,#_axradio_sync_periodcorr
      003018 E0               [24] 6032 	movx	a,@dptr
      003019 FE               [12] 6033 	mov	r6,a
      00301A A3               [24] 6034 	inc	dptr
      00301B E0               [24] 6035 	movx	a,@dptr
      00301C FF               [12] 6036 	mov	r7,a
      00301D 90 91 11         [24] 6037 	mov	dptr,#_axradio_sync_slave_maxperiod
      003020 E4               [12] 6038 	clr	a
      003021 93               [24] 6039 	movc	a,@a+dptr
      003022 C0 E0            [24] 6040 	push	acc
      003024 74 01            [12] 6041 	mov	a,#0x01
      003026 93               [24] 6042 	movc	a,@a+dptr
      003027 C0 E0            [24] 6043 	push	acc
      003029 8E 82            [24] 6044 	mov	dpl,r6
      00302B 8F 83            [24] 6045 	mov	dph,r7
      00302D 12 84 35         [24] 6046 	lcall	_signedlimit16
      003030 AE 82            [24] 6047 	mov	r6,dpl
      003032 AF 83            [24] 6048 	mov	r7,dph
      003034 15 81            [12] 6049 	dec	sp
      003036 15 81            [12] 6050 	dec	sp
      003038 90 00 FA         [24] 6051 	mov	dptr,#_axradio_sync_periodcorr
      00303B EE               [12] 6052 	mov	a,r6
      00303C F0               [24] 6053 	movx	@dptr,a
      00303D EF               [12] 6054 	mov	a,r7
      00303E A3               [24] 6055 	inc	dptr
      00303F F0               [24] 6056 	movx	@dptr,a
                                   6057 ;	..\src\COMMON\easyax5043.c:1064: }
      003040 22               [24] 6058 	ret
                                   6059 ;------------------------------------------------------------
                                   6060 ;Allocation info for local variables in function 'axradio_sync_slave_nextperiod'
                                   6061 ;------------------------------------------------------------
                                   6062 ;c                         Allocated to registers r6 r7 
                                   6063 ;------------------------------------------------------------
                                   6064 ;	..\src\COMMON\easyax5043.c:1066: static void axradio_sync_slave_nextperiod()
                                   6065 ;	-----------------------------------------
                                   6066 ;	 function axradio_sync_slave_nextperiod
                                   6067 ;	-----------------------------------------
      003041                       6068 _axradio_sync_slave_nextperiod:
                                   6069 ;	..\src\COMMON\easyax5043.c:1068: axradio_sync_addtime(axradio_sync_period);
      003041 90 90 FD         [24] 6070 	mov	dptr,#_axradio_sync_period
      003044 E4               [12] 6071 	clr	a
      003045 93               [24] 6072 	movc	a,@a+dptr
      003046 FC               [12] 6073 	mov	r4,a
      003047 74 01            [12] 6074 	mov	a,#0x01
      003049 93               [24] 6075 	movc	a,@a+dptr
      00304A FD               [12] 6076 	mov	r5,a
      00304B 74 02            [12] 6077 	mov	a,#0x02
      00304D 93               [24] 6078 	movc	a,@a+dptr
      00304E FE               [12] 6079 	mov	r6,a
      00304F 74 03            [12] 6080 	mov	a,#0x03
      003051 93               [24] 6081 	movc	a,@a+dptr
      003052 8C 82            [24] 6082 	mov	dpl,r4
      003054 8D 83            [24] 6083 	mov	dph,r5
      003056 8E F0            [24] 6084 	mov	b,r6
      003058 12 2E 93         [24] 6085 	lcall	_axradio_sync_addtime
                                   6086 ;	..\src\COMMON\easyax5043.c:1069: if (!checksignedlimit16(axradio_sync_periodcorr, axradio_sync_slave_maxperiod))
      00305B 90 00 FA         [24] 6087 	mov	dptr,#_axradio_sync_periodcorr
      00305E E0               [24] 6088 	movx	a,@dptr
      00305F FE               [12] 6089 	mov	r6,a
      003060 A3               [24] 6090 	inc	dptr
      003061 E0               [24] 6091 	movx	a,@dptr
      003062 FF               [12] 6092 	mov	r7,a
      003063 90 91 11         [24] 6093 	mov	dptr,#_axradio_sync_slave_maxperiod
      003066 E4               [12] 6094 	clr	a
      003067 93               [24] 6095 	movc	a,@a+dptr
      003068 C0 E0            [24] 6096 	push	acc
      00306A 74 01            [12] 6097 	mov	a,#0x01
      00306C 93               [24] 6098 	movc	a,@a+dptr
      00306D C0 E0            [24] 6099 	push	acc
      00306F 8E 82            [24] 6100 	mov	dpl,r6
      003071 8F 83            [24] 6101 	mov	dph,r7
      003073 12 82 63         [24] 6102 	lcall	_checksignedlimit16
      003076 AF 82            [24] 6103 	mov	r7,dpl
      003078 15 81            [12] 6104 	dec	sp
      00307A 15 81            [12] 6105 	dec	sp
      00307C EF               [12] 6106 	mov	a,r7
      00307D 70 01            [24] 6107 	jnz	00102$
                                   6108 ;	..\src\COMMON\easyax5043.c:1070: return;
      00307F 22               [24] 6109 	ret
      003080                       6110 00102$:
                                   6111 ;	..\src\COMMON\easyax5043.c:1072: int16_t __autodata c = axradio_sync_periodcorr;
      003080 90 00 FA         [24] 6112 	mov	dptr,#_axradio_sync_periodcorr
      003083 E0               [24] 6113 	movx	a,@dptr
      003084 FE               [12] 6114 	mov	r6,a
      003085 A3               [24] 6115 	inc	dptr
      003086 E0               [24] 6116 	movx	a,@dptr
                                   6117 ;	..\src\COMMON\easyax5043.c:1073: axradio_sync_addtime(c >> SYNC_K1);
      003087 C4               [12] 6118 	swap	a
      003088 03               [12] 6119 	rr	a
      003089 CE               [12] 6120 	xch	a,r6
      00308A C4               [12] 6121 	swap	a
      00308B 03               [12] 6122 	rr	a
      00308C 54 07            [12] 6123 	anl	a,#0x07
      00308E 6E               [12] 6124 	xrl	a,r6
      00308F CE               [12] 6125 	xch	a,r6
      003090 54 07            [12] 6126 	anl	a,#0x07
      003092 CE               [12] 6127 	xch	a,r6
      003093 6E               [12] 6128 	xrl	a,r6
      003094 CE               [12] 6129 	xch	a,r6
      003095 30 E2 02         [24] 6130 	jnb	acc.2,00110$
      003098 44 F8            [12] 6131 	orl	a,#0xf8
      00309A                       6132 00110$:
      00309A FF               [12] 6133 	mov	r7,a
      00309B 33               [12] 6134 	rlc	a
      00309C 95 E0            [12] 6135 	subb	a,acc
      00309E FD               [12] 6136 	mov	r5,a
      00309F 8E 82            [24] 6137 	mov	dpl,r6
      0030A1 8F 83            [24] 6138 	mov	dph,r7
      0030A3 8D F0            [24] 6139 	mov	b,r5
                                   6140 ;	..\src\COMMON\easyax5043.c:1075: }
      0030A5 02 2E 93         [24] 6141 	ljmp	_axradio_sync_addtime
                                   6142 ;------------------------------------------------------------
                                   6143 ;Allocation info for local variables in function 'axradio_timer_callback'
                                   6144 ;------------------------------------------------------------
                                   6145 ;r                         Allocated to registers r7 
                                   6146 ;idx                       Allocated to registers r7 
                                   6147 ;rs                        Allocated to registers r6 
                                   6148 ;idx                       Allocated to registers r7 
                                   6149 ;desc                      Allocated with name '_axradio_timer_callback_desc_65536_350'
                                   6150 ;------------------------------------------------------------
                                   6151 ;	..\src\COMMON\easyax5043.c:1079: static void axradio_timer_callback(struct wtimer_desc __xdata *desc)
                                   6152 ;	-----------------------------------------
                                   6153 ;	 function axradio_timer_callback
                                   6154 ;	-----------------------------------------
      0030A8                       6155 _axradio_timer_callback:
                                   6156 ;	..\src\COMMON\easyax5043.c:1082: switch (axradio_mode) {
      0030A8 AF 17            [24] 6157 	mov	r7,_axradio_mode
      0030AA BF 10 00         [24] 6158 	cjne	r7,#0x10,00295$
      0030AD                       6159 00295$:
      0030AD 50 01            [24] 6160 	jnc	00296$
      0030AF 22               [24] 6161 	ret
      0030B0                       6162 00296$:
      0030B0 EF               [12] 6163 	mov	a,r7
      0030B1 24 CC            [12] 6164 	add	a,#0xff - 0x33
      0030B3 50 01            [24] 6165 	jnc	00297$
      0030B5 22               [24] 6166 	ret
      0030B6                       6167 00297$:
      0030B6 EF               [12] 6168 	mov	a,r7
      0030B7 24 F0            [12] 6169 	add	a,#0xf0
      0030B9 FF               [12] 6170 	mov	r7,a
      0030BA 24 0A            [12] 6171 	add	a,#(00298$-3-.)
      0030BC 83               [24] 6172 	movc	a,@a+pc
      0030BD F5 82            [12] 6173 	mov	dpl,a
      0030BF EF               [12] 6174 	mov	a,r7
      0030C0 24 28            [12] 6175 	add	a,#(00299$-3-.)
      0030C2 83               [24] 6176 	movc	a,@a+pc
      0030C3 F5 83            [12] 6177 	mov	dph,a
      0030C5 E4               [12] 6178 	clr	a
      0030C6 73               [24] 6179 	jmp	@a+dptr
      0030C7                       6180 00298$:
      0030C7 AD                    6181 	.db	00114$
      0030C8 AD                    6182 	.db	00114$
      0030C9 43                    6183 	.db	00124$
      0030CA 43                    6184 	.db	00124$
      0030CB E5                    6185 	.db	00177$
      0030CC E5                    6186 	.db	00177$
      0030CD E5                    6187 	.db	00177$
      0030CE E5                    6188 	.db	00177$
      0030CF E5                    6189 	.db	00177$
      0030D0 E5                    6190 	.db	00177$
      0030D1 E5                    6191 	.db	00177$
      0030D2 E5                    6192 	.db	00177$
      0030D3 E5                    6193 	.db	00177$
      0030D4 E5                    6194 	.db	00177$
      0030D5 E5                    6195 	.db	00177$
      0030D6 E5                    6196 	.db	00177$
      0030D7 0F                    6197 	.db	00107$
      0030D8 0F                    6198 	.db	00107$
      0030D9 A5                    6199 	.db	00130$
      0030DA A5                    6200 	.db	00130$
      0030DB E5                    6201 	.db	00177$
      0030DC E5                    6202 	.db	00177$
      0030DD E5                    6203 	.db	00177$
      0030DE E5                    6204 	.db	00177$
      0030DF 0F                    6205 	.db	00107$
      0030E0 0F                    6206 	.db	00107$
      0030E1 0F                    6207 	.db	00107$
      0030E2 0F                    6208 	.db	00107$
      0030E3 0F                    6209 	.db	00107$
      0030E4 E5                    6210 	.db	00177$
      0030E5 E5                    6211 	.db	00177$
      0030E6 E5                    6212 	.db	00177$
      0030E7 3E                    6213 	.db	00140$
      0030E8 3E                    6214 	.db	00140$
      0030E9 DE                    6215 	.db	00153$
      0030EA DE                    6216 	.db	00153$
      0030EB                       6217 00299$:
      0030EB 31                    6218 	.db	00114$>>8
      0030EC 31                    6219 	.db	00114$>>8
      0030ED 32                    6220 	.db	00124$>>8
      0030EE 32                    6221 	.db	00124$>>8
      0030EF 38                    6222 	.db	00177$>>8
      0030F0 38                    6223 	.db	00177$>>8
      0030F1 38                    6224 	.db	00177$>>8
      0030F2 38                    6225 	.db	00177$>>8
      0030F3 38                    6226 	.db	00177$>>8
      0030F4 38                    6227 	.db	00177$>>8
      0030F5 38                    6228 	.db	00177$>>8
      0030F6 38                    6229 	.db	00177$>>8
      0030F7 38                    6230 	.db	00177$>>8
      0030F8 38                    6231 	.db	00177$>>8
      0030F9 38                    6232 	.db	00177$>>8
      0030FA 38                    6233 	.db	00177$>>8
      0030FB 31                    6234 	.db	00107$>>8
      0030FC 31                    6235 	.db	00107$>>8
      0030FD 32                    6236 	.db	00130$>>8
      0030FE 32                    6237 	.db	00130$>>8
      0030FF 38                    6238 	.db	00177$>>8
      003100 38                    6239 	.db	00177$>>8
      003101 38                    6240 	.db	00177$>>8
      003102 38                    6241 	.db	00177$>>8
      003103 31                    6242 	.db	00107$>>8
      003104 31                    6243 	.db	00107$>>8
      003105 31                    6244 	.db	00107$>>8
      003106 31                    6245 	.db	00107$>>8
      003107 31                    6246 	.db	00107$>>8
      003108 38                    6247 	.db	00177$>>8
      003109 38                    6248 	.db	00177$>>8
      00310A 38                    6249 	.db	00177$>>8
      00310B 33                    6250 	.db	00140$>>8
      00310C 33                    6251 	.db	00140$>>8
      00310D 34                    6252 	.db	00153$>>8
      00310E 34                    6253 	.db	00153$>>8
                                   6254 ;	..\src\COMMON\easyax5043.c:1089: case AXRADIO_MODE_WOR_RECEIVE:
      00310F                       6255 00107$:
                                   6256 ;	..\src\COMMON\easyax5043.c:1090: if (axradio_syncstate == syncstate_asynctx)
      00310F 90 00 EA         [24] 6257 	mov	dptr,#_axradio_syncstate
      003112 E0               [24] 6258 	movx	a,@dptr
      003113 FF               [12] 6259 	mov	r7,a
      003114 BF 02 03         [24] 6260 	cjne	r7,#0x02,00300$
      003117 02 31 AD         [24] 6261 	ljmp	00114$
      00311A                       6262 00300$:
                                   6263 ;	..\src\COMMON\easyax5043.c:1092: wtimer_remove(&axradio_timer);
      00311A 90 03 6F         [24] 6264 	mov	dptr,#_axradio_timer
      00311D 12 85 39         [24] 6265 	lcall	_wtimer_remove
                                   6266 ;	..\src\COMMON\easyax5043.c:1093: rearmcstimer:
      003120                       6267 00110$:
                                   6268 ;	..\src\COMMON\easyax5043.c:1094: axradio_timer.time = axradio_phy_cs_period;
      003120 90 90 D0         [24] 6269 	mov	dptr,#_axradio_phy_cs_period
      003123 E4               [12] 6270 	clr	a
      003124 93               [24] 6271 	movc	a,@a+dptr
      003125 FE               [12] 6272 	mov	r6,a
      003126 74 01            [12] 6273 	mov	a,#0x01
      003128 93               [24] 6274 	movc	a,@a+dptr
      003129 FF               [12] 6275 	mov	r7,a
      00312A 7D 00            [12] 6276 	mov	r5,#0x00
      00312C 7C 00            [12] 6277 	mov	r4,#0x00
      00312E 90 03 73         [24] 6278 	mov	dptr,#(_axradio_timer + 0x0004)
      003131 EE               [12] 6279 	mov	a,r6
      003132 F0               [24] 6280 	movx	@dptr,a
      003133 EF               [12] 6281 	mov	a,r7
      003134 A3               [24] 6282 	inc	dptr
      003135 F0               [24] 6283 	movx	@dptr,a
      003136 ED               [12] 6284 	mov	a,r5
      003137 A3               [24] 6285 	inc	dptr
      003138 F0               [24] 6286 	movx	@dptr,a
      003139 EC               [12] 6287 	mov	a,r4
      00313A A3               [24] 6288 	inc	dptr
      00313B F0               [24] 6289 	movx	@dptr,a
                                   6290 ;	..\src\COMMON\easyax5043.c:1095: wtimer0_addrelative(&axradio_timer);
      00313C 90 03 6F         [24] 6291 	mov	dptr,#_axradio_timer
      00313F 12 78 78         [24] 6292 	lcall	_wtimer0_addrelative
                                   6293 ;	..\src\COMMON\easyax5043.c:1096: chanstatecb:
      003142                       6294 00111$:
                                   6295 ;	..\src\COMMON\easyax5043.c:1097: update_timeanchor();
      003142 12 1E 70         [24] 6296 	lcall	_update_timeanchor
                                   6297 ;	..\src\COMMON\easyax5043.c:1098: wtimer_remove_callback(&axradio_cb_channelstate.cb);
      003145 90 03 44         [24] 6298 	mov	dptr,#_axradio_cb_channelstate
      003148 12 87 AB         [24] 6299 	lcall	_wtimer_remove_callback
                                   6300 ;	..\src\COMMON\easyax5043.c:1099: axradio_cb_channelstate.st.error = AXRADIO_ERR_NOERROR;
      00314B 90 03 49         [24] 6301 	mov	dptr,#(_axradio_cb_channelstate + 0x0005)
      00314E E4               [12] 6302 	clr	a
      00314F F0               [24] 6303 	movx	@dptr,a
                                   6304 ;	..\src\COMMON\easyax5043.c:1101: int8_t __autodata r = AX5043_RSSI;
      003150 90 40 40         [24] 6305 	mov	dptr,#_AX5043_RSSI
      003153 E0               [24] 6306 	movx	a,@dptr
                                   6307 ;	..\src\COMMON\easyax5043.c:1102: axradio_cb_channelstate.st.cs.rssi = r - (int16_t)axradio_phy_rssioffset;
      003154 FF               [12] 6308 	mov	r7,a
      003155 FD               [12] 6309 	mov	r5,a
      003156 33               [12] 6310 	rlc	a
      003157 95 E0            [12] 6311 	subb	a,acc
      003159 FE               [12] 6312 	mov	r6,a
      00315A 90 90 CD         [24] 6313 	mov	dptr,#_axradio_phy_rssioffset
      00315D E4               [12] 6314 	clr	a
      00315E 93               [24] 6315 	movc	a,@a+dptr
      00315F FC               [12] 6316 	mov	r4,a
      003160 33               [12] 6317 	rlc	a
      003161 95 E0            [12] 6318 	subb	a,acc
      003163 FB               [12] 6319 	mov	r3,a
      003164 ED               [12] 6320 	mov	a,r5
      003165 C3               [12] 6321 	clr	c
      003166 9C               [12] 6322 	subb	a,r4
      003167 FD               [12] 6323 	mov	r5,a
      003168 EE               [12] 6324 	mov	a,r6
      003169 9B               [12] 6325 	subb	a,r3
      00316A FE               [12] 6326 	mov	r6,a
      00316B 90 03 4E         [24] 6327 	mov	dptr,#(_axradio_cb_channelstate + 0x000a)
      00316E ED               [12] 6328 	mov	a,r5
      00316F F0               [24] 6329 	movx	@dptr,a
      003170 EE               [12] 6330 	mov	a,r6
      003171 A3               [24] 6331 	inc	dptr
      003172 F0               [24] 6332 	movx	@dptr,a
                                   6333 ;	..\src\COMMON\easyax5043.c:1103: axradio_cb_channelstate.st.cs.busy = r >= axradio_phy_channelbusy;
      003173 90 90 CF         [24] 6334 	mov	dptr,#_axradio_phy_channelbusy
      003176 E4               [12] 6335 	clr	a
      003177 93               [24] 6336 	movc	a,@a+dptr
      003178 FE               [12] 6337 	mov	r6,a
      003179 C3               [12] 6338 	clr	c
      00317A EF               [12] 6339 	mov	a,r7
      00317B 64 80            [12] 6340 	xrl	a,#0x80
      00317D 8E F0            [24] 6341 	mov	b,r6
      00317F 63 F0 80         [24] 6342 	xrl	b,#0x80
      003182 95 F0            [12] 6343 	subb	a,b
      003184 B3               [12] 6344 	cpl	c
      003185 E4               [12] 6345 	clr	a
      003186 33               [12] 6346 	rlc	a
      003187 90 03 50         [24] 6347 	mov	dptr,#(_axradio_cb_channelstate + 0x000c)
      00318A F0               [24] 6348 	movx	@dptr,a
                                   6349 ;	..\src\COMMON\easyax5043.c:1105: axradio_cb_channelstate.st.time.t = axradio_timeanchor.radiotimer;
      00318B 90 01 00         [24] 6350 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      00318E E0               [24] 6351 	movx	a,@dptr
      00318F FC               [12] 6352 	mov	r4,a
      003190 A3               [24] 6353 	inc	dptr
      003191 E0               [24] 6354 	movx	a,@dptr
      003192 FD               [12] 6355 	mov	r5,a
      003193 A3               [24] 6356 	inc	dptr
      003194 E0               [24] 6357 	movx	a,@dptr
      003195 FE               [12] 6358 	mov	r6,a
      003196 A3               [24] 6359 	inc	dptr
      003197 E0               [24] 6360 	movx	a,@dptr
      003198 FF               [12] 6361 	mov	r7,a
      003199 90 03 4A         [24] 6362 	mov	dptr,#(_axradio_cb_channelstate + 0x0006)
      00319C EC               [12] 6363 	mov	a,r4
      00319D F0               [24] 6364 	movx	@dptr,a
      00319E ED               [12] 6365 	mov	a,r5
      00319F A3               [24] 6366 	inc	dptr
      0031A0 F0               [24] 6367 	movx	@dptr,a
      0031A1 EE               [12] 6368 	mov	a,r6
      0031A2 A3               [24] 6369 	inc	dptr
      0031A3 F0               [24] 6370 	movx	@dptr,a
      0031A4 EF               [12] 6371 	mov	a,r7
      0031A5 A3               [24] 6372 	inc	dptr
      0031A6 F0               [24] 6373 	movx	@dptr,a
                                   6374 ;	..\src\COMMON\easyax5043.c:1106: wtimer_add_callback(&axradio_cb_channelstate.cb);
      0031A7 90 03 44         [24] 6375 	mov	dptr,#_axradio_cb_channelstate
                                   6376 ;	..\src\COMMON\easyax5043.c:1107: break;
      0031AA 02 78 5E         [24] 6377 	ljmp	_wtimer_add_callback
                                   6378 ;	..\src\COMMON\easyax5043.c:1111: transmitcs:
      0031AD                       6379 00114$:
                                   6380 ;	..\src\COMMON\easyax5043.c:1112: if (axradio_ack_count)
      0031AD 90 00 F4         [24] 6381 	mov	dptr,#_axradio_ack_count
      0031B0 E0               [24] 6382 	movx	a,@dptr
      0031B1 FF               [12] 6383 	mov	r7,a
      0031B2 E0               [24] 6384 	movx	a,@dptr
      0031B3 60 06            [24] 6385 	jz	00116$
                                   6386 ;	..\src\COMMON\easyax5043.c:1113: --axradio_ack_count;
      0031B5 EF               [12] 6387 	mov	a,r7
      0031B6 14               [12] 6388 	dec	a
      0031B7 90 00 F4         [24] 6389 	mov	dptr,#_axradio_ack_count
      0031BA F0               [24] 6390 	movx	@dptr,a
      0031BB                       6391 00116$:
                                   6392 ;	..\src\COMMON\easyax5043.c:1114: wtimer_remove(&axradio_timer);
      0031BB 90 03 6F         [24] 6393 	mov	dptr,#_axradio_timer
      0031BE 12 85 39         [24] 6394 	lcall	_wtimer_remove
                                   6395 ;	..\src\COMMON\easyax5043.c:1115: if ((int8_t)AX5043_RSSI < axradio_phy_channelbusy ||
      0031C1 90 40 40         [24] 6396 	mov	dptr,#_AX5043_RSSI
      0031C4 E0               [24] 6397 	movx	a,@dptr
      0031C5 FF               [12] 6398 	mov	r7,a
      0031C6 90 90 CF         [24] 6399 	mov	dptr,#_axradio_phy_channelbusy
      0031C9 E4               [12] 6400 	clr	a
      0031CA 93               [24] 6401 	movc	a,@a+dptr
      0031CB FE               [12] 6402 	mov	r6,a
      0031CC C3               [12] 6403 	clr	c
      0031CD EF               [12] 6404 	mov	a,r7
      0031CE 64 80            [12] 6405 	xrl	a,#0x80
      0031D0 8E F0            [24] 6406 	mov	b,r6
      0031D2 63 F0 80         [24] 6407 	xrl	b,#0x80
      0031D5 95 F0            [12] 6408 	subb	a,b
      0031D7 40 0F            [24] 6409 	jc	00117$
                                   6410 ;	..\src\COMMON\easyax5043.c:1116: (!axradio_ack_count && axradio_phy_lbt_forcetx)) {
      0031D9 90 00 F4         [24] 6411 	mov	dptr,#_axradio_ack_count
      0031DC E0               [24] 6412 	movx	a,@dptr
      0031DD FF               [12] 6413 	mov	r7,a
      0031DE E0               [24] 6414 	movx	a,@dptr
      0031DF 70 23            [24] 6415 	jnz	00118$
      0031E1 90 90 D4         [24] 6416 	mov	dptr,#_axradio_phy_lbt_forcetx
      0031E4 E4               [12] 6417 	clr	a
      0031E5 93               [24] 6418 	movc	a,@a+dptr
      0031E6 60 1C            [24] 6419 	jz	00118$
      0031E8                       6420 00117$:
                                   6421 ;	..\src\COMMON\easyax5043.c:1117: axradio_syncstate = syncstate_off;
      0031E8 90 00 EA         [24] 6422 	mov	dptr,#_axradio_syncstate
      0031EB E4               [12] 6423 	clr	a
      0031EC F0               [24] 6424 	movx	@dptr,a
                                   6425 ;	..\src\COMMON\easyax5043.c:1118: axradio_txbuffer_cnt = axradio_phy_preamble_longlen;
      0031ED 90 90 D9         [24] 6426 	mov	dptr,#_axradio_phy_preamble_longlen
                                   6427 ;	genFromRTrack removed	clr	a
      0031F0 93               [24] 6428 	movc	a,@a+dptr
      0031F1 FD               [12] 6429 	mov	r5,a
      0031F2 74 01            [12] 6430 	mov	a,#0x01
      0031F4 93               [24] 6431 	movc	a,@a+dptr
      0031F5 FE               [12] 6432 	mov	r6,a
      0031F6 90 00 ED         [24] 6433 	mov	dptr,#_axradio_txbuffer_cnt
      0031F9 ED               [12] 6434 	mov	a,r5
      0031FA F0               [24] 6435 	movx	@dptr,a
      0031FB EE               [12] 6436 	mov	a,r6
      0031FC A3               [24] 6437 	inc	dptr
      0031FD F0               [24] 6438 	movx	@dptr,a
                                   6439 ;	..\src\COMMON\easyax5043.c:1119: ax5043_prepare_tx();
      0031FE 12 2C C7         [24] 6440 	lcall	_ax5043_prepare_tx
                                   6441 ;	..\src\COMMON\easyax5043.c:1120: goto chanstatecb;
      003201 02 31 42         [24] 6442 	ljmp	00111$
      003204                       6443 00118$:
                                   6444 ;	..\src\COMMON\easyax5043.c:1122: if (axradio_ack_count)
      003204 EF               [12] 6445 	mov	a,r7
      003205 60 03            [24] 6446 	jz	00306$
      003207 02 31 20         [24] 6447 	ljmp	00110$
      00320A                       6448 00306$:
                                   6449 ;	..\src\COMMON\easyax5043.c:1124: update_timeanchor();
      00320A 12 1E 70         [24] 6450 	lcall	_update_timeanchor
                                   6451 ;	..\src\COMMON\easyax5043.c:1125: axradio_syncstate = syncstate_off;
      00320D 90 00 EA         [24] 6452 	mov	dptr,#_axradio_syncstate
      003210 E4               [12] 6453 	clr	a
      003211 F0               [24] 6454 	movx	@dptr,a
                                   6455 ;	..\src\COMMON\easyax5043.c:1126: ax5043_off();
      003212 12 2C F0         [24] 6456 	lcall	_ax5043_off
                                   6457 ;	..\src\COMMON\easyax5043.c:1127: wtimer_remove_callback(&axradio_cb_transmitstart.cb);
      003215 90 03 51         [24] 6458 	mov	dptr,#_axradio_cb_transmitstart
      003218 12 87 AB         [24] 6459 	lcall	_wtimer_remove_callback
                                   6460 ;	..\src\COMMON\easyax5043.c:1128: axradio_cb_transmitstart.st.error = AXRADIO_ERR_TIMEOUT;
      00321B 90 03 56         [24] 6461 	mov	dptr,#(_axradio_cb_transmitstart + 0x0005)
      00321E 74 03            [12] 6462 	mov	a,#0x03
      003220 F0               [24] 6463 	movx	@dptr,a
                                   6464 ;	..\src\COMMON\easyax5043.c:1129: axradio_cb_transmitstart.st.time.t = axradio_timeanchor.radiotimer;
      003221 90 01 00         [24] 6465 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      003224 E0               [24] 6466 	movx	a,@dptr
      003225 FC               [12] 6467 	mov	r4,a
      003226 A3               [24] 6468 	inc	dptr
      003227 E0               [24] 6469 	movx	a,@dptr
      003228 FD               [12] 6470 	mov	r5,a
      003229 A3               [24] 6471 	inc	dptr
      00322A E0               [24] 6472 	movx	a,@dptr
      00322B FE               [12] 6473 	mov	r6,a
      00322C A3               [24] 6474 	inc	dptr
      00322D E0               [24] 6475 	movx	a,@dptr
      00322E FF               [12] 6476 	mov	r7,a
      00322F 90 03 57         [24] 6477 	mov	dptr,#(_axradio_cb_transmitstart + 0x0006)
      003232 EC               [12] 6478 	mov	a,r4
      003233 F0               [24] 6479 	movx	@dptr,a
      003234 ED               [12] 6480 	mov	a,r5
      003235 A3               [24] 6481 	inc	dptr
      003236 F0               [24] 6482 	movx	@dptr,a
      003237 EE               [12] 6483 	mov	a,r6
      003238 A3               [24] 6484 	inc	dptr
      003239 F0               [24] 6485 	movx	@dptr,a
      00323A EF               [12] 6486 	mov	a,r7
      00323B A3               [24] 6487 	inc	dptr
      00323C F0               [24] 6488 	movx	@dptr,a
                                   6489 ;	..\src\COMMON\easyax5043.c:1130: wtimer_add_callback(&axradio_cb_transmitstart.cb);
      00323D 90 03 51         [24] 6490 	mov	dptr,#_axradio_cb_transmitstart
                                   6491 ;	..\src\COMMON\easyax5043.c:1131: break;
      003240 02 78 5E         [24] 6492 	ljmp	_wtimer_add_callback
                                   6493 ;	..\src\COMMON\easyax5043.c:1134: case AXRADIO_MODE_WOR_ACK_TRANSMIT:
      003243                       6494 00124$:
                                   6495 ;	..\src\COMMON\easyax5043.c:1135: if (axradio_syncstate == syncstate_lbt)
      003243 90 00 EA         [24] 6496 	mov	dptr,#_axradio_syncstate
      003246 E0               [24] 6497 	movx	a,@dptr
      003247 FF               [12] 6498 	mov	r7,a
      003248 BF 01 03         [24] 6499 	cjne	r7,#0x01,00307$
      00324B 02 31 AD         [24] 6500 	ljmp	00114$
      00324E                       6501 00307$:
                                   6502 ;	..\src\COMMON\easyax5043.c:1137: ax5043_off();
      00324E 12 2C F0         [24] 6503 	lcall	_ax5043_off
                                   6504 ;	..\src\COMMON\easyax5043.c:1138: if (!axradio_ack_count) {
      003251 90 00 F4         [24] 6505 	mov	dptr,#_axradio_ack_count
      003254 E0               [24] 6506 	movx	a,@dptr
      003255 FF               [12] 6507 	mov	r7,a
      003256 E0               [24] 6508 	movx	a,@dptr
      003257 70 31            [24] 6509 	jnz	00128$
                                   6510 ;	..\src\COMMON\easyax5043.c:1139: update_timeanchor();
      003259 12 1E 70         [24] 6511 	lcall	_update_timeanchor
                                   6512 ;	..\src\COMMON\easyax5043.c:1140: wtimer_remove_callback(&axradio_cb_transmitend.cb);
      00325C 90 03 5B         [24] 6513 	mov	dptr,#_axradio_cb_transmitend
      00325F 12 87 AB         [24] 6514 	lcall	_wtimer_remove_callback
                                   6515 ;	..\src\COMMON\easyax5043.c:1141: axradio_cb_transmitend.st.error = AXRADIO_ERR_TIMEOUT;
      003262 90 03 60         [24] 6516 	mov	dptr,#(_axradio_cb_transmitend + 0x0005)
      003265 74 03            [12] 6517 	mov	a,#0x03
      003267 F0               [24] 6518 	movx	@dptr,a
                                   6519 ;	..\src\COMMON\easyax5043.c:1142: axradio_cb_transmitend.st.time.t = axradio_timeanchor.radiotimer;
      003268 90 01 00         [24] 6520 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      00326B E0               [24] 6521 	movx	a,@dptr
      00326C FB               [12] 6522 	mov	r3,a
      00326D A3               [24] 6523 	inc	dptr
      00326E E0               [24] 6524 	movx	a,@dptr
      00326F FC               [12] 6525 	mov	r4,a
      003270 A3               [24] 6526 	inc	dptr
      003271 E0               [24] 6527 	movx	a,@dptr
      003272 FD               [12] 6528 	mov	r5,a
      003273 A3               [24] 6529 	inc	dptr
      003274 E0               [24] 6530 	movx	a,@dptr
      003275 FE               [12] 6531 	mov	r6,a
      003276 90 03 61         [24] 6532 	mov	dptr,#(_axradio_cb_transmitend + 0x0006)
      003279 EB               [12] 6533 	mov	a,r3
      00327A F0               [24] 6534 	movx	@dptr,a
      00327B EC               [12] 6535 	mov	a,r4
      00327C A3               [24] 6536 	inc	dptr
      00327D F0               [24] 6537 	movx	@dptr,a
      00327E ED               [12] 6538 	mov	a,r5
      00327F A3               [24] 6539 	inc	dptr
      003280 F0               [24] 6540 	movx	@dptr,a
      003281 EE               [12] 6541 	mov	a,r6
      003282 A3               [24] 6542 	inc	dptr
      003283 F0               [24] 6543 	movx	@dptr,a
                                   6544 ;	..\src\COMMON\easyax5043.c:1143: wtimer_add_callback(&axradio_cb_transmitend.cb);
      003284 90 03 5B         [24] 6545 	mov	dptr,#_axradio_cb_transmitend
                                   6546 ;	..\src\COMMON\easyax5043.c:1144: break;
      003287 02 78 5E         [24] 6547 	ljmp	_wtimer_add_callback
      00328A                       6548 00128$:
                                   6549 ;	..\src\COMMON\easyax5043.c:1146: --axradio_ack_count;
      00328A EF               [12] 6550 	mov	a,r7
      00328B 14               [12] 6551 	dec	a
      00328C 90 00 F4         [24] 6552 	mov	dptr,#_axradio_ack_count
      00328F F0               [24] 6553 	movx	@dptr,a
                                   6554 ;	..\src\COMMON\easyax5043.c:1147: axradio_txbuffer_cnt = axradio_phy_preamble_longlen;
      003290 90 90 D9         [24] 6555 	mov	dptr,#_axradio_phy_preamble_longlen
      003293 E4               [12] 6556 	clr	a
      003294 93               [24] 6557 	movc	a,@a+dptr
      003295 FE               [12] 6558 	mov	r6,a
      003296 74 01            [12] 6559 	mov	a,#0x01
      003298 93               [24] 6560 	movc	a,@a+dptr
      003299 FF               [12] 6561 	mov	r7,a
      00329A 90 00 ED         [24] 6562 	mov	dptr,#_axradio_txbuffer_cnt
      00329D EE               [12] 6563 	mov	a,r6
      00329E F0               [24] 6564 	movx	@dptr,a
      00329F EF               [12] 6565 	mov	a,r7
      0032A0 A3               [24] 6566 	inc	dptr
      0032A1 F0               [24] 6567 	movx	@dptr,a
                                   6568 ;	..\src\COMMON\easyax5043.c:1148: ax5043_prepare_tx();
                                   6569 ;	..\src\COMMON\easyax5043.c:1149: break;
      0032A2 02 2C C7         [24] 6570 	ljmp	_ax5043_prepare_tx
                                   6571 ;	..\src\COMMON\easyax5043.c:1152: case AXRADIO_MODE_WOR_ACK_RECEIVE:
      0032A5                       6572 00130$:
                                   6573 ;	..\src\COMMON\easyax5043.c:1153: if (axradio_syncstate == syncstate_lbt)
      0032A5 90 00 EA         [24] 6574 	mov	dptr,#_axradio_syncstate
      0032A8 E0               [24] 6575 	movx	a,@dptr
      0032A9 FF               [12] 6576 	mov	r7,a
      0032AA BF 01 03         [24] 6577 	cjne	r7,#0x01,00309$
      0032AD 02 31 AD         [24] 6578 	ljmp	00114$
      0032B0                       6579 00309$:
                                   6580 ;	..\src\COMMON\easyax5043.c:1155: transmitack:
      0032B0                       6581 00133$:
                                   6582 ;	..\src\COMMON\easyax5043.c:1156: AX5043_FIFOSTAT = 3;
      0032B0 90 40 28         [24] 6583 	mov	dptr,#_AX5043_FIFOSTAT
      0032B3 74 03            [12] 6584 	mov	a,#0x03
      0032B5 F0               [24] 6585 	movx	@dptr,a
                                   6586 ;	..\src\COMMON\easyax5043.c:1157: AX5043_PWRMODE = AX5043_PWRSTATE_FULL_TX;
      0032B6 90 40 02         [24] 6587 	mov	dptr,#_AX5043_PWRMODE
      0032B9 74 0D            [12] 6588 	mov	a,#0x0d
      0032BB F0               [24] 6589 	movx	@dptr,a
                                   6590 ;	..\src\COMMON\easyax5043.c:1158: while (!(AX5043_POWSTAT & 0x08)); // wait for modem vdd so writing the FIFO is safe
      0032BC                       6591 00134$:
      0032BC 90 40 03         [24] 6592 	mov	dptr,#_AX5043_POWSTAT
      0032BF E0               [24] 6593 	movx	a,@dptr
      0032C0 30 E3 F9         [24] 6594 	jnb	acc.3,00134$
                                   6595 ;	..\src\COMMON\easyax5043.c:1159: ax5043_init_registers_tx();
      0032C3 12 1F 4A         [24] 6596 	lcall	_ax5043_init_registers_tx
                                   6597 ;	..\src\COMMON\easyax5043.c:1160: AX5043_RADIOEVENTREQ0; // make sure REVRDONE bit is cleared, so it is a reliable indicator that the packet is out
      0032C6 90 40 0F         [24] 6598 	mov	dptr,#_AX5043_RADIOEVENTREQ0
      0032C9 E0               [24] 6599 	movx	a,@dptr
                                   6600 ;	..\src\COMMON\easyax5043.c:1161: AX5043_FIFOTHRESH1 = 0;
      0032CA 90 40 2E         [24] 6601 	mov	dptr,#_AX5043_FIFOTHRESH1
      0032CD E4               [12] 6602 	clr	a
      0032CE F0               [24] 6603 	movx	@dptr,a
                                   6604 ;	..\src\COMMON\easyax5043.c:1162: AX5043_FIFOTHRESH0 = 0x80;
      0032CF 90 40 2F         [24] 6605 	mov	dptr,#_AX5043_FIFOTHRESH0
      0032D2 74 80            [12] 6606 	mov	a,#0x80
      0032D4 F0               [24] 6607 	movx	@dptr,a
                                   6608 ;	..\src\COMMON\easyax5043.c:1163: axradio_trxstate = trxstate_tx_longpreamble;
      0032D5 75 18 0A         [24] 6609 	mov	_axradio_trxstate,#0x0a
                                   6610 ;	..\src\COMMON\easyax5043.c:1164: axradio_txbuffer_cnt = axradio_phy_preamble_longlen;
      0032D8 90 90 D9         [24] 6611 	mov	dptr,#_axradio_phy_preamble_longlen
      0032DB E4               [12] 6612 	clr	a
      0032DC 93               [24] 6613 	movc	a,@a+dptr
      0032DD FE               [12] 6614 	mov	r6,a
      0032DE 74 01            [12] 6615 	mov	a,#0x01
      0032E0 93               [24] 6616 	movc	a,@a+dptr
      0032E1 FF               [12] 6617 	mov	r7,a
      0032E2 90 00 ED         [24] 6618 	mov	dptr,#_axradio_txbuffer_cnt
      0032E5 EE               [12] 6619 	mov	a,r6
      0032E6 F0               [24] 6620 	movx	@dptr,a
      0032E7 EF               [12] 6621 	mov	a,r7
      0032E8 A3               [24] 6622 	inc	dptr
      0032E9 F0               [24] 6623 	movx	@dptr,a
                                   6624 ;	..\src\COMMON\easyax5043.c:1166: if ((AX5043_MODULATION & 0x0F) == 9) { // 4-FSK
      0032EA 90 40 10         [24] 6625 	mov	dptr,#_AX5043_MODULATION
      0032ED E0               [24] 6626 	movx	a,@dptr
      0032EE FF               [12] 6627 	mov	r7,a
      0032EF 53 07 0F         [24] 6628 	anl	ar7,#0x0f
      0032F2 7E 00            [12] 6629 	mov	r6,#0x00
      0032F4 BF 09 11         [24] 6630 	cjne	r7,#0x09,00138$
      0032F7 BE 00 0E         [24] 6631 	cjne	r6,#0x00,00138$
                                   6632 ;	..\src\COMMON\easyax5043.c:1167: AX5043_FIFODATA = AX5043_FIFOCMD_DATA | (7 << 5);
      0032FA 90 40 29         [24] 6633 	mov	dptr,#_AX5043_FIFODATA
      0032FD 74 E1            [12] 6634 	mov	a,#0xe1
      0032FF F0               [24] 6635 	movx	@dptr,a
                                   6636 ;	..\src\COMMON\easyax5043.c:1168: AX5043_FIFODATA = 2;  // length (including flags)
      003300 74 02            [12] 6637 	mov	a,#0x02
      003302 F0               [24] 6638 	movx	@dptr,a
                                   6639 ;	..\src\COMMON\easyax5043.c:1169: AX5043_FIFODATA = 0x01;  // flag PKTSTART -> dibit sync
      003303 14               [12] 6640 	dec	a
      003304 F0               [24] 6641 	movx	@dptr,a
                                   6642 ;	..\src\COMMON\easyax5043.c:1170: AX5043_FIFODATA = 0x11; // dummy byte for forcing dibit sync
      003305 74 11            [12] 6643 	mov	a,#0x11
      003307 F0               [24] 6644 	movx	@dptr,a
      003308                       6645 00138$:
                                   6646 ;	..\src\COMMON\easyax5043.c:1177: AX5043_IRQMASK0 = 0x08; // enable fifo free threshold
      003308 90 40 07         [24] 6647 	mov	dptr,#_AX5043_IRQMASK0
      00330B 74 08            [12] 6648 	mov	a,#0x08
      00330D F0               [24] 6649 	movx	@dptr,a
                                   6650 ;	..\src\COMMON\easyax5043.c:1178: update_timeanchor();
      00330E 12 1E 70         [24] 6651 	lcall	_update_timeanchor
                                   6652 ;	..\src\COMMON\easyax5043.c:1179: wtimer_remove_callback(&axradio_cb_transmitstart.cb);
      003311 90 03 51         [24] 6653 	mov	dptr,#_axradio_cb_transmitstart
      003314 12 87 AB         [24] 6654 	lcall	_wtimer_remove_callback
                                   6655 ;	..\src\COMMON\easyax5043.c:1180: axradio_cb_transmitstart.st.error = AXRADIO_ERR_NOERROR;
      003317 90 03 56         [24] 6656 	mov	dptr,#(_axradio_cb_transmitstart + 0x0005)
      00331A E4               [12] 6657 	clr	a
      00331B F0               [24] 6658 	movx	@dptr,a
                                   6659 ;	..\src\COMMON\easyax5043.c:1181: axradio_cb_transmitstart.st.time.t = axradio_timeanchor.radiotimer;
      00331C 90 01 00         [24] 6660 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      00331F E0               [24] 6661 	movx	a,@dptr
      003320 FC               [12] 6662 	mov	r4,a
      003321 A3               [24] 6663 	inc	dptr
      003322 E0               [24] 6664 	movx	a,@dptr
      003323 FD               [12] 6665 	mov	r5,a
      003324 A3               [24] 6666 	inc	dptr
      003325 E0               [24] 6667 	movx	a,@dptr
      003326 FE               [12] 6668 	mov	r6,a
      003327 A3               [24] 6669 	inc	dptr
      003328 E0               [24] 6670 	movx	a,@dptr
      003329 FF               [12] 6671 	mov	r7,a
      00332A 90 03 57         [24] 6672 	mov	dptr,#(_axradio_cb_transmitstart + 0x0006)
      00332D EC               [12] 6673 	mov	a,r4
      00332E F0               [24] 6674 	movx	@dptr,a
      00332F ED               [12] 6675 	mov	a,r5
      003330 A3               [24] 6676 	inc	dptr
      003331 F0               [24] 6677 	movx	@dptr,a
      003332 EE               [12] 6678 	mov	a,r6
      003333 A3               [24] 6679 	inc	dptr
      003334 F0               [24] 6680 	movx	@dptr,a
      003335 EF               [12] 6681 	mov	a,r7
      003336 A3               [24] 6682 	inc	dptr
      003337 F0               [24] 6683 	movx	@dptr,a
                                   6684 ;	..\src\COMMON\easyax5043.c:1182: wtimer_add_callback(&axradio_cb_transmitstart.cb);
      003338 90 03 51         [24] 6685 	mov	dptr,#_axradio_cb_transmitstart
                                   6686 ;	..\src\COMMON\easyax5043.c:1183: break;
      00333B 02 78 5E         [24] 6687 	ljmp	_wtimer_add_callback
                                   6688 ;	..\src\COMMON\easyax5043.c:1186: case AXRADIO_MODE_SYNC_ACK_MASTER:
      00333E                       6689 00140$:
                                   6690 ;	..\src\COMMON\easyax5043.c:1187: switch (axradio_syncstate) {
      00333E 90 00 EA         [24] 6691 	mov	dptr,#_axradio_syncstate
      003341 E0               [24] 6692 	movx	a,@dptr
      003342 FF               [12] 6693 	mov	r7,a
      003343 BF 04 02         [24] 6694 	cjne	r7,#0x04,00313$
      003346 80 58            [24] 6695 	sjmp	00142$
      003348                       6696 00313$:
      003348 BF 05 03         [24] 6697 	cjne	r7,#0x05,00314$
      00334B 02 34 7E         [24] 6698 	ljmp	00150$
      00334E                       6699 00314$:
                                   6700 ;	..\src\COMMON\easyax5043.c:1189: AX5043_PWRMODE = AX5043_PWRSTATE_XTAL_ON;
      00334E 90 40 02         [24] 6701 	mov	dptr,#_AX5043_PWRMODE
      003351 74 05            [12] 6702 	mov	a,#0x05
      003353 F0               [24] 6703 	movx	@dptr,a
                                   6704 ;	..\src\COMMON\easyax5043.c:1190: ax5043_init_registers_tx();
      003354 12 1F 4A         [24] 6705 	lcall	_ax5043_init_registers_tx
                                   6706 ;	..\src\COMMON\easyax5043.c:1191: axradio_syncstate = syncstate_master_xostartup;
      003357 90 00 EA         [24] 6707 	mov	dptr,#_axradio_syncstate
      00335A 74 04            [12] 6708 	mov	a,#0x04
      00335C F0               [24] 6709 	movx	@dptr,a
                                   6710 ;	..\src\COMMON\easyax5043.c:1192: wtimer_remove_callback(&axradio_cb_transmitdata.cb);
      00335D 90 03 65         [24] 6711 	mov	dptr,#_axradio_cb_transmitdata
      003360 12 87 AB         [24] 6712 	lcall	_wtimer_remove_callback
                                   6713 ;	..\src\COMMON\easyax5043.c:1193: axradio_cb_transmitdata.st.error = AXRADIO_ERR_NOERROR;
      003363 90 03 6A         [24] 6714 	mov	dptr,#(_axradio_cb_transmitdata + 0x0005)
      003366 E4               [12] 6715 	clr	a
      003367 F0               [24] 6716 	movx	@dptr,a
                                   6717 ;	..\src\COMMON\easyax5043.c:1194: axradio_cb_transmitdata.st.time.t = 0;
      003368 90 03 6B         [24] 6718 	mov	dptr,#(_axradio_cb_transmitdata + 0x0006)
      00336B F0               [24] 6719 	movx	@dptr,a
      00336C A3               [24] 6720 	inc	dptr
      00336D F0               [24] 6721 	movx	@dptr,a
      00336E A3               [24] 6722 	inc	dptr
      00336F F0               [24] 6723 	movx	@dptr,a
      003370 A3               [24] 6724 	inc	dptr
      003371 F0               [24] 6725 	movx	@dptr,a
                                   6726 ;	..\src\COMMON\easyax5043.c:1195: wtimer_add_callback(&axradio_cb_transmitdata.cb);
      003372 90 03 65         [24] 6727 	mov	dptr,#_axradio_cb_transmitdata
      003375 12 78 5E         [24] 6728 	lcall	_wtimer_add_callback
                                   6729 ;	..\src\COMMON\easyax5043.c:1196: wtimer_remove(&axradio_timer);
      003378 90 03 6F         [24] 6730 	mov	dptr,#_axradio_timer
      00337B 12 85 39         [24] 6731 	lcall	_wtimer_remove
                                   6732 ;	..\src\COMMON\easyax5043.c:1197: axradio_timer.time = axradio_sync_time;
      00337E 90 00 F6         [24] 6733 	mov	dptr,#_axradio_sync_time
      003381 E0               [24] 6734 	movx	a,@dptr
      003382 FC               [12] 6735 	mov	r4,a
      003383 A3               [24] 6736 	inc	dptr
      003384 E0               [24] 6737 	movx	a,@dptr
      003385 FD               [12] 6738 	mov	r5,a
      003386 A3               [24] 6739 	inc	dptr
      003387 E0               [24] 6740 	movx	a,@dptr
      003388 FE               [12] 6741 	mov	r6,a
      003389 A3               [24] 6742 	inc	dptr
      00338A E0               [24] 6743 	movx	a,@dptr
      00338B FF               [12] 6744 	mov	r7,a
      00338C 90 03 73         [24] 6745 	mov	dptr,#(_axradio_timer + 0x0004)
      00338F EC               [12] 6746 	mov	a,r4
      003390 F0               [24] 6747 	movx	@dptr,a
      003391 ED               [12] 6748 	mov	a,r5
      003392 A3               [24] 6749 	inc	dptr
      003393 F0               [24] 6750 	movx	@dptr,a
      003394 EE               [12] 6751 	mov	a,r6
      003395 A3               [24] 6752 	inc	dptr
      003396 F0               [24] 6753 	movx	@dptr,a
      003397 EF               [12] 6754 	mov	a,r7
      003398 A3               [24] 6755 	inc	dptr
      003399 F0               [24] 6756 	movx	@dptr,a
                                   6757 ;	..\src\COMMON\easyax5043.c:1198: wtimer0_addabsolute(&axradio_timer);
      00339A 90 03 6F         [24] 6758 	mov	dptr,#_axradio_timer
                                   6759 ;	..\src\COMMON\easyax5043.c:1199: break;
      00339D 02 79 8E         [24] 6760 	ljmp	_wtimer0_addabsolute
                                   6761 ;	..\src\COMMON\easyax5043.c:1201: case syncstate_master_xostartup:
      0033A0                       6762 00142$:
                                   6763 ;	..\src\COMMON\easyax5043.c:1202: AX5043_FIFOSTAT = 3;
      0033A0 90 40 28         [24] 6764 	mov	dptr,#_AX5043_FIFOSTAT
      0033A3 74 03            [12] 6765 	mov	a,#0x03
      0033A5 F0               [24] 6766 	movx	@dptr,a
                                   6767 ;	..\src\COMMON\easyax5043.c:1203: AX5043_PWRMODE = AX5043_PWRSTATE_FULL_TX;
      0033A6 90 40 02         [24] 6768 	mov	dptr,#_AX5043_PWRMODE
      0033A9 74 0D            [12] 6769 	mov	a,#0x0d
      0033AB F0               [24] 6770 	movx	@dptr,a
                                   6771 ;	..\src\COMMON\easyax5043.c:1204: while (!(AX5043_POWSTAT & 0x08)); // wait for modem vdd so writing the FIFO is safe
      0033AC                       6772 00143$:
      0033AC 90 40 03         [24] 6773 	mov	dptr,#_AX5043_POWSTAT
      0033AF E0               [24] 6774 	movx	a,@dptr
      0033B0 30 E3 F9         [24] 6775 	jnb	acc.3,00143$
                                   6776 ;	..\src\COMMON\easyax5043.c:1205: AX5043_RADIOEVENTREQ0; // make sure REVRDONE bit is cleared, so it is a reliable indicator that the packet is out
      0033B3 90 40 0F         [24] 6777 	mov	dptr,#_AX5043_RADIOEVENTREQ0
      0033B6 E0               [24] 6778 	movx	a,@dptr
                                   6779 ;	..\src\COMMON\easyax5043.c:1206: AX5043_FIFOTHRESH1 = 0;
      0033B7 90 40 2E         [24] 6780 	mov	dptr,#_AX5043_FIFOTHRESH1
      0033BA E4               [12] 6781 	clr	a
      0033BB F0               [24] 6782 	movx	@dptr,a
                                   6783 ;	..\src\COMMON\easyax5043.c:1207: AX5043_FIFOTHRESH0 = 0x80;
      0033BC 90 40 2F         [24] 6784 	mov	dptr,#_AX5043_FIFOTHRESH0
      0033BF 74 80            [12] 6785 	mov	a,#0x80
      0033C1 F0               [24] 6786 	movx	@dptr,a
                                   6787 ;	..\src\COMMON\easyax5043.c:1208: axradio_trxstate = trxstate_tx_longpreamble;
      0033C2 75 18 0A         [24] 6788 	mov	_axradio_trxstate,#0x0a
                                   6789 ;	..\src\COMMON\easyax5043.c:1209: axradio_txbuffer_cnt = axradio_phy_preamble_longlen;
      0033C5 90 90 D9         [24] 6790 	mov	dptr,#_axradio_phy_preamble_longlen
      0033C8 E4               [12] 6791 	clr	a
      0033C9 93               [24] 6792 	movc	a,@a+dptr
      0033CA FE               [12] 6793 	mov	r6,a
      0033CB 74 01            [12] 6794 	mov	a,#0x01
      0033CD 93               [24] 6795 	movc	a,@a+dptr
      0033CE FF               [12] 6796 	mov	r7,a
      0033CF 90 00 ED         [24] 6797 	mov	dptr,#_axradio_txbuffer_cnt
      0033D2 EE               [12] 6798 	mov	a,r6
      0033D3 F0               [24] 6799 	movx	@dptr,a
      0033D4 EF               [12] 6800 	mov	a,r7
      0033D5 A3               [24] 6801 	inc	dptr
      0033D6 F0               [24] 6802 	movx	@dptr,a
                                   6803 ;	..\src\COMMON\easyax5043.c:1211: if ((AX5043_MODULATION & 0x0F) == 9) { // 4-FSK
      0033D7 90 40 10         [24] 6804 	mov	dptr,#_AX5043_MODULATION
      0033DA E0               [24] 6805 	movx	a,@dptr
      0033DB FF               [12] 6806 	mov	r7,a
      0033DC 53 07 0F         [24] 6807 	anl	ar7,#0x0f
      0033DF 7E 00            [12] 6808 	mov	r6,#0x00
      0033E1 BF 09 11         [24] 6809 	cjne	r7,#0x09,00147$
      0033E4 BE 00 0E         [24] 6810 	cjne	r6,#0x00,00147$
                                   6811 ;	..\src\COMMON\easyax5043.c:1212: AX5043_FIFODATA = AX5043_FIFOCMD_DATA | (7 << 5);
      0033E7 90 40 29         [24] 6812 	mov	dptr,#_AX5043_FIFODATA
      0033EA 74 E1            [12] 6813 	mov	a,#0xe1
      0033EC F0               [24] 6814 	movx	@dptr,a
                                   6815 ;	..\src\COMMON\easyax5043.c:1213: AX5043_FIFODATA = 2;  // length (including flags)
      0033ED 74 02            [12] 6816 	mov	a,#0x02
      0033EF F0               [24] 6817 	movx	@dptr,a
                                   6818 ;	..\src\COMMON\easyax5043.c:1214: AX5043_FIFODATA = 0x01;  // flag PKTSTART -> dibit sync
      0033F0 14               [12] 6819 	dec	a
      0033F1 F0               [24] 6820 	movx	@dptr,a
                                   6821 ;	..\src\COMMON\easyax5043.c:1215: AX5043_FIFODATA = 0x11; // dummy byte for forcing dibit sync
      0033F2 74 11            [12] 6822 	mov	a,#0x11
      0033F4 F0               [24] 6823 	movx	@dptr,a
      0033F5                       6824 00147$:
                                   6825 ;	..\src\COMMON\easyax5043.c:1222: wtimer_remove(&axradio_timer);
      0033F5 90 03 6F         [24] 6826 	mov	dptr,#_axradio_timer
      0033F8 12 85 39         [24] 6827 	lcall	_wtimer_remove
                                   6828 ;	..\src\COMMON\easyax5043.c:1223: update_timeanchor();
      0033FB 12 1E 70         [24] 6829 	lcall	_update_timeanchor
                                   6830 ;	..\src\COMMON\easyax5043.c:1224: AX5043_IRQMASK0 = 0x08; // enable fifo free threshold
      0033FE 90 40 07         [24] 6831 	mov	dptr,#_AX5043_IRQMASK0
      003401 74 08            [12] 6832 	mov	a,#0x08
      003403 F0               [24] 6833 	movx	@dptr,a
                                   6834 ;	..\src\COMMON\easyax5043.c:1225: axradio_sync_addtime(axradio_sync_period);
      003404 90 90 FD         [24] 6835 	mov	dptr,#_axradio_sync_period
      003407 E4               [12] 6836 	clr	a
      003408 93               [24] 6837 	movc	a,@a+dptr
      003409 FC               [12] 6838 	mov	r4,a
      00340A 74 01            [12] 6839 	mov	a,#0x01
      00340C 93               [24] 6840 	movc	a,@a+dptr
      00340D FD               [12] 6841 	mov	r5,a
      00340E 74 02            [12] 6842 	mov	a,#0x02
      003410 93               [24] 6843 	movc	a,@a+dptr
      003411 FE               [12] 6844 	mov	r6,a
      003412 74 03            [12] 6845 	mov	a,#0x03
      003414 93               [24] 6846 	movc	a,@a+dptr
      003415 8C 82            [24] 6847 	mov	dpl,r4
      003417 8D 83            [24] 6848 	mov	dph,r5
      003419 8E F0            [24] 6849 	mov	b,r6
      00341B 12 2E 93         [24] 6850 	lcall	_axradio_sync_addtime
                                   6851 ;	..\src\COMMON\easyax5043.c:1226: axradio_syncstate = syncstate_master_waitack;
      00341E 90 00 EA         [24] 6852 	mov	dptr,#_axradio_syncstate
      003421 74 05            [12] 6853 	mov	a,#0x05
      003423 F0               [24] 6854 	movx	@dptr,a
                                   6855 ;	..\src\COMMON\easyax5043.c:1227: if (axradio_mode != AXRADIO_MODE_SYNC_ACK_MASTER) {
      003424 74 31            [12] 6856 	mov	a,#0x31
      003426 B5 17 02         [24] 6857 	cjne	a,_axradio_mode,00318$
      003429 80 26            [24] 6858 	sjmp	00149$
      00342B                       6859 00318$:
                                   6860 ;	..\src\COMMON\easyax5043.c:1228: axradio_syncstate = syncstate_master_normal;
      00342B 90 00 EA         [24] 6861 	mov	dptr,#_axradio_syncstate
      00342E 74 03            [12] 6862 	mov	a,#0x03
      003430 F0               [24] 6863 	movx	@dptr,a
                                   6864 ;	..\src\COMMON\easyax5043.c:1229: axradio_sync_settimeradv(axradio_sync_xoscstartup);
      003431 90 91 01         [24] 6865 	mov	dptr,#_axradio_sync_xoscstartup
      003434 E4               [12] 6866 	clr	a
      003435 93               [24] 6867 	movc	a,@a+dptr
      003436 FC               [12] 6868 	mov	r4,a
      003437 74 01            [12] 6869 	mov	a,#0x01
      003439 93               [24] 6870 	movc	a,@a+dptr
      00343A FD               [12] 6871 	mov	r5,a
      00343B 74 02            [12] 6872 	mov	a,#0x02
      00343D 93               [24] 6873 	movc	a,@a+dptr
      00343E FE               [12] 6874 	mov	r6,a
      00343F 74 03            [12] 6875 	mov	a,#0x03
      003441 93               [24] 6876 	movc	a,@a+dptr
      003442 8C 82            [24] 6877 	mov	dpl,r4
      003444 8D 83            [24] 6878 	mov	dph,r5
      003446 8E F0            [24] 6879 	mov	b,r6
      003448 12 2E E4         [24] 6880 	lcall	_axradio_sync_settimeradv
                                   6881 ;	..\src\COMMON\easyax5043.c:1230: wtimer0_addabsolute(&axradio_timer);
      00344B 90 03 6F         [24] 6882 	mov	dptr,#_axradio_timer
      00344E 12 79 8E         [24] 6883 	lcall	_wtimer0_addabsolute
      003451                       6884 00149$:
                                   6885 ;	..\src\COMMON\easyax5043.c:1232: wtimer_remove_callback(&axradio_cb_transmitstart.cb);
      003451 90 03 51         [24] 6886 	mov	dptr,#_axradio_cb_transmitstart
      003454 12 87 AB         [24] 6887 	lcall	_wtimer_remove_callback
                                   6888 ;	..\src\COMMON\easyax5043.c:1233: axradio_cb_transmitstart.st.error = AXRADIO_ERR_NOERROR;
      003457 90 03 56         [24] 6889 	mov	dptr,#(_axradio_cb_transmitstart + 0x0005)
      00345A E4               [12] 6890 	clr	a
      00345B F0               [24] 6891 	movx	@dptr,a
                                   6892 ;	..\src\COMMON\easyax5043.c:1234: axradio_cb_transmitstart.st.time.t = axradio_timeanchor.radiotimer;
      00345C 90 01 00         [24] 6893 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      00345F E0               [24] 6894 	movx	a,@dptr
      003460 FC               [12] 6895 	mov	r4,a
      003461 A3               [24] 6896 	inc	dptr
      003462 E0               [24] 6897 	movx	a,@dptr
      003463 FD               [12] 6898 	mov	r5,a
      003464 A3               [24] 6899 	inc	dptr
      003465 E0               [24] 6900 	movx	a,@dptr
      003466 FE               [12] 6901 	mov	r6,a
      003467 A3               [24] 6902 	inc	dptr
      003468 E0               [24] 6903 	movx	a,@dptr
      003469 FF               [12] 6904 	mov	r7,a
      00346A 90 03 57         [24] 6905 	mov	dptr,#(_axradio_cb_transmitstart + 0x0006)
      00346D EC               [12] 6906 	mov	a,r4
      00346E F0               [24] 6907 	movx	@dptr,a
      00346F ED               [12] 6908 	mov	a,r5
      003470 A3               [24] 6909 	inc	dptr
      003471 F0               [24] 6910 	movx	@dptr,a
      003472 EE               [12] 6911 	mov	a,r6
      003473 A3               [24] 6912 	inc	dptr
      003474 F0               [24] 6913 	movx	@dptr,a
      003475 EF               [12] 6914 	mov	a,r7
      003476 A3               [24] 6915 	inc	dptr
      003477 F0               [24] 6916 	movx	@dptr,a
                                   6917 ;	..\src\COMMON\easyax5043.c:1235: wtimer_add_callback(&axradio_cb_transmitstart.cb);
      003478 90 03 51         [24] 6918 	mov	dptr,#_axradio_cb_transmitstart
                                   6919 ;	..\src\COMMON\easyax5043.c:1236: break;
      00347B 02 78 5E         [24] 6920 	ljmp	_wtimer_add_callback
                                   6921 ;	..\src\COMMON\easyax5043.c:1238: case syncstate_master_waitack:
      00347E                       6922 00150$:
                                   6923 ;	..\src\COMMON\easyax5043.c:1239: ax5043_off();
      00347E 12 2C F0         [24] 6924 	lcall	_ax5043_off
                                   6925 ;	..\src\COMMON\easyax5043.c:1240: axradio_syncstate = syncstate_master_normal;
      003481 90 00 EA         [24] 6926 	mov	dptr,#_axradio_syncstate
      003484 74 03            [12] 6927 	mov	a,#0x03
      003486 F0               [24] 6928 	movx	@dptr,a
                                   6929 ;	..\src\COMMON\easyax5043.c:1241: wtimer_remove(&axradio_timer);
      003487 90 03 6F         [24] 6930 	mov	dptr,#_axradio_timer
      00348A 12 85 39         [24] 6931 	lcall	_wtimer_remove
                                   6932 ;	..\src\COMMON\easyax5043.c:1242: axradio_sync_settimeradv(axradio_sync_xoscstartup);
      00348D 90 91 01         [24] 6933 	mov	dptr,#_axradio_sync_xoscstartup
      003490 E4               [12] 6934 	clr	a
      003491 93               [24] 6935 	movc	a,@a+dptr
      003492 FC               [12] 6936 	mov	r4,a
      003493 74 01            [12] 6937 	mov	a,#0x01
      003495 93               [24] 6938 	movc	a,@a+dptr
      003496 FD               [12] 6939 	mov	r5,a
      003497 74 02            [12] 6940 	mov	a,#0x02
      003499 93               [24] 6941 	movc	a,@a+dptr
      00349A FE               [12] 6942 	mov	r6,a
      00349B 74 03            [12] 6943 	mov	a,#0x03
      00349D 93               [24] 6944 	movc	a,@a+dptr
      00349E 8C 82            [24] 6945 	mov	dpl,r4
      0034A0 8D 83            [24] 6946 	mov	dph,r5
      0034A2 8E F0            [24] 6947 	mov	b,r6
      0034A4 12 2E E4         [24] 6948 	lcall	_axradio_sync_settimeradv
                                   6949 ;	..\src\COMMON\easyax5043.c:1243: wtimer0_addabsolute(&axradio_timer);
      0034A7 90 03 6F         [24] 6950 	mov	dptr,#_axradio_timer
      0034AA 12 79 8E         [24] 6951 	lcall	_wtimer0_addabsolute
                                   6952 ;	..\src\COMMON\easyax5043.c:1244: update_timeanchor();
      0034AD 12 1E 70         [24] 6953 	lcall	_update_timeanchor
                                   6954 ;	..\src\COMMON\easyax5043.c:1245: wtimer_remove_callback(&axradio_cb_transmitend.cb);
      0034B0 90 03 5B         [24] 6955 	mov	dptr,#_axradio_cb_transmitend
      0034B3 12 87 AB         [24] 6956 	lcall	_wtimer_remove_callback
                                   6957 ;	..\src\COMMON\easyax5043.c:1246: axradio_cb_transmitend.st.error = AXRADIO_ERR_TIMEOUT;
      0034B6 90 03 60         [24] 6958 	mov	dptr,#(_axradio_cb_transmitend + 0x0005)
      0034B9 74 03            [12] 6959 	mov	a,#0x03
      0034BB F0               [24] 6960 	movx	@dptr,a
                                   6961 ;	..\src\COMMON\easyax5043.c:1247: axradio_cb_transmitend.st.time.t = axradio_timeanchor.radiotimer;
      0034BC 90 01 00         [24] 6962 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      0034BF E0               [24] 6963 	movx	a,@dptr
      0034C0 FC               [12] 6964 	mov	r4,a
      0034C1 A3               [24] 6965 	inc	dptr
      0034C2 E0               [24] 6966 	movx	a,@dptr
      0034C3 FD               [12] 6967 	mov	r5,a
      0034C4 A3               [24] 6968 	inc	dptr
      0034C5 E0               [24] 6969 	movx	a,@dptr
      0034C6 FE               [12] 6970 	mov	r6,a
      0034C7 A3               [24] 6971 	inc	dptr
      0034C8 E0               [24] 6972 	movx	a,@dptr
      0034C9 FF               [12] 6973 	mov	r7,a
      0034CA 90 03 61         [24] 6974 	mov	dptr,#(_axradio_cb_transmitend + 0x0006)
      0034CD EC               [12] 6975 	mov	a,r4
      0034CE F0               [24] 6976 	movx	@dptr,a
      0034CF ED               [12] 6977 	mov	a,r5
      0034D0 A3               [24] 6978 	inc	dptr
      0034D1 F0               [24] 6979 	movx	@dptr,a
      0034D2 EE               [12] 6980 	mov	a,r6
      0034D3 A3               [24] 6981 	inc	dptr
      0034D4 F0               [24] 6982 	movx	@dptr,a
      0034D5 EF               [12] 6983 	mov	a,r7
      0034D6 A3               [24] 6984 	inc	dptr
      0034D7 F0               [24] 6985 	movx	@dptr,a
                                   6986 ;	..\src\COMMON\easyax5043.c:1248: wtimer_add_callback(&axradio_cb_transmitend.cb);
      0034D8 90 03 5B         [24] 6987 	mov	dptr,#_axradio_cb_transmitend
                                   6988 ;	..\src\COMMON\easyax5043.c:1251: break;
      0034DB 02 78 5E         [24] 6989 	ljmp	_wtimer_add_callback
                                   6990 ;	..\src\COMMON\easyax5043.c:1254: case AXRADIO_MODE_SYNC_ACK_SLAVE:
      0034DE                       6991 00153$:
                                   6992 ;	..\src\COMMON\easyax5043.c:1255: switch (axradio_syncstate) {
      0034DE 90 00 EA         [24] 6993 	mov	dptr,#_axradio_syncstate
      0034E1 E0               [24] 6994 	movx	a,@dptr
      0034E2 FF               [12] 6995 	mov  r7,a
      0034E3 24 F3            [12] 6996 	add	a,#0xff - 0x0c
      0034E5 50 03            [24] 6997 	jnc	00319$
      0034E7 02 35 15         [24] 6998 	ljmp	00155$
      0034EA                       6999 00319$:
      0034EA EF               [12] 7000 	mov	a,r7
      0034EB F5 F0            [12] 7001 	mov	b,a
      0034ED 24 0B            [12] 7002 	add	a,#(00320$-3-.)
      0034EF 83               [24] 7003 	movc	a,@a+pc
      0034F0 F5 82            [12] 7004 	mov	dpl,a
      0034F2 E5 F0            [12] 7005 	mov	a,b
      0034F4 24 11            [12] 7006 	add	a,#(00321$-3-.)
      0034F6 83               [24] 7007 	movc	a,@a+pc
      0034F7 F5 83            [12] 7008 	mov	dph,a
      0034F9 E4               [12] 7009 	clr	a
      0034FA 73               [24] 7010 	jmp	@a+dptr
      0034FB                       7011 00320$:
      0034FB 15                    7012 	.db	00155$
      0034FC 15                    7013 	.db	00155$
      0034FD 15                    7014 	.db	00155$
      0034FE 15                    7015 	.db	00155$
      0034FF 15                    7016 	.db	00155$
      003500 15                    7017 	.db	00155$
      003501 15                    7018 	.db	00155$
      003502 A5                    7019 	.db	00156$
      003503 38                    7020 	.db	00157$
      003504 8A                    7021 	.db	00158$
      003505 40                    7022 	.db	00161$
      003506 9E                    7023 	.db	00166$
      003507 B6                    7024 	.db	00173$
      003508                       7025 00321$:
      003508 35                    7026 	.db	00155$>>8
      003509 35                    7027 	.db	00155$>>8
      00350A 35                    7028 	.db	00155$>>8
      00350B 35                    7029 	.db	00155$>>8
      00350C 35                    7030 	.db	00155$>>8
      00350D 35                    7031 	.db	00155$>>8
      00350E 35                    7032 	.db	00155$>>8
      00350F 35                    7033 	.db	00156$>>8
      003510 36                    7034 	.db	00157$>>8
      003511 36                    7035 	.db	00158$>>8
      003512 37                    7036 	.db	00161$>>8
      003513 37                    7037 	.db	00166$>>8
      003514 38                    7038 	.db	00173$>>8
                                   7039 ;	..\src\COMMON\easyax5043.c:1257: case syncstate_slave_synchunt:
      003515                       7040 00155$:
                                   7041 ;	..\src\COMMON\easyax5043.c:1258: ax5043_off();
      003515 12 2C F0         [24] 7042 	lcall	_ax5043_off
                                   7043 ;	..\src\COMMON\easyax5043.c:1259: axradio_syncstate = syncstate_slave_syncpause;
      003518 90 00 EA         [24] 7044 	mov	dptr,#_axradio_syncstate
      00351B 74 07            [12] 7045 	mov	a,#0x07
      00351D F0               [24] 7046 	movx	@dptr,a
                                   7047 ;	..\src\COMMON\easyax5043.c:1260: axradio_sync_addtime(axradio_sync_slave_syncpause);
      00351E 90 91 0D         [24] 7048 	mov	dptr,#_axradio_sync_slave_syncpause
      003521 E4               [12] 7049 	clr	a
      003522 93               [24] 7050 	movc	a,@a+dptr
      003523 FC               [12] 7051 	mov	r4,a
      003524 74 01            [12] 7052 	mov	a,#0x01
      003526 93               [24] 7053 	movc	a,@a+dptr
      003527 FD               [12] 7054 	mov	r5,a
      003528 74 02            [12] 7055 	mov	a,#0x02
      00352A 93               [24] 7056 	movc	a,@a+dptr
      00352B FE               [12] 7057 	mov	r6,a
      00352C 74 03            [12] 7058 	mov	a,#0x03
      00352E 93               [24] 7059 	movc	a,@a+dptr
      00352F 8C 82            [24] 7060 	mov	dpl,r4
      003531 8D 83            [24] 7061 	mov	dph,r5
      003533 8E F0            [24] 7062 	mov	b,r6
      003535 12 2E 93         [24] 7063 	lcall	_axradio_sync_addtime
                                   7064 ;	..\src\COMMON\easyax5043.c:1261: wtimer_remove(&axradio_timer);
      003538 90 03 6F         [24] 7065 	mov	dptr,#_axradio_timer
      00353B 12 85 39         [24] 7066 	lcall	_wtimer_remove
                                   7067 ;	..\src\COMMON\easyax5043.c:1262: axradio_timer.time = axradio_sync_time;
      00353E 90 00 F6         [24] 7068 	mov	dptr,#_axradio_sync_time
      003541 E0               [24] 7069 	movx	a,@dptr
      003542 FC               [12] 7070 	mov	r4,a
      003543 A3               [24] 7071 	inc	dptr
      003544 E0               [24] 7072 	movx	a,@dptr
      003545 FD               [12] 7073 	mov	r5,a
      003546 A3               [24] 7074 	inc	dptr
      003547 E0               [24] 7075 	movx	a,@dptr
      003548 FE               [12] 7076 	mov	r6,a
      003549 A3               [24] 7077 	inc	dptr
      00354A E0               [24] 7078 	movx	a,@dptr
      00354B FF               [12] 7079 	mov	r7,a
      00354C 90 03 73         [24] 7080 	mov	dptr,#(_axradio_timer + 0x0004)
      00354F EC               [12] 7081 	mov	a,r4
      003550 F0               [24] 7082 	movx	@dptr,a
      003551 ED               [12] 7083 	mov	a,r5
      003552 A3               [24] 7084 	inc	dptr
      003553 F0               [24] 7085 	movx	@dptr,a
      003554 EE               [12] 7086 	mov	a,r6
      003555 A3               [24] 7087 	inc	dptr
      003556 F0               [24] 7088 	movx	@dptr,a
      003557 EF               [12] 7089 	mov	a,r7
      003558 A3               [24] 7090 	inc	dptr
      003559 F0               [24] 7091 	movx	@dptr,a
                                   7092 ;	..\src\COMMON\easyax5043.c:1263: wtimer0_addabsolute(&axradio_timer);
      00355A 90 03 6F         [24] 7093 	mov	dptr,#_axradio_timer
      00355D 12 79 8E         [24] 7094 	lcall	_wtimer0_addabsolute
                                   7095 ;	..\src\COMMON\easyax5043.c:1264: wtimer_remove_callback(&axradio_cb_receive.cb);
      003560 90 03 18         [24] 7096 	mov	dptr,#_axradio_cb_receive
      003563 12 87 AB         [24] 7097 	lcall	_wtimer_remove_callback
                                   7098 ;	..\src\COMMON\easyax5043.c:1265: memset_xdata(&axradio_cb_receive.st, 0, sizeof(axradio_cb_receive.st));
      003566 90 04 58         [24] 7099 	mov	dptr,#_memset_PARM_2
      003569 E4               [12] 7100 	clr	a
      00356A F0               [24] 7101 	movx	@dptr,a
      00356B 90 04 59         [24] 7102 	mov	dptr,#_memset_PARM_3
      00356E 74 1E            [12] 7103 	mov	a,#0x1e
      003570 F0               [24] 7104 	movx	@dptr,a
      003571 E4               [12] 7105 	clr	a
      003572 A3               [24] 7106 	inc	dptr
      003573 F0               [24] 7107 	movx	@dptr,a
      003574 90 03 1C         [24] 7108 	mov	dptr,#(_axradio_cb_receive + 0x0004)
      003577 75 F0 00         [24] 7109 	mov	b,#0x00
      00357A 12 75 1E         [24] 7110 	lcall	_memset
                                   7111 ;	..\src\COMMON\easyax5043.c:1266: axradio_cb_receive.st.time.t = axradio_timeanchor.radiotimer;
      00357D 90 01 00         [24] 7112 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      003580 E0               [24] 7113 	movx	a,@dptr
      003581 FC               [12] 7114 	mov	r4,a
      003582 A3               [24] 7115 	inc	dptr
      003583 E0               [24] 7116 	movx	a,@dptr
      003584 FD               [12] 7117 	mov	r5,a
      003585 A3               [24] 7118 	inc	dptr
      003586 E0               [24] 7119 	movx	a,@dptr
      003587 FE               [12] 7120 	mov	r6,a
      003588 A3               [24] 7121 	inc	dptr
      003589 E0               [24] 7122 	movx	a,@dptr
      00358A FF               [12] 7123 	mov	r7,a
      00358B 90 03 1E         [24] 7124 	mov	dptr,#(_axradio_cb_receive + 0x0006)
      00358E EC               [12] 7125 	mov	a,r4
      00358F F0               [24] 7126 	movx	@dptr,a
      003590 ED               [12] 7127 	mov	a,r5
      003591 A3               [24] 7128 	inc	dptr
      003592 F0               [24] 7129 	movx	@dptr,a
      003593 EE               [12] 7130 	mov	a,r6
      003594 A3               [24] 7131 	inc	dptr
      003595 F0               [24] 7132 	movx	@dptr,a
      003596 EF               [12] 7133 	mov	a,r7
      003597 A3               [24] 7134 	inc	dptr
      003598 F0               [24] 7135 	movx	@dptr,a
                                   7136 ;	..\src\COMMON\easyax5043.c:1267: axradio_cb_receive.st.error = AXRADIO_ERR_RESYNCTIMEOUT;
      003599 90 03 1D         [24] 7137 	mov	dptr,#(_axradio_cb_receive + 0x0005)
      00359C 74 0A            [12] 7138 	mov	a,#0x0a
      00359E F0               [24] 7139 	movx	@dptr,a
                                   7140 ;	..\src\COMMON\easyax5043.c:1268: wtimer_add_callback(&axradio_cb_receive.cb);
      00359F 90 03 18         [24] 7141 	mov	dptr,#_axradio_cb_receive
                                   7142 ;	..\src\COMMON\easyax5043.c:1269: break;
      0035A2 02 78 5E         [24] 7143 	ljmp	_wtimer_add_callback
                                   7144 ;	..\src\COMMON\easyax5043.c:1271: case syncstate_slave_syncpause:
      0035A5                       7145 00156$:
                                   7146 ;	..\src\COMMON\easyax5043.c:1272: ax5043_receiver_on_continuous();
      0035A5 12 2B 9B         [24] 7147 	lcall	_ax5043_receiver_on_continuous
                                   7148 ;	..\src\COMMON\easyax5043.c:1273: axradio_syncstate = syncstate_slave_synchunt;
      0035A8 90 00 EA         [24] 7149 	mov	dptr,#_axradio_syncstate
      0035AB 74 06            [12] 7150 	mov	a,#0x06
      0035AD F0               [24] 7151 	movx	@dptr,a
                                   7152 ;	..\src\COMMON\easyax5043.c:1274: axradio_sync_addtime(axradio_sync_slave_syncwindow);
      0035AE 90 91 05         [24] 7153 	mov	dptr,#_axradio_sync_slave_syncwindow
      0035B1 E4               [12] 7154 	clr	a
      0035B2 93               [24] 7155 	movc	a,@a+dptr
      0035B3 FC               [12] 7156 	mov	r4,a
      0035B4 74 01            [12] 7157 	mov	a,#0x01
      0035B6 93               [24] 7158 	movc	a,@a+dptr
      0035B7 FD               [12] 7159 	mov	r5,a
      0035B8 74 02            [12] 7160 	mov	a,#0x02
      0035BA 93               [24] 7161 	movc	a,@a+dptr
      0035BB FE               [12] 7162 	mov	r6,a
      0035BC 74 03            [12] 7163 	mov	a,#0x03
      0035BE 93               [24] 7164 	movc	a,@a+dptr
      0035BF 8C 82            [24] 7165 	mov	dpl,r4
      0035C1 8D 83            [24] 7166 	mov	dph,r5
      0035C3 8E F0            [24] 7167 	mov	b,r6
      0035C5 12 2E 93         [24] 7168 	lcall	_axradio_sync_addtime
                                   7169 ;	..\src\COMMON\easyax5043.c:1275: wtimer_remove(&axradio_timer);
      0035C8 90 03 6F         [24] 7170 	mov	dptr,#_axradio_timer
      0035CB 12 85 39         [24] 7171 	lcall	_wtimer_remove
                                   7172 ;	..\src\COMMON\easyax5043.c:1276: axradio_timer.time = axradio_sync_time;
      0035CE 90 00 F6         [24] 7173 	mov	dptr,#_axradio_sync_time
      0035D1 E0               [24] 7174 	movx	a,@dptr
      0035D2 FC               [12] 7175 	mov	r4,a
      0035D3 A3               [24] 7176 	inc	dptr
      0035D4 E0               [24] 7177 	movx	a,@dptr
      0035D5 FD               [12] 7178 	mov	r5,a
      0035D6 A3               [24] 7179 	inc	dptr
      0035D7 E0               [24] 7180 	movx	a,@dptr
      0035D8 FE               [12] 7181 	mov	r6,a
      0035D9 A3               [24] 7182 	inc	dptr
      0035DA E0               [24] 7183 	movx	a,@dptr
      0035DB FF               [12] 7184 	mov	r7,a
      0035DC 90 03 73         [24] 7185 	mov	dptr,#(_axradio_timer + 0x0004)
      0035DF EC               [12] 7186 	mov	a,r4
      0035E0 F0               [24] 7187 	movx	@dptr,a
      0035E1 ED               [12] 7188 	mov	a,r5
      0035E2 A3               [24] 7189 	inc	dptr
      0035E3 F0               [24] 7190 	movx	@dptr,a
      0035E4 EE               [12] 7191 	mov	a,r6
      0035E5 A3               [24] 7192 	inc	dptr
      0035E6 F0               [24] 7193 	movx	@dptr,a
      0035E7 EF               [12] 7194 	mov	a,r7
      0035E8 A3               [24] 7195 	inc	dptr
      0035E9 F0               [24] 7196 	movx	@dptr,a
                                   7197 ;	..\src\COMMON\easyax5043.c:1277: wtimer0_addabsolute(&axradio_timer);
      0035EA 90 03 6F         [24] 7198 	mov	dptr,#_axradio_timer
      0035ED 12 79 8E         [24] 7199 	lcall	_wtimer0_addabsolute
                                   7200 ;	..\src\COMMON\easyax5043.c:1278: update_timeanchor();
      0035F0 12 1E 70         [24] 7201 	lcall	_update_timeanchor
                                   7202 ;	..\src\COMMON\easyax5043.c:1279: wtimer_remove_callback(&axradio_cb_receive.cb);
      0035F3 90 03 18         [24] 7203 	mov	dptr,#_axradio_cb_receive
      0035F6 12 87 AB         [24] 7204 	lcall	_wtimer_remove_callback
                                   7205 ;	..\src\COMMON\easyax5043.c:1280: memset_xdata(&axradio_cb_receive.st, 0, sizeof(axradio_cb_receive.st));
      0035F9 90 04 58         [24] 7206 	mov	dptr,#_memset_PARM_2
      0035FC E4               [12] 7207 	clr	a
      0035FD F0               [24] 7208 	movx	@dptr,a
      0035FE 90 04 59         [24] 7209 	mov	dptr,#_memset_PARM_3
      003601 74 1E            [12] 7210 	mov	a,#0x1e
      003603 F0               [24] 7211 	movx	@dptr,a
      003604 E4               [12] 7212 	clr	a
      003605 A3               [24] 7213 	inc	dptr
      003606 F0               [24] 7214 	movx	@dptr,a
      003607 90 03 1C         [24] 7215 	mov	dptr,#(_axradio_cb_receive + 0x0004)
      00360A 75 F0 00         [24] 7216 	mov	b,#0x00
      00360D 12 75 1E         [24] 7217 	lcall	_memset
                                   7218 ;	..\src\COMMON\easyax5043.c:1281: axradio_cb_receive.st.time.t = axradio_timeanchor.radiotimer;
      003610 90 01 00         [24] 7219 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      003613 E0               [24] 7220 	movx	a,@dptr
      003614 FC               [12] 7221 	mov	r4,a
      003615 A3               [24] 7222 	inc	dptr
      003616 E0               [24] 7223 	movx	a,@dptr
      003617 FD               [12] 7224 	mov	r5,a
      003618 A3               [24] 7225 	inc	dptr
      003619 E0               [24] 7226 	movx	a,@dptr
      00361A FE               [12] 7227 	mov	r6,a
      00361B A3               [24] 7228 	inc	dptr
      00361C E0               [24] 7229 	movx	a,@dptr
      00361D FF               [12] 7230 	mov	r7,a
      00361E 90 03 1E         [24] 7231 	mov	dptr,#(_axradio_cb_receive + 0x0006)
      003621 EC               [12] 7232 	mov	a,r4
      003622 F0               [24] 7233 	movx	@dptr,a
      003623 ED               [12] 7234 	mov	a,r5
      003624 A3               [24] 7235 	inc	dptr
      003625 F0               [24] 7236 	movx	@dptr,a
      003626 EE               [12] 7237 	mov	a,r6
      003627 A3               [24] 7238 	inc	dptr
      003628 F0               [24] 7239 	movx	@dptr,a
      003629 EF               [12] 7240 	mov	a,r7
      00362A A3               [24] 7241 	inc	dptr
      00362B F0               [24] 7242 	movx	@dptr,a
                                   7243 ;	..\src\COMMON\easyax5043.c:1282: axradio_cb_receive.st.error = AXRADIO_ERR_RESYNC;
      00362C 90 03 1D         [24] 7244 	mov	dptr,#(_axradio_cb_receive + 0x0005)
      00362F 74 09            [12] 7245 	mov	a,#0x09
      003631 F0               [24] 7246 	movx	@dptr,a
                                   7247 ;	..\src\COMMON\easyax5043.c:1283: wtimer_add_callback(&axradio_cb_receive.cb);
      003632 90 03 18         [24] 7248 	mov	dptr,#_axradio_cb_receive
                                   7249 ;	..\src\COMMON\easyax5043.c:1284: break;
      003635 02 78 5E         [24] 7250 	ljmp	_wtimer_add_callback
                                   7251 ;	..\src\COMMON\easyax5043.c:1286: case syncstate_slave_rxidle:
      003638                       7252 00157$:
                                   7253 ;	..\src\COMMON\easyax5043.c:1287: AX5043_PWRMODE = AX5043_PWRSTATE_XTAL_ON;
      003638 90 40 02         [24] 7254 	mov	dptr,#_AX5043_PWRMODE
      00363B 74 05            [12] 7255 	mov	a,#0x05
      00363D F0               [24] 7256 	movx	@dptr,a
                                   7257 ;	..\src\COMMON\easyax5043.c:1288: axradio_syncstate = syncstate_slave_rxxosc;
      00363E 90 00 EA         [24] 7258 	mov	dptr,#_axradio_syncstate
      003641 74 09            [12] 7259 	mov	a,#0x09
      003643 F0               [24] 7260 	movx	@dptr,a
                                   7261 ;	..\src\COMMON\easyax5043.c:1289: wtimer_remove(&axradio_timer);
      003644 90 03 6F         [24] 7262 	mov	dptr,#_axradio_timer
      003647 12 85 39         [24] 7263 	lcall	_wtimer_remove
                                   7264 ;	..\src\COMMON\easyax5043.c:1290: axradio_timer.time += axradio_sync_xoscstartup;
      00364A 90 03 73         [24] 7265 	mov	dptr,#(_axradio_timer + 0x0004)
      00364D E0               [24] 7266 	movx	a,@dptr
      00364E FC               [12] 7267 	mov	r4,a
      00364F A3               [24] 7268 	inc	dptr
      003650 E0               [24] 7269 	movx	a,@dptr
      003651 FD               [12] 7270 	mov	r5,a
      003652 A3               [24] 7271 	inc	dptr
      003653 E0               [24] 7272 	movx	a,@dptr
      003654 FE               [12] 7273 	mov	r6,a
      003655 A3               [24] 7274 	inc	dptr
      003656 E0               [24] 7275 	movx	a,@dptr
      003657 FF               [12] 7276 	mov	r7,a
      003658 90 91 01         [24] 7277 	mov	dptr,#_axradio_sync_xoscstartup
      00365B E4               [12] 7278 	clr	a
      00365C 93               [24] 7279 	movc	a,@a+dptr
      00365D F8               [12] 7280 	mov	r0,a
      00365E 74 01            [12] 7281 	mov	a,#0x01
      003660 93               [24] 7282 	movc	a,@a+dptr
      003661 F9               [12] 7283 	mov	r1,a
      003662 74 02            [12] 7284 	mov	a,#0x02
      003664 93               [24] 7285 	movc	a,@a+dptr
      003665 FA               [12] 7286 	mov	r2,a
      003666 74 03            [12] 7287 	mov	a,#0x03
      003668 93               [24] 7288 	movc	a,@a+dptr
      003669 FB               [12] 7289 	mov	r3,a
      00366A E8               [12] 7290 	mov	a,r0
      00366B 2C               [12] 7291 	add	a,r4
      00366C FC               [12] 7292 	mov	r4,a
      00366D E9               [12] 7293 	mov	a,r1
      00366E 3D               [12] 7294 	addc	a,r5
      00366F FD               [12] 7295 	mov	r5,a
      003670 EA               [12] 7296 	mov	a,r2
      003671 3E               [12] 7297 	addc	a,r6
      003672 FE               [12] 7298 	mov	r6,a
      003673 EB               [12] 7299 	mov	a,r3
      003674 3F               [12] 7300 	addc	a,r7
      003675 FF               [12] 7301 	mov	r7,a
      003676 90 03 73         [24] 7302 	mov	dptr,#(_axradio_timer + 0x0004)
      003679 EC               [12] 7303 	mov	a,r4
      00367A F0               [24] 7304 	movx	@dptr,a
      00367B ED               [12] 7305 	mov	a,r5
      00367C A3               [24] 7306 	inc	dptr
      00367D F0               [24] 7307 	movx	@dptr,a
      00367E EE               [12] 7308 	mov	a,r6
      00367F A3               [24] 7309 	inc	dptr
      003680 F0               [24] 7310 	movx	@dptr,a
      003681 EF               [12] 7311 	mov	a,r7
      003682 A3               [24] 7312 	inc	dptr
      003683 F0               [24] 7313 	movx	@dptr,a
                                   7314 ;	..\src\COMMON\easyax5043.c:1291: wtimer0_addabsolute(&axradio_timer);
      003684 90 03 6F         [24] 7315 	mov	dptr,#_axradio_timer
                                   7316 ;	..\src\COMMON\easyax5043.c:1292: break;
      003687 02 79 8E         [24] 7317 	ljmp	_wtimer0_addabsolute
                                   7318 ;	..\src\COMMON\easyax5043.c:1294: case syncstate_slave_rxxosc:
      00368A                       7319 00158$:
                                   7320 ;	..\src\COMMON\easyax5043.c:1295: ax5043_receiver_on_continuous();
      00368A 12 2B 9B         [24] 7321 	lcall	_ax5043_receiver_on_continuous
                                   7322 ;	..\src\COMMON\easyax5043.c:1296: axradio_syncstate = syncstate_slave_rxsfdwindow;
      00368D 90 00 EA         [24] 7323 	mov	dptr,#_axradio_syncstate
      003690 74 0A            [12] 7324 	mov	a,#0x0a
      003692 F0               [24] 7325 	movx	@dptr,a
                                   7326 ;	..\src\COMMON\easyax5043.c:1297: update_timeanchor();
      003693 12 1E 70         [24] 7327 	lcall	_update_timeanchor
                                   7328 ;	..\src\COMMON\easyax5043.c:1298: wtimer_remove_callback(&axradio_cb_receive.cb);
      003696 90 03 18         [24] 7329 	mov	dptr,#_axradio_cb_receive
      003699 12 87 AB         [24] 7330 	lcall	_wtimer_remove_callback
                                   7331 ;	..\src\COMMON\easyax5043.c:1299: memset_xdata(&axradio_cb_receive.st, 0, sizeof(axradio_cb_receive.st));
      00369C 90 04 58         [24] 7332 	mov	dptr,#_memset_PARM_2
      00369F E4               [12] 7333 	clr	a
      0036A0 F0               [24] 7334 	movx	@dptr,a
      0036A1 90 04 59         [24] 7335 	mov	dptr,#_memset_PARM_3
      0036A4 74 1E            [12] 7336 	mov	a,#0x1e
      0036A6 F0               [24] 7337 	movx	@dptr,a
      0036A7 E4               [12] 7338 	clr	a
      0036A8 A3               [24] 7339 	inc	dptr
      0036A9 F0               [24] 7340 	movx	@dptr,a
      0036AA 90 03 1C         [24] 7341 	mov	dptr,#(_axradio_cb_receive + 0x0004)
      0036AD 75 F0 00         [24] 7342 	mov	b,#0x00
      0036B0 12 75 1E         [24] 7343 	lcall	_memset
                                   7344 ;	..\src\COMMON\easyax5043.c:1300: axradio_cb_receive.st.time.t = axradio_timeanchor.radiotimer;
      0036B3 90 01 00         [24] 7345 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      0036B6 E0               [24] 7346 	movx	a,@dptr
      0036B7 FC               [12] 7347 	mov	r4,a
      0036B8 A3               [24] 7348 	inc	dptr
      0036B9 E0               [24] 7349 	movx	a,@dptr
      0036BA FD               [12] 7350 	mov	r5,a
      0036BB A3               [24] 7351 	inc	dptr
      0036BC E0               [24] 7352 	movx	a,@dptr
      0036BD FE               [12] 7353 	mov	r6,a
      0036BE A3               [24] 7354 	inc	dptr
      0036BF E0               [24] 7355 	movx	a,@dptr
      0036C0 FF               [12] 7356 	mov	r7,a
      0036C1 90 03 1E         [24] 7357 	mov	dptr,#(_axradio_cb_receive + 0x0006)
      0036C4 EC               [12] 7358 	mov	a,r4
      0036C5 F0               [24] 7359 	movx	@dptr,a
      0036C6 ED               [12] 7360 	mov	a,r5
      0036C7 A3               [24] 7361 	inc	dptr
      0036C8 F0               [24] 7362 	movx	@dptr,a
      0036C9 EE               [12] 7363 	mov	a,r6
      0036CA A3               [24] 7364 	inc	dptr
      0036CB F0               [24] 7365 	movx	@dptr,a
      0036CC EF               [12] 7366 	mov	a,r7
      0036CD A3               [24] 7367 	inc	dptr
      0036CE F0               [24] 7368 	movx	@dptr,a
                                   7369 ;	..\src\COMMON\easyax5043.c:1301: axradio_cb_receive.st.error = AXRADIO_ERR_RECEIVESTART;
      0036CF 90 03 1D         [24] 7370 	mov	dptr,#(_axradio_cb_receive + 0x0005)
      0036D2 74 0B            [12] 7371 	mov	a,#0x0b
      0036D4 F0               [24] 7372 	movx	@dptr,a
                                   7373 ;	..\src\COMMON\easyax5043.c:1302: wtimer_add_callback(&axradio_cb_receive.cb);
      0036D5 90 03 18         [24] 7374 	mov	dptr,#_axradio_cb_receive
      0036D8 12 78 5E         [24] 7375 	lcall	_wtimer_add_callback
                                   7376 ;	..\src\COMMON\easyax5043.c:1303: wtimer_remove(&axradio_timer);
      0036DB 90 03 6F         [24] 7377 	mov	dptr,#_axradio_timer
      0036DE 12 85 39         [24] 7378 	lcall	_wtimer_remove
                                   7379 ;	..\src\COMMON\easyax5043.c:1305: uint8_t __autodata idx = axradio_sync_seqnr;
      0036E1 90 00 F5         [24] 7380 	mov	dptr,#_axradio_ack_seqnr
      0036E4 E0               [24] 7381 	movx	a,@dptr
      0036E5 FF               [12] 7382 	mov	r7,a
                                   7383 ;	..\src\COMMON\easyax5043.c:1306: if (idx >= axradio_sync_slave_nrrx)
      0036E6 90 91 14         [24] 7384 	mov	dptr,#_axradio_sync_slave_nrrx
      0036E9 E4               [12] 7385 	clr	a
      0036EA 93               [24] 7386 	movc	a,@a+dptr
      0036EB FE               [12] 7387 	mov	r6,a
      0036EC C3               [12] 7388 	clr	c
      0036ED EF               [12] 7389 	mov	a,r7
      0036EE 9E               [12] 7390 	subb	a,r6
      0036EF 40 03            [24] 7391 	jc	00160$
                                   7392 ;	..\src\COMMON\easyax5043.c:1307: idx = axradio_sync_slave_nrrx - 1;
      0036F1 EE               [12] 7393 	mov	a,r6
      0036F2 14               [12] 7394 	dec	a
      0036F3 FF               [12] 7395 	mov	r7,a
      0036F4                       7396 00160$:
                                   7397 ;	..\src\COMMON\easyax5043.c:1308: axradio_timer.time += axradio_sync_slave_rxwindow[idx];
      0036F4 90 03 73         [24] 7398 	mov	dptr,#(_axradio_timer + 0x0004)
      0036F7 E0               [24] 7399 	movx	a,@dptr
      0036F8 FB               [12] 7400 	mov	r3,a
      0036F9 A3               [24] 7401 	inc	dptr
      0036FA E0               [24] 7402 	movx	a,@dptr
      0036FB FC               [12] 7403 	mov	r4,a
      0036FC A3               [24] 7404 	inc	dptr
      0036FD E0               [24] 7405 	movx	a,@dptr
      0036FE FD               [12] 7406 	mov	r5,a
      0036FF A3               [24] 7407 	inc	dptr
      003700 E0               [24] 7408 	movx	a,@dptr
      003701 FE               [12] 7409 	mov	r6,a
      003702 EF               [12] 7410 	mov	a,r7
      003703 75 F0 04         [24] 7411 	mov	b,#0x04
      003706 A4               [48] 7412 	mul	ab
      003707 24 21            [12] 7413 	add	a,#_axradio_sync_slave_rxwindow
      003709 F5 82            [12] 7414 	mov	dpl,a
      00370B 74 91            [12] 7415 	mov	a,#(_axradio_sync_slave_rxwindow >> 8)
      00370D 35 F0            [12] 7416 	addc	a,b
      00370F F5 83            [12] 7417 	mov	dph,a
      003711 E4               [12] 7418 	clr	a
      003712 93               [24] 7419 	movc	a,@a+dptr
      003713 F8               [12] 7420 	mov	r0,a
      003714 A3               [24] 7421 	inc	dptr
      003715 E4               [12] 7422 	clr	a
      003716 93               [24] 7423 	movc	a,@a+dptr
      003717 F9               [12] 7424 	mov	r1,a
      003718 A3               [24] 7425 	inc	dptr
      003719 E4               [12] 7426 	clr	a
      00371A 93               [24] 7427 	movc	a,@a+dptr
      00371B FA               [12] 7428 	mov	r2,a
      00371C A3               [24] 7429 	inc	dptr
      00371D E4               [12] 7430 	clr	a
      00371E 93               [24] 7431 	movc	a,@a+dptr
      00371F FF               [12] 7432 	mov	r7,a
      003720 E8               [12] 7433 	mov	a,r0
      003721 2B               [12] 7434 	add	a,r3
      003722 FB               [12] 7435 	mov	r3,a
      003723 E9               [12] 7436 	mov	a,r1
      003724 3C               [12] 7437 	addc	a,r4
      003725 FC               [12] 7438 	mov	r4,a
      003726 EA               [12] 7439 	mov	a,r2
      003727 3D               [12] 7440 	addc	a,r5
      003728 FD               [12] 7441 	mov	r5,a
      003729 EF               [12] 7442 	mov	a,r7
      00372A 3E               [12] 7443 	addc	a,r6
      00372B FE               [12] 7444 	mov	r6,a
      00372C 90 03 73         [24] 7445 	mov	dptr,#(_axradio_timer + 0x0004)
      00372F EB               [12] 7446 	mov	a,r3
      003730 F0               [24] 7447 	movx	@dptr,a
      003731 EC               [12] 7448 	mov	a,r4
      003732 A3               [24] 7449 	inc	dptr
      003733 F0               [24] 7450 	movx	@dptr,a
      003734 ED               [12] 7451 	mov	a,r5
      003735 A3               [24] 7452 	inc	dptr
      003736 F0               [24] 7453 	movx	@dptr,a
      003737 EE               [12] 7454 	mov	a,r6
      003738 A3               [24] 7455 	inc	dptr
      003739 F0               [24] 7456 	movx	@dptr,a
                                   7457 ;	..\src\COMMON\easyax5043.c:1310: wtimer0_addabsolute(&axradio_timer);
      00373A 90 03 6F         [24] 7458 	mov	dptr,#_axradio_timer
                                   7459 ;	..\src\COMMON\easyax5043.c:1311: break;
      00373D 02 79 8E         [24] 7460 	ljmp	_wtimer0_addabsolute
                                   7461 ;	..\src\COMMON\easyax5043.c:1313: case syncstate_slave_rxsfdwindow:
      003740                       7462 00161$:
                                   7463 ;	..\src\COMMON\easyax5043.c:1315: uint8_t __autodata rs = AX5043_RADIOSTATE;
      003740 90 40 1C         [24] 7464 	mov	dptr,#_AX5043_RADIOSTATE
      003743 E0               [24] 7465 	movx	a,@dptr
                                   7466 ;	..\src\COMMON\easyax5043.c:1316: if( !rs )
      003744 FF               [12] 7467 	mov	r7,a
      003745 FE               [12] 7468 	mov	r6,a
      003746 70 01            [24] 7469 	jnz	00323$
      003748 22               [24] 7470 	ret
      003749                       7471 00323$:
                                   7472 ;	..\src\COMMON\easyax5043.c:1319: if (!(0x0F & (uint8_t)~rs)) {
      003749 EE               [12] 7473 	mov	a,r6
      00374A F4               [12] 7474 	cpl	a
      00374B FE               [12] 7475 	mov	r6,a
      00374C 54 0F            [12] 7476 	anl	a,#0x0f
      00374E 60 02            [24] 7477 	jz	00325$
      003750 80 4C            [24] 7478 	sjmp	00166$
      003752                       7479 00325$:
                                   7480 ;	..\src\COMMON\easyax5043.c:1320: axradio_syncstate = syncstate_slave_rxpacket;
      003752 90 00 EA         [24] 7481 	mov	dptr,#_axradio_syncstate
      003755 74 0B            [12] 7482 	mov	a,#0x0b
      003757 F0               [24] 7483 	movx	@dptr,a
                                   7484 ;	..\src\COMMON\easyax5043.c:1321: wtimer_remove(&axradio_timer);
      003758 90 03 6F         [24] 7485 	mov	dptr,#_axradio_timer
      00375B 12 85 39         [24] 7486 	lcall	_wtimer_remove
                                   7487 ;	..\src\COMMON\easyax5043.c:1322: axradio_timer.time += axradio_sync_slave_rxtimeout;
      00375E 90 03 73         [24] 7488 	mov	dptr,#(_axradio_timer + 0x0004)
      003761 E0               [24] 7489 	movx	a,@dptr
      003762 FC               [12] 7490 	mov	r4,a
      003763 A3               [24] 7491 	inc	dptr
      003764 E0               [24] 7492 	movx	a,@dptr
      003765 FD               [12] 7493 	mov	r5,a
      003766 A3               [24] 7494 	inc	dptr
      003767 E0               [24] 7495 	movx	a,@dptr
      003768 FE               [12] 7496 	mov	r6,a
      003769 A3               [24] 7497 	inc	dptr
      00376A E0               [24] 7498 	movx	a,@dptr
      00376B FF               [12] 7499 	mov	r7,a
      00376C 90 91 2D         [24] 7500 	mov	dptr,#_axradio_sync_slave_rxtimeout
      00376F E4               [12] 7501 	clr	a
      003770 93               [24] 7502 	movc	a,@a+dptr
      003771 F8               [12] 7503 	mov	r0,a
      003772 74 01            [12] 7504 	mov	a,#0x01
      003774 93               [24] 7505 	movc	a,@a+dptr
      003775 F9               [12] 7506 	mov	r1,a
      003776 74 02            [12] 7507 	mov	a,#0x02
      003778 93               [24] 7508 	movc	a,@a+dptr
      003779 FA               [12] 7509 	mov	r2,a
      00377A 74 03            [12] 7510 	mov	a,#0x03
      00377C 93               [24] 7511 	movc	a,@a+dptr
      00377D FB               [12] 7512 	mov	r3,a
      00377E E8               [12] 7513 	mov	a,r0
      00377F 2C               [12] 7514 	add	a,r4
      003780 FC               [12] 7515 	mov	r4,a
      003781 E9               [12] 7516 	mov	a,r1
      003782 3D               [12] 7517 	addc	a,r5
      003783 FD               [12] 7518 	mov	r5,a
      003784 EA               [12] 7519 	mov	a,r2
      003785 3E               [12] 7520 	addc	a,r6
      003786 FE               [12] 7521 	mov	r6,a
      003787 EB               [12] 7522 	mov	a,r3
      003788 3F               [12] 7523 	addc	a,r7
      003789 FF               [12] 7524 	mov	r7,a
      00378A 90 03 73         [24] 7525 	mov	dptr,#(_axradio_timer + 0x0004)
      00378D EC               [12] 7526 	mov	a,r4
      00378E F0               [24] 7527 	movx	@dptr,a
      00378F ED               [12] 7528 	mov	a,r5
      003790 A3               [24] 7529 	inc	dptr
      003791 F0               [24] 7530 	movx	@dptr,a
      003792 EE               [12] 7531 	mov	a,r6
      003793 A3               [24] 7532 	inc	dptr
      003794 F0               [24] 7533 	movx	@dptr,a
      003795 EF               [12] 7534 	mov	a,r7
      003796 A3               [24] 7535 	inc	dptr
      003797 F0               [24] 7536 	movx	@dptr,a
                                   7537 ;	..\src\COMMON\easyax5043.c:1323: wtimer0_addabsolute(&axradio_timer);
      003798 90 03 6F         [24] 7538 	mov	dptr,#_axradio_timer
                                   7539 ;	..\src\COMMON\easyax5043.c:1324: break;
      00379B 02 79 8E         [24] 7540 	ljmp	_wtimer0_addabsolute
                                   7541 ;	..\src\COMMON\easyax5043.c:1329: case syncstate_slave_rxpacket:
      00379E                       7542 00166$:
                                   7543 ;	..\src\COMMON\easyax5043.c:1330: ax5043_off();
      00379E 12 2C F0         [24] 7544 	lcall	_ax5043_off
                                   7545 ;	..\src\COMMON\easyax5043.c:1331: if (!axradio_sync_seqnr)
      0037A1 90 00 F5         [24] 7546 	mov	dptr,#_axradio_ack_seqnr
      0037A4 E0               [24] 7547 	movx	a,@dptr
      0037A5 70 06            [24] 7548 	jnz	00168$
                                   7549 ;	..\src\COMMON\easyax5043.c:1332: axradio_sync_seqnr = 1;
      0037A7 90 00 F5         [24] 7550 	mov	dptr,#_axradio_ack_seqnr
      0037AA 74 01            [12] 7551 	mov	a,#0x01
      0037AC F0               [24] 7552 	movx	@dptr,a
      0037AD                       7553 00168$:
                                   7554 ;	..\src\COMMON\easyax5043.c:1333: ++axradio_sync_seqnr;
      0037AD 90 00 F5         [24] 7555 	mov	dptr,#_axradio_ack_seqnr
      0037B0 E0               [24] 7556 	movx	a,@dptr
      0037B1 24 01            [12] 7557 	add	a,#0x01
      0037B3 F0               [24] 7558 	movx	@dptr,a
                                   7559 ;	..\src\COMMON\easyax5043.c:1334: update_timeanchor();
      0037B4 12 1E 70         [24] 7560 	lcall	_update_timeanchor
                                   7561 ;	..\src\COMMON\easyax5043.c:1335: wtimer_remove_callback(&axradio_cb_receive.cb);
      0037B7 90 03 18         [24] 7562 	mov	dptr,#_axradio_cb_receive
      0037BA 12 87 AB         [24] 7563 	lcall	_wtimer_remove_callback
                                   7564 ;	..\src\COMMON\easyax5043.c:1336: memset_xdata(&axradio_cb_receive.st, 0, sizeof(axradio_cb_receive.st));
      0037BD 90 04 58         [24] 7565 	mov	dptr,#_memset_PARM_2
      0037C0 E4               [12] 7566 	clr	a
      0037C1 F0               [24] 7567 	movx	@dptr,a
      0037C2 90 04 59         [24] 7568 	mov	dptr,#_memset_PARM_3
      0037C5 74 1E            [12] 7569 	mov	a,#0x1e
      0037C7 F0               [24] 7570 	movx	@dptr,a
      0037C8 E4               [12] 7571 	clr	a
      0037C9 A3               [24] 7572 	inc	dptr
      0037CA F0               [24] 7573 	movx	@dptr,a
      0037CB 90 03 1C         [24] 7574 	mov	dptr,#(_axradio_cb_receive + 0x0004)
      0037CE 75 F0 00         [24] 7575 	mov	b,#0x00
      0037D1 12 75 1E         [24] 7576 	lcall	_memset
                                   7577 ;	..\src\COMMON\easyax5043.c:1337: axradio_cb_receive.st.time.t = axradio_timeanchor.radiotimer;
      0037D4 90 01 00         [24] 7578 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      0037D7 E0               [24] 7579 	movx	a,@dptr
      0037D8 FC               [12] 7580 	mov	r4,a
      0037D9 A3               [24] 7581 	inc	dptr
      0037DA E0               [24] 7582 	movx	a,@dptr
      0037DB FD               [12] 7583 	mov	r5,a
      0037DC A3               [24] 7584 	inc	dptr
      0037DD E0               [24] 7585 	movx	a,@dptr
      0037DE FE               [12] 7586 	mov	r6,a
      0037DF A3               [24] 7587 	inc	dptr
      0037E0 E0               [24] 7588 	movx	a,@dptr
      0037E1 FF               [12] 7589 	mov	r7,a
      0037E2 90 03 1E         [24] 7590 	mov	dptr,#(_axradio_cb_receive + 0x0006)
      0037E5 EC               [12] 7591 	mov	a,r4
      0037E6 F0               [24] 7592 	movx	@dptr,a
      0037E7 ED               [12] 7593 	mov	a,r5
      0037E8 A3               [24] 7594 	inc	dptr
      0037E9 F0               [24] 7595 	movx	@dptr,a
      0037EA EE               [12] 7596 	mov	a,r6
      0037EB A3               [24] 7597 	inc	dptr
      0037EC F0               [24] 7598 	movx	@dptr,a
      0037ED EF               [12] 7599 	mov	a,r7
      0037EE A3               [24] 7600 	inc	dptr
      0037EF F0               [24] 7601 	movx	@dptr,a
                                   7602 ;	..\src\COMMON\easyax5043.c:1338: axradio_cb_receive.st.error = AXRADIO_ERR_TIMEOUT;
      0037F0 90 03 1D         [24] 7603 	mov	dptr,#(_axradio_cb_receive + 0x0005)
      0037F3 74 03            [12] 7604 	mov	a,#0x03
      0037F5 F0               [24] 7605 	movx	@dptr,a
                                   7606 ;	..\src\COMMON\easyax5043.c:1339: if (axradio_sync_seqnr <= axradio_sync_slave_resyncloss) {
      0037F6 90 00 F5         [24] 7607 	mov	dptr,#_axradio_ack_seqnr
      0037F9 E0               [24] 7608 	movx	a,@dptr
      0037FA FF               [12] 7609 	mov	r7,a
      0037FB 90 91 13         [24] 7610 	mov	dptr,#_axradio_sync_slave_resyncloss
      0037FE E4               [12] 7611 	clr	a
      0037FF 93               [24] 7612 	movc	a,@a+dptr
      003800 C3               [12] 7613 	clr	c
      003801 9F               [12] 7614 	subb	a,r7
      003802 40 54            [24] 7615 	jc	00172$
                                   7616 ;	..\src\COMMON\easyax5043.c:1340: wtimer_add_callback(&axradio_cb_receive.cb);
      003804 90 03 18         [24] 7617 	mov	dptr,#_axradio_cb_receive
      003807 12 78 5E         [24] 7618 	lcall	_wtimer_add_callback
                                   7619 ;	..\src\COMMON\easyax5043.c:1341: axradio_sync_slave_nextperiod();
      00380A 12 30 41         [24] 7620 	lcall	_axradio_sync_slave_nextperiod
                                   7621 ;	..\src\COMMON\easyax5043.c:1342: axradio_syncstate = syncstate_slave_rxidle;
      00380D 90 00 EA         [24] 7622 	mov	dptr,#_axradio_syncstate
      003810 74 08            [12] 7623 	mov	a,#0x08
      003812 F0               [24] 7624 	movx	@dptr,a
                                   7625 ;	..\src\COMMON\easyax5043.c:1343: wtimer_remove(&axradio_timer);
      003813 90 03 6F         [24] 7626 	mov	dptr,#_axradio_timer
      003816 12 85 39         [24] 7627 	lcall	_wtimer_remove
                                   7628 ;	..\src\COMMON\easyax5043.c:1345: uint8_t __autodata idx = axradio_sync_seqnr;
      003819 90 00 F5         [24] 7629 	mov	dptr,#_axradio_ack_seqnr
      00381C E0               [24] 7630 	movx	a,@dptr
      00381D FF               [12] 7631 	mov	r7,a
                                   7632 ;	..\src\COMMON\easyax5043.c:1346: if (idx >= axradio_sync_slave_nrrx)
      00381E 90 91 14         [24] 7633 	mov	dptr,#_axradio_sync_slave_nrrx
      003821 E4               [12] 7634 	clr	a
      003822 93               [24] 7635 	movc	a,@a+dptr
      003823 FE               [12] 7636 	mov	r6,a
      003824 C3               [12] 7637 	clr	c
      003825 EF               [12] 7638 	mov	a,r7
      003826 9E               [12] 7639 	subb	a,r6
      003827 40 03            [24] 7640 	jc	00170$
                                   7641 ;	..\src\COMMON\easyax5043.c:1347: idx = axradio_sync_slave_nrrx - 1;
      003829 EE               [12] 7642 	mov	a,r6
      00382A 14               [12] 7643 	dec	a
      00382B FF               [12] 7644 	mov	r7,a
      00382C                       7645 00170$:
                                   7646 ;	..\src\COMMON\easyax5043.c:1348: axradio_sync_settimeradv(axradio_sync_slave_rxadvance[idx]);
      00382C EF               [12] 7647 	mov	a,r7
      00382D 75 F0 04         [24] 7648 	mov	b,#0x04
      003830 A4               [48] 7649 	mul	ab
      003831 24 15            [12] 7650 	add	a,#_axradio_sync_slave_rxadvance
      003833 F5 82            [12] 7651 	mov	dpl,a
      003835 74 91            [12] 7652 	mov	a,#(_axradio_sync_slave_rxadvance >> 8)
      003837 35 F0            [12] 7653 	addc	a,b
      003839 F5 83            [12] 7654 	mov	dph,a
      00383B E4               [12] 7655 	clr	a
      00383C 93               [24] 7656 	movc	a,@a+dptr
      00383D FC               [12] 7657 	mov	r4,a
      00383E A3               [24] 7658 	inc	dptr
      00383F E4               [12] 7659 	clr	a
      003840 93               [24] 7660 	movc	a,@a+dptr
      003841 FD               [12] 7661 	mov	r5,a
      003842 A3               [24] 7662 	inc	dptr
      003843 E4               [12] 7663 	clr	a
      003844 93               [24] 7664 	movc	a,@a+dptr
      003845 FE               [12] 7665 	mov	r6,a
      003846 A3               [24] 7666 	inc	dptr
      003847 E4               [12] 7667 	clr	a
      003848 93               [24] 7668 	movc	a,@a+dptr
      003849 8C 82            [24] 7669 	mov	dpl,r4
      00384B 8D 83            [24] 7670 	mov	dph,r5
      00384D 8E F0            [24] 7671 	mov	b,r6
      00384F 12 2E E4         [24] 7672 	lcall	_axradio_sync_settimeradv
                                   7673 ;	..\src\COMMON\easyax5043.c:1350: wtimer0_addabsolute(&axradio_timer);
      003852 90 03 6F         [24] 7674 	mov	dptr,#_axradio_timer
                                   7675 ;	..\src\COMMON\easyax5043.c:1351: break;
      003855 02 79 8E         [24] 7676 	ljmp	_wtimer0_addabsolute
      003858                       7677 00172$:
                                   7678 ;	..\src\COMMON\easyax5043.c:1353: axradio_cb_receive.st.error = AXRADIO_ERR_RESYNC;
      003858 90 03 1D         [24] 7679 	mov	dptr,#(_axradio_cb_receive + 0x0005)
      00385B 74 09            [12] 7680 	mov	a,#0x09
      00385D F0               [24] 7681 	movx	@dptr,a
                                   7682 ;	..\src\COMMON\easyax5043.c:1354: wtimer_add_callback(&axradio_cb_receive.cb);
      00385E 90 03 18         [24] 7683 	mov	dptr,#_axradio_cb_receive
      003861 12 78 5E         [24] 7684 	lcall	_wtimer_add_callback
                                   7685 ;	..\src\COMMON\easyax5043.c:1355: ax5043_receiver_on_continuous();
      003864 12 2B 9B         [24] 7686 	lcall	_ax5043_receiver_on_continuous
                                   7687 ;	..\src\COMMON\easyax5043.c:1356: axradio_syncstate = syncstate_slave_synchunt;
      003867 90 00 EA         [24] 7688 	mov	dptr,#_axradio_syncstate
      00386A 74 06            [12] 7689 	mov	a,#0x06
      00386C F0               [24] 7690 	movx	@dptr,a
                                   7691 ;	..\src\COMMON\easyax5043.c:1357: wtimer_remove(&axradio_timer);
      00386D 90 03 6F         [24] 7692 	mov	dptr,#_axradio_timer
      003870 12 85 39         [24] 7693 	lcall	_wtimer_remove
                                   7694 ;	..\src\COMMON\easyax5043.c:1358: axradio_timer.time = axradio_sync_slave_syncwindow;
      003873 90 91 05         [24] 7695 	mov	dptr,#_axradio_sync_slave_syncwindow
      003876 E4               [12] 7696 	clr	a
      003877 93               [24] 7697 	movc	a,@a+dptr
      003878 FC               [12] 7698 	mov	r4,a
      003879 74 01            [12] 7699 	mov	a,#0x01
      00387B 93               [24] 7700 	movc	a,@a+dptr
      00387C FD               [12] 7701 	mov	r5,a
      00387D 74 02            [12] 7702 	mov	a,#0x02
      00387F 93               [24] 7703 	movc	a,@a+dptr
      003880 FE               [12] 7704 	mov	r6,a
      003881 74 03            [12] 7705 	mov	a,#0x03
      003883 93               [24] 7706 	movc	a,@a+dptr
      003884 FF               [12] 7707 	mov	r7,a
      003885 90 03 73         [24] 7708 	mov	dptr,#(_axradio_timer + 0x0004)
      003888 EC               [12] 7709 	mov	a,r4
      003889 F0               [24] 7710 	movx	@dptr,a
      00388A ED               [12] 7711 	mov	a,r5
      00388B A3               [24] 7712 	inc	dptr
      00388C F0               [24] 7713 	movx	@dptr,a
      00388D EE               [12] 7714 	mov	a,r6
      00388E A3               [24] 7715 	inc	dptr
      00388F F0               [24] 7716 	movx	@dptr,a
      003890 EF               [12] 7717 	mov	a,r7
      003891 A3               [24] 7718 	inc	dptr
      003892 F0               [24] 7719 	movx	@dptr,a
                                   7720 ;	..\src\COMMON\easyax5043.c:1359: wtimer0_addrelative(&axradio_timer);
      003893 90 03 6F         [24] 7721 	mov	dptr,#_axradio_timer
      003896 12 78 78         [24] 7722 	lcall	_wtimer0_addrelative
                                   7723 ;	..\src\COMMON\easyax5043.c:1360: axradio_sync_time = axradio_timer.time;
      003899 90 03 73         [24] 7724 	mov	dptr,#(_axradio_timer + 0x0004)
      00389C E0               [24] 7725 	movx	a,@dptr
      00389D FC               [12] 7726 	mov	r4,a
      00389E A3               [24] 7727 	inc	dptr
      00389F E0               [24] 7728 	movx	a,@dptr
      0038A0 FD               [12] 7729 	mov	r5,a
      0038A1 A3               [24] 7730 	inc	dptr
      0038A2 E0               [24] 7731 	movx	a,@dptr
      0038A3 FE               [12] 7732 	mov	r6,a
      0038A4 A3               [24] 7733 	inc	dptr
      0038A5 E0               [24] 7734 	movx	a,@dptr
      0038A6 FF               [12] 7735 	mov	r7,a
      0038A7 90 00 F6         [24] 7736 	mov	dptr,#_axradio_sync_time
      0038AA EC               [12] 7737 	mov	a,r4
      0038AB F0               [24] 7738 	movx	@dptr,a
      0038AC ED               [12] 7739 	mov	a,r5
      0038AD A3               [24] 7740 	inc	dptr
      0038AE F0               [24] 7741 	movx	@dptr,a
      0038AF EE               [12] 7742 	mov	a,r6
      0038B0 A3               [24] 7743 	inc	dptr
      0038B1 F0               [24] 7744 	movx	@dptr,a
      0038B2 EF               [12] 7745 	mov	a,r7
      0038B3 A3               [24] 7746 	inc	dptr
      0038B4 F0               [24] 7747 	movx	@dptr,a
                                   7748 ;	..\src\COMMON\easyax5043.c:1361: break;
                                   7749 ;	..\src\COMMON\easyax5043.c:1363: case syncstate_slave_rxack:
      0038B5 22               [24] 7750 	ret
      0038B6                       7751 00173$:
                                   7752 ;	..\src\COMMON\easyax5043.c:1364: axradio_syncstate = syncstate_slave_rxidle;
      0038B6 90 00 EA         [24] 7753 	mov	dptr,#_axradio_syncstate
      0038B9 74 08            [12] 7754 	mov	a,#0x08
      0038BB F0               [24] 7755 	movx	@dptr,a
                                   7756 ;	..\src\COMMON\easyax5043.c:1365: wtimer_remove(&axradio_timer);
      0038BC 90 03 6F         [24] 7757 	mov	dptr,#_axradio_timer
      0038BF 12 85 39         [24] 7758 	lcall	_wtimer_remove
                                   7759 ;	..\src\COMMON\easyax5043.c:1366: axradio_sync_settimeradv(axradio_sync_slave_rxadvance[1]);
      0038C2 90 91 19         [24] 7760 	mov	dptr,#(_axradio_sync_slave_rxadvance + 0x0004)
      0038C5 E4               [12] 7761 	clr	a
      0038C6 93               [24] 7762 	movc	a,@a+dptr
      0038C7 FC               [12] 7763 	mov	r4,a
      0038C8 A3               [24] 7764 	inc	dptr
      0038C9 E4               [12] 7765 	clr	a
      0038CA 93               [24] 7766 	movc	a,@a+dptr
      0038CB FD               [12] 7767 	mov	r5,a
      0038CC A3               [24] 7768 	inc	dptr
      0038CD E4               [12] 7769 	clr	a
      0038CE 93               [24] 7770 	movc	a,@a+dptr
      0038CF FE               [12] 7771 	mov	r6,a
      0038D0 A3               [24] 7772 	inc	dptr
      0038D1 E4               [12] 7773 	clr	a
      0038D2 93               [24] 7774 	movc	a,@a+dptr
      0038D3 8C 82            [24] 7775 	mov	dpl,r4
      0038D5 8D 83            [24] 7776 	mov	dph,r5
      0038D7 8E F0            [24] 7777 	mov	b,r6
      0038D9 12 2E E4         [24] 7778 	lcall	_axradio_sync_settimeradv
                                   7779 ;	..\src\COMMON\easyax5043.c:1367: wtimer0_addabsolute(&axradio_timer);
      0038DC 90 03 6F         [24] 7780 	mov	dptr,#_axradio_timer
      0038DF 12 79 8E         [24] 7781 	lcall	_wtimer0_addabsolute
                                   7782 ;	..\src\COMMON\easyax5043.c:1368: goto transmitack;
      0038E2 02 32 B0         [24] 7783 	ljmp	00133$
                                   7784 ;	..\src\COMMON\easyax5043.c:1374: }
      0038E5                       7785 00177$:
                                   7786 ;	..\src\COMMON\easyax5043.c:1375: }
      0038E5 22               [24] 7787 	ret
                                   7788 ;------------------------------------------------------------
                                   7789 ;Allocation info for local variables in function 'axradio_callback_fwd'
                                   7790 ;------------------------------------------------------------
                                   7791 ;desc                      Allocated to registers r6 r7 
                                   7792 ;------------------------------------------------------------
                                   7793 ;	..\src\COMMON\easyax5043.c:1377: static __reentrantb void axradio_callback_fwd(struct wtimer_callback __xdata *desc) __reentrant
                                   7794 ;	-----------------------------------------
                                   7795 ;	 function axradio_callback_fwd
                                   7796 ;	-----------------------------------------
      0038E6                       7797 _axradio_callback_fwd:
      0038E6 AE 82            [24] 7798 	mov	r6,dpl
      0038E8 AF 83            [24] 7799 	mov	r7,dph
                                   7800 ;	..\src\COMMON\easyax5043.c:1379: axradio_statuschange((struct axradio_status __xdata *)(desc + 1));
      0038EA 74 04            [12] 7801 	mov	a,#0x04
      0038EC 2E               [12] 7802 	add	a,r6
      0038ED FE               [12] 7803 	mov	r6,a
      0038EE E4               [12] 7804 	clr	a
      0038EF 3F               [12] 7805 	addc	a,r7
      0038F0 FF               [12] 7806 	mov	r7,a
      0038F1 8E 82            [24] 7807 	mov	dpl,r6
      0038F3 8F 83            [24] 7808 	mov	dph,r7
                                   7809 ;	..\src\COMMON\easyax5043.c:1380: }
      0038F5 02 04 3C         [24] 7810 	ljmp	_axradio_statuschange
                                   7811 ;------------------------------------------------------------
                                   7812 ;Allocation info for local variables in function 'axradio_receive_callback_fwd'
                                   7813 ;------------------------------------------------------------
                                   7814 ;len                       Allocated to registers r5 r4 
                                   7815 ;len                       Allocated to registers r6 r7 
                                   7816 ;trxst                     Allocated to registers r6 
                                   7817 ;iesave                    Allocated to registers r7 
                                   7818 ;desc                      Allocated with name '_axradio_receive_callback_fwd_desc_65536_368'
                                   7819 ;seqnr                     Allocated with name '_axradio_receive_callback_fwd_seqnr_196608_376'
                                   7820 ;len_byte                  Allocated with name '_axradio_receive_callback_fwd_len_byte_196608_378'
                                   7821 ;------------------------------------------------------------
                                   7822 ;	..\src\COMMON\easyax5043.c:1382: static void axradio_receive_callback_fwd(struct wtimer_callback __xdata *desc)
                                   7823 ;	-----------------------------------------
                                   7824 ;	 function axradio_receive_callback_fwd
                                   7825 ;	-----------------------------------------
      0038F8                       7826 _axradio_receive_callback_fwd:
                                   7827 ;	..\src\COMMON\easyax5043.c:1386: if (axradio_cb_receive.st.error != AXRADIO_ERR_NOERROR) {
      0038F8 90 03 1D         [24] 7828 	mov	dptr,#(_axradio_cb_receive + 0x0005)
      0038FB E0               [24] 7829 	movx	a,@dptr
      0038FC 60 06            [24] 7830 	jz	00102$
                                   7831 ;	..\src\COMMON\easyax5043.c:1387: axradio_statuschange((struct axradio_status __xdata *)&axradio_cb_receive.st);
      0038FE 90 03 1C         [24] 7832 	mov	dptr,#(_axradio_cb_receive + 0x0004)
                                   7833 ;	..\src\COMMON\easyax5043.c:1388: return;
      003901 02 04 3C         [24] 7834 	ljmp	_axradio_statuschange
      003904                       7835 00102$:
                                   7836 ;	..\src\COMMON\easyax5043.c:1390: if (axradio_phy_pn9 && !AXRADIO_MODE_IS_STREAM_RECEIVE(axradio_mode)) {
      003904 90 90 C0         [24] 7837 	mov	dptr,#_axradio_phy_pn9
      003907 E4               [12] 7838 	clr	a
      003908 93               [24] 7839 	movc	a,@a+dptr
      003909 60 56            [24] 7840 	jz	00104$
      00390B AE 17            [24] 7841 	mov	r6,_axradio_mode
      00390D 53 06 F8         [24] 7842 	anl	ar6,#0xf8
      003910 7F 00            [12] 7843 	mov	r7,#0x00
      003912 BE 28 05         [24] 7844 	cjne	r6,#0x28,00350$
      003915 BF 00 02         [24] 7845 	cjne	r7,#0x00,00350$
      003918 80 47            [24] 7846 	sjmp	00104$
      00391A                       7847 00350$:
                                   7848 ;	..\src\COMMON\easyax5043.c:1391: uint16_t __autodata len = axradio_cb_receive.st.rx.pktlen;
      00391A 90 03 38         [24] 7849 	mov	dptr,#(_axradio_cb_receive + 0x0020)
      00391D E0               [24] 7850 	movx	a,@dptr
      00391E FE               [12] 7851 	mov	r6,a
      00391F A3               [24] 7852 	inc	dptr
      003920 E0               [24] 7853 	movx	a,@dptr
      003921 FF               [12] 7854 	mov	r7,a
                                   7855 ;	..\src\COMMON\easyax5043.c:1392: len += axradio_framing_maclen;
      003922 90 90 E1         [24] 7856 	mov	dptr,#_axradio_framing_maclen
      003925 E4               [12] 7857 	clr	a
      003926 93               [24] 7858 	movc	a,@a+dptr
      003927 7C 00            [12] 7859 	mov	r4,#0x00
      003929 2E               [12] 7860 	add	a,r6
      00392A FD               [12] 7861 	mov	r5,a
      00392B EC               [12] 7862 	mov	a,r4
      00392C 3F               [12] 7863 	addc	a,r7
      00392D FC               [12] 7864 	mov	r4,a
                                   7865 ;	..\src\COMMON\easyax5043.c:1393: pn9_buffer((__xdata uint8_t *)axradio_cb_receive.st.rx.mac.raw, len, 0x1ff, -(AX5043_ENCODING & 0x01));
      00392E 90 40 11         [24] 7866 	mov	dptr,#_AX5043_ENCODING
      003931 E0               [24] 7867 	movx	a,@dptr
      003932 FF               [12] 7868 	mov	r7,a
      003933 53 07 01         [24] 7869 	anl	ar7,#0x01
      003936 C3               [12] 7870 	clr	c
      003937 E4               [12] 7871 	clr	a
      003938 9F               [12] 7872 	subb	a,r7
      003939 FF               [12] 7873 	mov	r7,a
      00393A 90 03 34         [24] 7874 	mov	dptr,#(_axradio_cb_receive + 0x001c)
      00393D E0               [24] 7875 	movx	a,@dptr
      00393E FB               [12] 7876 	mov	r3,a
      00393F A3               [24] 7877 	inc	dptr
      003940 E0               [24] 7878 	movx	a,@dptr
      003941 FE               [12] 7879 	mov	r6,a
      003942 7A 00            [12] 7880 	mov	r2,#0x00
      003944 C0 07            [24] 7881 	push	ar7
      003946 74 FF            [12] 7882 	mov	a,#0xff
      003948 C0 E0            [24] 7883 	push	acc
      00394A 74 01            [12] 7884 	mov	a,#0x01
      00394C C0 E0            [24] 7885 	push	acc
      00394E C0 05            [24] 7886 	push	ar5
      003950 C0 04            [24] 7887 	push	ar4
      003952 8B 82            [24] 7888 	mov	dpl,r3
      003954 8E 83            [24] 7889 	mov	dph,r6
      003956 8A F0            [24] 7890 	mov	b,r2
      003958 12 79 E1         [24] 7891 	lcall	_pn9_buffer
      00395B E5 81            [12] 7892 	mov	a,sp
      00395D 24 FB            [12] 7893 	add	a,#0xfb
      00395F F5 81            [12] 7894 	mov	sp,a
      003961                       7895 00104$:
                                   7896 ;	..\src\COMMON\easyax5043.c:1395: if (axradio_framing_swcrclen && !AXRADIO_MODE_IS_STREAM_RECEIVE(axradio_mode)) {
      003961 90 90 E8         [24] 7897 	mov	dptr,#_axradio_framing_swcrclen
      003964 E4               [12] 7898 	clr	a
      003965 93               [24] 7899 	movc	a,@a+dptr
      003966 60 6B            [24] 7900 	jz	00109$
      003968 AE 17            [24] 7901 	mov	r6,_axradio_mode
      00396A 53 06 F8         [24] 7902 	anl	ar6,#0xf8
      00396D 7F 00            [12] 7903 	mov	r7,#0x00
      00396F BE 28 05         [24] 7904 	cjne	r6,#0x28,00352$
      003972 BF 00 02         [24] 7905 	cjne	r7,#0x00,00352$
      003975 80 5C            [24] 7906 	sjmp	00109$
      003977                       7907 00352$:
                                   7908 ;	..\src\COMMON\easyax5043.c:1396: uint16_t __autodata len = axradio_cb_receive.st.rx.pktlen;
      003977 90 03 38         [24] 7909 	mov	dptr,#(_axradio_cb_receive + 0x0020)
      00397A E0               [24] 7910 	movx	a,@dptr
      00397B FE               [12] 7911 	mov	r6,a
      00397C A3               [24] 7912 	inc	dptr
      00397D E0               [24] 7913 	movx	a,@dptr
      00397E FF               [12] 7914 	mov	r7,a
                                   7915 ;	..\src\COMMON\easyax5043.c:1397: len += axradio_framing_maclen;
      00397F 90 90 E1         [24] 7916 	mov	dptr,#_axradio_framing_maclen
      003982 E4               [12] 7917 	clr	a
      003983 93               [24] 7918 	movc	a,@a+dptr
      003984 7C 00            [12] 7919 	mov	r4,#0x00
      003986 2E               [12] 7920 	add	a,r6
      003987 FD               [12] 7921 	mov	r5,a
      003988 EC               [12] 7922 	mov	a,r4
      003989 3F               [12] 7923 	addc	a,r7
      00398A FC               [12] 7924 	mov	r4,a
                                   7925 ;	..\src\COMMON\easyax5043.c:1398: len = axradio_framing_check_crc((uint8_t __xdata *)axradio_cb_receive.st.rx.mac.raw, len);
      00398B 90 03 34         [24] 7926 	mov	dptr,#(_axradio_cb_receive + 0x001c)
      00398E E0               [24] 7927 	movx	a,@dptr
      00398F FE               [12] 7928 	mov	r6,a
      003990 A3               [24] 7929 	inc	dptr
      003991 E0               [24] 7930 	movx	a,@dptr
      003992 FF               [12] 7931 	mov	r7,a
      003993 C0 05            [24] 7932 	push	ar5
      003995 C0 04            [24] 7933 	push	ar4
      003997 8E 82            [24] 7934 	mov	dpl,r6
      003999 8F 83            [24] 7935 	mov	dph,r7
      00399B 12 1C 6A         [24] 7936 	lcall	_axradio_framing_check_crc
      00399E AE 82            [24] 7937 	mov	r6,dpl
      0039A0 AF 83            [24] 7938 	mov	r7,dph
      0039A2 15 81            [12] 7939 	dec	sp
      0039A4 15 81            [12] 7940 	dec	sp
                                   7941 ;	..\src\COMMON\easyax5043.c:1399: if (!len)
      0039A6 EE               [12] 7942 	mov	a,r6
      0039A7 4F               [12] 7943 	orl	a,r7
      0039A8 70 03            [24] 7944 	jnz	00353$
      0039AA 02 3D E1         [24] 7945 	ljmp	00159$
      0039AD                       7946 00353$:
                                   7947 ;	..\src\COMMON\easyax5043.c:1402: len -= axradio_framing_maclen;
      0039AD 90 90 E1         [24] 7948 	mov	dptr,#_axradio_framing_maclen
      0039B0 E4               [12] 7949 	clr	a
      0039B1 93               [24] 7950 	movc	a,@a+dptr
      0039B2 FD               [12] 7951 	mov	r5,a
      0039B3 7C 00            [12] 7952 	mov	r4,#0x00
      0039B5 EE               [12] 7953 	mov	a,r6
      0039B6 C3               [12] 7954 	clr	c
      0039B7 9D               [12] 7955 	subb	a,r5
      0039B8 FE               [12] 7956 	mov	r6,a
      0039B9 EF               [12] 7957 	mov	a,r7
      0039BA 9C               [12] 7958 	subb	a,r4
      0039BB FF               [12] 7959 	mov	r7,a
                                   7960 ;	..\src\COMMON\easyax5043.c:1403: len -= axradio_framing_swcrclen; // drop crc
      0039BC 90 90 E8         [24] 7961 	mov	dptr,#_axradio_framing_swcrclen
      0039BF E4               [12] 7962 	clr	a
      0039C0 93               [24] 7963 	movc	a,@a+dptr
      0039C1 FD               [12] 7964 	mov	r5,a
      0039C2 7C 00            [12] 7965 	mov	r4,#0x00
      0039C4 EE               [12] 7966 	mov	a,r6
      0039C5 C3               [12] 7967 	clr	c
      0039C6 9D               [12] 7968 	subb	a,r5
      0039C7 FD               [12] 7969 	mov	r5,a
      0039C8 EF               [12] 7970 	mov	a,r7
      0039C9 9C               [12] 7971 	subb	a,r4
      0039CA FC               [12] 7972 	mov	r4,a
                                   7973 ;	..\src\COMMON\easyax5043.c:1404: axradio_cb_receive.st.rx.pktlen = len;
      0039CB 90 03 38         [24] 7974 	mov	dptr,#(_axradio_cb_receive + 0x0020)
      0039CE ED               [12] 7975 	mov	a,r5
      0039CF F0               [24] 7976 	movx	@dptr,a
      0039D0 EC               [12] 7977 	mov	a,r4
      0039D1 A3               [24] 7978 	inc	dptr
      0039D2 F0               [24] 7979 	movx	@dptr,a
      0039D3                       7980 00109$:
                                   7981 ;	..\src\COMMON\easyax5043.c:1408: axradio_cb_receive.st.rx.phy.timeoffset = 0;
      0039D3 90 03 28         [24] 7982 	mov	dptr,#(_axradio_cb_receive + 0x0010)
      0039D6 E4               [12] 7983 	clr	a
      0039D7 F0               [24] 7984 	movx	@dptr,a
      0039D8 A3               [24] 7985 	inc	dptr
      0039D9 F0               [24] 7986 	movx	@dptr,a
                                   7987 ;	..\src\COMMON\easyax5043.c:1409: axradio_cb_receive.st.rx.phy.period = 0;
      0039DA 90 03 2A         [24] 7988 	mov	dptr,#(_axradio_cb_receive + 0x0012)
      0039DD F0               [24] 7989 	movx	@dptr,a
      0039DE A3               [24] 7990 	inc	dptr
      0039DF F0               [24] 7991 	movx	@dptr,a
                                   7992 ;	..\src\COMMON\easyax5043.c:1410: if (axradio_mode == AXRADIO_MODE_ACK_TRANSMIT ||
      0039E0 74 12            [12] 7993 	mov	a,#0x12
      0039E2 B5 17 02         [24] 7994 	cjne	a,_axradio_mode,00354$
      0039E5 80 0C            [24] 7995 	sjmp	00113$
      0039E7                       7996 00354$:
                                   7997 ;	..\src\COMMON\easyax5043.c:1411: axradio_mode == AXRADIO_MODE_WOR_ACK_TRANSMIT ||
      0039E7 74 13            [12] 7998 	mov	a,#0x13
      0039E9 B5 17 02         [24] 7999 	cjne	a,_axradio_mode,00355$
      0039EC 80 05            [24] 8000 	sjmp	00113$
      0039EE                       8001 00355$:
                                   8002 ;	..\src\COMMON\easyax5043.c:1412: axradio_mode == AXRADIO_MODE_SYNC_ACK_MASTER) {
      0039EE 74 31            [12] 8003 	mov	a,#0x31
      0039F0 B5 17 65         [24] 8004 	cjne	a,_axradio_mode,00114$
      0039F3                       8005 00113$:
                                   8006 ;	..\src\COMMON\easyax5043.c:1413: ax5043_off();
      0039F3 12 2C F0         [24] 8007 	lcall	_ax5043_off
                                   8008 ;	..\src\COMMON\easyax5043.c:1414: wtimer_remove(&axradio_timer);
      0039F6 90 03 6F         [24] 8009 	mov	dptr,#_axradio_timer
      0039F9 12 85 39         [24] 8010 	lcall	_wtimer_remove
                                   8011 ;	..\src\COMMON\easyax5043.c:1415: if (axradio_mode == AXRADIO_MODE_SYNC_ACK_MASTER) {
      0039FC 74 31            [12] 8012 	mov	a,#0x31
      0039FE B5 17 26         [24] 8013 	cjne	a,_axradio_mode,00112$
                                   8014 ;	..\src\COMMON\easyax5043.c:1416: axradio_syncstate = syncstate_master_normal;
      003A01 90 00 EA         [24] 8015 	mov	dptr,#_axradio_syncstate
      003A04 74 03            [12] 8016 	mov	a,#0x03
      003A06 F0               [24] 8017 	movx	@dptr,a
                                   8018 ;	..\src\COMMON\easyax5043.c:1417: axradio_sync_settimeradv(axradio_sync_xoscstartup);
      003A07 90 91 01         [24] 8019 	mov	dptr,#_axradio_sync_xoscstartup
      003A0A E4               [12] 8020 	clr	a
      003A0B 93               [24] 8021 	movc	a,@a+dptr
      003A0C FC               [12] 8022 	mov	r4,a
      003A0D 74 01            [12] 8023 	mov	a,#0x01
      003A0F 93               [24] 8024 	movc	a,@a+dptr
      003A10 FD               [12] 8025 	mov	r5,a
      003A11 74 02            [12] 8026 	mov	a,#0x02
      003A13 93               [24] 8027 	movc	a,@a+dptr
      003A14 FE               [12] 8028 	mov	r6,a
      003A15 74 03            [12] 8029 	mov	a,#0x03
      003A17 93               [24] 8030 	movc	a,@a+dptr
      003A18 8C 82            [24] 8031 	mov	dpl,r4
      003A1A 8D 83            [24] 8032 	mov	dph,r5
      003A1C 8E F0            [24] 8033 	mov	b,r6
      003A1E 12 2E E4         [24] 8034 	lcall	_axradio_sync_settimeradv
                                   8035 ;	..\src\COMMON\easyax5043.c:1418: wtimer0_addabsolute(&axradio_timer);
      003A21 90 03 6F         [24] 8036 	mov	dptr,#_axradio_timer
      003A24 12 79 8E         [24] 8037 	lcall	_wtimer0_addabsolute
      003A27                       8038 00112$:
                                   8039 ;	..\src\COMMON\easyax5043.c:1420: wtimer_remove_callback(&axradio_cb_transmitend.cb);
      003A27 90 03 5B         [24] 8040 	mov	dptr,#_axradio_cb_transmitend
      003A2A 12 87 AB         [24] 8041 	lcall	_wtimer_remove_callback
                                   8042 ;	..\src\COMMON\easyax5043.c:1421: axradio_cb_transmitend.st.error = AXRADIO_ERR_NOERROR;
      003A2D 90 03 60         [24] 8043 	mov	dptr,#(_axradio_cb_transmitend + 0x0005)
      003A30 E4               [12] 8044 	clr	a
      003A31 F0               [24] 8045 	movx	@dptr,a
                                   8046 ;	..\src\COMMON\easyax5043.c:1422: axradio_cb_transmitend.st.time.t = radio_read24((uint16_t)&AX5043_TIMER2);
      003A32 7E 59            [12] 8047 	mov	r6,#_AX5043_TIMER2
      003A34 7F 40            [12] 8048 	mov	r7,#(_AX5043_TIMER2 >> 8)
      003A36 8E 82            [24] 8049 	mov	dpl,r6
      003A38 8F 83            [24] 8050 	mov	dph,r7
      003A3A 12 79 BA         [24] 8051 	lcall	_radio_read24
      003A3D AC 82            [24] 8052 	mov	r4,dpl
      003A3F AD 83            [24] 8053 	mov	r5,dph
      003A41 AE F0            [24] 8054 	mov	r6,b
      003A43 FF               [12] 8055 	mov	r7,a
      003A44 90 03 61         [24] 8056 	mov	dptr,#(_axradio_cb_transmitend + 0x0006)
      003A47 EC               [12] 8057 	mov	a,r4
      003A48 F0               [24] 8058 	movx	@dptr,a
      003A49 ED               [12] 8059 	mov	a,r5
      003A4A A3               [24] 8060 	inc	dptr
      003A4B F0               [24] 8061 	movx	@dptr,a
      003A4C EE               [12] 8062 	mov	a,r6
      003A4D A3               [24] 8063 	inc	dptr
      003A4E F0               [24] 8064 	movx	@dptr,a
      003A4F EF               [12] 8065 	mov	a,r7
      003A50 A3               [24] 8066 	inc	dptr
      003A51 F0               [24] 8067 	movx	@dptr,a
                                   8068 ;	..\src\COMMON\easyax5043.c:1423: wtimer_add_callback(&axradio_cb_transmitend.cb);
      003A52 90 03 5B         [24] 8069 	mov	dptr,#_axradio_cb_transmitend
      003A55 12 78 5E         [24] 8070 	lcall	_wtimer_add_callback
      003A58                       8071 00114$:
                                   8072 ;	..\src\COMMON\easyax5043.c:1425: if (axradio_framing_destaddrpos != 0xff)
      003A58 90 90 E3         [24] 8073 	mov	dptr,#_axradio_framing_destaddrpos
      003A5B E4               [12] 8074 	clr	a
      003A5C 93               [24] 8075 	movc	a,@a+dptr
      003A5D FF               [12] 8076 	mov	r7,a
      003A5E BF FF 02         [24] 8077 	cjne	r7,#0xff,00360$
      003A61 80 34            [24] 8078 	sjmp	00118$
      003A63                       8079 00360$:
                                   8080 ;	..\src\COMMON\easyax5043.c:1426: memcpy_xdata(&axradio_cb_receive.st.rx.mac.localaddr, &axradio_cb_receive.st.rx.mac.raw[axradio_framing_destaddrpos], axradio_framing_addrlen);
      003A63 90 03 34         [24] 8081 	mov	dptr,#(_axradio_cb_receive + 0x001c)
      003A66 E0               [24] 8082 	movx	a,@dptr
      003A67 FD               [12] 8083 	mov	r5,a
      003A68 A3               [24] 8084 	inc	dptr
      003A69 E0               [24] 8085 	movx	a,@dptr
      003A6A FE               [12] 8086 	mov	r6,a
      003A6B EF               [12] 8087 	mov	a,r7
      003A6C 2D               [12] 8088 	add	a,r5
      003A6D FF               [12] 8089 	mov	r7,a
      003A6E E4               [12] 8090 	clr	a
      003A6F 3E               [12] 8091 	addc	a,r6
      003A70 FC               [12] 8092 	mov	r4,a
      003A71 7E 00            [12] 8093 	mov	r6,#0x00
      003A73 90 90 E2         [24] 8094 	mov	dptr,#_axradio_framing_addrlen
      003A76 E4               [12] 8095 	clr	a
      003A77 93               [24] 8096 	movc	a,@a+dptr
      003A78 FD               [12] 8097 	mov	r5,a
      003A79 7B 00            [12] 8098 	mov	r3,#0x00
      003A7B 90 04 6D         [24] 8099 	mov	dptr,#_memcpy_PARM_2
      003A7E EF               [12] 8100 	mov	a,r7
      003A7F F0               [24] 8101 	movx	@dptr,a
      003A80 EC               [12] 8102 	mov	a,r4
      003A81 A3               [24] 8103 	inc	dptr
      003A82 F0               [24] 8104 	movx	@dptr,a
      003A83 EE               [12] 8105 	mov	a,r6
      003A84 A3               [24] 8106 	inc	dptr
      003A85 F0               [24] 8107 	movx	@dptr,a
      003A86 90 04 70         [24] 8108 	mov	dptr,#_memcpy_PARM_3
      003A89 ED               [12] 8109 	mov	a,r5
      003A8A F0               [24] 8110 	movx	@dptr,a
      003A8B EB               [12] 8111 	mov	a,r3
      003A8C A3               [24] 8112 	inc	dptr
      003A8D F0               [24] 8113 	movx	@dptr,a
      003A8E 90 03 30         [24] 8114 	mov	dptr,#(_axradio_cb_receive + 0x0018)
      003A91 75 F0 00         [24] 8115 	mov	b,#0x00
      003A94 12 78 14         [24] 8116 	lcall	_memcpy
      003A97                       8117 00118$:
                                   8118 ;	..\src\COMMON\easyax5043.c:1427: if (axradio_framing_sourceaddrpos != 0xff)
      003A97 90 90 E4         [24] 8119 	mov	dptr,#_axradio_framing_sourceaddrpos
      003A9A E4               [12] 8120 	clr	a
      003A9B 93               [24] 8121 	movc	a,@a+dptr
      003A9C FF               [12] 8122 	mov	r7,a
      003A9D BF FF 02         [24] 8123 	cjne	r7,#0xff,00361$
      003AA0 80 34            [24] 8124 	sjmp	00120$
      003AA2                       8125 00361$:
                                   8126 ;	..\src\COMMON\easyax5043.c:1428: memcpy_xdata(&axradio_cb_receive.st.rx.mac.remoteaddr, &axradio_cb_receive.st.rx.mac.raw[axradio_framing_sourceaddrpos], axradio_framing_addrlen);
      003AA2 90 03 34         [24] 8127 	mov	dptr,#(_axradio_cb_receive + 0x001c)
      003AA5 E0               [24] 8128 	movx	a,@dptr
      003AA6 FD               [12] 8129 	mov	r5,a
      003AA7 A3               [24] 8130 	inc	dptr
      003AA8 E0               [24] 8131 	movx	a,@dptr
      003AA9 FE               [12] 8132 	mov	r6,a
      003AAA EF               [12] 8133 	mov	a,r7
      003AAB 2D               [12] 8134 	add	a,r5
      003AAC FF               [12] 8135 	mov	r7,a
      003AAD E4               [12] 8136 	clr	a
      003AAE 3E               [12] 8137 	addc	a,r6
      003AAF FC               [12] 8138 	mov	r4,a
      003AB0 7E 00            [12] 8139 	mov	r6,#0x00
      003AB2 90 90 E2         [24] 8140 	mov	dptr,#_axradio_framing_addrlen
      003AB5 E4               [12] 8141 	clr	a
      003AB6 93               [24] 8142 	movc	a,@a+dptr
      003AB7 FD               [12] 8143 	mov	r5,a
      003AB8 7B 00            [12] 8144 	mov	r3,#0x00
      003ABA 90 04 6D         [24] 8145 	mov	dptr,#_memcpy_PARM_2
      003ABD EF               [12] 8146 	mov	a,r7
      003ABE F0               [24] 8147 	movx	@dptr,a
      003ABF EC               [12] 8148 	mov	a,r4
      003AC0 A3               [24] 8149 	inc	dptr
      003AC1 F0               [24] 8150 	movx	@dptr,a
      003AC2 EE               [12] 8151 	mov	a,r6
      003AC3 A3               [24] 8152 	inc	dptr
      003AC4 F0               [24] 8153 	movx	@dptr,a
      003AC5 90 04 70         [24] 8154 	mov	dptr,#_memcpy_PARM_3
      003AC8 ED               [12] 8155 	mov	a,r5
      003AC9 F0               [24] 8156 	movx	@dptr,a
      003ACA EB               [12] 8157 	mov	a,r3
      003ACB A3               [24] 8158 	inc	dptr
      003ACC F0               [24] 8159 	movx	@dptr,a
      003ACD 90 03 2C         [24] 8160 	mov	dptr,#(_axradio_cb_receive + 0x0014)
      003AD0 75 F0 00         [24] 8161 	mov	b,#0x00
      003AD3 12 78 14         [24] 8162 	lcall	_memcpy
      003AD6                       8163 00120$:
                                   8164 ;	..\src\COMMON\easyax5043.c:1429: if (axradio_mode == AXRADIO_MODE_ACK_RECEIVE ||
      003AD6 74 22            [12] 8165 	mov	a,#0x22
      003AD8 B5 17 02         [24] 8166 	cjne	a,_axradio_mode,00362$
      003ADB 80 11            [24] 8167 	sjmp	00142$
      003ADD                       8168 00362$:
                                   8169 ;	..\src\COMMON\easyax5043.c:1430: axradio_mode == AXRADIO_MODE_WOR_ACK_RECEIVE ||
      003ADD 74 23            [12] 8170 	mov	a,#0x23
      003ADF B5 17 02         [24] 8171 	cjne	a,_axradio_mode,00363$
      003AE2 80 0A            [24] 8172 	sjmp	00142$
      003AE4                       8173 00363$:
                                   8174 ;	..\src\COMMON\easyax5043.c:1431: axradio_mode == AXRADIO_MODE_SYNC_ACK_SLAVE) {
      003AE4 74 33            [12] 8175 	mov	a,#0x33
      003AE6 B5 17 02         [24] 8176 	cjne	a,_axradio_mode,00364$
      003AE9 80 03            [24] 8177 	sjmp	00365$
      003AEB                       8178 00364$:
      003AEB 02 3C F8         [24] 8179 	ljmp	00143$
      003AEE                       8180 00365$:
      003AEE                       8181 00142$:
                                   8182 ;	..\src\COMMON\easyax5043.c:1432: axradio_ack_count = 0;
      003AEE 90 00 F4         [24] 8183 	mov	dptr,#_axradio_ack_count
      003AF1 E4               [12] 8184 	clr	a
      003AF2 F0               [24] 8185 	movx	@dptr,a
                                   8186 ;	..\src\COMMON\easyax5043.c:1433: axradio_txbuffer_len = axradio_framing_maclen + axradio_framing_minpayloadlen;
      003AF3 90 90 E1         [24] 8187 	mov	dptr,#_axradio_framing_maclen
                                   8188 ;	genFromRTrack removed	clr	a
      003AF6 93               [24] 8189 	movc	a,@a+dptr
      003AF7 FF               [12] 8190 	mov	r7,a
      003AF8 FD               [12] 8191 	mov	r5,a
      003AF9 7E 00            [12] 8192 	mov	r6,#0x00
      003AFB 90 90 FA         [24] 8193 	mov	dptr,#_axradio_framing_minpayloadlen
      003AFE E4               [12] 8194 	clr	a
      003AFF 93               [24] 8195 	movc	a,@a+dptr
      003B00 FC               [12] 8196 	mov	r4,a
      003B01 7B 00            [12] 8197 	mov	r3,#0x00
      003B03 90 00 EB         [24] 8198 	mov	dptr,#_axradio_txbuffer_len
      003B06 EC               [12] 8199 	mov	a,r4
      003B07 2D               [12] 8200 	add	a,r5
      003B08 F0               [24] 8201 	movx	@dptr,a
      003B09 EB               [12] 8202 	mov	a,r3
      003B0A 3E               [12] 8203 	addc	a,r6
      003B0B A3               [24] 8204 	inc	dptr
      003B0C F0               [24] 8205 	movx	@dptr,a
                                   8206 ;	..\src\COMMON\easyax5043.c:1434: memset_xdata(axradio_txbuffer, 0, axradio_framing_maclen);
      003B0D 7E 00            [12] 8207 	mov	r6,#0x00
      003B0F 90 04 58         [24] 8208 	mov	dptr,#_memset_PARM_2
      003B12 E4               [12] 8209 	clr	a
      003B13 F0               [24] 8210 	movx	@dptr,a
      003B14 90 04 59         [24] 8211 	mov	dptr,#_memset_PARM_3
      003B17 EF               [12] 8212 	mov	a,r7
      003B18 F0               [24] 8213 	movx	@dptr,a
      003B19 EE               [12] 8214 	mov	a,r6
      003B1A A3               [24] 8215 	inc	dptr
      003B1B F0               [24] 8216 	movx	@dptr,a
      003B1C 90 01 10         [24] 8217 	mov	dptr,#_axradio_txbuffer
      003B1F 75 F0 00         [24] 8218 	mov	b,#0x00
      003B22 12 75 1E         [24] 8219 	lcall	_memset
                                   8220 ;	..\src\COMMON\easyax5043.c:1435: if (axradio_framing_ack_seqnrpos != 0xff) {
      003B25 90 90 F9         [24] 8221 	mov	dptr,#_axradio_framing_ack_seqnrpos
      003B28 E4               [12] 8222 	clr	a
      003B29 93               [24] 8223 	movc	a,@a+dptr
      003B2A FF               [12] 8224 	mov	r7,a
      003B2B BF FF 02         [24] 8225 	cjne	r7,#0xff,00366$
      003B2E 80 35            [24] 8226 	sjmp	00125$
      003B30                       8227 00366$:
                                   8228 ;	..\src\COMMON\easyax5043.c:1436: uint8_t seqnr = axradio_cb_receive.st.rx.mac.raw[axradio_framing_ack_seqnrpos];
      003B30 90 03 34         [24] 8229 	mov	dptr,#(_axradio_cb_receive + 0x001c)
      003B33 E0               [24] 8230 	movx	a,@dptr
      003B34 FD               [12] 8231 	mov	r5,a
      003B35 A3               [24] 8232 	inc	dptr
      003B36 E0               [24] 8233 	movx	a,@dptr
      003B37 FE               [12] 8234 	mov	r6,a
      003B38 EF               [12] 8235 	mov	a,r7
      003B39 2D               [12] 8236 	add	a,r5
      003B3A F5 82            [12] 8237 	mov	dpl,a
      003B3C E4               [12] 8238 	clr	a
      003B3D 3E               [12] 8239 	addc	a,r6
      003B3E F5 83            [12] 8240 	mov	dph,a
      003B40 E0               [24] 8241 	movx	a,@dptr
      003B41 FE               [12] 8242 	mov	r6,a
                                   8243 ;	..\src\COMMON\easyax5043.c:1437: axradio_txbuffer[axradio_framing_ack_seqnrpos] = seqnr;
      003B42 EF               [12] 8244 	mov	a,r7
      003B43 24 10            [12] 8245 	add	a,#_axradio_txbuffer
      003B45 F5 82            [12] 8246 	mov	dpl,a
      003B47 E4               [12] 8247 	clr	a
      003B48 34 01            [12] 8248 	addc	a,#(_axradio_txbuffer >> 8)
      003B4A F5 83            [12] 8249 	mov	dph,a
      003B4C EE               [12] 8250 	mov	a,r6
      003B4D F0               [24] 8251 	movx	@dptr,a
                                   8252 ;	..\src\COMMON\easyax5043.c:1438: if (axradio_ack_seqnr != seqnr)
      003B4E 90 00 F5         [24] 8253 	mov	dptr,#_axradio_ack_seqnr
      003B51 E0               [24] 8254 	movx	a,@dptr
      003B52 FF               [12] 8255 	mov	r7,a
      003B53 B5 06 02         [24] 8256 	cjne	a,ar6,00367$
      003B56 80 07            [24] 8257 	sjmp	00122$
      003B58                       8258 00367$:
                                   8259 ;	..\src\COMMON\easyax5043.c:1439: axradio_ack_seqnr = seqnr;
      003B58 90 00 F5         [24] 8260 	mov	dptr,#_axradio_ack_seqnr
      003B5B EE               [12] 8261 	mov	a,r6
      003B5C F0               [24] 8262 	movx	@dptr,a
      003B5D 80 06            [24] 8263 	sjmp	00125$
      003B5F                       8264 00122$:
                                   8265 ;	..\src\COMMON\easyax5043.c:1441: axradio_cb_receive.st.error = AXRADIO_ERR_RETRANSMISSION;
      003B5F 90 03 1D         [24] 8266 	mov	dptr,#(_axradio_cb_receive + 0x0005)
      003B62 74 08            [12] 8267 	mov	a,#0x08
      003B64 F0               [24] 8268 	movx	@dptr,a
      003B65                       8269 00125$:
                                   8270 ;	..\src\COMMON\easyax5043.c:1443: if (axradio_framing_destaddrpos != 0xff) {
      003B65 90 90 E3         [24] 8271 	mov	dptr,#_axradio_framing_destaddrpos
      003B68 E4               [12] 8272 	clr	a
      003B69 93               [24] 8273 	movc	a,@a+dptr
      003B6A FF               [12] 8274 	mov	r7,a
      003B6B BF FF 02         [24] 8275 	cjne	r7,#0xff,00368$
      003B6E 80 6D            [24] 8276 	sjmp	00130$
      003B70                       8277 00368$:
                                   8278 ;	..\src\COMMON\easyax5043.c:1444: if (axradio_framing_sourceaddrpos != 0xff)
      003B70 90 90 E4         [24] 8279 	mov	dptr,#_axradio_framing_sourceaddrpos
      003B73 E4               [12] 8280 	clr	a
      003B74 93               [24] 8281 	movc	a,@a+dptr
      003B75 FE               [12] 8282 	mov	r6,a
      003B76 BE FF 02         [24] 8283 	cjne	r6,#0xff,00369$
      003B79 80 32            [24] 8284 	sjmp	00127$
      003B7B                       8285 00369$:
                                   8286 ;	..\src\COMMON\easyax5043.c:1445: memcpy_xdata(&axradio_txbuffer[axradio_framing_destaddrpos], &axradio_cb_receive.st.rx.mac.remoteaddr, axradio_framing_addrlen);
      003B7B EF               [12] 8287 	mov	a,r7
      003B7C 24 10            [12] 8288 	add	a,#_axradio_txbuffer
      003B7E FD               [12] 8289 	mov	r5,a
      003B7F E4               [12] 8290 	clr	a
      003B80 34 01            [12] 8291 	addc	a,#(_axradio_txbuffer >> 8)
      003B82 FE               [12] 8292 	mov	r6,a
      003B83 7C 00            [12] 8293 	mov	r4,#0x00
      003B85 90 90 E2         [24] 8294 	mov	dptr,#_axradio_framing_addrlen
      003B88 E4               [12] 8295 	clr	a
      003B89 93               [24] 8296 	movc	a,@a+dptr
      003B8A FB               [12] 8297 	mov	r3,a
      003B8B 7A 00            [12] 8298 	mov	r2,#0x00
      003B8D 90 04 6D         [24] 8299 	mov	dptr,#_memcpy_PARM_2
      003B90 74 2C            [12] 8300 	mov	a,#(_axradio_cb_receive + 0x0014)
      003B92 F0               [24] 8301 	movx	@dptr,a
      003B93 74 03            [12] 8302 	mov	a,#((_axradio_cb_receive + 0x0014) >> 8)
      003B95 A3               [24] 8303 	inc	dptr
      003B96 F0               [24] 8304 	movx	@dptr,a
      003B97 E4               [12] 8305 	clr	a
      003B98 A3               [24] 8306 	inc	dptr
      003B99 F0               [24] 8307 	movx	@dptr,a
      003B9A 90 04 70         [24] 8308 	mov	dptr,#_memcpy_PARM_3
      003B9D EB               [12] 8309 	mov	a,r3
      003B9E F0               [24] 8310 	movx	@dptr,a
      003B9F EA               [12] 8311 	mov	a,r2
      003BA0 A3               [24] 8312 	inc	dptr
      003BA1 F0               [24] 8313 	movx	@dptr,a
      003BA2 8D 82            [24] 8314 	mov	dpl,r5
      003BA4 8E 83            [24] 8315 	mov	dph,r6
      003BA6 8C F0            [24] 8316 	mov	b,r4
      003BA8 12 78 14         [24] 8317 	lcall	_memcpy
      003BAB 80 30            [24] 8318 	sjmp	00130$
      003BAD                       8319 00127$:
                                   8320 ;	..\src\COMMON\easyax5043.c:1447: memcpy_xdata(&axradio_txbuffer[axradio_framing_destaddrpos], &axradio_default_remoteaddr, axradio_framing_addrlen);
      003BAD EF               [12] 8321 	mov	a,r7
      003BAE 24 10            [12] 8322 	add	a,#_axradio_txbuffer
      003BB0 FF               [12] 8323 	mov	r7,a
      003BB1 E4               [12] 8324 	clr	a
      003BB2 34 01            [12] 8325 	addc	a,#(_axradio_txbuffer >> 8)
      003BB4 FE               [12] 8326 	mov	r6,a
      003BB5 7D 00            [12] 8327 	mov	r5,#0x00
      003BB7 90 90 E2         [24] 8328 	mov	dptr,#_axradio_framing_addrlen
      003BBA E4               [12] 8329 	clr	a
      003BBB 93               [24] 8330 	movc	a,@a+dptr
      003BBC FC               [12] 8331 	mov	r4,a
      003BBD 7B 00            [12] 8332 	mov	r3,#0x00
      003BBF 90 04 6D         [24] 8333 	mov	dptr,#_memcpy_PARM_2
      003BC2 74 0C            [12] 8334 	mov	a,#_axradio_default_remoteaddr
      003BC4 F0               [24] 8335 	movx	@dptr,a
      003BC5 74 01            [12] 8336 	mov	a,#(_axradio_default_remoteaddr >> 8)
      003BC7 A3               [24] 8337 	inc	dptr
      003BC8 F0               [24] 8338 	movx	@dptr,a
      003BC9 E4               [12] 8339 	clr	a
      003BCA A3               [24] 8340 	inc	dptr
      003BCB F0               [24] 8341 	movx	@dptr,a
      003BCC 90 04 70         [24] 8342 	mov	dptr,#_memcpy_PARM_3
      003BCF EC               [12] 8343 	mov	a,r4
      003BD0 F0               [24] 8344 	movx	@dptr,a
      003BD1 EB               [12] 8345 	mov	a,r3
      003BD2 A3               [24] 8346 	inc	dptr
      003BD3 F0               [24] 8347 	movx	@dptr,a
      003BD4 8F 82            [24] 8348 	mov	dpl,r7
      003BD6 8E 83            [24] 8349 	mov	dph,r6
      003BD8 8D F0            [24] 8350 	mov	b,r5
      003BDA 12 78 14         [24] 8351 	lcall	_memcpy
      003BDD                       8352 00130$:
                                   8353 ;	..\src\COMMON\easyax5043.c:1449: if (axradio_framing_sourceaddrpos != 0xff)
      003BDD 90 90 E4         [24] 8354 	mov	dptr,#_axradio_framing_sourceaddrpos
      003BE0 E4               [12] 8355 	clr	a
      003BE1 93               [24] 8356 	movc	a,@a+dptr
      003BE2 FF               [12] 8357 	mov	r7,a
      003BE3 BF FF 02         [24] 8358 	cjne	r7,#0xff,00370$
      003BE6 80 30            [24] 8359 	sjmp	00132$
      003BE8                       8360 00370$:
                                   8361 ;	..\src\COMMON\easyax5043.c:1450: memcpy_xdata(&axradio_txbuffer[axradio_framing_sourceaddrpos], &axradio_localaddr.addr, axradio_framing_addrlen);
      003BE8 EF               [12] 8362 	mov	a,r7
      003BE9 24 10            [12] 8363 	add	a,#_axradio_txbuffer
      003BEB FF               [12] 8364 	mov	r7,a
      003BEC E4               [12] 8365 	clr	a
      003BED 34 01            [12] 8366 	addc	a,#(_axradio_txbuffer >> 8)
      003BEF FE               [12] 8367 	mov	r6,a
      003BF0 7D 00            [12] 8368 	mov	r5,#0x00
      003BF2 90 90 E2         [24] 8369 	mov	dptr,#_axradio_framing_addrlen
      003BF5 E4               [12] 8370 	clr	a
      003BF6 93               [24] 8371 	movc	a,@a+dptr
      003BF7 FC               [12] 8372 	mov	r4,a
      003BF8 7B 00            [12] 8373 	mov	r3,#0x00
      003BFA 90 04 6D         [24] 8374 	mov	dptr,#_memcpy_PARM_2
      003BFD 74 04            [12] 8375 	mov	a,#_axradio_localaddr
      003BFF F0               [24] 8376 	movx	@dptr,a
      003C00 74 01            [12] 8377 	mov	a,#(_axradio_localaddr >> 8)
      003C02 A3               [24] 8378 	inc	dptr
      003C03 F0               [24] 8379 	movx	@dptr,a
      003C04 E4               [12] 8380 	clr	a
      003C05 A3               [24] 8381 	inc	dptr
      003C06 F0               [24] 8382 	movx	@dptr,a
      003C07 90 04 70         [24] 8383 	mov	dptr,#_memcpy_PARM_3
      003C0A EC               [12] 8384 	mov	a,r4
      003C0B F0               [24] 8385 	movx	@dptr,a
      003C0C EB               [12] 8386 	mov	a,r3
      003C0D A3               [24] 8387 	inc	dptr
      003C0E F0               [24] 8388 	movx	@dptr,a
      003C0F 8F 82            [24] 8389 	mov	dpl,r7
      003C11 8E 83            [24] 8390 	mov	dph,r6
      003C13 8D F0            [24] 8391 	mov	b,r5
      003C15 12 78 14         [24] 8392 	lcall	_memcpy
      003C18                       8393 00132$:
                                   8394 ;	..\src\COMMON\easyax5043.c:1451: if (axradio_framing_lenmask) {
      003C18 90 90 E7         [24] 8395 	mov	dptr,#_axradio_framing_lenmask
      003C1B E4               [12] 8396 	clr	a
      003C1C 93               [24] 8397 	movc	a,@a+dptr
      003C1D FF               [12] 8398 	mov	r7,a
      003C1E 60 30            [24] 8399 	jz	00134$
                                   8400 ;	..\src\COMMON\easyax5043.c:1452: uint8_t len_byte = (uint8_t)(axradio_txbuffer_len - axradio_framing_lenoffs) & axradio_framing_lenmask; // if you prefer not counting the len byte itself, set LENOFFS = 1
      003C20 90 00 EB         [24] 8401 	mov	dptr,#_axradio_txbuffer_len
      003C23 E0               [24] 8402 	movx	a,@dptr
      003C24 FD               [12] 8403 	mov	r5,a
      003C25 A3               [24] 8404 	inc	dptr
      003C26 E0               [24] 8405 	movx	a,@dptr
      003C27 90 90 E6         [24] 8406 	mov	dptr,#_axradio_framing_lenoffs
      003C2A E4               [12] 8407 	clr	a
      003C2B 93               [24] 8408 	movc	a,@a+dptr
      003C2C FE               [12] 8409 	mov	r6,a
      003C2D ED               [12] 8410 	mov	a,r5
      003C2E C3               [12] 8411 	clr	c
      003C2F 9E               [12] 8412 	subb	a,r6
      003C30 5F               [12] 8413 	anl	a,r7
      003C31 FE               [12] 8414 	mov	r6,a
                                   8415 ;	..\src\COMMON\easyax5043.c:1453: axradio_txbuffer[axradio_framing_lenpos] = (axradio_txbuffer[axradio_framing_lenpos] & (uint8_t)~axradio_framing_lenmask) | len_byte;
      003C32 90 90 E5         [24] 8416 	mov	dptr,#_axradio_framing_lenpos
      003C35 E4               [12] 8417 	clr	a
      003C36 93               [24] 8418 	movc	a,@a+dptr
      003C37 24 10            [12] 8419 	add	a,#_axradio_txbuffer
      003C39 FD               [12] 8420 	mov	r5,a
      003C3A E4               [12] 8421 	clr	a
      003C3B 34 01            [12] 8422 	addc	a,#(_axradio_txbuffer >> 8)
      003C3D FC               [12] 8423 	mov	r4,a
      003C3E 8D 82            [24] 8424 	mov	dpl,r5
      003C40 8C 83            [24] 8425 	mov	dph,r4
      003C42 E0               [24] 8426 	movx	a,@dptr
      003C43 FB               [12] 8427 	mov	r3,a
      003C44 EF               [12] 8428 	mov	a,r7
      003C45 F4               [12] 8429 	cpl	a
      003C46 FF               [12] 8430 	mov	r7,a
      003C47 5B               [12] 8431 	anl	a,r3
      003C48 42 06            [12] 8432 	orl	ar6,a
      003C4A 8D 82            [24] 8433 	mov	dpl,r5
      003C4C 8C 83            [24] 8434 	mov	dph,r4
      003C4E EE               [12] 8435 	mov	a,r6
      003C4F F0               [24] 8436 	movx	@dptr,a
      003C50                       8437 00134$:
                                   8438 ;	..\src\COMMON\easyax5043.c:1455: if (axradio_framing_swcrclen)
      003C50 90 90 E8         [24] 8439 	mov	dptr,#_axradio_framing_swcrclen
      003C53 E4               [12] 8440 	clr	a
      003C54 93               [24] 8441 	movc	a,@a+dptr
      003C55 60 20            [24] 8442 	jz	00136$
                                   8443 ;	..\src\COMMON\easyax5043.c:1456: axradio_txbuffer_len = axradio_framing_append_crc(axradio_txbuffer, axradio_txbuffer_len);
      003C57 90 00 EB         [24] 8444 	mov	dptr,#_axradio_txbuffer_len
      003C5A E0               [24] 8445 	movx	a,@dptr
      003C5B C0 E0            [24] 8446 	push	acc
      003C5D A3               [24] 8447 	inc	dptr
      003C5E E0               [24] 8448 	movx	a,@dptr
      003C5F C0 E0            [24] 8449 	push	acc
      003C61 90 01 10         [24] 8450 	mov	dptr,#_axradio_txbuffer
      003C64 12 1C C1         [24] 8451 	lcall	_axradio_framing_append_crc
      003C67 AE 82            [24] 8452 	mov	r6,dpl
      003C69 AF 83            [24] 8453 	mov	r7,dph
      003C6B 15 81            [12] 8454 	dec	sp
      003C6D 15 81            [12] 8455 	dec	sp
      003C6F 90 00 EB         [24] 8456 	mov	dptr,#_axradio_txbuffer_len
      003C72 EE               [12] 8457 	mov	a,r6
      003C73 F0               [24] 8458 	movx	@dptr,a
      003C74 EF               [12] 8459 	mov	a,r7
      003C75 A3               [24] 8460 	inc	dptr
      003C76 F0               [24] 8461 	movx	@dptr,a
      003C77                       8462 00136$:
                                   8463 ;	..\src\COMMON\easyax5043.c:1457: if (axradio_phy_pn9) {
      003C77 90 90 C0         [24] 8464 	mov	dptr,#_axradio_phy_pn9
      003C7A E4               [12] 8465 	clr	a
      003C7B 93               [24] 8466 	movc	a,@a+dptr
      003C7C 60 2F            [24] 8467 	jz	00138$
                                   8468 ;	..\src\COMMON\easyax5043.c:1458: pn9_buffer(axradio_txbuffer, axradio_txbuffer_len, 0x1ff, -(AX5043_ENCODING & 0x01));
      003C7E 90 40 11         [24] 8469 	mov	dptr,#_AX5043_ENCODING
      003C81 E0               [24] 8470 	movx	a,@dptr
      003C82 FF               [12] 8471 	mov	r7,a
      003C83 53 07 01         [24] 8472 	anl	ar7,#0x01
      003C86 C3               [12] 8473 	clr	c
      003C87 E4               [12] 8474 	clr	a
      003C88 9F               [12] 8475 	subb	a,r7
      003C89 FF               [12] 8476 	mov	r7,a
      003C8A C0 07            [24] 8477 	push	ar7
      003C8C 74 FF            [12] 8478 	mov	a,#0xff
      003C8E C0 E0            [24] 8479 	push	acc
      003C90 74 01            [12] 8480 	mov	a,#0x01
      003C92 C0 E0            [24] 8481 	push	acc
      003C94 90 00 EB         [24] 8482 	mov	dptr,#_axradio_txbuffer_len
      003C97 E0               [24] 8483 	movx	a,@dptr
      003C98 C0 E0            [24] 8484 	push	acc
      003C9A A3               [24] 8485 	inc	dptr
      003C9B E0               [24] 8486 	movx	a,@dptr
      003C9C C0 E0            [24] 8487 	push	acc
      003C9E 90 01 10         [24] 8488 	mov	dptr,#_axradio_txbuffer
      003CA1 75 F0 00         [24] 8489 	mov	b,#0x00
      003CA4 12 79 E1         [24] 8490 	lcall	_pn9_buffer
      003CA7 E5 81            [12] 8491 	mov	a,sp
      003CA9 24 FB            [12] 8492 	add	a,#0xfb
      003CAB F5 81            [12] 8493 	mov	sp,a
      003CAD                       8494 00138$:
                                   8495 ;	..\src\COMMON\easyax5043.c:1460: AX5043_IRQMASK1 = 0x00;
      003CAD 90 40 06         [24] 8496 	mov	dptr,#_AX5043_IRQMASK1
      003CB0 E4               [12] 8497 	clr	a
      003CB1 F0               [24] 8498 	movx	@dptr,a
                                   8499 ;	..\src\COMMON\easyax5043.c:1461: AX5043_IRQMASK0 = 0x00;
      003CB2 90 40 07         [24] 8500 	mov	dptr,#_AX5043_IRQMASK0
      003CB5 F0               [24] 8501 	movx	@dptr,a
                                   8502 ;	..\src\COMMON\easyax5043.c:1462: AX5043_PWRMODE = AX5043_PWRSTATE_XTAL_ON;
      003CB6 90 40 02         [24] 8503 	mov	dptr,#_AX5043_PWRMODE
      003CB9 74 05            [12] 8504 	mov	a,#0x05
      003CBB F0               [24] 8505 	movx	@dptr,a
                                   8506 ;	..\src\COMMON\easyax5043.c:1463: AX5043_FIFOSTAT = 3;
      003CBC 90 40 28         [24] 8507 	mov	dptr,#_AX5043_FIFOSTAT
      003CBF 74 03            [12] 8508 	mov	a,#0x03
      003CC1 F0               [24] 8509 	movx	@dptr,a
                                   8510 ;	..\src\COMMON\easyax5043.c:1464: axradio_trxstate = trxstate_tx_longpreamble; // ensure that trxstate != off, otherwise we would prematurely enable the receiver, see below
      003CC2 75 18 0A         [24] 8511 	mov	_axradio_trxstate,#0x0a
                                   8512 ;	..\src\COMMON\easyax5043.c:1465: while (AX5043_POWSTAT & 0x08);
      003CC5                       8513 00139$:
      003CC5 90 40 03         [24] 8514 	mov	dptr,#_AX5043_POWSTAT
      003CC8 E0               [24] 8515 	movx	a,@dptr
      003CC9 20 E3 F9         [24] 8516 	jb	acc.3,00139$
                                   8517 ;	..\src\COMMON\easyax5043.c:1466: wtimer_remove(&axradio_timer);
      003CCC 90 03 6F         [24] 8518 	mov	dptr,#_axradio_timer
      003CCF 12 85 39         [24] 8519 	lcall	_wtimer_remove
                                   8520 ;	..\src\COMMON\easyax5043.c:1467: axradio_timer.time = axradio_framing_ack_delay;
      003CD2 90 90 F4         [24] 8521 	mov	dptr,#_axradio_framing_ack_delay
      003CD5 E4               [12] 8522 	clr	a
      003CD6 93               [24] 8523 	movc	a,@a+dptr
      003CD7 FC               [12] 8524 	mov	r4,a
      003CD8 74 01            [12] 8525 	mov	a,#0x01
      003CDA 93               [24] 8526 	movc	a,@a+dptr
      003CDB FD               [12] 8527 	mov	r5,a
      003CDC 74 02            [12] 8528 	mov	a,#0x02
      003CDE 93               [24] 8529 	movc	a,@a+dptr
      003CDF FE               [12] 8530 	mov	r6,a
      003CE0 74 03            [12] 8531 	mov	a,#0x03
      003CE2 93               [24] 8532 	movc	a,@a+dptr
      003CE3 FF               [12] 8533 	mov	r7,a
      003CE4 90 03 73         [24] 8534 	mov	dptr,#(_axradio_timer + 0x0004)
      003CE7 EC               [12] 8535 	mov	a,r4
      003CE8 F0               [24] 8536 	movx	@dptr,a
      003CE9 ED               [12] 8537 	mov	a,r5
      003CEA A3               [24] 8538 	inc	dptr
      003CEB F0               [24] 8539 	movx	@dptr,a
      003CEC EE               [12] 8540 	mov	a,r6
      003CED A3               [24] 8541 	inc	dptr
      003CEE F0               [24] 8542 	movx	@dptr,a
      003CEF EF               [12] 8543 	mov	a,r7
      003CF0 A3               [24] 8544 	inc	dptr
      003CF1 F0               [24] 8545 	movx	@dptr,a
                                   8546 ;	..\src\COMMON\easyax5043.c:1468: wtimer1_addrelative(&axradio_timer);
      003CF2 90 03 6F         [24] 8547 	mov	dptr,#_axradio_timer
      003CF5 12 78 BF         [24] 8548 	lcall	_wtimer1_addrelative
      003CF8                       8549 00143$:
                                   8550 ;	..\src\COMMON\easyax5043.c:1470: if (axradio_mode == AXRADIO_MODE_SYNC_SLAVE ||
      003CF8 74 32            [12] 8551 	mov	a,#0x32
      003CFA B5 17 02         [24] 8552 	cjne	a,_axradio_mode,00375$
      003CFD 80 0A            [24] 8553 	sjmp	00156$
      003CFF                       8554 00375$:
                                   8555 ;	..\src\COMMON\easyax5043.c:1471: axradio_mode == AXRADIO_MODE_SYNC_ACK_SLAVE) {
      003CFF 74 33            [12] 8556 	mov	a,#0x33
      003D01 B5 17 02         [24] 8557 	cjne	a,_axradio_mode,00376$
      003D04 80 03            [24] 8558 	sjmp	00377$
      003D06                       8559 00376$:
      003D06 02 3D DB         [24] 8560 	ljmp	00157$
      003D09                       8561 00377$:
      003D09                       8562 00156$:
                                   8563 ;	..\src\COMMON\easyax5043.c:1472: if (axradio_mode != AXRADIO_MODE_SYNC_ACK_SLAVE)
      003D09 74 33            [12] 8564 	mov	a,#0x33
      003D0B B5 17 02         [24] 8565 	cjne	a,_axradio_mode,00378$
      003D0E 80 03            [24] 8566 	sjmp	00147$
      003D10                       8567 00378$:
                                   8568 ;	..\src\COMMON\easyax5043.c:1473: ax5043_off();
      003D10 12 2C F0         [24] 8569 	lcall	_ax5043_off
      003D13                       8570 00147$:
                                   8571 ;	..\src\COMMON\easyax5043.c:1474: switch (axradio_syncstate) {
      003D13 90 00 EA         [24] 8572 	mov	dptr,#_axradio_syncstate
      003D16 E0               [24] 8573 	movx	a,@dptr
      003D17 FF               [12] 8574 	mov	r7,a
      003D18 BF 08 02         [24] 8575 	cjne	r7,#0x08,00379$
      003D1B 80 45            [24] 8576 	sjmp	00151$
      003D1D                       8577 00379$:
      003D1D BF 0A 02         [24] 8578 	cjne	r7,#0x0a,00380$
      003D20 80 40            [24] 8579 	sjmp	00151$
      003D22                       8580 00380$:
      003D22 BF 0B 02         [24] 8581 	cjne	r7,#0x0b,00381$
      003D25 80 3B            [24] 8582 	sjmp	00151$
      003D27                       8583 00381$:
                                   8584 ;	..\src\COMMON\easyax5043.c:1478: axradio_sync_time = axradio_conv_time_totimer0(axradio_cb_receive.st.time.t);
      003D27 90 03 1E         [24] 8585 	mov	dptr,#(_axradio_cb_receive + 0x0006)
      003D2A E0               [24] 8586 	movx	a,@dptr
      003D2B FC               [12] 8587 	mov	r4,a
      003D2C A3               [24] 8588 	inc	dptr
      003D2D E0               [24] 8589 	movx	a,@dptr
      003D2E FD               [12] 8590 	mov	r5,a
      003D2F A3               [24] 8591 	inc	dptr
      003D30 E0               [24] 8592 	movx	a,@dptr
      003D31 FE               [12] 8593 	mov	r6,a
      003D32 A3               [24] 8594 	inc	dptr
      003D33 E0               [24] 8595 	movx	a,@dptr
      003D34 8C 82            [24] 8596 	mov	dpl,r4
      003D36 8D 83            [24] 8597 	mov	dph,r5
      003D38 8E F0            [24] 8598 	mov	b,r6
      003D3A 12 1E B7         [24] 8599 	lcall	_axradio_conv_time_totimer0
      003D3D AC 82            [24] 8600 	mov	r4,dpl
      003D3F AD 83            [24] 8601 	mov	r5,dph
      003D41 AE F0            [24] 8602 	mov	r6,b
      003D43 FF               [12] 8603 	mov	r7,a
      003D44 90 00 F6         [24] 8604 	mov	dptr,#_axradio_sync_time
      003D47 EC               [12] 8605 	mov	a,r4
      003D48 F0               [24] 8606 	movx	@dptr,a
      003D49 ED               [12] 8607 	mov	a,r5
      003D4A A3               [24] 8608 	inc	dptr
      003D4B F0               [24] 8609 	movx	@dptr,a
      003D4C EE               [12] 8610 	mov	a,r6
      003D4D A3               [24] 8611 	inc	dptr
      003D4E F0               [24] 8612 	movx	@dptr,a
      003D4F EF               [12] 8613 	mov	a,r7
      003D50 A3               [24] 8614 	inc	dptr
      003D51 F0               [24] 8615 	movx	@dptr,a
                                   8616 ;	..\src\COMMON\easyax5043.c:1479: axradio_sync_periodcorr = -32768;
      003D52 90 00 FA         [24] 8617 	mov	dptr,#_axradio_sync_periodcorr
      003D55 E4               [12] 8618 	clr	a
      003D56 F0               [24] 8619 	movx	@dptr,a
      003D57 74 80            [12] 8620 	mov	a,#0x80
      003D59 A3               [24] 8621 	inc	dptr
      003D5A F0               [24] 8622 	movx	@dptr,a
                                   8623 ;	..\src\COMMON\easyax5043.c:1480: axradio_sync_seqnr = 0;
      003D5B 90 00 F5         [24] 8624 	mov	dptr,#_axradio_ack_seqnr
      003D5E E4               [12] 8625 	clr	a
      003D5F F0               [24] 8626 	movx	@dptr,a
                                   8627 ;	..\src\COMMON\easyax5043.c:1481: break;
                                   8628 ;	..\src\COMMON\easyax5043.c:1485: case syncstate_slave_rxpacket:
      003D60 80 2C            [24] 8629 	sjmp	00152$
      003D62                       8630 00151$:
                                   8631 ;	..\src\COMMON\easyax5043.c:1486: axradio_sync_adjustperiodcorr();
      003D62 12 2F 23         [24] 8632 	lcall	_axradio_sync_adjustperiodcorr
                                   8633 ;	..\src\COMMON\easyax5043.c:1487: axradio_cb_receive.st.rx.phy.period = axradio_sync_periodcorr >> SYNC_K1;
      003D65 90 00 FA         [24] 8634 	mov	dptr,#_axradio_sync_periodcorr
      003D68 E0               [24] 8635 	movx	a,@dptr
      003D69 FE               [12] 8636 	mov	r6,a
      003D6A A3               [24] 8637 	inc	dptr
      003D6B E0               [24] 8638 	movx	a,@dptr
      003D6C C4               [12] 8639 	swap	a
      003D6D 03               [12] 8640 	rr	a
      003D6E CE               [12] 8641 	xch	a,r6
      003D6F C4               [12] 8642 	swap	a
      003D70 03               [12] 8643 	rr	a
      003D71 54 07            [12] 8644 	anl	a,#0x07
      003D73 6E               [12] 8645 	xrl	a,r6
      003D74 CE               [12] 8646 	xch	a,r6
      003D75 54 07            [12] 8647 	anl	a,#0x07
      003D77 CE               [12] 8648 	xch	a,r6
      003D78 6E               [12] 8649 	xrl	a,r6
      003D79 CE               [12] 8650 	xch	a,r6
      003D7A 30 E2 02         [24] 8651 	jnb	acc.2,00382$
      003D7D 44 F8            [12] 8652 	orl	a,#0xf8
      003D7F                       8653 00382$:
      003D7F FF               [12] 8654 	mov	r7,a
      003D80 90 03 2A         [24] 8655 	mov	dptr,#(_axradio_cb_receive + 0x0012)
      003D83 EE               [12] 8656 	mov	a,r6
      003D84 F0               [24] 8657 	movx	@dptr,a
      003D85 EF               [12] 8658 	mov	a,r7
      003D86 A3               [24] 8659 	inc	dptr
      003D87 F0               [24] 8660 	movx	@dptr,a
                                   8661 ;	..\src\COMMON\easyax5043.c:1488: axradio_sync_seqnr = 1;
      003D88 90 00 F5         [24] 8662 	mov	dptr,#_axradio_ack_seqnr
      003D8B 74 01            [12] 8663 	mov	a,#0x01
      003D8D F0               [24] 8664 	movx	@dptr,a
                                   8665 ;	..\src\COMMON\easyax5043.c:1490: };
      003D8E                       8666 00152$:
                                   8667 ;	..\src\COMMON\easyax5043.c:1491: axradio_sync_slave_nextperiod();
      003D8E 12 30 41         [24] 8668 	lcall	_axradio_sync_slave_nextperiod
                                   8669 ;	..\src\COMMON\easyax5043.c:1492: if (axradio_mode != AXRADIO_MODE_SYNC_ACK_SLAVE) {
      003D91 74 33            [12] 8670 	mov	a,#0x33
      003D93 B5 17 02         [24] 8671 	cjne	a,_axradio_mode,00383$
      003D96 80 3D            [24] 8672 	sjmp	00154$
      003D98                       8673 00383$:
                                   8674 ;	..\src\COMMON\easyax5043.c:1493: axradio_syncstate = syncstate_slave_rxidle;
      003D98 90 00 EA         [24] 8675 	mov	dptr,#_axradio_syncstate
      003D9B 74 08            [12] 8676 	mov	a,#0x08
      003D9D F0               [24] 8677 	movx	@dptr,a
                                   8678 ;	..\src\COMMON\easyax5043.c:1494: wtimer_remove(&axradio_timer);
      003D9E 90 03 6F         [24] 8679 	mov	dptr,#_axradio_timer
      003DA1 12 85 39         [24] 8680 	lcall	_wtimer_remove
                                   8681 ;	..\src\COMMON\easyax5043.c:1495: axradio_sync_settimeradv(axradio_sync_slave_rxadvance[axradio_sync_seqnr]);
      003DA4 90 00 F5         [24] 8682 	mov	dptr,#_axradio_ack_seqnr
      003DA7 E0               [24] 8683 	movx	a,@dptr
      003DA8 75 F0 04         [24] 8684 	mov	b,#0x04
      003DAB A4               [48] 8685 	mul	ab
      003DAC 24 15            [12] 8686 	add	a,#_axradio_sync_slave_rxadvance
      003DAE F5 82            [12] 8687 	mov	dpl,a
      003DB0 74 91            [12] 8688 	mov	a,#(_axradio_sync_slave_rxadvance >> 8)
      003DB2 35 F0            [12] 8689 	addc	a,b
      003DB4 F5 83            [12] 8690 	mov	dph,a
      003DB6 E4               [12] 8691 	clr	a
      003DB7 93               [24] 8692 	movc	a,@a+dptr
      003DB8 FC               [12] 8693 	mov	r4,a
      003DB9 A3               [24] 8694 	inc	dptr
      003DBA E4               [12] 8695 	clr	a
      003DBB 93               [24] 8696 	movc	a,@a+dptr
      003DBC FD               [12] 8697 	mov	r5,a
      003DBD A3               [24] 8698 	inc	dptr
      003DBE E4               [12] 8699 	clr	a
      003DBF 93               [24] 8700 	movc	a,@a+dptr
      003DC0 FE               [12] 8701 	mov	r6,a
      003DC1 A3               [24] 8702 	inc	dptr
      003DC2 E4               [12] 8703 	clr	a
      003DC3 93               [24] 8704 	movc	a,@a+dptr
      003DC4 8C 82            [24] 8705 	mov	dpl,r4
      003DC6 8D 83            [24] 8706 	mov	dph,r5
      003DC8 8E F0            [24] 8707 	mov	b,r6
      003DCA 12 2E E4         [24] 8708 	lcall	_axradio_sync_settimeradv
                                   8709 ;	..\src\COMMON\easyax5043.c:1496: wtimer0_addabsolute(&axradio_timer);
      003DCD 90 03 6F         [24] 8710 	mov	dptr,#_axradio_timer
      003DD0 12 79 8E         [24] 8711 	lcall	_wtimer0_addabsolute
      003DD3 80 06            [24] 8712 	sjmp	00157$
      003DD5                       8713 00154$:
                                   8714 ;	..\src\COMMON\easyax5043.c:1498: axradio_syncstate = syncstate_slave_rxack;
      003DD5 90 00 EA         [24] 8715 	mov	dptr,#_axradio_syncstate
      003DD8 74 0C            [12] 8716 	mov	a,#0x0c
      003DDA F0               [24] 8717 	movx	@dptr,a
      003DDB                       8718 00157$:
                                   8719 ;	..\src\COMMON\easyax5043.c:1501: axradio_statuschange((struct axradio_status __xdata *)&axradio_cb_receive.st);
      003DDB 90 03 1C         [24] 8720 	mov	dptr,#(_axradio_cb_receive + 0x0004)
      003DDE 12 04 3C         [24] 8721 	lcall	_axradio_statuschange
                                   8722 ;	..\src\COMMON\easyax5043.c:1502: endcb:
      003DE1                       8723 00159$:
                                   8724 ;	..\src\COMMON\easyax5043.c:1503: if (axradio_mode == AXRADIO_MODE_WOR_RECEIVE) {
      003DE1 74 21            [12] 8725 	mov	a,#0x21
      003DE3 B5 17 03         [24] 8726 	cjne	a,_axradio_mode,00174$
                                   8727 ;	..\src\COMMON\easyax5043.c:1504: ax5043_receiver_on_wor();
      003DE6 02 2C 01         [24] 8728 	ljmp	_ax5043_receiver_on_wor
      003DE9                       8729 00174$:
                                   8730 ;	..\src\COMMON\easyax5043.c:1505: } else if (axradio_mode == AXRADIO_MODE_ACK_RECEIVE ||
      003DE9 74 22            [12] 8731 	mov	a,#0x22
      003DEB B5 17 02         [24] 8732 	cjne	a,_axradio_mode,00386$
      003DEE 80 05            [24] 8733 	sjmp	00169$
      003DF0                       8734 00386$:
                                   8735 ;	..\src\COMMON\easyax5043.c:1506: axradio_mode == AXRADIO_MODE_WOR_ACK_RECEIVE) {
      003DF0 74 23            [12] 8736 	mov	a,#0x23
      003DF2 B5 17 20         [24] 8737 	cjne	a,_axradio_mode,00170$
      003DF5                       8738 00169$:
                                   8739 ;	..\src\COMMON\easyax5043.c:1509: uint8_t __autodata iesave = IE & 0x80;
      003DF5 E5 A8            [12] 8740 	mov	a,_IE
      003DF7 54 80            [12] 8741 	anl	a,#0x80
      003DF9 FF               [12] 8742 	mov	r7,a
                                   8743 ;	..\src\COMMON\easyax5043.c:1510: EA = 0;
                                   8744 ;	assignBit
      003DFA C2 AF            [12] 8745 	clr	_EA
                                   8746 ;	..\src\COMMON\easyax5043.c:1511: trxst = axradio_trxstate;
      003DFC AE 18            [24] 8747 	mov	r6,_axradio_trxstate
                                   8748 ;	..\src\COMMON\easyax5043.c:1512: axradio_cb_receive.st.error = AXRADIO_ERR_PACKETDONE;
      003DFE 90 03 1D         [24] 8749 	mov	dptr,#(_axradio_cb_receive + 0x0005)
      003E01 74 F0            [12] 8750 	mov	a,#0xf0
      003E03 F0               [24] 8751 	movx	@dptr,a
                                   8752 ;	..\src\COMMON\easyax5043.c:1513: IE |= iesave;
      003E04 EF               [12] 8753 	mov	a,r7
      003E05 42 A8            [12] 8754 	orl	_IE,a
                                   8755 ;	..\src\COMMON\easyax5043.c:1515: if (trxst == trxstate_off) {
      003E07 EE               [12] 8756 	mov	a,r6
      003E08 70 1D            [24] 8757 	jnz	00176$
                                   8758 ;	..\src\COMMON\easyax5043.c:1516: if (axradio_mode == AXRADIO_MODE_WOR_ACK_RECEIVE)
      003E0A 74 23            [12] 8759 	mov	a,#0x23
      003E0C B5 17 03         [24] 8760 	cjne	a,_axradio_mode,00161$
                                   8761 ;	..\src\COMMON\easyax5043.c:1517: ax5043_receiver_on_wor();
      003E0F 02 2C 01         [24] 8762 	ljmp	_ax5043_receiver_on_wor
      003E12                       8763 00161$:
                                   8764 ;	..\src\COMMON\easyax5043.c:1519: ax5043_receiver_on_continuous();
      003E12 02 2B 9B         [24] 8765 	ljmp	_ax5043_receiver_on_continuous
      003E15                       8766 00170$:
                                   8767 ;	..\src\COMMON\easyax5043.c:1522: switch (axradio_trxstate) {
      003E15 AF 18            [24] 8768 	mov	r7,_axradio_trxstate
      003E17 BF 01 02         [24] 8769 	cjne	r7,#0x01,00392$
      003E1A 80 03            [24] 8770 	sjmp	00166$
      003E1C                       8771 00392$:
      003E1C BF 02 08         [24] 8772 	cjne	r7,#0x02,00176$
                                   8773 ;	..\src\COMMON\easyax5043.c:1524: case trxstate_rxwor:
      003E1F                       8774 00166$:
                                   8775 ;	..\src\COMMON\easyax5043.c:1525: AX5043_IRQMASK0 |= 0x01; // re-enable FIFO not empty irq
      003E1F 90 40 07         [24] 8776 	mov	dptr,#_AX5043_IRQMASK0
      003E22 E0               [24] 8777 	movx	a,@dptr
      003E23 43 E0 01         [24] 8778 	orl	acc,#0x01
      003E26 F0               [24] 8779 	movx	@dptr,a
                                   8780 ;	..\src\COMMON\easyax5043.c:1530: }
      003E27                       8781 00176$:
                                   8782 ;	..\src\COMMON\easyax5043.c:1532: }
      003E27 22               [24] 8783 	ret
                                   8784 ;------------------------------------------------------------
                                   8785 ;Allocation info for local variables in function 'axradio_killallcb'
                                   8786 ;------------------------------------------------------------
                                   8787 ;	..\src\COMMON\easyax5043.c:1534: static void axradio_killallcb(void)
                                   8788 ;	-----------------------------------------
                                   8789 ;	 function axradio_killallcb
                                   8790 ;	-----------------------------------------
      003E28                       8791 _axradio_killallcb:
                                   8792 ;	..\src\COMMON\easyax5043.c:1536: wtimer_remove_callback(&axradio_cb_receive.cb);
      003E28 90 03 18         [24] 8793 	mov	dptr,#_axradio_cb_receive
      003E2B 12 87 AB         [24] 8794 	lcall	_wtimer_remove_callback
                                   8795 ;	..\src\COMMON\easyax5043.c:1537: wtimer_remove_callback(&axradio_cb_receivesfd.cb);
      003E2E 90 03 3A         [24] 8796 	mov	dptr,#_axradio_cb_receivesfd
      003E31 12 87 AB         [24] 8797 	lcall	_wtimer_remove_callback
                                   8798 ;	..\src\COMMON\easyax5043.c:1538: wtimer_remove_callback(&axradio_cb_channelstate.cb);
      003E34 90 03 44         [24] 8799 	mov	dptr,#_axradio_cb_channelstate
      003E37 12 87 AB         [24] 8800 	lcall	_wtimer_remove_callback
                                   8801 ;	..\src\COMMON\easyax5043.c:1539: wtimer_remove_callback(&axradio_cb_transmitstart.cb);
      003E3A 90 03 51         [24] 8802 	mov	dptr,#_axradio_cb_transmitstart
      003E3D 12 87 AB         [24] 8803 	lcall	_wtimer_remove_callback
                                   8804 ;	..\src\COMMON\easyax5043.c:1540: wtimer_remove_callback(&axradio_cb_transmitend.cb);
      003E40 90 03 5B         [24] 8805 	mov	dptr,#_axradio_cb_transmitend
      003E43 12 87 AB         [24] 8806 	lcall	_wtimer_remove_callback
                                   8807 ;	..\src\COMMON\easyax5043.c:1541: wtimer_remove_callback(&axradio_cb_transmitdata.cb);
      003E46 90 03 65         [24] 8808 	mov	dptr,#_axradio_cb_transmitdata
      003E49 12 87 AB         [24] 8809 	lcall	_wtimer_remove_callback
                                   8810 ;	..\src\COMMON\easyax5043.c:1542: wtimer_remove(&axradio_timer);
      003E4C 90 03 6F         [24] 8811 	mov	dptr,#_axradio_timer
                                   8812 ;	..\src\COMMON\easyax5043.c:1543: }
      003E4F 02 85 39         [24] 8813 	ljmp	_wtimer_remove
                                   8814 ;------------------------------------------------------------
                                   8815 ;Allocation info for local variables in function 'axradio_tunevoltage'
                                   8816 ;------------------------------------------------------------
                                   8817 ;x                         Allocated with name '_axradio_tunevoltage_x_196608_398'
                                   8818 ;r                         Allocated to registers r6 r7 
                                   8819 ;cnt                       Allocated to registers r5 
                                   8820 ;------------------------------------------------------------
                                   8821 ;	..\src\COMMON\easyax5043.c:1569: static int16_t axradio_tunevoltage(void)
                                   8822 ;	-----------------------------------------
                                   8823 ;	 function axradio_tunevoltage
                                   8824 ;	-----------------------------------------
      003E52                       8825 _axradio_tunevoltage:
                                   8826 ;	..\src\COMMON\easyax5043.c:1571: int16_t __autodata r = 0;
      003E52 7E 00            [12] 8827 	mov	r6,#0x00
      003E54 7F 00            [12] 8828 	mov	r7,#0x00
                                   8829 ;	..\src\COMMON\easyax5043.c:1573: do {
      003E56 7D 40            [12] 8830 	mov	r5,#0x40
      003E58                       8831 00103$:
                                   8832 ;	..\src\COMMON\easyax5043.c:1574: AX5043_GPADCCTRL = 0x84;
      003E58 90 43 00         [24] 8833 	mov	dptr,#_AX5043_GPADCCTRL
      003E5B 74 84            [12] 8834 	mov	a,#0x84
      003E5D F0               [24] 8835 	movx	@dptr,a
                                   8836 ;	..\src\COMMON\easyax5043.c:1575: do {} while (AX5043_GPADCCTRL & 0x80);
      003E5E                       8837 00101$:
      003E5E 90 43 00         [24] 8838 	mov	dptr,#_AX5043_GPADCCTRL
      003E61 E0               [24] 8839 	movx	a,@dptr
      003E62 20 E7 F9         [24] 8840 	jb	acc.7,00101$
                                   8841 ;	..\src\COMMON\easyax5043.c:1576: } while (--cnt);
      003E65 DD F1            [24] 8842 	djnz	r5,00103$
                                   8843 ;	..\src\COMMON\easyax5043.c:1578: do {
      003E67 7D 20            [12] 8844 	mov	r5,#0x20
      003E69                       8845 00108$:
                                   8846 ;	..\src\COMMON\easyax5043.c:1579: AX5043_GPADCCTRL = 0x84;
      003E69 90 43 00         [24] 8847 	mov	dptr,#_AX5043_GPADCCTRL
      003E6C 74 84            [12] 8848 	mov	a,#0x84
      003E6E F0               [24] 8849 	movx	@dptr,a
                                   8850 ;	..\src\COMMON\easyax5043.c:1580: do {} while (AX5043_GPADCCTRL & 0x80);
      003E6F                       8851 00106$:
      003E6F 90 43 00         [24] 8852 	mov	dptr,#_AX5043_GPADCCTRL
      003E72 E0               [24] 8853 	movx	a,@dptr
      003E73 20 E7 F9         [24] 8854 	jb	acc.7,00106$
                                   8855 ;	..\src\COMMON\easyax5043.c:1582: int16_t x = AX5043_GPADC13VALUE1 & 0x03;
      003E76 90 43 08         [24] 8856 	mov	dptr,#_AX5043_GPADC13VALUE1
      003E79 E0               [24] 8857 	movx	a,@dptr
      003E7A FC               [12] 8858 	mov	r4,a
      003E7B 53 04 03         [24] 8859 	anl	ar4,#0x03
                                   8860 ;	..\src\COMMON\easyax5043.c:1583: x <<= 8;
      003E7E 8C 03            [24] 8861 	mov	ar3,r4
      003E80 7C 00            [12] 8862 	mov	r4,#0x00
                                   8863 ;	..\src\COMMON\easyax5043.c:1584: x |= AX5043_GPADC13VALUE0;
      003E82 90 43 09         [24] 8864 	mov	dptr,#_AX5043_GPADC13VALUE0
      003E85 E0               [24] 8865 	movx	a,@dptr
      003E86 F9               [12] 8866 	mov	r1,a
      003E87 7A 00            [12] 8867 	mov	r2,#0x00
      003E89 E9               [12] 8868 	mov	a,r1
      003E8A 42 04            [12] 8869 	orl	ar4,a
      003E8C EA               [12] 8870 	mov	a,r2
      003E8D 42 03            [12] 8871 	orl	ar3,a
                                   8872 ;	..\src\COMMON\easyax5043.c:1585: r += x;
      003E8F EC               [12] 8873 	mov	a,r4
      003E90 2E               [12] 8874 	add	a,r6
      003E91 FE               [12] 8875 	mov	r6,a
      003E92 EB               [12] 8876 	mov	a,r3
      003E93 3F               [12] 8877 	addc	a,r7
      003E94 FF               [12] 8878 	mov	r7,a
                                   8879 ;	..\src\COMMON\easyax5043.c:1587: } while (--cnt);
      003E95 DD D2            [24] 8880 	djnz	r5,00108$
                                   8881 ;	..\src\COMMON\easyax5043.c:1588: return r;
      003E97 8E 82            [24] 8882 	mov	dpl,r6
      003E99 8F 83            [24] 8883 	mov	dph,r7
                                   8884 ;	..\src\COMMON\easyax5043.c:1589: }
      003E9B 22               [24] 8885 	ret
                                   8886 ;------------------------------------------------------------
                                   8887 ;Allocation info for local variables in function 'axradio_adjustvcoi'
                                   8888 ;------------------------------------------------------------
                                   8889 ;rng                       Allocated to registers r7 
                                   8890 ;offs                      Allocated to registers r3 
                                   8891 ;bestrng                   Allocated to registers r4 
                                   8892 ;bestval                   Allocated to registers r5 r6 
                                   8893 ;val                       Allocated to stack - _bp +3
                                   8894 ;sloc0                     Allocated to stack - _bp +1
                                   8895 ;------------------------------------------------------------
                                   8896 ;	..\src\COMMON\easyax5043.c:1593: static __reentrantb uint8_t axradio_adjustvcoi(uint8_t rng) __reentrant
                                   8897 ;	-----------------------------------------
                                   8898 ;	 function axradio_adjustvcoi
                                   8899 ;	-----------------------------------------
      003E9C                       8900 _axradio_adjustvcoi:
      003E9C C0 1F            [24] 8901 	push	_bp
      003E9E E5 81            [12] 8902 	mov	a,sp
      003EA0 F5 1F            [12] 8903 	mov	_bp,a
      003EA2 24 04            [12] 8904 	add	a,#0x04
      003EA4 F5 81            [12] 8905 	mov	sp,a
      003EA6 AF 82            [24] 8906 	mov	r7,dpl
                                   8907 ;	..\src\COMMON\easyax5043.c:1597: uint16_t bestval = ~0;
      003EA8 7D FF            [12] 8908 	mov	r5,#0xff
      003EAA 7E FF            [12] 8909 	mov	r6,#0xff
                                   8910 ;	..\src\COMMON\easyax5043.c:1598: rng &= 0x7F;
      003EAC 53 07 7F         [24] 8911 	anl	ar7,#0x7f
                                   8912 ;	..\src\COMMON\easyax5043.c:1599: bestrng = rng;
      003EAF 8F 04            [24] 8913 	mov	ar4,r7
                                   8914 ;	..\src\COMMON\easyax5043.c:1600: for (offs = 0; offs != 16; ++offs) {
      003EB1 7B 00            [12] 8915 	mov	r3,#0x00
      003EB3                       8916 00115$:
                                   8917 ;	..\src\COMMON\easyax5043.c:1602: if (!((uint8_t)(rng + offs) & 0xC0)) {
      003EB3 EB               [12] 8918 	mov	a,r3
      003EB4 2F               [12] 8919 	add	a,r7
      003EB5 54 C0            [12] 8920 	anl	a,#0xc0
      003EB7 60 02            [24] 8921 	jz	00160$
      003EB9 80 3E            [24] 8922 	sjmp	00104$
      003EBB                       8923 00160$:
                                   8924 ;	..\src\COMMON\easyax5043.c:1603: AX5043_PLLVCOI = 0x80 | (rng + offs);
      003EBB EB               [12] 8925 	mov	a,r3
      003EBC 2F               [12] 8926 	add	a,r7
      003EBD 90 41 80         [24] 8927 	mov	dptr,#_AX5043_PLLVCOI
      003EC0 44 80            [12] 8928 	orl	a,#0x80
      003EC2 F0               [24] 8929 	movx	@dptr,a
                                   8930 ;	..\src\COMMON\easyax5043.c:1604: val = axradio_tunevoltage();
      003EC3 C0 07            [24] 8931 	push	ar7
      003EC5 C0 06            [24] 8932 	push	ar6
      003EC7 C0 05            [24] 8933 	push	ar5
      003EC9 C0 04            [24] 8934 	push	ar4
      003ECB C0 03            [24] 8935 	push	ar3
      003ECD 12 3E 52         [24] 8936 	lcall	_axradio_tunevoltage
      003ED0 A8 1F            [24] 8937 	mov	r0,_bp
      003ED2 08               [12] 8938 	inc	r0
      003ED3 A6 82            [24] 8939 	mov	@r0,dpl
      003ED5 08               [12] 8940 	inc	r0
      003ED6 A6 83            [24] 8941 	mov	@r0,dph
      003ED8 D0 03            [24] 8942 	pop	ar3
      003EDA D0 04            [24] 8943 	pop	ar4
      003EDC D0 05            [24] 8944 	pop	ar5
      003EDE D0 06            [24] 8945 	pop	ar6
      003EE0 D0 07            [24] 8946 	pop	ar7
                                   8947 ;	..\src\COMMON\easyax5043.c:1605: if (val < bestval) {
      003EE2 A8 1F            [24] 8948 	mov	r0,_bp
      003EE4 08               [12] 8949 	inc	r0
      003EE5 C3               [12] 8950 	clr	c
      003EE6 E6               [12] 8951 	mov	a,@r0
      003EE7 9D               [12] 8952 	subb	a,r5
      003EE8 08               [12] 8953 	inc	r0
      003EE9 E6               [12] 8954 	mov	a,@r0
      003EEA 9E               [12] 8955 	subb	a,r6
      003EEB 50 0C            [24] 8956 	jnc	00104$
                                   8957 ;	..\src\COMMON\easyax5043.c:1606: bestval = val;
      003EED A8 1F            [24] 8958 	mov	r0,_bp
      003EEF 08               [12] 8959 	inc	r0
      003EF0 86 05            [24] 8960 	mov	ar5,@r0
      003EF2 08               [12] 8961 	inc	r0
      003EF3 86 06            [24] 8962 	mov	ar6,@r0
                                   8963 ;	..\src\COMMON\easyax5043.c:1607: bestrng = rng + offs;
      003EF5 EB               [12] 8964 	mov	a,r3
      003EF6 2F               [12] 8965 	add	a,r7
      003EF7 FA               [12] 8966 	mov	r2,a
      003EF8 FC               [12] 8967 	mov	r4,a
      003EF9                       8968 00104$:
                                   8969 ;	..\src\COMMON\easyax5043.c:1610: if (!offs)
      003EF9 EB               [12] 8970 	mov	a,r3
      003EFA 60 53            [24] 8971 	jz	00111$
                                   8972 ;	..\src\COMMON\easyax5043.c:1612: if (!((uint8_t)(rng - offs) & 0xC0)) {
      003EFC EF               [12] 8973 	mov	a,r7
      003EFD C3               [12] 8974 	clr	c
      003EFE 9B               [12] 8975 	subb	a,r3
      003EFF 54 C0            [12] 8976 	anl	a,#0xc0
      003F01 60 02            [24] 8977 	jz	00164$
      003F03 80 4A            [24] 8978 	sjmp	00111$
      003F05                       8979 00164$:
                                   8980 ;	..\src\COMMON\easyax5043.c:1613: AX5043_PLLVCOI = 0x80 | (rng - offs);
      003F05 C0 04            [24] 8981 	push	ar4
      003F07 EF               [12] 8982 	mov	a,r7
      003F08 C3               [12] 8983 	clr	c
      003F09 9B               [12] 8984 	subb	a,r3
      003F0A 90 41 80         [24] 8985 	mov	dptr,#_AX5043_PLLVCOI
      003F0D 44 80            [12] 8986 	orl	a,#0x80
      003F0F F0               [24] 8987 	movx	@dptr,a
                                   8988 ;	..\src\COMMON\easyax5043.c:1614: val = axradio_tunevoltage();
      003F10 C0 07            [24] 8989 	push	ar7
      003F12 C0 06            [24] 8990 	push	ar6
      003F14 C0 05            [24] 8991 	push	ar5
      003F16 C0 03            [24] 8992 	push	ar3
      003F18 12 3E 52         [24] 8993 	lcall	_axradio_tunevoltage
      003F1B AA 82            [24] 8994 	mov	r2,dpl
      003F1D AC 83            [24] 8995 	mov	r4,dph
      003F1F D0 03            [24] 8996 	pop	ar3
      003F21 D0 05            [24] 8997 	pop	ar5
      003F23 D0 06            [24] 8998 	pop	ar6
      003F25 D0 07            [24] 8999 	pop	ar7
      003F27 E5 1F            [12] 9000 	mov	a,_bp
      003F29 24 03            [12] 9001 	add	a,#0x03
      003F2B F8               [12] 9002 	mov	r0,a
      003F2C A6 02            [24] 9003 	mov	@r0,ar2
      003F2E 08               [12] 9004 	inc	r0
      003F2F A6 04            [24] 9005 	mov	@r0,ar4
                                   9006 ;	..\src\COMMON\easyax5043.c:1615: if (val < bestval) {
      003F31 E5 1F            [12] 9007 	mov	a,_bp
      003F33 24 03            [12] 9008 	add	a,#0x03
      003F35 F8               [12] 9009 	mov	r0,a
      003F36 C3               [12] 9010 	clr	c
      003F37 E6               [12] 9011 	mov	a,@r0
      003F38 9D               [12] 9012 	subb	a,r5
      003F39 08               [12] 9013 	inc	r0
      003F3A E6               [12] 9014 	mov	a,@r0
      003F3B 9E               [12] 9015 	subb	a,r6
      003F3C D0 04            [24] 9016 	pop	ar4
      003F3E 50 0F            [24] 9017 	jnc	00111$
                                   9018 ;	..\src\COMMON\easyax5043.c:1616: bestval = val;
      003F40 E5 1F            [12] 9019 	mov	a,_bp
      003F42 24 03            [12] 9020 	add	a,#0x03
      003F44 F8               [12] 9021 	mov	r0,a
      003F45 86 05            [24] 9022 	mov	ar5,@r0
      003F47 08               [12] 9023 	inc	r0
      003F48 86 06            [24] 9024 	mov	ar6,@r0
                                   9025 ;	..\src\COMMON\easyax5043.c:1617: bestrng = rng - offs;
      003F4A EF               [12] 9026 	mov	a,r7
      003F4B C3               [12] 9027 	clr	c
      003F4C 9B               [12] 9028 	subb	a,r3
      003F4D FA               [12] 9029 	mov	r2,a
      003F4E FC               [12] 9030 	mov	r4,a
      003F4F                       9031 00111$:
                                   9032 ;	..\src\COMMON\easyax5043.c:1600: for (offs = 0; offs != 16; ++offs) {
      003F4F 0B               [12] 9033 	inc	r3
      003F50 BB 10 02         [24] 9034 	cjne	r3,#0x10,00166$
      003F53 80 03            [24] 9035 	sjmp	00167$
      003F55                       9036 00166$:
      003F55 02 3E B3         [24] 9037 	ljmp	00115$
      003F58                       9038 00167$:
                                   9039 ;	..\src\COMMON\easyax5043.c:1622: if (bestval <= 0x0010)
      003F58 C3               [12] 9040 	clr	c
      003F59 74 10            [12] 9041 	mov	a,#0x10
      003F5B 9D               [12] 9042 	subb	a,r5
      003F5C E4               [12] 9043 	clr	a
      003F5D 9E               [12] 9044 	subb	a,r6
      003F5E 40 07            [24] 9045 	jc	00114$
                                   9046 ;	..\src\COMMON\easyax5043.c:1623: return rng | 0x80;
      003F60 43 07 80         [24] 9047 	orl	ar7,#0x80
      003F63 8F 82            [24] 9048 	mov	dpl,r7
      003F65 80 05            [24] 9049 	sjmp	00116$
      003F67                       9050 00114$:
                                   9051 ;	..\src\COMMON\easyax5043.c:1624: return bestrng | 0x80;
      003F67 43 04 80         [24] 9052 	orl	ar4,#0x80
      003F6A 8C 82            [24] 9053 	mov	dpl,r4
      003F6C                       9054 00116$:
                                   9055 ;	..\src\COMMON\easyax5043.c:1625: }
      003F6C 85 1F 81         [24] 9056 	mov	sp,_bp
      003F6F D0 1F            [24] 9057 	pop	_bp
      003F71 22               [24] 9058 	ret
                                   9059 ;------------------------------------------------------------
                                   9060 ;Allocation info for local variables in function 'axradio_calvcoi'
                                   9061 ;------------------------------------------------------------
                                   9062 ;i                         Allocated to registers r2 
                                   9063 ;r                         Allocated to registers r7 
                                   9064 ;vmin                      Allocated to registers r5 r6 
                                   9065 ;vmax                      Allocated to stack - _bp +1
                                   9066 ;curtune                   Allocated to registers r3 r4 
                                   9067 ;------------------------------------------------------------
                                   9068 ;	..\src\COMMON\easyax5043.c:1627: static __reentrantb uint8_t axradio_calvcoi(void) __reentrant
                                   9069 ;	-----------------------------------------
                                   9070 ;	 function axradio_calvcoi
                                   9071 ;	-----------------------------------------
      003F72                       9072 _axradio_calvcoi:
      003F72 C0 1F            [24] 9073 	push	_bp
      003F74 85 81 1F         [24] 9074 	mov	_bp,sp
      003F77 05 81            [12] 9075 	inc	sp
      003F79 05 81            [12] 9076 	inc	sp
                                   9077 ;	..\src\COMMON\easyax5043.c:1630: uint8_t r = 0;
      003F7B 7F 00            [12] 9078 	mov	r7,#0x00
                                   9079 ;	..\src\COMMON\easyax5043.c:1631: uint16_t vmin = 0xffff;
      003F7D 7D FF            [12] 9080 	mov	r5,#0xff
      003F7F 7E FF            [12] 9081 	mov	r6,#0xff
                                   9082 ;	..\src\COMMON\easyax5043.c:1632: uint16_t vmax = 0x0000;
      003F81 A8 1F            [24] 9083 	mov	r0,_bp
      003F83 08               [12] 9084 	inc	r0
      003F84 E4               [12] 9085 	clr	a
      003F85 F6               [12] 9086 	mov	@r0,a
      003F86 08               [12] 9087 	inc	r0
      003F87 F6               [12] 9088 	mov	@r0,a
                                   9089 ;	..\src\COMMON\easyax5043.c:1633: for (i = 0x40; i != 0;) {
      003F88 7A 40            [12] 9090 	mov	r2,#0x40
      003F8A                       9091 00113$:
                                   9092 ;	..\src\COMMON\easyax5043.c:1635: --i;
      003F8A C0 07            [24] 9093 	push	ar7
      003F8C 1A               [12] 9094 	dec	r2
                                   9095 ;	..\src\COMMON\easyax5043.c:1636: AX5043_PLLVCOI = 0x80 | i;
      003F8D 90 41 80         [24] 9096 	mov	dptr,#_AX5043_PLLVCOI
      003F90 74 80            [12] 9097 	mov	a,#0x80
      003F92 4A               [12] 9098 	orl	a,r2
      003F93 F0               [24] 9099 	movx	@dptr,a
                                   9100 ;	..\src\COMMON\easyax5043.c:1637: AX5043_PLLRANGINGA; // clear PLL lock loss
      003F94 90 40 33         [24] 9101 	mov	dptr,#_AX5043_PLLRANGINGA
      003F97 E0               [24] 9102 	movx	a,@dptr
                                   9103 ;	..\src\COMMON\easyax5043.c:1638: curtune = axradio_tunevoltage();
      003F98 C0 06            [24] 9104 	push	ar6
      003F9A C0 05            [24] 9105 	push	ar5
      003F9C C0 02            [24] 9106 	push	ar2
      003F9E 12 3E 52         [24] 9107 	lcall	_axradio_tunevoltage
      003FA1 AF 82            [24] 9108 	mov	r7,dpl
      003FA3 AC 83            [24] 9109 	mov	r4,dph
      003FA5 D0 02            [24] 9110 	pop	ar2
      003FA7 D0 05            [24] 9111 	pop	ar5
      003FA9 D0 06            [24] 9112 	pop	ar6
      003FAB 8F 03            [24] 9113 	mov	ar3,r7
                                   9114 ;	..\src\COMMON\easyax5043.c:1639: AX5043_PLLRANGINGA; // clear PLL lock loss
      003FAD 90 40 33         [24] 9115 	mov	dptr,#_AX5043_PLLRANGINGA
      003FB0 E0               [24] 9116 	movx	a,@dptr
                                   9117 ;	..\src\COMMON\easyax5043.c:1640: ((uint16_t __xdata *)axradio_rxbuffer)[i] = curtune;
      003FB1 EA               [12] 9118 	mov	a,r2
      003FB2 75 F0 02         [24] 9119 	mov	b,#0x02
      003FB5 A4               [48] 9120 	mul	ab
      003FB6 24 14            [12] 9121 	add	a,#_axradio_rxbuffer
      003FB8 F5 82            [12] 9122 	mov	dpl,a
      003FBA 74 02            [12] 9123 	mov	a,#(_axradio_rxbuffer >> 8)
      003FBC 35 F0            [12] 9124 	addc	a,b
      003FBE F5 83            [12] 9125 	mov	dph,a
      003FC0 EB               [12] 9126 	mov	a,r3
      003FC1 F0               [24] 9127 	movx	@dptr,a
      003FC2 EC               [12] 9128 	mov	a,r4
      003FC3 A3               [24] 9129 	inc	dptr
      003FC4 F0               [24] 9130 	movx	@dptr,a
                                   9131 ;	..\src\COMMON\easyax5043.c:1641: if (curtune > vmax)
      003FC5 A8 1F            [24] 9132 	mov	r0,_bp
      003FC7 08               [12] 9133 	inc	r0
      003FC8 C3               [12] 9134 	clr	c
      003FC9 E6               [12] 9135 	mov	a,@r0
      003FCA 9B               [12] 9136 	subb	a,r3
      003FCB 08               [12] 9137 	inc	r0
      003FCC E6               [12] 9138 	mov	a,@r0
      003FCD 9C               [12] 9139 	subb	a,r4
      003FCE D0 07            [24] 9140 	pop	ar7
      003FD0 50 08            [24] 9141 	jnc	00102$
                                   9142 ;	..\src\COMMON\easyax5043.c:1642: vmax = curtune;
      003FD2 A8 1F            [24] 9143 	mov	r0,_bp
      003FD4 08               [12] 9144 	inc	r0
      003FD5 A6 03            [24] 9145 	mov	@r0,ar3
      003FD7 08               [12] 9146 	inc	r0
      003FD8 A6 04            [24] 9147 	mov	@r0,ar4
      003FDA                       9148 00102$:
                                   9149 ;	..\src\COMMON\easyax5043.c:1643: if (curtune < vmin) {
      003FDA C3               [12] 9150 	clr	c
      003FDB EB               [12] 9151 	mov	a,r3
      003FDC 9D               [12] 9152 	subb	a,r5
      003FDD EC               [12] 9153 	mov	a,r4
      003FDE 9E               [12] 9154 	subb	a,r6
      003FDF 50 14            [24] 9155 	jnc	00114$
                                   9156 ;	..\src\COMMON\easyax5043.c:1644: vmin = curtune;
      003FE1 8B 05            [24] 9157 	mov	ar5,r3
      003FE3 8C 06            [24] 9158 	mov	ar6,r4
                                   9159 ;	..\src\COMMON\easyax5043.c:1646: if (!(0xC0 & (uint8_t)~AX5043_PLLRANGINGA))
      003FE5 90 40 33         [24] 9160 	mov	dptr,#_AX5043_PLLRANGINGA
      003FE8 E0               [24] 9161 	movx	a,@dptr
      003FE9 F4               [12] 9162 	cpl	a
      003FEA FC               [12] 9163 	mov	r4,a
      003FEB 54 C0            [12] 9164 	anl	a,#0xc0
      003FED 60 02            [24] 9165 	jz	00153$
      003FEF 80 04            [24] 9166 	sjmp	00114$
      003FF1                       9167 00153$:
                                   9168 ;	..\src\COMMON\easyax5043.c:1647: r = i | 0x80;
      003FF1 74 80            [12] 9169 	mov	a,#0x80
      003FF3 4A               [12] 9170 	orl	a,r2
      003FF4 FF               [12] 9171 	mov	r7,a
      003FF5                       9172 00114$:
                                   9173 ;	..\src\COMMON\easyax5043.c:1633: for (i = 0x40; i != 0;) {
      003FF5 EA               [12] 9174 	mov	a,r2
      003FF6 70 92            [24] 9175 	jnz	00113$
                                   9176 ;	..\src\COMMON\easyax5043.c:1650: if (!(r & 0x80) || vmax >= 0xFF00 || vmin < 0x0100 || vmax - vmin < 0x6000)
      003FF8 EF               [12] 9177 	mov	a,r7
      003FF9 30 E7 1F         [24] 9178 	jnb	acc.7,00108$
      003FFC A8 1F            [24] 9179 	mov	r0,_bp
      003FFE 08               [12] 9180 	inc	r0
      003FFF C3               [12] 9181 	clr	c
      004000 08               [12] 9182 	inc	r0
      004001 E6               [12] 9183 	mov	a,@r0
      004002 94 FF            [12] 9184 	subb	a,#0xff
      004004 50 15            [24] 9185 	jnc	00108$
      004006 74 FF            [12] 9186 	mov	a,#0x100 - 0x01
      004008 2E               [12] 9187 	add	a,r6
      004009 50 10            [24] 9188 	jnc	00108$
      00400B A8 1F            [24] 9189 	mov	r0,_bp
      00400D 08               [12] 9190 	inc	r0
      00400E E6               [12] 9191 	mov	a,@r0
      00400F C3               [12] 9192 	clr	c
      004010 9D               [12] 9193 	subb	a,r5
      004011 FD               [12] 9194 	mov	r5,a
      004012 08               [12] 9195 	inc	r0
      004013 E6               [12] 9196 	mov	a,@r0
      004014 9E               [12] 9197 	subb	a,r6
      004015 FE               [12] 9198 	mov	r6,a
      004016 C3               [12] 9199 	clr	c
      004017 94 60            [12] 9200 	subb	a,#0x60
      004019 50 05            [24] 9201 	jnc	00109$
      00401B                       9202 00108$:
                                   9203 ;	..\src\COMMON\easyax5043.c:1651: return 0;
      00401B 75 82 00         [24] 9204 	mov	dpl,#0x00
      00401E 80 02            [24] 9205 	sjmp	00115$
      004020                       9206 00109$:
                                   9207 ;	..\src\COMMON\easyax5043.c:1652: return r;
      004020 8F 82            [24] 9208 	mov	dpl,r7
      004022                       9209 00115$:
                                   9210 ;	..\src\COMMON\easyax5043.c:1653: }
      004022 85 1F 81         [24] 9211 	mov	sp,_bp
      004025 D0 1F            [24] 9212 	pop	_bp
      004027 22               [24] 9213 	ret
                                   9214 ;------------------------------------------------------------
                                   9215 ;Allocation info for local variables in function 'axradio_init'
                                   9216 ;------------------------------------------------------------
                                   9217 ;i                         Allocated to registers r7 
                                   9218 ;iesave                    Allocated to registers r6 
                                   9219 ;f                         Allocated to registers r3 r4 r5 r6 
                                   9220 ;r                         Allocated to registers r4 
                                   9221 ;vcoisave                  Allocated to registers r7 
                                   9222 ;f                         Allocated to registers r0 r1 r2 r3 
                                   9223 ;f                         Allocated to registers r4 r5 r6 r7 
                                   9224 ;x                         Allocated with name '_axradio_init_x_196608_424'
                                   9225 ;j                         Allocated with name '_axradio_init_j_196608_425'
                                   9226 ;x                         Allocated with name '_axradio_init_x_458752_430'
                                   9227 ;------------------------------------------------------------
                                   9228 ;	..\src\COMMON\easyax5043.c:1659: uint8_t axradio_init(void)
                                   9229 ;	-----------------------------------------
                                   9230 ;	 function axradio_init
                                   9231 ;	-----------------------------------------
      004028                       9232 _axradio_init:
                                   9233 ;	..\src\COMMON\easyax5043.c:1662: axradio_mode = AXRADIO_MODE_UNINIT;
      004028 75 17 00         [24] 9234 	mov	_axradio_mode,#0x00
                                   9235 ;	..\src\COMMON\easyax5043.c:1663: axradio_killallcb();
      00402B 12 3E 28         [24] 9236 	lcall	_axradio_killallcb
                                   9237 ;	..\src\COMMON\easyax5043.c:1664: axradio_cb_receive.cb.handler = axradio_receive_callback_fwd;
      00402E 90 03 1A         [24] 9238 	mov	dptr,#(_axradio_cb_receive + 0x0002)
      004031 74 F8            [12] 9239 	mov	a,#_axradio_receive_callback_fwd
      004033 F0               [24] 9240 	movx	@dptr,a
      004034 74 38            [12] 9241 	mov	a,#(_axradio_receive_callback_fwd >> 8)
      004036 A3               [24] 9242 	inc	dptr
      004037 F0               [24] 9243 	movx	@dptr,a
                                   9244 ;	..\src\COMMON\easyax5043.c:1665: axradio_cb_receive.st.status = AXRADIO_STAT_RECEIVE;
      004038 90 03 1C         [24] 9245 	mov	dptr,#(_axradio_cb_receive + 0x0004)
      00403B E4               [12] 9246 	clr	a
      00403C F0               [24] 9247 	movx	@dptr,a
                                   9248 ;	..\src\COMMON\easyax5043.c:1666: memset_xdata(axradio_cb_receive.st.rx.mac.remoteaddr, 0, sizeof(axradio_cb_receive.st.rx.mac.remoteaddr));
      00403D 90 04 58         [24] 9249 	mov	dptr,#_memset_PARM_2
      004040 F0               [24] 9250 	movx	@dptr,a
      004041 90 04 59         [24] 9251 	mov	dptr,#_memset_PARM_3
      004044 74 04            [12] 9252 	mov	a,#0x04
      004046 F0               [24] 9253 	movx	@dptr,a
      004047 E4               [12] 9254 	clr	a
      004048 A3               [24] 9255 	inc	dptr
      004049 F0               [24] 9256 	movx	@dptr,a
      00404A 90 03 2C         [24] 9257 	mov	dptr,#(_axradio_cb_receive + 0x0014)
      00404D 75 F0 00         [24] 9258 	mov	b,#0x00
      004050 12 75 1E         [24] 9259 	lcall	_memset
                                   9260 ;	..\src\COMMON\easyax5043.c:1667: memset_xdata(axradio_cb_receive.st.rx.mac.localaddr, 0, sizeof(axradio_cb_receive.st.rx.mac.localaddr));
      004053 90 04 58         [24] 9261 	mov	dptr,#_memset_PARM_2
      004056 E4               [12] 9262 	clr	a
      004057 F0               [24] 9263 	movx	@dptr,a
      004058 90 04 59         [24] 9264 	mov	dptr,#_memset_PARM_3
      00405B 74 04            [12] 9265 	mov	a,#0x04
      00405D F0               [24] 9266 	movx	@dptr,a
      00405E E4               [12] 9267 	clr	a
      00405F A3               [24] 9268 	inc	dptr
      004060 F0               [24] 9269 	movx	@dptr,a
      004061 90 03 30         [24] 9270 	mov	dptr,#(_axradio_cb_receive + 0x0018)
      004064 75 F0 00         [24] 9271 	mov	b,#0x00
      004067 12 75 1E         [24] 9272 	lcall	_memset
                                   9273 ;	..\src\COMMON\easyax5043.c:1668: axradio_cb_receivesfd.cb.handler = axradio_callback_fwd;
      00406A 90 03 3C         [24] 9274 	mov	dptr,#(_axradio_cb_receivesfd + 0x0002)
      00406D 74 E6            [12] 9275 	mov	a,#_axradio_callback_fwd
      00406F F0               [24] 9276 	movx	@dptr,a
      004070 74 38            [12] 9277 	mov	a,#(_axradio_callback_fwd >> 8)
      004072 A3               [24] 9278 	inc	dptr
      004073 F0               [24] 9279 	movx	@dptr,a
                                   9280 ;	..\src\COMMON\easyax5043.c:1669: axradio_cb_receivesfd.st.status = AXRADIO_STAT_RECEIVESFD;
      004074 90 03 3E         [24] 9281 	mov	dptr,#(_axradio_cb_receivesfd + 0x0004)
      004077 74 01            [12] 9282 	mov	a,#0x01
      004079 F0               [24] 9283 	movx	@dptr,a
                                   9284 ;	..\src\COMMON\easyax5043.c:1670: axradio_cb_channelstate.cb.handler = axradio_callback_fwd;
      00407A 90 03 46         [24] 9285 	mov	dptr,#(_axradio_cb_channelstate + 0x0002)
      00407D 74 E6            [12] 9286 	mov	a,#_axradio_callback_fwd
      00407F F0               [24] 9287 	movx	@dptr,a
      004080 74 38            [12] 9288 	mov	a,#(_axradio_callback_fwd >> 8)
      004082 A3               [24] 9289 	inc	dptr
      004083 F0               [24] 9290 	movx	@dptr,a
                                   9291 ;	..\src\COMMON\easyax5043.c:1671: axradio_cb_channelstate.st.status = AXRADIO_STAT_CHANNELSTATE;
      004084 90 03 48         [24] 9292 	mov	dptr,#(_axradio_cb_channelstate + 0x0004)
      004087 74 02            [12] 9293 	mov	a,#0x02
      004089 F0               [24] 9294 	movx	@dptr,a
                                   9295 ;	..\src\COMMON\easyax5043.c:1672: axradio_cb_transmitstart.cb.handler = axradio_callback_fwd;
      00408A 90 03 53         [24] 9296 	mov	dptr,#(_axradio_cb_transmitstart + 0x0002)
      00408D 74 E6            [12] 9297 	mov	a,#_axradio_callback_fwd
      00408F F0               [24] 9298 	movx	@dptr,a
      004090 74 38            [12] 9299 	mov	a,#(_axradio_callback_fwd >> 8)
      004092 A3               [24] 9300 	inc	dptr
      004093 F0               [24] 9301 	movx	@dptr,a
                                   9302 ;	..\src\COMMON\easyax5043.c:1673: axradio_cb_transmitstart.st.status = AXRADIO_STAT_TRANSMITSTART;
      004094 90 03 55         [24] 9303 	mov	dptr,#(_axradio_cb_transmitstart + 0x0004)
      004097 74 03            [12] 9304 	mov	a,#0x03
      004099 F0               [24] 9305 	movx	@dptr,a
                                   9306 ;	..\src\COMMON\easyax5043.c:1674: axradio_cb_transmitend.cb.handler = axradio_callback_fwd;
      00409A 90 03 5D         [24] 9307 	mov	dptr,#(_axradio_cb_transmitend + 0x0002)
      00409D 74 E6            [12] 9308 	mov	a,#_axradio_callback_fwd
      00409F F0               [24] 9309 	movx	@dptr,a
      0040A0 74 38            [12] 9310 	mov	a,#(_axradio_callback_fwd >> 8)
      0040A2 A3               [24] 9311 	inc	dptr
      0040A3 F0               [24] 9312 	movx	@dptr,a
                                   9313 ;	..\src\COMMON\easyax5043.c:1675: axradio_cb_transmitend.st.status = AXRADIO_STAT_TRANSMITEND;
      0040A4 90 03 5F         [24] 9314 	mov	dptr,#(_axradio_cb_transmitend + 0x0004)
      0040A7 74 04            [12] 9315 	mov	a,#0x04
      0040A9 F0               [24] 9316 	movx	@dptr,a
                                   9317 ;	..\src\COMMON\easyax5043.c:1676: axradio_cb_transmitdata.cb.handler = axradio_callback_fwd;
      0040AA 90 03 67         [24] 9318 	mov	dptr,#(_axradio_cb_transmitdata + 0x0002)
      0040AD 74 E6            [12] 9319 	mov	a,#_axradio_callback_fwd
      0040AF F0               [24] 9320 	movx	@dptr,a
      0040B0 74 38            [12] 9321 	mov	a,#(_axradio_callback_fwd >> 8)
      0040B2 A3               [24] 9322 	inc	dptr
      0040B3 F0               [24] 9323 	movx	@dptr,a
                                   9324 ;	..\src\COMMON\easyax5043.c:1677: axradio_cb_transmitdata.st.status = AXRADIO_STAT_TRANSMITDATA;
      0040B4 90 03 69         [24] 9325 	mov	dptr,#(_axradio_cb_transmitdata + 0x0004)
      0040B7 74 05            [12] 9326 	mov	a,#0x05
      0040B9 F0               [24] 9327 	movx	@dptr,a
                                   9328 ;	..\src\COMMON\easyax5043.c:1678: axradio_timer.handler = axradio_timer_callback;
      0040BA 90 03 71         [24] 9329 	mov	dptr,#(_axradio_timer + 0x0002)
      0040BD 74 A8            [12] 9330 	mov	a,#_axradio_timer_callback
      0040BF F0               [24] 9331 	movx	@dptr,a
      0040C0 74 30            [12] 9332 	mov	a,#(_axradio_timer_callback >> 8)
      0040C2 A3               [24] 9333 	inc	dptr
      0040C3 F0               [24] 9334 	movx	@dptr,a
                                   9335 ;	..\src\COMMON\easyax5043.c:1679: axradio_curchannel = 0;
      0040C4 90 00 EF         [24] 9336 	mov	dptr,#_axradio_curchannel
      0040C7 E4               [12] 9337 	clr	a
      0040C8 F0               [24] 9338 	movx	@dptr,a
                                   9339 ;	..\src\COMMON\easyax5043.c:1680: axradio_curfreqoffset = 0;
      0040C9 90 00 F0         [24] 9340 	mov	dptr,#_axradio_curfreqoffset
      0040CC F0               [24] 9341 	movx	@dptr,a
      0040CD A3               [24] 9342 	inc	dptr
      0040CE F0               [24] 9343 	movx	@dptr,a
      0040CF A3               [24] 9344 	inc	dptr
      0040D0 F0               [24] 9345 	movx	@dptr,a
      0040D1 A3               [24] 9346 	inc	dptr
      0040D2 F0               [24] 9347 	movx	@dptr,a
                                   9348 ;	..\src\COMMON\easyax5043.c:1681: IE_4 = 0;
                                   9349 ;	assignBit
      0040D3 C2 AC            [12] 9350 	clr	_IE_4
                                   9351 ;	..\src\COMMON\easyax5043.c:1682: axradio_trxstate = trxstate_off;
      0040D5 75 18 00         [24] 9352 	mov	_axradio_trxstate,#0x00
                                   9353 ;	..\src\COMMON\easyax5043.c:1683: if (ax5043_reset())
      0040D8 12 70 C0         [24] 9354 	lcall	_ax5043_reset
      0040DB E5 82            [12] 9355 	mov	a,dpl
      0040DD 60 04            [24] 9356 	jz	00102$
                                   9357 ;	..\src\COMMON\easyax5043.c:1685: return AXRADIO_ERR_NOCHIP;
      0040DF 75 82 05         [24] 9358 	mov	dpl,#0x05
      0040E2 22               [24] 9359 	ret
      0040E3                       9360 00102$:
                                   9361 ;	..\src\COMMON\easyax5043.c:1687: ax5043_init_registers();
      0040E3 12 2E 65         [24] 9362 	lcall	_ax5043_init_registers
                                   9363 ;	..\src\COMMON\easyax5043.c:1688: ax5043_set_registers_tx();
      0040E6 12 18 71         [24] 9364 	lcall	_ax5043_set_registers_tx
                                   9365 ;	..\src\COMMON\easyax5043.c:1689: AX5043_PLLLOOP = 0x09; // default 100kHz loop BW for ranging
      0040E9 90 40 30         [24] 9366 	mov	dptr,#_AX5043_PLLLOOP
      0040EC 74 09            [12] 9367 	mov	a,#0x09
      0040EE F0               [24] 9368 	movx	@dptr,a
                                   9369 ;	..\src\COMMON\easyax5043.c:1690: AX5043_PLLCPI = 0x08;
      0040EF 90 40 31         [24] 9370 	mov	dptr,#_AX5043_PLLCPI
      0040F2 14               [12] 9371 	dec	a
      0040F3 F0               [24] 9372 	movx	@dptr,a
                                   9373 ;	..\src\COMMON\easyax5043.c:1692: IE_4 = 1;
                                   9374 ;	assignBit
      0040F4 D2 AC            [12] 9375 	setb	_IE_4
                                   9376 ;	..\src\COMMON\easyax5043.c:1694: AX5043_PWRMODE = AX5043_PWRSTATE_XTAL_ON;
      0040F6 90 40 02         [24] 9377 	mov	dptr,#_AX5043_PWRMODE
      0040F9 74 05            [12] 9378 	mov	a,#0x05
      0040FB F0               [24] 9379 	movx	@dptr,a
                                   9380 ;	..\src\COMMON\easyax5043.c:1695: AX5043_MODULATION = 0x08;
      0040FC 90 40 10         [24] 9381 	mov	dptr,#_AX5043_MODULATION
      0040FF 74 08            [12] 9382 	mov	a,#0x08
      004101 F0               [24] 9383 	movx	@dptr,a
                                   9384 ;	..\src\COMMON\easyax5043.c:1696: AX5043_FSKDEV2 = 0x00;
      004102 90 41 61         [24] 9385 	mov	dptr,#_AX5043_FSKDEV2
      004105 E4               [12] 9386 	clr	a
      004106 F0               [24] 9387 	movx	@dptr,a
                                   9388 ;	..\src\COMMON\easyax5043.c:1697: AX5043_FSKDEV1 = 0x00;
      004107 90 41 62         [24] 9389 	mov	dptr,#_AX5043_FSKDEV1
      00410A F0               [24] 9390 	movx	@dptr,a
                                   9391 ;	..\src\COMMON\easyax5043.c:1698: AX5043_FSKDEV0 = 0x00;
      00410B 90 41 63         [24] 9392 	mov	dptr,#_AX5043_FSKDEV0
      00410E F0               [24] 9393 	movx	@dptr,a
                                   9394 ;	..\src\COMMON\easyax5043.c:1699: axradio_wait_for_xtal();
      00410F 12 2D 10         [24] 9395 	lcall	_axradio_wait_for_xtal
                                   9396 ;	..\src\COMMON\easyax5043.c:1701: for (i = 0; i < axradio_phy_nrchannels; ++i) {
      004112 7F 00            [12] 9397 	mov	r7,#0x00
      004114                       9398 00136$:
      004114 90 90 C1         [24] 9399 	mov	dptr,#_axradio_phy_nrchannels
      004117 E4               [12] 9400 	clr	a
      004118 93               [24] 9401 	movc	a,@a+dptr
      004119 FE               [12] 9402 	mov	r6,a
      00411A C3               [12] 9403 	clr	c
      00411B EF               [12] 9404 	mov	a,r7
      00411C 9E               [12] 9405 	subb	a,r6
      00411D 40 03            [24] 9406 	jc	00249$
      00411F 02 41 E6         [24] 9407 	ljmp	00113$
      004122                       9408 00249$:
                                   9409 ;	..\src\COMMON\easyax5043.c:1704: uint32_t __autodata f = axradio_phy_chanfreq[i];
      004122 EF               [12] 9410 	mov	a,r7
      004123 75 F0 04         [24] 9411 	mov	b,#0x04
      004126 A4               [48] 9412 	mul	ab
      004127 24 C2            [12] 9413 	add	a,#_axradio_phy_chanfreq
      004129 F5 82            [12] 9414 	mov	dpl,a
      00412B 74 90            [12] 9415 	mov	a,#(_axradio_phy_chanfreq >> 8)
      00412D 35 F0            [12] 9416 	addc	a,b
      00412F F5 83            [12] 9417 	mov	dph,a
      004131 E4               [12] 9418 	clr	a
      004132 93               [24] 9419 	movc	a,@a+dptr
      004133 FB               [12] 9420 	mov	r3,a
      004134 A3               [24] 9421 	inc	dptr
      004135 E4               [12] 9422 	clr	a
      004136 93               [24] 9423 	movc	a,@a+dptr
      004137 FC               [12] 9424 	mov	r4,a
      004138 A3               [24] 9425 	inc	dptr
      004139 E4               [12] 9426 	clr	a
      00413A 93               [24] 9427 	movc	a,@a+dptr
      00413B FD               [12] 9428 	mov	r5,a
      00413C A3               [24] 9429 	inc	dptr
      00413D E4               [12] 9430 	clr	a
      00413E 93               [24] 9431 	movc	a,@a+dptr
      00413F FE               [12] 9432 	mov	r6,a
                                   9433 ;	..\src\COMMON\easyax5043.c:1705: AX5043_FREQA0 = f;
      004140 90 40 37         [24] 9434 	mov	dptr,#_AX5043_FREQA0
      004143 EB               [12] 9435 	mov	a,r3
      004144 F0               [24] 9436 	movx	@dptr,a
                                   9437 ;	..\src\COMMON\easyax5043.c:1706: AX5043_FREQA1 = f >> 8;
      004145 90 40 36         [24] 9438 	mov	dptr,#_AX5043_FREQA1
      004148 EC               [12] 9439 	mov	a,r4
      004149 F0               [24] 9440 	movx	@dptr,a
                                   9441 ;	..\src\COMMON\easyax5043.c:1707: AX5043_FREQA2 = f >> 16;
      00414A 90 40 35         [24] 9442 	mov	dptr,#_AX5043_FREQA2
      00414D ED               [12] 9443 	mov	a,r5
      00414E F0               [24] 9444 	movx	@dptr,a
                                   9445 ;	..\src\COMMON\easyax5043.c:1708: AX5043_FREQA3 = f >> 24;
      00414F 90 40 34         [24] 9446 	mov	dptr,#_AX5043_FREQA3
      004152 EE               [12] 9447 	mov	a,r6
      004153 F0               [24] 9448 	movx	@dptr,a
                                   9449 ;	..\src\COMMON\easyax5043.c:1710: iesave = IE & 0x80;
      004154 E5 A8            [12] 9450 	mov	a,_IE
      004156 54 80            [12] 9451 	anl	a,#0x80
      004158 FE               [12] 9452 	mov	r6,a
                                   9453 ;	..\src\COMMON\easyax5043.c:1711: EA = 0;
                                   9454 ;	assignBit
      004159 C2 AF            [12] 9455 	clr	_EA
                                   9456 ;	..\src\COMMON\easyax5043.c:1712: axradio_trxstate = trxstate_pll_ranging;
      00415B 75 18 05         [24] 9457 	mov	_axradio_trxstate,#0x05
                                   9458 ;	..\src\COMMON\easyax5043.c:1713: AX5043_IRQMASK1 = 0x10; // enable pll autoranging done interrupt
      00415E 90 40 06         [24] 9459 	mov	dptr,#_AX5043_IRQMASK1
      004161 74 10            [12] 9460 	mov	a,#0x10
      004163 F0               [24] 9461 	movx	@dptr,a
                                   9462 ;	..\src\COMMON\easyax5043.c:1716: if( !(axradio_phy_chanpllrnginit[0] & 0xF0) ) { // start values for ranging available
      004164 90 90 C6         [24] 9463 	mov	dptr,#_axradio_phy_chanpllrnginit
      004167 E4               [12] 9464 	clr	a
      004168 93               [24] 9465 	movc	a,@a+dptr
      004169 54 F0            [12] 9466 	anl	a,#0xf0
      00416B 70 0B            [24] 9467 	jnz	00108$
                                   9468 ;	..\src\COMMON\easyax5043.c:1717: r = axradio_phy_chanpllrnginit[i] | 0x10;
      00416D EF               [12] 9469 	mov	a,r7
      00416E 90 90 C6         [24] 9470 	mov	dptr,#_axradio_phy_chanpllrnginit
      004171 93               [24] 9471 	movc	a,@a+dptr
      004172 FD               [12] 9472 	mov	r5,a
      004173 43 05 10         [24] 9473 	orl	ar5,#0x10
      004176 80 25            [24] 9474 	sjmp	00109$
      004178                       9475 00108$:
                                   9476 ;	..\src\COMMON\easyax5043.c:1720: r = 0x18;
      004178 7D 18            [12] 9477 	mov	r5,#0x18
                                   9478 ;	..\src\COMMON\easyax5043.c:1721: if (i) {
      00417A EF               [12] 9479 	mov	a,r7
      00417B 60 20            [24] 9480 	jz	00109$
                                   9481 ;	..\src\COMMON\easyax5043.c:1722: r = axradio_phy_chanpllrng[i - 1];
      00417D 8F 03            [24] 9482 	mov	ar3,r7
      00417F 7C 00            [12] 9483 	mov	r4,#0x00
      004181 1B               [12] 9484 	dec	r3
      004182 BB FF 01         [24] 9485 	cjne	r3,#0xff,00253$
      004185 1C               [12] 9486 	dec	r4
      004186                       9487 00253$:
      004186 EB               [12] 9488 	mov	a,r3
      004187 24 E6            [12] 9489 	add	a,#_axradio_phy_chanpllrng
      004189 F5 82            [12] 9490 	mov	dpl,a
      00418B EC               [12] 9491 	mov	a,r4
      00418C 34 00            [12] 9492 	addc	a,#(_axradio_phy_chanpllrng >> 8)
      00418E F5 83            [12] 9493 	mov	dph,a
      004190 E0               [24] 9494 	movx	a,@dptr
                                   9495 ;	..\src\COMMON\easyax5043.c:1723: if (r & 0x20)
      004191 FC               [12] 9496 	mov	r4,a
      004192 30 E5 02         [24] 9497 	jnb	acc.5,00104$
                                   9498 ;	..\src\COMMON\easyax5043.c:1724: r = 0x08;
      004195 7C 08            [12] 9499 	mov	r4,#0x08
      004197                       9500 00104$:
                                   9501 ;	..\src\COMMON\easyax5043.c:1725: r &= 0x0F;
      004197 74 0F            [12] 9502 	mov	a,#0x0f
      004199 5C               [12] 9503 	anl	a,r4
                                   9504 ;	..\src\COMMON\easyax5043.c:1726: r |= 0x10;
      00419A 44 10            [12] 9505 	orl	a,#0x10
      00419C FD               [12] 9506 	mov	r5,a
      00419D                       9507 00109$:
                                   9508 ;	..\src\COMMON\easyax5043.c:1729: AX5043_PLLRANGINGA = r; // init ranging process starting from "range"
      00419D 90 40 33         [24] 9509 	mov	dptr,#_AX5043_PLLRANGINGA
      0041A0 ED               [12] 9510 	mov	a,r5
      0041A1 F0               [24] 9511 	movx	@dptr,a
      0041A2                       9512 00133$:
                                   9513 ;	..\src\COMMON\easyax5043.c:1732: EA = 0;
                                   9514 ;	assignBit
      0041A2 C2 AF            [12] 9515 	clr	_EA
                                   9516 ;	..\src\COMMON\easyax5043.c:1733: if (axradio_trxstate == trxstate_pll_ranging_done)
      0041A4 74 06            [12] 9517 	mov	a,#0x06
      0041A6 B5 18 02         [24] 9518 	cjne	a,_axradio_trxstate,00255$
      0041A9 80 1A            [24] 9519 	sjmp	00112$
      0041AB                       9520 00255$:
                                   9521 ;	..\src\COMMON\easyax5043.c:1735: wtimer_idle(WTFLAG_CANSTANDBY);
      0041AB 75 82 02         [24] 9522 	mov	dpl,#0x02
      0041AE C0 07            [24] 9523 	push	ar7
      0041B0 C0 06            [24] 9524 	push	ar6
      0041B2 12 77 90         [24] 9525 	lcall	_wtimer_idle
      0041B5 D0 06            [24] 9526 	pop	ar6
                                   9527 ;	..\src\COMMON\easyax5043.c:1736: IE |= iesave;
      0041B7 EE               [12] 9528 	mov	a,r6
      0041B8 42 A8            [12] 9529 	orl	_IE,a
                                   9530 ;	..\src\COMMON\easyax5043.c:1737: wtimer_runcallbacks();
      0041BA C0 06            [24] 9531 	push	ar6
      0041BC 12 77 0F         [24] 9532 	lcall	_wtimer_runcallbacks
      0041BF D0 06            [24] 9533 	pop	ar6
      0041C1 D0 07            [24] 9534 	pop	ar7
      0041C3 80 DD            [24] 9535 	sjmp	00133$
      0041C5                       9536 00112$:
                                   9537 ;	..\src\COMMON\easyax5043.c:1739: axradio_trxstate = trxstate_off;
      0041C5 75 18 00         [24] 9538 	mov	_axradio_trxstate,#0x00
                                   9539 ;	..\src\COMMON\easyax5043.c:1740: AX5043_IRQMASK1 = 0x00;
      0041C8 90 40 06         [24] 9540 	mov	dptr,#_AX5043_IRQMASK1
      0041CB E4               [12] 9541 	clr	a
      0041CC F0               [24] 9542 	movx	@dptr,a
                                   9543 ;	..\src\COMMON\easyax5043.c:1741: axradio_phy_chanpllrng[i] = AX5043_PLLRANGINGA;
      0041CD EF               [12] 9544 	mov	a,r7
      0041CE 24 E6            [12] 9545 	add	a,#_axradio_phy_chanpllrng
      0041D0 FC               [12] 9546 	mov	r4,a
      0041D1 E4               [12] 9547 	clr	a
      0041D2 34 00            [12] 9548 	addc	a,#(_axradio_phy_chanpllrng >> 8)
      0041D4 FD               [12] 9549 	mov	r5,a
      0041D5 90 40 33         [24] 9550 	mov	dptr,#_AX5043_PLLRANGINGA
      0041D8 E0               [24] 9551 	movx	a,@dptr
      0041D9 FB               [12] 9552 	mov	r3,a
      0041DA 8C 82            [24] 9553 	mov	dpl,r4
      0041DC 8D 83            [24] 9554 	mov	dph,r5
      0041DE F0               [24] 9555 	movx	@dptr,a
                                   9556 ;	..\src\COMMON\easyax5043.c:1742: IE |= iesave;
      0041DF EE               [12] 9557 	mov	a,r6
      0041E0 42 A8            [12] 9558 	orl	_IE,a
                                   9559 ;	..\src\COMMON\easyax5043.c:1701: for (i = 0; i < axradio_phy_nrchannels; ++i) {
      0041E2 0F               [12] 9560 	inc	r7
      0041E3 02 41 14         [24] 9561 	ljmp	00136$
      0041E6                       9562 00113$:
                                   9563 ;	..\src\COMMON\easyax5043.c:1745: if (axradio_phy_vcocalib) {
      0041E6 90 90 C8         [24] 9564 	mov	dptr,#_axradio_phy_vcocalib
      0041E9 E4               [12] 9565 	clr	a
      0041EA 93               [24] 9566 	movc	a,@a+dptr
      0041EB 70 03            [24] 9567 	jnz	00256$
      0041ED 02 43 5C         [24] 9568 	ljmp	00129$
      0041F0                       9569 00256$:
                                   9570 ;	..\src\COMMON\easyax5043.c:1746: ax5043_set_registers_tx();
      0041F0 12 18 71         [24] 9571 	lcall	_ax5043_set_registers_tx
                                   9572 ;	..\src\COMMON\easyax5043.c:1747: AX5043_MODULATION = 0x08;
      0041F3 90 40 10         [24] 9573 	mov	dptr,#_AX5043_MODULATION
      0041F6 74 08            [12] 9574 	mov	a,#0x08
      0041F8 F0               [24] 9575 	movx	@dptr,a
                                   9576 ;	..\src\COMMON\easyax5043.c:1748: AX5043_FSKDEV2 = 0x00;
      0041F9 90 41 61         [24] 9577 	mov	dptr,#_AX5043_FSKDEV2
      0041FC E4               [12] 9578 	clr	a
      0041FD F0               [24] 9579 	movx	@dptr,a
                                   9580 ;	..\src\COMMON\easyax5043.c:1749: AX5043_FSKDEV1 = 0x00;
      0041FE 90 41 62         [24] 9581 	mov	dptr,#_AX5043_FSKDEV1
      004201 F0               [24] 9582 	movx	@dptr,a
                                   9583 ;	..\src\COMMON\easyax5043.c:1750: AX5043_FSKDEV0 = 0x00;
      004202 90 41 63         [24] 9584 	mov	dptr,#_AX5043_FSKDEV0
      004205 F0               [24] 9585 	movx	@dptr,a
                                   9586 ;	..\src\COMMON\easyax5043.c:1751: AX5043_PLLLOOP |= 0x04;
      004206 90 40 30         [24] 9587 	mov	dptr,#_AX5043_PLLLOOP
      004209 E0               [24] 9588 	movx	a,@dptr
      00420A 43 E0 04         [24] 9589 	orl	acc,#0x04
      00420D F0               [24] 9590 	movx	@dptr,a
                                   9591 ;	..\src\COMMON\easyax5043.c:1753: uint8_t x = AX5043_0xF35;
      00420E 90 4F 35         [24] 9592 	mov	dptr,#_AX5043_0xF35
      004211 E0               [24] 9593 	movx	a,@dptr
                                   9594 ;	..\src\COMMON\easyax5043.c:1754: x |= 0x80;
      004212 44 80            [12] 9595 	orl	a,#0x80
      004214 FF               [12] 9596 	mov	r7,a
      004215 90 03 94         [24] 9597 	mov	dptr,#_axradio_init_x_196608_424
      004218 F0               [24] 9598 	movx	@dptr,a
                                   9599 ;	..\src\COMMON\easyax5043.c:1755: if (2 & (uint8_t)~x)
      004219 EF               [12] 9600 	mov	a,r7
      00421A F4               [12] 9601 	cpl	a
      00421B 30 E1 06         [24] 9602 	jnb	acc.1,00115$
                                   9603 ;	..\src\COMMON\easyax5043.c:1756: ++x;
      00421E 90 03 94         [24] 9604 	mov	dptr,#_axradio_init_x_196608_424
      004221 EF               [12] 9605 	mov	a,r7
      004222 04               [12] 9606 	inc	a
      004223 F0               [24] 9607 	movx	@dptr,a
      004224                       9608 00115$:
                                   9609 ;	..\src\COMMON\easyax5043.c:1757: AX5043_0xF35 = x;
      004224 90 03 94         [24] 9610 	mov	dptr,#_axradio_init_x_196608_424
      004227 E0               [24] 9611 	movx	a,@dptr
      004228 90 4F 35         [24] 9612 	mov	dptr,#_AX5043_0xF35
      00422B F0               [24] 9613 	movx	@dptr,a
                                   9614 ;	..\src\COMMON\easyax5043.c:1759: AX5043_PWRMODE = AX5043_PWRSTATE_SYNTH_TX;
      00422C 90 40 02         [24] 9615 	mov	dptr,#_AX5043_PWRMODE
      00422F 74 0C            [12] 9616 	mov	a,#0x0c
      004231 F0               [24] 9617 	movx	@dptr,a
                                   9618 ;	..\src\COMMON\easyax5043.c:1761: uint8_t __autodata vcoisave = AX5043_PLLVCOI;
      004232 90 41 80         [24] 9619 	mov	dptr,#_AX5043_PLLVCOI
      004235 E0               [24] 9620 	movx	a,@dptr
      004236 FF               [12] 9621 	mov	r7,a
                                   9622 ;	..\src\COMMON\easyax5043.c:1762: uint8_t j = 2;
      004237 90 03 95         [24] 9623 	mov	dptr,#_axradio_init_j_196608_425
      00423A 74 02            [12] 9624 	mov	a,#0x02
      00423C F0               [24] 9625 	movx	@dptr,a
                                   9626 ;	..\src\COMMON\easyax5043.c:1763: for (i = 0; i < axradio_phy_nrchannels; ++i) {
      00423D 7E 00            [12] 9627 	mov	r6,#0x00
      00423F                       9628 00139$:
      00423F 90 90 C1         [24] 9629 	mov	dptr,#_axradio_phy_nrchannels
      004242 E4               [12] 9630 	clr	a
      004243 93               [24] 9631 	movc	a,@a+dptr
      004244 FD               [12] 9632 	mov	r5,a
      004245 C3               [12] 9633 	clr	c
      004246 EE               [12] 9634 	mov	a,r6
      004247 9D               [12] 9635 	subb	a,r5
      004248 40 03            [24] 9636 	jc	00258$
      00424A 02 43 57         [24] 9637 	ljmp	00127$
      00424D                       9638 00258$:
                                   9639 ;	..\src\COMMON\easyax5043.c:1764: axradio_phy_chanvcoi[i] = 0;
      00424D EE               [12] 9640 	mov	a,r6
      00424E 24 E7            [12] 9641 	add	a,#_axradio_phy_chanvcoi
      004250 F5 82            [12] 9642 	mov	dpl,a
      004252 E4               [12] 9643 	clr	a
      004253 34 00            [12] 9644 	addc	a,#(_axradio_phy_chanvcoi >> 8)
      004255 F5 83            [12] 9645 	mov	dph,a
      004257 E4               [12] 9646 	clr	a
      004258 F0               [24] 9647 	movx	@dptr,a
                                   9648 ;	..\src\COMMON\easyax5043.c:1765: if (axradio_phy_chanpllrng[i] & 0x20)
      004259 EE               [12] 9649 	mov	a,r6
      00425A 24 E6            [12] 9650 	add	a,#_axradio_phy_chanpllrng
      00425C FC               [12] 9651 	mov	r4,a
      00425D E4               [12] 9652 	clr	a
      00425E 34 00            [12] 9653 	addc	a,#(_axradio_phy_chanpllrng >> 8)
      004260 FD               [12] 9654 	mov	r5,a
      004261 8C 82            [24] 9655 	mov	dpl,r4
      004263 8D 83            [24] 9656 	mov	dph,r5
      004265 E0               [24] 9657 	movx	a,@dptr
      004266 FB               [12] 9658 	mov	r3,a
      004267 30 E5 03         [24] 9659 	jnb	acc.5,00259$
      00426A 02 43 53         [24] 9660 	ljmp	00126$
      00426D                       9661 00259$:
                                   9662 ;	..\src\COMMON\easyax5043.c:1767: AX5043_PLLRANGINGA = axradio_phy_chanpllrng[i] & 0x0F;
      00426D 90 40 33         [24] 9663 	mov	dptr,#_AX5043_PLLRANGINGA
      004270 74 0F            [12] 9664 	mov	a,#0x0f
      004272 5B               [12] 9665 	anl	a,r3
      004273 F0               [24] 9666 	movx	@dptr,a
                                   9667 ;	..\src\COMMON\easyax5043.c:1769: uint32_t __autodata f = axradio_phy_chanfreq[i];
      004274 EE               [12] 9668 	mov	a,r6
      004275 75 F0 04         [24] 9669 	mov	b,#0x04
      004278 A4               [48] 9670 	mul	ab
      004279 24 C2            [12] 9671 	add	a,#_axradio_phy_chanfreq
      00427B F5 82            [12] 9672 	mov	dpl,a
      00427D 74 90            [12] 9673 	mov	a,#(_axradio_phy_chanfreq >> 8)
      00427F 35 F0            [12] 9674 	addc	a,b
      004281 F5 83            [12] 9675 	mov	dph,a
      004283 E4               [12] 9676 	clr	a
      004284 93               [24] 9677 	movc	a,@a+dptr
      004285 F8               [12] 9678 	mov	r0,a
      004286 A3               [24] 9679 	inc	dptr
      004287 E4               [12] 9680 	clr	a
      004288 93               [24] 9681 	movc	a,@a+dptr
      004289 F9               [12] 9682 	mov	r1,a
      00428A A3               [24] 9683 	inc	dptr
      00428B E4               [12] 9684 	clr	a
      00428C 93               [24] 9685 	movc	a,@a+dptr
      00428D FA               [12] 9686 	mov	r2,a
      00428E A3               [24] 9687 	inc	dptr
      00428F E4               [12] 9688 	clr	a
      004290 93               [24] 9689 	movc	a,@a+dptr
      004291 FB               [12] 9690 	mov	r3,a
                                   9691 ;	..\src\COMMON\easyax5043.c:1770: AX5043_FREQA0 = f;
      004292 90 40 37         [24] 9692 	mov	dptr,#_AX5043_FREQA0
      004295 E8               [12] 9693 	mov	a,r0
      004296 F0               [24] 9694 	movx	@dptr,a
                                   9695 ;	..\src\COMMON\easyax5043.c:1771: AX5043_FREQA1 = f >> 8;
      004297 90 40 36         [24] 9696 	mov	dptr,#_AX5043_FREQA1
      00429A E9               [12] 9697 	mov	a,r1
      00429B F0               [24] 9698 	movx	@dptr,a
                                   9699 ;	..\src\COMMON\easyax5043.c:1772: AX5043_FREQA2 = f >> 16;
      00429C 90 40 35         [24] 9700 	mov	dptr,#_AX5043_FREQA2
      00429F EA               [12] 9701 	mov	a,r2
      0042A0 F0               [24] 9702 	movx	@dptr,a
                                   9703 ;	..\src\COMMON\easyax5043.c:1773: AX5043_FREQA3 = f >> 24;
      0042A1 90 40 34         [24] 9704 	mov	dptr,#_AX5043_FREQA3
      0042A4 EB               [12] 9705 	mov	a,r3
      0042A5 F0               [24] 9706 	movx	@dptr,a
                                   9707 ;	..\src\COMMON\easyax5043.c:1775: do {
      0042A6 90 03 95         [24] 9708 	mov	dptr,#_axradio_init_j_196608_425
      0042A9 E0               [24] 9709 	movx	a,@dptr
      0042AA FB               [12] 9710 	mov	r3,a
      0042AB                       9711 00123$:
                                   9712 ;	..\src\COMMON\easyax5043.c:1776: if (axradio_phy_chanvcoiinit[0]) {
      0042AB 90 90 C7         [24] 9713 	mov	dptr,#_axradio_phy_chanvcoiinit
      0042AE E4               [12] 9714 	clr	a
      0042AF 93               [24] 9715 	movc	a,@a+dptr
      0042B0 60 65            [24] 9716 	jz	00121$
                                   9717 ;	..\src\COMMON\easyax5043.c:1777: uint8_t x = axradio_phy_chanvcoiinit[i];
      0042B2 EE               [12] 9718 	mov	a,r6
      0042B3 90 90 C7         [24] 9719 	mov	dptr,#_axradio_phy_chanvcoiinit
      0042B6 93               [24] 9720 	movc	a,@a+dptr
      0042B7 FA               [12] 9721 	mov	r2,a
      0042B8 90 03 96         [24] 9722 	mov	dptr,#_axradio_init_x_458752_430
      0042BB F0               [24] 9723 	movx	@dptr,a
                                   9724 ;	..\src\COMMON\easyax5043.c:1778: if (!(axradio_phy_chanpllrnginit[0] & 0xF0))
      0042BC 90 90 C6         [24] 9725 	mov	dptr,#_axradio_phy_chanpllrnginit
      0042BF E4               [12] 9726 	clr	a
      0042C0 93               [24] 9727 	movc	a,@a+dptr
      0042C1 54 F0            [12] 9728 	anl	a,#0xf0
      0042C3 70 1B            [24] 9729 	jnz	00119$
                                   9730 ;	..\src\COMMON\easyax5043.c:1779: x += (axradio_phy_chanpllrng[i] & 0x0F) - (axradio_phy_chanpllrnginit[i] & 0x0F);
      0042C5 8C 82            [24] 9731 	mov	dpl,r4
      0042C7 8D 83            [24] 9732 	mov	dph,r5
      0042C9 E0               [24] 9733 	movx	a,@dptr
      0042CA F9               [12] 9734 	mov	r1,a
      0042CB 53 01 0F         [24] 9735 	anl	ar1,#0x0f
      0042CE EE               [12] 9736 	mov	a,r6
      0042CF 90 90 C6         [24] 9737 	mov	dptr,#_axradio_phy_chanpllrnginit
      0042D2 93               [24] 9738 	movc	a,@a+dptr
      0042D3 F8               [12] 9739 	mov	r0,a
      0042D4 74 0F            [12] 9740 	mov	a,#0x0f
      0042D6 58               [12] 9741 	anl	a,r0
      0042D7 D3               [12] 9742 	setb	c
      0042D8 99               [12] 9743 	subb	a,r1
      0042D9 F4               [12] 9744 	cpl	a
      0042DA F9               [12] 9745 	mov	r1,a
      0042DB 90 03 96         [24] 9746 	mov	dptr,#_axradio_init_x_458752_430
      0042DE 2A               [12] 9747 	add	a,r2
      0042DF F0               [24] 9748 	movx	@dptr,a
      0042E0                       9749 00119$:
                                   9750 ;	..\src\COMMON\easyax5043.c:1780: axradio_phy_chanvcoi[i] = axradio_adjustvcoi(x);
      0042E0 EE               [12] 9751 	mov	a,r6
      0042E1 24 E7            [12] 9752 	add	a,#_axradio_phy_chanvcoi
      0042E3 F9               [12] 9753 	mov	r1,a
      0042E4 E4               [12] 9754 	clr	a
      0042E5 34 00            [12] 9755 	addc	a,#(_axradio_phy_chanvcoi >> 8)
      0042E7 FA               [12] 9756 	mov	r2,a
      0042E8 90 03 96         [24] 9757 	mov	dptr,#_axradio_init_x_458752_430
      0042EB E0               [24] 9758 	movx	a,@dptr
      0042EC F5 82            [12] 9759 	mov	dpl,a
      0042EE C0 07            [24] 9760 	push	ar7
      0042F0 C0 06            [24] 9761 	push	ar6
      0042F2 C0 05            [24] 9762 	push	ar5
      0042F4 C0 04            [24] 9763 	push	ar4
      0042F6 C0 03            [24] 9764 	push	ar3
      0042F8 C0 02            [24] 9765 	push	ar2
      0042FA C0 01            [24] 9766 	push	ar1
      0042FC 12 3E 9C         [24] 9767 	lcall	_axradio_adjustvcoi
      0042FF A8 82            [24] 9768 	mov	r0,dpl
      004301 D0 01            [24] 9769 	pop	ar1
      004303 D0 02            [24] 9770 	pop	ar2
      004305 D0 03            [24] 9771 	pop	ar3
      004307 D0 04            [24] 9772 	pop	ar4
      004309 D0 05            [24] 9773 	pop	ar5
      00430B D0 06            [24] 9774 	pop	ar6
      00430D D0 07            [24] 9775 	pop	ar7
      00430F 89 82            [24] 9776 	mov	dpl,r1
      004311 8A 83            [24] 9777 	mov	dph,r2
      004313 E8               [12] 9778 	mov	a,r0
      004314 F0               [24] 9779 	movx	@dptr,a
      004315 80 2F            [24] 9780 	sjmp	00124$
      004317                       9781 00121$:
                                   9782 ;	..\src\COMMON\easyax5043.c:1782: axradio_phy_chanvcoi[i] = axradio_calvcoi();
      004317 EE               [12] 9783 	mov	a,r6
      004318 24 E7            [12] 9784 	add	a,#_axradio_phy_chanvcoi
      00431A F9               [12] 9785 	mov	r1,a
      00431B E4               [12] 9786 	clr	a
      00431C 34 00            [12] 9787 	addc	a,#(_axradio_phy_chanvcoi >> 8)
      00431E FA               [12] 9788 	mov	r2,a
      00431F C0 07            [24] 9789 	push	ar7
      004321 C0 06            [24] 9790 	push	ar6
      004323 C0 05            [24] 9791 	push	ar5
      004325 C0 04            [24] 9792 	push	ar4
      004327 C0 03            [24] 9793 	push	ar3
      004329 C0 02            [24] 9794 	push	ar2
      00432B C0 01            [24] 9795 	push	ar1
      00432D 12 3F 72         [24] 9796 	lcall	_axradio_calvcoi
      004330 A8 82            [24] 9797 	mov	r0,dpl
      004332 D0 01            [24] 9798 	pop	ar1
      004334 D0 02            [24] 9799 	pop	ar2
      004336 D0 03            [24] 9800 	pop	ar3
      004338 D0 04            [24] 9801 	pop	ar4
      00433A D0 05            [24] 9802 	pop	ar5
      00433C D0 06            [24] 9803 	pop	ar6
      00433E D0 07            [24] 9804 	pop	ar7
      004340 89 82            [24] 9805 	mov	dpl,r1
      004342 8A 83            [24] 9806 	mov	dph,r2
      004344 E8               [12] 9807 	mov	a,r0
      004345 F0               [24] 9808 	movx	@dptr,a
      004346                       9809 00124$:
                                   9810 ;	..\src\COMMON\easyax5043.c:1784: } while (--j);
      004346 1B               [12] 9811 	dec	r3
      004347 EB               [12] 9812 	mov	a,r3
      004348 60 03            [24] 9813 	jz	00263$
      00434A 02 42 AB         [24] 9814 	ljmp	00123$
      00434D                       9815 00263$:
                                   9816 ;	..\src\COMMON\easyax5043.c:1785: j = 1;
      00434D 90 03 95         [24] 9817 	mov	dptr,#_axradio_init_j_196608_425
      004350 74 01            [12] 9818 	mov	a,#0x01
      004352 F0               [24] 9819 	movx	@dptr,a
      004353                       9820 00126$:
                                   9821 ;	..\src\COMMON\easyax5043.c:1763: for (i = 0; i < axradio_phy_nrchannels; ++i) {
      004353 0E               [12] 9822 	inc	r6
      004354 02 42 3F         [24] 9823 	ljmp	00139$
      004357                       9824 00127$:
                                   9825 ;	..\src\COMMON\easyax5043.c:1788: AX5043_PLLVCOI = vcoisave;
      004357 90 41 80         [24] 9826 	mov	dptr,#_AX5043_PLLVCOI
      00435A EF               [12] 9827 	mov	a,r7
      00435B F0               [24] 9828 	movx	@dptr,a
      00435C                       9829 00129$:
                                   9830 ;	..\src\COMMON\easyax5043.c:1792: AX5043_PWRMODE = AX5043_PWRSTATE_POWERDOWN;
      00435C 90 40 02         [24] 9831 	mov	dptr,#_AX5043_PWRMODE
      00435F E4               [12] 9832 	clr	a
      004360 F0               [24] 9833 	movx	@dptr,a
                                   9834 ;	..\src\COMMON\easyax5043.c:1793: ax5043_init_registers();
      004361 12 2E 65         [24] 9835 	lcall	_ax5043_init_registers
                                   9836 ;	..\src\COMMON\easyax5043.c:1794: ax5043_set_registers_rx();
      004364 12 18 95         [24] 9837 	lcall	_ax5043_set_registers_rx
                                   9838 ;	..\src\COMMON\easyax5043.c:1795: AX5043_PLLRANGINGA = axradio_phy_chanpllrng[0] & 0x0F;
      004367 90 00 E6         [24] 9839 	mov	dptr,#_axradio_phy_chanpllrng
      00436A E0               [24] 9840 	movx	a,@dptr
      00436B FF               [12] 9841 	mov	r7,a
      00436C 90 40 33         [24] 9842 	mov	dptr,#_AX5043_PLLRANGINGA
      00436F 74 0F            [12] 9843 	mov	a,#0x0f
      004371 5F               [12] 9844 	anl	a,r7
      004372 F0               [24] 9845 	movx	@dptr,a
                                   9846 ;	..\src\COMMON\easyax5043.c:1797: uint32_t __autodata f = axradio_phy_chanfreq[0];
      004373 90 90 C2         [24] 9847 	mov	dptr,#_axradio_phy_chanfreq
      004376 E4               [12] 9848 	clr	a
      004377 93               [24] 9849 	movc	a,@a+dptr
      004378 FC               [12] 9850 	mov	r4,a
      004379 A3               [24] 9851 	inc	dptr
      00437A E4               [12] 9852 	clr	a
      00437B 93               [24] 9853 	movc	a,@a+dptr
      00437C FD               [12] 9854 	mov	r5,a
      00437D A3               [24] 9855 	inc	dptr
      00437E E4               [12] 9856 	clr	a
      00437F 93               [24] 9857 	movc	a,@a+dptr
      004380 FE               [12] 9858 	mov	r6,a
      004381 A3               [24] 9859 	inc	dptr
      004382 E4               [12] 9860 	clr	a
      004383 93               [24] 9861 	movc	a,@a+dptr
      004384 FF               [12] 9862 	mov	r7,a
                                   9863 ;	..\src\COMMON\easyax5043.c:1798: AX5043_FREQA0 = f;
      004385 90 40 37         [24] 9864 	mov	dptr,#_AX5043_FREQA0
      004388 EC               [12] 9865 	mov	a,r4
      004389 F0               [24] 9866 	movx	@dptr,a
                                   9867 ;	..\src\COMMON\easyax5043.c:1799: AX5043_FREQA1 = f >> 8;
      00438A 90 40 36         [24] 9868 	mov	dptr,#_AX5043_FREQA1
      00438D ED               [12] 9869 	mov	a,r5
      00438E F0               [24] 9870 	movx	@dptr,a
                                   9871 ;	..\src\COMMON\easyax5043.c:1800: AX5043_FREQA2 = f >> 16;
      00438F 90 40 35         [24] 9872 	mov	dptr,#_AX5043_FREQA2
      004392 EE               [12] 9873 	mov	a,r6
      004393 F0               [24] 9874 	movx	@dptr,a
                                   9875 ;	..\src\COMMON\easyax5043.c:1801: AX5043_FREQA3 = f >> 24;
      004394 90 40 34         [24] 9876 	mov	dptr,#_AX5043_FREQA3
      004397 EF               [12] 9877 	mov	a,r7
      004398 F0               [24] 9878 	movx	@dptr,a
                                   9879 ;	..\src\COMMON\easyax5043.c:1804: axradio_mode = AXRADIO_MODE_OFF;
      004399 75 17 01         [24] 9880 	mov	_axradio_mode,#0x01
                                   9881 ;	..\src\COMMON\easyax5043.c:1805: for (i = 0; i < axradio_phy_nrchannels; ++i)
      00439C 7F 00            [12] 9882 	mov	r7,#0x00
      00439E                       9883 00141$:
      00439E 90 90 C1         [24] 9884 	mov	dptr,#_axradio_phy_nrchannels
      0043A1 E4               [12] 9885 	clr	a
      0043A2 93               [24] 9886 	movc	a,@a+dptr
      0043A3 FE               [12] 9887 	mov	r6,a
      0043A4 C3               [12] 9888 	clr	c
      0043A5 EF               [12] 9889 	mov	a,r7
      0043A6 9E               [12] 9890 	subb	a,r6
      0043A7 50 16            [24] 9891 	jnc	00132$
                                   9892 ;	..\src\COMMON\easyax5043.c:1806: if (axradio_phy_chanpllrng[i] & 0x20)
      0043A9 EF               [12] 9893 	mov	a,r7
      0043AA 24 E6            [12] 9894 	add	a,#_axradio_phy_chanpllrng
      0043AC F5 82            [12] 9895 	mov	dpl,a
      0043AE E4               [12] 9896 	clr	a
      0043AF 34 00            [12] 9897 	addc	a,#(_axradio_phy_chanpllrng >> 8)
      0043B1 F5 83            [12] 9898 	mov	dph,a
      0043B3 E0               [24] 9899 	movx	a,@dptr
      0043B4 FE               [12] 9900 	mov	r6,a
      0043B5 30 E5 04         [24] 9901 	jnb	acc.5,00142$
                                   9902 ;	..\src\COMMON\easyax5043.c:1807: return AXRADIO_ERR_RANGING;
      0043B8 75 82 06         [24] 9903 	mov	dpl,#0x06
      0043BB 22               [24] 9904 	ret
      0043BC                       9905 00142$:
                                   9906 ;	..\src\COMMON\easyax5043.c:1805: for (i = 0; i < axradio_phy_nrchannels; ++i)
      0043BC 0F               [12] 9907 	inc	r7
      0043BD 80 DF            [24] 9908 	sjmp	00141$
      0043BF                       9909 00132$:
                                   9910 ;	..\src\COMMON\easyax5043.c:1808: return AXRADIO_ERR_NOERROR;
      0043BF 75 82 00         [24] 9911 	mov	dpl,#0x00
                                   9912 ;	..\src\COMMON\easyax5043.c:1809: }
      0043C2 22               [24] 9913 	ret
                                   9914 ;------------------------------------------------------------
                                   9915 ;Allocation info for local variables in function 'axradio_cansleep'
                                   9916 ;------------------------------------------------------------
                                   9917 ;	..\src\COMMON\easyax5043.c:1811: __reentrantb uint8_t axradio_cansleep(void) __reentrant
                                   9918 ;	-----------------------------------------
                                   9919 ;	 function axradio_cansleep
                                   9920 ;	-----------------------------------------
      0043C3                       9921 _axradio_cansleep:
                                   9922 ;	..\src\COMMON\easyax5043.c:1813: if (axradio_trxstate == trxstate_off || axradio_trxstate == trxstate_rxwor)
      0043C3 E5 18            [12] 9923 	mov	a,_axradio_trxstate
      0043C5 60 05            [24] 9924 	jz	00101$
      0043C7 74 02            [12] 9925 	mov	a,#0x02
      0043C9 B5 18 04         [24] 9926 	cjne	a,_axradio_trxstate,00102$
      0043CC                       9927 00101$:
                                   9928 ;	..\src\COMMON\easyax5043.c:1814: return 1;
      0043CC 75 82 01         [24] 9929 	mov	dpl,#0x01
      0043CF 22               [24] 9930 	ret
      0043D0                       9931 00102$:
                                   9932 ;	..\src\COMMON\easyax5043.c:1815: return 0;
      0043D0 75 82 00         [24] 9933 	mov	dpl,#0x00
                                   9934 ;	..\src\COMMON\easyax5043.c:1816: }
      0043D3 22               [24] 9935 	ret
                                   9936 ;------------------------------------------------------------
                                   9937 ;Allocation info for local variables in function 'axradio_set_mode'
                                   9938 ;------------------------------------------------------------
                                   9939 ;r                         Allocated to registers r6 
                                   9940 ;r                         Allocated to registers 
                                   9941 ;iesave                    Allocated to registers r7 
                                   9942 ;mode                      Allocated with name '_axradio_set_mode_mode_65536_436'
                                   9943 ;------------------------------------------------------------
                                   9944 ;	..\src\COMMON\easyax5043.c:1819: uint8_t axradio_set_mode(uint8_t mode)
                                   9945 ;	-----------------------------------------
                                   9946 ;	 function axradio_set_mode
                                   9947 ;	-----------------------------------------
      0043D4                       9948 _axradio_set_mode:
      0043D4 E5 82            [12] 9949 	mov	a,dpl
      0043D6 90 03 97         [24] 9950 	mov	dptr,#_axradio_set_mode_mode_65536_436
      0043D9 F0               [24] 9951 	movx	@dptr,a
                                   9952 ;	..\src\COMMON\easyax5043.c:1821: if (mode == axradio_mode)
      0043DA E0               [24] 9953 	movx	a,@dptr
      0043DB B5 17 04         [24] 9954 	cjne	a,_axradio_mode,00102$
                                   9955 ;	..\src\COMMON\easyax5043.c:1822: return AXRADIO_ERR_NOERROR;
      0043DE 75 82 00         [24] 9956 	mov	dpl,#0x00
      0043E1 22               [24] 9957 	ret
      0043E2                       9958 00102$:
                                   9959 ;	..\src\COMMON\easyax5043.c:1823: switch (axradio_mode) {
      0043E2 AF 17            [24] 9960 	mov	r7,_axradio_mode
      0043E4 BF 00 02         [24] 9961 	cjne	r7,#0x00,00309$
      0043E7 80 47            [24] 9962 	sjmp	00103$
      0043E9                       9963 00309$:
      0043E9 BF 02 02         [24] 9964 	cjne	r7,#0x02,00310$
      0043EC 80 4E            [24] 9965 	sjmp	00106$
      0043EE                       9966 00310$:
      0043EE BF 03 02         [24] 9967 	cjne	r7,#0x03,00311$
      0043F1 80 5F            [24] 9968 	sjmp	00114$
      0043F3                       9969 00311$:
      0043F3 BF 18 02         [24] 9970 	cjne	r7,#0x18,00312$
      0043F6 80 5A            [24] 9971 	sjmp	00114$
      0043F8                       9972 00312$:
      0043F8 BF 19 02         [24] 9973 	cjne	r7,#0x19,00313$
      0043FB 80 55            [24] 9974 	sjmp	00114$
      0043FD                       9975 00313$:
      0043FD BF 1A 02         [24] 9976 	cjne	r7,#0x1a,00314$
      004400 80 50            [24] 9977 	sjmp	00114$
      004402                       9978 00314$:
      004402 BF 1B 02         [24] 9979 	cjne	r7,#0x1b,00315$
      004405 80 4B            [24] 9980 	sjmp	00114$
      004407                       9981 00315$:
      004407 BF 1C 02         [24] 9982 	cjne	r7,#0x1c,00316$
      00440A 80 46            [24] 9983 	sjmp	00114$
      00440C                       9984 00316$:
      00440C BF 28 03         [24] 9985 	cjne	r7,#0x28,00317$
      00440F 02 44 A3         [24] 9986 	ljmp	00122$
      004412                       9987 00317$:
      004412 BF 29 03         [24] 9988 	cjne	r7,#0x29,00318$
      004415 02 44 A3         [24] 9989 	ljmp	00122$
      004418                       9990 00318$:
      004418 BF 2A 03         [24] 9991 	cjne	r7,#0x2a,00319$
      00441B 02 44 A3         [24] 9992 	ljmp	00122$
      00441E                       9993 00319$:
      00441E BF 2B 03         [24] 9994 	cjne	r7,#0x2b,00320$
      004421 02 44 A3         [24] 9995 	ljmp	00122$
      004424                       9996 00320$:
      004424 BF 2C 02         [24] 9997 	cjne	r7,#0x2c,00321$
      004427 80 7A            [24] 9998 	sjmp	00122$
      004429                       9999 00321$:
      004429 BF 2D 02         [24]10000 	cjne	r7,#0x2d,00322$
      00442C 80 75            [24]10001 	sjmp	00122$
      00442E                      10002 00322$:
                                  10003 ;	..\src\COMMON\easyax5043.c:1824: case AXRADIO_MODE_UNINIT:
      00442E 80 7C            [24]10004 	sjmp	00123$
      004430                      10005 00103$:
                                  10006 ;	..\src\COMMON\easyax5043.c:1826: uint8_t __autodata r = axradio_init();
      004430 12 40 28         [24]10007 	lcall	_axradio_init
                                  10008 ;	..\src\COMMON\easyax5043.c:1827: if (r != AXRADIO_ERR_NOERROR)
      004433 E5 82            [12]10009 	mov	a,dpl
      004435 FF               [12]10010 	mov	r7,a
      004436 FE               [12]10011 	mov	r6,a
      004437 60 79            [24]10012 	jz	00124$
                                  10013 ;	..\src\COMMON\easyax5043.c:1828: return r;
      004439 8E 82            [24]10014 	mov	dpl,r6
      00443B 22               [24]10015 	ret
                                  10016 ;	..\src\COMMON\easyax5043.c:1832: case AXRADIO_MODE_DEEPSLEEP:
      00443C                      10017 00106$:
                                  10018 ;	..\src\COMMON\easyax5043.c:1834: uint8_t __autodata r = ax5043_wakeup_deepsleep();
      00443C 12 70 7D         [24]10019 	lcall	_ax5043_wakeup_deepsleep
      00443F E5 82            [12]10020 	mov	a,dpl
                                  10021 ;	..\src\COMMON\easyax5043.c:1835: if (r)
      004441 60 04            [24]10022 	jz	00108$
                                  10023 ;	..\src\COMMON\easyax5043.c:1836: return AXRADIO_ERR_NOCHIP;
      004443 75 82 05         [24]10024 	mov	dpl,#0x05
      004446 22               [24]10025 	ret
      004447                      10026 00108$:
                                  10027 ;	..\src\COMMON\easyax5043.c:1837: ax5043_init_registers();
      004447 12 2E 65         [24]10028 	lcall	_ax5043_init_registers
                                  10029 ;	..\src\COMMON\easyax5043.c:1841: axradio_trxstate = trxstate_off;
      00444A 75 18 00         [24]10030 	mov	_axradio_trxstate,#0x00
                                  10031 ;	..\src\COMMON\easyax5043.c:1842: axradio_mode = AXRADIO_MODE_OFF;
      00444D 75 17 01         [24]10032 	mov	_axradio_mode,#0x01
                                  10033 ;	..\src\COMMON\easyax5043.c:1843: break;
                                  10034 ;	..\src\COMMON\easyax5043.c:1851: case AXRADIO_MODE_CW_TRANSMIT:
      004450 80 60            [24]10035 	sjmp	00124$
      004452                      10036 00114$:
                                  10037 ;	..\src\COMMON\easyax5043.c:1853: uint8_t __autodata iesave = IE & 0x80;
      004452 E5 A8            [12]10038 	mov	a,_IE
      004454 54 80            [12]10039 	anl	a,#0x80
      004456 FF               [12]10040 	mov	r7,a
                                  10041 ;	..\src\COMMON\easyax5043.c:1854: EA = 0;
                                  10042 ;	assignBit
      004457 C2 AF            [12]10043 	clr	_EA
                                  10044 ;	..\src\COMMON\easyax5043.c:1855: if (axradio_trxstate == trxstate_off) {
      004459 E5 18            [12]10045 	mov	a,_axradio_trxstate
      00445B 70 34            [24]10046 	jnz	00116$
                                  10047 ;	..\src\COMMON\easyax5043.c:1856: update_timeanchor();
      00445D C0 07            [24]10048 	push	ar7
      00445F 12 1E 70         [24]10049 	lcall	_update_timeanchor
                                  10050 ;	..\src\COMMON\easyax5043.c:1857: wtimer_remove_callback(&axradio_cb_transmitend.cb);
      004462 90 03 5B         [24]10051 	mov	dptr,#_axradio_cb_transmitend
      004465 12 87 AB         [24]10052 	lcall	_wtimer_remove_callback
                                  10053 ;	..\src\COMMON\easyax5043.c:1858: axradio_cb_transmitend.st.error = AXRADIO_ERR_NOERROR;
      004468 90 03 60         [24]10054 	mov	dptr,#(_axradio_cb_transmitend + 0x0005)
      00446B E4               [12]10055 	clr	a
      00446C F0               [24]10056 	movx	@dptr,a
                                  10057 ;	..\src\COMMON\easyax5043.c:1859: axradio_cb_transmitend.st.time.t = axradio_timeanchor.radiotimer;
      00446D 90 01 00         [24]10058 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      004470 E0               [24]10059 	movx	a,@dptr
      004471 FB               [12]10060 	mov	r3,a
      004472 A3               [24]10061 	inc	dptr
      004473 E0               [24]10062 	movx	a,@dptr
      004474 FC               [12]10063 	mov	r4,a
      004475 A3               [24]10064 	inc	dptr
      004476 E0               [24]10065 	movx	a,@dptr
      004477 FD               [12]10066 	mov	r5,a
      004478 A3               [24]10067 	inc	dptr
      004479 E0               [24]10068 	movx	a,@dptr
      00447A FE               [12]10069 	mov	r6,a
      00447B 90 03 61         [24]10070 	mov	dptr,#(_axradio_cb_transmitend + 0x0006)
      00447E EB               [12]10071 	mov	a,r3
      00447F F0               [24]10072 	movx	@dptr,a
      004480 EC               [12]10073 	mov	a,r4
      004481 A3               [24]10074 	inc	dptr
      004482 F0               [24]10075 	movx	@dptr,a
      004483 ED               [12]10076 	mov	a,r5
      004484 A3               [24]10077 	inc	dptr
      004485 F0               [24]10078 	movx	@dptr,a
      004486 EE               [12]10079 	mov	a,r6
      004487 A3               [24]10080 	inc	dptr
      004488 F0               [24]10081 	movx	@dptr,a
                                  10082 ;	..\src\COMMON\easyax5043.c:1860: wtimer_add_callback(&axradio_cb_transmitend.cb);
      004489 90 03 5B         [24]10083 	mov	dptr,#_axradio_cb_transmitend
      00448C 12 78 5E         [24]10084 	lcall	_wtimer_add_callback
      00448F D0 07            [24]10085 	pop	ar7
      004491                      10086 00116$:
                                  10087 ;	..\src\COMMON\easyax5043.c:1862: ax5043_off();
      004491 C0 07            [24]10088 	push	ar7
      004493 12 2C F0         [24]10089 	lcall	_ax5043_off
      004496 D0 07            [24]10090 	pop	ar7
                                  10091 ;	..\src\COMMON\easyax5043.c:1863: IE |= iesave;
      004498 EF               [12]10092 	mov	a,r7
      004499 42 A8            [12]10093 	orl	_IE,a
                                  10094 ;	..\src\COMMON\easyax5043.c:1865: ax5043_init_registers();
      00449B 12 2E 65         [24]10095 	lcall	_ax5043_init_registers
                                  10096 ;	..\src\COMMON\easyax5043.c:1866: axradio_mode = AXRADIO_MODE_OFF;
      00449E 75 17 01         [24]10097 	mov	_axradio_mode,#0x01
                                  10098 ;	..\src\COMMON\easyax5043.c:1867: break;
                                  10099 ;	..\src\COMMON\easyax5043.c:1875: case AXRADIO_MODE_STREAM_RECEIVE_DATAPIN:
      0044A1 80 0F            [24]10100 	sjmp	00124$
      0044A3                      10101 00122$:
                                  10102 ;	..\src\COMMON\easyax5043.c:1876: ax5043_off();
      0044A3 12 2C F0         [24]10103 	lcall	_ax5043_off
                                  10104 ;	..\src\COMMON\easyax5043.c:1877: ax5043_init_registers();
      0044A6 12 2E 65         [24]10105 	lcall	_ax5043_init_registers
                                  10106 ;	..\src\COMMON\easyax5043.c:1878: axradio_mode = AXRADIO_MODE_OFF;
      0044A9 75 17 01         [24]10107 	mov	_axradio_mode,#0x01
                                  10108 ;	..\src\COMMON\easyax5043.c:1880: default:
      0044AC                      10109 00123$:
                                  10110 ;	..\src\COMMON\easyax5043.c:1881: ax5043_off();
      0044AC 12 2C F0         [24]10111 	lcall	_ax5043_off
                                  10112 ;	..\src\COMMON\easyax5043.c:1882: axradio_mode = AXRADIO_MODE_OFF;
      0044AF 75 17 01         [24]10113 	mov	_axradio_mode,#0x01
                                  10114 ;	..\src\COMMON\easyax5043.c:1884: }
      0044B2                      10115 00124$:
                                  10116 ;	..\src\COMMON\easyax5043.c:1885: axradio_killallcb();
      0044B2 12 3E 28         [24]10117 	lcall	_axradio_killallcb
                                  10118 ;	..\src\COMMON\easyax5043.c:1886: if (mode == AXRADIO_MODE_UNINIT)
      0044B5 90 03 97         [24]10119 	mov	dptr,#_axradio_set_mode_mode_65536_436
      0044B8 E0               [24]10120 	movx	a,@dptr
      0044B9 FF               [12]10121 	mov	r7,a
      0044BA 70 04            [24]10122 	jnz	00126$
                                  10123 ;	..\src\COMMON\easyax5043.c:1887: return AXRADIO_ERR_NOTSUPPORTED;
      0044BC 75 82 01         [24]10124 	mov	dpl,#0x01
      0044BF 22               [24]10125 	ret
      0044C0                      10126 00126$:
                                  10127 ;	..\src\COMMON\easyax5043.c:1888: axradio_syncstate = syncstate_off;
      0044C0 90 00 EA         [24]10128 	mov	dptr,#_axradio_syncstate
      0044C3 E4               [12]10129 	clr	a
      0044C4 F0               [24]10130 	movx	@dptr,a
                                  10131 ;	..\src\COMMON\easyax5043.c:1889: switch (mode) {
      0044C5 EF               [12]10132 	mov	a,r7
      0044C6 FE               [12]10133 	mov	r6,a
      0044C7 24 CC            [12]10134 	add	a,#0xff - 0x33
      0044C9 50 03            [24]10135 	jnc	00327$
      0044CB 02 47 F0         [24]10136 	ljmp	00179$
      0044CE                      10137 00327$:
      0044CE EE               [12]10138 	mov	a,r6
      0044CF 24 0A            [12]10139 	add	a,#(00328$-3-.)
      0044D1 83               [24]10140 	movc	a,@a+pc
      0044D2 F5 82            [12]10141 	mov	dpl,a
      0044D4 EE               [12]10142 	mov	a,r6
      0044D5 24 38            [12]10143 	add	a,#(00329$-3-.)
      0044D7 83               [24]10144 	movc	a,@a+pc
      0044D8 F5 83            [12]10145 	mov	dph,a
      0044DA E4               [12]10146 	clr	a
      0044DB 73               [24]10147 	jmp	@a+dptr
      0044DC                      10148 00328$:
      0044DC F0                   10149 	.db	00179$
      0044DD 44                   10150 	.db	00127$
      0044DE 48                   10151 	.db	00128$
      0044DF B3                   10152 	.db	00174$
      0044E0 F0                   10153 	.db	00179$
      0044E1 F0                   10154 	.db	00179$
      0044E2 F0                   10155 	.db	00179$
      0044E3 F0                   10156 	.db	00179$
      0044E4 F0                   10157 	.db	00179$
      0044E5 F0                   10158 	.db	00179$
      0044E6 F0                   10159 	.db	00179$
      0044E7 F0                   10160 	.db	00179$
      0044E8 F0                   10161 	.db	00179$
      0044E9 F0                   10162 	.db	00179$
      0044EA F0                   10163 	.db	00179$
      0044EB F0                   10164 	.db	00179$
      0044EC 52                   10165 	.db	00130$
      0044ED 61                   10166 	.db	00132$
      0044EE 52                   10167 	.db	00130$
      0044EF 61                   10168 	.db	00132$
      0044F0 F0                   10169 	.db	00179$
      0044F1 F0                   10170 	.db	00179$
      0044F2 F0                   10171 	.db	00179$
      0044F3 F0                   10172 	.db	00179$
      0044F4 C3                   10173 	.db	00144$
      0044F5 C3                   10174 	.db	00144$
      0044F6 C3                   10175 	.db	00144$
      0044F7 C3                   10176 	.db	00144$
      0044F8 C3                   10177 	.db	00144$
      0044F9 F0                   10178 	.db	00179$
      0044FA F0                   10179 	.db	00179$
      0044FB F0                   10180 	.db	00179$
      0044FC 70                   10181 	.db	00134$
      0044FD B1                   10182 	.db	00139$
      0044FE 70                   10183 	.db	00134$
      0044FF B1                   10184 	.db	00139$
      004500 F0                   10185 	.db	00179$
      004501 F0                   10186 	.db	00179$
      004502 F0                   10187 	.db	00179$
      004503 F0                   10188 	.db	00179$
      004504 4F                   10189 	.db	00162$
      004505 4F                   10190 	.db	00162$
      004506 4F                   10191 	.db	00162$
      004507 4F                   10192 	.db	00162$
      004508 4F                   10193 	.db	00162$
      004509 4F                   10194 	.db	00162$
      00450A F0                   10195 	.db	00179$
      00450B F0                   10196 	.db	00179$
      00450C F6                   10197 	.db	00176$
      00450D F6                   10198 	.db	00176$
      00450E 51                   10199 	.db	00178$
      00450F 51                   10200 	.db	00178$
      004510                      10201 00329$:
      004510 47                   10202 	.db	00179$>>8
      004511 45                   10203 	.db	00127$>>8
      004512 45                   10204 	.db	00128$>>8
      004513 46                   10205 	.db	00174$>>8
      004514 47                   10206 	.db	00179$>>8
      004515 47                   10207 	.db	00179$>>8
      004516 47                   10208 	.db	00179$>>8
      004517 47                   10209 	.db	00179$>>8
      004518 47                   10210 	.db	00179$>>8
      004519 47                   10211 	.db	00179$>>8
      00451A 47                   10212 	.db	00179$>>8
      00451B 47                   10213 	.db	00179$>>8
      00451C 47                   10214 	.db	00179$>>8
      00451D 47                   10215 	.db	00179$>>8
      00451E 47                   10216 	.db	00179$>>8
      00451F 47                   10217 	.db	00179$>>8
      004520 45                   10218 	.db	00130$>>8
      004521 45                   10219 	.db	00132$>>8
      004522 45                   10220 	.db	00130$>>8
      004523 45                   10221 	.db	00132$>>8
      004524 47                   10222 	.db	00179$>>8
      004525 47                   10223 	.db	00179$>>8
      004526 47                   10224 	.db	00179$>>8
      004527 47                   10225 	.db	00179$>>8
      004528 45                   10226 	.db	00144$>>8
      004529 45                   10227 	.db	00144$>>8
      00452A 45                   10228 	.db	00144$>>8
      00452B 45                   10229 	.db	00144$>>8
      00452C 45                   10230 	.db	00144$>>8
      00452D 47                   10231 	.db	00179$>>8
      00452E 47                   10232 	.db	00179$>>8
      00452F 47                   10233 	.db	00179$>>8
      004530 45                   10234 	.db	00134$>>8
      004531 45                   10235 	.db	00139$>>8
      004532 45                   10236 	.db	00134$>>8
      004533 45                   10237 	.db	00139$>>8
      004534 47                   10238 	.db	00179$>>8
      004535 47                   10239 	.db	00179$>>8
      004536 47                   10240 	.db	00179$>>8
      004537 47                   10241 	.db	00179$>>8
      004538 46                   10242 	.db	00162$>>8
      004539 46                   10243 	.db	00162$>>8
      00453A 46                   10244 	.db	00162$>>8
      00453B 46                   10245 	.db	00162$>>8
      00453C 46                   10246 	.db	00162$>>8
      00453D 46                   10247 	.db	00162$>>8
      00453E 47                   10248 	.db	00179$>>8
      00453F 47                   10249 	.db	00179$>>8
      004540 46                   10250 	.db	00176$>>8
      004541 46                   10251 	.db	00176$>>8
      004542 47                   10252 	.db	00178$>>8
      004543 47                   10253 	.db	00178$>>8
                                  10254 ;	..\src\COMMON\easyax5043.c:1890: case AXRADIO_MODE_OFF:
      004544                      10255 00127$:
                                  10256 ;	..\src\COMMON\easyax5043.c:1891: return AXRADIO_ERR_NOERROR;
      004544 75 82 00         [24]10257 	mov	dpl,#0x00
      004547 22               [24]10258 	ret
                                  10259 ;	..\src\COMMON\easyax5043.c:1893: case AXRADIO_MODE_DEEPSLEEP:
      004548                      10260 00128$:
                                  10261 ;	..\src\COMMON\easyax5043.c:1894: ax5043_enter_deepsleep();
      004548 12 70 5D         [24]10262 	lcall	_ax5043_enter_deepsleep
                                  10263 ;	..\src\COMMON\easyax5043.c:1895: axradio_mode = AXRADIO_MODE_DEEPSLEEP;
      00454B 75 17 02         [24]10264 	mov	_axradio_mode,#0x02
                                  10265 ;	..\src\COMMON\easyax5043.c:1896: return AXRADIO_ERR_NOERROR;
      00454E 75 82 00         [24]10266 	mov	dpl,#0x00
      004551 22               [24]10267 	ret
                                  10268 ;	..\src\COMMON\easyax5043.c:1899: case AXRADIO_MODE_ACK_TRANSMIT:
      004552                      10269 00130$:
                                  10270 ;	..\src\COMMON\easyax5043.c:1900: axradio_mode = mode;
      004552 8F 17            [24]10271 	mov	_axradio_mode,r7
                                  10272 ;	..\src\COMMON\easyax5043.c:1901: axradio_ack_seqnr = 0xff;
      004554 90 00 F5         [24]10273 	mov	dptr,#_axradio_ack_seqnr
      004557 74 FF            [12]10274 	mov	a,#0xff
      004559 F0               [24]10275 	movx	@dptr,a
                                  10276 ;	..\src\COMMON\easyax5043.c:1902: ax5043_init_registers_tx();
      00455A 12 1F 4A         [24]10277 	lcall	_ax5043_init_registers_tx
                                  10278 ;	..\src\COMMON\easyax5043.c:1903: return AXRADIO_ERR_NOERROR;
      00455D 75 82 00         [24]10279 	mov	dpl,#0x00
      004560 22               [24]10280 	ret
                                  10281 ;	..\src\COMMON\easyax5043.c:1906: case AXRADIO_MODE_WOR_ACK_TRANSMIT:
      004561                      10282 00132$:
                                  10283 ;	..\src\COMMON\easyax5043.c:1907: axradio_mode = mode;
      004561 8F 17            [24]10284 	mov	_axradio_mode,r7
                                  10285 ;	..\src\COMMON\easyax5043.c:1908: axradio_ack_seqnr = 0xff;
      004563 90 00 F5         [24]10286 	mov	dptr,#_axradio_ack_seqnr
      004566 74 FF            [12]10287 	mov	a,#0xff
      004568 F0               [24]10288 	movx	@dptr,a
                                  10289 ;	..\src\COMMON\easyax5043.c:1909: ax5043_init_registers_tx();
      004569 12 1F 4A         [24]10290 	lcall	_ax5043_init_registers_tx
                                  10291 ;	..\src\COMMON\easyax5043.c:1910: return AXRADIO_ERR_NOERROR;
      00456C 75 82 00         [24]10292 	mov	dpl,#0x00
      00456F 22               [24]10293 	ret
                                  10294 ;	..\src\COMMON\easyax5043.c:1913: case AXRADIO_MODE_ACK_RECEIVE:
      004570                      10295 00134$:
                                  10296 ;	..\src\COMMON\easyax5043.c:1914: axradio_mode = mode;
      004570 8F 17            [24]10297 	mov	_axradio_mode,r7
                                  10298 ;	..\src\COMMON\easyax5043.c:1915: axradio_ack_seqnr = 0xff;
      004572 90 00 F5         [24]10299 	mov	dptr,#_axradio_ack_seqnr
      004575 74 FF            [12]10300 	mov	a,#0xff
      004577 F0               [24]10301 	movx	@dptr,a
                                  10302 ;	..\src\COMMON\easyax5043.c:1916: ax5043_init_registers_rx();
      004578 12 1F 50         [24]10303 	lcall	_ax5043_init_registers_rx
                                  10304 ;	..\src\COMMON\easyax5043.c:1917: ax5043_receiver_on_continuous();
      00457B 12 2B 9B         [24]10305 	lcall	_ax5043_receiver_on_continuous
                                  10306 ;	..\src\COMMON\easyax5043.c:1918: enablecs:
      00457E                      10307 00135$:
                                  10308 ;	..\src\COMMON\easyax5043.c:1919: if (axradio_phy_cs_enabled) {
      00457E 90 90 D2         [24]10309 	mov	dptr,#_axradio_phy_cs_enabled
      004581 E4               [12]10310 	clr	a
      004582 93               [24]10311 	movc	a,@a+dptr
      004583 60 28            [24]10312 	jz	00137$
                                  10313 ;	..\src\COMMON\easyax5043.c:1920: wtimer_remove(&axradio_timer);
      004585 90 03 6F         [24]10314 	mov	dptr,#_axradio_timer
      004588 12 85 39         [24]10315 	lcall	_wtimer_remove
                                  10316 ;	..\src\COMMON\easyax5043.c:1921: axradio_timer.time = axradio_phy_cs_period;
      00458B 90 90 D0         [24]10317 	mov	dptr,#_axradio_phy_cs_period
      00458E E4               [12]10318 	clr	a
      00458F 93               [24]10319 	movc	a,@a+dptr
      004590 FD               [12]10320 	mov	r5,a
      004591 74 01            [12]10321 	mov	a,#0x01
      004593 93               [24]10322 	movc	a,@a+dptr
      004594 FE               [12]10323 	mov	r6,a
      004595 7C 00            [12]10324 	mov	r4,#0x00
      004597 7B 00            [12]10325 	mov	r3,#0x00
      004599 90 03 73         [24]10326 	mov	dptr,#(_axradio_timer + 0x0004)
      00459C ED               [12]10327 	mov	a,r5
      00459D F0               [24]10328 	movx	@dptr,a
      00459E EE               [12]10329 	mov	a,r6
      00459F A3               [24]10330 	inc	dptr
      0045A0 F0               [24]10331 	movx	@dptr,a
      0045A1 EC               [12]10332 	mov	a,r4
      0045A2 A3               [24]10333 	inc	dptr
      0045A3 F0               [24]10334 	movx	@dptr,a
      0045A4 EB               [12]10335 	mov	a,r3
      0045A5 A3               [24]10336 	inc	dptr
      0045A6 F0               [24]10337 	movx	@dptr,a
                                  10338 ;	..\src\COMMON\easyax5043.c:1922: wtimer0_addrelative(&axradio_timer);
      0045A7 90 03 6F         [24]10339 	mov	dptr,#_axradio_timer
      0045AA 12 78 78         [24]10340 	lcall	_wtimer0_addrelative
      0045AD                      10341 00137$:
                                  10342 ;	..\src\COMMON\easyax5043.c:1924: return AXRADIO_ERR_NOERROR;
      0045AD 75 82 00         [24]10343 	mov	dpl,#0x00
      0045B0 22               [24]10344 	ret
                                  10345 ;	..\src\COMMON\easyax5043.c:1927: case AXRADIO_MODE_WOR_ACK_RECEIVE:
      0045B1                      10346 00139$:
                                  10347 ;	..\src\COMMON\easyax5043.c:1928: axradio_ack_seqnr = 0xff;
      0045B1 90 00 F5         [24]10348 	mov	dptr,#_axradio_ack_seqnr
      0045B4 74 FF            [12]10349 	mov	a,#0xff
      0045B6 F0               [24]10350 	movx	@dptr,a
                                  10351 ;	..\src\COMMON\easyax5043.c:1929: axradio_mode = mode;
      0045B7 8F 17            [24]10352 	mov	_axradio_mode,r7
                                  10353 ;	..\src\COMMON\easyax5043.c:1930: ax5043_init_registers_rx();
      0045B9 12 1F 50         [24]10354 	lcall	_ax5043_init_registers_rx
                                  10355 ;	..\src\COMMON\easyax5043.c:1931: ax5043_receiver_on_wor();
      0045BC 12 2C 01         [24]10356 	lcall	_ax5043_receiver_on_wor
                                  10357 ;	..\src\COMMON\easyax5043.c:1932: return AXRADIO_ERR_NOERROR;
      0045BF 75 82 00         [24]10358 	mov	dpl,#0x00
      0045C2 22               [24]10359 	ret
                                  10360 ;	..\src\COMMON\easyax5043.c:1938: case AXRADIO_MODE_STREAM_TRANSMIT_SCRAM_LSB:
      0045C3                      10361 00144$:
                                  10362 ;	..\src\COMMON\easyax5043.c:1939: axradio_mode = mode;
      0045C3 8F 17            [24]10363 	mov	_axradio_mode,r7
                                  10364 ;	..\src\COMMON\easyax5043.c:1940: if (axradio_mode == AXRADIO_MODE_STREAM_TRANSMIT_UNENC ||
      0045C5 74 18            [12]10365 	mov	a,#0x18
      0045C7 B5 17 02         [24]10366 	cjne	a,_axradio_mode,00331$
      0045CA 80 05            [24]10367 	sjmp	00145$
      0045CC                      10368 00331$:
                                  10369 ;	..\src\COMMON\easyax5043.c:1941: axradio_mode == AXRADIO_MODE_STREAM_TRANSMIT_UNENC_LSB)
      0045CC 74 1A            [12]10370 	mov	a,#0x1a
      0045CE B5 17 05         [24]10371 	cjne	a,_axradio_mode,00146$
      0045D1                      10372 00145$:
                                  10373 ;	..\src\COMMON\easyax5043.c:1942: AX5043_ENCODING = 0;
      0045D1 90 40 11         [24]10374 	mov	dptr,#_AX5043_ENCODING
      0045D4 E4               [12]10375 	clr	a
      0045D5 F0               [24]10376 	movx	@dptr,a
      0045D6                      10377 00146$:
                                  10378 ;	..\src\COMMON\easyax5043.c:1943: if (axradio_mode == AXRADIO_MODE_STREAM_TRANSMIT_SCRAM ||
      0045D6 74 19            [12]10379 	mov	a,#0x19
      0045D8 B5 17 02         [24]10380 	cjne	a,_axradio_mode,00334$
      0045DB 80 05            [24]10381 	sjmp	00148$
      0045DD                      10382 00334$:
                                  10383 ;	..\src\COMMON\easyax5043.c:1944: axradio_mode == AXRADIO_MODE_STREAM_TRANSMIT_SCRAM_LSB)
      0045DD 74 1B            [12]10384 	mov	a,#0x1b
      0045DF B5 17 06         [24]10385 	cjne	a,_axradio_mode,00149$
      0045E2                      10386 00148$:
                                  10387 ;	..\src\COMMON\easyax5043.c:1945: AX5043_ENCODING = 4;
      0045E2 90 40 11         [24]10388 	mov	dptr,#_AX5043_ENCODING
      0045E5 74 04            [12]10389 	mov	a,#0x04
      0045E7 F0               [24]10390 	movx	@dptr,a
      0045E8                      10391 00149$:
                                  10392 ;	..\src\COMMON\easyax5043.c:1946: if (axradio_mode == AXRADIO_MODE_STREAM_TRANSMIT_UNENC_LSB ||
      0045E8 74 1A            [12]10393 	mov	a,#0x1a
      0045EA B5 17 02         [24]10394 	cjne	a,_axradio_mode,00337$
      0045ED 80 05            [24]10395 	sjmp	00151$
      0045EF                      10396 00337$:
                                  10397 ;	..\src\COMMON\easyax5043.c:1947: axradio_mode == AXRADIO_MODE_STREAM_TRANSMIT_SCRAM_LSB)
      0045EF 74 1B            [12]10398 	mov	a,#0x1b
      0045F1 B5 17 08         [24]10399 	cjne	a,_axradio_mode,00152$
      0045F4                      10400 00151$:
                                  10401 ;	..\src\COMMON\easyax5043.c:1948: AX5043_PKTADDRCFG &= 0x7F;
      0045F4 90 42 00         [24]10402 	mov	dptr,#_AX5043_PKTADDRCFG
      0045F7 E0               [24]10403 	movx	a,@dptr
      0045F8 53 E0 7F         [24]10404 	anl	acc,#0x7f
      0045FB F0               [24]10405 	movx	@dptr,a
      0045FC                      10406 00152$:
                                  10407 ;	..\src\COMMON\easyax5043.c:1949: ax5043_init_registers_tx();
      0045FC 12 1F 4A         [24]10408 	lcall	_ax5043_init_registers_tx
                                  10409 ;	..\src\COMMON\easyax5043.c:1950: AX5043_FRAMING = 0;
      0045FF 90 40 12         [24]10410 	mov	dptr,#_AX5043_FRAMING
      004602 E4               [12]10411 	clr	a
      004603 F0               [24]10412 	movx	@dptr,a
                                  10413 ;	..\src\COMMON\easyax5043.c:1951: ax5043_prepare_tx();
      004604 12 2C C7         [24]10414 	lcall	_ax5043_prepare_tx
                                  10415 ;	..\src\COMMON\easyax5043.c:1952: axradio_trxstate = trxstate_txstream_xtalwait;
      004607 75 18 0F         [24]10416 	mov	_axradio_trxstate,#0x0f
                                  10417 ;	..\src\COMMON\easyax5043.c:1953: while (!(AX5043_POWSTAT & 0x08)) {}; // wait for modem vdd so writing the FIFO is safe
      00460A                      10418 00154$:
      00460A 90 40 03         [24]10419 	mov	dptr,#_AX5043_POWSTAT
      00460D E0               [24]10420 	movx	a,@dptr
      00460E 30 E3 F9         [24]10421 	jnb	acc.3,00154$
                                  10422 ;	..\src\COMMON\easyax5043.c:1954: AX5043_FIFOSTAT = 3; // clear FIFO data & flags (prevent transmitting anything left over in the FIFO, this has no effect if the FIFO is not powerered, in this case it is reset any way)
      004611 90 40 28         [24]10423 	mov	dptr,#_AX5043_FIFOSTAT
      004614 74 03            [12]10424 	mov	a,#0x03
      004616 F0               [24]10425 	movx	@dptr,a
                                  10426 ;	..\src\COMMON\easyax5043.c:1955: AX5043_RADIOEVENTREQ0; // make sure REVRDONE bit is cleared, so it is a reliable indicator that the packet is out
      004617 90 40 0F         [24]10427 	mov	dptr,#_AX5043_RADIOEVENTREQ0
      00461A E0               [24]10428 	movx	a,@dptr
                                  10429 ;	..\src\COMMON\easyax5043.c:1956: update_timeanchor();
      00461B 12 1E 70         [24]10430 	lcall	_update_timeanchor
                                  10431 ;	..\src\COMMON\easyax5043.c:1957: wtimer_remove_callback(&axradio_cb_transmitdata.cb);
      00461E 90 03 65         [24]10432 	mov	dptr,#_axradio_cb_transmitdata
      004621 12 87 AB         [24]10433 	lcall	_wtimer_remove_callback
                                  10434 ;	..\src\COMMON\easyax5043.c:1958: axradio_cb_transmitdata.st.error = AXRADIO_ERR_NOERROR;
      004624 90 03 6A         [24]10435 	mov	dptr,#(_axradio_cb_transmitdata + 0x0005)
      004627 E4               [12]10436 	clr	a
      004628 F0               [24]10437 	movx	@dptr,a
                                  10438 ;	..\src\COMMON\easyax5043.c:1959: axradio_cb_transmitdata.st.time.t = axradio_timeanchor.radiotimer;
      004629 90 01 00         [24]10439 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      00462C E0               [24]10440 	movx	a,@dptr
      00462D FB               [12]10441 	mov	r3,a
      00462E A3               [24]10442 	inc	dptr
      00462F E0               [24]10443 	movx	a,@dptr
      004630 FC               [12]10444 	mov	r4,a
      004631 A3               [24]10445 	inc	dptr
      004632 E0               [24]10446 	movx	a,@dptr
      004633 FD               [12]10447 	mov	r5,a
      004634 A3               [24]10448 	inc	dptr
      004635 E0               [24]10449 	movx	a,@dptr
      004636 FE               [12]10450 	mov	r6,a
      004637 90 03 6B         [24]10451 	mov	dptr,#(_axradio_cb_transmitdata + 0x0006)
      00463A EB               [12]10452 	mov	a,r3
      00463B F0               [24]10453 	movx	@dptr,a
      00463C EC               [12]10454 	mov	a,r4
      00463D A3               [24]10455 	inc	dptr
      00463E F0               [24]10456 	movx	@dptr,a
      00463F ED               [12]10457 	mov	a,r5
      004640 A3               [24]10458 	inc	dptr
      004641 F0               [24]10459 	movx	@dptr,a
      004642 EE               [12]10460 	mov	a,r6
      004643 A3               [24]10461 	inc	dptr
      004644 F0               [24]10462 	movx	@dptr,a
                                  10463 ;	..\src\COMMON\easyax5043.c:1960: wtimer_add_callback(&axradio_cb_transmitdata.cb);
      004645 90 03 65         [24]10464 	mov	dptr,#_axradio_cb_transmitdata
      004648 12 78 5E         [24]10465 	lcall	_wtimer_add_callback
                                  10466 ;	..\src\COMMON\easyax5043.c:1961: return AXRADIO_ERR_NOERROR;
      00464B 75 82 00         [24]10467 	mov	dpl,#0x00
      00464E 22               [24]10468 	ret
                                  10469 ;	..\src\COMMON\easyax5043.c:1968: case AXRADIO_MODE_STREAM_RECEIVE_DATAPIN:
      00464F                      10470 00162$:
                                  10471 ;	..\src\COMMON\easyax5043.c:1969: axradio_mode = mode;
      00464F 8F 17            [24]10472 	mov	_axradio_mode,r7
                                  10473 ;	..\src\COMMON\easyax5043.c:1970: ax5043_init_registers_rx();
      004651 12 1F 50         [24]10474 	lcall	_ax5043_init_registers_rx
                                  10475 ;	..\src\COMMON\easyax5043.c:1971: if (axradio_mode == AXRADIO_MODE_STREAM_RECEIVE_UNENC ||
      004654 74 28            [12]10476 	mov	a,#0x28
      004656 B5 17 02         [24]10477 	cjne	a,_axradio_mode,00341$
      004659 80 05            [24]10478 	sjmp	00163$
      00465B                      10479 00341$:
                                  10480 ;	..\src\COMMON\easyax5043.c:1972: axradio_mode == AXRADIO_MODE_STREAM_RECEIVE_UNENC_LSB)
      00465B 74 2A            [12]10481 	mov	a,#0x2a
      00465D B5 17 05         [24]10482 	cjne	a,_axradio_mode,00164$
      004660                      10483 00163$:
                                  10484 ;	..\src\COMMON\easyax5043.c:1973: AX5043_ENCODING = 0;
      004660 90 40 11         [24]10485 	mov	dptr,#_AX5043_ENCODING
      004663 E4               [12]10486 	clr	a
      004664 F0               [24]10487 	movx	@dptr,a
      004665                      10488 00164$:
                                  10489 ;	..\src\COMMON\easyax5043.c:1974: if (axradio_mode == AXRADIO_MODE_STREAM_RECEIVE_SCRAM ||
      004665 74 29            [12]10490 	mov	a,#0x29
      004667 B5 17 02         [24]10491 	cjne	a,_axradio_mode,00344$
      00466A 80 05            [24]10492 	sjmp	00166$
      00466C                      10493 00344$:
                                  10494 ;	..\src\COMMON\easyax5043.c:1975: axradio_mode == AXRADIO_MODE_STREAM_RECEIVE_SCRAM_LSB)
      00466C 74 2B            [12]10495 	mov	a,#0x2b
      00466E B5 17 06         [24]10496 	cjne	a,_axradio_mode,00167$
      004671                      10497 00166$:
                                  10498 ;	..\src\COMMON\easyax5043.c:1976: AX5043_ENCODING = 4;
      004671 90 40 11         [24]10499 	mov	dptr,#_AX5043_ENCODING
      004674 74 04            [12]10500 	mov	a,#0x04
      004676 F0               [24]10501 	movx	@dptr,a
      004677                      10502 00167$:
                                  10503 ;	..\src\COMMON\easyax5043.c:1977: if (axradio_mode == AXRADIO_MODE_STREAM_RECEIVE_UNENC_LSB ||
      004677 74 2A            [12]10504 	mov	a,#0x2a
      004679 B5 17 02         [24]10505 	cjne	a,_axradio_mode,00347$
      00467C 80 05            [24]10506 	sjmp	00169$
      00467E                      10507 00347$:
                                  10508 ;	..\src\COMMON\easyax5043.c:1978: axradio_mode == AXRADIO_MODE_STREAM_RECEIVE_SCRAM_LSB)
      00467E 74 2B            [12]10509 	mov	a,#0x2b
      004680 B5 17 08         [24]10510 	cjne	a,_axradio_mode,00170$
      004683                      10511 00169$:
                                  10512 ;	..\src\COMMON\easyax5043.c:1979: AX5043_PKTADDRCFG &= 0x7F;
      004683 90 42 00         [24]10513 	mov	dptr,#_AX5043_PKTADDRCFG
      004686 E0               [24]10514 	movx	a,@dptr
      004687 53 E0 7F         [24]10515 	anl	acc,#0x7f
      00468A F0               [24]10516 	movx	@dptr,a
      00468B                      10517 00170$:
                                  10518 ;	..\src\COMMON\easyax5043.c:1980: AX5043_FRAMING = 0;
      00468B 90 40 12         [24]10519 	mov	dptr,#_AX5043_FRAMING
      00468E E4               [12]10520 	clr	a
      00468F F0               [24]10521 	movx	@dptr,a
                                  10522 ;	..\src\COMMON\easyax5043.c:1981: AX5043_PKTCHUNKSIZE = 8; // 64 byte
      004690 90 42 30         [24]10523 	mov	dptr,#_AX5043_PKTCHUNKSIZE
      004693 74 08            [12]10524 	mov	a,#0x08
      004695 F0               [24]10525 	movx	@dptr,a
                                  10526 ;	..\src\COMMON\easyax5043.c:1982: AX5043_RXPARAMSETS = 0x00;
      004696 90 41 17         [24]10527 	mov	dptr,#_AX5043_RXPARAMSETS
      004699 E4               [12]10528 	clr	a
      00469A F0               [24]10529 	movx	@dptr,a
                                  10530 ;	..\src\COMMON\easyax5043.c:1983: if( axradio_mode == AXRADIO_MODE_STREAM_RECEIVE_DATAPIN )
      00469B 74 2D            [12]10531 	mov	a,#0x2d
      00469D B5 17 0D         [24]10532 	cjne	a,_axradio_mode,00173$
                                  10533 ;	..\src\COMMON\easyax5043.c:1985: ax5043_set_registers_rxcont_singleparamset();
      0046A0 12 18 DA         [24]10534 	lcall	_ax5043_set_registers_rxcont_singleparamset
                                  10535 ;	..\src\COMMON\easyax5043.c:1986: AX5043_PINFUNCDATA = 0x04;
      0046A3 90 40 23         [24]10536 	mov	dptr,#_AX5043_PINFUNCDATA
      0046A6 74 04            [12]10537 	mov	a,#0x04
      0046A8 F0               [24]10538 	movx	@dptr,a
                                  10539 ;	..\src\COMMON\easyax5043.c:1987: AX5043_PINFUNCDCLK = 0x04;
      0046A9 90 40 22         [24]10540 	mov	dptr,#_AX5043_PINFUNCDCLK
      0046AC F0               [24]10541 	movx	@dptr,a
      0046AD                      10542 00173$:
                                  10543 ;	..\src\COMMON\easyax5043.c:1989: ax5043_receiver_on_continuous();
      0046AD 12 2B 9B         [24]10544 	lcall	_ax5043_receiver_on_continuous
                                  10545 ;	..\src\COMMON\easyax5043.c:1990: goto enablecs;
      0046B0 02 45 7E         [24]10546 	ljmp	00135$
                                  10547 ;	..\src\COMMON\easyax5043.c:1992: case AXRADIO_MODE_CW_TRANSMIT:
      0046B3                      10548 00174$:
                                  10549 ;	..\src\COMMON\easyax5043.c:1993: axradio_mode = AXRADIO_MODE_CW_TRANSMIT;
      0046B3 75 17 03         [24]10550 	mov	_axradio_mode,#0x03
                                  10551 ;	..\src\COMMON\easyax5043.c:1994: ax5043_init_registers_tx();
      0046B6 12 1F 4A         [24]10552 	lcall	_ax5043_init_registers_tx
                                  10553 ;	..\src\COMMON\easyax5043.c:1995: AX5043_MODULATION = 8;   // Set an FSK mode
      0046B9 90 40 10         [24]10554 	mov	dptr,#_AX5043_MODULATION
      0046BC 74 08            [12]10555 	mov	a,#0x08
      0046BE F0               [24]10556 	movx	@dptr,a
                                  10557 ;	..\src\COMMON\easyax5043.c:1996: AX5043_FSKDEV2 = 0x00;
      0046BF 90 41 61         [24]10558 	mov	dptr,#_AX5043_FSKDEV2
      0046C2 E4               [12]10559 	clr	a
      0046C3 F0               [24]10560 	movx	@dptr,a
                                  10561 ;	..\src\COMMON\easyax5043.c:1997: AX5043_FSKDEV1 = 0x00;
      0046C4 90 41 62         [24]10562 	mov	dptr,#_AX5043_FSKDEV1
      0046C7 F0               [24]10563 	movx	@dptr,a
                                  10564 ;	..\src\COMMON\easyax5043.c:1998: AX5043_FSKDEV0 = 0x00;
      0046C8 90 41 63         [24]10565 	mov	dptr,#_AX5043_FSKDEV0
      0046CB F0               [24]10566 	movx	@dptr,a
                                  10567 ;	..\src\COMMON\easyax5043.c:1999: AX5043_TXRATE2 = 0x00;
      0046CC 90 41 65         [24]10568 	mov	dptr,#_AX5043_TXRATE2
      0046CF F0               [24]10569 	movx	@dptr,a
                                  10570 ;	..\src\COMMON\easyax5043.c:2000: AX5043_TXRATE1 = 0x00;
      0046D0 90 41 66         [24]10571 	mov	dptr,#_AX5043_TXRATE1
      0046D3 F0               [24]10572 	movx	@dptr,a
                                  10573 ;	..\src\COMMON\easyax5043.c:2001: AX5043_TXRATE0 = 0x01;
      0046D4 90 41 67         [24]10574 	mov	dptr,#_AX5043_TXRATE0
      0046D7 04               [12]10575 	inc	a
      0046D8 F0               [24]10576 	movx	@dptr,a
                                  10577 ;	..\src\COMMON\easyax5043.c:2002: AX5043_PINFUNCDATA = 0x04;
      0046D9 90 40 23         [24]10578 	mov	dptr,#_AX5043_PINFUNCDATA
      0046DC 74 04            [12]10579 	mov	a,#0x04
      0046DE F0               [24]10580 	movx	@dptr,a
                                  10581 ;	..\src\COMMON\easyax5043.c:2003: AX5043_PWRMODE = AX5043_PWRSTATE_FIFO_ON;
      0046DF 90 40 02         [24]10582 	mov	dptr,#_AX5043_PWRMODE
      0046E2 74 07            [12]10583 	mov	a,#0x07
      0046E4 F0               [24]10584 	movx	@dptr,a
                                  10585 ;	..\src\COMMON\easyax5043.c:2004: axradio_trxstate = trxstate_txcw_xtalwait;
      0046E5 75 18 0E         [24]10586 	mov	_axradio_trxstate,#0x0e
                                  10587 ;	..\src\COMMON\easyax5043.c:2005: AX5043_IRQMASK0 = 0x00;
      0046E8 90 40 07         [24]10588 	mov	dptr,#_AX5043_IRQMASK0
      0046EB E4               [12]10589 	clr	a
      0046EC F0               [24]10590 	movx	@dptr,a
                                  10591 ;	..\src\COMMON\easyax5043.c:2006: AX5043_IRQMASK1 = 0x01; // enable xtal ready interrupt
      0046ED 90 40 06         [24]10592 	mov	dptr,#_AX5043_IRQMASK1
      0046F0 04               [12]10593 	inc	a
      0046F1 F0               [24]10594 	movx	@dptr,a
                                  10595 ;	..\src\COMMON\easyax5043.c:2007: return AXRADIO_ERR_NOERROR;
      0046F2 75 82 00         [24]10596 	mov	dpl,#0x00
      0046F5 22               [24]10597 	ret
                                  10598 ;	..\src\COMMON\easyax5043.c:2010: case AXRADIO_MODE_SYNC_ACK_MASTER:
      0046F6                      10599 00176$:
                                  10600 ;	..\src\COMMON\easyax5043.c:2011: axradio_mode = mode;
      0046F6 8F 17            [24]10601 	mov	_axradio_mode,r7
                                  10602 ;	..\src\COMMON\easyax5043.c:2012: axradio_syncstate = syncstate_master_normal;
      0046F8 90 00 EA         [24]10603 	mov	dptr,#_axradio_syncstate
      0046FB 74 03            [12]10604 	mov	a,#0x03
      0046FD F0               [24]10605 	movx	@dptr,a
                                  10606 ;	..\src\COMMON\easyax5043.c:2014: wtimer_remove(&axradio_timer);
      0046FE 90 03 6F         [24]10607 	mov	dptr,#_axradio_timer
      004701 12 85 39         [24]10608 	lcall	_wtimer_remove
                                  10609 ;	..\src\COMMON\easyax5043.c:2015: axradio_timer.time = 2;
      004704 90 03 73         [24]10610 	mov	dptr,#(_axradio_timer + 0x0004)
      004707 74 02            [12]10611 	mov	a,#0x02
      004709 F0               [24]10612 	movx	@dptr,a
      00470A E4               [12]10613 	clr	a
      00470B A3               [24]10614 	inc	dptr
      00470C F0               [24]10615 	movx	@dptr,a
      00470D A3               [24]10616 	inc	dptr
      00470E F0               [24]10617 	movx	@dptr,a
      00470F A3               [24]10618 	inc	dptr
      004710 F0               [24]10619 	movx	@dptr,a
                                  10620 ;	..\src\COMMON\easyax5043.c:2016: wtimer0_addrelative(&axradio_timer);
      004711 90 03 6F         [24]10621 	mov	dptr,#_axradio_timer
      004714 12 78 78         [24]10622 	lcall	_wtimer0_addrelative
                                  10623 ;	..\src\COMMON\easyax5043.c:2017: axradio_sync_time = axradio_timer.time;
      004717 90 03 73         [24]10624 	mov	dptr,#(_axradio_timer + 0x0004)
      00471A E0               [24]10625 	movx	a,@dptr
      00471B FB               [12]10626 	mov	r3,a
      00471C A3               [24]10627 	inc	dptr
      00471D E0               [24]10628 	movx	a,@dptr
      00471E FC               [12]10629 	mov	r4,a
      00471F A3               [24]10630 	inc	dptr
      004720 E0               [24]10631 	movx	a,@dptr
      004721 FD               [12]10632 	mov	r5,a
      004722 A3               [24]10633 	inc	dptr
      004723 E0               [24]10634 	movx	a,@dptr
      004724 FE               [12]10635 	mov	r6,a
      004725 90 00 F6         [24]10636 	mov	dptr,#_axradio_sync_time
      004728 EB               [12]10637 	mov	a,r3
      004729 F0               [24]10638 	movx	@dptr,a
      00472A EC               [12]10639 	mov	a,r4
      00472B A3               [24]10640 	inc	dptr
      00472C F0               [24]10641 	movx	@dptr,a
      00472D ED               [12]10642 	mov	a,r5
      00472E A3               [24]10643 	inc	dptr
      00472F F0               [24]10644 	movx	@dptr,a
      004730 EE               [12]10645 	mov	a,r6
      004731 A3               [24]10646 	inc	dptr
      004732 F0               [24]10647 	movx	@dptr,a
                                  10648 ;	..\src\COMMON\easyax5043.c:2018: axradio_sync_addtime(axradio_sync_xoscstartup);
      004733 90 91 01         [24]10649 	mov	dptr,#_axradio_sync_xoscstartup
      004736 E4               [12]10650 	clr	a
      004737 93               [24]10651 	movc	a,@a+dptr
      004738 FB               [12]10652 	mov	r3,a
      004739 74 01            [12]10653 	mov	a,#0x01
      00473B 93               [24]10654 	movc	a,@a+dptr
      00473C FC               [12]10655 	mov	r4,a
      00473D 74 02            [12]10656 	mov	a,#0x02
      00473F 93               [24]10657 	movc	a,@a+dptr
      004740 FD               [12]10658 	mov	r5,a
      004741 74 03            [12]10659 	mov	a,#0x03
      004743 93               [24]10660 	movc	a,@a+dptr
      004744 8B 82            [24]10661 	mov	dpl,r3
      004746 8C 83            [24]10662 	mov	dph,r4
      004748 8D F0            [24]10663 	mov	b,r5
      00474A 12 2E 93         [24]10664 	lcall	_axradio_sync_addtime
                                  10665 ;	..\src\COMMON\easyax5043.c:2019: return AXRADIO_ERR_NOERROR;
      00474D 75 82 00         [24]10666 	mov	dpl,#0x00
      004750 22               [24]10667 	ret
                                  10668 ;	..\src\COMMON\easyax5043.c:2022: case AXRADIO_MODE_SYNC_ACK_SLAVE:
      004751                      10669 00178$:
                                  10670 ;	..\src\COMMON\easyax5043.c:2023: axradio_mode = mode;
      004751 8F 17            [24]10671 	mov	_axradio_mode,r7
                                  10672 ;	..\src\COMMON\easyax5043.c:2024: ax5043_init_registers_rx();
      004753 12 1F 50         [24]10673 	lcall	_ax5043_init_registers_rx
                                  10674 ;	..\src\COMMON\easyax5043.c:2025: ax5043_receiver_on_continuous();
      004756 12 2B 9B         [24]10675 	lcall	_ax5043_receiver_on_continuous
                                  10676 ;	..\src\COMMON\easyax5043.c:2026: axradio_syncstate = syncstate_slave_synchunt;
      004759 90 00 EA         [24]10677 	mov	dptr,#_axradio_syncstate
      00475C 74 06            [12]10678 	mov	a,#0x06
      00475E F0               [24]10679 	movx	@dptr,a
                                  10680 ;	..\src\COMMON\easyax5043.c:2027: wtimer_remove(&axradio_timer);
      00475F 90 03 6F         [24]10681 	mov	dptr,#_axradio_timer
      004762 12 85 39         [24]10682 	lcall	_wtimer_remove
                                  10683 ;	..\src\COMMON\easyax5043.c:2028: axradio_timer.time = axradio_sync_slave_initialsyncwindow;
      004765 90 91 09         [24]10684 	mov	dptr,#_axradio_sync_slave_initialsyncwindow
      004768 E4               [12]10685 	clr	a
      004769 93               [24]10686 	movc	a,@a+dptr
      00476A FC               [12]10687 	mov	r4,a
      00476B 74 01            [12]10688 	mov	a,#0x01
      00476D 93               [24]10689 	movc	a,@a+dptr
      00476E FD               [12]10690 	mov	r5,a
      00476F 74 02            [12]10691 	mov	a,#0x02
      004771 93               [24]10692 	movc	a,@a+dptr
      004772 FE               [12]10693 	mov	r6,a
      004773 74 03            [12]10694 	mov	a,#0x03
      004775 93               [24]10695 	movc	a,@a+dptr
      004776 FF               [12]10696 	mov	r7,a
      004777 90 03 73         [24]10697 	mov	dptr,#(_axradio_timer + 0x0004)
      00477A EC               [12]10698 	mov	a,r4
      00477B F0               [24]10699 	movx	@dptr,a
      00477C ED               [12]10700 	mov	a,r5
      00477D A3               [24]10701 	inc	dptr
      00477E F0               [24]10702 	movx	@dptr,a
      00477F EE               [12]10703 	mov	a,r6
      004780 A3               [24]10704 	inc	dptr
      004781 F0               [24]10705 	movx	@dptr,a
      004782 EF               [12]10706 	mov	a,r7
      004783 A3               [24]10707 	inc	dptr
      004784 F0               [24]10708 	movx	@dptr,a
                                  10709 ;	..\src\COMMON\easyax5043.c:2029: wtimer0_addrelative(&axradio_timer);
      004785 90 03 6F         [24]10710 	mov	dptr,#_axradio_timer
      004788 12 78 78         [24]10711 	lcall	_wtimer0_addrelative
                                  10712 ;	..\src\COMMON\easyax5043.c:2030: axradio_sync_time = axradio_timer.time;
      00478B 90 03 73         [24]10713 	mov	dptr,#(_axradio_timer + 0x0004)
      00478E E0               [24]10714 	movx	a,@dptr
      00478F FC               [12]10715 	mov	r4,a
      004790 A3               [24]10716 	inc	dptr
      004791 E0               [24]10717 	movx	a,@dptr
      004792 FD               [12]10718 	mov	r5,a
      004793 A3               [24]10719 	inc	dptr
      004794 E0               [24]10720 	movx	a,@dptr
      004795 FE               [12]10721 	mov	r6,a
      004796 A3               [24]10722 	inc	dptr
      004797 E0               [24]10723 	movx	a,@dptr
      004798 FF               [12]10724 	mov	r7,a
      004799 90 00 F6         [24]10725 	mov	dptr,#_axradio_sync_time
      00479C EC               [12]10726 	mov	a,r4
      00479D F0               [24]10727 	movx	@dptr,a
      00479E ED               [12]10728 	mov	a,r5
      00479F A3               [24]10729 	inc	dptr
      0047A0 F0               [24]10730 	movx	@dptr,a
      0047A1 EE               [12]10731 	mov	a,r6
      0047A2 A3               [24]10732 	inc	dptr
      0047A3 F0               [24]10733 	movx	@dptr,a
      0047A4 EF               [12]10734 	mov	a,r7
      0047A5 A3               [24]10735 	inc	dptr
      0047A6 F0               [24]10736 	movx	@dptr,a
                                  10737 ;	..\src\COMMON\easyax5043.c:2031: wtimer_remove_callback(&axradio_cb_receive.cb);
      0047A7 90 03 18         [24]10738 	mov	dptr,#_axradio_cb_receive
      0047AA 12 87 AB         [24]10739 	lcall	_wtimer_remove_callback
                                  10740 ;	..\src\COMMON\easyax5043.c:2032: memset_xdata(&axradio_cb_receive.st, 0, sizeof(axradio_cb_receive.st));
      0047AD 90 04 58         [24]10741 	mov	dptr,#_memset_PARM_2
      0047B0 E4               [12]10742 	clr	a
      0047B1 F0               [24]10743 	movx	@dptr,a
      0047B2 90 04 59         [24]10744 	mov	dptr,#_memset_PARM_3
      0047B5 74 1E            [12]10745 	mov	a,#0x1e
      0047B7 F0               [24]10746 	movx	@dptr,a
      0047B8 E4               [12]10747 	clr	a
      0047B9 A3               [24]10748 	inc	dptr
      0047BA F0               [24]10749 	movx	@dptr,a
      0047BB 90 03 1C         [24]10750 	mov	dptr,#(_axradio_cb_receive + 0x0004)
      0047BE 75 F0 00         [24]10751 	mov	b,#0x00
      0047C1 12 75 1E         [24]10752 	lcall	_memset
                                  10753 ;	..\src\COMMON\easyax5043.c:2033: axradio_cb_receive.st.time.t = axradio_timeanchor.radiotimer;
      0047C4 90 01 00         [24]10754 	mov	dptr,#(_axradio_timeanchor + 0x0004)
      0047C7 E0               [24]10755 	movx	a,@dptr
      0047C8 FC               [12]10756 	mov	r4,a
      0047C9 A3               [24]10757 	inc	dptr
      0047CA E0               [24]10758 	movx	a,@dptr
      0047CB FD               [12]10759 	mov	r5,a
      0047CC A3               [24]10760 	inc	dptr
      0047CD E0               [24]10761 	movx	a,@dptr
      0047CE FE               [12]10762 	mov	r6,a
      0047CF A3               [24]10763 	inc	dptr
      0047D0 E0               [24]10764 	movx	a,@dptr
      0047D1 FF               [12]10765 	mov	r7,a
      0047D2 90 03 1E         [24]10766 	mov	dptr,#(_axradio_cb_receive + 0x0006)
      0047D5 EC               [12]10767 	mov	a,r4
      0047D6 F0               [24]10768 	movx	@dptr,a
      0047D7 ED               [12]10769 	mov	a,r5
      0047D8 A3               [24]10770 	inc	dptr
      0047D9 F0               [24]10771 	movx	@dptr,a
      0047DA EE               [12]10772 	mov	a,r6
      0047DB A3               [24]10773 	inc	dptr
      0047DC F0               [24]10774 	movx	@dptr,a
      0047DD EF               [12]10775 	mov	a,r7
      0047DE A3               [24]10776 	inc	dptr
      0047DF F0               [24]10777 	movx	@dptr,a
                                  10778 ;	..\src\COMMON\easyax5043.c:2034: axradio_cb_receive.st.error = AXRADIO_ERR_RESYNC;
      0047E0 90 03 1D         [24]10779 	mov	dptr,#(_axradio_cb_receive + 0x0005)
      0047E3 74 09            [12]10780 	mov	a,#0x09
      0047E5 F0               [24]10781 	movx	@dptr,a
                                  10782 ;	..\src\COMMON\easyax5043.c:2035: wtimer_add_callback(&axradio_cb_receive.cb);
      0047E6 90 03 18         [24]10783 	mov	dptr,#_axradio_cb_receive
      0047E9 12 78 5E         [24]10784 	lcall	_wtimer_add_callback
                                  10785 ;	..\src\COMMON\easyax5043.c:2036: return AXRADIO_ERR_NOERROR;
      0047EC 75 82 00         [24]10786 	mov	dpl,#0x00
                                  10787 ;	..\src\COMMON\easyax5043.c:2038: default:
      0047EF 22               [24]10788 	ret
      0047F0                      10789 00179$:
                                  10790 ;	..\src\COMMON\easyax5043.c:2039: return AXRADIO_ERR_NOTSUPPORTED;
      0047F0 75 82 01         [24]10791 	mov	dpl,#0x01
                                  10792 ;	..\src\COMMON\easyax5043.c:2040: }
                                  10793 ;	..\src\COMMON\easyax5043.c:2041: }
      0047F3 22               [24]10794 	ret
                                  10795 ;------------------------------------------------------------
                                  10796 ;Allocation info for local variables in function 'axradio_set_channel'
                                  10797 ;------------------------------------------------------------
                                  10798 ;freq                      Allocated with name '_axradio_set_channel_freq_65536_447'
                                  10799 ;rng                       Allocated to registers r7 
                                  10800 ;------------------------------------------------------------
                                  10801 ;	..\src\COMMON\easyax5043.c:2043: uint8_t axradio_set_channel(uint32_t freq)
                                  10802 ;	-----------------------------------------
                                  10803 ;	 function axradio_set_channel
                                  10804 ;	-----------------------------------------
      0047F4                      10805 _axradio_set_channel:
      0047F4 AF 82            [24]10806 	mov	r7,dpl
      0047F6 AE 83            [24]10807 	mov	r6,dph
      0047F8 AD F0            [24]10808 	mov	r5,b
      0047FA FC               [12]10809 	mov	r4,a
      0047FB 90 03 98         [24]10810 	mov	dptr,#_axradio_set_channel_freq_65536_447
      0047FE EF               [12]10811 	mov	a,r7
      0047FF F0               [24]10812 	movx	@dptr,a
      004800 EE               [12]10813 	mov	a,r6
      004801 A3               [24]10814 	inc	dptr
      004802 F0               [24]10815 	movx	@dptr,a
      004803 ED               [12]10816 	mov	a,r5
      004804 A3               [24]10817 	inc	dptr
      004805 F0               [24]10818 	movx	@dptr,a
      004806 EC               [12]10819 	mov	a,r4
      004807 A3               [24]10820 	inc	dptr
      004808 F0               [24]10821 	movx	@dptr,a
                                  10822 ;	..\src\COMMON\easyax5043.c:2049: EA = 0;
                                  10823 ;	assignBit
      004809 C2 AF            [12]10824 	clr	_EA
                                  10825 ;	..\src\COMMON\easyax5043.c:2052: rng = axradio_phy_chanpllrng[0]; // volver a poner si falla
      00480B 90 00 E6         [24]10826 	mov	dptr,#_axradio_phy_chanpllrng
      00480E E0               [24]10827 	movx	a,@dptr
      00480F FF               [12]10828 	mov	r7,a
                                  10829 ;	..\src\COMMON\easyax5043.c:2058: if (AX5043_PLLLOOP & 0x80) {
      004810 90 40 30         [24]10830 	mov	dptr,#_AX5043_PLLLOOP
      004813 E0               [24]10831 	movx	a,@dptr
      004814 30 E7 2B         [24]10832 	jnb	acc.7,00102$
                                  10833 ;	..\src\COMMON\easyax5043.c:2059: AX5043_PLLRANGINGA = rng & 0x0F;// volver a poner si falla
      004817 90 40 33         [24]10834 	mov	dptr,#_AX5043_PLLRANGINGA
      00481A 74 0F            [12]10835 	mov	a,#0x0f
      00481C 5F               [12]10836 	anl	a,r7
      00481D F0               [24]10837 	movx	@dptr,a
                                  10838 ;	..\src\COMMON\easyax5043.c:2060: AX5043_FREQA0 = freq;
      00481E 90 03 98         [24]10839 	mov	dptr,#_axradio_set_channel_freq_65536_447
      004821 E0               [24]10840 	movx	a,@dptr
      004822 FB               [12]10841 	mov	r3,a
      004823 A3               [24]10842 	inc	dptr
      004824 E0               [24]10843 	movx	a,@dptr
      004825 FC               [12]10844 	mov	r4,a
      004826 A3               [24]10845 	inc	dptr
      004827 E0               [24]10846 	movx	a,@dptr
      004828 FD               [12]10847 	mov	r5,a
      004829 A3               [24]10848 	inc	dptr
      00482A E0               [24]10849 	movx	a,@dptr
      00482B FE               [12]10850 	mov	r6,a
      00482C 90 40 37         [24]10851 	mov	dptr,#_AX5043_FREQA0
      00482F EB               [12]10852 	mov	a,r3
      004830 F0               [24]10853 	movx	@dptr,a
                                  10854 ;	..\src\COMMON\easyax5043.c:2061: AX5043_FREQA1 = freq >> 8;
      004831 90 40 36         [24]10855 	mov	dptr,#_AX5043_FREQA1
      004834 EC               [12]10856 	mov	a,r4
      004835 F0               [24]10857 	movx	@dptr,a
                                  10858 ;	..\src\COMMON\easyax5043.c:2062: AX5043_FREQA2 = freq >> 16;
      004836 90 40 35         [24]10859 	mov	dptr,#_AX5043_FREQA2
      004839 ED               [12]10860 	mov	a,r5
      00483A F0               [24]10861 	movx	@dptr,a
                                  10862 ;	..\src\COMMON\easyax5043.c:2063: AX5043_FREQA3 = freq >> 24;
      00483B 90 40 34         [24]10863 	mov	dptr,#_AX5043_FREQA3
      00483E EE               [12]10864 	mov	a,r6
      00483F F0               [24]10865 	movx	@dptr,a
      004840 80 29            [24]10866 	sjmp	00103$
      004842                      10867 00102$:
                                  10868 ;	..\src\COMMON\easyax5043.c:2065: AX5043_PLLRANGINGB = rng & 0x0F; //volver a poner si falla
      004842 90 40 3B         [24]10869 	mov	dptr,#_AX5043_PLLRANGINGB
      004845 74 0F            [12]10870 	mov	a,#0x0f
      004847 5F               [12]10871 	anl	a,r7
      004848 F0               [24]10872 	movx	@dptr,a
                                  10873 ;	..\src\COMMON\easyax5043.c:2066: AX5043_FREQB0 = freq;
      004849 90 03 98         [24]10874 	mov	dptr,#_axradio_set_channel_freq_65536_447
      00484C E0               [24]10875 	movx	a,@dptr
      00484D FC               [12]10876 	mov	r4,a
      00484E A3               [24]10877 	inc	dptr
      00484F E0               [24]10878 	movx	a,@dptr
      004850 FD               [12]10879 	mov	r5,a
      004851 A3               [24]10880 	inc	dptr
      004852 E0               [24]10881 	movx	a,@dptr
      004853 FE               [12]10882 	mov	r6,a
      004854 A3               [24]10883 	inc	dptr
      004855 E0               [24]10884 	movx	a,@dptr
      004856 FF               [12]10885 	mov	r7,a
      004857 90 40 3F         [24]10886 	mov	dptr,#_AX5043_FREQB0
      00485A EC               [12]10887 	mov	a,r4
      00485B F0               [24]10888 	movx	@dptr,a
                                  10889 ;	..\src\COMMON\easyax5043.c:2067: AX5043_FREQB1 = freq >> 8;
      00485C 90 40 3E         [24]10890 	mov	dptr,#_AX5043_FREQB1
      00485F ED               [12]10891 	mov	a,r5
      004860 F0               [24]10892 	movx	@dptr,a
                                  10893 ;	..\src\COMMON\easyax5043.c:2068: AX5043_FREQB2 = freq >> 16;
      004861 90 40 3D         [24]10894 	mov	dptr,#_AX5043_FREQB2
      004864 EE               [12]10895 	mov	a,r6
      004865 F0               [24]10896 	movx	@dptr,a
                                  10897 ;	..\src\COMMON\easyax5043.c:2069: AX5043_FREQB3 = freq >> 24;
      004866 90 40 3C         [24]10898 	mov	dptr,#_AX5043_FREQB3
      004869 EF               [12]10899 	mov	a,r7
      00486A F0               [24]10900 	movx	@dptr,a
      00486B                      10901 00103$:
                                  10902 ;	..\src\COMMON\easyax5043.c:2072: AX5043_PLLLOOP ^= 0x80;  // volver a poner si falla
      00486B 90 40 30         [24]10903 	mov	dptr,#_AX5043_PLLLOOP
      00486E E0               [24]10904 	movx	a,@dptr
      00486F FF               [12]10905 	mov	r7,a
      004870 63 07 80         [24]10906 	xrl	ar7,#0x80
      004873 90 40 30         [24]10907 	mov	dptr,#_AX5043_PLLLOOP
      004876 EF               [12]10908 	mov	a,r7
      004877 F0               [24]10909 	movx	@dptr,a
                                  10910 ;	..\src\COMMON\easyax5043.c:2073: EA = 1;
                                  10911 ;	assignBit
      004878 D2 AF            [12]10912 	setb	_EA
                                  10913 ;	..\src\COMMON\easyax5043.c:2076: return AXRADIO_ERR_NOERROR;
      00487A 75 82 00         [24]10914 	mov	dpl,#0x00
                                  10915 ;	..\src\COMMON\easyax5043.c:2077: }
      00487D 22               [24]10916 	ret
                                  10917 ;------------------------------------------------------------
                                  10918 ;Allocation info for local variables in function 'axradio_get_pllvcoi'
                                  10919 ;------------------------------------------------------------
                                  10920 ;x                         Allocated with name '_axradio_get_pllvcoi_x_131072_453'
                                  10921 ;x                         Allocated with name '_axradio_get_pllvcoi_x_131072_454'
                                  10922 ;------------------------------------------------------------
                                  10923 ;	..\src\COMMON\easyax5043.c:2079: uint8_t axradio_get_pllvcoi(void)
                                  10924 ;	-----------------------------------------
                                  10925 ;	 function axradio_get_pllvcoi
                                  10926 ;	-----------------------------------------
      00487E                      10927 _axradio_get_pllvcoi:
                                  10928 ;	..\src\COMMON\easyax5043.c:2081: if (axradio_phy_vcocalib) {
      00487E 90 90 C8         [24]10929 	mov	dptr,#_axradio_phy_vcocalib
      004881 E4               [12]10930 	clr	a
      004882 93               [24]10931 	movc	a,@a+dptr
      004883 60 15            [24]10932 	jz	00104$
                                  10933 ;	..\src\COMMON\easyax5043.c:2082: uint8_t x = axradio_phy_chanvcoi[axradio_curchannel];
      004885 90 00 EF         [24]10934 	mov	dptr,#_axradio_curchannel
      004888 E0               [24]10935 	movx	a,@dptr
      004889 24 E7            [12]10936 	add	a,#_axradio_phy_chanvcoi
      00488B F5 82            [12]10937 	mov	dpl,a
      00488D E4               [12]10938 	clr	a
      00488E 34 00            [12]10939 	addc	a,#(_axradio_phy_chanvcoi >> 8)
      004890 F5 83            [12]10940 	mov	dph,a
      004892 E0               [24]10941 	movx	a,@dptr
                                  10942 ;	..\src\COMMON\easyax5043.c:2083: if (x & 0x80)
      004893 FF               [12]10943 	mov	r7,a
      004894 30 E7 03         [24]10944 	jnb	acc.7,00104$
                                  10945 ;	..\src\COMMON\easyax5043.c:2084: return x;
      004897 8F 82            [24]10946 	mov	dpl,r7
      004899 22               [24]10947 	ret
      00489A                      10948 00104$:
                                  10949 ;	..\src\COMMON\easyax5043.c:2087: uint8_t x = axradio_phy_chanvcoiinit[axradio_curchannel];
      00489A 90 00 EF         [24]10950 	mov	dptr,#_axradio_curchannel
      00489D E0               [24]10951 	movx	a,@dptr
      00489E FF               [12]10952 	mov	r7,a
      00489F 90 90 C7         [24]10953 	mov	dptr,#_axradio_phy_chanvcoiinit
      0048A2 93               [24]10954 	movc	a,@a+dptr
      0048A3 FE               [12]10955 	mov	r6,a
      0048A4 90 03 9C         [24]10956 	mov	dptr,#_axradio_get_pllvcoi_x_131072_454
      0048A7 F0               [24]10957 	movx	@dptr,a
                                  10958 ;	..\src\COMMON\easyax5043.c:2088: if (x & 0x80) {
      0048A8 EE               [12]10959 	mov	a,r6
      0048A9 30 E7 38         [24]10960 	jnb	acc.7,00108$
                                  10961 ;	..\src\COMMON\easyax5043.c:2089: if (!(axradio_phy_chanpllrnginit[0] & 0xF0)) {
      0048AC 90 90 C6         [24]10962 	mov	dptr,#_axradio_phy_chanpllrnginit
      0048AF E4               [12]10963 	clr	a
      0048B0 93               [24]10964 	movc	a,@a+dptr
      0048B1 FD               [12]10965 	mov	r5,a
      0048B2 54 F0            [12]10966 	anl	a,#0xf0
      0048B4 60 02            [24]10967 	jz	00131$
      0048B6 80 25            [24]10968 	sjmp	00106$
      0048B8                      10969 00131$:
                                  10970 ;	..\src\COMMON\easyax5043.c:2090: x += (axradio_phy_chanpllrng[axradio_curchannel] & 0x0F) - (axradio_phy_chanpllrnginit[axradio_curchannel] & 0x0F);
      0048B8 EF               [12]10971 	mov	a,r7
      0048B9 24 E6            [12]10972 	add	a,#_axradio_phy_chanpllrng
      0048BB F5 82            [12]10973 	mov	dpl,a
      0048BD E4               [12]10974 	clr	a
      0048BE 34 00            [12]10975 	addc	a,#(_axradio_phy_chanpllrng >> 8)
      0048C0 F5 83            [12]10976 	mov	dph,a
      0048C2 E0               [24]10977 	movx	a,@dptr
      0048C3 FD               [12]10978 	mov	r5,a
      0048C4 53 05 0F         [24]10979 	anl	ar5,#0x0f
      0048C7 EF               [12]10980 	mov	a,r7
      0048C8 90 90 C6         [24]10981 	mov	dptr,#_axradio_phy_chanpllrnginit
      0048CB 93               [24]10982 	movc	a,@a+dptr
      0048CC FF               [12]10983 	mov	r7,a
      0048CD 74 0F            [12]10984 	mov	a,#0x0f
      0048CF 5F               [12]10985 	anl	a,r7
      0048D0 D3               [12]10986 	setb	c
      0048D1 9D               [12]10987 	subb	a,r5
      0048D2 F4               [12]10988 	cpl	a
      0048D3 FD               [12]10989 	mov	r5,a
      0048D4 2E               [12]10990 	add	a,r6
                                  10991 ;	..\src\COMMON\easyax5043.c:2091: x &= 0x3f;
      0048D5 54 3F            [12]10992 	anl	a,#0x3f
                                  10993 ;	..\src\COMMON\easyax5043.c:2092: x |= 0x80;
      0048D7 90 03 9C         [24]10994 	mov	dptr,#_axradio_get_pllvcoi_x_131072_454
      0048DA 44 80            [12]10995 	orl	a,#0x80
      0048DC F0               [24]10996 	movx	@dptr,a
      0048DD                      10997 00106$:
                                  10998 ;	..\src\COMMON\easyax5043.c:2094: return x;
      0048DD 90 03 9C         [24]10999 	mov	dptr,#_axradio_get_pllvcoi_x_131072_454
      0048E0 E0               [24]11000 	movx	a,@dptr
      0048E1 F5 82            [12]11001 	mov	dpl,a
      0048E3 22               [24]11002 	ret
      0048E4                      11003 00108$:
                                  11004 ;	..\src\COMMON\easyax5043.c:2097: return AX5043_PLLVCOI;
      0048E4 90 41 80         [24]11005 	mov	dptr,#_AX5043_PLLVCOI
      0048E7 E0               [24]11006 	movx	a,@dptr
                                  11007 ;	..\src\COMMON\easyax5043.c:2098: }
      0048E8 F5 82            [12]11008 	mov	dpl,a
      0048EA 22               [24]11009 	ret
                                  11010 ;------------------------------------------------------------
                                  11011 ;Allocation info for local variables in function 'axradio_set_curfreqoffset'
                                  11012 ;------------------------------------------------------------
                                  11013 ;offs                      Allocated with name '_axradio_set_curfreqoffset_offs_65536_457'
                                  11014 ;------------------------------------------------------------
                                  11015 ;	..\src\COMMON\easyax5043.c:2100: static uint8_t axradio_set_curfreqoffset(int32_t offs)
                                  11016 ;	-----------------------------------------
                                  11017 ;	 function axradio_set_curfreqoffset
                                  11018 ;	-----------------------------------------
      0048EB                      11019 _axradio_set_curfreqoffset:
      0048EB AF 82            [24]11020 	mov	r7,dpl
      0048ED AE 83            [24]11021 	mov	r6,dph
      0048EF AD F0            [24]11022 	mov	r5,b
      0048F1 FC               [12]11023 	mov	r4,a
      0048F2 90 03 9D         [24]11024 	mov	dptr,#_axradio_set_curfreqoffset_offs_65536_457
      0048F5 EF               [12]11025 	mov	a,r7
      0048F6 F0               [24]11026 	movx	@dptr,a
      0048F7 EE               [12]11027 	mov	a,r6
      0048F8 A3               [24]11028 	inc	dptr
      0048F9 F0               [24]11029 	movx	@dptr,a
      0048FA ED               [12]11030 	mov	a,r5
      0048FB A3               [24]11031 	inc	dptr
      0048FC F0               [24]11032 	movx	@dptr,a
      0048FD EC               [12]11033 	mov	a,r4
      0048FE A3               [24]11034 	inc	dptr
      0048FF F0               [24]11035 	movx	@dptr,a
                                  11036 ;	..\src\COMMON\easyax5043.c:2102: axradio_curfreqoffset = offs;
      004900 90 03 9D         [24]11037 	mov	dptr,#_axradio_set_curfreqoffset_offs_65536_457
      004903 E0               [24]11038 	movx	a,@dptr
      004904 FC               [12]11039 	mov	r4,a
      004905 A3               [24]11040 	inc	dptr
      004906 E0               [24]11041 	movx	a,@dptr
      004907 FD               [12]11042 	mov	r5,a
      004908 A3               [24]11043 	inc	dptr
      004909 E0               [24]11044 	movx	a,@dptr
      00490A FE               [12]11045 	mov	r6,a
      00490B A3               [24]11046 	inc	dptr
      00490C E0               [24]11047 	movx	a,@dptr
      00490D FF               [12]11048 	mov	r7,a
      00490E 90 00 F0         [24]11049 	mov	dptr,#_axradio_curfreqoffset
      004911 EC               [12]11050 	mov	a,r4
      004912 F0               [24]11051 	movx	@dptr,a
      004913 ED               [12]11052 	mov	a,r5
      004914 A3               [24]11053 	inc	dptr
      004915 F0               [24]11054 	movx	@dptr,a
      004916 EE               [12]11055 	mov	a,r6
      004917 A3               [24]11056 	inc	dptr
      004918 F0               [24]11057 	movx	@dptr,a
      004919 EF               [12]11058 	mov	a,r7
      00491A A3               [24]11059 	inc	dptr
      00491B F0               [24]11060 	movx	@dptr,a
                                  11061 ;	..\src\COMMON\easyax5043.c:2103: if (checksignedlimit32(offs, axradio_phy_maxfreqoffset))
      00491C 90 90 C9         [24]11062 	mov	dptr,#_axradio_phy_maxfreqoffset
      00491F E4               [12]11063 	clr	a
      004920 93               [24]11064 	movc	a,@a+dptr
      004921 C0 E0            [24]11065 	push	acc
      004923 74 01            [12]11066 	mov	a,#0x01
      004925 93               [24]11067 	movc	a,@a+dptr
      004926 C0 E0            [24]11068 	push	acc
      004928 74 02            [12]11069 	mov	a,#0x02
      00492A 93               [24]11070 	movc	a,@a+dptr
      00492B C0 E0            [24]11071 	push	acc
      00492D 74 03            [12]11072 	mov	a,#0x03
      00492F 93               [24]11073 	movc	a,@a+dptr
      004930 C0 E0            [24]11074 	push	acc
      004932 8C 82            [24]11075 	mov	dpl,r4
      004934 8D 83            [24]11076 	mov	dph,r5
      004936 8E F0            [24]11077 	mov	b,r6
      004938 EF               [12]11078 	mov	a,r7
      004939 12 81 56         [24]11079 	lcall	_checksignedlimit32
      00493C AF 82            [24]11080 	mov	r7,dpl
      00493E E5 81            [12]11081 	mov	a,sp
      004940 24 FC            [12]11082 	add	a,#0xfc
      004942 F5 81            [12]11083 	mov	sp,a
      004944 EF               [12]11084 	mov	a,r7
      004945 60 04            [24]11085 	jz	00102$
                                  11086 ;	..\src\COMMON\easyax5043.c:2104: return AXRADIO_ERR_NOERROR;
      004947 75 82 00         [24]11087 	mov	dpl,#0x00
      00494A 22               [24]11088 	ret
      00494B                      11089 00102$:
                                  11090 ;	..\src\COMMON\easyax5043.c:2105: if (axradio_curfreqoffset < 0)
      00494B 90 00 F0         [24]11091 	mov	dptr,#_axradio_curfreqoffset
      00494E E0               [24]11092 	movx	a,@dptr
      00494F A3               [24]11093 	inc	dptr
      004950 E0               [24]11094 	movx	a,@dptr
      004951 A3               [24]11095 	inc	dptr
      004952 E0               [24]11096 	movx	a,@dptr
      004953 A3               [24]11097 	inc	dptr
      004954 E0               [24]11098 	movx	a,@dptr
      004955 30 E7 27         [24]11099 	jnb	acc.7,00104$
                                  11100 ;	..\src\COMMON\easyax5043.c:2106: axradio_curfreqoffset = -axradio_phy_maxfreqoffset;
      004958 90 90 C9         [24]11101 	mov	dptr,#_axradio_phy_maxfreqoffset
      00495B E4               [12]11102 	clr	a
      00495C 93               [24]11103 	movc	a,@a+dptr
      00495D FC               [12]11104 	mov	r4,a
      00495E 74 01            [12]11105 	mov	a,#0x01
      004960 93               [24]11106 	movc	a,@a+dptr
      004961 FD               [12]11107 	mov	r5,a
      004962 74 02            [12]11108 	mov	a,#0x02
      004964 93               [24]11109 	movc	a,@a+dptr
      004965 FE               [12]11110 	mov	r6,a
      004966 74 03            [12]11111 	mov	a,#0x03
      004968 93               [24]11112 	movc	a,@a+dptr
      004969 FF               [12]11113 	mov	r7,a
      00496A 90 00 F0         [24]11114 	mov	dptr,#_axradio_curfreqoffset
      00496D C3               [12]11115 	clr	c
      00496E E4               [12]11116 	clr	a
      00496F 9C               [12]11117 	subb	a,r4
      004970 F0               [24]11118 	movx	@dptr,a
      004971 E4               [12]11119 	clr	a
      004972 9D               [12]11120 	subb	a,r5
      004973 A3               [24]11121 	inc	dptr
      004974 F0               [24]11122 	movx	@dptr,a
      004975 E4               [12]11123 	clr	a
      004976 9E               [12]11124 	subb	a,r6
      004977 A3               [24]11125 	inc	dptr
      004978 F0               [24]11126 	movx	@dptr,a
      004979 E4               [12]11127 	clr	a
      00497A 9F               [12]11128 	subb	a,r7
      00497B A3               [24]11129 	inc	dptr
      00497C F0               [24]11130 	movx	@dptr,a
      00497D 80 20            [24]11131 	sjmp	00105$
      00497F                      11132 00104$:
                                  11133 ;	..\src\COMMON\easyax5043.c:2108: axradio_curfreqoffset = axradio_phy_maxfreqoffset;
      00497F 90 90 C9         [24]11134 	mov	dptr,#_axradio_phy_maxfreqoffset
      004982 E4               [12]11135 	clr	a
      004983 93               [24]11136 	movc	a,@a+dptr
      004984 FC               [12]11137 	mov	r4,a
      004985 74 01            [12]11138 	mov	a,#0x01
      004987 93               [24]11139 	movc	a,@a+dptr
      004988 FD               [12]11140 	mov	r5,a
      004989 74 02            [12]11141 	mov	a,#0x02
      00498B 93               [24]11142 	movc	a,@a+dptr
      00498C FE               [12]11143 	mov	r6,a
      00498D 74 03            [12]11144 	mov	a,#0x03
      00498F 93               [24]11145 	movc	a,@a+dptr
      004990 FF               [12]11146 	mov	r7,a
      004991 90 00 F0         [24]11147 	mov	dptr,#_axradio_curfreqoffset
      004994 EC               [12]11148 	mov	a,r4
      004995 F0               [24]11149 	movx	@dptr,a
      004996 ED               [12]11150 	mov	a,r5
      004997 A3               [24]11151 	inc	dptr
      004998 F0               [24]11152 	movx	@dptr,a
      004999 EE               [12]11153 	mov	a,r6
      00499A A3               [24]11154 	inc	dptr
      00499B F0               [24]11155 	movx	@dptr,a
      00499C EF               [12]11156 	mov	a,r7
      00499D A3               [24]11157 	inc	dptr
      00499E F0               [24]11158 	movx	@dptr,a
      00499F                      11159 00105$:
                                  11160 ;	..\src\COMMON\easyax5043.c:2109: return AXRADIO_ERR_INVALID;
      00499F 75 82 04         [24]11161 	mov	dpl,#0x04
                                  11162 ;	..\src\COMMON\easyax5043.c:2110: }
      0049A2 22               [24]11163 	ret
                                  11164 ;------------------------------------------------------------
                                  11165 ;Allocation info for local variables in function 'axradio_set_local_address'
                                  11166 ;------------------------------------------------------------
                                  11167 ;addr                      Allocated with name '_axradio_set_local_address_addr_65536_459'
                                  11168 ;------------------------------------------------------------
                                  11169 ;	..\src\COMMON\easyax5043.c:2115: void axradio_set_local_address(const struct axradio_address_mask __generic *addr)
                                  11170 ;	-----------------------------------------
                                  11171 ;	 function axradio_set_local_address
                                  11172 ;	-----------------------------------------
      0049A3                      11173 _axradio_set_local_address:
      0049A3 AF F0            [24]11174 	mov	r7,b
      0049A5 AE 83            [24]11175 	mov	r6,dph
      0049A7 E5 82            [12]11176 	mov	a,dpl
      0049A9 90 03 A1         [24]11177 	mov	dptr,#_axradio_set_local_address_addr_65536_459
      0049AC F0               [24]11178 	movx	@dptr,a
      0049AD EE               [12]11179 	mov	a,r6
      0049AE A3               [24]11180 	inc	dptr
      0049AF F0               [24]11181 	movx	@dptr,a
      0049B0 EF               [12]11182 	mov	a,r7
      0049B1 A3               [24]11183 	inc	dptr
      0049B2 F0               [24]11184 	movx	@dptr,a
                                  11185 ;	..\src\COMMON\easyax5043.c:2117: memcpy_xdatageneric(&axradio_localaddr, addr, sizeof(axradio_localaddr));
      0049B3 90 03 A1         [24]11186 	mov	dptr,#_axradio_set_local_address_addr_65536_459
      0049B6 E0               [24]11187 	movx	a,@dptr
      0049B7 FD               [12]11188 	mov	r5,a
      0049B8 A3               [24]11189 	inc	dptr
      0049B9 E0               [24]11190 	movx	a,@dptr
      0049BA FE               [12]11191 	mov	r6,a
      0049BB A3               [24]11192 	inc	dptr
      0049BC E0               [24]11193 	movx	a,@dptr
      0049BD FF               [12]11194 	mov	r7,a
      0049BE 90 04 6D         [24]11195 	mov	dptr,#_memcpy_PARM_2
      0049C1 ED               [12]11196 	mov	a,r5
      0049C2 F0               [24]11197 	movx	@dptr,a
      0049C3 EE               [12]11198 	mov	a,r6
      0049C4 A3               [24]11199 	inc	dptr
      0049C5 F0               [24]11200 	movx	@dptr,a
      0049C6 EF               [12]11201 	mov	a,r7
      0049C7 A3               [24]11202 	inc	dptr
      0049C8 F0               [24]11203 	movx	@dptr,a
      0049C9 90 04 70         [24]11204 	mov	dptr,#_memcpy_PARM_3
      0049CC 74 08            [12]11205 	mov	a,#0x08
      0049CE F0               [24]11206 	movx	@dptr,a
      0049CF E4               [12]11207 	clr	a
      0049D0 A3               [24]11208 	inc	dptr
      0049D1 F0               [24]11209 	movx	@dptr,a
      0049D2 90 01 04         [24]11210 	mov	dptr,#_axradio_localaddr
      0049D5 75 F0 00         [24]11211 	mov	b,#0x00
      0049D8 12 78 14         [24]11212 	lcall	_memcpy
                                  11213 ;	..\src\COMMON\easyax5043.c:2118: axradio_setaddrregs();
                                  11214 ;	..\src\COMMON\easyax5043.c:2119: }
      0049DB 02 2D 40         [24]11215 	ljmp	_axradio_setaddrregs
                                  11216 ;------------------------------------------------------------
                                  11217 ;Allocation info for local variables in function 'axradio_set_default_remote_address'
                                  11218 ;------------------------------------------------------------
                                  11219 ;addr                      Allocated with name '_axradio_set_default_remote_address_addr_65536_461'
                                  11220 ;------------------------------------------------------------
                                  11221 ;	..\src\COMMON\easyax5043.c:2122: void axradio_set_default_remote_address(const struct axradio_address __generic *addr)
                                  11222 ;	-----------------------------------------
                                  11223 ;	 function axradio_set_default_remote_address
                                  11224 ;	-----------------------------------------
      0049DE                      11225 _axradio_set_default_remote_address:
      0049DE AF F0            [24]11226 	mov	r7,b
      0049E0 AE 83            [24]11227 	mov	r6,dph
      0049E2 E5 82            [12]11228 	mov	a,dpl
      0049E4 90 03 A4         [24]11229 	mov	dptr,#_axradio_set_default_remote_address_addr_65536_461
      0049E7 F0               [24]11230 	movx	@dptr,a
      0049E8 EE               [12]11231 	mov	a,r6
      0049E9 A3               [24]11232 	inc	dptr
      0049EA F0               [24]11233 	movx	@dptr,a
      0049EB EF               [12]11234 	mov	a,r7
      0049EC A3               [24]11235 	inc	dptr
      0049ED F0               [24]11236 	movx	@dptr,a
                                  11237 ;	..\src\COMMON\easyax5043.c:2124: memcpy_xdatageneric(&axradio_default_remoteaddr, addr, sizeof(axradio_default_remoteaddr));
      0049EE 90 03 A4         [24]11238 	mov	dptr,#_axradio_set_default_remote_address_addr_65536_461
      0049F1 E0               [24]11239 	movx	a,@dptr
      0049F2 FD               [12]11240 	mov	r5,a
      0049F3 A3               [24]11241 	inc	dptr
      0049F4 E0               [24]11242 	movx	a,@dptr
      0049F5 FE               [12]11243 	mov	r6,a
      0049F6 A3               [24]11244 	inc	dptr
      0049F7 E0               [24]11245 	movx	a,@dptr
      0049F8 FF               [12]11246 	mov	r7,a
      0049F9 90 04 6D         [24]11247 	mov	dptr,#_memcpy_PARM_2
      0049FC ED               [12]11248 	mov	a,r5
      0049FD F0               [24]11249 	movx	@dptr,a
      0049FE EE               [12]11250 	mov	a,r6
      0049FF A3               [24]11251 	inc	dptr
      004A00 F0               [24]11252 	movx	@dptr,a
      004A01 EF               [12]11253 	mov	a,r7
      004A02 A3               [24]11254 	inc	dptr
      004A03 F0               [24]11255 	movx	@dptr,a
      004A04 90 04 70         [24]11256 	mov	dptr,#_memcpy_PARM_3
      004A07 74 04            [12]11257 	mov	a,#0x04
      004A09 F0               [24]11258 	movx	@dptr,a
      004A0A E4               [12]11259 	clr	a
      004A0B A3               [24]11260 	inc	dptr
      004A0C F0               [24]11261 	movx	@dptr,a
      004A0D 90 01 0C         [24]11262 	mov	dptr,#_axradio_default_remoteaddr
      004A10 75 F0 00         [24]11263 	mov	b,#0x00
                                  11264 ;	..\src\COMMON\easyax5043.c:2125: }
      004A13 02 78 14         [24]11265 	ljmp	_memcpy
                                  11266 ;------------------------------------------------------------
                                  11267 ;Allocation info for local variables in function 'axradio_transmit'
                                  11268 ;------------------------------------------------------------
                                  11269 ;fifofree                  Allocated to registers r6 r7 
                                  11270 ;i                         Allocated to registers r4 
                                  11271 ;iesave                    Allocated to registers r7 
                                  11272 ;len_byte                  Allocated to registers r6 
                                  11273 ;pkt                       Allocated with name '_axradio_transmit_PARM_2'
                                  11274 ;pktlen                    Allocated with name '_axradio_transmit_PARM_3'
                                  11275 ;addr                      Allocated with name '_axradio_transmit_addr_65536_463'
                                  11276 ;------------------------------------------------------------
                                  11277 ;	..\src\COMMON\easyax5043.c:2128: uint8_t axradio_transmit(const struct axradio_address __generic *addr, const uint8_t __generic *pkt, uint16_t pktlen)
                                  11278 ;	-----------------------------------------
                                  11279 ;	 function axradio_transmit
                                  11280 ;	-----------------------------------------
      004A16                      11281 _axradio_transmit:
      004A16 AF F0            [24]11282 	mov	r7,b
      004A18 AE 83            [24]11283 	mov	r6,dph
      004A1A E5 82            [12]11284 	mov	a,dpl
      004A1C 90 03 AC         [24]11285 	mov	dptr,#_axradio_transmit_addr_65536_463
      004A1F F0               [24]11286 	movx	@dptr,a
      004A20 EE               [12]11287 	mov	a,r6
      004A21 A3               [24]11288 	inc	dptr
      004A22 F0               [24]11289 	movx	@dptr,a
      004A23 EF               [12]11290 	mov	a,r7
      004A24 A3               [24]11291 	inc	dptr
      004A25 F0               [24]11292 	movx	@dptr,a
                                  11293 ;	..\src\COMMON\easyax5043.c:2130: switch (axradio_mode) {
      004A26 AF 17            [24]11294 	mov	r7,_axradio_mode
      004A28 BF 10 03         [24]11295 	cjne	r7,#0x10,00315$
      004A2B 02 4B 17         [24]11296 	ljmp	00125$
      004A2E                      11297 00315$:
      004A2E BF 11 03         [24]11298 	cjne	r7,#0x11,00316$
      004A31 02 4B 17         [24]11299 	ljmp	00125$
      004A34                      11300 00316$:
      004A34 BF 12 03         [24]11301 	cjne	r7,#0x12,00317$
      004A37 02 4B 17         [24]11302 	ljmp	00125$
      004A3A                      11303 00317$:
      004A3A BF 13 03         [24]11304 	cjne	r7,#0x13,00318$
      004A3D 02 4B 17         [24]11305 	ljmp	00125$
      004A40                      11306 00318$:
      004A40 BF 18 02         [24]11307 	cjne	r7,#0x18,00319$
      004A43 80 2F            [24]11308 	sjmp	00105$
      004A45                      11309 00319$:
      004A45 BF 19 02         [24]11310 	cjne	r7,#0x19,00320$
      004A48 80 2A            [24]11311 	sjmp	00105$
      004A4A                      11312 00320$:
      004A4A BF 1A 02         [24]11313 	cjne	r7,#0x1a,00321$
      004A4D 80 25            [24]11314 	sjmp	00105$
      004A4F                      11315 00321$:
      004A4F BF 1B 02         [24]11316 	cjne	r7,#0x1b,00322$
      004A52 80 20            [24]11317 	sjmp	00105$
      004A54                      11318 00322$:
      004A54 BF 1C 02         [24]11319 	cjne	r7,#0x1c,00323$
      004A57 80 1B            [24]11320 	sjmp	00105$
      004A59                      11321 00323$:
      004A59 BF 20 03         [24]11322 	cjne	r7,#0x20,00324$
      004A5C 02 4A EC         [24]11323 	ljmp	00116$
      004A5F                      11324 00324$:
      004A5F BF 21 03         [24]11325 	cjne	r7,#0x21,00325$
      004A62 02 4A EC         [24]11326 	ljmp	00116$
      004A65                      11327 00325$:
      004A65 BF 30 03         [24]11328 	cjne	r7,#0x30,00326$
      004A68 02 4B 21         [24]11329 	ljmp	00128$
      004A6B                      11330 00326$:
      004A6B BF 31 03         [24]11331 	cjne	r7,#0x31,00327$
      004A6E 02 4B 21         [24]11332 	ljmp	00128$
      004A71                      11333 00327$:
      004A71 02 4D B4         [24]11334 	ljmp	00162$
                                  11335 ;	..\src\COMMON\easyax5043.c:2135: case AXRADIO_MODE_STREAM_TRANSMIT_SCRAM_LSB:
      004A74                      11336 00105$:
                                  11337 ;	..\src\COMMON\easyax5043.c:2137: uint16_t __autodata fifofree = radio_read16((uint16_t)&AX5043_FIFOFREE1);
      004A74 7E 2C            [12]11338 	mov	r6,#_AX5043_FIFOFREE1
      004A76 7F 40            [12]11339 	mov	r7,#(_AX5043_FIFOFREE1 >> 8)
      004A78 8E 82            [24]11340 	mov	dpl,r6
      004A7A 8F 83            [24]11341 	mov	dph,r7
      004A7C 12 7A F3         [24]11342 	lcall	_radio_read16
      004A7F AE 82            [24]11343 	mov	r6,dpl
      004A81 AF 83            [24]11344 	mov	r7,dph
                                  11345 ;	..\src\COMMON\easyax5043.c:2138: if (fifofree < pktlen + 3)
      004A83 90 03 AA         [24]11346 	mov	dptr,#_axradio_transmit_PARM_3
      004A86 E0               [24]11347 	movx	a,@dptr
      004A87 FC               [12]11348 	mov	r4,a
      004A88 A3               [24]11349 	inc	dptr
      004A89 E0               [24]11350 	movx	a,@dptr
      004A8A FD               [12]11351 	mov	r5,a
      004A8B 74 03            [12]11352 	mov	a,#0x03
      004A8D 2C               [12]11353 	add	a,r4
      004A8E FA               [12]11354 	mov	r2,a
      004A8F E4               [12]11355 	clr	a
      004A90 3D               [12]11356 	addc	a,r5
      004A91 FB               [12]11357 	mov	r3,a
      004A92 C3               [12]11358 	clr	c
      004A93 EE               [12]11359 	mov	a,r6
      004A94 9A               [12]11360 	subb	a,r2
      004A95 EF               [12]11361 	mov	a,r7
      004A96 9B               [12]11362 	subb	a,r3
      004A97 50 04            [24]11363 	jnc	00107$
                                  11364 ;	..\src\COMMON\easyax5043.c:2139: return AXRADIO_ERR_INVALID;
      004A99 75 82 04         [24]11365 	mov	dpl,#0x04
      004A9C 22               [24]11366 	ret
      004A9D                      11367 00107$:
                                  11368 ;	..\src\COMMON\easyax5043.c:2141: if (pktlen) {
      004A9D EC               [12]11369 	mov	a,r4
      004A9E 4D               [12]11370 	orl	a,r5
      004A9F 60 2F            [24]11371 	jz	00112$
                                  11372 ;	..\src\COMMON\easyax5043.c:2142: uint8_t __autodata i = pktlen;
                                  11373 ;	..\src\COMMON\easyax5043.c:2143: AX5043_FIFODATA = AX5043_FIFOCMD_DATA | (7 << 5);
      004AA1 90 40 29         [24]11374 	mov	dptr,#_AX5043_FIFODATA
      004AA4 74 E1            [12]11375 	mov	a,#0xe1
      004AA6 F0               [24]11376 	movx	@dptr,a
                                  11377 ;	..\src\COMMON\easyax5043.c:2144: AX5043_FIFODATA = i + 1;
      004AA7 8C 07            [24]11378 	mov	ar7,r4
      004AA9 EF               [12]11379 	mov	a,r7
      004AAA 04               [12]11380 	inc	a
      004AAB F0               [24]11381 	movx	@dptr,a
                                  11382 ;	..\src\COMMON\easyax5043.c:2145: AX5043_FIFODATA = 0x08;
      004AAC 74 08            [12]11383 	mov	a,#0x08
      004AAE F0               [24]11384 	movx	@dptr,a
                                  11385 ;	..\src\COMMON\easyax5043.c:2146: do {
      004AAF 90 03 A7         [24]11386 	mov	dptr,#_axradio_transmit_PARM_2
      004AB2 E0               [24]11387 	movx	a,@dptr
      004AB3 FD               [12]11388 	mov	r5,a
      004AB4 A3               [24]11389 	inc	dptr
      004AB5 E0               [24]11390 	movx	a,@dptr
      004AB6 FE               [12]11391 	mov	r6,a
      004AB7 A3               [24]11392 	inc	dptr
      004AB8 E0               [24]11393 	movx	a,@dptr
      004AB9 FF               [12]11394 	mov	r7,a
      004ABA                      11395 00108$:
                                  11396 ;	..\src\COMMON\easyax5043.c:2147: AX5043_FIFODATA = *pkt++;
      004ABA 8D 82            [24]11397 	mov	dpl,r5
      004ABC 8E 83            [24]11398 	mov	dph,r6
      004ABE 8F F0            [24]11399 	mov	b,r7
      004AC0 12 8D 8E         [24]11400 	lcall	__gptrget
      004AC3 FB               [12]11401 	mov	r3,a
      004AC4 A3               [24]11402 	inc	dptr
      004AC5 AD 82            [24]11403 	mov	r5,dpl
      004AC7 AE 83            [24]11404 	mov	r6,dph
      004AC9 90 40 29         [24]11405 	mov	dptr,#_AX5043_FIFODATA
      004ACC EB               [12]11406 	mov	a,r3
      004ACD F0               [24]11407 	movx	@dptr,a
                                  11408 ;	..\src\COMMON\easyax5043.c:2148: } while (--i);
      004ACE DC EA            [24]11409 	djnz	r4,00108$
      004AD0                      11410 00112$:
                                  11411 ;	..\src\COMMON\easyax5043.c:2150: AX5043_FIFOSTAT =  4; // FIFO commit
      004AD0 90 40 28         [24]11412 	mov	dptr,#_AX5043_FIFOSTAT
      004AD3 74 04            [12]11413 	mov	a,#0x04
      004AD5 F0               [24]11414 	movx	@dptr,a
                                  11415 ;	..\src\COMMON\easyax5043.c:2152: uint8_t __autodata iesave = IE & 0x80;
      004AD6 E5 A8            [12]11416 	mov	a,_IE
      004AD8 54 80            [12]11417 	anl	a,#0x80
      004ADA FF               [12]11418 	mov	r7,a
                                  11419 ;	..\src\COMMON\easyax5043.c:2153: EA = 0;
                                  11420 ;	assignBit
      004ADB C2 AF            [12]11421 	clr	_EA
                                  11422 ;	..\src\COMMON\easyax5043.c:2154: AX5043_IRQMASK0 |= 0x08;
      004ADD 90 40 07         [24]11423 	mov	dptr,#_AX5043_IRQMASK0
      004AE0 E0               [24]11424 	movx	a,@dptr
      004AE1 43 E0 08         [24]11425 	orl	acc,#0x08
      004AE4 F0               [24]11426 	movx	@dptr,a
                                  11427 ;	..\src\COMMON\easyax5043.c:2155: IE |= iesave;
      004AE5 EF               [12]11428 	mov	a,r7
      004AE6 42 A8            [12]11429 	orl	_IE,a
                                  11430 ;	..\src\COMMON\easyax5043.c:2157: return AXRADIO_ERR_NOERROR;
      004AE8 75 82 00         [24]11431 	mov	dpl,#0x00
      004AEB 22               [24]11432 	ret
                                  11433 ;	..\src\COMMON\easyax5043.c:2164: case AXRADIO_MODE_WOR_RECEIVE:
      004AEC                      11434 00116$:
                                  11435 ;	..\src\COMMON\easyax5043.c:2165: if (axradio_syncstate != syncstate_off)
      004AEC 90 00 EA         [24]11436 	mov	dptr,#_axradio_syncstate
      004AEF E0               [24]11437 	movx	a,@dptr
      004AF0 60 04            [24]11438 	jz	00118$
                                  11439 ;	..\src\COMMON\easyax5043.c:2166: return AXRADIO_ERR_BUSY;
      004AF2 75 82 02         [24]11440 	mov	dpl,#0x02
      004AF5 22               [24]11441 	ret
      004AF6                      11442 00118$:
                                  11443 ;	..\src\COMMON\easyax5043.c:2167: AX5043_IRQMASK1 = 0x00;
      004AF6 90 40 06         [24]11444 	mov	dptr,#_AX5043_IRQMASK1
      004AF9 E4               [12]11445 	clr	a
      004AFA F0               [24]11446 	movx	@dptr,a
                                  11447 ;	..\src\COMMON\easyax5043.c:2168: AX5043_IRQMASK0 = 0x00;
      004AFB 90 40 07         [24]11448 	mov	dptr,#_AX5043_IRQMASK0
      004AFE F0               [24]11449 	movx	@dptr,a
                                  11450 ;	..\src\COMMON\easyax5043.c:2169: AX5043_PWRMODE = AX5043_PWRSTATE_XTAL_ON;
      004AFF 90 40 02         [24]11451 	mov	dptr,#_AX5043_PWRMODE
      004B02 74 05            [12]11452 	mov	a,#0x05
      004B04 F0               [24]11453 	movx	@dptr,a
                                  11454 ;	..\src\COMMON\easyax5043.c:2170: AX5043_FIFOSTAT = 3;
      004B05 90 40 28         [24]11455 	mov	dptr,#_AX5043_FIFOSTAT
      004B08 74 03            [12]11456 	mov	a,#0x03
      004B0A F0               [24]11457 	movx	@dptr,a
                                  11458 ;	..\src\COMMON\easyax5043.c:2171: while (AX5043_POWSTAT & 0x08);
      004B0B                      11459 00119$:
      004B0B 90 40 03         [24]11460 	mov	dptr,#_AX5043_POWSTAT
      004B0E E0               [24]11461 	movx	a,@dptr
      004B0F 20 E3 F9         [24]11462 	jb	acc.3,00119$
                                  11463 ;	..\src\COMMON\easyax5043.c:2172: ax5043_init_registers_tx();
      004B12 12 1F 4A         [24]11464 	lcall	_ax5043_init_registers_tx
                                  11465 ;	..\src\COMMON\easyax5043.c:2173: goto dotx;
                                  11466 ;	..\src\COMMON\easyax5043.c:2178: case AXRADIO_MODE_WOR_ACK_TRANSMIT:
      004B15 80 0A            [24]11467 	sjmp	00128$
      004B17                      11468 00125$:
                                  11469 ;	..\src\COMMON\easyax5043.c:2179: if (axradio_syncstate != syncstate_off)
      004B17 90 00 EA         [24]11470 	mov	dptr,#_axradio_syncstate
      004B1A E0               [24]11471 	movx	a,@dptr
      004B1B 60 04            [24]11472 	jz	00128$
                                  11473 ;	..\src\COMMON\easyax5043.c:2180: return AXRADIO_ERR_BUSY;
      004B1D 75 82 02         [24]11474 	mov	dpl,#0x02
      004B20 22               [24]11475 	ret
                                  11476 ;	..\src\COMMON\easyax5043.c:2181: dotx:
      004B21                      11477 00128$:
                                  11478 ;	..\src\COMMON\easyax5043.c:2182: axradio_ack_count = axradio_framing_ack_retransmissions;
      004B21 90 90 F8         [24]11479 	mov	dptr,#_axradio_framing_ack_retransmissions
      004B24 E4               [12]11480 	clr	a
      004B25 93               [24]11481 	movc	a,@a+dptr
      004B26 90 00 F4         [24]11482 	mov	dptr,#_axradio_ack_count
      004B29 F0               [24]11483 	movx	@dptr,a
                                  11484 ;	..\src\COMMON\easyax5043.c:2183: ++axradio_ack_seqnr;
      004B2A 90 00 F5         [24]11485 	mov	dptr,#_axradio_ack_seqnr
      004B2D E0               [24]11486 	movx	a,@dptr
      004B2E 24 01            [12]11487 	add	a,#0x01
      004B30 F0               [24]11488 	movx	@dptr,a
                                  11489 ;	..\src\COMMON\easyax5043.c:2184: axradio_txbuffer_len = pktlen + axradio_framing_maclen;
      004B31 90 90 E1         [24]11490 	mov	dptr,#_axradio_framing_maclen
      004B34 E4               [12]11491 	clr	a
      004B35 93               [24]11492 	movc	a,@a+dptr
      004B36 FF               [12]11493 	mov	r7,a
      004B37 FD               [12]11494 	mov	r5,a
      004B38 7E 00            [12]11495 	mov	r6,#0x00
      004B3A 90 03 AA         [24]11496 	mov	dptr,#_axradio_transmit_PARM_3
      004B3D E0               [24]11497 	movx	a,@dptr
      004B3E FB               [12]11498 	mov	r3,a
      004B3F A3               [24]11499 	inc	dptr
      004B40 E0               [24]11500 	movx	a,@dptr
      004B41 FC               [12]11501 	mov	r4,a
      004B42 ED               [12]11502 	mov	a,r5
      004B43 2B               [12]11503 	add	a,r3
      004B44 FD               [12]11504 	mov	r5,a
      004B45 EE               [12]11505 	mov	a,r6
      004B46 3C               [12]11506 	addc	a,r4
      004B47 FE               [12]11507 	mov	r6,a
      004B48 90 00 EB         [24]11508 	mov	dptr,#_axradio_txbuffer_len
      004B4B ED               [12]11509 	mov	a,r5
      004B4C F0               [24]11510 	movx	@dptr,a
      004B4D EE               [12]11511 	mov	a,r6
      004B4E A3               [24]11512 	inc	dptr
      004B4F F0               [24]11513 	movx	@dptr,a
                                  11514 ;	..\src\COMMON\easyax5043.c:2185: if (axradio_txbuffer_len > sizeof(axradio_txbuffer))
      004B50 C3               [12]11515 	clr	c
      004B51 74 04            [12]11516 	mov	a,#0x04
      004B53 9D               [12]11517 	subb	a,r5
      004B54 74 01            [12]11518 	mov	a,#0x01
      004B56 9E               [12]11519 	subb	a,r6
      004B57 50 04            [24]11520 	jnc	00130$
                                  11521 ;	..\src\COMMON\easyax5043.c:2186: return AXRADIO_ERR_INVALID;
      004B59 75 82 04         [24]11522 	mov	dpl,#0x04
      004B5C 22               [24]11523 	ret
      004B5D                      11524 00130$:
                                  11525 ;	..\src\COMMON\easyax5043.c:2187: memset_xdata(axradio_txbuffer, 0, axradio_framing_maclen);
      004B5D 7E 00            [12]11526 	mov	r6,#0x00
      004B5F 90 04 58         [24]11527 	mov	dptr,#_memset_PARM_2
      004B62 E4               [12]11528 	clr	a
      004B63 F0               [24]11529 	movx	@dptr,a
      004B64 90 04 59         [24]11530 	mov	dptr,#_memset_PARM_3
      004B67 EF               [12]11531 	mov	a,r7
      004B68 F0               [24]11532 	movx	@dptr,a
      004B69 EE               [12]11533 	mov	a,r6
      004B6A A3               [24]11534 	inc	dptr
      004B6B F0               [24]11535 	movx	@dptr,a
      004B6C 90 01 10         [24]11536 	mov	dptr,#_axradio_txbuffer
      004B6F 75 F0 00         [24]11537 	mov	b,#0x00
      004B72 C0 04            [24]11538 	push	ar4
      004B74 C0 03            [24]11539 	push	ar3
      004B76 12 75 1E         [24]11540 	lcall	_memset
      004B79 D0 03            [24]11541 	pop	ar3
      004B7B D0 04            [24]11542 	pop	ar4
                                  11543 ;	..\src\COMMON\easyax5043.c:2188: memcpy_xdatageneric(&axradio_txbuffer[axradio_framing_maclen], pkt, pktlen);
      004B7D 90 90 E1         [24]11544 	mov	dptr,#_axradio_framing_maclen
      004B80 E4               [12]11545 	clr	a
      004B81 93               [24]11546 	movc	a,@a+dptr
      004B82 24 10            [12]11547 	add	a,#_axradio_txbuffer
      004B84 FF               [12]11548 	mov	r7,a
      004B85 E4               [12]11549 	clr	a
      004B86 34 01            [12]11550 	addc	a,#(_axradio_txbuffer >> 8)
      004B88 FE               [12]11551 	mov	r6,a
      004B89 7D 00            [12]11552 	mov	r5,#0x00
      004B8B 90 03 A7         [24]11553 	mov	dptr,#_axradio_transmit_PARM_2
      004B8E E0               [24]11554 	movx	a,@dptr
      004B8F F8               [12]11555 	mov	r0,a
      004B90 A3               [24]11556 	inc	dptr
      004B91 E0               [24]11557 	movx	a,@dptr
      004B92 F9               [12]11558 	mov	r1,a
      004B93 A3               [24]11559 	inc	dptr
      004B94 E0               [24]11560 	movx	a,@dptr
      004B95 FA               [12]11561 	mov	r2,a
      004B96 90 04 6D         [24]11562 	mov	dptr,#_memcpy_PARM_2
      004B99 E8               [12]11563 	mov	a,r0
      004B9A F0               [24]11564 	movx	@dptr,a
      004B9B E9               [12]11565 	mov	a,r1
      004B9C A3               [24]11566 	inc	dptr
      004B9D F0               [24]11567 	movx	@dptr,a
      004B9E EA               [12]11568 	mov	a,r2
      004B9F A3               [24]11569 	inc	dptr
      004BA0 F0               [24]11570 	movx	@dptr,a
      004BA1 90 04 70         [24]11571 	mov	dptr,#_memcpy_PARM_3
      004BA4 EB               [12]11572 	mov	a,r3
      004BA5 F0               [24]11573 	movx	@dptr,a
      004BA6 EC               [12]11574 	mov	a,r4
      004BA7 A3               [24]11575 	inc	dptr
      004BA8 F0               [24]11576 	movx	@dptr,a
      004BA9 8F 82            [24]11577 	mov	dpl,r7
      004BAB 8E 83            [24]11578 	mov	dph,r6
      004BAD 8D F0            [24]11579 	mov	b,r5
      004BAF 12 78 14         [24]11580 	lcall	_memcpy
                                  11581 ;	..\src\COMMON\easyax5043.c:2189: if (axradio_framing_ack_seqnrpos != 0xff)
      004BB2 90 90 F9         [24]11582 	mov	dptr,#_axradio_framing_ack_seqnrpos
      004BB5 E4               [12]11583 	clr	a
      004BB6 93               [24]11584 	movc	a,@a+dptr
      004BB7 FF               [12]11585 	mov	r7,a
      004BB8 BF FF 02         [24]11586 	cjne	r7,#0xff,00335$
      004BBB 80 12            [24]11587 	sjmp	00132$
      004BBD                      11588 00335$:
                                  11589 ;	..\src\COMMON\easyax5043.c:2190: axradio_txbuffer[axradio_framing_ack_seqnrpos] = axradio_ack_seqnr;
      004BBD EF               [12]11590 	mov	a,r7
      004BBE 24 10            [12]11591 	add	a,#_axradio_txbuffer
      004BC0 FF               [12]11592 	mov	r7,a
      004BC1 E4               [12]11593 	clr	a
      004BC2 34 01            [12]11594 	addc	a,#(_axradio_txbuffer >> 8)
      004BC4 FE               [12]11595 	mov	r6,a
      004BC5 90 00 F5         [24]11596 	mov	dptr,#_axradio_ack_seqnr
      004BC8 E0               [24]11597 	movx	a,@dptr
      004BC9 FD               [12]11598 	mov	r5,a
      004BCA 8F 82            [24]11599 	mov	dpl,r7
      004BCC 8E 83            [24]11600 	mov	dph,r6
      004BCE F0               [24]11601 	movx	@dptr,a
      004BCF                      11602 00132$:
                                  11603 ;	..\src\COMMON\easyax5043.c:2191: if (axradio_framing_destaddrpos != 0xff)
      004BCF 90 90 E3         [24]11604 	mov	dptr,#_axradio_framing_destaddrpos
      004BD2 E4               [12]11605 	clr	a
      004BD3 93               [24]11606 	movc	a,@a+dptr
      004BD4 FF               [12]11607 	mov	r7,a
      004BD5 BF FF 02         [24]11608 	cjne	r7,#0xff,00336$
      004BD8 80 39            [24]11609 	sjmp	00134$
      004BDA                      11610 00336$:
                                  11611 ;	..\src\COMMON\easyax5043.c:2192: memcpy_xdatageneric(&axradio_txbuffer[axradio_framing_destaddrpos], &addr->addr, axradio_framing_addrlen);
      004BDA EF               [12]11612 	mov	a,r7
      004BDB 24 10            [12]11613 	add	a,#_axradio_txbuffer
      004BDD FF               [12]11614 	mov	r7,a
      004BDE E4               [12]11615 	clr	a
      004BDF 34 01            [12]11616 	addc	a,#(_axradio_txbuffer >> 8)
      004BE1 FE               [12]11617 	mov	r6,a
      004BE2 7D 00            [12]11618 	mov	r5,#0x00
      004BE4 90 03 AC         [24]11619 	mov	dptr,#_axradio_transmit_addr_65536_463
      004BE7 E0               [24]11620 	movx	a,@dptr
      004BE8 FA               [12]11621 	mov	r2,a
      004BE9 A3               [24]11622 	inc	dptr
      004BEA E0               [24]11623 	movx	a,@dptr
      004BEB FB               [12]11624 	mov	r3,a
      004BEC A3               [24]11625 	inc	dptr
      004BED E0               [24]11626 	movx	a,@dptr
      004BEE FC               [12]11627 	mov	r4,a
      004BEF 90 90 E2         [24]11628 	mov	dptr,#_axradio_framing_addrlen
      004BF2 E4               [12]11629 	clr	a
      004BF3 93               [24]11630 	movc	a,@a+dptr
      004BF4 F8               [12]11631 	mov	r0,a
      004BF5 79 00            [12]11632 	mov	r1,#0x00
      004BF7 90 04 6D         [24]11633 	mov	dptr,#_memcpy_PARM_2
      004BFA EA               [12]11634 	mov	a,r2
      004BFB F0               [24]11635 	movx	@dptr,a
      004BFC EB               [12]11636 	mov	a,r3
      004BFD A3               [24]11637 	inc	dptr
      004BFE F0               [24]11638 	movx	@dptr,a
      004BFF EC               [12]11639 	mov	a,r4
      004C00 A3               [24]11640 	inc	dptr
      004C01 F0               [24]11641 	movx	@dptr,a
      004C02 90 04 70         [24]11642 	mov	dptr,#_memcpy_PARM_3
      004C05 E8               [12]11643 	mov	a,r0
      004C06 F0               [24]11644 	movx	@dptr,a
      004C07 E9               [12]11645 	mov	a,r1
      004C08 A3               [24]11646 	inc	dptr
      004C09 F0               [24]11647 	movx	@dptr,a
      004C0A 8F 82            [24]11648 	mov	dpl,r7
      004C0C 8E 83            [24]11649 	mov	dph,r6
      004C0E 8D F0            [24]11650 	mov	b,r5
      004C10 12 78 14         [24]11651 	lcall	_memcpy
      004C13                      11652 00134$:
                                  11653 ;	..\src\COMMON\easyax5043.c:2193: if (axradio_framing_sourceaddrpos != 0xff)
      004C13 90 90 E4         [24]11654 	mov	dptr,#_axradio_framing_sourceaddrpos
      004C16 E4               [12]11655 	clr	a
      004C17 93               [24]11656 	movc	a,@a+dptr
      004C18 FF               [12]11657 	mov	r7,a
      004C19 BF FF 02         [24]11658 	cjne	r7,#0xff,00337$
      004C1C 80 30            [24]11659 	sjmp	00136$
      004C1E                      11660 00337$:
                                  11661 ;	..\src\COMMON\easyax5043.c:2194: memcpy_xdata(&axradio_txbuffer[axradio_framing_sourceaddrpos], &axradio_localaddr.addr, axradio_framing_addrlen);
      004C1E EF               [12]11662 	mov	a,r7
      004C1F 24 10            [12]11663 	add	a,#_axradio_txbuffer
      004C21 FF               [12]11664 	mov	r7,a
      004C22 E4               [12]11665 	clr	a
      004C23 34 01            [12]11666 	addc	a,#(_axradio_txbuffer >> 8)
      004C25 FE               [12]11667 	mov	r6,a
      004C26 7D 00            [12]11668 	mov	r5,#0x00
      004C28 90 90 E2         [24]11669 	mov	dptr,#_axradio_framing_addrlen
      004C2B E4               [12]11670 	clr	a
      004C2C 93               [24]11671 	movc	a,@a+dptr
      004C2D FC               [12]11672 	mov	r4,a
      004C2E 7B 00            [12]11673 	mov	r3,#0x00
      004C30 90 04 6D         [24]11674 	mov	dptr,#_memcpy_PARM_2
      004C33 74 04            [12]11675 	mov	a,#_axradio_localaddr
      004C35 F0               [24]11676 	movx	@dptr,a
      004C36 74 01            [12]11677 	mov	a,#(_axradio_localaddr >> 8)
      004C38 A3               [24]11678 	inc	dptr
      004C39 F0               [24]11679 	movx	@dptr,a
      004C3A E4               [12]11680 	clr	a
      004C3B A3               [24]11681 	inc	dptr
      004C3C F0               [24]11682 	movx	@dptr,a
      004C3D 90 04 70         [24]11683 	mov	dptr,#_memcpy_PARM_3
      004C40 EC               [12]11684 	mov	a,r4
      004C41 F0               [24]11685 	movx	@dptr,a
      004C42 EB               [12]11686 	mov	a,r3
      004C43 A3               [24]11687 	inc	dptr
      004C44 F0               [24]11688 	movx	@dptr,a
      004C45 8F 82            [24]11689 	mov	dpl,r7
      004C47 8E 83            [24]11690 	mov	dph,r6
      004C49 8D F0            [24]11691 	mov	b,r5
      004C4B 12 78 14         [24]11692 	lcall	_memcpy
      004C4E                      11693 00136$:
                                  11694 ;	..\src\COMMON\easyax5043.c:2195: if (axradio_framing_lenmask) {
      004C4E 90 90 E7         [24]11695 	mov	dptr,#_axradio_framing_lenmask
      004C51 E4               [12]11696 	clr	a
      004C52 93               [24]11697 	movc	a,@a+dptr
      004C53 FF               [12]11698 	mov	r7,a
      004C54 60 30            [24]11699 	jz	00138$
                                  11700 ;	..\src\COMMON\easyax5043.c:2196: uint8_t __autodata len_byte = (uint8_t)(axradio_txbuffer_len - axradio_framing_lenoffs) & axradio_framing_lenmask; // if you prefer not counting the len byte itself, set LENOFFS = 1
      004C56 90 00 EB         [24]11701 	mov	dptr,#_axradio_txbuffer_len
      004C59 E0               [24]11702 	movx	a,@dptr
      004C5A FD               [12]11703 	mov	r5,a
      004C5B A3               [24]11704 	inc	dptr
      004C5C E0               [24]11705 	movx	a,@dptr
      004C5D 90 90 E6         [24]11706 	mov	dptr,#_axradio_framing_lenoffs
      004C60 E4               [12]11707 	clr	a
      004C61 93               [24]11708 	movc	a,@a+dptr
      004C62 FE               [12]11709 	mov	r6,a
      004C63 ED               [12]11710 	mov	a,r5
      004C64 C3               [12]11711 	clr	c
      004C65 9E               [12]11712 	subb	a,r6
      004C66 5F               [12]11713 	anl	a,r7
      004C67 FE               [12]11714 	mov	r6,a
                                  11715 ;	..\src\COMMON\easyax5043.c:2197: axradio_txbuffer[axradio_framing_lenpos] = (axradio_txbuffer[axradio_framing_lenpos] & (uint8_t)~axradio_framing_lenmask) | len_byte;
      004C68 90 90 E5         [24]11716 	mov	dptr,#_axradio_framing_lenpos
      004C6B E4               [12]11717 	clr	a
      004C6C 93               [24]11718 	movc	a,@a+dptr
      004C6D 24 10            [12]11719 	add	a,#_axradio_txbuffer
      004C6F FD               [12]11720 	mov	r5,a
      004C70 E4               [12]11721 	clr	a
      004C71 34 01            [12]11722 	addc	a,#(_axradio_txbuffer >> 8)
      004C73 FC               [12]11723 	mov	r4,a
      004C74 8D 82            [24]11724 	mov	dpl,r5
      004C76 8C 83            [24]11725 	mov	dph,r4
      004C78 E0               [24]11726 	movx	a,@dptr
      004C79 FB               [12]11727 	mov	r3,a
      004C7A EF               [12]11728 	mov	a,r7
      004C7B F4               [12]11729 	cpl	a
      004C7C FF               [12]11730 	mov	r7,a
      004C7D 5B               [12]11731 	anl	a,r3
      004C7E 42 06            [12]11732 	orl	ar6,a
      004C80 8D 82            [24]11733 	mov	dpl,r5
      004C82 8C 83            [24]11734 	mov	dph,r4
      004C84 EE               [12]11735 	mov	a,r6
      004C85 F0               [24]11736 	movx	@dptr,a
      004C86                      11737 00138$:
                                  11738 ;	..\src\COMMON\easyax5043.c:2199: if (axradio_framing_swcrclen)
      004C86 90 90 E8         [24]11739 	mov	dptr,#_axradio_framing_swcrclen
      004C89 E4               [12]11740 	clr	a
      004C8A 93               [24]11741 	movc	a,@a+dptr
      004C8B 60 20            [24]11742 	jz	00140$
                                  11743 ;	..\src\COMMON\easyax5043.c:2200: axradio_txbuffer_len = axradio_framing_append_crc(axradio_txbuffer, axradio_txbuffer_len);
      004C8D 90 00 EB         [24]11744 	mov	dptr,#_axradio_txbuffer_len
      004C90 E0               [24]11745 	movx	a,@dptr
      004C91 C0 E0            [24]11746 	push	acc
      004C93 A3               [24]11747 	inc	dptr
      004C94 E0               [24]11748 	movx	a,@dptr
      004C95 C0 E0            [24]11749 	push	acc
      004C97 90 01 10         [24]11750 	mov	dptr,#_axradio_txbuffer
      004C9A 12 1C C1         [24]11751 	lcall	_axradio_framing_append_crc
      004C9D AE 82            [24]11752 	mov	r6,dpl
      004C9F AF 83            [24]11753 	mov	r7,dph
      004CA1 15 81            [12]11754 	dec	sp
      004CA3 15 81            [12]11755 	dec	sp
      004CA5 90 00 EB         [24]11756 	mov	dptr,#_axradio_txbuffer_len
      004CA8 EE               [12]11757 	mov	a,r6
      004CA9 F0               [24]11758 	movx	@dptr,a
      004CAA EF               [12]11759 	mov	a,r7
      004CAB A3               [24]11760 	inc	dptr
      004CAC F0               [24]11761 	movx	@dptr,a
      004CAD                      11762 00140$:
                                  11763 ;	..\src\COMMON\easyax5043.c:2201: if (axradio_phy_pn9)
      004CAD 90 90 C0         [24]11764 	mov	dptr,#_axradio_phy_pn9
      004CB0 E4               [12]11765 	clr	a
      004CB1 93               [24]11766 	movc	a,@a+dptr
      004CB2 60 2F            [24]11767 	jz	00142$
                                  11768 ;	..\src\COMMON\easyax5043.c:2202: pn9_buffer(axradio_txbuffer, axradio_txbuffer_len, 0x1ff, -(AX5043_ENCODING & 0x01));
      004CB4 90 40 11         [24]11769 	mov	dptr,#_AX5043_ENCODING
      004CB7 E0               [24]11770 	movx	a,@dptr
      004CB8 FF               [12]11771 	mov	r7,a
      004CB9 53 07 01         [24]11772 	anl	ar7,#0x01
      004CBC C3               [12]11773 	clr	c
      004CBD E4               [12]11774 	clr	a
      004CBE 9F               [12]11775 	subb	a,r7
      004CBF FF               [12]11776 	mov	r7,a
      004CC0 C0 07            [24]11777 	push	ar7
      004CC2 74 FF            [12]11778 	mov	a,#0xff
      004CC4 C0 E0            [24]11779 	push	acc
      004CC6 74 01            [12]11780 	mov	a,#0x01
      004CC8 C0 E0            [24]11781 	push	acc
      004CCA 90 00 EB         [24]11782 	mov	dptr,#_axradio_txbuffer_len
      004CCD E0               [24]11783 	movx	a,@dptr
      004CCE C0 E0            [24]11784 	push	acc
      004CD0 A3               [24]11785 	inc	dptr
      004CD1 E0               [24]11786 	movx	a,@dptr
      004CD2 C0 E0            [24]11787 	push	acc
      004CD4 90 01 10         [24]11788 	mov	dptr,#_axradio_txbuffer
      004CD7 75 F0 00         [24]11789 	mov	b,#0x00
      004CDA 12 79 E1         [24]11790 	lcall	_pn9_buffer
      004CDD E5 81            [12]11791 	mov	a,sp
      004CDF 24 FB            [12]11792 	add	a,#0xfb
      004CE1 F5 81            [12]11793 	mov	sp,a
      004CE3                      11794 00142$:
                                  11795 ;	..\src\COMMON\easyax5043.c:2203: if (axradio_mode == AXRADIO_MODE_SYNC_MASTER ||
      004CE3 74 30            [12]11796 	mov	a,#0x30
      004CE5 B5 17 02         [24]11797 	cjne	a,_axradio_mode,00341$
      004CE8 80 05            [24]11798 	sjmp	00143$
      004CEA                      11799 00341$:
                                  11800 ;	..\src\COMMON\easyax5043.c:2204: axradio_mode == AXRADIO_MODE_SYNC_ACK_MASTER)
      004CEA 74 31            [12]11801 	mov	a,#0x31
      004CEC B5 17 04         [24]11802 	cjne	a,_axradio_mode,00144$
      004CEF                      11803 00143$:
                                  11804 ;	..\src\COMMON\easyax5043.c:2205: return AXRADIO_ERR_NOERROR;
      004CEF 75 82 00         [24]11805 	mov	dpl,#0x00
      004CF2 22               [24]11806 	ret
      004CF3                      11807 00144$:
                                  11808 ;	..\src\COMMON\easyax5043.c:2206: if (axradio_mode == AXRADIO_MODE_WOR_TRANSMIT ||
      004CF3 74 11            [12]11809 	mov	a,#0x11
      004CF5 B5 17 02         [24]11810 	cjne	a,_axradio_mode,00344$
      004CF8 80 05            [24]11811 	sjmp	00146$
      004CFA                      11812 00344$:
                                  11813 ;	..\src\COMMON\easyax5043.c:2207: axradio_mode == AXRADIO_MODE_WOR_ACK_TRANSMIT)
      004CFA 74 13            [12]11814 	mov	a,#0x13
      004CFC B5 17 14         [24]11815 	cjne	a,_axradio_mode,00147$
      004CFF                      11816 00146$:
                                  11817 ;	..\src\COMMON\easyax5043.c:2208: axradio_txbuffer_cnt = axradio_phy_preamble_wor_longlen;
      004CFF 90 90 D5         [24]11818 	mov	dptr,#_axradio_phy_preamble_wor_longlen
      004D02 E4               [12]11819 	clr	a
      004D03 93               [24]11820 	movc	a,@a+dptr
      004D04 FE               [12]11821 	mov	r6,a
      004D05 74 01            [12]11822 	mov	a,#0x01
      004D07 93               [24]11823 	movc	a,@a+dptr
      004D08 FF               [12]11824 	mov	r7,a
      004D09 90 00 ED         [24]11825 	mov	dptr,#_axradio_txbuffer_cnt
      004D0C EE               [12]11826 	mov	a,r6
      004D0D F0               [24]11827 	movx	@dptr,a
      004D0E EF               [12]11828 	mov	a,r7
      004D0F A3               [24]11829 	inc	dptr
      004D10 F0               [24]11830 	movx	@dptr,a
      004D11 80 12            [24]11831 	sjmp	00148$
      004D13                      11832 00147$:
                                  11833 ;	..\src\COMMON\easyax5043.c:2210: axradio_txbuffer_cnt = axradio_phy_preamble_longlen;
      004D13 90 90 D9         [24]11834 	mov	dptr,#_axradio_phy_preamble_longlen
      004D16 E4               [12]11835 	clr	a
      004D17 93               [24]11836 	movc	a,@a+dptr
      004D18 FE               [12]11837 	mov	r6,a
      004D19 74 01            [12]11838 	mov	a,#0x01
      004D1B 93               [24]11839 	movc	a,@a+dptr
      004D1C FF               [12]11840 	mov	r7,a
      004D1D 90 00 ED         [24]11841 	mov	dptr,#_axradio_txbuffer_cnt
      004D20 EE               [12]11842 	mov	a,r6
      004D21 F0               [24]11843 	movx	@dptr,a
      004D22 EF               [12]11844 	mov	a,r7
      004D23 A3               [24]11845 	inc	dptr
      004D24 F0               [24]11846 	movx	@dptr,a
      004D25                      11847 00148$:
                                  11848 ;	..\src\COMMON\easyax5043.c:2211: if (axradio_phy_lbt_retries) {
      004D25 90 90 D3         [24]11849 	mov	dptr,#_axradio_phy_lbt_retries
      004D28 E4               [12]11850 	clr	a
      004D29 93               [24]11851 	movc	a,@a+dptr
      004D2A 70 03            [24]11852 	jnz	00347$
      004D2C 02 4D A7         [24]11853 	ljmp	00161$
      004D2F                      11854 00347$:
                                  11855 ;	..\src\COMMON\easyax5043.c:2212: switch (axradio_mode) {
      004D2F AF 17            [24]11856 	mov	r7,_axradio_mode
      004D31 BF 10 02         [24]11857 	cjne	r7,#0x10,00348$
      004D34 80 21            [24]11858 	sjmp	00157$
      004D36                      11859 00348$:
      004D36 BF 11 02         [24]11860 	cjne	r7,#0x11,00349$
      004D39 80 1C            [24]11861 	sjmp	00157$
      004D3B                      11862 00349$:
      004D3B BF 12 02         [24]11863 	cjne	r7,#0x12,00350$
      004D3E 80 17            [24]11864 	sjmp	00157$
      004D40                      11865 00350$:
      004D40 BF 13 02         [24]11866 	cjne	r7,#0x13,00351$
      004D43 80 12            [24]11867 	sjmp	00157$
      004D45                      11868 00351$:
      004D45 BF 20 02         [24]11869 	cjne	r7,#0x20,00352$
      004D48 80 0D            [24]11870 	sjmp	00157$
      004D4A                      11871 00352$:
      004D4A BF 21 02         [24]11872 	cjne	r7,#0x21,00353$
      004D4D 80 08            [24]11873 	sjmp	00157$
      004D4F                      11874 00353$:
      004D4F BF 22 02         [24]11875 	cjne	r7,#0x22,00354$
      004D52 80 03            [24]11876 	sjmp	00157$
      004D54                      11877 00354$:
      004D54 BF 23 50         [24]11878 	cjne	r7,#0x23,00161$
                                  11879 ;	..\src\COMMON\easyax5043.c:2220: case AXRADIO_MODE_ACK_RECEIVE:
      004D57                      11880 00157$:
                                  11881 ;	..\src\COMMON\easyax5043.c:2221: ax5043_off_xtal();
      004D57 12 2C F9         [24]11882 	lcall	_ax5043_off_xtal
                                  11883 ;	..\src\COMMON\easyax5043.c:2222: ax5043_init_registers_rx();
      004D5A 12 1F 50         [24]11884 	lcall	_ax5043_init_registers_rx
                                  11885 ;	..\src\COMMON\easyax5043.c:2223: AX5043_RSSIREFERENCE = axradio_phy_rssireference;
      004D5D 90 90 CE         [24]11886 	mov	dptr,#_axradio_phy_rssireference
      004D60 E4               [12]11887 	clr	a
      004D61 93               [24]11888 	movc	a,@a+dptr
      004D62 90 42 2C         [24]11889 	mov	dptr,#_AX5043_RSSIREFERENCE
      004D65 F0               [24]11890 	movx	@dptr,a
                                  11891 ;	..\src\COMMON\easyax5043.c:2224: AX5043_PWRMODE = AX5043_PWRSTATE_FULL_RX;
      004D66 90 40 02         [24]11892 	mov	dptr,#_AX5043_PWRMODE
      004D69 74 09            [12]11893 	mov	a,#0x09
      004D6B F0               [24]11894 	movx	@dptr,a
                                  11895 ;	..\src\COMMON\easyax5043.c:2225: axradio_ack_count = axradio_phy_lbt_retries;
      004D6C 90 90 D3         [24]11896 	mov	dptr,#_axradio_phy_lbt_retries
      004D6F E4               [12]11897 	clr	a
      004D70 93               [24]11898 	movc	a,@a+dptr
      004D71 90 00 F4         [24]11899 	mov	dptr,#_axradio_ack_count
      004D74 F0               [24]11900 	movx	@dptr,a
                                  11901 ;	..\src\COMMON\easyax5043.c:2226: axradio_syncstate = syncstate_lbt;
      004D75 90 00 EA         [24]11902 	mov	dptr,#_axradio_syncstate
      004D78 74 01            [12]11903 	mov	a,#0x01
      004D7A F0               [24]11904 	movx	@dptr,a
                                  11905 ;	..\src\COMMON\easyax5043.c:2227: wtimer_remove(&axradio_timer);
      004D7B 90 03 6F         [24]11906 	mov	dptr,#_axradio_timer
      004D7E 12 85 39         [24]11907 	lcall	_wtimer_remove
                                  11908 ;	..\src\COMMON\easyax5043.c:2228: axradio_timer.time = axradio_phy_cs_period;
      004D81 90 90 D0         [24]11909 	mov	dptr,#_axradio_phy_cs_period
      004D84 E4               [12]11910 	clr	a
      004D85 93               [24]11911 	movc	a,@a+dptr
      004D86 FE               [12]11912 	mov	r6,a
      004D87 74 01            [12]11913 	mov	a,#0x01
      004D89 93               [24]11914 	movc	a,@a+dptr
      004D8A FF               [12]11915 	mov	r7,a
      004D8B 7D 00            [12]11916 	mov	r5,#0x00
      004D8D 7C 00            [12]11917 	mov	r4,#0x00
      004D8F 90 03 73         [24]11918 	mov	dptr,#(_axradio_timer + 0x0004)
      004D92 EE               [12]11919 	mov	a,r6
      004D93 F0               [24]11920 	movx	@dptr,a
      004D94 EF               [12]11921 	mov	a,r7
      004D95 A3               [24]11922 	inc	dptr
      004D96 F0               [24]11923 	movx	@dptr,a
      004D97 ED               [12]11924 	mov	a,r5
      004D98 A3               [24]11925 	inc	dptr
      004D99 F0               [24]11926 	movx	@dptr,a
      004D9A EC               [12]11927 	mov	a,r4
      004D9B A3               [24]11928 	inc	dptr
      004D9C F0               [24]11929 	movx	@dptr,a
                                  11930 ;	..\src\COMMON\easyax5043.c:2229: wtimer0_addrelative(&axradio_timer);
      004D9D 90 03 6F         [24]11931 	mov	dptr,#_axradio_timer
      004DA0 12 78 78         [24]11932 	lcall	_wtimer0_addrelative
                                  11933 ;	..\src\COMMON\easyax5043.c:2230: return AXRADIO_ERR_NOERROR;
      004DA3 75 82 00         [24]11934 	mov	dpl,#0x00
                                  11935 ;	..\src\COMMON\easyax5043.c:2234: }
      004DA6 22               [24]11936 	ret
      004DA7                      11937 00161$:
                                  11938 ;	..\src\COMMON\easyax5043.c:2236: axradio_syncstate = syncstate_asynctx;
      004DA7 90 00 EA         [24]11939 	mov	dptr,#_axradio_syncstate
      004DAA 74 02            [12]11940 	mov	a,#0x02
      004DAC F0               [24]11941 	movx	@dptr,a
                                  11942 ;	..\src\COMMON\easyax5043.c:2237: ax5043_prepare_tx();
      004DAD 12 2C C7         [24]11943 	lcall	_ax5043_prepare_tx
                                  11944 ;	..\src\COMMON\easyax5043.c:2238: return AXRADIO_ERR_NOERROR;
      004DB0 75 82 00         [24]11945 	mov	dpl,#0x00
                                  11946 ;	..\src\COMMON\easyax5043.c:2240: default:
      004DB3 22               [24]11947 	ret
      004DB4                      11948 00162$:
                                  11949 ;	..\src\COMMON\easyax5043.c:2241: return AXRADIO_ERR_NOTSUPPORTED;
      004DB4 75 82 01         [24]11950 	mov	dpl,#0x01
                                  11951 ;	..\src\COMMON\easyax5043.c:2242: }
                                  11952 ;	..\src\COMMON\easyax5043.c:2243: }
      004DB7 22               [24]11953 	ret
                                  11954 ;------------------------------------------------------------
                                  11955 ;Allocation info for local variables in function 'axradio_set_paramsets'
                                  11956 ;------------------------------------------------------------
                                  11957 ;val                       Allocated to registers r7 
                                  11958 ;------------------------------------------------------------
                                  11959 ;	..\src\COMMON\easyax5043.c:2245: static __reentrantb uint8_t axradio_set_paramsets(uint8_t val) __reentrant
                                  11960 ;	-----------------------------------------
                                  11961 ;	 function axradio_set_paramsets
                                  11962 ;	-----------------------------------------
      004DB8                      11963 _axradio_set_paramsets:
      004DB8 AF 82            [24]11964 	mov	r7,dpl
                                  11965 ;	..\src\COMMON\easyax5043.c:2247: if (!AXRADIO_MODE_IS_STREAM_RECEIVE(axradio_mode))
      004DBA AD 17            [24]11966 	mov	r5,_axradio_mode
      004DBC 53 05 F8         [24]11967 	anl	ar5,#0xf8
      004DBF 7E 00            [12]11968 	mov	r6,#0x00
      004DC1 BD 28 05         [24]11969 	cjne	r5,#0x28,00109$
      004DC4 BE 00 02         [24]11970 	cjne	r6,#0x00,00109$
      004DC7 80 04            [24]11971 	sjmp	00102$
      004DC9                      11972 00109$:
                                  11973 ;	..\src\COMMON\easyax5043.c:2248: return AXRADIO_ERR_NOTSUPPORTED;
      004DC9 75 82 01         [24]11974 	mov	dpl,#0x01
      004DCC 22               [24]11975 	ret
      004DCD                      11976 00102$:
                                  11977 ;	..\src\COMMON\easyax5043.c:2249: AX5043_RXPARAMSETS = val;
      004DCD 90 41 17         [24]11978 	mov	dptr,#_AX5043_RXPARAMSETS
      004DD0 EF               [12]11979 	mov	a,r7
      004DD1 F0               [24]11980 	movx	@dptr,a
                                  11981 ;	..\src\COMMON\easyax5043.c:2250: return AXRADIO_ERR_NOERROR;
      004DD2 75 82 00         [24]11982 	mov	dpl,#0x00
                                  11983 ;	..\src\COMMON\easyax5043.c:2251: }
      004DD5 22               [24]11984 	ret
                                  11985 	.area CSEG    (CODE)
                                  11986 	.area CONST   (CODE)
                                  11987 	.area CONST   (CODE)
      0091BF                      11988 ___str_0:
      0091BF 20                   11989 	.ascii " "
      0091C0 0A                   11990 	.db 0x0a
      0091C1 20 52 65 63 65 69 76 11991 	.ascii " Receive isr "
             65 20 69 73 72 20
      0091CE 00                   11992 	.db 0x00
                                  11993 	.area CSEG    (CODE)
                                  11994 	.area CONST   (CODE)
      0091CF                      11995 ___str_1:
      0091CF 20 64 69 73 63 61 72 11996 	.ascii " discarded "
             64 65 64 20
      0091DA 00                   11997 	.db 0x00
                                  11998 	.area CSEG    (CODE)
                                  11999 	.area XINIT   (CODE)
      009CDF                      12000 __xinit__f30_saved:
      009CDF 3F                   12001 	.db #0x3f	; 63
      009CE0                      12002 __xinit__f31_saved:
      009CE0 F0                   12003 	.db #0xf0	; 240
      009CE1                      12004 __xinit__f32_saved:
      009CE1 3F                   12005 	.db #0x3f	; 63
      009CE2                      12006 __xinit__f33_saved:
      009CE2 F0                   12007 	.db #0xf0	; 240
                                  12008 	.area CABS    (ABS,CODE)
