                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.6.0 #9615 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module UartComProc
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _delay_ms
                                     12 	.globl _memcpy
                                     13 	.globl _free
                                     14 	.globl _malloc
                                     15 	.globl _uart_timer1_baud
                                     16 	.globl _uart0_tx
                                     17 	.globl _uart0_rx
                                     18 	.globl _uart0_init
                                     19 	.globl _uart0_rxcount
                                     20 	.globl _PORTC_7
                                     21 	.globl _PORTC_6
                                     22 	.globl _PORTC_5
                                     23 	.globl _PORTC_4
                                     24 	.globl _PORTC_3
                                     25 	.globl _PORTC_2
                                     26 	.globl _PORTC_1
                                     27 	.globl _PORTC_0
                                     28 	.globl _PORTB_7
                                     29 	.globl _PORTB_6
                                     30 	.globl _PORTB_5
                                     31 	.globl _PORTB_4
                                     32 	.globl _PORTB_3
                                     33 	.globl _PORTB_2
                                     34 	.globl _PORTB_1
                                     35 	.globl _PORTB_0
                                     36 	.globl _PORTA_7
                                     37 	.globl _PORTA_6
                                     38 	.globl _PORTA_5
                                     39 	.globl _PORTA_4
                                     40 	.globl _PORTA_3
                                     41 	.globl _PORTA_2
                                     42 	.globl _PORTA_1
                                     43 	.globl _PORTA_0
                                     44 	.globl _PINC_7
                                     45 	.globl _PINC_6
                                     46 	.globl _PINC_5
                                     47 	.globl _PINC_4
                                     48 	.globl _PINC_3
                                     49 	.globl _PINC_2
                                     50 	.globl _PINC_1
                                     51 	.globl _PINC_0
                                     52 	.globl _PINB_7
                                     53 	.globl _PINB_6
                                     54 	.globl _PINB_5
                                     55 	.globl _PINB_4
                                     56 	.globl _PINB_3
                                     57 	.globl _PINB_2
                                     58 	.globl _PINB_1
                                     59 	.globl _PINB_0
                                     60 	.globl _PINA_7
                                     61 	.globl _PINA_6
                                     62 	.globl _PINA_5
                                     63 	.globl _PINA_4
                                     64 	.globl _PINA_3
                                     65 	.globl _PINA_2
                                     66 	.globl _PINA_1
                                     67 	.globl _PINA_0
                                     68 	.globl _CY
                                     69 	.globl _AC
                                     70 	.globl _F0
                                     71 	.globl _RS1
                                     72 	.globl _RS0
                                     73 	.globl _OV
                                     74 	.globl _F1
                                     75 	.globl _P
                                     76 	.globl _IP_7
                                     77 	.globl _IP_6
                                     78 	.globl _IP_5
                                     79 	.globl _IP_4
                                     80 	.globl _IP_3
                                     81 	.globl _IP_2
                                     82 	.globl _IP_1
                                     83 	.globl _IP_0
                                     84 	.globl _EA
                                     85 	.globl _IE_7
                                     86 	.globl _IE_6
                                     87 	.globl _IE_5
                                     88 	.globl _IE_4
                                     89 	.globl _IE_3
                                     90 	.globl _IE_2
                                     91 	.globl _IE_1
                                     92 	.globl _IE_0
                                     93 	.globl _EIP_7
                                     94 	.globl _EIP_6
                                     95 	.globl _EIP_5
                                     96 	.globl _EIP_4
                                     97 	.globl _EIP_3
                                     98 	.globl _EIP_2
                                     99 	.globl _EIP_1
                                    100 	.globl _EIP_0
                                    101 	.globl _EIE_7
                                    102 	.globl _EIE_6
                                    103 	.globl _EIE_5
                                    104 	.globl _EIE_4
                                    105 	.globl _EIE_3
                                    106 	.globl _EIE_2
                                    107 	.globl _EIE_1
                                    108 	.globl _EIE_0
                                    109 	.globl _E2IP_7
                                    110 	.globl _E2IP_6
                                    111 	.globl _E2IP_5
                                    112 	.globl _E2IP_4
                                    113 	.globl _E2IP_3
                                    114 	.globl _E2IP_2
                                    115 	.globl _E2IP_1
                                    116 	.globl _E2IP_0
                                    117 	.globl _E2IE_7
                                    118 	.globl _E2IE_6
                                    119 	.globl _E2IE_5
                                    120 	.globl _E2IE_4
                                    121 	.globl _E2IE_3
                                    122 	.globl _E2IE_2
                                    123 	.globl _E2IE_1
                                    124 	.globl _E2IE_0
                                    125 	.globl _B_7
                                    126 	.globl _B_6
                                    127 	.globl _B_5
                                    128 	.globl _B_4
                                    129 	.globl _B_3
                                    130 	.globl _B_2
                                    131 	.globl _B_1
                                    132 	.globl _B_0
                                    133 	.globl _ACC_7
                                    134 	.globl _ACC_6
                                    135 	.globl _ACC_5
                                    136 	.globl _ACC_4
                                    137 	.globl _ACC_3
                                    138 	.globl _ACC_2
                                    139 	.globl _ACC_1
                                    140 	.globl _ACC_0
                                    141 	.globl _WTSTAT
                                    142 	.globl _WTIRQEN
                                    143 	.globl _WTEVTD
                                    144 	.globl _WTEVTD1
                                    145 	.globl _WTEVTD0
                                    146 	.globl _WTEVTC
                                    147 	.globl _WTEVTC1
                                    148 	.globl _WTEVTC0
                                    149 	.globl _WTEVTB
                                    150 	.globl _WTEVTB1
                                    151 	.globl _WTEVTB0
                                    152 	.globl _WTEVTA
                                    153 	.globl _WTEVTA1
                                    154 	.globl _WTEVTA0
                                    155 	.globl _WTCNTR1
                                    156 	.globl _WTCNTB
                                    157 	.globl _WTCNTB1
                                    158 	.globl _WTCNTB0
                                    159 	.globl _WTCNTA
                                    160 	.globl _WTCNTA1
                                    161 	.globl _WTCNTA0
                                    162 	.globl _WTCFGB
                                    163 	.globl _WTCFGA
                                    164 	.globl _WDTRESET
                                    165 	.globl _WDTCFG
                                    166 	.globl _U1STATUS
                                    167 	.globl _U1SHREG
                                    168 	.globl _U1MODE
                                    169 	.globl _U1CTRL
                                    170 	.globl _U0STATUS
                                    171 	.globl _U0SHREG
                                    172 	.globl _U0MODE
                                    173 	.globl _U0CTRL
                                    174 	.globl _T2STATUS
                                    175 	.globl _T2PERIOD
                                    176 	.globl _T2PERIOD1
                                    177 	.globl _T2PERIOD0
                                    178 	.globl _T2MODE
                                    179 	.globl _T2CNT
                                    180 	.globl _T2CNT1
                                    181 	.globl _T2CNT0
                                    182 	.globl _T2CLKSRC
                                    183 	.globl _T1STATUS
                                    184 	.globl _T1PERIOD
                                    185 	.globl _T1PERIOD1
                                    186 	.globl _T1PERIOD0
                                    187 	.globl _T1MODE
                                    188 	.globl _T1CNT
                                    189 	.globl _T1CNT1
                                    190 	.globl _T1CNT0
                                    191 	.globl _T1CLKSRC
                                    192 	.globl _T0STATUS
                                    193 	.globl _T0PERIOD
                                    194 	.globl _T0PERIOD1
                                    195 	.globl _T0PERIOD0
                                    196 	.globl _T0MODE
                                    197 	.globl _T0CNT
                                    198 	.globl _T0CNT1
                                    199 	.globl _T0CNT0
                                    200 	.globl _T0CLKSRC
                                    201 	.globl _SPSTATUS
                                    202 	.globl _SPSHREG
                                    203 	.globl _SPMODE
                                    204 	.globl _SPCLKSRC
                                    205 	.globl _RADIOSTAT
                                    206 	.globl _RADIOSTAT1
                                    207 	.globl _RADIOSTAT0
                                    208 	.globl _RADIODATA
                                    209 	.globl _RADIODATA3
                                    210 	.globl _RADIODATA2
                                    211 	.globl _RADIODATA1
                                    212 	.globl _RADIODATA0
                                    213 	.globl _RADIOADDR
                                    214 	.globl _RADIOADDR1
                                    215 	.globl _RADIOADDR0
                                    216 	.globl _RADIOACC
                                    217 	.globl _OC1STATUS
                                    218 	.globl _OC1PIN
                                    219 	.globl _OC1MODE
                                    220 	.globl _OC1COMP
                                    221 	.globl _OC1COMP1
                                    222 	.globl _OC1COMP0
                                    223 	.globl _OC0STATUS
                                    224 	.globl _OC0PIN
                                    225 	.globl _OC0MODE
                                    226 	.globl _OC0COMP
                                    227 	.globl _OC0COMP1
                                    228 	.globl _OC0COMP0
                                    229 	.globl _NVSTATUS
                                    230 	.globl _NVKEY
                                    231 	.globl _NVDATA
                                    232 	.globl _NVDATA1
                                    233 	.globl _NVDATA0
                                    234 	.globl _NVADDR
                                    235 	.globl _NVADDR1
                                    236 	.globl _NVADDR0
                                    237 	.globl _IC1STATUS
                                    238 	.globl _IC1MODE
                                    239 	.globl _IC1CAPT
                                    240 	.globl _IC1CAPT1
                                    241 	.globl _IC1CAPT0
                                    242 	.globl _IC0STATUS
                                    243 	.globl _IC0MODE
                                    244 	.globl _IC0CAPT
                                    245 	.globl _IC0CAPT1
                                    246 	.globl _IC0CAPT0
                                    247 	.globl _PORTR
                                    248 	.globl _PORTC
                                    249 	.globl _PORTB
                                    250 	.globl _PORTA
                                    251 	.globl _PINR
                                    252 	.globl _PINC
                                    253 	.globl _PINB
                                    254 	.globl _PINA
                                    255 	.globl _DIRR
                                    256 	.globl _DIRC
                                    257 	.globl _DIRB
                                    258 	.globl _DIRA
                                    259 	.globl _DBGLNKSTAT
                                    260 	.globl _DBGLNKBUF
                                    261 	.globl _CODECONFIG
                                    262 	.globl _CLKSTAT
                                    263 	.globl _CLKCON
                                    264 	.globl _ANALOGCOMP
                                    265 	.globl _ADCCONV
                                    266 	.globl _ADCCLKSRC
                                    267 	.globl _ADCCH3CONFIG
                                    268 	.globl _ADCCH2CONFIG
                                    269 	.globl _ADCCH1CONFIG
                                    270 	.globl _ADCCH0CONFIG
                                    271 	.globl __XPAGE
                                    272 	.globl _XPAGE
                                    273 	.globl _SP
                                    274 	.globl _PSW
                                    275 	.globl _PCON
                                    276 	.globl _IP
                                    277 	.globl _IE
                                    278 	.globl _EIP
                                    279 	.globl _EIE
                                    280 	.globl _E2IP
                                    281 	.globl _E2IE
                                    282 	.globl _DPS
                                    283 	.globl _DPTR1
                                    284 	.globl _DPTR0
                                    285 	.globl _DPL1
                                    286 	.globl _DPL
                                    287 	.globl _DPH1
                                    288 	.globl _DPH
                                    289 	.globl _B
                                    290 	.globl _ACC
                                    291 	.globl _UART_Calc_CRC32_PARM_3
                                    292 	.globl _UART_Calc_CRC32_PARM_2
                                    293 	.globl _UART_Proc_SendMessage_PARM_3
                                    294 	.globl _UART_Proc_SendMessage_PARM_2
                                    295 	.globl _UART_Proc_ModifiyKissSpecialCharacters_PARM_2
                                    296 	.globl _UART_Proc_VerifyIncomingMsg_PARM_3
                                    297 	.globl _UART_Proc_VerifyIncomingMsg_PARM_2
                                    298 	.globl _AX5043_TIMEGAIN3NB
                                    299 	.globl _AX5043_TIMEGAIN2NB
                                    300 	.globl _AX5043_TIMEGAIN1NB
                                    301 	.globl _AX5043_TIMEGAIN0NB
                                    302 	.globl _AX5043_RXPARAMSETSNB
                                    303 	.globl _AX5043_RXPARAMCURSETNB
                                    304 	.globl _AX5043_PKTMAXLENNB
                                    305 	.globl _AX5043_PKTLENOFFSETNB
                                    306 	.globl _AX5043_PKTLENCFGNB
                                    307 	.globl _AX5043_PKTADDRMASK3NB
                                    308 	.globl _AX5043_PKTADDRMASK2NB
                                    309 	.globl _AX5043_PKTADDRMASK1NB
                                    310 	.globl _AX5043_PKTADDRMASK0NB
                                    311 	.globl _AX5043_PKTADDRCFGNB
                                    312 	.globl _AX5043_PKTADDR3NB
                                    313 	.globl _AX5043_PKTADDR2NB
                                    314 	.globl _AX5043_PKTADDR1NB
                                    315 	.globl _AX5043_PKTADDR0NB
                                    316 	.globl _AX5043_PHASEGAIN3NB
                                    317 	.globl _AX5043_PHASEGAIN2NB
                                    318 	.globl _AX5043_PHASEGAIN1NB
                                    319 	.globl _AX5043_PHASEGAIN0NB
                                    320 	.globl _AX5043_FREQUENCYLEAKNB
                                    321 	.globl _AX5043_FREQUENCYGAIND3NB
                                    322 	.globl _AX5043_FREQUENCYGAIND2NB
                                    323 	.globl _AX5043_FREQUENCYGAIND1NB
                                    324 	.globl _AX5043_FREQUENCYGAIND0NB
                                    325 	.globl _AX5043_FREQUENCYGAINC3NB
                                    326 	.globl _AX5043_FREQUENCYGAINC2NB
                                    327 	.globl _AX5043_FREQUENCYGAINC1NB
                                    328 	.globl _AX5043_FREQUENCYGAINC0NB
                                    329 	.globl _AX5043_FREQUENCYGAINB3NB
                                    330 	.globl _AX5043_FREQUENCYGAINB2NB
                                    331 	.globl _AX5043_FREQUENCYGAINB1NB
                                    332 	.globl _AX5043_FREQUENCYGAINB0NB
                                    333 	.globl _AX5043_FREQUENCYGAINA3NB
                                    334 	.globl _AX5043_FREQUENCYGAINA2NB
                                    335 	.globl _AX5043_FREQUENCYGAINA1NB
                                    336 	.globl _AX5043_FREQUENCYGAINA0NB
                                    337 	.globl _AX5043_FREQDEV13NB
                                    338 	.globl _AX5043_FREQDEV12NB
                                    339 	.globl _AX5043_FREQDEV11NB
                                    340 	.globl _AX5043_FREQDEV10NB
                                    341 	.globl _AX5043_FREQDEV03NB
                                    342 	.globl _AX5043_FREQDEV02NB
                                    343 	.globl _AX5043_FREQDEV01NB
                                    344 	.globl _AX5043_FREQDEV00NB
                                    345 	.globl _AX5043_FOURFSK3NB
                                    346 	.globl _AX5043_FOURFSK2NB
                                    347 	.globl _AX5043_FOURFSK1NB
                                    348 	.globl _AX5043_FOURFSK0NB
                                    349 	.globl _AX5043_DRGAIN3NB
                                    350 	.globl _AX5043_DRGAIN2NB
                                    351 	.globl _AX5043_DRGAIN1NB
                                    352 	.globl _AX5043_DRGAIN0NB
                                    353 	.globl _AX5043_BBOFFSRES3NB
                                    354 	.globl _AX5043_BBOFFSRES2NB
                                    355 	.globl _AX5043_BBOFFSRES1NB
                                    356 	.globl _AX5043_BBOFFSRES0NB
                                    357 	.globl _AX5043_AMPLITUDEGAIN3NB
                                    358 	.globl _AX5043_AMPLITUDEGAIN2NB
                                    359 	.globl _AX5043_AMPLITUDEGAIN1NB
                                    360 	.globl _AX5043_AMPLITUDEGAIN0NB
                                    361 	.globl _AX5043_AGCTARGET3NB
                                    362 	.globl _AX5043_AGCTARGET2NB
                                    363 	.globl _AX5043_AGCTARGET1NB
                                    364 	.globl _AX5043_AGCTARGET0NB
                                    365 	.globl _AX5043_AGCMINMAX3NB
                                    366 	.globl _AX5043_AGCMINMAX2NB
                                    367 	.globl _AX5043_AGCMINMAX1NB
                                    368 	.globl _AX5043_AGCMINMAX0NB
                                    369 	.globl _AX5043_AGCGAIN3NB
                                    370 	.globl _AX5043_AGCGAIN2NB
                                    371 	.globl _AX5043_AGCGAIN1NB
                                    372 	.globl _AX5043_AGCGAIN0NB
                                    373 	.globl _AX5043_AGCAHYST3NB
                                    374 	.globl _AX5043_AGCAHYST2NB
                                    375 	.globl _AX5043_AGCAHYST1NB
                                    376 	.globl _AX5043_AGCAHYST0NB
                                    377 	.globl _AX5043_0xF44NB
                                    378 	.globl _AX5043_0xF35NB
                                    379 	.globl _AX5043_0xF34NB
                                    380 	.globl _AX5043_0xF33NB
                                    381 	.globl _AX5043_0xF32NB
                                    382 	.globl _AX5043_0xF31NB
                                    383 	.globl _AX5043_0xF30NB
                                    384 	.globl _AX5043_0xF26NB
                                    385 	.globl _AX5043_0xF23NB
                                    386 	.globl _AX5043_0xF22NB
                                    387 	.globl _AX5043_0xF21NB
                                    388 	.globl _AX5043_0xF1CNB
                                    389 	.globl _AX5043_0xF18NB
                                    390 	.globl _AX5043_0xF0CNB
                                    391 	.globl _AX5043_0xF00NB
                                    392 	.globl _AX5043_XTALSTATUSNB
                                    393 	.globl _AX5043_XTALOSCNB
                                    394 	.globl _AX5043_XTALCAPNB
                                    395 	.globl _AX5043_XTALAMPLNB
                                    396 	.globl _AX5043_WAKEUPXOEARLYNB
                                    397 	.globl _AX5043_WAKEUPTIMER1NB
                                    398 	.globl _AX5043_WAKEUPTIMER0NB
                                    399 	.globl _AX5043_WAKEUPFREQ1NB
                                    400 	.globl _AX5043_WAKEUPFREQ0NB
                                    401 	.globl _AX5043_WAKEUP1NB
                                    402 	.globl _AX5043_WAKEUP0NB
                                    403 	.globl _AX5043_TXRATE2NB
                                    404 	.globl _AX5043_TXRATE1NB
                                    405 	.globl _AX5043_TXRATE0NB
                                    406 	.globl _AX5043_TXPWRCOEFFE1NB
                                    407 	.globl _AX5043_TXPWRCOEFFE0NB
                                    408 	.globl _AX5043_TXPWRCOEFFD1NB
                                    409 	.globl _AX5043_TXPWRCOEFFD0NB
                                    410 	.globl _AX5043_TXPWRCOEFFC1NB
                                    411 	.globl _AX5043_TXPWRCOEFFC0NB
                                    412 	.globl _AX5043_TXPWRCOEFFB1NB
                                    413 	.globl _AX5043_TXPWRCOEFFB0NB
                                    414 	.globl _AX5043_TXPWRCOEFFA1NB
                                    415 	.globl _AX5043_TXPWRCOEFFA0NB
                                    416 	.globl _AX5043_TRKRFFREQ2NB
                                    417 	.globl _AX5043_TRKRFFREQ1NB
                                    418 	.globl _AX5043_TRKRFFREQ0NB
                                    419 	.globl _AX5043_TRKPHASE1NB
                                    420 	.globl _AX5043_TRKPHASE0NB
                                    421 	.globl _AX5043_TRKFSKDEMOD1NB
                                    422 	.globl _AX5043_TRKFSKDEMOD0NB
                                    423 	.globl _AX5043_TRKFREQ1NB
                                    424 	.globl _AX5043_TRKFREQ0NB
                                    425 	.globl _AX5043_TRKDATARATE2NB
                                    426 	.globl _AX5043_TRKDATARATE1NB
                                    427 	.globl _AX5043_TRKDATARATE0NB
                                    428 	.globl _AX5043_TRKAMPLITUDE1NB
                                    429 	.globl _AX5043_TRKAMPLITUDE0NB
                                    430 	.globl _AX5043_TRKAFSKDEMOD1NB
                                    431 	.globl _AX5043_TRKAFSKDEMOD0NB
                                    432 	.globl _AX5043_TMGTXSETTLENB
                                    433 	.globl _AX5043_TMGTXBOOSTNB
                                    434 	.globl _AX5043_TMGRXSETTLENB
                                    435 	.globl _AX5043_TMGRXRSSINB
                                    436 	.globl _AX5043_TMGRXPREAMBLE3NB
                                    437 	.globl _AX5043_TMGRXPREAMBLE2NB
                                    438 	.globl _AX5043_TMGRXPREAMBLE1NB
                                    439 	.globl _AX5043_TMGRXOFFSACQNB
                                    440 	.globl _AX5043_TMGRXCOARSEAGCNB
                                    441 	.globl _AX5043_TMGRXBOOSTNB
                                    442 	.globl _AX5043_TMGRXAGCNB
                                    443 	.globl _AX5043_TIMER2NB
                                    444 	.globl _AX5043_TIMER1NB
                                    445 	.globl _AX5043_TIMER0NB
                                    446 	.globl _AX5043_SILICONREVISIONNB
                                    447 	.globl _AX5043_SCRATCHNB
                                    448 	.globl _AX5043_RXDATARATE2NB
                                    449 	.globl _AX5043_RXDATARATE1NB
                                    450 	.globl _AX5043_RXDATARATE0NB
                                    451 	.globl _AX5043_RSSIREFERENCENB
                                    452 	.globl _AX5043_RSSIABSTHRNB
                                    453 	.globl _AX5043_RSSINB
                                    454 	.globl _AX5043_REFNB
                                    455 	.globl _AX5043_RADIOSTATENB
                                    456 	.globl _AX5043_RADIOEVENTREQ1NB
                                    457 	.globl _AX5043_RADIOEVENTREQ0NB
                                    458 	.globl _AX5043_RADIOEVENTMASK1NB
                                    459 	.globl _AX5043_RADIOEVENTMASK0NB
                                    460 	.globl _AX5043_PWRMODENB
                                    461 	.globl _AX5043_PWRAMPNB
                                    462 	.globl _AX5043_POWSTICKYSTATNB
                                    463 	.globl _AX5043_POWSTATNB
                                    464 	.globl _AX5043_POWIRQMASKNB
                                    465 	.globl _AX5043_POWCTRL1NB
                                    466 	.globl _AX5043_PLLVCOIRNB
                                    467 	.globl _AX5043_PLLVCOINB
                                    468 	.globl _AX5043_PLLVCODIVNB
                                    469 	.globl _AX5043_PLLRNGCLKNB
                                    470 	.globl _AX5043_PLLRANGINGBNB
                                    471 	.globl _AX5043_PLLRANGINGANB
                                    472 	.globl _AX5043_PLLLOOPBOOSTNB
                                    473 	.globl _AX5043_PLLLOOPNB
                                    474 	.globl _AX5043_PLLLOCKDETNB
                                    475 	.globl _AX5043_PLLCPIBOOSTNB
                                    476 	.globl _AX5043_PLLCPINB
                                    477 	.globl _AX5043_PKTSTOREFLAGSNB
                                    478 	.globl _AX5043_PKTMISCFLAGSNB
                                    479 	.globl _AX5043_PKTCHUNKSIZENB
                                    480 	.globl _AX5043_PKTACCEPTFLAGSNB
                                    481 	.globl _AX5043_PINSTATENB
                                    482 	.globl _AX5043_PINFUNCSYSCLKNB
                                    483 	.globl _AX5043_PINFUNCPWRAMPNB
                                    484 	.globl _AX5043_PINFUNCIRQNB
                                    485 	.globl _AX5043_PINFUNCDCLKNB
                                    486 	.globl _AX5043_PINFUNCDATANB
                                    487 	.globl _AX5043_PINFUNCANTSELNB
                                    488 	.globl _AX5043_MODULATIONNB
                                    489 	.globl _AX5043_MODCFGPNB
                                    490 	.globl _AX5043_MODCFGFNB
                                    491 	.globl _AX5043_MODCFGANB
                                    492 	.globl _AX5043_MAXRFOFFSET2NB
                                    493 	.globl _AX5043_MAXRFOFFSET1NB
                                    494 	.globl _AX5043_MAXRFOFFSET0NB
                                    495 	.globl _AX5043_MAXDROFFSET2NB
                                    496 	.globl _AX5043_MAXDROFFSET1NB
                                    497 	.globl _AX5043_MAXDROFFSET0NB
                                    498 	.globl _AX5043_MATCH1PAT1NB
                                    499 	.globl _AX5043_MATCH1PAT0NB
                                    500 	.globl _AX5043_MATCH1MINNB
                                    501 	.globl _AX5043_MATCH1MAXNB
                                    502 	.globl _AX5043_MATCH1LENNB
                                    503 	.globl _AX5043_MATCH0PAT3NB
                                    504 	.globl _AX5043_MATCH0PAT2NB
                                    505 	.globl _AX5043_MATCH0PAT1NB
                                    506 	.globl _AX5043_MATCH0PAT0NB
                                    507 	.globl _AX5043_MATCH0MINNB
                                    508 	.globl _AX5043_MATCH0MAXNB
                                    509 	.globl _AX5043_MATCH0LENNB
                                    510 	.globl _AX5043_LPOSCSTATUSNB
                                    511 	.globl _AX5043_LPOSCREF1NB
                                    512 	.globl _AX5043_LPOSCREF0NB
                                    513 	.globl _AX5043_LPOSCPER1NB
                                    514 	.globl _AX5043_LPOSCPER0NB
                                    515 	.globl _AX5043_LPOSCKFILT1NB
                                    516 	.globl _AX5043_LPOSCKFILT0NB
                                    517 	.globl _AX5043_LPOSCFREQ1NB
                                    518 	.globl _AX5043_LPOSCFREQ0NB
                                    519 	.globl _AX5043_LPOSCCONFIGNB
                                    520 	.globl _AX5043_IRQREQUEST1NB
                                    521 	.globl _AX5043_IRQREQUEST0NB
                                    522 	.globl _AX5043_IRQMASK1NB
                                    523 	.globl _AX5043_IRQMASK0NB
                                    524 	.globl _AX5043_IRQINVERSION1NB
                                    525 	.globl _AX5043_IRQINVERSION0NB
                                    526 	.globl _AX5043_IFFREQ1NB
                                    527 	.globl _AX5043_IFFREQ0NB
                                    528 	.globl _AX5043_GPADCPERIODNB
                                    529 	.globl _AX5043_GPADCCTRLNB
                                    530 	.globl _AX5043_GPADC13VALUE1NB
                                    531 	.globl _AX5043_GPADC13VALUE0NB
                                    532 	.globl _AX5043_FSKDMIN1NB
                                    533 	.globl _AX5043_FSKDMIN0NB
                                    534 	.globl _AX5043_FSKDMAX1NB
                                    535 	.globl _AX5043_FSKDMAX0NB
                                    536 	.globl _AX5043_FSKDEV2NB
                                    537 	.globl _AX5043_FSKDEV1NB
                                    538 	.globl _AX5043_FSKDEV0NB
                                    539 	.globl _AX5043_FREQB3NB
                                    540 	.globl _AX5043_FREQB2NB
                                    541 	.globl _AX5043_FREQB1NB
                                    542 	.globl _AX5043_FREQB0NB
                                    543 	.globl _AX5043_FREQA3NB
                                    544 	.globl _AX5043_FREQA2NB
                                    545 	.globl _AX5043_FREQA1NB
                                    546 	.globl _AX5043_FREQA0NB
                                    547 	.globl _AX5043_FRAMINGNB
                                    548 	.globl _AX5043_FIFOTHRESH1NB
                                    549 	.globl _AX5043_FIFOTHRESH0NB
                                    550 	.globl _AX5043_FIFOSTATNB
                                    551 	.globl _AX5043_FIFOFREE1NB
                                    552 	.globl _AX5043_FIFOFREE0NB
                                    553 	.globl _AX5043_FIFODATANB
                                    554 	.globl _AX5043_FIFOCOUNT1NB
                                    555 	.globl _AX5043_FIFOCOUNT0NB
                                    556 	.globl _AX5043_FECSYNCNB
                                    557 	.globl _AX5043_FECSTATUSNB
                                    558 	.globl _AX5043_FECNB
                                    559 	.globl _AX5043_ENCODINGNB
                                    560 	.globl _AX5043_DIVERSITYNB
                                    561 	.globl _AX5043_DECIMATIONNB
                                    562 	.globl _AX5043_DACVALUE1NB
                                    563 	.globl _AX5043_DACVALUE0NB
                                    564 	.globl _AX5043_DACCONFIGNB
                                    565 	.globl _AX5043_CRCINIT3NB
                                    566 	.globl _AX5043_CRCINIT2NB
                                    567 	.globl _AX5043_CRCINIT1NB
                                    568 	.globl _AX5043_CRCINIT0NB
                                    569 	.globl _AX5043_BGNDRSSITHRNB
                                    570 	.globl _AX5043_BGNDRSSIGAINNB
                                    571 	.globl _AX5043_BGNDRSSINB
                                    572 	.globl _AX5043_BBTUNENB
                                    573 	.globl _AX5043_BBOFFSCAPNB
                                    574 	.globl _AX5043_AMPLFILTERNB
                                    575 	.globl _AX5043_AGCCOUNTERNB
                                    576 	.globl _AX5043_AFSKSPACE1NB
                                    577 	.globl _AX5043_AFSKSPACE0NB
                                    578 	.globl _AX5043_AFSKMARK1NB
                                    579 	.globl _AX5043_AFSKMARK0NB
                                    580 	.globl _AX5043_AFSKCTRLNB
                                    581 	.globl _AX5043_TIMEGAIN3
                                    582 	.globl _AX5043_TIMEGAIN2
                                    583 	.globl _AX5043_TIMEGAIN1
                                    584 	.globl _AX5043_TIMEGAIN0
                                    585 	.globl _AX5043_RXPARAMSETS
                                    586 	.globl _AX5043_RXPARAMCURSET
                                    587 	.globl _AX5043_PKTMAXLEN
                                    588 	.globl _AX5043_PKTLENOFFSET
                                    589 	.globl _AX5043_PKTLENCFG
                                    590 	.globl _AX5043_PKTADDRMASK3
                                    591 	.globl _AX5043_PKTADDRMASK2
                                    592 	.globl _AX5043_PKTADDRMASK1
                                    593 	.globl _AX5043_PKTADDRMASK0
                                    594 	.globl _AX5043_PKTADDRCFG
                                    595 	.globl _AX5043_PKTADDR3
                                    596 	.globl _AX5043_PKTADDR2
                                    597 	.globl _AX5043_PKTADDR1
                                    598 	.globl _AX5043_PKTADDR0
                                    599 	.globl _AX5043_PHASEGAIN3
                                    600 	.globl _AX5043_PHASEGAIN2
                                    601 	.globl _AX5043_PHASEGAIN1
                                    602 	.globl _AX5043_PHASEGAIN0
                                    603 	.globl _AX5043_FREQUENCYLEAK
                                    604 	.globl _AX5043_FREQUENCYGAIND3
                                    605 	.globl _AX5043_FREQUENCYGAIND2
                                    606 	.globl _AX5043_FREQUENCYGAIND1
                                    607 	.globl _AX5043_FREQUENCYGAIND0
                                    608 	.globl _AX5043_FREQUENCYGAINC3
                                    609 	.globl _AX5043_FREQUENCYGAINC2
                                    610 	.globl _AX5043_FREQUENCYGAINC1
                                    611 	.globl _AX5043_FREQUENCYGAINC0
                                    612 	.globl _AX5043_FREQUENCYGAINB3
                                    613 	.globl _AX5043_FREQUENCYGAINB2
                                    614 	.globl _AX5043_FREQUENCYGAINB1
                                    615 	.globl _AX5043_FREQUENCYGAINB0
                                    616 	.globl _AX5043_FREQUENCYGAINA3
                                    617 	.globl _AX5043_FREQUENCYGAINA2
                                    618 	.globl _AX5043_FREQUENCYGAINA1
                                    619 	.globl _AX5043_FREQUENCYGAINA0
                                    620 	.globl _AX5043_FREQDEV13
                                    621 	.globl _AX5043_FREQDEV12
                                    622 	.globl _AX5043_FREQDEV11
                                    623 	.globl _AX5043_FREQDEV10
                                    624 	.globl _AX5043_FREQDEV03
                                    625 	.globl _AX5043_FREQDEV02
                                    626 	.globl _AX5043_FREQDEV01
                                    627 	.globl _AX5043_FREQDEV00
                                    628 	.globl _AX5043_FOURFSK3
                                    629 	.globl _AX5043_FOURFSK2
                                    630 	.globl _AX5043_FOURFSK1
                                    631 	.globl _AX5043_FOURFSK0
                                    632 	.globl _AX5043_DRGAIN3
                                    633 	.globl _AX5043_DRGAIN2
                                    634 	.globl _AX5043_DRGAIN1
                                    635 	.globl _AX5043_DRGAIN0
                                    636 	.globl _AX5043_BBOFFSRES3
                                    637 	.globl _AX5043_BBOFFSRES2
                                    638 	.globl _AX5043_BBOFFSRES1
                                    639 	.globl _AX5043_BBOFFSRES0
                                    640 	.globl _AX5043_AMPLITUDEGAIN3
                                    641 	.globl _AX5043_AMPLITUDEGAIN2
                                    642 	.globl _AX5043_AMPLITUDEGAIN1
                                    643 	.globl _AX5043_AMPLITUDEGAIN0
                                    644 	.globl _AX5043_AGCTARGET3
                                    645 	.globl _AX5043_AGCTARGET2
                                    646 	.globl _AX5043_AGCTARGET1
                                    647 	.globl _AX5043_AGCTARGET0
                                    648 	.globl _AX5043_AGCMINMAX3
                                    649 	.globl _AX5043_AGCMINMAX2
                                    650 	.globl _AX5043_AGCMINMAX1
                                    651 	.globl _AX5043_AGCMINMAX0
                                    652 	.globl _AX5043_AGCGAIN3
                                    653 	.globl _AX5043_AGCGAIN2
                                    654 	.globl _AX5043_AGCGAIN1
                                    655 	.globl _AX5043_AGCGAIN0
                                    656 	.globl _AX5043_AGCAHYST3
                                    657 	.globl _AX5043_AGCAHYST2
                                    658 	.globl _AX5043_AGCAHYST1
                                    659 	.globl _AX5043_AGCAHYST0
                                    660 	.globl _AX5043_0xF44
                                    661 	.globl _AX5043_0xF35
                                    662 	.globl _AX5043_0xF34
                                    663 	.globl _AX5043_0xF33
                                    664 	.globl _AX5043_0xF32
                                    665 	.globl _AX5043_0xF31
                                    666 	.globl _AX5043_0xF30
                                    667 	.globl _AX5043_0xF26
                                    668 	.globl _AX5043_0xF23
                                    669 	.globl _AX5043_0xF22
                                    670 	.globl _AX5043_0xF21
                                    671 	.globl _AX5043_0xF1C
                                    672 	.globl _AX5043_0xF18
                                    673 	.globl _AX5043_0xF0C
                                    674 	.globl _AX5043_0xF00
                                    675 	.globl _AX5043_XTALSTATUS
                                    676 	.globl _AX5043_XTALOSC
                                    677 	.globl _AX5043_XTALCAP
                                    678 	.globl _AX5043_XTALAMPL
                                    679 	.globl _AX5043_WAKEUPXOEARLY
                                    680 	.globl _AX5043_WAKEUPTIMER1
                                    681 	.globl _AX5043_WAKEUPTIMER0
                                    682 	.globl _AX5043_WAKEUPFREQ1
                                    683 	.globl _AX5043_WAKEUPFREQ0
                                    684 	.globl _AX5043_WAKEUP1
                                    685 	.globl _AX5043_WAKEUP0
                                    686 	.globl _AX5043_TXRATE2
                                    687 	.globl _AX5043_TXRATE1
                                    688 	.globl _AX5043_TXRATE0
                                    689 	.globl _AX5043_TXPWRCOEFFE1
                                    690 	.globl _AX5043_TXPWRCOEFFE0
                                    691 	.globl _AX5043_TXPWRCOEFFD1
                                    692 	.globl _AX5043_TXPWRCOEFFD0
                                    693 	.globl _AX5043_TXPWRCOEFFC1
                                    694 	.globl _AX5043_TXPWRCOEFFC0
                                    695 	.globl _AX5043_TXPWRCOEFFB1
                                    696 	.globl _AX5043_TXPWRCOEFFB0
                                    697 	.globl _AX5043_TXPWRCOEFFA1
                                    698 	.globl _AX5043_TXPWRCOEFFA0
                                    699 	.globl _AX5043_TRKRFFREQ2
                                    700 	.globl _AX5043_TRKRFFREQ1
                                    701 	.globl _AX5043_TRKRFFREQ0
                                    702 	.globl _AX5043_TRKPHASE1
                                    703 	.globl _AX5043_TRKPHASE0
                                    704 	.globl _AX5043_TRKFSKDEMOD1
                                    705 	.globl _AX5043_TRKFSKDEMOD0
                                    706 	.globl _AX5043_TRKFREQ1
                                    707 	.globl _AX5043_TRKFREQ0
                                    708 	.globl _AX5043_TRKDATARATE2
                                    709 	.globl _AX5043_TRKDATARATE1
                                    710 	.globl _AX5043_TRKDATARATE0
                                    711 	.globl _AX5043_TRKAMPLITUDE1
                                    712 	.globl _AX5043_TRKAMPLITUDE0
                                    713 	.globl _AX5043_TRKAFSKDEMOD1
                                    714 	.globl _AX5043_TRKAFSKDEMOD0
                                    715 	.globl _AX5043_TMGTXSETTLE
                                    716 	.globl _AX5043_TMGTXBOOST
                                    717 	.globl _AX5043_TMGRXSETTLE
                                    718 	.globl _AX5043_TMGRXRSSI
                                    719 	.globl _AX5043_TMGRXPREAMBLE3
                                    720 	.globl _AX5043_TMGRXPREAMBLE2
                                    721 	.globl _AX5043_TMGRXPREAMBLE1
                                    722 	.globl _AX5043_TMGRXOFFSACQ
                                    723 	.globl _AX5043_TMGRXCOARSEAGC
                                    724 	.globl _AX5043_TMGRXBOOST
                                    725 	.globl _AX5043_TMGRXAGC
                                    726 	.globl _AX5043_TIMER2
                                    727 	.globl _AX5043_TIMER1
                                    728 	.globl _AX5043_TIMER0
                                    729 	.globl _AX5043_SILICONREVISION
                                    730 	.globl _AX5043_SCRATCH
                                    731 	.globl _AX5043_RXDATARATE2
                                    732 	.globl _AX5043_RXDATARATE1
                                    733 	.globl _AX5043_RXDATARATE0
                                    734 	.globl _AX5043_RSSIREFERENCE
                                    735 	.globl _AX5043_RSSIABSTHR
                                    736 	.globl _AX5043_RSSI
                                    737 	.globl _AX5043_REF
                                    738 	.globl _AX5043_RADIOSTATE
                                    739 	.globl _AX5043_RADIOEVENTREQ1
                                    740 	.globl _AX5043_RADIOEVENTREQ0
                                    741 	.globl _AX5043_RADIOEVENTMASK1
                                    742 	.globl _AX5043_RADIOEVENTMASK0
                                    743 	.globl _AX5043_PWRMODE
                                    744 	.globl _AX5043_PWRAMP
                                    745 	.globl _AX5043_POWSTICKYSTAT
                                    746 	.globl _AX5043_POWSTAT
                                    747 	.globl _AX5043_POWIRQMASK
                                    748 	.globl _AX5043_POWCTRL1
                                    749 	.globl _AX5043_PLLVCOIR
                                    750 	.globl _AX5043_PLLVCOI
                                    751 	.globl _AX5043_PLLVCODIV
                                    752 	.globl _AX5043_PLLRNGCLK
                                    753 	.globl _AX5043_PLLRANGINGB
                                    754 	.globl _AX5043_PLLRANGINGA
                                    755 	.globl _AX5043_PLLLOOPBOOST
                                    756 	.globl _AX5043_PLLLOOP
                                    757 	.globl _AX5043_PLLLOCKDET
                                    758 	.globl _AX5043_PLLCPIBOOST
                                    759 	.globl _AX5043_PLLCPI
                                    760 	.globl _AX5043_PKTSTOREFLAGS
                                    761 	.globl _AX5043_PKTMISCFLAGS
                                    762 	.globl _AX5043_PKTCHUNKSIZE
                                    763 	.globl _AX5043_PKTACCEPTFLAGS
                                    764 	.globl _AX5043_PINSTATE
                                    765 	.globl _AX5043_PINFUNCSYSCLK
                                    766 	.globl _AX5043_PINFUNCPWRAMP
                                    767 	.globl _AX5043_PINFUNCIRQ
                                    768 	.globl _AX5043_PINFUNCDCLK
                                    769 	.globl _AX5043_PINFUNCDATA
                                    770 	.globl _AX5043_PINFUNCANTSEL
                                    771 	.globl _AX5043_MODULATION
                                    772 	.globl _AX5043_MODCFGP
                                    773 	.globl _AX5043_MODCFGF
                                    774 	.globl _AX5043_MODCFGA
                                    775 	.globl _AX5043_MAXRFOFFSET2
                                    776 	.globl _AX5043_MAXRFOFFSET1
                                    777 	.globl _AX5043_MAXRFOFFSET0
                                    778 	.globl _AX5043_MAXDROFFSET2
                                    779 	.globl _AX5043_MAXDROFFSET1
                                    780 	.globl _AX5043_MAXDROFFSET0
                                    781 	.globl _AX5043_MATCH1PAT1
                                    782 	.globl _AX5043_MATCH1PAT0
                                    783 	.globl _AX5043_MATCH1MIN
                                    784 	.globl _AX5043_MATCH1MAX
                                    785 	.globl _AX5043_MATCH1LEN
                                    786 	.globl _AX5043_MATCH0PAT3
                                    787 	.globl _AX5043_MATCH0PAT2
                                    788 	.globl _AX5043_MATCH0PAT1
                                    789 	.globl _AX5043_MATCH0PAT0
                                    790 	.globl _AX5043_MATCH0MIN
                                    791 	.globl _AX5043_MATCH0MAX
                                    792 	.globl _AX5043_MATCH0LEN
                                    793 	.globl _AX5043_LPOSCSTATUS
                                    794 	.globl _AX5043_LPOSCREF1
                                    795 	.globl _AX5043_LPOSCREF0
                                    796 	.globl _AX5043_LPOSCPER1
                                    797 	.globl _AX5043_LPOSCPER0
                                    798 	.globl _AX5043_LPOSCKFILT1
                                    799 	.globl _AX5043_LPOSCKFILT0
                                    800 	.globl _AX5043_LPOSCFREQ1
                                    801 	.globl _AX5043_LPOSCFREQ0
                                    802 	.globl _AX5043_LPOSCCONFIG
                                    803 	.globl _AX5043_IRQREQUEST1
                                    804 	.globl _AX5043_IRQREQUEST0
                                    805 	.globl _AX5043_IRQMASK1
                                    806 	.globl _AX5043_IRQMASK0
                                    807 	.globl _AX5043_IRQINVERSION1
                                    808 	.globl _AX5043_IRQINVERSION0
                                    809 	.globl _AX5043_IFFREQ1
                                    810 	.globl _AX5043_IFFREQ0
                                    811 	.globl _AX5043_GPADCPERIOD
                                    812 	.globl _AX5043_GPADCCTRL
                                    813 	.globl _AX5043_GPADC13VALUE1
                                    814 	.globl _AX5043_GPADC13VALUE0
                                    815 	.globl _AX5043_FSKDMIN1
                                    816 	.globl _AX5043_FSKDMIN0
                                    817 	.globl _AX5043_FSKDMAX1
                                    818 	.globl _AX5043_FSKDMAX0
                                    819 	.globl _AX5043_FSKDEV2
                                    820 	.globl _AX5043_FSKDEV1
                                    821 	.globl _AX5043_FSKDEV0
                                    822 	.globl _AX5043_FREQB3
                                    823 	.globl _AX5043_FREQB2
                                    824 	.globl _AX5043_FREQB1
                                    825 	.globl _AX5043_FREQB0
                                    826 	.globl _AX5043_FREQA3
                                    827 	.globl _AX5043_FREQA2
                                    828 	.globl _AX5043_FREQA1
                                    829 	.globl _AX5043_FREQA0
                                    830 	.globl _AX5043_FRAMING
                                    831 	.globl _AX5043_FIFOTHRESH1
                                    832 	.globl _AX5043_FIFOTHRESH0
                                    833 	.globl _AX5043_FIFOSTAT
                                    834 	.globl _AX5043_FIFOFREE1
                                    835 	.globl _AX5043_FIFOFREE0
                                    836 	.globl _AX5043_FIFODATA
                                    837 	.globl _AX5043_FIFOCOUNT1
                                    838 	.globl _AX5043_FIFOCOUNT0
                                    839 	.globl _AX5043_FECSYNC
                                    840 	.globl _AX5043_FECSTATUS
                                    841 	.globl _AX5043_FEC
                                    842 	.globl _AX5043_ENCODING
                                    843 	.globl _AX5043_DIVERSITY
                                    844 	.globl _AX5043_DECIMATION
                                    845 	.globl _AX5043_DACVALUE1
                                    846 	.globl _AX5043_DACVALUE0
                                    847 	.globl _AX5043_DACCONFIG
                                    848 	.globl _AX5043_CRCINIT3
                                    849 	.globl _AX5043_CRCINIT2
                                    850 	.globl _AX5043_CRCINIT1
                                    851 	.globl _AX5043_CRCINIT0
                                    852 	.globl _AX5043_BGNDRSSITHR
                                    853 	.globl _AX5043_BGNDRSSIGAIN
                                    854 	.globl _AX5043_BGNDRSSI
                                    855 	.globl _AX5043_BBTUNE
                                    856 	.globl _AX5043_BBOFFSCAP
                                    857 	.globl _AX5043_AMPLFILTER
                                    858 	.globl _AX5043_AGCCOUNTER
                                    859 	.globl _AX5043_AFSKSPACE1
                                    860 	.globl _AX5043_AFSKSPACE0
                                    861 	.globl _AX5043_AFSKMARK1
                                    862 	.globl _AX5043_AFSKMARK0
                                    863 	.globl _AX5043_AFSKCTRL
                                    864 	.globl _XWTSTAT
                                    865 	.globl _XWTIRQEN
                                    866 	.globl _XWTEVTD
                                    867 	.globl _XWTEVTD1
                                    868 	.globl _XWTEVTD0
                                    869 	.globl _XWTEVTC
                                    870 	.globl _XWTEVTC1
                                    871 	.globl _XWTEVTC0
                                    872 	.globl _XWTEVTB
                                    873 	.globl _XWTEVTB1
                                    874 	.globl _XWTEVTB0
                                    875 	.globl _XWTEVTA
                                    876 	.globl _XWTEVTA1
                                    877 	.globl _XWTEVTA0
                                    878 	.globl _XWTCNTR1
                                    879 	.globl _XWTCNTB
                                    880 	.globl _XWTCNTB1
                                    881 	.globl _XWTCNTB0
                                    882 	.globl _XWTCNTA
                                    883 	.globl _XWTCNTA1
                                    884 	.globl _XWTCNTA0
                                    885 	.globl _XWTCFGB
                                    886 	.globl _XWTCFGA
                                    887 	.globl _XWDTRESET
                                    888 	.globl _XWDTCFG
                                    889 	.globl _XU1STATUS
                                    890 	.globl _XU1SHREG
                                    891 	.globl _XU1MODE
                                    892 	.globl _XU1CTRL
                                    893 	.globl _XU0STATUS
                                    894 	.globl _XU0SHREG
                                    895 	.globl _XU0MODE
                                    896 	.globl _XU0CTRL
                                    897 	.globl _XT2STATUS
                                    898 	.globl _XT2PERIOD
                                    899 	.globl _XT2PERIOD1
                                    900 	.globl _XT2PERIOD0
                                    901 	.globl _XT2MODE
                                    902 	.globl _XT2CNT
                                    903 	.globl _XT2CNT1
                                    904 	.globl _XT2CNT0
                                    905 	.globl _XT2CLKSRC
                                    906 	.globl _XT1STATUS
                                    907 	.globl _XT1PERIOD
                                    908 	.globl _XT1PERIOD1
                                    909 	.globl _XT1PERIOD0
                                    910 	.globl _XT1MODE
                                    911 	.globl _XT1CNT
                                    912 	.globl _XT1CNT1
                                    913 	.globl _XT1CNT0
                                    914 	.globl _XT1CLKSRC
                                    915 	.globl _XT0STATUS
                                    916 	.globl _XT0PERIOD
                                    917 	.globl _XT0PERIOD1
                                    918 	.globl _XT0PERIOD0
                                    919 	.globl _XT0MODE
                                    920 	.globl _XT0CNT
                                    921 	.globl _XT0CNT1
                                    922 	.globl _XT0CNT0
                                    923 	.globl _XT0CLKSRC
                                    924 	.globl _XSPSTATUS
                                    925 	.globl _XSPSHREG
                                    926 	.globl _XSPMODE
                                    927 	.globl _XSPCLKSRC
                                    928 	.globl _XRADIOSTAT
                                    929 	.globl _XRADIOSTAT1
                                    930 	.globl _XRADIOSTAT0
                                    931 	.globl _XRADIODATA3
                                    932 	.globl _XRADIODATA2
                                    933 	.globl _XRADIODATA1
                                    934 	.globl _XRADIODATA0
                                    935 	.globl _XRADIOADDR1
                                    936 	.globl _XRADIOADDR0
                                    937 	.globl _XRADIOACC
                                    938 	.globl _XOC1STATUS
                                    939 	.globl _XOC1PIN
                                    940 	.globl _XOC1MODE
                                    941 	.globl _XOC1COMP
                                    942 	.globl _XOC1COMP1
                                    943 	.globl _XOC1COMP0
                                    944 	.globl _XOC0STATUS
                                    945 	.globl _XOC0PIN
                                    946 	.globl _XOC0MODE
                                    947 	.globl _XOC0COMP
                                    948 	.globl _XOC0COMP1
                                    949 	.globl _XOC0COMP0
                                    950 	.globl _XNVSTATUS
                                    951 	.globl _XNVKEY
                                    952 	.globl _XNVDATA
                                    953 	.globl _XNVDATA1
                                    954 	.globl _XNVDATA0
                                    955 	.globl _XNVADDR
                                    956 	.globl _XNVADDR1
                                    957 	.globl _XNVADDR0
                                    958 	.globl _XIC1STATUS
                                    959 	.globl _XIC1MODE
                                    960 	.globl _XIC1CAPT
                                    961 	.globl _XIC1CAPT1
                                    962 	.globl _XIC1CAPT0
                                    963 	.globl _XIC0STATUS
                                    964 	.globl _XIC0MODE
                                    965 	.globl _XIC0CAPT
                                    966 	.globl _XIC0CAPT1
                                    967 	.globl _XIC0CAPT0
                                    968 	.globl _XPORTR
                                    969 	.globl _XPORTC
                                    970 	.globl _XPORTB
                                    971 	.globl _XPORTA
                                    972 	.globl _XPINR
                                    973 	.globl _XPINC
                                    974 	.globl _XPINB
                                    975 	.globl _XPINA
                                    976 	.globl _XDIRR
                                    977 	.globl _XDIRC
                                    978 	.globl _XDIRB
                                    979 	.globl _XDIRA
                                    980 	.globl _XDBGLNKSTAT
                                    981 	.globl _XDBGLNKBUF
                                    982 	.globl _XCODECONFIG
                                    983 	.globl _XCLKSTAT
                                    984 	.globl _XCLKCON
                                    985 	.globl _XANALOGCOMP
                                    986 	.globl _XADCCONV
                                    987 	.globl _XADCCLKSRC
                                    988 	.globl _XADCCH3CONFIG
                                    989 	.globl _XADCCH2CONFIG
                                    990 	.globl _XADCCH1CONFIG
                                    991 	.globl _XADCCH0CONFIG
                                    992 	.globl _XPCON
                                    993 	.globl _XIP
                                    994 	.globl _XIE
                                    995 	.globl _XDPTR1
                                    996 	.globl _XDPTR0
                                    997 	.globl _XTALREADY
                                    998 	.globl _XTALOSC
                                    999 	.globl _XTALAMPL
                                   1000 	.globl _SILICONREV
                                   1001 	.globl _SCRATCH3
                                   1002 	.globl _SCRATCH2
                                   1003 	.globl _SCRATCH1
                                   1004 	.globl _SCRATCH0
                                   1005 	.globl _RADIOMUX
                                   1006 	.globl _RADIOFSTATADDR
                                   1007 	.globl _RADIOFSTATADDR1
                                   1008 	.globl _RADIOFSTATADDR0
                                   1009 	.globl _RADIOFDATAADDR
                                   1010 	.globl _RADIOFDATAADDR1
                                   1011 	.globl _RADIOFDATAADDR0
                                   1012 	.globl _OSCRUN
                                   1013 	.globl _OSCREADY
                                   1014 	.globl _OSCFORCERUN
                                   1015 	.globl _OSCCALIB
                                   1016 	.globl _MISCCTRL
                                   1017 	.globl _LPXOSCGM
                                   1018 	.globl _LPOSCREF
                                   1019 	.globl _LPOSCREF1
                                   1020 	.globl _LPOSCREF0
                                   1021 	.globl _LPOSCPER
                                   1022 	.globl _LPOSCPER1
                                   1023 	.globl _LPOSCPER0
                                   1024 	.globl _LPOSCKFILT
                                   1025 	.globl _LPOSCKFILT1
                                   1026 	.globl _LPOSCKFILT0
                                   1027 	.globl _LPOSCFREQ
                                   1028 	.globl _LPOSCFREQ1
                                   1029 	.globl _LPOSCFREQ0
                                   1030 	.globl _LPOSCCONFIG
                                   1031 	.globl _PINSEL
                                   1032 	.globl _PINCHGC
                                   1033 	.globl _PINCHGB
                                   1034 	.globl _PINCHGA
                                   1035 	.globl _PALTRADIO
                                   1036 	.globl _PALTC
                                   1037 	.globl _PALTB
                                   1038 	.globl _PALTA
                                   1039 	.globl _INTCHGC
                                   1040 	.globl _INTCHGB
                                   1041 	.globl _INTCHGA
                                   1042 	.globl _EXTIRQ
                                   1043 	.globl _GPIOENABLE
                                   1044 	.globl _ANALOGA
                                   1045 	.globl _FRCOSCREF
                                   1046 	.globl _FRCOSCREF1
                                   1047 	.globl _FRCOSCREF0
                                   1048 	.globl _FRCOSCPER
                                   1049 	.globl _FRCOSCPER1
                                   1050 	.globl _FRCOSCPER0
                                   1051 	.globl _FRCOSCKFILT
                                   1052 	.globl _FRCOSCKFILT1
                                   1053 	.globl _FRCOSCKFILT0
                                   1054 	.globl _FRCOSCFREQ
                                   1055 	.globl _FRCOSCFREQ1
                                   1056 	.globl _FRCOSCFREQ0
                                   1057 	.globl _FRCOSCCTRL
                                   1058 	.globl _FRCOSCCONFIG
                                   1059 	.globl _DMA1CONFIG
                                   1060 	.globl _DMA1ADDR
                                   1061 	.globl _DMA1ADDR1
                                   1062 	.globl _DMA1ADDR0
                                   1063 	.globl _DMA0CONFIG
                                   1064 	.globl _DMA0ADDR
                                   1065 	.globl _DMA0ADDR1
                                   1066 	.globl _DMA0ADDR0
                                   1067 	.globl _ADCTUNE2
                                   1068 	.globl _ADCTUNE1
                                   1069 	.globl _ADCTUNE0
                                   1070 	.globl _ADCCH3VAL
                                   1071 	.globl _ADCCH3VAL1
                                   1072 	.globl _ADCCH3VAL0
                                   1073 	.globl _ADCCH2VAL
                                   1074 	.globl _ADCCH2VAL1
                                   1075 	.globl _ADCCH2VAL0
                                   1076 	.globl _ADCCH1VAL
                                   1077 	.globl _ADCCH1VAL1
                                   1078 	.globl _ADCCH1VAL0
                                   1079 	.globl _ADCCH0VAL
                                   1080 	.globl _ADCCH0VAL1
                                   1081 	.globl _ADCCH0VAL0
                                   1082 	.globl _aligned_alloc_PARM_2
                                   1083 	.globl _UART_Proc_PortInit
                                   1084 	.globl _UART_Proc_VerifyIncomingMsg
                                   1085 	.globl _UART_Proc_ModifiyKissSpecialCharacters
                                   1086 	.globl _UART_Proc_SendMessage
                                   1087 	.globl _UART_Calc_CRC32
                                   1088 ;--------------------------------------------------------
                                   1089 ; special function registers
                                   1090 ;--------------------------------------------------------
                                   1091 	.area RSEG    (ABS,DATA)
      000000                       1092 	.org 0x0000
                           0000E0  1093 _ACC	=	0x00e0
                           0000F0  1094 _B	=	0x00f0
                           000083  1095 _DPH	=	0x0083
                           000085  1096 _DPH1	=	0x0085
                           000082  1097 _DPL	=	0x0082
                           000084  1098 _DPL1	=	0x0084
                           008382  1099 _DPTR0	=	0x8382
                           008584  1100 _DPTR1	=	0x8584
                           000086  1101 _DPS	=	0x0086
                           0000A0  1102 _E2IE	=	0x00a0
                           0000C0  1103 _E2IP	=	0x00c0
                           000098  1104 _EIE	=	0x0098
                           0000B0  1105 _EIP	=	0x00b0
                           0000A8  1106 _IE	=	0x00a8
                           0000B8  1107 _IP	=	0x00b8
                           000087  1108 _PCON	=	0x0087
                           0000D0  1109 _PSW	=	0x00d0
                           000081  1110 _SP	=	0x0081
                           0000D9  1111 _XPAGE	=	0x00d9
                           0000D9  1112 __XPAGE	=	0x00d9
                           0000CA  1113 _ADCCH0CONFIG	=	0x00ca
                           0000CB  1114 _ADCCH1CONFIG	=	0x00cb
                           0000D2  1115 _ADCCH2CONFIG	=	0x00d2
                           0000D3  1116 _ADCCH3CONFIG	=	0x00d3
                           0000D1  1117 _ADCCLKSRC	=	0x00d1
                           0000C9  1118 _ADCCONV	=	0x00c9
                           0000E1  1119 _ANALOGCOMP	=	0x00e1
                           0000C6  1120 _CLKCON	=	0x00c6
                           0000C7  1121 _CLKSTAT	=	0x00c7
                           000097  1122 _CODECONFIG	=	0x0097
                           0000E3  1123 _DBGLNKBUF	=	0x00e3
                           0000E2  1124 _DBGLNKSTAT	=	0x00e2
                           000089  1125 _DIRA	=	0x0089
                           00008A  1126 _DIRB	=	0x008a
                           00008B  1127 _DIRC	=	0x008b
                           00008E  1128 _DIRR	=	0x008e
                           0000C8  1129 _PINA	=	0x00c8
                           0000E8  1130 _PINB	=	0x00e8
                           0000F8  1131 _PINC	=	0x00f8
                           00008D  1132 _PINR	=	0x008d
                           000080  1133 _PORTA	=	0x0080
                           000088  1134 _PORTB	=	0x0088
                           000090  1135 _PORTC	=	0x0090
                           00008C  1136 _PORTR	=	0x008c
                           0000CE  1137 _IC0CAPT0	=	0x00ce
                           0000CF  1138 _IC0CAPT1	=	0x00cf
                           00CFCE  1139 _IC0CAPT	=	0xcfce
                           0000CC  1140 _IC0MODE	=	0x00cc
                           0000CD  1141 _IC0STATUS	=	0x00cd
                           0000D6  1142 _IC1CAPT0	=	0x00d6
                           0000D7  1143 _IC1CAPT1	=	0x00d7
                           00D7D6  1144 _IC1CAPT	=	0xd7d6
                           0000D4  1145 _IC1MODE	=	0x00d4
                           0000D5  1146 _IC1STATUS	=	0x00d5
                           000092  1147 _NVADDR0	=	0x0092
                           000093  1148 _NVADDR1	=	0x0093
                           009392  1149 _NVADDR	=	0x9392
                           000094  1150 _NVDATA0	=	0x0094
                           000095  1151 _NVDATA1	=	0x0095
                           009594  1152 _NVDATA	=	0x9594
                           000096  1153 _NVKEY	=	0x0096
                           000091  1154 _NVSTATUS	=	0x0091
                           0000BC  1155 _OC0COMP0	=	0x00bc
                           0000BD  1156 _OC0COMP1	=	0x00bd
                           00BDBC  1157 _OC0COMP	=	0xbdbc
                           0000B9  1158 _OC0MODE	=	0x00b9
                           0000BA  1159 _OC0PIN	=	0x00ba
                           0000BB  1160 _OC0STATUS	=	0x00bb
                           0000C4  1161 _OC1COMP0	=	0x00c4
                           0000C5  1162 _OC1COMP1	=	0x00c5
                           00C5C4  1163 _OC1COMP	=	0xc5c4
                           0000C1  1164 _OC1MODE	=	0x00c1
                           0000C2  1165 _OC1PIN	=	0x00c2
                           0000C3  1166 _OC1STATUS	=	0x00c3
                           0000B1  1167 _RADIOACC	=	0x00b1
                           0000B3  1168 _RADIOADDR0	=	0x00b3
                           0000B2  1169 _RADIOADDR1	=	0x00b2
                           00B2B3  1170 _RADIOADDR	=	0xb2b3
                           0000B7  1171 _RADIODATA0	=	0x00b7
                           0000B6  1172 _RADIODATA1	=	0x00b6
                           0000B5  1173 _RADIODATA2	=	0x00b5
                           0000B4  1174 _RADIODATA3	=	0x00b4
                           B4B5B6B7  1175 _RADIODATA	=	0xb4b5b6b7
                           0000BE  1176 _RADIOSTAT0	=	0x00be
                           0000BF  1177 _RADIOSTAT1	=	0x00bf
                           00BFBE  1178 _RADIOSTAT	=	0xbfbe
                           0000DF  1179 _SPCLKSRC	=	0x00df
                           0000DC  1180 _SPMODE	=	0x00dc
                           0000DE  1181 _SPSHREG	=	0x00de
                           0000DD  1182 _SPSTATUS	=	0x00dd
                           00009A  1183 _T0CLKSRC	=	0x009a
                           00009C  1184 _T0CNT0	=	0x009c
                           00009D  1185 _T0CNT1	=	0x009d
                           009D9C  1186 _T0CNT	=	0x9d9c
                           000099  1187 _T0MODE	=	0x0099
                           00009E  1188 _T0PERIOD0	=	0x009e
                           00009F  1189 _T0PERIOD1	=	0x009f
                           009F9E  1190 _T0PERIOD	=	0x9f9e
                           00009B  1191 _T0STATUS	=	0x009b
                           0000A2  1192 _T1CLKSRC	=	0x00a2
                           0000A4  1193 _T1CNT0	=	0x00a4
                           0000A5  1194 _T1CNT1	=	0x00a5
                           00A5A4  1195 _T1CNT	=	0xa5a4
                           0000A1  1196 _T1MODE	=	0x00a1
                           0000A6  1197 _T1PERIOD0	=	0x00a6
                           0000A7  1198 _T1PERIOD1	=	0x00a7
                           00A7A6  1199 _T1PERIOD	=	0xa7a6
                           0000A3  1200 _T1STATUS	=	0x00a3
                           0000AA  1201 _T2CLKSRC	=	0x00aa
                           0000AC  1202 _T2CNT0	=	0x00ac
                           0000AD  1203 _T2CNT1	=	0x00ad
                           00ADAC  1204 _T2CNT	=	0xadac
                           0000A9  1205 _T2MODE	=	0x00a9
                           0000AE  1206 _T2PERIOD0	=	0x00ae
                           0000AF  1207 _T2PERIOD1	=	0x00af
                           00AFAE  1208 _T2PERIOD	=	0xafae
                           0000AB  1209 _T2STATUS	=	0x00ab
                           0000E4  1210 _U0CTRL	=	0x00e4
                           0000E7  1211 _U0MODE	=	0x00e7
                           0000E6  1212 _U0SHREG	=	0x00e6
                           0000E5  1213 _U0STATUS	=	0x00e5
                           0000EC  1214 _U1CTRL	=	0x00ec
                           0000EF  1215 _U1MODE	=	0x00ef
                           0000EE  1216 _U1SHREG	=	0x00ee
                           0000ED  1217 _U1STATUS	=	0x00ed
                           0000DA  1218 _WDTCFG	=	0x00da
                           0000DB  1219 _WDTRESET	=	0x00db
                           0000F1  1220 _WTCFGA	=	0x00f1
                           0000F9  1221 _WTCFGB	=	0x00f9
                           0000F2  1222 _WTCNTA0	=	0x00f2
                           0000F3  1223 _WTCNTA1	=	0x00f3
                           00F3F2  1224 _WTCNTA	=	0xf3f2
                           0000FA  1225 _WTCNTB0	=	0x00fa
                           0000FB  1226 _WTCNTB1	=	0x00fb
                           00FBFA  1227 _WTCNTB	=	0xfbfa
                           0000EB  1228 _WTCNTR1	=	0x00eb
                           0000F4  1229 _WTEVTA0	=	0x00f4
                           0000F5  1230 _WTEVTA1	=	0x00f5
                           00F5F4  1231 _WTEVTA	=	0xf5f4
                           0000F6  1232 _WTEVTB0	=	0x00f6
                           0000F7  1233 _WTEVTB1	=	0x00f7
                           00F7F6  1234 _WTEVTB	=	0xf7f6
                           0000FC  1235 _WTEVTC0	=	0x00fc
                           0000FD  1236 _WTEVTC1	=	0x00fd
                           00FDFC  1237 _WTEVTC	=	0xfdfc
                           0000FE  1238 _WTEVTD0	=	0x00fe
                           0000FF  1239 _WTEVTD1	=	0x00ff
                           00FFFE  1240 _WTEVTD	=	0xfffe
                           0000E9  1241 _WTIRQEN	=	0x00e9
                           0000EA  1242 _WTSTAT	=	0x00ea
                                   1243 ;--------------------------------------------------------
                                   1244 ; special function bits
                                   1245 ;--------------------------------------------------------
                                   1246 	.area RSEG    (ABS,DATA)
      000000                       1247 	.org 0x0000
                           0000E0  1248 _ACC_0	=	0x00e0
                           0000E1  1249 _ACC_1	=	0x00e1
                           0000E2  1250 _ACC_2	=	0x00e2
                           0000E3  1251 _ACC_3	=	0x00e3
                           0000E4  1252 _ACC_4	=	0x00e4
                           0000E5  1253 _ACC_5	=	0x00e5
                           0000E6  1254 _ACC_6	=	0x00e6
                           0000E7  1255 _ACC_7	=	0x00e7
                           0000F0  1256 _B_0	=	0x00f0
                           0000F1  1257 _B_1	=	0x00f1
                           0000F2  1258 _B_2	=	0x00f2
                           0000F3  1259 _B_3	=	0x00f3
                           0000F4  1260 _B_4	=	0x00f4
                           0000F5  1261 _B_5	=	0x00f5
                           0000F6  1262 _B_6	=	0x00f6
                           0000F7  1263 _B_7	=	0x00f7
                           0000A0  1264 _E2IE_0	=	0x00a0
                           0000A1  1265 _E2IE_1	=	0x00a1
                           0000A2  1266 _E2IE_2	=	0x00a2
                           0000A3  1267 _E2IE_3	=	0x00a3
                           0000A4  1268 _E2IE_4	=	0x00a4
                           0000A5  1269 _E2IE_5	=	0x00a5
                           0000A6  1270 _E2IE_6	=	0x00a6
                           0000A7  1271 _E2IE_7	=	0x00a7
                           0000C0  1272 _E2IP_0	=	0x00c0
                           0000C1  1273 _E2IP_1	=	0x00c1
                           0000C2  1274 _E2IP_2	=	0x00c2
                           0000C3  1275 _E2IP_3	=	0x00c3
                           0000C4  1276 _E2IP_4	=	0x00c4
                           0000C5  1277 _E2IP_5	=	0x00c5
                           0000C6  1278 _E2IP_6	=	0x00c6
                           0000C7  1279 _E2IP_7	=	0x00c7
                           000098  1280 _EIE_0	=	0x0098
                           000099  1281 _EIE_1	=	0x0099
                           00009A  1282 _EIE_2	=	0x009a
                           00009B  1283 _EIE_3	=	0x009b
                           00009C  1284 _EIE_4	=	0x009c
                           00009D  1285 _EIE_5	=	0x009d
                           00009E  1286 _EIE_6	=	0x009e
                           00009F  1287 _EIE_7	=	0x009f
                           0000B0  1288 _EIP_0	=	0x00b0
                           0000B1  1289 _EIP_1	=	0x00b1
                           0000B2  1290 _EIP_2	=	0x00b2
                           0000B3  1291 _EIP_3	=	0x00b3
                           0000B4  1292 _EIP_4	=	0x00b4
                           0000B5  1293 _EIP_5	=	0x00b5
                           0000B6  1294 _EIP_6	=	0x00b6
                           0000B7  1295 _EIP_7	=	0x00b7
                           0000A8  1296 _IE_0	=	0x00a8
                           0000A9  1297 _IE_1	=	0x00a9
                           0000AA  1298 _IE_2	=	0x00aa
                           0000AB  1299 _IE_3	=	0x00ab
                           0000AC  1300 _IE_4	=	0x00ac
                           0000AD  1301 _IE_5	=	0x00ad
                           0000AE  1302 _IE_6	=	0x00ae
                           0000AF  1303 _IE_7	=	0x00af
                           0000AF  1304 _EA	=	0x00af
                           0000B8  1305 _IP_0	=	0x00b8
                           0000B9  1306 _IP_1	=	0x00b9
                           0000BA  1307 _IP_2	=	0x00ba
                           0000BB  1308 _IP_3	=	0x00bb
                           0000BC  1309 _IP_4	=	0x00bc
                           0000BD  1310 _IP_5	=	0x00bd
                           0000BE  1311 _IP_6	=	0x00be
                           0000BF  1312 _IP_7	=	0x00bf
                           0000D0  1313 _P	=	0x00d0
                           0000D1  1314 _F1	=	0x00d1
                           0000D2  1315 _OV	=	0x00d2
                           0000D3  1316 _RS0	=	0x00d3
                           0000D4  1317 _RS1	=	0x00d4
                           0000D5  1318 _F0	=	0x00d5
                           0000D6  1319 _AC	=	0x00d6
                           0000D7  1320 _CY	=	0x00d7
                           0000C8  1321 _PINA_0	=	0x00c8
                           0000C9  1322 _PINA_1	=	0x00c9
                           0000CA  1323 _PINA_2	=	0x00ca
                           0000CB  1324 _PINA_3	=	0x00cb
                           0000CC  1325 _PINA_4	=	0x00cc
                           0000CD  1326 _PINA_5	=	0x00cd
                           0000CE  1327 _PINA_6	=	0x00ce
                           0000CF  1328 _PINA_7	=	0x00cf
                           0000E8  1329 _PINB_0	=	0x00e8
                           0000E9  1330 _PINB_1	=	0x00e9
                           0000EA  1331 _PINB_2	=	0x00ea
                           0000EB  1332 _PINB_3	=	0x00eb
                           0000EC  1333 _PINB_4	=	0x00ec
                           0000ED  1334 _PINB_5	=	0x00ed
                           0000EE  1335 _PINB_6	=	0x00ee
                           0000EF  1336 _PINB_7	=	0x00ef
                           0000F8  1337 _PINC_0	=	0x00f8
                           0000F9  1338 _PINC_1	=	0x00f9
                           0000FA  1339 _PINC_2	=	0x00fa
                           0000FB  1340 _PINC_3	=	0x00fb
                           0000FC  1341 _PINC_4	=	0x00fc
                           0000FD  1342 _PINC_5	=	0x00fd
                           0000FE  1343 _PINC_6	=	0x00fe
                           0000FF  1344 _PINC_7	=	0x00ff
                           000080  1345 _PORTA_0	=	0x0080
                           000081  1346 _PORTA_1	=	0x0081
                           000082  1347 _PORTA_2	=	0x0082
                           000083  1348 _PORTA_3	=	0x0083
                           000084  1349 _PORTA_4	=	0x0084
                           000085  1350 _PORTA_5	=	0x0085
                           000086  1351 _PORTA_6	=	0x0086
                           000087  1352 _PORTA_7	=	0x0087
                           000088  1353 _PORTB_0	=	0x0088
                           000089  1354 _PORTB_1	=	0x0089
                           00008A  1355 _PORTB_2	=	0x008a
                           00008B  1356 _PORTB_3	=	0x008b
                           00008C  1357 _PORTB_4	=	0x008c
                           00008D  1358 _PORTB_5	=	0x008d
                           00008E  1359 _PORTB_6	=	0x008e
                           00008F  1360 _PORTB_7	=	0x008f
                           000090  1361 _PORTC_0	=	0x0090
                           000091  1362 _PORTC_1	=	0x0091
                           000092  1363 _PORTC_2	=	0x0092
                           000093  1364 _PORTC_3	=	0x0093
                           000094  1365 _PORTC_4	=	0x0094
                           000095  1366 _PORTC_5	=	0x0095
                           000096  1367 _PORTC_6	=	0x0096
                           000097  1368 _PORTC_7	=	0x0097
                                   1369 ;--------------------------------------------------------
                                   1370 ; overlayable register banks
                                   1371 ;--------------------------------------------------------
                                   1372 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                       1373 	.ds 8
                                   1374 ;--------------------------------------------------------
                                   1375 ; internal ram data
                                   1376 ;--------------------------------------------------------
                                   1377 	.area DSEG    (DATA)
      000026                       1378 _UART_Proc_VerifyIncomingMsg_sloc0_1_0:
      000026                       1379 	.ds 4
      00002A                       1380 _UART_Proc_SendMessage_sloc0_1_0:
      00002A                       1381 	.ds 3
                                   1382 ;--------------------------------------------------------
                                   1383 ; overlayable items in internal ram 
                                   1384 ;--------------------------------------------------------
                                   1385 	.area	OSEG    (OVR,DATA)
      00006A                       1386 _UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0:
      00006A                       1387 	.ds 3
      00006D                       1388 _UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0:
      00006D                       1389 	.ds 3
      000070                       1390 _UART_Proc_ModifiyKissSpecialCharacters_sloc2_1_0:
      000070                       1391 	.ds 1
      000071                       1392 _UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0:
      000071                       1393 	.ds 3
                                   1394 	.area	OSEG    (OVR,DATA)
      00006A                       1395 _UART_Calc_CRC32_sloc0_1_0:
      00006A                       1396 	.ds 3
      00006D                       1397 _UART_Calc_CRC32_sloc1_1_0:
      00006D                       1398 	.ds 4
      000071                       1399 _UART_Calc_CRC32_sloc2_1_0:
      000071                       1400 	.ds 2
                                   1401 ;--------------------------------------------------------
                                   1402 ; indirectly addressable internal ram data
                                   1403 ;--------------------------------------------------------
                                   1404 	.area ISEG    (DATA)
                                   1405 ;--------------------------------------------------------
                                   1406 ; absolute internal ram data
                                   1407 ;--------------------------------------------------------
                                   1408 	.area IABS    (ABS,DATA)
                                   1409 	.area IABS    (ABS,DATA)
                                   1410 ;--------------------------------------------------------
                                   1411 ; bit data
                                   1412 ;--------------------------------------------------------
                                   1413 	.area BSEG    (BIT)
                                   1414 ;--------------------------------------------------------
                                   1415 ; paged external ram data
                                   1416 ;--------------------------------------------------------
                                   1417 	.area PSEG    (PAG,XDATA)
                                   1418 ;--------------------------------------------------------
                                   1419 ; external ram data
                                   1420 ;--------------------------------------------------------
                                   1421 	.area XSEG    (XDATA)
      00038B                       1422 _aligned_alloc_PARM_2:
      00038B                       1423 	.ds 2
                           007020  1424 _ADCCH0VAL0	=	0x7020
                           007021  1425 _ADCCH0VAL1	=	0x7021
                           007020  1426 _ADCCH0VAL	=	0x7020
                           007022  1427 _ADCCH1VAL0	=	0x7022
                           007023  1428 _ADCCH1VAL1	=	0x7023
                           007022  1429 _ADCCH1VAL	=	0x7022
                           007024  1430 _ADCCH2VAL0	=	0x7024
                           007025  1431 _ADCCH2VAL1	=	0x7025
                           007024  1432 _ADCCH2VAL	=	0x7024
                           007026  1433 _ADCCH3VAL0	=	0x7026
                           007027  1434 _ADCCH3VAL1	=	0x7027
                           007026  1435 _ADCCH3VAL	=	0x7026
                           007028  1436 _ADCTUNE0	=	0x7028
                           007029  1437 _ADCTUNE1	=	0x7029
                           00702A  1438 _ADCTUNE2	=	0x702a
                           007010  1439 _DMA0ADDR0	=	0x7010
                           007011  1440 _DMA0ADDR1	=	0x7011
                           007010  1441 _DMA0ADDR	=	0x7010
                           007014  1442 _DMA0CONFIG	=	0x7014
                           007012  1443 _DMA1ADDR0	=	0x7012
                           007013  1444 _DMA1ADDR1	=	0x7013
                           007012  1445 _DMA1ADDR	=	0x7012
                           007015  1446 _DMA1CONFIG	=	0x7015
                           007070  1447 _FRCOSCCONFIG	=	0x7070
                           007071  1448 _FRCOSCCTRL	=	0x7071
                           007076  1449 _FRCOSCFREQ0	=	0x7076
                           007077  1450 _FRCOSCFREQ1	=	0x7077
                           007076  1451 _FRCOSCFREQ	=	0x7076
                           007072  1452 _FRCOSCKFILT0	=	0x7072
                           007073  1453 _FRCOSCKFILT1	=	0x7073
                           007072  1454 _FRCOSCKFILT	=	0x7072
                           007078  1455 _FRCOSCPER0	=	0x7078
                           007079  1456 _FRCOSCPER1	=	0x7079
                           007078  1457 _FRCOSCPER	=	0x7078
                           007074  1458 _FRCOSCREF0	=	0x7074
                           007075  1459 _FRCOSCREF1	=	0x7075
                           007074  1460 _FRCOSCREF	=	0x7074
                           007007  1461 _ANALOGA	=	0x7007
                           00700C  1462 _GPIOENABLE	=	0x700c
                           007003  1463 _EXTIRQ	=	0x7003
                           007000  1464 _INTCHGA	=	0x7000
                           007001  1465 _INTCHGB	=	0x7001
                           007002  1466 _INTCHGC	=	0x7002
                           007008  1467 _PALTA	=	0x7008
                           007009  1468 _PALTB	=	0x7009
                           00700A  1469 _PALTC	=	0x700a
                           007046  1470 _PALTRADIO	=	0x7046
                           007004  1471 _PINCHGA	=	0x7004
                           007005  1472 _PINCHGB	=	0x7005
                           007006  1473 _PINCHGC	=	0x7006
                           00700B  1474 _PINSEL	=	0x700b
                           007060  1475 _LPOSCCONFIG	=	0x7060
                           007066  1476 _LPOSCFREQ0	=	0x7066
                           007067  1477 _LPOSCFREQ1	=	0x7067
                           007066  1478 _LPOSCFREQ	=	0x7066
                           007062  1479 _LPOSCKFILT0	=	0x7062
                           007063  1480 _LPOSCKFILT1	=	0x7063
                           007062  1481 _LPOSCKFILT	=	0x7062
                           007068  1482 _LPOSCPER0	=	0x7068
                           007069  1483 _LPOSCPER1	=	0x7069
                           007068  1484 _LPOSCPER	=	0x7068
                           007064  1485 _LPOSCREF0	=	0x7064
                           007065  1486 _LPOSCREF1	=	0x7065
                           007064  1487 _LPOSCREF	=	0x7064
                           007054  1488 _LPXOSCGM	=	0x7054
                           007F01  1489 _MISCCTRL	=	0x7f01
                           007053  1490 _OSCCALIB	=	0x7053
                           007050  1491 _OSCFORCERUN	=	0x7050
                           007052  1492 _OSCREADY	=	0x7052
                           007051  1493 _OSCRUN	=	0x7051
                           007040  1494 _RADIOFDATAADDR0	=	0x7040
                           007041  1495 _RADIOFDATAADDR1	=	0x7041
                           007040  1496 _RADIOFDATAADDR	=	0x7040
                           007042  1497 _RADIOFSTATADDR0	=	0x7042
                           007043  1498 _RADIOFSTATADDR1	=	0x7043
                           007042  1499 _RADIOFSTATADDR	=	0x7042
                           007044  1500 _RADIOMUX	=	0x7044
                           007084  1501 _SCRATCH0	=	0x7084
                           007085  1502 _SCRATCH1	=	0x7085
                           007086  1503 _SCRATCH2	=	0x7086
                           007087  1504 _SCRATCH3	=	0x7087
                           007F00  1505 _SILICONREV	=	0x7f00
                           007F19  1506 _XTALAMPL	=	0x7f19
                           007F18  1507 _XTALOSC	=	0x7f18
                           007F1A  1508 _XTALREADY	=	0x7f1a
                           003F82  1509 _XDPTR0	=	0x3f82
                           003F84  1510 _XDPTR1	=	0x3f84
                           003FA8  1511 _XIE	=	0x3fa8
                           003FB8  1512 _XIP	=	0x3fb8
                           003F87  1513 _XPCON	=	0x3f87
                           003FCA  1514 _XADCCH0CONFIG	=	0x3fca
                           003FCB  1515 _XADCCH1CONFIG	=	0x3fcb
                           003FD2  1516 _XADCCH2CONFIG	=	0x3fd2
                           003FD3  1517 _XADCCH3CONFIG	=	0x3fd3
                           003FD1  1518 _XADCCLKSRC	=	0x3fd1
                           003FC9  1519 _XADCCONV	=	0x3fc9
                           003FE1  1520 _XANALOGCOMP	=	0x3fe1
                           003FC6  1521 _XCLKCON	=	0x3fc6
                           003FC7  1522 _XCLKSTAT	=	0x3fc7
                           003F97  1523 _XCODECONFIG	=	0x3f97
                           003FE3  1524 _XDBGLNKBUF	=	0x3fe3
                           003FE2  1525 _XDBGLNKSTAT	=	0x3fe2
                           003F89  1526 _XDIRA	=	0x3f89
                           003F8A  1527 _XDIRB	=	0x3f8a
                           003F8B  1528 _XDIRC	=	0x3f8b
                           003F8E  1529 _XDIRR	=	0x3f8e
                           003FC8  1530 _XPINA	=	0x3fc8
                           003FE8  1531 _XPINB	=	0x3fe8
                           003FF8  1532 _XPINC	=	0x3ff8
                           003F8D  1533 _XPINR	=	0x3f8d
                           003F80  1534 _XPORTA	=	0x3f80
                           003F88  1535 _XPORTB	=	0x3f88
                           003F90  1536 _XPORTC	=	0x3f90
                           003F8C  1537 _XPORTR	=	0x3f8c
                           003FCE  1538 _XIC0CAPT0	=	0x3fce
                           003FCF  1539 _XIC0CAPT1	=	0x3fcf
                           003FCE  1540 _XIC0CAPT	=	0x3fce
                           003FCC  1541 _XIC0MODE	=	0x3fcc
                           003FCD  1542 _XIC0STATUS	=	0x3fcd
                           003FD6  1543 _XIC1CAPT0	=	0x3fd6
                           003FD7  1544 _XIC1CAPT1	=	0x3fd7
                           003FD6  1545 _XIC1CAPT	=	0x3fd6
                           003FD4  1546 _XIC1MODE	=	0x3fd4
                           003FD5  1547 _XIC1STATUS	=	0x3fd5
                           003F92  1548 _XNVADDR0	=	0x3f92
                           003F93  1549 _XNVADDR1	=	0x3f93
                           003F92  1550 _XNVADDR	=	0x3f92
                           003F94  1551 _XNVDATA0	=	0x3f94
                           003F95  1552 _XNVDATA1	=	0x3f95
                           003F94  1553 _XNVDATA	=	0x3f94
                           003F96  1554 _XNVKEY	=	0x3f96
                           003F91  1555 _XNVSTATUS	=	0x3f91
                           003FBC  1556 _XOC0COMP0	=	0x3fbc
                           003FBD  1557 _XOC0COMP1	=	0x3fbd
                           003FBC  1558 _XOC0COMP	=	0x3fbc
                           003FB9  1559 _XOC0MODE	=	0x3fb9
                           003FBA  1560 _XOC0PIN	=	0x3fba
                           003FBB  1561 _XOC0STATUS	=	0x3fbb
                           003FC4  1562 _XOC1COMP0	=	0x3fc4
                           003FC5  1563 _XOC1COMP1	=	0x3fc5
                           003FC4  1564 _XOC1COMP	=	0x3fc4
                           003FC1  1565 _XOC1MODE	=	0x3fc1
                           003FC2  1566 _XOC1PIN	=	0x3fc2
                           003FC3  1567 _XOC1STATUS	=	0x3fc3
                           003FB1  1568 _XRADIOACC	=	0x3fb1
                           003FB3  1569 _XRADIOADDR0	=	0x3fb3
                           003FB2  1570 _XRADIOADDR1	=	0x3fb2
                           003FB7  1571 _XRADIODATA0	=	0x3fb7
                           003FB6  1572 _XRADIODATA1	=	0x3fb6
                           003FB5  1573 _XRADIODATA2	=	0x3fb5
                           003FB4  1574 _XRADIODATA3	=	0x3fb4
                           003FBE  1575 _XRADIOSTAT0	=	0x3fbe
                           003FBF  1576 _XRADIOSTAT1	=	0x3fbf
                           003FBE  1577 _XRADIOSTAT	=	0x3fbe
                           003FDF  1578 _XSPCLKSRC	=	0x3fdf
                           003FDC  1579 _XSPMODE	=	0x3fdc
                           003FDE  1580 _XSPSHREG	=	0x3fde
                           003FDD  1581 _XSPSTATUS	=	0x3fdd
                           003F9A  1582 _XT0CLKSRC	=	0x3f9a
                           003F9C  1583 _XT0CNT0	=	0x3f9c
                           003F9D  1584 _XT0CNT1	=	0x3f9d
                           003F9C  1585 _XT0CNT	=	0x3f9c
                           003F99  1586 _XT0MODE	=	0x3f99
                           003F9E  1587 _XT0PERIOD0	=	0x3f9e
                           003F9F  1588 _XT0PERIOD1	=	0x3f9f
                           003F9E  1589 _XT0PERIOD	=	0x3f9e
                           003F9B  1590 _XT0STATUS	=	0x3f9b
                           003FA2  1591 _XT1CLKSRC	=	0x3fa2
                           003FA4  1592 _XT1CNT0	=	0x3fa4
                           003FA5  1593 _XT1CNT1	=	0x3fa5
                           003FA4  1594 _XT1CNT	=	0x3fa4
                           003FA1  1595 _XT1MODE	=	0x3fa1
                           003FA6  1596 _XT1PERIOD0	=	0x3fa6
                           003FA7  1597 _XT1PERIOD1	=	0x3fa7
                           003FA6  1598 _XT1PERIOD	=	0x3fa6
                           003FA3  1599 _XT1STATUS	=	0x3fa3
                           003FAA  1600 _XT2CLKSRC	=	0x3faa
                           003FAC  1601 _XT2CNT0	=	0x3fac
                           003FAD  1602 _XT2CNT1	=	0x3fad
                           003FAC  1603 _XT2CNT	=	0x3fac
                           003FA9  1604 _XT2MODE	=	0x3fa9
                           003FAE  1605 _XT2PERIOD0	=	0x3fae
                           003FAF  1606 _XT2PERIOD1	=	0x3faf
                           003FAE  1607 _XT2PERIOD	=	0x3fae
                           003FAB  1608 _XT2STATUS	=	0x3fab
                           003FE4  1609 _XU0CTRL	=	0x3fe4
                           003FE7  1610 _XU0MODE	=	0x3fe7
                           003FE6  1611 _XU0SHREG	=	0x3fe6
                           003FE5  1612 _XU0STATUS	=	0x3fe5
                           003FEC  1613 _XU1CTRL	=	0x3fec
                           003FEF  1614 _XU1MODE	=	0x3fef
                           003FEE  1615 _XU1SHREG	=	0x3fee
                           003FED  1616 _XU1STATUS	=	0x3fed
                           003FDA  1617 _XWDTCFG	=	0x3fda
                           003FDB  1618 _XWDTRESET	=	0x3fdb
                           003FF1  1619 _XWTCFGA	=	0x3ff1
                           003FF9  1620 _XWTCFGB	=	0x3ff9
                           003FF2  1621 _XWTCNTA0	=	0x3ff2
                           003FF3  1622 _XWTCNTA1	=	0x3ff3
                           003FF2  1623 _XWTCNTA	=	0x3ff2
                           003FFA  1624 _XWTCNTB0	=	0x3ffa
                           003FFB  1625 _XWTCNTB1	=	0x3ffb
                           003FFA  1626 _XWTCNTB	=	0x3ffa
                           003FEB  1627 _XWTCNTR1	=	0x3feb
                           003FF4  1628 _XWTEVTA0	=	0x3ff4
                           003FF5  1629 _XWTEVTA1	=	0x3ff5
                           003FF4  1630 _XWTEVTA	=	0x3ff4
                           003FF6  1631 _XWTEVTB0	=	0x3ff6
                           003FF7  1632 _XWTEVTB1	=	0x3ff7
                           003FF6  1633 _XWTEVTB	=	0x3ff6
                           003FFC  1634 _XWTEVTC0	=	0x3ffc
                           003FFD  1635 _XWTEVTC1	=	0x3ffd
                           003FFC  1636 _XWTEVTC	=	0x3ffc
                           003FFE  1637 _XWTEVTD0	=	0x3ffe
                           003FFF  1638 _XWTEVTD1	=	0x3fff
                           003FFE  1639 _XWTEVTD	=	0x3ffe
                           003FE9  1640 _XWTIRQEN	=	0x3fe9
                           003FEA  1641 _XWTSTAT	=	0x3fea
                           004114  1642 _AX5043_AFSKCTRL	=	0x4114
                           004113  1643 _AX5043_AFSKMARK0	=	0x4113
                           004112  1644 _AX5043_AFSKMARK1	=	0x4112
                           004111  1645 _AX5043_AFSKSPACE0	=	0x4111
                           004110  1646 _AX5043_AFSKSPACE1	=	0x4110
                           004043  1647 _AX5043_AGCCOUNTER	=	0x4043
                           004115  1648 _AX5043_AMPLFILTER	=	0x4115
                           004189  1649 _AX5043_BBOFFSCAP	=	0x4189
                           004188  1650 _AX5043_BBTUNE	=	0x4188
                           004041  1651 _AX5043_BGNDRSSI	=	0x4041
                           00422E  1652 _AX5043_BGNDRSSIGAIN	=	0x422e
                           00422F  1653 _AX5043_BGNDRSSITHR	=	0x422f
                           004017  1654 _AX5043_CRCINIT0	=	0x4017
                           004016  1655 _AX5043_CRCINIT1	=	0x4016
                           004015  1656 _AX5043_CRCINIT2	=	0x4015
                           004014  1657 _AX5043_CRCINIT3	=	0x4014
                           004332  1658 _AX5043_DACCONFIG	=	0x4332
                           004331  1659 _AX5043_DACVALUE0	=	0x4331
                           004330  1660 _AX5043_DACVALUE1	=	0x4330
                           004102  1661 _AX5043_DECIMATION	=	0x4102
                           004042  1662 _AX5043_DIVERSITY	=	0x4042
                           004011  1663 _AX5043_ENCODING	=	0x4011
                           004018  1664 _AX5043_FEC	=	0x4018
                           00401A  1665 _AX5043_FECSTATUS	=	0x401a
                           004019  1666 _AX5043_FECSYNC	=	0x4019
                           00402B  1667 _AX5043_FIFOCOUNT0	=	0x402b
                           00402A  1668 _AX5043_FIFOCOUNT1	=	0x402a
                           004029  1669 _AX5043_FIFODATA	=	0x4029
                           00402D  1670 _AX5043_FIFOFREE0	=	0x402d
                           00402C  1671 _AX5043_FIFOFREE1	=	0x402c
                           004028  1672 _AX5043_FIFOSTAT	=	0x4028
                           00402F  1673 _AX5043_FIFOTHRESH0	=	0x402f
                           00402E  1674 _AX5043_FIFOTHRESH1	=	0x402e
                           004012  1675 _AX5043_FRAMING	=	0x4012
                           004037  1676 _AX5043_FREQA0	=	0x4037
                           004036  1677 _AX5043_FREQA1	=	0x4036
                           004035  1678 _AX5043_FREQA2	=	0x4035
                           004034  1679 _AX5043_FREQA3	=	0x4034
                           00403F  1680 _AX5043_FREQB0	=	0x403f
                           00403E  1681 _AX5043_FREQB1	=	0x403e
                           00403D  1682 _AX5043_FREQB2	=	0x403d
                           00403C  1683 _AX5043_FREQB3	=	0x403c
                           004163  1684 _AX5043_FSKDEV0	=	0x4163
                           004162  1685 _AX5043_FSKDEV1	=	0x4162
                           004161  1686 _AX5043_FSKDEV2	=	0x4161
                           00410D  1687 _AX5043_FSKDMAX0	=	0x410d
                           00410C  1688 _AX5043_FSKDMAX1	=	0x410c
                           00410F  1689 _AX5043_FSKDMIN0	=	0x410f
                           00410E  1690 _AX5043_FSKDMIN1	=	0x410e
                           004309  1691 _AX5043_GPADC13VALUE0	=	0x4309
                           004308  1692 _AX5043_GPADC13VALUE1	=	0x4308
                           004300  1693 _AX5043_GPADCCTRL	=	0x4300
                           004301  1694 _AX5043_GPADCPERIOD	=	0x4301
                           004101  1695 _AX5043_IFFREQ0	=	0x4101
                           004100  1696 _AX5043_IFFREQ1	=	0x4100
                           00400B  1697 _AX5043_IRQINVERSION0	=	0x400b
                           00400A  1698 _AX5043_IRQINVERSION1	=	0x400a
                           004007  1699 _AX5043_IRQMASK0	=	0x4007
                           004006  1700 _AX5043_IRQMASK1	=	0x4006
                           00400D  1701 _AX5043_IRQREQUEST0	=	0x400d
                           00400C  1702 _AX5043_IRQREQUEST1	=	0x400c
                           004310  1703 _AX5043_LPOSCCONFIG	=	0x4310
                           004317  1704 _AX5043_LPOSCFREQ0	=	0x4317
                           004316  1705 _AX5043_LPOSCFREQ1	=	0x4316
                           004313  1706 _AX5043_LPOSCKFILT0	=	0x4313
                           004312  1707 _AX5043_LPOSCKFILT1	=	0x4312
                           004319  1708 _AX5043_LPOSCPER0	=	0x4319
                           004318  1709 _AX5043_LPOSCPER1	=	0x4318
                           004315  1710 _AX5043_LPOSCREF0	=	0x4315
                           004314  1711 _AX5043_LPOSCREF1	=	0x4314
                           004311  1712 _AX5043_LPOSCSTATUS	=	0x4311
                           004214  1713 _AX5043_MATCH0LEN	=	0x4214
                           004216  1714 _AX5043_MATCH0MAX	=	0x4216
                           004215  1715 _AX5043_MATCH0MIN	=	0x4215
                           004213  1716 _AX5043_MATCH0PAT0	=	0x4213
                           004212  1717 _AX5043_MATCH0PAT1	=	0x4212
                           004211  1718 _AX5043_MATCH0PAT2	=	0x4211
                           004210  1719 _AX5043_MATCH0PAT3	=	0x4210
                           00421C  1720 _AX5043_MATCH1LEN	=	0x421c
                           00421E  1721 _AX5043_MATCH1MAX	=	0x421e
                           00421D  1722 _AX5043_MATCH1MIN	=	0x421d
                           004219  1723 _AX5043_MATCH1PAT0	=	0x4219
                           004218  1724 _AX5043_MATCH1PAT1	=	0x4218
                           004108  1725 _AX5043_MAXDROFFSET0	=	0x4108
                           004107  1726 _AX5043_MAXDROFFSET1	=	0x4107
                           004106  1727 _AX5043_MAXDROFFSET2	=	0x4106
                           00410B  1728 _AX5043_MAXRFOFFSET0	=	0x410b
                           00410A  1729 _AX5043_MAXRFOFFSET1	=	0x410a
                           004109  1730 _AX5043_MAXRFOFFSET2	=	0x4109
                           004164  1731 _AX5043_MODCFGA	=	0x4164
                           004160  1732 _AX5043_MODCFGF	=	0x4160
                           004F5F  1733 _AX5043_MODCFGP	=	0x4f5f
                           004010  1734 _AX5043_MODULATION	=	0x4010
                           004025  1735 _AX5043_PINFUNCANTSEL	=	0x4025
                           004023  1736 _AX5043_PINFUNCDATA	=	0x4023
                           004022  1737 _AX5043_PINFUNCDCLK	=	0x4022
                           004024  1738 _AX5043_PINFUNCIRQ	=	0x4024
                           004026  1739 _AX5043_PINFUNCPWRAMP	=	0x4026
                           004021  1740 _AX5043_PINFUNCSYSCLK	=	0x4021
                           004020  1741 _AX5043_PINSTATE	=	0x4020
                           004233  1742 _AX5043_PKTACCEPTFLAGS	=	0x4233
                           004230  1743 _AX5043_PKTCHUNKSIZE	=	0x4230
                           004231  1744 _AX5043_PKTMISCFLAGS	=	0x4231
                           004232  1745 _AX5043_PKTSTOREFLAGS	=	0x4232
                           004031  1746 _AX5043_PLLCPI	=	0x4031
                           004039  1747 _AX5043_PLLCPIBOOST	=	0x4039
                           004182  1748 _AX5043_PLLLOCKDET	=	0x4182
                           004030  1749 _AX5043_PLLLOOP	=	0x4030
                           004038  1750 _AX5043_PLLLOOPBOOST	=	0x4038
                           004033  1751 _AX5043_PLLRANGINGA	=	0x4033
                           00403B  1752 _AX5043_PLLRANGINGB	=	0x403b
                           004183  1753 _AX5043_PLLRNGCLK	=	0x4183
                           004032  1754 _AX5043_PLLVCODIV	=	0x4032
                           004180  1755 _AX5043_PLLVCOI	=	0x4180
                           004181  1756 _AX5043_PLLVCOIR	=	0x4181
                           004F08  1757 _AX5043_POWCTRL1	=	0x4f08
                           004005  1758 _AX5043_POWIRQMASK	=	0x4005
                           004003  1759 _AX5043_POWSTAT	=	0x4003
                           004004  1760 _AX5043_POWSTICKYSTAT	=	0x4004
                           004027  1761 _AX5043_PWRAMP	=	0x4027
                           004002  1762 _AX5043_PWRMODE	=	0x4002
                           004009  1763 _AX5043_RADIOEVENTMASK0	=	0x4009
                           004008  1764 _AX5043_RADIOEVENTMASK1	=	0x4008
                           00400F  1765 _AX5043_RADIOEVENTREQ0	=	0x400f
                           00400E  1766 _AX5043_RADIOEVENTREQ1	=	0x400e
                           00401C  1767 _AX5043_RADIOSTATE	=	0x401c
                           004F0D  1768 _AX5043_REF	=	0x4f0d
                           004040  1769 _AX5043_RSSI	=	0x4040
                           00422D  1770 _AX5043_RSSIABSTHR	=	0x422d
                           00422C  1771 _AX5043_RSSIREFERENCE	=	0x422c
                           004105  1772 _AX5043_RXDATARATE0	=	0x4105
                           004104  1773 _AX5043_RXDATARATE1	=	0x4104
                           004103  1774 _AX5043_RXDATARATE2	=	0x4103
                           004001  1775 _AX5043_SCRATCH	=	0x4001
                           004000  1776 _AX5043_SILICONREVISION	=	0x4000
                           00405B  1777 _AX5043_TIMER0	=	0x405b
                           00405A  1778 _AX5043_TIMER1	=	0x405a
                           004059  1779 _AX5043_TIMER2	=	0x4059
                           004227  1780 _AX5043_TMGRXAGC	=	0x4227
                           004223  1781 _AX5043_TMGRXBOOST	=	0x4223
                           004226  1782 _AX5043_TMGRXCOARSEAGC	=	0x4226
                           004225  1783 _AX5043_TMGRXOFFSACQ	=	0x4225
                           004229  1784 _AX5043_TMGRXPREAMBLE1	=	0x4229
                           00422A  1785 _AX5043_TMGRXPREAMBLE2	=	0x422a
                           00422B  1786 _AX5043_TMGRXPREAMBLE3	=	0x422b
                           004228  1787 _AX5043_TMGRXRSSI	=	0x4228
                           004224  1788 _AX5043_TMGRXSETTLE	=	0x4224
                           004220  1789 _AX5043_TMGTXBOOST	=	0x4220
                           004221  1790 _AX5043_TMGTXSETTLE	=	0x4221
                           004055  1791 _AX5043_TRKAFSKDEMOD0	=	0x4055
                           004054  1792 _AX5043_TRKAFSKDEMOD1	=	0x4054
                           004049  1793 _AX5043_TRKAMPLITUDE0	=	0x4049
                           004048  1794 _AX5043_TRKAMPLITUDE1	=	0x4048
                           004047  1795 _AX5043_TRKDATARATE0	=	0x4047
                           004046  1796 _AX5043_TRKDATARATE1	=	0x4046
                           004045  1797 _AX5043_TRKDATARATE2	=	0x4045
                           004051  1798 _AX5043_TRKFREQ0	=	0x4051
                           004050  1799 _AX5043_TRKFREQ1	=	0x4050
                           004053  1800 _AX5043_TRKFSKDEMOD0	=	0x4053
                           004052  1801 _AX5043_TRKFSKDEMOD1	=	0x4052
                           00404B  1802 _AX5043_TRKPHASE0	=	0x404b
                           00404A  1803 _AX5043_TRKPHASE1	=	0x404a
                           00404F  1804 _AX5043_TRKRFFREQ0	=	0x404f
                           00404E  1805 _AX5043_TRKRFFREQ1	=	0x404e
                           00404D  1806 _AX5043_TRKRFFREQ2	=	0x404d
                           004169  1807 _AX5043_TXPWRCOEFFA0	=	0x4169
                           004168  1808 _AX5043_TXPWRCOEFFA1	=	0x4168
                           00416B  1809 _AX5043_TXPWRCOEFFB0	=	0x416b
                           00416A  1810 _AX5043_TXPWRCOEFFB1	=	0x416a
                           00416D  1811 _AX5043_TXPWRCOEFFC0	=	0x416d
                           00416C  1812 _AX5043_TXPWRCOEFFC1	=	0x416c
                           00416F  1813 _AX5043_TXPWRCOEFFD0	=	0x416f
                           00416E  1814 _AX5043_TXPWRCOEFFD1	=	0x416e
                           004171  1815 _AX5043_TXPWRCOEFFE0	=	0x4171
                           004170  1816 _AX5043_TXPWRCOEFFE1	=	0x4170
                           004167  1817 _AX5043_TXRATE0	=	0x4167
                           004166  1818 _AX5043_TXRATE1	=	0x4166
                           004165  1819 _AX5043_TXRATE2	=	0x4165
                           00406B  1820 _AX5043_WAKEUP0	=	0x406b
                           00406A  1821 _AX5043_WAKEUP1	=	0x406a
                           00406D  1822 _AX5043_WAKEUPFREQ0	=	0x406d
                           00406C  1823 _AX5043_WAKEUPFREQ1	=	0x406c
                           004069  1824 _AX5043_WAKEUPTIMER0	=	0x4069
                           004068  1825 _AX5043_WAKEUPTIMER1	=	0x4068
                           00406E  1826 _AX5043_WAKEUPXOEARLY	=	0x406e
                           004F11  1827 _AX5043_XTALAMPL	=	0x4f11
                           004184  1828 _AX5043_XTALCAP	=	0x4184
                           004F10  1829 _AX5043_XTALOSC	=	0x4f10
                           00401D  1830 _AX5043_XTALSTATUS	=	0x401d
                           004F00  1831 _AX5043_0xF00	=	0x4f00
                           004F0C  1832 _AX5043_0xF0C	=	0x4f0c
                           004F18  1833 _AX5043_0xF18	=	0x4f18
                           004F1C  1834 _AX5043_0xF1C	=	0x4f1c
                           004F21  1835 _AX5043_0xF21	=	0x4f21
                           004F22  1836 _AX5043_0xF22	=	0x4f22
                           004F23  1837 _AX5043_0xF23	=	0x4f23
                           004F26  1838 _AX5043_0xF26	=	0x4f26
                           004F30  1839 _AX5043_0xF30	=	0x4f30
                           004F31  1840 _AX5043_0xF31	=	0x4f31
                           004F32  1841 _AX5043_0xF32	=	0x4f32
                           004F33  1842 _AX5043_0xF33	=	0x4f33
                           004F34  1843 _AX5043_0xF34	=	0x4f34
                           004F35  1844 _AX5043_0xF35	=	0x4f35
                           004F44  1845 _AX5043_0xF44	=	0x4f44
                           004122  1846 _AX5043_AGCAHYST0	=	0x4122
                           004132  1847 _AX5043_AGCAHYST1	=	0x4132
                           004142  1848 _AX5043_AGCAHYST2	=	0x4142
                           004152  1849 _AX5043_AGCAHYST3	=	0x4152
                           004120  1850 _AX5043_AGCGAIN0	=	0x4120
                           004130  1851 _AX5043_AGCGAIN1	=	0x4130
                           004140  1852 _AX5043_AGCGAIN2	=	0x4140
                           004150  1853 _AX5043_AGCGAIN3	=	0x4150
                           004123  1854 _AX5043_AGCMINMAX0	=	0x4123
                           004133  1855 _AX5043_AGCMINMAX1	=	0x4133
                           004143  1856 _AX5043_AGCMINMAX2	=	0x4143
                           004153  1857 _AX5043_AGCMINMAX3	=	0x4153
                           004121  1858 _AX5043_AGCTARGET0	=	0x4121
                           004131  1859 _AX5043_AGCTARGET1	=	0x4131
                           004141  1860 _AX5043_AGCTARGET2	=	0x4141
                           004151  1861 _AX5043_AGCTARGET3	=	0x4151
                           00412B  1862 _AX5043_AMPLITUDEGAIN0	=	0x412b
                           00413B  1863 _AX5043_AMPLITUDEGAIN1	=	0x413b
                           00414B  1864 _AX5043_AMPLITUDEGAIN2	=	0x414b
                           00415B  1865 _AX5043_AMPLITUDEGAIN3	=	0x415b
                           00412F  1866 _AX5043_BBOFFSRES0	=	0x412f
                           00413F  1867 _AX5043_BBOFFSRES1	=	0x413f
                           00414F  1868 _AX5043_BBOFFSRES2	=	0x414f
                           00415F  1869 _AX5043_BBOFFSRES3	=	0x415f
                           004125  1870 _AX5043_DRGAIN0	=	0x4125
                           004135  1871 _AX5043_DRGAIN1	=	0x4135
                           004145  1872 _AX5043_DRGAIN2	=	0x4145
                           004155  1873 _AX5043_DRGAIN3	=	0x4155
                           00412E  1874 _AX5043_FOURFSK0	=	0x412e
                           00413E  1875 _AX5043_FOURFSK1	=	0x413e
                           00414E  1876 _AX5043_FOURFSK2	=	0x414e
                           00415E  1877 _AX5043_FOURFSK3	=	0x415e
                           00412D  1878 _AX5043_FREQDEV00	=	0x412d
                           00413D  1879 _AX5043_FREQDEV01	=	0x413d
                           00414D  1880 _AX5043_FREQDEV02	=	0x414d
                           00415D  1881 _AX5043_FREQDEV03	=	0x415d
                           00412C  1882 _AX5043_FREQDEV10	=	0x412c
                           00413C  1883 _AX5043_FREQDEV11	=	0x413c
                           00414C  1884 _AX5043_FREQDEV12	=	0x414c
                           00415C  1885 _AX5043_FREQDEV13	=	0x415c
                           004127  1886 _AX5043_FREQUENCYGAINA0	=	0x4127
                           004137  1887 _AX5043_FREQUENCYGAINA1	=	0x4137
                           004147  1888 _AX5043_FREQUENCYGAINA2	=	0x4147
                           004157  1889 _AX5043_FREQUENCYGAINA3	=	0x4157
                           004128  1890 _AX5043_FREQUENCYGAINB0	=	0x4128
                           004138  1891 _AX5043_FREQUENCYGAINB1	=	0x4138
                           004148  1892 _AX5043_FREQUENCYGAINB2	=	0x4148
                           004158  1893 _AX5043_FREQUENCYGAINB3	=	0x4158
                           004129  1894 _AX5043_FREQUENCYGAINC0	=	0x4129
                           004139  1895 _AX5043_FREQUENCYGAINC1	=	0x4139
                           004149  1896 _AX5043_FREQUENCYGAINC2	=	0x4149
                           004159  1897 _AX5043_FREQUENCYGAINC3	=	0x4159
                           00412A  1898 _AX5043_FREQUENCYGAIND0	=	0x412a
                           00413A  1899 _AX5043_FREQUENCYGAIND1	=	0x413a
                           00414A  1900 _AX5043_FREQUENCYGAIND2	=	0x414a
                           00415A  1901 _AX5043_FREQUENCYGAIND3	=	0x415a
                           004116  1902 _AX5043_FREQUENCYLEAK	=	0x4116
                           004126  1903 _AX5043_PHASEGAIN0	=	0x4126
                           004136  1904 _AX5043_PHASEGAIN1	=	0x4136
                           004146  1905 _AX5043_PHASEGAIN2	=	0x4146
                           004156  1906 _AX5043_PHASEGAIN3	=	0x4156
                           004207  1907 _AX5043_PKTADDR0	=	0x4207
                           004206  1908 _AX5043_PKTADDR1	=	0x4206
                           004205  1909 _AX5043_PKTADDR2	=	0x4205
                           004204  1910 _AX5043_PKTADDR3	=	0x4204
                           004200  1911 _AX5043_PKTADDRCFG	=	0x4200
                           00420B  1912 _AX5043_PKTADDRMASK0	=	0x420b
                           00420A  1913 _AX5043_PKTADDRMASK1	=	0x420a
                           004209  1914 _AX5043_PKTADDRMASK2	=	0x4209
                           004208  1915 _AX5043_PKTADDRMASK3	=	0x4208
                           004201  1916 _AX5043_PKTLENCFG	=	0x4201
                           004202  1917 _AX5043_PKTLENOFFSET	=	0x4202
                           004203  1918 _AX5043_PKTMAXLEN	=	0x4203
                           004118  1919 _AX5043_RXPARAMCURSET	=	0x4118
                           004117  1920 _AX5043_RXPARAMSETS	=	0x4117
                           004124  1921 _AX5043_TIMEGAIN0	=	0x4124
                           004134  1922 _AX5043_TIMEGAIN1	=	0x4134
                           004144  1923 _AX5043_TIMEGAIN2	=	0x4144
                           004154  1924 _AX5043_TIMEGAIN3	=	0x4154
                           005114  1925 _AX5043_AFSKCTRLNB	=	0x5114
                           005113  1926 _AX5043_AFSKMARK0NB	=	0x5113
                           005112  1927 _AX5043_AFSKMARK1NB	=	0x5112
                           005111  1928 _AX5043_AFSKSPACE0NB	=	0x5111
                           005110  1929 _AX5043_AFSKSPACE1NB	=	0x5110
                           005043  1930 _AX5043_AGCCOUNTERNB	=	0x5043
                           005115  1931 _AX5043_AMPLFILTERNB	=	0x5115
                           005189  1932 _AX5043_BBOFFSCAPNB	=	0x5189
                           005188  1933 _AX5043_BBTUNENB	=	0x5188
                           005041  1934 _AX5043_BGNDRSSINB	=	0x5041
                           00522E  1935 _AX5043_BGNDRSSIGAINNB	=	0x522e
                           00522F  1936 _AX5043_BGNDRSSITHRNB	=	0x522f
                           005017  1937 _AX5043_CRCINIT0NB	=	0x5017
                           005016  1938 _AX5043_CRCINIT1NB	=	0x5016
                           005015  1939 _AX5043_CRCINIT2NB	=	0x5015
                           005014  1940 _AX5043_CRCINIT3NB	=	0x5014
                           005332  1941 _AX5043_DACCONFIGNB	=	0x5332
                           005331  1942 _AX5043_DACVALUE0NB	=	0x5331
                           005330  1943 _AX5043_DACVALUE1NB	=	0x5330
                           005102  1944 _AX5043_DECIMATIONNB	=	0x5102
                           005042  1945 _AX5043_DIVERSITYNB	=	0x5042
                           005011  1946 _AX5043_ENCODINGNB	=	0x5011
                           005018  1947 _AX5043_FECNB	=	0x5018
                           00501A  1948 _AX5043_FECSTATUSNB	=	0x501a
                           005019  1949 _AX5043_FECSYNCNB	=	0x5019
                           00502B  1950 _AX5043_FIFOCOUNT0NB	=	0x502b
                           00502A  1951 _AX5043_FIFOCOUNT1NB	=	0x502a
                           005029  1952 _AX5043_FIFODATANB	=	0x5029
                           00502D  1953 _AX5043_FIFOFREE0NB	=	0x502d
                           00502C  1954 _AX5043_FIFOFREE1NB	=	0x502c
                           005028  1955 _AX5043_FIFOSTATNB	=	0x5028
                           00502F  1956 _AX5043_FIFOTHRESH0NB	=	0x502f
                           00502E  1957 _AX5043_FIFOTHRESH1NB	=	0x502e
                           005012  1958 _AX5043_FRAMINGNB	=	0x5012
                           005037  1959 _AX5043_FREQA0NB	=	0x5037
                           005036  1960 _AX5043_FREQA1NB	=	0x5036
                           005035  1961 _AX5043_FREQA2NB	=	0x5035
                           005034  1962 _AX5043_FREQA3NB	=	0x5034
                           00503F  1963 _AX5043_FREQB0NB	=	0x503f
                           00503E  1964 _AX5043_FREQB1NB	=	0x503e
                           00503D  1965 _AX5043_FREQB2NB	=	0x503d
                           00503C  1966 _AX5043_FREQB3NB	=	0x503c
                           005163  1967 _AX5043_FSKDEV0NB	=	0x5163
                           005162  1968 _AX5043_FSKDEV1NB	=	0x5162
                           005161  1969 _AX5043_FSKDEV2NB	=	0x5161
                           00510D  1970 _AX5043_FSKDMAX0NB	=	0x510d
                           00510C  1971 _AX5043_FSKDMAX1NB	=	0x510c
                           00510F  1972 _AX5043_FSKDMIN0NB	=	0x510f
                           00510E  1973 _AX5043_FSKDMIN1NB	=	0x510e
                           005309  1974 _AX5043_GPADC13VALUE0NB	=	0x5309
                           005308  1975 _AX5043_GPADC13VALUE1NB	=	0x5308
                           005300  1976 _AX5043_GPADCCTRLNB	=	0x5300
                           005301  1977 _AX5043_GPADCPERIODNB	=	0x5301
                           005101  1978 _AX5043_IFFREQ0NB	=	0x5101
                           005100  1979 _AX5043_IFFREQ1NB	=	0x5100
                           00500B  1980 _AX5043_IRQINVERSION0NB	=	0x500b
                           00500A  1981 _AX5043_IRQINVERSION1NB	=	0x500a
                           005007  1982 _AX5043_IRQMASK0NB	=	0x5007
                           005006  1983 _AX5043_IRQMASK1NB	=	0x5006
                           00500D  1984 _AX5043_IRQREQUEST0NB	=	0x500d
                           00500C  1985 _AX5043_IRQREQUEST1NB	=	0x500c
                           005310  1986 _AX5043_LPOSCCONFIGNB	=	0x5310
                           005317  1987 _AX5043_LPOSCFREQ0NB	=	0x5317
                           005316  1988 _AX5043_LPOSCFREQ1NB	=	0x5316
                           005313  1989 _AX5043_LPOSCKFILT0NB	=	0x5313
                           005312  1990 _AX5043_LPOSCKFILT1NB	=	0x5312
                           005319  1991 _AX5043_LPOSCPER0NB	=	0x5319
                           005318  1992 _AX5043_LPOSCPER1NB	=	0x5318
                           005315  1993 _AX5043_LPOSCREF0NB	=	0x5315
                           005314  1994 _AX5043_LPOSCREF1NB	=	0x5314
                           005311  1995 _AX5043_LPOSCSTATUSNB	=	0x5311
                           005214  1996 _AX5043_MATCH0LENNB	=	0x5214
                           005216  1997 _AX5043_MATCH0MAXNB	=	0x5216
                           005215  1998 _AX5043_MATCH0MINNB	=	0x5215
                           005213  1999 _AX5043_MATCH0PAT0NB	=	0x5213
                           005212  2000 _AX5043_MATCH0PAT1NB	=	0x5212
                           005211  2001 _AX5043_MATCH0PAT2NB	=	0x5211
                           005210  2002 _AX5043_MATCH0PAT3NB	=	0x5210
                           00521C  2003 _AX5043_MATCH1LENNB	=	0x521c
                           00521E  2004 _AX5043_MATCH1MAXNB	=	0x521e
                           00521D  2005 _AX5043_MATCH1MINNB	=	0x521d
                           005219  2006 _AX5043_MATCH1PAT0NB	=	0x5219
                           005218  2007 _AX5043_MATCH1PAT1NB	=	0x5218
                           005108  2008 _AX5043_MAXDROFFSET0NB	=	0x5108
                           005107  2009 _AX5043_MAXDROFFSET1NB	=	0x5107
                           005106  2010 _AX5043_MAXDROFFSET2NB	=	0x5106
                           00510B  2011 _AX5043_MAXRFOFFSET0NB	=	0x510b
                           00510A  2012 _AX5043_MAXRFOFFSET1NB	=	0x510a
                           005109  2013 _AX5043_MAXRFOFFSET2NB	=	0x5109
                           005164  2014 _AX5043_MODCFGANB	=	0x5164
                           005160  2015 _AX5043_MODCFGFNB	=	0x5160
                           005F5F  2016 _AX5043_MODCFGPNB	=	0x5f5f
                           005010  2017 _AX5043_MODULATIONNB	=	0x5010
                           005025  2018 _AX5043_PINFUNCANTSELNB	=	0x5025
                           005023  2019 _AX5043_PINFUNCDATANB	=	0x5023
                           005022  2020 _AX5043_PINFUNCDCLKNB	=	0x5022
                           005024  2021 _AX5043_PINFUNCIRQNB	=	0x5024
                           005026  2022 _AX5043_PINFUNCPWRAMPNB	=	0x5026
                           005021  2023 _AX5043_PINFUNCSYSCLKNB	=	0x5021
                           005020  2024 _AX5043_PINSTATENB	=	0x5020
                           005233  2025 _AX5043_PKTACCEPTFLAGSNB	=	0x5233
                           005230  2026 _AX5043_PKTCHUNKSIZENB	=	0x5230
                           005231  2027 _AX5043_PKTMISCFLAGSNB	=	0x5231
                           005232  2028 _AX5043_PKTSTOREFLAGSNB	=	0x5232
                           005031  2029 _AX5043_PLLCPINB	=	0x5031
                           005039  2030 _AX5043_PLLCPIBOOSTNB	=	0x5039
                           005182  2031 _AX5043_PLLLOCKDETNB	=	0x5182
                           005030  2032 _AX5043_PLLLOOPNB	=	0x5030
                           005038  2033 _AX5043_PLLLOOPBOOSTNB	=	0x5038
                           005033  2034 _AX5043_PLLRANGINGANB	=	0x5033
                           00503B  2035 _AX5043_PLLRANGINGBNB	=	0x503b
                           005183  2036 _AX5043_PLLRNGCLKNB	=	0x5183
                           005032  2037 _AX5043_PLLVCODIVNB	=	0x5032
                           005180  2038 _AX5043_PLLVCOINB	=	0x5180
                           005181  2039 _AX5043_PLLVCOIRNB	=	0x5181
                           005F08  2040 _AX5043_POWCTRL1NB	=	0x5f08
                           005005  2041 _AX5043_POWIRQMASKNB	=	0x5005
                           005003  2042 _AX5043_POWSTATNB	=	0x5003
                           005004  2043 _AX5043_POWSTICKYSTATNB	=	0x5004
                           005027  2044 _AX5043_PWRAMPNB	=	0x5027
                           005002  2045 _AX5043_PWRMODENB	=	0x5002
                           005009  2046 _AX5043_RADIOEVENTMASK0NB	=	0x5009
                           005008  2047 _AX5043_RADIOEVENTMASK1NB	=	0x5008
                           00500F  2048 _AX5043_RADIOEVENTREQ0NB	=	0x500f
                           00500E  2049 _AX5043_RADIOEVENTREQ1NB	=	0x500e
                           00501C  2050 _AX5043_RADIOSTATENB	=	0x501c
                           005F0D  2051 _AX5043_REFNB	=	0x5f0d
                           005040  2052 _AX5043_RSSINB	=	0x5040
                           00522D  2053 _AX5043_RSSIABSTHRNB	=	0x522d
                           00522C  2054 _AX5043_RSSIREFERENCENB	=	0x522c
                           005105  2055 _AX5043_RXDATARATE0NB	=	0x5105
                           005104  2056 _AX5043_RXDATARATE1NB	=	0x5104
                           005103  2057 _AX5043_RXDATARATE2NB	=	0x5103
                           005001  2058 _AX5043_SCRATCHNB	=	0x5001
                           005000  2059 _AX5043_SILICONREVISIONNB	=	0x5000
                           00505B  2060 _AX5043_TIMER0NB	=	0x505b
                           00505A  2061 _AX5043_TIMER1NB	=	0x505a
                           005059  2062 _AX5043_TIMER2NB	=	0x5059
                           005227  2063 _AX5043_TMGRXAGCNB	=	0x5227
                           005223  2064 _AX5043_TMGRXBOOSTNB	=	0x5223
                           005226  2065 _AX5043_TMGRXCOARSEAGCNB	=	0x5226
                           005225  2066 _AX5043_TMGRXOFFSACQNB	=	0x5225
                           005229  2067 _AX5043_TMGRXPREAMBLE1NB	=	0x5229
                           00522A  2068 _AX5043_TMGRXPREAMBLE2NB	=	0x522a
                           00522B  2069 _AX5043_TMGRXPREAMBLE3NB	=	0x522b
                           005228  2070 _AX5043_TMGRXRSSINB	=	0x5228
                           005224  2071 _AX5043_TMGRXSETTLENB	=	0x5224
                           005220  2072 _AX5043_TMGTXBOOSTNB	=	0x5220
                           005221  2073 _AX5043_TMGTXSETTLENB	=	0x5221
                           005055  2074 _AX5043_TRKAFSKDEMOD0NB	=	0x5055
                           005054  2075 _AX5043_TRKAFSKDEMOD1NB	=	0x5054
                           005049  2076 _AX5043_TRKAMPLITUDE0NB	=	0x5049
                           005048  2077 _AX5043_TRKAMPLITUDE1NB	=	0x5048
                           005047  2078 _AX5043_TRKDATARATE0NB	=	0x5047
                           005046  2079 _AX5043_TRKDATARATE1NB	=	0x5046
                           005045  2080 _AX5043_TRKDATARATE2NB	=	0x5045
                           005051  2081 _AX5043_TRKFREQ0NB	=	0x5051
                           005050  2082 _AX5043_TRKFREQ1NB	=	0x5050
                           005053  2083 _AX5043_TRKFSKDEMOD0NB	=	0x5053
                           005052  2084 _AX5043_TRKFSKDEMOD1NB	=	0x5052
                           00504B  2085 _AX5043_TRKPHASE0NB	=	0x504b
                           00504A  2086 _AX5043_TRKPHASE1NB	=	0x504a
                           00504F  2087 _AX5043_TRKRFFREQ0NB	=	0x504f
                           00504E  2088 _AX5043_TRKRFFREQ1NB	=	0x504e
                           00504D  2089 _AX5043_TRKRFFREQ2NB	=	0x504d
                           005169  2090 _AX5043_TXPWRCOEFFA0NB	=	0x5169
                           005168  2091 _AX5043_TXPWRCOEFFA1NB	=	0x5168
                           00516B  2092 _AX5043_TXPWRCOEFFB0NB	=	0x516b
                           00516A  2093 _AX5043_TXPWRCOEFFB1NB	=	0x516a
                           00516D  2094 _AX5043_TXPWRCOEFFC0NB	=	0x516d
                           00516C  2095 _AX5043_TXPWRCOEFFC1NB	=	0x516c
                           00516F  2096 _AX5043_TXPWRCOEFFD0NB	=	0x516f
                           00516E  2097 _AX5043_TXPWRCOEFFD1NB	=	0x516e
                           005171  2098 _AX5043_TXPWRCOEFFE0NB	=	0x5171
                           005170  2099 _AX5043_TXPWRCOEFFE1NB	=	0x5170
                           005167  2100 _AX5043_TXRATE0NB	=	0x5167
                           005166  2101 _AX5043_TXRATE1NB	=	0x5166
                           005165  2102 _AX5043_TXRATE2NB	=	0x5165
                           00506B  2103 _AX5043_WAKEUP0NB	=	0x506b
                           00506A  2104 _AX5043_WAKEUP1NB	=	0x506a
                           00506D  2105 _AX5043_WAKEUPFREQ0NB	=	0x506d
                           00506C  2106 _AX5043_WAKEUPFREQ1NB	=	0x506c
                           005069  2107 _AX5043_WAKEUPTIMER0NB	=	0x5069
                           005068  2108 _AX5043_WAKEUPTIMER1NB	=	0x5068
                           00506E  2109 _AX5043_WAKEUPXOEARLYNB	=	0x506e
                           005F11  2110 _AX5043_XTALAMPLNB	=	0x5f11
                           005184  2111 _AX5043_XTALCAPNB	=	0x5184
                           005F10  2112 _AX5043_XTALOSCNB	=	0x5f10
                           00501D  2113 _AX5043_XTALSTATUSNB	=	0x501d
                           005F00  2114 _AX5043_0xF00NB	=	0x5f00
                           005F0C  2115 _AX5043_0xF0CNB	=	0x5f0c
                           005F18  2116 _AX5043_0xF18NB	=	0x5f18
                           005F1C  2117 _AX5043_0xF1CNB	=	0x5f1c
                           005F21  2118 _AX5043_0xF21NB	=	0x5f21
                           005F22  2119 _AX5043_0xF22NB	=	0x5f22
                           005F23  2120 _AX5043_0xF23NB	=	0x5f23
                           005F26  2121 _AX5043_0xF26NB	=	0x5f26
                           005F30  2122 _AX5043_0xF30NB	=	0x5f30
                           005F31  2123 _AX5043_0xF31NB	=	0x5f31
                           005F32  2124 _AX5043_0xF32NB	=	0x5f32
                           005F33  2125 _AX5043_0xF33NB	=	0x5f33
                           005F34  2126 _AX5043_0xF34NB	=	0x5f34
                           005F35  2127 _AX5043_0xF35NB	=	0x5f35
                           005F44  2128 _AX5043_0xF44NB	=	0x5f44
                           005122  2129 _AX5043_AGCAHYST0NB	=	0x5122
                           005132  2130 _AX5043_AGCAHYST1NB	=	0x5132
                           005142  2131 _AX5043_AGCAHYST2NB	=	0x5142
                           005152  2132 _AX5043_AGCAHYST3NB	=	0x5152
                           005120  2133 _AX5043_AGCGAIN0NB	=	0x5120
                           005130  2134 _AX5043_AGCGAIN1NB	=	0x5130
                           005140  2135 _AX5043_AGCGAIN2NB	=	0x5140
                           005150  2136 _AX5043_AGCGAIN3NB	=	0x5150
                           005123  2137 _AX5043_AGCMINMAX0NB	=	0x5123
                           005133  2138 _AX5043_AGCMINMAX1NB	=	0x5133
                           005143  2139 _AX5043_AGCMINMAX2NB	=	0x5143
                           005153  2140 _AX5043_AGCMINMAX3NB	=	0x5153
                           005121  2141 _AX5043_AGCTARGET0NB	=	0x5121
                           005131  2142 _AX5043_AGCTARGET1NB	=	0x5131
                           005141  2143 _AX5043_AGCTARGET2NB	=	0x5141
                           005151  2144 _AX5043_AGCTARGET3NB	=	0x5151
                           00512B  2145 _AX5043_AMPLITUDEGAIN0NB	=	0x512b
                           00513B  2146 _AX5043_AMPLITUDEGAIN1NB	=	0x513b
                           00514B  2147 _AX5043_AMPLITUDEGAIN2NB	=	0x514b
                           00515B  2148 _AX5043_AMPLITUDEGAIN3NB	=	0x515b
                           00512F  2149 _AX5043_BBOFFSRES0NB	=	0x512f
                           00513F  2150 _AX5043_BBOFFSRES1NB	=	0x513f
                           00514F  2151 _AX5043_BBOFFSRES2NB	=	0x514f
                           00515F  2152 _AX5043_BBOFFSRES3NB	=	0x515f
                           005125  2153 _AX5043_DRGAIN0NB	=	0x5125
                           005135  2154 _AX5043_DRGAIN1NB	=	0x5135
                           005145  2155 _AX5043_DRGAIN2NB	=	0x5145
                           005155  2156 _AX5043_DRGAIN3NB	=	0x5155
                           00512E  2157 _AX5043_FOURFSK0NB	=	0x512e
                           00513E  2158 _AX5043_FOURFSK1NB	=	0x513e
                           00514E  2159 _AX5043_FOURFSK2NB	=	0x514e
                           00515E  2160 _AX5043_FOURFSK3NB	=	0x515e
                           00512D  2161 _AX5043_FREQDEV00NB	=	0x512d
                           00513D  2162 _AX5043_FREQDEV01NB	=	0x513d
                           00514D  2163 _AX5043_FREQDEV02NB	=	0x514d
                           00515D  2164 _AX5043_FREQDEV03NB	=	0x515d
                           00512C  2165 _AX5043_FREQDEV10NB	=	0x512c
                           00513C  2166 _AX5043_FREQDEV11NB	=	0x513c
                           00514C  2167 _AX5043_FREQDEV12NB	=	0x514c
                           00515C  2168 _AX5043_FREQDEV13NB	=	0x515c
                           005127  2169 _AX5043_FREQUENCYGAINA0NB	=	0x5127
                           005137  2170 _AX5043_FREQUENCYGAINA1NB	=	0x5137
                           005147  2171 _AX5043_FREQUENCYGAINA2NB	=	0x5147
                           005157  2172 _AX5043_FREQUENCYGAINA3NB	=	0x5157
                           005128  2173 _AX5043_FREQUENCYGAINB0NB	=	0x5128
                           005138  2174 _AX5043_FREQUENCYGAINB1NB	=	0x5138
                           005148  2175 _AX5043_FREQUENCYGAINB2NB	=	0x5148
                           005158  2176 _AX5043_FREQUENCYGAINB3NB	=	0x5158
                           005129  2177 _AX5043_FREQUENCYGAINC0NB	=	0x5129
                           005139  2178 _AX5043_FREQUENCYGAINC1NB	=	0x5139
                           005149  2179 _AX5043_FREQUENCYGAINC2NB	=	0x5149
                           005159  2180 _AX5043_FREQUENCYGAINC3NB	=	0x5159
                           00512A  2181 _AX5043_FREQUENCYGAIND0NB	=	0x512a
                           00513A  2182 _AX5043_FREQUENCYGAIND1NB	=	0x513a
                           00514A  2183 _AX5043_FREQUENCYGAIND2NB	=	0x514a
                           00515A  2184 _AX5043_FREQUENCYGAIND3NB	=	0x515a
                           005116  2185 _AX5043_FREQUENCYLEAKNB	=	0x5116
                           005126  2186 _AX5043_PHASEGAIN0NB	=	0x5126
                           005136  2187 _AX5043_PHASEGAIN1NB	=	0x5136
                           005146  2188 _AX5043_PHASEGAIN2NB	=	0x5146
                           005156  2189 _AX5043_PHASEGAIN3NB	=	0x5156
                           005207  2190 _AX5043_PKTADDR0NB	=	0x5207
                           005206  2191 _AX5043_PKTADDR1NB	=	0x5206
                           005205  2192 _AX5043_PKTADDR2NB	=	0x5205
                           005204  2193 _AX5043_PKTADDR3NB	=	0x5204
                           005200  2194 _AX5043_PKTADDRCFGNB	=	0x5200
                           00520B  2195 _AX5043_PKTADDRMASK0NB	=	0x520b
                           00520A  2196 _AX5043_PKTADDRMASK1NB	=	0x520a
                           005209  2197 _AX5043_PKTADDRMASK2NB	=	0x5209
                           005208  2198 _AX5043_PKTADDRMASK3NB	=	0x5208
                           005201  2199 _AX5043_PKTLENCFGNB	=	0x5201
                           005202  2200 _AX5043_PKTLENOFFSETNB	=	0x5202
                           005203  2201 _AX5043_PKTMAXLENNB	=	0x5203
                           005118  2202 _AX5043_RXPARAMCURSETNB	=	0x5118
                           005117  2203 _AX5043_RXPARAMSETSNB	=	0x5117
                           005124  2204 _AX5043_TIMEGAIN0NB	=	0x5124
                           005134  2205 _AX5043_TIMEGAIN1NB	=	0x5134
                           005144  2206 _AX5043_TIMEGAIN2NB	=	0x5144
                           005154  2207 _AX5043_TIMEGAIN3NB	=	0x5154
      00038D                       2208 _UART_Proc_VerifyIncomingMsg_PARM_2:
      00038D                       2209 	.ds 3
      000390                       2210 _UART_Proc_VerifyIncomingMsg_PARM_3:
      000390                       2211 	.ds 3
      000393                       2212 _UART_Proc_VerifyIncomingMsg_cmd_1_238:
      000393                       2213 	.ds 3
      000396                       2214 _UART_Proc_VerifyIncomingMsg_RetVal_1_239:
      000396                       2215 	.ds 1
      000397                       2216 _UART_Proc_VerifyIncomingMsg_RxLength_1_239:
      000397                       2217 	.ds 1
      000398                       2218 _UART_Proc_VerifyIncomingMsg_CRC32Rx_1_239:
      000398                       2219 	.ds 4
      00039C                       2220 _UART_Proc_ModifiyKissSpecialCharacters_PARM_2:
      00039C                       2221 	.ds 3
      00039F                       2222 _UART_Proc_ModifiyKissSpecialCharacters_UartRxBuffer_1_250:
      00039F                       2223 	.ds 3
      0003A2                       2224 _UART_Proc_SendMessage_PARM_2:
      0003A2                       2225 	.ds 1
      0003A3                       2226 _UART_Proc_SendMessage_PARM_3:
      0003A3                       2227 	.ds 1
      0003A4                       2228 _UART_Proc_SendMessage_payload_1_258:
      0003A4                       2229 	.ds 3
      0003A7                       2230 _UART_Calc_CRC32_PARM_2:
      0003A7                       2231 	.ds 1
      0003A8                       2232 _UART_Calc_CRC32_PARM_3:
      0003A8                       2233 	.ds 1
      0003A9                       2234 _UART_Calc_CRC32_message_1_261:
      0003A9                       2235 	.ds 3
      0003AC                       2236 _UART_Calc_CRC32_crc_1_262:
      0003AC                       2237 	.ds 4
                                   2238 ;--------------------------------------------------------
                                   2239 ; absolute external ram data
                                   2240 ;--------------------------------------------------------
                                   2241 	.area XABS    (ABS,XDATA)
                                   2242 ;--------------------------------------------------------
                                   2243 ; external initialized ram data
                                   2244 ;--------------------------------------------------------
                                   2245 	.area XISEG   (XDATA)
                                   2246 	.area HOME    (CODE)
                                   2247 	.area GSINIT0 (CODE)
                                   2248 	.area GSINIT1 (CODE)
                                   2249 	.area GSINIT2 (CODE)
                                   2250 	.area GSINIT3 (CODE)
                                   2251 	.area GSINIT4 (CODE)
                                   2252 	.area GSINIT5 (CODE)
                                   2253 	.area GSINIT  (CODE)
                                   2254 	.area GSFINAL (CODE)
                                   2255 	.area CSEG    (CODE)
                                   2256 ;--------------------------------------------------------
                                   2257 ; global & static initialisations
                                   2258 ;--------------------------------------------------------
                                   2259 	.area HOME    (CODE)
                                   2260 	.area GSINIT  (CODE)
                                   2261 	.area GSFINAL (CODE)
                                   2262 	.area GSINIT  (CODE)
                                   2263 ;--------------------------------------------------------
                                   2264 ; Home
                                   2265 ;--------------------------------------------------------
                                   2266 	.area HOME    (CODE)
                                   2267 	.area HOME    (CODE)
                                   2268 ;--------------------------------------------------------
                                   2269 ; code
                                   2270 ;--------------------------------------------------------
                                   2271 	.area CSEG    (CODE)
                                   2272 ;------------------------------------------------------------
                                   2273 ;Allocation info for local variables in function 'UART_Proc_PortInit'
                                   2274 ;------------------------------------------------------------
                                   2275 ;	..\src\COMMON\UartComProc.c:24: __reentrantb void UART_Proc_PortInit(void) __reentrant
                                   2276 ;	-----------------------------------------
                                   2277 ;	 function UART_Proc_PortInit
                                   2278 ;	-----------------------------------------
      00573E                       2279 _UART_Proc_PortInit:
                           000007  2280 	ar7 = 0x07
                           000006  2281 	ar6 = 0x06
                           000005  2282 	ar5 = 0x05
                           000004  2283 	ar4 = 0x04
                           000003  2284 	ar3 = 0x03
                           000002  2285 	ar2 = 0x02
                           000001  2286 	ar1 = 0x01
                           000000  2287 	ar0 = 0x00
                                   2288 ;	..\src\COMMON\UartComProc.c:26: PALTB |= 0x11;
      00573E 90 70 09         [24] 2289 	mov	dptr,#_PALTB
      005741 E0               [24] 2290 	movx	a,@dptr
      005742 FF               [12] 2291 	mov	r7,a
      005743 74 11            [12] 2292 	mov	a,#0x11
      005745 4F               [12] 2293 	orl	a,r7
      005746 F0               [24] 2294 	movx	@dptr,a
                                   2295 ;	..\src\COMMON\UartComProc.c:27: DIRB |= (1<<0) | (1<<4);
      005747 43 8A 11         [24] 2296 	orl	_DIRB,#0x11
                                   2297 ;	..\src\COMMON\UartComProc.c:28: DIRB &= (uint8_t)~(1<<5);
      00574A 53 8A DF         [24] 2298 	anl	_DIRB,#0xdf
                                   2299 ;	..\src\COMMON\UartComProc.c:29: DIRB &= (uint8_t)~(1<<1);
      00574D 53 8A FD         [24] 2300 	anl	_DIRB,#0xfd
                                   2301 ;	..\src\COMMON\UartComProc.c:30: PINSEL &= (uint8_t)~((1<<0) | (1<<1));
      005750 90 70 0B         [24] 2302 	mov	dptr,#_PINSEL
      005753 E0               [24] 2303 	movx	a,@dptr
      005754 FF               [12] 2304 	mov	r7,a
      005755 74 FC            [12] 2305 	mov	a,#0xfc
      005757 5F               [12] 2306 	anl	a,r7
      005758 F0               [24] 2307 	movx	@dptr,a
                                   2308 ;	..\src\COMMON\UartComProc.c:31: uart_timer1_baud(CLKSRC_SYSCLK, 9600, 20000000UL);
      005759 E4               [12] 2309 	clr	a
      00575A C0 E0            [24] 2310 	push	acc
      00575C 74 2D            [12] 2311 	mov	a,#0x2d
      00575E C0 E0            [24] 2312 	push	acc
      005760 74 31            [12] 2313 	mov	a,#0x31
      005762 C0 E0            [24] 2314 	push	acc
      005764 74 01            [12] 2315 	mov	a,#0x01
      005766 C0 E0            [24] 2316 	push	acc
      005768 03               [12] 2317 	rr	a
      005769 C0 E0            [24] 2318 	push	acc
      00576B 74 25            [12] 2319 	mov	a,#0x25
      00576D C0 E0            [24] 2320 	push	acc
      00576F E4               [12] 2321 	clr	a
      005770 C0 E0            [24] 2322 	push	acc
      005772 C0 E0            [24] 2323 	push	acc
      005774 75 82 06         [24] 2324 	mov	dpl,#0x06
      005777 12 7E 10         [24] 2325 	lcall	_uart_timer1_baud
      00577A E5 81            [12] 2326 	mov	a,sp
      00577C 24 F8            [12] 2327 	add	a,#0xf8
      00577E F5 81            [12] 2328 	mov	sp,a
                                   2329 ;	..\src\COMMON\UartComProc.c:32: uart0_init(1, 8, 1);
      005780 90 04 D7         [24] 2330 	mov	dptr,#_uart0_init_PARM_2
      005783 74 08            [12] 2331 	mov	a,#0x08
      005785 F0               [24] 2332 	movx	@dptr,a
      005786 90 04 D8         [24] 2333 	mov	dptr,#_uart0_init_PARM_3
      005789 74 01            [12] 2334 	mov	a,#0x01
      00578B F0               [24] 2335 	movx	@dptr,a
      00578C 75 82 01         [24] 2336 	mov	dpl,#0x01
      00578F 02 8D CB         [24] 2337 	ljmp	_uart0_init
                                   2338 ;------------------------------------------------------------
                                   2339 ;Allocation info for local variables in function 'UART_Proc_VerifyIncomingMsg'
                                   2340 ;------------------------------------------------------------
                                   2341 ;sloc0                     Allocated with name '_UART_Proc_VerifyIncomingMsg_sloc0_1_0'
                                   2342 ;payload                   Allocated with name '_UART_Proc_VerifyIncomingMsg_PARM_2'
                                   2343 ;PayloadLength             Allocated with name '_UART_Proc_VerifyIncomingMsg_PARM_3'
                                   2344 ;cmd                       Allocated with name '_UART_Proc_VerifyIncomingMsg_cmd_1_238'
                                   2345 ;RetVal                    Allocated with name '_UART_Proc_VerifyIncomingMsg_RetVal_1_239'
                                   2346 ;i                         Allocated with name '_UART_Proc_VerifyIncomingMsg_i_1_239'
                                   2347 ;scape                     Allocated with name '_UART_Proc_VerifyIncomingMsg_scape_1_239'
                                   2348 ;UartRxBuffer              Allocated with name '_UART_Proc_VerifyIncomingMsg_UartRxBuffer_1_239'
                                   2349 ;RxLength                  Allocated with name '_UART_Proc_VerifyIncomingMsg_RxLength_1_239'
                                   2350 ;CRC32                     Allocated with name '_UART_Proc_VerifyIncomingMsg_CRC32_1_239'
                                   2351 ;CRC32Rx                   Allocated with name '_UART_Proc_VerifyIncomingMsg_CRC32Rx_1_239'
                                   2352 ;------------------------------------------------------------
                                   2353 ;	..\src\COMMON\UartComProc.c:53: uint8_t  UART_Proc_VerifyIncomingMsg(uint8_t *cmd, uint8_t *payload,uint8_t *PayloadLength)
                                   2354 ;	-----------------------------------------
                                   2355 ;	 function UART_Proc_VerifyIncomingMsg
                                   2356 ;	-----------------------------------------
      005792                       2357 _UART_Proc_VerifyIncomingMsg:
      005792 AF F0            [24] 2358 	mov	r7,b
      005794 AE 83            [24] 2359 	mov	r6,dph
      005796 E5 82            [12] 2360 	mov	a,dpl
      005798 90 03 93         [24] 2361 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_cmd_1_238
      00579B F0               [24] 2362 	movx	@dptr,a
      00579C EE               [12] 2363 	mov	a,r6
      00579D A3               [24] 2364 	inc	dptr
      00579E F0               [24] 2365 	movx	@dptr,a
      00579F EF               [12] 2366 	mov	a,r7
      0057A0 A3               [24] 2367 	inc	dptr
      0057A1 F0               [24] 2368 	movx	@dptr,a
                                   2369 ;	..\src\COMMON\UartComProc.c:62: UartRxBuffer  =(uint8_t*) malloc(sizeof(uint8_t)*40);
      0057A2 90 00 28         [24] 2370 	mov	dptr,#0x0028
      0057A5 12 89 2E         [24] 2371 	lcall	_malloc
      0057A8 AE 82            [24] 2372 	mov	r6,dpl
      0057AA AF 83            [24] 2373 	mov	r7,dph
                                   2374 ;	..\src\COMMON\UartComProc.c:63: if(uart0_rxcount())
      0057AC 12 A3 B7         [24] 2375 	lcall	_uart0_rxcount
      0057AF E5 82            [12] 2376 	mov	a,dpl
      0057B1 70 01            [24] 2377 	jnz	00149$
      0057B3 22               [24] 2378 	ret
      0057B4                       2379 00149$:
                                   2380 ;	..\src\COMMON\UartComProc.c:65: *UartRxBuffer = uart0_rx();
      0057B4 12 87 6D         [24] 2381 	lcall	_uart0_rx
      0057B7 AD 82            [24] 2382 	mov	r5,dpl
      0057B9 8E 82            [24] 2383 	mov	dpl,r6
      0057BB 8F 83            [24] 2384 	mov	dph,r7
      0057BD ED               [12] 2385 	mov	a,r5
      0057BE F0               [24] 2386 	movx	@dptr,a
                                   2387 ;	..\src\COMMON\UartComProc.c:66: RetVal = ERR_BAD_FORMAT; // Indico que se recibio algo, pero de volver con este valor, no se recibio una trama KISS
      0057BF 90 03 96         [24] 2388 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RetVal_1_239
      0057C2 74 02            [12] 2389 	mov	a,#0x02
      0057C4 F0               [24] 2390 	movx	@dptr,a
                                   2391 ;	..\src\COMMON\UartComProc.c:67: delay_ms(500);
      0057C5 90 01 F4         [24] 2392 	mov	dptr,#0x01f4
      0057C8 C0 07            [24] 2393 	push	ar7
      0057CA C0 06            [24] 2394 	push	ar6
      0057CC 12 10 62         [24] 2395 	lcall	_delay_ms
      0057CF D0 06            [24] 2396 	pop	ar6
      0057D1 D0 07            [24] 2397 	pop	ar7
                                   2398 ;	..\src\COMMON\UartComProc.c:68: RxLength = 1;
      0057D3 90 03 97         [24] 2399 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RxLength_1_239
      0057D6 74 01            [12] 2400 	mov	a,#0x01
      0057D8 F0               [24] 2401 	movx	@dptr,a
                                   2402 ;	..\src\COMMON\UartComProc.c:69: if(*UartRxBuffer == 0xC0)
      0057D9 8E 82            [24] 2403 	mov	dpl,r6
      0057DB 8F 83            [24] 2404 	mov	dph,r7
      0057DD E0               [24] 2405 	movx	a,@dptr
      0057DE FD               [12] 2406 	mov	r5,a
      0057DF BD C0 02         [24] 2407 	cjne	r5,#0xc0,00150$
      0057E2 80 03            [24] 2408 	sjmp	00151$
      0057E4                       2409 00150$:
      0057E4 02 59 C6         [24] 2410 	ljmp	00117$
      0057E7                       2411 00151$:
                                   2412 ;	..\src\COMMON\UartComProc.c:71: do{
      0057E7 7D 00            [12] 2413 	mov	r5,#0x00
      0057E9 7C 01            [12] 2414 	mov	r4,#0x01
      0057EB                       2415 00104$:
                                   2416 ;	..\src\COMMON\UartComProc.c:72: if(uart0_rxcount())
      0057EB 12 A3 B7         [24] 2417 	lcall	_uart0_rxcount
      0057EE E5 82            [12] 2418 	mov	a,dpl
      0057F0 60 19            [24] 2419 	jz	00102$
                                   2420 ;	..\src\COMMON\UartComProc.c:74: *(UartRxBuffer+RxLength)=uart0_rx();
      0057F2 EC               [12] 2421 	mov	a,r4
      0057F3 2E               [12] 2422 	add	a,r6
      0057F4 FA               [12] 2423 	mov	r2,a
      0057F5 E4               [12] 2424 	clr	a
      0057F6 3F               [12] 2425 	addc	a,r7
      0057F7 FB               [12] 2426 	mov	r3,a
      0057F8 12 87 6D         [24] 2427 	lcall	_uart0_rx
      0057FB A9 82            [24] 2428 	mov	r1,dpl
      0057FD 8A 82            [24] 2429 	mov	dpl,r2
      0057FF 8B 83            [24] 2430 	mov	dph,r3
      005801 E9               [12] 2431 	mov	a,r1
      005802 F0               [24] 2432 	movx	@dptr,a
                                   2433 ;	..\src\COMMON\UartComProc.c:75: RxLength=(RxLength)+1;
      005803 0C               [12] 2434 	inc	r4
      005804 90 03 97         [24] 2435 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RxLength_1_239
      005807 EC               [12] 2436 	mov	a,r4
      005808 F0               [24] 2437 	movx	@dptr,a
      005809 80 17            [24] 2438 	sjmp	00105$
      00580B                       2439 00102$:
                                   2440 ;	..\src\COMMON\UartComProc.c:80: scape ++;
      00580B 0D               [12] 2441 	inc	r5
                                   2442 ;	..\src\COMMON\UartComProc.c:81: delay_ms(1);
      00580C 90 00 01         [24] 2443 	mov	dptr,#0x0001
      00580F C0 07            [24] 2444 	push	ar7
      005811 C0 06            [24] 2445 	push	ar6
      005813 C0 05            [24] 2446 	push	ar5
      005815 C0 04            [24] 2447 	push	ar4
      005817 12 10 62         [24] 2448 	lcall	_delay_ms
      00581A D0 04            [24] 2449 	pop	ar4
      00581C D0 05            [24] 2450 	pop	ar5
      00581E D0 06            [24] 2451 	pop	ar6
      005820 D0 07            [24] 2452 	pop	ar7
      005822                       2453 00105$:
                                   2454 ;	..\src\COMMON\UartComProc.c:83: }while(*(UartRxBuffer+(RxLength-1)) != 0xC0);
      005822 8C 02            [24] 2455 	mov	ar2,r4
      005824 7B 00            [12] 2456 	mov	r3,#0x00
      005826 1A               [12] 2457 	dec	r2
      005827 BA FF 01         [24] 2458 	cjne	r2,#0xff,00153$
      00582A 1B               [12] 2459 	dec	r3
      00582B                       2460 00153$:
      00582B EA               [12] 2461 	mov	a,r2
      00582C 2E               [12] 2462 	add	a,r6
      00582D F5 82            [12] 2463 	mov	dpl,a
      00582F EB               [12] 2464 	mov	a,r3
      005830 3F               [12] 2465 	addc	a,r7
      005831 F5 83            [12] 2466 	mov	dph,a
      005833 E0               [24] 2467 	movx	a,@dptr
      005834 FB               [12] 2468 	mov	r3,a
      005835 BB C0 B3         [24] 2469 	cjne	r3,#0xc0,00104$
                                   2470 ;	..\src\COMMON\UartComProc.c:84: if(scape == 250)
      005838 90 03 97         [24] 2471 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RxLength_1_239
      00583B EC               [12] 2472 	mov	a,r4
      00583C F0               [24] 2473 	movx	@dptr,a
      00583D BD FA 09         [24] 2474 	cjne	r5,#0xfa,00114$
                                   2475 ;	..\src\COMMON\UartComProc.c:86: RetVal = ERR_BAD_ENDING;// indico que no se recibio el paquete correctamente
      005840 90 03 96         [24] 2476 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RetVal_1_239
      005843 74 03            [12] 2477 	mov	a,#0x03
      005845 F0               [24] 2478 	movx	@dptr,a
      005846 02 59 C6         [24] 2479 	ljmp	00117$
      005849                       2480 00114$:
                                   2481 ;	..\src\COMMON\UartComProc.c:91: UART_Proc_ModifiyKissSpecialCharacters(UartRxBuffer,&RxLength);
      005849 8E 04            [24] 2482 	mov	ar4,r6
      00584B 8F 05            [24] 2483 	mov	ar5,r7
      00584D 7B 00            [12] 2484 	mov	r3,#0x00
      00584F 90 03 9C         [24] 2485 	mov	dptr,#_UART_Proc_ModifiyKissSpecialCharacters_PARM_2
      005852 74 97            [12] 2486 	mov	a,#_UART_Proc_VerifyIncomingMsg_RxLength_1_239
      005854 F0               [24] 2487 	movx	@dptr,a
      005855 74 03            [12] 2488 	mov	a,#(_UART_Proc_VerifyIncomingMsg_RxLength_1_239 >> 8)
      005857 A3               [24] 2489 	inc	dptr
      005858 F0               [24] 2490 	movx	@dptr,a
      005859 E4               [12] 2491 	clr	a
      00585A A3               [24] 2492 	inc	dptr
      00585B F0               [24] 2493 	movx	@dptr,a
      00585C 8C 82            [24] 2494 	mov	dpl,r4
      00585E 8D 83            [24] 2495 	mov	dph,r5
      005860 8B F0            [24] 2496 	mov	b,r3
      005862 C0 07            [24] 2497 	push	ar7
      005864 C0 06            [24] 2498 	push	ar6
      005866 12 59 D8         [24] 2499 	lcall	_UART_Proc_ModifiyKissSpecialCharacters
      005869 D0 06            [24] 2500 	pop	ar6
      00586B D0 07            [24] 2501 	pop	ar7
                                   2502 ;	..\src\COMMON\UartComProc.c:93: if(*UartRxBuffer == 0xC0 && *(UartRxBuffer+RxLength-1) == 0xC0)
      00586D 8E 82            [24] 2503 	mov	dpl,r6
      00586F 8F 83            [24] 2504 	mov	dph,r7
      005871 E0               [24] 2505 	movx	a,@dptr
      005872 FD               [12] 2506 	mov	r5,a
      005873 BD C0 02         [24] 2507 	cjne	r5,#0xc0,00158$
      005876 80 03            [24] 2508 	sjmp	00159$
      005878                       2509 00158$:
      005878 02 59 C6         [24] 2510 	ljmp	00117$
      00587B                       2511 00159$:
      00587B 90 03 97         [24] 2512 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RxLength_1_239
      00587E E0               [24] 2513 	movx	a,@dptr
      00587F FD               [12] 2514 	mov	r5,a
      005880 2E               [12] 2515 	add	a,r6
      005881 FB               [12] 2516 	mov	r3,a
      005882 E4               [12] 2517 	clr	a
      005883 3F               [12] 2518 	addc	a,r7
      005884 FC               [12] 2519 	mov	r4,a
      005885 EB               [12] 2520 	mov	a,r3
      005886 24 FF            [12] 2521 	add	a,#0xff
      005888 F5 82            [12] 2522 	mov	dpl,a
      00588A EC               [12] 2523 	mov	a,r4
      00588B 34 FF            [12] 2524 	addc	a,#0xff
      00588D F5 83            [12] 2525 	mov	dph,a
      00588F E0               [24] 2526 	movx	a,@dptr
      005890 FC               [12] 2527 	mov	r4,a
      005891 BC C0 02         [24] 2528 	cjne	r4,#0xc0,00160$
      005894 80 03            [24] 2529 	sjmp	00161$
      005896                       2530 00160$:
      005896 02 59 C6         [24] 2531 	ljmp	00117$
      005899                       2532 00161$:
                                   2533 ;	..\src\COMMON\UartComProc.c:95: CRC32 = UART_Calc_CRC32(UartRxBuffer+1,RxLength-START_CHARACTER_LENGTH-END_CHARACTER_LENGTH-CRC_LENGTH,1);// lo pongo en little endian, ya que el memcpy lo pasa asi
      005899 74 01            [12] 2534 	mov	a,#0x01
      00589B 2E               [12] 2535 	add	a,r6
      00589C FB               [12] 2536 	mov	r3,a
      00589D E4               [12] 2537 	clr	a
      00589E 3F               [12] 2538 	addc	a,r7
      00589F FC               [12] 2539 	mov	r4,a
      0058A0 8B 00            [24] 2540 	mov	ar0,r3
      0058A2 8C 01            [24] 2541 	mov	ar1,r4
      0058A4 7A 00            [12] 2542 	mov	r2,#0x00
      0058A6 ED               [12] 2543 	mov	a,r5
      0058A7 24 FA            [12] 2544 	add	a,#0xfa
      0058A9 90 03 A7         [24] 2545 	mov	dptr,#_UART_Calc_CRC32_PARM_2
      0058AC F0               [24] 2546 	movx	@dptr,a
      0058AD 90 03 A8         [24] 2547 	mov	dptr,#_UART_Calc_CRC32_PARM_3
      0058B0 74 01            [12] 2548 	mov	a,#0x01
      0058B2 F0               [24] 2549 	movx	@dptr,a
      0058B3 88 82            [24] 2550 	mov	dpl,r0
      0058B5 89 83            [24] 2551 	mov	dph,r1
      0058B7 8A F0            [24] 2552 	mov	b,r2
      0058B9 C0 07            [24] 2553 	push	ar7
      0058BB C0 06            [24] 2554 	push	ar6
      0058BD C0 04            [24] 2555 	push	ar4
      0058BF C0 03            [24] 2556 	push	ar3
      0058C1 12 5C 2E         [24] 2557 	lcall	_UART_Calc_CRC32
      0058C4 85 82 26         [24] 2558 	mov	_UART_Proc_VerifyIncomingMsg_sloc0_1_0,dpl
      0058C7 85 83 27         [24] 2559 	mov	(_UART_Proc_VerifyIncomingMsg_sloc0_1_0 + 1),dph
      0058CA 85 F0 28         [24] 2560 	mov	(_UART_Proc_VerifyIncomingMsg_sloc0_1_0 + 2),b
      0058CD F5 29            [12] 2561 	mov	(_UART_Proc_VerifyIncomingMsg_sloc0_1_0 + 3),a
      0058CF D0 03            [24] 2562 	pop	ar3
      0058D1 D0 04            [24] 2563 	pop	ar4
      0058D3 D0 06            [24] 2564 	pop	ar6
      0058D5 D0 07            [24] 2565 	pop	ar7
                                   2566 ;	..\src\COMMON\UartComProc.c:96: memcpy(&CRC32Rx,UartRxBuffer+RxLength-START_CHARACTER_LENGTH-CRC_LENGTH,CRC_LENGTH);
      0058D7 90 03 97         [24] 2567 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RxLength_1_239
      0058DA E0               [24] 2568 	movx	a,@dptr
      0058DB 2E               [12] 2569 	add	a,r6
      0058DC FD               [12] 2570 	mov	r5,a
      0058DD E4               [12] 2571 	clr	a
      0058DE 3F               [12] 2572 	addc	a,r7
      0058DF FA               [12] 2573 	mov	r2,a
      0058E0 ED               [12] 2574 	mov	a,r5
      0058E1 24 FB            [12] 2575 	add	a,#0xfb
      0058E3 FD               [12] 2576 	mov	r5,a
      0058E4 EA               [12] 2577 	mov	a,r2
      0058E5 34 FF            [12] 2578 	addc	a,#0xff
      0058E7 FA               [12] 2579 	mov	r2,a
      0058E8 90 04 3D         [24] 2580 	mov	dptr,#_memcpy_PARM_2
      0058EB ED               [12] 2581 	mov	a,r5
      0058EC F0               [24] 2582 	movx	@dptr,a
      0058ED EA               [12] 2583 	mov	a,r2
      0058EE A3               [24] 2584 	inc	dptr
      0058EF F0               [24] 2585 	movx	@dptr,a
      0058F0 E4               [12] 2586 	clr	a
      0058F1 A3               [24] 2587 	inc	dptr
      0058F2 F0               [24] 2588 	movx	@dptr,a
      0058F3 90 04 40         [24] 2589 	mov	dptr,#_memcpy_PARM_3
      0058F6 74 04            [12] 2590 	mov	a,#0x04
      0058F8 F0               [24] 2591 	movx	@dptr,a
      0058F9 E4               [12] 2592 	clr	a
      0058FA A3               [24] 2593 	inc	dptr
      0058FB F0               [24] 2594 	movx	@dptr,a
      0058FC 90 03 98         [24] 2595 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_CRC32Rx_1_239
      0058FF 75 F0 00         [24] 2596 	mov	b,#0x00
      005902 C0 07            [24] 2597 	push	ar7
      005904 C0 06            [24] 2598 	push	ar6
      005906 C0 04            [24] 2599 	push	ar4
      005908 C0 03            [24] 2600 	push	ar3
      00590A 12 83 01         [24] 2601 	lcall	_memcpy
      00590D D0 03            [24] 2602 	pop	ar3
      00590F D0 04            [24] 2603 	pop	ar4
      005911 D0 06            [24] 2604 	pop	ar6
      005913 D0 07            [24] 2605 	pop	ar7
                                   2606 ;	..\src\COMMON\UartComProc.c:98: if(CRC32Rx == CRC32)
      005915 90 03 98         [24] 2607 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_CRC32Rx_1_239
      005918 E0               [24] 2608 	movx	a,@dptr
      005919 F8               [12] 2609 	mov	r0,a
      00591A A3               [24] 2610 	inc	dptr
      00591B E0               [24] 2611 	movx	a,@dptr
      00591C F9               [12] 2612 	mov	r1,a
      00591D A3               [24] 2613 	inc	dptr
      00591E E0               [24] 2614 	movx	a,@dptr
      00591F FA               [12] 2615 	mov	r2,a
      005920 A3               [24] 2616 	inc	dptr
      005921 E0               [24] 2617 	movx	a,@dptr
      005922 FD               [12] 2618 	mov	r5,a
      005923 E8               [12] 2619 	mov	a,r0
      005924 B5 26 0E         [24] 2620 	cjne	a,_UART_Proc_VerifyIncomingMsg_sloc0_1_0,00162$
      005927 E9               [12] 2621 	mov	a,r1
      005928 B5 27 0A         [24] 2622 	cjne	a,(_UART_Proc_VerifyIncomingMsg_sloc0_1_0 + 1),00162$
      00592B EA               [12] 2623 	mov	a,r2
      00592C B5 28 06         [24] 2624 	cjne	a,(_UART_Proc_VerifyIncomingMsg_sloc0_1_0 + 2),00162$
      00592F ED               [12] 2625 	mov	a,r5
      005930 B5 29 02         [24] 2626 	cjne	a,(_UART_Proc_VerifyIncomingMsg_sloc0_1_0 + 3),00162$
      005933 80 02            [24] 2627 	sjmp	00163$
      005935                       2628 00162$:
      005935 80 6E            [24] 2629 	sjmp	00108$
      005937                       2630 00163$:
                                   2631 ;	..\src\COMMON\UartComProc.c:101: RetVal = RX_SUCCESFULL;
      005937 90 03 96         [24] 2632 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RetVal_1_239
      00593A 74 04            [12] 2633 	mov	a,#0x04
      00593C F0               [24] 2634 	movx	@dptr,a
                                   2635 ;	..\src\COMMON\UartComProc.c:102: *cmd = *(UartRxBuffer+1);
      00593D 90 03 93         [24] 2636 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_cmd_1_238
      005940 E0               [24] 2637 	movx	a,@dptr
      005941 F9               [12] 2638 	mov	r1,a
      005942 A3               [24] 2639 	inc	dptr
      005943 E0               [24] 2640 	movx	a,@dptr
      005944 FA               [12] 2641 	mov	r2,a
      005945 A3               [24] 2642 	inc	dptr
      005946 E0               [24] 2643 	movx	a,@dptr
      005947 FD               [12] 2644 	mov	r5,a
      005948 8B 82            [24] 2645 	mov	dpl,r3
      00594A 8C 83            [24] 2646 	mov	dph,r4
      00594C E0               [24] 2647 	movx	a,@dptr
      00594D 89 82            [24] 2648 	mov	dpl,r1
      00594F 8A 83            [24] 2649 	mov	dph,r2
      005951 8D F0            [24] 2650 	mov	b,r5
      005953 12 87 88         [24] 2651 	lcall	__gptrput
                                   2652 ;	..\src\COMMON\UartComProc.c:103: memcpy(payload,UartRxBuffer+START_CHARACTER_LENGTH+COMMAND_LENGTH,RxLength-START_CHARACTER_LENGTH-CRC_LENGTH-END_CHARACTER_LENGTH); // porq esta linea me adelanta !!
      005956 90 03 8D         [24] 2653 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_PARM_2
      005959 E0               [24] 2654 	movx	a,@dptr
      00595A FB               [12] 2655 	mov	r3,a
      00595B A3               [24] 2656 	inc	dptr
      00595C E0               [24] 2657 	movx	a,@dptr
      00595D FC               [12] 2658 	mov	r4,a
      00595E A3               [24] 2659 	inc	dptr
      00595F E0               [24] 2660 	movx	a,@dptr
      005960 FD               [12] 2661 	mov	r5,a
      005961 74 02            [12] 2662 	mov	a,#0x02
      005963 2E               [12] 2663 	add	a,r6
      005964 F9               [12] 2664 	mov	r1,a
      005965 E4               [12] 2665 	clr	a
      005966 3F               [12] 2666 	addc	a,r7
      005967 F8               [12] 2667 	mov	r0,a
      005968 7A 00            [12] 2668 	mov	r2,#0x00
      00596A C0 06            [24] 2669 	push	ar6
      00596C C0 07            [24] 2670 	push	ar7
      00596E 90 03 97         [24] 2671 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RxLength_1_239
      005971 E0               [24] 2672 	movx	a,@dptr
      005972 7E 00            [12] 2673 	mov	r6,#0x00
      005974 24 FA            [12] 2674 	add	a,#0xfa
      005976 FF               [12] 2675 	mov	r7,a
      005977 EE               [12] 2676 	mov	a,r6
      005978 34 FF            [12] 2677 	addc	a,#0xff
      00597A FE               [12] 2678 	mov	r6,a
      00597B 90 04 3D         [24] 2679 	mov	dptr,#_memcpy_PARM_2
      00597E E9               [12] 2680 	mov	a,r1
      00597F F0               [24] 2681 	movx	@dptr,a
      005980 E8               [12] 2682 	mov	a,r0
      005981 A3               [24] 2683 	inc	dptr
      005982 F0               [24] 2684 	movx	@dptr,a
      005983 EA               [12] 2685 	mov	a,r2
      005984 A3               [24] 2686 	inc	dptr
      005985 F0               [24] 2687 	movx	@dptr,a
      005986 90 04 40         [24] 2688 	mov	dptr,#_memcpy_PARM_3
      005989 EF               [12] 2689 	mov	a,r7
      00598A F0               [24] 2690 	movx	@dptr,a
      00598B EE               [12] 2691 	mov	a,r6
      00598C A3               [24] 2692 	inc	dptr
      00598D F0               [24] 2693 	movx	@dptr,a
      00598E 8B 82            [24] 2694 	mov	dpl,r3
      005990 8C 83            [24] 2695 	mov	dph,r4
      005992 8D F0            [24] 2696 	mov	b,r5
      005994 C0 07            [24] 2697 	push	ar7
      005996 C0 06            [24] 2698 	push	ar6
      005998 12 83 01         [24] 2699 	lcall	_memcpy
      00599B D0 06            [24] 2700 	pop	ar6
      00599D D0 07            [24] 2701 	pop	ar7
      00599F D0 07            [24] 2702 	pop	ar7
      0059A1 D0 06            [24] 2703 	pop	ar6
      0059A3 80 06            [24] 2704 	sjmp	00109$
      0059A5                       2705 00108$:
                                   2706 ;	..\src\COMMON\UartComProc.c:107: RetVal = ERR_BAD_CRC;
      0059A5 90 03 96         [24] 2707 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RetVal_1_239
      0059A8 74 01            [12] 2708 	mov	a,#0x01
      0059AA F0               [24] 2709 	movx	@dptr,a
      0059AB                       2710 00109$:
                                   2711 ;	..\src\COMMON\UartComProc.c:109: *PayloadLength = RxLength -START_CHARACTER_LENGTH-COMMAND_LENGTH-CRC_LENGTH-END_CHARACTER_LENGTH;
      0059AB 90 03 90         [24] 2712 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_PARM_3
      0059AE E0               [24] 2713 	movx	a,@dptr
      0059AF FB               [12] 2714 	mov	r3,a
      0059B0 A3               [24] 2715 	inc	dptr
      0059B1 E0               [24] 2716 	movx	a,@dptr
      0059B2 FC               [12] 2717 	mov	r4,a
      0059B3 A3               [24] 2718 	inc	dptr
      0059B4 E0               [24] 2719 	movx	a,@dptr
      0059B5 FD               [12] 2720 	mov	r5,a
      0059B6 90 03 97         [24] 2721 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RxLength_1_239
      0059B9 E0               [24] 2722 	movx	a,@dptr
      0059BA 24 F9            [12] 2723 	add	a,#0xf9
      0059BC FA               [12] 2724 	mov	r2,a
      0059BD 8B 82            [24] 2725 	mov	dpl,r3
      0059BF 8C 83            [24] 2726 	mov	dph,r4
      0059C1 8D F0            [24] 2727 	mov	b,r5
      0059C3 12 87 88         [24] 2728 	lcall	__gptrput
      0059C6                       2729 00117$:
                                   2730 ;	..\src\COMMON\UartComProc.c:113: free(UartRxBuffer);
      0059C6 7D 00            [12] 2731 	mov	r5,#0x00
      0059C8 8E 82            [24] 2732 	mov	dpl,r6
      0059CA 8F 83            [24] 2733 	mov	dph,r7
      0059CC 8D F0            [24] 2734 	mov	b,r5
      0059CE 12 78 B7         [24] 2735 	lcall	_free
                                   2736 ;	..\src\COMMON\UartComProc.c:114: return RetVal;
      0059D1 90 03 96         [24] 2737 	mov	dptr,#_UART_Proc_VerifyIncomingMsg_RetVal_1_239
      0059D4 E0               [24] 2738 	movx	a,@dptr
      0059D5 F5 82            [12] 2739 	mov	dpl,a
      0059D7 22               [24] 2740 	ret
                                   2741 ;------------------------------------------------------------
                                   2742 ;Allocation info for local variables in function 'UART_Proc_ModifiyKissSpecialCharacters'
                                   2743 ;------------------------------------------------------------
                                   2744 ;RxLength                  Allocated with name '_UART_Proc_ModifiyKissSpecialCharacters_PARM_2'
                                   2745 ;UartRxBuffer              Allocated with name '_UART_Proc_ModifiyKissSpecialCharacters_UartRxBuffer_1_250'
                                   2746 ;i                         Allocated with name '_UART_Proc_ModifiyKissSpecialCharacters_i_1_251'
                                   2747 ;j                         Allocated with name '_UART_Proc_ModifiyKissSpecialCharacters_j_1_251'
                                   2748 ;sloc0                     Allocated with name '_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0'
                                   2749 ;sloc1                     Allocated with name '_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0'
                                   2750 ;sloc2                     Allocated with name '_UART_Proc_ModifiyKissSpecialCharacters_sloc2_1_0'
                                   2751 ;sloc3                     Allocated with name '_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0'
                                   2752 ;------------------------------------------------------------
                                   2753 ;	..\src\COMMON\UartComProc.c:131: void UART_Proc_ModifiyKissSpecialCharacters(uint8_t *UartRxBuffer, uint8_t *RxLength)
                                   2754 ;	-----------------------------------------
                                   2755 ;	 function UART_Proc_ModifiyKissSpecialCharacters
                                   2756 ;	-----------------------------------------
      0059D8                       2757 _UART_Proc_ModifiyKissSpecialCharacters:
      0059D8 AF F0            [24] 2758 	mov	r7,b
      0059DA AE 83            [24] 2759 	mov	r6,dph
      0059DC E5 82            [12] 2760 	mov	a,dpl
      0059DE 90 03 9F         [24] 2761 	mov	dptr,#_UART_Proc_ModifiyKissSpecialCharacters_UartRxBuffer_1_250
      0059E1 F0               [24] 2762 	movx	@dptr,a
      0059E2 EE               [12] 2763 	mov	a,r6
      0059E3 A3               [24] 2764 	inc	dptr
      0059E4 F0               [24] 2765 	movx	@dptr,a
      0059E5 EF               [12] 2766 	mov	a,r7
      0059E6 A3               [24] 2767 	inc	dptr
      0059E7 F0               [24] 2768 	movx	@dptr,a
                                   2769 ;	..\src\COMMON\UartComProc.c:135: for(i=0;i<*RxLength;i++)
      0059E8 90 03 9F         [24] 2770 	mov	dptr,#_UART_Proc_ModifiyKissSpecialCharacters_UartRxBuffer_1_250
      0059EB E0               [24] 2771 	movx	a,@dptr
      0059EC F5 6D            [12] 2772 	mov	_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0,a
      0059EE A3               [24] 2773 	inc	dptr
      0059EF E0               [24] 2774 	movx	a,@dptr
      0059F0 F5 6E            [12] 2775 	mov	(_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0 + 1),a
      0059F2 A3               [24] 2776 	inc	dptr
      0059F3 E0               [24] 2777 	movx	a,@dptr
      0059F4 F5 6F            [12] 2778 	mov	(_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0 + 2),a
      0059F6 7C 00            [12] 2779 	mov	r4,#0x00
      0059F8                       2780 00118$:
      0059F8 90 03 9C         [24] 2781 	mov	dptr,#_UART_Proc_ModifiyKissSpecialCharacters_PARM_2
      0059FB E0               [24] 2782 	movx	a,@dptr
      0059FC F9               [12] 2783 	mov	r1,a
      0059FD A3               [24] 2784 	inc	dptr
      0059FE E0               [24] 2785 	movx	a,@dptr
      0059FF FA               [12] 2786 	mov	r2,a
      005A00 A3               [24] 2787 	inc	dptr
      005A01 E0               [24] 2788 	movx	a,@dptr
      005A02 FB               [12] 2789 	mov	r3,a
      005A03 89 82            [24] 2790 	mov	dpl,r1
      005A05 8A 83            [24] 2791 	mov	dph,r2
      005A07 8B F0            [24] 2792 	mov	b,r3
      005A09 12 96 F0         [24] 2793 	lcall	__gptrget
      005A0C F8               [12] 2794 	mov	r0,a
      005A0D C3               [12] 2795 	clr	c
      005A0E EC               [12] 2796 	mov	a,r4
      005A0F 98               [12] 2797 	subb	a,r0
      005A10 40 01            [24] 2798 	jc	00152$
      005A12 22               [24] 2799 	ret
      005A13                       2800 00152$:
                                   2801 ;	..\src\COMMON\UartComProc.c:137: if (*(UartRxBuffer+i) == FRAME_SCAPE)
      005A13 EC               [12] 2802 	mov	a,r4
      005A14 25 6D            [12] 2803 	add	a,_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0
      005A16 F5 6A            [12] 2804 	mov	_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0,a
      005A18 E4               [12] 2805 	clr	a
      005A19 35 6E            [12] 2806 	addc	a,(_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0 + 1)
      005A1B F5 6B            [12] 2807 	mov	(_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0 + 1),a
      005A1D 85 6F 6C         [24] 2808 	mov	(_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0 + 2),(_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0 + 2)
      005A20 85 6A 82         [24] 2809 	mov	dpl,_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0
      005A23 85 6B 83         [24] 2810 	mov	dph,(_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0 + 1)
      005A26 85 6C F0         [24] 2811 	mov	b,(_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0 + 2)
      005A29 12 96 F0         [24] 2812 	lcall	__gptrget
      005A2C F8               [12] 2813 	mov	r0,a
      005A2D B8 DB 02         [24] 2814 	cjne	r0,#0xdb,00153$
      005A30 80 03            [24] 2815 	sjmp	00154$
      005A32                       2816 00153$:
      005A32 02 5B 21         [24] 2817 	ljmp	00119$
      005A35                       2818 00154$:
                                   2819 ;	..\src\COMMON\UartComProc.c:139: if(*(UartRxBuffer+i+1)== TRANSPOSE_FRAME_END)
      005A35 C0 04            [24] 2820 	push	ar4
      005A37 74 01            [12] 2821 	mov	a,#0x01
      005A39 25 6A            [12] 2822 	add	a,_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0
      005A3B F8               [12] 2823 	mov	r0,a
      005A3C E4               [12] 2824 	clr	a
      005A3D 35 6B            [12] 2825 	addc	a,(_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0 + 1)
      005A3F FC               [12] 2826 	mov	r4,a
      005A40 AF 6C            [24] 2827 	mov	r7,(_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0 + 2)
      005A42 88 82            [24] 2828 	mov	dpl,r0
      005A44 8C 83            [24] 2829 	mov	dph,r4
      005A46 8F F0            [24] 2830 	mov	b,r7
      005A48 12 96 F0         [24] 2831 	lcall	__gptrget
      005A4B F5 70            [12] 2832 	mov	_UART_Proc_ModifiyKissSpecialCharacters_sloc2_1_0,a
      005A4D 74 DC            [12] 2833 	mov	a,#0xdc
      005A4F B5 70 02         [24] 2834 	cjne	a,_UART_Proc_ModifiyKissSpecialCharacters_sloc2_1_0,00155$
      005A52 80 04            [24] 2835 	sjmp	00156$
      005A54                       2836 00155$:
      005A54 D0 04            [24] 2837 	pop	ar4
      005A56 80 64            [24] 2838 	sjmp	00106$
      005A58                       2839 00156$:
      005A58 D0 04            [24] 2840 	pop	ar4
                                   2841 ;	..\src\COMMON\UartComProc.c:141: *(UartRxBuffer+i) = FRAME_END;
      005A5A 85 6A 82         [24] 2842 	mov	dpl,_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0
      005A5D 85 6B 83         [24] 2843 	mov	dph,(_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0 + 1)
      005A60 85 6C F0         [24] 2844 	mov	b,(_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0 + 2)
      005A63 74 C0            [12] 2845 	mov	a,#0xc0
      005A65 12 87 88         [24] 2846 	lcall	__gptrput
                                   2847 ;	..\src\COMMON\UartComProc.c:142: for(j=i+1;j<*RxLength;j++)
      005A68 EC               [12] 2848 	mov	a,r4
      005A69 04               [12] 2849 	inc	a
      005A6A FF               [12] 2850 	mov	r7,a
      005A6B                       2851 00112$:
      005A6B 89 82            [24] 2852 	mov	dpl,r1
      005A6D 8A 83            [24] 2853 	mov	dph,r2
      005A6F 8B F0            [24] 2854 	mov	b,r3
      005A71 12 96 F0         [24] 2855 	lcall	__gptrget
      005A74 FE               [12] 2856 	mov	r6,a
      005A75 C3               [12] 2857 	clr	c
      005A76 EF               [12] 2858 	mov	a,r7
      005A77 9E               [12] 2859 	subb	a,r6
      005A78 50 35            [24] 2860 	jnc	00101$
                                   2861 ;	..\src\COMMON\UartComProc.c:144: *(UartRxBuffer+j) = *(UartRxBuffer+j+1);
      005A7A C0 04            [24] 2862 	push	ar4
      005A7C EF               [12] 2863 	mov	a,r7
      005A7D 25 6D            [12] 2864 	add	a,_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0
      005A7F F5 71            [12] 2865 	mov	_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0,a
      005A81 E4               [12] 2866 	clr	a
      005A82 35 6E            [12] 2867 	addc	a,(_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0 + 1)
      005A84 F5 72            [12] 2868 	mov	(_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0 + 1),a
      005A86 85 6F 73         [24] 2869 	mov	(_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0 + 2),(_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0 + 2)
      005A89 74 01            [12] 2870 	mov	a,#0x01
      005A8B 25 71            [12] 2871 	add	a,_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0
      005A8D F8               [12] 2872 	mov	r0,a
      005A8E E4               [12] 2873 	clr	a
      005A8F 35 72            [12] 2874 	addc	a,(_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0 + 1)
      005A91 FC               [12] 2875 	mov	r4,a
      005A92 AD 73            [24] 2876 	mov	r5,(_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0 + 2)
      005A94 88 82            [24] 2877 	mov	dpl,r0
      005A96 8C 83            [24] 2878 	mov	dph,r4
      005A98 8D F0            [24] 2879 	mov	b,r5
      005A9A 12 96 F0         [24] 2880 	lcall	__gptrget
      005A9D F8               [12] 2881 	mov	r0,a
      005A9E 85 71 82         [24] 2882 	mov	dpl,_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0
      005AA1 85 72 83         [24] 2883 	mov	dph,(_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0 + 1)
      005AA4 85 73 F0         [24] 2884 	mov	b,(_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0 + 2)
      005AA7 12 87 88         [24] 2885 	lcall	__gptrput
                                   2886 ;	..\src\COMMON\UartComProc.c:142: for(j=i+1;j<*RxLength;j++)
      005AAA 0F               [12] 2887 	inc	r7
      005AAB D0 04            [24] 2888 	pop	ar4
      005AAD 80 BC            [24] 2889 	sjmp	00112$
      005AAF                       2890 00101$:
                                   2891 ;	..\src\COMMON\UartComProc.c:146: *RxLength= *RxLength -1; //suprimi un caracter por lo cual el mensaje se disminuye su tama�o en 1
      005AAF 1E               [12] 2892 	dec	r6
      005AB0 89 82            [24] 2893 	mov	dpl,r1
      005AB2 8A 83            [24] 2894 	mov	dph,r2
      005AB4 8B F0            [24] 2895 	mov	b,r3
      005AB6 EE               [12] 2896 	mov	a,r6
      005AB7 12 87 88         [24] 2897 	lcall	__gptrput
      005ABA 80 65            [24] 2898 	sjmp	00119$
      005ABC                       2899 00106$:
                                   2900 ;	..\src\COMMON\UartComProc.c:148: else if(*(UartRxBuffer+i+1) == TRANSPOSE_FRAME_ESCAPE)
      005ABC 74 DD            [12] 2901 	mov	a,#0xdd
      005ABE B5 70 60         [24] 2902 	cjne	a,_UART_Proc_ModifiyKissSpecialCharacters_sloc2_1_0,00119$
                                   2903 ;	..\src\COMMON\UartComProc.c:150: *(UartRxBuffer+i) = FRAME_SCAPE;
      005AC1 85 6A 82         [24] 2904 	mov	dpl,_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0
      005AC4 85 6B 83         [24] 2905 	mov	dph,(_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0 + 1)
      005AC7 85 6C F0         [24] 2906 	mov	b,(_UART_Proc_ModifiyKissSpecialCharacters_sloc0_1_0 + 2)
      005ACA 74 DB            [12] 2907 	mov	a,#0xdb
      005ACC 12 87 88         [24] 2908 	lcall	__gptrput
                                   2909 ;	..\src\COMMON\UartComProc.c:151: for(j=i+1;j<*RxLength;j++)
      005ACF EC               [12] 2910 	mov	a,r4
      005AD0 04               [12] 2911 	inc	a
      005AD1 FF               [12] 2912 	mov	r7,a
      005AD2                       2913 00115$:
      005AD2 89 82            [24] 2914 	mov	dpl,r1
      005AD4 8A 83            [24] 2915 	mov	dph,r2
      005AD6 8B F0            [24] 2916 	mov	b,r3
      005AD8 12 96 F0         [24] 2917 	lcall	__gptrget
      005ADB FE               [12] 2918 	mov	r6,a
      005ADC C3               [12] 2919 	clr	c
      005ADD EF               [12] 2920 	mov	a,r7
      005ADE 9E               [12] 2921 	subb	a,r6
      005ADF 50 35            [24] 2922 	jnc	00102$
                                   2923 ;	..\src\COMMON\UartComProc.c:153: *(UartRxBuffer+j) = *(UartRxBuffer+j+1);
      005AE1 C0 04            [24] 2924 	push	ar4
      005AE3 EF               [12] 2925 	mov	a,r7
      005AE4 25 6D            [12] 2926 	add	a,_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0
      005AE6 F5 71            [12] 2927 	mov	_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0,a
      005AE8 E4               [12] 2928 	clr	a
      005AE9 35 6E            [12] 2929 	addc	a,(_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0 + 1)
      005AEB F5 72            [12] 2930 	mov	(_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0 + 1),a
      005AED 85 6F 73         [24] 2931 	mov	(_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0 + 2),(_UART_Proc_ModifiyKissSpecialCharacters_sloc1_1_0 + 2)
      005AF0 74 01            [12] 2932 	mov	a,#0x01
      005AF2 25 71            [12] 2933 	add	a,_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0
      005AF4 F8               [12] 2934 	mov	r0,a
      005AF5 E4               [12] 2935 	clr	a
      005AF6 35 72            [12] 2936 	addc	a,(_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0 + 1)
      005AF8 FC               [12] 2937 	mov	r4,a
      005AF9 AD 73            [24] 2938 	mov	r5,(_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0 + 2)
      005AFB 88 82            [24] 2939 	mov	dpl,r0
      005AFD 8C 83            [24] 2940 	mov	dph,r4
      005AFF 8D F0            [24] 2941 	mov	b,r5
      005B01 12 96 F0         [24] 2942 	lcall	__gptrget
      005B04 F8               [12] 2943 	mov	r0,a
      005B05 85 71 82         [24] 2944 	mov	dpl,_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0
      005B08 85 72 83         [24] 2945 	mov	dph,(_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0 + 1)
      005B0B 85 73 F0         [24] 2946 	mov	b,(_UART_Proc_ModifiyKissSpecialCharacters_sloc3_1_0 + 2)
      005B0E 12 87 88         [24] 2947 	lcall	__gptrput
                                   2948 ;	..\src\COMMON\UartComProc.c:151: for(j=i+1;j<*RxLength;j++)
      005B11 0F               [12] 2949 	inc	r7
      005B12 D0 04            [24] 2950 	pop	ar4
      005B14 80 BC            [24] 2951 	sjmp	00115$
      005B16                       2952 00102$:
                                   2953 ;	..\src\COMMON\UartComProc.c:155: *RxLength = *RxLength -1; //suprimi un caracter por lo cual el mensaje se disminuye su tama�o en 1
      005B16 1E               [12] 2954 	dec	r6
      005B17 89 82            [24] 2955 	mov	dpl,r1
      005B19 8A 83            [24] 2956 	mov	dph,r2
      005B1B 8B F0            [24] 2957 	mov	b,r3
      005B1D EE               [12] 2958 	mov	a,r6
      005B1E 12 87 88         [24] 2959 	lcall	__gptrput
      005B21                       2960 00119$:
                                   2961 ;	..\src\COMMON\UartComProc.c:135: for(i=0;i<*RxLength;i++)
      005B21 0C               [12] 2962 	inc	r4
      005B22 02 59 F8         [24] 2963 	ljmp	00118$
                                   2964 ;------------------------------------------------------------
                                   2965 ;Allocation info for local variables in function 'UART_Proc_SendMessage'
                                   2966 ;------------------------------------------------------------
                                   2967 ;sloc0                     Allocated with name '_UART_Proc_SendMessage_sloc0_1_0'
                                   2968 ;length                    Allocated with name '_UART_Proc_SendMessage_PARM_2'
                                   2969 ;command                   Allocated with name '_UART_Proc_SendMessage_PARM_3'
                                   2970 ;payload                   Allocated with name '_UART_Proc_SendMessage_payload_1_258'
                                   2971 ;i                         Allocated with name '_UART_Proc_SendMessage_i_1_259'
                                   2972 ;RetVal                    Allocated with name '_UART_Proc_SendMessage_RetVal_1_259'
                                   2973 ;CrcBuffer                 Allocated with name '_UART_Proc_SendMessage_CrcBuffer_1_259'
                                   2974 ;CRC32                     Allocated with name '_UART_Proc_SendMessage_CRC32_1_259'
                                   2975 ;------------------------------------------------------------
                                   2976 ;	..\src\COMMON\UartComProc.c:181: uint8_t UART_Proc_SendMessage(uint8_t *payload, uint8_t length, uint8_t command)
                                   2977 ;	-----------------------------------------
                                   2978 ;	 function UART_Proc_SendMessage
                                   2979 ;	-----------------------------------------
      005B25                       2980 _UART_Proc_SendMessage:
      005B25 AF F0            [24] 2981 	mov	r7,b
      005B27 AE 83            [24] 2982 	mov	r6,dph
      005B29 E5 82            [12] 2983 	mov	a,dpl
      005B2B 90 03 A4         [24] 2984 	mov	dptr,#_UART_Proc_SendMessage_payload_1_258
      005B2E F0               [24] 2985 	movx	@dptr,a
      005B2F EE               [12] 2986 	mov	a,r6
      005B30 A3               [24] 2987 	inc	dptr
      005B31 F0               [24] 2988 	movx	@dptr,a
      005B32 EF               [12] 2989 	mov	a,r7
      005B33 A3               [24] 2990 	inc	dptr
      005B34 F0               [24] 2991 	movx	@dptr,a
                                   2992 ;	..\src\COMMON\UartComProc.c:187: CrcBuffer = (uint8_t) malloc(sizeof(uint8_t)*(length+1));
      005B35 90 03 A2         [24] 2993 	mov	dptr,#_UART_Proc_SendMessage_PARM_2
      005B38 E0               [24] 2994 	movx	a,@dptr
      005B39 FF               [12] 2995 	mov	r7,a
      005B3A FD               [12] 2996 	mov	r5,a
      005B3B 7E 00            [12] 2997 	mov	r6,#0x00
      005B3D 0D               [12] 2998 	inc	r5
      005B3E BD 00 01         [24] 2999 	cjne	r5,#0x00,00114$
      005B41 0E               [12] 3000 	inc	r6
      005B42                       3001 00114$:
      005B42 8D 82            [24] 3002 	mov	dpl,r5
      005B44 8E 83            [24] 3003 	mov	dph,r6
      005B46 C0 07            [24] 3004 	push	ar7
      005B48 12 89 2E         [24] 3005 	lcall	_malloc
      005B4B AD 82            [24] 3006 	mov	r5,dpl
      005B4D D0 07            [24] 3007 	pop	ar7
      005B4F 7E 00            [12] 3008 	mov	r6,#0x00
                                   3009 ;	..\src\COMMON\UartComProc.c:188: *CrcBuffer = command;
      005B51 90 03 A3         [24] 3010 	mov	dptr,#_UART_Proc_SendMessage_PARM_3
      005B54 E0               [24] 3011 	movx	a,@dptr
      005B55 FC               [12] 3012 	mov	r4,a
      005B56 8D 82            [24] 3013 	mov	dpl,r5
      005B58 8E 83            [24] 3014 	mov	dph,r6
      005B5A F0               [24] 3015 	movx	@dptr,a
                                   3016 ;	..\src\COMMON\UartComProc.c:190: memcpy(CrcBuffer+1,payload,length);
      005B5B 74 01            [12] 3017 	mov	a,#0x01
      005B5D 2D               [12] 3018 	add	a,r5
      005B5E FA               [12] 3019 	mov	r2,a
      005B5F E4               [12] 3020 	clr	a
      005B60 3E               [12] 3021 	addc	a,r6
      005B61 F9               [12] 3022 	mov	r1,a
      005B62 7B 00            [12] 3023 	mov	r3,#0x00
      005B64 90 03 A4         [24] 3024 	mov	dptr,#_UART_Proc_SendMessage_payload_1_258
      005B67 E0               [24] 3025 	movx	a,@dptr
      005B68 F5 2A            [12] 3026 	mov	_UART_Proc_SendMessage_sloc0_1_0,a
      005B6A A3               [24] 3027 	inc	dptr
      005B6B E0               [24] 3028 	movx	a,@dptr
      005B6C F5 2B            [12] 3029 	mov	(_UART_Proc_SendMessage_sloc0_1_0 + 1),a
      005B6E A3               [24] 3030 	inc	dptr
      005B6F E0               [24] 3031 	movx	a,@dptr
      005B70 F5 2C            [12] 3032 	mov	(_UART_Proc_SendMessage_sloc0_1_0 + 2),a
      005B72 90 04 3D         [24] 3033 	mov	dptr,#_memcpy_PARM_2
      005B75 E5 2A            [12] 3034 	mov	a,_UART_Proc_SendMessage_sloc0_1_0
      005B77 F0               [24] 3035 	movx	@dptr,a
      005B78 E5 2B            [12] 3036 	mov	a,(_UART_Proc_SendMessage_sloc0_1_0 + 1)
      005B7A A3               [24] 3037 	inc	dptr
      005B7B F0               [24] 3038 	movx	@dptr,a
      005B7C E5 2C            [12] 3039 	mov	a,(_UART_Proc_SendMessage_sloc0_1_0 + 2)
      005B7E A3               [24] 3040 	inc	dptr
      005B7F F0               [24] 3041 	movx	@dptr,a
      005B80 90 04 40         [24] 3042 	mov	dptr,#_memcpy_PARM_3
      005B83 EF               [12] 3043 	mov	a,r7
      005B84 F0               [24] 3044 	movx	@dptr,a
      005B85 E4               [12] 3045 	clr	a
      005B86 A3               [24] 3046 	inc	dptr
      005B87 F0               [24] 3047 	movx	@dptr,a
      005B88 8A 82            [24] 3048 	mov	dpl,r2
      005B8A 89 83            [24] 3049 	mov	dph,r1
      005B8C 8B F0            [24] 3050 	mov	b,r3
      005B8E C0 07            [24] 3051 	push	ar7
      005B90 C0 06            [24] 3052 	push	ar6
      005B92 C0 05            [24] 3053 	push	ar5
      005B94 C0 04            [24] 3054 	push	ar4
      005B96 12 83 01         [24] 3055 	lcall	_memcpy
      005B99 D0 04            [24] 3056 	pop	ar4
      005B9B D0 05            [24] 3057 	pop	ar5
      005B9D D0 06            [24] 3058 	pop	ar6
      005B9F D0 07            [24] 3059 	pop	ar7
                                   3060 ;	..\src\COMMON\UartComProc.c:191: CRC32 = UART_Calc_CRC32(CrcBuffer,length+1,0);
      005BA1 7B 00            [12] 3061 	mov	r3,#0x00
      005BA3 90 03 A7         [24] 3062 	mov	dptr,#_UART_Calc_CRC32_PARM_2
      005BA6 EF               [12] 3063 	mov	a,r7
      005BA7 04               [12] 3064 	inc	a
      005BA8 F0               [24] 3065 	movx	@dptr,a
      005BA9 90 03 A8         [24] 3066 	mov	dptr,#_UART_Calc_CRC32_PARM_3
      005BAC E4               [12] 3067 	clr	a
      005BAD F0               [24] 3068 	movx	@dptr,a
      005BAE 8D 82            [24] 3069 	mov	dpl,r5
      005BB0 8E 83            [24] 3070 	mov	dph,r6
      005BB2 8B F0            [24] 3071 	mov	b,r3
      005BB4 C0 07            [24] 3072 	push	ar7
      005BB6 C0 04            [24] 3073 	push	ar4
      005BB8 12 5C 2E         [24] 3074 	lcall	_UART_Calc_CRC32
      005BBB AA 82            [24] 3075 	mov	r2,dpl
      005BBD AB 83            [24] 3076 	mov	r3,dph
      005BBF AD F0            [24] 3077 	mov	r5,b
      005BC1 FE               [12] 3078 	mov	r6,a
      005BC2 D0 04            [24] 3079 	pop	ar4
      005BC4 D0 07            [24] 3080 	pop	ar7
                                   3081 ;	..\src\COMMON\UartComProc.c:192: uart0_tx(FRAME_START);
      005BC6 75 82 C0         [24] 3082 	mov	dpl,#0xc0
      005BC9 12 88 43         [24] 3083 	lcall	_uart0_tx
                                   3084 ;	..\src\COMMON\UartComProc.c:193: uart0_tx(command);
      005BCC 8C 82            [24] 3085 	mov	dpl,r4
      005BCE 12 88 43         [24] 3086 	lcall	_uart0_tx
                                   3087 ;	..\src\COMMON\UartComProc.c:194: for(i=0;i<length;i++)
      005BD1 7C 00            [12] 3088 	mov	r4,#0x00
      005BD3                       3089 00103$:
      005BD3 C3               [12] 3090 	clr	c
      005BD4 EC               [12] 3091 	mov	a,r4
      005BD5 9F               [12] 3092 	subb	a,r7
      005BD6 50 2C            [24] 3093 	jnc	00101$
                                   3094 ;	..\src\COMMON\UartComProc.c:196: uart0_tx(*(payload+i));
      005BD8 C0 02            [24] 3095 	push	ar2
      005BDA C0 03            [24] 3096 	push	ar3
      005BDC C0 05            [24] 3097 	push	ar5
      005BDE C0 06            [24] 3098 	push	ar6
      005BE0 EC               [12] 3099 	mov	a,r4
      005BE1 25 2A            [12] 3100 	add	a,_UART_Proc_SendMessage_sloc0_1_0
      005BE3 F8               [12] 3101 	mov	r0,a
      005BE4 E4               [12] 3102 	clr	a
      005BE5 35 2B            [12] 3103 	addc	a,(_UART_Proc_SendMessage_sloc0_1_0 + 1)
      005BE7 F9               [12] 3104 	mov	r1,a
      005BE8 AE 2C            [24] 3105 	mov	r6,(_UART_Proc_SendMessage_sloc0_1_0 + 2)
      005BEA 88 82            [24] 3106 	mov	dpl,r0
      005BEC 89 83            [24] 3107 	mov	dph,r1
      005BEE 8E F0            [24] 3108 	mov	b,r6
      005BF0 12 96 F0         [24] 3109 	lcall	__gptrget
      005BF3 F8               [12] 3110 	mov	r0,a
      005BF4 F5 82            [12] 3111 	mov	dpl,a
      005BF6 12 88 43         [24] 3112 	lcall	_uart0_tx
                                   3113 ;	..\src\COMMON\UartComProc.c:194: for(i=0;i<length;i++)
      005BF9 0C               [12] 3114 	inc	r4
      005BFA D0 06            [24] 3115 	pop	ar6
      005BFC D0 05            [24] 3116 	pop	ar5
      005BFE D0 03            [24] 3117 	pop	ar3
      005C00 D0 02            [24] 3118 	pop	ar2
      005C02 80 CF            [24] 3119 	sjmp	00103$
      005C04                       3120 00101$:
                                   3121 ;	..\src\COMMON\UartComProc.c:198: uart0_tx((uint8_t)((CRC32 & 0xFF000000) >>24));
      005C04 8E 07            [24] 3122 	mov	ar7,r6
      005C06 8F 00            [24] 3123 	mov	ar0,r7
      005C08 88 82            [24] 3124 	mov	dpl,r0
      005C0A 12 88 43         [24] 3125 	lcall	_uart0_tx
                                   3126 ;	..\src\COMMON\UartComProc.c:199: uart0_tx((uint8_t)((CRC32 & 0x00FF0000) >>16));
      005C0D 8D 04            [24] 3127 	mov	ar4,r5
      005C0F 8C 00            [24] 3128 	mov	ar0,r4
      005C11 88 82            [24] 3129 	mov	dpl,r0
      005C13 12 88 43         [24] 3130 	lcall	_uart0_tx
                                   3131 ;	..\src\COMMON\UartComProc.c:200: uart0_tx((uint8_t)((CRC32 & 0x0000FF00) >>8));
      005C16 8B 01            [24] 3132 	mov	ar1,r3
      005C18 89 00            [24] 3133 	mov	ar0,r1
      005C1A 88 82            [24] 3134 	mov	dpl,r0
      005C1C 12 88 43         [24] 3135 	lcall	_uart0_tx
                                   3136 ;	..\src\COMMON\UartComProc.c:201: uart0_tx((uint8_t)(CRC32 & 0x000000FF));
      005C1F 8A 82            [24] 3137 	mov	dpl,r2
      005C21 12 88 43         [24] 3138 	lcall	_uart0_tx
                                   3139 ;	..\src\COMMON\UartComProc.c:203: uart0_tx(FRAME_END);
      005C24 75 82 C0         [24] 3140 	mov	dpl,#0xc0
      005C27 12 88 43         [24] 3141 	lcall	_uart0_tx
                                   3142 ;	..\src\COMMON\UartComProc.c:211: return RetVal;
      005C2A 75 82 00         [24] 3143 	mov	dpl,#0x00
      005C2D 22               [24] 3144 	ret
                                   3145 ;------------------------------------------------------------
                                   3146 ;Allocation info for local variables in function 'UART_Calc_CRC32'
                                   3147 ;------------------------------------------------------------
                                   3148 ;length                    Allocated with name '_UART_Calc_CRC32_PARM_2'
                                   3149 ;littleEndian              Allocated with name '_UART_Calc_CRC32_PARM_3'
                                   3150 ;message                   Allocated with name '_UART_Calc_CRC32_message_1_261'
                                   3151 ;i                         Allocated with name '_UART_Calc_CRC32_i_1_262'
                                   3152 ;j                         Allocated with name '_UART_Calc_CRC32_j_1_262'
                                   3153 ;byte                      Allocated with name '_UART_Calc_CRC32_byte_1_262'
                                   3154 ;crc                       Allocated with name '_UART_Calc_CRC32_crc_1_262'
                                   3155 ;mask                      Allocated with name '_UART_Calc_CRC32_mask_1_262'
                                   3156 ;sloc0                     Allocated with name '_UART_Calc_CRC32_sloc0_1_0'
                                   3157 ;sloc1                     Allocated with name '_UART_Calc_CRC32_sloc1_1_0'
                                   3158 ;sloc2                     Allocated with name '_UART_Calc_CRC32_sloc2_1_0'
                                   3159 ;------------------------------------------------------------
                                   3160 ;	..\src\COMMON\UartComProc.c:235: uint32_t UART_Calc_CRC32(uint8_t *message,uint8_t length,uint8_t littleEndian) {
                                   3161 ;	-----------------------------------------
                                   3162 ;	 function UART_Calc_CRC32
                                   3163 ;	-----------------------------------------
      005C2E                       3164 _UART_Calc_CRC32:
      005C2E AF F0            [24] 3165 	mov	r7,b
      005C30 AE 83            [24] 3166 	mov	r6,dph
      005C32 E5 82            [12] 3167 	mov	a,dpl
      005C34 90 03 A9         [24] 3168 	mov	dptr,#_UART_Calc_CRC32_message_1_261
      005C37 F0               [24] 3169 	movx	@dptr,a
      005C38 EE               [12] 3170 	mov	a,r6
      005C39 A3               [24] 3171 	inc	dptr
      005C3A F0               [24] 3172 	movx	@dptr,a
      005C3B EF               [12] 3173 	mov	a,r7
      005C3C A3               [24] 3174 	inc	dptr
      005C3D F0               [24] 3175 	movx	@dptr,a
                                   3176 ;	..\src\COMMON\UartComProc.c:240: crc = 0xFFFFFFFF;
      005C3E 90 03 AC         [24] 3177 	mov	dptr,#_UART_Calc_CRC32_crc_1_262
      005C41 74 FF            [12] 3178 	mov	a,#0xff
      005C43 F0               [24] 3179 	movx	@dptr,a
      005C44 A3               [24] 3180 	inc	dptr
      005C45 F0               [24] 3181 	movx	@dptr,a
      005C46 A3               [24] 3182 	inc	dptr
      005C47 F0               [24] 3183 	movx	@dptr,a
      005C48 A3               [24] 3184 	inc	dptr
      005C49 F0               [24] 3185 	movx	@dptr,a
                                   3186 ;	..\src\COMMON\UartComProc.c:241: while (i < length) {
      005C4A 90 03 A9         [24] 3187 	mov	dptr,#_UART_Calc_CRC32_message_1_261
      005C4D E0               [24] 3188 	movx	a,@dptr
      005C4E F5 6A            [12] 3189 	mov	_UART_Calc_CRC32_sloc0_1_0,a
      005C50 A3               [24] 3190 	inc	dptr
      005C51 E0               [24] 3191 	movx	a,@dptr
      005C52 F5 6B            [12] 3192 	mov	(_UART_Calc_CRC32_sloc0_1_0 + 1),a
      005C54 A3               [24] 3193 	inc	dptr
      005C55 E0               [24] 3194 	movx	a,@dptr
      005C56 F5 6C            [12] 3195 	mov	(_UART_Calc_CRC32_sloc0_1_0 + 2),a
      005C58 90 03 A7         [24] 3196 	mov	dptr,#_UART_Calc_CRC32_PARM_2
      005C5B E0               [24] 3197 	movx	a,@dptr
      005C5C FC               [12] 3198 	mov	r4,a
      005C5D E4               [12] 3199 	clr	a
      005C5E F5 71            [12] 3200 	mov	_UART_Calc_CRC32_sloc2_1_0,a
      005C60 F5 72            [12] 3201 	mov	(_UART_Calc_CRC32_sloc2_1_0 + 1),a
      005C62                       3202 00102$:
      005C62 8C 00            [24] 3203 	mov	ar0,r4
      005C64 79 00            [12] 3204 	mov	r1,#0x00
      005C66 C3               [12] 3205 	clr	c
      005C67 E5 71            [12] 3206 	mov	a,_UART_Calc_CRC32_sloc2_1_0
      005C69 98               [12] 3207 	subb	a,r0
      005C6A E5 72            [12] 3208 	mov	a,(_UART_Calc_CRC32_sloc2_1_0 + 1)
      005C6C 64 80            [12] 3209 	xrl	a,#0x80
      005C6E 89 F0            [24] 3210 	mov	b,r1
      005C70 63 F0 80         [24] 3211 	xrl	b,#0x80
      005C73 95 F0            [12] 3212 	subb	a,b
      005C75 40 03            [24] 3213 	jc	00128$
      005C77 02 5D 36         [24] 3214 	ljmp	00104$
      005C7A                       3215 00128$:
                                   3216 ;	..\src\COMMON\UartComProc.c:242: byte = message[i];
      005C7A C0 04            [24] 3217 	push	ar4
      005C7C E5 71            [12] 3218 	mov	a,_UART_Calc_CRC32_sloc2_1_0
      005C7E 25 6A            [12] 3219 	add	a,_UART_Calc_CRC32_sloc0_1_0
      005C80 F8               [12] 3220 	mov	r0,a
      005C81 E5 72            [12] 3221 	mov	a,(_UART_Calc_CRC32_sloc2_1_0 + 1)
      005C83 35 6B            [12] 3222 	addc	a,(_UART_Calc_CRC32_sloc0_1_0 + 1)
      005C85 F9               [12] 3223 	mov	r1,a
      005C86 AC 6C            [24] 3224 	mov	r4,(_UART_Calc_CRC32_sloc0_1_0 + 2)
      005C88 88 82            [24] 3225 	mov	dpl,r0
      005C8A 89 83            [24] 3226 	mov	dph,r1
      005C8C 8C F0            [24] 3227 	mov	b,r4
      005C8E 12 96 F0         [24] 3228 	lcall	__gptrget
      005C91 F8               [12] 3229 	mov	r0,a
      005C92 88 6D            [24] 3230 	mov	_UART_Calc_CRC32_sloc1_1_0,r0
      005C94 75 6E 00         [24] 3231 	mov	(_UART_Calc_CRC32_sloc1_1_0 + 1),#0x00
      005C97 75 6F 00         [24] 3232 	mov	(_UART_Calc_CRC32_sloc1_1_0 + 2),#0x00
      005C9A 75 70 00         [24] 3233 	mov	(_UART_Calc_CRC32_sloc1_1_0 + 3),#0x00
                                   3234 ;	..\src\COMMON\UartComProc.c:244: crc = crc ^ byte;
      005C9D 90 03 AC         [24] 3235 	mov	dptr,#_UART_Calc_CRC32_crc_1_262
      005CA0 E0               [24] 3236 	movx	a,@dptr
      005CA1 FC               [12] 3237 	mov	r4,a
      005CA2 A3               [24] 3238 	inc	dptr
      005CA3 E0               [24] 3239 	movx	a,@dptr
      005CA4 FD               [12] 3240 	mov	r5,a
      005CA5 A3               [24] 3241 	inc	dptr
      005CA6 E0               [24] 3242 	movx	a,@dptr
      005CA7 FE               [12] 3243 	mov	r6,a
      005CA8 A3               [24] 3244 	inc	dptr
      005CA9 E0               [24] 3245 	movx	a,@dptr
      005CAA FF               [12] 3246 	mov	r7,a
      005CAB 90 03 AC         [24] 3247 	mov	dptr,#_UART_Calc_CRC32_crc_1_262
      005CAE E5 6D            [12] 3248 	mov	a,_UART_Calc_CRC32_sloc1_1_0
      005CB0 6C               [12] 3249 	xrl	a,r4
      005CB1 F0               [24] 3250 	movx	@dptr,a
      005CB2 E5 6E            [12] 3251 	mov	a,(_UART_Calc_CRC32_sloc1_1_0 + 1)
      005CB4 6D               [12] 3252 	xrl	a,r5
      005CB5 A3               [24] 3253 	inc	dptr
      005CB6 F0               [24] 3254 	movx	@dptr,a
      005CB7 E5 6F            [12] 3255 	mov	a,(_UART_Calc_CRC32_sloc1_1_0 + 2)
      005CB9 6E               [12] 3256 	xrl	a,r6
      005CBA A3               [24] 3257 	inc	dptr
      005CBB F0               [24] 3258 	movx	@dptr,a
      005CBC E5 70            [12] 3259 	mov	a,(_UART_Calc_CRC32_sloc1_1_0 + 3)
      005CBE 6F               [12] 3260 	xrl	a,r7
      005CBF A3               [24] 3261 	inc	dptr
      005CC0 F0               [24] 3262 	movx	@dptr,a
                                   3263 ;	..\src\COMMON\UartComProc.c:245: for (j = 7; j >= 0; j--) {    // Do eight times.
      005CC1 75 6D 07         [24] 3264 	mov	_UART_Calc_CRC32_sloc1_1_0,#0x07
      005CC4 75 6E 00         [24] 3265 	mov	(_UART_Calc_CRC32_sloc1_1_0 + 1),#0x00
                                   3266 ;	..\src\COMMON\UartComProc.c:253: return crc;
      005CC7 D0 04            [24] 3267 	pop	ar4
                                   3268 ;	..\src\COMMON\UartComProc.c:245: for (j = 7; j >= 0; j--) {    // Do eight times.
      005CC9                       3269 00107$:
                                   3270 ;	..\src\COMMON\UartComProc.c:246: mask = -(crc & 1);
      005CC9 C0 04            [24] 3271 	push	ar4
      005CCB 90 03 AC         [24] 3272 	mov	dptr,#_UART_Calc_CRC32_crc_1_262
      005CCE E0               [24] 3273 	movx	a,@dptr
      005CCF F9               [12] 3274 	mov	r1,a
      005CD0 A3               [24] 3275 	inc	dptr
      005CD1 E0               [24] 3276 	movx	a,@dptr
      005CD2 FD               [12] 3277 	mov	r5,a
      005CD3 A3               [24] 3278 	inc	dptr
      005CD4 E0               [24] 3279 	movx	a,@dptr
      005CD5 FE               [12] 3280 	mov	r6,a
      005CD6 A3               [24] 3281 	inc	dptr
      005CD7 E0               [24] 3282 	movx	a,@dptr
      005CD8 FF               [12] 3283 	mov	r7,a
      005CD9 74 01            [12] 3284 	mov	a,#0x01
      005CDB 59               [12] 3285 	anl	a,r1
      005CDC F8               [12] 3286 	mov	r0,a
      005CDD 7A 00            [12] 3287 	mov	r2,#0x00
      005CDF 7B 00            [12] 3288 	mov	r3,#0x00
      005CE1 7C 00            [12] 3289 	mov	r4,#0x00
      005CE3 C3               [12] 3290 	clr	c
      005CE4 E4               [12] 3291 	clr	a
      005CE5 98               [12] 3292 	subb	a,r0
      005CE6 F8               [12] 3293 	mov	r0,a
      005CE7 E4               [12] 3294 	clr	a
      005CE8 9A               [12] 3295 	subb	a,r2
      005CE9 FA               [12] 3296 	mov	r2,a
      005CEA E4               [12] 3297 	clr	a
      005CEB 9B               [12] 3298 	subb	a,r3
      005CEC FB               [12] 3299 	mov	r3,a
      005CED E4               [12] 3300 	clr	a
      005CEE 9C               [12] 3301 	subb	a,r4
      005CEF FC               [12] 3302 	mov	r4,a
                                   3303 ;	..\src\COMMON\UartComProc.c:247: crc = (crc >> 1) ^ (0xEDB88320 & mask);
      005CF0 EF               [12] 3304 	mov	a,r7
      005CF1 C3               [12] 3305 	clr	c
      005CF2 13               [12] 3306 	rrc	a
      005CF3 FF               [12] 3307 	mov	r7,a
      005CF4 EE               [12] 3308 	mov	a,r6
      005CF5 13               [12] 3309 	rrc	a
      005CF6 FE               [12] 3310 	mov	r6,a
      005CF7 ED               [12] 3311 	mov	a,r5
      005CF8 13               [12] 3312 	rrc	a
      005CF9 FD               [12] 3313 	mov	r5,a
      005CFA E9               [12] 3314 	mov	a,r1
      005CFB 13               [12] 3315 	rrc	a
      005CFC F9               [12] 3316 	mov	r1,a
      005CFD 53 00 20         [24] 3317 	anl	ar0,#0x20
      005D00 53 02 83         [24] 3318 	anl	ar2,#0x83
      005D03 53 03 B8         [24] 3319 	anl	ar3,#0xb8
      005D06 53 04 ED         [24] 3320 	anl	ar4,#0xed
      005D09 90 03 AC         [24] 3321 	mov	dptr,#_UART_Calc_CRC32_crc_1_262
      005D0C E8               [12] 3322 	mov	a,r0
      005D0D 69               [12] 3323 	xrl	a,r1
      005D0E F0               [24] 3324 	movx	@dptr,a
      005D0F EA               [12] 3325 	mov	a,r2
      005D10 6D               [12] 3326 	xrl	a,r5
      005D11 A3               [24] 3327 	inc	dptr
      005D12 F0               [24] 3328 	movx	@dptr,a
      005D13 EB               [12] 3329 	mov	a,r3
      005D14 6E               [12] 3330 	xrl	a,r6
      005D15 A3               [24] 3331 	inc	dptr
      005D16 F0               [24] 3332 	movx	@dptr,a
      005D17 EC               [12] 3333 	mov	a,r4
      005D18 6F               [12] 3334 	xrl	a,r7
      005D19 A3               [24] 3335 	inc	dptr
      005D1A F0               [24] 3336 	movx	@dptr,a
                                   3337 ;	..\src\COMMON\UartComProc.c:245: for (j = 7; j >= 0; j--) {    // Do eight times.
      005D1B 15 6D            [12] 3338 	dec	_UART_Calc_CRC32_sloc1_1_0
      005D1D 74 FF            [12] 3339 	mov	a,#0xff
      005D1F B5 6D 02         [24] 3340 	cjne	a,_UART_Calc_CRC32_sloc1_1_0,00129$
      005D22 15 6E            [12] 3341 	dec	(_UART_Calc_CRC32_sloc1_1_0 + 1)
      005D24                       3342 00129$:
      005D24 E5 6E            [12] 3343 	mov	a,(_UART_Calc_CRC32_sloc1_1_0 + 1)
      005D26 D0 04            [24] 3344 	pop	ar4
      005D28 30 E7 9E         [24] 3345 	jnb	acc.7,00107$
                                   3346 ;	..\src\COMMON\UartComProc.c:249: i = i + 1;
      005D2B 05 71            [12] 3347 	inc	_UART_Calc_CRC32_sloc2_1_0
      005D2D E4               [12] 3348 	clr	a
      005D2E B5 71 02         [24] 3349 	cjne	a,_UART_Calc_CRC32_sloc2_1_0,00131$
      005D31 05 72            [12] 3350 	inc	(_UART_Calc_CRC32_sloc2_1_0 + 1)
      005D33                       3351 00131$:
      005D33 02 5C 62         [24] 3352 	ljmp	00102$
      005D36                       3353 00104$:
                                   3354 ;	..\src\COMMON\UartComProc.c:251: crc = ~crc;
      005D36 90 03 AC         [24] 3355 	mov	dptr,#_UART_Calc_CRC32_crc_1_262
      005D39 E0               [24] 3356 	movx	a,@dptr
      005D3A FC               [12] 3357 	mov	r4,a
      005D3B A3               [24] 3358 	inc	dptr
      005D3C E0               [24] 3359 	movx	a,@dptr
      005D3D FD               [12] 3360 	mov	r5,a
      005D3E A3               [24] 3361 	inc	dptr
      005D3F E0               [24] 3362 	movx	a,@dptr
      005D40 FE               [12] 3363 	mov	r6,a
      005D41 A3               [24] 3364 	inc	dptr
      005D42 E0               [24] 3365 	movx	a,@dptr
      005D43 FF               [12] 3366 	mov	r7,a
      005D44 90 03 AC         [24] 3367 	mov	dptr,#_UART_Calc_CRC32_crc_1_262
      005D47 EC               [12] 3368 	mov	a,r4
      005D48 F4               [12] 3369 	cpl	a
      005D49 F0               [24] 3370 	movx	@dptr,a
      005D4A ED               [12] 3371 	mov	a,r5
      005D4B F4               [12] 3372 	cpl	a
      005D4C A3               [24] 3373 	inc	dptr
      005D4D F0               [24] 3374 	movx	@dptr,a
      005D4E EE               [12] 3375 	mov	a,r6
      005D4F F4               [12] 3376 	cpl	a
      005D50 A3               [24] 3377 	inc	dptr
      005D51 F0               [24] 3378 	movx	@dptr,a
      005D52 EF               [12] 3379 	mov	a,r7
      005D53 F4               [12] 3380 	cpl	a
      005D54 A3               [24] 3381 	inc	dptr
      005D55 F0               [24] 3382 	movx	@dptr,a
                                   3383 ;	..\src\COMMON\UartComProc.c:252: if(littleEndian)crc=(((crc & 0x000000FF) << 24)+((crc & 0x0000FF00) << 8)+ ((crc & 0x00FF0000) >> 8)+((crc & 0xFF000000) >> 24));//swap little endian big endian
      005D56 90 03 A8         [24] 3384 	mov	dptr,#_UART_Calc_CRC32_PARM_3
      005D59 E0               [24] 3385 	movx	a,@dptr
      005D5A 70 03            [24] 3386 	jnz	00132$
      005D5C 02 5D D1         [24] 3387 	ljmp	00106$
      005D5F                       3388 00132$:
      005D5F 90 03 AC         [24] 3389 	mov	dptr,#_UART_Calc_CRC32_crc_1_262
      005D62 E0               [24] 3390 	movx	a,@dptr
      005D63 FC               [12] 3391 	mov	r4,a
      005D64 A3               [24] 3392 	inc	dptr
      005D65 E0               [24] 3393 	movx	a,@dptr
      005D66 FD               [12] 3394 	mov	r5,a
      005D67 A3               [24] 3395 	inc	dptr
      005D68 E0               [24] 3396 	movx	a,@dptr
      005D69 FE               [12] 3397 	mov	r6,a
      005D6A A3               [24] 3398 	inc	dptr
      005D6B E0               [24] 3399 	movx	a,@dptr
      005D6C FF               [12] 3400 	mov	r7,a
      005D6D 8C 00            [24] 3401 	mov	ar0,r4
      005D6F 7B 00            [12] 3402 	mov	r3,#0x00
      005D71 88 70            [24] 3403 	mov	(_UART_Calc_CRC32_sloc1_1_0 + 3),r0
                                   3404 ;	1-genFromRTrack replaced	mov	_UART_Calc_CRC32_sloc1_1_0,#0x00
      005D73 8B 6D            [24] 3405 	mov	_UART_Calc_CRC32_sloc1_1_0,r3
                                   3406 ;	1-genFromRTrack replaced	mov	(_UART_Calc_CRC32_sloc1_1_0 + 1),#0x00
      005D75 8B 6E            [24] 3407 	mov	(_UART_Calc_CRC32_sloc1_1_0 + 1),r3
                                   3408 ;	1-genFromRTrack replaced	mov	(_UART_Calc_CRC32_sloc1_1_0 + 2),#0x00
      005D77 8B 6F            [24] 3409 	mov	(_UART_Calc_CRC32_sloc1_1_0 + 2),r3
      005D79 78 00            [12] 3410 	mov	r0,#0x00
      005D7B 8D 01            [24] 3411 	mov	ar1,r5
      005D7D 7A 00            [12] 3412 	mov	r2,#0x00
      005D7F 8A 03            [24] 3413 	mov	ar3,r2
      005D81 89 02            [24] 3414 	mov	ar2,r1
      005D83 88 01            [24] 3415 	mov	ar1,r0
      005D85 E4               [12] 3416 	clr	a
      005D86 25 6D            [12] 3417 	add	a,_UART_Calc_CRC32_sloc1_1_0
      005D88 F5 6D            [12] 3418 	mov	_UART_Calc_CRC32_sloc1_1_0,a
      005D8A E9               [12] 3419 	mov	a,r1
      005D8B 35 6E            [12] 3420 	addc	a,(_UART_Calc_CRC32_sloc1_1_0 + 1)
      005D8D F5 6E            [12] 3421 	mov	(_UART_Calc_CRC32_sloc1_1_0 + 1),a
      005D8F EA               [12] 3422 	mov	a,r2
      005D90 35 6F            [12] 3423 	addc	a,(_UART_Calc_CRC32_sloc1_1_0 + 2)
      005D92 F5 6F            [12] 3424 	mov	(_UART_Calc_CRC32_sloc1_1_0 + 2),a
      005D94 EB               [12] 3425 	mov	a,r3
      005D95 35 70            [12] 3426 	addc	a,(_UART_Calc_CRC32_sloc1_1_0 + 3)
      005D97 F5 70            [12] 3427 	mov	(_UART_Calc_CRC32_sloc1_1_0 + 3),a
      005D99 79 00            [12] 3428 	mov	r1,#0x00
      005D9B 8E 02            [24] 3429 	mov	ar2,r6
      005D9D 7B 00            [12] 3430 	mov	r3,#0x00
      005D9F 89 00            [24] 3431 	mov	ar0,r1
      005DA1 8A 01            [24] 3432 	mov	ar1,r2
      005DA3 8B 02            [24] 3433 	mov	ar2,r3
      005DA5 7B 00            [12] 3434 	mov	r3,#0x00
      005DA7 E8               [12] 3435 	mov	a,r0
      005DA8 25 6D            [12] 3436 	add	a,_UART_Calc_CRC32_sloc1_1_0
      005DAA F8               [12] 3437 	mov	r0,a
      005DAB E9               [12] 3438 	mov	a,r1
      005DAC 35 6E            [12] 3439 	addc	a,(_UART_Calc_CRC32_sloc1_1_0 + 1)
      005DAE F9               [12] 3440 	mov	r1,a
      005DAF EA               [12] 3441 	mov	a,r2
      005DB0 35 6F            [12] 3442 	addc	a,(_UART_Calc_CRC32_sloc1_1_0 + 2)
      005DB2 FA               [12] 3443 	mov	r2,a
      005DB3 EB               [12] 3444 	mov	a,r3
      005DB4 35 70            [12] 3445 	addc	a,(_UART_Calc_CRC32_sloc1_1_0 + 3)
      005DB6 FB               [12] 3446 	mov	r3,a
      005DB7 8F 04            [24] 3447 	mov	ar4,r7
      005DB9 7D 00            [12] 3448 	mov	r5,#0x00
      005DBB 7E 00            [12] 3449 	mov	r6,#0x00
      005DBD 7F 00            [12] 3450 	mov	r7,#0x00
      005DBF 90 03 AC         [24] 3451 	mov	dptr,#_UART_Calc_CRC32_crc_1_262
      005DC2 EC               [12] 3452 	mov	a,r4
      005DC3 28               [12] 3453 	add	a,r0
      005DC4 F0               [24] 3454 	movx	@dptr,a
      005DC5 ED               [12] 3455 	mov	a,r5
      005DC6 39               [12] 3456 	addc	a,r1
      005DC7 A3               [24] 3457 	inc	dptr
      005DC8 F0               [24] 3458 	movx	@dptr,a
      005DC9 EE               [12] 3459 	mov	a,r6
      005DCA 3A               [12] 3460 	addc	a,r2
      005DCB A3               [24] 3461 	inc	dptr
      005DCC F0               [24] 3462 	movx	@dptr,a
      005DCD EF               [12] 3463 	mov	a,r7
      005DCE 3B               [12] 3464 	addc	a,r3
      005DCF A3               [24] 3465 	inc	dptr
      005DD0 F0               [24] 3466 	movx	@dptr,a
      005DD1                       3467 00106$:
                                   3468 ;	..\src\COMMON\UartComProc.c:253: return crc;
      005DD1 90 03 AC         [24] 3469 	mov	dptr,#_UART_Calc_CRC32_crc_1_262
      005DD4 E0               [24] 3470 	movx	a,@dptr
      005DD5 FC               [12] 3471 	mov	r4,a
      005DD6 A3               [24] 3472 	inc	dptr
      005DD7 E0               [24] 3473 	movx	a,@dptr
      005DD8 FD               [12] 3474 	mov	r5,a
      005DD9 A3               [24] 3475 	inc	dptr
      005DDA E0               [24] 3476 	movx	a,@dptr
      005DDB FE               [12] 3477 	mov	r6,a
      005DDC A3               [24] 3478 	inc	dptr
      005DDD E0               [24] 3479 	movx	a,@dptr
      005DDE 8C 82            [24] 3480 	mov	dpl,r4
      005DE0 8D 83            [24] 3481 	mov	dph,r5
      005DE2 8E F0            [24] 3482 	mov	b,r6
      005DE4 22               [24] 3483 	ret
                                   3484 	.area CSEG    (CODE)
                                   3485 	.area CONST   (CODE)
                                   3486 	.area XINIT   (CODE)
                                   3487 	.area CABS    (ABS,CODE)
