                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.9.3 #11354 (MINGW32)
                                      4 ;--------------------------------------------------------
                                      5 	.module configmaster
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _lpxosc_settlingtime
                                     12 	.globl _framing_counter_pos
                                     13 	.globl _framing_insert_counter
                                     14 	.globl _localaddr
                                     15 	.globl _remoteaddr
                                     16 	.globl _demo_packet
                                     17 ;--------------------------------------------------------
                                     18 ; special function registers
                                     19 ;--------------------------------------------------------
                                     20 	.area RSEG    (ABS,DATA)
      000000                         21 	.org 0x0000
                                     22 ;--------------------------------------------------------
                                     23 ; special function bits
                                     24 ;--------------------------------------------------------
                                     25 	.area RSEG    (ABS,DATA)
      000000                         26 	.org 0x0000
                                     27 ;--------------------------------------------------------
                                     28 ; overlayable register banks
                                     29 ;--------------------------------------------------------
                                     30 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                         31 	.ds 8
                                     32 ;--------------------------------------------------------
                                     33 ; internal ram data
                                     34 ;--------------------------------------------------------
                                     35 	.area DSEG    (DATA)
                                     36 ;--------------------------------------------------------
                                     37 ; overlayable items in internal ram 
                                     38 ;--------------------------------------------------------
                                     39 ;--------------------------------------------------------
                                     40 ; indirectly addressable internal ram data
                                     41 ;--------------------------------------------------------
                                     42 	.area ISEG    (DATA)
                                     43 ;--------------------------------------------------------
                                     44 ; absolute internal ram data
                                     45 ;--------------------------------------------------------
                                     46 	.area IABS    (ABS,DATA)
                                     47 	.area IABS    (ABS,DATA)
                                     48 ;--------------------------------------------------------
                                     49 ; bit data
                                     50 ;--------------------------------------------------------
                                     51 	.area BSEG    (BIT)
                                     52 ;--------------------------------------------------------
                                     53 ; paged external ram data
                                     54 ;--------------------------------------------------------
                                     55 	.area PSEG    (PAG,XDATA)
                                     56 ;--------------------------------------------------------
                                     57 ; external ram data
                                     58 ;--------------------------------------------------------
                                     59 	.area XSEG    (XDATA)
                                     60 ;--------------------------------------------------------
                                     61 ; absolute external ram data
                                     62 ;--------------------------------------------------------
                                     63 	.area XABS    (ABS,XDATA)
                                     64 ;--------------------------------------------------------
                                     65 ; external initialized ram data
                                     66 ;--------------------------------------------------------
                                     67 	.area XISEG   (XDATA)
      000A36                         68 _demo_packet::
      000A36                         69 	.ds 24
                                     70 	.area HOME    (CODE)
                                     71 	.area GSINIT0 (CODE)
                                     72 	.area GSINIT1 (CODE)
                                     73 	.area GSINIT2 (CODE)
                                     74 	.area GSINIT3 (CODE)
                                     75 	.area GSINIT4 (CODE)
                                     76 	.area GSINIT5 (CODE)
                                     77 	.area GSINIT  (CODE)
                                     78 	.area GSFINAL (CODE)
                                     79 	.area CSEG    (CODE)
                                     80 ;--------------------------------------------------------
                                     81 ; global & static initialisations
                                     82 ;--------------------------------------------------------
                                     83 	.area HOME    (CODE)
                                     84 	.area GSINIT  (CODE)
                                     85 	.area GSFINAL (CODE)
                                     86 	.area GSINIT  (CODE)
                                     87 ;--------------------------------------------------------
                                     88 ; Home
                                     89 ;--------------------------------------------------------
                                     90 	.area HOME    (CODE)
                                     91 	.area HOME    (CODE)
                                     92 ;--------------------------------------------------------
                                     93 ; code
                                     94 ;--------------------------------------------------------
                                     95 	.area CSEG    (CODE)
                                     96 	.area CSEG    (CODE)
                                     97 	.area CONST   (CODE)
      009131                         98 _remoteaddr:
      009131 32                      99 	.db #0x32	; 50	'2'
      009132 34                     100 	.db #0x34	; 52	'4'
      009133 00                     101 	.db #0x00	; 0
      009134 00                     102 	.db #0x00	; 0
      009135                        103 _localaddr:
      009135 32                     104 	.db #0x32	; 50	'2'
      009136 34                     105 	.db #0x34	; 52	'4'
      009137 00                     106 	.db #0x00	; 0
      009138 00                     107 	.db #0x00	; 0
      009139 FF                     108 	.db #0xff	; 255
      00913A FF                     109 	.db #0xff	; 255
      00913B 00                     110 	.db #0x00	; 0
      00913C 00                     111 	.db #0x00	; 0
      00913D                        112 _framing_insert_counter:
      00913D 00                     113 	.db #0x00	; 0
      00913E                        114 _framing_counter_pos:
      00913E 00                     115 	.db #0x00	; 0
      00913F                        116 _lpxosc_settlingtime:
      00913F B8 0B                  117 	.byte #0xb8, #0x0b	; 3000
                                    118 	.area XINIT   (CODE)
      009CC7                        119 __xinit__demo_packet:
      009CC7 A1                     120 	.db #0xa1	; 161
      009CC8 A5                     121 	.db #0xa5	; 165
      009CC9 C5                     122 	.db #0xc5	; 197
      009CCA 05                     123 	.db #0x05	; 5
      009CCB 99                     124 	.db #0x99	; 153
      009CCC 13                     125 	.db #0x13	; 19
      009CCD 14                     126 	.db #0x14	; 20
      009CCE 11                     127 	.db #0x11	; 17
      009CCF 22                     128 	.db #0x22	; 34
      009CD0 33                     129 	.db #0x33	; 51	'3'
      009CD1 44                     130 	.db #0x44	; 68	'D'
      009CD2 55                     131 	.db #0x55	; 85	'U'
      009CD3 9C                     132 	.db #0x9c	; 156
      009CD4 DF                     133 	.db #0xdf	; 223
      009CD5 13                     134 	.db #0x13	; 19
      009CD6 78                     135 	.db #0x78	; 120	'x'
      009CD7 01                     136 	.db #0x01	; 1
      009CD8 23                     137 	.db #0x23	; 35
      009CD9 45                     138 	.db #0x45	; 69	'E'
      009CDA 67                     139 	.db #0x67	; 103	'g'
      009CDB 89                     140 	.db #0x89	; 137
      009CDC AB                     141 	.db #0xab	; 171
      009CDD DC                     142 	.db #0xdc	; 220
      009CDE EF                     143 	.db #0xef	; 239
                                    144 	.area CABS    (ABS,CODE)
