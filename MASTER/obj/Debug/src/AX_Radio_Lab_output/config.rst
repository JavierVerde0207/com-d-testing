                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.9.3 #11354 (MINGW32)
                                      4 ;--------------------------------------------------------
                                      5 	.module config
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _axradio_sync_slave_rxtimeout
                                     12 	.globl _axradio_sync_slave_rxwindow
                                     13 	.globl _axradio_sync_slave_rxadvance
                                     14 	.globl _axradio_sync_slave_nrrx
                                     15 	.globl _axradio_sync_slave_resyncloss
                                     16 	.globl _axradio_sync_slave_maxperiod
                                     17 	.globl _axradio_sync_slave_syncpause
                                     18 	.globl _axradio_sync_slave_initialsyncwindow
                                     19 	.globl _axradio_sync_slave_syncwindow
                                     20 	.globl _axradio_sync_xoscstartup
                                     21 	.globl _axradio_sync_period
                                     22 	.globl _axradio_wor_period
                                     23 	.globl _axradio_framing_minpayloadlen
                                     24 	.globl _axradio_framing_ack_seqnrpos
                                     25 	.globl _axradio_framing_ack_retransmissions
                                     26 	.globl _axradio_framing_ack_delay
                                     27 	.globl _axradio_framing_ack_timeout
                                     28 	.globl _axradio_framing_enable_sfdcallback
                                     29 	.globl _axradio_framing_syncflags
                                     30 	.globl _axradio_framing_syncword
                                     31 	.globl _axradio_framing_synclen
                                     32 	.globl _axradio_framing_swcrclen
                                     33 	.globl _axradio_framing_lenmask
                                     34 	.globl _axradio_framing_lenoffs
                                     35 	.globl _axradio_framing_lenpos
                                     36 	.globl _axradio_framing_sourceaddrpos
                                     37 	.globl _axradio_framing_destaddrpos
                                     38 	.globl _axradio_framing_addrlen
                                     39 	.globl _axradio_framing_maclen
                                     40 	.globl _axradio_phy_preamble_appendpattern
                                     41 	.globl _axradio_phy_preamble_appendbits
                                     42 	.globl _axradio_phy_preamble_flags
                                     43 	.globl _axradio_phy_preamble_byte
                                     44 	.globl _axradio_phy_preamble_len
                                     45 	.globl _axradio_phy_preamble_longlen
                                     46 	.globl _axradio_phy_preamble_wor_len
                                     47 	.globl _axradio_phy_preamble_wor_longlen
                                     48 	.globl _axradio_phy_lbt_forcetx
                                     49 	.globl _axradio_phy_lbt_retries
                                     50 	.globl _axradio_phy_cs_enabled
                                     51 	.globl _axradio_phy_cs_period
                                     52 	.globl _axradio_phy_channelbusy
                                     53 	.globl _axradio_phy_rssireference
                                     54 	.globl _axradio_phy_rssioffset
                                     55 	.globl _axradio_phy_maxfreqoffset
                                     56 	.globl _axradio_phy_vcocalib
                                     57 	.globl _axradio_phy_chanvcoiinit
                                     58 	.globl _axradio_phy_chanpllrnginit
                                     59 	.globl _axradio_phy_chanfreq
                                     60 	.globl _axradio_phy_nrchannels
                                     61 	.globl _axradio_phy_pn9
                                     62 	.globl _axradio_phy_innerfreqloop
                                     63 	.globl _axradio_byteconv
                                     64 	.globl _crc_crc32
                                     65 	.globl _PORTC_7
                                     66 	.globl _PORTC_6
                                     67 	.globl _PORTC_5
                                     68 	.globl _PORTC_4
                                     69 	.globl _PORTC_3
                                     70 	.globl _PORTC_2
                                     71 	.globl _PORTC_1
                                     72 	.globl _PORTC_0
                                     73 	.globl _PORTB_7
                                     74 	.globl _PORTB_6
                                     75 	.globl _PORTB_5
                                     76 	.globl _PORTB_4
                                     77 	.globl _PORTB_3
                                     78 	.globl _PORTB_2
                                     79 	.globl _PORTB_1
                                     80 	.globl _PORTB_0
                                     81 	.globl _PORTA_7
                                     82 	.globl _PORTA_6
                                     83 	.globl _PORTA_5
                                     84 	.globl _PORTA_4
                                     85 	.globl _PORTA_3
                                     86 	.globl _PORTA_2
                                     87 	.globl _PORTA_1
                                     88 	.globl _PORTA_0
                                     89 	.globl _PINC_7
                                     90 	.globl _PINC_6
                                     91 	.globl _PINC_5
                                     92 	.globl _PINC_4
                                     93 	.globl _PINC_3
                                     94 	.globl _PINC_2
                                     95 	.globl _PINC_1
                                     96 	.globl _PINC_0
                                     97 	.globl _PINB_7
                                     98 	.globl _PINB_6
                                     99 	.globl _PINB_5
                                    100 	.globl _PINB_4
                                    101 	.globl _PINB_3
                                    102 	.globl _PINB_2
                                    103 	.globl _PINB_1
                                    104 	.globl _PINB_0
                                    105 	.globl _PINA_7
                                    106 	.globl _PINA_6
                                    107 	.globl _PINA_5
                                    108 	.globl _PINA_4
                                    109 	.globl _PINA_3
                                    110 	.globl _PINA_2
                                    111 	.globl _PINA_1
                                    112 	.globl _PINA_0
                                    113 	.globl _CY
                                    114 	.globl _AC
                                    115 	.globl _F0
                                    116 	.globl _RS1
                                    117 	.globl _RS0
                                    118 	.globl _OV
                                    119 	.globl _F1
                                    120 	.globl _P
                                    121 	.globl _IP_7
                                    122 	.globl _IP_6
                                    123 	.globl _IP_5
                                    124 	.globl _IP_4
                                    125 	.globl _IP_3
                                    126 	.globl _IP_2
                                    127 	.globl _IP_1
                                    128 	.globl _IP_0
                                    129 	.globl _EA
                                    130 	.globl _IE_7
                                    131 	.globl _IE_6
                                    132 	.globl _IE_5
                                    133 	.globl _IE_4
                                    134 	.globl _IE_3
                                    135 	.globl _IE_2
                                    136 	.globl _IE_1
                                    137 	.globl _IE_0
                                    138 	.globl _EIP_7
                                    139 	.globl _EIP_6
                                    140 	.globl _EIP_5
                                    141 	.globl _EIP_4
                                    142 	.globl _EIP_3
                                    143 	.globl _EIP_2
                                    144 	.globl _EIP_1
                                    145 	.globl _EIP_0
                                    146 	.globl _EIE_7
                                    147 	.globl _EIE_6
                                    148 	.globl _EIE_5
                                    149 	.globl _EIE_4
                                    150 	.globl _EIE_3
                                    151 	.globl _EIE_2
                                    152 	.globl _EIE_1
                                    153 	.globl _EIE_0
                                    154 	.globl _E2IP_7
                                    155 	.globl _E2IP_6
                                    156 	.globl _E2IP_5
                                    157 	.globl _E2IP_4
                                    158 	.globl _E2IP_3
                                    159 	.globl _E2IP_2
                                    160 	.globl _E2IP_1
                                    161 	.globl _E2IP_0
                                    162 	.globl _E2IE_7
                                    163 	.globl _E2IE_6
                                    164 	.globl _E2IE_5
                                    165 	.globl _E2IE_4
                                    166 	.globl _E2IE_3
                                    167 	.globl _E2IE_2
                                    168 	.globl _E2IE_1
                                    169 	.globl _E2IE_0
                                    170 	.globl _B_7
                                    171 	.globl _B_6
                                    172 	.globl _B_5
                                    173 	.globl _B_4
                                    174 	.globl _B_3
                                    175 	.globl _B_2
                                    176 	.globl _B_1
                                    177 	.globl _B_0
                                    178 	.globl _ACC_7
                                    179 	.globl _ACC_6
                                    180 	.globl _ACC_5
                                    181 	.globl _ACC_4
                                    182 	.globl _ACC_3
                                    183 	.globl _ACC_2
                                    184 	.globl _ACC_1
                                    185 	.globl _ACC_0
                                    186 	.globl _WTSTAT
                                    187 	.globl _WTIRQEN
                                    188 	.globl _WTEVTD
                                    189 	.globl _WTEVTD1
                                    190 	.globl _WTEVTD0
                                    191 	.globl _WTEVTC
                                    192 	.globl _WTEVTC1
                                    193 	.globl _WTEVTC0
                                    194 	.globl _WTEVTB
                                    195 	.globl _WTEVTB1
                                    196 	.globl _WTEVTB0
                                    197 	.globl _WTEVTA
                                    198 	.globl _WTEVTA1
                                    199 	.globl _WTEVTA0
                                    200 	.globl _WTCNTR1
                                    201 	.globl _WTCNTB
                                    202 	.globl _WTCNTB1
                                    203 	.globl _WTCNTB0
                                    204 	.globl _WTCNTA
                                    205 	.globl _WTCNTA1
                                    206 	.globl _WTCNTA0
                                    207 	.globl _WTCFGB
                                    208 	.globl _WTCFGA
                                    209 	.globl _WDTRESET
                                    210 	.globl _WDTCFG
                                    211 	.globl _U1STATUS
                                    212 	.globl _U1SHREG
                                    213 	.globl _U1MODE
                                    214 	.globl _U1CTRL
                                    215 	.globl _U0STATUS
                                    216 	.globl _U0SHREG
                                    217 	.globl _U0MODE
                                    218 	.globl _U0CTRL
                                    219 	.globl _T2STATUS
                                    220 	.globl _T2PERIOD
                                    221 	.globl _T2PERIOD1
                                    222 	.globl _T2PERIOD0
                                    223 	.globl _T2MODE
                                    224 	.globl _T2CNT
                                    225 	.globl _T2CNT1
                                    226 	.globl _T2CNT0
                                    227 	.globl _T2CLKSRC
                                    228 	.globl _T1STATUS
                                    229 	.globl _T1PERIOD
                                    230 	.globl _T1PERIOD1
                                    231 	.globl _T1PERIOD0
                                    232 	.globl _T1MODE
                                    233 	.globl _T1CNT
                                    234 	.globl _T1CNT1
                                    235 	.globl _T1CNT0
                                    236 	.globl _T1CLKSRC
                                    237 	.globl _T0STATUS
                                    238 	.globl _T0PERIOD
                                    239 	.globl _T0PERIOD1
                                    240 	.globl _T0PERIOD0
                                    241 	.globl _T0MODE
                                    242 	.globl _T0CNT
                                    243 	.globl _T0CNT1
                                    244 	.globl _T0CNT0
                                    245 	.globl _T0CLKSRC
                                    246 	.globl _SPSTATUS
                                    247 	.globl _SPSHREG
                                    248 	.globl _SPMODE
                                    249 	.globl _SPCLKSRC
                                    250 	.globl _RADIOSTAT
                                    251 	.globl _RADIOSTAT1
                                    252 	.globl _RADIOSTAT0
                                    253 	.globl _RADIODATA
                                    254 	.globl _RADIODATA3
                                    255 	.globl _RADIODATA2
                                    256 	.globl _RADIODATA1
                                    257 	.globl _RADIODATA0
                                    258 	.globl _RADIOADDR
                                    259 	.globl _RADIOADDR1
                                    260 	.globl _RADIOADDR0
                                    261 	.globl _RADIOACC
                                    262 	.globl _OC1STATUS
                                    263 	.globl _OC1PIN
                                    264 	.globl _OC1MODE
                                    265 	.globl _OC1COMP
                                    266 	.globl _OC1COMP1
                                    267 	.globl _OC1COMP0
                                    268 	.globl _OC0STATUS
                                    269 	.globl _OC0PIN
                                    270 	.globl _OC0MODE
                                    271 	.globl _OC0COMP
                                    272 	.globl _OC0COMP1
                                    273 	.globl _OC0COMP0
                                    274 	.globl _NVSTATUS
                                    275 	.globl _NVKEY
                                    276 	.globl _NVDATA
                                    277 	.globl _NVDATA1
                                    278 	.globl _NVDATA0
                                    279 	.globl _NVADDR
                                    280 	.globl _NVADDR1
                                    281 	.globl _NVADDR0
                                    282 	.globl _IC1STATUS
                                    283 	.globl _IC1MODE
                                    284 	.globl _IC1CAPT
                                    285 	.globl _IC1CAPT1
                                    286 	.globl _IC1CAPT0
                                    287 	.globl _IC0STATUS
                                    288 	.globl _IC0MODE
                                    289 	.globl _IC0CAPT
                                    290 	.globl _IC0CAPT1
                                    291 	.globl _IC0CAPT0
                                    292 	.globl _PORTR
                                    293 	.globl _PORTC
                                    294 	.globl _PORTB
                                    295 	.globl _PORTA
                                    296 	.globl _PINR
                                    297 	.globl _PINC
                                    298 	.globl _PINB
                                    299 	.globl _PINA
                                    300 	.globl _DIRR
                                    301 	.globl _DIRC
                                    302 	.globl _DIRB
                                    303 	.globl _DIRA
                                    304 	.globl _DBGLNKSTAT
                                    305 	.globl _DBGLNKBUF
                                    306 	.globl _CODECONFIG
                                    307 	.globl _CLKSTAT
                                    308 	.globl _CLKCON
                                    309 	.globl _ANALOGCOMP
                                    310 	.globl _ADCCONV
                                    311 	.globl _ADCCLKSRC
                                    312 	.globl _ADCCH3CONFIG
                                    313 	.globl _ADCCH2CONFIG
                                    314 	.globl _ADCCH1CONFIG
                                    315 	.globl _ADCCH0CONFIG
                                    316 	.globl __XPAGE
                                    317 	.globl _XPAGE
                                    318 	.globl _SP
                                    319 	.globl _PSW
                                    320 	.globl _PCON
                                    321 	.globl _IP
                                    322 	.globl _IE
                                    323 	.globl _EIP
                                    324 	.globl _EIE
                                    325 	.globl _E2IP
                                    326 	.globl _E2IE
                                    327 	.globl _DPS
                                    328 	.globl _DPTR1
                                    329 	.globl _DPTR0
                                    330 	.globl _DPL1
                                    331 	.globl _DPL
                                    332 	.globl _DPH1
                                    333 	.globl _DPH
                                    334 	.globl _B
                                    335 	.globl _ACC
                                    336 	.globl _axradio_phy_chanvcoi
                                    337 	.globl _axradio_phy_chanpllrng
                                    338 	.globl _AX5043_XTALAMPL
                                    339 	.globl _AX5043_XTALOSC
                                    340 	.globl _AX5043_MODCFGP
                                    341 	.globl _AX5043_POWCTRL1
                                    342 	.globl _AX5043_REF
                                    343 	.globl _AX5043_0xF44
                                    344 	.globl _AX5043_0xF35
                                    345 	.globl _AX5043_0xF34
                                    346 	.globl _AX5043_0xF33
                                    347 	.globl _AX5043_0xF32
                                    348 	.globl _AX5043_0xF31
                                    349 	.globl _AX5043_0xF30
                                    350 	.globl _AX5043_0xF26
                                    351 	.globl _AX5043_0xF23
                                    352 	.globl _AX5043_0xF22
                                    353 	.globl _AX5043_0xF21
                                    354 	.globl _AX5043_0xF1C
                                    355 	.globl _AX5043_0xF18
                                    356 	.globl _AX5043_0xF11
                                    357 	.globl _AX5043_0xF10
                                    358 	.globl _AX5043_0xF0C
                                    359 	.globl _AX5043_0xF00
                                    360 	.globl _AX5043_TIMEGAIN3NB
                                    361 	.globl _AX5043_TIMEGAIN2NB
                                    362 	.globl _AX5043_TIMEGAIN1NB
                                    363 	.globl _AX5043_TIMEGAIN0NB
                                    364 	.globl _AX5043_RXPARAMSETSNB
                                    365 	.globl _AX5043_RXPARAMCURSETNB
                                    366 	.globl _AX5043_PKTMAXLENNB
                                    367 	.globl _AX5043_PKTLENOFFSETNB
                                    368 	.globl _AX5043_PKTLENCFGNB
                                    369 	.globl _AX5043_PKTADDRMASK3NB
                                    370 	.globl _AX5043_PKTADDRMASK2NB
                                    371 	.globl _AX5043_PKTADDRMASK1NB
                                    372 	.globl _AX5043_PKTADDRMASK0NB
                                    373 	.globl _AX5043_PKTADDRCFGNB
                                    374 	.globl _AX5043_PKTADDR3NB
                                    375 	.globl _AX5043_PKTADDR2NB
                                    376 	.globl _AX5043_PKTADDR1NB
                                    377 	.globl _AX5043_PKTADDR0NB
                                    378 	.globl _AX5043_PHASEGAIN3NB
                                    379 	.globl _AX5043_PHASEGAIN2NB
                                    380 	.globl _AX5043_PHASEGAIN1NB
                                    381 	.globl _AX5043_PHASEGAIN0NB
                                    382 	.globl _AX5043_FREQUENCYLEAKNB
                                    383 	.globl _AX5043_FREQUENCYGAIND3NB
                                    384 	.globl _AX5043_FREQUENCYGAIND2NB
                                    385 	.globl _AX5043_FREQUENCYGAIND1NB
                                    386 	.globl _AX5043_FREQUENCYGAIND0NB
                                    387 	.globl _AX5043_FREQUENCYGAINC3NB
                                    388 	.globl _AX5043_FREQUENCYGAINC2NB
                                    389 	.globl _AX5043_FREQUENCYGAINC1NB
                                    390 	.globl _AX5043_FREQUENCYGAINC0NB
                                    391 	.globl _AX5043_FREQUENCYGAINB3NB
                                    392 	.globl _AX5043_FREQUENCYGAINB2NB
                                    393 	.globl _AX5043_FREQUENCYGAINB1NB
                                    394 	.globl _AX5043_FREQUENCYGAINB0NB
                                    395 	.globl _AX5043_FREQUENCYGAINA3NB
                                    396 	.globl _AX5043_FREQUENCYGAINA2NB
                                    397 	.globl _AX5043_FREQUENCYGAINA1NB
                                    398 	.globl _AX5043_FREQUENCYGAINA0NB
                                    399 	.globl _AX5043_FREQDEV13NB
                                    400 	.globl _AX5043_FREQDEV12NB
                                    401 	.globl _AX5043_FREQDEV11NB
                                    402 	.globl _AX5043_FREQDEV10NB
                                    403 	.globl _AX5043_FREQDEV03NB
                                    404 	.globl _AX5043_FREQDEV02NB
                                    405 	.globl _AX5043_FREQDEV01NB
                                    406 	.globl _AX5043_FREQDEV00NB
                                    407 	.globl _AX5043_FOURFSK3NB
                                    408 	.globl _AX5043_FOURFSK2NB
                                    409 	.globl _AX5043_FOURFSK1NB
                                    410 	.globl _AX5043_FOURFSK0NB
                                    411 	.globl _AX5043_DRGAIN3NB
                                    412 	.globl _AX5043_DRGAIN2NB
                                    413 	.globl _AX5043_DRGAIN1NB
                                    414 	.globl _AX5043_DRGAIN0NB
                                    415 	.globl _AX5043_BBOFFSRES3NB
                                    416 	.globl _AX5043_BBOFFSRES2NB
                                    417 	.globl _AX5043_BBOFFSRES1NB
                                    418 	.globl _AX5043_BBOFFSRES0NB
                                    419 	.globl _AX5043_AMPLITUDEGAIN3NB
                                    420 	.globl _AX5043_AMPLITUDEGAIN2NB
                                    421 	.globl _AX5043_AMPLITUDEGAIN1NB
                                    422 	.globl _AX5043_AMPLITUDEGAIN0NB
                                    423 	.globl _AX5043_AGCTARGET3NB
                                    424 	.globl _AX5043_AGCTARGET2NB
                                    425 	.globl _AX5043_AGCTARGET1NB
                                    426 	.globl _AX5043_AGCTARGET0NB
                                    427 	.globl _AX5043_AGCMINMAX3NB
                                    428 	.globl _AX5043_AGCMINMAX2NB
                                    429 	.globl _AX5043_AGCMINMAX1NB
                                    430 	.globl _AX5043_AGCMINMAX0NB
                                    431 	.globl _AX5043_AGCGAIN3NB
                                    432 	.globl _AX5043_AGCGAIN2NB
                                    433 	.globl _AX5043_AGCGAIN1NB
                                    434 	.globl _AX5043_AGCGAIN0NB
                                    435 	.globl _AX5043_AGCAHYST3NB
                                    436 	.globl _AX5043_AGCAHYST2NB
                                    437 	.globl _AX5043_AGCAHYST1NB
                                    438 	.globl _AX5043_AGCAHYST0NB
                                    439 	.globl _AX5043_0xF44NB
                                    440 	.globl _AX5043_0xF35NB
                                    441 	.globl _AX5043_0xF34NB
                                    442 	.globl _AX5043_0xF33NB
                                    443 	.globl _AX5043_0xF32NB
                                    444 	.globl _AX5043_0xF31NB
                                    445 	.globl _AX5043_0xF30NB
                                    446 	.globl _AX5043_0xF26NB
                                    447 	.globl _AX5043_0xF23NB
                                    448 	.globl _AX5043_0xF22NB
                                    449 	.globl _AX5043_0xF21NB
                                    450 	.globl _AX5043_0xF1CNB
                                    451 	.globl _AX5043_0xF18NB
                                    452 	.globl _AX5043_0xF0CNB
                                    453 	.globl _AX5043_0xF00NB
                                    454 	.globl _AX5043_XTALSTATUSNB
                                    455 	.globl _AX5043_XTALOSCNB
                                    456 	.globl _AX5043_XTALCAPNB
                                    457 	.globl _AX5043_XTALAMPLNB
                                    458 	.globl _AX5043_WAKEUPXOEARLYNB
                                    459 	.globl _AX5043_WAKEUPTIMER1NB
                                    460 	.globl _AX5043_WAKEUPTIMER0NB
                                    461 	.globl _AX5043_WAKEUPFREQ1NB
                                    462 	.globl _AX5043_WAKEUPFREQ0NB
                                    463 	.globl _AX5043_WAKEUP1NB
                                    464 	.globl _AX5043_WAKEUP0NB
                                    465 	.globl _AX5043_TXRATE2NB
                                    466 	.globl _AX5043_TXRATE1NB
                                    467 	.globl _AX5043_TXRATE0NB
                                    468 	.globl _AX5043_TXPWRCOEFFE1NB
                                    469 	.globl _AX5043_TXPWRCOEFFE0NB
                                    470 	.globl _AX5043_TXPWRCOEFFD1NB
                                    471 	.globl _AX5043_TXPWRCOEFFD0NB
                                    472 	.globl _AX5043_TXPWRCOEFFC1NB
                                    473 	.globl _AX5043_TXPWRCOEFFC0NB
                                    474 	.globl _AX5043_TXPWRCOEFFB1NB
                                    475 	.globl _AX5043_TXPWRCOEFFB0NB
                                    476 	.globl _AX5043_TXPWRCOEFFA1NB
                                    477 	.globl _AX5043_TXPWRCOEFFA0NB
                                    478 	.globl _AX5043_TRKRFFREQ2NB
                                    479 	.globl _AX5043_TRKRFFREQ1NB
                                    480 	.globl _AX5043_TRKRFFREQ0NB
                                    481 	.globl _AX5043_TRKPHASE1NB
                                    482 	.globl _AX5043_TRKPHASE0NB
                                    483 	.globl _AX5043_TRKFSKDEMOD1NB
                                    484 	.globl _AX5043_TRKFSKDEMOD0NB
                                    485 	.globl _AX5043_TRKFREQ1NB
                                    486 	.globl _AX5043_TRKFREQ0NB
                                    487 	.globl _AX5043_TRKDATARATE2NB
                                    488 	.globl _AX5043_TRKDATARATE1NB
                                    489 	.globl _AX5043_TRKDATARATE0NB
                                    490 	.globl _AX5043_TRKAMPLITUDE1NB
                                    491 	.globl _AX5043_TRKAMPLITUDE0NB
                                    492 	.globl _AX5043_TRKAFSKDEMOD1NB
                                    493 	.globl _AX5043_TRKAFSKDEMOD0NB
                                    494 	.globl _AX5043_TMGTXSETTLENB
                                    495 	.globl _AX5043_TMGTXBOOSTNB
                                    496 	.globl _AX5043_TMGRXSETTLENB
                                    497 	.globl _AX5043_TMGRXRSSINB
                                    498 	.globl _AX5043_TMGRXPREAMBLE3NB
                                    499 	.globl _AX5043_TMGRXPREAMBLE2NB
                                    500 	.globl _AX5043_TMGRXPREAMBLE1NB
                                    501 	.globl _AX5043_TMGRXOFFSACQNB
                                    502 	.globl _AX5043_TMGRXCOARSEAGCNB
                                    503 	.globl _AX5043_TMGRXBOOSTNB
                                    504 	.globl _AX5043_TMGRXAGCNB
                                    505 	.globl _AX5043_TIMER2NB
                                    506 	.globl _AX5043_TIMER1NB
                                    507 	.globl _AX5043_TIMER0NB
                                    508 	.globl _AX5043_SILICONREVISIONNB
                                    509 	.globl _AX5043_SCRATCHNB
                                    510 	.globl _AX5043_RXDATARATE2NB
                                    511 	.globl _AX5043_RXDATARATE1NB
                                    512 	.globl _AX5043_RXDATARATE0NB
                                    513 	.globl _AX5043_RSSIREFERENCENB
                                    514 	.globl _AX5043_RSSIABSTHRNB
                                    515 	.globl _AX5043_RSSINB
                                    516 	.globl _AX5043_REFNB
                                    517 	.globl _AX5043_RADIOSTATENB
                                    518 	.globl _AX5043_RADIOEVENTREQ1NB
                                    519 	.globl _AX5043_RADIOEVENTREQ0NB
                                    520 	.globl _AX5043_RADIOEVENTMASK1NB
                                    521 	.globl _AX5043_RADIOEVENTMASK0NB
                                    522 	.globl _AX5043_PWRMODENB
                                    523 	.globl _AX5043_PWRAMPNB
                                    524 	.globl _AX5043_POWSTICKYSTATNB
                                    525 	.globl _AX5043_POWSTATNB
                                    526 	.globl _AX5043_POWIRQMASKNB
                                    527 	.globl _AX5043_POWCTRL1NB
                                    528 	.globl _AX5043_PLLVCOIRNB
                                    529 	.globl _AX5043_PLLVCOINB
                                    530 	.globl _AX5043_PLLVCODIVNB
                                    531 	.globl _AX5043_PLLRNGCLKNB
                                    532 	.globl _AX5043_PLLRANGINGBNB
                                    533 	.globl _AX5043_PLLRANGINGANB
                                    534 	.globl _AX5043_PLLLOOPBOOSTNB
                                    535 	.globl _AX5043_PLLLOOPNB
                                    536 	.globl _AX5043_PLLLOCKDETNB
                                    537 	.globl _AX5043_PLLCPIBOOSTNB
                                    538 	.globl _AX5043_PLLCPINB
                                    539 	.globl _AX5043_PKTSTOREFLAGSNB
                                    540 	.globl _AX5043_PKTMISCFLAGSNB
                                    541 	.globl _AX5043_PKTCHUNKSIZENB
                                    542 	.globl _AX5043_PKTACCEPTFLAGSNB
                                    543 	.globl _AX5043_PINSTATENB
                                    544 	.globl _AX5043_PINFUNCSYSCLKNB
                                    545 	.globl _AX5043_PINFUNCPWRAMPNB
                                    546 	.globl _AX5043_PINFUNCIRQNB
                                    547 	.globl _AX5043_PINFUNCDCLKNB
                                    548 	.globl _AX5043_PINFUNCDATANB
                                    549 	.globl _AX5043_PINFUNCANTSELNB
                                    550 	.globl _AX5043_MODULATIONNB
                                    551 	.globl _AX5043_MODCFGPNB
                                    552 	.globl _AX5043_MODCFGFNB
                                    553 	.globl _AX5043_MODCFGANB
                                    554 	.globl _AX5043_MAXRFOFFSET2NB
                                    555 	.globl _AX5043_MAXRFOFFSET1NB
                                    556 	.globl _AX5043_MAXRFOFFSET0NB
                                    557 	.globl _AX5043_MAXDROFFSET2NB
                                    558 	.globl _AX5043_MAXDROFFSET1NB
                                    559 	.globl _AX5043_MAXDROFFSET0NB
                                    560 	.globl _AX5043_MATCH1PAT1NB
                                    561 	.globl _AX5043_MATCH1PAT0NB
                                    562 	.globl _AX5043_MATCH1MINNB
                                    563 	.globl _AX5043_MATCH1MAXNB
                                    564 	.globl _AX5043_MATCH1LENNB
                                    565 	.globl _AX5043_MATCH0PAT3NB
                                    566 	.globl _AX5043_MATCH0PAT2NB
                                    567 	.globl _AX5043_MATCH0PAT1NB
                                    568 	.globl _AX5043_MATCH0PAT0NB
                                    569 	.globl _AX5043_MATCH0MINNB
                                    570 	.globl _AX5043_MATCH0MAXNB
                                    571 	.globl _AX5043_MATCH0LENNB
                                    572 	.globl _AX5043_LPOSCSTATUSNB
                                    573 	.globl _AX5043_LPOSCREF1NB
                                    574 	.globl _AX5043_LPOSCREF0NB
                                    575 	.globl _AX5043_LPOSCPER1NB
                                    576 	.globl _AX5043_LPOSCPER0NB
                                    577 	.globl _AX5043_LPOSCKFILT1NB
                                    578 	.globl _AX5043_LPOSCKFILT0NB
                                    579 	.globl _AX5043_LPOSCFREQ1NB
                                    580 	.globl _AX5043_LPOSCFREQ0NB
                                    581 	.globl _AX5043_LPOSCCONFIGNB
                                    582 	.globl _AX5043_IRQREQUEST1NB
                                    583 	.globl _AX5043_IRQREQUEST0NB
                                    584 	.globl _AX5043_IRQMASK1NB
                                    585 	.globl _AX5043_IRQMASK0NB
                                    586 	.globl _AX5043_IRQINVERSION1NB
                                    587 	.globl _AX5043_IRQINVERSION0NB
                                    588 	.globl _AX5043_IFFREQ1NB
                                    589 	.globl _AX5043_IFFREQ0NB
                                    590 	.globl _AX5043_GPADCPERIODNB
                                    591 	.globl _AX5043_GPADCCTRLNB
                                    592 	.globl _AX5043_GPADC13VALUE1NB
                                    593 	.globl _AX5043_GPADC13VALUE0NB
                                    594 	.globl _AX5043_FSKDMIN1NB
                                    595 	.globl _AX5043_FSKDMIN0NB
                                    596 	.globl _AX5043_FSKDMAX1NB
                                    597 	.globl _AX5043_FSKDMAX0NB
                                    598 	.globl _AX5043_FSKDEV2NB
                                    599 	.globl _AX5043_FSKDEV1NB
                                    600 	.globl _AX5043_FSKDEV0NB
                                    601 	.globl _AX5043_FREQB3NB
                                    602 	.globl _AX5043_FREQB2NB
                                    603 	.globl _AX5043_FREQB1NB
                                    604 	.globl _AX5043_FREQB0NB
                                    605 	.globl _AX5043_FREQA3NB
                                    606 	.globl _AX5043_FREQA2NB
                                    607 	.globl _AX5043_FREQA1NB
                                    608 	.globl _AX5043_FREQA0NB
                                    609 	.globl _AX5043_FRAMINGNB
                                    610 	.globl _AX5043_FIFOTHRESH1NB
                                    611 	.globl _AX5043_FIFOTHRESH0NB
                                    612 	.globl _AX5043_FIFOSTATNB
                                    613 	.globl _AX5043_FIFOFREE1NB
                                    614 	.globl _AX5043_FIFOFREE0NB
                                    615 	.globl _AX5043_FIFODATANB
                                    616 	.globl _AX5043_FIFOCOUNT1NB
                                    617 	.globl _AX5043_FIFOCOUNT0NB
                                    618 	.globl _AX5043_FECSYNCNB
                                    619 	.globl _AX5043_FECSTATUSNB
                                    620 	.globl _AX5043_FECNB
                                    621 	.globl _AX5043_ENCODINGNB
                                    622 	.globl _AX5043_DIVERSITYNB
                                    623 	.globl _AX5043_DECIMATIONNB
                                    624 	.globl _AX5043_DACVALUE1NB
                                    625 	.globl _AX5043_DACVALUE0NB
                                    626 	.globl _AX5043_DACCONFIGNB
                                    627 	.globl _AX5043_CRCINIT3NB
                                    628 	.globl _AX5043_CRCINIT2NB
                                    629 	.globl _AX5043_CRCINIT1NB
                                    630 	.globl _AX5043_CRCINIT0NB
                                    631 	.globl _AX5043_BGNDRSSITHRNB
                                    632 	.globl _AX5043_BGNDRSSIGAINNB
                                    633 	.globl _AX5043_BGNDRSSINB
                                    634 	.globl _AX5043_BBTUNENB
                                    635 	.globl _AX5043_BBOFFSCAPNB
                                    636 	.globl _AX5043_AMPLFILTERNB
                                    637 	.globl _AX5043_AGCCOUNTERNB
                                    638 	.globl _AX5043_AFSKSPACE1NB
                                    639 	.globl _AX5043_AFSKSPACE0NB
                                    640 	.globl _AX5043_AFSKMARK1NB
                                    641 	.globl _AX5043_AFSKMARK0NB
                                    642 	.globl _AX5043_AFSKCTRLNB
                                    643 	.globl _AX5043_TIMEGAIN3
                                    644 	.globl _AX5043_TIMEGAIN2
                                    645 	.globl _AX5043_TIMEGAIN1
                                    646 	.globl _AX5043_TIMEGAIN0
                                    647 	.globl _AX5043_RXPARAMSETS
                                    648 	.globl _AX5043_RXPARAMCURSET
                                    649 	.globl _AX5043_PKTMAXLEN
                                    650 	.globl _AX5043_PKTLENOFFSET
                                    651 	.globl _AX5043_PKTLENCFG
                                    652 	.globl _AX5043_PKTADDRMASK3
                                    653 	.globl _AX5043_PKTADDRMASK2
                                    654 	.globl _AX5043_PKTADDRMASK1
                                    655 	.globl _AX5043_PKTADDRMASK0
                                    656 	.globl _AX5043_PKTADDRCFG
                                    657 	.globl _AX5043_PKTADDR3
                                    658 	.globl _AX5043_PKTADDR2
                                    659 	.globl _AX5043_PKTADDR1
                                    660 	.globl _AX5043_PKTADDR0
                                    661 	.globl _AX5043_PHASEGAIN3
                                    662 	.globl _AX5043_PHASEGAIN2
                                    663 	.globl _AX5043_PHASEGAIN1
                                    664 	.globl _AX5043_PHASEGAIN0
                                    665 	.globl _AX5043_FREQUENCYLEAK
                                    666 	.globl _AX5043_FREQUENCYGAIND3
                                    667 	.globl _AX5043_FREQUENCYGAIND2
                                    668 	.globl _AX5043_FREQUENCYGAIND1
                                    669 	.globl _AX5043_FREQUENCYGAIND0
                                    670 	.globl _AX5043_FREQUENCYGAINC3
                                    671 	.globl _AX5043_FREQUENCYGAINC2
                                    672 	.globl _AX5043_FREQUENCYGAINC1
                                    673 	.globl _AX5043_FREQUENCYGAINC0
                                    674 	.globl _AX5043_FREQUENCYGAINB3
                                    675 	.globl _AX5043_FREQUENCYGAINB2
                                    676 	.globl _AX5043_FREQUENCYGAINB1
                                    677 	.globl _AX5043_FREQUENCYGAINB0
                                    678 	.globl _AX5043_FREQUENCYGAINA3
                                    679 	.globl _AX5043_FREQUENCYGAINA2
                                    680 	.globl _AX5043_FREQUENCYGAINA1
                                    681 	.globl _AX5043_FREQUENCYGAINA0
                                    682 	.globl _AX5043_FREQDEV13
                                    683 	.globl _AX5043_FREQDEV12
                                    684 	.globl _AX5043_FREQDEV11
                                    685 	.globl _AX5043_FREQDEV10
                                    686 	.globl _AX5043_FREQDEV03
                                    687 	.globl _AX5043_FREQDEV02
                                    688 	.globl _AX5043_FREQDEV01
                                    689 	.globl _AX5043_FREQDEV00
                                    690 	.globl _AX5043_FOURFSK3
                                    691 	.globl _AX5043_FOURFSK2
                                    692 	.globl _AX5043_FOURFSK1
                                    693 	.globl _AX5043_FOURFSK0
                                    694 	.globl _AX5043_DRGAIN3
                                    695 	.globl _AX5043_DRGAIN2
                                    696 	.globl _AX5043_DRGAIN1
                                    697 	.globl _AX5043_DRGAIN0
                                    698 	.globl _AX5043_BBOFFSRES3
                                    699 	.globl _AX5043_BBOFFSRES2
                                    700 	.globl _AX5043_BBOFFSRES1
                                    701 	.globl _AX5043_BBOFFSRES0
                                    702 	.globl _AX5043_AMPLITUDEGAIN3
                                    703 	.globl _AX5043_AMPLITUDEGAIN2
                                    704 	.globl _AX5043_AMPLITUDEGAIN1
                                    705 	.globl _AX5043_AMPLITUDEGAIN0
                                    706 	.globl _AX5043_AGCTARGET3
                                    707 	.globl _AX5043_AGCTARGET2
                                    708 	.globl _AX5043_AGCTARGET1
                                    709 	.globl _AX5043_AGCTARGET0
                                    710 	.globl _AX5043_AGCMINMAX3
                                    711 	.globl _AX5043_AGCMINMAX2
                                    712 	.globl _AX5043_AGCMINMAX1
                                    713 	.globl _AX5043_AGCMINMAX0
                                    714 	.globl _AX5043_AGCGAIN3
                                    715 	.globl _AX5043_AGCGAIN2
                                    716 	.globl _AX5043_AGCGAIN1
                                    717 	.globl _AX5043_AGCGAIN0
                                    718 	.globl _AX5043_AGCAHYST3
                                    719 	.globl _AX5043_AGCAHYST2
                                    720 	.globl _AX5043_AGCAHYST1
                                    721 	.globl _AX5043_AGCAHYST0
                                    722 	.globl _AX5043_XTALSTATUS
                                    723 	.globl _AX5043_XTALCAP
                                    724 	.globl _AX5043_WAKEUPXOEARLY
                                    725 	.globl _AX5043_WAKEUPTIMER1
                                    726 	.globl _AX5043_WAKEUPTIMER0
                                    727 	.globl _AX5043_WAKEUPFREQ1
                                    728 	.globl _AX5043_WAKEUPFREQ0
                                    729 	.globl _AX5043_WAKEUP1
                                    730 	.globl _AX5043_WAKEUP0
                                    731 	.globl _AX5043_TXRATE2
                                    732 	.globl _AX5043_TXRATE1
                                    733 	.globl _AX5043_TXRATE0
                                    734 	.globl _AX5043_TXPWRCOEFFE1
                                    735 	.globl _AX5043_TXPWRCOEFFE0
                                    736 	.globl _AX5043_TXPWRCOEFFD1
                                    737 	.globl _AX5043_TXPWRCOEFFD0
                                    738 	.globl _AX5043_TXPWRCOEFFC1
                                    739 	.globl _AX5043_TXPWRCOEFFC0
                                    740 	.globl _AX5043_TXPWRCOEFFB1
                                    741 	.globl _AX5043_TXPWRCOEFFB0
                                    742 	.globl _AX5043_TXPWRCOEFFA1
                                    743 	.globl _AX5043_TXPWRCOEFFA0
                                    744 	.globl _AX5043_TRKRFFREQ2
                                    745 	.globl _AX5043_TRKRFFREQ1
                                    746 	.globl _AX5043_TRKRFFREQ0
                                    747 	.globl _AX5043_TRKPHASE1
                                    748 	.globl _AX5043_TRKPHASE0
                                    749 	.globl _AX5043_TRKFSKDEMOD1
                                    750 	.globl _AX5043_TRKFSKDEMOD0
                                    751 	.globl _AX5043_TRKFREQ1
                                    752 	.globl _AX5043_TRKFREQ0
                                    753 	.globl _AX5043_TRKDATARATE2
                                    754 	.globl _AX5043_TRKDATARATE1
                                    755 	.globl _AX5043_TRKDATARATE0
                                    756 	.globl _AX5043_TRKAMPLITUDE1
                                    757 	.globl _AX5043_TRKAMPLITUDE0
                                    758 	.globl _AX5043_TRKAFSKDEMOD1
                                    759 	.globl _AX5043_TRKAFSKDEMOD0
                                    760 	.globl _AX5043_TMGTXSETTLE
                                    761 	.globl _AX5043_TMGTXBOOST
                                    762 	.globl _AX5043_TMGRXSETTLE
                                    763 	.globl _AX5043_TMGRXRSSI
                                    764 	.globl _AX5043_TMGRXPREAMBLE3
                                    765 	.globl _AX5043_TMGRXPREAMBLE2
                                    766 	.globl _AX5043_TMGRXPREAMBLE1
                                    767 	.globl _AX5043_TMGRXOFFSACQ
                                    768 	.globl _AX5043_TMGRXCOARSEAGC
                                    769 	.globl _AX5043_TMGRXBOOST
                                    770 	.globl _AX5043_TMGRXAGC
                                    771 	.globl _AX5043_TIMER2
                                    772 	.globl _AX5043_TIMER1
                                    773 	.globl _AX5043_TIMER0
                                    774 	.globl _AX5043_SILICONREVISION
                                    775 	.globl _AX5043_SCRATCH
                                    776 	.globl _AX5043_RXDATARATE2
                                    777 	.globl _AX5043_RXDATARATE1
                                    778 	.globl _AX5043_RXDATARATE0
                                    779 	.globl _AX5043_RSSIREFERENCE
                                    780 	.globl _AX5043_RSSIABSTHR
                                    781 	.globl _AX5043_RSSI
                                    782 	.globl _AX5043_RADIOSTATE
                                    783 	.globl _AX5043_RADIOEVENTREQ1
                                    784 	.globl _AX5043_RADIOEVENTREQ0
                                    785 	.globl _AX5043_RADIOEVENTMASK1
                                    786 	.globl _AX5043_RADIOEVENTMASK0
                                    787 	.globl _AX5043_PWRMODE
                                    788 	.globl _AX5043_PWRAMP
                                    789 	.globl _AX5043_POWSTICKYSTAT
                                    790 	.globl _AX5043_POWSTAT
                                    791 	.globl _AX5043_POWIRQMASK
                                    792 	.globl _AX5043_PLLVCOIR
                                    793 	.globl _AX5043_PLLVCOI
                                    794 	.globl _AX5043_PLLVCODIV
                                    795 	.globl _AX5043_PLLRNGCLK
                                    796 	.globl _AX5043_PLLRANGINGB
                                    797 	.globl _AX5043_PLLRANGINGA
                                    798 	.globl _AX5043_PLLLOOPBOOST
                                    799 	.globl _AX5043_PLLLOOP
                                    800 	.globl _AX5043_PLLLOCKDET
                                    801 	.globl _AX5043_PLLCPIBOOST
                                    802 	.globl _AX5043_PLLCPI
                                    803 	.globl _AX5043_PKTSTOREFLAGS
                                    804 	.globl _AX5043_PKTMISCFLAGS
                                    805 	.globl _AX5043_PKTCHUNKSIZE
                                    806 	.globl _AX5043_PKTACCEPTFLAGS
                                    807 	.globl _AX5043_PINSTATE
                                    808 	.globl _AX5043_PINFUNCSYSCLK
                                    809 	.globl _AX5043_PINFUNCPWRAMP
                                    810 	.globl _AX5043_PINFUNCIRQ
                                    811 	.globl _AX5043_PINFUNCDCLK
                                    812 	.globl _AX5043_PINFUNCDATA
                                    813 	.globl _AX5043_PINFUNCANTSEL
                                    814 	.globl _AX5043_MODULATION
                                    815 	.globl _AX5043_MODCFGF
                                    816 	.globl _AX5043_MODCFGA
                                    817 	.globl _AX5043_MAXRFOFFSET2
                                    818 	.globl _AX5043_MAXRFOFFSET1
                                    819 	.globl _AX5043_MAXRFOFFSET0
                                    820 	.globl _AX5043_MAXDROFFSET2
                                    821 	.globl _AX5043_MAXDROFFSET1
                                    822 	.globl _AX5043_MAXDROFFSET0
                                    823 	.globl _AX5043_MATCH1PAT1
                                    824 	.globl _AX5043_MATCH1PAT0
                                    825 	.globl _AX5043_MATCH1MIN
                                    826 	.globl _AX5043_MATCH1MAX
                                    827 	.globl _AX5043_MATCH1LEN
                                    828 	.globl _AX5043_MATCH0PAT3
                                    829 	.globl _AX5043_MATCH0PAT2
                                    830 	.globl _AX5043_MATCH0PAT1
                                    831 	.globl _AX5043_MATCH0PAT0
                                    832 	.globl _AX5043_MATCH0MIN
                                    833 	.globl _AX5043_MATCH0MAX
                                    834 	.globl _AX5043_MATCH0LEN
                                    835 	.globl _AX5043_LPOSCSTATUS
                                    836 	.globl _AX5043_LPOSCREF1
                                    837 	.globl _AX5043_LPOSCREF0
                                    838 	.globl _AX5043_LPOSCPER1
                                    839 	.globl _AX5043_LPOSCPER0
                                    840 	.globl _AX5043_LPOSCKFILT1
                                    841 	.globl _AX5043_LPOSCKFILT0
                                    842 	.globl _AX5043_LPOSCFREQ1
                                    843 	.globl _AX5043_LPOSCFREQ0
                                    844 	.globl _AX5043_LPOSCCONFIG
                                    845 	.globl _AX5043_IRQREQUEST1
                                    846 	.globl _AX5043_IRQREQUEST0
                                    847 	.globl _AX5043_IRQMASK1
                                    848 	.globl _AX5043_IRQMASK0
                                    849 	.globl _AX5043_IRQINVERSION1
                                    850 	.globl _AX5043_IRQINVERSION0
                                    851 	.globl _AX5043_IFFREQ1
                                    852 	.globl _AX5043_IFFREQ0
                                    853 	.globl _AX5043_GPADCPERIOD
                                    854 	.globl _AX5043_GPADCCTRL
                                    855 	.globl _AX5043_GPADC13VALUE1
                                    856 	.globl _AX5043_GPADC13VALUE0
                                    857 	.globl _AX5043_FSKDMIN1
                                    858 	.globl _AX5043_FSKDMIN0
                                    859 	.globl _AX5043_FSKDMAX1
                                    860 	.globl _AX5043_FSKDMAX0
                                    861 	.globl _AX5043_FSKDEV2
                                    862 	.globl _AX5043_FSKDEV1
                                    863 	.globl _AX5043_FSKDEV0
                                    864 	.globl _AX5043_FREQB3
                                    865 	.globl _AX5043_FREQB2
                                    866 	.globl _AX5043_FREQB1
                                    867 	.globl _AX5043_FREQB0
                                    868 	.globl _AX5043_FREQA3
                                    869 	.globl _AX5043_FREQA2
                                    870 	.globl _AX5043_FREQA1
                                    871 	.globl _AX5043_FREQA0
                                    872 	.globl _AX5043_FRAMING
                                    873 	.globl _AX5043_FIFOTHRESH1
                                    874 	.globl _AX5043_FIFOTHRESH0
                                    875 	.globl _AX5043_FIFOSTAT
                                    876 	.globl _AX5043_FIFOFREE1
                                    877 	.globl _AX5043_FIFOFREE0
                                    878 	.globl _AX5043_FIFODATA
                                    879 	.globl _AX5043_FIFOCOUNT1
                                    880 	.globl _AX5043_FIFOCOUNT0
                                    881 	.globl _AX5043_FECSYNC
                                    882 	.globl _AX5043_FECSTATUS
                                    883 	.globl _AX5043_FEC
                                    884 	.globl _AX5043_ENCODING
                                    885 	.globl _AX5043_DIVERSITY
                                    886 	.globl _AX5043_DECIMATION
                                    887 	.globl _AX5043_DACVALUE1
                                    888 	.globl _AX5043_DACVALUE0
                                    889 	.globl _AX5043_DACCONFIG
                                    890 	.globl _AX5043_CRCINIT3
                                    891 	.globl _AX5043_CRCINIT2
                                    892 	.globl _AX5043_CRCINIT1
                                    893 	.globl _AX5043_CRCINIT0
                                    894 	.globl _AX5043_BGNDRSSITHR
                                    895 	.globl _AX5043_BGNDRSSIGAIN
                                    896 	.globl _AX5043_BGNDRSSI
                                    897 	.globl _AX5043_BBTUNE
                                    898 	.globl _AX5043_BBOFFSCAP
                                    899 	.globl _AX5043_AMPLFILTER
                                    900 	.globl _AX5043_AGCCOUNTER
                                    901 	.globl _AX5043_AFSKSPACE1
                                    902 	.globl _AX5043_AFSKSPACE0
                                    903 	.globl _AX5043_AFSKMARK1
                                    904 	.globl _AX5043_AFSKMARK0
                                    905 	.globl _AX5043_AFSKCTRL
                                    906 	.globl _XWTSTAT
                                    907 	.globl _XWTIRQEN
                                    908 	.globl _XWTEVTD
                                    909 	.globl _XWTEVTD1
                                    910 	.globl _XWTEVTD0
                                    911 	.globl _XWTEVTC
                                    912 	.globl _XWTEVTC1
                                    913 	.globl _XWTEVTC0
                                    914 	.globl _XWTEVTB
                                    915 	.globl _XWTEVTB1
                                    916 	.globl _XWTEVTB0
                                    917 	.globl _XWTEVTA
                                    918 	.globl _XWTEVTA1
                                    919 	.globl _XWTEVTA0
                                    920 	.globl _XWTCNTR1
                                    921 	.globl _XWTCNTB
                                    922 	.globl _XWTCNTB1
                                    923 	.globl _XWTCNTB0
                                    924 	.globl _XWTCNTA
                                    925 	.globl _XWTCNTA1
                                    926 	.globl _XWTCNTA0
                                    927 	.globl _XWTCFGB
                                    928 	.globl _XWTCFGA
                                    929 	.globl _XWDTRESET
                                    930 	.globl _XWDTCFG
                                    931 	.globl _XU1STATUS
                                    932 	.globl _XU1SHREG
                                    933 	.globl _XU1MODE
                                    934 	.globl _XU1CTRL
                                    935 	.globl _XU0STATUS
                                    936 	.globl _XU0SHREG
                                    937 	.globl _XU0MODE
                                    938 	.globl _XU0CTRL
                                    939 	.globl _XT2STATUS
                                    940 	.globl _XT2PERIOD
                                    941 	.globl _XT2PERIOD1
                                    942 	.globl _XT2PERIOD0
                                    943 	.globl _XT2MODE
                                    944 	.globl _XT2CNT
                                    945 	.globl _XT2CNT1
                                    946 	.globl _XT2CNT0
                                    947 	.globl _XT2CLKSRC
                                    948 	.globl _XT1STATUS
                                    949 	.globl _XT1PERIOD
                                    950 	.globl _XT1PERIOD1
                                    951 	.globl _XT1PERIOD0
                                    952 	.globl _XT1MODE
                                    953 	.globl _XT1CNT
                                    954 	.globl _XT1CNT1
                                    955 	.globl _XT1CNT0
                                    956 	.globl _XT1CLKSRC
                                    957 	.globl _XT0STATUS
                                    958 	.globl _XT0PERIOD
                                    959 	.globl _XT0PERIOD1
                                    960 	.globl _XT0PERIOD0
                                    961 	.globl _XT0MODE
                                    962 	.globl _XT0CNT
                                    963 	.globl _XT0CNT1
                                    964 	.globl _XT0CNT0
                                    965 	.globl _XT0CLKSRC
                                    966 	.globl _XSPSTATUS
                                    967 	.globl _XSPSHREG
                                    968 	.globl _XSPMODE
                                    969 	.globl _XSPCLKSRC
                                    970 	.globl _XRADIOSTAT
                                    971 	.globl _XRADIOSTAT1
                                    972 	.globl _XRADIOSTAT0
                                    973 	.globl _XRADIODATA3
                                    974 	.globl _XRADIODATA2
                                    975 	.globl _XRADIODATA1
                                    976 	.globl _XRADIODATA0
                                    977 	.globl _XRADIOADDR1
                                    978 	.globl _XRADIOADDR0
                                    979 	.globl _XRADIOACC
                                    980 	.globl _XOC1STATUS
                                    981 	.globl _XOC1PIN
                                    982 	.globl _XOC1MODE
                                    983 	.globl _XOC1COMP
                                    984 	.globl _XOC1COMP1
                                    985 	.globl _XOC1COMP0
                                    986 	.globl _XOC0STATUS
                                    987 	.globl _XOC0PIN
                                    988 	.globl _XOC0MODE
                                    989 	.globl _XOC0COMP
                                    990 	.globl _XOC0COMP1
                                    991 	.globl _XOC0COMP0
                                    992 	.globl _XNVSTATUS
                                    993 	.globl _XNVKEY
                                    994 	.globl _XNVDATA
                                    995 	.globl _XNVDATA1
                                    996 	.globl _XNVDATA0
                                    997 	.globl _XNVADDR
                                    998 	.globl _XNVADDR1
                                    999 	.globl _XNVADDR0
                                   1000 	.globl _XIC1STATUS
                                   1001 	.globl _XIC1MODE
                                   1002 	.globl _XIC1CAPT
                                   1003 	.globl _XIC1CAPT1
                                   1004 	.globl _XIC1CAPT0
                                   1005 	.globl _XIC0STATUS
                                   1006 	.globl _XIC0MODE
                                   1007 	.globl _XIC0CAPT
                                   1008 	.globl _XIC0CAPT1
                                   1009 	.globl _XIC0CAPT0
                                   1010 	.globl _XPORTR
                                   1011 	.globl _XPORTC
                                   1012 	.globl _XPORTB
                                   1013 	.globl _XPORTA
                                   1014 	.globl _XPINR
                                   1015 	.globl _XPINC
                                   1016 	.globl _XPINB
                                   1017 	.globl _XPINA
                                   1018 	.globl _XDIRR
                                   1019 	.globl _XDIRC
                                   1020 	.globl _XDIRB
                                   1021 	.globl _XDIRA
                                   1022 	.globl _XDBGLNKSTAT
                                   1023 	.globl _XDBGLNKBUF
                                   1024 	.globl _XCODECONFIG
                                   1025 	.globl _XCLKSTAT
                                   1026 	.globl _XCLKCON
                                   1027 	.globl _XANALOGCOMP
                                   1028 	.globl _XADCCONV
                                   1029 	.globl _XADCCLKSRC
                                   1030 	.globl _XADCCH3CONFIG
                                   1031 	.globl _XADCCH2CONFIG
                                   1032 	.globl _XADCCH1CONFIG
                                   1033 	.globl _XADCCH0CONFIG
                                   1034 	.globl _XPCON
                                   1035 	.globl _XIP
                                   1036 	.globl _XIE
                                   1037 	.globl _XDPTR1
                                   1038 	.globl _XDPTR0
                                   1039 	.globl _RNGCLKSRC1
                                   1040 	.globl _RNGCLKSRC0
                                   1041 	.globl _RNGMODE
                                   1042 	.globl _AESOUTADDR
                                   1043 	.globl _AESOUTADDR1
                                   1044 	.globl _AESOUTADDR0
                                   1045 	.globl _AESMODE
                                   1046 	.globl _AESKEYADDR
                                   1047 	.globl _AESKEYADDR1
                                   1048 	.globl _AESKEYADDR0
                                   1049 	.globl _AESINADDR
                                   1050 	.globl _AESINADDR1
                                   1051 	.globl _AESINADDR0
                                   1052 	.globl _AESCURBLOCK
                                   1053 	.globl _AESCONFIG
                                   1054 	.globl _RNGBYTE
                                   1055 	.globl _XTALREADY
                                   1056 	.globl _XTALOSC
                                   1057 	.globl _XTALAMPL
                                   1058 	.globl _SILICONREV
                                   1059 	.globl _SCRATCH3
                                   1060 	.globl _SCRATCH2
                                   1061 	.globl _SCRATCH1
                                   1062 	.globl _SCRATCH0
                                   1063 	.globl _RADIOMUX
                                   1064 	.globl _RADIOFSTATADDR
                                   1065 	.globl _RADIOFSTATADDR1
                                   1066 	.globl _RADIOFSTATADDR0
                                   1067 	.globl _RADIOFDATAADDR
                                   1068 	.globl _RADIOFDATAADDR1
                                   1069 	.globl _RADIOFDATAADDR0
                                   1070 	.globl _OSCRUN
                                   1071 	.globl _OSCREADY
                                   1072 	.globl _OSCFORCERUN
                                   1073 	.globl _OSCCALIB
                                   1074 	.globl _MISCCTRL
                                   1075 	.globl _LPXOSCGM
                                   1076 	.globl _LPOSCREF
                                   1077 	.globl _LPOSCREF1
                                   1078 	.globl _LPOSCREF0
                                   1079 	.globl _LPOSCPER
                                   1080 	.globl _LPOSCPER1
                                   1081 	.globl _LPOSCPER0
                                   1082 	.globl _LPOSCKFILT
                                   1083 	.globl _LPOSCKFILT1
                                   1084 	.globl _LPOSCKFILT0
                                   1085 	.globl _LPOSCFREQ
                                   1086 	.globl _LPOSCFREQ1
                                   1087 	.globl _LPOSCFREQ0
                                   1088 	.globl _LPOSCCONFIG
                                   1089 	.globl _PINSEL
                                   1090 	.globl _PINCHGC
                                   1091 	.globl _PINCHGB
                                   1092 	.globl _PINCHGA
                                   1093 	.globl _PALTRADIO
                                   1094 	.globl _PALTC
                                   1095 	.globl _PALTB
                                   1096 	.globl _PALTA
                                   1097 	.globl _INTCHGC
                                   1098 	.globl _INTCHGB
                                   1099 	.globl _INTCHGA
                                   1100 	.globl _EXTIRQ
                                   1101 	.globl _GPIOENABLE
                                   1102 	.globl _ANALOGA
                                   1103 	.globl _FRCOSCREF
                                   1104 	.globl _FRCOSCREF1
                                   1105 	.globl _FRCOSCREF0
                                   1106 	.globl _FRCOSCPER
                                   1107 	.globl _FRCOSCPER1
                                   1108 	.globl _FRCOSCPER0
                                   1109 	.globl _FRCOSCKFILT
                                   1110 	.globl _FRCOSCKFILT1
                                   1111 	.globl _FRCOSCKFILT0
                                   1112 	.globl _FRCOSCFREQ
                                   1113 	.globl _FRCOSCFREQ1
                                   1114 	.globl _FRCOSCFREQ0
                                   1115 	.globl _FRCOSCCTRL
                                   1116 	.globl _FRCOSCCONFIG
                                   1117 	.globl _DMA1CONFIG
                                   1118 	.globl _DMA1ADDR
                                   1119 	.globl _DMA1ADDR1
                                   1120 	.globl _DMA1ADDR0
                                   1121 	.globl _DMA0CONFIG
                                   1122 	.globl _DMA0ADDR
                                   1123 	.globl _DMA0ADDR1
                                   1124 	.globl _DMA0ADDR0
                                   1125 	.globl _ADCTUNE2
                                   1126 	.globl _ADCTUNE1
                                   1127 	.globl _ADCTUNE0
                                   1128 	.globl _ADCCH3VAL
                                   1129 	.globl _ADCCH3VAL1
                                   1130 	.globl _ADCCH3VAL0
                                   1131 	.globl _ADCCH2VAL
                                   1132 	.globl _ADCCH2VAL1
                                   1133 	.globl _ADCCH2VAL0
                                   1134 	.globl _ADCCH1VAL
                                   1135 	.globl _ADCCH1VAL1
                                   1136 	.globl _ADCCH1VAL0
                                   1137 	.globl _ADCCH0VAL
                                   1138 	.globl _ADCCH0VAL1
                                   1139 	.globl _ADCCH0VAL0
                                   1140 	.globl _ax5043_set_registers
                                   1141 	.globl _ax5043_set_registers_tx
                                   1142 	.globl _ax5043_set_registers_rx
                                   1143 	.globl _ax5043_set_registers_rxwor
                                   1144 	.globl _ax5043_set_registers_rxcont
                                   1145 	.globl _ax5043_set_registers_rxcont_singleparamset
                                   1146 	.globl _axradio_setup_pincfg1
                                   1147 	.globl _axradio_setup_pincfg2
                                   1148 	.globl _axradio_conv_freq_fromhz
                                   1149 	.globl _axradio_conv_freq_tohz
                                   1150 	.globl _axradio_conv_freq_fromreg
                                   1151 	.globl _axradio_conv_timeinterval_totimer0
                                   1152 	.globl _axradio_framing_check_crc
                                   1153 	.globl _axradio_framing_append_crc
                                   1154 ;--------------------------------------------------------
                                   1155 ; special function registers
                                   1156 ;--------------------------------------------------------
                                   1157 	.area RSEG    (ABS,DATA)
      000000                       1158 	.org 0x0000
                           0000E0  1159 _ACC	=	0x00e0
                           0000F0  1160 _B	=	0x00f0
                           000083  1161 _DPH	=	0x0083
                           000085  1162 _DPH1	=	0x0085
                           000082  1163 _DPL	=	0x0082
                           000084  1164 _DPL1	=	0x0084
                           008382  1165 _DPTR0	=	0x8382
                           008584  1166 _DPTR1	=	0x8584
                           000086  1167 _DPS	=	0x0086
                           0000A0  1168 _E2IE	=	0x00a0
                           0000C0  1169 _E2IP	=	0x00c0
                           000098  1170 _EIE	=	0x0098
                           0000B0  1171 _EIP	=	0x00b0
                           0000A8  1172 _IE	=	0x00a8
                           0000B8  1173 _IP	=	0x00b8
                           000087  1174 _PCON	=	0x0087
                           0000D0  1175 _PSW	=	0x00d0
                           000081  1176 _SP	=	0x0081
                           0000D9  1177 _XPAGE	=	0x00d9
                           0000D9  1178 __XPAGE	=	0x00d9
                           0000CA  1179 _ADCCH0CONFIG	=	0x00ca
                           0000CB  1180 _ADCCH1CONFIG	=	0x00cb
                           0000D2  1181 _ADCCH2CONFIG	=	0x00d2
                           0000D3  1182 _ADCCH3CONFIG	=	0x00d3
                           0000D1  1183 _ADCCLKSRC	=	0x00d1
                           0000C9  1184 _ADCCONV	=	0x00c9
                           0000E1  1185 _ANALOGCOMP	=	0x00e1
                           0000C6  1186 _CLKCON	=	0x00c6
                           0000C7  1187 _CLKSTAT	=	0x00c7
                           000097  1188 _CODECONFIG	=	0x0097
                           0000E3  1189 _DBGLNKBUF	=	0x00e3
                           0000E2  1190 _DBGLNKSTAT	=	0x00e2
                           000089  1191 _DIRA	=	0x0089
                           00008A  1192 _DIRB	=	0x008a
                           00008B  1193 _DIRC	=	0x008b
                           00008E  1194 _DIRR	=	0x008e
                           0000C8  1195 _PINA	=	0x00c8
                           0000E8  1196 _PINB	=	0x00e8
                           0000F8  1197 _PINC	=	0x00f8
                           00008D  1198 _PINR	=	0x008d
                           000080  1199 _PORTA	=	0x0080
                           000088  1200 _PORTB	=	0x0088
                           000090  1201 _PORTC	=	0x0090
                           00008C  1202 _PORTR	=	0x008c
                           0000CE  1203 _IC0CAPT0	=	0x00ce
                           0000CF  1204 _IC0CAPT1	=	0x00cf
                           00CFCE  1205 _IC0CAPT	=	0xcfce
                           0000CC  1206 _IC0MODE	=	0x00cc
                           0000CD  1207 _IC0STATUS	=	0x00cd
                           0000D6  1208 _IC1CAPT0	=	0x00d6
                           0000D7  1209 _IC1CAPT1	=	0x00d7
                           00D7D6  1210 _IC1CAPT	=	0xd7d6
                           0000D4  1211 _IC1MODE	=	0x00d4
                           0000D5  1212 _IC1STATUS	=	0x00d5
                           000092  1213 _NVADDR0	=	0x0092
                           000093  1214 _NVADDR1	=	0x0093
                           009392  1215 _NVADDR	=	0x9392
                           000094  1216 _NVDATA0	=	0x0094
                           000095  1217 _NVDATA1	=	0x0095
                           009594  1218 _NVDATA	=	0x9594
                           000096  1219 _NVKEY	=	0x0096
                           000091  1220 _NVSTATUS	=	0x0091
                           0000BC  1221 _OC0COMP0	=	0x00bc
                           0000BD  1222 _OC0COMP1	=	0x00bd
                           00BDBC  1223 _OC0COMP	=	0xbdbc
                           0000B9  1224 _OC0MODE	=	0x00b9
                           0000BA  1225 _OC0PIN	=	0x00ba
                           0000BB  1226 _OC0STATUS	=	0x00bb
                           0000C4  1227 _OC1COMP0	=	0x00c4
                           0000C5  1228 _OC1COMP1	=	0x00c5
                           00C5C4  1229 _OC1COMP	=	0xc5c4
                           0000C1  1230 _OC1MODE	=	0x00c1
                           0000C2  1231 _OC1PIN	=	0x00c2
                           0000C3  1232 _OC1STATUS	=	0x00c3
                           0000B1  1233 _RADIOACC	=	0x00b1
                           0000B3  1234 _RADIOADDR0	=	0x00b3
                           0000B2  1235 _RADIOADDR1	=	0x00b2
                           00B2B3  1236 _RADIOADDR	=	0xb2b3
                           0000B7  1237 _RADIODATA0	=	0x00b7
                           0000B6  1238 _RADIODATA1	=	0x00b6
                           0000B5  1239 _RADIODATA2	=	0x00b5
                           0000B4  1240 _RADIODATA3	=	0x00b4
                           B4B5B6B7  1241 _RADIODATA	=	0xb4b5b6b7
                           0000BE  1242 _RADIOSTAT0	=	0x00be
                           0000BF  1243 _RADIOSTAT1	=	0x00bf
                           00BFBE  1244 _RADIOSTAT	=	0xbfbe
                           0000DF  1245 _SPCLKSRC	=	0x00df
                           0000DC  1246 _SPMODE	=	0x00dc
                           0000DE  1247 _SPSHREG	=	0x00de
                           0000DD  1248 _SPSTATUS	=	0x00dd
                           00009A  1249 _T0CLKSRC	=	0x009a
                           00009C  1250 _T0CNT0	=	0x009c
                           00009D  1251 _T0CNT1	=	0x009d
                           009D9C  1252 _T0CNT	=	0x9d9c
                           000099  1253 _T0MODE	=	0x0099
                           00009E  1254 _T0PERIOD0	=	0x009e
                           00009F  1255 _T0PERIOD1	=	0x009f
                           009F9E  1256 _T0PERIOD	=	0x9f9e
                           00009B  1257 _T0STATUS	=	0x009b
                           0000A2  1258 _T1CLKSRC	=	0x00a2
                           0000A4  1259 _T1CNT0	=	0x00a4
                           0000A5  1260 _T1CNT1	=	0x00a5
                           00A5A4  1261 _T1CNT	=	0xa5a4
                           0000A1  1262 _T1MODE	=	0x00a1
                           0000A6  1263 _T1PERIOD0	=	0x00a6
                           0000A7  1264 _T1PERIOD1	=	0x00a7
                           00A7A6  1265 _T1PERIOD	=	0xa7a6
                           0000A3  1266 _T1STATUS	=	0x00a3
                           0000AA  1267 _T2CLKSRC	=	0x00aa
                           0000AC  1268 _T2CNT0	=	0x00ac
                           0000AD  1269 _T2CNT1	=	0x00ad
                           00ADAC  1270 _T2CNT	=	0xadac
                           0000A9  1271 _T2MODE	=	0x00a9
                           0000AE  1272 _T2PERIOD0	=	0x00ae
                           0000AF  1273 _T2PERIOD1	=	0x00af
                           00AFAE  1274 _T2PERIOD	=	0xafae
                           0000AB  1275 _T2STATUS	=	0x00ab
                           0000E4  1276 _U0CTRL	=	0x00e4
                           0000E7  1277 _U0MODE	=	0x00e7
                           0000E6  1278 _U0SHREG	=	0x00e6
                           0000E5  1279 _U0STATUS	=	0x00e5
                           0000EC  1280 _U1CTRL	=	0x00ec
                           0000EF  1281 _U1MODE	=	0x00ef
                           0000EE  1282 _U1SHREG	=	0x00ee
                           0000ED  1283 _U1STATUS	=	0x00ed
                           0000DA  1284 _WDTCFG	=	0x00da
                           0000DB  1285 _WDTRESET	=	0x00db
                           0000F1  1286 _WTCFGA	=	0x00f1
                           0000F9  1287 _WTCFGB	=	0x00f9
                           0000F2  1288 _WTCNTA0	=	0x00f2
                           0000F3  1289 _WTCNTA1	=	0x00f3
                           00F3F2  1290 _WTCNTA	=	0xf3f2
                           0000FA  1291 _WTCNTB0	=	0x00fa
                           0000FB  1292 _WTCNTB1	=	0x00fb
                           00FBFA  1293 _WTCNTB	=	0xfbfa
                           0000EB  1294 _WTCNTR1	=	0x00eb
                           0000F4  1295 _WTEVTA0	=	0x00f4
                           0000F5  1296 _WTEVTA1	=	0x00f5
                           00F5F4  1297 _WTEVTA	=	0xf5f4
                           0000F6  1298 _WTEVTB0	=	0x00f6
                           0000F7  1299 _WTEVTB1	=	0x00f7
                           00F7F6  1300 _WTEVTB	=	0xf7f6
                           0000FC  1301 _WTEVTC0	=	0x00fc
                           0000FD  1302 _WTEVTC1	=	0x00fd
                           00FDFC  1303 _WTEVTC	=	0xfdfc
                           0000FE  1304 _WTEVTD0	=	0x00fe
                           0000FF  1305 _WTEVTD1	=	0x00ff
                           00FFFE  1306 _WTEVTD	=	0xfffe
                           0000E9  1307 _WTIRQEN	=	0x00e9
                           0000EA  1308 _WTSTAT	=	0x00ea
                                   1309 ;--------------------------------------------------------
                                   1310 ; special function bits
                                   1311 ;--------------------------------------------------------
                                   1312 	.area RSEG    (ABS,DATA)
      000000                       1313 	.org 0x0000
                           0000E0  1314 _ACC_0	=	0x00e0
                           0000E1  1315 _ACC_1	=	0x00e1
                           0000E2  1316 _ACC_2	=	0x00e2
                           0000E3  1317 _ACC_3	=	0x00e3
                           0000E4  1318 _ACC_4	=	0x00e4
                           0000E5  1319 _ACC_5	=	0x00e5
                           0000E6  1320 _ACC_6	=	0x00e6
                           0000E7  1321 _ACC_7	=	0x00e7
                           0000F0  1322 _B_0	=	0x00f0
                           0000F1  1323 _B_1	=	0x00f1
                           0000F2  1324 _B_2	=	0x00f2
                           0000F3  1325 _B_3	=	0x00f3
                           0000F4  1326 _B_4	=	0x00f4
                           0000F5  1327 _B_5	=	0x00f5
                           0000F6  1328 _B_6	=	0x00f6
                           0000F7  1329 _B_7	=	0x00f7
                           0000A0  1330 _E2IE_0	=	0x00a0
                           0000A1  1331 _E2IE_1	=	0x00a1
                           0000A2  1332 _E2IE_2	=	0x00a2
                           0000A3  1333 _E2IE_3	=	0x00a3
                           0000A4  1334 _E2IE_4	=	0x00a4
                           0000A5  1335 _E2IE_5	=	0x00a5
                           0000A6  1336 _E2IE_6	=	0x00a6
                           0000A7  1337 _E2IE_7	=	0x00a7
                           0000C0  1338 _E2IP_0	=	0x00c0
                           0000C1  1339 _E2IP_1	=	0x00c1
                           0000C2  1340 _E2IP_2	=	0x00c2
                           0000C3  1341 _E2IP_3	=	0x00c3
                           0000C4  1342 _E2IP_4	=	0x00c4
                           0000C5  1343 _E2IP_5	=	0x00c5
                           0000C6  1344 _E2IP_6	=	0x00c6
                           0000C7  1345 _E2IP_7	=	0x00c7
                           000098  1346 _EIE_0	=	0x0098
                           000099  1347 _EIE_1	=	0x0099
                           00009A  1348 _EIE_2	=	0x009a
                           00009B  1349 _EIE_3	=	0x009b
                           00009C  1350 _EIE_4	=	0x009c
                           00009D  1351 _EIE_5	=	0x009d
                           00009E  1352 _EIE_6	=	0x009e
                           00009F  1353 _EIE_7	=	0x009f
                           0000B0  1354 _EIP_0	=	0x00b0
                           0000B1  1355 _EIP_1	=	0x00b1
                           0000B2  1356 _EIP_2	=	0x00b2
                           0000B3  1357 _EIP_3	=	0x00b3
                           0000B4  1358 _EIP_4	=	0x00b4
                           0000B5  1359 _EIP_5	=	0x00b5
                           0000B6  1360 _EIP_6	=	0x00b6
                           0000B7  1361 _EIP_7	=	0x00b7
                           0000A8  1362 _IE_0	=	0x00a8
                           0000A9  1363 _IE_1	=	0x00a9
                           0000AA  1364 _IE_2	=	0x00aa
                           0000AB  1365 _IE_3	=	0x00ab
                           0000AC  1366 _IE_4	=	0x00ac
                           0000AD  1367 _IE_5	=	0x00ad
                           0000AE  1368 _IE_6	=	0x00ae
                           0000AF  1369 _IE_7	=	0x00af
                           0000AF  1370 _EA	=	0x00af
                           0000B8  1371 _IP_0	=	0x00b8
                           0000B9  1372 _IP_1	=	0x00b9
                           0000BA  1373 _IP_2	=	0x00ba
                           0000BB  1374 _IP_3	=	0x00bb
                           0000BC  1375 _IP_4	=	0x00bc
                           0000BD  1376 _IP_5	=	0x00bd
                           0000BE  1377 _IP_6	=	0x00be
                           0000BF  1378 _IP_7	=	0x00bf
                           0000D0  1379 _P	=	0x00d0
                           0000D1  1380 _F1	=	0x00d1
                           0000D2  1381 _OV	=	0x00d2
                           0000D3  1382 _RS0	=	0x00d3
                           0000D4  1383 _RS1	=	0x00d4
                           0000D5  1384 _F0	=	0x00d5
                           0000D6  1385 _AC	=	0x00d6
                           0000D7  1386 _CY	=	0x00d7
                           0000C8  1387 _PINA_0	=	0x00c8
                           0000C9  1388 _PINA_1	=	0x00c9
                           0000CA  1389 _PINA_2	=	0x00ca
                           0000CB  1390 _PINA_3	=	0x00cb
                           0000CC  1391 _PINA_4	=	0x00cc
                           0000CD  1392 _PINA_5	=	0x00cd
                           0000CE  1393 _PINA_6	=	0x00ce
                           0000CF  1394 _PINA_7	=	0x00cf
                           0000E8  1395 _PINB_0	=	0x00e8
                           0000E9  1396 _PINB_1	=	0x00e9
                           0000EA  1397 _PINB_2	=	0x00ea
                           0000EB  1398 _PINB_3	=	0x00eb
                           0000EC  1399 _PINB_4	=	0x00ec
                           0000ED  1400 _PINB_5	=	0x00ed
                           0000EE  1401 _PINB_6	=	0x00ee
                           0000EF  1402 _PINB_7	=	0x00ef
                           0000F8  1403 _PINC_0	=	0x00f8
                           0000F9  1404 _PINC_1	=	0x00f9
                           0000FA  1405 _PINC_2	=	0x00fa
                           0000FB  1406 _PINC_3	=	0x00fb
                           0000FC  1407 _PINC_4	=	0x00fc
                           0000FD  1408 _PINC_5	=	0x00fd
                           0000FE  1409 _PINC_6	=	0x00fe
                           0000FF  1410 _PINC_7	=	0x00ff
                           000080  1411 _PORTA_0	=	0x0080
                           000081  1412 _PORTA_1	=	0x0081
                           000082  1413 _PORTA_2	=	0x0082
                           000083  1414 _PORTA_3	=	0x0083
                           000084  1415 _PORTA_4	=	0x0084
                           000085  1416 _PORTA_5	=	0x0085
                           000086  1417 _PORTA_6	=	0x0086
                           000087  1418 _PORTA_7	=	0x0087
                           000088  1419 _PORTB_0	=	0x0088
                           000089  1420 _PORTB_1	=	0x0089
                           00008A  1421 _PORTB_2	=	0x008a
                           00008B  1422 _PORTB_3	=	0x008b
                           00008C  1423 _PORTB_4	=	0x008c
                           00008D  1424 _PORTB_5	=	0x008d
                           00008E  1425 _PORTB_6	=	0x008e
                           00008F  1426 _PORTB_7	=	0x008f
                           000090  1427 _PORTC_0	=	0x0090
                           000091  1428 _PORTC_1	=	0x0091
                           000092  1429 _PORTC_2	=	0x0092
                           000093  1430 _PORTC_3	=	0x0093
                           000094  1431 _PORTC_4	=	0x0094
                           000095  1432 _PORTC_5	=	0x0095
                           000096  1433 _PORTC_6	=	0x0096
                           000097  1434 _PORTC_7	=	0x0097
                                   1435 ;--------------------------------------------------------
                                   1436 ; overlayable register banks
                                   1437 ;--------------------------------------------------------
                                   1438 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                       1439 	.ds 8
                                   1440 ;--------------------------------------------------------
                                   1441 ; internal ram data
                                   1442 ;--------------------------------------------------------
                                   1443 	.area DSEG    (DATA)
                                   1444 ;--------------------------------------------------------
                                   1445 ; overlayable items in internal ram 
                                   1446 ;--------------------------------------------------------
                                   1447 ;--------------------------------------------------------
                                   1448 ; indirectly addressable internal ram data
                                   1449 ;--------------------------------------------------------
                                   1450 	.area ISEG    (DATA)
                                   1451 ;--------------------------------------------------------
                                   1452 ; absolute internal ram data
                                   1453 ;--------------------------------------------------------
                                   1454 	.area IABS    (ABS,DATA)
                                   1455 	.area IABS    (ABS,DATA)
                                   1456 ;--------------------------------------------------------
                                   1457 ; bit data
                                   1458 ;--------------------------------------------------------
                                   1459 	.area BSEG    (BIT)
                                   1460 ;--------------------------------------------------------
                                   1461 ; paged external ram data
                                   1462 ;--------------------------------------------------------
                                   1463 	.area PSEG    (PAG,XDATA)
                                   1464 ;--------------------------------------------------------
                                   1465 ; external ram data
                                   1466 ;--------------------------------------------------------
                                   1467 	.area XSEG    (XDATA)
                           007020  1468 _ADCCH0VAL0	=	0x7020
                           007021  1469 _ADCCH0VAL1	=	0x7021
                           007020  1470 _ADCCH0VAL	=	0x7020
                           007022  1471 _ADCCH1VAL0	=	0x7022
                           007023  1472 _ADCCH1VAL1	=	0x7023
                           007022  1473 _ADCCH1VAL	=	0x7022
                           007024  1474 _ADCCH2VAL0	=	0x7024
                           007025  1475 _ADCCH2VAL1	=	0x7025
                           007024  1476 _ADCCH2VAL	=	0x7024
                           007026  1477 _ADCCH3VAL0	=	0x7026
                           007027  1478 _ADCCH3VAL1	=	0x7027
                           007026  1479 _ADCCH3VAL	=	0x7026
                           007028  1480 _ADCTUNE0	=	0x7028
                           007029  1481 _ADCTUNE1	=	0x7029
                           00702A  1482 _ADCTUNE2	=	0x702a
                           007010  1483 _DMA0ADDR0	=	0x7010
                           007011  1484 _DMA0ADDR1	=	0x7011
                           007010  1485 _DMA0ADDR	=	0x7010
                           007014  1486 _DMA0CONFIG	=	0x7014
                           007012  1487 _DMA1ADDR0	=	0x7012
                           007013  1488 _DMA1ADDR1	=	0x7013
                           007012  1489 _DMA1ADDR	=	0x7012
                           007015  1490 _DMA1CONFIG	=	0x7015
                           007070  1491 _FRCOSCCONFIG	=	0x7070
                           007071  1492 _FRCOSCCTRL	=	0x7071
                           007076  1493 _FRCOSCFREQ0	=	0x7076
                           007077  1494 _FRCOSCFREQ1	=	0x7077
                           007076  1495 _FRCOSCFREQ	=	0x7076
                           007072  1496 _FRCOSCKFILT0	=	0x7072
                           007073  1497 _FRCOSCKFILT1	=	0x7073
                           007072  1498 _FRCOSCKFILT	=	0x7072
                           007078  1499 _FRCOSCPER0	=	0x7078
                           007079  1500 _FRCOSCPER1	=	0x7079
                           007078  1501 _FRCOSCPER	=	0x7078
                           007074  1502 _FRCOSCREF0	=	0x7074
                           007075  1503 _FRCOSCREF1	=	0x7075
                           007074  1504 _FRCOSCREF	=	0x7074
                           007007  1505 _ANALOGA	=	0x7007
                           00700C  1506 _GPIOENABLE	=	0x700c
                           007003  1507 _EXTIRQ	=	0x7003
                           007000  1508 _INTCHGA	=	0x7000
                           007001  1509 _INTCHGB	=	0x7001
                           007002  1510 _INTCHGC	=	0x7002
                           007008  1511 _PALTA	=	0x7008
                           007009  1512 _PALTB	=	0x7009
                           00700A  1513 _PALTC	=	0x700a
                           007046  1514 _PALTRADIO	=	0x7046
                           007004  1515 _PINCHGA	=	0x7004
                           007005  1516 _PINCHGB	=	0x7005
                           007006  1517 _PINCHGC	=	0x7006
                           00700B  1518 _PINSEL	=	0x700b
                           007060  1519 _LPOSCCONFIG	=	0x7060
                           007066  1520 _LPOSCFREQ0	=	0x7066
                           007067  1521 _LPOSCFREQ1	=	0x7067
                           007066  1522 _LPOSCFREQ	=	0x7066
                           007062  1523 _LPOSCKFILT0	=	0x7062
                           007063  1524 _LPOSCKFILT1	=	0x7063
                           007062  1525 _LPOSCKFILT	=	0x7062
                           007068  1526 _LPOSCPER0	=	0x7068
                           007069  1527 _LPOSCPER1	=	0x7069
                           007068  1528 _LPOSCPER	=	0x7068
                           007064  1529 _LPOSCREF0	=	0x7064
                           007065  1530 _LPOSCREF1	=	0x7065
                           007064  1531 _LPOSCREF	=	0x7064
                           007054  1532 _LPXOSCGM	=	0x7054
                           007F01  1533 _MISCCTRL	=	0x7f01
                           007053  1534 _OSCCALIB	=	0x7053
                           007050  1535 _OSCFORCERUN	=	0x7050
                           007052  1536 _OSCREADY	=	0x7052
                           007051  1537 _OSCRUN	=	0x7051
                           007040  1538 _RADIOFDATAADDR0	=	0x7040
                           007041  1539 _RADIOFDATAADDR1	=	0x7041
                           007040  1540 _RADIOFDATAADDR	=	0x7040
                           007042  1541 _RADIOFSTATADDR0	=	0x7042
                           007043  1542 _RADIOFSTATADDR1	=	0x7043
                           007042  1543 _RADIOFSTATADDR	=	0x7042
                           007044  1544 _RADIOMUX	=	0x7044
                           007084  1545 _SCRATCH0	=	0x7084
                           007085  1546 _SCRATCH1	=	0x7085
                           007086  1547 _SCRATCH2	=	0x7086
                           007087  1548 _SCRATCH3	=	0x7087
                           007F00  1549 _SILICONREV	=	0x7f00
                           007F19  1550 _XTALAMPL	=	0x7f19
                           007F18  1551 _XTALOSC	=	0x7f18
                           007F1A  1552 _XTALREADY	=	0x7f1a
                           007081  1553 _RNGBYTE	=	0x7081
                           007091  1554 _AESCONFIG	=	0x7091
                           007098  1555 _AESCURBLOCK	=	0x7098
                           007094  1556 _AESINADDR0	=	0x7094
                           007095  1557 _AESINADDR1	=	0x7095
                           007094  1558 _AESINADDR	=	0x7094
                           007092  1559 _AESKEYADDR0	=	0x7092
                           007093  1560 _AESKEYADDR1	=	0x7093
                           007092  1561 _AESKEYADDR	=	0x7092
                           007090  1562 _AESMODE	=	0x7090
                           007096  1563 _AESOUTADDR0	=	0x7096
                           007097  1564 _AESOUTADDR1	=	0x7097
                           007096  1565 _AESOUTADDR	=	0x7096
                           007080  1566 _RNGMODE	=	0x7080
                           007082  1567 _RNGCLKSRC0	=	0x7082
                           007083  1568 _RNGCLKSRC1	=	0x7083
                           003F82  1569 _XDPTR0	=	0x3f82
                           003F84  1570 _XDPTR1	=	0x3f84
                           003FA8  1571 _XIE	=	0x3fa8
                           003FB8  1572 _XIP	=	0x3fb8
                           003F87  1573 _XPCON	=	0x3f87
                           003FCA  1574 _XADCCH0CONFIG	=	0x3fca
                           003FCB  1575 _XADCCH1CONFIG	=	0x3fcb
                           003FD2  1576 _XADCCH2CONFIG	=	0x3fd2
                           003FD3  1577 _XADCCH3CONFIG	=	0x3fd3
                           003FD1  1578 _XADCCLKSRC	=	0x3fd1
                           003FC9  1579 _XADCCONV	=	0x3fc9
                           003FE1  1580 _XANALOGCOMP	=	0x3fe1
                           003FC6  1581 _XCLKCON	=	0x3fc6
                           003FC7  1582 _XCLKSTAT	=	0x3fc7
                           003F97  1583 _XCODECONFIG	=	0x3f97
                           003FE3  1584 _XDBGLNKBUF	=	0x3fe3
                           003FE2  1585 _XDBGLNKSTAT	=	0x3fe2
                           003F89  1586 _XDIRA	=	0x3f89
                           003F8A  1587 _XDIRB	=	0x3f8a
                           003F8B  1588 _XDIRC	=	0x3f8b
                           003F8E  1589 _XDIRR	=	0x3f8e
                           003FC8  1590 _XPINA	=	0x3fc8
                           003FE8  1591 _XPINB	=	0x3fe8
                           003FF8  1592 _XPINC	=	0x3ff8
                           003F8D  1593 _XPINR	=	0x3f8d
                           003F80  1594 _XPORTA	=	0x3f80
                           003F88  1595 _XPORTB	=	0x3f88
                           003F90  1596 _XPORTC	=	0x3f90
                           003F8C  1597 _XPORTR	=	0x3f8c
                           003FCE  1598 _XIC0CAPT0	=	0x3fce
                           003FCF  1599 _XIC0CAPT1	=	0x3fcf
                           003FCE  1600 _XIC0CAPT	=	0x3fce
                           003FCC  1601 _XIC0MODE	=	0x3fcc
                           003FCD  1602 _XIC0STATUS	=	0x3fcd
                           003FD6  1603 _XIC1CAPT0	=	0x3fd6
                           003FD7  1604 _XIC1CAPT1	=	0x3fd7
                           003FD6  1605 _XIC1CAPT	=	0x3fd6
                           003FD4  1606 _XIC1MODE	=	0x3fd4
                           003FD5  1607 _XIC1STATUS	=	0x3fd5
                           003F92  1608 _XNVADDR0	=	0x3f92
                           003F93  1609 _XNVADDR1	=	0x3f93
                           003F92  1610 _XNVADDR	=	0x3f92
                           003F94  1611 _XNVDATA0	=	0x3f94
                           003F95  1612 _XNVDATA1	=	0x3f95
                           003F94  1613 _XNVDATA	=	0x3f94
                           003F96  1614 _XNVKEY	=	0x3f96
                           003F91  1615 _XNVSTATUS	=	0x3f91
                           003FBC  1616 _XOC0COMP0	=	0x3fbc
                           003FBD  1617 _XOC0COMP1	=	0x3fbd
                           003FBC  1618 _XOC0COMP	=	0x3fbc
                           003FB9  1619 _XOC0MODE	=	0x3fb9
                           003FBA  1620 _XOC0PIN	=	0x3fba
                           003FBB  1621 _XOC0STATUS	=	0x3fbb
                           003FC4  1622 _XOC1COMP0	=	0x3fc4
                           003FC5  1623 _XOC1COMP1	=	0x3fc5
                           003FC4  1624 _XOC1COMP	=	0x3fc4
                           003FC1  1625 _XOC1MODE	=	0x3fc1
                           003FC2  1626 _XOC1PIN	=	0x3fc2
                           003FC3  1627 _XOC1STATUS	=	0x3fc3
                           003FB1  1628 _XRADIOACC	=	0x3fb1
                           003FB3  1629 _XRADIOADDR0	=	0x3fb3
                           003FB2  1630 _XRADIOADDR1	=	0x3fb2
                           003FB7  1631 _XRADIODATA0	=	0x3fb7
                           003FB6  1632 _XRADIODATA1	=	0x3fb6
                           003FB5  1633 _XRADIODATA2	=	0x3fb5
                           003FB4  1634 _XRADIODATA3	=	0x3fb4
                           003FBE  1635 _XRADIOSTAT0	=	0x3fbe
                           003FBF  1636 _XRADIOSTAT1	=	0x3fbf
                           003FBE  1637 _XRADIOSTAT	=	0x3fbe
                           003FDF  1638 _XSPCLKSRC	=	0x3fdf
                           003FDC  1639 _XSPMODE	=	0x3fdc
                           003FDE  1640 _XSPSHREG	=	0x3fde
                           003FDD  1641 _XSPSTATUS	=	0x3fdd
                           003F9A  1642 _XT0CLKSRC	=	0x3f9a
                           003F9C  1643 _XT0CNT0	=	0x3f9c
                           003F9D  1644 _XT0CNT1	=	0x3f9d
                           003F9C  1645 _XT0CNT	=	0x3f9c
                           003F99  1646 _XT0MODE	=	0x3f99
                           003F9E  1647 _XT0PERIOD0	=	0x3f9e
                           003F9F  1648 _XT0PERIOD1	=	0x3f9f
                           003F9E  1649 _XT0PERIOD	=	0x3f9e
                           003F9B  1650 _XT0STATUS	=	0x3f9b
                           003FA2  1651 _XT1CLKSRC	=	0x3fa2
                           003FA4  1652 _XT1CNT0	=	0x3fa4
                           003FA5  1653 _XT1CNT1	=	0x3fa5
                           003FA4  1654 _XT1CNT	=	0x3fa4
                           003FA1  1655 _XT1MODE	=	0x3fa1
                           003FA6  1656 _XT1PERIOD0	=	0x3fa6
                           003FA7  1657 _XT1PERIOD1	=	0x3fa7
                           003FA6  1658 _XT1PERIOD	=	0x3fa6
                           003FA3  1659 _XT1STATUS	=	0x3fa3
                           003FAA  1660 _XT2CLKSRC	=	0x3faa
                           003FAC  1661 _XT2CNT0	=	0x3fac
                           003FAD  1662 _XT2CNT1	=	0x3fad
                           003FAC  1663 _XT2CNT	=	0x3fac
                           003FA9  1664 _XT2MODE	=	0x3fa9
                           003FAE  1665 _XT2PERIOD0	=	0x3fae
                           003FAF  1666 _XT2PERIOD1	=	0x3faf
                           003FAE  1667 _XT2PERIOD	=	0x3fae
                           003FAB  1668 _XT2STATUS	=	0x3fab
                           003FE4  1669 _XU0CTRL	=	0x3fe4
                           003FE7  1670 _XU0MODE	=	0x3fe7
                           003FE6  1671 _XU0SHREG	=	0x3fe6
                           003FE5  1672 _XU0STATUS	=	0x3fe5
                           003FEC  1673 _XU1CTRL	=	0x3fec
                           003FEF  1674 _XU1MODE	=	0x3fef
                           003FEE  1675 _XU1SHREG	=	0x3fee
                           003FED  1676 _XU1STATUS	=	0x3fed
                           003FDA  1677 _XWDTCFG	=	0x3fda
                           003FDB  1678 _XWDTRESET	=	0x3fdb
                           003FF1  1679 _XWTCFGA	=	0x3ff1
                           003FF9  1680 _XWTCFGB	=	0x3ff9
                           003FF2  1681 _XWTCNTA0	=	0x3ff2
                           003FF3  1682 _XWTCNTA1	=	0x3ff3
                           003FF2  1683 _XWTCNTA	=	0x3ff2
                           003FFA  1684 _XWTCNTB0	=	0x3ffa
                           003FFB  1685 _XWTCNTB1	=	0x3ffb
                           003FFA  1686 _XWTCNTB	=	0x3ffa
                           003FEB  1687 _XWTCNTR1	=	0x3feb
                           003FF4  1688 _XWTEVTA0	=	0x3ff4
                           003FF5  1689 _XWTEVTA1	=	0x3ff5
                           003FF4  1690 _XWTEVTA	=	0x3ff4
                           003FF6  1691 _XWTEVTB0	=	0x3ff6
                           003FF7  1692 _XWTEVTB1	=	0x3ff7
                           003FF6  1693 _XWTEVTB	=	0x3ff6
                           003FFC  1694 _XWTEVTC0	=	0x3ffc
                           003FFD  1695 _XWTEVTC1	=	0x3ffd
                           003FFC  1696 _XWTEVTC	=	0x3ffc
                           003FFE  1697 _XWTEVTD0	=	0x3ffe
                           003FFF  1698 _XWTEVTD1	=	0x3fff
                           003FFE  1699 _XWTEVTD	=	0x3ffe
                           003FE9  1700 _XWTIRQEN	=	0x3fe9
                           003FEA  1701 _XWTSTAT	=	0x3fea
                           004114  1702 _AX5043_AFSKCTRL	=	0x4114
                           004113  1703 _AX5043_AFSKMARK0	=	0x4113
                           004112  1704 _AX5043_AFSKMARK1	=	0x4112
                           004111  1705 _AX5043_AFSKSPACE0	=	0x4111
                           004110  1706 _AX5043_AFSKSPACE1	=	0x4110
                           004043  1707 _AX5043_AGCCOUNTER	=	0x4043
                           004115  1708 _AX5043_AMPLFILTER	=	0x4115
                           004189  1709 _AX5043_BBOFFSCAP	=	0x4189
                           004188  1710 _AX5043_BBTUNE	=	0x4188
                           004041  1711 _AX5043_BGNDRSSI	=	0x4041
                           00422E  1712 _AX5043_BGNDRSSIGAIN	=	0x422e
                           00422F  1713 _AX5043_BGNDRSSITHR	=	0x422f
                           004017  1714 _AX5043_CRCINIT0	=	0x4017
                           004016  1715 _AX5043_CRCINIT1	=	0x4016
                           004015  1716 _AX5043_CRCINIT2	=	0x4015
                           004014  1717 _AX5043_CRCINIT3	=	0x4014
                           004332  1718 _AX5043_DACCONFIG	=	0x4332
                           004331  1719 _AX5043_DACVALUE0	=	0x4331
                           004330  1720 _AX5043_DACVALUE1	=	0x4330
                           004102  1721 _AX5043_DECIMATION	=	0x4102
                           004042  1722 _AX5043_DIVERSITY	=	0x4042
                           004011  1723 _AX5043_ENCODING	=	0x4011
                           004018  1724 _AX5043_FEC	=	0x4018
                           00401A  1725 _AX5043_FECSTATUS	=	0x401a
                           004019  1726 _AX5043_FECSYNC	=	0x4019
                           00402B  1727 _AX5043_FIFOCOUNT0	=	0x402b
                           00402A  1728 _AX5043_FIFOCOUNT1	=	0x402a
                           004029  1729 _AX5043_FIFODATA	=	0x4029
                           00402D  1730 _AX5043_FIFOFREE0	=	0x402d
                           00402C  1731 _AX5043_FIFOFREE1	=	0x402c
                           004028  1732 _AX5043_FIFOSTAT	=	0x4028
                           00402F  1733 _AX5043_FIFOTHRESH0	=	0x402f
                           00402E  1734 _AX5043_FIFOTHRESH1	=	0x402e
                           004012  1735 _AX5043_FRAMING	=	0x4012
                           004037  1736 _AX5043_FREQA0	=	0x4037
                           004036  1737 _AX5043_FREQA1	=	0x4036
                           004035  1738 _AX5043_FREQA2	=	0x4035
                           004034  1739 _AX5043_FREQA3	=	0x4034
                           00403F  1740 _AX5043_FREQB0	=	0x403f
                           00403E  1741 _AX5043_FREQB1	=	0x403e
                           00403D  1742 _AX5043_FREQB2	=	0x403d
                           00403C  1743 _AX5043_FREQB3	=	0x403c
                           004163  1744 _AX5043_FSKDEV0	=	0x4163
                           004162  1745 _AX5043_FSKDEV1	=	0x4162
                           004161  1746 _AX5043_FSKDEV2	=	0x4161
                           00410D  1747 _AX5043_FSKDMAX0	=	0x410d
                           00410C  1748 _AX5043_FSKDMAX1	=	0x410c
                           00410F  1749 _AX5043_FSKDMIN0	=	0x410f
                           00410E  1750 _AX5043_FSKDMIN1	=	0x410e
                           004309  1751 _AX5043_GPADC13VALUE0	=	0x4309
                           004308  1752 _AX5043_GPADC13VALUE1	=	0x4308
                           004300  1753 _AX5043_GPADCCTRL	=	0x4300
                           004301  1754 _AX5043_GPADCPERIOD	=	0x4301
                           004101  1755 _AX5043_IFFREQ0	=	0x4101
                           004100  1756 _AX5043_IFFREQ1	=	0x4100
                           00400B  1757 _AX5043_IRQINVERSION0	=	0x400b
                           00400A  1758 _AX5043_IRQINVERSION1	=	0x400a
                           004007  1759 _AX5043_IRQMASK0	=	0x4007
                           004006  1760 _AX5043_IRQMASK1	=	0x4006
                           00400D  1761 _AX5043_IRQREQUEST0	=	0x400d
                           00400C  1762 _AX5043_IRQREQUEST1	=	0x400c
                           004310  1763 _AX5043_LPOSCCONFIG	=	0x4310
                           004317  1764 _AX5043_LPOSCFREQ0	=	0x4317
                           004316  1765 _AX5043_LPOSCFREQ1	=	0x4316
                           004313  1766 _AX5043_LPOSCKFILT0	=	0x4313
                           004312  1767 _AX5043_LPOSCKFILT1	=	0x4312
                           004319  1768 _AX5043_LPOSCPER0	=	0x4319
                           004318  1769 _AX5043_LPOSCPER1	=	0x4318
                           004315  1770 _AX5043_LPOSCREF0	=	0x4315
                           004314  1771 _AX5043_LPOSCREF1	=	0x4314
                           004311  1772 _AX5043_LPOSCSTATUS	=	0x4311
                           004214  1773 _AX5043_MATCH0LEN	=	0x4214
                           004216  1774 _AX5043_MATCH0MAX	=	0x4216
                           004215  1775 _AX5043_MATCH0MIN	=	0x4215
                           004213  1776 _AX5043_MATCH0PAT0	=	0x4213
                           004212  1777 _AX5043_MATCH0PAT1	=	0x4212
                           004211  1778 _AX5043_MATCH0PAT2	=	0x4211
                           004210  1779 _AX5043_MATCH0PAT3	=	0x4210
                           00421C  1780 _AX5043_MATCH1LEN	=	0x421c
                           00421E  1781 _AX5043_MATCH1MAX	=	0x421e
                           00421D  1782 _AX5043_MATCH1MIN	=	0x421d
                           004219  1783 _AX5043_MATCH1PAT0	=	0x4219
                           004218  1784 _AX5043_MATCH1PAT1	=	0x4218
                           004108  1785 _AX5043_MAXDROFFSET0	=	0x4108
                           004107  1786 _AX5043_MAXDROFFSET1	=	0x4107
                           004106  1787 _AX5043_MAXDROFFSET2	=	0x4106
                           00410B  1788 _AX5043_MAXRFOFFSET0	=	0x410b
                           00410A  1789 _AX5043_MAXRFOFFSET1	=	0x410a
                           004109  1790 _AX5043_MAXRFOFFSET2	=	0x4109
                           004164  1791 _AX5043_MODCFGA	=	0x4164
                           004160  1792 _AX5043_MODCFGF	=	0x4160
                           004010  1793 _AX5043_MODULATION	=	0x4010
                           004025  1794 _AX5043_PINFUNCANTSEL	=	0x4025
                           004023  1795 _AX5043_PINFUNCDATA	=	0x4023
                           004022  1796 _AX5043_PINFUNCDCLK	=	0x4022
                           004024  1797 _AX5043_PINFUNCIRQ	=	0x4024
                           004026  1798 _AX5043_PINFUNCPWRAMP	=	0x4026
                           004021  1799 _AX5043_PINFUNCSYSCLK	=	0x4021
                           004020  1800 _AX5043_PINSTATE	=	0x4020
                           004233  1801 _AX5043_PKTACCEPTFLAGS	=	0x4233
                           004230  1802 _AX5043_PKTCHUNKSIZE	=	0x4230
                           004231  1803 _AX5043_PKTMISCFLAGS	=	0x4231
                           004232  1804 _AX5043_PKTSTOREFLAGS	=	0x4232
                           004031  1805 _AX5043_PLLCPI	=	0x4031
                           004039  1806 _AX5043_PLLCPIBOOST	=	0x4039
                           004182  1807 _AX5043_PLLLOCKDET	=	0x4182
                           004030  1808 _AX5043_PLLLOOP	=	0x4030
                           004038  1809 _AX5043_PLLLOOPBOOST	=	0x4038
                           004033  1810 _AX5043_PLLRANGINGA	=	0x4033
                           00403B  1811 _AX5043_PLLRANGINGB	=	0x403b
                           004183  1812 _AX5043_PLLRNGCLK	=	0x4183
                           004032  1813 _AX5043_PLLVCODIV	=	0x4032
                           004180  1814 _AX5043_PLLVCOI	=	0x4180
                           004181  1815 _AX5043_PLLVCOIR	=	0x4181
                           004005  1816 _AX5043_POWIRQMASK	=	0x4005
                           004003  1817 _AX5043_POWSTAT	=	0x4003
                           004004  1818 _AX5043_POWSTICKYSTAT	=	0x4004
                           004027  1819 _AX5043_PWRAMP	=	0x4027
                           004002  1820 _AX5043_PWRMODE	=	0x4002
                           004009  1821 _AX5043_RADIOEVENTMASK0	=	0x4009
                           004008  1822 _AX5043_RADIOEVENTMASK1	=	0x4008
                           00400F  1823 _AX5043_RADIOEVENTREQ0	=	0x400f
                           00400E  1824 _AX5043_RADIOEVENTREQ1	=	0x400e
                           00401C  1825 _AX5043_RADIOSTATE	=	0x401c
                           004040  1826 _AX5043_RSSI	=	0x4040
                           00422D  1827 _AX5043_RSSIABSTHR	=	0x422d
                           00422C  1828 _AX5043_RSSIREFERENCE	=	0x422c
                           004105  1829 _AX5043_RXDATARATE0	=	0x4105
                           004104  1830 _AX5043_RXDATARATE1	=	0x4104
                           004103  1831 _AX5043_RXDATARATE2	=	0x4103
                           004001  1832 _AX5043_SCRATCH	=	0x4001
                           004000  1833 _AX5043_SILICONREVISION	=	0x4000
                           00405B  1834 _AX5043_TIMER0	=	0x405b
                           00405A  1835 _AX5043_TIMER1	=	0x405a
                           004059  1836 _AX5043_TIMER2	=	0x4059
                           004227  1837 _AX5043_TMGRXAGC	=	0x4227
                           004223  1838 _AX5043_TMGRXBOOST	=	0x4223
                           004226  1839 _AX5043_TMGRXCOARSEAGC	=	0x4226
                           004225  1840 _AX5043_TMGRXOFFSACQ	=	0x4225
                           004229  1841 _AX5043_TMGRXPREAMBLE1	=	0x4229
                           00422A  1842 _AX5043_TMGRXPREAMBLE2	=	0x422a
                           00422B  1843 _AX5043_TMGRXPREAMBLE3	=	0x422b
                           004228  1844 _AX5043_TMGRXRSSI	=	0x4228
                           004224  1845 _AX5043_TMGRXSETTLE	=	0x4224
                           004220  1846 _AX5043_TMGTXBOOST	=	0x4220
                           004221  1847 _AX5043_TMGTXSETTLE	=	0x4221
                           004055  1848 _AX5043_TRKAFSKDEMOD0	=	0x4055
                           004054  1849 _AX5043_TRKAFSKDEMOD1	=	0x4054
                           004049  1850 _AX5043_TRKAMPLITUDE0	=	0x4049
                           004048  1851 _AX5043_TRKAMPLITUDE1	=	0x4048
                           004047  1852 _AX5043_TRKDATARATE0	=	0x4047
                           004046  1853 _AX5043_TRKDATARATE1	=	0x4046
                           004045  1854 _AX5043_TRKDATARATE2	=	0x4045
                           004051  1855 _AX5043_TRKFREQ0	=	0x4051
                           004050  1856 _AX5043_TRKFREQ1	=	0x4050
                           004053  1857 _AX5043_TRKFSKDEMOD0	=	0x4053
                           004052  1858 _AX5043_TRKFSKDEMOD1	=	0x4052
                           00404B  1859 _AX5043_TRKPHASE0	=	0x404b
                           00404A  1860 _AX5043_TRKPHASE1	=	0x404a
                           00404F  1861 _AX5043_TRKRFFREQ0	=	0x404f
                           00404E  1862 _AX5043_TRKRFFREQ1	=	0x404e
                           00404D  1863 _AX5043_TRKRFFREQ2	=	0x404d
                           004169  1864 _AX5043_TXPWRCOEFFA0	=	0x4169
                           004168  1865 _AX5043_TXPWRCOEFFA1	=	0x4168
                           00416B  1866 _AX5043_TXPWRCOEFFB0	=	0x416b
                           00416A  1867 _AX5043_TXPWRCOEFFB1	=	0x416a
                           00416D  1868 _AX5043_TXPWRCOEFFC0	=	0x416d
                           00416C  1869 _AX5043_TXPWRCOEFFC1	=	0x416c
                           00416F  1870 _AX5043_TXPWRCOEFFD0	=	0x416f
                           00416E  1871 _AX5043_TXPWRCOEFFD1	=	0x416e
                           004171  1872 _AX5043_TXPWRCOEFFE0	=	0x4171
                           004170  1873 _AX5043_TXPWRCOEFFE1	=	0x4170
                           004167  1874 _AX5043_TXRATE0	=	0x4167
                           004166  1875 _AX5043_TXRATE1	=	0x4166
                           004165  1876 _AX5043_TXRATE2	=	0x4165
                           00406B  1877 _AX5043_WAKEUP0	=	0x406b
                           00406A  1878 _AX5043_WAKEUP1	=	0x406a
                           00406D  1879 _AX5043_WAKEUPFREQ0	=	0x406d
                           00406C  1880 _AX5043_WAKEUPFREQ1	=	0x406c
                           004069  1881 _AX5043_WAKEUPTIMER0	=	0x4069
                           004068  1882 _AX5043_WAKEUPTIMER1	=	0x4068
                           00406E  1883 _AX5043_WAKEUPXOEARLY	=	0x406e
                           004184  1884 _AX5043_XTALCAP	=	0x4184
                           00401D  1885 _AX5043_XTALSTATUS	=	0x401d
                           004122  1886 _AX5043_AGCAHYST0	=	0x4122
                           004132  1887 _AX5043_AGCAHYST1	=	0x4132
                           004142  1888 _AX5043_AGCAHYST2	=	0x4142
                           004152  1889 _AX5043_AGCAHYST3	=	0x4152
                           004120  1890 _AX5043_AGCGAIN0	=	0x4120
                           004130  1891 _AX5043_AGCGAIN1	=	0x4130
                           004140  1892 _AX5043_AGCGAIN2	=	0x4140
                           004150  1893 _AX5043_AGCGAIN3	=	0x4150
                           004123  1894 _AX5043_AGCMINMAX0	=	0x4123
                           004133  1895 _AX5043_AGCMINMAX1	=	0x4133
                           004143  1896 _AX5043_AGCMINMAX2	=	0x4143
                           004153  1897 _AX5043_AGCMINMAX3	=	0x4153
                           004121  1898 _AX5043_AGCTARGET0	=	0x4121
                           004131  1899 _AX5043_AGCTARGET1	=	0x4131
                           004141  1900 _AX5043_AGCTARGET2	=	0x4141
                           004151  1901 _AX5043_AGCTARGET3	=	0x4151
                           00412B  1902 _AX5043_AMPLITUDEGAIN0	=	0x412b
                           00413B  1903 _AX5043_AMPLITUDEGAIN1	=	0x413b
                           00414B  1904 _AX5043_AMPLITUDEGAIN2	=	0x414b
                           00415B  1905 _AX5043_AMPLITUDEGAIN3	=	0x415b
                           00412F  1906 _AX5043_BBOFFSRES0	=	0x412f
                           00413F  1907 _AX5043_BBOFFSRES1	=	0x413f
                           00414F  1908 _AX5043_BBOFFSRES2	=	0x414f
                           00415F  1909 _AX5043_BBOFFSRES3	=	0x415f
                           004125  1910 _AX5043_DRGAIN0	=	0x4125
                           004135  1911 _AX5043_DRGAIN1	=	0x4135
                           004145  1912 _AX5043_DRGAIN2	=	0x4145
                           004155  1913 _AX5043_DRGAIN3	=	0x4155
                           00412E  1914 _AX5043_FOURFSK0	=	0x412e
                           00413E  1915 _AX5043_FOURFSK1	=	0x413e
                           00414E  1916 _AX5043_FOURFSK2	=	0x414e
                           00415E  1917 _AX5043_FOURFSK3	=	0x415e
                           00412D  1918 _AX5043_FREQDEV00	=	0x412d
                           00413D  1919 _AX5043_FREQDEV01	=	0x413d
                           00414D  1920 _AX5043_FREQDEV02	=	0x414d
                           00415D  1921 _AX5043_FREQDEV03	=	0x415d
                           00412C  1922 _AX5043_FREQDEV10	=	0x412c
                           00413C  1923 _AX5043_FREQDEV11	=	0x413c
                           00414C  1924 _AX5043_FREQDEV12	=	0x414c
                           00415C  1925 _AX5043_FREQDEV13	=	0x415c
                           004127  1926 _AX5043_FREQUENCYGAINA0	=	0x4127
                           004137  1927 _AX5043_FREQUENCYGAINA1	=	0x4137
                           004147  1928 _AX5043_FREQUENCYGAINA2	=	0x4147
                           004157  1929 _AX5043_FREQUENCYGAINA3	=	0x4157
                           004128  1930 _AX5043_FREQUENCYGAINB0	=	0x4128
                           004138  1931 _AX5043_FREQUENCYGAINB1	=	0x4138
                           004148  1932 _AX5043_FREQUENCYGAINB2	=	0x4148
                           004158  1933 _AX5043_FREQUENCYGAINB3	=	0x4158
                           004129  1934 _AX5043_FREQUENCYGAINC0	=	0x4129
                           004139  1935 _AX5043_FREQUENCYGAINC1	=	0x4139
                           004149  1936 _AX5043_FREQUENCYGAINC2	=	0x4149
                           004159  1937 _AX5043_FREQUENCYGAINC3	=	0x4159
                           00412A  1938 _AX5043_FREQUENCYGAIND0	=	0x412a
                           00413A  1939 _AX5043_FREQUENCYGAIND1	=	0x413a
                           00414A  1940 _AX5043_FREQUENCYGAIND2	=	0x414a
                           00415A  1941 _AX5043_FREQUENCYGAIND3	=	0x415a
                           004116  1942 _AX5043_FREQUENCYLEAK	=	0x4116
                           004126  1943 _AX5043_PHASEGAIN0	=	0x4126
                           004136  1944 _AX5043_PHASEGAIN1	=	0x4136
                           004146  1945 _AX5043_PHASEGAIN2	=	0x4146
                           004156  1946 _AX5043_PHASEGAIN3	=	0x4156
                           004207  1947 _AX5043_PKTADDR0	=	0x4207
                           004206  1948 _AX5043_PKTADDR1	=	0x4206
                           004205  1949 _AX5043_PKTADDR2	=	0x4205
                           004204  1950 _AX5043_PKTADDR3	=	0x4204
                           004200  1951 _AX5043_PKTADDRCFG	=	0x4200
                           00420B  1952 _AX5043_PKTADDRMASK0	=	0x420b
                           00420A  1953 _AX5043_PKTADDRMASK1	=	0x420a
                           004209  1954 _AX5043_PKTADDRMASK2	=	0x4209
                           004208  1955 _AX5043_PKTADDRMASK3	=	0x4208
                           004201  1956 _AX5043_PKTLENCFG	=	0x4201
                           004202  1957 _AX5043_PKTLENOFFSET	=	0x4202
                           004203  1958 _AX5043_PKTMAXLEN	=	0x4203
                           004118  1959 _AX5043_RXPARAMCURSET	=	0x4118
                           004117  1960 _AX5043_RXPARAMSETS	=	0x4117
                           004124  1961 _AX5043_TIMEGAIN0	=	0x4124
                           004134  1962 _AX5043_TIMEGAIN1	=	0x4134
                           004144  1963 _AX5043_TIMEGAIN2	=	0x4144
                           004154  1964 _AX5043_TIMEGAIN3	=	0x4154
                           005114  1965 _AX5043_AFSKCTRLNB	=	0x5114
                           005113  1966 _AX5043_AFSKMARK0NB	=	0x5113
                           005112  1967 _AX5043_AFSKMARK1NB	=	0x5112
                           005111  1968 _AX5043_AFSKSPACE0NB	=	0x5111
                           005110  1969 _AX5043_AFSKSPACE1NB	=	0x5110
                           005043  1970 _AX5043_AGCCOUNTERNB	=	0x5043
                           005115  1971 _AX5043_AMPLFILTERNB	=	0x5115
                           005189  1972 _AX5043_BBOFFSCAPNB	=	0x5189
                           005188  1973 _AX5043_BBTUNENB	=	0x5188
                           005041  1974 _AX5043_BGNDRSSINB	=	0x5041
                           00522E  1975 _AX5043_BGNDRSSIGAINNB	=	0x522e
                           00522F  1976 _AX5043_BGNDRSSITHRNB	=	0x522f
                           005017  1977 _AX5043_CRCINIT0NB	=	0x5017
                           005016  1978 _AX5043_CRCINIT1NB	=	0x5016
                           005015  1979 _AX5043_CRCINIT2NB	=	0x5015
                           005014  1980 _AX5043_CRCINIT3NB	=	0x5014
                           005332  1981 _AX5043_DACCONFIGNB	=	0x5332
                           005331  1982 _AX5043_DACVALUE0NB	=	0x5331
                           005330  1983 _AX5043_DACVALUE1NB	=	0x5330
                           005102  1984 _AX5043_DECIMATIONNB	=	0x5102
                           005042  1985 _AX5043_DIVERSITYNB	=	0x5042
                           005011  1986 _AX5043_ENCODINGNB	=	0x5011
                           005018  1987 _AX5043_FECNB	=	0x5018
                           00501A  1988 _AX5043_FECSTATUSNB	=	0x501a
                           005019  1989 _AX5043_FECSYNCNB	=	0x5019
                           00502B  1990 _AX5043_FIFOCOUNT0NB	=	0x502b
                           00502A  1991 _AX5043_FIFOCOUNT1NB	=	0x502a
                           005029  1992 _AX5043_FIFODATANB	=	0x5029
                           00502D  1993 _AX5043_FIFOFREE0NB	=	0x502d
                           00502C  1994 _AX5043_FIFOFREE1NB	=	0x502c
                           005028  1995 _AX5043_FIFOSTATNB	=	0x5028
                           00502F  1996 _AX5043_FIFOTHRESH0NB	=	0x502f
                           00502E  1997 _AX5043_FIFOTHRESH1NB	=	0x502e
                           005012  1998 _AX5043_FRAMINGNB	=	0x5012
                           005037  1999 _AX5043_FREQA0NB	=	0x5037
                           005036  2000 _AX5043_FREQA1NB	=	0x5036
                           005035  2001 _AX5043_FREQA2NB	=	0x5035
                           005034  2002 _AX5043_FREQA3NB	=	0x5034
                           00503F  2003 _AX5043_FREQB0NB	=	0x503f
                           00503E  2004 _AX5043_FREQB1NB	=	0x503e
                           00503D  2005 _AX5043_FREQB2NB	=	0x503d
                           00503C  2006 _AX5043_FREQB3NB	=	0x503c
                           005163  2007 _AX5043_FSKDEV0NB	=	0x5163
                           005162  2008 _AX5043_FSKDEV1NB	=	0x5162
                           005161  2009 _AX5043_FSKDEV2NB	=	0x5161
                           00510D  2010 _AX5043_FSKDMAX0NB	=	0x510d
                           00510C  2011 _AX5043_FSKDMAX1NB	=	0x510c
                           00510F  2012 _AX5043_FSKDMIN0NB	=	0x510f
                           00510E  2013 _AX5043_FSKDMIN1NB	=	0x510e
                           005309  2014 _AX5043_GPADC13VALUE0NB	=	0x5309
                           005308  2015 _AX5043_GPADC13VALUE1NB	=	0x5308
                           005300  2016 _AX5043_GPADCCTRLNB	=	0x5300
                           005301  2017 _AX5043_GPADCPERIODNB	=	0x5301
                           005101  2018 _AX5043_IFFREQ0NB	=	0x5101
                           005100  2019 _AX5043_IFFREQ1NB	=	0x5100
                           00500B  2020 _AX5043_IRQINVERSION0NB	=	0x500b
                           00500A  2021 _AX5043_IRQINVERSION1NB	=	0x500a
                           005007  2022 _AX5043_IRQMASK0NB	=	0x5007
                           005006  2023 _AX5043_IRQMASK1NB	=	0x5006
                           00500D  2024 _AX5043_IRQREQUEST0NB	=	0x500d
                           00500C  2025 _AX5043_IRQREQUEST1NB	=	0x500c
                           005310  2026 _AX5043_LPOSCCONFIGNB	=	0x5310
                           005317  2027 _AX5043_LPOSCFREQ0NB	=	0x5317
                           005316  2028 _AX5043_LPOSCFREQ1NB	=	0x5316
                           005313  2029 _AX5043_LPOSCKFILT0NB	=	0x5313
                           005312  2030 _AX5043_LPOSCKFILT1NB	=	0x5312
                           005319  2031 _AX5043_LPOSCPER0NB	=	0x5319
                           005318  2032 _AX5043_LPOSCPER1NB	=	0x5318
                           005315  2033 _AX5043_LPOSCREF0NB	=	0x5315
                           005314  2034 _AX5043_LPOSCREF1NB	=	0x5314
                           005311  2035 _AX5043_LPOSCSTATUSNB	=	0x5311
                           005214  2036 _AX5043_MATCH0LENNB	=	0x5214
                           005216  2037 _AX5043_MATCH0MAXNB	=	0x5216
                           005215  2038 _AX5043_MATCH0MINNB	=	0x5215
                           005213  2039 _AX5043_MATCH0PAT0NB	=	0x5213
                           005212  2040 _AX5043_MATCH0PAT1NB	=	0x5212
                           005211  2041 _AX5043_MATCH0PAT2NB	=	0x5211
                           005210  2042 _AX5043_MATCH0PAT3NB	=	0x5210
                           00521C  2043 _AX5043_MATCH1LENNB	=	0x521c
                           00521E  2044 _AX5043_MATCH1MAXNB	=	0x521e
                           00521D  2045 _AX5043_MATCH1MINNB	=	0x521d
                           005219  2046 _AX5043_MATCH1PAT0NB	=	0x5219
                           005218  2047 _AX5043_MATCH1PAT1NB	=	0x5218
                           005108  2048 _AX5043_MAXDROFFSET0NB	=	0x5108
                           005107  2049 _AX5043_MAXDROFFSET1NB	=	0x5107
                           005106  2050 _AX5043_MAXDROFFSET2NB	=	0x5106
                           00510B  2051 _AX5043_MAXRFOFFSET0NB	=	0x510b
                           00510A  2052 _AX5043_MAXRFOFFSET1NB	=	0x510a
                           005109  2053 _AX5043_MAXRFOFFSET2NB	=	0x5109
                           005164  2054 _AX5043_MODCFGANB	=	0x5164
                           005160  2055 _AX5043_MODCFGFNB	=	0x5160
                           005F5F  2056 _AX5043_MODCFGPNB	=	0x5f5f
                           005010  2057 _AX5043_MODULATIONNB	=	0x5010
                           005025  2058 _AX5043_PINFUNCANTSELNB	=	0x5025
                           005023  2059 _AX5043_PINFUNCDATANB	=	0x5023
                           005022  2060 _AX5043_PINFUNCDCLKNB	=	0x5022
                           005024  2061 _AX5043_PINFUNCIRQNB	=	0x5024
                           005026  2062 _AX5043_PINFUNCPWRAMPNB	=	0x5026
                           005021  2063 _AX5043_PINFUNCSYSCLKNB	=	0x5021
                           005020  2064 _AX5043_PINSTATENB	=	0x5020
                           005233  2065 _AX5043_PKTACCEPTFLAGSNB	=	0x5233
                           005230  2066 _AX5043_PKTCHUNKSIZENB	=	0x5230
                           005231  2067 _AX5043_PKTMISCFLAGSNB	=	0x5231
                           005232  2068 _AX5043_PKTSTOREFLAGSNB	=	0x5232
                           005031  2069 _AX5043_PLLCPINB	=	0x5031
                           005039  2070 _AX5043_PLLCPIBOOSTNB	=	0x5039
                           005182  2071 _AX5043_PLLLOCKDETNB	=	0x5182
                           005030  2072 _AX5043_PLLLOOPNB	=	0x5030
                           005038  2073 _AX5043_PLLLOOPBOOSTNB	=	0x5038
                           005033  2074 _AX5043_PLLRANGINGANB	=	0x5033
                           00503B  2075 _AX5043_PLLRANGINGBNB	=	0x503b
                           005183  2076 _AX5043_PLLRNGCLKNB	=	0x5183
                           005032  2077 _AX5043_PLLVCODIVNB	=	0x5032
                           005180  2078 _AX5043_PLLVCOINB	=	0x5180
                           005181  2079 _AX5043_PLLVCOIRNB	=	0x5181
                           005F08  2080 _AX5043_POWCTRL1NB	=	0x5f08
                           005005  2081 _AX5043_POWIRQMASKNB	=	0x5005
                           005003  2082 _AX5043_POWSTATNB	=	0x5003
                           005004  2083 _AX5043_POWSTICKYSTATNB	=	0x5004
                           005027  2084 _AX5043_PWRAMPNB	=	0x5027
                           005002  2085 _AX5043_PWRMODENB	=	0x5002
                           005009  2086 _AX5043_RADIOEVENTMASK0NB	=	0x5009
                           005008  2087 _AX5043_RADIOEVENTMASK1NB	=	0x5008
                           00500F  2088 _AX5043_RADIOEVENTREQ0NB	=	0x500f
                           00500E  2089 _AX5043_RADIOEVENTREQ1NB	=	0x500e
                           00501C  2090 _AX5043_RADIOSTATENB	=	0x501c
                           005F0D  2091 _AX5043_REFNB	=	0x5f0d
                           005040  2092 _AX5043_RSSINB	=	0x5040
                           00522D  2093 _AX5043_RSSIABSTHRNB	=	0x522d
                           00522C  2094 _AX5043_RSSIREFERENCENB	=	0x522c
                           005105  2095 _AX5043_RXDATARATE0NB	=	0x5105
                           005104  2096 _AX5043_RXDATARATE1NB	=	0x5104
                           005103  2097 _AX5043_RXDATARATE2NB	=	0x5103
                           005001  2098 _AX5043_SCRATCHNB	=	0x5001
                           005000  2099 _AX5043_SILICONREVISIONNB	=	0x5000
                           00505B  2100 _AX5043_TIMER0NB	=	0x505b
                           00505A  2101 _AX5043_TIMER1NB	=	0x505a
                           005059  2102 _AX5043_TIMER2NB	=	0x5059
                           005227  2103 _AX5043_TMGRXAGCNB	=	0x5227
                           005223  2104 _AX5043_TMGRXBOOSTNB	=	0x5223
                           005226  2105 _AX5043_TMGRXCOARSEAGCNB	=	0x5226
                           005225  2106 _AX5043_TMGRXOFFSACQNB	=	0x5225
                           005229  2107 _AX5043_TMGRXPREAMBLE1NB	=	0x5229
                           00522A  2108 _AX5043_TMGRXPREAMBLE2NB	=	0x522a
                           00522B  2109 _AX5043_TMGRXPREAMBLE3NB	=	0x522b
                           005228  2110 _AX5043_TMGRXRSSINB	=	0x5228
                           005224  2111 _AX5043_TMGRXSETTLENB	=	0x5224
                           005220  2112 _AX5043_TMGTXBOOSTNB	=	0x5220
                           005221  2113 _AX5043_TMGTXSETTLENB	=	0x5221
                           005055  2114 _AX5043_TRKAFSKDEMOD0NB	=	0x5055
                           005054  2115 _AX5043_TRKAFSKDEMOD1NB	=	0x5054
                           005049  2116 _AX5043_TRKAMPLITUDE0NB	=	0x5049
                           005048  2117 _AX5043_TRKAMPLITUDE1NB	=	0x5048
                           005047  2118 _AX5043_TRKDATARATE0NB	=	0x5047
                           005046  2119 _AX5043_TRKDATARATE1NB	=	0x5046
                           005045  2120 _AX5043_TRKDATARATE2NB	=	0x5045
                           005051  2121 _AX5043_TRKFREQ0NB	=	0x5051
                           005050  2122 _AX5043_TRKFREQ1NB	=	0x5050
                           005053  2123 _AX5043_TRKFSKDEMOD0NB	=	0x5053
                           005052  2124 _AX5043_TRKFSKDEMOD1NB	=	0x5052
                           00504B  2125 _AX5043_TRKPHASE0NB	=	0x504b
                           00504A  2126 _AX5043_TRKPHASE1NB	=	0x504a
                           00504F  2127 _AX5043_TRKRFFREQ0NB	=	0x504f
                           00504E  2128 _AX5043_TRKRFFREQ1NB	=	0x504e
                           00504D  2129 _AX5043_TRKRFFREQ2NB	=	0x504d
                           005169  2130 _AX5043_TXPWRCOEFFA0NB	=	0x5169
                           005168  2131 _AX5043_TXPWRCOEFFA1NB	=	0x5168
                           00516B  2132 _AX5043_TXPWRCOEFFB0NB	=	0x516b
                           00516A  2133 _AX5043_TXPWRCOEFFB1NB	=	0x516a
                           00516D  2134 _AX5043_TXPWRCOEFFC0NB	=	0x516d
                           00516C  2135 _AX5043_TXPWRCOEFFC1NB	=	0x516c
                           00516F  2136 _AX5043_TXPWRCOEFFD0NB	=	0x516f
                           00516E  2137 _AX5043_TXPWRCOEFFD1NB	=	0x516e
                           005171  2138 _AX5043_TXPWRCOEFFE0NB	=	0x5171
                           005170  2139 _AX5043_TXPWRCOEFFE1NB	=	0x5170
                           005167  2140 _AX5043_TXRATE0NB	=	0x5167
                           005166  2141 _AX5043_TXRATE1NB	=	0x5166
                           005165  2142 _AX5043_TXRATE2NB	=	0x5165
                           00506B  2143 _AX5043_WAKEUP0NB	=	0x506b
                           00506A  2144 _AX5043_WAKEUP1NB	=	0x506a
                           00506D  2145 _AX5043_WAKEUPFREQ0NB	=	0x506d
                           00506C  2146 _AX5043_WAKEUPFREQ1NB	=	0x506c
                           005069  2147 _AX5043_WAKEUPTIMER0NB	=	0x5069
                           005068  2148 _AX5043_WAKEUPTIMER1NB	=	0x5068
                           00506E  2149 _AX5043_WAKEUPXOEARLYNB	=	0x506e
                           005F11  2150 _AX5043_XTALAMPLNB	=	0x5f11
                           005184  2151 _AX5043_XTALCAPNB	=	0x5184
                           005F10  2152 _AX5043_XTALOSCNB	=	0x5f10
                           00501D  2153 _AX5043_XTALSTATUSNB	=	0x501d
                           005F00  2154 _AX5043_0xF00NB	=	0x5f00
                           005F0C  2155 _AX5043_0xF0CNB	=	0x5f0c
                           005F18  2156 _AX5043_0xF18NB	=	0x5f18
                           005F1C  2157 _AX5043_0xF1CNB	=	0x5f1c
                           005F21  2158 _AX5043_0xF21NB	=	0x5f21
                           005F22  2159 _AX5043_0xF22NB	=	0x5f22
                           005F23  2160 _AX5043_0xF23NB	=	0x5f23
                           005F26  2161 _AX5043_0xF26NB	=	0x5f26
                           005F30  2162 _AX5043_0xF30NB	=	0x5f30
                           005F31  2163 _AX5043_0xF31NB	=	0x5f31
                           005F32  2164 _AX5043_0xF32NB	=	0x5f32
                           005F33  2165 _AX5043_0xF33NB	=	0x5f33
                           005F34  2166 _AX5043_0xF34NB	=	0x5f34
                           005F35  2167 _AX5043_0xF35NB	=	0x5f35
                           005F44  2168 _AX5043_0xF44NB	=	0x5f44
                           005122  2169 _AX5043_AGCAHYST0NB	=	0x5122
                           005132  2170 _AX5043_AGCAHYST1NB	=	0x5132
                           005142  2171 _AX5043_AGCAHYST2NB	=	0x5142
                           005152  2172 _AX5043_AGCAHYST3NB	=	0x5152
                           005120  2173 _AX5043_AGCGAIN0NB	=	0x5120
                           005130  2174 _AX5043_AGCGAIN1NB	=	0x5130
                           005140  2175 _AX5043_AGCGAIN2NB	=	0x5140
                           005150  2176 _AX5043_AGCGAIN3NB	=	0x5150
                           005123  2177 _AX5043_AGCMINMAX0NB	=	0x5123
                           005133  2178 _AX5043_AGCMINMAX1NB	=	0x5133
                           005143  2179 _AX5043_AGCMINMAX2NB	=	0x5143
                           005153  2180 _AX5043_AGCMINMAX3NB	=	0x5153
                           005121  2181 _AX5043_AGCTARGET0NB	=	0x5121
                           005131  2182 _AX5043_AGCTARGET1NB	=	0x5131
                           005141  2183 _AX5043_AGCTARGET2NB	=	0x5141
                           005151  2184 _AX5043_AGCTARGET3NB	=	0x5151
                           00512B  2185 _AX5043_AMPLITUDEGAIN0NB	=	0x512b
                           00513B  2186 _AX5043_AMPLITUDEGAIN1NB	=	0x513b
                           00514B  2187 _AX5043_AMPLITUDEGAIN2NB	=	0x514b
                           00515B  2188 _AX5043_AMPLITUDEGAIN3NB	=	0x515b
                           00512F  2189 _AX5043_BBOFFSRES0NB	=	0x512f
                           00513F  2190 _AX5043_BBOFFSRES1NB	=	0x513f
                           00514F  2191 _AX5043_BBOFFSRES2NB	=	0x514f
                           00515F  2192 _AX5043_BBOFFSRES3NB	=	0x515f
                           005125  2193 _AX5043_DRGAIN0NB	=	0x5125
                           005135  2194 _AX5043_DRGAIN1NB	=	0x5135
                           005145  2195 _AX5043_DRGAIN2NB	=	0x5145
                           005155  2196 _AX5043_DRGAIN3NB	=	0x5155
                           00512E  2197 _AX5043_FOURFSK0NB	=	0x512e
                           00513E  2198 _AX5043_FOURFSK1NB	=	0x513e
                           00514E  2199 _AX5043_FOURFSK2NB	=	0x514e
                           00515E  2200 _AX5043_FOURFSK3NB	=	0x515e
                           00512D  2201 _AX5043_FREQDEV00NB	=	0x512d
                           00513D  2202 _AX5043_FREQDEV01NB	=	0x513d
                           00514D  2203 _AX5043_FREQDEV02NB	=	0x514d
                           00515D  2204 _AX5043_FREQDEV03NB	=	0x515d
                           00512C  2205 _AX5043_FREQDEV10NB	=	0x512c
                           00513C  2206 _AX5043_FREQDEV11NB	=	0x513c
                           00514C  2207 _AX5043_FREQDEV12NB	=	0x514c
                           00515C  2208 _AX5043_FREQDEV13NB	=	0x515c
                           005127  2209 _AX5043_FREQUENCYGAINA0NB	=	0x5127
                           005137  2210 _AX5043_FREQUENCYGAINA1NB	=	0x5137
                           005147  2211 _AX5043_FREQUENCYGAINA2NB	=	0x5147
                           005157  2212 _AX5043_FREQUENCYGAINA3NB	=	0x5157
                           005128  2213 _AX5043_FREQUENCYGAINB0NB	=	0x5128
                           005138  2214 _AX5043_FREQUENCYGAINB1NB	=	0x5138
                           005148  2215 _AX5043_FREQUENCYGAINB2NB	=	0x5148
                           005158  2216 _AX5043_FREQUENCYGAINB3NB	=	0x5158
                           005129  2217 _AX5043_FREQUENCYGAINC0NB	=	0x5129
                           005139  2218 _AX5043_FREQUENCYGAINC1NB	=	0x5139
                           005149  2219 _AX5043_FREQUENCYGAINC2NB	=	0x5149
                           005159  2220 _AX5043_FREQUENCYGAINC3NB	=	0x5159
                           00512A  2221 _AX5043_FREQUENCYGAIND0NB	=	0x512a
                           00513A  2222 _AX5043_FREQUENCYGAIND1NB	=	0x513a
                           00514A  2223 _AX5043_FREQUENCYGAIND2NB	=	0x514a
                           00515A  2224 _AX5043_FREQUENCYGAIND3NB	=	0x515a
                           005116  2225 _AX5043_FREQUENCYLEAKNB	=	0x5116
                           005126  2226 _AX5043_PHASEGAIN0NB	=	0x5126
                           005136  2227 _AX5043_PHASEGAIN1NB	=	0x5136
                           005146  2228 _AX5043_PHASEGAIN2NB	=	0x5146
                           005156  2229 _AX5043_PHASEGAIN3NB	=	0x5156
                           005207  2230 _AX5043_PKTADDR0NB	=	0x5207
                           005206  2231 _AX5043_PKTADDR1NB	=	0x5206
                           005205  2232 _AX5043_PKTADDR2NB	=	0x5205
                           005204  2233 _AX5043_PKTADDR3NB	=	0x5204
                           005200  2234 _AX5043_PKTADDRCFGNB	=	0x5200
                           00520B  2235 _AX5043_PKTADDRMASK0NB	=	0x520b
                           00520A  2236 _AX5043_PKTADDRMASK1NB	=	0x520a
                           005209  2237 _AX5043_PKTADDRMASK2NB	=	0x5209
                           005208  2238 _AX5043_PKTADDRMASK3NB	=	0x5208
                           005201  2239 _AX5043_PKTLENCFGNB	=	0x5201
                           005202  2240 _AX5043_PKTLENOFFSETNB	=	0x5202
                           005203  2241 _AX5043_PKTMAXLENNB	=	0x5203
                           005118  2242 _AX5043_RXPARAMCURSETNB	=	0x5118
                           005117  2243 _AX5043_RXPARAMSETSNB	=	0x5117
                           005124  2244 _AX5043_TIMEGAIN0NB	=	0x5124
                           005134  2245 _AX5043_TIMEGAIN1NB	=	0x5134
                           005144  2246 _AX5043_TIMEGAIN2NB	=	0x5144
                           005154  2247 _AX5043_TIMEGAIN3NB	=	0x5154
                           004F00  2248 _AX5043_0xF00	=	0x4f00
                           004F0C  2249 _AX5043_0xF0C	=	0x4f0c
                           004F10  2250 _AX5043_0xF10	=	0x4f10
                           004F11  2251 _AX5043_0xF11	=	0x4f11
                           004F18  2252 _AX5043_0xF18	=	0x4f18
                           004F1C  2253 _AX5043_0xF1C	=	0x4f1c
                           004F21  2254 _AX5043_0xF21	=	0x4f21
                           004F22  2255 _AX5043_0xF22	=	0x4f22
                           004F23  2256 _AX5043_0xF23	=	0x4f23
                           004F26  2257 _AX5043_0xF26	=	0x4f26
                           004F30  2258 _AX5043_0xF30	=	0x4f30
                           004F31  2259 _AX5043_0xF31	=	0x4f31
                           004F32  2260 _AX5043_0xF32	=	0x4f32
                           004F33  2261 _AX5043_0xF33	=	0x4f33
                           004F34  2262 _AX5043_0xF34	=	0x4f34
                           004F35  2263 _AX5043_0xF35	=	0x4f35
                           004F44  2264 _AX5043_0xF44	=	0x4f44
                           004F0D  2265 _AX5043_REF	=	0x4f0d
                           004F08  2266 _AX5043_POWCTRL1	=	0x4f08
                           004F5F  2267 _AX5043_MODCFGP	=	0x4f5f
                           004F10  2268 _AX5043_XTALOSC	=	0x4f10
                           004F11  2269 _AX5043_XTALAMPL	=	0x4f11
      0000DE                       2270 _axradio_conv_timeinterval_totimer0_dt_65536_138:
      0000DE                       2271 	.ds 4
      0000E2                       2272 _axradio_conv_timeinterval_totimer0_r_65536_139:
      0000E2                       2273 	.ds 4
      0000E6                       2274 _axradio_phy_chanpllrng::
      0000E6                       2275 	.ds 1
      0000E7                       2276 _axradio_phy_chanvcoi::
      0000E7                       2277 	.ds 1
                                   2278 ;--------------------------------------------------------
                                   2279 ; absolute external ram data
                                   2280 ;--------------------------------------------------------
                                   2281 	.area XABS    (ABS,XDATA)
                                   2282 ;--------------------------------------------------------
                                   2283 ; external initialized ram data
                                   2284 ;--------------------------------------------------------
                                   2285 	.area XISEG   (XDATA)
                                   2286 	.area HOME    (CODE)
                                   2287 	.area GSINIT0 (CODE)
                                   2288 	.area GSINIT1 (CODE)
                                   2289 	.area GSINIT2 (CODE)
                                   2290 	.area GSINIT3 (CODE)
                                   2291 	.area GSINIT4 (CODE)
                                   2292 	.area GSINIT5 (CODE)
                                   2293 	.area GSINIT  (CODE)
                                   2294 	.area GSFINAL (CODE)
                                   2295 	.area CSEG    (CODE)
                                   2296 ;--------------------------------------------------------
                                   2297 ; global & static initialisations
                                   2298 ;--------------------------------------------------------
                                   2299 	.area HOME    (CODE)
                                   2300 	.area GSINIT  (CODE)
                                   2301 	.area GSFINAL (CODE)
                                   2302 	.area GSINIT  (CODE)
                                   2303 ;--------------------------------------------------------
                                   2304 ; Home
                                   2305 ;--------------------------------------------------------
                                   2306 	.area HOME    (CODE)
                                   2307 	.area HOME    (CODE)
                                   2308 ;--------------------------------------------------------
                                   2309 ; code
                                   2310 ;--------------------------------------------------------
                                   2311 	.area CSEG    (CODE)
                                   2312 ;------------------------------------------------------------
                                   2313 ;Allocation info for local variables in function 'ax5043_set_registers'
                                   2314 ;------------------------------------------------------------
                                   2315 ;	..\src\AX_Radio_Lab_output\config.c:12: __reentrantb void ax5043_set_registers(void) __reentrant
                                   2316 ;	-----------------------------------------
                                   2317 ;	 function ax5043_set_registers
                                   2318 ;	-----------------------------------------
      0015B5                       2319 _ax5043_set_registers:
                           000007  2320 	ar7 = 0x07
                           000006  2321 	ar6 = 0x06
                           000005  2322 	ar5 = 0x05
                           000004  2323 	ar4 = 0x04
                           000003  2324 	ar3 = 0x03
                           000002  2325 	ar2 = 0x02
                           000001  2326 	ar1 = 0x01
                           000000  2327 	ar0 = 0x00
                                   2328 ;	..\src\AX_Radio_Lab_output\config.c:14: AX5043_MODULATION              = 0x08;
      0015B5 90 40 10         [24] 2329 	mov	dptr,#_AX5043_MODULATION
      0015B8 74 08            [12] 2330 	mov	a,#0x08
      0015BA F0               [24] 2331 	movx	@dptr,a
                                   2332 ;	..\src\AX_Radio_Lab_output\config.c:15: AX5043_ENCODING                = 0x01;
      0015BB 90 40 11         [24] 2333 	mov	dptr,#_AX5043_ENCODING
      0015BE 74 01            [12] 2334 	mov	a,#0x01
      0015C0 F0               [24] 2335 	movx	@dptr,a
                                   2336 ;	..\src\AX_Radio_Lab_output\config.c:16: AX5043_FRAMING                 = 0x66;
      0015C1 90 40 12         [24] 2337 	mov	dptr,#_AX5043_FRAMING
      0015C4 74 66            [12] 2338 	mov	a,#0x66
      0015C6 F0               [24] 2339 	movx	@dptr,a
                                   2340 ;	..\src\AX_Radio_Lab_output\config.c:17: AX5043_PINFUNCSYSCLK           = 0x01;
      0015C7 90 40 21         [24] 2341 	mov	dptr,#_AX5043_PINFUNCSYSCLK
      0015CA 74 01            [12] 2342 	mov	a,#0x01
      0015CC F0               [24] 2343 	movx	@dptr,a
                                   2344 ;	..\src\AX_Radio_Lab_output\config.c:18: AX5043_PINFUNCDCLK             = 0x01;
      0015CD 90 40 22         [24] 2345 	mov	dptr,#_AX5043_PINFUNCDCLK
      0015D0 F0               [24] 2346 	movx	@dptr,a
                                   2347 ;	..\src\AX_Radio_Lab_output\config.c:19: AX5043_PINFUNCDATA             = 0x01;
      0015D1 90 40 23         [24] 2348 	mov	dptr,#_AX5043_PINFUNCDATA
      0015D4 F0               [24] 2349 	movx	@dptr,a
                                   2350 ;	..\src\AX_Radio_Lab_output\config.c:20: AX5043_PINFUNCANTSEL           = 0x04;
      0015D5 90 40 25         [24] 2351 	mov	dptr,#_AX5043_PINFUNCANTSEL
      0015D8 74 04            [12] 2352 	mov	a,#0x04
      0015DA F0               [24] 2353 	movx	@dptr,a
                                   2354 ;	..\src\AX_Radio_Lab_output\config.c:21: AX5043_PINFUNCPWRAMP           = 0x07;
      0015DB 90 40 26         [24] 2355 	mov	dptr,#_AX5043_PINFUNCPWRAMP
      0015DE 74 07            [12] 2356 	mov	a,#0x07
      0015E0 F0               [24] 2357 	movx	@dptr,a
                                   2358 ;	..\src\AX_Radio_Lab_output\config.c:22: AX5043_WAKEUPXOEARLY           = 0x01;
      0015E1 90 40 6E         [24] 2359 	mov	dptr,#_AX5043_WAKEUPXOEARLY
      0015E4 74 01            [12] 2360 	mov	a,#0x01
      0015E6 F0               [24] 2361 	movx	@dptr,a
                                   2362 ;	..\src\AX_Radio_Lab_output\config.c:23: AX5043_IFFREQ1                 = 0x02;
      0015E7 90 41 00         [24] 2363 	mov	dptr,#_AX5043_IFFREQ1
      0015EA 04               [12] 2364 	inc	a
      0015EB F0               [24] 2365 	movx	@dptr,a
                                   2366 ;	..\src\AX_Radio_Lab_output\config.c:24: AX5043_IFFREQ0                 = 0x0C;
      0015EC 90 41 01         [24] 2367 	mov	dptr,#_AX5043_IFFREQ0
      0015EF 74 0C            [12] 2368 	mov	a,#0x0c
      0015F1 F0               [24] 2369 	movx	@dptr,a
                                   2370 ;	..\src\AX_Radio_Lab_output\config.c:25: AX5043_DECIMATION              = 0x14;
      0015F2 90 41 02         [24] 2371 	mov	dptr,#_AX5043_DECIMATION
      0015F5 74 14            [12] 2372 	mov	a,#0x14
      0015F7 F0               [24] 2373 	movx	@dptr,a
                                   2374 ;	..\src\AX_Radio_Lab_output\config.c:26: AX5043_RXDATARATE2             = 0x00;
      0015F8 90 41 03         [24] 2375 	mov	dptr,#_AX5043_RXDATARATE2
      0015FB E4               [12] 2376 	clr	a
      0015FC F0               [24] 2377 	movx	@dptr,a
                                   2378 ;	..\src\AX_Radio_Lab_output\config.c:27: AX5043_RXDATARATE1             = 0x3E;
      0015FD 90 41 04         [24] 2379 	mov	dptr,#_AX5043_RXDATARATE1
      001600 74 3E            [12] 2380 	mov	a,#0x3e
      001602 F0               [24] 2381 	movx	@dptr,a
                                   2382 ;	..\src\AX_Radio_Lab_output\config.c:28: AX5043_RXDATARATE0             = 0x80;
      001603 90 41 05         [24] 2383 	mov	dptr,#_AX5043_RXDATARATE0
      001606 74 80            [12] 2384 	mov	a,#0x80
      001608 F0               [24] 2385 	movx	@dptr,a
                                   2386 ;	..\src\AX_Radio_Lab_output\config.c:29: AX5043_MAXDROFFSET2            = 0x00;
      001609 90 41 06         [24] 2387 	mov	dptr,#_AX5043_MAXDROFFSET2
      00160C E4               [12] 2388 	clr	a
      00160D F0               [24] 2389 	movx	@dptr,a
                                   2390 ;	..\src\AX_Radio_Lab_output\config.c:30: AX5043_MAXDROFFSET1            = 0x00;
      00160E 90 41 07         [24] 2391 	mov	dptr,#_AX5043_MAXDROFFSET1
      001611 F0               [24] 2392 	movx	@dptr,a
                                   2393 ;	..\src\AX_Radio_Lab_output\config.c:31: AX5043_MAXDROFFSET0            = 0x00;
      001612 90 41 08         [24] 2394 	mov	dptr,#_AX5043_MAXDROFFSET0
      001615 F0               [24] 2395 	movx	@dptr,a
                                   2396 ;	..\src\AX_Radio_Lab_output\config.c:32: AX5043_MAXRFOFFSET2            = 0x80;
      001616 90 41 09         [24] 2397 	mov	dptr,#_AX5043_MAXRFOFFSET2
      001619 74 80            [12] 2398 	mov	a,#0x80
      00161B F0               [24] 2399 	movx	@dptr,a
                                   2400 ;	..\src\AX_Radio_Lab_output\config.c:33: AX5043_MAXRFOFFSET1            = 0x01;
      00161C 90 41 0A         [24] 2401 	mov	dptr,#_AX5043_MAXRFOFFSET1
      00161F 23               [12] 2402 	rl	a
      001620 F0               [24] 2403 	movx	@dptr,a
                                   2404 ;	..\src\AX_Radio_Lab_output\config.c:34: AX5043_MAXRFOFFSET0            = 0x31;
      001621 90 41 0B         [24] 2405 	mov	dptr,#_AX5043_MAXRFOFFSET0
      001624 74 31            [12] 2406 	mov	a,#0x31
      001626 F0               [24] 2407 	movx	@dptr,a
                                   2408 ;	..\src\AX_Radio_Lab_output\config.c:35: AX5043_FSKDMAX1                = 0x00;
      001627 90 41 0C         [24] 2409 	mov	dptr,#_AX5043_FSKDMAX1
      00162A E4               [12] 2410 	clr	a
      00162B F0               [24] 2411 	movx	@dptr,a
                                   2412 ;	..\src\AX_Radio_Lab_output\config.c:36: AX5043_FSKDMAX0                = 0xA6;
      00162C 90 41 0D         [24] 2413 	mov	dptr,#_AX5043_FSKDMAX0
      00162F 74 A6            [12] 2414 	mov	a,#0xa6
      001631 F0               [24] 2415 	movx	@dptr,a
                                   2416 ;	..\src\AX_Radio_Lab_output\config.c:37: AX5043_FSKDMIN1                = 0xFF;
      001632 90 41 0E         [24] 2417 	mov	dptr,#_AX5043_FSKDMIN1
      001635 74 FF            [12] 2418 	mov	a,#0xff
      001637 F0               [24] 2419 	movx	@dptr,a
                                   2420 ;	..\src\AX_Radio_Lab_output\config.c:38: AX5043_FSKDMIN0                = 0x5A;
      001638 90 41 0F         [24] 2421 	mov	dptr,#_AX5043_FSKDMIN0
      00163B 74 5A            [12] 2422 	mov	a,#0x5a
      00163D F0               [24] 2423 	movx	@dptr,a
                                   2424 ;	..\src\AX_Radio_Lab_output\config.c:39: AX5043_AMPLFILTER              = 0x00;
      00163E 90 41 15         [24] 2425 	mov	dptr,#_AX5043_AMPLFILTER
      001641 E4               [12] 2426 	clr	a
      001642 F0               [24] 2427 	movx	@dptr,a
                                   2428 ;	..\src\AX_Radio_Lab_output\config.c:40: AX5043_RXPARAMSETS             = 0xF4;
      001643 90 41 17         [24] 2429 	mov	dptr,#_AX5043_RXPARAMSETS
      001646 74 F4            [12] 2430 	mov	a,#0xf4
      001648 F0               [24] 2431 	movx	@dptr,a
                                   2432 ;	..\src\AX_Radio_Lab_output\config.c:41: AX5043_AGCGAIN0                = 0xC5;
      001649 90 41 20         [24] 2433 	mov	dptr,#_AX5043_AGCGAIN0
      00164C 74 C5            [12] 2434 	mov	a,#0xc5
      00164E F0               [24] 2435 	movx	@dptr,a
                                   2436 ;	..\src\AX_Radio_Lab_output\config.c:42: AX5043_AGCTARGET0              = 0x84;
      00164F 90 41 21         [24] 2437 	mov	dptr,#_AX5043_AGCTARGET0
      001652 74 84            [12] 2438 	mov	a,#0x84
      001654 F0               [24] 2439 	movx	@dptr,a
                                   2440 ;	..\src\AX_Radio_Lab_output\config.c:43: AX5043_TIMEGAIN0               = 0xF8;
      001655 90 41 24         [24] 2441 	mov	dptr,#_AX5043_TIMEGAIN0
      001658 74 F8            [12] 2442 	mov	a,#0xf8
      00165A F0               [24] 2443 	movx	@dptr,a
                                   2444 ;	..\src\AX_Radio_Lab_output\config.c:44: AX5043_DRGAIN0                 = 0xF2;
      00165B 90 41 25         [24] 2445 	mov	dptr,#_AX5043_DRGAIN0
      00165E 74 F2            [12] 2446 	mov	a,#0xf2
      001660 F0               [24] 2447 	movx	@dptr,a
                                   2448 ;	..\src\AX_Radio_Lab_output\config.c:45: AX5043_PHASEGAIN0              = 0xC3;
      001661 90 41 26         [24] 2449 	mov	dptr,#_AX5043_PHASEGAIN0
      001664 74 C3            [12] 2450 	mov	a,#0xc3
      001666 F0               [24] 2451 	movx	@dptr,a
                                   2452 ;	..\src\AX_Radio_Lab_output\config.c:46: AX5043_FREQUENCYGAINA0         = 0x0F;
      001667 90 41 27         [24] 2453 	mov	dptr,#_AX5043_FREQUENCYGAINA0
      00166A 74 0F            [12] 2454 	mov	a,#0x0f
      00166C F0               [24] 2455 	movx	@dptr,a
                                   2456 ;	..\src\AX_Radio_Lab_output\config.c:47: AX5043_FREQUENCYGAINB0         = 0x1F;
      00166D 90 41 28         [24] 2457 	mov	dptr,#_AX5043_FREQUENCYGAINB0
      001670 74 1F            [12] 2458 	mov	a,#0x1f
      001672 F0               [24] 2459 	movx	@dptr,a
                                   2460 ;	..\src\AX_Radio_Lab_output\config.c:48: AX5043_FREQUENCYGAINC0         = 0x08;
      001673 90 41 29         [24] 2461 	mov	dptr,#_AX5043_FREQUENCYGAINC0
      001676 74 08            [12] 2462 	mov	a,#0x08
      001678 F0               [24] 2463 	movx	@dptr,a
                                   2464 ;	..\src\AX_Radio_Lab_output\config.c:49: AX5043_FREQUENCYGAIND0         = 0x08;
      001679 90 41 2A         [24] 2465 	mov	dptr,#_AX5043_FREQUENCYGAIND0
      00167C F0               [24] 2466 	movx	@dptr,a
                                   2467 ;	..\src\AX_Radio_Lab_output\config.c:50: AX5043_AMPLITUDEGAIN0          = 0x06;
      00167D 90 41 2B         [24] 2468 	mov	dptr,#_AX5043_AMPLITUDEGAIN0
      001680 74 06            [12] 2469 	mov	a,#0x06
      001682 F0               [24] 2470 	movx	@dptr,a
                                   2471 ;	..\src\AX_Radio_Lab_output\config.c:51: AX5043_FREQDEV10               = 0x00;
      001683 90 41 2C         [24] 2472 	mov	dptr,#_AX5043_FREQDEV10
      001686 E4               [12] 2473 	clr	a
      001687 F0               [24] 2474 	movx	@dptr,a
                                   2475 ;	..\src\AX_Radio_Lab_output\config.c:52: AX5043_FREQDEV00               = 0x00;
      001688 90 41 2D         [24] 2476 	mov	dptr,#_AX5043_FREQDEV00
      00168B F0               [24] 2477 	movx	@dptr,a
                                   2478 ;	..\src\AX_Radio_Lab_output\config.c:53: AX5043_BBOFFSRES0              = 0x00;
      00168C 90 41 2F         [24] 2479 	mov	dptr,#_AX5043_BBOFFSRES0
      00168F F0               [24] 2480 	movx	@dptr,a
                                   2481 ;	..\src\AX_Radio_Lab_output\config.c:54: AX5043_AGCGAIN1                = 0xC5;
      001690 90 41 30         [24] 2482 	mov	dptr,#_AX5043_AGCGAIN1
      001693 74 C5            [12] 2483 	mov	a,#0xc5
      001695 F0               [24] 2484 	movx	@dptr,a
                                   2485 ;	..\src\AX_Radio_Lab_output\config.c:55: AX5043_AGCTARGET1              = 0x84;
      001696 90 41 31         [24] 2486 	mov	dptr,#_AX5043_AGCTARGET1
      001699 74 84            [12] 2487 	mov	a,#0x84
      00169B F0               [24] 2488 	movx	@dptr,a
                                   2489 ;	..\src\AX_Radio_Lab_output\config.c:56: AX5043_AGCAHYST1               = 0x00;
      00169C 90 41 32         [24] 2490 	mov	dptr,#_AX5043_AGCAHYST1
      00169F E4               [12] 2491 	clr	a
      0016A0 F0               [24] 2492 	movx	@dptr,a
                                   2493 ;	..\src\AX_Radio_Lab_output\config.c:57: AX5043_AGCMINMAX1              = 0x00;
      0016A1 90 41 33         [24] 2494 	mov	dptr,#_AX5043_AGCMINMAX1
      0016A4 F0               [24] 2495 	movx	@dptr,a
                                   2496 ;	..\src\AX_Radio_Lab_output\config.c:58: AX5043_TIMEGAIN1               = 0xF6;
      0016A5 90 41 34         [24] 2497 	mov	dptr,#_AX5043_TIMEGAIN1
      0016A8 74 F6            [12] 2498 	mov	a,#0xf6
      0016AA F0               [24] 2499 	movx	@dptr,a
                                   2500 ;	..\src\AX_Radio_Lab_output\config.c:59: AX5043_DRGAIN1                 = 0xF1;
      0016AB 90 41 35         [24] 2501 	mov	dptr,#_AX5043_DRGAIN1
      0016AE 74 F1            [12] 2502 	mov	a,#0xf1
      0016B0 F0               [24] 2503 	movx	@dptr,a
                                   2504 ;	..\src\AX_Radio_Lab_output\config.c:60: AX5043_PHASEGAIN1              = 0xC3;
      0016B1 90 41 36         [24] 2505 	mov	dptr,#_AX5043_PHASEGAIN1
      0016B4 74 C3            [12] 2506 	mov	a,#0xc3
      0016B6 F0               [24] 2507 	movx	@dptr,a
                                   2508 ;	..\src\AX_Radio_Lab_output\config.c:61: AX5043_FREQUENCYGAINA1         = 0x0F;
      0016B7 90 41 37         [24] 2509 	mov	dptr,#_AX5043_FREQUENCYGAINA1
      0016BA 74 0F            [12] 2510 	mov	a,#0x0f
      0016BC F0               [24] 2511 	movx	@dptr,a
                                   2512 ;	..\src\AX_Radio_Lab_output\config.c:62: AX5043_FREQUENCYGAINB1         = 0x1F;
      0016BD 90 41 38         [24] 2513 	mov	dptr,#_AX5043_FREQUENCYGAINB1
      0016C0 74 1F            [12] 2514 	mov	a,#0x1f
      0016C2 F0               [24] 2515 	movx	@dptr,a
                                   2516 ;	..\src\AX_Radio_Lab_output\config.c:63: AX5043_FREQUENCYGAINC1         = 0x08;
      0016C3 90 41 39         [24] 2517 	mov	dptr,#_AX5043_FREQUENCYGAINC1
      0016C6 74 08            [12] 2518 	mov	a,#0x08
      0016C8 F0               [24] 2519 	movx	@dptr,a
                                   2520 ;	..\src\AX_Radio_Lab_output\config.c:64: AX5043_FREQUENCYGAIND1         = 0x08;
      0016C9 90 41 3A         [24] 2521 	mov	dptr,#_AX5043_FREQUENCYGAIND1
      0016CC F0               [24] 2522 	movx	@dptr,a
                                   2523 ;	..\src\AX_Radio_Lab_output\config.c:65: AX5043_AMPLITUDEGAIN1          = 0x06;
      0016CD 90 41 3B         [24] 2524 	mov	dptr,#_AX5043_AMPLITUDEGAIN1
      0016D0 74 06            [12] 2525 	mov	a,#0x06
      0016D2 F0               [24] 2526 	movx	@dptr,a
                                   2527 ;	..\src\AX_Radio_Lab_output\config.c:66: AX5043_FREQDEV11               = 0x00;
      0016D3 90 41 3C         [24] 2528 	mov	dptr,#_AX5043_FREQDEV11
      0016D6 E4               [12] 2529 	clr	a
      0016D7 F0               [24] 2530 	movx	@dptr,a
                                   2531 ;	..\src\AX_Radio_Lab_output\config.c:67: AX5043_FREQDEV01               = 0x43;
      0016D8 90 41 3D         [24] 2532 	mov	dptr,#_AX5043_FREQDEV01
      0016DB 74 43            [12] 2533 	mov	a,#0x43
      0016DD F0               [24] 2534 	movx	@dptr,a
                                   2535 ;	..\src\AX_Radio_Lab_output\config.c:68: AX5043_FOURFSK1                = 0x16;
      0016DE 90 41 3E         [24] 2536 	mov	dptr,#_AX5043_FOURFSK1
      0016E1 74 16            [12] 2537 	mov	a,#0x16
      0016E3 F0               [24] 2538 	movx	@dptr,a
                                   2539 ;	..\src\AX_Radio_Lab_output\config.c:69: AX5043_BBOFFSRES1              = 0x00;
      0016E4 90 41 3F         [24] 2540 	mov	dptr,#_AX5043_BBOFFSRES1
      0016E7 E4               [12] 2541 	clr	a
      0016E8 F0               [24] 2542 	movx	@dptr,a
                                   2543 ;	..\src\AX_Radio_Lab_output\config.c:70: AX5043_AGCGAIN3                = 0xFF;
      0016E9 90 41 50         [24] 2544 	mov	dptr,#_AX5043_AGCGAIN3
      0016EC 14               [12] 2545 	dec	a
      0016ED F0               [24] 2546 	movx	@dptr,a
                                   2547 ;	..\src\AX_Radio_Lab_output\config.c:71: AX5043_AGCTARGET3              = 0x84;
      0016EE 90 41 51         [24] 2548 	mov	dptr,#_AX5043_AGCTARGET3
      0016F1 74 84            [12] 2549 	mov	a,#0x84
      0016F3 F0               [24] 2550 	movx	@dptr,a
                                   2551 ;	..\src\AX_Radio_Lab_output\config.c:72: AX5043_AGCAHYST3               = 0x00;
      0016F4 90 41 52         [24] 2552 	mov	dptr,#_AX5043_AGCAHYST3
      0016F7 E4               [12] 2553 	clr	a
      0016F8 F0               [24] 2554 	movx	@dptr,a
                                   2555 ;	..\src\AX_Radio_Lab_output\config.c:73: AX5043_AGCMINMAX3              = 0x00;
      0016F9 90 41 53         [24] 2556 	mov	dptr,#_AX5043_AGCMINMAX3
      0016FC F0               [24] 2557 	movx	@dptr,a
                                   2558 ;	..\src\AX_Radio_Lab_output\config.c:74: AX5043_TIMEGAIN3               = 0xF5;
      0016FD 90 41 54         [24] 2559 	mov	dptr,#_AX5043_TIMEGAIN3
      001700 74 F5            [12] 2560 	mov	a,#0xf5
      001702 F0               [24] 2561 	movx	@dptr,a
                                   2562 ;	..\src\AX_Radio_Lab_output\config.c:75: AX5043_DRGAIN3                 = 0xF0;
      001703 90 41 55         [24] 2563 	mov	dptr,#_AX5043_DRGAIN3
      001706 74 F0            [12] 2564 	mov	a,#0xf0
      001708 F0               [24] 2565 	movx	@dptr,a
                                   2566 ;	..\src\AX_Radio_Lab_output\config.c:76: AX5043_PHASEGAIN3              = 0xC3;
      001709 90 41 56         [24] 2567 	mov	dptr,#_AX5043_PHASEGAIN3
      00170C 74 C3            [12] 2568 	mov	a,#0xc3
      00170E F0               [24] 2569 	movx	@dptr,a
                                   2570 ;	..\src\AX_Radio_Lab_output\config.c:77: AX5043_FREQUENCYGAINA3         = 0x0F;
      00170F 90 41 57         [24] 2571 	mov	dptr,#_AX5043_FREQUENCYGAINA3
      001712 74 0F            [12] 2572 	mov	a,#0x0f
      001714 F0               [24] 2573 	movx	@dptr,a
                                   2574 ;	..\src\AX_Radio_Lab_output\config.c:78: AX5043_FREQUENCYGAINB3         = 0x1F;
      001715 90 41 58         [24] 2575 	mov	dptr,#_AX5043_FREQUENCYGAINB3
      001718 74 1F            [12] 2576 	mov	a,#0x1f
      00171A F0               [24] 2577 	movx	@dptr,a
                                   2578 ;	..\src\AX_Radio_Lab_output\config.c:79: AX5043_FREQUENCYGAINC3         = 0x0C;
      00171B 90 41 59         [24] 2579 	mov	dptr,#_AX5043_FREQUENCYGAINC3
      00171E 74 0C            [12] 2580 	mov	a,#0x0c
      001720 F0               [24] 2581 	movx	@dptr,a
                                   2582 ;	..\src\AX_Radio_Lab_output\config.c:80: AX5043_FREQUENCYGAIND3         = 0x0C;
      001721 90 41 5A         [24] 2583 	mov	dptr,#_AX5043_FREQUENCYGAIND3
      001724 F0               [24] 2584 	movx	@dptr,a
                                   2585 ;	..\src\AX_Radio_Lab_output\config.c:81: AX5043_AMPLITUDEGAIN3          = 0x06;
      001725 90 41 5B         [24] 2586 	mov	dptr,#_AX5043_AMPLITUDEGAIN3
      001728 03               [12] 2587 	rr	a
      001729 F0               [24] 2588 	movx	@dptr,a
                                   2589 ;	..\src\AX_Radio_Lab_output\config.c:82: AX5043_FREQDEV13               = 0x00;
      00172A 90 41 5C         [24] 2590 	mov	dptr,#_AX5043_FREQDEV13
      00172D E4               [12] 2591 	clr	a
      00172E F0               [24] 2592 	movx	@dptr,a
                                   2593 ;	..\src\AX_Radio_Lab_output\config.c:83: AX5043_FREQDEV03               = 0x43;
      00172F 90 41 5D         [24] 2594 	mov	dptr,#_AX5043_FREQDEV03
      001732 74 43            [12] 2595 	mov	a,#0x43
      001734 F0               [24] 2596 	movx	@dptr,a
                                   2597 ;	..\src\AX_Radio_Lab_output\config.c:84: AX5043_FOURFSK3                = 0x16;
      001735 90 41 5E         [24] 2598 	mov	dptr,#_AX5043_FOURFSK3
      001738 74 16            [12] 2599 	mov	a,#0x16
      00173A F0               [24] 2600 	movx	@dptr,a
                                   2601 ;	..\src\AX_Radio_Lab_output\config.c:85: AX5043_BBOFFSRES3              = 0x00;
      00173B 90 41 5F         [24] 2602 	mov	dptr,#_AX5043_BBOFFSRES3
      00173E E4               [12] 2603 	clr	a
      00173F F0               [24] 2604 	movx	@dptr,a
                                   2605 ;	..\src\AX_Radio_Lab_output\config.c:86: AX5043_MODCFGF                 = 0x03;
      001740 90 41 60         [24] 2606 	mov	dptr,#_AX5043_MODCFGF
      001743 74 03            [12] 2607 	mov	a,#0x03
      001745 F0               [24] 2608 	movx	@dptr,a
                                   2609 ;	..\src\AX_Radio_Lab_output\config.c:87: AX5043_FSKDEV2                 = 0x00;
      001746 90 41 61         [24] 2610 	mov	dptr,#_AX5043_FSKDEV2
      001749 E4               [12] 2611 	clr	a
      00174A F0               [24] 2612 	movx	@dptr,a
                                   2613 ;	..\src\AX_Radio_Lab_output\config.c:88: AX5043_FSKDEV1                 = 0x04;
      00174B 90 41 62         [24] 2614 	mov	dptr,#_AX5043_FSKDEV1
      00174E 74 04            [12] 2615 	mov	a,#0x04
      001750 F0               [24] 2616 	movx	@dptr,a
                                   2617 ;	..\src\AX_Radio_Lab_output\config.c:89: AX5043_FSKDEV0                 = 0x5E;
      001751 90 41 63         [24] 2618 	mov	dptr,#_AX5043_FSKDEV0
      001754 74 5E            [12] 2619 	mov	a,#0x5e
      001756 F0               [24] 2620 	movx	@dptr,a
                                   2621 ;	..\src\AX_Radio_Lab_output\config.c:90: AX5043_MODCFGA                 = 0x05;
      001757 90 41 64         [24] 2622 	mov	dptr,#_AX5043_MODCFGA
      00175A 74 05            [12] 2623 	mov	a,#0x05
      00175C F0               [24] 2624 	movx	@dptr,a
                                   2625 ;	..\src\AX_Radio_Lab_output\config.c:91: AX5043_TXRATE2                 = 0x00;
      00175D 90 41 65         [24] 2626 	mov	dptr,#_AX5043_TXRATE2
      001760 E4               [12] 2627 	clr	a
      001761 F0               [24] 2628 	movx	@dptr,a
                                   2629 ;	..\src\AX_Radio_Lab_output\config.c:92: AX5043_TXRATE1                 = 0x0D;
      001762 90 41 66         [24] 2630 	mov	dptr,#_AX5043_TXRATE1
      001765 74 0D            [12] 2631 	mov	a,#0x0d
      001767 F0               [24] 2632 	movx	@dptr,a
                                   2633 ;	..\src\AX_Radio_Lab_output\config.c:93: AX5043_TXRATE0                 = 0x1B;
      001768 90 41 67         [24] 2634 	mov	dptr,#_AX5043_TXRATE0
      00176B 74 1B            [12] 2635 	mov	a,#0x1b
      00176D F0               [24] 2636 	movx	@dptr,a
                                   2637 ;	..\src\AX_Radio_Lab_output\config.c:94: AX5043_TXPWRCOEFFB1            = TX_POWER_COEFF1;
      00176E 90 41 6A         [24] 2638 	mov	dptr,#_AX5043_TXPWRCOEFFB1
      001771 74 0F            [12] 2639 	mov	a,#0x0f
      001773 F0               [24] 2640 	movx	@dptr,a
                                   2641 ;	..\src\AX_Radio_Lab_output\config.c:95: AX5043_TXPWRCOEFFB0            = TX_POWER_COEFF0;
      001774 90 41 6B         [24] 2642 	mov	dptr,#_AX5043_TXPWRCOEFFB0
      001777 74 FF            [12] 2643 	mov	a,#0xff
      001779 F0               [24] 2644 	movx	@dptr,a
                                   2645 ;	..\src\AX_Radio_Lab_output\config.c:96: AX5043_PLLVCOI                 = 0x98;
      00177A 90 41 80         [24] 2646 	mov	dptr,#_AX5043_PLLVCOI
      00177D 74 98            [12] 2647 	mov	a,#0x98
      00177F F0               [24] 2648 	movx	@dptr,a
                                   2649 ;	..\src\AX_Radio_Lab_output\config.c:97: AX5043_PLLRNGCLK               = 0x05;
      001780 90 41 83         [24] 2650 	mov	dptr,#_AX5043_PLLRNGCLK
      001783 74 05            [12] 2651 	mov	a,#0x05
      001785 F0               [24] 2652 	movx	@dptr,a
                                   2653 ;	..\src\AX_Radio_Lab_output\config.c:98: AX5043_BBTUNE                  = 0x0F;
      001786 90 41 88         [24] 2654 	mov	dptr,#_AX5043_BBTUNE
      001789 74 0F            [12] 2655 	mov	a,#0x0f
      00178B F0               [24] 2656 	movx	@dptr,a
                                   2657 ;	..\src\AX_Radio_Lab_output\config.c:99: AX5043_BBOFFSCAP               = 0x77;
      00178C 90 41 89         [24] 2658 	mov	dptr,#_AX5043_BBOFFSCAP
      00178F 74 77            [12] 2659 	mov	a,#0x77
      001791 F0               [24] 2660 	movx	@dptr,a
                                   2661 ;	..\src\AX_Radio_Lab_output\config.c:100: AX5043_PKTADDRCFG              = 0x01;
      001792 90 42 00         [24] 2662 	mov	dptr,#_AX5043_PKTADDRCFG
      001795 74 01            [12] 2663 	mov	a,#0x01
      001797 F0               [24] 2664 	movx	@dptr,a
                                   2665 ;	..\src\AX_Radio_Lab_output\config.c:101: AX5043_PKTLENCFG               = 0x80;
      001798 90 42 01         [24] 2666 	mov	dptr,#_AX5043_PKTLENCFG
      00179B 03               [12] 2667 	rr	a
      00179C F0               [24] 2668 	movx	@dptr,a
                                   2669 ;	..\src\AX_Radio_Lab_output\config.c:102: AX5043_PKTLENOFFSET            = 0x00;
      00179D 90 42 02         [24] 2670 	mov	dptr,#_AX5043_PKTLENOFFSET
      0017A0 E4               [12] 2671 	clr	a
      0017A1 F0               [24] 2672 	movx	@dptr,a
                                   2673 ;	..\src\AX_Radio_Lab_output\config.c:103: AX5043_PKTMAXLEN               = 0xC8;
      0017A2 90 42 03         [24] 2674 	mov	dptr,#_AX5043_PKTMAXLEN
      0017A5 74 C8            [12] 2675 	mov	a,#0xc8
      0017A7 F0               [24] 2676 	movx	@dptr,a
                                   2677 ;	..\src\AX_Radio_Lab_output\config.c:104: AX5043_MATCH0PAT3              = 0xA5;
      0017A8 90 42 10         [24] 2678 	mov	dptr,#_AX5043_MATCH0PAT3
      0017AB 74 A5            [12] 2679 	mov	a,#0xa5
      0017AD F0               [24] 2680 	movx	@dptr,a
                                   2681 ;	..\src\AX_Radio_Lab_output\config.c:105: AX5043_MATCH0PAT2              = 0x00;
      0017AE 90 42 11         [24] 2682 	mov	dptr,#_AX5043_MATCH0PAT2
      0017B1 E4               [12] 2683 	clr	a
      0017B2 F0               [24] 2684 	movx	@dptr,a
                                   2685 ;	..\src\AX_Radio_Lab_output\config.c:106: AX5043_MATCH0PAT1              = 0x00;
      0017B3 90 42 12         [24] 2686 	mov	dptr,#_AX5043_MATCH0PAT1
      0017B6 F0               [24] 2687 	movx	@dptr,a
                                   2688 ;	..\src\AX_Radio_Lab_output\config.c:107: AX5043_MATCH0PAT0              = 0x00;
      0017B7 90 42 13         [24] 2689 	mov	dptr,#_AX5043_MATCH0PAT0
      0017BA F0               [24] 2690 	movx	@dptr,a
                                   2691 ;	..\src\AX_Radio_Lab_output\config.c:108: AX5043_MATCH0LEN               = 0x87;
      0017BB 90 42 14         [24] 2692 	mov	dptr,#_AX5043_MATCH0LEN
      0017BE 74 87            [12] 2693 	mov	a,#0x87
      0017C0 F0               [24] 2694 	movx	@dptr,a
                                   2695 ;	..\src\AX_Radio_Lab_output\config.c:109: AX5043_MATCH0MAX               = 0x07;
      0017C1 90 42 16         [24] 2696 	mov	dptr,#_AX5043_MATCH0MAX
      0017C4 74 07            [12] 2697 	mov	a,#0x07
      0017C6 F0               [24] 2698 	movx	@dptr,a
                                   2699 ;	..\src\AX_Radio_Lab_output\config.c:110: AX5043_MATCH1PAT1              = 0x55;
      0017C7 90 42 18         [24] 2700 	mov	dptr,#_AX5043_MATCH1PAT1
      0017CA 74 55            [12] 2701 	mov	a,#0x55
      0017CC F0               [24] 2702 	movx	@dptr,a
                                   2703 ;	..\src\AX_Radio_Lab_output\config.c:111: AX5043_MATCH1PAT0              = 0x55;
      0017CD 90 42 19         [24] 2704 	mov	dptr,#_AX5043_MATCH1PAT0
      0017D0 F0               [24] 2705 	movx	@dptr,a
                                   2706 ;	..\src\AX_Radio_Lab_output\config.c:112: AX5043_MATCH1LEN               = 0x8A;
      0017D1 90 42 1C         [24] 2707 	mov	dptr,#_AX5043_MATCH1LEN
      0017D4 74 8A            [12] 2708 	mov	a,#0x8a
      0017D6 F0               [24] 2709 	movx	@dptr,a
                                   2710 ;	..\src\AX_Radio_Lab_output\config.c:113: AX5043_MATCH1MAX               = 0x0A;
      0017D7 90 42 1E         [24] 2711 	mov	dptr,#_AX5043_MATCH1MAX
      0017DA 74 0A            [12] 2712 	mov	a,#0x0a
      0017DC F0               [24] 2713 	movx	@dptr,a
                                   2714 ;	..\src\AX_Radio_Lab_output\config.c:114: AX5043_TMGTXBOOST              = 0x5B;
      0017DD 90 42 20         [24] 2715 	mov	dptr,#_AX5043_TMGTXBOOST
      0017E0 74 5B            [12] 2716 	mov	a,#0x5b
      0017E2 F0               [24] 2717 	movx	@dptr,a
                                   2718 ;	..\src\AX_Radio_Lab_output\config.c:115: AX5043_TMGTXSETTLE             = 0x3E;
      0017E3 90 42 21         [24] 2719 	mov	dptr,#_AX5043_TMGTXSETTLE
      0017E6 74 3E            [12] 2720 	mov	a,#0x3e
      0017E8 F0               [24] 2721 	movx	@dptr,a
                                   2722 ;	..\src\AX_Radio_Lab_output\config.c:116: AX5043_TMGRXBOOST              = 0x5B;
      0017E9 90 42 23         [24] 2723 	mov	dptr,#_AX5043_TMGRXBOOST
      0017EC 74 5B            [12] 2724 	mov	a,#0x5b
      0017EE F0               [24] 2725 	movx	@dptr,a
                                   2726 ;	..\src\AX_Radio_Lab_output\config.c:117: AX5043_TMGRXSETTLE             = 0x3E;
      0017EF 90 42 24         [24] 2727 	mov	dptr,#_AX5043_TMGRXSETTLE
      0017F2 74 3E            [12] 2728 	mov	a,#0x3e
      0017F4 F0               [24] 2729 	movx	@dptr,a
                                   2730 ;	..\src\AX_Radio_Lab_output\config.c:118: AX5043_TMGRXOFFSACQ            = 0x00;
      0017F5 90 42 25         [24] 2731 	mov	dptr,#_AX5043_TMGRXOFFSACQ
      0017F8 E4               [12] 2732 	clr	a
      0017F9 F0               [24] 2733 	movx	@dptr,a
                                   2734 ;	..\src\AX_Radio_Lab_output\config.c:119: AX5043_TMGRXCOARSEAGC          = 0x9C;
      0017FA 90 42 26         [24] 2735 	mov	dptr,#_AX5043_TMGRXCOARSEAGC
      0017FD 74 9C            [12] 2736 	mov	a,#0x9c
      0017FF F0               [24] 2737 	movx	@dptr,a
                                   2738 ;	..\src\AX_Radio_Lab_output\config.c:120: AX5043_TMGRXRSSI               = 0x03;
      001800 90 42 28         [24] 2739 	mov	dptr,#_AX5043_TMGRXRSSI
      001803 74 03            [12] 2740 	mov	a,#0x03
      001805 F0               [24] 2741 	movx	@dptr,a
                                   2742 ;	..\src\AX_Radio_Lab_output\config.c:121: AX5043_TMGRXPREAMBLE2          = 0x12;
      001806 90 42 2A         [24] 2743 	mov	dptr,#_AX5043_TMGRXPREAMBLE2
      001809 74 12            [12] 2744 	mov	a,#0x12
      00180B F0               [24] 2745 	movx	@dptr,a
                                   2746 ;	..\src\AX_Radio_Lab_output\config.c:122: AX5043_RSSIABSTHR              = 0xE3;
      00180C 90 42 2D         [24] 2747 	mov	dptr,#_AX5043_RSSIABSTHR
      00180F 74 E3            [12] 2748 	mov	a,#0xe3
      001811 F0               [24] 2749 	movx	@dptr,a
                                   2750 ;	..\src\AX_Radio_Lab_output\config.c:123: AX5043_BGNDRSSITHR             = 0x00;
      001812 90 42 2F         [24] 2751 	mov	dptr,#_AX5043_BGNDRSSITHR
      001815 E4               [12] 2752 	clr	a
      001816 F0               [24] 2753 	movx	@dptr,a
                                   2754 ;	..\src\AX_Radio_Lab_output\config.c:124: AX5043_PKTCHUNKSIZE            = 0x0D;
      001817 90 42 30         [24] 2755 	mov	dptr,#_AX5043_PKTCHUNKSIZE
      00181A 74 0D            [12] 2756 	mov	a,#0x0d
      00181C F0               [24] 2757 	movx	@dptr,a
                                   2758 ;	..\src\AX_Radio_Lab_output\config.c:125: AX5043_PKTACCEPTFLAGS          = 0x20;
      00181D 90 42 33         [24] 2759 	mov	dptr,#_AX5043_PKTACCEPTFLAGS
      001820 74 20            [12] 2760 	mov	a,#0x20
      001822 F0               [24] 2761 	movx	@dptr,a
                                   2762 ;	..\src\AX_Radio_Lab_output\config.c:126: AX5043_DACVALUE1               = 0x00;
      001823 90 43 30         [24] 2763 	mov	dptr,#_AX5043_DACVALUE1
      001826 E4               [12] 2764 	clr	a
      001827 F0               [24] 2765 	movx	@dptr,a
                                   2766 ;	..\src\AX_Radio_Lab_output\config.c:127: AX5043_DACVALUE0               = 0x00;
      001828 90 43 31         [24] 2767 	mov	dptr,#_AX5043_DACVALUE0
      00182B F0               [24] 2768 	movx	@dptr,a
                                   2769 ;	..\src\AX_Radio_Lab_output\config.c:128: AX5043_DACCONFIG               = 0x00;
      00182C 90 43 32         [24] 2770 	mov	dptr,#_AX5043_DACCONFIG
      00182F F0               [24] 2771 	movx	@dptr,a
                                   2772 ;	..\src\AX_Radio_Lab_output\config.c:129: AX5043_REF                     = 0x03;
      001830 90 4F 0D         [24] 2773 	mov	dptr,#_AX5043_REF
      001833 74 03            [12] 2774 	mov	a,#0x03
      001835 F0               [24] 2775 	movx	@dptr,a
                                   2776 ;	..\src\AX_Radio_Lab_output\config.c:130: AX5043_XTALOSC                 = 0x04;
      001836 90 4F 10         [24] 2777 	mov	dptr,#_AX5043_XTALOSC
      001839 04               [12] 2778 	inc	a
      00183A F0               [24] 2779 	movx	@dptr,a
                                   2780 ;	..\src\AX_Radio_Lab_output\config.c:131: AX5043_XTALAMPL                = 0x00;
      00183B 90 4F 11         [24] 2781 	mov	dptr,#_AX5043_XTALAMPL
      00183E E4               [12] 2782 	clr	a
      00183F F0               [24] 2783 	movx	@dptr,a
                                   2784 ;	..\src\AX_Radio_Lab_output\config.c:132: AX5043_0xF1C                   = 0x07;
      001840 90 4F 1C         [24] 2785 	mov	dptr,#_AX5043_0xF1C
      001843 74 07            [12] 2786 	mov	a,#0x07
      001845 F0               [24] 2787 	movx	@dptr,a
                                   2788 ;	..\src\AX_Radio_Lab_output\config.c:133: AX5043_0xF21                   = 0x68;
      001846 90 4F 21         [24] 2789 	mov	dptr,#_AX5043_0xF21
      001849 74 68            [12] 2790 	mov	a,#0x68
      00184B F0               [24] 2791 	movx	@dptr,a
                                   2792 ;	..\src\AX_Radio_Lab_output\config.c:134: AX5043_0xF22                   = 0xFF;
      00184C 90 4F 22         [24] 2793 	mov	dptr,#_AX5043_0xF22
      00184F 74 FF            [12] 2794 	mov	a,#0xff
      001851 F0               [24] 2795 	movx	@dptr,a
                                   2796 ;	..\src\AX_Radio_Lab_output\config.c:135: AX5043_0xF23                   = 0x84;
      001852 90 4F 23         [24] 2797 	mov	dptr,#_AX5043_0xF23
      001855 74 84            [12] 2798 	mov	a,#0x84
      001857 F0               [24] 2799 	movx	@dptr,a
                                   2800 ;	..\src\AX_Radio_Lab_output\config.c:136: AX5043_0xF26                   = 0x98;
      001858 90 4F 26         [24] 2801 	mov	dptr,#_AX5043_0xF26
      00185B 74 98            [12] 2802 	mov	a,#0x98
      00185D F0               [24] 2803 	movx	@dptr,a
                                   2804 ;	..\src\AX_Radio_Lab_output\config.c:137: AX5043_0xF34                   = 0x28;
      00185E 90 4F 34         [24] 2805 	mov	dptr,#_AX5043_0xF34
      001861 74 28            [12] 2806 	mov	a,#0x28
      001863 F0               [24] 2807 	movx	@dptr,a
                                   2808 ;	..\src\AX_Radio_Lab_output\config.c:138: AX5043_0xF35                   = 0x11;
      001864 90 4F 35         [24] 2809 	mov	dptr,#_AX5043_0xF35
      001867 74 11            [12] 2810 	mov	a,#0x11
      001869 F0               [24] 2811 	movx	@dptr,a
                                   2812 ;	..\src\AX_Radio_Lab_output\config.c:139: AX5043_0xF44                   = 0x25;
      00186A 90 4F 44         [24] 2813 	mov	dptr,#_AX5043_0xF44
      00186D 74 25            [12] 2814 	mov	a,#0x25
      00186F F0               [24] 2815 	movx	@dptr,a
                                   2816 ;	..\src\AX_Radio_Lab_output\config.c:140: }
      001870 22               [24] 2817 	ret
                                   2818 ;------------------------------------------------------------
                                   2819 ;Allocation info for local variables in function 'ax5043_set_registers_tx'
                                   2820 ;------------------------------------------------------------
                                   2821 ;	..\src\AX_Radio_Lab_output\config.c:143: __reentrantb void ax5043_set_registers_tx(void) __reentrant
                                   2822 ;	-----------------------------------------
                                   2823 ;	 function ax5043_set_registers_tx
                                   2824 ;	-----------------------------------------
      001871                       2825 _ax5043_set_registers_tx:
                                   2826 ;	..\src\AX_Radio_Lab_output\config.c:145: AX5043_PLLLOOP                 = 0x09;
      001871 90 40 30         [24] 2827 	mov	dptr,#_AX5043_PLLLOOP
      001874 74 09            [12] 2828 	mov	a,#0x09
      001876 F0               [24] 2829 	movx	@dptr,a
                                   2830 ;	..\src\AX_Radio_Lab_output\config.c:146: AX5043_PLLCPI                  = 0x02;
      001877 90 40 31         [24] 2831 	mov	dptr,#_AX5043_PLLCPI
      00187A 74 02            [12] 2832 	mov	a,#0x02
      00187C F0               [24] 2833 	movx	@dptr,a
                                   2834 ;	..\src\AX_Radio_Lab_output\config.c:147: AX5043_PLLVCODIV               = 0x24;
      00187D 90 40 32         [24] 2835 	mov	dptr,#_AX5043_PLLVCODIV
      001880 74 24            [12] 2836 	mov	a,#0x24
      001882 F0               [24] 2837 	movx	@dptr,a
                                   2838 ;	..\src\AX_Radio_Lab_output\config.c:148: AX5043_XTALCAP                 = 0x00;
      001883 90 41 84         [24] 2839 	mov	dptr,#_AX5043_XTALCAP
      001886 E4               [12] 2840 	clr	a
      001887 F0               [24] 2841 	movx	@dptr,a
                                   2842 ;	..\src\AX_Radio_Lab_output\config.c:149: AX5043_0xF00                   = 0x0F;
      001888 90 4F 00         [24] 2843 	mov	dptr,#_AX5043_0xF00
      00188B 74 0F            [12] 2844 	mov	a,#0x0f
      00188D F0               [24] 2845 	movx	@dptr,a
                                   2846 ;	..\src\AX_Radio_Lab_output\config.c:150: AX5043_0xF18                   = 0x06;
      00188E 90 4F 18         [24] 2847 	mov	dptr,#_AX5043_0xF18
      001891 74 06            [12] 2848 	mov	a,#0x06
      001893 F0               [24] 2849 	movx	@dptr,a
                                   2850 ;	..\src\AX_Radio_Lab_output\config.c:151: }
      001894 22               [24] 2851 	ret
                                   2852 ;------------------------------------------------------------
                                   2853 ;Allocation info for local variables in function 'ax5043_set_registers_rx'
                                   2854 ;------------------------------------------------------------
                                   2855 ;	..\src\AX_Radio_Lab_output\config.c:154: __reentrantb void ax5043_set_registers_rx(void) __reentrant
                                   2856 ;	-----------------------------------------
                                   2857 ;	 function ax5043_set_registers_rx
                                   2858 ;	-----------------------------------------
      001895                       2859 _ax5043_set_registers_rx:
                                   2860 ;	..\src\AX_Radio_Lab_output\config.c:156: AX5043_PLLLOOP                 = 0x0B;
      001895 90 40 30         [24] 2861 	mov	dptr,#_AX5043_PLLLOOP
      001898 74 0B            [12] 2862 	mov	a,#0x0b
      00189A F0               [24] 2863 	movx	@dptr,a
                                   2864 ;	..\src\AX_Radio_Lab_output\config.c:157: AX5043_PLLCPI                  = 0x10;
      00189B 90 40 31         [24] 2865 	mov	dptr,#_AX5043_PLLCPI
      00189E 74 10            [12] 2866 	mov	a,#0x10
      0018A0 F0               [24] 2867 	movx	@dptr,a
                                   2868 ;	..\src\AX_Radio_Lab_output\config.c:158: AX5043_PLLVCODIV               = 0x25;
      0018A1 90 40 32         [24] 2869 	mov	dptr,#_AX5043_PLLVCODIV
      0018A4 74 25            [12] 2870 	mov	a,#0x25
      0018A6 F0               [24] 2871 	movx	@dptr,a
                                   2872 ;	..\src\AX_Radio_Lab_output\config.c:159: AX5043_XTALCAP                 = 0x00;
      0018A7 90 41 84         [24] 2873 	mov	dptr,#_AX5043_XTALCAP
      0018AA E4               [12] 2874 	clr	a
      0018AB F0               [24] 2875 	movx	@dptr,a
                                   2876 ;	..\src\AX_Radio_Lab_output\config.c:160: AX5043_0xF00                   = 0x0F;
      0018AC 90 4F 00         [24] 2877 	mov	dptr,#_AX5043_0xF00
      0018AF 74 0F            [12] 2878 	mov	a,#0x0f
      0018B1 F0               [24] 2879 	movx	@dptr,a
                                   2880 ;	..\src\AX_Radio_Lab_output\config.c:161: AX5043_0xF18                   = 0x02;
      0018B2 90 4F 18         [24] 2881 	mov	dptr,#_AX5043_0xF18
      0018B5 74 02            [12] 2882 	mov	a,#0x02
      0018B7 F0               [24] 2883 	movx	@dptr,a
                                   2884 ;	..\src\AX_Radio_Lab_output\config.c:162: }
      0018B8 22               [24] 2885 	ret
                                   2886 ;------------------------------------------------------------
                                   2887 ;Allocation info for local variables in function 'ax5043_set_registers_rxwor'
                                   2888 ;------------------------------------------------------------
                                   2889 ;	..\src\AX_Radio_Lab_output\config.c:165: __reentrantb void ax5043_set_registers_rxwor(void) __reentrant
                                   2890 ;	-----------------------------------------
                                   2891 ;	 function ax5043_set_registers_rxwor
                                   2892 ;	-----------------------------------------
      0018B9                       2893 _ax5043_set_registers_rxwor:
                                   2894 ;	..\src\AX_Radio_Lab_output\config.c:167: AX5043_TMGRXAGC                = 0x02;
      0018B9 90 42 27         [24] 2895 	mov	dptr,#_AX5043_TMGRXAGC
      0018BC 74 02            [12] 2896 	mov	a,#0x02
      0018BE F0               [24] 2897 	movx	@dptr,a
                                   2898 ;	..\src\AX_Radio_Lab_output\config.c:168: AX5043_TMGRXPREAMBLE1          = 0x19;
      0018BF 90 42 29         [24] 2899 	mov	dptr,#_AX5043_TMGRXPREAMBLE1
      0018C2 74 19            [12] 2900 	mov	a,#0x19
      0018C4 F0               [24] 2901 	movx	@dptr,a
                                   2902 ;	..\src\AX_Radio_Lab_output\config.c:169: AX5043_PKTMISCFLAGS            = 0x03;
      0018C5 90 42 31         [24] 2903 	mov	dptr,#_AX5043_PKTMISCFLAGS
      0018C8 74 03            [12] 2904 	mov	a,#0x03
      0018CA F0               [24] 2905 	movx	@dptr,a
                                   2906 ;	..\src\AX_Radio_Lab_output\config.c:170: }
      0018CB 22               [24] 2907 	ret
                                   2908 ;------------------------------------------------------------
                                   2909 ;Allocation info for local variables in function 'ax5043_set_registers_rxcont'
                                   2910 ;------------------------------------------------------------
                                   2911 ;	..\src\AX_Radio_Lab_output\config.c:173: __reentrantb void ax5043_set_registers_rxcont(void) __reentrant
                                   2912 ;	-----------------------------------------
                                   2913 ;	 function ax5043_set_registers_rxcont
                                   2914 ;	-----------------------------------------
      0018CC                       2915 _ax5043_set_registers_rxcont:
                                   2916 ;	..\src\AX_Radio_Lab_output\config.c:175: AX5043_TMGRXAGC                = 0x00;
      0018CC 90 42 27         [24] 2917 	mov	dptr,#_AX5043_TMGRXAGC
      0018CF E4               [12] 2918 	clr	a
      0018D0 F0               [24] 2919 	movx	@dptr,a
                                   2920 ;	..\src\AX_Radio_Lab_output\config.c:176: AX5043_TMGRXPREAMBLE1          = 0x00;
      0018D1 90 42 29         [24] 2921 	mov	dptr,#_AX5043_TMGRXPREAMBLE1
      0018D4 F0               [24] 2922 	movx	@dptr,a
                                   2923 ;	..\src\AX_Radio_Lab_output\config.c:177: AX5043_PKTMISCFLAGS            = 0x00;
      0018D5 90 42 31         [24] 2924 	mov	dptr,#_AX5043_PKTMISCFLAGS
      0018D8 F0               [24] 2925 	movx	@dptr,a
                                   2926 ;	..\src\AX_Radio_Lab_output\config.c:178: }
      0018D9 22               [24] 2927 	ret
                                   2928 ;------------------------------------------------------------
                                   2929 ;Allocation info for local variables in function 'ax5043_set_registers_rxcont_singleparamset'
                                   2930 ;------------------------------------------------------------
                                   2931 ;	..\src\AX_Radio_Lab_output\config.c:181: __reentrantb void ax5043_set_registers_rxcont_singleparamset(void) __reentrant
                                   2932 ;	-----------------------------------------
                                   2933 ;	 function ax5043_set_registers_rxcont_singleparamset
                                   2934 ;	-----------------------------------------
      0018DA                       2935 _ax5043_set_registers_rxcont_singleparamset:
                                   2936 ;	..\src\AX_Radio_Lab_output\config.c:183: AX5043_RXPARAMSETS             = 0xFF;
      0018DA 90 41 17         [24] 2937 	mov	dptr,#_AX5043_RXPARAMSETS
      0018DD 74 FF            [12] 2938 	mov	a,#0xff
      0018DF F0               [24] 2939 	movx	@dptr,a
                                   2940 ;	..\src\AX_Radio_Lab_output\config.c:184: AX5043_FREQDEV13               = 0x00;
      0018E0 90 41 5C         [24] 2941 	mov	dptr,#_AX5043_FREQDEV13
      0018E3 E4               [12] 2942 	clr	a
      0018E4 F0               [24] 2943 	movx	@dptr,a
                                   2944 ;	..\src\AX_Radio_Lab_output\config.c:185: AX5043_FREQDEV03               = 0x00;
      0018E5 90 41 5D         [24] 2945 	mov	dptr,#_AX5043_FREQDEV03
      0018E8 F0               [24] 2946 	movx	@dptr,a
                                   2947 ;	..\src\AX_Radio_Lab_output\config.c:186: AX5043_AGCGAIN3                = 0xE7;
      0018E9 90 41 50         [24] 2948 	mov	dptr,#_AX5043_AGCGAIN3
      0018EC 74 E7            [12] 2949 	mov	a,#0xe7
      0018EE F0               [24] 2950 	movx	@dptr,a
                                   2951 ;	..\src\AX_Radio_Lab_output\config.c:187: }
      0018EF 22               [24] 2952 	ret
                                   2953 ;------------------------------------------------------------
                                   2954 ;Allocation info for local variables in function 'axradio_setup_pincfg1'
                                   2955 ;------------------------------------------------------------
                                   2956 ;	..\src\AX_Radio_Lab_output\config.c:191: __reentrantb void axradio_setup_pincfg1(void) __reentrant
                                   2957 ;	-----------------------------------------
                                   2958 ;	 function axradio_setup_pincfg1
                                   2959 ;	-----------------------------------------
      0018F0                       2960 _axradio_setup_pincfg1:
                                   2961 ;	..\src\AX_Radio_Lab_output\config.c:193: PALTRADIO = 0xc0; //pass through PWRAMP -> PB2 ANTSEL -> PB3
      0018F0 90 70 46         [24] 2962 	mov	dptr,#_PALTRADIO
      0018F3 74 C0            [12] 2963 	mov	a,#0xc0
      0018F5 F0               [24] 2964 	movx	@dptr,a
                                   2965 ;	..\src\AX_Radio_Lab_output\config.c:194: }
      0018F6 22               [24] 2966 	ret
                                   2967 ;------------------------------------------------------------
                                   2968 ;Allocation info for local variables in function 'axradio_setup_pincfg2'
                                   2969 ;------------------------------------------------------------
                                   2970 ;	..\src\AX_Radio_Lab_output\config.c:196: __reentrantb void axradio_setup_pincfg2(void) __reentrant
                                   2971 ;	-----------------------------------------
                                   2972 ;	 function axradio_setup_pincfg2
                                   2973 ;	-----------------------------------------
      0018F7                       2974 _axradio_setup_pincfg2:
                                   2975 ;	..\src\AX_Radio_Lab_output\config.c:198: PORTR = (PORTR & 0x3F) | 0x00; //AX8052F143 --> no pull-ups on PR6, PR7
      0018F7 53 8C 3F         [24] 2976 	anl	_PORTR,#0x3f
                                   2977 ;	..\src\AX_Radio_Lab_output\config.c:199: }
      0018FA 22               [24] 2978 	ret
                                   2979 ;------------------------------------------------------------
                                   2980 ;Allocation info for local variables in function 'axradio_conv_freq_fromhz'
                                   2981 ;------------------------------------------------------------
                                   2982 ;f                         Allocated with name '_axradio_conv_freq_fromhz_f_65536_132'
                                   2983 ;------------------------------------------------------------
                                   2984 ;	..\src\AX_Radio_Lab_output\config.c:604: int32_t axradio_conv_freq_fromhz(int32_t f)
                                   2985 ;	-----------------------------------------
                                   2986 ;	 function axradio_conv_freq_fromhz
                                   2987 ;	-----------------------------------------
      0018FB                       2988 _axradio_conv_freq_fromhz:
                                   2989 ;	..\src\AX_Radio_Lab_output\config.c:610: CONSTMULFIX24(0x597a7e);
      0018FB A8 82            [24] 2990 	mov r0,dpl 
      0018FD A9 83            [24] 2991 	mov r1,dph 
      0018FF AA F0            [24] 2992 	mov r2,b 
      001901 FB               [12] 2993 	mov r3,a 
      001902 C0 E0            [24] 2994 	push acc 
      001904 30 E7 0D         [24] 2995 	jnb acc.7,00000$ 
      001907 C3               [12] 2996 	clr c 
      001908 E4               [12] 2997 	clr a 
      001909 98               [12] 2998 	subb a,r0 
      00190A F8               [12] 2999 	mov r0,a 
      00190B E4               [12] 3000 	clr a 
      00190C 99               [12] 3001 	subb a,r1 
      00190D F9               [12] 3002 	mov r1,a 
      00190E E4               [12] 3003 	clr a 
      00190F 9A               [12] 3004 	subb a,r2 
      001910 FA               [12] 3005 	mov r2,a 
      001911 E4               [12] 3006 	clr a 
      001912 9B               [12] 3007 	subb a,r3 
      001913 FB               [12] 3008 	mov r3,a 
      001914                       3009 	 00000$:
      001914 E4               [12] 3010 	clr a 
      001915 FC               [12] 3011 	mov r4,a 
      001916 FD               [12] 3012 	mov r5,a 
      001917 FE               [12] 3013 	mov r6,a 
      001918 FF               [12] 3014 	mov r7,a 
                                   3015 ;; stage -1 
                           000001  3016 	.if (((0x597a7e)>>16)&0xff) 
      001919 74 59            [12] 3017 	mov a,# (((0x597a7e)>>16)&0xff) 
      00191B 88 F0            [24] 3018 	mov b,r0 
      00191D A4               [48] 3019 	mul ab 
      00191E FF               [12] 3020 	mov r7,a 
      00191F AC F0            [24] 3021 	mov r4,b 
                                   3022 	.endif 
                           000001  3023 	.if (((0x597a7e)>>8)&0xff) 
      001921 74 7A            [12] 3024 	mov a,# (((0x597a7e)>>8)&0xff) 
      001923 89 F0            [24] 3025 	mov b,r1 
      001925 A4               [48] 3026 	mul ab 
                           000001  3027 	.if (((0x597a7e)>>16)&0xff) 
      001926 2F               [12] 3028 	add a,r7 
      001927 FF               [12] 3029 	mov r7,a 
      001928 E5 F0            [12] 3030 	mov a,b 
      00192A 3C               [12] 3031 	addc a,r4 
      00192B FC               [12] 3032 	mov r4,a 
      00192C E4               [12] 3033 	clr a 
      00192D 3D               [12] 3034 	addc a,r5 
      00192E FD               [12] 3035 	mov r5,a 
                           000000  3036 	.else 
                                   3037 	mov r7,a 
                                   3038 	mov r4,b 
                                   3039 	.endif 
                                   3040 	.endif 
                           000001  3041 	.if ((0x597a7e)&0xff) 
      00192F 74 7E            [12] 3042 	mov a,# ((0x597a7e)&0xff) 
      001931 8A F0            [24] 3043 	mov b,r2 
      001933 A4               [48] 3044 	mul ab 
                           000001  3045 	.if (((0x597a7e)>>8)&0xffff) 
      001934 2F               [12] 3046 	add a,r7 
      001935 FF               [12] 3047 	mov r7,a 
      001936 E5 F0            [12] 3048 	mov a,b 
      001938 3C               [12] 3049 	addc a,r4 
      001939 FC               [12] 3050 	mov r4,a 
      00193A E4               [12] 3051 	clr a 
      00193B 3D               [12] 3052 	addc a,r5 
      00193C FD               [12] 3053 	mov r5,a 
                           000000  3054 	.else 
                                   3055 	mov r7,a 
                                   3056 	mov r4,b 
                                   3057 	.endif 
                                   3058 	.endif 
                                   3059 ;; clear precision extension 
      00193D E4               [12] 3060 	clr a 
      00193E FF               [12] 3061 	mov r7,a 
                                   3062 ;; stage 0 
                           000000  3063 	.if (((0x597a7e)>>24)&0xff) 
                                   3064 	mov a,# (((0x597a7e)>>24)&0xff) 
                                   3065 	mov b,r0 
                                   3066 	mul ab 
                                   3067 	add a,r4 
                                   3068 	mov r4,a 
                                   3069 	mov a,b 
                                   3070 	addc a,r5 
                                   3071 	mov r5,a 
                                   3072 	clr a 
                                   3073 	addc a,r6 
                                   3074 	mov r6,a 
                                   3075 	.endif 
                           000001  3076 	.if (((0x597a7e)>>16)&0xff) 
      00193F 74 59            [12] 3077 	mov a,# (((0x597a7e)>>16)&0xff) 
      001941 89 F0            [24] 3078 	mov b,r1 
      001943 A4               [48] 3079 	mul ab 
      001944 2C               [12] 3080 	add a,r4 
      001945 FC               [12] 3081 	mov r4,a 
      001946 E5 F0            [12] 3082 	mov a,b 
      001948 3D               [12] 3083 	addc a,r5 
      001949 FD               [12] 3084 	mov r5,a 
      00194A E4               [12] 3085 	clr a 
      00194B 3E               [12] 3086 	addc a,r6 
      00194C FE               [12] 3087 	mov r6,a 
                                   3088 	.endif 
                           000001  3089 	.if (((0x597a7e)>>8)&0xff) 
      00194D 74 7A            [12] 3090 	mov a,# (((0x597a7e)>>8)&0xff) 
      00194F 8A F0            [24] 3091 	mov b,r2 
      001951 A4               [48] 3092 	mul ab 
      001952 2C               [12] 3093 	add a,r4 
      001953 FC               [12] 3094 	mov r4,a 
      001954 E5 F0            [12] 3095 	mov a,b 
      001956 3D               [12] 3096 	addc a,r5 
      001957 FD               [12] 3097 	mov r5,a 
      001958 E4               [12] 3098 	clr a 
      001959 3E               [12] 3099 	addc a,r6 
      00195A FE               [12] 3100 	mov r6,a 
                                   3101 	.endif 
                           000001  3102 	.if ((0x597a7e)&0xff) 
      00195B 74 7E            [12] 3103 	mov a,# ((0x597a7e)&0xff) 
      00195D 8B F0            [24] 3104 	mov b,r3 
      00195F A4               [48] 3105 	mul ab 
      001960 2C               [12] 3106 	add a,r4 
      001961 FC               [12] 3107 	mov r4,a 
      001962 E5 F0            [12] 3108 	mov a,b 
      001964 3D               [12] 3109 	addc a,r5 
      001965 FD               [12] 3110 	mov r5,a 
      001966 E4               [12] 3111 	clr a 
      001967 3E               [12] 3112 	addc a,r6 
      001968 FE               [12] 3113 	mov r6,a 
                                   3114 	.endif 
                                   3115 ;; stage 1 
                           000000  3116 	.if (((0x597a7e)>>24)&0xff) 
                                   3117 	mov a,# (((0x597a7e)>>24)&0xff) 
                                   3118 	mov b,r1 
                                   3119 	mul ab 
                                   3120 	add a,r5 
                                   3121 	mov r5,a 
                                   3122 	mov a,b 
                                   3123 	addc a,r6 
                                   3124 	mov r6,a 
                                   3125 	clr a 
                                   3126 	addc a,r7 
                                   3127 	mov r7,a 
                                   3128 	.endif 
                           000001  3129 	.if (((0x597a7e)>>16)&0xff) 
      001969 74 59            [12] 3130 	mov a,# (((0x597a7e)>>16)&0xff) 
      00196B 8A F0            [24] 3131 	mov b,r2 
      00196D A4               [48] 3132 	mul ab 
      00196E 2D               [12] 3133 	add a,r5 
      00196F FD               [12] 3134 	mov r5,a 
      001970 E5 F0            [12] 3135 	mov a,b 
      001972 3E               [12] 3136 	addc a,r6 
      001973 FE               [12] 3137 	mov r6,a 
      001974 E4               [12] 3138 	clr a 
      001975 3F               [12] 3139 	addc a,r7 
      001976 FF               [12] 3140 	mov r7,a 
                                   3141 	.endif 
                           000001  3142 	.if (((0x597a7e)>>8)&0xff) 
      001977 74 7A            [12] 3143 	mov a,# (((0x597a7e)>>8)&0xff) 
      001979 8B F0            [24] 3144 	mov b,r3 
      00197B A4               [48] 3145 	mul ab 
      00197C 2D               [12] 3146 	add a,r5 
      00197D FD               [12] 3147 	mov r5,a 
      00197E E5 F0            [12] 3148 	mov a,b 
      001980 3E               [12] 3149 	addc a,r6 
      001981 FE               [12] 3150 	mov r6,a 
      001982 E4               [12] 3151 	clr a 
      001983 3F               [12] 3152 	addc a,r7 
      001984 FF               [12] 3153 	mov r7,a 
                                   3154 	.endif 
                                   3155 ;; stage 2 
                           000000  3156 	.if (((0x597a7e)>>24)&0xff) 
                                   3157 	mov a,# (((0x597a7e)>>24)&0xff) 
                                   3158 	mov b,r2 
                                   3159 	mul ab 
                                   3160 	add a,r6 
                                   3161 	mov r6,a 
                                   3162 	mov a,b 
                                   3163 	addc a,r7 
                                   3164 	mov r7,a 
                                   3165 	.endif 
                           000001  3166 	.if (((0x597a7e)>>16)&0xff) 
      001985 74 59            [12] 3167 	mov a,# (((0x597a7e)>>16)&0xff) 
      001987 8B F0            [24] 3168 	mov b,r3 
      001989 A4               [48] 3169 	mul ab 
      00198A 2E               [12] 3170 	add a,r6 
      00198B FE               [12] 3171 	mov r6,a 
      00198C E5 F0            [12] 3172 	mov a,b 
      00198E 3F               [12] 3173 	addc a,r7 
      00198F FF               [12] 3174 	mov r7,a 
                                   3175 	.endif 
                                   3176 ;; stage 3 
                           000000  3177 	.if (((0x597a7e)>>24)&0xff) 
                                   3178 	mov a,# (((0x597a7e)>>24)&0xff) 
                                   3179 	mov b,r3 
                                   3180 	mul ab 
                                   3181 	add a,r7 
                                   3182 	mov r7,a 
                                   3183 	.endif 
      001990 D0 E0            [24] 3184 	pop acc 
      001992 30 E7 11         [24] 3185 	jnb acc.7,00001$ 
      001995 C3               [12] 3186 	clr c 
      001996 E4               [12] 3187 	clr a 
      001997 9C               [12] 3188 	subb a,r4 
      001998 F5 82            [12] 3189 	mov dpl,a 
      00199A E4               [12] 3190 	clr a 
      00199B 9D               [12] 3191 	subb a,r5 
      00199C F5 83            [12] 3192 	mov dph,a 
      00199E E4               [12] 3193 	clr a 
      00199F 9E               [12] 3194 	subb a,r6 
      0019A0 F5 F0            [12] 3195 	mov b,a 
      0019A2 E4               [12] 3196 	clr a 
      0019A3 9F               [12] 3197 	subb a,r7 
      0019A4 80 07            [24] 3198 	sjmp 00002$ 
      0019A6                       3199 	 00001$:
      0019A6 8C 82            [24] 3200 	mov dpl,r4 
      0019A8 8D 83            [24] 3201 	mov dph,r5 
      0019AA 8E F0            [24] 3202 	mov b,r6 
      0019AC EF               [12] 3203 	mov a,r7 
      0019AD                       3204 	 00002$:
                                   3205 ;	..\src\AX_Radio_Lab_output\config.c:611: }
      0019AD 22               [24] 3206 	ret
                                   3207 ;------------------------------------------------------------
                                   3208 ;Allocation info for local variables in function 'axradio_conv_freq_tohz'
                                   3209 ;------------------------------------------------------------
                                   3210 ;f                         Allocated with name '_axradio_conv_freq_tohz_f_65536_134'
                                   3211 ;------------------------------------------------------------
                                   3212 ;	..\src\AX_Radio_Lab_output\config.c:616: int32_t axradio_conv_freq_tohz(int32_t f)
                                   3213 ;	-----------------------------------------
                                   3214 ;	 function axradio_conv_freq_tohz
                                   3215 ;	-----------------------------------------
      0019AE                       3216 _axradio_conv_freq_tohz:
                                   3217 ;	..\src\AX_Radio_Lab_output\config.c:622: CONSTMULFIX24(0x2dc6c00);
      0019AE A8 82            [24] 3218 	mov r0,dpl 
      0019B0 A9 83            [24] 3219 	mov r1,dph 
      0019B2 AA F0            [24] 3220 	mov r2,b 
      0019B4 FB               [12] 3221 	mov r3,a 
      0019B5 C0 E0            [24] 3222 	push acc 
      0019B7 30 E7 0D         [24] 3223 	jnb acc.7,00000$ 
      0019BA C3               [12] 3224 	clr c 
      0019BB E4               [12] 3225 	clr a 
      0019BC 98               [12] 3226 	subb a,r0 
      0019BD F8               [12] 3227 	mov r0,a 
      0019BE E4               [12] 3228 	clr a 
      0019BF 99               [12] 3229 	subb a,r1 
      0019C0 F9               [12] 3230 	mov r1,a 
      0019C1 E4               [12] 3231 	clr a 
      0019C2 9A               [12] 3232 	subb a,r2 
      0019C3 FA               [12] 3233 	mov r2,a 
      0019C4 E4               [12] 3234 	clr a 
      0019C5 9B               [12] 3235 	subb a,r3 
      0019C6 FB               [12] 3236 	mov r3,a 
      0019C7                       3237 	 00000$:
      0019C7 E4               [12] 3238 	clr a 
      0019C8 FC               [12] 3239 	mov r4,a 
      0019C9 FD               [12] 3240 	mov r5,a 
      0019CA FE               [12] 3241 	mov r6,a 
      0019CB FF               [12] 3242 	mov r7,a 
                                   3243 ;; stage -1 
                           000001  3244 	.if (((0x2dc6c00)>>16)&0xff) 
      0019CC 74 DC            [12] 3245 	mov a,# (((0x2dc6c00)>>16)&0xff) 
      0019CE 88 F0            [24] 3246 	mov b,r0 
      0019D0 A4               [48] 3247 	mul ab 
      0019D1 FF               [12] 3248 	mov r7,a 
      0019D2 AC F0            [24] 3249 	mov r4,b 
                                   3250 	.endif 
                           000001  3251 	.if (((0x2dc6c00)>>8)&0xff) 
      0019D4 74 6C            [12] 3252 	mov a,# (((0x2dc6c00)>>8)&0xff) 
      0019D6 89 F0            [24] 3253 	mov b,r1 
      0019D8 A4               [48] 3254 	mul ab 
                           000001  3255 	.if (((0x2dc6c00)>>16)&0xff) 
      0019D9 2F               [12] 3256 	add a,r7 
      0019DA FF               [12] 3257 	mov r7,a 
      0019DB E5 F0            [12] 3258 	mov a,b 
      0019DD 3C               [12] 3259 	addc a,r4 
      0019DE FC               [12] 3260 	mov r4,a 
      0019DF E4               [12] 3261 	clr a 
      0019E0 3D               [12] 3262 	addc a,r5 
      0019E1 FD               [12] 3263 	mov r5,a 
                           000000  3264 	.else 
                                   3265 	mov r7,a 
                                   3266 	mov r4,b 
                                   3267 	.endif 
                                   3268 	.endif 
                           000000  3269 	.if ((0x2dc6c00)&0xff) 
                                   3270 	mov a,# ((0x2dc6c00)&0xff) 
                                   3271 	mov b,r2 
                                   3272 	mul ab 
                                   3273 	.if (((0x2dc6c00)>>8)&0xffff) 
                                   3274 	add a,r7 
                                   3275 	mov r7,a 
                                   3276 	mov a,b 
                                   3277 	addc a,r4 
                                   3278 	mov r4,a 
                                   3279 	clr a 
                                   3280 	addc a,r5 
                                   3281 	mov r5,a 
                                   3282 	.else 
                                   3283 	mov r7,a 
                                   3284 	mov r4,b 
                                   3285 	.endif 
                                   3286 	.endif 
                                   3287 ;; clear precision extension 
      0019E2 E4               [12] 3288 	clr a 
      0019E3 FF               [12] 3289 	mov r7,a 
                                   3290 ;; stage 0 
                           000001  3291 	.if (((0x2dc6c00)>>24)&0xff) 
      0019E4 74 02            [12] 3292 	mov a,# (((0x2dc6c00)>>24)&0xff) 
      0019E6 88 F0            [24] 3293 	mov b,r0 
      0019E8 A4               [48] 3294 	mul ab 
      0019E9 2C               [12] 3295 	add a,r4 
      0019EA FC               [12] 3296 	mov r4,a 
      0019EB E5 F0            [12] 3297 	mov a,b 
      0019ED 3D               [12] 3298 	addc a,r5 
      0019EE FD               [12] 3299 	mov r5,a 
      0019EF E4               [12] 3300 	clr a 
      0019F0 3E               [12] 3301 	addc a,r6 
      0019F1 FE               [12] 3302 	mov r6,a 
                                   3303 	.endif 
                           000001  3304 	.if (((0x2dc6c00)>>16)&0xff) 
      0019F2 74 DC            [12] 3305 	mov a,# (((0x2dc6c00)>>16)&0xff) 
      0019F4 89 F0            [24] 3306 	mov b,r1 
      0019F6 A4               [48] 3307 	mul ab 
      0019F7 2C               [12] 3308 	add a,r4 
      0019F8 FC               [12] 3309 	mov r4,a 
      0019F9 E5 F0            [12] 3310 	mov a,b 
      0019FB 3D               [12] 3311 	addc a,r5 
      0019FC FD               [12] 3312 	mov r5,a 
      0019FD E4               [12] 3313 	clr a 
      0019FE 3E               [12] 3314 	addc a,r6 
      0019FF FE               [12] 3315 	mov r6,a 
                                   3316 	.endif 
                           000001  3317 	.if (((0x2dc6c00)>>8)&0xff) 
      001A00 74 6C            [12] 3318 	mov a,# (((0x2dc6c00)>>8)&0xff) 
      001A02 8A F0            [24] 3319 	mov b,r2 
      001A04 A4               [48] 3320 	mul ab 
      001A05 2C               [12] 3321 	add a,r4 
      001A06 FC               [12] 3322 	mov r4,a 
      001A07 E5 F0            [12] 3323 	mov a,b 
      001A09 3D               [12] 3324 	addc a,r5 
      001A0A FD               [12] 3325 	mov r5,a 
      001A0B E4               [12] 3326 	clr a 
      001A0C 3E               [12] 3327 	addc a,r6 
      001A0D FE               [12] 3328 	mov r6,a 
                                   3329 	.endif 
                           000000  3330 	.if ((0x2dc6c00)&0xff) 
                                   3331 	mov a,# ((0x2dc6c00)&0xff) 
                                   3332 	mov b,r3 
                                   3333 	mul ab 
                                   3334 	add a,r4 
                                   3335 	mov r4,a 
                                   3336 	mov a,b 
                                   3337 	addc a,r5 
                                   3338 	mov r5,a 
                                   3339 	clr a 
                                   3340 	addc a,r6 
                                   3341 	mov r6,a 
                                   3342 	.endif 
                                   3343 ;; stage 1 
                           000001  3344 	.if (((0x2dc6c00)>>24)&0xff) 
      001A0E 74 02            [12] 3345 	mov a,# (((0x2dc6c00)>>24)&0xff) 
      001A10 89 F0            [24] 3346 	mov b,r1 
      001A12 A4               [48] 3347 	mul ab 
      001A13 2D               [12] 3348 	add a,r5 
      001A14 FD               [12] 3349 	mov r5,a 
      001A15 E5 F0            [12] 3350 	mov a,b 
      001A17 3E               [12] 3351 	addc a,r6 
      001A18 FE               [12] 3352 	mov r6,a 
      001A19 E4               [12] 3353 	clr a 
      001A1A 3F               [12] 3354 	addc a,r7 
      001A1B FF               [12] 3355 	mov r7,a 
                                   3356 	.endif 
                           000001  3357 	.if (((0x2dc6c00)>>16)&0xff) 
      001A1C 74 DC            [12] 3358 	mov a,# (((0x2dc6c00)>>16)&0xff) 
      001A1E 8A F0            [24] 3359 	mov b,r2 
      001A20 A4               [48] 3360 	mul ab 
      001A21 2D               [12] 3361 	add a,r5 
      001A22 FD               [12] 3362 	mov r5,a 
      001A23 E5 F0            [12] 3363 	mov a,b 
      001A25 3E               [12] 3364 	addc a,r6 
      001A26 FE               [12] 3365 	mov r6,a 
      001A27 E4               [12] 3366 	clr a 
      001A28 3F               [12] 3367 	addc a,r7 
      001A29 FF               [12] 3368 	mov r7,a 
                                   3369 	.endif 
                           000001  3370 	.if (((0x2dc6c00)>>8)&0xff) 
      001A2A 74 6C            [12] 3371 	mov a,# (((0x2dc6c00)>>8)&0xff) 
      001A2C 8B F0            [24] 3372 	mov b,r3 
      001A2E A4               [48] 3373 	mul ab 
      001A2F 2D               [12] 3374 	add a,r5 
      001A30 FD               [12] 3375 	mov r5,a 
      001A31 E5 F0            [12] 3376 	mov a,b 
      001A33 3E               [12] 3377 	addc a,r6 
      001A34 FE               [12] 3378 	mov r6,a 
      001A35 E4               [12] 3379 	clr a 
      001A36 3F               [12] 3380 	addc a,r7 
      001A37 FF               [12] 3381 	mov r7,a 
                                   3382 	.endif 
                                   3383 ;; stage 2 
                           000001  3384 	.if (((0x2dc6c00)>>24)&0xff) 
      001A38 74 02            [12] 3385 	mov a,# (((0x2dc6c00)>>24)&0xff) 
      001A3A 8A F0            [24] 3386 	mov b,r2 
      001A3C A4               [48] 3387 	mul ab 
      001A3D 2E               [12] 3388 	add a,r6 
      001A3E FE               [12] 3389 	mov r6,a 
      001A3F E5 F0            [12] 3390 	mov a,b 
      001A41 3F               [12] 3391 	addc a,r7 
      001A42 FF               [12] 3392 	mov r7,a 
                                   3393 	.endif 
                           000001  3394 	.if (((0x2dc6c00)>>16)&0xff) 
      001A43 74 DC            [12] 3395 	mov a,# (((0x2dc6c00)>>16)&0xff) 
      001A45 8B F0            [24] 3396 	mov b,r3 
      001A47 A4               [48] 3397 	mul ab 
      001A48 2E               [12] 3398 	add a,r6 
      001A49 FE               [12] 3399 	mov r6,a 
      001A4A E5 F0            [12] 3400 	mov a,b 
      001A4C 3F               [12] 3401 	addc a,r7 
      001A4D FF               [12] 3402 	mov r7,a 
                                   3403 	.endif 
                                   3404 ;; stage 3 
                           000001  3405 	.if (((0x2dc6c00)>>24)&0xff) 
      001A4E 74 02            [12] 3406 	mov a,# (((0x2dc6c00)>>24)&0xff) 
      001A50 8B F0            [24] 3407 	mov b,r3 
      001A52 A4               [48] 3408 	mul ab 
      001A53 2F               [12] 3409 	add a,r7 
      001A54 FF               [12] 3410 	mov r7,a 
                                   3411 	.endif 
      001A55 D0 E0            [24] 3412 	pop acc 
      001A57 30 E7 11         [24] 3413 	jnb acc.7,00001$ 
      001A5A C3               [12] 3414 	clr c 
      001A5B E4               [12] 3415 	clr a 
      001A5C 9C               [12] 3416 	subb a,r4 
      001A5D F5 82            [12] 3417 	mov dpl,a 
      001A5F E4               [12] 3418 	clr a 
      001A60 9D               [12] 3419 	subb a,r5 
      001A61 F5 83            [12] 3420 	mov dph,a 
      001A63 E4               [12] 3421 	clr a 
      001A64 9E               [12] 3422 	subb a,r6 
      001A65 F5 F0            [12] 3423 	mov b,a 
      001A67 E4               [12] 3424 	clr a 
      001A68 9F               [12] 3425 	subb a,r7 
      001A69 80 07            [24] 3426 	sjmp 00002$ 
      001A6B                       3427 	 00001$:
      001A6B 8C 82            [24] 3428 	mov dpl,r4 
      001A6D 8D 83            [24] 3429 	mov dph,r5 
      001A6F 8E F0            [24] 3430 	mov b,r6 
      001A71 EF               [12] 3431 	mov a,r7 
      001A72                       3432 	 00002$:
                                   3433 ;	..\src\AX_Radio_Lab_output\config.c:623: }
      001A72 22               [24] 3434 	ret
                                   3435 ;------------------------------------------------------------
                                   3436 ;Allocation info for local variables in function 'axradio_conv_freq_fromreg'
                                   3437 ;------------------------------------------------------------
                                   3438 ;f                         Allocated with name '_axradio_conv_freq_fromreg_f_65536_136'
                                   3439 ;------------------------------------------------------------
                                   3440 ;	..\src\AX_Radio_Lab_output\config.c:630: int32_t axradio_conv_freq_fromreg(int32_t f)
                                   3441 ;	-----------------------------------------
                                   3442 ;	 function axradio_conv_freq_fromreg
                                   3443 ;	-----------------------------------------
      001A73                       3444 _axradio_conv_freq_fromreg:
                                   3445 ;	..\src\AX_Radio_Lab_output\config.c:636: CONSTMULFIX16(0x1000000);
      001A73 A8 82            [24] 3446 	mov r0,dpl 
      001A75 E5 83            [12] 3447 	mov a,dph 
      001A77 F9               [12] 3448 	mov r1,a 
      001A78 C0 E0            [24] 3449 	push acc 
      001A7A 30 E7 07         [24] 3450 	jnb acc.7,00000$ 
      001A7D C3               [12] 3451 	clr c 
      001A7E E4               [12] 3452 	clr a 
      001A7F 98               [12] 3453 	subb a,r0 
      001A80 F8               [12] 3454 	mov r0,a 
      001A81 E4               [12] 3455 	clr a 
      001A82 99               [12] 3456 	subb a,r1 
      001A83 F9               [12] 3457 	mov r1,a 
      001A84                       3458 	 00000$:
      001A84 E4               [12] 3459 	clr a 
      001A85 FC               [12] 3460 	mov r4,a 
      001A86 FD               [12] 3461 	mov r5,a 
      001A87 FE               [12] 3462 	mov r6,a 
      001A88 FF               [12] 3463 	mov r7,a 
                                   3464 ;; stage -1 
                           000000  3465 	.if (((0x1000000)>>16)&0xff) 
                                   3466 	mov a,# (((0x1000000)>>16)&0xff) 
                                   3467 	mov b,r0 
                                   3468 	mul ab 
                                   3469 	mov r7,a 
                                   3470 	mov r4,b 
                                   3471 	.endif 
                           000000  3472 	.if (((0x1000000)>>8)&0xff) 
                                   3473 	mov a,# (((0x1000000)>>8)&0xff) 
                                   3474 	mov b,r1 
                                   3475 	mul ab 
                                   3476 	.if (((0x1000000)>>16)&0xff) 
                                   3477 	add a,r7 
                                   3478 	mov r7,a 
                                   3479 	mov a,b 
                                   3480 	addc a,r4 
                                   3481 	mov r4,a 
                                   3482 	clr a 
                                   3483 	addc a,r5 
                                   3484 	mov r5,a 
                                   3485 	.else 
                                   3486 	mov r7,a 
                                   3487 	mov r4,b 
                                   3488 	.endif 
                                   3489 	.endif 
                                   3490 ;; clear precision extension 
      001A89 E4               [12] 3491 	clr a 
      001A8A FF               [12] 3492 	mov r7,a 
                                   3493 ;; stage 0 
                           000001  3494 	.if (((0x1000000)>>24)&0xff) 
      001A8B 74 01            [12] 3495 	mov a,# (((0x1000000)>>24)&0xff) 
      001A8D 88 F0            [24] 3496 	mov b,r0 
      001A8F A4               [48] 3497 	mul ab 
      001A90 2C               [12] 3498 	add a,r4 
      001A91 FC               [12] 3499 	mov r4,a 
      001A92 E5 F0            [12] 3500 	mov a,b 
      001A94 3D               [12] 3501 	addc a,r5 
      001A95 FD               [12] 3502 	mov r5,a 
      001A96 E4               [12] 3503 	clr a 
      001A97 3E               [12] 3504 	addc a,r6 
      001A98 FE               [12] 3505 	mov r6,a 
                                   3506 	.endif 
                           000000  3507 	.if (((0x1000000)>>16)&0xff) 
                                   3508 	mov a,# (((0x1000000)>>16)&0xff) 
                                   3509 	mov b,r1 
                                   3510 	mul ab 
                                   3511 	add a,r4 
                                   3512 	mov r4,a 
                                   3513 	mov a,b 
                                   3514 	addc a,r5 
                                   3515 	mov r5,a 
                                   3516 	clr a 
                                   3517 	addc a,r6 
                                   3518 	mov r6,a 
                                   3519 	.endif 
                                   3520 ;; stage 1 
                           000001  3521 	.if (((0x1000000)>>24)&0xff) 
      001A99 74 01            [12] 3522 	mov a,# (((0x1000000)>>24)&0xff) 
      001A9B 89 F0            [24] 3523 	mov b,r1 
      001A9D A4               [48] 3524 	mul ab 
      001A9E 2D               [12] 3525 	add a,r5 
      001A9F FD               [12] 3526 	mov r5,a 
      001AA0 E5 F0            [12] 3527 	mov a,b 
      001AA2 3E               [12] 3528 	addc a,r6 
      001AA3 FE               [12] 3529 	mov r6,a 
      001AA4 E4               [12] 3530 	clr a 
      001AA5 3F               [12] 3531 	addc a,r7 
      001AA6 FF               [12] 3532 	mov r7,a 
                                   3533 	.endif 
      001AA7 D0 E0            [24] 3534 	pop acc 
      001AA9 30 E7 11         [24] 3535 	jnb acc.7,00001$ 
      001AAC C3               [12] 3536 	clr c 
      001AAD E4               [12] 3537 	clr a 
      001AAE 9C               [12] 3538 	subb a,r4 
      001AAF F5 82            [12] 3539 	mov dpl,a 
      001AB1 E4               [12] 3540 	clr a 
      001AB2 9D               [12] 3541 	subb a,r5 
      001AB3 F5 83            [12] 3542 	mov dph,a 
      001AB5 E4               [12] 3543 	clr a 
      001AB6 9E               [12] 3544 	subb a,r6 
      001AB7 F5 F0            [12] 3545 	mov b,a 
      001AB9 E4               [12] 3546 	clr a 
      001ABA 9F               [12] 3547 	subb a,r7 
      001ABB 80 07            [24] 3548 	sjmp 00002$ 
      001ABD                       3549 	 00001$:
      001ABD 8C 82            [24] 3550 	mov dpl,r4 
      001ABF 8D 83            [24] 3551 	mov dph,r5 
      001AC1 8E F0            [24] 3552 	mov b,r6 
      001AC3 EF               [12] 3553 	mov a,r7 
      001AC4                       3554 	 00002$:
                                   3555 ;	..\src\AX_Radio_Lab_output\config.c:637: }
      001AC4 22               [24] 3556 	ret
                                   3557 ;------------------------------------------------------------
                                   3558 ;Allocation info for local variables in function 'axradio_conv_timeinterval_totimer0'
                                   3559 ;------------------------------------------------------------
                                   3560 ;dt                        Allocated with name '_axradio_conv_timeinterval_totimer0_dt_65536_138'
                                   3561 ;r                         Allocated with name '_axradio_conv_timeinterval_totimer0_r_65536_139'
                                   3562 ;------------------------------------------------------------
                                   3563 ;	..\src\AX_Radio_Lab_output\config.c:642: int32_t axradio_conv_timeinterval_totimer0(int32_t dt)
                                   3564 ;	-----------------------------------------
                                   3565 ;	 function axradio_conv_timeinterval_totimer0
                                   3566 ;	-----------------------------------------
      001AC5                       3567 _axradio_conv_timeinterval_totimer0:
      001AC5 AF 82            [24] 3568 	mov	r7,dpl
      001AC7 AE 83            [24] 3569 	mov	r6,dph
      001AC9 AD F0            [24] 3570 	mov	r5,b
      001ACB FC               [12] 3571 	mov	r4,a
      001ACC 90 00 DE         [24] 3572 	mov	dptr,#_axradio_conv_timeinterval_totimer0_dt_65536_138
      001ACF EF               [12] 3573 	mov	a,r7
      001AD0 F0               [24] 3574 	movx	@dptr,a
      001AD1 EE               [12] 3575 	mov	a,r6
      001AD2 A3               [24] 3576 	inc	dptr
      001AD3 F0               [24] 3577 	movx	@dptr,a
      001AD4 ED               [12] 3578 	mov	a,r5
      001AD5 A3               [24] 3579 	inc	dptr
      001AD6 F0               [24] 3580 	movx	@dptr,a
      001AD7 EC               [12] 3581 	mov	a,r4
      001AD8 A3               [24] 3582 	inc	dptr
      001AD9 F0               [24] 3583 	movx	@dptr,a
                                   3584 ;	..\src\AX_Radio_Lab_output\config.c:649: dt >>= 6;
      001ADA 90 00 DE         [24] 3585 	mov	dptr,#_axradio_conv_timeinterval_totimer0_dt_65536_138
      001ADD E0               [24] 3586 	movx	a,@dptr
      001ADE FC               [12] 3587 	mov	r4,a
      001ADF A3               [24] 3588 	inc	dptr
      001AE0 E0               [24] 3589 	movx	a,@dptr
      001AE1 FD               [12] 3590 	mov	r5,a
      001AE2 A3               [24] 3591 	inc	dptr
      001AE3 E0               [24] 3592 	movx	a,@dptr
      001AE4 FE               [12] 3593 	mov	r6,a
      001AE5 A3               [24] 3594 	inc	dptr
      001AE6 E0               [24] 3595 	movx	a,@dptr
      001AE7 FF               [12] 3596 	mov	r7,a
      001AE8 ED               [12] 3597 	mov	a,r5
      001AE9 A2 E7            [12] 3598 	mov	c,acc.7
      001AEB CC               [12] 3599 	xch	a,r4
      001AEC 33               [12] 3600 	rlc	a
      001AED CC               [12] 3601 	xch	a,r4
      001AEE 33               [12] 3602 	rlc	a
      001AEF A2 E7            [12] 3603 	mov	c,acc.7
      001AF1 CC               [12] 3604 	xch	a,r4
      001AF2 33               [12] 3605 	rlc	a
      001AF3 CC               [12] 3606 	xch	a,r4
      001AF4 33               [12] 3607 	rlc	a
      001AF5 CC               [12] 3608 	xch	a,r4
      001AF6 54 03            [12] 3609 	anl	a,#0x03
      001AF8 FD               [12] 3610 	mov	r5,a
      001AF9 EE               [12] 3611 	mov	a,r6
      001AFA 2E               [12] 3612 	add	a,r6
      001AFB 25 E0            [12] 3613 	add	a,acc
      001AFD 4D               [12] 3614 	orl	a,r5
      001AFE FD               [12] 3615 	mov	r5,a
      001AFF EF               [12] 3616 	mov	a,r7
      001B00 A2 E7            [12] 3617 	mov	c,acc.7
      001B02 CE               [12] 3618 	xch	a,r6
      001B03 33               [12] 3619 	rlc	a
      001B04 CE               [12] 3620 	xch	a,r6
      001B05 33               [12] 3621 	rlc	a
      001B06 A2 E7            [12] 3622 	mov	c,acc.7
      001B08 CE               [12] 3623 	xch	a,r6
      001B09 33               [12] 3624 	rlc	a
      001B0A CE               [12] 3625 	xch	a,r6
      001B0B 33               [12] 3626 	rlc	a
      001B0C CE               [12] 3627 	xch	a,r6
      001B0D 54 03            [12] 3628 	anl	a,#0x03
      001B0F 30 E1 02         [24] 3629 	jnb	acc.1,00103$
      001B12 44 FC            [12] 3630 	orl	a,#0xfc
      001B14                       3631 00103$:
      001B14 FF               [12] 3632 	mov	r7,a
      001B15 90 00 DE         [24] 3633 	mov	dptr,#_axradio_conv_timeinterval_totimer0_dt_65536_138
      001B18 EC               [12] 3634 	mov	a,r4
      001B19 F0               [24] 3635 	movx	@dptr,a
      001B1A ED               [12] 3636 	mov	a,r5
      001B1B A3               [24] 3637 	inc	dptr
      001B1C F0               [24] 3638 	movx	@dptr,a
      001B1D EE               [12] 3639 	mov	a,r6
      001B1E A3               [24] 3640 	inc	dptr
      001B1F F0               [24] 3641 	movx	@dptr,a
      001B20 EF               [12] 3642 	mov	a,r7
      001B21 A3               [24] 3643 	inc	dptr
      001B22 F0               [24] 3644 	movx	@dptr,a
                                   3645 ;	..\src\AX_Radio_Lab_output\config.c:650: r = dt;
      001B23 90 00 DE         [24] 3646 	mov	dptr,#_axradio_conv_timeinterval_totimer0_dt_65536_138
      001B26 E0               [24] 3647 	movx	a,@dptr
      001B27 FC               [12] 3648 	mov	r4,a
      001B28 A3               [24] 3649 	inc	dptr
      001B29 E0               [24] 3650 	movx	a,@dptr
      001B2A FD               [12] 3651 	mov	r5,a
      001B2B A3               [24] 3652 	inc	dptr
      001B2C E0               [24] 3653 	movx	a,@dptr
      001B2D FE               [12] 3654 	mov	r6,a
      001B2E A3               [24] 3655 	inc	dptr
      001B2F E0               [24] 3656 	movx	a,@dptr
      001B30 FF               [12] 3657 	mov	r7,a
      001B31 90 00 E2         [24] 3658 	mov	dptr,#_axradio_conv_timeinterval_totimer0_r_65536_139
      001B34 EC               [12] 3659 	mov	a,r4
      001B35 F0               [24] 3660 	movx	@dptr,a
      001B36 ED               [12] 3661 	mov	a,r5
      001B37 A3               [24] 3662 	inc	dptr
      001B38 F0               [24] 3663 	movx	@dptr,a
      001B39 EE               [12] 3664 	mov	a,r6
      001B3A A3               [24] 3665 	inc	dptr
      001B3B F0               [24] 3666 	movx	@dptr,a
      001B3C EF               [12] 3667 	mov	a,r7
      001B3D A3               [24] 3668 	inc	dptr
      001B3E F0               [24] 3669 	movx	@dptr,a
                                   3670 ;	..\src\AX_Radio_Lab_output\config.c:651: dt >>= 2;
      001B3F EF               [12] 3671 	mov	a,r7
      001B40 A2 E7            [12] 3672 	mov	c,acc.7
      001B42 13               [12] 3673 	rrc	a
      001B43 FF               [12] 3674 	mov	r7,a
      001B44 EE               [12] 3675 	mov	a,r6
      001B45 13               [12] 3676 	rrc	a
      001B46 FE               [12] 3677 	mov	r6,a
      001B47 ED               [12] 3678 	mov	a,r5
      001B48 13               [12] 3679 	rrc	a
      001B49 FD               [12] 3680 	mov	r5,a
      001B4A EC               [12] 3681 	mov	a,r4
      001B4B 13               [12] 3682 	rrc	a
      001B4C FC               [12] 3683 	mov	r4,a
      001B4D EF               [12] 3684 	mov	a,r7
      001B4E A2 E7            [12] 3685 	mov	c,acc.7
      001B50 13               [12] 3686 	rrc	a
      001B51 FF               [12] 3687 	mov	r7,a
      001B52 EE               [12] 3688 	mov	a,r6
      001B53 13               [12] 3689 	rrc	a
      001B54 FE               [12] 3690 	mov	r6,a
      001B55 ED               [12] 3691 	mov	a,r5
      001B56 13               [12] 3692 	rrc	a
      001B57 FD               [12] 3693 	mov	r5,a
      001B58 EC               [12] 3694 	mov	a,r4
      001B59 13               [12] 3695 	rrc	a
      001B5A 90 00 DE         [24] 3696 	mov	dptr,#_axradio_conv_timeinterval_totimer0_dt_65536_138
      001B5D F0               [24] 3697 	movx	@dptr,a
      001B5E ED               [12] 3698 	mov	a,r5
      001B5F A3               [24] 3699 	inc	dptr
      001B60 F0               [24] 3700 	movx	@dptr,a
      001B61 EE               [12] 3701 	mov	a,r6
      001B62 A3               [24] 3702 	inc	dptr
      001B63 F0               [24] 3703 	movx	@dptr,a
      001B64 EF               [12] 3704 	mov	a,r7
      001B65 A3               [24] 3705 	inc	dptr
      001B66 F0               [24] 3706 	movx	@dptr,a
                                   3707 ;	..\src\AX_Radio_Lab_output\config.c:652: r -= dt;
      001B67 90 00 DE         [24] 3708 	mov	dptr,#_axradio_conv_timeinterval_totimer0_dt_65536_138
      001B6A E0               [24] 3709 	movx	a,@dptr
      001B6B FC               [12] 3710 	mov	r4,a
      001B6C A3               [24] 3711 	inc	dptr
      001B6D E0               [24] 3712 	movx	a,@dptr
      001B6E FD               [12] 3713 	mov	r5,a
      001B6F A3               [24] 3714 	inc	dptr
      001B70 E0               [24] 3715 	movx	a,@dptr
      001B71 FE               [12] 3716 	mov	r6,a
      001B72 A3               [24] 3717 	inc	dptr
      001B73 E0               [24] 3718 	movx	a,@dptr
      001B74 FF               [12] 3719 	mov	r7,a
      001B75 90 00 E2         [24] 3720 	mov	dptr,#_axradio_conv_timeinterval_totimer0_r_65536_139
      001B78 E0               [24] 3721 	movx	a,@dptr
      001B79 F8               [12] 3722 	mov	r0,a
      001B7A A3               [24] 3723 	inc	dptr
      001B7B E0               [24] 3724 	movx	a,@dptr
      001B7C F9               [12] 3725 	mov	r1,a
      001B7D A3               [24] 3726 	inc	dptr
      001B7E E0               [24] 3727 	movx	a,@dptr
      001B7F FA               [12] 3728 	mov	r2,a
      001B80 A3               [24] 3729 	inc	dptr
      001B81 E0               [24] 3730 	movx	a,@dptr
      001B82 FB               [12] 3731 	mov	r3,a
      001B83 90 00 E2         [24] 3732 	mov	dptr,#_axradio_conv_timeinterval_totimer0_r_65536_139
      001B86 E8               [12] 3733 	mov	a,r0
      001B87 C3               [12] 3734 	clr	c
      001B88 9C               [12] 3735 	subb	a,r4
      001B89 F0               [24] 3736 	movx	@dptr,a
      001B8A E9               [12] 3737 	mov	a,r1
      001B8B 9D               [12] 3738 	subb	a,r5
      001B8C A3               [24] 3739 	inc	dptr
      001B8D F0               [24] 3740 	movx	@dptr,a
      001B8E EA               [12] 3741 	mov	a,r2
      001B8F 9E               [12] 3742 	subb	a,r6
      001B90 A3               [24] 3743 	inc	dptr
      001B91 F0               [24] 3744 	movx	@dptr,a
      001B92 EB               [12] 3745 	mov	a,r3
      001B93 9F               [12] 3746 	subb	a,r7
      001B94 A3               [24] 3747 	inc	dptr
      001B95 F0               [24] 3748 	movx	@dptr,a
                                   3749 ;	..\src\AX_Radio_Lab_output\config.c:653: dt >>= 2;
      001B96 EF               [12] 3750 	mov	a,r7
      001B97 A2 E7            [12] 3751 	mov	c,acc.7
      001B99 13               [12] 3752 	rrc	a
      001B9A FF               [12] 3753 	mov	r7,a
      001B9B EE               [12] 3754 	mov	a,r6
      001B9C 13               [12] 3755 	rrc	a
      001B9D FE               [12] 3756 	mov	r6,a
      001B9E ED               [12] 3757 	mov	a,r5
      001B9F 13               [12] 3758 	rrc	a
      001BA0 FD               [12] 3759 	mov	r5,a
      001BA1 EC               [12] 3760 	mov	a,r4
      001BA2 13               [12] 3761 	rrc	a
      001BA3 FC               [12] 3762 	mov	r4,a
      001BA4 EF               [12] 3763 	mov	a,r7
      001BA5 A2 E7            [12] 3764 	mov	c,acc.7
      001BA7 13               [12] 3765 	rrc	a
      001BA8 FF               [12] 3766 	mov	r7,a
      001BA9 EE               [12] 3767 	mov	a,r6
      001BAA 13               [12] 3768 	rrc	a
      001BAB FE               [12] 3769 	mov	r6,a
      001BAC ED               [12] 3770 	mov	a,r5
      001BAD 13               [12] 3771 	rrc	a
      001BAE FD               [12] 3772 	mov	r5,a
      001BAF EC               [12] 3773 	mov	a,r4
      001BB0 13               [12] 3774 	rrc	a
      001BB1 90 00 DE         [24] 3775 	mov	dptr,#_axradio_conv_timeinterval_totimer0_dt_65536_138
      001BB4 F0               [24] 3776 	movx	@dptr,a
      001BB5 ED               [12] 3777 	mov	a,r5
      001BB6 A3               [24] 3778 	inc	dptr
      001BB7 F0               [24] 3779 	movx	@dptr,a
      001BB8 EE               [12] 3780 	mov	a,r6
      001BB9 A3               [24] 3781 	inc	dptr
      001BBA F0               [24] 3782 	movx	@dptr,a
      001BBB EF               [12] 3783 	mov	a,r7
      001BBC A3               [24] 3784 	inc	dptr
      001BBD F0               [24] 3785 	movx	@dptr,a
                                   3786 ;	..\src\AX_Radio_Lab_output\config.c:654: r -= dt;
      001BBE 90 00 DE         [24] 3787 	mov	dptr,#_axradio_conv_timeinterval_totimer0_dt_65536_138
      001BC1 E0               [24] 3788 	movx	a,@dptr
      001BC2 FC               [12] 3789 	mov	r4,a
      001BC3 A3               [24] 3790 	inc	dptr
      001BC4 E0               [24] 3791 	movx	a,@dptr
      001BC5 FD               [12] 3792 	mov	r5,a
      001BC6 A3               [24] 3793 	inc	dptr
      001BC7 E0               [24] 3794 	movx	a,@dptr
      001BC8 FE               [12] 3795 	mov	r6,a
      001BC9 A3               [24] 3796 	inc	dptr
      001BCA E0               [24] 3797 	movx	a,@dptr
      001BCB FF               [12] 3798 	mov	r7,a
      001BCC 90 00 E2         [24] 3799 	mov	dptr,#_axradio_conv_timeinterval_totimer0_r_65536_139
      001BCF E0               [24] 3800 	movx	a,@dptr
      001BD0 F8               [12] 3801 	mov	r0,a
      001BD1 A3               [24] 3802 	inc	dptr
      001BD2 E0               [24] 3803 	movx	a,@dptr
      001BD3 F9               [12] 3804 	mov	r1,a
      001BD4 A3               [24] 3805 	inc	dptr
      001BD5 E0               [24] 3806 	movx	a,@dptr
      001BD6 FA               [12] 3807 	mov	r2,a
      001BD7 A3               [24] 3808 	inc	dptr
      001BD8 E0               [24] 3809 	movx	a,@dptr
      001BD9 FB               [12] 3810 	mov	r3,a
      001BDA 90 00 E2         [24] 3811 	mov	dptr,#_axradio_conv_timeinterval_totimer0_r_65536_139
      001BDD E8               [12] 3812 	mov	a,r0
      001BDE C3               [12] 3813 	clr	c
      001BDF 9C               [12] 3814 	subb	a,r4
      001BE0 F0               [24] 3815 	movx	@dptr,a
      001BE1 E9               [12] 3816 	mov	a,r1
      001BE2 9D               [12] 3817 	subb	a,r5
      001BE3 A3               [24] 3818 	inc	dptr
      001BE4 F0               [24] 3819 	movx	@dptr,a
      001BE5 EA               [12] 3820 	mov	a,r2
      001BE6 9E               [12] 3821 	subb	a,r6
      001BE7 A3               [24] 3822 	inc	dptr
      001BE8 F0               [24] 3823 	movx	@dptr,a
      001BE9 EB               [12] 3824 	mov	a,r3
      001BEA 9F               [12] 3825 	subb	a,r7
      001BEB A3               [24] 3826 	inc	dptr
      001BEC F0               [24] 3827 	movx	@dptr,a
                                   3828 ;	..\src\AX_Radio_Lab_output\config.c:655: dt >>= 3;
      001BED ED               [12] 3829 	mov	a,r5
      001BEE C4               [12] 3830 	swap	a
      001BEF 23               [12] 3831 	rl	a
      001BF0 CC               [12] 3832 	xch	a,r4
      001BF1 C4               [12] 3833 	swap	a
      001BF2 23               [12] 3834 	rl	a
      001BF3 54 1F            [12] 3835 	anl	a,#0x1f
      001BF5 6C               [12] 3836 	xrl	a,r4
      001BF6 CC               [12] 3837 	xch	a,r4
      001BF7 54 1F            [12] 3838 	anl	a,#0x1f
      001BF9 CC               [12] 3839 	xch	a,r4
      001BFA 6C               [12] 3840 	xrl	a,r4
      001BFB CC               [12] 3841 	xch	a,r4
      001BFC FD               [12] 3842 	mov	r5,a
      001BFD EE               [12] 3843 	mov	a,r6
      001BFE C4               [12] 3844 	swap	a
      001BFF 23               [12] 3845 	rl	a
      001C00 54 E0            [12] 3846 	anl	a,#0xe0
      001C02 4D               [12] 3847 	orl	a,r5
      001C03 FD               [12] 3848 	mov	r5,a
      001C04 EF               [12] 3849 	mov	a,r7
      001C05 C4               [12] 3850 	swap	a
      001C06 23               [12] 3851 	rl	a
      001C07 CE               [12] 3852 	xch	a,r6
      001C08 C4               [12] 3853 	swap	a
      001C09 23               [12] 3854 	rl	a
      001C0A 54 1F            [12] 3855 	anl	a,#0x1f
      001C0C 6E               [12] 3856 	xrl	a,r6
      001C0D CE               [12] 3857 	xch	a,r6
      001C0E 54 1F            [12] 3858 	anl	a,#0x1f
      001C10 CE               [12] 3859 	xch	a,r6
      001C11 6E               [12] 3860 	xrl	a,r6
      001C12 CE               [12] 3861 	xch	a,r6
      001C13 30 E4 02         [24] 3862 	jnb	acc.4,00104$
      001C16 44 E0            [12] 3863 	orl	a,#0xe0
      001C18                       3864 00104$:
      001C18 FF               [12] 3865 	mov	r7,a
      001C19 90 00 DE         [24] 3866 	mov	dptr,#_axradio_conv_timeinterval_totimer0_dt_65536_138
      001C1C EC               [12] 3867 	mov	a,r4
      001C1D F0               [24] 3868 	movx	@dptr,a
      001C1E ED               [12] 3869 	mov	a,r5
      001C1F A3               [24] 3870 	inc	dptr
      001C20 F0               [24] 3871 	movx	@dptr,a
      001C21 EE               [12] 3872 	mov	a,r6
      001C22 A3               [24] 3873 	inc	dptr
      001C23 F0               [24] 3874 	movx	@dptr,a
      001C24 EF               [12] 3875 	mov	a,r7
      001C25 A3               [24] 3876 	inc	dptr
      001C26 F0               [24] 3877 	movx	@dptr,a
                                   3878 ;	..\src\AX_Radio_Lab_output\config.c:656: r += dt;
      001C27 90 00 DE         [24] 3879 	mov	dptr,#_axradio_conv_timeinterval_totimer0_dt_65536_138
      001C2A E0               [24] 3880 	movx	a,@dptr
      001C2B FC               [12] 3881 	mov	r4,a
      001C2C A3               [24] 3882 	inc	dptr
      001C2D E0               [24] 3883 	movx	a,@dptr
      001C2E FD               [12] 3884 	mov	r5,a
      001C2F A3               [24] 3885 	inc	dptr
      001C30 E0               [24] 3886 	movx	a,@dptr
      001C31 FE               [12] 3887 	mov	r6,a
      001C32 A3               [24] 3888 	inc	dptr
      001C33 E0               [24] 3889 	movx	a,@dptr
      001C34 FF               [12] 3890 	mov	r7,a
      001C35 90 00 E2         [24] 3891 	mov	dptr,#_axradio_conv_timeinterval_totimer0_r_65536_139
      001C38 E0               [24] 3892 	movx	a,@dptr
      001C39 F8               [12] 3893 	mov	r0,a
      001C3A A3               [24] 3894 	inc	dptr
      001C3B E0               [24] 3895 	movx	a,@dptr
      001C3C F9               [12] 3896 	mov	r1,a
      001C3D A3               [24] 3897 	inc	dptr
      001C3E E0               [24] 3898 	movx	a,@dptr
      001C3F FA               [12] 3899 	mov	r2,a
      001C40 A3               [24] 3900 	inc	dptr
      001C41 E0               [24] 3901 	movx	a,@dptr
      001C42 FB               [12] 3902 	mov	r3,a
      001C43 90 00 E2         [24] 3903 	mov	dptr,#_axradio_conv_timeinterval_totimer0_r_65536_139
      001C46 EC               [12] 3904 	mov	a,r4
      001C47 28               [12] 3905 	add	a,r0
      001C48 F0               [24] 3906 	movx	@dptr,a
      001C49 ED               [12] 3907 	mov	a,r5
      001C4A 39               [12] 3908 	addc	a,r1
      001C4B A3               [24] 3909 	inc	dptr
      001C4C F0               [24] 3910 	movx	@dptr,a
      001C4D EE               [12] 3911 	mov	a,r6
      001C4E 3A               [12] 3912 	addc	a,r2
      001C4F A3               [24] 3913 	inc	dptr
      001C50 F0               [24] 3914 	movx	@dptr,a
      001C51 EF               [12] 3915 	mov	a,r7
      001C52 3B               [12] 3916 	addc	a,r3
      001C53 A3               [24] 3917 	inc	dptr
      001C54 F0               [24] 3918 	movx	@dptr,a
                                   3919 ;	..\src\AX_Radio_Lab_output\config.c:657: return r;
      001C55 90 00 E2         [24] 3920 	mov	dptr,#_axradio_conv_timeinterval_totimer0_r_65536_139
      001C58 E0               [24] 3921 	movx	a,@dptr
      001C59 FC               [12] 3922 	mov	r4,a
      001C5A A3               [24] 3923 	inc	dptr
      001C5B E0               [24] 3924 	movx	a,@dptr
      001C5C FD               [12] 3925 	mov	r5,a
      001C5D A3               [24] 3926 	inc	dptr
      001C5E E0               [24] 3927 	movx	a,@dptr
      001C5F FE               [12] 3928 	mov	r6,a
      001C60 A3               [24] 3929 	inc	dptr
      001C61 E0               [24] 3930 	movx	a,@dptr
      001C62 8C 82            [24] 3931 	mov	dpl,r4
      001C64 8D 83            [24] 3932 	mov	dph,r5
      001C66 8E F0            [24] 3933 	mov	b,r6
                                   3934 ;	..\src\AX_Radio_Lab_output\config.c:658: }
      001C68 22               [24] 3935 	ret
                                   3936 ;------------------------------------------------------------
                                   3937 ;Allocation info for local variables in function 'axradio_byteconv'
                                   3938 ;------------------------------------------------------------
                                   3939 ;b                         Allocated to registers r7 
                                   3940 ;------------------------------------------------------------
                                   3941 ;	..\src\AX_Radio_Lab_output\config.c:660: __reentrantb uint8_t axradio_byteconv(uint8_t b) __reentrant
                                   3942 ;	-----------------------------------------
                                   3943 ;	 function axradio_byteconv
                                   3944 ;	-----------------------------------------
      001C69                       3945 _axradio_byteconv:
                                   3946 ;	..\src\AX_Radio_Lab_output\config.c:662: return b;
                                   3947 ;	..\src\AX_Radio_Lab_output\config.c:663: }
      001C69 22               [24] 3948 	ret
                                   3949 ;------------------------------------------------------------
                                   3950 ;Allocation info for local variables in function 'axradio_framing_check_crc'
                                   3951 ;------------------------------------------------------------
                                   3952 ;cnt                       Allocated to stack - _bp -4
                                   3953 ;pkt                       Allocated to registers r6 r7 
                                   3954 ;------------------------------------------------------------
                                   3955 ;	..\src\AX_Radio_Lab_output\config.c:667: __reentrantb uint16_t axradio_framing_check_crc(uint8_t __xdata *pkt, uint16_t cnt) __reentrant
                                   3956 ;	-----------------------------------------
                                   3957 ;	 function axradio_framing_check_crc
                                   3958 ;	-----------------------------------------
      001C6A                       3959 _axradio_framing_check_crc:
      001C6A C0 1F            [24] 3960 	push	_bp
      001C6C 85 81 1F         [24] 3961 	mov	_bp,sp
      001C6F AE 82            [24] 3962 	mov	r6,dpl
      001C71 AF 83            [24] 3963 	mov	r7,dph
                                   3964 ;	..\src\AX_Radio_Lab_output\config.c:669: if (crc_crc32(pkt, cnt, 0xFFFFFFFF) != 0xDEBB20E3)
      001C73 7D 00            [12] 3965 	mov	r5,#0x00
      001C75 74 FF            [12] 3966 	mov	a,#0xff
      001C77 C0 E0            [24] 3967 	push	acc
      001C79 C0 E0            [24] 3968 	push	acc
      001C7B C0 E0            [24] 3969 	push	acc
      001C7D C0 E0            [24] 3970 	push	acc
      001C7F E5 1F            [12] 3971 	mov	a,_bp
      001C81 24 FC            [12] 3972 	add	a,#0xfc
      001C83 F8               [12] 3973 	mov	r0,a
      001C84 E6               [12] 3974 	mov	a,@r0
      001C85 C0 E0            [24] 3975 	push	acc
      001C87 08               [12] 3976 	inc	r0
      001C88 E6               [12] 3977 	mov	a,@r0
      001C89 C0 E0            [24] 3978 	push	acc
      001C8B 8E 82            [24] 3979 	mov	dpl,r6
      001C8D 8F 83            [24] 3980 	mov	dph,r7
      001C8F 8D F0            [24] 3981 	mov	b,r5
      001C91 12 7B 37         [24] 3982 	lcall	_crc_crc32
      001C94 AC 82            [24] 3983 	mov	r4,dpl
      001C96 AD 83            [24] 3984 	mov	r5,dph
      001C98 AE F0            [24] 3985 	mov	r6,b
      001C9A FF               [12] 3986 	mov	r7,a
      001C9B E5 81            [12] 3987 	mov	a,sp
      001C9D 24 FA            [12] 3988 	add	a,#0xfa
      001C9F F5 81            [12] 3989 	mov	sp,a
      001CA1 BC E3 0B         [24] 3990 	cjne	r4,#0xe3,00109$
      001CA4 BD 20 08         [24] 3991 	cjne	r5,#0x20,00109$
      001CA7 BE BB 05         [24] 3992 	cjne	r6,#0xbb,00109$
      001CAA BF DE 02         [24] 3993 	cjne	r7,#0xde,00109$
      001CAD 80 05            [24] 3994 	sjmp	00102$
      001CAF                       3995 00109$:
                                   3996 ;	..\src\AX_Radio_Lab_output\config.c:670: return 0;
      001CAF 90 00 00         [24] 3997 	mov	dptr,#0x0000
      001CB2 80 0A            [24] 3998 	sjmp	00103$
      001CB4                       3999 00102$:
                                   4000 ;	..\src\AX_Radio_Lab_output\config.c:671: return cnt;
      001CB4 E5 1F            [12] 4001 	mov	a,_bp
      001CB6 24 FC            [12] 4002 	add	a,#0xfc
      001CB8 F8               [12] 4003 	mov	r0,a
      001CB9 86 82            [24] 4004 	mov	dpl,@r0
      001CBB 08               [12] 4005 	inc	r0
      001CBC 86 83            [24] 4006 	mov	dph,@r0
      001CBE                       4007 00103$:
                                   4008 ;	..\src\AX_Radio_Lab_output\config.c:672: }
      001CBE D0 1F            [24] 4009 	pop	_bp
      001CC0 22               [24] 4010 	ret
                                   4011 ;------------------------------------------------------------
                                   4012 ;Allocation info for local variables in function 'axradio_framing_append_crc'
                                   4013 ;------------------------------------------------------------
                                   4014 ;cnt                       Allocated to stack - _bp -4
                                   4015 ;pkt                       Allocated to registers r6 r7 
                                   4016 ;s                         Allocated to stack - _bp +1
                                   4017 ;------------------------------------------------------------
                                   4018 ;	..\src\AX_Radio_Lab_output\config.c:674: __reentrantb uint16_t axradio_framing_append_crc(uint8_t __xdata *pkt, uint16_t cnt) __reentrant
                                   4019 ;	-----------------------------------------
                                   4020 ;	 function axradio_framing_append_crc
                                   4021 ;	-----------------------------------------
      001CC1                       4022 _axradio_framing_append_crc:
      001CC1 C0 1F            [24] 4023 	push	_bp
      001CC3 E5 81            [12] 4024 	mov	a,sp
      001CC5 F5 1F            [12] 4025 	mov	_bp,a
      001CC7 24 04            [12] 4026 	add	a,#0x04
      001CC9 F5 81            [12] 4027 	mov	sp,a
      001CCB AE 82            [24] 4028 	mov	r6,dpl
      001CCD AF 83            [24] 4029 	mov	r7,dph
                                   4030 ;	..\src\AX_Radio_Lab_output\config.c:677: s = crc_crc32(pkt, cnt, s);
      001CCF 8E 03            [24] 4031 	mov	ar3,r6
      001CD1 8F 04            [24] 4032 	mov	ar4,r7
      001CD3 7D 00            [12] 4033 	mov	r5,#0x00
      001CD5 C0 07            [24] 4034 	push	ar7
      001CD7 C0 06            [24] 4035 	push	ar6
      001CD9 74 FF            [12] 4036 	mov	a,#0xff
      001CDB C0 E0            [24] 4037 	push	acc
      001CDD C0 E0            [24] 4038 	push	acc
      001CDF C0 E0            [24] 4039 	push	acc
      001CE1 C0 E0            [24] 4040 	push	acc
      001CE3 E5 1F            [12] 4041 	mov	a,_bp
      001CE5 24 FC            [12] 4042 	add	a,#0xfc
      001CE7 F8               [12] 4043 	mov	r0,a
      001CE8 E6               [12] 4044 	mov	a,@r0
      001CE9 C0 E0            [24] 4045 	push	acc
      001CEB 08               [12] 4046 	inc	r0
      001CEC E6               [12] 4047 	mov	a,@r0
      001CED C0 E0            [24] 4048 	push	acc
      001CEF 8B 82            [24] 4049 	mov	dpl,r3
      001CF1 8C 83            [24] 4050 	mov	dph,r4
      001CF3 8D F0            [24] 4051 	mov	b,r5
      001CF5 12 7B 37         [24] 4052 	lcall	_crc_crc32
      001CF8 AA 82            [24] 4053 	mov	r2,dpl
      001CFA AB 83            [24] 4054 	mov	r3,dph
      001CFC AC F0            [24] 4055 	mov	r4,b
      001CFE FD               [12] 4056 	mov	r5,a
      001CFF E5 81            [12] 4057 	mov	a,sp
      001D01 24 FA            [12] 4058 	add	a,#0xfa
      001D03 F5 81            [12] 4059 	mov	sp,a
      001D05 D0 06            [24] 4060 	pop	ar6
      001D07 D0 07            [24] 4061 	pop	ar7
      001D09 A8 1F            [24] 4062 	mov	r0,_bp
      001D0B 08               [12] 4063 	inc	r0
      001D0C A6 02            [24] 4064 	mov	@r0,ar2
      001D0E 08               [12] 4065 	inc	r0
      001D0F A6 03            [24] 4066 	mov	@r0,ar3
      001D11 08               [12] 4067 	inc	r0
      001D12 A6 04            [24] 4068 	mov	@r0,ar4
      001D14 08               [12] 4069 	inc	r0
      001D15 A6 05            [24] 4070 	mov	@r0,ar5
                                   4071 ;	..\src\AX_Radio_Lab_output\config.c:678: pkt += cnt;
      001D17 E5 1F            [12] 4072 	mov	a,_bp
      001D19 24 FC            [12] 4073 	add	a,#0xfc
      001D1B F8               [12] 4074 	mov	r0,a
      001D1C E6               [12] 4075 	mov	a,@r0
      001D1D 2E               [12] 4076 	add	a,r6
      001D1E FE               [12] 4077 	mov	r6,a
      001D1F 08               [12] 4078 	inc	r0
      001D20 E6               [12] 4079 	mov	a,@r0
      001D21 3F               [12] 4080 	addc	a,r7
      001D22 FF               [12] 4081 	mov	r7,a
                                   4082 ;	..\src\AX_Radio_Lab_output\config.c:679: *pkt++ = ~(uint8_t)(s);
      001D23 A8 1F            [24] 4083 	mov	r0,_bp
      001D25 08               [12] 4084 	inc	r0
      001D26 E6               [12] 4085 	mov	a,@r0
      001D27 F4               [12] 4086 	cpl	a
      001D28 8E 82            [24] 4087 	mov	dpl,r6
      001D2A 8F 83            [24] 4088 	mov	dph,r7
      001D2C F0               [24] 4089 	movx	@dptr,a
      001D2D 0E               [12] 4090 	inc	r6
      001D2E BE 00 01         [24] 4091 	cjne	r6,#0x00,00103$
      001D31 0F               [12] 4092 	inc	r7
      001D32                       4093 00103$:
                                   4094 ;	..\src\AX_Radio_Lab_output\config.c:680: *pkt++ = ~(uint8_t)(s >> 8);
      001D32 A8 1F            [24] 4095 	mov	r0,_bp
      001D34 08               [12] 4096 	inc	r0
      001D35 08               [12] 4097 	inc	r0
      001D36 E6               [12] 4098 	mov	a,@r0
      001D37 F4               [12] 4099 	cpl	a
      001D38 8E 82            [24] 4100 	mov	dpl,r6
      001D3A 8F 83            [24] 4101 	mov	dph,r7
      001D3C F0               [24] 4102 	movx	@dptr,a
      001D3D 0E               [12] 4103 	inc	r6
      001D3E BE 00 01         [24] 4104 	cjne	r6,#0x00,00104$
      001D41 0F               [12] 4105 	inc	r7
      001D42                       4106 00104$:
                                   4107 ;	..\src\AX_Radio_Lab_output\config.c:681: *pkt++ = ~(uint8_t)(s >> 16);
      001D42 A8 1F            [24] 4108 	mov	r0,_bp
      001D44 08               [12] 4109 	inc	r0
      001D45 08               [12] 4110 	inc	r0
      001D46 08               [12] 4111 	inc	r0
      001D47 E6               [12] 4112 	mov	a,@r0
      001D48 F4               [12] 4113 	cpl	a
      001D49 8E 82            [24] 4114 	mov	dpl,r6
      001D4B 8F 83            [24] 4115 	mov	dph,r7
      001D4D F0               [24] 4116 	movx	@dptr,a
      001D4E 0E               [12] 4117 	inc	r6
      001D4F BE 00 01         [24] 4118 	cjne	r6,#0x00,00105$
      001D52 0F               [12] 4119 	inc	r7
      001D53                       4120 00105$:
                                   4121 ;	..\src\AX_Radio_Lab_output\config.c:682: *pkt++ = ~(uint8_t)(s >> 24);
      001D53 A8 1F            [24] 4122 	mov	r0,_bp
      001D55 08               [12] 4123 	inc	r0
      001D56 08               [12] 4124 	inc	r0
      001D57 08               [12] 4125 	inc	r0
      001D58 08               [12] 4126 	inc	r0
      001D59 E6               [12] 4127 	mov	a,@r0
      001D5A F4               [12] 4128 	cpl	a
      001D5B 8E 82            [24] 4129 	mov	dpl,r6
      001D5D 8F 83            [24] 4130 	mov	dph,r7
      001D5F F0               [24] 4131 	movx	@dptr,a
                                   4132 ;	..\src\AX_Radio_Lab_output\config.c:683: return cnt + 4;
      001D60 E5 1F            [12] 4133 	mov	a,_bp
      001D62 24 FC            [12] 4134 	add	a,#0xfc
      001D64 F8               [12] 4135 	mov	r0,a
      001D65 74 04            [12] 4136 	mov	a,#0x04
      001D67 26               [12] 4137 	add	a,@r0
      001D68 FE               [12] 4138 	mov	r6,a
      001D69 E4               [12] 4139 	clr	a
      001D6A 08               [12] 4140 	inc	r0
      001D6B 36               [12] 4141 	addc	a,@r0
      001D6C FF               [12] 4142 	mov	r7,a
      001D6D 8E 82            [24] 4143 	mov	dpl,r6
      001D6F 8F 83            [24] 4144 	mov	dph,r7
                                   4145 ;	..\src\AX_Radio_Lab_output\config.c:684: }
      001D71 85 1F 81         [24] 4146 	mov	sp,_bp
      001D74 D0 1F            [24] 4147 	pop	_bp
      001D76 22               [24] 4148 	ret
                                   4149 	.area CSEG    (CODE)
                                   4150 	.area CONST   (CODE)
      0090BF                       4151 _axradio_phy_innerfreqloop:
      0090BF 00                    4152 	.db #0x00	; 0
      0090C0                       4153 _axradio_phy_pn9:
      0090C0 00                    4154 	.db #0x00	; 0
      0090C1                       4155 _axradio_phy_nrchannels:
      0090C1 01                    4156 	.db #0x01	; 1
      0090C2                       4157 _axradio_phy_chanfreq:
      0090C2 2F 96 18 09           4158 	.byte #0x2f, #0x96, #0x18, #0x09	; 152606255
      0090C6                       4159 _axradio_phy_chanpllrnginit:
      0090C6 0A                    4160 	.db #0x0a	; 10
      0090C7                       4161 _axradio_phy_chanvcoiinit:
      0090C7 98                    4162 	.db #0x98	; 152
      0090C8                       4163 _axradio_phy_vcocalib:
      0090C8 00                    4164 	.db #0x00	; 0
      0090C9                       4165 _axradio_phy_maxfreqoffset:
      0090C9 94 03 00 00           4166 	.byte #0x94, #0x03, #0x00, #0x00	;  916
      0090CD                       4167 _axradio_phy_rssioffset:
      0090CD 40                    4168 	.db #0x40	;  64
      0090CE                       4169 _axradio_phy_rssireference:
      0090CE 37                    4170 	.db #0x37	;  55	'7'
      0090CF                       4171 _axradio_phy_channelbusy:
      0090CF E3                    4172 	.db #0xe3	; -29
      0090D0                       4173 _axradio_phy_cs_period:
      0090D0 07 00                 4174 	.byte #0x07, #0x00	; 7
      0090D2                       4175 _axradio_phy_cs_enabled:
      0090D2 00                    4176 	.db #0x00	; 0
      0090D3                       4177 _axradio_phy_lbt_retries:
      0090D3 00                    4178 	.db #0x00	; 0
      0090D4                       4179 _axradio_phy_lbt_forcetx:
      0090D4 00                    4180 	.db #0x00	; 0
      0090D5                       4181 _axradio_phy_preamble_wor_longlen:
      0090D5 09 00                 4182 	.byte #0x09, #0x00	; 9
      0090D7                       4183 _axradio_phy_preamble_wor_len:
      0090D7 08 00                 4184 	.byte #0x08, #0x00	; 8
      0090D9                       4185 _axradio_phy_preamble_longlen:
      0090D9 00 00                 4186 	.byte #0x00, #0x00	; 0
      0090DB                       4187 _axradio_phy_preamble_len:
      0090DB 08 00                 4188 	.byte #0x08, #0x00	; 8
      0090DD                       4189 _axradio_phy_preamble_byte:
      0090DD AA                    4190 	.db #0xaa	; 170
      0090DE                       4191 _axradio_phy_preamble_flags:
      0090DE 38                    4192 	.db #0x38	; 56	'8'
      0090DF                       4193 _axradio_phy_preamble_appendbits:
      0090DF 00                    4194 	.db #0x00	; 0
      0090E0                       4195 _axradio_phy_preamble_appendpattern:
      0090E0 00                    4196 	.db #0x00	; 0
      0090E1                       4197 _axradio_framing_maclen:
      0090E1 03                    4198 	.db #0x03	; 3
      0090E2                       4199 _axradio_framing_addrlen:
      0090E2 02                    4200 	.db #0x02	; 2
      0090E3                       4201 _axradio_framing_destaddrpos:
      0090E3 01                    4202 	.db #0x01	; 1
      0090E4                       4203 _axradio_framing_sourceaddrpos:
      0090E4 FF                    4204 	.db #0xff	; 255
      0090E5                       4205 _axradio_framing_lenpos:
      0090E5 00                    4206 	.db #0x00	; 0
      0090E6                       4207 _axradio_framing_lenoffs:
      0090E6 00                    4208 	.db #0x00	; 0
      0090E7                       4209 _axradio_framing_lenmask:
      0090E7 FF                    4210 	.db #0xff	; 255
      0090E8                       4211 _axradio_framing_swcrclen:
      0090E8 00                    4212 	.db #0x00	; 0
      0090E9                       4213 _axradio_framing_synclen:
      0090E9 08                    4214 	.db #0x08	; 8
      0090EA                       4215 _axradio_framing_syncword:
      0090EA 5A                    4216 	.db #0x5a	; 90	'Z'
      0090EB 55                    4217 	.db #0x55	; 85	'U'
      0090EC 33                    4218 	.db #0x33	; 51	'3'
      0090ED 55                    4219 	.db #0x55	; 85	'U'
      0090EE                       4220 _axradio_framing_syncflags:
      0090EE 38                    4221 	.db #0x38	; 56	'8'
      0090EF                       4222 _axradio_framing_enable_sfdcallback:
      0090EF 00                    4223 	.db #0x00	; 0
      0090F0                       4224 _axradio_framing_ack_timeout:
      0090F0 0C 00 00 00           4225 	.byte #0x0c, #0x00, #0x00, #0x00	; 12
      0090F4                       4226 _axradio_framing_ack_delay:
      0090F4 39 01 00 00           4227 	.byte #0x39, #0x01, #0x00, #0x00	; 313
      0090F8                       4228 _axradio_framing_ack_retransmissions:
      0090F8 00                    4229 	.db #0x00	; 0
      0090F9                       4230 _axradio_framing_ack_seqnrpos:
      0090F9 FF                    4231 	.db #0xff	; 255
      0090FA                       4232 _axradio_framing_minpayloadlen:
      0090FA 01                    4233 	.db #0x01	; 1
      0090FB                       4234 _axradio_wor_period:
      0090FB 80 00                 4235 	.byte #0x80, #0x00	; 128
      0090FD                       4236 _axradio_sync_period:
      0090FD 00 80 02 00           4237 	.byte #0x00, #0x80, #0x02, #0x00	; 163840
      009101                       4238 _axradio_sync_xoscstartup:
      009101 31 00 00 00           4239 	.byte #0x31, #0x00, #0x00, #0x00	; 49
      009105                       4240 _axradio_sync_slave_syncwindow:
      009105 00 80 07 00           4241 	.byte #0x00, #0x80, #0x07, #0x00	; 491520
      009109                       4242 _axradio_sync_slave_initialsyncwindow:
      009109 00 00 5A 00           4243 	.byte #0x00, #0x00, #0x5a, #0x00	; 5898240
      00910D                       4244 _axradio_sync_slave_syncpause:
      00910D 00 00 2C 01           4245 	.byte #0x00, #0x00, #0x2c, #0x01	; 19660800
      009111                       4246 _axradio_sync_slave_maxperiod:
      009111 72 0E                 4247 	.byte #0x72, #0x0e	;  3698
      009113                       4248 _axradio_sync_slave_resyncloss:
      009113 0B                    4249 	.db #0x0b	; 11
      009114                       4250 _axradio_sync_slave_nrrx:
      009114 03                    4251 	.db #0x03	; 3
      009115                       4252 _axradio_sync_slave_rxadvance:
      009115 30 01 00 00           4253 	.byte #0x30, #0x01, #0x00, #0x00	; 304
      009119 C7 00 00 00           4254 	.byte #0xc7, #0x00, #0x00, #0x00	; 199
      00911D 0C 01 00 00           4255 	.byte #0x0c, #0x01, #0x00, #0x00	; 268
      009121                       4256 _axradio_sync_slave_rxwindow:
      009121 7F 01 00 00           4257 	.byte #0x7f, #0x01, #0x00, #0x00	; 383
      009125 AD 00 00 00           4258 	.byte #0xad, #0x00, #0x00, #0x00	; 173
      009129 37 01 00 00           4259 	.byte #0x37, #0x01, #0x00, #0x00	; 311
      00912D                       4260 _axradio_sync_slave_rxtimeout:
      00912D 86 01 00 00           4261 	.byte #0x86, #0x01, #0x00, #0x00	; 390
                                   4262 	.area XINIT   (CODE)
                                   4263 	.area CABS    (ABS,CODE)
