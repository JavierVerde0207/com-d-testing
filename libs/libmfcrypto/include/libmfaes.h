#ifndef LIBMFAES_H
#define LIBMFAES_H

#include "libmftypes.h"

/*
 * AES Setup (generates key schedule, sets up AESCONFIG and AESKEYADDR registers
 */
extern void aes128_setup(const uint8_t key[16], uint8_t __xdata *keyschedule); /* keyschedule size 176 */
extern void aes192_setup(const uint8_t key[24], uint8_t __xdata *keyschedule); /* keyschedule size 208 */
extern void aes256_setup(const uint8_t key[32], uint8_t __xdata *keyschedule); /* keyschedule size 240 */

/*
 * AES Key Schedule Expansion
 */
extern void aes_keyexp(uint8_t __xdata *keysched, uint8_t Nk, uint8_t Nr);

#endif /* LIBMFAES_H */
