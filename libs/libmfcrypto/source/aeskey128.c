#include "ax8052crypto.h"
#include "aes.h"
#include <string.h>

void aes128_setup(const uint8_t key[16], uint8_t __xdata *keyschedule)
{
	memcpy((void *)keyschedule, key, 16);
	aes_keyexp(keyschedule, 4, 10);
	AESCONFIG = (AESCONFIG & 0xC0) | 10;
	AESKEYADDR0 = (uint16_t)keyschedule;
	AESKEYADDR1 = ((uint16_t)keyschedule) >> 8;
}
