#include "ax8052crypto.h"
#include "aes.h"
#include <string.h>

void aes256_setup(const uint8_t key[32], uint8_t __xdata *keyschedule)
{
	memcpy((void *)keyschedule, key, 32);
	aes_keyexp(keyschedule, 8, 14);
	AESCONFIG = (AESCONFIG & 0xC0) | 14;
	AESKEYADDR0 = (uint16_t)keyschedule;
	AESKEYADDR1 = ((uint16_t)keyschedule) >> 8;
}
