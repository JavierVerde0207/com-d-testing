#ifndef LIBDVK2LEDS_H
#define LIBDVK2LEDS_H

#include "libmftypes.h"

#define led0_toggle() do { PORTC ^= 0x08; } while (0)
#define led0_set(x)   do { PORTC_3 = (x); } while (0)
#define led0_on()     do { led0_set(1); } while (0)
#define led0_off()    do { led0_set(0); } while (0)

#define led1_toggle() do { PORTA ^= 0x01; } while (0)
#define led1_set(x)   do { PORTA_0 = (x); } while (0)
#define led1_on()     do { led1_set(1); } while (0)
#define led1_off()    do { led1_set(0); } while (0)

#define led2_toggle() do { PORTA ^= 0x04; } while (0)
#define led2_set(x)   do { PORTA_2 = (x); } while (0)
#define led2_on()     do { led2_set(1); } while (0)
#define led2_off()    do { led2_set(0); } while (0)

#define led3_toggle() do { PORTA ^= 0x20; } while (0)
#define led3_set(x)   do { PORTA_5 = (x); } while (0)
#define led3_on()     do { led3_set(1); } while (0)
#define led3_off()    do { led3_set(0); } while (0)

#endif /* LIBDVK2LEDS_H */
